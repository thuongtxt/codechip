/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : CliAtModuleAps.c
 *
 * Created Date: Jun 10, 2013
 *
 * Description : APS module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDevice.h"
#include "CliAtModuleAps.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char * cCmdApsEngineStateStr[] = {"unknown", "stop", "start"};
static const uint32 cCmdApsEngineStateVal[] = {cAtApsEngineStateUnknown,
                                               cAtApsEngineStateStop,
                                               cAtApsEngineStateStart};

static const char * cAtApsSwitchTypeStr[] = {"non-revertive", "revertive"};
static const uint32 cAtApsSwitchTypeVal[] = {cAtApsSwitchTypeNonRev, cAtApsSwitchTypeRev};

static const char * cAtApsSwitchingConditionStr[] = {"none", "sd", "sf"};
static const uint32 cAtApsSwitchingConditionVal[] = {cAtApsSwitchingConditionNone,
                                                     cAtApsSwitchingConditionSd,
                                                     cAtApsSwitchingConditionSf};

static const char * cAtApsRequestStateStr[] = {"unknown",
                                               "lp",
                                               "fs",
                                               "sf",
                                               "sd",
                                               "ms",
                                               "wtr",
                                               "nr"};
static const uint32 cAtApsRequestStateVal[] = {cAtApsRequestStateUnknown,
                                               cAtApsRequestStateLp,
                                               cAtApsRequestStateFs,
                                               cAtApsRequestStateSf,
                                               cAtApsRequestStateSd,
                                               cAtApsRequestStateMs,
                                               cAtApsRequestStateWtr,
                                               cAtApsRequestStateNr};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool InterruptEnable(char argc, char **argv, eBool enable)
    {
    eAtRet ret = AtModuleInterruptEnable((AtModule)CliAtApsModule(), enable);

    AtUnused(argv);
    AtUnused(argc);

    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "Cannot %s interrupt, ret = %s\n", enable ? "enable" : "disable", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

const char *CliAtApsSwitchingConditionToString(eAtApsSwitchingCondition switchingCondition)
    {
    return CliEnumToString(switchingCondition,
                           cAtApsSwitchingConditionStr,
                           cAtApsSwitchingConditionVal, mCount(cAtApsSwitchingConditionVal),
                           NULL);
    }

eAtApsSwitchingCondition CliAtApsSwitchingConditionFromString(const char *string)
    {
    eBool success = cAtTrue;
    eAtApsSwitchingCondition condition;

    condition =  CliStringToEnum(string,
                                 cAtApsSwitchingConditionStr,
                                 cAtApsSwitchingConditionVal, mCount(cAtApsSwitchingConditionVal),
                                 &success);
    if (success)
        return condition;

    return cAtApsSwitchingConditionInvalid;
    }

void CliAtApsExpectedSwitchingConditionsPrint(void)
    {
    CliExpectedValuesPrint(cSevCritical, cAtApsSwitchingConditionStr, mCount(cAtApsSwitchingConditionVal));
    }

const char *CliAtApsEngineStateToString(eAtApsEngineState state)
    {
    return CliEnumToString(state,
                           cCmdApsEngineStateStr,
                           cCmdApsEngineStateVal,
                           mCount(cCmdApsEngineStateVal),
                           NULL);
    }

eAtApsSwitchType CliAtApsSwitchTypeFromString(const char *string)
    {
    eBool success = cAtTrue;
    eAtApsSwitchType swType;
    swType = CliStringToEnum(string, cAtApsSwitchTypeStr, cAtApsSwitchTypeVal, mCount(cAtApsSwitchTypeVal), &success);
    if (success)
        return swType;
    return cAtApsSwitchTypeUnknown;
    }

const char * CliAtApsSwitchTypeToString(eAtApsSwitchType swType)
    {
    return CliEnumToString(swType, cAtApsSwitchTypeStr, cAtApsSwitchTypeVal, mCount(cAtApsSwitchTypeVal), NULL);
    }

const char *CliAtApsRequestStateToString(eAtApsRequestState state)
    {
    return CliEnumToString(state,
                           cAtApsRequestStateStr,
                           cAtApsRequestStateVal,
                           mCount(cAtApsRequestStateVal),
                           NULL);
    }

void CliAtApsExpectedSwitchTypesPrint(void)
    {
    CliExpectedValuesPrint(cSevCritical, cAtApsSwitchTypeStr, mCount(cAtApsSwitchTypeVal));
    }

AtModuleAps CliAtApsModule(void)
    {
    return (AtModuleAps)AtDeviceModuleGet(CliDevice(), cAtModuleAps);
    }

AtApsEngine *CliAtApsValidEnginesFromString(char *engineString, uint32 *numEngines,
                                            AtApsEngine (*EngineGetFunc)(AtModuleAps apsModule, uint32 engineId))
    {
    uint32 idBufferSize, numberIds, engine_i;
    uint32 *engineIds, numValidEngines = 0;
    uint32 engineBufferSize;
    AtApsEngine *engines = (AtApsEngine *)CliSharedChannelListGet(&engineBufferSize);
    AtModuleAps apsModule = CliAtApsModule();

    /* Have ID list */
    engineIds = CliSharedIdBufferGet(&idBufferSize);
    numberIds = CliIdListFromString(engineString, engineIds, idBufferSize);
    if (engineIds == NULL)
        return NULL;

    /* Only take valid engines */
    for (engine_i = 0; engine_i < numberIds; engine_i++)
        {
        AtApsEngine engine = EngineGetFunc(apsModule, engineIds[engine_i] - 1);

        if (engine)
            {
            engines[numValidEngines] = engine;
            numValidEngines = numValidEngines + 1;
            }

        if (numValidEngines >= engineBufferSize)
            break;
        }

    if (numEngines)
        *numEngines = numValidEngines;

    return engines;
    }

eBool CmdAtModuleApsDebug(char argc, char **argv)
    {
    AtUnused(argv);
    AtUnused(argc);
    AtModuleDebug((AtModule)CliAtApsModule());
    return cAtTrue;
    }

eBool CmdAtModuleApsInterruptEnable(char argc, char **argv)
    {
    return InterruptEnable(argc, argv, cAtTrue);
    }

eBool CmdAtModuleApsInterruptDisable(char argc, char **argv)
    {
    return InterruptEnable(argc, argv, cAtFalse);
    }

eBool CmdAtModuleApsShow(char argc, char **argv)
    {
    tTab *tabPtr;
    uint32 row = 0;
    const char *heading[] = {"Attribute", "Value"};
    AtModuleAps apsModule = CliAtApsModule();
    eBool enabled;
    const uint32 cNumRows = 6;

    AtUnused(argc);
    AtUnused(argv);

    /* Create table */
    tabPtr = TableAlloc(cNumRows, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    enabled = AtModuleInterruptIsEnabled((AtModule)apsModule);
    StrToCell(tabPtr, row, 0, "Interrupt");
    StrToCell(tabPtr, row, 1, CliBoolToString(enabled));
    row = row + 1;

    StrToCell(tabPtr, row, 0, "numLinearApsEngines");
    StrToCell(tabPtr, row, 1, CliNumber2String(AtModuleApsMaxNumLinearEngines(apsModule), "%u"));
    row = row + 1;

    StrToCell(tabPtr, row, 0, "numUpsrEngines");
    StrToCell(tabPtr, row, 1, CliNumber2String(AtModuleApsMaxNumUpsrEngines(apsModule), "%u"));
    row = row + 1;

    StrToCell(tabPtr, row, 0, "numApsGroups");
    StrToCell(tabPtr, row, 1, CliNumber2String(AtModuleApsMaxNumGroups(apsModule), "%u"));
    row = row + 1;

    StrToCell(tabPtr, row, 0, "numApsSelectors");
    StrToCell(tabPtr, row, 1, CliNumber2String(AtModuleApsMaxNumSelectors(apsModule), "%u"));
    row = row + 1;

    StrToCell(tabPtr, row, 0, "numStsGroups");
    StrToCell(tabPtr, row, 1, CliNumber2String(AtModuleApsMaxNumStsGroups(apsModule), "%u"));
    row = row + 1;

    TablePrint(tabPtr);
    TableFree(tabPtr);

    AtAssert(row == cNumRows); /* To avoid common mistake */

    return cAtTrue;
    }

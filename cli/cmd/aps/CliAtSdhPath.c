/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : CliAtSdhPath.c
 *
 * Created Date: Oct 24, 2016
 *
 * Description : CLIs to handle APS associations on Path
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtCliModule.h"
#include "AtApsEngine.h"
#include "AtApsSelector.h"
#include "../sdh/CliAtModuleSdh.h"
#include "CliAtModuleAps.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 *PathSelectors(AtSdhPath path, uint32 *numSelectors)
    {
    uint32 idBufferSize;
    uint32 *idList = CliSharedIdBufferGet(&idBufferSize);
    uint32 selector_i = 0;
    AtList selectors = AtSdhPathApsSelectorsGet(path);
    AtIterator iterator;
    AtApsSelector selector;

    if (numSelectors)
        *numSelectors = AtListLengthGet(selectors);

    if (selectors == NULL)
        return NULL;

    iterator = AtListIteratorCreate(selectors);

    while ((selector = (AtApsSelector)AtIteratorNext(iterator)) != NULL)
        {
        idList[selector_i] = AtApsSelectorIdGet(selector) + 1;
        selector_i = selector_i + 1;
        }

    AtObjectDelete((AtObject)iterator);

    return idList;
    }

static void SwitchingConditionToCell(tTab *tabPtr, uint32 row, uint32 column, eAtApsSwitchingCondition swCondition)
    {
    const char *conditionString = CliAtApsSwitchingConditionToString(swCondition);
    ColorStrToCell(tabPtr, row, column, conditionString ? conditionString : "Error", CliAtApsSwitchingConditionColor(swCondition));
    }

static void PutSwitchingConditionToCell(tTab *tabPtr, uint32 row, uint32 column, AtSdhPath path, eAtSdhPathAlarmType alarmType)
    {
    SwitchingConditionToCell(tabPtr, row, column, AtSdhPathApsSwitchingConditionGet(path, alarmType));
    }

eBool CmdAtSdhPathApsEngineShow(char argc, char **argv)
    {
    uint32 numberPaths, path_i;
    AtSdhPath *paths;
    tTab *tabPtr;
    const char *pHeading[] = {"PathId", "APS Engine"};

    AtUnused(argc);

    /* Get list of path */
    paths = CliSdhPathFromArgumentGet(argv[0], &numberPaths);
    if (numberPaths == 0)
        {
        AtPrintc(cSevNormal, "Error: ID is wrong\r\n");
        return cAtTrue;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numberPaths, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (path_i = 0; path_i < numberPaths; path_i++)
        {
        AtChannel path = (AtChannel)paths[path_i];
        AtApsEngine engine = AtSdhPathApsEngineGet(paths[path_i]);
        uint32 column = 0;

        StrToCell(tabPtr, path_i, column++, CliChannelIdStringGet(path));
        ColorStrToCell(tabPtr, path_i, column++,
                       (engine == NULL) ? "none" : CliNumber2String(AtApsEngineIdGet(engine) + 1, "%u"),
                       (engine == NULL) ? cSevNormal : cSevInfo);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtSdhPathApsSelectorsShow(char argc, char **argv)
    {
    uint32 numPaths, path_i;
    AtSdhPath *paths;
    tTab *tabPtr;
    const char *pHeading[] = {"PathId", "APS Selectors"};

    AtUnused(argc);

    /* Get list of path */
    paths = CliSdhPathFromArgumentGet(argv[0], &numPaths);
    if (numPaths == 0)
        {
        AtPrintc(cSevNormal, "Error: ID is wrong\r\n");
        return cAtTrue;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numPaths, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (path_i = 0; path_i < numPaths; path_i++)
        {
        uint32 numSelectors;
        AtChannel path = (AtChannel)paths[path_i];
        uint32 *selectors;
        uint32 column = 0;
        const char *strValue;
        char idString[128];

        StrToCell(tabPtr, path_i, column++, CliChannelIdStringGet(path));

        selectors = PathSelectors(paths[path_i], &numSelectors);
        strValue = AtStdNumbersToString(AtStdSharedStdGet(), idString, sizeof(idString), selectors, numSelectors);
        ColorStrToCell(tabPtr, path_i, column++, strValue ? strValue : "none", strValue ? cSevInfo : cSevNormal);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtSdhPathApsSwitchingConditionSet(char argc, char** argv)
    {
    AtSdhPath *pathList;
    uint32 numberOfPaths, path_i;
    uint32 defects = 0;
    eAtApsSwitchingCondition condition;
    eBool success = cAtTrue;
    AtUnused(argc);

    /* Get list of engines */
    pathList = CliSdhPathFromArgumentGet(argv[0], &numberOfPaths);
    if (numberOfPaths == 0)
        {
        AtPrintc(cSevNormal, "Error: ID is wrong\r\n");
        return cAtFalse;
        }

    defects = CliSdhPathAlarmMaskFromString(argv[1]);
    condition = CliAtApsSwitchingConditionFromString(argv[2]);
    if (condition == cAtApsSwitchingConditionInvalid)
        {
        AtPrintc(cSevCritical, "ERROR: Please input correct switching condition, expected: ");
        CliAtApsExpectedSwitchingConditionsPrint();
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    for (path_i = 0; path_i < numberOfPaths; path_i++)
        {
        eAtRet ret = AtSdhPathApsSwitchingConditionSet(pathList[path_i], defects, condition);
        if (ret != cAtOk)
            {
            mWarnNotReturnCliIfNotOk(ret, pathList[path_i], "Cannot configure path switching condition");
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtSdhPathApsSwitchingConditionShow(char argc, char **argv)
    {
    tTab *tabPtr;
    const char *heading[] ={"PathId", "AIS", "LOP", "TIM", "UNEQ", "PLM", "BER-SD", "BER-SF", "Forced-condition"};
    AtSdhPath *pathList;
    uint32 numberOfPaths, i;
    AtUnused(argc);

    /* Get list of engines */
    pathList = CliSdhPathFromArgumentGet(argv[0], &numberOfPaths);
    if (numberOfPaths == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No engine, the ID List may be wrong, expect 1,2-3,...\r\n");
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numberOfPaths, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\n");
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numberOfPaths; i++)
        {
        AtSdhPath path = pathList[i];
        uint8 column = 0;

        StrToCell(tabPtr, i, column++, CliChannelIdStringGet((AtChannel)path));

        PutSwitchingConditionToCell(tabPtr, i, column++, path, cAtSdhPathAlarmAis);
        PutSwitchingConditionToCell(tabPtr, i, column++, path, cAtSdhPathAlarmLop);
        PutSwitchingConditionToCell(tabPtr, i, column++, path, cAtSdhPathAlarmTim);
        PutSwitchingConditionToCell(tabPtr, i, column++, path, cAtSdhPathAlarmUneq);
        PutSwitchingConditionToCell(tabPtr, i, column++, path, cAtSdhPathAlarmPlm);
        PutSwitchingConditionToCell(tabPtr, i, column++, path, cAtSdhPathAlarmBerSd);
        PutSwitchingConditionToCell(tabPtr, i, column++, path, cAtSdhPathAlarmBerSf);

        SwitchingConditionToCell(tabPtr, i, column++, AtSdhPathApsForcedSwitchingConditionGet(path));
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtSdhPathApsForcedSwitchingConditionSet(char argc, char** argv)
    {
    AtSdhPath *pathList;
    uint32 numberOfPaths, path_i;
    eAtApsSwitchingCondition condition;
    eBool success = cAtTrue;
    AtUnused(argc);

    /* Get list of engines */
    pathList = CliSdhPathFromArgumentGet(argv[0], &numberOfPaths);
    if (numberOfPaths == 0)
        {
        AtPrintc(cSevNormal, "Error: ID is wrong\r\n");
        return cAtFalse;
        }

    condition = CliAtApsSwitchingConditionFromString(argv[1]);
    if (condition == cAtApsSwitchingConditionInvalid)
        {
        AtPrintc(cSevCritical, "ERROR: Please input correct switching condition, expected: ");
        CliAtApsExpectedSwitchingConditionsPrint();
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    for (path_i = 0; path_i < numberOfPaths; path_i++)
        {
        eAtRet ret = AtSdhPathApsForcedSwitchingConditionSet(pathList[path_i], condition);
        if (ret != cAtOk)
            {
            mWarnNotReturnCliIfNotOk(ret, pathList[path_i], "Cannot configure path switching condition");
            success = cAtFalse;
            }
        }

    return success;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : CliAtApsLinearEngine.c
 *
 * Created Date: Sep 17, 2016
 *
 * Description : Linear APS engine CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDevice.h"
#include "AtModuleSdh.h"
#include "CliAtModuleAps.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef eAtModuleApsRet (*AttributeSetFunc)(AtApsLinearEngine, uint32 value);
typedef eAtModuleApsRet (*NoParameterFunction)(AtApsEngine);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char * cCmdApsSwitchingTypeStr[] = {"non-revertive", "revertive"};
static const uint32 cCmdApsSwitchingTypeVal[] = {cAtApsLinearSwitchTypeNonRev, cAtApsLinearSwitchTypeRev};

static const char * cCmdApsArchitectureStr[] = {"1+1", "1:n"};
static const uint32 cCmdApsArchitectureVal[] = {cAtApsLinearArch11, cAtApsLinearArch1n};

static const char * cCmdApsOpModeStr[] = {"uni", "bid"};
static const uint32 cCmdApsOpModeVal[] = {cAtApsLinearOpModeUni, cAtApsLinearOpModeBid};

static const char * cCmdApsExtCmdStr[] = {"clear", "exer", "ms", "fs", "lp"};
static const uint32 cCmdApsExtCmdVal[] = {cAtApsLinearExtCmdClear,
                                          cAtApsLinearExtCmdExer,
                                          cAtApsLinearExtCmdMs,
                                          cAtApsLinearExtCmdFs,
                                          cAtApsLinearExtCmdLp};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleSdh SdhModule(void)
    {
    return (AtModuleSdh)AtDeviceModuleGet(CliDevice(), cAtModuleSdh);
    }

static AtSdhLine LineGet(AtModuleSdh sdhModule, uint32 lineId)
    {
    return AtModuleSdhLineGet(sdhModule, (uint8)lineId);
    }

static eAtRet ApsNormalSetupForLine(AtSdhLine line)
    {
    uint32 alarmMask = cAtSdhLineAlarmLos   |
                       cAtSdhLineAlarmLof   |
                       cAtSdhLineAlarmAis   |
                       cAtSdhLineAlarmBerSd |
                       cAtSdhLineAlarmBerSf;
    return AtChannelInterruptMaskSet((AtChannel)line, alarmMask, alarmMask);
    }

static eAtRet WorkingLinesSetup(AtSdhLine *lines, uint8 numLines)
    {
    uint8 i;
    eAtRet ret = cAtOk;

    for (i = 0; i < numLines; i++)
        ret |= ApsNormalSetupForLine(lines[i]);

    return ret;
    }

static eAtRet ProtectionLineSetup(AtSdhLine line)
    {
    eAtRet ret = cAtOk;
    uint32 mask;

    ret  |= ApsNormalSetupForLine(line);
    mask  = AtChannelInterruptMaskGet((AtChannel)line);
    mask |= cAtSdhLineAlarmKByteChange;
    ret  |= AtChannelInterruptMaskSet((AtChannel)line, mask, mask);

    return ret;
    }

static uint32 LineIdsFromString(char *lineListString, uint32 *lineIds, uint32 bufferSize)
    {
    uint32 numberOfLines, i;
    uint32 numValidLines = 0;

    numberOfLines = CliIdListFromString(lineListString, lineIds, bufferSize);
    if (numberOfLines == 0)
        return 0;

    for (i = 0; i < numberOfLines; i++)
        {
        if (LineGet(SdhModule(), lineIds[i] - 1) == NULL)
            continue;

        lineIds[numValidLines] = lineIds[i];
        numValidLines = numValidLines + 1;
        }

    return numValidLines;
    }

static AtSdhLine ProtectionLineFromString(char *lineIdString)
    {
    uint32 *lineIds;
    uint32 bufferSize, numberOfLines;
    AtSdhLine line;

    /* Get protection line */
    lineIds = CliSharedIdBufferGet(&bufferSize);
    numberOfLines = CliIdListFromString(lineIdString, lineIds, bufferSize);
    if (numberOfLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No protection line, the ID List may be wrong, expect 1,2-3,...\r\n");
        return NULL;
        }

    /* Just one is allowed */
    if (numberOfLines > 1)
        {
        AtPrintc(cSevCritical, "ERROR: Only one protection line is allowed\r\n");
        return cAtFalse;
        }

    line = LineGet(SdhModule(), lineIds[0] - 1);
    if (line == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Protection line %d does not exist.\r\n", lineIds[0]);
        return NULL;
        }

    return line;
    }

static eAtRet ApsGlobalSetup(void)
    {
    return AtModuleInterruptEnable((AtModule)SdhModule(), cAtTrue);
    }

static eBool AttributeSet(char argc, char **argv, uint32 value, AttributeSetFunc attributeSetFunc)
    {
    uint32 *engineIds;
    uint32 bufferSize, numberOfEngine, i;
    AtApsLinearEngine engine;
    eAtRet ret = cAtOk;
    AtUnused(argc);

    /* Get list of lines */
    engineIds = CliSharedIdBufferGet(&bufferSize);
    numberOfEngine = CliIdListFromString(argv[0], engineIds, bufferSize);
    if (engineIds == NULL )
        {
        AtPrintc(cSevCritical, "ERROR: No engine, the ID List may be wrong, expect 1,2-3,...\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numberOfEngine; i++)
        {
        engine = (AtApsLinearEngine)AtModuleApsLinearEngineGet(CliAtApsModule(), engineIds[i] - 1);
        ret |= attributeSetFunc(engine, value);
        }

    return (ret == cAtOk) ? cAtTrue : cAtFalse;
    }

static eBool LinearEngineNoParameterProcess(char argc, char **argv, NoParameterFunction Func)
    {
    uint32 *engineIds;
    uint32 bufferSize, numberOfEngine, i;
    AtApsEngine engine;
    AtUnused(argc);

    /* Get list of lines */
    engineIds = CliSharedIdBufferGet(&bufferSize);
    numberOfEngine = CliIdListFromString(argv[0], engineIds, bufferSize);
    if (engineIds == NULL )
        {
        AtPrintc(cSevCritical, "ERROR: No engine, the ID List may be wrong, expect 1,2-3,...\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numberOfEngine; i++)
        {
        engine = AtModuleApsLinearEngineGet(CliAtApsModule(), (uint8)(engineIds[i] - 1));
        Func(engine);
        }

    return cAtTrue;
    }

eBool CmdAtModuleApsLinearEngineCreate(char argc, char **argv)
    {
    uint32 *lineIds;
    uint32 bufferSize, i;
    uint32 numberOfWorkingLines;
    AtSdhLine *lines;
    eAtApsLinearArch arch = cAtApsLinearArch11;
    eBool convertSuccess;
    AtApsEngine engine;
    uint8 engineId;
    eAtRet ret = cAtOk;
    eBool success = cAtTrue;
    AtUnused(argc);

    /* Get engine ID */
    engineId = (uint8)AtStrToDw(argv[0]);
    if (engineId > AtModuleApsMaxNumLinearEngines(CliAtApsModule()))
        {
        AtPrintc(cSevCritical,
                 "ERROR: invalid engine ID, its range is 1-%d\r\n",
                 AtModuleApsMaxNumLinearEngines(CliAtApsModule()));
        return cAtFalse;
        }
    engineId = (uint8)(engineId - 1);

    /* Get working lines */
    lineIds = CliSharedIdBufferGet(&bufferSize);
    numberOfWorkingLines = LineIdsFromString(argv[1], lineIds, bufferSize);
    if (lineIds == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: No working line, the ID List may be wrong, expect 1,2-3,...\r\n");
        return cAtFalse;
        }

    /* Allocate memory for all input lines */
    lines = AtOsalMemAlloc((numberOfWorkingLines + 1) * sizeof(AtSdhLine));
    if (lines == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate memory\r\n");
        return cAtFalse;
        }

    /* Save them */
    for (i = 0; i < numberOfWorkingLines; i++)
        lines[i + 1] = LineGet(SdhModule(), lineIds[i] - 1);

    /* Get protection line */
    lines[0] = ProtectionLineFromString(argv[2]);
    if (lines[0] == NULL)
        {
        AtOsalMemFree(lines);
        return cAtFalse;
        }

    /* Architecture */
    arch = CliStringToEnum(argv[3], cCmdApsArchitectureStr, cCmdApsArchitectureVal, mCount(cCmdApsArchitectureVal), &convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid architecture, expected: ");
        CliExpectedValuesPrint(cSevCritical, cCmdApsArchitectureStr, mCount(cCmdApsArchitectureVal));
        AtPrintc(cSevCritical, "\r\n");
        AtOsalMemFree(lines);
        return cAtFalse;
        }

    /* Create and start engine */
    engine = AtModuleApsLinearEngineCreate(CliAtApsModule(), engineId, lines, (uint8)(numberOfWorkingLines + 1), arch);
    if (engine == NULL)
        {
        AtOsalMemFree(lines);
        return cAtFalse;
        }

    /* Common setting before starting engine */
    ret |= ApsGlobalSetup();
    ret |= WorkingLinesSetup(&(lines[1]), (uint8)numberOfWorkingLines);
    ret |= ProtectionLineSetup(lines[0]);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Cannot setup working lines or protection line, ret = %s\r\n",
                 AtRet2String(ret));
        success = cAtFalse;
        }

    /* Clean up */
    AtOsalMemFree(lines);

    return success;
    }

eBool CmdAtModuleApsLinearEngineDelete(char argc, char **argv)
    {
    uint32 *engineIds;
    uint32 bufferSize, numberOfEngine, i;
    eBool success = cAtTrue;
    AtUnused(argc);

    /* Get list of engines */
    engineIds = CliSharedIdBufferGet(&bufferSize);
    numberOfEngine = CliIdListFromString(argv[0], engineIds, bufferSize);
    if (engineIds == NULL )
        {
        AtPrintc(cSevCritical, "ERROR: No engine, the ID List may be wrong, expect 1,2-3,...\r\n");
        return cAtFalse;
        }

    /* Delete all of them */
    for (i = 0; i < numberOfEngine; i++)
        {
        eAtRet ret = AtModuleApsLinearEngineDelete(CliAtApsModule(), (uint8)(engineIds[i] - 1));
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot delete engine %d, ret = %s\r\n", engineIds[i], AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtModuleApsLinearEnginesShow(char argc, char **argv)
    {
    tTab *tabPtr;
    const char *heading[] ={"EngineId", "State", "Architecture", "OperationMode",
                            "SwitchingType", "ExternalCommand", "SwitchChannel", "RequestState"};
    uint32 numEngines = 0, engine_i;
    AtApsEngine *engines = CliAtApsValidEnginesFromString(argv[0], &numEngines, AtModuleApsLinearEngineGet);
    AtUnused(argc);

    /* Get list of engines */
    if (numEngines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No engine, the ID List may be wrong, expect 1,2-3,...\r\n");
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numEngines, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\n");
        return cAtFalse;
        }

    /* Make table content */
    for (engine_i = 0; engine_i < numEngines; engine_i++)
        {
        AtApsEngine engine = engines[engine_i];
        eAtApsEngineState state;
        AtChannel channel;
        uint16 column = 0;
        const char *strValue;
        eAtApsRequestState requestState;

        StrToCell(tabPtr, engine_i, column++, CliNumber2String(AtChannelIdGet((AtChannel)engine) + 1, "%d"));

        state = AtApsEngineStateGet(engine);
        strValue = CliAtApsEngineStateToString(AtApsEngineStateGet(engine));
        ColorStrToCell(tabPtr, engine_i, column++, strValue ? strValue : "Error", (state == cAtApsEngineStateStop) ? cSevCritical : cSevNormal);

        strValue = CliEnumToString(AtApsLinearEngineArchGet((AtApsLinearEngine)engine),
                                   cCmdApsArchitectureStr,
                                   cCmdApsArchitectureVal,
                                   mCount(cCmdApsArchitectureVal),
                                   NULL);
        StrToCell(tabPtr, engine_i, column++, strValue ? strValue : "Error");

        strValue = CliEnumToString(AtApsLinearEngineOpModeGet((AtApsLinearEngine)engine),
                                   cCmdApsOpModeStr,
                                   cCmdApsOpModeVal,
                                   mCount(cCmdApsOpModeVal),
                                   NULL);
        StrToCell(tabPtr, engine_i, column++, strValue ? strValue : "Error");

        strValue = CliEnumToString(AtApsLinearEngineSwitchTypeGet((AtApsLinearEngine)engine),
                                   cCmdApsSwitchingTypeStr,
                                   cCmdApsSwitchingTypeVal,
                                   mCount(cCmdApsSwitchingTypeVal),
                                   NULL);
        StrToCell(tabPtr, engine_i, column++, strValue ? strValue : "Error");

        strValue = CliEnumToString(AtApsLinearEngineExtCmdGet((AtApsLinearEngine)engine),
                                   cCmdApsExtCmdStr,
                                   cCmdApsExtCmdVal,
                                   mCount(cCmdApsExtCmdVal), NULL);
        StrToCell(tabPtr, engine_i, column++, strValue ? strValue : "Error");

        channel = (AtChannel)AtApsLinearEngineSwitchLineGet((AtApsLinearEngine)engine);
        StrToCell(tabPtr, engine_i, column++, channel ? CliNumber2String(AtChannelIdGet(channel) + 1, "%d") : "none");

        requestState = AtApsEngineRequestStateGet(engine);
        strValue = CliAtApsRequestStateToString(requestState);
        StrToCell(tabPtr, engine_i, column++, strValue ? strValue : "error");
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtApsLinearEngineStart(char argc, char **argv)
    {
    return LinearEngineNoParameterProcess(argc, argv, AtApsEngineStart);
    }

eBool CmdAtApsLinearEngineStop(char argc, char** argv)
    {
    return LinearEngineNoParameterProcess(argc, argv, AtApsEngineStop);
    }

eBool CmdAtApsLinearEngineDebug(char argc, char** argv)
    {
    return LinearEngineNoParameterProcess(argc, argv, (NoParameterFunction)AtChannelDebug);
    }

eBool CmdAtApsLinearEngineSwitchingTypeSet(char argc, char** argv)
    {
    uint32 value = CliStringToEnum(argv[1], cCmdApsSwitchingTypeStr, cCmdApsSwitchingTypeVal, mCount(cCmdApsSwitchingTypeVal), NULL);
    return AttributeSet(argc, argv, value, (AttributeSetFunc)AtApsLinearEngineSwitchTypeSet);
    }

eBool CmdAtApsLinearEngineOperationMode(char argc, char** argv)
    {
    uint32 value = CliStringToEnum(argv[1], cCmdApsOpModeStr, cCmdApsOpModeVal, mCount(cCmdApsOpModeVal), NULL);
    return AttributeSet(argc, argv, value, (AttributeSetFunc)AtApsLinearEngineOpModeSet);
    }

eBool CmdAtApsLinearEngineArchitectureMode(char argc, char** argv)
    {
    uint32 value = CliStringToEnum(argv[1], cCmdApsArchitectureStr, cCmdApsArchitectureVal, mCount(cCmdApsArchitectureVal), NULL);
    return AttributeSet(argc, argv, value, (AttributeSetFunc)AtApsLinearEngineArchSet);
    }

eBool CmdAtApsLinearEngineExternalCmd(char argc, char** argv)
    {
    uint32 *engineIds;
    uint32 bufferSize, numberOfEngine, i;
    AtSdhLine lineAffected;
    eAtApsLinearExtCmd extlCmd;
    eBool success = cAtTrue;
    eBool convertSuccess;

    AtUnused(argc);

    /* Get list of lines */
    engineIds = CliSharedIdBufferGet(&bufferSize);
    numberOfEngine = CliIdListFromString(argv[0], engineIds, bufferSize);
    if (engineIds == NULL )
        {
        AtPrintc(cSevCritical, "ERROR: No engine, the ID List may be wrong, expect 1,2-3,...\r\n");
        return cAtFalse;
        }

    lineAffected = LineGet((AtModuleSdh)AtDeviceModuleGet(CliDevice(), cAtModuleSdh), AtStrToDw(argv[1]) - 1);
    if (lineAffected == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: No line affected, the ID List may be wrong, expect 1,2-3,...\r\n");
        return cAtFalse;
        }

    extlCmd = CliStringToEnum(argv[2], cCmdApsExtCmdStr, cCmdApsExtCmdVal, mCount(cCmdApsExtCmdVal), &convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Unknown external command. Expect: ");
        CliExpectedValuesPrint(cSevCritical, cCmdApsExtCmdStr, mCount(cCmdApsExtCmdVal));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numberOfEngine; i++)
        {
        AtApsLinearEngine engine = (AtApsLinearEngine)AtModuleApsLinearEngineGet(CliAtApsModule(), engineIds[i] - 1);
        eAtRet ret = AtApsLinearEngineExtCmdSet(engine, extlCmd, lineAffected);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Issue external command %s on engine %d with affected line %s fail with ret = %s\r\n",
                     argv[2],
                     engineIds[i],
                     CliChannelIdStringGet((AtChannel)lineAffected),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

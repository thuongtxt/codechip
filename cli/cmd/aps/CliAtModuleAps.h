/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : CliAtModuleAps.h
 * 
 * Created Date: Sep 17, 2016
 *
 * Description : APS module CLIs
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLIATMODULEAPS_H_
#define _CLIATMODULEAPS_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCli.h"
#include "AtModuleAps.h"
#include "AtApsEngine.h"
#include "AtApsUpsrEngine.h"
#include "AtApsLinearEngine.h"
#include "AtApsGroup.h"
#include "AtApsSelector.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef eAtModuleApsRet (*ApsEngineAttributeSetFunc)(AtApsEngine, uint32 value);

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleAps CliAtApsModule(void);

AtApsEngine *CliAtApsValidEnginesFromString(char *engineString, uint32 *numEngines,
                                            AtApsEngine (*EngineGetFunc)(AtModuleAps apsModule, uint32 engineId));
const char *CliAtApsEngineStateToString(eAtApsEngineState state);

const char *CliAtApsSwitchingConditionToString(eAtApsSwitchingCondition switchingCondition);
void CliAtApsExpectedSwitchingConditionsPrint(void);
eAtApsSwitchingCondition CliAtApsSwitchingConditionFromString(const char *string);

eAtApsSwitchType CliAtApsSwitchTypeFromString(const char *string);
const char * CliAtApsSwitchTypeToString(eAtApsSwitchType swType);
const char *CliAtApsRequestStateToString(eAtApsRequestState state);
void CliAtApsExpectedSwitchTypesPrint(void);

eAtSevLevel CliAtApsSwitchingConditionColor(eAtApsSwitchingCondition swCondition);

#ifdef __cplusplus
}
#endif
#endif /* _CLIATMODULEAPS_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : CliAtApsUpsrEngine.c
 *
 * Created Date: Sep 17, 2016
 *
 * Description : UPSR engine CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtModuleAps.h"
#include "../sdh/CliAtModuleSdh.h"
#include "AtCliModule.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char * cAtApsUpsrExtCmdStr[] = {"clear", "fs2wrk", "fs2prt", "ms2wrk", "ms2prt", "lp"};
static const uint32 cAtApsUpsrExtCmdVal[] = {cAtApsUpsrExtCmdClear,
                                             cAtApsUpsrExtCmdFs2Wrk,
                                             cAtApsUpsrExtCmdFs2Prt,
                                             cAtApsUpsrExtCmdMs2Wrk,
                                             cAtApsUpsrExtCmdMs2Prt,
                                             cAtApsUpsrExtCmdLp};

static const char *cAtApsUpsrEngineEventTypeStr[] =
    {
     "switch",
     "state-change"
    };

static const uint32 cAtApsUpsrEngineEventTypeVal[] =
    {
     cAtApsEngineEventSwitch,
     cAtApsEngineEventStateChange
    };

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSdhPath* SdhPathFromString(char* arg, uint32 *numberPaths)
    {
    uint32 bufferSize;
    AtChannel *paths = CliSharedChannelListGet(&bufferSize);
    *numberPaths = CliChannelListFromString(arg, paths, bufferSize);
    return (AtSdhPath *)paths;
    }

static AtApsEngine *ApsUpsrEngineListFromString(char *engineString, uint32 *numEngines)
    {
    return CliAtApsValidEnginesFromString(engineString, numEngines, AtModuleApsUpsrEngineGet);
    }

static eBool AttributeSet(char *engineIdString, uint32 value, ApsEngineAttributeSetFunc attributeSetFunc)
    {
    AtApsEngine *engineList;
    uint32 numberOfEngine, i;
    eBool success = cAtTrue;

    /* Get list of engines */
    engineList = ApsUpsrEngineListFromString(engineIdString, &numberOfEngine);
    if (numberOfEngine == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No engine, the ID List may be wrong, expect 1,2-3,...\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numberOfEngine; i++)
        {
        eAtRet ret = cAtOk;

        ret = attributeSetFunc(engineList[i], value);
        if (ret != cAtOk)
            {
            mWarnNotReturnCliIfNotOk(ret, engineList[i], "Cannot apply configuration");
            success = cAtFalse;
            }
        }

    return success;
    }

static eBool NoParamSet(char argc, char **argv, eAtModuleApsRet (*Func)(AtApsEngine self))
    {
    AtApsEngine *engineList;
    uint32 numberOfEngine, engine_i;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get list of engines */
    engineList = ApsUpsrEngineListFromString(argv[0], &numberOfEngine);
    if (engineList == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: No engine, the ID List may be wrong, expect 1,2-3,...\r\n");
        return cAtFalse;
        }

    for (engine_i = 0; engine_i < numberOfEngine; engine_i++)
        {
        eAtRet ret;

        ret = Func(engineList[engine_i]);
        if (ret != cAtOk)
            {
            mWarnNotReturnCliIfNotOk(ret, engineList[engine_i], "Cannot apply configuration");
            success = cAtFalse;
            }
        }

    return success;
    }

eAtSevLevel CliAtApsSwitchingConditionColor(eAtApsSwitchingCondition swCondition)
    {
    switch (swCondition)
        {
        case cAtApsSwitchingConditionNone   : return cSevNormal;
        case cAtApsSwitchingConditionSd     : return cSevWarning;
        case cAtApsSwitchingConditionSf     : return cSevCritical;
        case cAtApsSwitchingConditionInvalid: return cSevCritical;
        default:
            return cSevNormal;
        }
    }

static void PutSwitchingConditionToCell(tTab *tabPtr, uint32 row, uint32 column, AtApsEngine engine, eAtSdhPathAlarmType alarmType)
    {
    eAtApsSwitchingCondition swCondition = AtApsEngineSwitchingConditionGet(engine, alarmType);
    const char *conditionString = CliAtApsSwitchingConditionToString(swCondition);
    ColorStrToCell(tabPtr, row, column, conditionString ? conditionString : "Error", CliAtApsSwitchingConditionColor(swCondition));
    }

static eAtModuleApsRet InterruptMaskEnable(AtApsEngine self, uint32 mask)
    {
    return AtChannelInterruptMaskSet((AtChannel)self, mask, mask);
    }

static eAtModuleApsRet InterruptMaskDisable(AtApsEngine self, uint32 mask)
    {
    return AtChannelInterruptMaskSet((AtChannel)self, mask, 0x0);
    }

static void AlarmChangeState(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    uint8 alarm_i;
    AtUnused(currentStatus);

    AtPrintc(cSevNormal, "\r\n%s [APS] Defect changed at engine %s", AtOsalDateTimeInUsGet(), CliChannelIdStringGet(channel));

    for (alarm_i = 0; alarm_i < mCount(cAtApsUpsrEngineEventTypeVal); alarm_i++)
        {
        uint32 alarmType = cAtApsUpsrEngineEventTypeVal[alarm_i];

        if ((changedAlarms & alarmType) == 0)
            continue;

        if (changedAlarms & alarmType & cAtApsEngineEventSwitch)
            AtPrintc(cSevNormal, " * [active:%s]", CliChannelIdStringGet((AtChannel)AtApsUpsrEngineActivePathGet((AtApsUpsrEngine)channel)));

        if (changedAlarms & alarmType & cAtApsEngineEventStateChange)
            AtPrintc(cSevNormal, " * [state:%s]", CliAtApsRequestStateToString(AtApsEngineRequestStateGet((AtApsEngine)channel)));
        }

    AtPrintc(cSevInfo, "\r\n");
    }

eBool CmdAtModuleApsUpsrEngineCreate(char argc, char **argv)
    {
    AtSdhPath *channel;
    AtSdhPath working;
    AtSdhPath protection;
    uint32 numWorkingChannels;
    uint32 numProtectionChannels;
    AtApsEngine engine;
    uint32 engineId;

    AtUnused(argc);

    /* Get engine ID */
    engineId = AtStrToDw(argv[0]);
    if (engineId > AtModuleApsMaxNumUpsrEngines(CliAtApsModule()))
        {
        AtPrintc(cSevCritical,
                 "ERROR: invalid engine ID, its range is 1-%d\r\n",
                 AtModuleApsMaxNumUpsrEngines(CliAtApsModule()));
        return cAtFalse;
        }
    engineId = engineId - 1;

    /* Get shared buffer, channel type and ID from argv[0], ex: aug1.1.1 -> channel type: aug1 and Id = 1.1 */
    /* Get Working path channel */
    channel = SdhPathFromString(argv[1], &numWorkingChannels);
    if (!channel)
        {
        AtPrintc(cSevCritical, "ERROR: Get Working Path Fail\r\n");
        return cAtFalse;
        }
    working = channel[0];

    /* Get Working path channel */
    channel = SdhPathFromString(argv[2], &numProtectionChannels);
    if (!channel)
        {
        AtPrintc(cSevCritical, "ERROR: Get Protection Path Fail\r\n");
        return cAtFalse;
        }
    protection = channel[0];

    /* Create and start engine */
    engine = AtModuleApsUpsrEngineCreate(CliAtApsModule(), engineId, working, protection);
    if (engine == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Create APS UPSR Engine Fail\r\n");
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAtModuleApsUpsrEngineDelete(char argc, char **argv)
    {
    uint32 *engineIds;
    uint32 bufferSize, numberOfEngine, i;
    eBool success = cAtTrue;
    AtUnused(argc);

    /* Get list of engines */
    engineIds = CliSharedIdBufferGet(&bufferSize);
    numberOfEngine = CliIdListFromString(argv[0], engineIds, bufferSize);
    if (engineIds == NULL )
        {
        AtPrintc(cSevCritical, "ERROR: No engine, the ID List may be wrong, expect 1,2-3,...\r\n");
        return cAtFalse;
        }

    /* Delete all of them */
    for (i = 0; i < numberOfEngine; i++)
        {
        eAtRet ret = AtModuleApsUpsrEngineDelete(CliAtApsModule(), (uint8)(engineIds[i] - 1));
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot delete engine %d, ret = %s\r\n", engineIds[i], AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtApsUpsrEngineStart(char argc, char **argv)
    {
    return NoParamSet(argc, argv, AtApsEngineStart);
    }

eBool CmdAtApsUpsrEngineStop(char argc, char** argv)
    {
    return NoParamSet(argc, argv, AtApsEngineStop);
    }

eBool CmdAtApsUpsrEngineDebug(char argc, char** argv)
    {
    AtApsEngine *engineList;
    uint32 numberOfEngine, i;

    AtUnused(argc);

    /* Get list of engines */
    engineList = ApsUpsrEngineListFromString(argv[0], &numberOfEngine);
    if (numberOfEngine == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No engine, the ID List may be wrong, expect 1,2-3,...\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numberOfEngine; i++)
        AtChannelDebug((AtChannel)engineList[i]);

    return cAtTrue;
    }

eBool CmdAtApsUpsrEngineWtrSet(char argc, char** argv)
    {
    uint32 value = AtStrToDw(argv[1]);
    AtUnused(argc);
    return AttributeSet(argv[0], value, (ApsEngineAttributeSetFunc)AtApsEngineWtrSet);
    }

eBool CmdAtApsUpsrEngineSwitchingConditionSet(char argc, char** argv)
    {
    AtApsEngine *engineList;
    uint32 numberOfEngine, engine_i;
    uint32 defects = 0;
    eAtApsSwitchingCondition condition;
    eBool success = cAtTrue;
    AtUnused(argc);

    /* Get list of engines */
    engineList = ApsUpsrEngineListFromString(argv[0], &numberOfEngine);
    if (numberOfEngine == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No engine, the ID List may be wrong, expect 1,2-3,...\r\n");
        return cAtFalse;
        }

    defects = CliSdhPathAlarmMaskFromString(argv[1]);
    condition = CliAtApsSwitchingConditionFromString(argv[2]);
    if (condition == cAtApsSwitchingConditionInvalid)
        {
        AtPrintc(cSevCritical, "ERROR: Please input correct switching condition, expected: ");
        CliAtApsExpectedSwitchingConditionsPrint();
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    for (engine_i = 0; engine_i < numberOfEngine; engine_i++)
        {
        eAtRet ret = AtApsEngineSwitchingConditionSet(engineList[engine_i], defects, condition);

        if (ret != cAtOk)
            {
            mWarnNotReturnCliIfNotOk(ret, engineList[engine_i], "Cannot configure switching condition");
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtApsUpsrEngineSwitchingTypeSet(char argc, char** argv)
    {
    eAtApsSwitchType swType;

    AtUnused(argc);

    /* Switching type */
    swType = CliAtApsSwitchTypeFromString(argv[1]);
    if (swType == cAtApsSwitchTypeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Please input correct switching type: ");
        CliAtApsExpectedSwitchTypesPrint();
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    return AttributeSet(argv[0], (uint32)swType, (ApsEngineAttributeSetFunc)AtApsEngineSwitchTypeSet);
    }

eBool CmdAtApsUpsrEngineExternalCmd(char argc, char** argv)
    {
    eAtApsUpsrExtCmd command;
    eBool convertSuccess;

    AtUnused(argc);

    command = CliStringToEnum(argv[1], cAtApsUpsrExtCmdStr, cAtApsUpsrExtCmdVal, mCount(cAtApsUpsrExtCmdVal), &convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Please input valid external command: ");
        CliExpectedValuesPrint(cSevCritical, cAtApsUpsrExtCmdStr, mCount(cAtApsUpsrExtCmdVal));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    return AttributeSet(argv[0], command, (ApsEngineAttributeSetFunc)AtApsUpsrEngineExtCmdSet);
    }

eBool CmdAtApsUpsrEnginesShow(char argc, char **argv)
    {
    tTab *tabPtr;
    const char *heading[] ={"EngineId", "State", "SwitchingType",
                            "ExternalCommand", "working", "protection", "active", "RequestState",
                            "wtr (seconds)", "InterruptMask"};
    AtApsEngine *engineList;
    uint32 numberOfEngine, engine_i;
    AtUnused(argc);

    /* Get list of engines */
    engineList = ApsUpsrEngineListFromString(argv[0], &numberOfEngine);
    if (numberOfEngine == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No engine, the ID List may be wrong, expect 1,2-3,...\r\n");
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numberOfEngine, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\n");
        return cAtFalse;
        }

    /* Make table content */
    for (engine_i = 0; engine_i < numberOfEngine; engine_i++)
        {
        AtApsEngine engine = engineList[engine_i];
        eAtApsEngineState state;
        AtChannel channel;
        uint16 column = 0;
        const char *strValue;
        eAtSevLevel color;
        uint32 intrMaskVal;

        StrToCell(tabPtr, engine_i, column++, CliNumber2String(AtChannelIdGet((AtChannel)engine) + 1, "%d"));

        state = AtApsEngineStateGet(engine);
        strValue = CliAtApsEngineStateToString(AtApsEngineStateGet(engine));
        color = (state == cAtApsEngineStateStop) ? cSevCritical : cSevInfo;
        ColorStrToCell(tabPtr, engine_i, column++, strValue ? strValue : "Error", color);

        strValue = CliAtApsSwitchTypeToString(AtApsEngineSwitchTypeGet((AtApsEngine)engine));
        StrToCell(tabPtr, engine_i, column++, strValue ? strValue : "Error");

        strValue = CliEnumToString(AtApsUpsrEngineExtCmdGet((AtApsUpsrEngine)engine),
                                   cAtApsUpsrExtCmdStr,
                                   cAtApsUpsrExtCmdVal,
                                   mCount(cAtApsUpsrExtCmdVal), NULL);
        StrToCell(tabPtr, engine_i, column++, strValue ? strValue : "Error");

        channel = (AtChannel)AtApsUpsrEngineWorkingPathGet((AtApsUpsrEngine)engine);
        StrToCell(tabPtr, engine_i, column++, channel ? CliChannelIdStringGet(channel) : "none");

        channel = (AtChannel)AtApsUpsrEngineProtectionPathGet((AtApsUpsrEngine)engine);
        StrToCell(tabPtr, engine_i, column++, channel ? CliChannelIdStringGet(channel) : "none");

        channel = (AtChannel)AtApsUpsrEngineActivePathGet((AtApsUpsrEngine)engine);
        StrToCell(tabPtr, engine_i, column++, channel ? CliChannelIdStringGet(channel) : "none");

        strValue = CliAtApsRequestStateToString(AtApsEngineRequestStateGet(engine));
        StrToCell(tabPtr, engine_i, column++, strValue ? strValue : "error");

        if (AtApsEngineSwitchTypeGet(engine) == cAtApsSwitchTypeRev)
            StrToCell(tabPtr, engine_i, column++, CliNumber2String(AtApsEngineWtrGet(engine), "%u"));
        else
            StrToCell(tabPtr, engine_i, column++, sAtNotCare);

        /* Interrupt mask */
        intrMaskVal = AtChannelInterruptMaskGet((AtChannel)engine);
        strValue = CliAlarmMaskToString(cAtApsUpsrEngineEventTypeStr, cAtApsUpsrEngineEventTypeVal, mCount(cAtApsUpsrEngineEventTypeVal), intrMaskVal);
        ColorStrToCell(tabPtr, engine_i, column++, intrMaskVal ? strValue : "None", intrMaskVal ? cSevCritical : cSevInfo);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtApsUpsrEnginesSwitchConditionShow(char argc, char **argv)
    {
    tTab *tabPtr;
    const char *heading[] ={"EngineId", "AIS", "LOP", "TIM", "UNEQ", "PLM", "BER-SD", "BER-SF"};
    AtApsEngine *engineList;
    uint32 numberOfEngine, i;
    AtUnused(argc);

    /* Get list of engines */
    engineList = ApsUpsrEngineListFromString(argv[0], &numberOfEngine);
    if (numberOfEngine == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No engine, the ID List may be wrong, expect 1,2-3,...\r\n");
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numberOfEngine, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\n");
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numberOfEngine; i++)
        {
        AtApsEngine engine = engineList[i];
        uint8 column = 0;

        StrToCell(tabPtr, i, column++, CliNumber2String(AtChannelIdGet((AtChannel)engine) + 1, "%d"));

        PutSwitchingConditionToCell(tabPtr, i, column++, engine, cAtSdhPathAlarmAis);
        PutSwitchingConditionToCell(tabPtr, i, column++, engine, cAtSdhPathAlarmLop);
        PutSwitchingConditionToCell(tabPtr, i, column++, engine, cAtSdhPathAlarmTim);
        PutSwitchingConditionToCell(tabPtr, i, column++, engine, cAtSdhPathAlarmUneq);
        PutSwitchingConditionToCell(tabPtr, i, column++, engine, cAtSdhPathAlarmPlm);
        PutSwitchingConditionToCell(tabPtr, i, column++, engine, cAtSdhPathAlarmBerSd);
        PutSwitchingConditionToCell(tabPtr, i, column++, engine, cAtSdhPathAlarmBerSf);
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtApsUpsrEngineInterruptMaskSet(char argc, char** argv)
    {
    uint32 intrMaskVal = CliMaskFromString(argv[1], cAtApsUpsrEngineEventTypeStr, cAtApsUpsrEngineEventTypeVal, mCount(cAtApsUpsrEngineEventTypeVal));
    eBool enable = CliBoolFromString(argv[2]);

    AtUnused(argc);

    if (enable)
        return AttributeSet(argv[0], intrMaskVal, (ApsEngineAttributeSetFunc)InterruptMaskEnable);
    else
        return AttributeSet(argv[0], intrMaskVal, (ApsEngineAttributeSetFunc)InterruptMaskDisable);
    }

eBool CmdAtApsUpsrEngineAlarmCapture(char argc, char **argv)
    {
    AtApsEngine *engineList;
    uint32 numberOfEngine, i;
    eBool success = cAtTrue;
    static tAtChannelEventListener intrListener;
    eBool captureEnabled = cAtFalse;
    AtUnused(argc);

    /* Get list of engines */
    engineList = ApsUpsrEngineListFromString(argv[0], &numberOfEngine);
    if (numberOfEngine == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No engine, the ID List may be wrong, expect 1,2-3,...\r\n");
        return cAtFalse;
        }

    AtOsalMemInit(&intrListener, 0, sizeof(intrListener));
    intrListener.AlarmChangeState = AlarmChangeState;

    /* Enabling */
    mAtStrToBool(argv[1], captureEnabled, success);
    if (!success)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enabling mode, expected en/dis\r\n");
        return cAtFalse;
        }

    /* Register Interrupt listener */
    for (i = 0; i < numberOfEngine; i++)
        {
        eAtRet ret = cAtOk;
        AtChannel engine = (AtChannel)engineList[i];

        /* Register/deregister alarm listener */
        if (captureEnabled)
            ret = AtChannelEventListenerAdd(engine, &intrListener);
        else
            ret = AtChannelEventListenerRemove(engine, &intrListener);

        /* Let user know if error */
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot %s alarm listener on %s, ret = %s\r\n",
                     captureEnabled ? "register" : "deregister",
                     CliChannelIdStringGet(engine),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtApsUpsrEngineInterruptShow(char argc, char **argv)
    {
    /* Declare variables */
    AtApsEngine *engineList;
    uint32  numberOfEngine, i, column, alarmBitmap;
    tTab *tabPtr = NULL;
    eBool silent = cAtFalse;
    eAtHistoryReadingMode readingMode;
    const char *pHeading[] = {"EngineId", "Switch", "StateChange"};
    AtUnused(argc);

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    /* Get the shared buffer to hold all of objects, then call the parse function */
    engineList = ApsUpsrEngineListFromString(argv[0], &numberOfEngine);
    if (numberOfEngine == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No engine, the ID List may be wrong, expect 1,2-3,...\r\n");
        return cAtTrue;
        }

    /* Get reading action mode */
    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading mode. Expected %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    if (argc > 2)
        silent = CliHistoryReadingShouldSilent(readingMode, argv[2]);

    /* Create table with titles */
    if (!silent)
        {
        tabPtr = TableAlloc(numberOfEngine, mCount(pHeading), pHeading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "Fail to create table\r\n");
            return cAtFalse;
            }
        }

    /* Get data */
    for (i = 0; i < numberOfEngine; i++)
        {
        AtChannel engine = (AtChannel)engineList[i];

        column = 0;
        StrToCell(tabPtr, i, column++, CliNumber2String(AtChannelIdGet((AtChannel)engine) + 1, "%d"));

        /* Get alarm state */
        if (readingMode == cAtHistoryReadingModeReadToClear)
            alarmBitmap = AtChannelAlarmInterruptClear(engine);
        else
            alarmBitmap = AtChannelAlarmInterruptGet(engine);

       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtApsEngineEventSwitch) ? "Set" : "Clear", (alarmBitmap & cAtApsEngineEventSwitch) ? cSevCritical : cSevInfo);
       ColorStrToCell(tabPtr, i, column++, (alarmBitmap & cAtApsEngineEventStateChange) ? "Set" : "Clear", (alarmBitmap & cAtApsEngineEventStateChange) ? cSevCritical : cSevInfo);
       }

    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

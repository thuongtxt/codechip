/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : CliAtApsGroup.c
 *
 * Created Date: Aug 15, 2016
 *
 * Description : APS Group
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtModuleAps.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char * cCmdApsGroupStateStr[] = {"unknown", "working", "protection"};
static const uint32 cCmdApsGroupStateVal[] = {cAtApsGroupStateUnknown,
                                              cAtApsGroupStateWorking,
                                              cAtApsGroupStateProtection};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtApsSelector SelectorGet(uint32 selectorId)
    {
    return AtModuleApsSelectorGet(CliAtApsModule(), selectorId);
    }

static AtApsGroup GroupGet(uint32 groupId)
    {
    return AtModuleApsGroupGet(CliAtApsModule(), groupId);
    }

static uint32* IdsFromString(char *string, uint32 *numId)
    {
    uint32 bufferSize;
    uint32* idBuffer = CliSharedIdBufferGet(&bufferSize);
    *numId = CliIdListFromString(string, idBuffer, bufferSize);
    return idBuffer;
    }

static eBool SelectorManipulate(char argc, char **argv, eAtModuleApsRet (*Func)(AtApsGroup self, AtApsSelector selector))
    {
    eBool success = cAtTrue;
    uint32 *selectorIds;
    uint32 numSelectors;
    uint32 selector_i;
    uint32 groupId;

    AtUnused(argc);

    /* Get group ID */
    groupId = AtStrToDw(argv[0]);
    if (groupId > AtModuleApsMaxNumGroups(CliAtApsModule()))
        {
        AtPrintc(cSevCritical,
                 "ERROR: invalid Group ID, its range is 1-%d\r\n",
                 AtModuleApsMaxNumGroups(CliAtApsModule()));
        return cAtFalse;
        }
    groupId = groupId - 1;

    /* Get selector ID */
    selectorIds = IdsFromString(argv[1], &numSelectors);
    if (numSelectors == 0)
        {
        AtPrintc(cSevWarning, "WARNING: no selectors to be added, ID may be wrong, expected: 1,2-4,...\r\n");
        return cAtTrue;
        }

    for (selector_i = 0; selector_i < numSelectors; selector_i ++)
        {
        eAtRet ret;
        uint32 selectorId = selectorIds[selector_i] - 1;
        AtApsSelector selector = SelectorGet(selectorId);

        if (selector == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: Ignore selector %d which does not exist\r\n", selectorIds[selector_i]);
            continue;
            }

        ret = Func(GroupGet(groupId), selector);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Manipulate selector %d on group %d fail with ret = %s\r\n",
                     selectorId + 1, groupId + 1, AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static uint32 *AllSelectorsGet(AtApsGroup group, uint32 *numSelectors)
    {
    uint32 idBufferSize;
    uint32 *idList = CliSharedIdBufferGet(&idBufferSize);
    uint32 selector_i;

    if (numSelectors)
        *numSelectors = AtApsGroupNumSelectors(group);

    for (selector_i = 0; selector_i < AtApsGroupNumSelectors(group); selector_i++)
        {
        AtApsSelector selector = AtApsGroupSelectorAtIndex(group, selector_i);
        idList[selector_i] = AtApsSelectorIdGet(selector) + 1;
        }

    return idList;
    }

static AtApsGroup *ValidGroupsFromString(char *idString, uint32 *numGroups)
    {
    uint32 idBufferSize, numberGroups, group_i;
    uint32 *groupIds, numValidGroup = 0;
    uint32 groupBufferSize;
    AtApsGroup *groups = (AtApsGroup *)CliSharedObjectListGet(&groupBufferSize);
    AtModuleAps apsModule = CliAtApsModule();

    if (numGroups)
        *numGroups = 0;

    /* Have ID list */
    groupIds = CliSharedIdBufferGet(&idBufferSize);
    numberGroups = CliIdListFromString(idString, groupIds, idBufferSize);
    if (groupIds == NULL)
        return NULL;

    /* Only take valid groups */
    for (group_i = 0; group_i < numberGroups; group_i++)
        {
        AtApsGroup group = AtModuleApsGroupGet(apsModule, groupIds[group_i] - 1);

        if (group)
            {
            groups[numValidGroup] = group;
            numValidGroup = numValidGroup + 1;
            }

        if (numValidGroup >= groupBufferSize)
            break;
        }

    if (numGroups)
        *numGroups = numValidGroup;

    return groups;
    }

eBool CmdAtApsGroupCreate(char argc, char **argv)
    {
    eBool success = cAtTrue;
    uint32 *groupIds;
    uint32 numGroups = 0;
    uint32 group_i;

    AtUnused(argc);

    groupIds = IdsFromString(argv[0], &numGroups);
    if (numGroups == 0)
        {
        AtPrintc(cSevCritical, "ERROR: no groups, ID may be wrong, expected: 1,2-4,...\r\n");
        return cAtFalse;
        }

    for (group_i = 0; group_i < numGroups; group_i ++)
        {
        AtApsGroup group = AtModuleApsGroupCreate(CliAtApsModule(), groupIds[group_i] - 1);

        if (group)
            continue;

        AtPrintc(cSevCritical, "ERROR: Create group %d fail, ID may be out of range\r\n", groupIds[group_i]);
        success = cAtFalse;
        }

    return success;
    }

eBool CmdAtApsGroupDelete(char argc, char **argv)
    {
    eBool success = cAtTrue;
    uint32 *groupIds;
    uint32 numGroups;
    uint32 group_i;

    AtUnused(argc);

    groupIds = IdsFromString(argv[0], &numGroups);
    if (numGroups == 0)
        {
        AtPrintc(cSevCritical, "ERROR: no groups, ID may be wrong, expected: 1,2-4,...\r\n");
        return cAtFalse;
        }

    /* Delete Group */
    for (group_i = 0; group_i < numGroups; group_i ++)
        {
        eAtRet ret = AtModuleApsGroupDelete(CliAtApsModule(), groupIds[group_i] - 1);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Delete group %d fail with ret = %s\r\n", groupIds[group_i], AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtApsGroupSelectorAdd(char argc, char **argv)
    {
    return SelectorManipulate(argc, argv, AtApsGroupSelectorAdd);
    }

eBool CmdAtApsGroupSelectorRemove(char argc, char **argv)
    {
    return SelectorManipulate(argc, argv, AtApsGroupSelectorRemove);
    }

eBool CmdAtApsGroupStateSet(char argc, char **argv)
    {
    eBool success = cAtTrue;
    uint32 *groupIds;
    uint32 numGroups;
    uint32 group_i;
    eAtApsGroupState state;

    AtUnused(argc);

    /* Get engine ID */
    groupIds = IdsFromString(argv[0], &numGroups);

    state = CliStringToEnum(argv[1],
                            cCmdApsGroupStateStr,
                            cCmdApsGroupStateVal,
                            mCount(cCmdApsGroupStateVal),
                            &success);
    if (success == cAtFalse)
        {
        AtPrintc(cSevCritical, "ERROR: Please input valid state: ");
        CliExpectedValuesPrint(cSevCritical, cCmdApsGroupStateStr, mCount(cCmdApsGroupStateVal));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    for (group_i = 0; group_i < numGroups; group_i ++)
        {
        eAtRet ret = AtApsGroupStateSet(GroupGet(groupIds[group_i] - 1), state);

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Change state on group %d fail with ret = %s\r\n", groupIds[group_i], AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtApsGroupShow(char argc, char **argv)
    {
    tTab *tabPtr;
    const char *heading[] ={"GroupId", "state", "selectors"};
    uint32 numGroups = 0, group_i;
    AtApsGroup *groups = ValidGroupsFromString(argv[0], &numGroups);

    AtUnused(argc);

    /* Create table with titles */
    tabPtr = TableAlloc(numGroups, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\n");
        return cAtFalse;
        }

    /* Make table content */
    for (group_i = 0; group_i < numGroups; group_i++)
        {
        AtApsGroup group = groups[group_i];
        eAtApsGroupState state;
        uint16 column = 0;
        const char *strValue;
        char idString[128];
        uint32 numSelectors;
        uint32 *selectors;

        StrToCell(tabPtr, group_i, column++, CliNumber2String(AtApsGroupIdGet(group) + 1, "%d"));

        state = AtApsGroupStateGet(group);
        strValue = CliEnumToString(state,
                                   cCmdApsGroupStateStr,
                                   cCmdApsGroupStateVal,
                                   mCount(cCmdApsGroupStateVal),
                                   NULL);
        ColorStrToCell(tabPtr, group_i, column++, strValue ? strValue : "Error", (state == cAtApsGroupStateProtection) ? cSevCritical : cSevInfo);

        selectors = AllSelectorsGet(group, &numSelectors);
        strValue = AtStdNumbersToString(AtStdSharedStdGet(), idString, sizeof(idString), selectors, numSelectors);
        StrToCell(tabPtr, group_i, column++, strValue ? strValue : "none");
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

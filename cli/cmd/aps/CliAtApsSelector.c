/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : CliAtModuleAps.c
 *
 * Created Date: Jun 10, 2013
 *
 * Description : APS module
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtModuleAps.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtApsSelector SelectorGet(uint32 selectorId)
    {
    return AtModuleApsSelectorGet(CliAtApsModule(), selectorId);
    }

static uint32* IdsFromString(char* arg, uint32 *numberPaths)
    {
    uint32 bufferSize;
    uint32 *idBuffer = CliSharedIdBufferGet(&bufferSize);
    *numberPaths = CliIdListFromString(arg, idBuffer, bufferSize);
    return idBuffer;
    }

static eBool SelectorInputsValidate(uint32 numSelectors,
                                    AtList workingPaths,
                                    AtList protectionPaths,
                                    AtList outgoingPaths)
    {
    uint32 numWorkings = AtListLengthGet(workingPaths);
    uint32 numProtections = AtListLengthGet(protectionPaths);
    uint32 numOutgoings = AtListLengthGet(outgoingPaths);

    if (numSelectors == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No selectors, ID may be wrong, expect: 1,2,3-4,...\r\n");
        return cAtFalse;
        }

    /* Number of inputs must be the same */
    if ((numSelectors == numWorkings) &&
        (numSelectors == numProtections) &&
        (numSelectors == numOutgoings))
        return cAtTrue;

    AtPrintc(cSevCritical,
             "ERROR: Number of selectors (%d), working paths (%d), protection paths (%d) and out going paths (%d) must be equal\r\n",
             numSelectors, numWorkings, numProtections, numOutgoings);
    return cAtFalse;
    }

static eBool Enable(char argc, char **argv, eBool enabled)
    {
    uint32 *selectorIds;
    uint32 numSelectors;
    uint32 selector_i;
    eBool  success;

    AtUnused(argc);

    /* Get engine ID */
    selectorIds = IdsFromString(argv[0], &numSelectors);
    if (numSelectors == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No selectors, ID may be wrong. Expected: 1,2,3-5,...\r\n");
        return cAtFalse;
        }

    /* Create and start engine */
    for (selector_i = 0; selector_i < numSelectors; selector_i ++)
        {
        uint32 selectorId = selectorIds[selector_i] - 1;
        eAtRet ret = AtApsSelectorEnable(SelectorGet(selectorId), enabled);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical, "ERROR: %s selector %u fail with ret = %s\r\n", enabled ? "Enable" : "Disable", selectorIds[selector_i], AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

static AtApsSelector *ValidSelectorsFromString(char *idString, uint32 *numSelectors)
    {
    uint32 idBufferSize, numberSelectors, selector_i;
    uint32 *selectorIds, numValidSelector = 0;
    uint32 selectorBufferSize;
    AtApsSelector *selectors = (AtApsSelector *)CliSharedObjectListGet(&selectorBufferSize);
    AtModuleAps apsModule = CliAtApsModule();

    /* Have ID list */
    selectorIds = CliSharedIdBufferGet(&idBufferSize);
    numberSelectors = CliIdListFromString(idString, selectorIds, idBufferSize);
    if (selectorIds == NULL)
        return NULL;

    /* Only take valid selectors */
    for (selector_i = 0; selector_i < numberSelectors; selector_i++)
        {
        AtApsSelector selector = AtModuleApsSelectorGet(apsModule, selectorIds[selector_i] - 1);

        if (selector)
            {
            selectors[numValidSelector] = selector;
            numValidSelector = numValidSelector + 1;
            }

        if (numValidSelector >= selectorBufferSize)
            break;
        }

    if (numSelectors)
        *numSelectors = numValidSelector;

    return selectors;
    }

eBool CmdAtModuleApsSelectorCreate(char argc, char **argv)
    {
    eBool success = cAtTrue;
    uint32 *selectorIds;
    uint32 numSelectors;
    uint32 selector_i;
    AtList workings, protections, outgoings;
    AtModuleAps apsModule = CliAtApsModule();

    AtUnused(argc);

    /* Parse all IDs */
    selectorIds = IdsFromString(argv[0], &numSelectors);
    workings = AtListCreate(0);
    CliChannelsFromString(workings, argv[1]);
    protections = AtListCreate(0);
    CliChannelsFromString(protections, argv[2]);
    outgoings = AtListCreate(0);
    CliChannelsFromString(outgoings, argv[3]);

    /* Validate inputs */
    if (!SelectorInputsValidate(numSelectors, workings, protections, outgoings))
        {
        AtObjectDelete((AtObject)workings);
        AtObjectDelete((AtObject)protections);
        AtObjectDelete((AtObject)outgoings);
        return cAtFalse;
        }

    /* Create and start selector */
    for (selector_i = 0; selector_i < numSelectors; selector_i ++)
        {
        uint32 selectorId = selectorIds[selector_i] - 1;
        AtChannel working = (AtChannel)AtListObjectGet(workings, selector_i);
        AtChannel protection = (AtChannel)AtListObjectGet(protections, selector_i);
        AtChannel outgoing = (AtChannel)AtListObjectGet(outgoings, selector_i);
        AtApsSelector selector;

        selector = AtModuleApsSelectorCreate(apsModule, selectorId, working, protection, outgoing);
        if (selector)
            continue;

        AtPrintc(cSevCritical,
                 "ERROR: Create selector %d with working=%s protection=%s outgoing=%s fail\r\n",
                 selectorIds[selector_i],
                 CliChannelIdStringGet(working),
                 CliChannelIdStringGet(protection),
                 CliChannelIdStringGet(outgoing));
        success = cAtFalse;
        }

    /* Cleanup */
    AtObjectDelete((AtObject)workings);
    AtObjectDelete((AtObject)protections);
    AtObjectDelete((AtObject)outgoings);

    return success;
    }

eBool CmdAtModuleApsSelectorDelete(char argc, char **argv)
    {
    eBool success = cAtTrue;
    uint32 *selectorIds;
    uint32 numSelectors;
    uint32 selector_i;

    AtUnused(argc);

    selectorIds = IdsFromString(argv[0], &numSelectors);
    if (numSelectors == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No selectors, ID may be wrong. Expected: 1,2,3-5,...\r\n");
        return cAtFalse;
        }

    for (selector_i = 0; selector_i < numSelectors; selector_i ++)
        {
        uint32 selectorId = selectorIds[selector_i] - 1;
        eAtRet ret = AtModuleApsSelectorDelete(CliAtApsModule(), selectorId);

        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical, "ERROR: Delete selector %d fail with ret = %s\r\n", selectorIds[selector_i], AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

eBool CmdAtApsSelectorEnable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue);
    }

eBool CmdAtApsSelectorDisable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse);
    }

eBool CmdAtApsSelectorChannelSelect(char argc, char **argv)
    {
    uint32 *selectorIds;
    uint32 numSelectors;
    uint32 numPaths;
    uint32 selector_i;
    AtList paths;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get Selector ID */
    selectorIds = IdsFromString(argv[0], &numSelectors);
    if (numSelectors == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No selectors, ID may be wrong. Expected: 1,2,3-5,...\r\n");
        return cAtFalse;
        }

    /* Get paths */
    paths = AtListCreate(0);
    CliChannelsFromString(paths, argv[1]);
    numPaths = AtListLengthGet(paths);

    /* Number of selectors and paths to select must be the same */
    if (numPaths != numSelectors)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Number of selector (%d) and selected paths (%d) must match\r\n",
                 numSelectors, numPaths);
        AtObjectDelete((AtObject)paths);
        return cAtFalse;
        }

    for (selector_i = 0; selector_i < numSelectors; selector_i ++)
        {
        uint32 selectorId = selectorIds[selector_i] - 1;
        AtChannel path = (AtChannel)AtListObjectGet(paths, selector_i);
        eAtRet ret = AtApsSelectorChannelSelect(SelectorGet(selectorId), path);

        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical, "ERROR: Select path %s on selector %d fail with ret = %s\r\n",
                 CliChannelIdStringGet(path),
                 selectorIds[selector_i],
                 AtRet2String(ret));
        success = cAtFalse;
        }

    AtObjectDelete((AtObject)paths);

    return success;
    }

eBool CmdAtApsSelectorShow(char argc, char **argv)
    {
    tTab *tabPtr;
    const char *heading[] ={"Selector", "state", "working", "protection", "outgoing", "selected"};
    uint32 numSelectors = 0, selector_i;
    AtApsSelector *selectors = ValidSelectorsFromString(argv[0], &numSelectors);

    AtUnused(argc);

    if (numSelectors == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No selectors, ID may be wrong. Expected: 1,2,3-5,...\r\n");
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numSelectors, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\n");
        return cAtFalse;
        }

    /* Make table content */
    for (selector_i = 0; selector_i < numSelectors; selector_i++)
        {
        AtApsSelector selector = selectors[selector_i];
        eBool enabled;
        uint16 column = 0;

        StrToCell(tabPtr, selector_i, column++, CliNumber2String(AtApsSelectorIdGet(selector) + 1, "%d"));

        enabled = AtApsSelectorIsEnabled(selector);
        ColorStrToCell(tabPtr, selector_i, column++, CliBoolToString(enabled), enabled ? cSevInfo : cSevCritical);

        StrToCell(tabPtr, selector_i, column++, CliChannelIdStringGet(AtApsSelectorWorkingChannelGet(selector)));
        StrToCell(tabPtr, selector_i, column++, CliChannelIdStringGet(AtApsSelectorProtectionChannelGet(selector)));
        StrToCell(tabPtr, selector_i, column++, CliChannelIdStringGet(AtApsSelectorOutgoingChannelGet(selector)));
        StrToCell(tabPtr, selector_i, column++, CliChannelIdStringGet(AtApsSelectorSelectedChannel(selector)));
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : CliAtStsGroup.c
 *
 * Created Date: Nov 30, 2016
 *
 * Description : Sts1 group
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtModuleAps.h"
#include "AtStsGroup.h"

/*--------------------------- Define -----------------------------------------*/
#define sAtSts1Type "sts1"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 CliMaxLineId(void)
    {
    return AtModuleSdhMaxLinesGet(CliModuleSdh()) + 1UL;
    }

static eAtSdhLineRate MaxLineRate(void)
    {
    return AtModuleSdhMaxLineRate(CliModuleSdh());
    }

static uint32 MaxNumSts1sForOneLine(void)
    {
    eAtSdhLineRate rate = MaxLineRate();
    if (rate == cAtSdhLineRateStm0)  return 1;
    if (rate == cAtSdhLineRateStm1)  return 3;
    if (rate == cAtSdhLineRateStm4)  return 12;
    if (rate == cAtSdhLineRateStm16) return 48;
    if (rate == cAtSdhLineRateStm64) return 192;

    return 0;
    }

static char *TwoLevelsIdMinFormat(void)
    {
    static char minFormat[] = "1.1";
    return minFormat;
    }

static char *TwoLevelsIdMaxFormat(void)
    {
    static char maxFormat[16];

    AtOsalMemInit(maxFormat, 0, sizeof(maxFormat));
    AtSprintf(maxFormat, "%u.%u", CliMaxLineId(), MaxNumSts1sForOneLine());

    return maxFormat;
    }

static uint32* GroupIdListFromString(char *pIdString, uint32 *numGroups)
    {
    uint32 bufferSize;
    uint32* idBuffer = CliSharedIdBufferGet(&bufferSize);
    *numGroups = CliIdListFromString(pIdString, idBuffer, bufferSize);
    return idBuffer;
    }

static AtStsGroup* GroupListFromString(char *pIdString, uint32 *numGroups)
    {
    uint32 bufferSize;
    AtStsGroup* groupList = (AtStsGroup*)CliSharedObjectListGet(&bufferSize);
    uint32* groupIdList = GroupIdListFromString(pIdString, numGroups);
    AtModuleAps apsModule = CliAtApsModule();
    uint32 numValidGroups = 0;
    uint32 group_i;

    for (group_i = 0; group_i < *numGroups; group_i++)
        {
        AtStsGroup group = AtModuleApsStsGroupGet(apsModule, CliId2DriverId(groupIdList[group_i]));

        if (group)
            {
            groupList[numValidGroups] = group;
            numValidGroups++;
            }

        if (numValidGroups >= bufferSize)
            break;
        }

    *numGroups = numValidGroups;
    return groupList;
    }

static uint32 StsListFromString(char *pStrIdList, AtSdhSts *stsList, uint32 stsBufferSize)
    {
    char *pDupString;
    char *idType, *idString;
    uint32 numValidSts1 = 0;
    AtIdParser idParser = NULL;
    uint32 i;

    /* Get ID type */
    pDupString = IdTypeAndIdListGet(pStrIdList, &idType, &idString);

    if (AtStrcmp(idType, sAtSts1Type) != 0)
        {
        AtPrintc(cSevCritical, "ERROR: wrong idType %s .Expected : sts1\r\n", idType);
        AtOsalMemFree(pDupString);
        return 0;
        }

    idParser = AtIdParserNew(idString, TwoLevelsIdMinFormat(), TwoLevelsIdMaxFormat());
    if (idParser == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: wrong format %s .Example : sts1.1.1-1.2 \r\n", pStrIdList);
        AtOsalMemFree(pDupString);
        return 0;
        }

    for (i = 0; i < (AtIdParserNumNumbers(idParser) / 2); i++)
        {
        uint8 lineId = (uint8)CliId2DriverId(AtIdParserNextNumber(idParser));
        AtSdhLine line = AtModuleSdhLineGet(CliModuleSdh(), lineId);
        uint8 sts1Id = (uint8)CliId2DriverId(AtIdParserNextNumber(idParser));

        if (line != NULL)
            {
            stsList[numValidSts1] = AtSdhLineStsGet(line, sts1Id);
            numValidSts1 = numValidSts1 + 1;
            }

        if (numValidSts1 >= stsBufferSize)
            break;
        }

    AtObjectDelete((AtObject)idParser);
    AtOsalMemFree(pDupString);

    return numValidSts1;
    }

static uint32 PutString(char *idString, uint32 remainingBytes, const char *aString)
    {
    uint32 len = AtStrlen(aString);

    AtStrncat(idString, aString, remainingBytes);
    if (len > remainingBytes)
        return 0;

    return (remainingBytes - len);
    }

static char * StsListToString(AtStsGroup group, char *pStrBuffer, uint32 bufferSize)
    {
    char idString[32];
    uint32 remainingSize = 0;
    uint32 numStsInGroup = AtStsGroupNumSts(group);
    uint32 sts_i, numStsInList, maxStsInLine;
    uint32 *idList = CliSharedIdBufferGet(&remainingSize);
    uint32 *lineList;
    uint32 *stsList;
    uint32 lastLineId = cInvalidUint32;
    uint32 currentLineId;
    uint32 currentStsId;

    if (numStsInGroup > (remainingSize / 2))
        return NULL;

    if (numStsInGroup == 0)
        {
        AtSnprintf(pStrBuffer, bufferSize, "none");
        return pStrBuffer;
        }

    lineList = idList;
    stsList = &idList[numStsInGroup];
    pStrBuffer[0] = '\0';

    sts_i = 0;
    numStsInList = 0;
    do
        {
        AtSdhSts sts = AtStsGroupStsAtIndex(group, sts_i);
        AtSdhLine line;

        currentLineId = (uint32)AtSdhChannelLineGet((AtSdhChannel)sts);
        currentStsId  = (uint32)AtSdhChannelSts1Get((AtSdhChannel)sts);

        lineList[numStsInList] = currentLineId + 1;
        stsList[numStsInList]  = currentStsId + 1;
        sts_i++;

        if ((lastLineId == currentLineId || lastLineId == cInvalidUint32) && (sts_i < numStsInGroup))
            {
            lastLineId = currentLineId;
            numStsInList++;
            continue;
            }

        line = AtModuleSdhLineGet(CliModuleSdh(), (uint8)currentLineId);
        maxStsInLine = AtSdhChannelNumSts((AtSdhChannel)line);

        if (lastLineId == currentLineId)
            numStsInList++;

        /* Export STS-1s string for this line. */
        AtStdTwoLevelNumbersToString(AtStdSharedStdGet(), idString, 32, lineList, stsList, numStsInList, maxStsInLine);

        remainingSize = PutString(pStrBuffer, remainingSize, idString);
        if (remainingSize < 1)
            return pStrBuffer;

        if ((lastLineId != currentLineId) && (sts_i < numStsInGroup))
            {
            remainingSize = PutString(pStrBuffer, remainingSize, ",");
            if (remainingSize < 1)
                return pStrBuffer;

            lineList[0] = currentLineId + 1;
            stsList[0]  = currentStsId + 1;
            lastLineId = currentLineId;
            numStsInList = 1;
            }

        } while (sts_i < numStsInGroup);

    return pStrBuffer;
    }

static eBool PassThroughEnable(char argc, char** argv, eBool enable)
    {
    eBool success = cAtTrue;
    AtStsGroup *groupList;
    uint32 numGroups;
    uint32 group_i;

    AtUnused(argc);

    groupList = GroupListFromString(argv[0], &numGroups);
    if (numGroups == 0)
        {
        AtPrintc(cSevCritical, "ERROR: no groups, ID may be wrong, expected: 1,2-4,...\r\n");
        return cAtFalse;
        }

    for (group_i = 0; group_i < numGroups; group_i++)
        {
        eAtRet ret = AtStsGroupPassThroughEnable(groupList[group_i], enable);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot set pass-through for STS group %d with ret = %s\r\n",
                     AtStsGroupIdGet(groupList[group_i]), AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static eBool StsHandle(char argc, char** argv, eAtRet (*StsFunc)(AtStsGroup, AtSdhSts))
    {
    uint32 groupId;
    AtStsGroup group;
    uint32 numSts;
    AtSdhSts *stsList = NULL;
    uint32 stsBufferSize;
    uint32 sts1_i;

    AtUnused(argc);

    /* Get group ID */
    groupId = AtStrToDw(argv[0]);
    group = AtModuleApsStsGroupGet(CliAtApsModule(), CliId2DriverId(groupId));
    if (group == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: no groups, ID may be wrong, expected: 1,2-4,...\r\n");
        return cAtFalse;
        }

    stsList = (AtSdhSts*)CliSharedChannelListGet(&stsBufferSize);
    numSts = StsListFromString(argv[1], stsList, stsBufferSize);
    if (numSts == 0)
        {
        AtPrintc(cSevCritical, "ERROR: no valid STS-1, ID may be wrong, expected: sts1.1.1-1.2,2.1,...\r\n");
        return cAtFalse;
        }

    for (sts1_i = 0; sts1_i < numSts; sts1_i++)
        {
        eAtRet ret = StsFunc(group, stsList[sts1_i]);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot manipulate %s for STS group %d with ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)stsList[sts1_i]),
                     AtStsGroupIdGet(group), AtRet2String(ret));
            return cAtFalse;
            }
        }

    return cAtTrue;
    }

eBool CmdAtStsGroupCreate(char argc, char** argv)
    {
    AtModuleAps apsModule = CliAtApsModule();
    eBool success = cAtTrue;
    uint32 *groupIdList;
    uint32 numGroups = 0;
    uint32 group_i;

    AtUnused(argc);

    groupIdList = GroupIdListFromString(argv[0], &numGroups);
    if (numGroups == 0)
        {
        AtPrintc(cSevCritical, "ERROR: no groups, ID may be wrong, expected: 1,2-4,...\r\n");
        return cAtFalse;
        }

    for (group_i = 0; group_i < numGroups; group_i ++)
        {
        uint32 groupId = CliId2DriverId(groupIdList[group_i]);
        AtStsGroup stsGroup = AtModuleApsStsGroupGet(apsModule, groupId);
        if (stsGroup)
            {
            AtPrintc(cSevWarning, "WARNING: This STS group %d was created!!\r\n", groupIdList[group_i]);
            continue;
            }

        stsGroup = AtModuleApsStsGroupCreate(apsModule, groupId);
        if (stsGroup == NULL)
            {
            AtPrintc(cSevCritical, "ERROR: Create new STS group %d failed, ID may be out of range\r\n", groupIdList[group_i]);
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtStsGroupDelete(char argc, char** argv)
    {
    eBool success = cAtTrue;
    uint32 *groupIdList;
    uint32 numGroups;
    uint32 group_i;

    AtUnused(argc);

    groupIdList = GroupIdListFromString(argv[0], &numGroups);
    if (numGroups == 0)
        {
        AtPrintc(cSevCritical, "ERROR: no groups, ID may be wrong, expected: 1,2-4,...\r\n");
        return cAtFalse;
        }

    for (group_i = 0; group_i < numGroups; group_i ++)
        {
        eAtRet ret = AtModuleApsStsGroupDelete(CliAtApsModule(), CliId2DriverId(groupIdList[group_i]));
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Delete STS group %d fail with ret = %s\r\n", groupIdList[group_i], AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtStsGroupConnect(char argc, char** argv)
    {
    eBool success = cAtTrue;
    AtStsGroup *srcGroups;
    uint32 numSrcGroups;
    AtStsGroup *destGroups;
    uint32 numDestGroups;
    uint32 group_i;

    AtUnused(argc);

    srcGroups = GroupListFromString(argv[0], &numSrcGroups);
    if (numSrcGroups == 0)
        {
        AtPrintc(cSevCritical, "ERROR: no source groups, ID may be wrong, expected: 1,2-4,...\r\n");
        return cAtFalse;
        }
    srcGroups = AtMemdup(srcGroups, numSrcGroups * (sizeof(AtStsGroup)));

    destGroups = GroupListFromString(argv[1], &numDestGroups);
    if (numDestGroups == 0)
        {
        AtPrintc(cSevCritical, "ERROR: no destination groups, ID may be wrong, expected: 1,2-4,...\r\n");
        AtOsalMemFree(srcGroups);
        return cAtFalse;
        }

    if (numSrcGroups != numDestGroups)
        {
        AtPrintc(cSevCritical, "ERROR: Number of STS groups are not equal together\r\n");
        AtOsalMemFree(srcGroups);
        return cAtFalse;
        }

    for (group_i = 0; group_i < numSrcGroups; group_i++)
        {
        eAtRet ret = AtStsGroupDestGroupSet(srcGroups[group_i], destGroups[group_i]);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot connect STS group %d to STS group %d with ret = %s\r\n",
                     AtStsGroupIdGet(srcGroups[group_i]), AtStsGroupIdGet(destGroups[group_i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    AtOsalMemFree(srcGroups);
    return success;
    }

eBool CmdAtStsGroupDisconnect(char argc, char** argv)
    {
    eAtRet ret;
    eBool success = cAtTrue;
    AtStsGroup *groupList;
    uint32 numGroups;
    uint32 group_i;

    AtUnused(argc);

    groupList = GroupListFromString(argv[0], &numGroups);
    if (numGroups == 0)
        {
        AtPrintc(cSevCritical, "ERROR: no groups, ID may be wrong, expected: 1,2-4,...\r\n");
        return cAtFalse;
        }

    for (group_i = 0; group_i < numGroups; group_i++)
        {
        ret = AtStsGroupDestGroupSet(groupList[group_i], NULL);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot disconnect source groupId %d with ret = %s\r\n",
                     AtStsGroupIdGet(groupList[group_i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtStsGroupStsAdd(char argc, char** argv)
    {
    return StsHandle(argc, argv, AtStsGroupStsAdd);
    }

eBool CmdAtStsGroupStsRemove(char argc, char** argv)
    {
    return StsHandle(argc, argv, AtStsGroupStsDelete);
    }

eBool CmdAtStsGroupPassThroughEnable(char argc, char** argv)
    {
    return PassThroughEnable(argc, argv, cAtTrue);
    }

eBool CmdAtStsGroupPassThroughDisable(char argc, char** argv)
    {
    return PassThroughEnable(argc, argv, cAtFalse);
    }

eBool CmdAtStsGroupShow(char argc, char** argv)
    {
    tTab *tabPtr;
    uint32 bufferSize;
    char *strIds = CliSharedCharBufferGet(&bufferSize);
    const char *heading[] ={"Group ID", "PassThrough", "Dest. GroupID", "NumSts", "StsList"};
    AtStsGroup *groupList;
    uint32 numGroups;
    uint32 group_i;
    AtStsGroup group;

    AtUnused(argc);

    groupList = GroupListFromString(argv[0], &numGroups);
    if (numGroups == 0)
        {
        AtPrintc(cSevCritical, "ERROR: no groups, ID may be wrong, expected: 1,2-4,...\r\n");
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numGroups, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\n");
        return cAtFalse;
        }

    for (group_i = 0; group_i < numGroups; group_i++)
        {
        uint16 column = 0;
        AtStsGroup destGroup;

        group = groupList[group_i];
        destGroup = AtStsGroupDestGroupGet(group);

        StrToCell(tabPtr, group_i, column++, CliNumber2String((AtStsGroupIdGet(group) + 1), "%d"));
        StrToCell(tabPtr, group_i, column++, CliBoolToString(AtStsGroupPassThroughIsEnabled(group)));
        StrToCell(tabPtr, group_i, column++, destGroup ? CliNumber2String((AtStsGroupIdGet(destGroup) + 1), "%d") : "none" );
        StrToCell(tabPtr, group_i, column++, CliNumber2String(AtStsGroupNumSts(group), "%d"));
        StrToCell(tabPtr, group_i, column++, StsListToString(group, strIds, bufferSize));
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

	return cAtTrue;
	}

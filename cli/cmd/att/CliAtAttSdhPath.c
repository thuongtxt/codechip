/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : CliAtAttSdhPath.c
 *
 * Created Date: Jul 8, 2016
 *
 * Description : ATT SDH path
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtAttController.h"
#include "CliAtAttController.h"
#include "../sdh/CliAtModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static const char *cAtAttSdhPathPointerAdjTypeStr[] =
    {
    "increase",
    "decrease"
    };

static const eAtSdhPathPointerAdjType cAtAttSdhPathPointerAdjTypeVal[] =
    {
	cAtSdhPathPointerAdjTypeIncrease,
	cAtSdhPathPointerAdjTypeDecrease
    };

static const char *cAtAttSdhPathErrorTypeStr[] =
    {
    "bip",
    "rei"
    };

static const uint32 cAtAttSdhPathErrorTypeVal[] =
    {
     cAtSdhPathErrorBip,
     cAtSdhPathErrorRei
    };

static const char *cAtAttSdhPathAlarmTypeStr[] =
    {
    "ais",
    "rdi",
    "lop",
    "uneq",
    "rfi",
    "erdi-s",
    "erdi-p",
    "erdi-c",
    "lom"
    };

static const uint32 cAtAttSdhPathAlarmTypeVal[] =
    {
     cAtSdhPathAlarmAis,
     cAtSdhPathAlarmRdi,
     cAtSdhPathAlarmLop,
     cAtSdhPathAlarmUneq,
     cAtSdhPathAlarmRfi,
     cAtSdhPathAlarmErdiS,
     cAtSdhPathAlarmErdiP,
     cAtSdhPathAlarmErdiC,
     cAtSdhPathAlarmLom
    };

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 CliAttSdhPathPointerAdjTypeMaskFromString(char *pPointerAdjTypeStr)
    {
    return CliMaskFromString(pPointerAdjTypeStr, cAtAttSdhPathPointerAdjTypeStr, cAtAttSdhPathPointerAdjTypeVal, mCount(cAtAttSdhPathPointerAdjTypeVal));
    }

static uint32 CliAttSdhPathErrorTypeMaskFromString(char *pErrorTypeStr)
    {
    return CliMaskFromString(pErrorTypeStr, cAtAttSdhPathErrorTypeStr, cAtAttSdhPathErrorTypeVal, mCount(cAtAttSdhPathErrorTypeVal));
    }

static uint32 CliAttSdhPathAlarmTypeMaskFromString(char *pAlarmTypeStr)
    {
    return CliMaskFromString(pAlarmTypeStr, cAtAttSdhPathAlarmTypeStr, cAtAttSdhPathAlarmTypeVal, mCount(cAtAttSdhPathAlarmTypeVal));
    }

eBool CmdAtAttSdhPathForceErrorMode(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttSdhPathErrorTypeStr, cAtAttSdhPathErrorTypeVal, mCount(cAtAttSdhPathErrorTypeVal), NULL);
    return CliAtAttControllerForceMode(argc, argv, CliAtModuleSdhChanelsFromString, cAtTrue, errorType);
    }

eBool CmdAtAttSdhPathForceErrorRate(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttSdhPathErrorTypeStr, cAtAttSdhPathErrorTypeVal, mCount(cAtAttSdhPathErrorTypeVal), NULL);
    return CliAtAttControllerForceErrorRate(argc, argv, CliAtModuleSdhChanelsFromString, errorType);
    }

eBool CmdAtAttSdhPathForceErrorDuration(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttSdhPathErrorTypeStr, cAtAttSdhPathErrorTypeVal, mCount(cAtAttSdhPathErrorTypeVal), NULL);
    return CliAtAttControllerForceErrorDuration(argc, argv, CliAtModuleSdhChanelsFromString, errorType);
    }

eBool CmdAtAttSdhPathForceErrorStepSet(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttSdhPathErrorTypeStr, cAtAttSdhPathErrorTypeVal, mCount(cAtAttSdhPathErrorTypeVal), NULL);
    return CliAtAttControllerForceErrorStepSet(argc, argv, CliAtModuleSdhChanelsFromString, errorType);
    }

eBool CmdAtAttSdhPathForceErrorPerSecond(char argc, char **argv)
    {
    return CliAtAttControllerForceErrorPerSecond(argc, argv, CliAtModuleSdhChanelsFromString, CliAttSdhPathErrorTypeMaskFromString);
    }

eBool CmdAtAttSdhPathForceError(char argc, char **argv)
    {
    return CliAtAttControllerForceError(argc, argv, CliAtModuleSdhChanelsFromString, CliAttSdhPathErrorTypeMaskFromString);
    }

eBool CmdAtAttSdhPathForceErrorNumErrorsSet(char argc, char **argv)
    {
    return CliAtAttControllerForceErrorNumErrorsSet(argc, argv, CliAtModuleSdhChanelsFromString, CliAttSdhPathErrorTypeMaskFromString);
    }

eBool CmdAtAttSdhPathForceErrorBitPostionSet(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttSdhPathErrorTypeStr, cAtAttSdhPathErrorTypeVal, mCount(cAtAttSdhPathErrorTypeVal), NULL);
    return CliAtAttControllerForceBitPositionSet(argc, argv, CliAtModuleSdhChanelsFromString, cAtTrue, errorType);
    }

eBool CmdAtAttSdhPathForceErrorDataMaskSet(char argc, char **argv)
    {
    return CliAtAttControllerForceErrorDataMaskSet(argc, argv, CliAtModuleSdhChanelsFromString, CliAttSdhPathErrorTypeMaskFromString);
    }

eBool CmdAtAttSdhPathForceErrorStatusShow(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttSdhPathErrorTypeStr, cAtAttSdhPathErrorTypeVal, mCount(cAtAttSdhPathErrorTypeVal), NULL);
    return CliAtAttControllerForceErrorStatusShow(argc, argv, errorType, CliAtModuleSdhChanelsFromString);
    }

eBool CmdAtAttSdhPathForceErrorShow(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttSdhPathErrorTypeStr, cAtAttSdhPathErrorTypeVal, mCount(cAtAttSdhPathErrorTypeVal), NULL);
    return CliAtAttControllerForceErrorShow(argc, argv, errorType, CliAtModuleSdhChanelsFromString);
    }

eBool CmdAtAttSdhPathForceAlarmModeSet(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttSdhPathAlarmTypeStr, cAtAttSdhPathAlarmTypeVal, mCount(cAtAttSdhPathAlarmTypeVal), NULL);
    return CliAtAttControllerForceMode(argc, argv, CliAtModuleSdhChanelsFromString, cAtFalse, alarmType);
    }

eBool CmdAtAttSdhPathForceAlarmNumEventPerSecond(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttSdhPathAlarmTypeStr, cAtAttSdhPathAlarmTypeVal, mCount(cAtAttSdhPathAlarmTypeVal), NULL);
    return CliAtAttControllerForceAlarmNumEventSet(argc, argv, CliAtModuleSdhChanelsFromString, alarmType);
    }

eBool CmdAtAttSdhPathForceAlarmDuration(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttSdhPathAlarmTypeStr, cAtAttSdhPathAlarmTypeVal, mCount(cAtAttSdhPathAlarmTypeVal), NULL);
    return CliAtAttControllerForceAlarmDurationSet(argc, argv, CliAtModuleSdhChanelsFromString, alarmType);
    }

eBool CmdAtAttSdhPathForceAlarmNumFrame(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttSdhPathAlarmTypeStr, cAtAttSdhPathAlarmTypeVal, mCount(cAtAttSdhPathAlarmTypeVal), NULL);
    return CliAtAttControllerForceAlarmNumFrameSet(argc, argv, CliAtModuleSdhChanelsFromString, alarmType);
    }

eBool CmdAtAttSdhPathRxAlarmCaptureEnable(char argc, char **argv)
    {
    return CliAtAttControllerRxAlarmCapture(argc, argv, CliAtModuleSdhChanelsFromString, cAtTrue);
    }

eBool CmdAtAttSdhPathRxAlarmCaptureDisable(char argc, char **argv)
    {
    return CliAtAttControllerRxAlarmCapture(argc, argv, CliAtModuleSdhChanelsFromString, cAtFalse);
    }

eBool CmdAtAttSdhPathRxAlarmCaptureDuration(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttSdhPathAlarmTypeStr, cAtAttSdhPathAlarmTypeVal, mCount(cAtAttSdhPathAlarmTypeVal), NULL);
    return CliAtAttControllerRxAlarmCaptureDuration(argc, argv, CliAtModuleSdhChanelsFromString, alarmType);
    }

eBool CmdAtAttSdhPathRxAlarmCaptureMode(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttSdhPathAlarmTypeStr, cAtAttSdhPathAlarmTypeVal, mCount(cAtAttSdhPathAlarmTypeVal), NULL);
    return CliAtAttControllerRxAlarmCaptureMode(argc, argv, CliAtModuleSdhChanelsFromString, alarmType);
    }

eBool CmdAtAttSdhPathRxAlarmCaptureNumEvent(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttSdhPathAlarmTypeStr, cAtAttSdhPathAlarmTypeVal, mCount(cAtAttSdhPathAlarmTypeVal), NULL);
    return CliAtAttControllerRxAlarmCaptureNumEvent(argc, argv, CliAtModuleSdhChanelsFromString, alarmType);
    }

eBool CmdAtAttSdhPathForceAlarmStepSet(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttSdhPathAlarmTypeStr, cAtAttSdhPathAlarmTypeVal, mCount(cAtAttSdhPathAlarmTypeVal), NULL);
    return CliAtAttControllerForceAlarmStepSet(argc, argv, CliAtModuleSdhChanelsFromString, alarmType);
    }

eBool CmdAtAttSdhPathForceAlarm(char argc, char **argv)
    {
    return CliAtAttControllerForceAlarm(argc, argv, CliAtModuleSdhChanelsFromString, CliAttSdhPathAlarmTypeMaskFromString);
    }

eBool CmdAtAttSdhPathFrequencyOffsetSet(char argc, char **argv)
    {
    return CliAtAttControllerFrequencyOffsetSet(argc, argv, CliAtModuleSdhChanelsFromString);
    }

eBool CmdAtAttSdhPathWanderFileNameSet(char argc, char **argv)
    {
    return CliAtAttControllerWanderFileNameSet(argc, argv, CliAtModuleSdhChanelsFromString);
    }

eBool CmdAtAttSdhPathWanderStart(char argc, char **argv)
    {
    return CliAtAttControllerWanderStart(argc, argv, CliAtModuleSdhChanelsFromString);
    }

eBool CmdAtAttSdhPathWanderStop(char argc, char **argv)
    {
    return CliAtAttControllerWanderStop(argc, argv, CliAtModuleSdhChanelsFromString);
    }

eBool CmdAtAttSdhPathForceAlarmStatusShow(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttSdhPathAlarmTypeStr, cAtAttSdhPathAlarmTypeVal, mCount(cAtAttSdhPathAlarmTypeVal), NULL);
    return CliAtAttControllerForceAlarmStatusShow(argc, argv, alarmType, CliAtModuleSdhChanelsFromString);
    }

eBool CmdAtAttSdhPathForceAlarmShow(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttSdhPathAlarmTypeStr, cAtAttSdhPathAlarmTypeVal, mCount(cAtAttSdhPathAlarmTypeVal), NULL);
    return CliAtAttControllerForceAlarmShow(argc, argv, alarmType, CliAtModuleSdhChanelsFromString);
    }

eBool CmdAtAttSdhPathRxAlarmCaptureShow(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttSdhPathAlarmTypeStr, cAtAttSdhPathAlarmTypeVal, mCount(cAtAttSdhPathAlarmTypeVal), NULL);
    return CliAtAttControllerRxAlarmCaptureShow(argc, argv, alarmType, CliAtModuleSdhChanelsFromString);
    }

eBool CmdAtAttSdhPathFrequencyOffsetShow(char argc, char **argv)
    {
    return CliAtAttControllerFrequencyOffsetShow(argc, argv, CliAtModuleSdhChanelsFromString);
    }

eBool CmdAtAttSdhPathForcePointerAdjModeSet(char argc, char **argv)
	{
	return CliAtAttControllerForcePointerAdjMode(argc, argv, CliAtModuleSdhChanelsFromString, CliAttSdhPathPointerAdjTypeMaskFromString);
	}

eBool CmdAtAttSdhPathForcePointerAdjNumEventSet(char argc, char **argv)
	{
	return CliAtAttControllerForcePointerAdjNumEventSet(argc, argv, CliAtModuleSdhChanelsFromString, CliAttSdhPathPointerAdjTypeMaskFromString);
	}

eBool CmdAtAttSdhPathForcePointerAdjDurationSet(char argc, char **argv)
	{
	return CliAtAttControllerForcePointerAdjDurationSet(argc, argv, CliAtModuleSdhChanelsFromString, CliAttSdhPathPointerAdjTypeMaskFromString);
	}

eBool CmdAtAttSdhPathForcePointerAdj(char argc, char **argv)
	{
	return CliAtAttControllerForcePointerAdj(argc, argv, CliAtModuleSdhChanelsFromString, CliAttSdhPathPointerAdjTypeMaskFromString);
	}

eBool CmdAtAttSdhPathForcePointerAdjBitPostionSet(char argc, char **argv)
    {
    return CliAtAttControllerForcePointerAdjBitPositionSet(argc, argv, CliAtModuleSdhChanelsFromString, CliAttSdhPathErrorTypeMaskFromString);
    }

eBool CmdAtAttSdhPathForcePointerAdjDataMaskSet(char argc, char **argv)
    {
    return CliAtAttControllerForcePointerAdjDataMaskSet(argc, argv, CliAtModuleSdhChanelsFromString, CliAttSdhPathErrorTypeMaskFromString);
    }

eBool CmdAtAttSdhPathForcePointerAdjStepSet(char argc, char **argv)
    {
    return CliAtAttControllerForcePointerAdjStepSet(argc, argv, CliAtModuleSdhChanelsFromString, CliAttSdhPathPointerAdjTypeMaskFromString);
    }

eBool CmdAtAttSdhPathBipCountShow(char argc, char **argv)
    {
    return CliAtAttControllerBipCountShow(argc, argv, CliAtModuleSdhChanelsFromString);
    }

static AtAttController AttController(AtChannel channel)
    {
    return (AtAttController)AtChannelAttController(channel);
    }

static eBool PointerAdjEnable(char argc, char **argv, eBool enable)
    {
    uint32 numChannels, i;
    AtChannel *channelList;
    eAtRet ret = cAtOk;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = CliAtModuleSdhChanelsFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid channel list, ...\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i++)
        {
        ret |= AtAttControllerPointerAdjEnable(AttController(channelList[i]), enable);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot force/un-force for channel %s, ret = %s\r\n",
                     AtObjectToString((AtObject)channelList[i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtAttSdhPathPointerAdjEnable(char argc, char **argv)
    {
    return PointerAdjEnable(argc, argv, cAtTrue);
    }

eBool CmdAtAttSdhPathPointerAdjDisable(char argc, char **argv)
    {
    return PointerAdjEnable(argc, argv, cAtFalse);
    }

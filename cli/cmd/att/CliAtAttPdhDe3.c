/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ATT
 *
 * File        : CliAtAttPdhDe3.c
 *
 * Created Date: Dec 9, 2015
 *
 * Description : ATT CLI implement
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtPdhChannel.h"
#include "AtAttController.h"
#include "CliAtAttController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static const char *cAtAttPdhDe3ErrorTypeStr[] =
    {
    "fbit",
    "cpbit",
    "pbit",
    "fabyte",
    "bip8byte",
    "reibit",
    "nrbyte",
    "gcbyte",
    "febe",
    };

static const uint32 cAtAttPdhDe3ErrorTypeVal[] =
    {
     cAtAttPdhDe3ErrorTypeFbit    ,
     cAtAttPdhDe3ErrorTypeCpbit   ,
     cAtAttPdhDe3ErrorTypePbit    ,
     cAtAttPdhDe3ErrorTypeFabyte  ,
     cAtAttPdhDe3ErrorTypeBip8byte,
     cAtAttPdhDe3ErrorTypeReibit  ,
     cAtAttPdhDe3ErrorTypeNrbyte  ,
     cAtAttPdhDe3ErrorTypeGcbyte  ,
     cAtAttPdhDe3ErrorTypeFebe
    };

static const char *cAtAttPdhDe3AlarmTypeStr[] =
    {
    "los",
    "ais",
    "rai",
    "lof"
    };

static const uint32 cAtAttPdhDe3AlarmTypeVal[] =
    {
    cAtPdhDe3AlarmLos,
    cAtPdhDe3AlarmAis,
    cAtPdhDe3AlarmRai,
    cAtPdhDe3AlarmLof
    };

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
uint32 CliAttPdhDe3ErrorTypeMaskFromString(char *pErrorTypeStr)
    {
    return CliMaskFromString(pErrorTypeStr, cAtAttPdhDe3ErrorTypeStr, cAtAttPdhDe3ErrorTypeVal, mCount(cAtAttPdhDe3ErrorTypeVal));
    }

static uint32 CliAttPdhDe3AlarmTypeMaskFromString(char *pAlarmTypeStr)
    {
    return CliMaskFromString(pAlarmTypeStr, cAtAttPdhDe3AlarmTypeStr, cAtAttPdhDe3AlarmTypeVal, mCount(cAtAttPdhDe3ErrorTypeVal));
    }

eBool CmdAtAttPdhDe3ForceErrorMode(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttPdhDe3ErrorTypeStr, cAtAttPdhDe3ErrorTypeVal, mCount(cAtAttPdhDe3ErrorTypeVal), NULL);
    return CliAtAttControllerForceMode(argc, argv, CliDe3ListFromString, cAtTrue, errorType);
    }

eBool CmdAtAttPdhDe3ForceErrorRate(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttPdhDe3ErrorTypeStr, cAtAttPdhDe3ErrorTypeVal, mCount(cAtAttPdhDe3ErrorTypeVal), NULL);
    return CliAtAttControllerForceErrorRate(argc, argv, CliDe3ListFromString, errorType);
    }

eBool CmdAtAttPdhDe3ForceErrorDuration(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttPdhDe3ErrorTypeStr, cAtAttPdhDe3ErrorTypeVal, mCount(cAtAttPdhDe3ErrorTypeVal), NULL);
    return CliAtAttControllerForceErrorDuration(argc, argv, CliDe3ListFromString, errorType);
    }

eBool CmdAtAttPdhDe3ForceErrorPerSecond(char argc, char **argv)
    {
    return CliAtAttControllerForceErrorPerSecond(argc, argv, CliDe3ListFromString, CliAttPdhDe3ErrorTypeMaskFromString);
    }

eBool CmdAtAttPdhDe3ForceError(char argc, char **argv)
    {
    return CliAtAttControllerForceError(argc, argv, CliDe3ListFromString, CliAttPdhDe3ErrorTypeMaskFromString);
    }

eBool CmdAtAttPdhDe3ForceErrorDebug(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttPdhDe3ErrorTypeStr, cAtAttPdhDe3ErrorTypeVal, mCount(cAtAttPdhDe3ErrorTypeVal), NULL);
    return CliAtAttControllerForceErrorDebug(argc, argv, CliDe3ListFromString, errorType);
    }

eBool CmdAtAttPdhDe3ForceErrorNumErrorsSet(char argc, char **argv)
    {
    return CliAtAttControllerForceErrorNumErrorsSet(argc, argv, CliDe3ListFromString, CliAttPdhDe3ErrorTypeMaskFromString);
    }

eBool CmdAtAttPdhDe3ForceErrorBitPostionSet(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttPdhDe3ErrorTypeStr, cAtAttPdhDe3ErrorTypeVal, mCount(cAtAttPdhDe3ErrorTypeVal), NULL);
    return CliAtAttControllerForceBitPositionSet(argc, argv, CliDe3ListFromString, cAtTrue, errorType);
    }

eBool CmdAtAttPdhDe3ForceErrorDataMaskSet(char argc, char **argv)
    {
    return CliAtAttControllerForceErrorDataMaskSet(argc, argv, CliDe3ListFromString, CliAttPdhDe3ErrorTypeMaskFromString);
    }

eBool CmdAtAttPdhDe3ForceErrorStepSet(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttPdhDe3ErrorTypeStr, cAtAttPdhDe3ErrorTypeVal, mCount(cAtAttPdhDe3ErrorTypeVal), NULL);
    return CliAtAttControllerForceErrorStepSet(argc, argv, CliDe3ListFromString, errorType);
    }

eBool CmdAtAttPdhDe3ForceErrorStatusShow(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttPdhDe3ErrorTypeStr, cAtAttPdhDe3ErrorTypeVal, mCount(cAtAttPdhDe3ErrorTypeVal), NULL);
    return CliAtAttControllerForceErrorStatusShow(argc, argv, errorType, CliDe3ListFromString);
    }

eBool CmdAtAttPdhDe3ForceErrorShow(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttPdhDe3ErrorTypeStr, cAtAttPdhDe3ErrorTypeVal, mCount(cAtAttPdhDe3ErrorTypeVal), NULL);
    return CliAtAttControllerForceErrorShow(argc, argv, errorType, CliDe3ListFromString);
    }

eBool CmdAtAttPdhDe3ForceAlarmMode(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttPdhDe3AlarmTypeStr, cAtAttPdhDe3AlarmTypeVal, mCount(cAtAttPdhDe3AlarmTypeVal), NULL);
    return CliAtAttControllerForceMode(argc, argv, CliDe3ListFromString, cAtFalse, alarmType);
    }

eBool CmdAtAttPdhDe3ForceAlarmNumEventPerSecond(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttPdhDe3AlarmTypeStr, cAtAttPdhDe3AlarmTypeVal, mCount(cAtAttPdhDe3AlarmTypeVal), NULL);
    return CliAtAttControllerForceAlarmNumEventSet(argc, argv, CliDe3ListFromString, alarmType);
    }

eBool CmdAtAttPdhDe3ForceAlarmBitPostionSet(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttPdhDe3AlarmTypeStr, cAtAttPdhDe3AlarmTypeVal, mCount(cAtAttPdhDe3AlarmTypeVal), NULL);
    return CliAtAttControllerForceBitPositionSet(argc, argv, CliDe3ListFromString, cAtFalse, alarmType);
    }

eBool CmdAtAttPdhDe3ForceAlarmDuration(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttPdhDe3AlarmTypeStr, cAtAttPdhDe3AlarmTypeVal, mCount(cAtAttPdhDe3AlarmTypeVal), NULL);
    return CliAtAttControllerForceAlarmDurationSet(argc, argv, CliDe3ListFromString, alarmType);
    }

eBool CmdAtAttPdhDe3ForceAlarmNumFrame(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttPdhDe3AlarmTypeStr, cAtAttPdhDe3AlarmTypeVal, mCount(cAtAttPdhDe3AlarmTypeVal), NULL);
    return CliAtAttControllerForceAlarmNumFrameSet(argc, argv, CliDe3ListFromString, alarmType);
    }

eBool CmdAtAttPdhDe3RxAlarmCaptureEnable(char argc, char **argv)
    {
    return CliAtAttControllerRxAlarmCapture(argc, argv, CliDe3ListFromString, cAtTrue);
    }

eBool CmdAtAttPdhDe3RxAlarmCaptureDisable(char argc, char **argv)
    {
    return CliAtAttControllerRxAlarmCapture(argc, argv, CliDe3ListFromString, cAtFalse);
    }

eBool CmdAtAttPdhDe3RxAlarmCaptureDuration(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttPdhDe3AlarmTypeStr, cAtAttPdhDe3AlarmTypeVal, mCount(cAtAttPdhDe3AlarmTypeVal), NULL);
    return CliAtAttControllerRxAlarmCaptureDuration(argc, argv, CliDe3ListFromString, alarmType);
    }

eBool CmdAtAttPdhDe3RxAlarmCaptureMode(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttPdhDe3AlarmTypeStr, cAtAttPdhDe3AlarmTypeVal, mCount(cAtAttPdhDe3AlarmTypeVal), NULL);
    return CliAtAttControllerRxAlarmCaptureMode(argc, argv, CliDe3ListFromString, alarmType);
    }

eBool CmdAtAttPdhDe3RxAlarmCaptureNumEvent(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttPdhDe3AlarmTypeStr, cAtAttPdhDe3AlarmTypeVal, mCount(cAtAttPdhDe3AlarmTypeVal), NULL);
    return CliAtAttControllerRxAlarmCaptureNumEvent(argc, argv, CliDe3ListFromString, alarmType);
    }

eBool CmdAtAttPdhDe3ForceAlarmStepSet(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttPdhDe3AlarmTypeStr, cAtAttPdhDe3AlarmTypeVal, mCount(cAtAttPdhDe3AlarmTypeVal), NULL);
    return CliAtAttControllerForceAlarmStepSet(argc, argv, CliDe3ListFromString, alarmType);
    }

eBool CmdAtAttPdhDe3ForceAlarm(char argc, char **argv)
    {
    return CliAtAttControllerForceAlarm(argc, argv, CliDe3ListFromString, CliAttPdhDe3AlarmTypeMaskFromString);
    }

eBool CmdAtAttPdhDe3FrequencyOffsetSet(char argc, char **argv)
    {
    return CliAtAttControllerFrequencyOffsetSet(argc, argv, CliDe3ListFromString);
    }

eBool CmdAtAttPdhDe3WanderFileNameSet(char argc, char **argv)
    {
    return CliAtAttControllerWanderFileNameSet(argc, argv, CliDe3ListFromString);
    }

eBool CmdAtAttPdhDe3WanderStart(char argc, char **argv)
    {
    return CliAtAttControllerWanderStart(argc, argv, CliDe3ListFromString);
    }

eBool CmdAtAttPdhDe3WanderStop(char argc, char **argv)
    {
    return CliAtAttControllerWanderStop(argc, argv, CliDe3ListFromString);
    }

eBool CmdAtAttPdhDe3ForceAlarmStatusShow(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttPdhDe3AlarmTypeStr, cAtAttPdhDe3AlarmTypeVal, mCount(cAtAttPdhDe3AlarmTypeVal), NULL);
    return CliAtAttControllerForceAlarmStatusShow(argc, argv, alarmType, CliDe3ListFromString);
    }

eBool CmdAtAttPdhDe3ForceAlarmShow(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttPdhDe3AlarmTypeStr, cAtAttPdhDe3AlarmTypeVal, mCount(cAtAttPdhDe3AlarmTypeVal), NULL);
    return CliAtAttControllerForceAlarmShow(argc, argv, alarmType, CliDe3ListFromString);
    }

eBool CmdAtAttPdhDe3RxAlarmCaptureShow(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttPdhDe3AlarmTypeStr, cAtAttPdhDe3AlarmTypeVal, mCount(cAtAttPdhDe3AlarmTypeVal), NULL);
    return CliAtAttControllerRxAlarmCaptureShow(argc, argv, alarmType, CliDe3ListFromString);
    }

eBool CmdAtAttPdhDe3FrequencyOffsetShow(char argc, char **argv)
    {
    return CliAtAttControllerFrequencyOffsetShow(argc, argv, CliDe3ListFromString);
    }

eBool CmdPdhDe3ErrorForce(char argc, char** argv)
    {
    uint32 numberOfDe3, i;
    eAtPdhDe3CounterType errorType = 0;
    AtChannel *de3s;
    eBool convertSuccess;
    eAtRet (*Force)(AtChannel, uint32);
    eBool success = cAtTrue;
    eBool force = cAtFalse;
    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    de3s = CliSharedChannelListGet(&numberOfDe3);
    numberOfDe3 = CliDe3ListFromString(argv[0], de3s, numberOfDe3);
    if (!numberOfDe3)
        {
        AtPrintc(cSevCritical, "Invalid DS3/E3 list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    /* Get error type */
    errorType = CliAttPdhDe3ErrorTypeMaskFromString(argv[1]);

    /* Enabling */
    mAtStrToBool(argv[2], force, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enabling, expected: en/dis\r\n");
        return cAtFalse;
        }

    /* Determine correct function to force */
    Force = force ? AtChannelTxErrorForce : AtChannelTxErrorUnForce;

    for (i = 0; i < numberOfDe3; i++)
        {
        eAtRet ret = Force((AtChannel)de3s[i], errorType);
        if (ret != cAtOk)
            {
            mWarnNotReturnCliIfNotOk(ret, de3s[i], "Can not force error");
            success = cAtFalse;
            }
        }

    return success;
    }


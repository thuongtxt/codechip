/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ATT
 *
 * File        : CliAtAttPdhDe2.c
 *
 * Created Date: May 11, 2018
 *
 * Description : DS2/E2 CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtAttController.h"
#include "CliAtAttController.h"
#include "AtPdhDe2.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


static const char *cAtAttPdhDe2ErrorTypeStr[] =
    {
    "fbit",
    "parity"
    };

static const uint32 cAtAttPdhDe2ErrorTypeVal[] =
    {
     cAtPdhDe2CounterFbe,
     cAtPdhDe2CounterParity
    };

static const char *cAtAttPdhDe2AlarmTypeStr[] =
    {
    "ais",
    "rai",
    "lof"
    };

static const uint32 cAtAttPdhDe2AlarmTypeVal[] =
    {
     cAtPdhDe2AlarmAis,
     cAtPdhDe2AlarmRai,
     cAtPdhDe2AlarmLof
    };

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 CliAttPdhDe2ErrorTypeMaskFromString(char *pErrorTypeStr)
    {
    return CliMaskFromString(pErrorTypeStr, cAtAttPdhDe2ErrorTypeStr, cAtAttPdhDe2ErrorTypeVal, mCount(cAtAttPdhDe2ErrorTypeVal));
    }

static uint32 CliAttPdhDe2AlarmTypeMaskFromString(char *pAlarmTypeStr)
    {
    return CliMaskFromString(pAlarmTypeStr, cAtAttPdhDe2AlarmTypeStr, cAtAttPdhDe2AlarmTypeVal, mCount(cAtAttPdhDe2AlarmTypeVal));
    }

eBool CmdAtAttPdhDe2ForceErrorMode(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttPdhDe2ErrorTypeStr, cAtAttPdhDe2ErrorTypeVal, mCount(cAtAttPdhDe2ErrorTypeVal), NULL);
    return CliAtAttControllerForceMode(argc, argv, CliDe2ListFromString, cAtTrue, errorType);
    }

eBool CmdAtAttPdhDe2ForceErrorRate(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttPdhDe2ErrorTypeStr, cAtAttPdhDe2ErrorTypeVal, mCount(cAtAttPdhDe2ErrorTypeVal), NULL);
    return CliAtAttControllerForceErrorRate(argc, argv, CliDe2ListFromString, errorType);
    }

eBool CmdAtAttPdhDe2ForceErrorDuration(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttPdhDe2ErrorTypeStr, cAtAttPdhDe2ErrorTypeVal, mCount(cAtAttPdhDe2ErrorTypeVal), NULL);
    return CliAtAttControllerForceErrorDuration(argc, argv, CliDe2ListFromString, errorType);
    }

eBool CmdAtAttPdhDe2ForceErrorPerSecond(char argc, char **argv)
    {
    return CliAtAttControllerForceErrorPerSecond(argc, argv, CliDe2ListFromString, CliAttPdhDe2ErrorTypeMaskFromString);
    }

eBool CmdAtAttPdhDe2ForceError(char argc, char **argv)
    {
    return CliAtAttControllerForceError(argc, argv, CliDe2ListFromString, CliAttPdhDe2ErrorTypeMaskFromString);
    }


eBool CmdAtAttPdhDe2ForceErrorNumErrorsSet(char argc, char **argv)
    {
    return CliAtAttControllerForceErrorNumErrorsSet(argc, argv, CliDe2ListFromString, CliAttPdhDe2ErrorTypeMaskFromString);
    }

eBool CmdAtAttPdhDe2ForceErrorBitPostionSet(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttPdhDe2ErrorTypeStr, cAtAttPdhDe2ErrorTypeVal, mCount(cAtAttPdhDe2ErrorTypeVal), NULL);
    return CliAtAttControllerForceBitPositionSet(argc, argv, CliDe2ListFromString, cAtTrue, errorType);
    }

eBool CmdAtAttPdhDe2ForceErrorDataMaskSet(char argc, char **argv)
    {
    return CliAtAttControllerForceErrorDataMaskSet(argc, argv, CliDe2ListFromString, CliAttPdhDe2ErrorTypeMaskFromString);
    }

eBool CmdAtAttPdhDe2ForceErrorStepSet(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttPdhDe2ErrorTypeStr, cAtAttPdhDe2ErrorTypeVal, mCount(cAtAttPdhDe2ErrorTypeVal), NULL);
    return CliAtAttControllerForceErrorStepSet(argc, argv, CliDe2ListFromString, errorType);
    }

eBool CmdAtAttPdhDe2ForceErrorStatusShow(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttPdhDe2ErrorTypeStr, cAtAttPdhDe2ErrorTypeVal, mCount(cAtAttPdhDe2ErrorTypeVal), NULL);
    return CliAtAttControllerForceErrorStatusShow(argc, argv, errorType, CliDe2ListFromString);
    }

eBool CmdAtAttPdhDe2ForceErrorShow(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttPdhDe2ErrorTypeStr, cAtAttPdhDe2ErrorTypeVal, mCount(cAtAttPdhDe2ErrorTypeVal), NULL);
    return CliAtAttControllerForceErrorShow(argc, argv, errorType, CliDe2ListFromString);
    }

eBool CmdAtAttPdhDe2ForceAlarmNumEventPerSecond(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttPdhDe2AlarmTypeStr, cAtAttPdhDe2AlarmTypeVal, mCount(cAtAttPdhDe2AlarmTypeVal), NULL);
    return CliAtAttControllerForceAlarmNumEventSet(argc, argv, CliDe2ListFromString, alarmType);
    return cAtTrue;
    }

eBool CmdAtAttPdhDe2ForceAlarmModeSet(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttPdhDe2AlarmTypeStr, cAtAttPdhDe2AlarmTypeVal, mCount(cAtAttPdhDe2AlarmTypeVal), NULL);
    return CliAtAttControllerForceMode(argc, argv, CliDe2ListFromString, cAtFalse, alarmType);
    }

eBool CmdAtAttPdhDe2ForceAlarmDuration(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttPdhDe2AlarmTypeStr, cAtAttPdhDe2AlarmTypeVal, mCount(cAtAttPdhDe2AlarmTypeVal), NULL);
    return CliAtAttControllerForceAlarmDurationSet(argc, argv, CliDe2ListFromString, alarmType);
    }

eBool CmdAtAttPdhDe2ForceAlarmNumFrame(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttPdhDe2AlarmTypeStr, cAtAttPdhDe2AlarmTypeVal, mCount(cAtAttPdhDe2AlarmTypeVal), NULL);
    return CliAtAttControllerForceAlarmNumFrameSet(argc, argv, CliDe2ListFromString, alarmType);
    }

eBool CmdAtAttPdhDe2RxAlarmCaptureEnable(char argc, char **argv)
    {
    return CliAtAttControllerRxAlarmCapture(argc, argv, CliDe2ListFromString, cAtTrue);
    }

eBool CmdAtAttPdhDe2RxAlarmCaptureDisable(char argc, char **argv)
    {
    return CliAtAttControllerRxAlarmCapture(argc, argv, CliDe2ListFromString, cAtFalse);
    }

eBool CmdAtAttPdhDe2RxAlarmCaptureDuration(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttPdhDe2AlarmTypeStr, cAtAttPdhDe2AlarmTypeVal, mCount(cAtAttPdhDe2AlarmTypeVal), NULL);
    return CliAtAttControllerRxAlarmCaptureDuration(argc, argv, CliDe2ListFromString, alarmType);
    }

eBool CmdAtAttPdhDe2RxAlarmCaptureMode(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttPdhDe2AlarmTypeStr, cAtAttPdhDe2AlarmTypeVal, mCount(cAtAttPdhDe2AlarmTypeVal), NULL);
    return CliAtAttControllerRxAlarmCaptureMode(argc, argv, CliDe2ListFromString, alarmType);
    }

eBool CmdAtAttPdhDe2RxAlarmCaptureNumEvent(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttPdhDe2AlarmTypeStr, cAtAttPdhDe2AlarmTypeVal, mCount(cAtAttPdhDe2AlarmTypeVal), NULL);
    return CliAtAttControllerRxAlarmCaptureNumEvent(argc, argv, CliDe2ListFromString, alarmType);
    }

eBool CmdAtAttPdhDe2ForceAlarmStepSet(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttPdhDe2AlarmTypeStr, cAtAttPdhDe2AlarmTypeVal, mCount(cAtAttPdhDe2AlarmTypeVal), NULL);
    return CliAtAttControllerForceAlarmStepSet(argc, argv, CliDe2ListFromString, alarmType);
    }

eBool CmdAtAttPdhDe2ForceAlarm(char argc, char **argv)
    {
    return CliAtAttControllerForceAlarm(argc, argv, CliDe2ListFromString, CliAttPdhDe2AlarmTypeMaskFromString);
    }

eBool CmdAtAttPdhDe2FrequencyOffsetSet(char argc, char **argv)
    {
    return CliAtAttControllerFrequencyOffsetSet(argc, argv, CliDe2ListFromString);
    }

eBool CmdAtAttPdhDe2WanderFileName(char argc, char **argv)
    {
    return CliAtAttControllerWanderFileNameSet(argc, argv, CliDe2ListFromString);
    }

eBool CmdAtAttPdhDe2WanderStart(char argc, char **argv)
    {
    return CliAtAttControllerWanderStart(argc, argv, CliDe2ListFromString);
    }

eBool CmdAtAttPdhDe2WanderStop(char argc, char **argv)
    {
    return CliAtAttControllerWanderStop(argc, argv, CliDe2ListFromString);
    }

eBool CmdAtAttPdhDe2ForceAlarmStatusShow(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttPdhDe2AlarmTypeStr, cAtAttPdhDe2AlarmTypeVal, mCount(cAtAttPdhDe2AlarmTypeVal), NULL);
    return CliAtAttControllerForceAlarmStatusShow(argc, argv, alarmType, CliDe2ListFromString);
    }

eBool CmdAtAttPdhDe2ForceAlarmShow(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttPdhDe2AlarmTypeStr, cAtAttPdhDe2AlarmTypeVal, mCount(cAtAttPdhDe2AlarmTypeVal), NULL);
    return CliAtAttControllerForceAlarmShow(argc, argv, alarmType, CliDe2ListFromString);
    }

eBool CmdAtAttPdhDe2RxAlarmCaptureShow(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttPdhDe2AlarmTypeStr, cAtAttPdhDe2AlarmTypeVal, mCount(cAtAttPdhDe2AlarmTypeVal), NULL);
    return CliAtAttControllerRxAlarmCaptureShow(argc, argv, alarmType, CliDe2ListFromString);
    }

eBool CmdAtAttPdhDe2FrequencyOffsetShow(char argc, char **argv)
    {
    return CliAtAttControllerFrequencyOffsetShow(argc, argv, CliDe2ListFromString);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : CliAttSdhLine.c
 *
 * Created Date: Jul 6, 2016
 *
 * Description : SDH line ATT CLI
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtSdhLine.h"
#include "AtAttController.h"
#include "CliAtAttController.h"
#include "../sdh/CliAtModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static const char *cAtAttSdhLineAlarmTypeStr[] =
    {
    "los",
    "lof",
    "oof",
    "ais",
    "rdi"
    };

static const char *cAtAttSdhLineErrorTypeStr[] =
    {
    "b1",
    "b2",
    "rei"
    };

static const uint32 cAtAttSdhLineAlarmTypeVal[] =
    {
     cAtSdhLineAlarmLos,
     cAtSdhLineAlarmLof,
     cAtSdhLineAlarmOof,
     cAtSdhLineAlarmAis,
     cAtSdhLineAlarmRdi
    };

static const uint32 cAtAttSdhLineErrorTypeVal[] =
    {
     cAtSdhLineErrorB1,
     cAtSdhLineErrorB2,
     cAtSdhLineErrorRei
    };

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 CliAttSdhLineAlarmTypeMaskFromString(char *pAlarmTypeStr)
    {
    return CliMaskFromString(pAlarmTypeStr, cAtAttSdhLineAlarmTypeStr, cAtAttSdhLineAlarmTypeVal, mCount(cAtAttSdhLineAlarmTypeVal));
    }

static uint32 CliAttSdhLineErrorTypeMaskFromString(char *pErrorTypeStr)
    {
    eBool     result = cAtFalse;
    uint32    errorType;
    mAtStrToEnum(cAtAttSdhLineErrorTypeStr, cAtAttSdhLineErrorTypeVal, pErrorTypeStr, errorType, result);
    if (!result)
        return 0xFF;
    return errorType;
    }

eBool CmdAtAttSdhLineForceErrorMode(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttSdhLineErrorTypeStr, cAtAttSdhLineErrorTypeVal, mCount(cAtAttSdhLineErrorTypeVal), NULL);
    return CliAtAttControllerForceMode(argc, argv, CliSdhLineArrayFromString, cAtTrue, errorType);
    }

eBool CmdAtAttSdhLineForceErrorRate(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttSdhLineErrorTypeStr, cAtAttSdhLineErrorTypeVal, mCount(cAtAttSdhLineErrorTypeVal), NULL);
    return CliAtAttControllerForceErrorRate(argc, argv, CliSdhLineArrayFromString, errorType);
    }

eBool CmdAtAttSdhLineForceErrorDuration(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttSdhLineErrorTypeStr, cAtAttSdhLineErrorTypeVal, mCount(cAtAttSdhLineErrorTypeVal), NULL);
    return CliAtAttControllerForceErrorDuration(argc, argv, CliSdhLineArrayFromString, errorType);
    }

eBool CmdAtAttSdhLineForceErrorStepSet(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttSdhLineErrorTypeStr, cAtAttSdhLineErrorTypeVal, mCount(cAtAttSdhLineErrorTypeVal), NULL);
    return CliAtAttControllerForceErrorStepSet(argc, argv, CliSdhLineArrayFromString, errorType);
    }

eBool CmdAtAttSdhLineForceErrorPerSecond(char argc, char **argv)
    {
    return CliAtAttControllerForceErrorPerSecond(argc, argv, CliSdhLineArrayFromString, CliAttSdhLineErrorTypeMaskFromString);
    }

eBool CmdAtAttSdhLineForceError(char argc, char **argv)
    {
    return CliAtAttControllerForceError(argc, argv, CliSdhLineArrayFromString, CliAttSdhLineErrorTypeMaskFromString);
    }

eBool CmdAtAttSdhLineForceErrorNumErrorsSet(char argc, char **argv)
    {
    return CliAtAttControllerForceErrorNumErrorsSet(argc, argv, CliSdhLineArrayFromString, CliAttSdhLineErrorTypeMaskFromString);
    }

eBool CmdAtAttSdhLineForceErrorBitPostionSet(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttSdhLineErrorTypeStr, cAtAttSdhLineErrorTypeVal, mCount(cAtAttSdhLineErrorTypeVal), NULL);
    return CliAtAttControllerForceBitPositionSet(argc, argv, CliSdhLineArrayFromString, cAtTrue, errorType);
    }

eBool CmdAtAttSdhLineForceErrorDataMaskSet(char argc, char **argv)
    {
    return CliAtAttControllerForceErrorDataMaskSet(argc, argv, CliSdhLineArrayFromString, CliAttSdhLineErrorTypeMaskFromString);
    }

eBool CmdAtAttSdhLineForceAlarmModeSet(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttSdhLineAlarmTypeStr, cAtAttSdhLineAlarmTypeVal, mCount(cAtAttSdhLineAlarmTypeVal), NULL);
    return CliAtAttControllerForceMode(argc, argv, CliSdhLineArrayFromString, cAtFalse, alarmType);
    }

eBool CmdAtAttSdhLineForceAlarmNumEventPerSecond(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttSdhLineAlarmTypeStr, cAtAttSdhLineAlarmTypeVal, mCount(cAtAttSdhLineAlarmTypeVal), NULL);
    return CliAtAttControllerForceAlarmNumEventSet(argc, argv, CliSdhLineArrayFromString, alarmType);
    }

eBool CmdAtAttSdhLineForceAlarmDuration(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttSdhLineAlarmTypeStr, cAtAttSdhLineAlarmTypeVal, mCount(cAtAttSdhLineAlarmTypeVal), NULL);
    return CliAtAttControllerForceAlarmDurationSet(argc, argv, CliSdhLineArrayFromString, alarmType);
    }

eBool CmdAtAttSdhLineForceAlarmNumFrame(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttSdhLineAlarmTypeStr, cAtAttSdhLineAlarmTypeVal, mCount(cAtAttSdhLineAlarmTypeVal), NULL);
    return CliAtAttControllerForceAlarmNumFrameSet(argc, argv, CliSdhLineArrayFromString, alarmType);
    }

eBool CmdAtAttSdhLineRxAlarmCaptureEnable(char argc, char **argv)
    {
    return CliAtAttControllerRxAlarmCapture(argc, argv, CliSdhLineArrayFromString, cAtTrue);
    }

eBool CmdAtAttSdhLineRxAlarmCaptureDisable(char argc, char **argv)
    {
    return CliAtAttControllerRxAlarmCapture(argc, argv, CliSdhLineArrayFromString, cAtFalse);
    }

eBool CmdAtAttSdhLineRxAlarmCaptureDuration(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttSdhLineAlarmTypeStr, cAtAttSdhLineAlarmTypeVal, mCount(cAtAttSdhLineAlarmTypeVal), NULL);
    return CliAtAttControllerRxAlarmCaptureDuration(argc, argv, CliSdhLineArrayFromString, alarmType);
    }

eBool CmdAtAttSdhLineRxAlarmCaptureMode(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttSdhLineAlarmTypeStr, cAtAttSdhLineAlarmTypeVal, mCount(cAtAttSdhLineAlarmTypeVal), NULL);
    return CliAtAttControllerRxAlarmCaptureMode(argc, argv, CliSdhLineArrayFromString, alarmType);
    }

eBool CmdAtAttSdhLineRxAlarmCaptureNumEvent(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttSdhLineAlarmTypeStr, cAtAttSdhLineAlarmTypeVal, mCount(cAtAttSdhLineAlarmTypeVal), NULL);
    return CliAtAttControllerRxAlarmCaptureNumEvent(argc, argv, CliSdhLineArrayFromString, alarmType);
    }

eBool CmdAtAttSdhLineForceAlarmStepSet(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttSdhLineAlarmTypeStr, cAtAttSdhLineAlarmTypeVal, mCount(cAtAttSdhLineAlarmTypeVal), NULL);
    return CliAtAttControllerForceAlarmStepSet(argc, argv, CliSdhLineArrayFromString, alarmType);
    }

eBool CmdAtAttSdhLineForceAlarm(char argc, char **argv)
    {
    return CliAtAttControllerForceAlarm(argc, argv, CliSdhLineArrayFromString, CliAttSdhLineAlarmTypeMaskFromString);
    }

eBool CmdAtAttSdhLineForceAlarmStatusShow(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttSdhLineAlarmTypeStr, cAtAttSdhLineAlarmTypeVal, mCount(cAtAttSdhLineAlarmTypeVal), NULL);
    return CliAtAttControllerForceAlarmStatusShow(argc, argv, alarmType, CliSdhLineArrayFromString);
    }

eBool CmdAtAttSdhLineForceAlarmShow(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttSdhLineAlarmTypeStr, cAtAttSdhLineAlarmTypeVal, mCount(cAtAttSdhLineAlarmTypeVal), NULL);
    return CliAtAttControllerForceAlarmShow(argc, argv, alarmType, CliSdhLineArrayFromString);
    }

eBool CmdAtAttSdhLineRxAlarmCaptureShow(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttSdhLineAlarmTypeStr, cAtAttSdhLineAlarmTypeVal, mCount(cAtAttSdhLineAlarmTypeVal), NULL);
    return CliAtAttControllerRxAlarmCaptureShow(argc, argv, alarmType, CliSdhLineArrayFromString);
    }

eBool CmdAtAttSdhLineForceErrorStatusShow(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttSdhLineErrorTypeStr, cAtAttSdhLineErrorTypeVal, mCount(cAtAttSdhLineErrorTypeVal), NULL);
    return CliAtAttControllerForceErrorStatusShow(argc, argv, errorType, CliSdhLineArrayFromString);
    }

eBool CmdAtAttSdhLineForceErrorShow(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttSdhLineErrorTypeStr, cAtAttSdhLineErrorTypeVal, mCount(cAtAttSdhLineErrorTypeVal), NULL);
    return CliAtAttControllerForceErrorShow(argc, argv, errorType, CliSdhLineArrayFromString);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ATT
 *
 * File        : CliAtAttController.c
 *
 * Created Date: May 17, 2016
 *
 * Description : ATT controller CLI
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "CliAtAttController.h"
#include "AtAttController.h"
#include "../pdh/CliAtModulePdh.h"
#include <stdlib.h>
#include "time.h"
#include "../../../driver/include/att/AtAttExpect.h"
/*--------------------------- Define -----------------------------------------*/
#define cNumBitsInDword 32

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static const char * cAtAttForceAlarmModeStr[] ={"continous", "set_in_t", "set_in_nframe", "nevent_in_t"};
static const eAtAttForceAlarmMode cAtAttForceAlarmModeValue[] = {cAtAttForceAlarmModeContinuous, cAtAttForceAlarmModeSetInT,cAtAttForceAlarmModeSetInNFrame, cAtAttForceAlarmModeNeventInT};

static const char * cAtAttForceErrorModeStr[] ={"continous", "rate", "oneshot", "nerror_in_t", "consecutive"};
static const eAtAttForceErrorMode cAtAttForceErrorModeValue[] = {cAtAttForceErrorModeContinuous, cAtAttForceErrorModeRate, cAtAttForceErrorModeOneshot, cAtAttForceErrorModeNerrorInT, cAtAttForceErrorModeConsecutive};

static const char * cAtAttForcePointerAdjModeStr[] ={"continous", "oneshot", "nevent_in_t",
                                                     "g783a", "g783b", "g783c", "g783d", "g783e", "g783f",
                                                     "g783g_part1","g783g_part2","g783g_part3","g783g_part4",
                                                     "g783h_parta","g783h_partb","g783h_partc","g783h_partd",
                                                     "g783i",
                                                     "g783j_parta","g783j_partb","g783j_partc","g783j_partd"
                                                     };
static const eAtAttForcePointerAdjMode cAtAttForcePointerAdjModeValue[] = {cAtAttForcePointerAdjModeContinuous,
                                                                           cAtAttForcePointerAdjModeOneshot,
                                                                           cAtAttForcePointerAdjModeNeventInT,
                                                                           cAtAttForcePointerAdjModeG783a,
                                                                           cAtAttForcePointerAdjModeG783b,
                                                                           cAtAttForcePointerAdjModeG783c,
                                                                           cAtAttForcePointerAdjModeG783d,
                                                                           cAtAttForcePointerAdjModeG783e,
                                                                           cAtAttForcePointerAdjModeG783f,
                                                                           cAtAttForcePointerAdjModeG783gPart1,
                                                                           cAtAttForcePointerAdjModeG783gPart2,
                                                                           cAtAttForcePointerAdjModeG783gPart3,
                                                                           cAtAttForcePointerAdjModeG783gPart4,
                                                                           cAtAttForcePointerAdjModeG783hParta,
                                                                           cAtAttForcePointerAdjModeG783hPartb,
                                                                           cAtAttForcePointerAdjModeG783hPartc,
                                                                           cAtAttForcePointerAdjModeG783hPartd,
                                                                           cAtAttForcePointerAdjModeG783i,
                                                                           cAtAttForcePointerAdjModeG783jParta,
                                                                           cAtAttForcePointerAdjModeG783jPartb,
                                                                           cAtAttForcePointerAdjModeG783jPartc,
                                                                           cAtAttForcePointerAdjModeG783jPartd};
/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
const char* AtCliTime2String(tAtOsalCurTime *t);
/*--------------------------- Implementation ---------------------------------*/
static AtAttController AttController(AtChannel channel)
    {
    return (AtAttController)AtChannelAttController(channel);
    }

static eBool ErrorForce(char argc, char **argv,
                        uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                        uint32 (*MaskFromString)(char *pErrorTypeStr),
                        eAtRet (*ErrorForceFunction)(AtAttController self, uint32 type),
                        eAtRet (*ErrorUnForceFunction)(AtAttController self, uint32 type))
    {
    uint32 numChannels, i;
    AtChannel *channelList;
    uint32 errorTypeMask;
    eBool force;
    eAtRet ret = cAtOk;
    eAtRet (*ForceControlFunction)(AtAttController self, uint32 type);

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = ChannelListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid channel list, ...\n");
        return cAtFalse;
        }

    errorTypeMask = MaskFromString(argv[1]);
    force = CliBoolFromString(argv[2]);
    ForceControlFunction = (force) ? ErrorForceFunction : ErrorUnForceFunction;
    for (i = 0; i < numChannels; i++)
        {
        ret |= ForceControlFunction(AttController(channelList[i]), errorTypeMask);
        }

    return (ret == cAtOk) ? cAtTrue : cAtFalse;
    }

static eBool ForceErrorRate(char argc, char **argv,
                            eAtRet (*fForceErrorRate)(AtAttController, uint32, eAtBerRate),
                            uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                            uint32 errorType)
    {
    eBool isOk = cAtTrue;
    AtChannel *channelList;
    uint32 numChannels=0, i=0;
    eAtBerRate rate = cAtBerRateUnknown;
    eAtRet ret = cAtOk;

    if (argc != 3)
        return cAtFalse;

    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = ChannelListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid channel list, ...\n");
        return cAtFalse;
        }

    rate = CliBerRateFromString(argv[2]);
    if(rate == cAtBerRateUnknown)
       {
       AtPrintc(cSevCritical, "ERROR: Invalid ber rate. Expected: %s\r\n", CliValidBerRates());
       ret = cAtFalse;
       }
    for (i = 0; i < numChannels; i++)
        {
        ret |= fForceErrorRate(AttController(channelList[i]), errorType, rate);
        if (ret!=cAtOk)
            {
            isOk= cAtFalse;
            AtPrintc(cSevCritical, "ERROR: \n");
            continue;
            }
        }
    return isOk;
    }

static eBool ForceErrorDebug(char argc, char **argv,
                             eAtRet (*fForceErrorDebug)(AtAttController, uint32),
                             uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                             uint32 errorType)
    {
    eBool isOk = cAtTrue;
    AtChannel *channelList;
    uint32 numChannels=0, i=0;
    eAtRet ret = cAtOk;

    if (argc != 2)
        return cAtFalse;

    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = ChannelListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid channel list, ...\n");
        return cAtFalse;
        }
    for (i = 0; i < numChannels; i++)
        {
        ret |= fForceErrorDebug(AttController(channelList[i]), errorType);
        if (ret!=cAtOk)
            {
            isOk = cAtFalse;
            AtPrintc(cSevCritical, "ERROR: %s\n", AtObjectToString((AtObject)channelList[i]));
            }
        }
    return isOk;
    }

static eAtAttForceAlarmMode CliAlarmModeFromString(const char *alarmModeString)
    {
    eAtAttForceAlarmMode alarmMode = cAtAttForceAlarmModeContinuous;
    eBool convertSuccess;

    mAtStrToEnum(cAtAttForceAlarmModeStr,
                 cAtAttForceAlarmModeValue,
                 alarmModeString,
                 alarmMode,
                 convertSuccess);
    return convertSuccess ? alarmMode : cAtAttForceAlarmModeContinuous;
    }

static eBool ForceAlarmMode(char argc, char **argv,
                            eAtRet (*fForceAlarmMode)(AtAttController, uint32, eAtAttForceAlarmMode),
                            uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                            uint32 errorType)
    {
    eBool isOk=cAtTrue;
    AtChannel *channelList;
    uint32 numChannels=0, i=0;
    uint32 mode = 0;
    eAtRet ret = cAtOk;

    if (argc != 3)
        return cAtFalse;

    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = ChannelListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid channel list, ...\n");
        return cAtFalse;
        }

    mode = CliAlarmModeFromString(argv[2]);
    for (i = 0; i < numChannels; i++)
        {
        ret |= fForceAlarmMode(AttController(channelList[i]), errorType, mode);
        if (ret!=cAtOk)
            {
            isOk = cAtFalse;
            AtPrintc(cSevCritical, "ERROR: %s\n", AtObjectToString((AtObject)channelList[i]));
            continue;
            }
        }
    return isOk;
    }

static eAtAttForceErrorMode CliErrorModeFromString(const char *errorModeString)
    {
    eAtAttForceErrorMode errorMode = cAtAttForceErrorModeContinuous;
    eBool convertSuccess;

    mAtStrToEnum(cAtAttForceErrorModeStr,
                 cAtAttForceErrorModeValue,
                 errorModeString,
                 errorMode,
                 convertSuccess);
    return convertSuccess ? errorMode : cAtAttForceErrorModeContinuous;
    }

static eAtAttForcePointerAdjMode CliPointerAdjModeFromString(const char *pointerAdjModeString)
    {
	eAtAttForcePointerAdjMode pointerMode = cAtAttForcePointerAdjModeContinuous;
    eBool convertSuccess;

    mAtStrToEnum(cAtAttForcePointerAdjModeStr,
                 cAtAttForcePointerAdjModeValue,
				 pointerAdjModeString,
				 pointerMode,
                 convertSuccess);
    return convertSuccess ? pointerMode : cAtAttForcePointerAdjModeContinuous;
    }

static eBool ForceErrorMode(char argc, char **argv,
                            eAtRet (*fForceErrorMode)(AtAttController, uint32, eAtAttForceErrorMode),
                            uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                            uint32 errorType)
    {
    eBool isOk = cAtTrue;
    AtChannel *channelList;
    uint32 numChannels=0, i=0;
    uint32 mode = 0;
    eAtRet ret = cAtOk;

    if (argc != 3)
        return cAtFalse;

    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = ChannelListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid channel list, ...\n");
        return cAtFalse;
        }

    mode = CliErrorModeFromString(argv[2]);
    for (i = 0; i < numChannels; i++)
        {
        ret |= fForceErrorMode(AttController(channelList[i]), errorType, mode);
        if (ret!=cAtOk)
            {
            isOk= cAtFalse;
            AtPrintc(cSevCritical, "ERROR: %s\n", AtObjectToString((AtObject)channelList[i]));
            continue;
            }
        }
    return isOk;
    }

static eBool ForcePointerAdjMode(char argc, char **argv,
                            eAtRet (*fForcePointerAdjMode)(AtAttController, uint32, eAtAttForcePointerAdjMode),
                            uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
							uint32 (*MaskFromString)(char *pPointerAdjTypeStr))
    {
    eBool isOk = cAtTrue;
    AtChannel *channelList;
    uint32 numChannels=0, i=0;
    uint32 pointerType;
    uint32 mode = 0;
    eAtRet ret = cAtOk;

    if (argc != 3)
        return cAtFalse;

    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = ChannelListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid channel list, ...\n");
        return cAtFalse;
        }

    pointerType = MaskFromString(argv[1]);
    mode = CliPointerAdjModeFromString(argv[2]);

    for (i = 0; i < numChannels; i++)
        {
        ret |= fForcePointerAdjMode(AttController(channelList[i]), pointerType, mode);
        if (ret!=cAtOk)
            {
            isOk= cAtFalse;
            AtPrintc(cSevCritical, "ERROR: %s\n", AtObjectToString((AtObject)channelList[i]));
            continue;
            }
        }
    return isOk;
    }

static eBool ForceErrorDurationStep(char argc, char **argv,
                            eAtRet (*fForceErrorDurationStep)(AtAttController, uint32, uint32),
                            uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                            uint32 errorType)
    {
    eBool isOk = cAtTrue;
    AtChannel *channelList;
    uint32 numChannels=0, i=0;
    uint32 duration = 0;
    eAtRet ret = cAtOk;

    if (argc != 3)
        return cAtFalse;

    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = ChannelListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid channel list, ...\n");
        return cAtFalse;
        }

    duration = AtStrToDw(argv[2]);
    for (i = 0; i < numChannels; i++)
        {
        ret |= fForceErrorDurationStep(AttController(channelList[i]), errorType, duration);
        if (ret!=cAtOk)
            {
            isOk = cAtFalse;
            AtPrintc(cSevCritical, "ERROR: %s\n", AtObjectToString((AtObject)channelList[i]));
            continue;
            }
        }
    return isOk;
    }

static void AlarmChangeState(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    uint8 maskBitId;
    uint32 alarmType= 0;

    for (maskBitId = 0; maskBitId < cNumBitsInDword; maskBitId++)
        {
        AtAttController att = AtChannelAttController(channel);
        alarmType = cBit0 << maskBitId;
        if ((changedAlarms & alarmType) == 0)
            continue;
        if (AtAttControllerAlarmForceIsSupported(att,alarmType))
            {
            if (currentStatus & changedAlarms)
                AtAttControllerRecordTimeAdd(att, alarmType, "set");
            else
                AtAttControllerRecordTimeAdd(att, alarmType, "clear");
            }
        }
    }

static eBool RxAlarmCapture(char argc, char **argv,
                            uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                            /*uint32 alarmType,*/
                            eBool captureEnabled)
    {
    eBool isOk = cAtTrue;
    AtChannel *channelList;
    static tAtChannelEventListener intrListener;
    uint32 numChannels=0, i=0;
    eAtRet ret = cAtOk;

    if (argc != 1)
        return cAtFalse;

    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = ChannelListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid channel list, ...\n");
        return cAtFalse;
        }
    intrListener.AlarmChangeState = AlarmChangeState;

    for (i = 0; i < numChannels; i++)
        {
        AtChannel channel = channelList[i];
        /*AtUnused(alarmType);*/
        if (captureEnabled)
            ret = AtChannelEventListenerAdd(channel, &intrListener);
        else
            ret = AtChannelEventListenerRemove(channel, &intrListener);

        if (ret!=cAtOk)
            {
            isOk = cAtFalse;
            AtPrintc(cSevCritical, "ERROR: %s\n", AtObjectToString((AtObject)channelList[i]));
            continue;
            }
        }
    return isOk;
    }

eBool CliAtAttControllerForceErrorRate(char argc, char **argv,
                                       uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                       uint32 errorType)
    {
    return ForceErrorRate(argc, argv,
                          AtAttControllerForceErrorRate,
                          ChannelListFromString,
                          errorType);
    }

eBool CliAtAttControllerForceErrorDebug(char argc, char **argv,
                                      uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                      uint32 errorType)
    {
    return ForceErrorDebug(argc, argv,
                              AtAttControllerDebugError,
                              ChannelListFromString,
                              errorType);
    }

eBool CliAtAttControllerForceErrorDuration(char argc, char **argv,
                                      uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                      uint32 errorType)
    {
    return ForceErrorDurationStep(argc, argv,
                              AtAttControllerForceErrorDuration,
                              ChannelListFromString,
                              errorType);
    }

eBool CliAtAttControllerForceErrorStepSet(char argc, char **argv,
                                      uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                      uint32 errorType)
    {
    return ForceErrorDurationStep(argc, argv,
                              AtAttControllerForceErrorStepSet,
                              ChannelListFromString,
                              errorType);
    }

eBool CliAtAttControllerForceErrorStep2Set(char argc, char **argv,
                                      uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                      uint32 errorType)
    {
    return ForceErrorDurationStep(argc, argv,
                              AtAttControllerForceErrorStep2Set,
                              ChannelListFromString,
                              errorType);
    }

eBool CliAtAttControllerForceErrorPerSecond(char argc, char **argv,
                                            uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                            uint32 (*MaskFromString)(char *pErrorTypeStr))
    {
    return ErrorForce(argc, argv, ChannelListFromString, MaskFromString, AtAttControllerForceErrorPerSecond, AtAttControllerUnForceError);
    }

eBool CliAtAttControllerForceError(char argc, char **argv,
                                   uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                   uint32 (*MaskFromString)(char *pErrorTypeStr))
    {
    return ErrorForce(argc, argv, ChannelListFromString, MaskFromString, AtAttControllerForceError, AtAttControllerUnForceError);
    }

eBool CliAtAttControllerForceErrorContinuous(char argc, char **argv,
                                             uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                             uint32 (*MaskFromString)(char *pErrorTypeStr))
    {
    return ErrorForce(argc, argv, ChannelListFromString, MaskFromString, AtAttControllerForceErrorContinuous, AtAttControllerUnForceError);
    }

eBool CliAtAttControllerForceMode(char argc, char **argv,
                                      uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                      eBool isError,
                                      uint32 alarmType)
    {
    if (isError)
        return ForceErrorMode(argc, argv,
                                  AtAttControllerForceErrorModeSet,
                                  ChannelListFromString,
                                  alarmType);

    return ForceAlarmMode(argc, argv,
                          AtAttControllerForceAlarmModeSet,
                          ChannelListFromString,
                          alarmType);
    }

eBool CliAtAttControllerForceAlarmDurationSet(char argc, char **argv,
                                      uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                      uint32 alarmType)
    {
    return ForceErrorDurationStep(argc, argv,
                              AtAttControllerForceAlarmDurationSet,
                              ChannelListFromString,
                              alarmType);
    }

eBool CliAtAttControllerRxAlarmCapture(char argc, char **argv,
                                       uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                       /*uint32 alarmType,*/
                                       eBool captureEnabled)
     {
     return RxAlarmCapture(argc, argv,
                           ChannelListFromString,
                           /*alarmType,*/ captureEnabled);
     }

eBool CliAtAttControllerRxAlarmCaptureDuration(char argc, char **argv,
                                      uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                      uint32 alarmType)
    {
    return ForceErrorDurationStep(argc, argv,
                              AtAttControllerRxAlarmCaptureDurationSet,
                              ChannelListFromString,
                              alarmType);
    }

eBool CliAtAttControllerRxAlarmCaptureMode(char argc, char **argv,
                                      uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                      uint32 alarmType)
    {
    return ForceAlarmMode(argc, argv,
                          AtAttControllerRxAlarmCaptureModeSet,
                          ChannelListFromString,
                          alarmType);
    }

eBool CliAtAttControllerRxAlarmCaptureNumEvent(char argc, char **argv,
                                      uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                      uint32 alarmType)
    {
    return ForceErrorDurationStep(argc, argv,
                              AtAttControllerRxAlarmCaptureNumEventSet,
                              ChannelListFromString,
                              alarmType);
    }

eBool CliAtAttControllerForceAlarmNumEventSet(char argc, char **argv,
                                      uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                      uint32 alarmType)
    {
    return ForceErrorDurationStep(argc, argv,
                              AtAttControllerForceAlarmNumEventSet,
                              ChannelListFromString,
                              alarmType);
    }

eBool CliAtAttControllerForceAlarmNumFrameSet(char argc, char **argv,
                                      uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                      uint32 alarmType)
    {
    return ForceErrorDurationStep(argc, argv,
                              AtAttControllerForceAlarmNumFrameSet,
                              ChannelListFromString,
                              alarmType);
    }

eBool CliAtAttControllerForceAlarmStepSet(char argc, char **argv,
                                      uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                      uint32 alarmType)
    {
    return ForceErrorDurationStep(argc, argv,
                              AtAttControllerForceAlarmStepSet,
                              ChannelListFromString,
                              alarmType);
    }

static eBool ErrorForceIntegerAttributeSet(char argc, char **argv,
                                           uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                           uint32 (*MaskFromString)(char *pErrorTypeStr),
                                           eAtRet (*IntegerAttributeSetFunction)(AtAttController self, uint32 type, uint32 value))
    {
    uint32 numChannels, i;
    AtChannel *channelList;
    uint32 errorTypeMask;
    uint32 numErrors;
    eAtRet ret = cAtOk;

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = ChannelListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid channel list, ...\n");
        return cAtFalse;
        }

    errorTypeMask = MaskFromString(argv[1]);
    numErrors = AtStrToDw(argv[2]);
    for (i = 0; i < numChannels; i++)
        {
        ret |= IntegerAttributeSetFunction(AttController(channelList[i]), errorTypeMask, numErrors);
        }

    return (ret == cAtOk) ? cAtTrue : cAtFalse;
    }

eBool CliAtAttControllerForceErrorNumErrorsSet(char argc, char **argv,
                                               uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                               uint32 (*MaskFromString)(char *pErrorTypeStr))
    {
    return ErrorForceIntegerAttributeSet(argc, argv, ChannelListFromString, MaskFromString, AtAttControllerForceErrorNumErrorsSet);
    }

eBool CliAtAttControllerForceBitPositionSet(char argc, char **argv,
                                            uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                            eBool isError,
                                            uint32 errorType)
    {
    uint32 numChannels, i;
    AtChannel *channelList;
    uint32 errorTypeMask;
    uint32 *bitList;
    uint32 numBits;
    uint32 bitPosition;
    eAtRet ret = cAtOk;

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = ChannelListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid channel list, ...\n");
        return cAtFalse;
        }

    errorTypeMask = errorType;
    bitList = CliSharedIdBufferGet(&numBits);
    numBits  = CliIdListFromString(argv[2], bitList, numBits);
    if (numBits >16)
        return cAtFalse;

    bitPosition = 0;
    for (i = 0; i < numBits; i++)
        bitPosition |= cBit0 << bitList[i];

    for (i = 0; i < numChannels; i++)
        {
        if (isError)
            ret |= AtAttControllerForceErrorBitPositionSet(AttController(channelList[i]), errorTypeMask, bitPosition);
        else
            ret |= AtAttControllerForceAlarmBitPositionSet(AttController(channelList[i]), errorTypeMask, bitPosition);
        }

    return (ret == cAtOk) ? cAtTrue : cAtFalse;
    }

eBool CliAtAttControllerForceBitPosition2Set(char argc, char **argv,
                                            uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                            eBool isError,
                                            uint32 errorType)
    {
    uint32 numChannels, i;
    AtChannel *channelList;
    uint32 errorTypeMask;
    uint32 *bitList;
    uint32 numBits;
    uint32 bitPosition;
    eAtRet ret = cAtOk;

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = ChannelListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid channel list, ...\n");
        return cAtFalse;
        }

    errorTypeMask = errorType;
    bitList = CliSharedIdBufferGet(&numBits);
    numBits  = CliIdListFromString(argv[2], bitList, numBits);
    if (numBits >16)
        return cAtFalse;

    bitPosition = 0;
    for (i = 0; i < numBits; i++)
        bitPosition |= cBit0 << bitList[i];

    for (i = 0; i < numChannels; i++)
        {
        if (isError)
            ret |= AtAttControllerForceErrorBitPosition2Set(AttController(channelList[i]), errorTypeMask, bitPosition);
        }

    return (ret == cAtOk) ? cAtTrue : cAtFalse;
    }

eBool CliAtAttControllerForceErrorDataMaskSet(char argc, char **argv,
                                              uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                              uint32 (*MaskFromString)(char *pErrorTypeStr))
    {
    return ErrorForceIntegerAttributeSet(argc, argv, ChannelListFromString, MaskFromString, AtAttControllerForceErrorDataMaskSet);
    }

eBool CliAtAttControllerForceErrorCrcMaskSet(char argc, char **argv,
                                              uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                              uint32 (*MaskFromString)(char *pErrorTypeStr))
    {
    return ErrorForceIntegerAttributeSet(argc, argv, ChannelListFromString, MaskFromString, AtAttControllerForceErrorCrcMaskSet);
    }

eBool CliAtAttControllerForceErrorCrcMask2Set(char argc, char **argv,
                                              uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                              uint32 (*MaskFromString)(char *pErrorTypeStr))
    {
    return ErrorForceIntegerAttributeSet(argc, argv, ChannelListFromString, MaskFromString, AtAttControllerForceErrorCrcMask2Set);
    }

eBool CliAtAttControllerFrequencyOffsetSet(char argc, char **argv,
                        uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize))
    {
    uint32 numChannels, i;
    AtChannel *channelList;
    float ppm;
    eAtRet ret = cAtOk;

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = ChannelListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid channel list, ...\n");
        return cAtFalse;
        }

    ppm = strtof(argv[1], NULL);
    for (i = 0; i < numChannels; i++)
        {
        ret |= AtAttControllerFrequenceOffsetSet(AttController(channelList[i]), ppm);
        }

    return (ret == cAtOk) ? cAtTrue : cAtFalse;
    }

eBool CliAtAttControllerWanderFileNameSet(char argc, char **argv,
                        uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize))
    {
    uint32 numChannels, i;
    AtChannel *channelList;
    char* filename;
    eAtRet ret = cAtOk;

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = ChannelListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid channel list, ...\n");
        return cAtFalse;
        }

    filename = argv[1];
    for (i = 0; i < numChannels; i++)
        {
        AtAttWanderChannel wander = AtAttControllerWanderGet(AttController(channelList[i]));
        ret |= AtAttWanderChannelFileNameSet(wander, filename);
        }

    return (ret == cAtOk) ? cAtTrue : cAtFalse;
    }


eBool CliAtAttControllerWanderStart(char argc, char **argv,
                        uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize))
    {
    uint32 numChannels, i;
    AtChannel *channelList;
    uint32 duration;
    eAtRet ret = cAtOk;

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = ChannelListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid channel list, ...\n");
        return cAtFalse;
        }
    if (argc>1)
        duration = AtStrToDw(argv[1]);
    else
        duration = 0;
    for (i = 0; i < numChannels; i++)
        {
        AtAttWanderChannel wander = AtAttControllerWanderGet(AttController(channelList[i]));
        ret |= AtAttWanderChannelDurationSet(wander, duration);
        ret |= AtAttWanderChannelStart(wander);
        }

    return (ret == cAtOk) ? cAtTrue : cAtFalse;
    }

eBool CliAtAttControllerWanderStop(char argc, char **argv,
                        uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize))
    {
    uint32 numChannels, i;
    AtChannel *channelList;
    eAtRet ret = cAtOk;

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = ChannelListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid channel list, ...\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i++)
        {
        AtAttWanderChannel wander = AtAttControllerWanderGet(AttController(channelList[i]));
        ret |= AtAttWanderChannelStop(wander);
        }

    return (ret == cAtOk) ? cAtTrue : cAtFalse;
    }

eBool CliAtAttControllerForceErrorStatusShow(char argc, char **argv, uint32 errorType,
                                             uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize))
    {
    tTab *tabPtr;
    uint32 numChannels, i;
    AtChannel *channelList;
    const char *pHeading[] = {"ID", "PositionMask Counter", "RemainForce", "ErrorStatus", "OneSecondEnableStatus"};

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = ChannelListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No channel to display\r\n");
        return cAtTrue;
        }

    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i++)
        {
        uint8 column = 0;
        tAtAttForceErrorStatus status;

        AtAttControllerForceErrorStatusGet(AttController(channelList[i]), errorType, &status);
        StrToCell(tabPtr, i, column++, CliChannelIdStringGet((AtChannel)channelList[i]));
        StrToCell(tabPtr, i, column++, CliNumber2String(status.positionMaskStatus, "%d"));
        StrToCell(tabPtr, i, column++, CliNumber2String(status.remainForceErrorStatus, "%d"));
        StrToCell(tabPtr, i, column++, (status.errorStatus == 0) ? "clear" : "set");
        StrToCell(tabPtr, i, column++, (status.oneSecondEnableStatus == 0) ? "done" : "forcing");
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

static const char * ForceTypeString(eAtAttForceType forceType)
    {
    if (forceType == cAtAttForceTypeOneShot)
        return "oneshot";
    if (forceType == cAtAttForceTypeContinuous)
        return "continuous";
    return "none";
    }

eBool CliAtAttControllerForceErrorShow(char argc, char **argv, uint32 errorType,
                                       uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize))
    {
    tTab *tabPtr;
    uint32 numChannels, i;
    AtChannel *channelList;
    const char *pHeading[] = {"DE3 ID", "PositionMaskCounter", "RemainForce", "ErrorStatus", "OneSecondEnableStatus", "BitPosition", "DataMask", "NumErrors", "ForceType", "Step"};

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = ChannelListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No channel to display\r\n");
        return cAtTrue;
        }

    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i++)
        {
        uint8 column = 0;
        tAtAttForceErrorConfiguration configuration;
        tAtAttForceErrorStatus status;

        AtAttControllerForceErrorStatusGet(AttController(channelList[i]), errorType, &status);
        AtAttControllerForceErrorGet(AttController(channelList[i]), errorType, &configuration);
        StrToCell(tabPtr, i, column++, CliChannelIdStringGet((AtChannel)channelList[i]));
        StrToCell(tabPtr, i, column++, CliNumber2String(status.positionMaskStatus, "%d"));
        StrToCell(tabPtr, i, column++, CliNumber2String(status.remainForceErrorStatus, "%d"));
        StrToCell(tabPtr, i, column++, (status.errorStatus == 0) ? "clear" : "set");
        StrToCell(tabPtr, i, column++, (status.oneSecondEnableStatus == 0) ? "done" : "forcing");
        StrToCell(tabPtr, i, column++, CliBitmap2String(configuration.bitPosition));
        StrToCell(tabPtr, i, column++, CliNumber2String(configuration.dataMask, "0x%X"));
        StrToCell(tabPtr, i, column++, CliNumber2String(configuration.numErrors, "%d"));
        StrToCell(tabPtr, i, column++, ForceTypeString(configuration.forceType));
        StrToCell(tabPtr, i, column++, CliNumber2String(AtAttControllerForceErrorStepGet(AttController(channelList[i]), errorType), "%d"));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

static eBool AlarmForce(char argc, char **argv,
                        uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                        uint32 (*MaskFromString)(char *pAlarmTypeStr),
                        eAtRet (*AlarmForceFunction)(AtAttController self, uint32 type))
    {
    uint32 numChannels, i;
    uint32 maskBitId;
    AtChannel *channelList;
    uint32 alarmTypeMask;
    eBool force;
    eAtRet ret = cAtOk;
    eAtRet (*ForceControlFunction)(AtAttController self, uint32 type);

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = ChannelListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid channel list, ...\n");
        return cAtFalse;
        }

    alarmTypeMask = MaskFromString(argv[1]);
    force = CliBoolFromString(argv[2]);
    ForceControlFunction = (force) ? AlarmForceFunction : AtAttControllerUnForceAlarm;
    for (i = 0; i < numChannels; i++)
        {
        for (maskBitId = 0; maskBitId < cNumBitsInDword; maskBitId++)
            {
            if (alarmTypeMask & (cBit0 << maskBitId))
                ret |= ForceControlFunction(AttController(channelList[i]), cBit0 << maskBitId);
            }
        }

    return (ret == cAtOk) ? cAtTrue : cAtFalse;
    }

eBool CliAtAttControllerForceAlarmPerSecond(char argc, char **argv,
                                            uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                            uint32 (*MaskFromString)(char *pAlarmTypeStr))
    {
    return AlarmForce(argc, argv, ChannelListFromString, MaskFromString, AtAttControllerForceAlarmPerSecond);
    }

eBool CliAtAttControllerForceAlarm(char argc, char **argv,
                                   uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                   uint32 (*MaskFromString)(char *pAlarmTypeStr))
    {
    return AlarmForce(argc, argv, ChannelListFromString, MaskFromString, AtAttControllerForceAlarm);
    }

eBool CliAtAttControllerForceAlarmContinuous(char argc, char **argv,
                                             uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                             uint32 (*MaskFromString)(char *pAlarmTypeStr))
    {
    return AlarmForce(argc, argv, ChannelListFromString, MaskFromString, AtAttControllerForceAlarmContinuous);
    }

eBool CliAtAttControllerForceAlarmShow(char argc, char **argv,
                                       uint32 alarmType,
                                       uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize))
    {
    tTab *tabPtr;
    uint32 numChannels, i;
    AtChannel *channelList;
    eBool success = cAtTrue;
    const char *pHeading[] = {"DE3 ID", "enableStatus", "secondStatus", "miliSecondStatus","ForceType", "NumEvent", "Duration"  };

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = ChannelListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No channel to display\r\n");
        return cAtTrue;
        }

    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i++)
        {
        uint8 column = 0;
        tAtAttForceAlarmStatus status;
        tAtAttForceAlarmConfiguration configuration;
        eAtRet ret = cAtOk;

        AtOsalMemInit(&status, 0, sizeof(tAtAttForceAlarmStatus));
        AtOsalMemInit(&configuration, 0, sizeof(configuration));
        ret |= AtAttControllerForceAlarmStatusGet(AttController(channelList[i]), alarmType, &status);
        ret |= AtAttControllerForceAlarmGet(AttController(channelList[i]), alarmType, &configuration);
        StrToCell(tabPtr, i, column++, CliChannelIdStringGet((AtChannel)channelList[i]));
        if (ret != cAtOk)
            {
            success = cAtFalse;
            StrToCell(tabPtr, i, column++, "Error");
            StrToCell(tabPtr, i, column++, "Error");
            StrToCell(tabPtr, i, column++, "Error");
            StrToCell(tabPtr, i, column++, "Error");
            StrToCell(tabPtr, i, column++, "Error");
            }
        else
            {
            StrToCell(tabPtr, i, column++, CliNumber2String(status.enableStatus, "%d"));
            StrToCell(tabPtr, i, column++, CliNumber2String(status.secondStatus, "%d"));
            StrToCell(tabPtr, i, column++, CliNumber2String(status.miliSecondStatus, "%d"));
            StrToCell(tabPtr, i, column++, ForceTypeString(configuration.forceType));
            StrToCell(tabPtr, i, column++, CliNumber2String(configuration.numEvent, "%d"));
            StrToCell(tabPtr, i, column++, CliNumber2String(configuration.duration, "%d"));
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return success;
    }

const char* AtCliTime2String(tAtOsalCurTime *t)
    {
    struct tm* tm_info;
    static char strTime[35];
    /*static char tmpTime[35];*/
    /*AtOsalMemInit(tmpTime, 0, sizeof(tmpTime));*/
    AtOsalMemInit(strTime, 0, sizeof(strTime));
    tm_info = localtime(((const time_t*)&t->sec));
    /*strftime(tmpTime, sizeof tmpTime, "%Y-%m-%d %H:%M:%S", tm_info);
    AtSnprintf(&strTime[AtStrlen(strTime)], 8, "%s.%06ld", tmpTime, (long)t->usec);*/
    strftime(strTime, 26, "%Y-%m-%d %H:%M:%S", tm_info);
    AtSnprintf(&strTime[AtStrlen(strTime)], 8, ".%06ld", (long)t->usec);
    return strTime;
    }

eBool CliAtAttControllerRxAlarmCaptureShow(char argc, char **argv,
                                           uint32 alarmType,
                                           uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize))
        {
        uint32 numChannels=0, i=0, row=0;
        eBool silent = cAtFalse;
        eBool success = cAtTrue;
        tTab *tabPtr;
        AtChannel *channelList;
        const char *pHeading[] = {"ID", "Mode", "Expect Duration(s)", "Expected NumEvent", "Num Event", "Num SET","Num CLEAR", "Diff(ms)", "IsOk" };
        eAtHistoryReadingMode readingMode;
        AtUnused(argc);

        /* Get the shared buffer to hold all of objects, then call the parse function */
        channelList = CliSharedChannelListGet(&numChannels);
        numChannels = ChannelListFromString(argv[0], channelList, numChannels);
        if (numChannels == 0)
            {
            AtPrintc(cSevNormal, "No channel to display\r\n");
            return cAtTrue;
            }
        readingMode = CliHistoryReadingModeGet(argv[2]);
        if (readingMode == cAtHistoryReadingModeUnknown)
            {
            AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
            return cAtFalse;
            }
        silent = CliHistoryReadingShouldSilent(readingMode, (argc > 3) ? argv[3] : NULL);

        if (!silent)
            {
            tabPtr = TableAlloc(0, mCount(pHeading), pHeading);
            if (!tabPtr)
                {
                AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
                return cAtFalse;
                }
            }

        for (i = 0; i < numChannels; i++)
            {
            eBool result=cAtTrue;
            char buf[100];
            uint8 column = 0;
            AtAttController att = AttController(channelList[i]);
            AtList times= AtAttControllerRecordTime(att, alarmType);
            uint32 duration = AtAttControllerRxAlarmCaptureDurationGet(att, alarmType);
            eAtAttForceAlarmMode mode = AtAttControllerRxAlarmCaptureModeGet(att, alarmType);
            uint32 numevent = AtAttControllerRxAlarmCaptureNumEventGet(att, alarmType);
            if (!silent)
                {
                TableAddRow(tabPtr, 1);
                StrToCell(tabPtr, row, column++, CliChannelIdStringGet((AtChannel)channelList[i]));

                AtOsalMemInit(buf, 0, sizeof(buf));
                mAtEnumToStr(cAtAttForceAlarmModeStr, cAtAttForceAlarmModeValue, mode, buf, result);
                if(!result)
                    {
                    success = cAtFalse;
                    }
                StrToCell(tabPtr, row, column++, buf);

                StrToCell(tabPtr, row, column++, CliNumber2String(duration, "%d"));
                StrToCell(tabPtr, row, column++, CliNumber2String(numevent, "%d"));
                if (times == NULL)
                    {
                    StrToCell(tabPtr, row, column++, "-");
                    StrToCell(tabPtr, row, column++, "-");
                    StrToCell(tabPtr, row, column++, "-");
                    StrToCell(tabPtr, row, column++, "-");
                    StrToCell(tabPtr, row, column++, "-");
                    success = cAtFalse;
                    row ++;
                    }
                else
                    {
                    eBool isOkToClear  = AtAttControllerRecordTimeIsOkToClear(att, alarmType);
                    while (!isOkToClear)
                        {
                        AtOsalUSleep(500000);
                        isOkToClear  = AtAttControllerRecordTimeIsOkToClear(att, alarmType);
                        }

                    if (isOkToClear)
                        {
                        uint32 length = AtListLengthGet(times);
                        uint32 numset   = AtAttControllerRecordTimeNumSet(att, alarmType);
                        uint32 numclear = AtAttControllerRecordTimeNumClear(att, alarmType);
                        uint32 diff     = AtAttControllerRecordTimeDiffGet(att, alarmType);
                        eBool isOk      = AtAttControllerRecordTimeIsOk(att, alarmType);
                        StrToCell(tabPtr, row, column++, CliNumber2String(length, "%d"));
                        StrToCell(tabPtr, row, column++, CliNumber2String(numset, "%d"));
                        StrToCell(tabPtr, row, column++, CliNumber2String(numclear, "%d"));
                        StrToCell(tabPtr, row, column++, CliNumber2String(diff, "%d"));
                        StrToCell(tabPtr, row, column++, CliNumber2String(isOk?"OK":"FAIL", "%s"));
                        if (length && isOk==cAtFalse)
                            {
                            uint32 time_i;
                            for (time_i=0; time_i<length; time_i++)
                                {
                                tAtAttExpectChannelTime *t=NULL;
                                TableAddRow(tabPtr, 1);
                                row++;
                                t = (tAtAttExpectChannelTime*)AtListObjectGet((AtList)times, time_i);
                                StrToCell(tabPtr, row, 3, AtCliTime2String(&t->time));
                                t = (tAtAttExpectChannelTime*)AtListObjectGet((AtList)times, time_i);
                                StrToCell(tabPtr, row, 4, t->set?"SET":"CLEAR");
                                }
                            }
                        }
                    }
                }

            if (readingMode == cAtHistoryReadingModeReadToClear)
                AtAttControllerRecordTimeClear(att, alarmType);

            if (!silent)
                row++;
            }

        if (!silent)
            {
            TablePrint(tabPtr);
            TableFree(tabPtr);
            }

        return success;
        }

eBool CliAtAttControllerForceAlarmStatusShow(char argc, char **argv, uint32 alarmType,
                                             uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize))
    {
    tTab *tabPtr;
    uint32 numChannels, i;
    AtChannel *channelList;
    eBool success = cAtTrue;
    const char *pHeading[] = {"ID", "positionIndex", "eventCounter", "secondCounter", "updatedStatus", "miliSecondStatus","secondStatus","enableStatus" };

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = ChannelListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No channel to display\r\n");
        return cAtTrue;
        }

    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i++)
        {
        uint8 column = 0;
        tAtAttForceAlarmStatus status;
        eAtRet ret = AtAttControllerForceAlarmStatusGet(AttController(channelList[i]), alarmType, &status);
        StrToCell(tabPtr, i, column++, CliChannelIdStringGet((AtChannel)channelList[i]));

        if (ret != cAtOk)
            {
            success = cAtFalse;
            StrToCell(tabPtr, i, column++, "Error");
            StrToCell(tabPtr, i, column++, "Error");
            StrToCell(tabPtr, i, column++, "Error");
            StrToCell(tabPtr, i, column++, "Error");
            StrToCell(tabPtr, i, column++, "Error");
            StrToCell(tabPtr, i, column++, "Error");
            StrToCell(tabPtr, i, column++, "Error");
            }
        else
            {
            StrToCell(tabPtr, i, column++, CliNumber2String(status.positionIndex, "%d"));
            StrToCell(tabPtr, i, column++, CliNumber2String(status.eventCounter, "%d"));
            StrToCell(tabPtr, i, column++, CliNumber2String(status.secondCounter, "%d"));
            StrToCell(tabPtr, i, column++, CliNumber2String(status.updatedStatus, "%d"));
            StrToCell(tabPtr, i, column++, CliNumber2String(status.miliSecondStatus, "%d"));
            StrToCell(tabPtr, i, column++, CliNumber2String(status.secondStatus, "%d"));
            StrToCell(tabPtr, i, column++, CliNumber2String(status.enableStatus, "%d"));
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return success;
    }

eBool CliAtAttControllerFrequencyOffsetShow(char argc, char **argv,
                                             uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize))
    {
    tTab *tabPtr;
    uint32 numChannels, i;
    AtChannel *channelList;
    eBool success = cAtTrue;
    float ppm;
    char buf[80];
    const char *pHeading[] = {"ID", "Frequency Offset(ppm)"};

    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = ChannelListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No channel to display\r\n");
        return cAtTrue;
        }

    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i++)
        {
        uint8 column = 0;
        ppm = AtAttControllerFrequenceOffsetGet(AttController(channelList[i]));
        StrToCell(tabPtr, i, column++, CliChannelIdStringGet((AtChannel)channelList[i]));
        AtSprintf(buf, "%.3f", ppm);
        StrToCell(tabPtr, i, column++, buf);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return success;
    }

eBool CliAtAttControllerBipCountShow(char argc, char **argv,
                                     uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize))
    {
    tTab *tabPtr;
    uint32 numChannels, i;
    AtChannel *channelList;
    eBool success = cAtTrue, r2c=cAtFalse, silent=cAtFalse;
    uint32 bipcnt;
    char buf[80];
    const char *pHeading[] = {"ID", "BipCnt"};
    eAtHistoryReadingMode readingMode = cAtHistoryReadingModeReadOnly;
    /*---------------------------------------------------------*/
    AtUnused(argc);
    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = ChannelListFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No channel to display\r\n");
        return cAtTrue;
        }
    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }
    r2c = (readingMode == cAtHistoryReadingModeReadToClear) ? cAtTrue : cAtFalse;
    silent = CliHistoryReadingShouldSilent(readingMode, (argc > 2) ? argv[1] : NULL);
    /*---------------------------------------------------------*/
    if (!silent)
        {
        tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
            return cAtFalse;
            }
        }

    for (i = 0; i < numChannels; i++)
        {
        uint8 column = 0;
        bipcnt = AtAttControllerBipCounterGet(AttController(channelList[i]), r2c);
        StrToCell(tabPtr, i, column++, CliChannelIdStringGet((AtChannel)channelList[i]));
        AtSprintf(buf, "%d", bipcnt);
        StrToCell(tabPtr, i, column++, buf);
        }
    if (!silent)
        {
        TablePrint(tabPtr);
        TableFree(tabPtr);
        }
    /*---------------------------------------------------------*/
    return success;
    }

eBool CliAtAttControllerForcePointerAdjMode(char argc, char **argv,
                                      uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
									  uint32 (*MaskFromString)(char *pPointerAdjTypeStr))
    {
    return ForcePointerAdjMode(argc, argv,
                          AtAttControllerForcePointerAdjModeSet,
                          ChannelListFromString,
						  MaskFromString);
    }

eBool CliAtAttControllerForcePointerAdj(char argc, char **argv,
                                   uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                   uint32 (*MaskFromString)(char *pPointerAdjTypeStr))
    {
    return ErrorForce(argc, argv, ChannelListFromString, MaskFromString, AtAttControllerForcePointerAdj, AtAttControllerUnForcePointerAdj);
    }

eBool CliAtAttControllerForcePointerAdjNumEventSet(char argc, char **argv,
                                      uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
									  uint32 (*MaskFromString)(char *pPointerAdjTypeStr))
    {
	uint32 errorType = MaskFromString(argv[1]);
    return ForceErrorDurationStep(argc, argv,
                              AtAttControllerForcePointerAdjNumEventSet,
                              ChannelListFromString,
							  errorType);
    }

eBool CliAtAttControllerForcePointerAdjDurationSet(char argc, char **argv,
                                      uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
									  uint32 (*MaskFromString)(char *pPointerAdjTypeStr))
    {
	uint32 errorType = MaskFromString(argv[1]);
    return ForceErrorDurationStep(argc, argv,
                              AtAttControllerForcePointerAdjDurationSet,
                              ChannelListFromString,
							  errorType);
    }

eBool CliAtAttControllerForcePointerAdjBitPositionSet(char argc, char **argv,
                                            uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                            uint32 (*MaskFromString)(char *pPointerAdjTypeStr))
    {
    uint32 numChannels, i;
    AtChannel *channelList;
    uint32 errorTypeMask;
    uint32 *bitList;
    uint32 numBits;
    uint32 bitPosition;
    eAtRet ret = cAtOk;
    uint32 errorType = MaskFromString(argv[1]);
    AtUnused(argc);

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = ChannelListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid channel list, ...\n");
        return cAtFalse;
        }

    errorTypeMask = errorType;
    bitList = CliSharedIdBufferGet(&numBits);
    numBits  = CliIdListFromString(argv[2], bitList, numBits);
    if (numBits >16)
        return cAtFalse;

    bitPosition = 0;
    for (i = 0; i < numBits; i++)
        bitPosition |= cBit0 << bitList[i];

    for (i = 0; i < numChannels; i++)
        {
        ret |= AtAttControllerForcePointerAdjBitPositionSet(AttController(channelList[i]), errorTypeMask, bitPosition);
        }

    return (ret == cAtOk) ? cAtTrue : cAtFalse;
    }

eBool CliAtAttControllerForcePointerAdjDataMaskSet(char argc, char **argv,
                                              uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                              uint32 (*MaskFromString)(char *pErrorTypeStr))
    {
    return ErrorForceIntegerAttributeSet(argc, argv, ChannelListFromString, MaskFromString, AtAttControllerForcePointerAdjDataMaskSet);
    }

eBool CliAtAttControllerForcePointerAdjStepSet(char argc, char **argv,
                                              uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                              uint32 (*MaskFromString)(char *pErrorTypeStr))
    {
    return ErrorForceIntegerAttributeSet(argc, argv, ChannelListFromString, MaskFromString, AtAttControllerForcePointerAdjStepSet);
    }


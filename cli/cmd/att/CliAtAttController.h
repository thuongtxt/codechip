/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : CliAtAttController.h
 * 
 * Created Date: May 17, 2016
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLIATATTCONTROLLER_H_
#define _CLIATATTCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCli.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 CliAttPdhDe3ErrorTypeMaskFromString(char *pErrorTypeStr);

eBool CliAtAttControllerForceAlarmStepSet(char argc, char **argv,
                                       uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                       uint32 alarmType);
eBool CliAtAttControllerForceAlarmNumEventSet(char argc, char **argv,
                                      uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                      uint32 alarmType);
eBool CliAtAttControllerForceAlarmNumFrameSet(char argc, char **argv,
                                      uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                      uint32 alarmType);
eBool CliAtAttControllerForceMode(char argc, char **argv,
                                       uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                       eBool isError,
                                       uint32 alarmType);
eBool CliAtAttControllerForceAlarmDurationSet(char argc, char **argv,
                                       uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                       uint32 alarmType);
eBool CliAtAttControllerRxAlarmCapture(char argc, char **argv,
                                       uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                       /*uint32 alarmType,*/
                                       eBool captureEnabled);
eBool CliAtAttControllerRxAlarmCaptureDuration(char argc, char **argv,
                                      uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                      uint32 alarmType);
eBool CliAtAttControllerRxAlarmCaptureMode(char argc, char **argv,
                                      uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                      uint32 alarmType);
eBool CliAtAttControllerRxAlarmCaptureNumEvent(char argc, char **argv,
                                      uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                      uint32 alarmType);
eBool CliAtAttControllerForceErrorDebug(char argc, char **argv,
                                        uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                        uint32 errorType);
eBool CliAtAttControllerForceErrorDuration(char argc, char **argv,
                                       uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                       uint32 errorType);
eBool CliAtAttControllerForceErrorStepSet(char argc, char **argv,
                                       uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                       uint32 errorType);
eBool CliAtAttControllerForceErrorStep2Set(char argc, char **argv,
                                      uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                      uint32 errorType);
eBool CliAtAttControllerForceErrorRate(char argc, char **argv,
                                       uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                       uint32 errorType);
eBool CliAtAttControllerForceErrorPerSecond(char argc, char **argv,
                                            uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                            uint32 (*MaskFromString)(char *pErrorTypeStr));
eBool CliAtAttControllerForceError(char argc, char **argv,
                                   uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                   uint32 (*MaskFromString)(char *pErrorTypeStr));
eBool CliAtAttControllerForceErrorContinuous(char argc, char **argv,
                                             uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                             uint32 (*MaskFromString)(char *pErrorTypeStr));
eBool CliAtAttControllerForceErrorNumErrorsSet(char argc, char **argv,
                                               uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                               uint32 (*MaskFromString)(char *pErrorTypeStr));
eBool CliAtAttControllerForceBitPositionSet(char argc, char **argv,
                                            uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                            eBool isError,
                                            uint32 errorType);
eBool CliAtAttControllerForceBitPosition2Set(char argc, char **argv,
                                            uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                            eBool isError,
                                            uint32 errorType);
eBool CliAtAttControllerForceErrorDataMaskSet(char argc, char **argv,
                                              uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                              uint32 (*MaskFromString)(char *pErrorTypeStr));
eBool CliAtAttControllerForceErrorCrcMaskSet(char argc, char **argv,
                                              uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                              uint32 (*MaskFromString)(char *pErrorTypeStr));
eBool CliAtAttControllerForceErrorCrcMask2Set(char argc, char **argv,
                                              uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                              uint32 (*MaskFromString)(char *pErrorTypeStr));
eBool CliAtAttControllerForceErrorStatusShow(char argc, char **argv, uint32 errorType,
                                             uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize));
eBool CliAtAttControllerForceErrorShow(char argc, char **argv, uint32 errorType,
                                       uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize));
eBool CliAtAttControllerForceAlarmPerSecond(char argc, char **argv,
                                            uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                            uint32 (*MaskFromString)(char *pAlarmTypeStr));
eBool CliAtAttControllerForceAlarm(char argc, char **argv,
                                   uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                   uint32 (*MaskFromString)(char *pAlarmTypeStr));
eBool CliAtAttControllerForceAlarmContinuous(char argc, char **argv,
                                             uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                             uint32 (*MaskFromString)(char *pAlarmTypeStr));
eBool CliAtAttControllerForceAlarmStatusShow(char argc, char **argv, uint32 alarmType,
                                             uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize));
eBool CliAtAttControllerForceAlarmShow(char argc, char **argv, uint32 alarmType,
                                       uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize));
eBool CliAtAttControllerRxAlarmCaptureShow(char argc, char **argv,
                                           uint32 alarmType,
                                           uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize));
eBool CliAtAttControllerFrequencyOffsetSet(char argc, char **argv,
                        uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize));
eBool CliAtAttControllerFrequencyOffsetShow(char argc, char **argv,
                                             uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize));
eBool CliAtAttControllerBipCountShow(char argc, char **argv,
                                     uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize));
eBool CliAtAttControllerWanderFileNameSet(char argc, char **argv,
                        uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize));
eBool CliAtAttControllerWanderStart(char argc, char **argv,
                        uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize));
eBool CliAtAttControllerWanderStop(char argc, char **argv,
                        uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize));

eBool CliAtAttControllerForcePointerAdjMode(char argc, char **argv,
                                            uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
									        uint32 (*MaskFromString)(char *pPointerAdjTypeStr));
eBool CliAtAttControllerForcePointerAdj(char argc, char **argv,
                                        uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                        uint32 (*MaskFromString)(char *pPointerAdjTypeStr));
eBool CliAtAttControllerForcePointerAdjNumEventSet(char argc, char **argv,
                                              uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
									          uint32 (*MaskFromString)(char *pPointerAdjTypeStr));
eBool CliAtAttControllerForcePointerAdjDurationSet(char argc, char **argv,
                                              uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
									          uint32 (*MaskFromString)(char *pPointerAdjTypeStr));
eBool CliAtAttControllerForcePointerAdjBitPositionSet(char argc, char **argv,
                                            uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                            uint32 (*MaskFromString)(char *pPointerAdjTypeStr));
eBool CliAtAttControllerForcePointerAdjDataMaskSet(char argc, char **argv,
                                            uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                            uint32 (*MaskFromString)(char *pPointerAdjTypeStr));
eBool CliAtAttControllerForcePointerAdjStepSet(char argc, char **argv,
                                            uint32 (*ChannelListFromString)(char *pStrIdList, AtChannel *channels, uint32 bufferSize),
                                            uint32 (*MaskFromString)(char *pPointerAdjTypeStr));

#ifdef __cplusplus
}
#endif
#endif /* _CLIATATTCONTROLLER_H_ */


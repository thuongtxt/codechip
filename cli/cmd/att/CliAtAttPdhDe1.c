/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ATT
 *
 * File        : CliAtAttPdhDe1.c
 *
 * Created Date: Dec 24, 2015
 *
 * Description : DS1/E1 CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtAttController.h"
#include "CliAtAttController.h"
#include "AtPdhNxDs0.h"
/*--------------------------- Define -----------------------------------------*/
#define cMaxNumDe1bitMask 32
#define sIdTypeBlankChar "."

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

static const char *cAttDe1FbitTypeStr[] = {
                                           "fas",
                                           "nfas",
                                           "crcmfas",
										   "mfas"
                                            };

static const uint32 cAttDe1FbitTypeVal[] = {cAtAttPdhDe1FbitTypeFas,
                                            cAtAttPdhDe1FbitTypeNfas,
											cAtAttPdhDe1FbitTypeCrcMfas,
                                            cAtAttPdhDe1FbitTypeMfas};

static const char *cAtAttPdhDe1ErrorTypeStr[] =
    {
    "fbit",
    "crc"
    };

static const uint32 cAtAttPdhDe1ErrorTypeVal[] =
    {
    cAtPdhDe1ErrorFe,
    cAtPdhDe1ErrorCrc
    };

static const char *cAtAttPdhDe1AlarmTypeStr[] =
    {
    "los",
    "ais",
    "rai",
    "sigrai",
    "lof",
    "siglof"
    };

static const uint32 cAtAttPdhDe1AlarmTypeVal[] =
    {
     cAtPdhDe1AlarmLos,
     cAtPdhDe1AlarmAis,
     cAtPdhDe1AlarmRai,
     cAtPdhDe1AlarmSigRai,
     cAtPdhDe1AlarmLof,
     cAtPdhDe1AlarmSigLof
    };

static const char *cAttSignalingModeStr[] = {
                                           "prbs",
                                           "fixed_ab",
                                           "fixed_abcd",
                                           "sequence"
                                            };

static const eAtAttSignalingMode cAttSignalingModeVal[] = {cAtAttSignalingModePrbs,
                                              cAtAttSignalingModeFixedAB,
                                              cAtAttSignalingModeFixedABCD,
                                              cAtAttSignalingModeSequence};

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 CliAttPdhDe1ErrorTypeMaskFromString(char *pErrorTypeStr)
    {
    return CliMaskFromString(pErrorTypeStr, cAtAttPdhDe1ErrorTypeStr, cAtAttPdhDe1ErrorTypeVal, mCount(cAtAttPdhDe1ErrorTypeVal));
    }

static uint32 CliAttPdhDe1AlarmTypeMaskFromString(char *pAlarmTypeStr)
    {
    return CliMaskFromString(pAlarmTypeStr, cAtAttPdhDe1AlarmTypeStr, cAtAttPdhDe1AlarmTypeVal, mCount(cAtAttPdhDe1AlarmTypeVal));
    }

eBool CmdAtAttPdhDe1ForceErrorMode(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttPdhDe1ErrorTypeStr, cAtAttPdhDe1ErrorTypeVal, mCount(cAtAttPdhDe1ErrorTypeVal), NULL);
    return CliAtAttControllerForceMode(argc, argv, De1ListFromString, cAtTrue, errorType);
    }

eBool CmdAtAttPdhDe1ForceErrorRate(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttPdhDe1ErrorTypeStr, cAtAttPdhDe1ErrorTypeVal, mCount(cAtAttPdhDe1ErrorTypeVal), NULL);
    return CliAtAttControllerForceErrorRate(argc, argv, De1ListFromString, errorType);
    }

eBool CmdAtAttPdhDe1ForceErrorDuration(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttPdhDe1ErrorTypeStr, cAtAttPdhDe1ErrorTypeVal, mCount(cAtAttPdhDe1ErrorTypeVal), NULL);
    return CliAtAttControllerForceErrorDuration(argc, argv, De1ListFromString, errorType);
    }

eBool CmdAtAttPdhDe1ForceErrorPerSecond(char argc, char **argv)
    {
    return CliAtAttControllerForceErrorPerSecond(argc, argv, De1ListFromString, CliAttPdhDe1ErrorTypeMaskFromString);
    }

eBool CmdAtAttPdhDe1ForceError(char argc, char **argv)
    {
    return CliAtAttControllerForceError(argc, argv, De1ListFromString, CliAttPdhDe1ErrorTypeMaskFromString);
    }


eBool CmdAtAttPdhDe1ForceErrorNumErrorsSet(char argc, char **argv)
    {
    return CliAtAttControllerForceErrorNumErrorsSet(argc, argv, De1ListFromString, CliAttPdhDe1ErrorTypeMaskFromString);
    }

eBool CmdAtAttPdhDe1ForceErrorBitPostionSet(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttPdhDe1ErrorTypeStr, cAtAttPdhDe1ErrorTypeVal, mCount(cAtAttPdhDe1ErrorTypeVal), NULL);
    return CliAtAttControllerForceBitPositionSet(argc, argv, De1ListFromString, cAtTrue, errorType);
    }
eBool CmdAtAttPdhDe1ForceErrorBitPostion2Set(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttPdhDe1ErrorTypeStr, cAtAttPdhDe1ErrorTypeVal, mCount(cAtAttPdhDe1ErrorTypeVal), NULL);
    return CliAtAttControllerForceBitPosition2Set(argc, argv, De1ListFromString, cAtTrue, errorType);
    }

eBool CmdAtAttPdhDe1ForceErrorDataMaskSet(char argc, char **argv)
    {
    return CliAtAttControllerForceErrorDataMaskSet(argc, argv, De1ListFromString, CliAttPdhDe1ErrorTypeMaskFromString);
    }

eBool CmdAtAttPdhDe1ForceErrorCrcMaskSet(char argc, char **argv)
    {
    return CliAtAttControllerForceErrorCrcMaskSet(argc, argv, De1ListFromString, CliAttPdhDe1ErrorTypeMaskFromString);
    }

eBool CmdAtAttPdhDe1ForceErrorCrcMask2Set(char argc, char **argv)
    {
    return CliAtAttControllerForceErrorCrcMask2Set(argc, argv, De1ListFromString, CliAttPdhDe1ErrorTypeMaskFromString);
    }

eBool CmdAtAttPdhDe1ForceErrorStepSet(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttPdhDe1ErrorTypeStr, cAtAttPdhDe1ErrorTypeVal, mCount(cAtAttPdhDe1ErrorTypeVal), NULL);
    return CliAtAttControllerForceErrorStepSet(argc, argv, De1ListFromString, errorType);
    }

eBool CmdAtAttPdhDe1ForceErrorStep2Set(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttPdhDe1ErrorTypeStr, cAtAttPdhDe1ErrorTypeVal, mCount(cAtAttPdhDe1ErrorTypeVal), NULL);
    return CliAtAttControllerForceErrorStep2Set(argc, argv, De1ListFromString, errorType);
    }

eBool CmdAtAttPdhDe1ForceErrorStatusShow(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttPdhDe1ErrorTypeStr, cAtAttPdhDe1ErrorTypeVal, mCount(cAtAttPdhDe1ErrorTypeVal), NULL);
    return CliAtAttControllerForceErrorStatusShow(argc, argv, errorType, De1ListFromString);
    }

eBool CmdAtAttPdhDe1ForceErrorShow(char argc, char **argv)
    {
    uint32 errorType = CliStringToEnum(argv[1], cAtAttPdhDe1ErrorTypeStr, cAtAttPdhDe1ErrorTypeVal, mCount(cAtAttPdhDe1ErrorTypeVal), NULL);
    return CliAtAttControllerForceErrorShow(argc, argv, errorType, De1ListFromString);
    }

eBool CmdAtAttPdhDe1ForceAlarmNumEventPerSecond(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttPdhDe1AlarmTypeStr, cAtAttPdhDe1AlarmTypeVal, mCount(cAtAttPdhDe1AlarmTypeVal), NULL);
    return CliAtAttControllerForceAlarmNumEventSet(argc, argv, De1ListFromString, alarmType);
    return cAtTrue;
    }

eBool CmdAtAttPdhDe1ForceAlarmModeSet(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttPdhDe1AlarmTypeStr, cAtAttPdhDe1AlarmTypeVal, mCount(cAtAttPdhDe1AlarmTypeVal), NULL);
    return CliAtAttControllerForceMode(argc, argv, De1ListFromString, cAtFalse, alarmType);
    }

eBool CmdAtAttPdhDe1ForceAlarmDuration(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttPdhDe1AlarmTypeStr, cAtAttPdhDe1AlarmTypeVal, mCount(cAtAttPdhDe1AlarmTypeVal), NULL);
    return CliAtAttControllerForceAlarmDurationSet(argc, argv, De1ListFromString, alarmType);
    }

eBool CmdAtAttPdhDe1ForceAlarmNumFrame(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttPdhDe1AlarmTypeStr, cAtAttPdhDe1AlarmTypeVal, mCount(cAtAttPdhDe1AlarmTypeVal), NULL);
    return CliAtAttControllerForceAlarmNumFrameSet(argc, argv, De1ListFromString, alarmType);
    }

eBool CmdAtAttPdhDe1RxAlarmCaptureEnable(char argc, char **argv)
    {
    return CliAtAttControllerRxAlarmCapture(argc, argv, De1ListFromString, cAtTrue);
    }

eBool CmdAtAttPdhDe1RxAlarmCaptureDisable(char argc, char **argv)
    {
    return CliAtAttControllerRxAlarmCapture(argc, argv, De1ListFromString, cAtFalse);
    }

eBool CmdAtAttPdhDe1RxAlarmCaptureDuration(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttPdhDe1AlarmTypeStr, cAtAttPdhDe1AlarmTypeVal, mCount(cAtAttPdhDe1AlarmTypeVal), NULL);
    return CliAtAttControllerRxAlarmCaptureDuration(argc, argv, De1ListFromString, alarmType);
    }

eBool CmdAtAttPdhDe1RxAlarmCaptureMode(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttPdhDe1AlarmTypeStr, cAtAttPdhDe1AlarmTypeVal, mCount(cAtAttPdhDe1AlarmTypeVal), NULL);
    return CliAtAttControllerRxAlarmCaptureMode(argc, argv, De1ListFromString, alarmType);
    }

eBool CmdAtAttPdhDe1RxAlarmCaptureNumEvent(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttPdhDe1AlarmTypeStr, cAtAttPdhDe1AlarmTypeVal, mCount(cAtAttPdhDe1AlarmTypeVal), NULL);
    return CliAtAttControllerRxAlarmCaptureNumEvent(argc, argv, De1ListFromString, alarmType);
    }

eBool CmdAtAttPdhDe1ForceAlarmStepSet(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttPdhDe1AlarmTypeStr, cAtAttPdhDe1AlarmTypeVal, mCount(cAtAttPdhDe1AlarmTypeVal), NULL);
    return CliAtAttControllerForceAlarmStepSet(argc, argv, De1ListFromString, alarmType);
    }

eBool CmdAtAttPdhDe1ForceAlarm(char argc, char **argv)
    {
    return CliAtAttControllerForceAlarm(argc, argv, De1ListFromString, CliAttPdhDe1AlarmTypeMaskFromString);
    }

eBool CmdAtAttPdhDe1FrequencyOffsetSet(char argc, char **argv)
    {
    return CliAtAttControllerFrequencyOffsetSet(argc, argv, De1ListFromString);
    }

eBool CmdAtAttPdhDe1WanderFileName(char argc, char **argv)
    {
    return CliAtAttControllerWanderFileNameSet(argc, argv, De1ListFromString);
    }

eBool CmdAtAttPdhDe1WanderStart(char argc, char **argv)
    {
    return CliAtAttControllerWanderStart(argc, argv, De1ListFromString);
    }

eBool CmdAtAttPdhDe1WanderStop(char argc, char **argv)
    {
    return CliAtAttControllerWanderStop(argc, argv, De1ListFromString);
    }

eBool CmdAtAttPdhDe1ForceAlarmStatusShow(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttPdhDe1AlarmTypeStr, cAtAttPdhDe1AlarmTypeVal, mCount(cAtAttPdhDe1AlarmTypeVal), NULL);
    return CliAtAttControllerForceAlarmStatusShow(argc, argv, alarmType, De1ListFromString);
    }

eBool CmdAtAttPdhDe1ForceAlarmShow(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttPdhDe1AlarmTypeStr, cAtAttPdhDe1AlarmTypeVal, mCount(cAtAttPdhDe1AlarmTypeVal), NULL);
    return CliAtAttControllerForceAlarmShow(argc, argv, alarmType, De1ListFromString);
    }

eBool CmdAtAttPdhDe1RxAlarmCaptureShow(char argc, char **argv)
    {
    uint32 alarmType = CliStringToEnum(argv[1], cAtAttPdhDe1AlarmTypeStr, cAtAttPdhDe1AlarmTypeVal, mCount(cAtAttPdhDe1AlarmTypeVal), NULL);
    return CliAtAttControllerRxAlarmCaptureShow(argc, argv, alarmType, De1ListFromString);
    }

static AtAttController AttController(AtChannel channel)
    {
    return (AtAttController)AtChannelAttController(channel);
    }

eBool CmdAtAttPdhDe1ForceFbitTypeSet(char argc, char **argv)
    {
    eAtRet    ret = cAtOk;
    uint32    numChannels=0, i=0, type=0;
    AtChannel *channelList=NULL;
    eBool     result = cAtFalse;
    eBool     en;

    if (argc!=3)
    	{
    	AtPrintc(cSevCritical, "ERROR: Not enough parameters, ...\n");
        return cAtFalse;
    	}

    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(argv[0], channelList, numChannels);
    if (!numChannels)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid channel list, ...\n");
        return cAtFalse;
        }
    mAtStrToEnum(cAttDe1FbitTypeStr, cAttDe1FbitTypeVal,argv[1], type, result);
    if (!result)
        return cAtFalse;

    mAtStrToBool(argv[2], en, result);
    if (!result)
		return cAtFalse;

    for (i = 0; i < numChannels; i++)
        {
        ret = AtAttControllerDe1ForceFbitTypeSet(AttController(channelList[i]), type, en);
        if (ret != cAtOk)
            return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAtAttPdhDe1FrequencyOffsetShow(char argc, char **argv)
    {
    return CliAtAttControllerFrequencyOffsetShow(argc, argv, De1ListFromString);
    }

static char* NxDs0De1IdAndDs0IdListGet_(const char *pStrIdList, char **pIdDe1, char **pIdDs0List)
    {
    char *pDupStrIdList = NULL;
    char* beginDs0List;

    if (pStrIdList == NULL)
        return NULL;

    /* Get from last "." */
    beginDs0List = AtStrrchr(pStrIdList, '.');
    if ((beginDs0List == NULL) || (AtStrlen(beginDs0List) < 2))
        return NULL;

    /* Ignore "." */
    beginDs0List++;

    /* Get ID type and ID list */
    pDupStrIdList = AtOsalMemAlloc(AtStrlen(pStrIdList) - AtStrlen(beginDs0List));
    if (pDupStrIdList == NULL)
        return NULL;

    AtStrncpy(pDupStrIdList, pStrIdList, (AtStrlen(pStrIdList) - AtStrlen(beginDs0List)) - 1);
    pDupStrIdList[(AtStrlen(pStrIdList) - AtStrlen(beginDs0List)) - 1] = '\0';

    if (pIdDs0List)
        *pIdDs0List = (char*)beginDs0List;

    if (pIdDe1)
        *pIdDe1 = pDupStrIdList;

    return pDupStrIdList;
    }

static uint32 NxDs0ListFromString_(char *pStrIdList, AtChannel *channels, uint32 *tsList, uint32 bufferSize)
    {
    AtChannel* pdhDe1;
    AtIdParser idParser;
    uint32 numDe1s, nxds0Index = 0;
    char *nxds0String, *nxds0De1String, *de1IdString = NULL, *ds0ListString = NULL, *idString, *idType;

    nxds0String = IdTypeAndIdListGet(pStrIdList, &idType, &idString);
    pdhDe1 = AtOsalMemAlloc(sizeof(AtChannel) * bufferSize);
    if (pdhDe1 == NULL)
        return 0;

    nxds0De1String = NxDs0De1IdAndDs0IdListGet_(idString, &de1IdString, &ds0ListString);
    if ((numDe1s = De1ListFromString(de1IdString, pdhDe1, bufferSize)) == 0)
        {
        AtOsalMemFree(nxds0De1String);
        AtOsalMemFree(nxds0String);
        AtOsalMemFree(pdhDe1);
        return 0;
        }

    if ((ds0ListString == NULL) || (AtStrcmp(idType, sAtChannelTypePdhNxDs0) != 0))
        {
        AtOsalMemFree(nxds0De1String);
        AtOsalMemFree(nxds0String);
        AtOsalMemFree(pdhDe1);
        return 0;
        }

    AtOsalMemCpy(channels, pdhDe1, sizeof(pdhDe1));
    idParser = AtIdParserNew(ds0ListString, NULL, NULL);
    while (AtIdParserHasNextNumber(idParser))
        {
        uint32 slotId = AtIdParserNextNumber(idParser);

        if (slotId > 32)
            {
            AtPrintc(cSevWarning, "WARNING: timeslot %u is out of range (0..31), it is ignored\r\n", slotId);
            continue;
            }

        tsList[nxds0Index++] = slotId;
        }

    /* Clean up */
    AtObjectDelete((AtObject)idParser);
    AtOsalMemFree(nxds0De1String);
    AtOsalMemFree(nxds0String);
    AtOsalMemFree(pdhDe1);

    return nxds0Index;
    }

static uint32 De1Nxds0ListGet_(char *pStrIdList, AtChannel * channelList, uint32 * tsList, uint32 bufferSize)
    {
    uint32    numChannel = 0;

    if (!AtStrcmp(pStrIdList, "none"))
        channelList = NULL;
    else
        {
        numChannel = NxDs0ListFromString_(pStrIdList, channelList, tsList, bufferSize);
        }

    if (!numChannel)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid channel list, ...\n");
        return cAtFalse;
        }

    return numChannel;
    }

static eBool CliAtAttPdhDe1SignalingPrbsSet(char argc, char **argv, eBool isTx, eBool isRx)
    {
    eBool               result;
    uint32              * tsList;
    uint32              i, tsId, sigPattern = 0, numchannel, threshold = 0;
    AtChannel           *channelList=NULL;
    eAtAttSignalingMode sigMode;
    uint32    bufferSize;

    tsList = CliSharedIdBufferGet(&bufferSize);
    channelList = CliSharedChannelListGet(&bufferSize);

    if (argc > 2)
        StrToDw(argv[2], 'd', &threshold);

    if (argc > 3)
        StrToDw(argv[3], 'h', &sigPattern);

    /* Signaling prbs mode get */
    mAtStrToEnum(cAttSignalingModeStr, cAttSignalingModeVal, argv[1], sigMode, result);
    if(!result)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid Prbs Mode parameter. Expected: en/dis\r\n");
        return cAtFalse;
        }

    /* Signaling prbs mode set */
    numchannel = De1Nxds0ListGet_(argv[0], channelList, tsList, bufferSize);
    for (i = 0; i < mCount(channelList); i++)
        {
        AtAttController controller = AttController(channelList[i]);
        for (tsId = 0; tsId < numchannel; tsId ++)
            {
            if (isTx) AtAttControllerTxSignalingPatternSet(controller, (uint8)tsList[tsId], sigMode, (uint8)threshold, (uint8)sigPattern);
            if (isRx) AtAttControllerRxExpectedSignalingPatternSet(controller, (uint8)tsList[tsId], sigMode, (uint8)threshold, (uint8)sigPattern);
            }
        }

    return cAtTrue;
    }

eBool CmdAtAttPdhDe1SignalingPrbsSet(char argc, char **argv)
    {
    return CliAtAttPdhDe1SignalingPrbsSet(argc, argv, cAtTrue, cAtTrue);
    }

eBool CmdAtAttPdhDe1TxSignalingPrbsSet(char argc, char **argv)
    {
    return CliAtAttPdhDe1SignalingPrbsSet(argc, argv, cAtTrue, cAtFalse);
    }

eBool CmdAtAttPdhDe1RxSignalingPrbsSet(char argc, char **argv)
    {
    return CliAtAttPdhDe1SignalingPrbsSet(argc, argv, cAtFalse, cAtTrue);
    }

static eBool CliAtAttPdhDe1SignalingEnable(char argc, char **argv, eBool enable, eBool isTx, eBool isRx)
    {
    uint32    * tsList;
    AtChannel *channelList=NULL;
    uint32    numChannel = 0, i, tsId;
    uint32    bufferSize;
    AtUnused(argc);

    tsList = CliSharedIdBufferGet(&bufferSize);
    channelList = CliSharedChannelListGet(&bufferSize);

    numChannel = De1Nxds0ListGet_(argv[0], channelList, tsList, bufferSize);
    for (i = 0; i < mCount(channelList); i++)
        {
        AtAttController controller = AttController(channelList[i]);
        for (tsId = 0; tsId < numChannel; tsId ++)
            {
            if (isTx)AtAttControllerTxSignalingEnable(controller, (uint8)tsList[tsId], enable);
            if (isRx)AtAttControllerRxSignalingEnable(controller, (uint8)tsList[tsId], enable);
            }
        }

    return cAtTrue;
    }

eBool CmdAtAttPdhDe1SignalingEnable(char argc, char **argv)
    {
    return CliAtAttPdhDe1SignalingEnable(argc, argv, cAtTrue, cAtTrue, cAtTrue);
    }

eBool CmdAtAttPdhDe1SignalingDisable(char argc, char **argv)
    {
    return CliAtAttPdhDe1SignalingEnable(argc, argv, cAtFalse, cAtTrue, cAtTrue);
    }

eBool CmdAtAttPdhDe1TxSignalingEnable(char argc, char **argv)
    {
    return CliAtAttPdhDe1SignalingEnable(argc, argv, cAtTrue, cAtTrue, cAtFalse);
    }

eBool CmdAtAttPdhDe1TxSignalingDisable(char argc, char **argv)
    {
    return CliAtAttPdhDe1SignalingEnable(argc, argv, cAtFalse, cAtTrue, cAtFalse);
    }
eBool CmdAtAttPdhDe1RxSignalingEnable(char argc, char **argv)
    {
    return CliAtAttPdhDe1SignalingEnable(argc, argv, cAtTrue, cAtFalse, cAtTrue);
    }

eBool CmdAtAttPdhDe1RxSignalingDisable(char argc, char **argv)
    {
    return CliAtAttPdhDe1SignalingEnable(argc, argv, cAtFalse, cAtFalse, cAtTrue);
    }

static const char* ToBinary(uint8 value, AtPdhDe1 de1)
    {
    uint16 frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)de1);
    static const char* ret[16]= {"0000", "0001", "0010", "0011","0100", "0101", "0110", "0111",
                               "1000", "1001", "1010", "1011","1100", "1101", "1110", "1111"};
    static const char* ret_ds1sf[4]= {"00xx", "01xx", "10xx", "11xx"};
    if (frameType==cAtPdhDs1FrmSf)
    	{
        if (value<4)
            return ret_ds1sf[value];
        }
    else
    	{
        if (value<16)
            return ret[value];
    	}
    return "xxxx";
    }
static eBool CliAtAttPdhDe1SignalingDump(char argc, char **argv, eBool isTx, eBool isRx, uint32 numValue)
    {
    uint32    * tsList;
    AtChannel *channelList=NULL;
    uint32    numChannel = 0, i, tsId;
    uint32    bufferSize;
    uint32    row_i = 0;
    uint8     value[32];
    tTab      *tabPtr;
    const char *pHeading[]       = {"NxDS0", "01","02","03","04","05","06","07","08", "09", "10", "11","12","13","14","15","16","17","18", "19", "20", "21","22","23","24","25","26","27","28", "29", "30","31","32"};
    const char *pHeading_short[] = {"NxDS0", "ABCD"};
    static char nxds0[32];

    AtUnused(argc);
    tsList = CliSharedIdBufferGet(&bufferSize);
    channelList = CliSharedChannelListGet(&bufferSize);

    numChannel = De1Nxds0ListGet_(argv[0], channelList, tsList, bufferSize);
    if (numValue==32)
	    tabPtr = TableAlloc(0, mCount(pHeading), pHeading);
	else	    
		tabPtr = TableAlloc(0, mCount(pHeading_short), pHeading_short);

    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Fail to create table\r\n");
        return cAtFalse;
        }
    for (i = 0; i < mCount(channelList); i++)
        {
        AtPdhDe1 de1 = (AtPdhDe1)channelList[i];
        AtAttController controller = AttController(channelList[i]);        

        for (tsId = 0; tsId < numChannel; tsId ++)
            {
            char buf[32];
            uint8 part_i = 0;
            if (numValue==1)
                {
                if (isTx)AtAttControllerTxSignalingDump(controller, (uint8)tsList[tsId], value, 31);
                if (isRx)AtAttControllerRxSignalingDump(controller, (uint8)tsList[tsId], value, 31);
                }
            else
                {
                if (isTx)AtAttControllerTxSignalingDumpDebug(controller, (uint8)tsList[tsId], value, 32);
                if (isRx)AtAttControllerRxSignalingDumpDebug(controller, (uint8)tsList[tsId], value, 32);
                }

            /*---------------------------------------------------------*/
            TableAddRow(tabPtr, 1);
            AtOsalMemInit(buf, 0, sizeof(buf));
            AtSprintf(buf, "%s", CliChannelIdStringGet((AtChannel)channelList[i]));
            AtStrncat(buf, ".", sizeof(buf));
            AtSprintf(nxds0, "%d", tsList[tsId]);
            AtStrncat(buf, nxds0, sizeof(buf));
            StrToCell(tabPtr, row_i, 0, buf);
            /*---------------------------------------------------------*/
            for (part_i=0; part_i<1; part_i++)/* Each print 3 rows */
                {
                uint8 colIdx = 0;
                for (colIdx=0; colIdx<numValue; colIdx++) /* Each row 10 values*/
                    {
                    uint8 value_index = (uint8)(colIdx+part_i*10);
                    if (value_index<numValue)
                       	{
                        AtOsalMemInit(buf, 0, sizeof(buf));
                        AtSprintf(buf, "%s", ToBinary(value[value_index], (AtPdhDe1)de1));
                        ColorStrToCell(tabPtr, row_i, (uint32)(colIdx)+1, buf, cSevInfo);
                        }
                    else
                        {
                        AtSprintf(buf, "%s", "");
                        ColorStrToCell(tabPtr, row_i, (uint32)(colIdx)+1, buf, cSevNormal);
                        }
                    }
                row_i ++;
                }
            }
        }
    TablePrint(tabPtr);
    TableFree(tabPtr);
    return cAtTrue;
    }

eBool CmdAtAttPdhDe1TxSignalingDump(char argc, char **argv)
    {
    return CliAtAttPdhDe1SignalingDump(argc, argv, cAtTrue, cAtFalse, 1);
    }

eBool CmdAtAttPdhDe1RxSignalingDump(char argc, char **argv)
    {
    return CliAtAttPdhDe1SignalingDump(argc, argv, cAtFalse, cAtTrue, 1);
    }

eBool CmdAtAttPdhDe1TxSignalingDumpDebug(char argc, char **argv)
    {
    return CliAtAttPdhDe1SignalingDump(argc, argv, cAtTrue, cAtFalse, 32);
    }

eBool CmdAtAttPdhDe1RxSignalingDumpDebug(char argc, char **argv)
    {
    return CliAtAttPdhDe1SignalingDump(argc, argv, cAtFalse, cAtTrue, 32);
    }

static const char *CliSignalingPrbsModeString(eAtAttSignalingMode prbsMode)
    {
    static char prbsModeString[16];
    eBool convertSuccess;

    mAtEnumToStr(cAttSignalingModeStr, cAttSignalingModeVal, prbsMode, prbsModeString, convertSuccess);
    if (!convertSuccess)
        return NULL;

    return prbsModeString;
    }

eBool CmdAtAttPdhDe1SignalingShow(char argc, char **argv)
    {
    uint8     colIdx;
    eBool     enable;
    tTab      *tabPtr;
    uint8     sigPattern = 0, tsId, threshold=0;
    uint32    numchannel = 0;
    uint32    row_i, i;
    uint32    * tsList;
    eAtAttSignalingMode patternMode;
    AtChannel *channelList=NULL;
    eBool silent = cAtFalse;
    eAtHistoryReadingMode readingMode;

    uint32    bufferSize;
    static char buf[32];
    static char nxds0[32];
    const char *patternModeString;
    const char *pHeading[] = {"NxDS0", "TxEnable", "TxSigMode","TxPattern","TxThreshold","RxEnable","RxSigMode","RxPattern","RxThreshold", "alarm", "sticky"};
    AtUnused(argc);


    tsList = CliSharedIdBufferGet(&bufferSize);
    channelList = CliSharedChannelListGet(&bufferSize);

    numchannel = De1Nxds0ListGet_(argv[0], channelList, tsList, bufferSize);
    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }
    silent = CliHistoryReadingShouldSilent(readingMode, (argc > 2) ? argv[2] : NULL);

    if (!silent)
        {
        tabPtr = TableAlloc(numchannel, mCount(pHeading), pHeading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "Fail to create table\r\n");
            return cAtFalse;
            }
        }
    row_i = 0;
    for (i = 0; i < mCount(channelList); i++)
        {
        AtAttController controller = AttController(channelList[i]);
        for (tsId = 0; tsId < numchannel; tsId ++)
            {
            eBool     alarm;
            uint32    sticky;

            /*---------------------------------------------------------*/
            colIdx = 0;
            AtOsalMemInit(buf, 0, sizeof(buf));
            AtSprintf(buf, "%s", CliChannelIdStringGet((AtChannel)channelList[i]));
            AtStrncat(buf, ".", sizeof(buf));
            AtSprintf(nxds0, "%d", tsList[tsId]);
            AtStrncat(buf, nxds0, sizeof(buf));
            StrToCell(tabPtr, row_i, colIdx++, buf);

            /*---------------------------------------------------------*/
            /* Tx Signaling enable */
            enable = AtAttControllerTxSignalingIsEnabled(controller, (uint8)tsList[tsId]);
            ColorStrToCell(tabPtr, row_i, colIdx++, (enable) ? "en" : "dis", cSevNormal);

            /* Tx Signaling pattern mode */
            if (enable)
                {
                patternMode = AtAttControllerTxSignalingPatternGet(controller, (uint8)tsList[tsId], &sigPattern, &threshold);
                patternModeString = CliSignalingPrbsModeString(patternMode);
                ColorStrToCell(tabPtr, row_i, colIdx++, patternModeString ? patternModeString : "Error", patternModeString ? cSevNormal : cSevCritical);
                }
            else
                StrToCell(tabPtr, row_i, colIdx++, "N/A");

            /* Tx Signaling pattern Value*/
            if (enable)
                {
                AtOsalMemInit(buf, 0, sizeof(buf));
                AtSprintf(buf, sAtNotApplicable);
                if (patternMode == cAtAttSignalingModeFixedAB||patternMode == cAtAttSignalingModeFixedABCD)
                    AtSprintf(buf, "%x", sigPattern);
                StrToCell(tabPtr, row_i, colIdx++, buf);
                }
            else
                StrToCell(tabPtr, row_i, colIdx++, "N/A");

            /* Tx Signaling pattern threshold */
            AtOsalMemInit(buf, 0, sizeof(buf));
            AtSprintf(buf, sAtNotApplicable);
            AtSprintf(buf, "%d", threshold);
            StrToCell(tabPtr, row_i, colIdx++, buf);

            /*---------------------------------------------------------*/
            /* Rx Signaling enable */
            enable = AtAttControllerRxSignalingIsEnabled(controller, (uint8)tsList[tsId]);
            ColorStrToCell(tabPtr, row_i, colIdx++, (enable) ? "en" : "dis", cSevNormal);

            /* Rx Expected Signaling pattern mode */
            if (enable)
                {
                patternMode = AtAttControllerRxExpectedSignalingPatternGet(controller, (uint8)tsList[tsId], &sigPattern, &threshold);
                patternModeString = CliSignalingPrbsModeString(patternMode);
                ColorStrToCell(tabPtr, row_i, colIdx++, patternModeString ? patternModeString : "Error", patternModeString ? cSevNormal : cSevCritical);
                }
            else
                StrToCell(tabPtr, row_i, colIdx++, "N/A");

            /* Rx Expected Signaling pattern Value*/
            if (enable)
                {
                AtOsalMemInit(buf, 0, sizeof(buf));
                AtSprintf(buf, sAtNotApplicable);
                if (patternMode == cAtAttSignalingModeFixedAB||patternMode == cAtAttSignalingModeFixedABCD)
                    AtSprintf(buf, "%x", sigPattern);
                StrToCell(tabPtr, row_i, colIdx++, buf);
                }
            else
                StrToCell(tabPtr, row_i, colIdx++, "N/A");

            /* Rx Signaling pattern threshold */
            AtOsalMemInit(buf, 0, sizeof(buf));
            AtSprintf(buf, sAtNotApplicable);
            AtSprintf(buf, "%d", threshold);
            StrToCell(tabPtr, row_i, colIdx++, buf);
            /*--------------------------------------------------------*/
            /* Signaling alarm */
            alarm = AtAttControllerSignalingLopAlarmGet(controller, (uint8)tsList[tsId]);
            if (enable)
                ColorStrToCell(tabPtr, row_i, colIdx++, (alarm) ? "lop" : "none", (alarm) ? cSevCritical : cSevInfo);
            else
                StrToCell(tabPtr, row_i, colIdx++, "N/A");

            /* Signaling Sticky */
            sticky        = AtAttControllerSignalingLopStickyGet(controller, (uint8)tsList[tsId], readingMode);
            if (enable)
                {
                AtOsalMemInit(buf, 0, sizeof(buf));
                AtSprintf(buf, "%s", "");
                if (sticky & cAtSignalingLop)
                    AtStrcat(buf, "lop");
                if (sticky & cAtSignalingRpe)
                    {
                    if (AtStrcmp(buf,""))
                        AtStrcat(buf, "|");
                    AtStrcat(buf, "rpe");
                    }
                ColorStrToCell(tabPtr, row_i, colIdx++, (sticky) ? buf : "none", (sticky) ? cSevCritical : cSevInfo);
                }
            else
                StrToCell(tabPtr, row_i, colIdx++, "N/A");

            row_i++;
            }
        }
    if (!silent)
        {
        TablePrint(tabPtr);
        TableFree(tabPtr);
        }
    return cAtTrue;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP
 * 
 * File        : CliPppOamProcess.h
 * 
 * Created Date: Feb 27, 2014
 *
 * Description : PPP link CLIs
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLIPPPOAMPROCESS_H_
#define _CLIPPPOAMPROCESS_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint8 *LcpBuild(uint8 code, uint8 *data, uint16 dataLength, uint16 *lcpLen);
uint8 *IpcpBuild(uint8 code, uint8 *data, uint8 dataLength, uint16 *ipcpLen);
uint8 *LcpMruOption(uint16 mruValue, uint8 *length);
uint8 *LcpEchoRequest(uint32 magicNumber, uint8 *data, uint16 dataLength, uint16 *length);
uint8 *IpcpIpAddress(uint8 *ipAddr, uint8 *length);
uint8 *LcpPfcOption(uint8 *length);
uint8 *PppBuild(uint16 *pppLen);
uint32 PppOamMaxPacketLength(void);

void PppPacketDump(uint32 linkId, const uint8 *packetBuf, uint32 length);
void CliShowOamPacketInTable(uint32 linkId, const uint8 *packetBuf, uint32 length);

#endif /* _CLIPPPOAMPROCESS_H_ */


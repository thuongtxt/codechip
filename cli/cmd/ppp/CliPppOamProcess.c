/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : CliPppOamDump.c
 *
 * Created Date: Dec 7, 2012
 *
 * Description : OAM processing
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "CliPppOamProcess.h"

/*--------------------------- Define -----------------------------------------*/
/* Supported protocols */
#define cAtPppProtIdLcp  0xc021
#define cAtPppProtIdBcp  0x8031
#define cAtPppProtIdIpcp 0x8021

#define cPppOamMaxPacketLength  512
#define cDefaultOamHeaderLength 6

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_oamId = 1;
static uint8 m_buf[cPppOamMaxPacketLength];

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/*
 * Get byte
 */
static uint8 GetByte(const uint8 *p, const uint8 **pNext)
    {
    uint8 value = *((const uint8*)p);
    if (pNext)
        *pNext = p + 1;
    return value;
    }

/*
 * Get word
 */
static uint16 GetWord(const uint8 *p, const uint8 **pNext)
    {
    uint16 value = *((const uint16*)(const void *)p);
    if (pNext)
        *pNext = p + 2;
    return value;
    }

/*
 * Get dword
 */
static uint32 GetDword(const uint8 *p, const uint8 **pNext)
    {
    uint32 value = *((const uint32*)(const void *)p);
    if (pNext)
        *pNext = p + 4;
    return value;
    }

static const char *Code2String(uint8 code)
    {
    int i;
    static char buf[16];

    struct
        {
        int code;
        const char *name;
        }codes[] = {
                   {1, "Configure-Request"},
                   {2, "Configure-Ack"},
                   {3, "Configure-Nak"},
                   {4, "Configure-Reject"},
                   {5, "Terminate-Request"},
                   {6, "Terminate-Ack"},
                   {7, "Code-Reject"},
                   {8, "Protocol-Reject"},
                   {9, "Echo-Request"},
                   {10,"Echo-Reply"},
                   {11,"Discard-Request"},
                   {-1, NULL},
                   };
    for (i = 0; codes[i].name; i++)
        {
        if (code == codes[i].code)
            return codes[i].name;
        }

    AtSprintf(buf, "Unknown(%u)", code);
    return buf;
    }

static const char *ProtocolCode2String(uint16 code)
    {
    int i;
    static char buf[16];
    struct
        {
        uint16 code;
        const char *name;
        }codes[] = {
                    {0x0021, "Internet Protocol"},
                    {0x0023, "OSI Network Layer"},
                    {0x0025, "Xerox NS IDP"},
                    {0x0027, "DECnet Phase IV"},
                    {0x0029, "Appletalk"},
                    {0x002b, "Novell IPX"},
                    {0x002d, "Van Jacobson Compressed TCP/IP"},
                    {0x002f, "Van Jacobson Uncompressed TCP/IP"},
                    {0x0031, "Bridging PDU"},
                    {0x0033, "Stream Protocol (ST-II)"},
                    {0x0035, "Banyan Vines"},
                    {0x0201, "802.1d Hello Packets"},
                    {0x0231, "Luxcom"},
                    {0x0233, "Sigma Network Systems"},
                    {cAtPppProtIdIpcp, "Internet Protocol Control Protocol"},
                    {0x8023, "OSI Network Layer Control Protocol"},
                    {0x8025, "Xerox NS IDP Control Protocol"},
                    {0x8027, "DECnet Phase IV Control Protocol"},
                    {0x8029, "Appletalk Control Protocol"},
                    {0x802b, "Novell IPX Control Protocol"},
                    {cAtPppProtIdBcp, "Bridging NCP"},
                    {0x8033, "Stream Protocol Control Protocol"},
                    {0x8035, "Banyan Vines Control Protocol"},
                    {cAtPppProtIdLcp, "Link Control Protocol"},
                    {0xc023, "Password Authentication Protocol"},
                    {0xc025, "Link Quality Report"},
                    {0xc223, "Challenge Handshake Authentication Protocol"},
                    {0xFFFF, NULL},
                   };
    for (i = 0; codes[i].name; i++)
        {
        if (code == codes[i].code)
            return codes[i].name;
        }

    AtSprintf(buf, "Unknown(%u)", code);
    return buf;
    }

/*
 * Dump LCP options
 */
static char *LcpOptionDump(const uint8 *buffer, const uint8 **p)
    {
    static char optionStr[64];
    uint8 len, type;
    const uint8 *next = buffer;

    type = GetByte(next, &next);
    len  = GetByte(next, &next);


    /* LCP options */
    if (type == 1)
        AtSprintf(optionStr, "MRU: %u", GetWord(next, &next));
    else if (type == 3)
        AtSprintf(optionStr, "Authentication-Protocol: %u", GetWord(next, &next));
    else if (type == 4)
        AtSprintf(optionStr, "Quality-Protocol: %u", GetWord(next, &next));
    else if (type == 5)
        AtSprintf(optionStr, "Magic-Number: %u", GetWord(next, &next));
    else if (type == 7)
        AtSprintf(optionStr, "Protocol-Field-Compression (PFC)");
    else if (type == 8)
        AtSprintf(optionStr, "Address-and-Control-Field-Compression (ACFC)");

    /* MLPPP options */
    else if (type == 17)
        AtSprintf(optionStr, "MRRU: %u", GetWord(next, &next));
    else if (type == 18)
        AtSprintf(optionStr, "Short sequence");
    else if (type == 19)
        AtSprintf(optionStr, "End-point Discriminator: class %u", GetByte(next, &next));
    else if (type == 26)
        AtSprintf(optionStr, "Prefix elision");
    else if (type == 27)
        {
        uint8 code = GetByte(next, &next);
        uint8 susp = GetByte(next, &next);
        AtSprintf(optionStr, "Multilink header format, code = %u, Susp Clses = %u", code, susp);
        }

    /* Unknown option */
    else
        AtSprintf(optionStr, "Unknown option %u", type);

    if (p)
        *p = buffer + len;
    return optionStr;
    }

/*
 * Dump BCP options
 */
static char *BcpOptionDump(const uint8 *buffer, const uint8 **p)
    {
    static char optionStr[64];
    uint8 len, type;
    const uint8 *next = buffer;

    type = GetByte(next, &next);
    len  = GetByte(next, &next);

    if (type == 1)
        AtSprintf(optionStr, "Bridge-Identification");
    else if (type == 2)
        AtSprintf(optionStr, "Line-Identification");
    else if (type == 3)
        AtSprintf(optionStr, "MAC-Support");
    else if (type == 7)
        AtSprintf(optionStr, "Spanning-Tree-Protocol");
    else if (type == 8)
        AtSprintf(optionStr, "IEEE-802-Tagged-Frame");
    else if (type == 9)
        AtSprintf(optionStr, "Management-Inline");
    else if (type == 10)
        AtSprintf(optionStr, "Bridge-Control-Packet-Indicator");

    /* Unknown option */
    else
        AtSprintf(optionStr, "Unknown option %u", type);

    if (p)
        *p = buffer + len;
    return optionStr;
    }

static void RawPacketDump(const char *prefix, const uint8 *packetBuf, uint32 length)
    {
    uint32 i;
    AtPrintc(cSevNormal, "%s: ", prefix);

    for (i = 0 ; i < length; i++)
        AtPrintc(cSevNormal, "%02X ", packetBuf[i]);

    AtPrintc(cSevNormal, "\r\n");
    }

/*
 * Get IP address from dword value
 */
static char *IpAddrFromDword(uint32 ipAddr)
    {
    static char buf[32];
    int i;
    const uint8 *p = (const uint8 *)&ipAddr;

    AtOsalMemInit(buf, 0, sizeof(buf));
    for (i = 0; i < 4; i++)
        {
        char tmp[10];

        AtOsalMemInit(tmp, 0, sizeof(tmp));
        AtSprintf(tmp, "%u.", GetByte(p, &p));
        AtStrcat(buf, tmp);
        }

    buf[AtStrlen(buf) - 1] = '\0'; /* Remove the last '. */

    return buf;
    }

/*
 * Dump IPCP options
 */
static char *IpcpOptionDump(const uint8 *buffer, const uint8 **p)
    {
    static char optionStr[64];
    uint8 len, type;
    const uint8 *next = buffer;

    type = GetByte(next, &next);
    len  = GetByte(next, &next);

    if (type == 2)
        AtSprintf(optionStr, "IP-Compression-Protocol");
    else if (type == 3)
        AtSprintf(optionStr, "IP-Address: %s", IpAddrFromDword(GetDword(next, &next)));

    /* Unknown option */
    else
        AtSprintf(optionStr, "Unknown option %u", type);

    if (p)
        *p = buffer + len;
    return optionStr;
    }

static void ProtocolPacketDump(const char* protocol,
                               const uint8 *proPacketBuf,
                               uint32 length,
                               char* (*optionDump)(const uint8 *, const uint8 **))
    {
    const uint8 *p = proPacketBuf;
    uint8 code;
    uint16 len;

    if (length < 4)
        {
        RawPacketDump(protocol, proPacketBuf, length);
        return;
        }

    code = GetByte(p, &p);
    AtPrintc(cSevNormal, "- Code: %s\r\n", Code2String(code));
    AtPrintc(cSevNormal, "- Identifier: %u\r\n", GetByte(p, &p));
    len = GetWord(p, &p);
    AtPrintc(cSevNormal, "- Length: %u\r\n", len);

    if (length < len)
        {
        RawPacketDump("Options", p, length - 4);
        return;
        }

    /* Configure-Request, Configure-Ack, Configure-Nak */
    if ((code == 1) ||
        (code == 2) ||
        (code == 3) ||
        (code == 4))
        {
        AtPrintc(cSevNormal, "- Options:\r\n");
        while ((p - proPacketBuf) < len)
            AtPrintc(cSevNormal, "  + %s\r\n", optionDump(p, &p));
        }
    }

/*
 * To show the meaningful format
 *
 * @param packetBuf Packet buffer
 */
void PppPacketDump(uint32 linkId, const uint8 *packetBuf, uint32 length)
    {
    const uint8 *p = packetBuf;
    uint16 protocol;
	AtUnused(linkId);

    RawPacketDump("Packet", packetBuf, length);

    if (length < 6)
        return;

    AtPrintc(cSevNormal, "Link: %u\r\n", GetWord(p, &p) + 1);
    AtPrintc(cSevNormal, "Address/Control: %x\r\n", GetWord(p, &p));
    protocol = GetWord(p, &p);
    AtPrintc(cSevNormal, "Protocol: %s\r\n", ProtocolCode2String(protocol));
    length -= 6;

    if (protocol == cAtPppProtIdLcp)
        ProtocolPacketDump("LCP", p, length, LcpOptionDump);
    if (protocol == cAtPppProtIdBcp)
        ProtocolPacketDump("BCP", p, length, BcpOptionDump);
    if (protocol == cAtPppProtIdIpcp)
        ProtocolPacketDump("IPCP", p, length, IpcpOptionDump);
    }

void CliShowOamPacketInTable(uint32 linkId, const uint8 *packetBuf, uint32 length)
    {
    const uint8 *p = packetBuf;
    const uint8 *q;
    uint16 protocol;
    uint8 code;
    uint16 len;
    tTab *tabPtr;
    char buff[80];
    const char *heading[] = {"LinkId", "Address/Control", "Protocol", "Code", "Identifier", "Length", "Option"};
	AtUnused(linkId);

    /* Create table with titles */
    tabPtr = TableAlloc(1, 7, heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot alloc table\r\n");
        return;
        }

    if (length < 6)
        {
        TableFree(tabPtr);
        return;
        }

    AtSprintf(buff, "%u", GetWord(p, &p) + 1);
    StrToCell(tabPtr, 0, 0, buff);

    AtSprintf(buff, "%x", GetWord(p, &p));
    StrToCell(tabPtr, 0, 1, buff);

    protocol = GetWord(p, &p);
    AtSprintf(buff, "%s", ProtocolCode2String(protocol));
    StrToCell(tabPtr, 0, 2, buff);

    q = p;
    length -= 6;
    if (length < 4)
        {
        TablePrint(tabPtr);
        TableFree(tabPtr);
        return;
        }

    code = GetByte(q, &q);
    AtSprintf(buff, "%s", Code2String(code));
    StrToCell(tabPtr, 0, 3, buff);

    AtSprintf(buff, "%u", GetByte(q, &q));
    StrToCell(tabPtr, 0, 4, buff);

    len = GetWord(q, &q);
    AtSprintf(buff, "%u", len);
    StrToCell(tabPtr, 0, 5, buff);

    if (length < len)
        {
        TablePrint(tabPtr);
        TableFree(tabPtr);
        return;
        }

    if ((code == 1) ||
        (code == 2) ||
        (code == 3) ||
        (code == 4))
        {
        while ((q - p) < len)
            {
            if (protocol == cAtPppProtIdLcp)
                AtSprintf(buff, "%s", LcpOptionDump(q, &q));
            if (protocol == cAtPppProtIdBcp)
                AtSprintf(buff, "%s", BcpOptionDump(q, &q));
            if (protocol == cAtPppProtIdIpcp)
                AtSprintf(buff, "%s", IpcpOptionDump(q, &q));

            StrToCell(tabPtr, 0, 6, buff);
            }
        }

    AtPrintc(cSevNormal, "\r\n");
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return;
    }

static eBool OamPacketLengthIsValid(uint16 oamPacketLength)
    {
    return (oamPacketLength <= PppOamMaxPacketLength()) ? cAtTrue : cAtFalse;
    }

static uint32 PppOamMaxDataLength(void)
    {
    return PppOamMaxPacketLength() - cDefaultOamHeaderLength;
    }

static eBool OamDataLengthIsValid(uint16 oamDataLength)
    {
    return (oamDataLength <= PppOamMaxDataLength()) ? cAtTrue : cAtFalse;
    }

static uint16 FullOamPacketLength(uint16 dataLength)
    {
    return (uint16)(dataLength + cDefaultOamHeaderLength);
    }

uint8 *LcpBuild(uint8 code, uint8 *data, uint16 dataLength, uint16 *lcpLen)
    {
    uint8 protocol[2] = {0xc0, 0x21};

    if (!OamDataLengthIsValid(dataLength))
        return NULL;

    if (!OamPacketLengthIsValid(FullOamPacketLength(dataLength)))
        return NULL;

    AtOsalMemInit(m_buf, 0, sizeof(m_buf));

    /* Protocol */
    AtOsalMemCpy(m_buf, protocol, 2);

    m_buf[2] = code;      /* Code */
    m_buf[3] = m_oamId++; /* Identifier */

    /* Length */
    *lcpLen  = (uint16)(4 + dataLength);
    m_buf[4] = (uint8)((*lcpLen & cBit15_8) >> 8);
    m_buf[5] = (uint8)(*lcpLen & cBit7_0);

    /* Data */
    if (dataLength)
        AtOsalMemCpy(&(m_buf[6]), data, dataLength);

    *lcpLen = (uint16)(*lcpLen + 2); /* For PPP ID field */

    return m_buf;
    }

uint8 *IpcpBuild(uint8 code, uint8 *data, uint8 dataLength, uint16 *ipcpLen)
    {
    uint8 protocol[2] = {0x80, 0x21};

    if (!OamDataLengthIsValid(dataLength))
        return NULL;

    if (!OamPacketLengthIsValid(FullOamPacketLength(dataLength)))
        return NULL;

    AtOsalMemInit(m_buf, 0, sizeof(m_buf));

    /* Protocol */
    AtOsalMemCpy(m_buf, protocol, 2);

    m_buf[2] = code;      /* Code */
    m_buf[3] = m_oamId++; /* Identifier */

    /* Length */
    *ipcpLen = (uint16)(4 + dataLength);
    m_buf[4] = (uint8)((*ipcpLen & cBit15_8) >> 8);
    m_buf[5] = (uint8)(*ipcpLen & cBit7_0);

    /* Data */
    AtOsalMemCpy(&(m_buf[6]), data, dataLength);

    *ipcpLen = (uint16)(*ipcpLen + 2); /* For PPP ID field */

    return m_buf;
    }

uint8 *LcpMruOption(uint16 mruValue, uint8 *length)
    {
    static uint8 buf[4];

    buf[0] = 1;                          /* Type */
    buf[1] = 4;                          /* Length */
    buf[2] = (uint8)((mruValue & cBit15_8) >> 8); /* MSB */
    buf[3] = (mruValue & cBit7_0);       /* LSB */

    *length = 4;

    return buf;
    }

uint8 *LcpPfcOption(uint8 *length)
    {
    static uint8 buf[4];

    buf[0] = 7;                          /* Type */
    buf[1] = 2;                          /* Length */

    *length = 2;

    return buf;
    }

uint8 *IpcpIpAddress(uint8 *ipAddr, uint8 *length)
    {
    static uint8 buf[6];

    buf[0] = 3;                          /* Type */
    buf[1] = 6;                          /* Length */
    buf[2] = *ipAddr; /* MSB */
    buf[3] = *(ipAddr + 1);
    buf[4] = *(ipAddr + 2);
    buf[5] = *(ipAddr + 3);

    *length = 6;

    return buf;
    }

uint8 *LcpEchoRequest(uint32 magicNumber, uint8 *data, uint16 dataLength, uint16 *length)
    {
    static uint8 buf[512];

    /* Not enough buffer */
    *length = (uint16)(dataLength + 4);
    if (*length > 512)
        return NULL;

    /* Magic Number */
    AtOsalMemCpy(buf, &magicNumber, 4);

    /* Data */
    AtOsalMemCpy(&(buf[4]), data, dataLength);

    return buf;
    }

uint8 *PppBuild(uint16 *pppLen)
    {
    static uint8 buf[128];
    uint8 protocol[2] = {0x08, 0x00};

    /* Init value of PPP packet */
    AtOsalMemInit(buf, 0xa, 128);

    /* Protocol */
    AtOsalMemCpy(buf, protocol, 2);

    /* Length */
    *pppLen = 10;

    return buf;
    }

uint32 PppOamMaxPacketLength(void)
    {
    return cPppOamMaxPacketLength;
    }


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2102 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : atclippp.c
 *
 * Created Date: Oct 9, 2012
 *
 * Description : PPP CLIs
 *
 * Notes       :
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"

#include "AtChannel.h"
#include "AtDevice.h"
#include "AtModulePpp.h"
#include "AtModuleEncap.h"
#include "AtHdlcLink.h"
#include "AtHdlcChannel.h"
#include "AtModuleEth.h"
#include "AtEthFlow.h"
#include "AtEncapChannel.h"
#include "AtMpBundle.h"
#include "AtChannelHierarchyDisplayer.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef eAtRet (*SequenceModeSetFunc)(AtMpBundle self, eAtMpSequenceMode seqMd);

/* Command parameter definitions */
static const char * cCmdFragmentSizeStr[] ={"none", "64bytes", "128bytes", "256bytes", "512bytes"};
static const eAtFragmentSize cCmdFragmentSizeVal[]={cAtNoFragment,
                                                    cAtFragmentSize64,
                                                    cAtFragmentSize128,
                                                    cAtFragmentSize256,
                                                    cAtFragmentSize512};

static const char * cCmdWorkingModeStr[] ={"lfi", "mcml"};
static const eAtMpWorkingMode cCmdWorkingModeVal[]={cAtMpWorkingModeLfi, cAtMpWorkingModeMcml};

static const char * cCmdSequenceModeStr[] ={"short", "long"};
static const eAtMpSequenceMode cCmdSequenceModeVal[]={cAtMpSequenceModeShort, cAtMpSequenceModeLong};

static const char * cAtMpSchedulingModeStr[] = {"random", "fair", "strict", "smart", "deficit"};
static const eAtMpSchedulingMode cAtMpSchedulingModeVal[]={cAtMpSchedulingModeRandom,
                                                           cAtMpSchedulingModeFairRR,
                                                           cAtMpSchedulingModeStrictRR,
                                                           cAtMpSchedulingModeSmartRR,
                                                           cAtMpSchedulingModeDWRR};

static const char *cPppBundlePduStr[] = { "ipv4", "ipv6", "ethernet", "any"};
static const uint32 cPppBundlePduVal[] = { cAtHdlcPduTypeIPv4, cAtHdlcPduTypeIPv6,
                                          cAtHdlcPduTypeEthernet, cAtHdlcPduTypeAny};

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModulePpp PppModule(void)
    {
    return (AtModulePpp)AtDeviceModuleGet(CliDevice(), cAtModulePpp);
    }

static AtEthFlow EthFlow(uint32 flowId)
    {
    AtModuleEth moduleEth= (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);

    return AtModuleEthFlowGet(moduleEth, (uint16)(flowId - 1));
    }

static eBool SequenceModeSet(char argc, char **argv, SequenceModeSetFunc sequenceModeSetFunc)
    {
    uint32 numBundle, bundle_i;
    uint32 *pBundleList, bufferSize;
    eAtRet ret;
    eAtMpSequenceMode sequence = cAtMpSequenceModeUnknown;
    eBool blResult;
	AtUnused(argc);

    /* Get list of link */
    ret = cAtOk;
    pBundleList = CliSharedIdBufferGet(&bufferSize);
    numBundle = CliIdListFromString(argv[0], pBundleList, bufferSize);
    if (numBundle == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Bundle parameter invalid, expected 1,2..\r\n");
        return cAtFalse;
        }

    mAtStrToEnum(cCmdSequenceModeStr,
                 cCmdSequenceModeVal,
                 argv[1],
                 sequence,
                 blResult);
    if(!blResult)
        {
        AtPrintc(cSevCritical, "ERROR: Sequence mode invalid, expected short/long\r\n");
        return cAtFalse;
        }

    /* Call API */
    for (bundle_i = 0; bundle_i < numBundle; bundle_i++)
        {
        AtMpBundle bundle = CliMpBundleGet(pBundleList[bundle_i]);
        if (bundle == NULL)
            continue;

        ret = sequenceModeSetFunc(bundle, sequence);
        if (ret != cAtOk)
            mWarnNotReturnCliIfNotOk(ret, bundle, "Can not set sequence mode");
        }

    return cAtTrue;
    }

static eBool BundlesEnable(char *idString, eBool enable)
    {
    uint32      numBundle, wIndex;
    uint32      *pBundleList, bufferSize;
    eAtRet      ret;

    /* Get list of link */
    ret = cAtOk;
    pBundleList = CliSharedIdBufferGet(&bufferSize);
    numBundle = CliIdListFromString(idString, pBundleList, bufferSize);

    /* Call API */
    for (wIndex = 0; wIndex < numBundle; wIndex++)
        {
        AtMpBundle bundle = CliMpBundleGet(pBundleList[wIndex]);
        if (bundle == NULL)
            continue;

        ret = AtChannelEnable((AtChannel)bundle, enable);
        if (ret != cAtOk)
            mWarnNotReturnCliIfNotOk(ret, bundle, "Can not be enabled");
        }

    return cAtTrue;
    }

static eAtRet BundleCountersShow(uint32 *listBundle, uint32 numberBundle, eAtHistoryReadingMode readMode, eBool silent)
    {
    eAtRet ret;
    uint32 wIndex;
    tTab   *tabPtr = NULL;
    uint8  column = 0;
    tAtHdlcBundleCounters hdlcBundleCounter;
    char   buff[50];
    const char *heading[] = {"BundleId", "txGoodByte", "txGoodPkt", "txFragment", "txDiscardPkt", "rxGoodBytes",
                             "rxGoodPkt", "rxFragmentPkt", "rxQueueGoodPkt", "rxQueueDiscardPkt", "rxDiscardFragment"};

    /* Initial value */
    ret = cAtOk;

    /* No bundle to display */
    if (numberBundle == 0)
        {
        AtPrintc(cSevInfo, "No bundle to display, check if input bundle list is valid, example: 1,2-3,5-8, ...");
        return cAtFalse;
        }

    /* Create table with titles */
    if (!silent)
        {
        tabPtr = TableAlloc(numberBundle, mCount(heading), heading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "Cannot alloc table\r\n");
            return cAtErrorRsrcNoAvail;
            }
        }

    for (wIndex = 0; wIndex < numberBundle; wIndex++)
        {
        eBool counterIsValid;
        AtMpBundle bundle = CliMpBundleGet(listBundle[wIndex]);
        if (bundle == NULL)
            continue;

        AtOsalMemInit(&hdlcBundleCounter, 0, sizeof(tAtHdlcBundleCounters));
        if (readMode == cAtHistoryReadingModeReadToClear)
            ret = AtChannelAllCountersClear((AtChannel)bundle, &hdlcBundleCounter);
        else
            ret = AtChannelAllCountersGet((AtChannel)bundle, &hdlcBundleCounter);

        if (ret != cAtOk)
            mWarnNotReturnCliIfNotOk(ret, bundle, "Can not get all counters of bundle");

        column = 0;
        /* Print ID */
        AtSprintf(buff, "%u", listBundle[wIndex]);
        StrToCell(tabPtr, wIndex, column++, buff);

        counterIsValid = (ret == cAtOk) ? cAtTrue : cAtFalse;
        CliCounterPrintToCell(tabPtr, wIndex, column++, hdlcBundleCounter.txGoodByte       , cAtCounterTypeGood  , counterIsValid);
        CliCounterPrintToCell(tabPtr, wIndex, column++, hdlcBundleCounter.txGoodPkt        , cAtCounterTypeGood  , counterIsValid);
        CliCounterPrintToCell(tabPtr, wIndex, column++, hdlcBundleCounter.txFragmentPkt    , cAtCounterTypeGood  , counterIsValid);
        CliCounterPrintToCell(tabPtr, wIndex, column++, hdlcBundleCounter.txDiscardPkt     , cAtCounterTypeError , counterIsValid);
        CliCounterPrintToCell(tabPtr, wIndex, column++, hdlcBundleCounter.rxGoodByte       , cAtCounterTypeGood  , counterIsValid);
        CliCounterPrintToCell(tabPtr, wIndex, column++, hdlcBundleCounter.rxGoodPkt        , cAtCounterTypeGood  , counterIsValid);
        CliCounterPrintToCell(tabPtr, wIndex, column++, hdlcBundleCounter.rxFragmentPkt    , cAtCounterTypeGood  , counterIsValid);
        CliCounterPrintToCell(tabPtr, wIndex, column++, hdlcBundleCounter.rxQueueGoodPkt   , cAtCounterTypeGood  , counterIsValid);
        CliCounterPrintToCell(tabPtr, wIndex, column++, hdlcBundleCounter.rxQueueDiscardPkt, cAtCounterTypeError , counterIsValid);
        CliCounterPrintToCell(tabPtr, wIndex, column++, hdlcBundleCounter.rxDiscardFragment, cAtCounterTypeError , counterIsValid);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return ret;
    }

static uint8 AllLinksInBundleHierarchyShow(AtMpBundle self)
    {
    uint32 numLink;
    AtHdlcLink link;
    AtChannelHierarchyDisplayer displayer = NULL;
    AtIterator iterator = AtHdlcBundleLinkIteratorCreate((AtHdlcBundle)self);
    if (iterator == NULL)
        return 0;

    numLink = (uint8)AtIteratorCount(iterator);
    if (numLink > 0)
        AtPrintc(cSevNormal, "            |\r\n");

    /* Init displayer with NULL and then set channel to prevent allocate and free many time */
    displayer = AtHdlcLinkHierarchyDisplayerGet(cAtChannelHierarchyModeAlarm);
    while((link = (AtHdlcLink)AtIteratorNext(iterator)) != NULL)
        {
        AtChannelHierarchyShow(displayer, (AtChannel)link, "            |");
        }

    AtObjectDelete((AtObject)iterator);
    return (uint8)numLink;
    }

static AtEthFlow *OamFlowListGet(char *listIds, uint32 *numFlows)
    {
    uint32 numBundles, bundle_i;
    uint32 *pBundleList, bufferSize;
    AtEthFlow *allFlows;
    uint32 numFlowIds;
    uint32 numValidFlows = 0;

    *numFlows = 0;

    /* Get the shared ID buffer */
    pBundleList = CliSharedIdBufferGet(&bufferSize);
    numBundles = CliBundleIdListFromString(listIds, pBundleList, bufferSize);
    if (numBundles == 0)
        {
        AtPrintc(cSevWarning, "ERROR: Invalid parameter <channelList> , expected 1-%d\r\n",
                 AtModulePppMaxBundlesGet(PppModule()));
        return NULL;
        }

    allFlows = (AtEthFlow *)((void**)CliSharedObjectListGet(&numFlowIds));
    for (bundle_i = 0; bundle_i < numBundles; bundle_i++)
        {
        AtHdlcBundle bundle = (AtHdlcBundle)CliMpBundleGet(pBundleList[bundle_i]);
        AtEthFlow flow = AtHdlcBundleOamFlowGet(bundle);

        if (flow != NULL)
            {
            allFlows[numValidFlows]= flow;
            numValidFlows++;

            /* If store full share buffer */
            if (numValidFlows == numFlowIds)
                break;
            }
        else
            AtPrintc(cSevWarning, "Flow does not exist on bundle %s\r\n", CliChannelIdStringGet((AtChannel)bundle));
        }
    *numFlows = numValidFlows;

    return allFlows;
    }

static eBool BundleOamFlowVlanSet(char argc, char **argv,
                                  eAtModuleEthRet (*VlanFuncs)(AtEthFlow self, const tAtEthVlanDesc * desc))
    {
    AtEthFlow *flows;
    uint32 flow_i;
    uint32 numFlows;
    tAtEthVlanDesc desc;
    eAtRet ret = cAtOk;
    AtUnused(argc);

    /* Get all flows */
    flows = OamFlowListGet(argv[0], &numFlows);
    if ((numFlows == 0) || (flows == NULL))
        {
        AtPrintc(cSevWarning, "No flow\n");
        return cAtTrue;
        }

    /* Get CVLAN and SVLAN Description */
    AtOsalMemInit(&desc, 0x0, sizeof(desc));
    desc.ethPortId = (uint8)AtStrToDw(argv[1]);
    if (!CliEthFlowVlanGet(argv[2], argv[3], &desc))
        {
        AtPrintc(cSevCritical, "Invalid VLAN, expected %s\r\n", CliExpectedVlanFormat());
        return cAtFalse;
        }

    for (flow_i = 0; flow_i < numFlows; flow_i++)
        ret |= VlanFuncs(flows[flow_i], &desc);

    return (ret != cAtOk) ? cAtFalse : cAtTrue;
    }

AtHdlcLink HdlcLink(uint32 linkId)
    {
    AtModuleEncap moduleEncap;
    AtEncapChannel encapChannel;
    AtHdlcLink hdlcLink;

    /* create encap hdlcppp link */
    linkId = linkId - 1; /* API always starts from 0 */
    moduleEncap = (AtModuleEncap)AtDeviceModuleGet(CliDevice(), cAtModuleEncap);
    encapChannel = AtModuleEncapChannelGet(moduleEncap, (uint16)linkId);
    if (AtEncapChannelEncapTypeGet(encapChannel) != cAtEncapHdlc)
        return NULL;

    /* get Hdlc ppplink */
    hdlcLink = AtHdlcChannelHdlcLinkGet((AtHdlcChannel)encapChannel);

    return hdlcLink;
    }

uint32 CliBundleIdListFromString(char *idString, uint32 *idlist, uint32 bufferSize)
    {
    uint32 numBundles = 0;
    AtIdParser idParser = AtIdParserNew(idString, NULL, NULL);

    while (AtIdParserHasNextNumber(idParser))
        {
        uint32 bundleId = AtIdParserNextNumber(idParser);

        if (CliMpBundleGet(bundleId))
            {
            idlist[numBundles] = bundleId;
            numBundles = numBundles + 1;
            }

        if (numBundles >= bufferSize)
            break;
        }

    AtObjectDelete((AtObject)idParser);

    return numBundles;
    }

/* Set maxdelay bundle */
eBool CmdPppBundleMaxDelay(char argc, char **argv)
    {
    uint32      numBundle, wIndex;
    uint32      *pBundleList, bufferSize;
    uint8       maxDelay;
    eAtRet      ret;
	AtUnused(argc);

    /* Get list of link */
    ret = cAtOk;
    pBundleList = CliSharedIdBufferGet(&bufferSize);
    numBundle = CliIdListFromString(argv[0], pBundleList, bufferSize);
    if (numBundle == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Bundle parameter invalid, expected 1,2..\r\n");
        return cAtFalse;
        }

    /* Call API */
    maxDelay = (uint8)AtStrToDw(argv[1]);
    for (wIndex = 0; wIndex < numBundle; wIndex++)
        {
        AtMpBundle bundle = CliMpBundleGet(pBundleList[wIndex]);
        if (bundle == NULL)
        	continue;
        	
        ret = AtHdlcBundleMaxDelaySet((AtHdlcBundle)bundle, maxDelay);
        if (ret != cAtOk)
            mWarnNotReturnCliIfNotOk(ret, bundle, "Can not set maximum Delay");
        }

    return cAtTrue;
    }

/* Set fragmentsize bundle */
eBool CmdPppBundleFragmentsize(char argc, char **argv)
    {
    uint32      numBundle, wIndex;
    uint32      *pBundleList, bufferSize;
    eAtFragmentSize fragmentSize = cAtFragmentSize128;
    eAtRet      ret = cAtOk;
    eBool       blResult;
	AtUnused(argc);

    pBundleList = CliSharedIdBufferGet(&bufferSize);
    numBundle = CliIdListFromString(argv[0], pBundleList, bufferSize);
    if (numBundle == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Bundle parameter invalid, expected 1,2..\r\n");
        return cAtFalse;
        }

    /* Call API */
    mAtStrToEnum(cCmdFragmentSizeStr,
                 cCmdFragmentSizeVal,
                 argv[1],
                 fragmentSize,
                 blResult);
    if(!blResult)
        {
        AtPrintc(cSevCritical, "ERROR: Fragment size invalid, expected none/128bytes\r\n");
        return cAtFalse;
        }

    for (wIndex = 0; wIndex < numBundle; wIndex++)
        {
        AtMpBundle bundle = CliMpBundleGet(pBundleList[wIndex]);
        if (bundle == NULL)
        	continue;
        	
        ret = AtHdlcBundleFragmentSizeSet((AtHdlcBundle)bundle, fragmentSize);
        if (ret != cAtOk)
            mWarnNotReturnCliIfNotOk(ret, bundle, "Can not set fragment size");
        }

    return cAtTrue;
    }

/* Set sequence mode */
eBool CmdPppBundleSequenceMode(char argc, char **argv)
    {
    return SequenceModeSet(argc, argv, (SequenceModeSetFunc)AtMpBundleSequenceModeSet);
    }

/* Set Tx sequence for bundle */
eBool CmdPppBundleTxSequenceMode(char argc, char **argv)
    {
    return SequenceModeSet(argc, argv, (SequenceModeSetFunc)AtMpBundleTxSequenceModeSet);
    }

/* Set Rx sequence mode for bundle */
eBool CmdPppBundleRxSequenceMode(char argc, char **argv)
    {
    return SequenceModeSet(argc, argv, (SequenceModeSetFunc)AtMpBundleRxSequenceModeSet);
    }
    
/* Set working mode */
eBool CmdPppBundleWorking(char argc, char **argv)
    {
    uint32      numBundle, wIndex;
    uint32      *pBundleList, bufferSize;
    eAtRet      ret;
    eAtMpWorkingMode working = cAtMpWorkingModeLfi;
    eBool       blResult;
	AtUnused(argc);

    /* Get list of link */
    ret = cAtOk;
    pBundleList = CliSharedIdBufferGet(&bufferSize);
    numBundle = CliIdListFromString(argv[0], pBundleList, bufferSize);
    if (numBundle == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Bundle parameter invalid, expected 1,2..\r\n");
        return cAtFalse;
        }

    mAtStrToEnum(cCmdWorkingModeStr,
                 cCmdWorkingModeVal,
                 argv[1],
                 working,
                 blResult);
    if(!blResult)
        {
        AtPrintc(cSevCritical, "ERROR: Working mode invalid, expected lfi/mcml\r\n");
        return cAtFalse;
        }

    /* Call API */
    for (wIndex = 0; wIndex < numBundle; wIndex++)
        {
        AtMpBundle bundle = CliMpBundleGet(pBundleList[wIndex]);
        if (bundle == NULL)
            continue;

        ret = AtMpBundleWorkingModeSet(bundle, working);
        if (ret != cAtOk)
            mWarnNotReturnCliIfNotOk(ret, bundle, "Can not set working mode");
        }

    return cAtTrue;
    }

/* Set Mrru mode */
eBool CmdPppBundleMrru(char argc, char **argv)
    {
    uint32      numBundle, wIndex;
    uint32      *pBundleList, bufferSize;
    eAtRet      ret;
    uint32      mrru;
	AtUnused(argc);

    /* Get list of link */
    ret = cAtOk;
    pBundleList = CliSharedIdBufferGet(&bufferSize);
    numBundle = CliIdListFromString(argv[0], pBundleList, bufferSize);
    if (numBundle == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Bundle parameter invalid, expected 1,2..\r\n");
        return cAtFalse;
        }

    /* Call API */
    mrru = (uint32)AtStrToDw(argv[1]);
    for (wIndex = 0; wIndex < numBundle; wIndex++)
        {
        AtMpBundle bundle = CliMpBundleGet(pBundleList[wIndex]);
        if (bundle == NULL)
            continue;

        ret = AtMpBundleMrruSet(bundle, mrru);
        if (ret != cAtOk)
            mWarnNotReturnCliIfNotOk(ret, bundle, "Can not set MRRU");
        }

    return cAtTrue;
    }

/* enable bundle */
eBool CmdPppBundleEnable(char argc, char **argv)
    {
	AtUnused(argc);
    return BundlesEnable(argv[0], cAtTrue);
    }

/* disable bundle */
eBool CmdPppBundleDisable(char argc, char **argv)
    {
	AtUnused(argc);
    return BundlesEnable(argv[0], cAtFalse);
    }

/* Add link to bundle */
eBool CmdPppBundleAddLink(char argc, char **argv)
    {
    uint32      numberOfLinkId, wIndex;
    uint32      *pLinkList, bufferSize;
    AtHdlcLink  link;
    eAtRet      ret;
    uint16      bundleId;
    AtMpBundle  bundle;
	AtUnused(argc);

    /* Initial value */
    ret = cAtOk;

    /* parse bundleId */
    bundleId = (uint16)AtStrToDw(argv[0]);
    bundle = CliMpBundleGet(bundleId);
    if (bundle == NULL)
        {
        AtPrintc(cSevCritical, "Bundle = %u, maybe not created\r\n", bundleId);
        return cAtFalse;
        }

    /* parse list of link */
    pLinkList = CliSharedIdBufferGet(&bufferSize);
    numberOfLinkId = CliIdListFromString(argv[1], pLinkList, bufferSize);
    if (numberOfLinkId == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Link parameter invalid, expected 1,2..\r\n");
        return cAtFalse;
        }

    /* Call API */
    for (wIndex = 0; wIndex < numberOfLinkId; wIndex++)
        {
        link = HdlcLink(pLinkList[wIndex]);
        if (link == NULL)
            {
            AtPrintc(cSevCritical, "ERROR: Link %u not been created\n", pLinkList[wIndex]);
            continue;
            }

        ret = AtHdlcBundleLinkAdd((AtHdlcBundle)bundle, link);
        if (ret != cAtOk)
            mWarnNotReturnCliIfNotOk(ret, bundle, "Bundle can not add link");
        }

    return cAtTrue;
    }

/* Remove link from bundle */
eBool CmdPppBundleRemoveLink(char argc, char **argv)
    {
    uint32      numberOfLinkId, wIndex;
    uint32      *pLinkList, bufferSize;
    AtHdlcLink  link;
    eAtRet      ret;
    uint16      bundleId;
    AtMpBundle  bundle;
	AtUnused(argc);

    /* Initial value */
    ret = cAtOk;

    /* parse bundleId */
    bundleId = (uint16)AtStrToDw(argv[0]);
    bundle = CliMpBundleGet(bundleId);
    if (bundle == NULL)
        {
        AtPrintc(cSevCritical, "Bundle = %u, maybe not created\r\n", bundleId);
        return cAtFalse;
        }

    /* parse list of link */
    pLinkList = CliSharedIdBufferGet(&bufferSize);
    numberOfLinkId = CliIdListFromString(argv[1], pLinkList, bufferSize);
    if (numberOfLinkId == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Link parameter invalid, expected 1,2..\r\n");
        return cAtFalse;
        }

    /* Call API */
    for (wIndex = 0; wIndex < numberOfLinkId; wIndex++)
        {
        link = HdlcLink(pLinkList[wIndex]);
        if (link == NULL)
            {
            AtPrintc(cSevCritical, "ERROR: Link %u not been created\r\n", pLinkList[wIndex]);
            continue;
            }

        ret = AtHdlcBundleLinkRemove((AtHdlcBundle)bundle, link);
        if (ret != cAtOk)
           mWarnNotReturnCliIfNotOk(ret, bundle, "Bundle can not remove link");
        }

    return cAtTrue;
    }

/* show number link in bundle */
eBool CmdPppBundleShowLink(char argc, char **argv)
    {
    uint32 bundleId;
    uint32 numBundles;
    uint32 numRows;
    uint32 *pBundleList, bufferSize;
    uint32 rowId = 0;
    tTab   *tabPtr;
    char   buff[16];
    const char *heading[] = {"BundleId", "LinkId"};
	AtUnused(argc);

    /* parse bundle Id */
    pBundleList = CliSharedIdBufferGet(&bufferSize);
    numBundles = CliIdListFromString(argv[0], pBundleList, bufferSize);

    /* Calculate number of links of all input bundles */
    numRows = 0;
    for (bundleId = 0; bundleId < numBundles; bundleId++)
        {
        AtHdlcBundle bundle = (AtHdlcBundle)CliMpBundleGet(pBundleList[bundleId]);
        numRows = numRows + AtHdlcBundleNumberOfLinksGet(bundle);
        }

    /* No links to show */
    if (numRows == 0)
        {
        AtPrintc(cSevInfo, "No link to display\r\n");
        return cAtTrue;
        }

    /* Make table */
    tabPtr = TableAlloc(numRows, 2, heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        return cAtFalse;
        }

    /* Show table */
    for (bundleId = 0; bundleId < numBundles; bundleId++)
        {
        AtIterator  linkIterator;
        AtHdlcLink  link;
        AtHdlcBundle bundle = (AtHdlcBundle)CliMpBundleGet(pBundleList[bundleId]);
        if (bundle == NULL)
            continue;

        /* Ignore bundle that has no link */
        if (AtHdlcBundleNumberOfLinksGet(bundle) == 0)
            continue;

        linkIterator = AtHdlcBundleLinkIteratorCreate(bundle);
        while ((link = (AtHdlcLink)AtIteratorNext(linkIterator)) != NULL)
            {
            /* Print Bundle Id */
            AtSprintf(buff, "%u", AtChannelIdGet((AtChannel)bundle) + 1);
            StrToCell(tabPtr, rowId, 0, buff);

            /* And this link */
            AtSprintf(buff, "%u", AtChannelIdGet((AtChannel)link) + 1);
            StrToCell(tabPtr, rowId, 1, buff);

            /* Next row */
            rowId = rowId + 1;
            }

        AtObjectDelete((AtObject)linkIterator);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

/* add flow to bundle */
eBool CmdPppBundleAddFlow(char argc, char **argv)
    {
    eAtRet      ret;
    AtEthFlow   flow;
    uint32      bundleId;
    uint32      flowId;
    uint8       classNumber;
    AtMpBundle  bundle;
	AtUnused(argc);

    /* Initial value */
    ret = cAtOk;

    /* parse bundle Id */
    bundleId = (uint32)AtStrToDw(argv[0]);
    bundle = CliMpBundleGet(bundleId);
    if (bundle == NULL)
        {
        AtPrintc(cSevCritical, "Bundle = %u, maybe not created\r\n", bundleId);
        return cAtFalse;
        }

    /* parse flow Id */
    flowId = (uint32)AtStrToDw(argv[1]);
    flow = EthFlow(flowId);
    if (flow == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Flow %u not been created\r\n", flowId);
        return cAtFalse;
        }

    /* parse class number */
    classNumber = (uint8)AtStrToDw(argv[2]);

    /* Call API */
    ret = AtMpBundleFlowAdd(bundle, flow, classNumber);
    if (ret != cAtOk)
        mWarnNotReturnCliIfNotOk(ret, bundle, "Bundle can not add flow");

    return cAtTrue;
    }

/* Remove flow to bundle */
eBool CmdPppBundleRemoveFlow(char argc, char **argv)
    {
    uint32      wIndex, bundleId;
    uint32      *pListFlow;
    uint32      bufferSize;
    uint32      numberFlowInBundle;
    eAtRet      ret;
    AtEthFlow   flow;
    AtMpBundle  bundle;
	AtUnused(argc);

    /* Initial value */
    ret = cAtOk;

    bundleId = (uint32)AtStrToDw(argv[0]);
    bundle = CliMpBundleGet(bundleId);
    if (bundle == NULL)
        {
        AtPrintc(cSevCritical, "Bundle = %u, maybe not created\r\n", bundleId);
        return cAtFalse;
        }

    /* Get list of flow */
    pListFlow = CliSharedIdBufferGet(&bufferSize);
    numberFlowInBundle = CliIdListFromString(argv[1], pListFlow, bufferSize);
    if (numberFlowInBundle == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Flow parameter invalid, Expected 1,2..\r\n");
        return cAtFalse;
        }

    /* Call API */
    for (wIndex = 0; wIndex < numberFlowInBundle; wIndex++)
        {
        flow = EthFlow(pListFlow[wIndex]);
        if (flow == NULL)
            AtPrintc(cSevCritical, "ERROR: Flow %u not created\r\n", pListFlow[wIndex]);

        ret = AtMpBundleFlowRemove(bundle, flow);
        if (ret != cAtOk)
            mWarnNotReturnCliIfNotOk(ret, bundle, "Bundle can not remvoe flow");
        }

    return cAtTrue;
    }

/* Set sequence number */
eBool CmdPppBundleSequenceNumberSet(char argc, char **argv)
    {
    eAtRet ret = cAtOk;
    uint32 bundleId;
    uint8  classNumber, i = 0;
    uint16 sequenceNumber;
    AtMpBundle  bundle;
	AtUnused(argc);

    /* parse bundle Id */
    bundleId = (uint32)AtStrToDw(argv[i++]);
    bundle = CliMpBundleGet(bundleId);
    if (bundle == NULL)
        {
        AtPrintc(cSevCritical, "Bundle = %u, maybe not created\r\n", bundleId);
        return cAtFalse;
        }

    /* parse class number */
    classNumber = (uint8)AtStrToDw(argv[i++]);

    /* parse sequence number */
    sequenceNumber = (uint16)AtStrToDw(argv[i]);

    /* Call API */
    ret = AtMpBundleSequenceNumberSet(bundle, classNumber, sequenceNumber);
    if (ret != cAtOk)
        mWarnNotReturnCliIfNotOk(ret, bundle, "Bundle not existing");

    return cAtTrue;
    }

/* show flow in bundle */
eBool CmdPppFlowInBundleShow(char argc, char **argv)
    {
    uint32 bundle_i, flowId;
    uint32 *bundleIds, bufferSize;
    uint32 numRows, numBundles;
    uint8 classNumber;
    uint32 rowId = 0;
    tTab *tabPtr;
    char buff[16];
    const char *heading[] = {"BundleId", "FlowId", "ClassNumber"};
	AtUnused(argc);

    /* Parse bundle Id */
    bundleIds = CliSharedIdBufferGet(&bufferSize);
    numBundles = CliIdListFromString(argv[0], bundleIds, bufferSize);

    /* Number of flows of all input bundles */
    numRows = 0;
    for (bundle_i = 0; bundle_i < numBundles; bundle_i++)
        numRows = numRows + AtMpBundleNumberOfFlowsGet(CliMpBundleGet(bundleIds[bundle_i]));

    /* No Flow to show */
    if (numRows == 0)
        {
        AtPrintc(cSevInfo, "No Flow to display\r\n");
        return cAtTrue;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numRows, 3, heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot alloc table\r\n");
        return cAtFalse;
        }

    /* Show table */
    for (bundle_i = 0; bundle_i < numBundles; bundle_i++)
        {
        AtEthFlow  flow;
        AtIterator flowIterator;
        AtMpBundle mpBundle = CliMpBundleGet(bundleIds[bundle_i]);
        if (mpBundle == NULL)
            continue;

        /* Ignore bundle that has no flow */
        if (AtMpBundleNumberOfFlowsGet(mpBundle) == 0)
            continue;

        flowIterator = AtMpBundleFlowIteratorCreate(mpBundle);
        while ((flow = (AtEthFlow)AtIteratorNext(flowIterator)) != NULL)
            {
            /* Print bundle */
            AtSprintf(buff, "%u", bundleIds[bundle_i]);
            StrToCell(tabPtr, rowId, 0, buff);

            /* Print flow */
            flowId = AtChannelIdGet((AtChannel)flow);
            AtSprintf(buff, "%u", flowId + 1);
            StrToCell(tabPtr, rowId, 1, buff);

            /* Print class number */
            classNumber = AtMpBundleFlowClassNumberGet(mpBundle, flow);
            AtSprintf(buff, "%u", classNumber);
            StrToCell(tabPtr, rowId, 2, buff);

            /* Next row */
            rowId = rowId + 1;
            }

        AtObjectDelete((AtObject)flowIterator);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

/* show counter bundle */
eBool CmdPppBundleCounterShow(char argc, char **argv)
    {
    eAtRet ret;
    uint32 numberBundle;
    uint32 *pListBundles, bufferSize;
    eBool silent = cAtFalse;
    eAtHistoryReadingMode readingMode;
	AtUnused(argc);

	if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    /* Get list of link */
    ret = cAtOk;
    pListBundles = CliSharedIdBufferGet(&bufferSize);
    numberBundle = CliBundleIdListFromString(argv[0], pListBundles, bufferSize);
    if (numberBundle == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Bundle list may be invalid or no created bundle\r\n");
        return cAtFalse;
        }

    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    if (argc > 2)
	    silent = CliHistoryReadingShouldSilent(readingMode, argv[2]);

    /* Call API */
    ret = BundleCountersShow(pListBundles, numberBundle, readingMode, silent);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "Can not show counters of bundles\n");
        return cAtFalse;
        }

    return cAtTrue;
    }

/* show counter bundle */
eBool CmdPppBundleConfigShow(char argc, char **argv)
    {
    uint32      bundle_i, numberBundle;
    uint32      *pBundleList, bufferSize;
    eAtRet      ret;
    eBool       convertSuccess;
    tTab        *tabPtr;
    char        buff[16];
    eAtMpSchedulingMode schedulingMode;
    tAtMpBundleAllConfig  bundleCfg;
    const char *heading[] = {"BundleId",
                             "WorkingMode",
                             "TxSequenceMode",
                             "RxSequenceMode",
                             "Fragmentsize",
                             "SchedulingMode",
                             "MaxDelay",
                             "Mrru",
                             "enable",
                             "PDU"};
	AtUnused(argc);

    ret = cAtOk;

    /* Get list of link */
    pBundleList  = CliSharedIdBufferGet(&bufferSize);
    numberBundle = CliBundleIdListFromString(argv[0], pBundleList, bufferSize);
    if (numberBundle == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid bundle list or no created bundle\r\n");
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numberBundle, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot alloc table\r\n");
        return cAtFalse;
        }

    /* Call API */
    for (bundle_i = 0; bundle_i < numberBundle; bundle_i++)
        {
        AtMpBundle bundle = CliMpBundleGet(pBundleList[bundle_i]);
        AtOsalMemInit(&bundleCfg, 0, sizeof(tAtMpBundleAllConfig));

        /* Check if bundle is created */
        if (bundle == NULL)
            {
            AtPrintc(cSevCritical, "Bundle %u not been created\r\n", pBundleList[bundle_i]);
            continue;
            }

        /* Show its configuration */
        ret = AtChannelAllConfigGet((AtChannel)bundle, &bundleCfg);
        if (ret != cAtOk)
            mWarnNotReturnCliIfNotOk(ret, bundle, "Can not get all configuration of this bundle");

        /* Print ID */
        AtSprintf(buff, "%u", pBundleList[bundle_i]);
        StrToCell(tabPtr, bundle_i, 0, buff);

        /* Print working mode */
        mAtEnumToStr(cCmdWorkingModeStr, cCmdWorkingModeVal, bundleCfg.workingMd, buff, convertSuccess);
        StrToCell(tabPtr, bundle_i, 1, convertSuccess ? buff : "Error");

        /* Print sequence direct Ethernet to TDM */
        bundleCfg.seqMd = AtMpBundleTxSequenceModeGet(bundle);
        mAtEnumToStr(cCmdSequenceModeStr, cCmdSequenceModeVal, bundleCfg.seqMd, buff, convertSuccess);
        StrToCell(tabPtr, bundle_i, 2, convertSuccess ? buff : "Error");

        /* Print sequence direct TDM to Ethernet */
        bundleCfg.seqMd = AtMpBundleRxSequenceModeGet(bundle);
        mAtEnumToStr(cCmdSequenceModeStr, cCmdSequenceModeVal, bundleCfg.seqMd, buff, convertSuccess);
        StrToCell(tabPtr, bundle_i, 3, convertSuccess ? buff : "Error");

        /* Print fragment size */
        mAtEnumToStr(cCmdFragmentSizeStr, cCmdFragmentSizeVal, bundleCfg.fragmentSize, buff, convertSuccess);
        StrToCell(tabPtr, bundle_i, 4, buff);

        /* Print scheduling mode */
        schedulingMode = AtMpBundleSchedulingModeGet(bundle);
        mAtEnumToStr(cAtMpSchedulingModeStr, cAtMpSchedulingModeVal, schedulingMode, buff, convertSuccess);
        StrToCell(tabPtr, bundle_i, 5, convertSuccess ? buff : "Error");

        /* Print maxdelay */
        AtSprintf(buff, "%d", bundleCfg.maxLinkDelay);
        StrToCell(tabPtr, bundle_i, 6, buff);

        /* Print Mrru */
        AtSprintf(buff, "%u", bundleCfg.mrru);
        StrToCell(tabPtr, bundle_i, 7, buff);

        /* Print enable */
        mAtBoolToStr(bundleCfg.enable, buff, convertSuccess);
        ColorStrToCell(tabPtr, bundle_i, 8, buff, bundleCfg.enable ? cSevNormal : cSevCritical);

        /* Print PDU */
        bundleCfg.pdu = AtHdlcBundlePduTypeGet((AtHdlcBundle)bundle);
        mAtEnumToStr(cPppBundlePduStr, cPppBundlePduVal, bundleCfg.pdu, buff, convertSuccess);
        StrToCell(tabPtr, bundle_i, 9, convertSuccess ? buff : "Error");
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtPppBundleDebug(char argc, char **argv)
    {
    uint32 numBundles, bufferSize;
    uint32 i;
    uint32 *bundleIds;
	AtUnused(argc);

    /* Get list of bundles */
    bundleIds = CliSharedIdBufferGet(&bufferSize);
    numBundles = CliIdListFromString(argv[0], bundleIds, bufferSize);

    /* Debug them */
    for (i = 0; i < numBundles; i++)
        AtChannelDebug((AtChannel)CliMpBundleGet(bundleIds[i]));

    return cAtTrue;
    }

eBool CmdMpBundleSchedulingModeSet(char argc, char **argv)
    {
    uint32 numBundles, bundle_i;
    uint32 *pBundleList, bufferSize;
    eAtMpSchedulingMode schedulingMode = cAtMpSchedulingModeRandom;
    eBool convertSuccess = cAtFalse;
    eBool success = cAtTrue;
	AtUnused(argc);

    /* Get list of bundle */
    pBundleList = CliSharedIdBufferGet(&bufferSize);
    numBundles = CliBundleIdListFromString(argv[0], pBundleList, bufferSize);
    if (numBundles == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Bundle parameter invalid, expected 1,2..\r\n");
        return cAtFalse;
        }

    /* Get fragment distribution mode */
    mAtStrToEnum(cAtMpSchedulingModeStr,
                 cAtMpSchedulingModeVal,
                 argv[1],
                 schedulingMode,
                 convertSuccess);
    if(!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Scheduling mode is invalid, expected: random/fair/strict/smart\r\n");
        return cAtFalse;
        }

    /* Call API */
    for (bundle_i = 0; bundle_i < numBundles; bundle_i++)
        {
        AtMpBundle bundle = CliMpBundleGet(pBundleList[bundle_i]);
        eAtRet ret = AtMpBundleSchedulingModeSet(bundle, schedulingMode);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot change scheduling mode for bundle %u, ret = %s\r\n",
                     AtChannelIdGet((AtChannel)bundle) + 1,
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdMpBundleHierarchyShow(char argc, char **argv)
    {
    uint32      numBundle, wIndex;
    uint32      *pBundleList, bufferSize;
    AtChannelHierarchyDisplayer displayer = NULL;
    AtMpBundle bundle;
	AtUnused(argc);

    /* Get list of bundle */
    pBundleList = CliSharedIdBufferGet(&bufferSize);
    numBundle = CliBundleIdListFromString(argv[0], pBundleList, bufferSize);
    if (numBundle == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Bundle parameter invalid, expected 1,2..\r\n");
        return cAtFalse;
        }

    /* Call API */
    for (wIndex = 0; wIndex < numBundle; wIndex++)
        {
        bundle = CliMpBundleGet(pBundleList[wIndex]);
        if (bundle == NULL)
            continue;

        displayer = AtMpBundleHierarchyDisplayerGet(cAtChannelHierarchyModeAlarm);
        AtChannelHierarchyShow(displayer, (AtChannel)bundle, "");

        /* Display all link in bundle */
        AllLinksInBundleHierarchyShow(bundle);
        AtPrintc(cSevInfo,"\r\n");
        }

    return cAtTrue;
    }

eBool CmdMpBundleOamFlowEgressVlanAdd(char argc, char **argv)
    {
    return BundleOamFlowVlanSet(argc, argv, AtEthFlowEgressVlanAdd);
    }

eBool CmdMpBundleOamFlowEgressVlanRemove(char argc, char **argv)
    {
    return BundleOamFlowVlanSet(argc, argv, AtEthFlowEgressVlanRemove);
    }

eBool CmdMpBundleOamFlowIngressVlanAdd(char argc, char **argv)
    {
    return BundleOamFlowVlanSet(argc, argv, AtEthFlowIngressVlanAdd);
    }

eBool CmdMpBundleOamFlowIngressVlanRemove(char argc, char **argv)
    {
    return BundleOamFlowVlanSet(argc, argv, AtEthFlowIngressVlanRemove);
    }

eBool CmdAtPppBundlePdu(char argc, char **argv)
    {
    uint32 numBundles;
    uint32 i;
    uint32 *pBundleList, bufferSize;
    uint16 pduType;
    eBool success;
    AtUnused(argc);

    /* Get list of bundle */
     pBundleList = CliSharedIdBufferGet(&bufferSize);
     numBundles = CliBundleIdListFromString(argv[0], pBundleList, bufferSize);
     if (numBundles == 0)
         {
         AtPrintc(cSevCritical, "ERROR: Bundle parameter invalid, expected 1,2..\r\n");
         return cAtFalse;
         }

     pduType = (uint16)CliStringToEnum(argv[1], cPppBundlePduStr, cPppBundlePduVal, sizeof(cPppBundlePduVal), &success);
     if (!success)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid parameter <pduType> , expected ethernet|ipv4|ipv6|any\r\n");
        return cAtFalse;
        }

     success = cAtTrue;
     for (i = 0; i < numBundles; i++)
          {
          AtHdlcBundle bundle = (AtHdlcBundle)CliMpBundleGet(pBundleList[i]);
          eAtRet ret = AtHdlcBundlePduTypeSet(bundle, pduType);
          if (ret != cAtOk)
              {
              AtPrintc(cSevWarning,
                       "Cannot changed PDU type on link %u, ret = %s\r\n",
                       AtChannelIdGet((AtChannel)bundle) + 1,
                       AtRet2String(ret));
              success = cAtFalse;
              }
          }

      return success;
    }

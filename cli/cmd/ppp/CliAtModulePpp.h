/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP
 * 
 * File        : CliAtModulePpp.h
 * 
 * Created Date: Sep 26, 2013
 *
 * Description : PPP CLI common declarations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLIATMODULEPPP_H_
#define _CLIATMODULEPPP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "AtModulePpp.h"
#include "AtMpBundle.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 CliPppLinkIdListFromString(char *idList, AtPppLink *links, uint32 bufferSize);

#endif /* _CLIATMODULEPPP_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : CliAtPidTable.c
 *
 * Created Date: Sep 26, 2013
 *
 * Description : PID table CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtModulePpp.h"
#include "AtPidTable.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModulePpp PppModule(void)
    {
    return (AtModulePpp)AtDeviceModuleGet(CliDevice(), cAtModulePpp);
    }

static AtPidTable PidTable(void)
    {
    return AtModulePppPidTableGet(PppModule());
    }

static uint32 *EntryListFromString(char *idListStr, uint32 *numEntries)
    {
    uint32 *idList;
    uint32 numIds, numValidEntries = 0;
    uint32 i;

    idList = CliSharedIdBufferGet(&numIds);
    numIds = CliIdListFromString(idListStr, idList, numIds);
    for (i = 0; i < numIds; i++)
        {
        if (AtPidTableEntryGet(PidTable(), idList[i] - 1) == NULL)
            continue;
        idList[numValidEntries] = idList[i];
        numValidEntries = numValidEntries + 1;
        }

    if (numEntries)
        *numEntries = numValidEntries;

    return idList;
    }

static eBool AttributeSet(char argc, char **argv, eAtRet (*AttributeSetFunc)(AtPidTableEntry self, uint32 value))
    {
    uint32 value;
    uint32 numEntries, entry_i;
    uint32 *entryList;
    eBool success = cAtTrue;
	AtUnused(argc);

    /* Get list of entries */
    entryList = EntryListFromString(argv[0], &numEntries);
    if (numEntries == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No entry, the ID list may be wrong or all of them are invalid\r\n");
        return cAtTrue;
        }

    /* And attribute value */
    value = AtStrToDw(argv[1]);

    /* Apply all */
    for (entry_i = 0; entry_i < numEntries; entry_i++)
        {
        AtPidTableEntry entry = AtPidTableEntryGet(PidTable(), entryList[entry_i] - 1);
        eAtRet ret = AttributeSetFunc(entry, value);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot configure entry %u, ret = %s\r\n",
                     entryList[entry_i], AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static eBool EntrySet(char argc, char **argv,
                      eAtRet (*EthTypeSet)(AtPidTableEntry self, uint32 ethType),
                      eAtRet (*PidSet)(AtPidTableEntry self, uint32 pid))
    {
    uint32 ethType, pidValue;
    uint32 entry_i, *entryList;
    uint32 numEntries;
    eBool success = cAtTrue;
	AtUnused(argc);

    /* Get list of entries */
    entryList = EntryListFromString(argv[0], &numEntries);
    if (numEntries == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No entry, the ID list may be wrong or all of them are invalid\r\n");
        return cAtTrue;
        }

    /* PID and Ethernet type values */
    pidValue = AtStrToDw(argv[1]);
    ethType  = AtStrToDw(argv[2]);

    for (entry_i = 0; entry_i < numEntries; entry_i++)
        {
        eAtRet ret = cAtOk;
        AtPidTableEntry entry = AtPidTableEntryGet(PidTable(), entryList[entry_i] - 1);

        ret |= EthTypeSet(entry, ethType);
        ret |= PidSet(entry, pidValue);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot set PID and Ethernet type at entry %u, ret = %s\r\n",
                     entryList[entry_i], AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static eBool EntryEnable(char argc, char **argv,
                         eAtRet (*Enable)(AtPidTableEntry self, eBool enable),
                         eBool enable)
    {
    uint32 entry_i, *entryList;
    uint32 numEntries;
    eBool success = cAtTrue;
	AtUnused(argc);

    /* Get list of entries */
    entryList = EntryListFromString(argv[0], &numEntries);
    if (numEntries == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No entry, the ID list may be wrong or all of them are invalid\r\n");
        return cAtTrue;
        }

    for (entry_i = 0; entry_i < numEntries; entry_i++)
        {
        AtPidTableEntry entry = AtPidTableEntryGet(PidTable(), entryList[entry_i] - 1);
        eAtRet ret = Enable(entry, enable);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot set enable/disable entry %u, ret = %s\r\n",
                     entryList[entry_i], AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static eAtRet AtPidTableEntryEnable(AtPidTableEntry self, eBool enable)
    {
    eAtRet ret = cAtOk;

    ret |= AtPidTableEntryTdmToPsnEnable(self, enable);
    ret |= AtPidTableEntryPsnToTdmEnable(self, enable);

    return ret;
    }

eBool CmdPppTablePidSet(char argc, char **argv)
    {
    return AttributeSet(argc, argv, AtPidTableEntryPidSet);
    }

eBool CmdPppTablePidTdmToPsnSet(char argc, char **argv)
    {
    return AttributeSet(argc, argv, AtPidTableEntryTdmToPsnPidSet);
    }

eBool CmdPppTablePidPsnToTdmSet(char argc, char **argv)
    {
    return AttributeSet(argc, argv, AtPidTableEntryPsnToTdmPidSet);
    }

eBool CmdPppTableEthTypeSet(char argc, char **argv)
    {
    return AttributeSet(argc, argv, AtPidTableEntryEthTypeSet);
    }

eBool CmdPppTableEthTypeTdmToPsnSet(char argc, char **argv)
    {
    return AttributeSet(argc, argv, AtPidTableEntryTdmToPsnEthTypeSet);
    }

eBool CmdPppTableEthTypePsnToTdmSet(char argc, char **argv)
    {
    return AttributeSet(argc, argv, AtPidTableEntryPsnToTdmEthTypeSet);
    }

eBool CmdPppTableEntrySet(char argc, char **argv)
    {
    return EntrySet(argc, argv, AtPidTableEntryEthTypeSet, AtPidTableEntryPidSet);
    }

eBool CmdPppTableEntryTdmToPsnSet(char argc, char **argv)
    {
    return EntrySet(argc, argv, AtPidTableEntryTdmToPsnEthTypeSet, AtPidTableEntryTdmToPsnPidSet);
    }

eBool CmdPppTableEntryPsnToTdmSet(char argc, char **argv)
    {
    return EntrySet(argc, argv, AtPidTableEntryPsnToTdmEthTypeSet, AtPidTableEntryPsnToTdmPidSet);
    }

eBool CmdPppTableEntryEnableSet(char argc, char **argv)
    {
    return EntryEnable(argc, argv, AtPidTableEntryEnable, cAtTrue);
    }

eBool CmdPppTableEntryDisableSet(char argc, char **argv)
    {
    return EntryEnable(argc, argv, AtPidTableEntryEnable, cAtFalse);
    }

eBool CmdPppTableEntryEnableTdmToPsnSet(char argc, char **argv)
    {
    return EntryEnable(argc, argv, AtPidTableEntryTdmToPsnEnable, cAtTrue);
    }

eBool CmdPppTableEntryEnablePsnToTdmSet(char argc, char **argv)
    {
    return EntryEnable(argc, argv, AtPidTableEntryPsnToTdmEnable, cAtTrue);
    }

eBool CmdPppTableEntryDisablePsnToTdmSet(char argc, char **argv)
    {
    return EntryEnable(argc, argv, AtPidTableEntryPsnToTdmEnable, cAtFalse);
    }

eBool CmdPppTableEntryDisableTdmToPsnSet(char argc, char **argv)
    {
    return EntryEnable(argc, argv, AtPidTableEntryTdmToPsnEnable, cAtFalse);
    }

eBool CmdPppTableEntryInit(char argc, char **argv)
    {
    eAtRet ret = AtPidTableInit(PidTable());
	AtUnused(argv);
	AtUnused(argc);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot initialize PID table, ret = %s\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdPppTableEntryGet(char argc, char **argv)
    {
    uint32 entry_i, *entryList = NULL;
    uint32 numEntries;
    tTab *tabPtr;
    char buffer[16];
    const char *heading[] = {"entry",
                             "pid(psn2tdm)", "ethtype(psn2tdm)", "enable(psn2tdm)",
                             "pid(tdm2psn)", "ethtype(tdm2psn)", "enable(tdm2psn)"};

    if (argc >= 1)
        {
        /* Get list of entries */
        entryList = EntryListFromString(argv[0], &numEntries);
        if (numEntries == 0)
            {
            AtPrintc(cSevWarning, "WARNING: No entry, the ID list may be wrong or all of them are invalid\r\n");
            return cAtTrue;
            }
        }
    else
        numEntries = AtPidTableMaxNumEntries(PidTable());


    /* Make table */
    tabPtr = TableAlloc(numEntries, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        return cAtFalse;
        }

    /* Show table */
    for (entry_i = 0; entry_i < numEntries; entry_i++)
        {
        uint32 entryId = (entryList) ? entryList[entry_i] - 1 : entry_i;
        AtPidTableEntry entry = AtPidTableEntryGet(PidTable(), entryId);

        AtSprintf(buffer, "%u", entryId + 1);
        StrToCell(tabPtr, entry_i, 0, buffer);

        AtSprintf(buffer, "0x%04x", AtPidTableEntryPsnToTdmPidGet(entry));
        StrToCell(tabPtr, entry_i, 1, buffer);

        AtSprintf(buffer, "0x%04x", AtPidTableEntryPsnToTdmEthTypeGet(entry));
        StrToCell(tabPtr, entry_i, 2, buffer);

        StrToCell(tabPtr, entry_i, 3, CliBoolToString(AtPidTableEntryPsnToTdmIsEnabled(entry)));

        AtSprintf(buffer, "0x%04x", AtPidTableEntryTdmToPsnPidGet(entry));
        StrToCell(tabPtr, entry_i, 4, buffer);

        AtSprintf(buffer, "0x%04x", AtPidTableEntryTdmToPsnEthTypeGet(entry));
        StrToCell(tabPtr, entry_i, 5, buffer);

        StrToCell(tabPtr, entry_i, 6, CliBoolToString(AtPidTableEntryTdmToPsnIsEnabled(entry)));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

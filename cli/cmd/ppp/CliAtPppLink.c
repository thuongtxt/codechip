/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : CliAtPppLink.c
 *
 * Created Date: Nov 23, 2012
 *
 * Description : PPP link CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtPppLink.h"
#include "AtHdlcLink.h"
#include "AtHdlcChannel.h"
#include "CliPppOamProcess.h"
#include "CliAtModulePpp.h"
#include "../eth/CliAtModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mAttributeSetFunc(func) ((LinkAttributeSetFunc)(func))
#define mWarnAndReturnIfCannotGetAnyLink(_numLinks)\
if (_numLinks == 0)\
{\
AtPrintc(cSevCritical, "ERROR: Link(s) %s does not exist, invalid encapsulation type or invalid id, expected 1,2...\r\n", argv[0]);\
return cAtFalse;\
}\

/*--------------------------- Local typedefs ---------------------------------*/
typedef eAtRet (*LinkAttributeSetFunc)(AtPppLink self, uint32 value);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char * cCmdPppLinkPhaseStr[] = {"dead", "establish", "network", "networkactive"};
static const eAtPppLinkPhase cCmdPppLinkPhaseVal[] = {cAtPppLinkPhaseDead, cAtPppLinkPhaseEstablish,
                                               cAtPppLinkPhaseEnterNetwork, cAtPppLinkPhaseNetworkActive};

static uint8 m_oamBuffer[1024];

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet CountersShow(AtPppLink *linksList, uint32 numLinks, eAtHistoryReadingMode readMode, eBool silent)
    {
    eAtRet ret;
    uint32 i;
    uint8  column;
    tTab *tabPtr = NULL;
    tAtPppLinkCounters linkCounters;
    char buff[50];
    const char *heading[] = {"LinkId", "txBytes", "txGoodBytes", "txGoodPkt", "txFragment", "txPlain",
                            "txOam", "txDiscardPkt", "txDiscardByte", "rxBytes, ""rxGoodBytes", "rxGoodPkt",
                            "rxFragment", "rxPlain", "rxOam", "rxDiscardPkt", "rxDiscardByte"};
    ret = cAtOk;

    /* No link to show */
    if (numLinks == 0)
        {
        AtPrintc(cSevInfo, "INFO: No link to show counters\r\n");
        return cAtOk;
        }

    /* Create table with titles */
    if (!silent)
        {
        tabPtr = TableAlloc(numLinks, mCount(heading), heading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "Cannot alloc table\r\n");
            return cAtFalse;
            }
        }

    for (i = 0; i < numLinks; i++)
        {
        eBool isValidCounter;
        /* Get counter */
        AtOsalMemInit(&linkCounters, 0, sizeof(tAtPppLinkCounters));
        if (readMode == cAtHistoryReadingModeReadToClear)
            ret = AtChannelAllCountersClear((AtChannel)linksList[i], &linkCounters);
        else
            ret = AtChannelAllCountersGet((AtChannel)linksList[i], &linkCounters);

        if (ret != cAtOk)
            mWarnNotReturnCliIfNotOk(ret, linksList[i], "Can not get all counters of link");

        column = 0;

        /* Print ID */
        AtSprintf(buff, "%s", AtChannelIdString((AtChannel)linksList[i]));
        StrToCell(tabPtr, i, column++, buff);

        isValidCounter = (ret == cAtOk) ? cAtTrue : cAtFalse;
        CliCounterPrintToCell(tabPtr, i, column++, linkCounters.txByte       , cAtCounterTypeGood    , isValidCounter);
        CliCounterPrintToCell(tabPtr, i, column++, linkCounters.txGoodByte   , cAtCounterTypeGood    , isValidCounter);
        CliCounterPrintToCell(tabPtr, i, column++, linkCounters.txGoodPkt    , cAtCounterTypeGood    , isValidCounter);
        CliCounterPrintToCell(tabPtr, i, column++, linkCounters.txFragment   , cAtCounterTypeGood    , isValidCounter);
        CliCounterPrintToCell(tabPtr, i, column++, linkCounters.txPlain      , cAtCounterTypeGood    , isValidCounter);
        CliCounterPrintToCell(tabPtr, i, column++, linkCounters.txOam        , cAtCounterTypeNeutral , isValidCounter);
        CliCounterPrintToCell(tabPtr, i, column++, linkCounters.txDiscardPkt , cAtCounterTypeError   , isValidCounter);
        CliCounterPrintToCell(tabPtr, i, column++, linkCounters.txDiscardByte , cAtCounterTypeError  , isValidCounter);
        CliCounterPrintToCell(tabPtr, i, column++, linkCounters.rxByte       , cAtCounterTypeGood    , isValidCounter);
        CliCounterPrintToCell(tabPtr, i, column++, linkCounters.rxGoodByte   , cAtCounterTypeGood    , isValidCounter);
        CliCounterPrintToCell(tabPtr, i, column++, linkCounters.rxGoodPkt    , cAtCounterTypeGood    , isValidCounter);
        CliCounterPrintToCell(tabPtr, i, column++, linkCounters.rxFragment   , cAtCounterTypeGood    , isValidCounter);
        CliCounterPrintToCell(tabPtr, i, column++, linkCounters.rxPlain      , cAtCounterTypeGood    , isValidCounter);
        CliCounterPrintToCell(tabPtr, i, column++, linkCounters.rxOam        , cAtCounterTypeNeutral , isValidCounter);
        CliCounterPrintToCell(tabPtr, i, column++, linkCounters.rxDiscardPkt , cAtCounterTypeError   , isValidCounter);
        CliCounterPrintToCell(tabPtr, i, column++, linkCounters.rxDiscardByte , cAtCounterTypeError  , isValidCounter);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtOk;
    }

static eBool AttributeSet(char argc, char **argv, LinkAttributeSetFunc attributeSet, uint32 value)
    {
    AtPppLink *linksList;
    uint32 numLinks;
    uint32 bufferSize, link_i;
    eBool success = cAtTrue;
	AtUnused(argc);

    /* Get list of link */
    linksList = (AtPppLink *) ((void**) CliSharedChannelListGet(&bufferSize));
    numLinks = CliPppLinkIdListFromString(argv[0], linksList, bufferSize);
    mWarnAndReturnIfCannotGetAnyLink(numLinks)

    for (link_i = 0; link_i < numLinks; link_i++)
        {
        eAtRet ret = attributeSet(linksList[link_i], value);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot change attribute on link %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)linksList[link_i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

/* show counter link */
static void OamReceived(AtChannel channel, uint8 *data, uint32 length)
    {
    CliShowOamPacketInTable((uint16)(AtChannelIdGet(channel) + 1), data, length);
    }

static eBool Enable(char argc, char **argv, eBool enable)
    {
    uint8 i;
    AtPppLink *linksList;
    eBool success = cAtTrue;
    uint32 bufferSize;
    uint32 numLinks;
    eAtRet ret;
	AtUnused(argc);

    /* Get list of link */
    linksList = (AtPppLink *) ((void**) CliSharedChannelListGet(&bufferSize));
    numLinks = CliPppLinkIdListFromString(argv[0], linksList, bufferSize);
    mWarnAndReturnIfCannotGetAnyLink(numLinks)

    for (i = 0; i < numLinks; i++)
        {
        ret = AtChannelEnable((AtChannel)linksList[i], enable);
        if (ret != cAtOk)
            {
            AtPrintc(cSevWarning, "Link %s cannot be enabled/disabled, ret = %s\r\n", CliChannelIdStringGet((AtChannel)linksList[i]), AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static uint32 StringToHex(const char *pStrValue, uint8 *buffer, uint32 bufferSize)
    {
    uint32  i, length;
    char c[3];
    uint8 *p;

    /* Prevent buffer overflow */
    length = AtStrlen(pStrValue);
    if (length > bufferSize * 2)
        length = bufferSize * 2;

    c[2] = 0;
    i = 0;
    p = buffer;

    /* Parse string to hex values */
    while(i < length)
        {
        if (!AtIsHexChar(pStrValue[i]))
            return 0;

        c[0] = pStrValue[i++];
        if (i == length) /* over last character */
            {
            c[1] = '0';
            i++;
            }
        else
            {
            if (!AtIsHexChar(pStrValue[i]))
                return 0;

            c[1] = pStrValue[i++];
            }

        *p = (uint8)AtStrtoul(c, NULL, 16);
        p++;
        }

    return (uint32)(p - buffer);
    }

uint32 CliPppLinkIdListFromString(char *idList, AtPppLink *links, uint32 bufferSize)
    {
    uint32 idBufferSize;
    uint32 link_i;
    uint32 numValidLink = 0;
    uint32 *idBuffer = CliSharedIdBufferGet(&idBufferSize);
    uint32 numberLinks = CliIdListFromString(idList, idBuffer, idBufferSize);

    for (link_i = 0; link_i < numberLinks; link_i++)
        {
        AtHdlcChannel  hdlcChannel;
        AtPppLink link;

        hdlcChannel = (AtHdlcChannel)AtModuleEncapChannelGet(CliAtEncapModule(), (uint16)(CliId2DriverId(idBuffer[link_i])));
        if (hdlcChannel == NULL)
            {
            AtPrintc(cSevWarning, "Channel = %u, maybe not created\r\n", idBuffer[link_i]);
            continue;
            }

        if (AtHdlcChannelFrameTypeGet(hdlcChannel) != cAtHdlcFrmPpp)
            {
            AtPrintc(cSevWarning, "Channel %u invalid encapsulation type\r\n", idBuffer[link_i]);
            continue;
            }

        link = (AtPppLink)AtHdlcChannelHdlcLinkGet(hdlcChannel) ;
        if (link == NULL)
            {
            AtPrintc(cSevWarning,"Link is not exits on ppp channel %u\r\n", idBuffer[link_i]);
            continue;
            }

        if (numValidLink >= bufferSize)
            break;

        links[numValidLink++] = link;
        }

    return numValidLink;
    }

eBool CmdPppLinkPhaseSet(char argc, char **argv)
    {
    eAtRet  ret = cAtOk;
    eBool   blResult;
    eAtPppLinkPhase phase = cAtPppLinkPhaseUnknown;
    AtPppLink *linksList;
    uint32  numLinks;
    uint32  bufferSize, wIndex;
	AtUnused(argc);

    /* Get list of link */
    linksList = (AtPppLink *) ((void**) CliSharedChannelListGet(&bufferSize));
    numLinks = CliPppLinkIdListFromString(argv[0], linksList, bufferSize);
    mWarnAndReturnIfCannotGetAnyLink(numLinks)

    mAtStrToEnum(cCmdPppLinkPhaseStr,
                 cCmdPppLinkPhaseVal,
                 argv[1],
                 phase,
                 blResult);
    if(!blResult)
        {
        AtPrintc(cSevCritical, "ERROR: Link phase invalid\r\n");
        return cAtFalse;
        }

    for (wIndex = 0; wIndex < numLinks; wIndex++)
        {
        ret = AtPppLinkPhaseSet(linksList[wIndex], phase);
        if (ret != cAtOk)
            {
            mWarnNotReturnCliIfNotOk(ret, linksList[wIndex], "Can not set link phase");
            return cAtFalse;
            }
        }

    return cAtTrue;
    }

eBool CmdPppLinkCounterShow(char argc, char **argv)
    {
    eAtRet ret = cAtOk;
    uint32 numLinks;
    AtPppLink *linksList;
    uint32 bufferSize;
    eBool silent = cAtFalse;
    eAtHistoryReadingMode readingMode;

	AtUnused(argc);

	if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    /* Get list of link */
    linksList = (AtPppLink *) ((void**) CliSharedChannelListGet(&bufferSize));
    numLinks = CliPppLinkIdListFromString(argv[0], linksList, bufferSize);
    mWarnAndReturnIfCannotGetAnyLink(numLinks)

    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    if (argc > 2)
        silent = CliHistoryReadingShouldSilent(readingMode, argv[2]);

    ret = CountersShow(linksList, numLinks, readingMode, silent);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "Can not show counters of links\n");
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdPppOamLcpMruSend(char argc, char **argv)
    {
    eAtRet ret = cAtOk;
    uint16 mruVal;
    uint32 numLinks, i;
    AtPppLink *linksList;
    uint32 bufferSize;
    uint8 dataLength;
    uint16 lcpLength;
    uint8 *lcpContent;
    uint8 *data;
    eBool success = cAtTrue;
	AtUnused(argc);

    /* Get list of link */
    linksList = (AtPppLink *) ((void**) CliSharedChannelListGet(&bufferSize));
    numLinks = CliPppLinkIdListFromString(argv[0], linksList, bufferSize);
    mWarnAndReturnIfCannotGetAnyLink(numLinks)

    /* Get MRU value */
    mruVal = (uint16)AtStrToDw(argv[1]);
    data = LcpMruOption(mruVal, &dataLength);
    lcpContent = LcpBuild(1, data, dataLength, &lcpLength);
    if (lcpContent == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: OAM packet length is too large, the limit is %u bytes\r\n", PppOamMaxPacketLength());
        return cAtFalse;
        }

    for (i = 0; i < numLinks; i++)
        {
        ret = AtHdlcLinkOamSend((AtHdlcLink)linksList[i], lcpContent, lcpLength);
        if (ret != cAtOk)
            {
            mWarnNotReturnCliIfNotOk(ret, linksList[i], "Can not send OAM package");
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdPppOamLcpEchoRequestSend(char argc, char **argv)
    {
    eAtRet ret = cAtOk;
    char *string;
    uint32 magicNumber;
    uint32 numLinks, i;
    AtPppLink *linksList;
    uint32 bufferSize;
    uint8 *lcpContent;
    uint16 lcpLen, echoRequestLen;
    uint8 *data;
    eBool success = cAtTrue;
	AtUnused(argc);

    /* Get list of link */
    linksList = (AtPppLink *) ((void**) CliSharedChannelListGet(&bufferSize));
    numLinks = CliPppLinkIdListFromString(argv[0], linksList, bufferSize);
    mWarnAndReturnIfCannotGetAnyLink(numLinks)

    /* Get input and build LCP packet */
    magicNumber = (uint32)AtStrToDw(argv[1]);
    string = argv[2];
    data = LcpEchoRequest(magicNumber, (uint8 *)string, (uint16)AtStrlen(string), &echoRequestLen);
    if (data == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: OAM packet length is too large, the limit is %u bytes\r\n", PppOamMaxPacketLength());
        return cAtFalse;
        }

    lcpContent = LcpBuild(9, data, echoRequestLen, &lcpLen);
    if (lcpContent == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: OAM packet length is too large, the limit is %u bytes\r\n", PppOamMaxPacketLength());
        return cAtFalse;
        }

    /* Send */
    for (i = 0; i < numLinks; i++)
        {
        ret = AtHdlcLinkOamSend((AtHdlcLink)linksList[i], lcpContent, lcpLen);
        if (ret != cAtOk)
            {
            mWarnNotReturnCliIfNotOk(ret, linksList[i], "Can not send OAM package");
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdPppOamReceive(char argc, char **argv)
    {
    uint32 numLinks, i;
    uint32 bufferSize;
    AtPppLink *linksList;
    uint32 length;
	AtUnused(argc);

    /* Initialize memory */
    AtOsalMemInit(m_oamBuffer, 0x0, sizeof(m_oamBuffer));

    /* Get list of link */
    linksList = (AtPppLink *) ((void**) CliSharedChannelListGet(&bufferSize));
    numLinks = CliPppLinkIdListFromString(argv[0], linksList, bufferSize);
    mWarnAndReturnIfCannotGetAnyLink(numLinks)

    for (i = 0; i < numLinks; i++)
        {
        AtOsalMemInit(m_oamBuffer, 0x0, sizeof(m_oamBuffer));
        length = AtHdlcLinkOamReceive((AtHdlcLink)linksList[i], m_oamBuffer, sizeof(m_oamBuffer));

        if (length == 0)
            AtPrintc(cSevInfo, "There is no OAM packet\n");
        else
            PppPacketDump((uint16)AtChannelIdGet((AtChannel)linksList[i]), m_oamBuffer, length);
        }

    return cAtTrue;
    }

eBool CmdPppOamAutoCapturingEnable(char argc, char **argv)
    {
    eAtRet  ret;
    AtPppLink *linksList;
    uint32 numLinks, i, bufferSize;
    eBool captureEnabled = cAtFalse;
    eBool convertSuccess;
    static tAtChannelEventListener oamListener;
	AtUnused(argc);

    ret = cAtOk;

    AtOsalMemInit(&oamListener, 0, sizeof(oamListener));
    oamListener.OamReceived      = OamReceived;

    /* Get list of links */
    linksList = (AtPppLink *) ((void**) CliSharedChannelListGet(&bufferSize));
    numLinks = CliPppLinkIdListFromString(argv[0], linksList, bufferSize);
    mWarnAndReturnIfCannotGetAnyLink(numLinks)

    /* Enabling */
    mAtStrToBool(argv[1], captureEnabled, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enabling mode, expected en/dis\r\n");
        return cAtFalse;
        }

    /* Handle all input links */
    for (i = 0; i < numLinks; i++)
        {
        /* Register OAM listener */
        if (captureEnabled)
            {
            ret = AtChannelEventListenerAdd((AtChannel)linksList[i], &oamListener);
            if (ret != cAtOk)
                mWarnNotReturnCliIfNotOk(ret, linksList[i], "Can not add Event listener for link");
            }
        else
            {
            ret = AtChannelEventListenerRemove((AtChannel)linksList[i], &oamListener);
            if (ret != cAtOk)
                mWarnNotReturnCliIfNotOk(ret, linksList[i], "Can not remove Event listener for link");
            }
        }

    return cAtTrue;
    }

eBool CmdPppOamIpcpIpAddressSend(char argc, char **argv)
    {
    eAtRet ret = cAtOk;
    eBool success = cAtTrue;
    AtPppLink *linksList;
    uint32 numLinks, i;
    uint32 bufferSize;
    uint8 *ipcpContent;
    uint16 ipcpLen = 0;
    uint8 dataLength;
    uint8 ipAddr[4];
    uint8 *data;
	AtUnused(argc);

    /* Get list of link */
    linksList = (AtPppLink *) ((void**) CliSharedChannelListGet(&bufferSize));
    numLinks = CliPppLinkIdListFromString(argv[0], linksList, bufferSize);
    mWarnAndReturnIfCannotGetAnyLink(numLinks)

    /* Get input and build IPCP packet */
    if (!CliAtModuleEthIpv4AddrStr2Byte(argv[1], ipAddr))
        {
        AtPrintc(cSevCritical, "Invalid IPv4\n");
        return cAtFalse;
        }

    data = IpcpIpAddress(ipAddr, &dataLength);
    ipcpContent = IpcpBuild(1, data, dataLength, &ipcpLen);
    if (ipcpContent == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: OAM packet length is too large, the limit is %u bytes\r\n", PppOamMaxPacketLength());
        return cAtFalse;
        }

    for (i = 0; i < numLinks; i++)
        {
        ret = AtHdlcLinkOamSend((AtHdlcLink)linksList[i], ipcpContent, ipcpLen);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "Link %s can not send Oam, ret = %s\r\n", AtChannelTypeString((AtChannel)linksList[i]), AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdPppLinkEnable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue);
    }

eBool CmdPppLinkDisable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse);
    }

eBool CmdPppOamLcpPfcSend(char argc, char **argv)
    {
    eAtRet ret = cAtOk;
    eBool success = cAtTrue;
    AtPppLink *linksList;
    uint32 numLinks, i;
    uint32 bufferSize;
    uint8 dataLength;
    uint16 lcpLength;
    uint8 *lcpContent;
    uint8 *data;
	AtUnused(argc);

    /* Get list of link */
    linksList = (AtPppLink *) ((void**) CliSharedChannelListGet(&bufferSize));
    numLinks = CliPppLinkIdListFromString(argv[0], linksList, bufferSize);
    mWarnAndReturnIfCannotGetAnyLink(numLinks)

    data = LcpPfcOption(&dataLength);
    lcpContent = LcpBuild(1, data, dataLength, &lcpLength);
    if (lcpContent == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: OAM packet length is too large, the limit is %u bytes\r\n", PppOamMaxPacketLength());
        return cAtFalse;
        }

    for (i = 0; i < numLinks; i++)
        {
        ret = AtHdlcLinkOamSend((AtHdlcLink)linksList[i], lcpContent, lcpLength);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "Link %s can not send Oam, ret = %s\r\n", AtChannelTypeString((AtChannel)linksList[i]), AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdPppOamLcpTerminateRequestSend(char argc, char **argv)
    {
    eAtRet ret = cAtOk;
    AtPppLink *linksList;
    uint32 numLinks, i;
    uint32 bufferSize;
    uint16 lcpLength;
    uint8 *lcpContent;
    eBool success = cAtTrue;
	AtUnused(argc);

    /* Get list of link */
    linksList = (AtPppLink *) ((void**) CliSharedChannelListGet(&bufferSize));
    numLinks = CliPppLinkIdListFromString(argv[0], linksList, bufferSize);
    mWarnAndReturnIfCannotGetAnyLink(numLinks)

    lcpContent = LcpBuild(5, NULL, 0, &lcpLength);
    if (lcpContent == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: OAM packet length is too large, the limit is %u bytes\r\n", PppOamMaxPacketLength());
        return cAtFalse;
        }

    for (i = 0; i < numLinks; i++)
        {
        ret = AtHdlcLinkOamSend((AtHdlcLink)linksList[i], lcpContent, lcpLength);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "Link %s can not send Oam, ret = %s\r\n", AtChannelTypeString((AtChannel)linksList[i]), AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdPppPktSend(char argc, char **argv)
    {
    eAtRet ret = cAtOk;
    AtPppLink *linksList;
    uint32 numLinks, i;
    uint32 bufferSize;
    uint16 pppLength;
    uint8 *content;
    eBool success = cAtTrue;
	AtUnused(argc);

    /* Get list of link */
    linksList = (AtPppLink *) ((void**) CliSharedChannelListGet(&bufferSize));
    numLinks = CliPppLinkIdListFromString(argv[0], linksList, bufferSize);
    mWarnAndReturnIfCannotGetAnyLink(numLinks)

    content = PppBuild(&pppLength);

    for (i = 0; i < numLinks; i++)
        {
        ret = AtHdlcLinkOamSend((AtHdlcLink)linksList[i], content, pppLength);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "Link %s can not send Oam, ret = %s\r\n", AtChannelTypeString((AtChannel)linksList[i]), AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtPppLinkDebug(char argc, char **argv)
    {
    uint32   numLinks;
    AtPppLink *linksList;
    uint32   bufferSize;
    uint32   wIndex;
	AtUnused(argc);

    /* parse id link */
    linksList = (AtPppLink *) ((void**) CliSharedChannelListGet(&bufferSize));
    numLinks = CliPppLinkIdListFromString(argv[0], linksList, bufferSize);
    mWarnAndReturnIfCannotGetAnyLink(numLinks)

    for (wIndex = 0; wIndex < numLinks; wIndex++)
        AtChannelDebug((AtChannel)linksList[wIndex]);

    return cAtTrue;
    }

eBool CmdPppLinkOamRawPktSend(char argc, char **argv)
    {
    AtPppLink *linksList;
    uint32 numLinks, i;
    uint32 bufferSize;
    uint32 numBytes;
    eAtRet ret = cAtOk;
    eBool success = cAtTrue;
	AtUnused(argc);

    /* Initialize memory */
    AtOsalMemInit(m_oamBuffer, 0x0, sizeof(m_oamBuffer));

    /* Get list of link */
    linksList = (AtPppLink *) ((void**) CliSharedChannelListGet(&bufferSize));
    numLinks = CliPppLinkIdListFromString(argv[0], linksList, bufferSize);
    mWarnAndReturnIfCannotGetAnyLink(numLinks)

    numBytes = StringToHex(argv[1], m_oamBuffer, sizeof(m_oamBuffer));
    if (numBytes == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No byte to send, the input data may be invalid\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numLinks; i++)
        {
        /* Send this OAM */
        ret = AtHdlcLinkOamSend((AtHdlcLink)linksList[i], m_oamBuffer, numBytes);
        if (ret != cAtOk)
            {
            mWarnNotReturnCliIfNotOk(ret, linksList[i], "Can not send OAM package");
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdPppLinkProtocolTxCompressEnable(char argc, char **argv)
    {
    return AttributeSet(argc, argv, mAttributeSetFunc(AtPppLinkTxProtocolFieldCompress), cAtTrue);
    }

eBool CmdPppLinkProtocolTxCompressDisable(char argc, char **argv)
    {
    return AttributeSet(argc, argv, mAttributeSetFunc(AtPppLinkTxProtocolFieldCompress), cAtFalse);
    }

eBool CmdPppLinkBcpLanFcsFieldInsertionEnable(char argc, char **argv)
    {
    return AttributeSet(argc, argv, mAttributeSetFunc(AtPppLinkBcpLanFcsInsertionEnable), cAtTrue);
    }

eBool CmdPppLinkBcpLanFcsFieldInsertionDisable(char argc, char **argv)
    {
    return AttributeSet(argc, argv, mAttributeSetFunc(AtPppLinkBcpLanFcsInsertionEnable), cAtFalse);
    }

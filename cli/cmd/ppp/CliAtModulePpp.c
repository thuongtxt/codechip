/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2102 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : atclimoduleppp.c
 *
 * Created Date: Oct 9, 2012
 *
 * Description : module PPP CLIs
 *
 * Notes       :
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtModulePpp.h"
#include "AtEncapBundle.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModulePpp PppModule(void)
    {
    return (AtModulePpp)AtDeviceModuleGet(CliDevice(), cAtModulePpp);
    }

static AtModuleEncap EncapModule(void)
    {
    return (AtModuleEncap)AtDeviceModuleGet(CliDevice(), cAtModuleEncap);
    }

static AtMpBundle MpBundleCreate(uint32 bundleId)
    {
    /* For old design, try creating object in ppp module */
    AtMpBundle mpBundle = AtModulePppMpBundleCreate(PppModule(), CliId2DriverId(bundleId));
    if (mpBundle)
        return mpBundle;

    return AtModuleEncapMpBundleCreate(EncapModule(), CliId2DriverId(bundleId));
    }

static eAtRet MpBundleDelete(uint32 bundleId)
    {
    uint32 driverBundleId = CliId2DriverId(bundleId);
    uint32 type;
    AtHdlcBundle bundle;

    /* For old design, try deleting in ppp module */
    eAtRet ret = AtModulePppMpBundleDelete(PppModule(), driverBundleId);
    if (ret == cAtOk)
        return ret;

    bundle = AtModuleEncapBundleGet(EncapModule(), driverBundleId);
    if (bundle == NULL)
        {
        AtPrintc(cSevCritical, "Bundle = %u is not exist\r\n", bundleId);
        return cAtErrorObjectNotExist;
        }

    type = AtEncapBundleTypeGet((AtEncapBundle) bundle);
    if (type != cAtEncapBundleTypeMlppp)
        {
        AtPrintc(cSevCritical, "Bundle = %u is not MLPPP bundle\r\n", bundleId);
        return cAtErrorRsrcNoAvail;
        }

    return AtModuleEncapBundleDelete(EncapModule(), driverBundleId);
    }

AtMpBundle CliMpBundleGet(uint32 bundleId)
    {
    uint32 driverId = CliId2DriverId(bundleId);

    /* For old design, try getting object in ppp module */
    AtMpBundle mpBundle = (AtMpBundle)AtModulePppMpBundleGet(PppModule(), driverId);
    if (mpBundle)
        return mpBundle;

    mpBundle = (AtMpBundle)AtModuleEncapBundleGet(EncapModule(), driverId);
    if (mpBundle == NULL)
        return NULL;

    if (AtEncapBundleTypeGet((AtEncapBundle) mpBundle) != cAtEncapBundleTypeMlppp)
        {
        AtPrintc(cSevCritical, "Bundle = %u is not MLPPP bundle\r\n", bundleId);
        return NULL;
        }

    return mpBundle;
    }

/* Create bundle */
eBool CmdPppBundleCreate(char argc, char **argv)
    {
    uint32      numBundle, wIndex;
    uint32      *pBundleList, bufferSize;
	AtUnused(argc);

    /* Get list of link */
    pBundleList = CliSharedIdBufferGet(&bufferSize);
    numBundle = CliIdListFromString(argv[0], pBundleList, bufferSize);
    if (numBundle == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Bundle parameter invalid, expect 1,2..\r\n");
        return cAtFalse;
        }

    /* Call API */
    for (wIndex = 0; wIndex < numBundle; wIndex++)
        {
        if (MpBundleCreate(pBundleList[wIndex]) == NULL)
            AtPrintc(cSevCritical, "ERROR: Bundle %u can not be created\n", pBundleList[wIndex]);
        }

    return cAtTrue;
    }

/* Delete bundle */
eBool CmdPppBundleDelete(char argc, char **argv)
    {
    eAtRet      ret;
    uint32      numBundle, wIndex;
    uint32      *pBundleList, bufferSize;
	AtUnused(argc);

    /* Get list of link */
    pBundleList = CliSharedIdBufferGet(&bufferSize);
    numBundle = CliIdListFromString(argv[0], pBundleList, bufferSize);
    if (numBundle == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Bundle parameter invalid, expect 1,2..\r\n");
        return cAtFalse;
        }

    /* Call API */
    for (wIndex = 0; wIndex < numBundle; wIndex++)
        {
        ret = MpBundleDelete(pBundleList[wIndex]);
        if (ret != cAtOk)
            AtPrintc(cSevCritical, "Bundle %u can not be created\n", pBundleList[wIndex]);
        }

    return cAtTrue;
    }

/* Enable interrupt */
eBool CmdPppInterruptEnable(char argc, char **argv)
    {
    eAtRet ret = AtModuleInterruptEnable((AtModule)PppModule(), cAtTrue);
	AtUnused(argv);
	AtUnused(argc);
    if (ret != cAtOk)
        AtPrintc(cSevCritical, "Ppp interrupt can not be enabled\n");

    return cAtTrue;
    }

/* Disable interrupt */
eBool CmdPppInterruptDisable(char argc, char **argv)
    {
    eAtRet ret = AtModuleInterruptEnable((AtModule)PppModule(), cAtFalse);
	AtUnused(argv);
	AtUnused(argc);
    if (ret != cAtOk)
        AtPrintc(cSevCritical, "Ppp interrupt can not be disabled\n");

    return cAtTrue;
    }

eBool CmdModulePppDebug(char argc, char **argv)
    {
    eAtRet ret = AtModuleDebug((AtModule)PppModule());
	AtUnused(argv);
	AtUnused(argc);
    if (ret != cAtOk)
        AtPrintc(cSevCritical, "ERROR: Module may not exist\n");

    return cAtTrue;
    }

eBool CmdModulePppHierarchyShow(char argc, char **argv)
    {
	AtUnused(argv);
	AtUnused(argc);
    return cAtTrue;
    }

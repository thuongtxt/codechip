/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : AtPtpPacketTest.c
 *
 * Created Date: Jul 16, 2018
 *
 * Description : PTP packet analyzer selftest
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtPacket.h"
#include "AtEthPacket.h"
#include "AtPtpPacket.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define PTP_TYPE_SYNC       0x00
#define PTP_TYPE_DELAY_REQ  0x01
#define PTP_TYPE_FOLLOW_UP  0x08
#define PTP_TYPE_DELAY_RESP 0x09
#define PTP_TYPE_ANNOUNCE   0x0B

#define PTP_CTRL_SYNC       0x00
#define PTP_CTRL_DELAY_REQ  0x01
#define PTP_CTRL_FOLLOW_UP  0x02
#define PTP_CTRL_DELAY_RESP 0x03

#define UUID            0x01, 0x02, 0x03, 0x04, 0x05
#define SEQUENCE_ID     0x06, 0x07
#define TIMESTAMP(s, ns)  0x##s##1, 0x##s##2, 0x##s##3, 0x##s##4, 0x##s##5, 0x##s##6, 0x##ns##0, 0x##ns##1, 0x##ns##2, 0x##ns##3

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void AtPtpPacketTest(void);

/*--------------------------- Implementation ---------------------------------*/

static uint8 *SyncPacketOverUdp(uint32 *length)
    {
    static uint8 mesgBuffer[]=
        {
         0x01, 0x00, 0x5E, 0x00, 0x01, 0x81,
         0xE4, 0xAF, 0xA1, 0x36, 0x00, 0x10,

         /* Ethernet Type */
         0x08, 0x00,

         /* IPv4 */
         0x45, 0x00, 0x00, 0x98, 0x00, 0x5D,
         0x40, 0x00, 0x01, 0x11, 0x29, 0x68,
         0xC0, 0xA8, 0x01, 0x01, 0xE0, 0x00, 0x01, 0x81,

         /* UDP */
         0x01, 0x3F, 0x01, 0x3F, 0x00, 0x84, 0xC0, 0x7B,

         /* PTP */
         PTP_TYPE_SYNC, 0x02,
         0x00, 0x2C, /* Length */
         0x00, 0x00, /* Domain, Reserved */
         0x00, 0x00, /* Flags */
         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* correctionField */
         0x00, 0x00, 0x00, 0x00, /* Reserved */
         0xE4, 0xAF, 0xA1, UUID, /* Clock Identity */
         0x00, 0x01, /* Source port ID */
         SEQUENCE_ID,
         PTP_CTRL_SYNC,
         0x01, /* log message interval */

         /* Payload */
         TIMESTAMP(1, 2),

         /* FCS */
         0xfe, 0xca, 0xca, 0xfe
        };

    if (length)
        *length = mCount(mesgBuffer);
    return mesgBuffer;
    }

static uint8 *FollowUpPacketOverUdp(uint32 *length)
    {
    static uint8 mesgBuffer[]=
        {
         0x01, 0x00, 0x5E, 0x00, 0x01, 0x81,
         0xE4, 0xAF, 0xA1, 0x36, 0x00, 0x10,

         /* Ethernet Type */
         0x08, 0x00,

         /* IPv4 */
         0x45, 0x00, 0x00, 0x98, 0x00, 0x5D,
         0x40, 0x00, 0x01, 0x11, 0x29, 0x68,
         0xC0, 0xA8, 0x01, 0x01, 0xE0, 0x00, 0x01, 0x81,

         /* UDP */
         0x01, 0x3F, 0x01, 0x3F, 0x00, 0x84, 0xC0, 0x7B,

         /* PTP */
         PTP_TYPE_FOLLOW_UP, 0x02,
         0x00, 0x2C, /* Length */
         0x00, 0x00, /* Domain, Reserved */
         0x00, 0x00, /* Flags */
         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* correctionField */
         0x00, 0x00, 0x00, 0x00, /* Reserved */
         0xE4, 0xAF, 0xA1, UUID, /* Clock Identity */
         0x00, 0x01, /* Source port ID */
         SEQUENCE_ID,
         PTP_CTRL_FOLLOW_UP,
         0x01, /* log message interval */

         /* Payload */
         TIMESTAMP(1, 2),

         /* FCS */
         0xfe, 0xca, 0xca, 0xfe
        };

    if (length)
        *length = mCount(mesgBuffer);
    return mesgBuffer;
    }

static uint8 *DelayReqPacketOverEthernet(uint32 *length)
    {
    static uint8 mesgBuffer[]=
        {
         0x01, 0x00, 0x5E, 0x00, 0x01, 0x81,
         0xE4, 0xAF, 0xA1, 0x36, 0x00, 0x10,

         /* Ethernet Type */
         0x88, 0xF7,

         /* PTP */
         PTP_TYPE_DELAY_REQ, 0x02,
         0x00, 0x2C, /* Length */
         0x00, 0x00, /* Domain, Reserved */
         0x00, 0x00, /* Flags */
         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* correctionField */
         0x00, 0x00, 0x00, 0x00, /* Reserved */
         0xE4, 0xAF, 0xA1, UUID, /* Clock Identity */
         0x00, 0x01, /* Source port ID */
         SEQUENCE_ID,
         PTP_CTRL_DELAY_REQ,
         0x01, /* log message interval */

         /* Payload */
         TIMESTAMP(3, 4),

         /* FCS */
         0xfe, 0xca, 0xca, 0xfe
        };

    if (length)
        *length = mCount(mesgBuffer);
    return mesgBuffer;
    }

static uint8 *DelayRespPacketOverUdp(uint32 *length)
    {
    static uint8 mesgBuffer[]=
        {
         0x01, 0x00, 0x5E, 0x00, 0x01, 0x81,
         0xE4, 0xAF, 0xA1, 0x36, 0x00, 0x10,

         /* Ethernet Type */
         0x08, 0x00,

         /* IPv4 */
         0x45, 0x00, 0x00, 0x98, 0x00, 0x5D,
         0x40, 0x00, 0x01, 0x11, 0x29, 0x68,
         0xC0, 0xA8, 0x01, 0x01, 0xE0, 0x00, 0x01, 0x81,

         /* UDP */
         0x01, 0x3F, 0x01, 0x3F, 0x00, 0x84, 0xC0, 0x7B,

         /* PTP */
         PTP_TYPE_DELAY_RESP, 0x02,
         0x00, 0x2C, /* Length */
         0x00, 0x00, /* Domain, Reserved */
         0x00, 0x00, /* Flags */
         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* correctionField */
         0x00, 0x00, 0x00, 0x00, /* Reserved */
         0xE4, 0xAF, 0xA1, UUID, /* Clock Identity */
         0x00, 0x01, /* Source port ID */
         SEQUENCE_ID,
         PTP_CTRL_DELAY_RESP,
         0x01, /* log message interval */

         /* Payload */
         TIMESTAMP(5, 0),
         0xE4, 0xAF, 0xAD, UUID, /* Clock Identity */
         0x00, 0x02, /* Source port ID */

         /* FCS */
         0xfe, 0xca, 0xca, 0xfe
        };

    if (length)
        *length = mCount(mesgBuffer);
    return mesgBuffer;
    }

static uint8 *AnnouncePacketOverUdp(uint32 *length)
    {
    static uint8 mesgBuffer[]=
        {
         0x01, 0x00, 0x5E, 0x00, 0x01, 0x81,
         0xE4, 0xAF, 0xA1, 0x36, 0x00, 0x10,

         /* Ethernet Type */
         0x08, 0x00,

         /* IPv4 */
         0x45, 0x00, 0x00, 0x98, 0x00, 0x5D,
         0x40, 0x00, 0x01, 0x11, 0x29, 0x68,
         0xC0, 0xA8, 0x01, 0x01, 0xE0, 0x00, 0x01, 0x81,

         /* UDP */
         0x01, 0x3F, 0x01, 0x3F, 0x00, 0x84, 0xC0, 0x7B,

         /* PTP */
         PTP_TYPE_ANNOUNCE, 0x02,
         0x00, 0x2C, /* Length */
         0x00, 0x00, /* Domain, Reserved */
         0x00, 0x00, /* Flags */
         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* correctionField */
         0x00, 0x00, 0x00, 0x00, /* Reserved */
         0xE4, 0xAF, 0xA1, UUID, /* Clock Identity */
         0x00, 0x01, /* Source port ID */
         SEQUENCE_ID,
         0x05,
         0x01, /* log message interval */

         /* Payload */
         TIMESTAMP(5, 0),
         0x12, 0x34, /* UTC offset */
         0x00,
         0x05, /* grandmaster priority 1 */
         0xaa, 0xbb, 0xcc, 0xdd, /* grandmaster quality */
         0x06, /* grandmaster priority 2 */
         0xE4, 0xAF, 0xAD, UUID, /* grandmaster identity */
         0x00, 0x02, /* Step removed */
         0x12, /* Time source */

         /* FCS */
         0xfe, 0xca, 0xca, 0xfe
        };

    if (length)
        *length = mCount(mesgBuffer);
    return mesgBuffer;
    }

static void AtPtpPacketTestWithBuffer(uint8 *buffer, const char *testsuite, uint32 length)
    {
    AtPacket newPacket = AtEthPacketNew(buffer, length, cAtFalse);

    AtPrintc(cSevInfo, "=================================\r\n");
    AtPrintc(cSevInfo, "== %s \r\n", testsuite);
    AtPrintc(cSevInfo, "=================================\r\n");

    AtPacketDisplay(newPacket);
    AtObjectDelete((AtObject)newPacket);
    }

void AtPtpPacketTest(void)
    {
    uint32 length;
    uint8 *buffer;

    buffer = SyncPacketOverUdp(&length);
    AtPtpPacketTestWithBuffer(buffer, "Sync over IPv4/UDP",  length);

    buffer = FollowUpPacketOverUdp(&length);
    AtPtpPacketTestWithBuffer(buffer, "Follow_Up over IPv4/UDP", length);

    buffer = DelayReqPacketOverEthernet(&length);
    AtPtpPacketTestWithBuffer(buffer, "Delay_Req over Ethernet", length);

    buffer = DelayRespPacketOverUdp(&length);
    AtPtpPacketTestWithBuffer(buffer, "Delay_Resp over IPv4/UDP", length);

    buffer = AnnouncePacketOverUdp(&length);
    AtPtpPacketTestWithBuffer(buffer, "Announce over IPv4/UDP", length);
    }


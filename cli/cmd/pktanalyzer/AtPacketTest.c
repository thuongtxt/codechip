/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : AtPacketTest.c
 *
 * Created Date: May 2, 2013
 *
 * Description : Test how a packet is displayed
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtEthPacket.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void AtPacketTest(void);

/*--------------------------- Implementation ---------------------------------*/
static uint8 *NoVlanEthPacket(uint32 *length)
    {
    static uint8 dataBuffer[] =
        {
        /* Preamble */
        0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0xD5,

        /* DMAC, SMAC */
        0xCA, 0xFE, 0xCA, 0xFE, 0xCA, 0xFE,
        0xC0, 0xCA, 0xC0, 0xCA, 0xC0, 0xCA,

        /* Type */
        0x81, 0x37,

        /* Payload */
        0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
        0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F,
        0x20, 0x21, 0x22,

        /* CRC */
        0x1, 0x2, 0x3, 0x4
        };
    *length = sizeof(dataBuffer);
    return dataBuffer;
    }

static uint8 *CVlanEthPacket(uint32 *length)
    {
    static uint8 dataBuffer[] =
        {
        /* Preamble */
        0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0xD5,

        /* DMAC, SMAC */
        0xCA, 0xFE, 0xCA, 0xFE, 0xCA, 0xFE,
        0xC0, 0xCA, 0xC0, 0xCA, 0xC0, 0xCA,

        /* C-VLAN    PCP        CFI        VID */
        0x81, 0x00, (((3 << 1) | 1) << 4), 0x12,

        /* Type */
        0x81, 0x37,

        /* Payload */
        0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
        0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F,
        0x20, 0x21, 0x22,

        /* CRC */
        0x1, 0x2, 0x3, 0x4
        };
    *length = sizeof(dataBuffer);
    return dataBuffer;
    }

static uint8 *SVlanCVlanEthPacket(uint32 *length)
    {
    static uint8 dataBuffer[] =
        {
        /* Preamble */
        0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0xD5,

        /* DMAC, SMAC */
        0xCA, 0xFE, 0xCA, 0xFE, 0xCA, 0xFE,
        0xC0, 0xCA, 0xC0, 0xCA, 0xC0, 0xCA,

        /* C-VLAN    PCP        CFI        VID */
        0x81, 0x00, (((3 << 1) | 1) << 4), 0x12,

        /* S-VLAN    PCP        CFI        VID */
        0x91, 0x00, (((4 << 1) | 0) << 4), 0x16,

        /* Type */
        0x81, 0x37,

        /* Payload */
        0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
        0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F,
        0x20, 0x21, 0x22,

        /* CRC */
        0x1, 0x2, 0x3, 0x4
        };
    *length = sizeof(dataBuffer);
    return dataBuffer;
    }

static uint8 *CVlanEthIpV4Packet(uint32 *length)
    {
    static uint8 dataBuffer[] =
        {
        /* Preamble */
        0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0xD5,

        /* DMAC, SMAC */
        0xCA, 0xFE, 0xCA, 0xFE, 0xCA, 0xFE,
        0xC0, 0xCA, 0xC0, 0xCA, 0xC0, 0xCA,

        /* C-VLAN     PCP       CFI        VID */
        0x81, 0x00, (((3 << 1) | 1) << 4), 0x12,

        /* Type */
        0x08, 0x00,

        /* IPV4 Payload */
		/* Version IHL */
		(4 << 4) | 2,

		/* DSCP         ECN*/
		(0x15 << 2) | 2,

		/* Length */
		((1024 & cBit15_8) >> 8), 0,

		/* Identification */
		((0x200 & cBit15_8) >> 8), 0,

		/* Flags | Fragment Offset */
		(0x5 << 5) | ((1234 & cBit12_8) >> 8), 1234 & cBit7_0,

		/* TTL | Protocol | Header Checksum */
		112, 0x34,      (0xabcd & cBit15_8) >> 8, 0xabcd & cBit7_0,

		/* Source, Dest */
		123, 124, 125, 126,
		223, 224, 225, 226,

		/* IP Data */
		0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
		0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F,
		0x20, 0x21, 0x22,

        /* CRC */
        0x1, 0x2, 0x3, 0x4
        };
    *length = sizeof(dataBuffer);
    return dataBuffer;
    }

static uint8 *CVlanEthIpV6Packet(uint32 *length)
    {
    static uint8 dataBuffer[] =
        {
        /* Preamble */
        0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0xD5,

        /* DMAC, SMAC */
        0xCA, 0xFE, 0xCA, 0xFE, 0xCA, 0xFE,
        0xC0, 0xCA, 0xC0, 0xCA, 0xC0, 0xCA,

        /* C-VLAN     PCP       CFI        VID */
        0x81, 0x00, (((3 << 1) | 1) << 4), 0x12,

        /* Type */
        0x86, 0xDD,

        /* IPV6 Payload */
            /* Version | Traffic class | Flow label */
            (6 << 4) | ((0xab & cBit7_4) >> 4), ((0xab & cBit3_0) << 4) | ((0x56789 & cBit19_16) >> 16), (0x56789 & cBit15_8) >> 8, (0x56789 & cBit7_0),

            /* Payload length */
            (1024 & cBit15_8) >> 8, 0x0,

            /* Next header, Hop Limit */
            0x23, 52,

            /* Source, Dest */
            123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138,
            223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238,

            /* IP Data */
            0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
            0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F,
            0x20, 0x21, 0x22,

        /* CRC */
        0x1, 0x2, 0x3, 0x4
        };
    *length = sizeof(dataBuffer);
    return dataBuffer;
    }

static void AtEthPacketTestWithBuffer(uint8 *buffer, uint32 length)
    {
    AtPacket ethPacket = AtEthPacketNew(buffer, length, cAtFalse);
    AtPrintc(cSevInfo, "=================================\r\n");
    AtPacketDisplay(ethPacket);
    AtObjectDelete((AtObject)ethPacket);
    }

void AtPacketTest(void)
    {
    uint32 length;
    uint8 *buffer;

    buffer = NoVlanEthPacket(&length);
    AtEthPacketTestWithBuffer(buffer, length);
    buffer = CVlanEthPacket(&length);
    AtEthPacketTestWithBuffer(buffer, length);
    buffer = SVlanCVlanEthPacket(&length);
    AtEthPacketTestWithBuffer(buffer, length);
    buffer = CVlanEthIpV4Packet(&length);
    AtEthPacketTestWithBuffer(buffer, length);
    buffer = CVlanEthIpV6Packet(&length);
    AtEthPacketTestWithBuffer(buffer, length);
    }

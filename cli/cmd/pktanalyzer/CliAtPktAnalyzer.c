/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : CliAtPktAnalyzer.c
 *
 * Created Date: Dec 19, 2012
 *
 * Description : Packet analyzer CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "AtModulePktAnalyzer.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char * cAtPktAnalyzerPatternCompareModeStr[] =
        {
        "any",
        "same",
        "different"
        };

static const eAtPktAnalyzerPatternCompareMode cAtPktAnalyzerPatternCompareModeVal[]  =
        {
         cAtPktAnalyzerPatternCompareModeAny,
         cAtPktAnalyzerPatternCompareModeSamePkt,
         cAtPktAnalyzerPatternCompareModeDifferent
        };

static const char * cAtPktAnalyzerPppPktTypeStr[] =
        {
       "mlpprotpkt",
       "pppprotpkt",
       "hdlc",
       "pppoam",
       "mlppppkt",
       "ppppkt"
        };

static const eAtPktAnalyzerPppPktType cAtPktAnalyzerPppPktTypeVal[]  =
        {
         cAtPktAnalyzerPppPktTypeMlpppProtocol,
         cAtPktAnalyzerPppPktTypePppProtocol,
         cAtPktAnalyzerPppPktTypeHdlc,
         cAtPktAnalyzerPppPktTypeOamPacket,
         cAtPktAnalyzerPppPktTypeMlpppPacket,
         cAtPktAnalyzerPppPktTypePppPacket
        };

static const char * cAtPktAnalyzerPacketDumpTypeStr[] =
        {
         "all",
         "error",
         "good"
        };

static const eAtPktAnalyzerPktDumpType cAtPktAnalyzerPacketDumpTypeVal[]  =
        {
         cAtPktAnalyzerDumpAllPkt,
         cAtPktAnalyzerDumpErrorPkt,
         cAtPktAnalyzerDumpGoodPkt
        };


static const char * cAtPktAnalyzerPacketDumpLengthStr[] =
        {
         "32bytes",
         "64bytes",
         "128bytes",
         "256bytes",
         "512bytes",
         "1024bytes",
         "full"
        };

static const uint32 cAtPktAnalyzerPacketDumpLengthVal[]  =
        {
        32,
        64,
        128,
        256,
        512,
        1024,
        0
        };

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPktAnalyzer CurrentAnalyzer(void)
    {
    AtModulePktAnalyzer pktAnalyzerModule = (AtModulePktAnalyzer)AtDeviceModuleGet(CliDevice(), cAtModulePktAnalyzer);
    return AtModulePacketAnalyzerCurrentAnalyzerGet(pktAnalyzerModule);
    }

static AtPktAnalyzer DefaultAnalyzerCreate(void)
    {
    AtModulePktAnalyzer pktAnalyzerModule = (AtModulePktAnalyzer)AtDeviceModuleGet(CliDevice(), cAtModulePktAnalyzer);
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    return AtModulePacketAnalyzerEthPortPktAnalyzerCreate(pktAnalyzerModule, AtModuleEthPortGet(ethModule, 0));
    }

static eBool HelperPktAnalyzerAnalyze(eAtDirection direction)
    {
    AtPktAnalyzer pktAnalyzer = CurrentAnalyzer();

    /* Create a default packet analyzer */
    if (pktAnalyzer == NULL)
        pktAnalyzer = DefaultAnalyzerCreate();

    if (direction == cAtDirectionTx)
        AtPktAnalyzerAnalyzeTxPackets(pktAnalyzer);
    else
        AtPktAnalyzerAnalyzeRxPackets(pktAnalyzer);

    return cAtTrue;
    }

eBool CmdPktAnalyzerPatternData(char argc, char **argv)
    {
	AtUnused(argv);
	AtUnused(argc);
    return cAtOk;
    }

eBool CmdPktAnalyzerPatternMask(char argc, char **argv)
    {
	AtUnused(argv);
	AtUnused(argc);
    return cAtOk;
    }

eBool CmdPktAnalyzerPatternCompareMode(char argc, char **argv)
    {
    eAtPktAnalyzerPatternCompareMode patternComparemode = cAtPktAnalyzerPatternCompareModeAny;
    eBool convertSuccess = cAtFalse;
	AtUnused(argc);

    mAtStrToEnum(cAtPktAnalyzerPatternCompareModeStr,
                 cAtPktAnalyzerPatternCompareModeVal,
                 argv[0],
                 patternComparemode,
                 convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "Invalid Pattern Compare mode\r\n");
        return cAtFalse;
        }

    AtPktAnalyzerPatternCompareModeSet(CurrentAnalyzer(), patternComparemode);

    return cAtTrue;
    }

eBool CmdPktAnalyzerRecapture(char argc, char **argv)
    {
	AtUnused(argv);
	AtUnused(argc);
    AtPktAnalyzerRecapture(CurrentAnalyzer());
    return cAtTrue;
    }

eBool CmdPktAnalyzerPacketLength(char argc, char **argv)
    {
    uint32 packetLength = 0;
    eBool convertSuccess = cAtFalse;
	AtUnused(argc);

    mAtStrToEnum(cAtPktAnalyzerPacketDumpLengthStr,
                 cAtPktAnalyzerPacketDumpLengthVal,
                 argv[0],
                 packetLength,
                 convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "Invalid PPP packet mode\r\n");
        return cAtFalse;
        }

    AtPktAnalyzerPacketLengthSet(CurrentAnalyzer(), packetLength);
    return cAtTrue;
    }

eBool CmdPktAnalyzerNumDisplayPackets(char argc, char **argv)
    {
    uint32 numPacket = AtStrToDw(argv[0]);
	AtUnused(argc);
    AtPktAnalyzerNumPacketsSet(CurrentAnalyzer(), numPacket);
    return cAtTrue;
    }

eBool CmdPktAnalyzerPppPacketMode(char argc, char **argv)
    {
    eAtPktAnalyzerPppPktType mode = cAtPktAnalyzerPppPktTypeOamPacket;
    eBool convertSuccess = cAtFalse;
	AtUnused(argc);

    mAtStrToEnum(cAtPktAnalyzerPppPktTypeStr,
                 cAtPktAnalyzerPppPktTypeVal,
                 argv[0],
                 mode,
                 convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "Invalid PPP packet mode\r\n");
        return cAtFalse;
        }

    AtPktAnalyzerPppPacketTypeSet(CurrentAnalyzer(), mode);

    return cAtTrue;
    }

eBool CmdPktAnalyzerPacketDumpType(char argc, char **argv)
    {
    eAtPktAnalyzerPktDumpType dumpType = cAtPktAnalyzerDumpAllPkt;
    eBool convertSuccess = cAtFalse;
	AtUnused(argc);

    mAtStrToEnum(cAtPktAnalyzerPacketDumpTypeStr,
                 cAtPktAnalyzerPacketDumpTypeVal,
                 argv[0],
                 dumpType,
                 convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "Invalid PPP packet mode\r\n");
        return cAtFalse;
        }

    AtPktAnalyzerPacketDumpTypeSet(CurrentAnalyzer(), dumpType);
    return cAtTrue;
    }

eBool CmdPktAnalyzerAnalyzeTx(char argc, char **argv)
    {
	AtUnused(argv);
	AtUnused(argc);
    return HelperPktAnalyzerAnalyze(cAtDirectionTx);
    }

eBool CmdPktAnalyzerAnalyzeRx(char argc, char **argv)
    {
	AtUnused(argv);
	AtUnused(argc);
    return HelperPktAnalyzerAnalyze(cAtDirectionRx);
    }
    
eBool CmdPktAnalyzerCreate(char argc, char **argv)
    {
    uint32      numPhysicals, bufferSize;
    AtChannel   *physicalList;
    const char *idType;
    AtModulePktAnalyzer pktAnalyzerModule = (AtModulePktAnalyzer)AtDeviceModuleGet(CliDevice(), cAtModulePktAnalyzer);
    AtPktAnalyzer pktAnalyzer = NULL;
	AtUnused(argc);

    /* Get the shared ID buffer */
    physicalList = CliSharedChannelListGet(&bufferSize);
    numPhysicals = CliChannelListFromString(argv[0], physicalList, bufferSize);

    /* Only create packet analyzer for one channel */
    if ((numPhysicals != 1) || (physicalList[0] == NULL))
        return cAtFalse;

    idType = AtChannelTypeString(physicalList[0]);
    if (idType == NULL)
        return cAtFalse;

    if (AtStrcmp(idType, sAtChannelTypeEthPort) == 0)
        pktAnalyzer = AtModulePacketAnalyzerEthPortPktAnalyzerCreate(pktAnalyzerModule, (AtEthPort)physicalList[0]);

    else if (AtStrcmp(idType, sAtChannelTypeEthFlow) == 0)
        pktAnalyzer = AtModulePacketAnalyzerEthFlowPktAnalyzerCreate(pktAnalyzerModule, (AtEthFlow)physicalList[0]);

    else if (AtStrcmp(idType, sAtChannelTypePppLink) == 0)
        pktAnalyzer = AtModulePacketAnalyzerPppLinkPktAnalyzerCreate(pktAnalyzerModule, (AtPppLink)physicalList[0]);

    else if (AtStrcmp(idType, sAtChannelTypeEncapHdlc) == 0)
        pktAnalyzer = AtModulePacketAnalyzerHdlcChannelPktAnalyzerCreate(pktAnalyzerModule, (AtHdlcChannel)physicalList[0]);

    else if (AtStrcmp(idType, sAtChannelTypeDcc) == 0)
        pktAnalyzer = AtModulePacketAnalyzerHdlcChannelPktAnalyzerCreate(pktAnalyzerModule, (AtHdlcChannel)physicalList[0]);

    else if (AtStrcmp(idType, sAtChannelTypeHdlcDcc) == 0)
        pktAnalyzer = AtModulePacketAnalyzerHdlcChannelPktAnalyzerCreate(pktAnalyzerModule, (AtHdlcChannel)physicalList[0]);

    return ((pktAnalyzer) ? cAtTrue : cAtFalse);
    }

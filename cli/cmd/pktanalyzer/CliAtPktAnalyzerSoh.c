/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : CliAtPktAnalyzerSoh.c
 *
 * Created Date: Feb 27, 2017
 *
 * Description : To analyze SONET/SDH Overhead over ETH
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "AtModulePktAnalyzer.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char * cAtPktAnalyzerSohPktTypeStr[] =
    {
    "dcc", "kbyte"
    };

static const eAtPktAnalyzerSohPktType cAtPktAnalyzerSohPktTypeVal[] =
    {
    cAtPktAnalyzerSohPktTypeDcc, cAtPktAnalyzerSohPktTypeKbyteAps
    };

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPktAnalyzer CurrentAnalyzer(void)
    {
    AtModulePktAnalyzer pktAnalyzerModule = (AtModulePktAnalyzer)AtDeviceModuleGet(CliDevice(), cAtModulePktAnalyzer);
    return AtModulePacketAnalyzerCurrentAnalyzerGet(pktAnalyzerModule);
    }

eBool CmdPktAnalyzerSohPacketType(char argc, char **argv)
    {
    eAtRet ret;
    eAtPktAnalyzerSohPktType type = cAtPktAnalyzerSohPktTypeDcc;
    eBool convertSuccess = cAtFalse;
    AtUnused(argc);

    mAtStrToEnum(cAtPktAnalyzerSohPktTypeStr,
                 cAtPktAnalyzerSohPktTypeVal,
                 argv[0],
                 type,
                 convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "Invalid SOH packet type\r\n");
        return cAtFalse;
        }

    ret = AtPktAnalyzerPacketTypeSet(CurrentAnalyzer(), type);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Can not to packet type %s, ret = %s\r\n",
                 argv[0],
                 AtRet2String(ret));
        return cAtFalse;
        }
    return cAtTrue;
    }

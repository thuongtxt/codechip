/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2011 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SONET/SDH
 *
 * File        : atclisdhmap.c
 *
 * Created Date: Nov 29, 2011
 *
 * Description : Mapping CLIs
 *
 * Notes       :
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtSdhAug.h"
#include "AtSdhTug.h"
#include "AtSdhVc.h"
#include "AtChannelHierarchyDisplayer.h"
#include "CliAtModuleSdh.h"
#include "AtCliModule.h"

/*--------------------------- Define -----------------------------------------*/
#define cInvalidMappingType 0xFF

/*--------------------------- Local typedefs ---------------------------------*/
static const uint32 cAtSdhAugMapTypeVal[] =
        {
        cAtSdhAugMapTypeAug64MapVc4_64c,
        cAtSdhAugMapTypeAug64Map4xAug16s,
        cAtSdhAugMapTypeAug16MapVc4_16c,
        cAtSdhAugMapTypeAug16Map4xAug4s,
        cAtSdhAugMapTypeAug4MapVc4_4c,
        cAtSdhAugMapTypeAug4Map4xAug1s,
        cAtSdhAugMapTypeAug1MapVc4,
        cAtSdhAugMapTypeAug1Map3xVc3s
        };

static const char * cAtSdhAugMapTypeStr[] =
        {
        "vc4_64c",
        "4xaug16s",
        "vc4_16c",
        "4xaug4s",
        "vc4_4c",
        "4xaug1s",
        "vc4",
        "3xvc3s"
        };

static const uint32 cAtSdhVcMapTypeVal[] =
        {
         /* VC4-64C mapping type */
         cAtSdhVcMapTypeVc4_64cMapC4_64c,

         /* VC4-16C mapping type */
         cAtSdhVcMapTypeVc4_16cMapC4_16c,

        /* VC4-4C mapping type */
         cAtSdhVcMapTypeVc4_4cMapC4_4c,

        /* VC-4 mapping type */
        cAtSdhVcMapTypeVc4MapC4,
        cAtSdhVcMapTypeVc4Map3xTug3s,

        /* VC-3 mapping type */
        cAtSdhVcMapTypeVc3MapC3,
        cAtSdhVcMapTypeVc3Map7xTug2s,
        cAtSdhVcMapTypeVc3MapDe3,

        /* VC-1x mapping type */
        cAtSdhVcMapTypeVc1xMapC1x,
        cAtSdhVcMapTypeVc1xMapDe1,
        cAtSdhVcMapTypeNone
        };

static const char * cAtSdhVcMapTypeStr[] =
        {
        "c4_64c",
        "c4_16c",
        "c4_4c",
        "c4",
        "3xtug3s",
        "c3",
        "7xtug2s",
        "de3",
        "c1x",
        "de1",
        "none"
        };

static const uint32 cAtSdhTugMapTypeVal[] =
        {
         cAtSdhTugMapTypeTug3MapVc3,
         cAtSdhTugMapTypeTug3Map7xTug2s,
         cAtSdhTugMapTypeTug2Map4xTu11s,
         cAtSdhTugMapTypeTug2Map3xTu12s
        };

static const char * cAtSdhTugMapTypeStr[] =
        {
        "vc3",
        "7xtug2s",
        "tu11",
        "tu12"
        };

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implement Functions ----------------------------*/

/*-------------------------------------------------------------------------------------------*/

/*------------------- Commands Implementation --------------------------------*/
static uint32 MappingTypeFromString(eAtSdhChannelType channelType, const char *mapTypeString)
    {
    eBool convertSuccess = cAtTrue;
    uint32 mapType = cInvalidMappingType;

    /* AUG mapping type */
    if ((channelType == cAtSdhChannelTypeAug64) ||
        (channelType == cAtSdhChannelTypeAug16) ||
        (channelType == cAtSdhChannelTypeAug4)  ||
        (channelType == cAtSdhChannelTypeAug1))
        mAtStrToEnum(cAtSdhAugMapTypeStr, cAtSdhAugMapTypeVal, mapTypeString, mapType, convertSuccess);

    /* VC mapping type */
    if ((channelType == cAtSdhChannelTypeVc4_64c) ||
        (channelType == cAtSdhChannelTypeVc4_16c) ||
        (channelType == cAtSdhChannelTypeVc4_4c)  ||
        (channelType == cAtSdhChannelTypeVc4_nc)  ||
        (channelType == cAtSdhChannelTypeVc4)     ||
        (channelType == cAtSdhChannelTypeVc3)     ||
        (channelType == cAtSdhChannelTypeVc12)    ||
        (channelType == cAtSdhChannelTypeVc11))
        mAtStrToEnum(cAtSdhVcMapTypeStr, cAtSdhVcMapTypeVal, mapTypeString, mapType, convertSuccess);

    /* TUG mapping type */
    if ((channelType == cAtSdhChannelTypeTug3) ||
        (channelType == cAtSdhChannelTypeTug2))
        mAtStrToEnum(cAtSdhTugMapTypeStr, cAtSdhTugMapTypeVal, mapTypeString, mapType, convertSuccess);

    if (!convertSuccess)
        mapType = cInvalidMappingType;

    return mapType;
    }

static const char *MappingTypeToString(eAtSdhChannelType channelType, uint8 mapType)
    {
    const char *mapTypeStr = NULL;

    /* AUG mapping type */
    if ((channelType == cAtSdhChannelTypeAug64) ||
        (channelType == cAtSdhChannelTypeAug16) ||
        (channelType == cAtSdhChannelTypeAug4)  ||
        (channelType == cAtSdhChannelTypeAug1))
        mapTypeStr = CliEnumToString(mapType, cAtSdhAugMapTypeStr, cAtSdhAugMapTypeVal, mCount(cAtSdhAugMapTypeVal), NULL);

    /* VC mapping type */
    if ((channelType == cAtSdhChannelTypeVc4_64c) ||
        (channelType == cAtSdhChannelTypeVc4_16c) ||
        (channelType == cAtSdhChannelTypeVc4_4c)  ||
        (channelType == cAtSdhChannelTypeVc4_nc)  ||
        (channelType == cAtSdhChannelTypeVc4)     ||
        (channelType == cAtSdhChannelTypeVc3)     ||
        (channelType == cAtSdhChannelTypeVc12)    ||
        (channelType == cAtSdhChannelTypeVc11))
        mapTypeStr = CliEnumToString(mapType, cAtSdhVcMapTypeStr, cAtSdhVcMapTypeVal, mCount(cAtSdhVcMapTypeVal), NULL);

    /* TUG mapping type */
    if ((channelType == cAtSdhChannelTypeTug3) ||
        (channelType == cAtSdhChannelTypeTug2))
        mapTypeStr = CliEnumToString(mapType, cAtSdhTugMapTypeStr, cAtSdhTugMapTypeVal, mCount(cAtSdhTugMapTypeVal), NULL);

    return mapTypeStr ? mapTypeStr : "None";
    }

static eBool ShouldRestAfterEachChannel(AtSdhChannel channel)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(channel);
    if ((channelType == cAtSdhChannelTypeLine) || (channelType == cAtSdhChannelTypeAug16) || (channelType == cAtSdhChannelTypeAug4))
        return cAtTrue;

    return cAtFalse;
    }

/* SET mapping for SDH channels */
eBool CmdSdhMapSet(char argc, char **argv)
    {
    eAtRet ret = cAtOk;
    AtChannel *channel;
    uint32 numChannels, i;
    uint32 mapType;
    eBool success = cAtTrue;
	AtUnused(argc);

    /* Get shared buffer, channel type and ID from argv[0], ex: aug1.1.1 -> channel type: aug1 and Id = 1.1 */
    channel = CliSharedChannelListGet(&numChannels);
    numChannels = CliChannelListFromString(argv[0], channel, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevInfo, "No channel to configure. The ID list may be wrong\r\n");
        return cAtOk;
        }

    /* Get mapping type from string */
    mapType = MappingTypeFromString(AtSdhChannelTypeGet((AtSdhChannel)channel[0]), argv[1]);
    if (mapType == cInvalidMappingType)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid mapping type\r\n");
        return cAtFalse;
        }

    /* Set mapping for channels */
    for(i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        ret = AtSdhChannelMapTypeSet((AtSdhChannel)channel[i], (uint8)mapType);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot change mapping for %s, ret = %s\r\n",
                     CliChannelIdStringGet(channel[i]), AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

/* Show SDH mapping */
eBool CmdSdhMapShow(char argc, char **argv)
    {
    eAtRet cliRet;
    tTab    *tabPtr;
    AtChannel *channel;
    uint32 numChannels, i;
    eAtSdhChannelType channelType;
    uint8   mapType;
    eAtSevLevel  color;
    const char *heading[] = {"ID", "MappingType"};
    char buf[16];
    const char *mapTypeString;
	AtUnused(argc);

    cliRet = cAtOk;
    numChannels = 0;

    /* Get shared buffer, channel type and ID from argv[0], ex: aug1.1.1 -> channel type: aug1 and Id = 1.1 */
    channel = CliSharedChannelListGet(&numChannels);
    numChannels = CliChannelListFromString(argv[0], channel, numChannels);

    /* Create table with titles */
    tabPtr = TableAlloc(numChannels, 2, heading);
    if (!tabPtr)
        {
        AtOsalTrace(cAtOsalTraceError, "Fail to create table with retCode: %d\r\n", cliRet);
        return cAtFalse;
        }

    /* Make table content */
    AtOsalMemInit(buf, 0, sizeof(buf));
    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        channelType = AtSdhChannelTypeGet((AtSdhChannel)channel[i]);
        mapType = AtSdhChannelMapTypeGet((AtSdhChannel)channel[i]);

        /* Get ID string from channel */
        StrToCell(tabPtr, i, 0, (char*)CliChannelIdStringGet(channel[i]));

        /* Get mapping type string */
        mapTypeString = MappingTypeToString(channelType, mapType);
        if (AtStrcmp(mapTypeString, "None") == 0)
            color = cSevNormal;
        else
            color = cSevInfo;
        ColorStrToCell(tabPtr, i, 1, mapTypeString, color);
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdSdhMapHierarchyShow(char argc, char **argv)
    {
    AtChannel *channel;
    uint32 numChannels, i;
    AtChannelHierarchyDisplayer displayer;
    uint8 debugLevel = 0;
    uint32 numChannelToRest = 1;

    /* Get shared buffer, channel type and ID from argv[0], ex: aug1.1.1 -> channel type: aug1 and Id = 1.1 */
    channel = CliSharedChannelListGet(&numChannels);
    numChannels = CliChannelListFromString(argv[0], channel, numChannels);

    if (argc == 2)
        {
        if (AtStrcmp(argv[1], "debug_sts") == 0)
            debugLevel = 1;
        else if (AtStrcmp(argv[1], "debug_bridgeandroll") == 0)
            debugLevel = 2;
        }

    displayer = AtSdhChannelHierarchyDisplayerGet(cAtChannelHierarchyModeAlarm, debugLevel);
    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, numChannelToRest))
        {
        if (channel[i] == NULL)
            continue;

        AtChannelHierarchyShow(displayer, channel[i], "");

        /* Determine should have a rest before next channel */
        numChannelToRest = (ShouldRestAfterEachChannel((AtSdhChannel)channel[i])) ? 1 : AtCliLimitNumRestTdmChannelGet();
        }

    return cAtTrue;
    }

eBool CmdSonetMapHierarchyShow(char argc, char **argv)
    {
    AtChannel *channel;
    uint32 numChannels, i;
    AtChannelHierarchyDisplayer displayer;
    uint8 debugLevel = 0;
    uint32 numChannelToRest = 1;

    /* Get shared buffer, channel type and ID from argv[0], ex: aug1.1.1 -> channel type: aug1 and Id = 1.1 */
    channel = CliSharedChannelListGet(&numChannels);
    numChannels = CliChannelListFromString(argv[0], channel, numChannels);

    if (argc == 2)
        {
        if (AtStrcmp(argv[1], "debug_sts") == 0)
            debugLevel = 1;
        else if (AtStrcmp(argv[1], "debug_bridgeandroll") == 0)
            debugLevel = 2;
        }

    displayer = AtSonetChannelHierarchyDisplayerGet(cAtChannelHierarchyModeAlarm, debugLevel);
    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, numChannelToRest))
        {
        if (channel[i] == NULL)
            continue;

        AtChannelHierarchyShow(displayer, channel[i], "");

        /* Determine should have a rest before next channel */
        numChannelToRest = (ShouldRestAfterEachChannel((AtSdhChannel)channel[i])) ? 1 : AtCliLimitNumRestTdmChannelGet();
        }

    return cAtTrue;
    }

eBool CmdSdhMapHierarchyHistoryShow(char argc, char **argv)
    {
    AtChannel *channel;
    uint32 numChannels, i;
    AtChannelHierarchyDisplayer displayer;
    uint8 debugMode = 0;
    uint32 numChannelToRest = 1;

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    /* Get shared buffer, channel type and ID from argv[0], ex: aug1.1.1 -> channel type: aug1 and Id = 1.1 */
    channel = CliSharedChannelListGet(&numChannels);
    numChannels = CliChannelListFromString(argv[0], channel, numChannels);

    if ((argc > 2) && (AtStrcmp(argv[2], "debug") == 0))
        debugMode = cAtTrue;

    if (AtStrcmp(argv[1], "r2c") == 0)
        displayer = AtSdhChannelHierarchyDisplayerGet(cAtChannelHierarchyModeHistoryR2C, debugMode);
    else
        displayer = AtSdhChannelHierarchyDisplayerGet(cAtChannelHierarchyModeHistoryRO, debugMode);

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, numChannelToRest))
        {
        if (channel[i] == NULL)
            continue;

        AtChannelHierarchyShow(displayer, channel[i], "");

        /* Determine should have a rest before next channel */
        numChannelToRest = (ShouldRestAfterEachChannel((AtSdhChannel)channel[i])) ? 1 : AtCliLimitNumRestTdmChannelGet();
        }

    return cAtTrue;
    }

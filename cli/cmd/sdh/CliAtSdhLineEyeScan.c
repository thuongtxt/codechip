/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : CliAtSdhLineEyeScan.c
 *
 * Created Date: Dec 30, 2014
 *
 * Description : SDH Line eye scan
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtModuleSdh.h"
#include "AtTokenizer.h"
#include "../physical/CliAtEyeScan.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void ShowLaneFormatError(void)
    {
    AtPrintc(cSevCritical, "ERROR: Invalid lane ID, expect format: <lineId>.<laneId>\r\n");
    }

static AtModuleSdh SdhModule(void)
    {
    return (AtModuleSdh)AtDeviceModuleGet(CliDevice(), cAtModuleSdh);
    }

static AtList LanesFromIdString(char *lanesString)
    {
    AtIdParser idParser;
    char idMaxformat[8];
    AtModuleSdh sdhModule = SdhModule();
    AtList lanes;

    /* Create ID parser */
    AtSprintf(idMaxformat, "%d.%d", AtModuleSdhMaxLinesGet(sdhModule), AtEyeScanControllerMaxNumLanes());
    idParser = AtIdParserNew(lanesString, "1.1", idMaxformat);
    if (idParser == NULL)
        {
        ShowLaneFormatError();
        return NULL;
        }

    /* Have all valid lanes */
    lanes = AtListCreate(0);
    while (AtIdParserHasNextNumber(idParser))
        {
        uint32 lineId   = AtIdParserNextNumber(idParser);
        uint32 laneId   = AtIdParserNextNumber(idParser);
        AtSdhLine line  = AtModuleSdhLineGet(sdhModule, (uint8)(lineId - 1));
        AtEyeScanController controller = AtSdhLineEyeScanControllerGet(line);
        AtEyeScanLane lane = AtEyeScanControllerLaneGet(controller, (uint8)(laneId - 1));
        if (lane)
            AtListObjectAdd(lanes, (AtObject)lane);
        else
            {
            AtPrintc(cSevWarning,
                     "WARNING: Ignore invalid lane %d.%d which does not exist\r\n",
                     lineId, laneId);
            }
        }

    /* Free resources */
    AtObjectDelete((AtObject)idParser);
    if (AtListLengthGet(lanes) == 0)
        {
        AtObjectDelete((AtObject)lanes);
        lanes = NULL;
        AtPrintc(cSevCritical, "ERROR: No lanes are parsed. The ID may be wrong, expected: <lineId>.<laneId>\r\n");
        }

    return lanes;
    }

static AtEyeScanLane LaneFromIdString(char *laneString)
    {
    AtTokenizer tokenizer = AtTokenizerSharedTokenizer(laneString, ".");
    uint32 lineId, laneId;
    AtSdhLine line;
    AtEyeScanLane lane;

    if (AtTokenizerNumStrings(tokenizer) != 2)
        return NULL;

    lineId = AtStrToDw(AtTokenizerNextString(tokenizer)) - 1;
    laneId = AtStrToDw(AtTokenizerNextString(tokenizer)) - 1;
    line   = AtModuleSdhLineGet(SdhModule(), (uint8)lineId);
    lane   = AtEyeScanControllerLaneGet(AtSdhLineEyeScanControllerGet(line), (uint8)laneId);
    if (lane == NULL)
        ShowLaneFormatError();

    return lane;
    }

static eBool AttributeSet(char argc, char **argv,
                          eBool (*attributeSetFunc)(AtList lanes, char *valueString))
    {
    AtList lanes = LanesFromIdString(argv[0]);
    eBool result = attributeSetFunc(lanes, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)lanes);
    return result;
    }

static eBool EyeScanEnable(char argc, char **argv, eBool enable)
    {
    AtList lanes = LanesFromIdString(argv[0]);
    eBool success = CliAtEyeScanLaneEnable(lanes, enable);
    AtUnused(argc);
    AtObjectDelete((AtObject)lanes);
    return success;
    }

static eBool DebugEnable(char argc, char **argv, eBool enable)
    {
    AtList lanes = LanesFromIdString(argv[0]);
    eBool success = CliAtEyeScanLaneDebugEnable(lanes, enable);
    AtUnused(argc);
    AtObjectDelete((AtObject)lanes);
    return success;
    }

AtList CliAtSdhLineSerdesEyeScanControllersFromIdString(char *idString)
    {
    uint8 i;
    uint32 *idBuf;
    uint32 bufferSize;
    uint32 numLines;
    AtModuleSdh sdhModule;
    AtList controllers = NULL;

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numLines = CliIdListFromString(idString, idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid line ID list. Expected: 1 or 1-2, ...\r\n");
        return NULL;
        }

    controllers = AtListCreate(0);
    sdhModule = (AtModuleSdh)AtDeviceModuleGet(CliDevice(), cAtModuleSdh);
    for (i = 0; i < numLines; i++)
        {
        AtSdhLine line = AtModuleSdhLineGet(sdhModule, (uint8)(idBuf[i] - 1));
        AtSerdesController serdesController = AtSdhLineSerdesController(line);
        AtEyeScanController controller = AtSerdesControllerEyeScanControllerGet(serdesController);
        if (controller)
            AtListObjectAdd(controllers, (AtObject)controller);
        else
            AtPrintc(cSevWarning, "WARNING: line %d does not have any eye scan controllers\r\n", idBuf[i]);
        }

    if (AtListLengthGet(controllers) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: no eye scan controllers, ID may be wrong or lines do not have any eye scan controllers\r\n");
        AtObjectDelete((AtObject)controllers);
        controllers = NULL;
        }

    return controllers;
    }

eBool CmdAtSdhLineSerdesEyeScan(char argc, char **argv)
    {
    AtList controllers = CliAtSdhLineSerdesEyeScanControllersFromIdString(argv[0]);
    eBool result = CliAtEyeScanControllerScan(controllers);
    AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return result;
    }

eBool CmdAtSdhLineSerdesEyeScanLaneDrpRead(char argc, char **argv)
    {
    return CliAtEyeScanLaneDrpRead(LaneFromIdString(argv[0]), argc, argv);
    }

eBool CmdAtSdhLineSerdesEyeScanLaneDrpWrite(char argc, char **argv)
    {
    return CliAtEyeScanLaneDrpWrite(LaneFromIdString(argv[0]), argc, argv);
    }

eBool CmdAtSdhLineSerdesEyeScanLaneStepSizeHorizonalSet(char argc, char **argv)
    {
    return AttributeSet(argc, argv, CliAtEyeScanLaneStepSizeHorizonalSet);
    }

eBool CmdAtSdhLineSerdesEyeScanLaneStepSizeVerticalSet(char argc, char **argv)
    {
    return AttributeSet(argc, argv, CliAtEyeScanLaneStepSizeVerticalSet);
    }

eBool CmdAtSdhLineSerdesEyeScanLaneMaxPrescaleSet(char argc, char **argv)
    {
    return AttributeSet(argc, argv, CliAtEyeScanLaneMaxPrescaleSet);
    }

eBool CmdAtSdhLineSerdesEyeScanLaneShow(char argc, char **argv)
    {
    AtList lanes = LanesFromIdString(argv[0]);
    eBool result = CliAtEyeScanLaneShow(lanes);
    AtUnused(argc);
    AtObjectDelete((AtObject)lanes);
    return result;
    }

eBool CmdAtSdhLineSerdesEyeScanLaneEnable(char argc, char **argv)
    {
    return EyeScanEnable(argc, argv, cAtTrue);
    }

eBool CmdAtSdhLineSerdesEyeScanLaneDisable(char argc, char **argv)
    {
    return EyeScanEnable(argc, argv, cAtFalse);
    }

eBool CmdAtSdhLineSerdesEyeScanControllerEqualizerSet(char argc, char **argv)
    {
    AtList controllers = CliAtSdhLineSerdesEyeScanControllersFromIdString(argv[0]);
    eBool success = CliAtEyeScanControllerEqualizerSet(controllers, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtSdhLineSerdesEyeScanLaneDebugEnable(char argc, char **argv)
    {
    return DebugEnable(argc, argv, cAtTrue);
    }

eBool CmdAtSdhLineSerdesEyeScanLaneDebugDisable(char argc, char **argv)
    {
    return DebugEnable(argc, argv, cAtFalse);
    }

eBool CmdAtSdhLineSerdesEyeScanLaneDebug(char argc, char **argv)
    {
    AtList lanes = LanesFromIdString(argv[0]);
    AtUnused(argc);
    CliAtEyeScanLaneDebug(lanes);
    AtObjectDelete((AtObject)lanes);
    return cAtTrue;
    }


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : CliAtModuleSdhIdParser.c
 *
 * Created Date: Nov 19, 2013
 *
 * Description : SDH ID parser
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDevice.h"
#include "AtModuleSdh.h"
#include "AtPdhDe3.h"
#include "AtSdhLine.h"
#include "AtPdhSerialLine.h"

#include "AtCli.h"
#include "CliAtModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/
#define cMaxLevels   5

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSdhLine LineGet(AtModuleSdh sdhModule, uint32 lineId)
    {
    if (lineId >= AtModuleSdhMaxLinesGet(sdhModule))
        return NULL;

    return AtModuleSdhLineGet(sdhModule, (uint8)lineId);
    }

static eAtSdhLineRate MaxLineRate(void)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(CliDevice(), cAtModuleSdh);
    return AtModuleSdhMaxLineRate(sdhModule);
    }

static uint8 MaxNumAug1sForOneLine(void)
    {
    eAtSdhLineRate rate = MaxLineRate();
    if (rate == cAtSdhLineRateStm0)  return 1;
    if (rate == cAtSdhLineRateStm1)  return 1;
    if (rate == cAtSdhLineRateStm4)  return 4;
    if (rate == cAtSdhLineRateStm16) return 16;
    if (rate == cAtSdhLineRateStm64) return 64;

    return 0;
    }

static char *ThreeLevelsIdMinFormat(void)
    {
    static char minFormat[] = "1.1.1";
    return minFormat;
    }

static uint32 MaxLineId(void)
    {
    return CliAtModuleSdhMaxLineIdGet(CliModuleSdh());
    }

static char *ThreeLevelsIdMaxFormat(void)
    {
    static char maxFormat[16];

    AtOsalMemInit(maxFormat, 0, sizeof(maxFormat));
    AtSprintf(maxFormat, "%u.%u.3", MaxLineId(), MaxNumAug1sForOneLine());

    return maxFormat;
    }

static char *FourLevelsIdMinFormat(void)
    {
    static char minFormat[] = "1.1.1.1";
    return minFormat;
    }

static char *FourLevelsIdMaxFormat(void)
    {
    static char maxFormat[16];

    AtOsalMemInit(maxFormat, 0, sizeof(maxFormat));
    AtSprintf(maxFormat, "%u.%u.3.7", MaxLineId(), MaxNumAug1sForOneLine());

    return maxFormat;
    }

static char *FiveLevelIdMinFormat(void)
    {
    static char minFormat[] = "1.1.1.1.1";
    return minFormat;
    }

static char *FiveLevelIdMaxFormat(void)
    {
    static char maxFormat[16];

    AtOsalMemInit(maxFormat, 0, sizeof(maxFormat));
    AtSprintf(maxFormat, "%u.%u.3.7.4", MaxLineId(), MaxNumAug1sForOneLine());

    return maxFormat;
    }

static char *TwoLevelsIdMinFormat(void)
    {
    static char minFormat[] = "1.1";
    return minFormat;
    }

static char *TwoLevelsIdMaxFormat(void)
    {
    static char maxFormat[16];

    AtOsalMemInit(maxFormat, 0, sizeof(maxFormat));
    AtSprintf(maxFormat, "%u.%u", MaxLineId(), MaxNumAug1sForOneLine());

    return maxFormat;
    }

static uint8 NumAugInLine(AtSdhLine line, uint32 augType)
    {
    eAtSdhLineRate rate = AtSdhLineRateGet(line);

    if (rate == cAtSdhLineRateStm1)
        if (augType == cAtSdhChannelTypeAug1) return 1;

    if (rate == cAtSdhLineRateStm4)
        {
        if (augType == cAtSdhChannelTypeAug1) return 4;
        if (augType == cAtSdhChannelTypeAug4) return 1;
        }

    if (rate == cAtSdhLineRateStm16)
        {
        if (augType == cAtSdhChannelTypeAug1)  return 16;
        if (augType == cAtSdhChannelTypeAug4)  return 4;
        if (augType == cAtSdhChannelTypeAug16) return 1;
        }

    if (rate == cAtSdhLineRateStm64)
        {
        if (augType == cAtSdhChannelTypeAug1)  return 64;
        if (augType == cAtSdhChannelTypeAug4)  return 16;
        if (augType == cAtSdhChannelTypeAug16) return 4;
        if (augType == cAtSdhChannelTypeAug64) return 1;
        }

    return 0;
    }

static uint32 TwoLevelsChannelListFromString(char *pStrIdList,
                                             AtChannel *channel,
                                             uint32 bufferSize,
                                             uint32 augType,
                                             AtChannel (*ChannelGet)(AtSdhLine self, uint8 augId))
    {
    uint32 i, numChannels = 0;
    AtIdParser idParser = AtIdParserNew(pStrIdList, TwoLevelsIdMinFormat(), TwoLevelsIdMaxFormat());

    for (i = 0; i < (AtIdParserNumNumbers(idParser) / 2); i++)
        {
        AtSdhLine line = LineGet(CliModuleSdh(), CliId2DriverId(AtIdParserNextNumber(idParser)));
        uint8 augId = (uint8)CliId2DriverId(AtIdParserNextNumber(idParser));

        if (line == NULL)
            continue;

        if (augId >= NumAugInLine(line, augType))
            continue;

        if (line == NULL)
            continue;

        channel[numChannels] = ChannelGet(line, augId);
        if (channel[numChannels])
            numChannels = numChannels + 1;

        if (numChannels >= bufferSize)
            break;
        }

    AtObjectDelete((AtObject)idParser);

    return numChannels;
    }

static uint32 ThreeLevelsChannelListFromString(char *pStrIdList,
                                               AtChannel *channel,
                                               uint32 bufferSize,
                                               AtChannel (*ChannelGet)(AtSdhLine self, uint8 aug1Id, uint8 au3Id))
    {
    uint32 i, numChannels;
    AtIdParser idParser = AtIdParserNew(pStrIdList, ThreeLevelsIdMinFormat(), ThreeLevelsIdMaxFormat());

    numChannels = 0;
    for (i = 0; i < (AtIdParserNumNumbers(idParser) / 3); i++)
        {
        uint32 levels[3];
        AtSdhLine line;
        uint8 augId;

        CliIdBuild(idParser, levels, 3);
        line = LineGet(CliModuleSdh(), CliId2DriverId(levels[0]));

        if (line == NULL)
            continue;

        augId = (uint8)CliId2DriverId(levels[1]);
        if (augId >= NumAugInLine(line, cAtSdhChannelTypeAug1))
            continue;

        channel[numChannels] = ChannelGet(line, augId, (uint8)CliId2DriverId(levels[2]));
        if (channel[numChannels])
            numChannels = numChannels + 1;

        if (numChannels >= bufferSize)
            break;
        }

    AtObjectDelete((AtObject)idParser);

    return numChannels;
    }

static uint32 FourLevelsChannelListFromString(const char *pStrIdList,
                                             AtChannel *channel,
                                             uint32 bufferSize,
                                             AtChannel (*ChannelGet)(AtSdhLine self, uint8 aug1Id, uint8 au3Tug3Id, uint8 tug2Id))
    {
    uint32 i;
    uint32 numChannels = 0;
    AtIdParser idParser = AtIdParserNew(pStrIdList, FourLevelsIdMinFormat(), FourLevelsIdMaxFormat());

    /* A triple items is an AU-3 ID */
    for (i = 0; i < (AtIdParserNumNumbers(idParser) / 4); i++)
        {
        uint32 levels[4];
        AtSdhLine line;

        CliIdBuild(idParser, levels, 4);
        line = LineGet(CliModuleSdh(), CliId2DriverId(levels[0]));

        if (line == NULL)
            continue;

        channel[numChannels] = ChannelGet(line,
                                          (uint8)CliId2DriverId(levels[1]),
                                          (uint8)CliId2DriverId(levels[2]),
                                          (uint8)CliId2DriverId(levels[3]));
        if (channel[numChannels])
            numChannels = numChannels + 1;

        if (numChannels >= bufferSize)
            break;
        }

    AtObjectDelete((AtObject)idParser);

    return numChannels;
    }

static uint32 FiveLevelsChannelListFromString(const char *pStrIdList,
                                             AtChannel *channel,
                                             uint32 bufferSize,
                                             AtChannel (*ChannelGet)(AtSdhLine self, uint8 aug1Id, uint8 au3Tug3Id, uint8 tug2Id, uint8 tuId))
    {
    uint32 i;
    uint32 numChannels = 0;
    AtIdParser idParser = AtIdParserNew(pStrIdList, FiveLevelIdMinFormat(), FiveLevelIdMaxFormat());

    /* A triple items is an AU-3 ID */
    for (i = 0; i < (AtIdParserNumNumbers(idParser) / 5); i++)
        {
        uint32 levels[5];
        AtSdhLine line;

        CliIdBuild(idParser, levels, 5);
        line = LineGet(CliModuleSdh(), CliId2DriverId(levels[0]));

        if (line == NULL)
            continue;

        channel[numChannels] = ChannelGet(line,
                                          (uint8)CliId2DriverId(levels[1]),
                                          (uint8)CliId2DriverId(levels[2]),
                                          (uint8)CliId2DriverId(levels[3]),
                                          (uint8)CliId2DriverId(levels[4]));
        if (channel[numChannels])
            numChannels = numChannels + 1;

        if (numChannels >= bufferSize)
            break;
        }

    AtObjectDelete((AtObject)idParser);

    return numChannels;
    }

static uint32 Ec1Tu1xThreeLevelsChannelListFromString(char *pStrIdList, AtChannel *channel, uint32 bufferSize,
                                                      AtChannel (*ChannelGet)(AtSdhLine self, uint8 aug1Id, uint8 au3Tug3Id, uint8 tug2Id, uint8 tuId))
    {
    uint32 i;
    uint32 numChannels = 0, numLevel = 3;
    char maxFormat[16];
    char minFormat[16];
    AtIdParser idParser;

    AtSprintf(maxFormat, "%u.7.4", (uint32)CliAtModuleSdhMaxEc1LineIdGet(CliModuleSdh()));
    AtSprintf(minFormat, "%u.1.1", (uint32)CliAtModuleSdhStartEc1LineIdGet(CliModuleSdh()));

    idParser = AtIdParserNew(pStrIdList, minFormat, maxFormat);

    /* A triple items is an AU-3 ID */
    for (i = 0; i < (AtIdParserNumNumbers(idParser) / numLevel); i++)
        {
        uint32 levels[cMaxLevels];
        AtSdhLine line;

        CliIdBuild(idParser, levels, (uint8)numLevel);
        line = LineGet(CliModuleSdh(), CliId2DriverId(levels[0]));

        if (line == NULL)
            continue;

        channel[numChannels] = ChannelGet(line,
                                          0,
                                          0,
                                          (uint8)CliId2DriverId(levels[1]),
                                          (uint8)CliId2DriverId(levels[2]));
        if (channel[numChannels])
            numChannels = numChannels + 1;

        if (numChannels >= bufferSize)
            break;
        }

    AtObjectDelete((AtObject)idParser);

    return numChannels;
    }

static uint32 LineListFromString(char *pStrIdList, AtChannel *channel, uint32 bufferSize)
    {
    AtIdParser idParser = AtIdParserNew(pStrIdList, NULL, NULL);
    uint32 numLines = 0;
	AtUnused(bufferSize);

    while (AtIdParserHasNextNumber(idParser))
        {
        channel[numLines] = (AtChannel)LineGet(CliModuleSdh(), CliId2DriverId(AtIdParserNextNumber(idParser)));
        if (channel[numLines])
            numLines = numLines + 1;
        }

    AtObjectDelete((AtObject)idParser);

    return numLines;
    }

static uint32 Aug64ListFromString(char *pStrIdList, AtChannel *channel, uint32 bufferSize)
    {
    uint32 numChannels;
    char buf[16];
    AtIdParser idParser;

    /* Create ID parser */
    AtSprintf(buf, "%d.1", CliAtModuleSdhMaxLineIdGet(CliModuleSdh()));
    idParser = AtIdParserNew(pStrIdList, TwoLevelsIdMinFormat(), buf);

    /* Parse */
    numChannels = 0;
    while (AtIdParserHasNextNumber(idParser))
        {
        uint32 levels[2];
        AtSdhLine line;

        CliIdBuild(idParser, levels, 2);
        line = LineGet(CliModuleSdh(), CliId2DriverId(levels[0]));
        if (line == NULL)
            continue;

        channel[numChannels] = (AtChannel)AtSdhLineAug64Get(line, (uint8)CliId2DriverId(levels[1]));
        if (channel[numChannels])
            numChannels = numChannels + 1;

        if (numChannels >= bufferSize)
            break;
        }

    AtObjectDelete((AtObject)idParser);

    return numChannels;
    }

static uint32 Aug16ListFromString(char *pStrIdList, AtChannel *channel, uint32 bufferSize)
    {
    uint32 numChannels;
    char buf[16];
    AtIdParser idParser;
    char minFormat[] = "1.1";

    /* Create ID parser */
    AtSprintf(buf, "%d.4", CliAtModuleSdhMaxLineIdGet(CliModuleSdh()));
    idParser = AtIdParserNew(pStrIdList, minFormat, buf);

    /* Parse */
    numChannels = 0;
    while (AtIdParserHasNextNumber(idParser))
        {
        uint32 levels[2];
        AtSdhLine line;

        CliIdBuild(idParser, levels, 2);
        line = LineGet(CliModuleSdh(), CliId2DriverId(levels[0]));
        if (line == NULL)
            continue;

        channel[numChannels] = (AtChannel)AtSdhLineAug16Get(line, (uint8)CliId2DriverId(levels[1]));
        if (channel[numChannels])
            numChannels = numChannels + 1;

        if (numChannels >= bufferSize)
            break;
        }

    AtObjectDelete((AtObject)idParser);

    return numChannels;
    }

static uint32 Aug4ListFromString(char *pStrIdList, AtChannel *channel, uint32 bufferSize)
    {
    uint32 i, numChannels = 0;
    char buf[16];
    AtIdParser idParser;

    /* Create ID parser */
    AtSprintf(buf, "%u.16", MaxLineId());
    idParser = AtIdParserNew(pStrIdList, TwoLevelsIdMinFormat(), buf);

    for (i = 0; i < (AtIdParserNumNumbers(idParser) / 2); i++)
        {
        uint32 levels[2];
        AtSdhLine line;

        CliIdBuild(idParser, levels, 2);
        line = LineGet(CliModuleSdh(), CliId2DriverId(levels[0]));
        if (line == NULL)
            continue;

        channel[numChannels] = (AtChannel)AtSdhLineAug4Get(line, (uint8)CliId2DriverId(levels[1]));
        if (channel[numChannels])
            numChannels = numChannels + 1;

        if (numChannels >= bufferSize)
            break;
        }

    AtObjectDelete((AtObject)idParser);

    return numChannels;
    }

static uint32 Aug1ListFromString(char *pStrIdList, AtChannel *channel, uint32 bufferSize)
    {
    return TwoLevelsChannelListFromString(pStrIdList,
                                          channel,
                                          bufferSize,
                                          cAtSdhChannelTypeAug1,
                                          (AtChannel (*)(AtSdhLine, uint8))AtSdhLineAug1Get);
    }

static uint32 Au4_4cListFromString(char *pStrIdList, AtChannel *channel, uint32 bufferSize)
    {
    return TwoLevelsChannelListFromString(pStrIdList,
                                          channel,
                                          bufferSize,
                                          cAtSdhChannelTypeAug4,
                                          (AtChannel (*)(AtSdhLine, uint8))AtSdhLineAu4_4cGet);
    }

static uint32 Au4_ncListFromString(char *pStrIdList, AtChannel *channel, uint32 bufferSize)
    {
    return TwoLevelsChannelListFromString(pStrIdList,
                                          channel,
                                          bufferSize,
                                          cAtSdhChannelTypeAug1,
                                          (AtChannel (*)(AtSdhLine, uint8))AtSdhLineAu4_ncGet);
    }

static uint32 Vc4_4cListFromString(char *pStrIdList, AtChannel *channel, uint32 bufferSize)
    {
    return TwoLevelsChannelListFromString(pStrIdList,
                                          channel,
                                          bufferSize,
                                          cAtSdhChannelTypeAug4,
                                          (AtChannel (*)(AtSdhLine, uint8))AtSdhLineVc4_4cGet);
    }

static uint32 Au4_16cListFromString(char *pStrIdList, AtChannel *channel, uint32 bufferSize)
    {
    return TwoLevelsChannelListFromString(pStrIdList,
                                          channel,
                                          bufferSize,
                                          cAtSdhChannelTypeAug16,
                                          (AtChannel (*)(AtSdhLine, uint8))AtSdhLineAu4_16cGet);
    }

static uint32 Vc4_16cListFromString(char *pStrIdList, AtChannel *channel, uint32 bufferSize)
    {
    return TwoLevelsChannelListFromString(pStrIdList,
                                          channel,
                                          bufferSize,
                                          cAtSdhChannelTypeAug16,
                                          (AtChannel (*)(AtSdhLine, uint8))AtSdhLineVc4_16cGet);
    }

static uint32 Au4_64cListFromString(char *pStrIdList, AtChannel *channel, uint32 bufferSize)
    {
    return TwoLevelsChannelListFromString(pStrIdList,
                                          channel,
                                          bufferSize,
                                          cAtSdhChannelTypeAug64,
                                          (AtChannel (*)(AtSdhLine, uint8))AtSdhLineAu4_64cGet);
    }

static uint32 Vc4_64cListFromString(char *pStrIdList, AtChannel *channel, uint32 bufferSize)
    {
    return TwoLevelsChannelListFromString(pStrIdList,
                                          channel,
                                          bufferSize,
                                          cAtSdhChannelTypeAug64,
                                          (AtChannel (*)(AtSdhLine, uint8))AtSdhLineVc4_64cGet);
    }

static uint32 Au4_16ncListFromString(char *pStrIdList, AtChannel *channel, uint32 bufferSize)
    {
    return TwoLevelsChannelListFromString(pStrIdList,
                                          channel,
                                          bufferSize,
                                          cAtSdhChannelTypeAug16,
                                          (AtChannel (*)(AtSdhLine, uint8))AtSdhLineAu4_16ncGet);
    }

static uint32 Vc4_16ncListFromString(char *pStrIdList, AtChannel *channel, uint32 bufferSize)
    {
    return TwoLevelsChannelListFromString(pStrIdList,
                                          channel,
                                          bufferSize,
                                          cAtSdhChannelTypeAug16,
                                          (AtChannel (*)(AtSdhLine, uint8))AtSdhLineVc4_16ncGet);
    }

static uint32 Vc4_ncListFromString(char *pStrIdList, AtChannel *channel, uint32 bufferSize)
    {
    return TwoLevelsChannelListFromString(pStrIdList,
                                          channel,
                                          bufferSize,
                                          cAtSdhChannelTypeAug1,
                                          (AtChannel (*)(AtSdhLine, uint8))AtSdhLineVc4_ncGet);
    }

static uint32 Vc4ListFromString(char *pStrIdList, AtChannel *channel, uint32 bufferSize)
    {
    return TwoLevelsChannelListFromString(pStrIdList,
                                          channel,
                                          bufferSize,
                                          cAtSdhChannelTypeAug1,
                                          (AtChannel (*)(AtSdhLine, uint8))AtSdhLineVc4Get);
    }

static uint32 Au4ListFromString(char *pStrIdList, AtChannel *channel, uint32 bufferSize)
    {
    return TwoLevelsChannelListFromString(pStrIdList,
                                          channel,
                                          bufferSize,
                                          cAtSdhChannelTypeAug1,
                                          (AtChannel (*)(AtSdhLine, uint8))AtSdhLineAu4Get);
    }

static uint32 OneLevelsChannelListFromString(char *pStrIdList,
                                             AtChannel *channel,
                                             uint32 bufferSize,
                                             AtChannel (*ChannelGet)(AtSdhLine self, uint8 aug1Id, uint8 au3Id))
    {
    uint32 *idBuffer, numValidChannels = 0, numChannels, i;
    idBuffer = CliSharedIdBufferGet(&numChannels);
    numChannels = CliIdListFromString(pStrIdList, idBuffer, numChannels);
    if (numChannels > bufferSize)
        numChannels = bufferSize;
    for (i = 0; i < numChannels; i++)
        {
        AtSdhLine line = LineGet(CliModuleSdh(),(uint32)(idBuffer[i] - 1));

        if (line == NULL)
            continue;

        if (AtSdhLineRateGet(line) != cAtSdhLineRateStm0)
            continue;

        channel[numValidChannels] = (AtChannel)ChannelGet(line, 0, 0);
        numValidChannels++;
        }

    return numValidChannels;
    }

static uint32 Au3ListFromString(char *pStrIdList, AtChannel *channel, uint32 bufferSize)
    {
    if (AtStrchr(pStrIdList, '.'))
        {
        return ThreeLevelsChannelListFromString(pStrIdList,
                                              channel,
                                              bufferSize,
                                              (AtChannel (*)(AtSdhLine, uint8, uint8))AtSdhLineAu3Get);
        }

    return OneLevelsChannelListFromString(pStrIdList,
                                          channel,
                                          bufferSize,
                                          (AtChannel (*)(AtSdhLine, uint8, uint8))AtSdhLineAu3Get);
    }

static uint32 Tug3ListFromString(char *pStrIdList, AtChannel *channel, uint32 bufferSize)
    {
    return ThreeLevelsChannelListFromString(pStrIdList,
                                              channel,
                                              bufferSize,
                                              (AtChannel (*)(AtSdhLine, uint8, uint8))AtSdhLineTug3Get);
    }

static uint32 Tu3ListFromString(char *pStrIdList, AtChannel *channel, uint32 bufferSize)
    {
    return ThreeLevelsChannelListFromString(pStrIdList,
                                              channel,
                                              bufferSize,
                                              (AtChannel (*)(AtSdhLine, uint8, uint8))AtSdhLineTu3Get);
    }

static uint32 Tug2ListFromString(char *pStrIdList, AtChannel *channel, uint32 bufferSize)
    {
    uint32 i, numChannels, numLevel;
    char maxFormat[16];
    AtIdParser idParser;
    char minFormat[] = "1.1.1.1";

    numLevel = CliAtNumIdLevel(pStrIdList);

    /* Create ID parser */
    if (numLevel == 2)
        {
        AtSprintf(maxFormat, "%u.7", CliAtModuleSdhMaxEc1LineIdGet(CliModuleSdh()));
        AtSprintf(minFormat, "%u.1", CliAtModuleSdhStartEc1LineIdGet(CliModuleSdh()));
        }

    else if (numLevel == 4)
        {
        AtSprintf(maxFormat, "%u.%u.3.7", MaxLineId(), MaxNumAug1sForOneLine());
        AtSprintf(minFormat, "1.1.1.1");
        }
    else
        return 0;

    idParser = AtIdParserNew(pStrIdList, minFormat, maxFormat);
    numChannels = 0;
    for (i = 0; i < (AtIdParserNumNumbers(idParser) / numLevel); i++)
        {
        uint32 levels[cMaxLevels];
        AtSdhLine line;

        CliIdBuild(idParser, levels, (uint8)numLevel);
        line = LineGet(CliModuleSdh(), CliId2DriverId(levels[0]));
        if (line == NULL)
            continue;

        if (numLevel == 2)
            channel[numChannels] = (AtChannel) AtSdhLineTug2Get(line, 0, 0, (uint8)CliId2DriverId(levels[1]));

        else
            channel[numChannels] = (AtChannel)AtSdhLineTug2Get(line, (uint8)CliId2DriverId(levels[1]),
                                                                     (uint8)CliId2DriverId(levels[2]),
                                                                     (uint8)CliId2DriverId(levels[3]));

        if (channel[numChannels])
            numChannels = numChannels + 1;

        if (numChannels >= bufferSize)
            break;
        }

    AtObjectDelete((AtObject)idParser);

    return numChannels;
    }

static uint32 SdhTu1xListFromString(char *pStrIdList, AtChannel *channel, uint32 bufferSize)
    {
    uint32 numLevel = CliAtNumIdLevel(pStrIdList);
    if (numLevel == 3)
        return Ec1Tu1xThreeLevelsChannelListFromString(pStrIdList, channel, bufferSize,
                                                       (AtChannel (*)(AtSdhLine, uint8, uint8, uint8, uint8))AtSdhLineTu1xGet);
    else
        return FiveLevelsChannelListFromString(pStrIdList, channel, bufferSize,
                                               (AtChannel (*)(AtSdhLine, uint8, uint8, uint8, uint8))AtSdhLineTu1xGet);

    }

static AtChannel De1Get(AtSdhLine line, uint8 aug1Id, uint8 au3Tug3Id, uint8 tug2Id, uint8 tuId)
    {
    AtSdhChannel vc1x, vc3;
    AtPdhDe3 de3;

    /* The DE1 may be mapped to VC1x */
    vc1x = (AtSdhChannel)AtSdhLineVc1xGet(line, aug1Id, au3Tug3Id, tug2Id, tuId);
    if (vc1x)
        {
        if (AtSdhChannelMapTypeGet(vc1x) != cAtSdhVcMapTypeVc1xMapDe1)
            return NULL;

        return AtSdhChannelMapChannelGet(vc1x);
        }

    /* Or this can belong to M13 */
    vc3 = (AtSdhChannel)AtSdhLineVc3Get(line, aug1Id, au3Tug3Id);
    if ((vc3 == NULL) || (AtSdhChannelMapTypeGet(vc3) != cAtSdhVcMapTypeVc3MapDe3))
        return NULL;

    /* Check DE3 level */
    de3 = (AtPdhDe3)AtSdhChannelMapChannelGet(vc3);
    return (AtChannel)AtPdhDe3De1Get(de3, tug2Id, tuId);
    }

static AtChannel De2Get(AtSdhLine line, uint8 aug1Id, uint8 au3Tug3Id, uint8 de2Id)
    {
    AtSdhChannel vc3;
    AtPdhDe3 de3;

    /* this can belong to M13 */
    vc3 = (AtSdhChannel)AtSdhLineVc3Get(line, aug1Id, au3Tug3Id);
    if (AtSdhChannelMapTypeGet(vc3) != cAtSdhVcMapTypeVc3MapDe3)
        return NULL;

    /* Check DE3 level */
    de3 = (AtPdhDe3)AtSdhChannelMapChannelGet(vc3);
    return (AtChannel)AtPdhDe3De2Get(de3, de2Id);
    }

uint32 CliAtModuleSdhChanelsFromString(char *pStrIdList, AtChannel *channelList, uint32 bufferSize)
    {
    char *pDupString;
    char *idType, *idString;
    uint32 numChannels = 0;

    /* Get ID type */
    pDupString = IdTypeAndIdListGet(pStrIdList, &idType, &idString);
    if ((idType == NULL) || (idString == NULL))
        {
        AtOsalMemFree(pDupString);
        return 0;
        }

    if (AtStrcmp(idType, sAtChannelTypeSdhLine) == 0)
        numChannels = LineListFromString(idString, channelList, bufferSize);

    else if (AtStrcmp(idType, sAtChannelTypeSdhAug64) == 0)
        numChannels = Aug64ListFromString(idString, channelList, bufferSize);

    else if (AtStrcmp(idType, sAtChannelTypeSdhAug16) == 0)
        numChannels = Aug16ListFromString(idString, channelList, bufferSize);

    else if (AtStrcmp(idType, sAtChannelTypeSdhAug4) == 0)
        numChannels = Aug4ListFromString(idString, channelList, bufferSize);

    else if (AtStrcmp(idType, sAtChannelTypeSdhAug1) == 0)
        numChannels = Aug1ListFromString(idString, channelList, bufferSize);

    else if (AtStrcmp(idType, sAtChannelTypeSdhAu4_4c) == 0)
        numChannels = Au4_4cListFromString(idString, channelList, bufferSize);

    else if (AtStrcmp(idType, sAtChannelTypeSdhAu4_nc) == 0)
        numChannels = Au4_ncListFromString(idString, channelList, bufferSize);

    else if (AtStrcmp(idType, sAtChannelTypeSdhVc4_4c) == 0)
        numChannels = Vc4_4cListFromString(idString, channelList, bufferSize);

    else if (AtStrcmp(idType, sAtChannelTypeSdhAu4_16c) == 0)
        numChannels = Au4_16cListFromString(idString, channelList, bufferSize);

    else if (AtStrcmp(idType, sAtChannelTypeSdhVc4_16c) == 0)
        numChannels = Vc4_16cListFromString(idString, channelList, bufferSize);

    else if (AtStrcmp(idType, sAtChannelTypeSdhAu4_64c) == 0)
        numChannels = Au4_64cListFromString(idString, channelList, bufferSize);

    else if (AtStrcmp(idType, sAtChannelTypeSdhVc4_64c) == 0)
        numChannels = Vc4_64cListFromString(idString, channelList, bufferSize);

    else if (AtStrcmp(idType, sAtChannelTypeSdhAu4_16nc) == 0)
        numChannels = Au4_16ncListFromString(idString, channelList, bufferSize);

    else if (AtStrcmp(idType, sAtChannelTypeSdhVc4_16nc) == 0)
        numChannels = Vc4_16ncListFromString(idString, channelList, bufferSize);

    else if (AtStrcmp(idType, sAtChannelTypeSdhVc4_nc) == 0)
        numChannels = Vc4_ncListFromString(idString, channelList, bufferSize);

    else if (AtStrcmp(idType, sAtChannelTypeSdhVc4) == 0)
        numChannels = Vc4ListFromString(idString, channelList, bufferSize);

    else if (AtStrcmp(idType, sAtChannelTypeSdhAu4) == 0)
        numChannels = Au4ListFromString(idString, channelList, bufferSize);

    else if (AtStrcmp(idType, sAtChannelTypeSdhAu3) == 0)
        numChannels = Au3ListFromString(idString, channelList, bufferSize);

    else if (AtStrcmp(idType, sAtChannelTypeSdhTug3) == 0)
        numChannels = Tug3ListFromString(idString, channelList, bufferSize);

    else if (AtStrcmp(idType, sAtChannelTypeSdhTu3) == 0)
        numChannels = Tu3ListFromString(idString, channelList, bufferSize);

    else if (AtStrcmp(idType, sAtChannelTypeSdhVc3) == 0)
        numChannels = AtSdhVc3ListFromString(idString, channelList, bufferSize);

    else if (AtStrcmp(idType, sAtChannelTypeSdhTug2) == 0)
        numChannels = Tug2ListFromString(idString, channelList, bufferSize);

    else if (AtStrcmp(idType, sAtChannelTypeSdhVc1x) == 0)
        numChannels = AtSdhVc1xListFromString(idString, channelList, bufferSize);

    else if (AtStrcmp(idType, sAtChannelTypeSdhTu1x) == 0)
        numChannels = SdhTu1xListFromString(idString, channelList, bufferSize);

    /* Free memory */
    AtOsalMemFree(pDupString);

    return numChannels;
    }

uint32 AtSdhVc1xListFromString(char *pStrIdList, AtChannel *channels, uint32 bufferSize)
    {
    uint32 numLevel = CliAtNumIdLevel(pStrIdList);
    if (numLevel == 3)
        return Ec1Tu1xThreeLevelsChannelListFromString(pStrIdList, channels, bufferSize,
                                                       (AtChannel (*)(AtSdhLine, uint8, uint8, uint8, uint8))AtSdhLineVc1xGet);
    else
        return FiveLevelsChannelListFromString(pStrIdList, channels, bufferSize,
                                               (AtChannel (*)(AtSdhLine, uint8, uint8, uint8, uint8))AtSdhLineVc1xGet);
    }

uint32 AtSdhVc3ListFromString(char *pStrIdList, AtChannel *channel, uint32 bufferSize)
    {
    if (AtStrchr(pStrIdList, '.'))
        return ThreeLevelsChannelListFromString(pStrIdList,
                                                channel,
                                                bufferSize,
                                                (AtChannel (*)(AtSdhLine, uint8, uint8))AtSdhLineVc3Get);
    else
        return OneLevelsChannelListFromString(pStrIdList,
                                              channel,
                                              bufferSize,
                                              (AtChannel (*)(AtSdhLine, uint8, uint8))AtSdhLineVc3Get);
    }

uint32 AtSdhDe1ListFromString(const char *pStrIdList, AtChannel *channels, uint32 bufferSize)
    {
    return FiveLevelsChannelListFromString(pStrIdList, channels, bufferSize, De1Get);
    }

uint32 AtSdhDe2ListFromString(const char *pStrIdList, AtChannel *channels, uint32 bufferSize)
    {
    return FourLevelsChannelListFromString(pStrIdList, channels, bufferSize, De2Get);
    }

uint32 CliSdhLineArrayFromString(char *pStrIdList, AtChannel *channel, uint32 bufferSize)
    {
    return LineListFromString(pStrIdList, channel, bufferSize);
    }

uint32 CliAtNumIdLevel(char *pStrIdList)
    {
    uint32 numLevel;
    AtIdParser idParser = AtIdParserNew(pStrIdList, NULL, NULL);
    numLevel = AtIdParserNumIdLevel(idParser);
    AtObjectDelete((AtObject)idParser);

    return numLevel;
    }

uint8 CliAtModuleSdhMaxLineIdGet(AtModuleSdh moduleSdh)
    {
    return (uint8)(AtModuleSdhMaxLinesGet(moduleSdh));
    }

uint8 CliAtModuleSdhMaxEc1LineIdGet(AtModuleSdh moduleSdh)
    {
    return (uint8)(AtModuleSdhStartEc1LineIdGet(moduleSdh) + AtModuleSdhNumEc1LinesGet(moduleSdh));
    }

uint8 CliAtModuleSdhStartEc1LineIdGet(AtModuleSdh moduleSdh)
    {
    return (uint8)(AtModuleSdhStartEc1LineIdGet(moduleSdh) + 1);
    }

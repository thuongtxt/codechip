/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2011 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SONET/SDH
 *
 * File        : CliAtSdhPath.c
 *
 * Created Date: Nov 13, 2012
 *
 * Description : path CLIs
 *
 * Notes       :
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "CliAtModuleSdh.h"
#include "AtCliModule.h"
#include "../common/CliAtChannelObserver.h"

/*--------------------------- Define -----------------------------------------*/
#define mAttributeSetFunc(func) ((PathAttributeSetFunc)(func))

/*--------------------------- Local typedefs ---------------------------------*/
typedef eAtRet (*PathAttributeSetFunc)(AtSdhPath self, uint32 value);
typedef eAtRet (*TtiAttributeSet)(AtSdhChannel self, const tAtSdhTti *tti);
typedef eAtRet (*BooldAttributeSetFunc)(AtSdhPath self, eBool enable);

/*--------------------------- Global variables -------------------------------*/
static const char *cCmdSdhPathErrorTypeStr[]={"bip", "rei"};
static const eAtSdhPathErrorType cCmdSdhPathErrorTypeVal[]={cAtSdhPathErrorBip,
                                                            cAtSdhPathErrorRei
                                                            };
static const char *cCmdSdhTtiModeStr[] ={"1byte", "16bytes", "64bytes"};
static const eAtSdhTtiMode cCmdSdhTtiModeVal[]={cAtSdhTtiMode1Byte, cAtSdhTtiMode16Byte, cAtSdhTtiMode64Byte};
static const char * cCmdSdhChannelModeStr[]={"sdh", "sonet"};
static const eAtSdhChannelMode cCmdSdhChannelModeVal[]={cAtSdhChannelModeSdh, cAtSdhChannelModeSonet};

static const char *cCmdSdhPathAlarmStr[] =
    {
     "ais",
     "lop",
     "tim",
     "uneq",
     "p-uneq",
     "plm",
     "rdi",
     "erdi-s",
     "erdi-p",
     "erdi-c",
     "ber-sd",
     "ber-sf",
     "ber-tca",
     "lom",
     "rfi",
     "clock-state-change",
     "tti-change",
     "psl-change",
     "pi-ndf",
     "pg-ndf",
     "new-pointer"
    };

static const uint32 cCmdSdhPathAlarmValue[]=
    {
     cAtSdhPathAlarmAis,
     cAtSdhPathAlarmLop,
     cAtSdhPathAlarmTim,
     cAtSdhPathAlarmUneq,
     cAtSdhPathAlarmPayloadUneq,
     cAtSdhPathAlarmPlm,
     cAtSdhPathAlarmRdi,
     cAtSdhPathAlarmErdiS,
     cAtSdhPathAlarmErdiP,
     cAtSdhPathAlarmErdiC,
     cAtSdhPathAlarmBerSd,
     cAtSdhPathAlarmBerSf,
     cAtSdhPathAlarmBerTca,
     cAtSdhPathAlarmLom,
     cAtSdhPathAlarmRfi,
     cAtSdhPathAlarmClockStateChange,
     cAtSdhPathAlarmTtiChange,
     cAtSdhPathAlarmPslChange,
     cAtSdhPathAlarmPiNdf,
     cAtSdhPathAlarmPgNdf,
     cAtSdhPathAlarmNewPointer
    };

static const char *cCmdSdhPathAlarmUpperStr[] =
    {
     "AIS",
     "LOP",
     "TIM",
     "UNEQ",
     "P-UNEQ",
     "PLM",
     "RDI",
     "ERDI-S",
     "ERDI-P",
     "ERDI-C",
     "BER-SD",
     "BER-SF",
     "BER-TCA",
     "LOM",
     "RFI",
     "ACR/DCR",
     "TTI",
     "PSL",
     "PI-NDF",
     "PG-NDF",
     "NewPointer"
    };

static const char *cCmdSdhPathOverheadByteStr[] ={"n1", "c2", "g1", "f2", "h4", "f3", "k3", "v5", "n2", "k4"};
static const uint32 cCmdSdhPathOverheadByteValue[]={cAtSdhPathOverheadByteN1, cAtSdhPathOverheadByteC2,
                                                    cAtSdhPathOverheadByteG1, cAtSdhPathOverheadByteF2,
                                                    cAtSdhPathOverheadByteH4, cAtSdhPathOverheadByteF3,
                                                    cAtSdhPathOverheadByteK3, cAtSdhPathOverheadByteV5,
                                                    cAtSdhPathOverheadByteN2, cAtSdhPathOverheadByteK4};

static const char *cCmdLoopbackModeStr[]={"release", "local", "remote"};
static const eAtLoopbackMode cCmdLoopbackModeVal[]={cAtLoopbackModeRelease, cAtLoopbackModeLocal, cAtLoopbackModeRemote };

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
extern uint8 AtChannelHwClockStateGet(AtChannel self);

/*--------------------------- Implement Functions ----------------------------*/
static eBool TtiSet(char argc, char **argv, TtiAttributeSet TtiSetFunc)
    {
    uint8 ttiMsgBuf[cCliSdhTti64Len + 1];
    uint8 ttiMsgLen;
    uint8 indexVal;
    uint32 numberPaths, i;
    eAtRet ret;
    AtSdhPath *paths;
    tAtSdhTti tti;
    eAtSdhTtiMode ttiMode = cAtSdhTtiMode1Byte;
    eBool blResult;
	AtUnused(argc);

    /* Initialize new memory area */
    AtOsalMemInit(ttiMsgBuf, 0,  sizeof(ttiMsgBuf));
    AtOsalMemInit(tti.message, 0,  sizeof(tti.message));
    blResult = cAtTrue;
    ret = cAtOk;
    indexVal = 0;

    /* Get list of path */
    paths = CliSdhPathFromArgumentGet(argv[indexVal], &numberPaths);
    if (numberPaths == 0)
        {
        AtPrintc(cSevNormal, "Error: ID is wrong\r\n");
        ret = cAtErrorInvlParm;
        }
    indexVal++;

    mAtStrToEnum(cCmdSdhTtiModeStr,
                 cCmdSdhTtiModeVal,
                 argv[indexVal],
                 ttiMode,
                 blResult);
    indexVal++;
    if(!blResult)
        {
        AtPrintc(cSevCritical, "Invalid param tti mode\r\n");
        ret = cAtErrorInvlParm;
        }

    /* Check padding information & store transmitted TTI message into buffer */
    ttiMsgLen = CliSdhTtiPaddingAdd(argv[indexVal], argv[indexVal + 1], ttiMsgBuf, ttiMode);
    AtSdhTtiMake(ttiMode, ttiMsgBuf, ttiMsgLen, &tti);
    for (i = 0; i < numberPaths; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        ret = TtiSetFunc((AtSdhChannel)paths[i], &tti);

    return (ret == cAtOk) ? cAtTrue : cAtFalse;
    }

static AtModuleSdh SdhModule(void)
    {
    return (AtModuleSdh)AtDeviceModuleGet(CliDevice(), cAtModuleSdh);
    }

static eBool AttributeSet(char *idListStr, uint32 value, PathAttributeSetFunc setFunc)
    {
    uint32 numberPaths, i;
    AtSdhPath *paths;
    eBool success = cAtTrue;

    /* Get list of path */
    paths = CliSdhPathFromArgumentGet(idListStr, &numberPaths);
    if (numberPaths == 0)
        {
        AtPrintc(cSevNormal, "ERROR: ID is wrong\r\n");
        return cAtFalse;
        }

    /* Call api */
    for (i = 0; i < numberPaths; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret = setFunc(paths[i], value);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot configure %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)paths[i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static eBool HexadecimalAttributeSet(char argc, char **argv, PathAttributeSetFunc attributeSetFunc)
    {
    uint32 value = (uint32)AtStrtoul(argv[1], NULL, 16);
    AtUnused(argc);
    return AttributeSet(argv[0], value, attributeSetFunc);
    }

static eBool DecimalAttributeSet(char argc, char **argv, PathAttributeSetFunc attributeSetFunc)
    {
    uint32 value = (uint32)AtStrtoul(argv[1], NULL, 10);
    AtUnused(argc);
    return AttributeSet(argv[0], value, attributeSetFunc);
    }

static eBool AlarmEnable(char argc, char **argv, eBool enable)
    {
    uint32 numberPaths, i;
    eAtRet ret = cAtOk;
    AtSdhPath *paths;
    uint32 alarmValue = 0;
    AtUnused(argc);

    /* Get list of paths */
    paths = CliSdhPathFromArgumentGet(argv[0], &numberPaths);
    if (numberPaths == 0)
        {
        AtPrintc(cSevNormal, "Error: ID is wrong\r\n");
        return cAtFalse;
        }

    /* Alarm types */
    if (argv[1] != NULL)
        alarmValue = CliSdhPathAlarmMaskFromString(argv[1]);
    else
        {
        AtPrintc(cSevCritical, "ERROR: Invalid parameter <rate> , expected los|oof|lof...\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numberPaths; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        ret |= AtSdhChannelAlarmAffectingEnable((AtSdhChannel)paths[i], alarmValue, enable);

    return (ret == cAtOk) ? cAtTrue : cAtFalse;
    }

/*
 * Enable Path alarm
 */
static eBool BoolAttributeSet(char argc, char **argv, eBool enable, BooldAttributeSetFunc boolSetFunc)
    {
    uint32 numberPaths, i;
    eAtRet ret = cAtOk;
    AtSdhPath *sdhVcs;
	AtUnused(argc);

    /* Get list of paths */
    sdhVcs = CliSdhPathFromArgumentGet(argv[0], &numberPaths);
    if (numberPaths == 0)
        {
        AtPrintc(cSevNormal, "No channels, check hierarchy\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numberPaths; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        ret |= boolSetFunc(sdhVcs[i], enable);

    return (ret == cAtOk) ? cAtTrue : cAtFalse;
    }

static eBool AutoRdiEnable(char argc, char **argv, eBool enable)
    {
    uint32 numberPaths, path_i;
    uint32 bufferSize;
    AtSdhPath *paths;
    eBool success = cAtTrue;
    uint32 alarms = cInvalidUint32;

	AtUnused(argc);

    /* Get list of paths */
    paths = (AtSdhPath *)CliSharedChannelListGet(&bufferSize);
    numberPaths = CliAtModuleSdhChanelsFromString(argv[0], (AtChannel *)paths, bufferSize);
    if (numberPaths == 0)
        {
        AtPrintc(cSevNormal, "WARNING: No paths to configure. Input ID may be wrong\r\n");
        return cAtTrue;
        }

    /* Get specific alarms */
    if (argc > 1)
        alarms = CliSdhPathAlarmMaskFromString(argv[1]);

    for (path_i = 0; path_i < numberPaths; path_i++)
            {
        eAtRet ret;
        AtSdhChannel channel = (AtSdhChannel)paths[path_i];

        if (alarms == cInvalidUint32)
            ret = AtSdhChannelAutoRdiEnable(channel, enable);
        else
            ret = AtSdhChannelAlarmAutoRdiEnable(channel, alarms, enable);

        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical,
             "ERROR: %s auto RDI on %s fail with ret = %s\r\n",
             enable ? "Enable" : "Disable",
             CliChannelIdStringGet((AtChannel)channel),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

static const char * TimingModeStr(uint32 mode)
    {
    switch (mode)
        {
        case cAtTimingModeAcr: return "ACR";
        case cAtTimingModeDcr: return "DCR";
        default:               return "Clock";
        }
    }

static void AlarmChangeState(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    uint8 alarm_i;

	AtPrintc(cSevNormal, "\r\n%s [SDH] Defect changed at channel %s", AtOsalDateTimeInUsGet(), CliChannelIdStringGet(channel));
    for (alarm_i = 0; alarm_i < mCount(cCmdSdhPathAlarmUpperStr); alarm_i++)
        {
        uint32 alarmType = cCmdSdhPathAlarmValue[alarm_i];
        if ((changedAlarms & alarmType) == 0)
            continue;

        if (changedAlarms & alarmType & cAtSdhPathAlarmClockStateChange)
            {
            AtPrintc(cSevNormal, " * [%s:", TimingModeStr(AtChannelTimingModeGet(channel)));
            AtPrintc(cSevNormal, "SW: %s, HW: %d",
                     CliClockStateString(AtChannelClockStateGet(channel)),
                     AtChannelHwClockStateGet(channel));
            }
        else
            {
        AtPrintc(cSevNormal, " * [%s:", cCmdSdhPathAlarmUpperStr[alarm_i]);
            if (changedAlarms & alarmType & cAtSdhPathAlarmTtiChange)
                AtPrintc(cSevNormal, "new_jn");
            else if (changedAlarms & alarmType & cAtSdhPathAlarmPslChange)
                AtPrintc(cSevNormal, "0x%x", AtSdhPathRxPslGet((AtSdhPath)channel));
            else
                {
        if (currentStatus & alarmType)
            AtPrintc(cSevCritical, "set");
        else
            AtPrintc(cSevInfo, "clear");
            }
            }
        AtPrintc(cSevNormal, "]");
        }
    AtPrintc(cSevNormal, "\r\n");
    }

static void AlarmChangeLogging(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    uint8 alarm_i;
    static char logBuffer[1024];
    char * logPtr = logBuffer;

    AtOsalMemInit(logBuffer, 0, sizeof(logBuffer));

    logPtr += AtSprintf(logPtr, "has defects changed at");
    for (alarm_i = 0; alarm_i < mCount(cCmdSdhPathAlarmUpperStr); alarm_i++)
        {
        uint32 alarmType = cCmdSdhPathAlarmValue[alarm_i];
        if ((changedAlarms & alarmType) == 0)
            continue;

        if (changedAlarms & alarmType & cAtSdhPathAlarmClockStateChange)
            {
            logPtr += AtSprintf(logPtr, " * [%s:", TimingModeStr(AtChannelTimingModeGet(channel)));
            logPtr += AtSprintf(logPtr, "SW: %s, HW: %d",
                                CliClockStateString(AtChannelClockStateGet(channel)),
                                AtChannelHwClockStateGet(channel));
            }
        else
            {
            logPtr += AtSprintf(logPtr, " * [%s:", cCmdSdhPathAlarmUpperStr[alarm_i]);
            if (currentStatus & alarmType)
                logPtr += AtSprintf(logPtr, "set");
            else
                logPtr += AtSprintf(logPtr, "clear");
            }
        logPtr += AtSprintf(logPtr, "]");
        }
    CliChannelLog(channel, logBuffer);
    }

static void DefectLogging(AtChannel channel, uint32 currentDefect, eBool enable)
    {
    uint8 alarm_i;

    for (alarm_i = 0; alarm_i < mCount(cCmdSdhPathAlarmUpperStr); alarm_i++)
        {
        eAtSdhPathAlarmType alarmType = cCmdSdhPathAlarmValue[alarm_i];
        if (currentDefect & alarmType)
            {
            AtPrintc(cSevNormal, "\r\n%s [SDH] Defect [%s] %s at channel %s\r\n",
                     AtOsalDateTimeInUsGet(), cCmdSdhPathAlarmUpperStr[alarm_i], enable ? "forced" : "unforced", CliChannelIdStringGet(channel));
            return;
            }
        }
    }

/* Convert string of OH bytes to value */
static uint32 CliSdhPathOverheadBytesFromString(char *pOHBytesStr)
    {
    return CliMaskFromString(pOHBytesStr, cCmdSdhPathOverheadByteStr, cCmdSdhPathOverheadByteValue, mCount(cCmdSdhPathOverheadByteValue));
    }

static eBool CliBridgeAndRollFunction(char argc, char **argv, eAtRet (*BridgeAndRollFunction)(AtSdhChannel self, AtSdhChannel otherChannel))
    {
    AtSdhChannel *srcPaths;
    AtSdhChannel *destPaths;
    uint32 numberSrcPaths, numberDestPaths, i;
    uint32 bufferSize;
    eBool success = cAtTrue;
    AtUnused(argc);

    /* Get list of source path */
    srcPaths = (AtSdhChannel *)CliSharedChannelListGet(&bufferSize);
    numberSrcPaths = CliAtModuleSdhChanelsFromString(argv[0], (AtChannel *)srcPaths, bufferSize / 2);
    if (numberSrcPaths == 0)
        {
        AtPrintc(cSevCritical, "Error: source path Id is wrong\r\n");
        return cAtFalse;
        }

    destPaths = &srcPaths[bufferSize / 2];
    numberDestPaths = CliAtModuleSdhChanelsFromString(argv[1], (AtChannel *)destPaths, bufferSize / 2);
    if (numberDestPaths == 0)
        {
        AtPrintc(cSevCritical, "Error: dest path Id is wrong\r\n");
        return cAtFalse;
        }

    if (numberSrcPaths != numberDestPaths)
        {
        AtPrintc(cSevCritical, "Error: number of source paths are not the same with number of dest paths\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numberSrcPaths; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret = BridgeAndRollFunction(srcPaths[i], destPaths[i]);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot switch/bridge channel %s to channel %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)srcPaths[i]),
                     CliChannelIdStringGet((AtChannel)destPaths[i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static eBool LogEnable(char argc, char** argv, eBool enable)
    {
    uint32 numberPaths, i;
    AtSdhPath *paths;
    eAtRet ret;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get list of path */
    paths = CliSdhPathFromArgumentGet(argv[0], &numberPaths);
    if (numberPaths == 0)
        {
        AtPrintc(cSevNormal, "Error: ID is wrong\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numberPaths; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtChannel thisPath = (AtChannel)paths[i];
        ret = AtChannelLogEnable(thisPath, enable);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot enable logging for channel %s, ret = %s\r\n",
                     CliChannelIdStringGet(thisPath),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }
    
/* Identify and convert alarm value to string */
const char * CliSdhPathAlarm2String(uint32 alarms)
    {
    uint16 i;
    static char alarmString[64];

    /* Initialize string */
    alarmString[0] = '\0';
    if (alarms == 0)
        AtSprintf(alarmString, "None");

    /* There are alarms */
    else
        {
        for (i = 0; i < mCount(cCmdSdhPathAlarmValue); i++)
            {
            if (alarms & (cCmdSdhPathAlarmValue[i]))
                AtSprintf(alarmString, "%s%s|", alarmString, cCmdSdhPathAlarmStr[i]);
            }

        if (AtStrlen(alarmString) == 0)
            return "None";

        /* Remove the last '|' */
        alarmString[AtStrlen(alarmString) - 1] = '\0';
        }

    return alarmString;
    }

static void AutotestAlarmChangeState(AtChannel channel, uint32 changedAlarms, uint32 currentStatus, void *userData)
    {
    AtChannelObserver observer = userData;

    AtChannelObserverDefectsUpdate(observer, changedAlarms, currentStatus);

    if (changedAlarms & cAtSdhPathAlarmClockStateChange)
        AtChannelObserverClockStatePush(observer, AtChannelClockStateGet(channel));
    }

AtSdhPath* CliSdhPathFromArgumentGet(char* arg, uint32 *numberPaths)
    {
    uint32 bufferSize;

    AtSdhPath* paths = (AtSdhPath *)((void**)CliSharedChannelListGet(&bufferSize));
    *numberPaths = CliAtModuleSdhChanelsFromString(arg, (AtChannel *)((void**)paths), bufferSize);

    return paths;
    }

uint32 CliSdhPathAlarmMaskFromString(char *alarmStr)
    {
    return CliMaskFromString(alarmStr, cCmdSdhPathAlarmStr, cCmdSdhPathAlarmValue, mCount(cCmdSdhPathAlarmValue));
    }

const char **CliAtSdhPathAlarmTypeStr(uint32 *numAlarms)
    {
    *numAlarms = mCount(cCmdSdhPathAlarmStr);
    return cCmdSdhPathAlarmStr;
    }

const uint32 *CliAtSdhPathAlarmTypeVal(uint32 *numAlarms)
    {
    *numAlarms = mCount(cCmdSdhPathAlarmValue);
    return cCmdSdhPathAlarmValue;
    }

/* SET transmitted TTI */
eBool CmdSdhPathTxTti(char argc, char **argv)
    {
    return TtiSet(argc, argv, (TtiAttributeSet)AtSdhChannelTxTtiSet);
    }

/* SET expected TTI */
eBool CmdSdhPathExptTti(char argc, char **argv)
    {
    return TtiSet(argc, argv, (TtiAttributeSet)AtSdhChannelExpectedTtiSet);
    }

/* TODO: This is deprecated by CmdSdhPathTtiGet, it should be removed after autotest module is updated */
eBool CmdSdhPathRxTti(char argc, char **argv)
    {
    eAtRet ret = cAtOk;
    AtSdhPath *paths;
    tAtSdhTti tti;
    uint32 ttiMsgLen;
    uint32 numberPaths, i;
    uint32 bufferSize;
    eBool  blResult;
    tTab *tabPtr;
    char buff[16];
    const char *pPadding;
    const char *pHeading[] = {"PathId", "Tti Mode", "Message", "Padding"};

    AtUnused(argc);

    /* Get list of path */
    paths = (AtSdhPath *)CliSharedChannelListGet(&bufferSize);
    numberPaths = CliChannelListFromString(argv[0], (AtChannel *)paths, bufferSize);
    if (numberPaths == 0)
        {
        AtPrintc(cSevNormal, "Error: ID is wrong\r\n");
        ret = cAtErrorInvlParm;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numberPaths, 4, pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numberPaths; i++)
        {
        /* Initialize value */
        AtOsalMemInit(&tti, 0, sizeof(tti));

        /* Print list pathsId */
        AtSprintf(buff, "%s", CliChannelIdStringGet((AtChannel)paths[i]));
        StrToCell(tabPtr, i, 0, buff);

        /* Call Api */
        ret = AtSdhChannelRxTtiGet((AtSdhChannel)paths[i], &tti);
        tti.message[cAtSdhChannelMaxTtiLength - 2] = 0; /* If this is 64-byte message, ignore the last two bytes */
        ttiMsgLen = AtStrlen((char *)(tti.message));
        mAtEnumToStr(cCmdSdhTtiModeStr,
                     cCmdSdhTtiModeVal,
                     tti.mode,
                     buff,
                     blResult);
        StrToCell(tabPtr, i, 1, blResult ? buff : "Error");

        if (ttiMsgLen == 0)
            StrToCell(tabPtr, i, 2, "None");
        else
            StrToCell(tabPtr, i, 2, (char *)(tti.message));

        pPadding = CliSdhTtiMsgRemvPadding(tti.message);
        StrToCell(tabPtr, i, 3, pPadding);
        }
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return (ret == cAtOk) ? cAtTrue : cAtFalse;
    }

/* GET received TTI */
eBool CmdSdhPathTtiGet(char argc, char **argv)
    {
    eAtModuleSdhRet ret = cAtOk;
    AtSdhPath *paths;
    tAtSdhTti tti;
    uint8 ttiMsgLen;
    uint32 numberPaths, i;
    eBool  blResult;
    tTab *tabPtr;
    char buff[16];
    const char *pPadding;
    const char *pHeading[] = {"PathId", "txTti", "txTtiMode", "txPadding", "expTti", "expTtiMode",
                              "expPadding","rxTti", "rxTtiMode", "rxPadding", "ttiCompare"};

    AtUnused(argc);
    /* Get list of path */
    paths = CliSdhPathFromArgumentGet(argv[0], &numberPaths);
    if (numberPaths == 0)
        {
        AtPrintc(cSevNormal, "Error: ID is wrong\r\n");
        ret = cAtErrorInvlParm;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numberPaths, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numberPaths; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        /* Initialize value */
        AtOsalMemInit(&tti, 0, sizeof(tti));

        /* Print list pathsId */
        StrToCell(tabPtr, i, 0, (char *)CliChannelIdStringGet((AtChannel)paths[i]));

        /* Get Tx TTI messages */
        ret = AtSdhChannelTxTtiGet((AtSdhChannel)paths[i], &tti);
        tti.message[cAtSdhChannelMaxTtiLength - 2] = 0; /* If this is 64-byte message, ignore the last two bytes */

        pPadding = CliSdhTtiMsgRemvPadding(tti.message);
        ttiMsgLen = (uint8)AtStrlen((char *)(tti.message));
        if (ttiMsgLen == 0)
            StrToCell(tabPtr, i, 1, "None");
        else
            StrToCell(tabPtr, i, 1, (const char *)tti.message);

        mAtEnumToStr(cCmdSdhTtiModeStr,
                     cCmdSdhTtiModeVal,
                     tti.mode,
                     buff,
                     blResult);
        StrToCell(tabPtr, i, 2, blResult ? buff : "Error");
        StrToCell(tabPtr, i, 3, pPadding);

        /* Get expected TTI message */
        AtOsalMemInit(&tti, 0, sizeof(tti));
        ret = AtSdhChannelExpectedTtiGet((AtSdhChannel)paths[i], &tti);
        tti.message[cAtSdhChannelMaxTtiLength - 2] = 0; /* If this is 64-byte message, ignore the last two bytes */

        pPadding = CliSdhTtiMsgRemvPadding(tti.message);
        ttiMsgLen = (uint8)AtStrlen((char *)(tti.message));
        if (ttiMsgLen == 0)
            StrToCell(tabPtr, i, 4, "None");
        else
            StrToCell(tabPtr, i, 4, (const char *)tti.message);

        mAtEnumToStr(cCmdSdhTtiModeStr,
                     cCmdSdhTtiModeVal,
                     tti.mode,
                     buff,
                     blResult);
        StrToCell(tabPtr, i, 5, blResult ? buff : "Error");
        StrToCell(tabPtr, i, 6, pPadding);

        /* Rx Tti */
        ret = AtSdhChannelRxTtiGet((AtSdhChannel)paths[i], &tti);
        tti.message[cAtSdhChannelMaxTtiLength - 2] = 0; /* If this is 64-byte message, ignore the last two bytes */
        ttiMsgLen = (uint8)AtStrlen((char *)(tti.message));
        if (ttiMsgLen == 0)
            StrToCell(tabPtr, i, 7, "None");
        else
            StrToCell(tabPtr, i, 7, (const char *)(tti.message));

        if (ret == cAtModuleSdhErrorTtiModeMismatch)
            {
            ColorStrToCell(tabPtr, i, 8, "mismatch", cSevCritical);
            ret = cAtOk;
            }
        else
            {
        mAtEnumToStr(cCmdSdhTtiModeStr,
                     cCmdSdhTtiModeVal,
                     tti.mode,
                     buff,
                     blResult);
        StrToCell(tabPtr, i, 8, blResult ? buff : "Error");
            }

        pPadding = CliSdhTtiMsgRemvPadding(tti.message);
        StrToCell(tabPtr, i, 9, pPadding);

        /* TTI compare */
        mAtBoolToStr(AtSdhChannelTtiCompareIsEnabled((AtSdhChannel)paths[i]), buff, blResult);
        StrToCell(tabPtr, i, 10, blResult ? buff : "Error");
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return (ret == cAtOk) ? cAtTrue : cAtFalse;
    }

/* SET transmitted PSL */
eBool CmdSdhPathTxPsl(char argc, char **argv)
    {
    return HexadecimalAttributeSet(argc, argv, mAttributeSetFunc(AtSdhPathTxPslSet));
    }

/* SET expected PSL */
eBool CmdSdhPathExptPsl(char argc, char **argv)
    {
    return HexadecimalAttributeSet(argc, argv, mAttributeSetFunc(AtSdhPathExpectedPslSet));
    }

eBool CmdSdhPathTxSs(char argc, char **argv)
    {
    return HexadecimalAttributeSet(argc, argv, mAttributeSetFunc(AtSdhPathTxSsSet));
    }

eBool CmdSdhPathExpectedSs(char argc, char **argv)
    {
    uint32 numPaths, i;
    AtSdhPath *paths;
    uint8 expectedSs;
    eBool success;
    uint8 indexVal;
    eBool ssCompare = cAtTrue;
	AtUnused(argc);

    /* Initial value */
    indexVal = 0;

    /* Get list of path */
    paths = CliSdhPathFromArgumentGet(argv[indexVal], &numPaths);
    if (numPaths == 0)
        {
        AtPrintc(cSevNormal, "Error: ID is wrong\r\n");
        return cAtFalse;
        }
    indexVal++;

    /* Compare */
    mAtStrToBool(argv[1], ssCompare, success);
    if (!success)
        {
        AtPrintc(cSevCritical, "ERROR: expected en/dis\n");
        return cAtFalse;
        }

    /* Expected SS valud */
    expectedSs = (uint8)AtStrtoul(argv[2], NULL, 16);

    /* Apply */
    success = cAtTrue;
    for (i = 0; i < numPaths; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret = cAtOk;

        ret |= AtSdhPathSsCompareEnable(paths[i], ssCompare);
        ret |= AtSdhPathExpectedSsSet(paths[i], expectedSs);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot apply for %s, ret = %d\n", CliChannelIdStringGet((AtChannel)paths[i]), ret);
            success = cAtFalse;
            }
        }

    return success;
    }

/* SET interrupt mask */
eBool CmdSdhPathIntrMask(char argc, char **argv)
    {
    AtSdhPath *paths;
    uint32 numberPaths, i;
    uint32 intrMaskVal;
    eBool convertSuccess;
    eBool enable = cAtFalse;
    eBool success = cAtTrue;
	AtUnused(argc);

    /* Get list of path */
    paths = CliSdhPathFromArgumentGet(argv[0], &numberPaths);
    if (numberPaths == 0)
        {
        AtPrintc(cSevNormal, "Error: ID is wrong\r\n");
        return cAtFalse;
        }

    /* Get interrupt mask value from input string */
    if ((intrMaskVal = CliSdhPathAlarmMaskFromString(argv[1])) == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Interrupt mask is invalid\r\n");
        return cAtFalse;
        }

    /* Enabling */
    mAtStrToBool(argv[2], enable, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Expected: en/dis\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numberPaths; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret;
        AtChannel path;

        path = (AtChannel)paths[i];
        ret = AtChannelInterruptMaskSet(path, intrMaskVal, enable ? intrMaskVal : 0);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot set interrupt mask for path %s, ret = %s\r\n",
                     CliChannelIdStringGet(path),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

/* SET counter value */
eBool CmdSdhPathCounterShow(char argc, char **argv)
    {
    AtSdhPath *paths;
    uint32 numberPaths, i, counter_i;
    eBool silent = cAtFalse;
    eAtHistoryReadingMode readMode;
    tTab *tabPtr = NULL;
    char buff[16];
    const char *pHeading[] = {"PathId", "BIP ERR", "REI-P ERR", "POS DET", "NEG DET", "POS GEN", "NEG GEN", "BIP BE", "REI-P BE"};
    uint32 (*BitErrorCounterGet)(AtChannel self, uint16 counterType)      = AtChannelCounterGet;
    uint32 (*BlockErrorCounterGet)(AtSdhChannel self, uint16 counterType) = AtSdhChannelBlockErrorCounterGet;
    uint16 counterType[]={cAtSdhPathCounterTypeBip,
                          cAtSdhPathCounterTypeRei,
                          cAtSdhPathCounterTypeRxPPJC,
                          cAtSdhPathCounterTypeRxNPJC,
                          cAtSdhPathCounterTypeTxPPJC,
                          cAtSdhPathCounterTypeTxNPJC};
	AtUnused(argc);

	if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    /* Get list of path */
    paths = CliSdhPathFromArgumentGet(argv[0], &numberPaths);
    if (numberPaths == 0)
        {
        AtPrintc(cSevNormal, "Error: ID is wrong\r\n");
        return cAtFalse;
        }

    /* Reading mode */
    readMode = CliHistoryReadingModeGet(argv[1]);
    if (readMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "Invalid reading mode\r\n");
        return cAtFalse;
        }

    /* Determine what functions to read counters */
    if (readMode == cAtHistoryReadingModeReadToClear)
        {
        BitErrorCounterGet   = AtChannelCounterClear;
        BlockErrorCounterGet = AtSdhChannelBlockErrorCounterClear;
        }

    /* Create table with titles */
    silent = CliHistoryReadingShouldSilent(readMode, (argc > 2) ? argv[2] : NULL);
    if (!silent)
        {
    tabPtr = TableAlloc(numberPaths, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }
        }

    AtModuleLock((AtModule)SdhModule());

    /* Make table content */
    for (i = 0; i < numberPaths; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        uint8 column = 0;
        uint32 counterValue;

        /* Print list pathsId */
        StrToCell(tabPtr, i, column++, (char *)CliChannelIdStringGet((AtChannel)paths[i]));

        /* Show bit error and pointer adjustment counters */
        for (counter_i = 0; counter_i < mCount(counterType); counter_i++)
            {
            counterValue = BitErrorCounterGet((AtChannel)paths[i], counterType[counter_i]);
            AtSprintf(buff, "%u", counterValue);
            ColorStrToCell(tabPtr, i, column++, buff, (counterValue == 0) ? cSevNormal : cSevCritical);
            }

        /* BIP block error counters */
        counterValue = BlockErrorCounterGet((AtSdhChannel)paths[i], cAtSdhPathCounterTypeBip);
        AtSprintf(buff, "%u", counterValue);
        ColorStrToCell(tabPtr, i, column++, buff, (counterValue == 0) ? cSevNormal : cSevCritical);

        /* REI block error counters */
        counterValue = BlockErrorCounterGet((AtSdhChannel)paths[i], cAtSdhPathCounterTypeRei);
        AtSprintf(buff, "%u", counterValue);
        ColorStrToCell(tabPtr, i, column++, buff, (counterValue == 0) ? cSevNormal : cSevCritical);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    AtModuleUnLock((AtModule)SdhModule());

    AtPrintc(cSevInfo,   "* Note: \r\n");
    AtPrintc(cSevNormal, "  - ERR: Bit error\r\n");
    AtPrintc(cSevNormal, "  - BE: Block error (not all of products support this)\r\n");

    return cAtTrue;
    }

/* Show Path configuration */
eBool CmdSdhPathShowCfg(char argc, char **argv)
    {
    uint8 pslVal;
    uint32 numberPaths, i;
    uint32 intrMaskVal;
    eBool  blResult = cAtTrue;
    AtSdhPath *paths;
    tAtSdhTti tti;
    eAtTimingMode timingMd;
    eAtSdhChannelMode mode;
    tTab *tabPtr;
    char buff[16];
    const char *pHeading[] = {"PathId", "txPsl", "expPsl", "rxPsl", "sdThres", "sfThres", "tcaThres",
                              "txSs", "expSs", "intrMask", "E-RDI", "TimingMode",
                              "TimingSource", "clockState", "hwClockState", "timMon", "autoRdi", "plmMon", "Hold-off(ms)", "Hold-on(ms)", "Bound", "mode"};
    AtUnused(argc);

    /* Get list of path */
    paths = CliSdhPathFromArgumentGet(argv[0], &numberPaths);
    if (numberPaths == 0)
        {
        AtPrintc(cSevWarning, "WARNING: no channels to display\r\n");
        return cAtTrue;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numberPaths, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numberPaths; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        const char *stringValue;
        AtBerController berController;
        char *timingMdString;
        AtChannel timingSrc, boundChannel;
        AtChannel thisPath = (AtChannel)paths[i];
        eBool enabled;
        uint8 column = 0;

        /* Initial value */
        AtOsalMemInit(&tti, 0, sizeof(tti));

        /* Print list pathsId */
        StrToCell(tabPtr, i, column++, (char *)CliChannelIdStringGet(thisPath));

        /* Get transmitted PSL value */
        pslVal = AtSdhPathTxPslGet(paths[i]);
        AtSprintf(buff, "0x%02x", pslVal);
        StrToCell(tabPtr, i, column++, buff);

        /* Get expected PSL value */
        pslVal = AtSdhPathExpectedPslGet(paths[i]);
        AtSprintf(buff, "0x%02x", pslVal);
        StrToCell(tabPtr, i, column++, buff);

        /* Get received PSL value */
        pslVal = AtSdhPathRxPslGet(paths[i]);
        AtSprintf(buff, "0x%02x", pslVal);
        StrToCell(tabPtr, i, column++, buff);

        /* Get BER-SD/SF/TCA threshold value */
        berController = AtSdhChannelBerControllerGet((AtSdhChannel)paths[i]);
        if (berController)
            {
            stringValue = CliBerRateString(AtBerControllerSdThresholdGet(berController));
            StrToCell(tabPtr, i, column++, stringValue ? stringValue : "Error");
            stringValue = CliBerRateString(AtBerControllerSfThresholdGet(berController));
            StrToCell(tabPtr, i, column++, stringValue ? stringValue : "Error");
            stringValue = CliBerRateString(AtBerControllerTcaThresholdGet(berController));
            StrToCell(tabPtr, i, column++, stringValue ? stringValue : "Error");
            }
        else
            {
            StrToCell(tabPtr, i, column++, sAtNotCare);
            StrToCell(tabPtr, i, column++, sAtNotCare);
            StrToCell(tabPtr, i, column++, sAtNotCare);
            }

        /* SS */
        if (AtSdhPathSsInsertionIsEnabled((AtSdhPath)paths[i]))
            AtSprintf(buff, "0x%x", AtSdhPathTxSsGet((AtSdhPath)paths[i]));
        else
            AtSprintf(buff, "%s", "dis");
        StrToCell(tabPtr, i, column++, buff);

        if (AtSdhPathSsCompareIsEnabled((AtSdhPath)paths[i]))
            AtSprintf(buff, "0x%x", AtSdhPathExpectedSsGet((AtSdhPath)paths[i]));
        else
            AtSprintf(buff, "dis");
        StrToCell(tabPtr, i, column++, buff);

        /* Print interrupt mask values */
        intrMaskVal = AtChannelInterruptMaskGet(thisPath);
        StrToCell(tabPtr, i, column++, CliSdhPathAlarm2String(intrMaskVal));

        /* Print Enhanced RDI enable/disable */
        mAtBoolToStr(AtSdhPathERdiIsEnabled((AtSdhPath)paths[i]), buff, blResult);
        StrToCell(tabPtr, i, column++, blResult ? buff : "Error");

        /* Timing mode */
        timingMd = AtChannelTimingModeGet(thisPath);
        timingMdString = CliTimingModeString(timingMd);
        ColorStrToCell(tabPtr, i, column++, timingMdString ? timingMdString : "Error", timingMdString ? cSevNormal : cSevCritical);

        /* Timing source */
        timingSrc = AtChannelTimingSourceGet(thisPath);
        StrToCell(tabPtr, i, column++, timingSrc ? CliChannelIdStringGet(timingSrc) : "none");

        /* Clock state */
        StrToCell(tabPtr, i, column++, CliClockStateString(AtChannelClockStateGet(thisPath)));
        AtSprintf(buff, "%d", AtChannelHwClockStateGet(thisPath));
        StrToCell(tabPtr, i, column++, buff);

        /* TIM monitoring */
        mAtBoolToStr(AtSdhChannelTimMonitorIsEnabled((AtSdhChannel)paths[i]), buff, blResult);
        StrToCell(tabPtr, i, column++, blResult ? buff : "Error");

        /* Auto RDI */
        enabled = AtSdhChannelAutoRdiIsEnabled((AtSdhChannel)paths[i]);
        ColorStrToCell(tabPtr, i, column++, CliBoolToString(enabled), enabled ? cSevInfo : cSevCritical);

        /* PLM monitoring */
        mAtBoolToStr(AtSdhPathPlmMonitorIsEnabled(paths[i]), buff, blResult);
        StrToCell(tabPtr, i, column++, blResult ? buff : "Error");

        /* Hold-off timer */
        AtSprintf(buff, "%u", AtSdhChannelHoldOffTimerGet((AtSdhChannel)paths[i]));
        StrToCell(tabPtr, i, column++, buff);

        /* Hold-on timer */
        AtSprintf(buff, "%u", AtSdhChannelHoldOnTimerGet((AtSdhChannel)paths[i]));
        StrToCell(tabPtr, i, column++, buff);

        /* Bound channel */
        boundChannel = (AtChannel)AtChannelBoundChannelGet((AtChannel)paths[i]);
        if (boundChannel)
            ColorStrToCell(tabPtr, i, column++, CliChannelIdStringGet(boundChannel), cSevInfo);
        else
            ColorStrToCell(tabPtr, i, column++, "None", cSevNormal);

        /* Print mode */
        mode = AtSdhChannelModeGet((AtSdhChannel)paths[i]);
        mAtEnumToStr(cCmdSdhChannelModeStr, cCmdSdhChannelModeVal, mode, buff, blResult);
        ColorStrToCell(tabPtr, i, column++, blResult ? buff : "Error", blResult ? cSevNormal : cSevCritical);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

/* Show Path alarm */
eBool CmdAtSdhPathShowAlarm(char argc, char **argv)
    {
    uint32 numberPaths, i;
    uint32 j;
    uint32 alarmStat;
    tTab *tabPtr;
    AtSdhPath *paths;
    const char *pHeading[] = {"PathId", "AIS", "LOP", "TIM", "UNEQ", "P-UNEQ", "PLM", "RDI", "ERDI-S",
                              "ERDI-P", "ERDI-C", "BER-SD", "BER-SF", "BER-TCA", "LOM", "RFI"};
	AtUnused(argc);

    /* Get list of path */
    paths = CliSdhPathFromArgumentGet(argv[0], &numberPaths);
    if (numberPaths == 0)
        {
        AtPrintc(cSevCritical, "ERROR: ID is wrong\r\n");
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numberPaths, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numberPaths; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        /* Print list pathsId */
        StrToCell(tabPtr, i, 0, CliChannelIdStringGet((AtChannel)paths[i]));

        /* Print alarm status */
        alarmStat = AtChannelAlarmGet((AtChannel)paths[i]);
        for (j = 0; j < mCount(cCmdSdhPathAlarmValue); j++)
            {
            if (alarmStat & cCmdSdhPathAlarmValue[j])
                ColorStrToCell(tabPtr, i, j + 1, "set", cSevCritical);
            else
                ColorStrToCell(tabPtr, i, j + 1, "clear", cSevInfo);
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

/* Show Path alarm */
eBool CmdAtSdhPathShowListenedAlarm(char argc, char **argv)
    {
    uint32 numberPaths, i;
    uint32 j;
    uint32 alarmStat;
    tTab *tabPtr;
    AtSdhPath *paths;
    const char *pHeading[] = {"PathId", "Count", "Matched", "AIS", "LOP", "TIM", "UNEQ", "P-UNEQ", "PLM", "RDI", "ERDI-S",
                              "ERDI-P", "ERDI-C", "BER-SD", "BER-SF", "BER-TCA", "LOM", "RFI"};
    uint32 (*ListenedDefectGet)(AtChannel self, uint32 * listenedCount);
    eAtHistoryReadingMode readingMode;
    uint32 counter;
    AtUnused(argc);

    /* Get list of path */
    paths = CliSdhPathFromArgumentGet(argv[0], &numberPaths);
    if (numberPaths == 0)
        {
        AtPrintc(cSevCritical, "ERROR: ID is wrong\r\n");
        return cAtFalse;
        }

    ListenedDefectGet = AtChannelListenedDefectGet;
    if (argc > 1)
        {
        /* Get reading action mode */
        readingMode = CliHistoryReadingModeGet(argv[1]);
        if (readingMode == cAtHistoryReadingModeUnknown)
            {
            AtPrintc(cSevCritical, "ERROR: Invalid reading mode. Expected %s\r\n", CliValidHistoryReadingModes());
            return cAtFalse;
            }

        if (readingMode == cAtHistoryReadingModeReadToClear)
            ListenedDefectGet = AtChannelListenedDefectClear;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numberPaths, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numberPaths; i++)
        {
        /* Print list pathsId */
        StrToCell(tabPtr, i, 0, CliChannelIdStringGet((AtChannel)paths[i]));

        /* Print alarm status */
        alarmStat = ListenedDefectGet((AtChannel)paths[i], &counter);
        StrToCell(tabPtr, i, 1, CliNumber2String(counter, "%u"));
        StrToCell(tabPtr, i, 2, "TBD");

        for (j = 0; j < mCount(cCmdSdhPathAlarmValue); j++)
            {
            if (alarmStat & cCmdSdhPathAlarmValue[j])
                ColorStrToCell(tabPtr, i, j + 3, "set", cSevCritical);
            else
                ColorStrToCell(tabPtr, i, j + 3, "clear", cSevInfo);
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

/* Show Path interrupt */
eBool CmdAtSdhPathShowInterrupt(char argc, char **argv)
    {
    uint32 numberPaths, i;
    uint32 j;
    uint32 alarmStat;
    eAtRet ret;
    eBool silent = cAtFalse;
    eAtHistoryReadingMode readMode;
    tTab *tabPtr = NULL;
    AtSdhPath *paths;
    const char *pHeading[] = {"PathId", "AIS", "LOP", "TIM", "UNEQ", "P-UNEQ", "PLM", "RDI", "ERDI-S",
                              "ERDI-P", "ERDI-C", "BER-SD", "BER-SF", "BER-TCA", "LOM", "RFI", "ClockStateChange",
                              "TTI Change", "PSL Change", "PI-NDF", "PG-NDF", "NewPointer"};
	AtUnused(argc);

    /* Initial value */
    ret = cAtOk;

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    /* Get list of path */
    paths = CliSdhPathFromArgumentGet(argv[0], &numberPaths);
    if (numberPaths == 0)
        {
        AtPrintc(cSevNormal, "Error: ID is wrong\r\n");
        ret = cAtErrorInvlParm;
        }

    readMode = CliHistoryReadingModeGet(argv[1]);
    if (readMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    /* Create table with titles */
    silent = CliHistoryReadingShouldSilent(readMode, (argc > 2) ? argv[2] : NULL);
    if (!silent)
        {
    	tabPtr = TableAlloc(numberPaths, mCount(pHeading), pHeading);
    	if (!tabPtr)
     	   	{
     	   	AtPrintc(cSevCritical, "Cannot allocate table\r\n");
     	   	return cAtFalse;
        	}
        }

    /* Make table content */
    for (i = 0; i < numberPaths; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        /* Print list pathsId */
        StrToCell(tabPtr, i, 0, (char *)CliChannelIdStringGet((AtChannel)paths[i]));

        /* Print alarm status */
        if (CliHistoryReadingModeGet(argv[1]) == cAtHistoryReadingModeReadToClear)
            alarmStat = AtChannelAlarmInterruptClear((AtChannel)paths[i]);
        else
            alarmStat = AtChannelAlarmInterruptGet((AtChannel)paths[i]);

        for (j = 0; j < mCount(cCmdSdhPathAlarmValue); j++)
            {
            if (alarmStat & cCmdSdhPathAlarmValue[j])
                ColorStrToCell(tabPtr, i, j + 1, "set", cSevCritical);
            else
                ColorStrToCell(tabPtr, i, j + 1, "clear", cSevInfo);
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return (ret == cAtOk) ? cAtTrue: cAtFalse;
    }

eBool CmdSdhPathForceAlarm(char argc, char **argv)
    {
    uint32 numberPaths, i;
    AtSdhPath *paths;
    uint32 alarmValue;
    eBool  blResult, success = cAtTrue;
    eBool  force = cAtFalse;
    eAtDirection direction;
    eAtRet (*AlarmForce)(AtChannel self, uint32 alarmType);
	AtUnused(argc);

    /* Get list of path */
    paths = CliSdhPathFromArgumentGet(argv[0], &numberPaths);
    if (numberPaths == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No channel to force, ID list may be wrong\r\n");
        return cAtFalse;
        }

    /* Alarm types */
    if (argv[1] != NULL)
        alarmValue = CliSdhPathAlarmMaskFromString(argv[1]);
    else
        {
        AtPrintc(cSevCritical, "ERROR: Invalid alarm type\r\n");
        return cAtFalse;
        }

    /* Direction */
    direction = CliDirectionFromStr(argv[2]);
    if(direction == cAtDirectionInvalid)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid direction. Expect: %s\r\n", CliValidDirections());
        return cAtFalse;
        }

    /* Enable/disable */
    mAtStrToBool(argv[3], force, blResult);
    if(!blResult)
        {
        AtPrintc(cSevCritical, "ERROR: expect en/dis, not %s\r\n", argv[3]);
        return cAtFalse;
        }

    /* Determine function to use */
    if (direction == cAtDirectionTx)
        AlarmForce = force ? AtChannelTxAlarmForce : AtChannelTxAlarmUnForce;
    else
        AlarmForce = force ? AtChannelRxAlarmForce : AtChannelRxAlarmUnForce;

    /* Force */
    for (i = 0; i < numberPaths; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret;
        ret = AlarmForce((AtChannel)paths[i], alarmValue);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot force alarm on %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)paths[i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        if (CliTimeLoggingIsEnabled())
            DefectLogging((AtChannel)paths[i], alarmValue, force);
        }

    return success;
    }

eBool CmdSdhPathAlarmEnable(char argc, char **argv)
    {
    return AlarmEnable(argc, argv, cAtTrue);
    }

/*
 * Disable Path alarm
 */
eBool CmdSdhPathAlarmDisable(char argc, char **argv)
    {
    return AlarmEnable(argc, argv, cAtFalse);
    }

/*
 * Show path alarm affect
 */
eBool CmdSdhPathAlarmAffectGet(char argc, char **argv)
    {
    uint32 numberPaths, i, j;
    eAtRet ret;
    tTab *tabPtr;
    char buff[16];
    AtSdhPath *paths;
    eBool  isEnable;
    const char *pHeading[] = {"PathId", "AIS", "LOP", "TIM", "UNEQ", "P-UNEQ", "PLM", "RDI", "ERDI-S",
                              "ERDI-P", "ERDI-C", "BER-SD", "BER-SF", "BER-TCA", "LOM", "RFI"};
	AtUnused(argc);

    /* Initial value */
    ret = cAtOk;

    /* Get list of path */
    paths = CliSdhPathFromArgumentGet(argv[0], &numberPaths);
    if (numberPaths == 0)
        {
        AtPrintc(cSevNormal, "Error: ID is wrong\r\n");
        ret = cAtErrorInvlParm;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numberPaths, 14, pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numberPaths; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        /* Print list pathsId */
        StrToCell(tabPtr, i, 0, (char *)CliChannelIdStringGet((AtChannel)paths[i]));

        /* Print alarm status */
        for (j = 0; j < mCount(cCmdSdhPathAlarmValue); j++)
            {
            isEnable = AtSdhChannelAlarmAffectingIsEnabled((AtSdhChannel)paths[i], cCmdSdhPathAlarmValue[j]);
            AtSprintf(buff, "%s", (isEnable ? "en" : "dis"));
            ColorStrToCell(tabPtr, i, j+1, buff, isEnable ? cSevNormal : cSevCritical);
            }
        }
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return (ret == cAtOk) ? cAtTrue: cAtFalse;
    }

eBool CmdSdhPathERdiEnable(char argc, char **argv)
    {
    return BoolAttributeSet(argc, argv, cAtTrue, (BooldAttributeSetFunc)AtSdhPathERdiEnable);
    }

eBool CmdSdhPathERdiDisable(char argc, char **argv)
    {
    return BoolAttributeSet(argc, argv, cAtFalse, (BooldAttributeSetFunc)AtSdhPathERdiEnable);
    }

eBool CmdSdhPathForceError(char argc, char** argv)
    {
    uint32 numberPaths, i;
    eBool blResult;
    AtSdhPath *paths;
    uint32 errorType = cAtSdhPathErrorNone;
    eBool forceNone = cAtFalse;
    eBool success = cAtTrue;

	AtUnused(argc);

    /* Get list of path */
    paths = CliSdhPathFromArgumentGet(argv[0], &numberPaths);
    if (numberPaths == 0)
        {
        AtPrintc(cSevNormal, "Error: ID is wrong\r\n");
        return cAtFalse;
        }

    /* Get error counter type */
    if (AtStrcmp(argv[1], "none") == 0)
        forceNone = cAtTrue;
    else
        {
        blResult = cAtFalse;
        mAtStrToEnum(cCmdSdhPathErrorTypeStr,
                     cCmdSdhPathErrorTypeVal,
                     argv[1],
                     errorType,
                     blResult);
        if (!blResult)
            {
            AtPrintc(cSevCritical, "ERROR: Invalid error type <mode> , expected bip, rei, none\r\n");
            return cAtFalse;
            }
        }

    for (i = 0; i < numberPaths; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        if (forceNone)
            {
            AtChannelTxErrorUnForce((AtChannel)paths[i], cAtSdhPathErrorBip);
            AtChannelTxErrorUnForce((AtChannel)paths[i], cAtSdhPathErrorRei);
            }
        else
            {
            eAtRet ret = AtChannelTxErrorForce((AtChannel)paths[i], (uint8)errorType);

            if (ret != cAtOk)
                {
                AtPrintc(cSevCritical, "ERROR: Force error on %s fail with ret = %s\r\n",
                         CliChannelIdStringGet((AtChannel)paths[i]),
                         AtRet2String(ret));
                success = cAtFalse;
                }
            }
        }

    return success;
    }

eBool CmdAtSdhPathTimMonitorEnable(char argc, char **argv)
    {
    AtUnused(argc);
    return AttributeSet(argv[0], cAtTrue, mAttributeSetFunc(AtSdhChannelTimMonitorEnable));
    }

eBool CmdAtSdhPathTimMonitorDisable(char argc, char **argv)
    {
    AtUnused(argc);
    return AttributeSet(argv[0], cAtFalse, mAttributeSetFunc(AtSdhChannelTimMonitorEnable));
    }

eBool CmdSdhPathTimingSet(char argc, char **argv)
    {
    uint32 numberPaths, bufferSize, i;
    AtChannel *sdhVcs, timingSrc;
    eAtTimingMode timingMode;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get list of paths */
    sdhVcs = CliSharedChannelListGet(&bufferSize);
    numberPaths = CliAtModuleSdhChanelsFromString(argv[0], (AtChannel *)((void**)sdhVcs), bufferSize);
    if (numberPaths == 0)
        {
        AtPrintc(cSevNormal, "No channels, check hierarchy\r\n");
        return cAtFalse;
        }

    /* Get timing mode */
    timingMode = CliTimingModeFromString(argv[1]);
    if(timingMode == cAtTimingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid timing mode parameter. Expected: %s\r\n", CliValidTimingModes());
        return cAtFalse;
        }

    /* Get timing source */
    timingSrc = NULL;
    if ((timingMode == cAtTimingModeSlave) ||
        (timingMode == cAtTimingModeSdhLineRef))
        {
        uint32 numMaster;
        numMaster = CliAtModuleSdhChanelsFromString(argv[2], &timingSrc, 1);
        if (numMaster != 1)
            {
            AtPrintc(cSevCritical, "ERROR: Timing source is not correct or too many timing sources\r\n");
            return cAtFalse;
            }
        }

    for (i = 0; i < numberPaths; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret = AtChannelTimingSet(sdhVcs[i], timingMode, timingSrc);

        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical, "ERROR: Set timing on %s fail with ret = %s\r\n",
                 CliChannelIdStringGet(sdhVcs[i]),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

eBool CmdSdhPathForcedAlarmsGet(char argc, char **argv)
    {
    uint32 numberPaths, i;
    AtSdhPath *sdhPaths;
    tTab *tabPtr;
    const char *heading[] = {"PathId", "ForcedTxAlarms", "ForcedRxAlarms"};
	AtUnused(argc);

    sdhPaths = CliSdhPathFromArgumentGet(argv[0], &numberPaths);
    if (numberPaths == 0)
        {
        AtPrintc(cSevNormal, "Error: ID is wrong\r\n");
        return cAtFalse;
        }

    tabPtr = TableAlloc(numberPaths, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numberPaths; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        uint32 alarmMask;
        AtChannel sdhPath = (AtChannel)sdhPaths[i];

        /* Print list pathsId */
        StrToCell(tabPtr, i, 0, (char *)CliChannelIdStringGet(sdhPath));

        alarmMask = AtChannelTxForcedAlarmGet(sdhPath);
        ColorStrToCell(tabPtr, i, 1, CliSdhPathAlarm2String(alarmMask), alarmMask ? cSevCritical : cSevInfo);

        alarmMask = AtChannelRxForcedAlarmGet(sdhPath);
        ColorStrToCell(tabPtr, i, 2, CliSdhPathAlarm2String(alarmMask), alarmMask ? cSevCritical : cSevInfo);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

/* SET transmitted Overhead byte */
eBool CmdAtSdhPathTxOverheadByteSet(char argc, char **argv)
    {
    eAtRet ret;
    uint32 numberPaths, i, j;
    uint32 ohBytesList;
    uint8  ohByteValue;
    AtSdhPath *paths;
    eBool success = cAtTrue;
	AtUnused(argc);

    paths = CliSdhPathFromArgumentGet(argv[0], &numberPaths);
    if (numberPaths == 0)
        {
        AtPrintc(cSevNormal, "ERROR: ID is wrong\r\n");
        return cAtFalse;
        }

    ohBytesList = CliSdhPathOverheadBytesFromString(argv[1]);
    if (ohBytesList == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid overhead bytes\r\n");
        return cAtFalse;
        }

    ohByteValue = (uint8)AtStrToDw(argv[2]);

    for (i = 0; i < numberPaths; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        for (j = 0; j < mCount(cCmdSdhPathOverheadByteStr); j++)
            {
            uint32 ohByte = ohBytesList & cCmdSdhPathOverheadByteValue[j];
            if (ohByte == 0)
                continue;

            ret = AtSdhChannelTxOverheadByteSet((AtSdhChannel)paths[i], ohByte, ohByteValue);
            if (ret != cAtOk)
                {
                AtPrintc(cSevCritical,
                         "ERROR: Cannot configure %s, ret = %s\r\n",
                         CliChannelIdStringGet((AtChannel)paths[i]),
                         AtRet2String(ret));
                success = cAtFalse;
                }
            }
        }

    return success;
    }

eBool CmdAtSdhPathOverheadByteGet(char argc, char **argv)
    {
    uint32 numberPaths, i;
    eBool isLowPath;
    eAtSdhChannelType channelType;
    AtSdhPath *sdhPaths;
    tTab *tabPtr;
    char buf[10];
    const char *highPathheading[] = {"PathId", "txN1", "rxN1", "txC2", "rxC2", "txG1", "rxG1", "txF2", "rxF2", "txH4", "rxH4", "txF3", "rxF3", "txK3", "rxK3"};
    const char *lowPathheading[]  = {"PathId", "txV5", "rxV5", "txN2", "rxN2", "txK4", "rxK4"};
	AtUnused(argc);

    sdhPaths = CliSdhPathFromArgumentGet(argv[0], &numberPaths);
    if (numberPaths == 0)
        {
        AtPrintc(cSevNormal, "Error: ID is wrong\r\n");
        return cAtFalse;
        }

    channelType =  AtSdhChannelTypeGet((AtSdhChannel)sdhPaths[0]);
    isLowPath   = ((channelType == cAtSdhChannelTypeVc12) || (channelType == cAtSdhChannelTypeVc11)) ? cAtTrue : cAtFalse;

    if (isLowPath)
        tabPtr = TableAlloc(numberPaths, mCount(lowPathheading), lowPathheading);
    else
        tabPtr = TableAlloc(numberPaths, mCount(highPathheading), highPathheading);

    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numberPaths; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtSdhChannel sdhPath = (AtSdhChannel)sdhPaths[i];

        /* Print list pathsId */
        StrToCell(tabPtr, i, 0, (char *)CliChannelIdStringGet((AtChannel)sdhPath));

        if (isLowPath)
            {
            AtSprintf(buf, "0x%x", AtSdhChannelTxOverheadByteGet(sdhPath, cAtSdhPathOverheadByteV5));
            ColorStrToCell(tabPtr, i, 1, buf, cSevNormal);
            AtSprintf(buf, "0x%x", AtSdhChannelRxOverheadByteGet(sdhPath, cAtSdhPathOverheadByteV5));
            ColorStrToCell(tabPtr, i, 2, buf, cSevNormal);

            AtSprintf(buf, "0x%x", AtSdhChannelTxOverheadByteGet(sdhPath, cAtSdhPathOverheadByteN2));
            ColorStrToCell(tabPtr, i, 3, buf, cSevNormal);
            AtSprintf(buf, "0x%x", AtSdhChannelRxOverheadByteGet(sdhPath, cAtSdhPathOverheadByteN2));
            ColorStrToCell(tabPtr, i, 4, buf, cSevNormal);

            AtSprintf(buf, "0x%x", AtSdhChannelTxOverheadByteGet(sdhPath, cAtSdhPathOverheadByteK4));
            ColorStrToCell(tabPtr, i, 5, buf, cSevNormal);
            AtSprintf(buf, "0x%x", AtSdhChannelRxOverheadByteGet(sdhPath, cAtSdhPathOverheadByteK4));
            ColorStrToCell(tabPtr, i, 6, buf, cSevNormal);
            }
        else
            {
            AtSprintf(buf, "0x%x", AtSdhChannelTxOverheadByteGet(sdhPath, cAtSdhPathOverheadByteN1));
            ColorStrToCell(tabPtr, i, 1, buf, cSevNormal);
            AtSprintf(buf, "0x%x", AtSdhChannelRxOverheadByteGet(sdhPath, cAtSdhPathOverheadByteN1));
            ColorStrToCell(tabPtr, i, 2, buf, cSevNormal);

            AtSprintf(buf, "0x%x", AtSdhChannelTxOverheadByteGet(sdhPath, cAtSdhPathOverheadByteC2));
            ColorStrToCell(tabPtr, i, 3, buf, cSevNormal);
            AtSprintf(buf, "0x%x", AtSdhChannelRxOverheadByteGet(sdhPath, cAtSdhPathOverheadByteC2));
            ColorStrToCell(tabPtr, i, 4, buf, cSevNormal);

            AtSprintf(buf, "0x%x", AtSdhChannelTxOverheadByteGet(sdhPath, cAtSdhPathOverheadByteG1));
            ColorStrToCell(tabPtr, i, 5, buf, cSevNormal);
            AtSprintf(buf, "0x%x", AtSdhChannelRxOverheadByteGet(sdhPath, cAtSdhPathOverheadByteG1));
            ColorStrToCell(tabPtr, i, 6, buf, cSevNormal);

            AtSprintf(buf, "0x%x", AtSdhChannelTxOverheadByteGet(sdhPath, cAtSdhPathOverheadByteF2));
            ColorStrToCell(tabPtr, i, 7, buf, cSevNormal);
            AtSprintf(buf, "0x%x", AtSdhChannelRxOverheadByteGet(sdhPath, cAtSdhPathOverheadByteF2));
            ColorStrToCell(tabPtr, i, 8, buf, cSevNormal);

            AtSprintf(buf, "0x%x", AtSdhChannelTxOverheadByteGet(sdhPath, cAtSdhPathOverheadByteH4));
            ColorStrToCell(tabPtr, i, 9, buf, cSevNormal);
            AtSprintf(buf, "0x%x", AtSdhChannelRxOverheadByteGet(sdhPath, cAtSdhPathOverheadByteH4));
            ColorStrToCell(tabPtr, i, 10, buf, cSevNormal);

            AtSprintf(buf, "0x%x", AtSdhChannelTxOverheadByteGet(sdhPath, cAtSdhPathOverheadByteF3));
            ColorStrToCell(tabPtr, i, 11, buf, cSevNormal);
            AtSprintf(buf, "0x%x", AtSdhChannelRxOverheadByteGet(sdhPath, cAtSdhPathOverheadByteF3));
            ColorStrToCell(tabPtr, i, 12, buf, cSevNormal);

            AtSprintf(buf, "0x%x", AtSdhChannelTxOverheadByteGet(sdhPath, cAtSdhPathOverheadByteK3));
            ColorStrToCell(tabPtr, i, 13, buf, cSevNormal);
            AtSprintf(buf, "0x%x", AtSdhChannelRxOverheadByteGet(sdhPath, cAtSdhPathOverheadByteK3));
            ColorStrToCell(tabPtr, i, 14, buf, cSevNormal);
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdSdhPathLoopbackSet(char argc, char **argv)
    {
    eAtLoopbackMode loopbackMode = cAtLoopbackModeRelease;
    eBool convertSuccess;
	AtUnused(argc);

    mAtStrToEnum(cCmdLoopbackModeStr,
                 cCmdLoopbackModeVal,
                 argv[1],
                 loopbackMode,
                 convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid parameter <loopbackMode> , expected release|local|remote\r\n");
        return cAtFalse;
        }

    return AttributeSet(argv[0], loopbackMode, (eAtRet (*)(AtSdhPath, uint32))AtChannelLoopbackSet);
    }

/*
 * Get Loopback
 */
eBool CmdSdhPathLoopbackGet(char argc, char **argv)
    {
    uint32 numberPaths, i;
    uint32 bufferSize;
    AtSdhChannel *paths;
    eBool blResult;
    eAtLoopbackMode loopbackMode;

    tTab *tabPtr;
    const char *heading[] ={"PathId", "Loopback"};
	AtUnused(argc);

    /* Initialize value */
    paths = (AtSdhChannel *)((void*)CliSharedChannelListGet(&bufferSize));
    numberPaths = CliAtModuleSdhChanelsFromString(argv[0], (AtChannel *)((void*)paths), bufferSize);
    if (numberPaths == 0)
        {
        AtPrintc(cSevNormal, "ERROR: ID is wrong\r\n");
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numberPaths, 2, heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numberPaths; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        char buff[16];

        AtOsalMemInit(buff, 0, sizeof(buff));

        /* Print ID */
        StrToCell(tabPtr, i, 0, CliChannelIdStringGet((AtChannel)paths[i]));
        loopbackMode = AtChannelLoopbackGet((AtChannel)paths[i]);

        mAtEnumToStr(cCmdLoopbackModeStr,
                     cCmdLoopbackModeVal,
                     loopbackMode,
                     buff,
                     blResult);
        ColorStrToCell(tabPtr, i, 1, blResult ? buff : "Error", blResult ? cSevInfo : cSevCritical);
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtSdhPathDebug(char argc, char **argv)
    {
    uint32 numberPaths, i;
    AtChannel *sdhPaths = CliSharedChannelListGet(&numberPaths);
    AtUnused(argc);

    numberPaths = CliAtModuleSdhChanelsFromString(argv[0], sdhPaths, numberPaths);
    if (numberPaths == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No path to display debug information, the ID list may be wrong, press ? or see manual for more detail\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numberPaths; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        AtChannelDebug(sdhPaths[i]);

    return cAtTrue;
    }

eBool CmdAtSdhPathAutoRdiEnable(char argc, char **argv)
    {
    return AutoRdiEnable(argc, argv, cAtTrue);
    }

eBool CmdAtSdhPathAutoRdiDisable(char argc, char **argv)
    {
    return AutoRdiEnable(argc, argv, cAtFalse);
    }

eBool CmdAtSdhPathAlarmCapture(char argc, char **argv)
    {
    AtSdhPath *paths;
    uint32 numberPaths, i;
    uint32 bufferSize;
    eBool convertSuccess;
    eBool success = cAtTrue;
    eBool captureEnabled = cAtFalse;
    static tAtChannelEventListener listener;
	AtUnused(argc);

    /* Get list of path */
    paths = (AtSdhPath *)CliSharedChannelListGet(&bufferSize);
    numberPaths = CliAtModuleSdhChanelsFromString(argv[0], (AtChannel *)paths, bufferSize);
    if (numberPaths == 0)
        {
        AtPrintc(cSevNormal, "Error: ID is wrong\r\n");
        return cAtFalse;
        }

    /* Enabling */
    mAtStrToBool(argv[1], captureEnabled, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enabling mode, expected en/dis\r\n");
        return cAtFalse;
        }

    AtOsalMemInit(&listener, 0, sizeof(listener));
    if (AutotestIsEnabled())
        listener.AlarmChangeStateWithUserData = AutotestAlarmChangeState;
    else if (CliInterruptLogIsEnabled())
        listener.AlarmChangeState = AlarmChangeLogging;
    else
        listener.AlarmChangeState = AlarmChangeState;

    for (i = 0; i < numberPaths; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret;
        AtChannel path;

        path = (AtChannel)paths[i];

        if (captureEnabled)
            {
            if (AutotestIsEnabled())
                ret = AtChannelEventListenerAddWithUserData(path, &listener, CliAtSdhPathObserverGet(path));
            else
                ret = AtChannelEventListenerAdd(path, &listener);
            }
        else
            {
            ret = AtChannelEventListenerRemove(path, &listener);

            if (AutotestIsEnabled())
                CliAtSdhPathObserverDelete(path);
            }

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot %s alarm listener for %s, ret = %s\r\n",
                     captureEnabled ? "register" : "deregister",
                     CliChannelIdStringGet(path),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtSdhPathRxTrafficSwitch(char argc, char **argv)
    {
    return CliBridgeAndRollFunction(argc, argv, AtSdhChannelRxTrafficSwitch);
    }

eBool CmdAtSdhPathTxTrafficBridge(char argc, char **argv)
    {
    return CliBridgeAndRollFunction(argc, argv, AtSdhChannelTxTrafficBridge);
    }

eBool CmdAtSdhPathTxTrafficCut(char argc, char **argv)
    {
    AtSdhChannel *srcPaths;
    uint32 numberSrcPaths, i;
    uint32 bufferSize;
    eBool success = cAtTrue;
    AtUnused(argc);

    /* Get list of source path */
    srcPaths = (AtSdhChannel *)CliSharedChannelListGet(&bufferSize);
    numberSrcPaths = CliAtModuleSdhChanelsFromString(argv[0], (AtChannel *)srcPaths, bufferSize);
    if (numberSrcPaths == 0)
        {
        AtPrintc(cSevCritical, "Error: source path Id is wrong\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numberSrcPaths; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret = AtSdhChannelTxTrafficCut(srcPaths[i]);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot cut traffic of channel %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)srcPaths[i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtSdhPathConcate(char argc, char **argv)
    {
    eAtRet ret;
    AtSdhChannel master;
    AtSdhChannel *paths;
    uint32 numMasters;
    uint32 numSlaves;
    uint32 bufferSize;
    AtUnused(argc);

    /* Get list of source path */
    paths = (AtSdhChannel *)CliSharedChannelListGet(&bufferSize);
    numMasters = CliAtModuleSdhChanelsFromString(argv[0], (AtChannel *)paths, bufferSize);
    if (numMasters != 1)
        {
        AtPrintc(cSevCritical, "Error: Master channel Id is wrong\r\n");
        return cAtFalse;
        }

    /* Store master variable to use path list for other purpose */
    master = paths[0];

    numSlaves = CliAtModuleSdhChanelsFromString(argv[1], (AtChannel *)paths, bufferSize);
    if (numSlaves == 0)
        {
        AtPrintc(cSevCritical, "Error: slave channel Id is wrong\r\n");
        return cAtFalse;
        }

    if ((ret = AtSdhChannelConcate(master, paths, (uint8)numSlaves)) != cAtOk)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Cannot concate master %s to slave list, ret = %s\r\n",
                 CliChannelIdStringGet((AtChannel)master),
                 AtRet2String(ret));
        }

    return cAtTrue;
    }

eBool CmdAtSdhPathDeconcate(char argc, char **argv)
    {
    eAtRet ret;
    AtSdhChannel *masters;
    uint32 numMasters, i;
    uint32 bufferSize;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get list of source path */
    masters = (AtSdhChannel *)CliSharedChannelListGet(&bufferSize);
    numMasters = CliAtModuleSdhChanelsFromString(argv[0], (AtChannel *)masters, bufferSize);
    if (numMasters == 0)
        {
        AtPrintc(cSevCritical, "Error: Master channel Id is wrong\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numMasters; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        if ((ret = AtSdhChannelDeconcate(masters[i])) != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot deconcate master %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)masters[i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtSdhPathConcateGet(char argc, char **argv)
    {
    uint32 numberPaths, i;
    uint32 bufferSize;
    AtSdhChannel *paths;
    tTab *tabPtr;
    const char *heading[] ={"PathId", "Slave channels"};
    AtUnused(argc);

    /* Initialize value */
    paths = (AtSdhChannel *)((void*)CliSharedChannelListGet(&bufferSize));
    numberPaths = CliAtModuleSdhChanelsFromString(argv[0], (AtChannel *)((void*)paths), bufferSize);
    if (numberPaths == 0)
        {
        AtPrintc(cSevNormal, "ERROR: ID is wrong\r\n");
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numberPaths, 2, heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numberPaths; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        static char buff[350];
        uint8 j;
        uint8 numSlaves = AtSdhChannelNumSlaveChannels((AtSdhChannel)paths[i]);

        AtOsalMemInit(buff, 0, sizeof(buff));

        /* Print ID */
        StrToCell(tabPtr, i, 0, CliChannelIdStringGet((AtChannel)paths[i]));

        for (j = 0; j < numSlaves; j++)
            {
            AtStrcat(buff, ",");
            AtStrcat(buff, CliChannelIdStringGet((AtChannel)AtSdhChannelSlaveChannelAtIndex((AtSdhChannel)paths[i], j)));
            }
        StrToCell(tabPtr, i, 1, &buff[1]);
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtSdhPathPlmMonitorEnable(char argc, char **argv)
    {
    AtUnused(argc);
    return AttributeSet(argv[0], cAtTrue, mAttributeSetFunc(AtSdhPathPlmMonitorEnable));
    }

eBool CmdAtSdhPathPlmMonitorDisable(char argc, char **argv)
    {
    AtUnused(argc);
    return AttributeSet(argv[0], cAtFalse, mAttributeSetFunc(AtSdhPathPlmMonitorEnable));
    }

eBool CmdAtSdhPathLogShow(char argc, char** argv)
    {
    uint32 numberPaths, i;
    AtSdhPath *paths;
    AtUnused(argc);

    /* Get list of path */
    paths = CliSdhPathFromArgumentGet(argv[0], &numberPaths);
    if (numberPaths == 0)
        {
        AtPrintc(cSevNormal, "Error: ID is wrong\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numberPaths; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtChannel thisPath = (AtChannel)paths[i];
        AtChannelLogShow(thisPath);
        }

    return cAtTrue;
    }

eBool CmdAtSdhPathLogEnable(char argc, char** argv)
    {
    return LogEnable(argc, argv, cAtTrue);
    }

eBool CmdAtSdhPathLogDisable(char argc, char** argv)
    {
    return LogEnable(argc, argv, cAtFalse);
    }

eBool CmdAtSdhPathHoldOffTimer(char argc, char **argv)
    {
    return DecimalAttributeSet(argc, argv, mAttributeSetFunc(AtSdhChannelHoldOffTimerSet));
    }

eBool CmdAtSdhPathHoldOnTimer(char argc, char **argv)
    {
    return DecimalAttributeSet(argc, argv, mAttributeSetFunc(AtSdhChannelHoldOnTimerSet));
    }

eBool CmdAtSdhPathAlarmCaptureShow(char argc, char **argv)
    {
    uint32 numChannels, i;
    uint32 bufferSize;
    const char *pHeading[] = {"PathId", "AIS", "LOP", "TIM", "UNEQ", "P-UNEQ",
                              "PLM", "RDI", "ERDI-S", "ERDI-P", "ERDI-C",
                              "BER-SD", "BER-SF", "BER-TCA", "LOM", "RFI", "ClockStateChange"};
    eAtHistoryReadingMode readMode;
    AtChannel *channelList;
    eBool silent = cAtFalse;
    tTab *tabPtr = NULL;
    AtUnused(argc);

    channelList = CliSharedChannelListGet(&bufferSize);
    numChannels = CliAtModuleSdhChanelsFromString(argv[0], channelList, bufferSize);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "Error: ID is wrong\r\n");
        return cAtFalse;
        }

    /* If list channel don't raised any event, just report not-changed */
    readMode = CliHistoryReadingModeGet(argv[1]);

    if (argc > 2)
        silent = CliHistoryReadingShouldSilent(readMode, argv[2]);

    if (!silent)
        {
        /* Create table with titles */
        tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
            return cAtFalse;
            }
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtChannel channel = channelList[i];
        AtChannelObserver observer = CliAtSdhPathObserverGet(channel);
        uint8 column = 0;
        uint32 current;
        uint32 history;

        StrToCell(tabPtr, i, column++, CliChannelIdStringGet(channel));

        current = AtChannelObserverDefectGet(observer);
        if (readMode == cAtHistoryReadingModeReadToClear)
            history = AtChannelObserverDefectHistoryClear(observer);
        else
            history = AtChannelObserverDefectHistoryGet(observer);

        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtSdhPathAlarmAis);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtSdhPathAlarmLop);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtSdhPathAlarmTim);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtSdhPathAlarmUneq);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtSdhPathAlarmPayloadUneq);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtSdhPathAlarmPlm);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtSdhPathAlarmRdi);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtSdhPathAlarmErdiS);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtSdhPathAlarmErdiP);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtSdhPathAlarmErdiC);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtSdhPathAlarmBerSd);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtSdhPathAlarmBerSf);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtSdhPathAlarmBerTca);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtSdhPathAlarmLom);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtSdhPathAlarmRfi);

        /* Print clock states recorded. */
        StrToCell(tabPtr, i, column++, AtChannelObserverClockStateString(observer));
        if (readMode == cAtHistoryReadingModeReadToClear)
            AtchannelObserverClockStateFlush(observer);

        /* Delete observer */
        CliAtSdhPathObserverDelete(channel);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtSdhPathTtiCompareEnable(char argc, char **argv)
    {
    AtUnused(argc);
    return AttributeSet(argv[0], cAtTrue, mAttributeSetFunc(AtSdhChannelTtiCompareEnable));
    }

eBool CmdAtSdhPathTtiCompareDisable(char argc, char **argv)
    {
    AtUnused(argc);
    return AttributeSet(argv[0], cAtFalse, mAttributeSetFunc(AtSdhChannelTtiCompareEnable));
    }

eBool CmdAtSdhPathSsInsertionEnable(char argc, char **argv)
    {
    AtUnused(argc);
    return AttributeSet(argv[0], cAtTrue, mAttributeSetFunc(AtSdhPathSsInsertionEnable));
    }

eBool CmdAtSdhPathSsInsertionDisable(char argc, char **argv)
    {
    AtUnused(argc);
    return AttributeSet(argv[0], cAtFalse, mAttributeSetFunc(AtSdhPathSsInsertionEnable));
    }

eBool CmdAtSdhPathMode(char argc, char **argv)
    {
    eBool convertSuccess;
    eAtSdhChannelMode mode = cAtSdhChannelModeSdh;

    AtUnused(argc);

    mAtStrToEnum(cCmdSdhChannelModeStr, cCmdSdhChannelModeVal, argv[1], mode, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid parameter <mode> , expected sdh|sonet\r\n");
        return cAtFalse;
        }

    return AttributeSet(argv[0], mode, mAttributeSetFunc(AtSdhChannelModeSet));
    }

eBool CmdAtSdhPathAlarmAutoRdiGet(char argc, char **argv)
    {
    uint32 numPaths, path_i, alarm_i;
    tTab *tabPtr;
    AtSdhPath *paths;
    const char *pHeading[] = {"PathId", "AIS", "LOP", "TIM", "UNEQ", "PLM"};
    const uint32 cSdhPathAlarmValues[] = {cAtSdhPathAlarmAis,
                                          cAtSdhPathAlarmLop,
                                          cAtSdhPathAlarmTim,
                                          cAtSdhPathAlarmUneq,
                                          cAtSdhPathAlarmPlm};
    AtUnused(argc);

    /* Get list of path */
    paths = CliSdhPathFromArgumentGet(argv[0], &numPaths);
    if (numPaths == 0)
        {
        AtPrintc(cSevNormal, "WARNING: no paths to configure. ID may be wrong\r\n");
        return cAtTrue;
        }

    tabPtr = TableAlloc(numPaths, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (path_i = 0; path_i < numPaths; path_i++)
        {
        uint32 column = 0;

        StrToCell(tabPtr, path_i, column++, CliChannelIdStringGet((AtChannel)paths[path_i]));

        for (alarm_i = 0; alarm_i < mCount(cSdhPathAlarmValues); alarm_i++)
            {
            eBool enabled = AtSdhChannelAlarmAutoRdiIsEnabled((AtSdhChannel)paths[path_i], cSdhPathAlarmValues[alarm_i]);
            ColorStrToCell(tabPtr, path_i, column++, CliBoolToString(enabled), enabled ? cSevInfo : cSevCritical);
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtSdhVcStuffingEnable(char argc, char **argv)
    {
    return BoolAttributeSet(argc, argv, cAtTrue, (BooldAttributeSetFunc)AtSdhVcStuffEnable);
    }

eBool CmdAtSdhVcStuffingDisable(char argc, char **argv)
    {
    return BoolAttributeSet(argc, argv, cAtFalse, (BooldAttributeSetFunc)AtSdhVcStuffEnable);
    }

eBool CmdAtSdhVcShow(char argc, char **argv)
    {
    uint32 numberPaths, i;
    uint32 bufferSize;
    AtSdhChannel *paths;
    tTab *tabPtr;
    const char *heading[] ={"PathId", "Stuffing"};
    AtUnused(argc);

    /* Initialize value */
    paths = (AtSdhChannel *)((void*)CliSharedChannelListGet(&bufferSize));
    numberPaths = CliAtModuleSdhChanelsFromString(argv[0], (AtChannel *)((void*)paths), bufferSize);
    if (numberPaths == 0)
        {
        AtPrintc(cSevNormal, "ERROR: ID is wrong\r\n");
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numberPaths, 2, heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numberPaths; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        eBool stuffEn = cAtFalse;
        uint32 column = 0;

        /* Print ID */
        StrToCell(tabPtr, i, column++, CliChannelIdStringGet((AtChannel)paths[i]));

        stuffEn = AtSdhVcStuffIsEnabled((AtSdhVc)paths[i]);
        StrToCell(tabPtr, i, column++, CliBoolToString(stuffEn));
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }


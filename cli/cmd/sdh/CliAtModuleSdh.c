/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : CliAtModuleSdh.c
 *
 * Created Date: Jan 24, 2013
 *
 * Description : CLIs of SDH module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtModuleSdh.h"
#include "../clock/CliAtModuleClock.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleSdh SdhModule(void)
    {
    return (AtModuleSdh)AtDeviceModuleGet(CliDevice(), cAtModuleSdh);
    }

static eBool InterruptEnable(char argc, char **argv, eBool enable)
    {
    eAtRet ret = AtModuleInterruptEnable((AtModule)SdhModule(), enable);
	AtUnused(argv);
	AtUnused(argc);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "Cannot %s interrupt, ret = %s\n", enable ? "enable" : "disable", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdSdhModuleOutClkSet(char argc, char **argv)
    {
    uint32 i;
    uint32 numLines, bufferSize;
    uint32 *lineIds;
    eAtRet ret = cAtOk;
    uint32 *refOutIds, numRefOutIds;
	AtUnused(argc);

    /* Partition buffer to two parts to hold line IDs and output clock IDs */
    lineIds = CliSharedIdBufferGet(&bufferSize);
    bufferSize = bufferSize / 2;
    refOutIds = &(lineIds[bufferSize]);

    /* Get ID of clock signal */
    numRefOutIds = CliIdListFromString(argv[0], refOutIds, bufferSize);
    if (numRefOutIds == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid list of output clock IDs, expect a list of flat IDs\r\n");
        return cAtFalse;
        }

    /* Clock source is disconnected by inputing "none" */
    if (AtStrcmp(argv[1], "none") == 0)
        lineIds = NULL;
    else
        {
        /* Get list of Lines */
        numLines = CliIdListFromString(argv[1], lineIds, bufferSize);
        if (numLines == 0)
            {
            AtPrintc(cSevCritical, "ERROR: No line is parsed\r\n");
            return cAtFalse;
            }

        /* Make sure that two lists have the same number of items */
        if (numLines != numRefOutIds)
            {
            AtPrintc(cSevCritical, "ERROR: Number of Lines and output clock IDs must be the same\r\n");
            return cAtFalse;
            }
        }

    /* Configure */
    for (i = 0; i < numRefOutIds; i++)
        {
        AtModuleSdh sdhModule;
        AtSdhLine line;

        sdhModule = (AtModuleSdh)AtDeviceModuleGet(CliDevice(), cAtModuleSdh);

        /* Get corresponding objects */
        if (lineIds == NULL)
            line = NULL;
        else
            line = AtModuleSdhLineGet(sdhModule, (uint8)(lineIds[i] - 1));

        /* Apply */
        ret = AtModuleSdhOutputClockSourceSet(sdhModule, (uint8)(refOutIds[i] - 1), line);
        if(ret != cAtOk)
            AtPrintc(cSevCritical,
                     "ERROR: Output clock of %s to clock #%u fail, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)line),
                     refOutIds[i],
                     AtRet2String(ret));
        }

    return cAtTrue;
    }

eBool CmdSdhModuleOutClkGet(char argc, char **argv)
    {
    tTab *tabPtr;
    uint32 numIds, i;
    const char *heading[] = {"Output clock ID", "Source"};
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(CliDevice(), cAtModuleSdh);
    AtList extractors = AtListCreate(0);
	AtUnused(argc);

    /* Get extractors */
    CliAtModuleClockExtractorsFromString(extractors, argv[0]);
    numIds = AtListLengthGet(extractors);
    if (numIds == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Please input correct extractor lists, expected format: 1,2-4,...\r\n");
        AtObjectDelete((AtObject)extractors);
        return cAtFalse;
        }

    /* Create table */
    tabPtr = TableAlloc(numIds, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        AtObjectDelete((AtObject)extractors);
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numIds; i++)
        {
        AtClockExtractor extractor = (AtClockExtractor)AtListObjectGet(extractors, i);
        uint8 extractorId = AtClockExtractorIdGet(extractor);
        StrToCell(tabPtr, i, 0, CliNumber2String(extractorId + 1UL, "%u"));
        StrToCell(tabPtr, i, 1, (char *)CliChannelIdStringGet((AtChannel)AtModuleSdhOutputClockSourceGet(sdhModule, extractorId)));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtObjectDelete((AtObject)extractors);

    return cAtTrue;
    }

eBool CmdSdhInterruptEnable(char argc, char **argv)
    {
    return InterruptEnable(argc, argv, cAtTrue);
    }

eBool CmdSdhInterruptDisable(char argc, char **argv)
    {
    return InterruptEnable(argc, argv, cAtFalse);
    }

eBool CmdSdhModuleDebugLoopback(char argc, char **argv)
    {
    eAtLoopbackMode loopMode = CliLoopbackModeFromString(argv[0]);
    eAtRet ret = AtModuleDebugLoopbackSet((AtModule)SdhModule(), (uint8)loopMode);
	AtUnused(argc);
    if (ret != cAtOk)
        {
        AtPrintc(cSevWarning, "ERROR: Can not set loopback mode for encap module, ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAtModuleSdhDebug(char argc, char **argv)
    {
	AtUnused(argv);
	AtUnused(argc);
    AtModuleDebug(AtDeviceModuleGet(CliDevice(), cAtModuleSdh));
    return cAtTrue;
    }

const char *CliSdhTtiMsgRemvPadding(uint8 *pTtiMsgBuf)
    {
    uint16 charCount;
    const char *pPadding = "null_padding";

    pPadding = NULL;
    charCount = 0;
    while ((charCount < cCliSdhTti64Len) && (pTtiMsgBuf[charCount] != '\0') && (pTtiMsgBuf[charCount] != ' '))
        charCount++;

    if (charCount == cCliSdhTti64Len)
        return pPadding;

    if (pTtiMsgBuf[charCount] == ' ')
        pPadding = "space_padding";

    if (pTtiMsgBuf[charCount] == '\0')
        pPadding = "null_padding";

    pTtiMsgBuf[charCount] = '\0';

    return pPadding;
    }

eBool CmdAtModuleSdhShow(char argc, char **argv)
    {
    tTab *tabPtr;
    uint32 row = 0;
    const char *heading[] = {"Attribute", "Value"};
    AtModuleSdh sdhModule = SdhModule();
    eBool enabled;

    AtUnused(argc);
    AtUnused(argv);

    /* Create table */
    tabPtr = TableAlloc(1, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    enabled = AtModuleInterruptIsEnabled((AtModule)sdhModule);
    StrToCell(tabPtr, row, 0, "Interrupt");
    StrToCell(tabPtr, row, 1, CliBoolToString(enabled));

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

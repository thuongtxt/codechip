/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2011 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : SONET/SDH
 *
 * File        : atclisdh.h
 *
 * Created Date: Dec 2, 2011
 *
 * Description : Contain common define for SDH cli
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

#ifndef ATCLISDH_H_
#define ATCLISDH_H_

/*--------------------------- Includes ---------------------------------------*/
/* SDK related header files */
#include "AtOsal.h"
#include "atidparse.h"
#include "atclib.h"
#include "AtCli.h"

/* SDH related header files */
#include "AtDriver.h"
#include "AtDevice.h"
#include "AtModuleSdh.h"
#include "AtSdhChannel.h"
#include "AtSdhPath.h"
#include "AtSdhVc.h"
#include "AtSdhLine.h"
#include "AtSdhTu.h"
#include "AtSdhAu.h"
#include "AtSdhVc.h"

#include "../textui/CliAtTextUI.h"

/*--------------------------- Define -----------------------------------------*/
/* Separate character and string */
#define cCliSdhDelimChar "|"

/* The defined length of TTI message */
#define cCliSdhTti64Len                                        64
#define cCliSdhTti16Len                                        16
#define cCliSdhTti1Len                                         1

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef eAtRet (*LineAttributeSetFunc)(AtSdhLine self, uint32 value);

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Prototypes -------------------------------------*/
uint32 CliAtNumIdLevel(char *pStrIdList);
uint8 CliAtModuleSdhMaxLineIdGet(AtModuleSdh moduleSdh);
uint8 CliAtModuleSdhMaxEc1LineIdGet(AtModuleSdh moduleSdh);
uint8 CliAtModuleSdhStartEc1LineIdGet(AtModuleSdh moduleSdh);

/* Add and detect padding character */
const char *CliSdhTtiPaddingFind(uint8 *pTtiMsgBuf);
uint8 CliSdhTtiPaddingAdd(const char *pTtiMsgStr, const char *pPaddingStr, uint8 *pTtiMsgBuf, eAtSdhTtiMode ttiMode);

uint32 AtSdhVc1xListFromString(char *pStrIdList, AtChannel *channels, uint32 bufferSize);
uint32 AtSdhVc3ListFromString(char *pStrIdList, AtChannel *channel, uint32 bufferSize);
AtList CliAtSdhLineSerdesControllers(char *pPortListString);
uint32 CliAtModuleSdhChanelsFromString(char *pStrIdList, AtChannel *channelList, uint32 bufferSize);
AtList CliSdhLineListFromString(AtList lineList, char *lineListString);
uint32 CliSdhLineArrayFromString(char *pStrIdList, AtChannel *channel, uint32 bufferSize);
uint32 CliAtSdhLinesFromString(char *pStrIdList, AtChannel *channels, uint32 bufferSize);

const char *CliSdhTtiMsgRemvPadding(uint8 *pTtiMsgBuf);
uint32 AtSdhDe1ListFromString(const char *pStrIdList, AtChannel *channels, uint32 bufferSize);
uint32 AtSdhDe2ListFromString(const char *pStrIdList, AtChannel *channels, uint32 bufferSize);

AtSdhPath* CliSdhPathFromArgumentGet(char* arg, uint32 *numberPaths);
uint32 CliSdhPathAlarmMaskFromString(char *alarmStr);
const char * CliSdhPathAlarm2String(uint32 alarms);

const char **CliAtSdhLineAlarmTypeStr(uint32 *numAlarms);
const uint32 *CliAtSdhLineAlarmTypeVal(uint32 *numAlarms);
const char **CliAtSdhPathAlarmTypeStr(uint32 *numAlarms);
const uint32 *CliAtSdhPathAlarmTypeVal(uint32 *numAlarms);
uint32 CliAtSdhLinesFromString(char *pStrIdList, AtChannel *channels, uint32 bufferSize);

eBool CliAtSdhLineAttributeSet(char *lineListString, uint32 value, LineAttributeSetFunc attributeSetFunc);

#endif /* ATCLISDH_H_ */

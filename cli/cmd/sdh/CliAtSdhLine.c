/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2011 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SONET/SDH
 *
 * File        : CliAtSdhLine.c
 *
 * Created Date: Nov 13, 2012
 *
 * Description : SONET/SDH Line CLI implementation
 *
 * Notes       :
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "CliAtModuleSdh.h"
#include "AtSdhLine.h"
#include "../physical/CliAtSerdesController.h"
#include "AtCliModule.h"
#include "../../../driver/src/generic/sdh/AtSdhLineInternal.h"
#include "../common/CliAtChannelObserver.h"

/*--------------------------- Define -----------------------------------------*/
#define cAtSdhLineEvent     (cAtSdhLineAlarmK1Change|cAtSdhLineAlarmK2Change|cAtSdhLineAlarmS1Change)

/*--------------------------- Macros-----------------------------------------*/

#define mAttributeSetFunc(func) ((LineAttributeSetFunc)(func))

/*--------------------------- Local typedefs ---------------------------------*/
typedef eAtRet (*TtiAttributeSet)(AtSdhChannel self, const tAtSdhTti *tti);
typedef eAtRet (*ApsConfigureFunc)(AtSdhLine self, AtSdhLine toLine);
typedef eAtRet (*SdhLineApsReleaseFunc)(AtSdhLine self);
typedef const char *(*RowTohName)(AtSdhChannel line, uint32 column, uint32 numSts, eAtSdhLineOverheadByte *overheadByte);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char *cAtSdhLineAlarmTypeStr[] =
    {
    "los",
    "oof",
    "lof",
    "tim",
    "ais",
    "rdi",
    "ber-sd",
    "ber-sf",
    "ber-tca",
    "kbytechange",
    "s1change",
    "rs-sd",
    "rs-sf",
    "k1change",
    "k2change"
    };

static const uint32 cAtSdhLineAlarmTypeVal[] =
    {
    cAtSdhLineAlarmLos,
    cAtSdhLineAlarmOof,
    cAtSdhLineAlarmLof,
    cAtSdhLineAlarmTim,
    cAtSdhLineAlarmAis,
    cAtSdhLineAlarmRdi,
    cAtSdhLineAlarmBerSd,
    cAtSdhLineAlarmBerSf,
    cAtSdhLineAlarmBerTca,
    cAtSdhLineAlarmKByteChange,
    cAtSdhLineAlarmS1Change,
    cAtSdhLineAlarmRsBerSd,
    cAtSdhLineAlarmRsBerSf,
    cAtSdhLineAlarmK1Change,
    cAtSdhLineAlarmK2Change
    };

const char *cAtSdhLineAlarmUpperStr[] =
   {
    "LOS",
    "OOF",
    "LOF",
    "TIM",
    "AIS",
    "RDI",
    "BER-SD",
    "BER-SF",
    "BER-TCA",
    "KByte",
    "S1",
    "RS-SD",
    "RS-SF",
    "K1",
    "K2"
   };


static const char * cCmdSdhLineErrorTypeStr[]={"b1", "b2", "rei"};
static const eAtSdhLineErrorType cCmdSdhLineErrorTypeVal[]={cAtSdhLineErrorB1,
                                                            cAtSdhLineErrorB2,
                                                            cAtSdhLineErrorRei
                                                            };
static char const* cCmdSdhLineRateStr[]={"stm0", "stm1", "stm4", "stm16", "stm64"};
static const eAtSdhLineRate cCmdSdhLineRateVal[]={cAtSdhLineRateStm0, cAtSdhLineRateStm1, cAtSdhLineRateStm4, cAtSdhLineRateStm16, cAtSdhLineRateStm64};
static const char * cCmdSdhChannelModeStr[]={"sdh", "sonet"};
static const eAtSdhChannelMode cCmdSdhChannelModeVal[]={cAtSdhChannelModeSdh, cAtSdhChannelModeSonet};
static const char * cCmdSdhTtiModeStr[]={"1byte", "16bytes", "64bytes"};
static const eAtSdhTtiMode cCmdSdhTtiModeVal[]={cAtSdhTtiMode1Byte, cAtSdhTtiMode16Byte, cAtSdhTtiMode64Byte};

/*static const char  *cCmdSdhLineCounterTypeStr[]={"b1", "b2", "rei"};*/
static const eAtSdhLineCounterType cCmdSdhLineCounterTypeVal[]={cAtSdhLineCounterTypeB1, cAtSdhLineCounterTypeB2, cAtSdhLineCounterTypeRei };
static const char * cCmdLoopbackModeStr[]={"release", "local", "remote"};
static const eAtLoopbackMode cCmdLoopbackModeVal[]={cAtLoopbackModeRelease, cAtLoopbackModeLocal, cAtLoopbackModeRemote };

static const char *cCmdSdhLineOverheadByteStr[] ={"a1", "a2", "j0", "z0", "b1", "e1", "f1", "d1", "d2", "d3", "h1", "h2", "h3", "rs-undefined",
                                                  "b2", "k1", "k2", "d4", "d5", "d6", "d7", "d8", "d9", "d10", "d11", "d12", "s1",
                                                  "z1", "z2", "m0", "m1", "e2", "ms-undefined"};
static const char *cCmdSdhLineOverheadByteUpperCaseStr[] = {"A1", "A2", "J0", "Z0", "B1", "E1", "F1", "D1", "D2", "D3", "H1", "H2", "H3", "RS-UNDEFINED",
                                                            "B2", "K1", "K2", "D4", "D5", "D6", "D7", "D8", "D9", "D10", "D11", "D12", "S1",
                                                            "Z1", "Z2", "M0", "M1", "E2", "MS-UNDEFINED"};
static const uint32 cCmdSdhLineOverheadByteValue[]={cAtSdhLineRsOverheadByteA1        ,
                                                    cAtSdhLineRsOverheadByteA2        ,
                                                    cAtSdhLineRsOverheadByteJ0        ,
                                                    cAtSdhLineRsOverheadByteZ0        ,
                                                    cAtSdhLineRsOverheadByteB1        ,
                                                    cAtSdhLineRsOverheadByteE1        ,
                                                    cAtSdhLineRsOverheadByteF1        ,
                                                    cAtSdhLineRsOverheadByteD1        ,
                                                    cAtSdhLineRsOverheadByteD2        ,
                                                    cAtSdhLineRsOverheadByteD3        ,
                                                    cAtSdhLineRsOverheadByteH1        ,
                                                    cAtSdhLineRsOverheadByteH2        ,
                                                    cAtSdhLineRsOverheadByteH3        ,
                                                    cAtSdhLineRsOverheadByteUndefined ,

                                                    cAtSdhLineMsOverheadByteB2        ,
                                                    cAtSdhLineMsOverheadByteK1        ,
                                                    cAtSdhLineMsOverheadByteK2        ,
                                                    cAtSdhLineMsOverheadByteD4        ,
                                                    cAtSdhLineMsOverheadByteD5        ,
                                                    cAtSdhLineMsOverheadByteD6        ,
                                                    cAtSdhLineMsOverheadByteD7        ,
                                                    cAtSdhLineMsOverheadByteD8        ,
                                                    cAtSdhLineMsOverheadByteD9        ,
                                                    cAtSdhLineMsOverheadByteD10       ,
                                                    cAtSdhLineMsOverheadByteD11       ,
                                                    cAtSdhLineMsOverheadByteD12       ,
                                                    cAtSdhLineMsOverheadByteS1        ,
                                                    cAtSdhLineMsOverheadByteZ1        ,
                                                    cAtSdhLineMsOverheadByteZ2        ,
                                                    cAtSdhLineMsOverheadByteM0        ,
                                                    cAtSdhLineMsOverheadByteM1        ,
                                                    cAtSdhLineMsOverheadByteE2        ,
                                                    cAtSdhLineMsOverheadByteUndefined};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implement Functions ----------------------------*/
/*
 * Get Module Sdh
 */
static AtModuleSdh SdhModule(void)
    {
    return (AtModuleSdh)AtDeviceModuleGet(CliDevice(), cAtModuleSdh);
    }

static AtSdhLine LineGet(AtModuleSdh sdhModule, uint32 lineId)
    {
	if (lineId >= AtModuleSdhMaxLinesGet(sdhModule))
	    return NULL;

    return AtModuleSdhLineGet(SdhModule(), (uint8)lineId);
    }

static char PaddingFromString(const char *pPaddingStr)
    {
    if (AtStrcmp(pPaddingStr, "space_padding") == 0)
        return ' ';

    return '\0';
    }

/*
 * @brief Add padding character to TTI message
 *
 * @param [in] pTtiMsgStr String represents TTI message
 * @param [in] pPaddingStr String represents padding message
 * @param [out] ttiMsgBuf Buffer to store 16/64 Byte TTI message
 *
 * @return 16/64 Byte TTI message length or 0
 */
static eBool AttributeSet(char *lineListString,
                          uint32 value,
                          LineAttributeSetFunc attributeSetFunc)
    {
    uint32 bufferSize, numberOfLines, i;
    uint32 *sdhLine = CliSharedIdBufferGet(&bufferSize);
    eBool success = cAtTrue;

    numberOfLines = CliIdListFromString(lineListString, sdhLine, bufferSize);
    if (numberOfLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No line, the ID List may be wrong, expect 1,2-3,...\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numberOfLines; i++)
        {
        eAtRet ret;
        AtSdhLine line = LineGet(SdhModule(), sdhLine[i] - 1);
        if (line == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: Ignore line %d which does not exist\r\n", sdhLine[i]);
            continue;
            }

        ret = attributeSetFunc(line, value);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot configure line %u, ret = %s\r\n",
                     AtChannelIdGet((AtChannel)line) + 1,
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

uint8 CliSdhTtiPaddingAdd(const char *pTtiMsgStr, const char *pPaddingStr, uint8 *pTtiMsgBuf, eAtSdhTtiMode ttiMode)
    {
    uint8 padChar = (uint8)PaddingFromString(pPaddingStr);
    uint32 i;
    uint32 msgLen;
    uint8 ttiMsgLen;

    /* Copy TTI message to buffer */
    ttiMsgLen = 0;
    msgLen = AtStrlen(pTtiMsgStr);

    AtOsalMemCpy((void *) pTtiMsgBuf, pTtiMsgStr, msgLen);

    /* 1Byte <= TTI message < 16Byte */
    if ((msgLen > cCliSdhTti1Len) && (msgLen < cCliSdhTti16Len) && (ttiMode != cAtSdhTtiMode64Byte))
        {
        ttiMsgLen = cCliSdhTti16Len;
        for (i = msgLen; i < cCliSdhTti16Len; i++)
            pTtiMsgBuf[i] = padChar;
        pTtiMsgBuf[cCliSdhTti16Len] = '\0';
        }

    /* 16Byte < TTI message <= 64Byte */
    else
        {
        ttiMsgLen = cCliSdhTti64Len;
        if (msgLen < cCliSdhTti64Len)
            {
            for (i = msgLen; i < cCliSdhTti64Len - 2; i++)
                pTtiMsgBuf[i] = padChar;

            /* Add CR and LF characters at the end of 64-byte stream */
            pTtiMsgBuf[cCliSdhTti64Len - 2] = '\r';
            pTtiMsgBuf[cCliSdhTti64Len - 1] = '\n';
            }
        pTtiMsgBuf[cCliSdhTti64Len] = '\0';
        }

    return ttiMsgLen;
    }

static eBool TtiSet(char argc, char **argv, TtiAttributeSet TtiSetFunc)
    {
    uint32 numberOfLines, i;
    eAtRet ret;
    uint8 ttiMsgBuf[cCliSdhTti64Len + 1];
    uint8 ttiMsgLen;
    eAtSdhTtiMode ttiMode = cAtSdhTtiMode1Byte;
    eBool convertResult;
    tAtSdhTti tti;
    eBool success = cAtTrue;
    AtList lineList = AtListCreate(0);
	AtUnused(argc);

    /* Initialize new memory area */
    AtOsalMemInit(ttiMsgBuf, 0, sizeof(ttiMsgBuf));
    AtOsalMemInit(tti.message, 0, sizeof(tti.message));
    convertResult = cAtTrue;
    ret = cAtOk;

    CliSdhLineListFromString(lineList, argv[0]);
    numberOfLines = AtListLengthGet(lineList);
    if (numberOfLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No line, the ID List may be wrong, expect 1,2-3,...\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    mAtStrToEnum(cCmdSdhTtiModeStr, cCmdSdhTtiModeVal, argv[1], ttiMode, convertResult);
    if (!convertResult)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid parameter <ttiMode> , expected 1_Byte|16_Byte|64_Byte\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    /* Check padding information & store transmitted TTI message into buffer */
    ttiMsgLen = CliSdhTtiPaddingAdd(argv[2], argv[3], ttiMsgBuf, ttiMode);
    AtSdhTtiMake(ttiMode, ttiMsgBuf, ttiMsgLen, &tti);
    for (i = 0; i < numberOfLines; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtSdhChannel line = (AtSdhChannel)AtListObjectGet(lineList, i);
        ret = TtiSetFunc(line, &tti);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot configure TTI at line %u, ret = %s\r\n", AtChannelIdGet((AtChannel)line) + 1, AtRet2String(ret));
            success = cAtFalse;
            }
        }

    AtObjectDelete((AtObject)lineList);
    return success;
    }

static eBool StatusShow(char argc, char **argv, uint32 (*AlarmGetFunc)(AtChannel self), eBool silent)
    {
    uint32 numberOfLines, i;
    uint32 j;
    uint32 alarmStat;
    tTab *tabPtr = NULL;
    char buff[16];
    const char *pHeading[] = {"LineId", "LOS", "OOF", "LOF", "TIM", "AIS", "RDI",
                              "BER-SD", "BER-SF", "BER-TCA", "KByte-Change",
                              "S1-Change", "RS-SD", "RS-SF", "K1-Change", "K2-Change"};
    uint32 numAlarms;
    AtSdhLine line;
    const uint32 *alarmTypeVal = CliAtSdhLineAlarmTypeVal(&numAlarms);
    AtList lineList = AtListCreate(0);
	AtUnused(argc);

    /* Get list of lines */
	CliSdhLineListFromString(lineList, argv[0]);
    numberOfLines = AtListLengthGet(lineList);
    if (numberOfLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No line, the ID List may be wrong, expect 1,2-3,...\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    /* Create table with titles */
    if (!silent)
        {
    tabPtr = TableAlloc(numberOfLines, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
            AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }
        }

    /* Make table content */
    for (i = 0; i < numberOfLines; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        line = (AtSdhLine)AtListObjectGet(lineList, i);
        
        /* Print list LineIds */
        AtSprintf(buff, "%u", AtChannelIdGet((AtChannel)line) + 1);
        StrToCell(tabPtr, i, 0, buff);

        /* Print alarm status */
        alarmStat = AlarmGetFunc((AtChannel) line);
        for (j = 0; j < numAlarms; j++)
            {
            if (alarmStat & alarmTypeVal[j])
                ColorStrToCell(tabPtr, i, j + 1, "set", cSevCritical);
            else
                ColorStrToCell(tabPtr, i, j + 1, "clear", cSevInfo);
            }
        }
    TablePrint(tabPtr);
    TableFree(tabPtr);

    AtObjectDelete((AtObject)lineList);
    return cAtTrue;
    }

static uint32 IntrMaskFromString(char *pIntrStr)
    {
    uint32 numAlarms = 0;
    const char **alarmTypeStrings = CliAtSdhLineAlarmTypeStr(&numAlarms);
    const uint32 *alarmTypes = CliAtSdhLineAlarmTypeVal(&numAlarms);
    return CliMaskFromString(pIntrStr, alarmTypeStrings, alarmTypes, numAlarms);
    }

static eBool AlarmEnable(char argc, char **argv, eBool enable)
    {
    uint32 numberOfLines, i;
    eAtRet ret = cAtOk;
    eAtSdhLineAlarmType alarmType = 0;
    AtList lineList = AtListCreate(0);
	AtUnused(argc);

    /* Get list of lines */
	CliSdhLineListFromString(lineList, argv[0]);
    numberOfLines = AtListLengthGet(lineList);
    if (numberOfLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No line, the ID List may be wrong, expect 1,2-3,...\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    /* Alarm types */
    if (argv[1] != NULL )
        alarmType = IntrMaskFromString(argv[1]);
    else
        {
        AtPrintc(cSevCritical, "ERROR: Invalid parameter <rate> , expected los|oof|lof...\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    for (i = 0; i < numberOfLines; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtSdhChannel line = (AtSdhChannel)AtListObjectGet(lineList, i);
        ret |= AtSdhChannelAlarmAffectingEnable(line, alarmType, enable);
        if (ret != cAtOk)
            AtPrintc(cSevCritical,
                     "ERROR: Configure Line %u fail, ret = %s\r\n",
                     AtChannelIdGet((AtChannel)line) + 1,
                     AtRet2String(ret));
        }

    AtObjectDelete((AtObject)lineList);
    return (ret == cAtOk) ? cAtTrue : cAtFalse;
    }

static eBool SerdesControllerEnable(char argc, char **argv, eBool enable)
    {
    AtList controllers = CliAtSdhLineSerdesControllers(argv[0]);
    eBool success = CliAtSerdesControllerEnable(controllers, enable);
	AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

static eBool ApsConfigure(uint8 fromLineId,
                          uint8 toLineId,
                          ApsConfigureFunc setFunc)
    {
    eAtRet ret;
    AtSdhLine fromLine = LineGet(SdhModule(), fromLineId - 1UL);
    AtSdhLine toLine   = LineGet(SdhModule(), toLineId - 1UL);

    ret = setFunc(fromLine, toLine);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Cannot configure line %d, ret = %s\r\n",
                 fromLineId,
                 AtRet2String(ret));

        return cAtFalse;
        }

    return cAtTrue;
    }

static eBool ApsRelease(char *lineListString, SdhLineApsReleaseFunc releaseFunc)
    {
    uint32 numberOfLines, i;
    AtSdhLine line;
    eBool success = cAtTrue;
    AtList lineList = AtListCreate(0);

    /* Get list of lines */
    CliSdhLineListFromString(lineList, lineListString);
    numberOfLines = AtListLengthGet(lineList);
    if (numberOfLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No line, the ID List may be wrong, expect 1,2-3,...\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    /* Apply configure for all input lines */
    for (i = 0; i < numberOfLines; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret;

        line = (AtSdhLine)AtListObjectGet(lineList, i);
        ret = releaseFunc(line);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot configure line %u, ret = %s\r\n",
                     AtChannelIdGet((AtChannel)line) + 1,
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    AtObjectDelete((AtObject)lineList);
    return success;
    }

static void AlarmChangeState(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    uint8 alarm_i;

    AtPrintc(cSevNormal, "\r\n%s [SDH] Defect changed at channel %s", AtOsalDateTimeInUsGet(), CliChannelIdStringGet(channel));
    for (alarm_i = 0; alarm_i < mCount(cAtSdhLineAlarmTypeVal); alarm_i++)
        {
        uint32 alarmType = cAtSdhLineAlarmTypeVal[alarm_i];
        if ((changedAlarms & alarmType) == 0)
            continue;

        AtPrintc(cSevNormal, " * [%s:", cAtSdhLineAlarmUpperStr[alarm_i]);
        if (changedAlarms & alarmType & cAtSdhLineEvent)
            {
            switch (alarmType)
                {
                case cAtSdhLineAlarmK1Change:
                    AtPrintc(cSevNormal, "0x%x", AtSdhLineRxK1Get((AtSdhLine)channel));
                    break;
                case cAtSdhLineAlarmK2Change:
                    AtPrintc(cSevNormal, "0x%x", AtSdhLineRxK2Get((AtSdhLine)channel));
                    break;
                case cAtSdhLineAlarmS1Change:
                    AtPrintc(cSevNormal, "0x%x", AtSdhLineRxS1Get((AtSdhLine)channel));
                    break;
                default:
                    AtPrintc(cSevInfo, "detect");
                }
            }
        else
            {
            if (currentStatus & alarmType)
                AtPrintc(cSevCritical, "set");
            else
                AtPrintc(cSevInfo, "clear");
            }
        AtPrintc(cSevNormal, "]");
        }
    AtPrintc(cSevNormal, "\r\n");
    }

static void AlarmChangeLogging(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    uint8 alarm_i;
    static char logBuffer[1024];
    char * logPtr = logBuffer;

    AtOsalMemInit(logBuffer, 0, sizeof(logBuffer));

    logPtr += AtSprintf(logPtr, "has defects changed at");
    for (alarm_i = 0; alarm_i < mCount(cAtSdhLineAlarmTypeVal); alarm_i++)
        {
        uint32 alarmType = cAtSdhLineAlarmTypeVal[alarm_i];
        if ((changedAlarms & alarmType) == 0)
            continue;

        logPtr += AtSprintf(logPtr, " * [%s:", cAtSdhLineAlarmUpperStr[alarm_i]);
        if (changedAlarms & alarmType & cAtSdhLineEvent)
            {
            switch (alarmType)
                {
                case cAtSdhLineAlarmK1Change:
                    logPtr += AtSprintf(logPtr, "0x%x", AtSdhLineRxK1Get((AtSdhLine)channel));
                    break;
                case cAtSdhLineAlarmK2Change:
                    logPtr += AtSprintf(logPtr, "0x%x", AtSdhLineRxK2Get((AtSdhLine)channel));
                    break;
                case cAtSdhLineAlarmS1Change:
                    logPtr += AtSprintf(logPtr, "0x%x", AtSdhLineRxS1Get((AtSdhLine)channel));
                    break;
                default:
                    logPtr += AtSprintf(logPtr, "detect");
                }
            }
        else
            {
            if (currentStatus & alarmType)
                logPtr += AtSprintf(logPtr, "set");
            else
                logPtr += AtSprintf(logPtr, "clear");
            }
        logPtr += AtSprintf(logPtr, "]");
        }
    CliChannelLog(channel, logBuffer);
    }

static eBool AutoRdiEnable(char argc, char **argv, eBool enable)
    {
    uint32 numberOfLines, i;
    eBool success = cAtTrue;
    AtList lineList = AtListCreate(0);
    uint32 alarms = cInvalidUint32;

	AtUnused(argc);

    /* Get list of lines */
	CliSdhLineListFromString(lineList, argv[0]);
    numberOfLines = AtListLengthGet(lineList);
    if (numberOfLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No line, the ID List may be wrong, expect 1,2-3,...\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    /* Get specific alarms */
    if (argc > 1)
        alarms = IntrMaskFromString(argv[1]);

    for (i = 0; i < numberOfLines; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtSdhChannel line = (AtSdhChannel)AtListObjectGet(lineList, i);
        eAtRet ret;

        if (alarms == cInvalidUint32)
            ret = AtSdhChannelAutoRdiEnable(line, enable);
        else
            ret = AtSdhChannelAlarmAutoRdiEnable(line, alarms, enable);

        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical,
             "ERROR: %s auto RDI on %s fail with ret = %s\r\n",
             enable ? "Enable" : "Disable",
             CliChannelIdStringGet((AtChannel)line),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    AtObjectDelete((AtObject)lineList);
    return success;
    }

/*
 * @brief Identify and convert alarm value to string
 *
 * @param [in] alarms alarm value
 *
 * @return pAlarmString
 */
static const char *Alarm2String(uint32 alarms)
    {
    uint16 i;
    static char alarmString[64];
    uint32 numAlarms;
    const char **alarmTypeStr = CliAtSdhLineAlarmTypeStr(&numAlarms);
    const uint32 *alarmTypeVal = CliAtSdhLineAlarmTypeVal(&numAlarms);

    /* Initialize string */
    alarmString[0] = '\0';
    if (alarms == 0)
        AtSprintf(alarmString, "None");

    /* There are alarms */
    else
        {
        for (i = 0; i < numAlarms; i++)
            {
            if (alarms & (alarmTypeVal[i]))
                AtSprintf(alarmString, "%s%s|", alarmString, alarmTypeStr[i]);
            }

        if (AtStrlen(alarmString) == 0)
            return "None";

        /* Remove the last '|' */
        alarmString[AtStrlen(alarmString) - 1] = '\0';
        }

    return alarmString;
    }

static uint32 LinesFromString(char *pStrIdList, AtChannel *channels, uint32 bufferSize)
    {
    uint32 *idBuffer, numChannels, i;
    uint32 numValidLines = 0;

    if (pStrIdList == NULL)
        return 0;

    /* It is just a list of flat ID */
    idBuffer = CliSharedIdBufferGet(&numChannels);
    numChannels = CliIdListFromString(pStrIdList, idBuffer, numChannels);
    if (numChannels > bufferSize)
        numChannels = bufferSize;

    if (numChannels == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No line, the ID List may be wrong, expect 1,2-3,...\r\n");
        return 0;
        }

    /* Just have valid lines */
    for (i = 0; i < numChannels; i++)
        {
        AtChannel line = (AtChannel) AtModuleSdhLineGet(CliModuleSdh(), (uint8)(idBuffer[i] - 1));

        if (line)
            {
            channels[numValidLines] = line;
            numValidLines = numValidLines + 1;
            }
        }

    return numValidLines;
    }

AtList CliSdhLineListFromString(AtList lineList, char *lineListString)
    {
    uint32 *sdhLine;
    uint32 bufferSize, numberOfLines, i;

    sdhLine = CliSharedIdBufferGet(&bufferSize);
    numberOfLines = CliIdListFromString(lineListString, sdhLine, bufferSize);
    if (numberOfLines == 0)
        return NULL;

    for (i = 0; i < numberOfLines; i++)
        {
        AtSdhLine line = LineGet(SdhModule(), sdhLine[i] - 1);
        if (line)
            AtListObjectAdd(lineList, (AtObject)line);
        }

    return lineList;
    }

AtList CliAtSdhLineSerdesControllers(char *pPortListString)
    {
    AtList controllers;
    uint32 numberOfLines;
    uint32 i;
    AtList lineList = AtListCreate(0);

    /* ID list of lines */
    CliSdhLineListFromString(lineList, pPortListString);
    numberOfLines = AtListLengthGet(lineList);
    if (numberOfLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No line, the ID List may be wrong, expect 1,2-3,...\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    /* Get controllers */
    controllers = AtListCreate(0);
    for (i = 0; i < numberOfLines; i++)
        {
        AtSdhLine line = (AtSdhLine)AtListObjectGet(lineList, i);
        AtSerdesController controler = AtSdhLineSerdesController(line);
        if (controler)
            AtListObjectAdd(controllers, (AtObject)controler);
        }

    AtObjectDelete((AtObject)lineList);

    /* No controller */
    if (AtListLengthGet(controllers) == 0)
        {
        AtObjectDelete((AtObject)controllers);
        return NULL;
        }

    return controllers;
    }

const char **CliAtSdhLineAlarmTypeStr(uint32 *numAlarms)
    {
        *numAlarms = mCount(cAtSdhLineAlarmTypeStr);
    return cAtSdhLineAlarmTypeStr;
    }

const uint32 *CliAtSdhLineAlarmTypeVal(uint32 *numAlarms)
    {
        *numAlarms = mCount(cAtSdhLineAlarmTypeVal);
    return cAtSdhLineAlarmTypeVal;
    }

static void AutotestAlarmChangeState(AtChannel channel, uint32 changedAlarms, uint32 currentStatus, void *userData)
    {
    AtChannelObserver observer = userData;
    AtUnused(channel);
    AtChannelObserverDefectsUpdate(observer, changedAlarms, currentStatus);
    }

static const char *TohName(AtSdhChannel line, uint32 column, uint32 row, eAtSdhLineOverheadByte *overheadByte)
    {
    static char overheadStr[16];
    eBool result = cAtFalse;

    *overheadByte = AtSdhLineOverheadByteGet((AtSdhLine)line, row, column);

    if (*overheadByte == cAtSdhLineRsOverheadByteUndefined ||
        *overheadByte == cAtSdhLineMsOverheadByteUndefined)
        return NULL;

    mAtEnumToStr(cCmdSdhLineOverheadByteUpperCaseStr,
                 cCmdSdhLineOverheadByteValue,
                 *overheadByte,
                 overheadStr, result);
    if (!result)
        return NULL;

    return overheadStr;
    }

static void ZnColumnSet(AtSdhLine line,
                        eBool (*ZnCanMonitor)(AtSdhLine),
                        eAtRet (*ZnMonitorPositionSet)(AtSdhLine, uint8),
                        uint32 column, uint8 numSts)
    {
    uint8 numSts3 = (uint8)(numSts / 3);
    uint8 aug1 = (uint8)(column % numSts3);
    uint8 sts1 = (uint8)(((column % numSts) / numSts3) % 3);
    sts1 = (uint8)(aug1 * 3 + sts1);

    if (ZnCanMonitor(line))
        ZnMonitorPositionSet(line, sts1);
    }

static eBool Stm1NumberIsValid(uint32 numSts, uint32 stm1Number, eBool ignoreStm1Number)
    {
    if (ignoreStm1Number)
        return cAtTrue;

    if (stm1Number == 0)
        return cAtFalse;

    /* Other rates */
    if (stm1Number > (numSts / 3))
        return cAtFalse;

    return cAtTrue;
    }

static eBool TohShow(AtSdhChannel line,
                     uint8 (*OverheadByteGet)(AtSdhChannel self, uint32 overheadByte),
                     eBool (*OverheadByteIsSupported)(AtSdhChannel self, uint32 overheadByte),
                     uint32 stm1Number, eBool needZnPossition)
    {
    uint8 numSts = AtSdhChannelNumSts(line);
    uint8 numSts3 = (uint8)(numSts / 3);
    static const uint8 cNumTohRows = 9;
    uint8 cMaxColumnPerTable = (numSts == 1) ? 3 : 9;
    static const char *heading[] = {"Row/Col", "", "", "", "", "", "", "", "", ""};
    uint32 column_i, row_i;
    tTab *tabPtr;
    eAtSdhLineOverheadByte overheadByte;
    uint32 tableColumnId;
    eBool ignoreStm1Number = (numSts == 1) ? cAtTrue : cAtFalse;

    if (Stm1NumberIsValid(numSts, stm1Number, ignoreStm1Number))
        stm1Number = stm1Number - 1;
    else
        {
        AtPrintc(cSevCritical, "ERROR: STM-1 number is out of range. Expected: 1..m (m <= %d)\r\n", (numSts / 3));
        return cAtTrue;
        }

    tabPtr = TableAlloc(cNumTohRows, (uint8)(cMaxColumnPerTable + 1), heading);
    if (tabPtr == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Print row IDs */
    for (row_i = 0; row_i < cNumTohRows; row_i++)
        StrToCell(tabPtr, row_i, 0, CliNumber2String(row_i + 1, "%d"));

    for (column_i = 0; column_i < cMaxColumnPerTable; column_i++)
        {
        uint32 columnIndex = ignoreStm1Number ? column_i : (column_i * numSts3 + stm1Number);

        /* Update title */
        tableColumnId = (column_i % cMaxColumnPerTable) + 1;
        TableColumnHeaderSet(tabPtr, tableColumnId, (const char *)CliNumber2String(columnIndex + 1, "%d"));

        for (row_i = 0; row_i < cNumTohRows; row_i++)
            {
            uint32 bufferSize;
            char *tohVal = CliSharedCharBufferGet(&bufferSize);
            const char *tohName;
            uint8 value, color;

            /* Build value */
            tohName = TohName(line, columnIndex, row_i, &overheadByte);

            if ((overheadByte == cAtSdhLineMsOverheadByteZ1) && needZnPossition)
                ZnColumnSet((AtSdhLine)line, AtSdhLineZ1CanMonitor, AtSdhLineZ1MonitorPositionSet, columnIndex, numSts);

            if ((overheadByte == cAtSdhLineMsOverheadByteZ2) && needZnPossition)
                ZnColumnSet((AtSdhLine)line, AtSdhLineZ2CanMonitor, AtSdhLineZ2MonitorPositionSet, columnIndex, numSts);

            value = OverheadByteGet(line, overheadByte);
            if (OverheadByteIsSupported(line, overheadByte))
                {
                if (tohName)
                    AtSprintf(tohVal, "%s(0x%02x)", tohName, value);
                else
                    AtSprintf(tohVal, "0x%02x", value);
                }
            else
                {
                if (tohName)
                    AtSprintf(tohVal, "%s(%s)", tohName, sAtNotSupported);
                else
                    AtSprintf(tohVal, "%s", sAtNotApplicable);
                }

            /* Put to corresponding cell */
            color = value ? cSevInfo : cSevNormal;
            ColorStrToCell(tabPtr, row_i, tableColumnId, tohVal, color);
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

static eBool LineTohShow(AtSdhChannel line, uint32 stm1Number, eAtDirection direction)
    {
    eBool success = cAtTrue;
    uint8 numSts = AtSdhChannelNumSts(line);
    char cSts1Str[16];

    if (numSts == 1)
        AtSprintf(cSts1Str, "sts1.1");
    else
        AtSprintf(cSts1Str, "sts3.%d__sts1.%d-%d", stm1Number, (stm1Number - 1) * 3 + 1, (stm1Number - 1) * 3 + 3);

    AtPrintc(cSevInfo, "\r\n * TOH Table: %s__%s\r\n", CliChannelIdStringGet((AtChannel)line), cSts1Str);

    if (direction == cAtDirectionRx)
        return TohShow(line, AtSdhChannelRxOverheadByteGet, AtSdhChannelRxOverheadByteIsSupported, stm1Number, cAtTrue);

    if (direction == cAtDirectionTx)
        return TohShow(line, AtSdhChannelTxOverheadByteGet, AtSdhChannelTxOverheadByteIsSupported, stm1Number, cAtFalse);

    AtPrintc(cSevInfo, " * TX\r\n");
    if (!TohShow(line, AtSdhChannelTxOverheadByteGet, AtSdhChannelTxOverheadByteIsSupported, stm1Number, cAtFalse))
        success = cAtFalse;

    AtPrintc(cSevInfo, "\r\n");

    AtPrintc(cSevInfo, " * RX\r\n");
    if (!TohShow(line, AtSdhChannelRxOverheadByteGet, AtSdhChannelRxOverheadByteIsSupported, stm1Number, cAtTrue))
        success = cAtFalse;

    return success;
    }

eBool CliAtSdhLineAttributeSet(char *lineListString, uint32 value, LineAttributeSetFunc attributeSetFunc)
    {
    return AttributeSet(lineListString, value, attributeSetFunc);
    }

uint32 CliAtSdhLinesFromString(char *pStrIdList, AtChannel *channels, uint32 bufferSize)
    {
    return LinesFromString(pStrIdList, channels, bufferSize);
    }

/*
 * Set line rate
 */
eBool CmdSdhLineRate(char argc, char **argv)
    {
    eBool blResult;
    eAtSdhLineRate lineRate = cAtSdhLineRateInvalid;
	AtUnused(argc);

    mAtStrToEnum(cCmdSdhLineRateStr, cCmdSdhLineRateVal, argv[1], lineRate, blResult);
    if (!blResult)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid parameter <rate> , expected stm1|stm4|stm16\r\n");
        return cAtFalse;
        }

    return AttributeSet(argv[0], lineRate, mAttributeSetFunc(AtSdhLineRateSet));
    }

/*
 * Set line mode
 */
eBool CmdSdhLineMode(char argc, char **argv)
    {
    eBool blResult;
    eAtSdhChannelMode lineMode = cAtSdhChannelModeSdh;
	AtUnused(argc);

    mAtStrToEnum(cCmdSdhChannelModeStr, cCmdSdhChannelModeVal, argv[1], lineMode, blResult);
    if (!blResult)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid parameter <mode> , expected sdh|sonet\r\n");
        return cAtFalse;
        }

    return AttributeSet(argv[0], lineMode, mAttributeSetFunc(AtSdhChannelModeSet));
    }

/*
 * SET TX TTI
 */
eBool CmdSdhLineTxTti(char argc, char **argv)
    {
    return TtiSet(argc, argv, (TtiAttributeSet)AtSdhChannelTxTtiSet);
    }

/*
 * SET expected TTI
 */
eBool CmdSdhLineExptTti(char argc, char **argv)
    {
    return TtiSet(argc, argv, (TtiAttributeSet)AtSdhChannelExpectedTtiSet);
    }

/* TODO: It is deprecated by CmdSdhLineTtiGet, remove it after autotest module is updated */
eBool CmdSdhLineRxTti(char argc, char **argv)
    {
    uint32 ttiMsgLen;
    uint32 bufferSize, numberOfLines, i;
    eAtRet ret = cAtOk;
    eBool convertSuccess;
    tTab *tabPtr;
    char buff[16];
    const char *pPadding;
    const char *pHeading[] = {"LineId", "Tti Mode", "Message", "Padding"};
    uint32 *lineIds;
    AtSdhChannel sdhChannel;
    tAtSdhTti tti;

    AtUnused(argc);

    /* Get list of lines */
    lineIds = CliSharedIdBufferGet(&bufferSize);
    numberOfLines = CliIdListFromString(argv[0], lineIds, bufferSize);
    if (lineIds == NULL )
        {
        AtPrintc(cSevCritical, "ERROR: No line, the ID List may be wrong, expect 1,2-3,...\r\n");
        return cAtFalse;
        }
    mCliCheckLinesValid(numberOfLines);

    /* Create table with titles */
    tabPtr = TableAlloc(numberOfLines, 4, pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numberOfLines; i++)
        {
        /* Initialize value */
        AtOsalMemInit(&tti, 0, sizeof(tti));

        /* Print ID */
        AtSprintf(buff, "%d", lineIds[i]);
        StrToCell(tabPtr, i, 0, buff);

        /* Show RX TTI */
        sdhChannel = (AtSdhChannel)LineGet(SdhModule(), lineIds[i] - 1);
        ret = AtSdhChannelRxTtiGet(sdhChannel, &tti);
        tti.message[cAtSdhChannelMaxTtiLength - 2] = 0; /* If this is 64-byte message, ignore the last two bytes */
        ttiMsgLen = AtStrlen((char *)(tti.message));
        mAtEnumToStr(cCmdSdhTtiModeStr,
                     cCmdSdhTtiModeVal,
                     tti.mode,
                     buff,
                     convertSuccess);
        StrToCell(tabPtr, i, 1, convertSuccess ? buff : "Error");
        if (ttiMsgLen == 0)
            {
            StrToCell(tabPtr, i, 2, "None");
            StrToCell(tabPtr, i, 3, "None");
            }
        else
            {
            /* write message into the table */
            ColorStrToCell(tabPtr, i, 2, (char *)tti.message, cSevInfo);

            /* Detect padding character & restore original byte stream */
            pPadding = CliSdhTtiMsgRemvPadding(tti.message);
            ColorStrToCell(tabPtr, i, 3, pPadding, cSevInfo);
            }
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return (ret == cAtOk) ? cAtTrue : cAtFalse;
    }

/*
 * Show TTI message
 */
eBool CmdSdhLineTtiGet(char argc, char **argv)
    {
    uint8 ttiMsgLen;
    uint32 i, numberOfLines;
    eAtModuleSdhRet ret = cAtOk;
    eBool convertSuccess;
    tAtSdhTti tti;
    tTab *tabPtr;
    char buff[16];
    const char *pPadding;
    const char *pHeading[] = {"LineId", "txTti", "txTtiMode", "txPadding", "expTti", "expTtiMode",
                              "expPadding","rxTti", "rxTtiMode", "rxPadding", "ttiCompare"};
    AtList lineList = AtListCreate(0);
	AtUnused(argc);

    /* Get list of lines */
    CliSdhLineListFromString(lineList, argv[0]);
    numberOfLines = AtListLengthGet(lineList);
    if (numberOfLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No line, the ID List may be wrong, expect 1,2-3,...\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numberOfLines, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numberOfLines; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtSdhChannel line = (AtSdhChannel)AtListObjectGet(lineList, i);

        /* Initialize value */
        AtOsalMemInit(&tti, 0, sizeof(tti));

        /* Print ID */
        AtSprintf(buff, "%u", AtChannelIdGet((AtChannel)line) + 1);
        StrToCell(tabPtr, i, 0, buff);

        /* Print Tx TTI message */
        ret = AtSdhChannelTxTtiGet(line, &tti);
        tti.message[cAtSdhChannelMaxTtiLength - 2] = 0; /* If this is 64-byte message, ignore the last two bytes */

        pPadding = CliSdhTtiMsgRemvPadding(tti.message);
        ttiMsgLen = (uint8)AtStrlen((char *)tti.message);
        if (ttiMsgLen == 0)
            StrToCell(tabPtr, i, 1, "None");
        else
            ColorStrToCell(tabPtr, i, 1, (const char *)tti.message, cSevNormal);

        mAtEnumToStr(cCmdSdhTtiModeStr,
                     cCmdSdhTtiModeVal,
                     tti.mode,
                     buff,
                     convertSuccess);
        StrToCell(tabPtr, i, 2, convertSuccess ? buff : "Error");
        ColorStrToCell(tabPtr, i, 3, pPadding, cSevInfo);

        /* Expected message */
        AtOsalMemInit(&tti, 0, sizeof(tti));
        ret = AtSdhChannelExpectedTtiGet(line, &tti);
        tti.message[cAtSdhChannelMaxTtiLength - 2] = 0; /* If this is 64-byte message, ignore the last two bytes */

        /* Remove padding mode */
        pPadding = CliSdhTtiMsgRemvPadding(tti.message);
        ttiMsgLen = (uint8)AtStrlen((char *)tti.message);
        if (ttiMsgLen == 0)
            StrToCell(tabPtr, i, 4, "None");
        else
            StrToCell(tabPtr, i, 4, (const char *)tti.message);

        mAtEnumToStr(cCmdSdhTtiModeStr,
                     cCmdSdhTtiModeVal,
                     tti.mode,
                     buff,
                     convertSuccess);
        StrToCell(tabPtr, i, 5, convertSuccess ? buff : "Error");
        ColorStrToCell(tabPtr, i, 6, pPadding, cSevInfo);

        /* RX TTI */
        ret = AtSdhChannelRxTtiGet(line, &tti);
        tti.message[cAtSdhChannelMaxTtiLength - 2] = 0; /* If this is 64-byte message, ignore the last two bytes */
        ttiMsgLen = (uint8)AtStrlen((char *)(tti.message));
        if (ttiMsgLen == 0)
            StrToCell(tabPtr, i, 7, "None");
        else
            /* write message into the table */
            StrToCell(tabPtr, i, 7, (const char *)(tti.message));

        if (ret == cAtModuleSdhErrorTtiModeMismatch)
            {
            ColorStrToCell(tabPtr, i, 8, "mismatch", cSevCritical);
            ret = cAtOk;
            }
        else
            {
            mAtEnumToStr(cCmdSdhTtiModeStr,
                         cCmdSdhTtiModeVal,
                         tti.mode,
                         buff,
                         convertSuccess);
            StrToCell(tabPtr, i, 8, convertSuccess ? buff : "Error");
            }

            /* Detect padding character & restore original byte stream */
            pPadding = CliSdhTtiMsgRemvPadding(tti.message);
            ColorStrToCell(tabPtr, i, 9, pPadding, cSevInfo);

        /* TTI compare */
        mAtBoolToStr(AtSdhChannelTtiCompareIsEnabled(line), buff, convertSuccess);
        StrToCell(tabPtr, i, 10, convertSuccess ? buff : "Error");
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    AtObjectDelete((AtObject)lineList);
    return (ret == cAtOk) ? cAtTrue : cAtFalse;
    }

/*
 * SET transmit K1
 */
eBool CmdSdhLineTxK1(char argc, char **argv)
    {
    uint8 txK1Val = (uint8)AtStrtoul(argv[1], NULL, 16);
	AtUnused(argc);
    return AttributeSet(argv[0], txK1Val, (eAtRet (*)(AtSdhLine, uint32))AtSdhLineTxK1Set);
    }

/*
 * SET transmit K2
 */
eBool CmdSdhLineTxK2(char argc, char **argv)
    {
    uint8 txK2Val = (uint8)AtStrtoul(argv[1], NULL, 16);
	AtUnused(argc);
    return AttributeSet(argv[0], txK2Val, (eAtRet (*)(AtSdhLine, uint32))AtSdhLineTxK2Set);
    }

/*
 * SET transmit S1
 */
eBool CmdSdhLineTxS1(char argc, char **argv)
    {
    uint8 txS1Val = (uint8)AtStrtoul(argv[1], NULL, 16);
	AtUnused(argc);
    return AttributeSet(argv[0], txS1Val, (eAtRet (*)(AtSdhLine, uint32))AtSdhLineTxS1Set);
    }

/*
 * SET interrupt mask
 */
eBool CmdSdhLineIntrMsk(char argc, char **argv)
    {
    uint32 intrMaskVal;
    eBool enable = cAtFalse;
    char bufTemp[cMaxItem][cMaxBuf + 1];
    uint32 numberOfLines, i;
    eBool success = cAtTrue;
    eBool convertSuccess = cAtFalse;
    AtList lineList = AtListCreate(0);
	AtUnused(argc);

    /* Initialize value */
    AtOsalMemInit(bufTemp, 0, sizeof(bufTemp));

    /* Get list of lines */
    CliSdhLineListFromString(lineList, argv[0]);
    numberOfLines = AtListLengthGet(lineList);
    if (numberOfLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No line, the ID List may be wrong, expect 1,2-3,...\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    /* Get interrupt mask value from input string */
    if ((intrMaskVal = IntrMaskFromString(argv[1])) == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Interrupt mask is invalid\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    /* Enabling */
    mAtStrToBool(argv[2], enable, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Expected: en/dis\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    for (i = 0; i < numberOfLines; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret;

        AtSdhChannel line = (AtSdhChannel)AtListObjectGet(lineList, i);
        ret = AtChannelInterruptMaskSet((AtChannel)line, intrMaskVal, enable ? intrMaskVal : 0x0);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot configure interrupt mask for line %u, ret = %s\r\n",
                     AtChannelIdGet((AtChannel)line) + 1,
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    AtObjectDelete((AtObject)lineList);
    return success;
    }

/*
 * Set Loopback
 */
eBool CmdSdhLineLoopbackSet(char argc, char **argv)
    {
    eAtLoopbackMode loopbackMode = cAtLoopbackModeRelease;
    eBool convertSuccess;
	AtUnused(argc);

    mAtStrToEnum(cCmdLoopbackModeStr,
                 cCmdLoopbackModeVal,
                 argv[1],
                 loopbackMode,
                 convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid parameter <loopbackMode> , expected release|local|remote\r\n");
        return cAtFalse;
        }

    return AttributeSet(argv[0], loopbackMode, (eAtRet (*)(AtSdhLine, uint32))AtChannelLoopbackSet);
    }

/*
 * Get Loopback
 */
eBool CmdSdhLineLoopbackGet(char argc, char **argv)
    {
    eAtLoopbackMode loopbackMode;
    uint32 numberOfLines, i;
    eBool blResult;
    tTab *tabPtr;
    char buff[16];
    const char *heading[] ={"LineId", "Loopback"};
    AtList lineList = AtListCreate(0);
	AtUnused(argc);

    /* Get list of lines */
    CliSdhLineListFromString(lineList, argv[0]);
    numberOfLines = AtListLengthGet(lineList);
    if (numberOfLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No line, the ID List may be wrong, expect 1,2-3,...\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numberOfLines, 2, heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numberOfLines; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtSdhChannel line = (AtSdhChannel)AtListObjectGet(lineList, i);

        /* Print ID */
        AtSprintf(buff, "%u", AtChannelIdGet((AtChannel)line) + 1);
        StrToCell(tabPtr, i, 0, buff);

        /* Print loopback mode */

        loopbackMode = AtChannelLoopbackGet((AtChannel) line);

        mAtEnumToStr(cCmdLoopbackModeStr,
                     cCmdLoopbackModeVal,
                     loopbackMode,
                     buff,
                     blResult);
        ColorStrToCell(tabPtr, i, 1, blResult ? buff : "Error", blResult ? cSevInfo : cSevCritical);
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    AtObjectDelete((AtObject)lineList);
    return cAtTrue;
    }

/*
 * Alarm forcing
 */
eBool CmdSdhLineForceAlarm(char argc, char **argv)
    {
    eAtDirection direction;
    uint32 alarmTypes;
    eBool convertSuccess;
    eBool force = cAtFalse;
    eAtRet (*AlarmForce)(AtChannel self, uint32 alarmType);
	AtUnused(argc);

    /* Alarm types */
    if (argv[1] != NULL )
        alarmTypes = IntrMaskFromString(argv[1]);
    else
        {
        AtPrintc(cSevCritical, "ERROR: Invalid alarm type\r\n");
        return cAtFalse;
        }

    /* Direction */
    direction = CliDirectionFromStr(argv[2]);
    if(direction == cAtDirectionInvalid)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid direction. Expect: %s\r\n", CliValidDirections());
        return cAtFalse;
        }

    /* Enable/disable */
    mAtStrToBool(argv[3], force, convertSuccess);
    if(!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enable force. Expect: en/dis\r\n");
        return cAtFalse;
        }

    /* Determine function to use */
    if (direction == cAtDirectionTx)
        AlarmForce = force ? AtChannelTxAlarmForce : AtChannelTxAlarmUnForce;
    else
        AlarmForce = force ? AtChannelRxAlarmForce : AtChannelRxAlarmUnForce;

    return AttributeSet(argv[0], alarmTypes, (eAtRet (*)(AtSdhLine, uint32))AlarmForce);
    }

/*
 * Read/clear counter
 */
eBool CmdSdhLineCounterShow(char argc, char **argv)
    {
    uint32 numberOfLines, i;
    uint8 counter_i;
    eBool silent = cAtFalse;
    eAtHistoryReadingMode readMode;
    tTab *tabPtr = NULL;
    const char *pHeading[] = {"LineId", "B1 ERR", "B2 ERR", "REI", "B1 BE", "B2 BE", "REI BE"};
    uint32 (*BitErrorCounterGet)(AtChannel self, uint16 counterType)      = AtChannelCounterGet;
    uint32 (*BlockErrorCounterGet)(AtSdhChannel self, uint16 counterType) = AtSdhChannelBlockErrorCounterGet;
    AtList lineList = AtListCreate(0);
	AtUnused(argc);

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    /* Get list of lines */
    CliSdhLineListFromString(lineList, argv[0]);
    numberOfLines = AtListLengthGet(lineList);
    if (numberOfLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No line, the ID List may be wrong, expect 1,2-3,...\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    readMode = CliHistoryReadingModeGet(argv[1]);
    silent = CliHistoryReadingShouldSilent(readMode, (argc > 2) ? argv[2] : NULL);
    if (readMode == cAtHistoryReadingModeReadToClear)
        {
        BitErrorCounterGet   = AtChannelCounterClear;
        BlockErrorCounterGet = AtSdhChannelBlockErrorCounterClear;
        }

    /* Create table with titles */
    if (!silent)
        {
    tabPtr = TableAlloc(numberOfLines, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
            AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }
        }

    AtModuleLock((AtModule)SdhModule());

    /* Make table content */
    for (i = 0; i < numberOfLines; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        char buff[16];
        uint8 column = 0;
        uint32 counterValue;
        AtSdhChannel line = (AtSdhChannel)AtListObjectGet(lineList, i);

        /* Print list sdhLineId */
        AtSprintf(buff, "%u", AtChannelIdGet((AtChannel)line) + 1);
        StrToCell(tabPtr, i, column++, buff);

        /* Show bit error counters */
        for (counter_i = 0; counter_i < mCount(cCmdSdhLineCounterTypeVal); counter_i++)
            {
            counterValue = BitErrorCounterGet((AtChannel)line, cCmdSdhLineCounterTypeVal[counter_i]);
            AtSprintf(buff, "%u", counterValue);
            ColorStrToCell(tabPtr, i, column++, buff, (counterValue == 0) ? cSevInfo : cSevCritical);
            }

        /* Show block error counters */
        for (counter_i = 0; counter_i < mCount(cCmdSdhLineCounterTypeVal); counter_i++)
            {
            counterValue = BlockErrorCounterGet(line, cCmdSdhLineCounterTypeVal[counter_i]);
            AtSprintf(buff, "%u", counterValue);
            ColorStrToCell(tabPtr, i, column++, buff, (counterValue == 0) ? cSevInfo : cSevCritical);
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    AtModuleUnLock((AtModule)SdhModule());

    AtPrintc(cSevInfo,   "* Note: \r\n");
    AtPrintc(cSevNormal, "  - ERR: Bit error\r\n");
    AtPrintc(cSevNormal, "  - BE: Block error (not all of products support this)\r\n");

    AtObjectDelete((AtObject)lineList);
    return cAtTrue;
    }

/*
 * Show Line configuration
 */
eBool CmdSdhLineShowCfg(char argc, char **argv)
    {
    eAtSdhLineRate lineRate;
    eAtSdhChannelMode lineMode;
    uint32 i, numberOfLines;
    uint32 intrMaskVal;
    eAtTimingMode timingMd;
    eBool convertSuccess = cAtTrue;
    eAtLedState ledState;
    tTab *tabPtr;
    char buff[16];
    const char *heading[] ={"LineId", "rate", "mode","BER-SD", "BER-SF", "BER-TCA", "txK1", "rxK1","txK2",
                            "rxK2", "txS1", "rxS1", "intrMask", "LED", "scramble", "timMon",
                            "txEnable", "rxEnable", "autoRdi", "timingMode", "autoRei"};
    AtList lineList = AtListCreate(0);
	AtUnused(argc);

    /* Get list of lines */
    CliSdhLineListFromString(lineList, argv[0]);
    numberOfLines = AtListLengthGet(lineList);
    if (numberOfLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No line, the ID List may be wrong, expect 1,2-3,...\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numberOfLines, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numberOfLines; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        const char *stringValue;
        char *timingMdString;
        AtBerController berController;
        uint32 column = 0;
        eBool enabled;
        AtSdhLine line = (AtSdhLine)AtListObjectGet(lineList, i);

        /* Print ID */
        AtSprintf(buff, "%u", AtChannelIdGet((AtChannel)line) + 1);
        StrToCell(tabPtr, i, column++, buff);

        /* Print line rate */
        lineRate = AtSdhLineRateGet(line);
        mAtEnumToStr(cCmdSdhLineRateStr, cCmdSdhLineRateVal, lineRate, buff, convertSuccess);
        ColorStrToCell(tabPtr, i, column++, convertSuccess ? buff : "Error", convertSuccess ? cSevNormal : cSevCritical);

        /* Print line mode */
        lineMode = AtSdhChannelModeGet((AtSdhChannel)line);
        mAtEnumToStr(cCmdSdhChannelModeStr, cCmdSdhChannelModeVal, lineMode, buff, convertSuccess);
        ColorStrToCell(tabPtr, i, column++, convertSuccess ? buff : "Error", convertSuccess ? cSevNormal : cSevCritical);

        /* Print BER SD/SF/TCA threshold values */
        berController = AtSdhChannelBerControllerGet((AtSdhChannel)line);
        if (berController)
            {
        stringValue = CliBerRateString(AtBerControllerSdThresholdGet(berController));
        ColorStrToCell(tabPtr, i, column++, stringValue ? stringValue : "Error", stringValue ? cSevInfo : cSevCritical);
        stringValue = CliBerRateString(AtBerControllerSfThresholdGet(berController));
        ColorStrToCell(tabPtr, i, column++, stringValue ? stringValue : "Error", stringValue ? cSevInfo : cSevCritical);
        stringValue = CliBerRateString(AtBerControllerTcaThresholdGet(berController));
        ColorStrToCell(tabPtr, i, column++, stringValue ? stringValue : "Error", stringValue ? cSevInfo : cSevCritical);
            }
        else
            {
            StrToCell(tabPtr, i, column++, sAtNotCare);
            StrToCell(tabPtr, i, column++, sAtNotCare);
            StrToCell(tabPtr, i, column++, sAtNotCare);
            }

        /* K1 value */
        AtSprintf(buff, "0x%02x", AtSdhLineTxK1Get(line));
        ColorStrToCell(tabPtr, i, column++, buff, cSevNormal);
        AtSprintf(buff, "0x%02x", AtSdhLineRxK1Get(line));
        ColorStrToCell(tabPtr, i, column++, buff, cSevNormal);

        /* K2 value */
        AtSprintf(buff, "0x%02x", AtSdhLineTxK2Get(line));
        ColorStrToCell(tabPtr, i, column++, buff, cSevNormal);
        AtSprintf(buff, "0x%02x", AtSdhLineRxK2Get(line));
        ColorStrToCell(tabPtr, i, column++, buff, cSevNormal);

        /* S1 value */
        AtSprintf(buff, "0x%02x", AtSdhLineTxS1Get(line));
        ColorStrToCell(tabPtr, i, column++, buff, cSevNormal);
        AtSprintf(buff, "0x%02x", AtSdhLineRxS1Get(line));
        ColorStrToCell(tabPtr, i, column++, buff, cSevNormal);

        /* Print interrupt mask values */
        intrMaskVal = AtChannelInterruptMaskGet((AtChannel)line);
        ColorStrToCell(tabPtr, i, column++, Alarm2String(intrMaskVal), cSevNormal);

        /* LED state */
        ledState = AtSdhLineLedStateGet(line);
        ColorStrToCell(tabPtr, i, column++, CliLedStateStr(ledState), (ledState == cAtLedStateOff) ? cSevCritical : cSevInfo);

        /* Scramble */
        StrToCell(tabPtr, i, column++, AtSdhLineScrambleIsEnabled(line) ? "en" : "dis");

        /* Tim */
        StrToCell(tabPtr, i, column++, AtSdhChannelTimMonitorIsEnabled((AtSdhChannel)line) ? "en" : "dis");
        
        /* TX enabling */
        mAtBoolToStr(AtChannelTxIsEnabled((AtChannel)line), buff, convertSuccess);
        StrToCell(tabPtr, i, column++, convertSuccess ? buff : "Error");

        /* RX enabling */
        mAtBoolToStr(AtChannelRxIsEnabled((AtChannel)line), buff, convertSuccess);
        StrToCell(tabPtr, i, column++, convertSuccess ? buff : "Error");

        /* Auto RDI enabling */
        enabled = AtSdhChannelAutoRdiIsEnabled((AtSdhChannel)line);
        ColorStrToCell(tabPtr, i, column++, CliBoolToString(enabled), enabled ? cSevInfo : cSevCritical);

        /* Timing mode */
        timingMd = AtChannelTimingModeGet((AtChannel)line);
        timingMdString = CliTimingModeString(timingMd);
        ColorStrToCell(tabPtr, i, column++, timingMdString ? timingMdString : "N/A", cSevNormal);

        /* Auto REI enabling */
        enabled = AtSdhChannelAutoReiIsEnabled((AtSdhChannel)line);
        StrToCell(tabPtr, i, column++, CliBoolToString(enabled));
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    AtObjectDelete((AtObject)lineList);
    return cAtTrue;
    }

eBool CmdSdhLineShowAlarm(char argc, char **argv)
    {
    return StatusShow(argc, argv, AtChannelAlarmGet, cAtFalse);
    }

eBool CmdSdhLineShowListenedAlarm(char argc, char **argv)
    {
    uint32 bufferSize, numberOfLines, i;
    uint32 j;
    uint32 alarmStat;
    uint32 *sdhLine;
    AtSdhChannel sdhChannel;
    tTab *tabPtr;
    char buff[16];
    const char *pHeading[] = {"LineId", "Count", "Matched", "LOS", "OOF", "LOF", "TIM", "AIS", "RDI",
                              "BER-SD", "BER-SF", "BER-TCA", "KByte-Change", "KByte-Fail",
                              "S1-Change", "RS-SD", "RS-SF"};
    uint32 numAlarms;
    const uint32 *alarmTypeVal = CliAtSdhLineAlarmTypeVal(&numAlarms);
    uint32 (*ListenedDefectGet)(AtChannel self, uint32 * listenedCount);
    eAtHistoryReadingMode readingMode;
    uint32 counter;
    AtUnused(argc);

    /* Get list of lines */
    sdhLine = CliSharedIdBufferGet(&bufferSize);
    numberOfLines = CliIdListFromString(argv[0], sdhLine, bufferSize);
    if (sdhLine == NULL )
        {
        AtPrintc(cSevCritical, "ERROR: No line, the ID List may be wrong, expect 1,2-3,...\r\n");
        return cAtFalse;
        }
    mCliCheckLinesValid(numberOfLines);

    ListenedDefectGet = AtChannelListenedDefectGet;
    if (argc > 1)
        {
        /* Get reading action mode */
        readingMode = CliHistoryReadingModeGet(argv[1]);
        if (readingMode == cAtHistoryReadingModeUnknown)
            {
            AtPrintc(cSevCritical, "ERROR: Invalid reading mode. Expected %s\r\n", CliValidHistoryReadingModes());
            return cAtFalse;
            }

        if (readingMode == cAtHistoryReadingModeReadToClear)
            ListenedDefectGet = AtChannelListenedDefectClear;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numberOfLines, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numberOfLines; i++)
        {
        /* Print list LineIds */
        AtSprintf(buff, "%u", sdhLine[i]);
        StrToCell(tabPtr, i, 0, buff);

        /* Print alarm status */
        sdhChannel = (AtSdhChannel) LineGet(SdhModule(), sdhLine[i] - 1);
        alarmStat = ListenedDefectGet((AtChannel) sdhChannel, &counter);
        StrToCell(tabPtr, i, 1, CliNumber2String(counter, "%u"));
        StrToCell(tabPtr, i, 2, "TBD");

        for (j = 0; j < numAlarms; j++)
            {
            if (alarmStat & alarmTypeVal[j])
                ColorStrToCell(tabPtr, i, j + 3, "set", cSevCritical);
            else
                ColorStrToCell(tabPtr, i, j + 3, "clear", cSevInfo);
            }
        }
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

/*
 * Show Line interrupt
 */
eBool CmdSdhLineShowInterrupt(char argc, char **argv)
    {
    eAtHistoryReadingMode readingMode;
    eBool silent = cAtFalse;

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    silent = CliHistoryReadingShouldSilent(readingMode, (argc > 2) ? argv[2] : NULL);

    if (readingMode == cAtHistoryReadingModeReadToClear)
        return StatusShow(argc, argv, AtChannelAlarmInterruptClear, silent);
    else
        return StatusShow(argc, argv, AtChannelAlarmInterruptGet, silent);
    }

eBool CmdSdhLineLedStateSet(char argc, char **argv)
    {
    uint32 numberOfLines, i;
    eAtRet ret;
    eAtLedState ledState;
    AtList lineList = AtListCreate(0);
	AtUnused(argc);

    /* Get list of lines */
    CliSdhLineListFromString(lineList, argv[0]);
    numberOfLines = AtListLengthGet(lineList);
    if (numberOfLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No line, the ID List may be wrong, expect 1,2-3,...\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    /* Led state */
    ledState = CliLedStateFromStr(argv[1]);
    if (!AtLedStateIsValid(ledState))
        {
        AtPrintc(cSevCritical, "ERROR: Invalid LED state\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    /* Apply all input lines */
    for (i = 0; i < numberOfLines; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtSdhLine line = (AtSdhLine)AtListObjectGet(lineList, i);
        ret = AtSdhLineLedStateSet(line, ledState);
        if (ret != cAtOk)
            AtPrintc(cSevCritical,
                     "ERROR: Set LED state on Line %u fail, ret = %s\r\n",
                     AtChannelIdGet((AtChannel)line) + 1,
                     AtRet2String(ret));
        }

    AtObjectDelete((AtObject)lineList);
    return cAtTrue;
    }

/*
 * Enable Line alarm
 */
eBool CmdSdhLineAlarmEnable(char argc, char **argv)
    {
    return AlarmEnable(argc, argv, cAtTrue);
    }

/*
 * Disable Line alarm
 */
eBool CmdSdhLineAlarmDisable(char argc, char **argv)
    {
    return AlarmEnable(argc, argv, cAtFalse);
    }

eBool CmdSdhLineAlarmAffectGet(char argc, char **argv)
    {
    uint32 numberOfLines;
    uint32 i, j;
    eBool  isEnable;
    tTab *tabPtr;
    char buff[16];
    const char *pHeading[] = {"LineId", "LOS", "OOF", "LOF", "TIM", "AIS", "RDI",
                              "BER-SD", "BER-SF", "BER-TCA", "KByte-Change", "KBybe-Fail",
                              "S1-Change", "RS-SD", "RS-SF"};
    uint32 numAlarm;
    const uint32 *alarmTypeVal = CliAtSdhLineAlarmTypeVal(&numAlarm);
    AtList lineList = AtListCreate(0);
	AtUnused(argc);

    /* Get list of lines */
    CliSdhLineListFromString(lineList, argv[0]);
    numberOfLines = AtListLengthGet(lineList);
    if (numberOfLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No line, the ID List may be wrong, expect 1,2-3,...\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numberOfLines, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numberOfLines; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtSdhChannel line = (AtSdhChannel)AtListObjectGet(lineList, i);
        
        /* Print list LineIds */
        AtSprintf(buff, "%u", AtChannelIdGet((AtChannel)line) + 1);
        StrToCell(tabPtr, i, 0, buff);

        /* Print alarm affect */
        for (j = 0; j < numAlarm; j++)
            {
            isEnable = AtSdhChannelAlarmAffectingIsEnabled(line, alarmTypeVal[j]);
            AtSprintf(buff, "%s", (isEnable ? "en" : "dis"));
            ColorStrToCell(tabPtr, i, j+1, buff, isEnable ? cSevNormal : cSevCritical);
            }
        }
    TablePrint(tabPtr);
    TableFree(tabPtr);

    AtObjectDelete((AtObject)lineList);
    return cAtTrue;
    }

eBool CmdSdhLineForceError(char argc, char** argv)
    {
    uint32 numberOfLines, i;
    uint32 errorType = cAtSdhLineErrorNone;
    eBool convertSuccess;
    eAtRet ret = cAtOk;
    eBool forceNone = cAtFalse;
    AtList lineList = AtListCreate(0);
	AtUnused(argc);

    /* Get list of line */
    CliSdhLineListFromString(lineList, argv[0]);
    numberOfLines = AtListLengthGet(lineList);
    if (numberOfLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No line, the ID List may be wrong, expect 1,2-3,...\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    /* Get error counter type */
    if (AtStrcmp(argv[1], "none") == 0)
        forceNone = cAtTrue;
    else
        {
        mAtStrToEnum(cCmdSdhLineErrorTypeStr,
                     cCmdSdhLineErrorTypeVal,
                     argv[1],
                     errorType,
                     convertSuccess);
        if (!convertSuccess)
            {
            AtPrintc(cSevCritical, "ERROR: Invalid parameter <mode> , expected b1, b2, rei, none\r\n");
            AtObjectDelete((AtObject)lineList);
            return cAtFalse;
            }
        }

    for (i = 0; i < numberOfLines; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtChannel line = (AtChannel)AtListObjectGet(lineList, i);
        if (forceNone)
            {
            AtChannelTxErrorUnForce(line, cAtSdhLineErrorB1);
            AtChannelTxErrorUnForce(line, cAtSdhLineErrorB2);
            AtChannelTxErrorUnForce(line, cAtSdhLineErrorRei);
            }
        else
            {
            ret = AtChannelTxErrorForce(line, (uint8)errorType);
            if (ret != cAtOk)
                mWarnNotReturnCliIfNotOk(ret, line, "Can not force error counter type for line");
            }
        }

    AtObjectDelete((AtObject)lineList);
    return cAtTrue;
    }

eBool CmdSdhLineSerdesTimingModeSet(char argc, char** argv)
    {
    AtList controllers = CliAtSdhLineSerdesControllers(argv[0]);
    eBool success = CliAtSerdesControllerTimingModeSet(controllers, argv[1]);
	AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdSdhLineSerdesTimingModeGet(char argc, char** argv)
    {
    tTab *tabPtr;
    const char *pHeading[] = {"LineId", "serdesTiming"};
    uint32 numberOfLineSerdes, i;
    AtList controllers;

	AtUnused(argc);

    /* Get list of controllers */
    controllers = CliAtSdhLineSerdesControllers(argv[0]);
    if (AtListLengthGet(controllers) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No SERDES controller, list of lines may be invalid\r\n");
        return cAtTrue;
        }

    /* Create table with titles */
    numberOfLineSerdes = AtListLengthGet(controllers);
    tabPtr = TableAlloc(numberOfLineSerdes, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        AtObjectDelete((AtObject)controllers);
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numberOfLineSerdes; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtSerdesController controller = (AtSerdesController)AtListObjectGet(controllers, i);
        const char *stringValue;

        /* Line ID */
        StrToCell(tabPtr, i, 0, (char *)CliChannelIdStringGet(AtSerdesControllerPhysicalPortGet(controller)));

        /* Timing mode */
        stringValue = CliAtSerdesTimingModeString(AtSerdesControllerTimingModeGet(controller));
        StrToCell(tabPtr, i, 1, stringValue ? stringValue : "Error");
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    AtObjectDelete((AtObject)controllers);

    return cAtTrue;
    }

eBool CmdAtSdhLineDebug(char argc, char **argv)
    {
    uint32 numberOfLines, i;
    AtList lineList = AtListCreate(0);
	AtUnused(argc);

    /* Get list of line */
    CliSdhLineListFromString(lineList, argv[0]);
    numberOfLines = AtListLengthGet(lineList);
    if (numberOfLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No line, the ID List may be wrong, expect 1,2-3,...\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    for (i = 0; i < numberOfLines; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtChannel line = (AtChannel)AtListObjectGet(lineList, i);
        AtChannelDebug(line);
        }

    AtObjectDelete((AtObject)lineList);
    return cAtTrue;
    }

eBool CmdAtSdhLineScrambleEnable(char argc, char **argv)
    {
    AtUnused(argc);
    return AttributeSet(argv[0], cAtTrue, mAttributeSetFunc(AtSdhLineScrambleEnable));
    }

eBool CmdAtSdhLineScrambleDisable(char argc, char **argv)
    {
    AtUnused(argc);
    return AttributeSet(argv[0], cAtFalse, mAttributeSetFunc(AtSdhLineScrambleEnable));
    }

eBool CmdAtSdhLineTimMonitorEnable(char argc, char **argv)
    {
    AtUnused(argc);
    return AttributeSet(argv[0], cAtTrue, mAttributeSetFunc(AtSdhChannelTimMonitorEnable));
    }

eBool CmdAtSdhLineTimMonitorDisable(char argc, char **argv)
    {
    AtUnused(argc);
    return AttributeSet(argv[0], cAtFalse, mAttributeSetFunc(AtSdhChannelTimMonitorEnable));
    }

eBool CmdAtSdhLineEnable(char argc, char **argv)
    {
    AtUnused(argc);
    return AttributeSet(argv[0], cAtTrue, mAttributeSetFunc(AtChannelEnable));
    }

eBool CmdAtSdhLineDisable(char argc, char **argv)
    {
    AtUnused(argc);
    return AttributeSet(argv[0], cAtFalse, mAttributeSetFunc(AtChannelEnable));
    }

eBool CmdAtSdhLineSerdesControllerInit(char argc, char **argv)
    {
    AtList controllers = CliAtSdhLineSerdesControllers(argv[0]);
    eBool success = CliAtSerdesControllerInit(controllers);
	AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtSdhLineSerdesControllerEnable(char argc, char **argv)
    {
    return SerdesControllerEnable(argc, argv, cAtTrue);
    }

eBool CmdAtSdhLineSerdesControllerDisable(char argc, char **argv)
    {
    return SerdesControllerEnable(argc, argv, cAtFalse);
    }

eBool CmdAtSdhLineSerdesPhyParamSet(char argc, char **argv)
    {
    AtList controllers = CliAtSdhLineSerdesControllers(argv[0]);
    eBool success = CliAtSerdesControllerPhyParamSet(controllers, argv[1], argv[2]);
	AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtSdhLineSerdesShow(char argc, char **argv)
    {
    AtList controllers = CliAtSdhLineSerdesControllers(argv[0]);
    eBool success = CliAtSerdesControllersShow(controllers);
	AtUnused(argc);
    AtObjectDelete((AtObject)controllers);

    return success;
    }

eBool CmdAtSdhLineSerdesAlarmShow(char argc, char **argv)
    {
    eBool silent = cAtFalse;
    AtList controllers = CliAtSdhLineSerdesControllers(argv[0]);
    eBool success = CliAtSerdesControllersAlarmShow(controllers, AtSerdesControllerAlarmGet, silent);
	AtUnused(argc);
    AtObjectDelete((AtObject)controllers);

    return success;
    }

eBool CmdAtSdhLineSerdesAlarmHistoryShow(char argc, char **argv)
    {
    eBool silent = cAtFalse;
    AtList controllers = CliAtSdhLineSerdesControllers(argv[0]);
    eBool success;
    eAtHistoryReadingMode readingMode;
    uint32 (*AlarmGet)(AtSerdesController);
    AtUnused(argc);

    /* Get reading action mode */
    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        AtObjectDelete((AtObject)controllers);
        return cAtFalse;
        }

    if (readingMode == cAtHistoryReadingModeReadOnly)
        AlarmGet = AtSerdesControllerAlarmHistoryGet;
    else
        AlarmGet = AtSerdesControllerAlarmHistoryClear;

    success = CliAtSerdesControllersAlarmShow(controllers, AlarmGet, silent);
    AtObjectDelete((AtObject)controllers);

    return success;
    }

eBool CmdAtSdhLineSerdesParamShow(char argc, char **argv)
    {
    AtList controllers = CliAtSdhLineSerdesControllers(argv[0]);
    eBool success = CliAtSerdesControllersParamShow(controllers, (argc < 2) ? NULL : argv[1]);
    AtObjectDelete((AtObject)controllers);

    return success;
    }

eBool CmdAtSdhLineSerdesDebug(char argc, char **argv)
    {
    AtList controllers = CliAtSdhLineSerdesControllers(argv[0]);
    eBool success = CliAtSerdesControllerDebug(controllers);
	AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtSdhLineSerdesLoopback(char argc, char **argv)
    {
    AtList controllers = CliAtSdhLineSerdesControllers(argv[0]);
    eBool success = CliAtSerdesControllerLoopback(controllers, argv[1]);
	AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtSdhLineSwitch(char argc, char **argv)
    {
    uint8 fromLineId = (uint8)AtStrtoul(argv[0], NULL, 10);
    uint8 toLineId   = (uint8)AtStrtoul(argv[1], NULL, 10);
	AtUnused(argc);
    return ApsConfigure(fromLineId, toLineId, (eAtRet (*)(AtSdhLine, AtSdhLine))AtSdhLineSwitch);
    }

eBool CmdAtSdhLineSwitchRelease(char argc, char **argv)
    {
	AtUnused(argc);
    return ApsRelease(argv[0], (eAtRet (*)(AtSdhLine))AtSdhLineSwitchRelease);
    }

eBool CmdAtSdhLineBridge(char argc, char **argv)
    {
    uint8 fromLineId = (uint8)AtStrtoul(argv[0], NULL, 10);
    uint8 toLineId   = (uint8)AtStrtoul(argv[1], NULL, 10);
	AtUnused(argc);
    return ApsConfigure(fromLineId, toLineId, (eAtRet (*)(AtSdhLine, AtSdhLine))AtSdhLineBridge);
    }

eBool CmdAtSdhLineBridgeRelease(char argc, char **argv)
    {
	AtUnused(argc);
    return ApsRelease(argv[0], (eAtRet (*)(AtSdhLine))AtSdhLineBridgeRelease);
    }

eBool CmdAtSdhLineApsGet(char argc, char **argv)
    {
    tTab *tabPtr;
    const char *pHeading[] = {"LineId", "switchedLine", "bridgedLine"};
    uint32 numberOfLines, i;
    AtSdhLine line;
    AtList lineList = AtListCreate(0);
	AtUnused(argc);

    /* Get list of line */
    CliSdhLineListFromString(lineList, argv[0]);
    numberOfLines = AtListLengthGet(lineList);
    if (numberOfLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No line, the ID List may be wrong, expect 1,2-3,...\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numberOfLines, 3, pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numberOfLines; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtSdhLine tempLine;

        line = (AtSdhLine)AtListObjectGet(lineList, i);

        /* Print list linesId */
        StrToCell(tabPtr, i, 0, CliChannelIdStringGet((AtChannel)line));

        tempLine = AtSdhLineSwitchedLineGet(line);
        if (tempLine == NULL)
            StrToCell(tabPtr, i, 1, "none");
        else
            StrToCell(tabPtr, i, 1, (const char*)CliNumber2String(AtChannelIdGet((AtChannel)tempLine) + 1, "%u"));

        tempLine = AtSdhLineBridgedLineGet(line);
        if (tempLine == NULL)
            StrToCell(tabPtr, i, 2, "none");
        else
            StrToCell(tabPtr, i, 2, (const char*)CliNumber2String(AtChannelIdGet((AtChannel)tempLine) + 1, "%u"));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    AtObjectDelete((AtObject)lineList);
    return cAtTrue;
    }

eBool CmdAtSdhLineAlarmCapture(char argc, char **argv)
    {
    eAtRet ret = cAtOk;
    uint32 numberOfLines, i;
    static tAtChannelEventListener intrListener;
    eBool captureEnabled = cAtTrue;
    eBool convertSuccess;
    eBool success = cAtTrue;
    AtList lineList = AtListCreate(0);
	AtUnused(argc);

    /* Get list of line */
    CliSdhLineListFromString(lineList, argv[0]);
    numberOfLines = AtListLengthGet(lineList);
    if (numberOfLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No line, the ID List may be wrong, expect 1,2-3,...\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    AtOsalMemInit(&intrListener, 0, sizeof(intrListener));
    if (AutotestIsEnabled())
        intrListener.AlarmChangeStateWithUserData = AutotestAlarmChangeState;
    else if (CliInterruptLogIsEnabled())
        intrListener.AlarmChangeState = AlarmChangeLogging;
    else
        intrListener.AlarmChangeState = AlarmChangeState;

    /* Enabling */
    mAtStrToBool(argv[1], captureEnabled, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enabling mode, expected en/dis\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    for (i = 0; i < numberOfLines; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtChannel line = (AtChannel)AtListObjectGet(lineList, i);

        if (captureEnabled)
            {
            if (AutotestIsEnabled())
                ret = AtChannelEventListenerAddWithUserData(line, &intrListener, CliAtSdhLineObserverGet(line));
            else
                ret = AtChannelEventListenerAdd(line, &intrListener);
            }
        else
            {
            ret = AtChannelEventListenerRemove(line, &intrListener);

            if (AutotestIsEnabled())
                CliAtSdhLineObserverDelete(line);
            }

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot %s alarm listener for line %u, ret = %s\r\n",
                     captureEnabled ? "register" : "deregister",
                     AtChannelIdGet(line) + 1,
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    AtObjectDelete((AtObject)lineList);
    return success;
    }

eBool CmdAtSdhLineForcedAlarmsGet(char argc, char **argv)
    {
    uint32 numberOfLines, i;
    tTab *tabPtr;
    const char *heading[] = {"LineId", "ForcedTxAlarms", "ForcedRxAlarms"};
    AtList lineList = AtListCreate(0);
	AtUnused(argc);

    /* Get list of lines */
    CliSdhLineListFromString(lineList, argv[0]);
    numberOfLines = AtListLengthGet(lineList);
    if (numberOfLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No line, the ID List may be wrong, expect 1,2-3,...\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numberOfLines, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    /* Make table content */
    for (i = 0; i < numberOfLines; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        uint32 alarmMask;
        AtChannel line = (AtChannel)AtListObjectGet(lineList, i);

        StrToCell(tabPtr, i, 0, CliNumber2String(AtChannelIdGet(line) + 1, "%u"));

        alarmMask = AtChannelTxForcedAlarmGet(line);
        ColorStrToCell(tabPtr, i, 1, Alarm2String(alarmMask), alarmMask ? cSevCritical : cSevNormal);

        alarmMask = AtChannelRxForcedAlarmGet(line);
        ColorStrToCell(tabPtr, i, 2, Alarm2String(alarmMask), alarmMask ? cSevCritical : cSevNormal);
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    AtObjectDelete((AtObject)lineList);
    return cAtTrue;
    }

eBool CmdAtSdhLineTxEnable(char argc, char **argv)
    {
    AtUnused(argc);
    return AttributeSet(argv[0], cAtTrue, mAttributeSetFunc(AtChannelTxEnable));
    }

eBool CmdAtSdhLineTxDisable(char argc, char **argv)
    {
    AtUnused(argc);
    return AttributeSet(argv[0], cAtFalse, mAttributeSetFunc(AtChannelTxEnable));
    }

eBool CmdAtSdhLineRxEnable(char argc, char **argv)
    {
    AtUnused(argc);
    return AttributeSet(argv[0], cAtTrue, mAttributeSetFunc(AtChannelRxEnable));
    }

eBool CmdAtSdhLineRxDisable(char argc, char **argv)
    {
    AtUnused(argc);
    return AttributeSet(argv[0], cAtFalse, mAttributeSetFunc(AtChannelRxEnable));
    }

eBool CmdAtSdhLineAutoRdiEnable(char argc, char **argv)
    {
    return AutoRdiEnable(argc, argv, cAtTrue);
    }

eBool CmdAtSdhLineAutoRdiDisable(char argc, char **argv)
    {
    return AutoRdiEnable(argc, argv, cAtFalse);
    }

eBool CmdAtSdhLineLogShow(char argc, char** argv)
    {
    uint32 numberOfLines, i;
    AtList lineList = AtListCreate(0);
    AtUnused(argc);

    /* Get list of lines */
    CliSdhLineListFromString(lineList, argv[0]);
    numberOfLines = AtListLengthGet(lineList);
    if (numberOfLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No line, the ID List may be wrong, expect 1,2-3,...\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    for (i = 0; i < numberOfLines; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        AtChannelLogShow((AtChannel)AtListObjectGet(lineList, i));

    AtObjectDelete((AtObject)lineList);
    return cAtTrue;
    }

static eBool LogEnable(char argc, char** argv, eBool enable)
    {
    uint32 numberOfLines, i;
    AtChannel line;
    eAtRet ret;
    eBool success = cAtTrue;
    AtList lineList = AtListCreate(0);
    AtUnused(argc);

    /* Get list of lines */
    CliSdhLineListFromString(lineList, argv[0]);
    numberOfLines = AtListLengthGet(lineList);
    if (numberOfLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No line, the ID List may be wrong, expect 1,2-3,...\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    for (i = 0; i < numberOfLines; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        line = (AtChannel)AtListObjectGet(lineList, i);
        ret = AtChannelLogEnable(line, enable);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot enable logging for line %s, ret = %s\r\n",
                     CliChannelIdStringGet(line),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    AtObjectDelete((AtObject)lineList);
    return success;
    }

eBool CmdAtSdhLineLogEnable(char argc, char** argv)
    {
    return LogEnable(argc, argv, cAtTrue);
    }

eBool CmdAtSdhLineLogDisable(char argc, char** argv)
    {
    return LogEnable(argc, argv, cAtFalse);
    }

eBool CmdAtSdhLineSerdesControllerReset(char argc, char **argv)
    {
    AtList controllers = CliAtSdhLineSerdesControllers(argv[0]);
    eBool success = CliAtSerdesControllerReset(controllers);
    AtUnused(argc);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdSdhLineTimingModeSet(char argc, char **argv)
    {
    uint32          numberOfLines, i;
    AtChannel       timingSrc = NULL;
    eAtTimingMode   timingMode;
    eBool success = cAtTrue;
    AtList lineList = AtListCreate(0);

    AtUnused(argc);

    /* Get list of lines */
    CliSdhLineListFromString(lineList, argv[0]);
    numberOfLines = AtListLengthGet(lineList);
    if (numberOfLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No line, the ID List may be wrong, expect 1,2-3,...\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    /* Parse timing mode */
    timingMode = CliTimingModeFromString(argv[1]);
    if (timingMode != cAtTimingModeLoop && timingMode != cAtTimingModeSys && timingMode != cAtTimingModeExt1Ref)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid timing mode parameter. Expected: %s\r\n", "system/loop/ext#1");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    /* Apply for all of input lines */
    for (i = 0; i < numberOfLines; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
       {
       AtChannel line = (AtChannel)AtListObjectGet(lineList, i);
       eAtRet ret = AtChannelTimingSet(line, timingMode, timingSrc);

       if (ret != cAtOk)
           {
           AtPrintc(cSevCritical, "ERROR: Can not set timing mode for %s, ret = %s\r\n",
                    CliChannelIdStringGet(line),
                    AtRet2String(ret));
           success = cAtFalse;
           }
       }

    AtObjectDelete((AtObject)lineList);
    return success;
    }

eBool CmdAtSdhLineAlarmCaptureShow(char argc, char **argv)
    {
    uint32 numChannels, i;
    const char *pHeading[] = {"LineId", "LOS", "OOF", "LOF", "TIM", "AIS", "RDI",
                              "BER-SD", "BER-SF", "BER-TCA", "KByte-Change",
                              "S1-Change", "RS-SD", "RS-SF", "K1-Change", "K2-Change"};
    eAtHistoryReadingMode readMode;
    AtChannel *channelList;
    tTab *tabPtr = NULL;
    eBool silent = cAtFalse;

    AtUnused(argc);

    /* Get list of lines */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = LinesFromString(argv[0], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid SDH line list, expected: 1 or 1-3, ...\n");
        return cAtFalse;
        }

    /* If list channel don't raised any event, just report not-changed */
    readMode = CliHistoryReadingModeGet(argv[1]);

    if (argc > 2)
        silent = CliHistoryReadingShouldSilent(readMode, argv[2]);

    if (!silent)
        {
        /* Create table with titles */
        tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
            return cAtFalse;
            }
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtChannel channel = channelList[i];
        AtChannelObserver observer = CliAtSdhLineObserverGet(channel);
        uint8 column = 0;
        uint32 current;
        uint32 history;

        StrToCell(tabPtr, i, column++, CliChannelIdStringGet(channel));

        current = AtChannelObserverDefectGet(observer);
        if (readMode == cAtHistoryReadingModeReadToClear)
            history = AtChannelObserverDefectHistoryClear(observer);
        else
            history = AtChannelObserverDefectHistoryGet(observer);

        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtSdhLineAlarmLos);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtSdhLineAlarmOof);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtSdhLineAlarmLof);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtSdhLineAlarmTim);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtSdhLineAlarmAis);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtSdhLineAlarmRdi);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtSdhLineAlarmBerSd);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtSdhLineAlarmBerSf);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtSdhLineAlarmBerTca);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtSdhLineAlarmKByteChange);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtSdhLineAlarmS1Change);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtSdhLineAlarmRsBerSd);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtSdhLineAlarmRsBerSf);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtSdhLineAlarmK1Change);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtSdhLineAlarmK2Change);

        /* Delete observer */
        CliAtSdhLineObserverDelete(channel);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtSdhLineTtiCompareEnable(char argc, char **argv)
    {
    AtUnused(argc);
    return AttributeSet(argv[0], cAtTrue, mAttributeSetFunc(AtSdhChannelTtiCompareEnable));
    }

eBool CmdAtSdhLineTtiCompareDisable(char argc, char **argv)
    {
    AtUnused(argc);
    return AttributeSet(argv[0], cAtFalse, mAttributeSetFunc(AtSdhChannelTtiCompareEnable));
    }

eBool CmdAtSdhLineSerdesControllerEqualizerModeSet(char argc, char **argv)
    {
    AtList controllers = CliAtSdhLineSerdesControllers(argv[0]);
    eBool success = CliAtSerdesControllerEqualizerModeSet(controllers, argc, argv);
    AtObjectDelete((AtObject)controllers);
    return success;
    }

eBool CmdAtSdhLineTxOverheadByteSet(char argc, char **argv)
    {
    uint32 line_i;
    uint32 ohByte;
    uint8  ohByteValue;
    AtChannel *channelList;
    uint32 numLines;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get list of lines */
    channelList = CliSharedChannelListGet(&numLines);
    numLines = LinesFromString(argv[0], channelList, numLines);
    if (!numLines)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid SDH line list, expected: 1 or 1-3, ...\n");
        return cAtFalse;
        }

    /* Get input overhead byte */
    ohByte = CliStringToEnum(argv[1],
                             cCmdSdhLineOverheadByteStr,
                             cCmdSdhLineOverheadByteValue,
                             mCount(cCmdSdhLineOverheadByteValue),
                             &success);
    if (success == cAtFalse)
        {
        AtPrintc(cSevCritical, "ERROR: Please input correct overhead byte. Expect: ");
        CliExpectedValuesPrint(cSevCritical, cCmdSdhLineOverheadByteStr, mCount(cCmdSdhLineOverheadByteValue));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    ohByteValue = (uint8)AtStrToDw(argv[2]);
    for (line_i = 0; line_i < numLines; line_i = AtCliNextIdWithSleep(line_i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret = AtSdhChannelTxOverheadByteSet((AtSdhChannel)channelList[line_i], ohByte, ohByteValue);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical,
                 "ERROR: Setting TOH on %s fail with ret = %s\r\n",
                 CliChannelIdStringGet((AtChannel)channelList[line_i]),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

eBool CmdAtSdhLineOverheadByteGet(char argc, char **argv)
    {
    eAtDirection direction = cAtDirectionAll;
    eBool success = cAtTrue;
    uint32 stm1Number;
    uint32 line_i;
    AtChannel *channelList;
    uint32 numLines;

    /* Get list of lines */
    channelList = CliSharedChannelListGet(&numLines);
    numLines = LinesFromString(argv[0], channelList, numLines);
    if (numLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid SDH line list, expected: 1 or 1-3, ...\n");
        return cAtFalse;
        }

    /* STM-1 number */
    stm1Number = AtStrtoul(argv[1], NULL, 10);

    /* Optional direction. */
    if (argc > 2)
        {
        direction = CliDirectionFromStr(argv[2]);
        if(direction == cAtDirectionInvalid)
            {
            AtPrintc(cSevCritical, "ERROR: Invalid direction parameter. Expected: %s\r\n", CliValidDirections());
            return cAtFalse;
            }
        }

    for (line_i = 0; line_i < numLines; line_i++)
        success |= LineTohShow((AtSdhChannel)channelList[line_i], stm1Number, direction);

    return success;
    }

eBool CmdAtSdhLineAlarmAutoRdiGet(char argc, char **argv)
    {
    uint32 numLines;
    uint32 line_i, alarm_i;
    tTab *tabPtr;
    const char *pHeading[] = {"LineId", "LOS", "LOF", "AIS", "TIM"};
    const uint32 cSdhLineAlarmType[] = {cAtSdhLineAlarmLos,
                                        cAtSdhLineAlarmLof,
                                        cAtSdhLineAlarmAis,
                                        cAtSdhLineAlarmTim};
    AtList lineList = AtListCreate(0);

    AtUnused(argc);

    /* Get list of lines */
    CliSdhLineListFromString(lineList, argv[0]);
    numLines = AtListLengthGet(lineList);
    if (numLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No line, the ID List may be wrong, expect 1,2-3,...\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numLines, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    /* Make table content */
    for (line_i = 0; line_i < numLines; line_i++)
        {
        char *string;
        AtSdhChannel line = (AtSdhChannel)AtListObjectGet(lineList, line_i);
        uint32 column = 0;

        /* Line ID */
        string = CliNumber2String(AtChannelIdGet((AtChannel)line) + 1, "%u");
        StrToCell(tabPtr, line_i, column++, string);

        /* Print alarm affect */
        for (alarm_i = 0; alarm_i < mCount(cSdhLineAlarmType); alarm_i++)
            {
            eBool enabled = AtSdhChannelAlarmAutoRdiIsEnabled(line, cSdhLineAlarmType[alarm_i]);
            ColorStrToCell(tabPtr, line_i, column++, CliBoolToString(enabled), enabled ? cSevInfo : cSevCritical);
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtObjectDelete((AtObject)lineList);

    return cAtTrue;
    }

static eBool AutoReiEnable(char argc, char **argv, eBool enable)
    {
    uint32 numberOfLines, i;
    eBool success = cAtTrue;
    AtList lineList = AtListCreate(0);

    AtUnused(argc);

    /* Get list of lines */
    CliSdhLineListFromString(lineList, argv[0]);
    numberOfLines = AtListLengthGet(lineList);
    if (numberOfLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No line, the ID List may be wrong, expect 1,2-3,...\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    for (i = 0; i < numberOfLines; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtSdhChannel line = (AtSdhChannel)AtListObjectGet(lineList, i);
        eAtRet ret;
        ret = AtSdhChannelAutoReiEnable(line, enable);

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: %s auto REI on %s fail with ret = %s\r\n",
                     enable ? "Enable" : "Disable",
                     CliChannelIdStringGet((AtChannel)line),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    AtObjectDelete((AtObject)lineList);
    return success;
    }

eBool CmdAtSdhLineAutoReiEnable(char argc, char **argv)
    {
    return AutoReiEnable(argc, argv, cAtTrue);
    }

eBool CmdAtSdhLineAutoReiDisable(char argc, char **argv)
    {
    return AutoReiEnable(argc, argv, cAtFalse);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : CliAtSdhLineDcc.h
 * 
 * Created Date: Sep 9, 2016
 *
 * Description : Line DCC
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLIATSDHLINEDCC_H_
#define _CLIATSDHLINEDCC_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtList.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtList CliPwDccHdlcListFromString(char *pStringDccIds);
AtPw* CliPwDccArrayFromStringGet(char *pStringDccIds, uint32 *numberPws);
AtList CliAtDccHdlcListFromString(char *pStringDccIds);
AtHdlcChannel *CliDccHdlcArrayFromString(char *pStringDccIds, uint32 *numChannels);

#ifdef __cplusplus
}
#endif
#endif /* _CLIATSDHLINEDCC_H_ */


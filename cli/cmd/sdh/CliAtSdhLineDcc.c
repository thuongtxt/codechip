/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : CliAtSdhLineDcc.c
 *
 * Created Date: Sep 9, 2016
 *
 * Description : CLIs to control DCC
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTokenizer.h"
#include "AtHdlcChannel.h"
#include "AtSdhLine.h"
#include "CliAtSdhLineDcc.h"
#include "CliAtModuleSdh.h"
#include "../encap/CliAtHdlcChannel.h"
#include "../encap/CliAtModuleEncap.h"
#include "AtCliModule.h"
#include "AtChannelHierarchyDisplayer.h"

/*--------------------------- Define -----------------------------------------*/
#define cAtSdhLineDccLayerNumbers 2
typedef eAtModuleEncapRet (*HdlcChannelAttributeSetFunc)(AtHdlcChannel, uint32);
typedef eAtRet (*EnableAttributeSetFunc)(AtChannel, eBool);

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static const char *cAtSdhLineDccLayerStr[] =
    {
    "unknown", "section", "line", "all"
    };

static const uint32 cAtSdhLineDccLayerVal[] =
    {
    cAtSdhLineDccLayerUnknown, cAtSdhLineDccLayerSection, cAtSdhLineDccLayerLine, cAtSdhLineDccLayerAll
    };

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModulePw PwModule(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return (AtModulePw) AtDeviceModuleGet(device, cAtModulePw);
    }

static eBool ListUpdate(AtList hdlcList, char *lineListString, eAtSdhLineDccLayer layer)
    {
    uint32 numberOfLines, i;
    eBool success = cAtTrue;
    AtList lineList = AtListCreate(0);

    CliSdhLineListFromString(lineList, lineListString);
    numberOfLines = AtListLengthGet(lineList);
    if (numberOfLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No line, the ID List may be wrong, expect 1,2-3,...\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    /* Apply configure for all input lines */
    for (i = 0; i < numberOfLines; i++)
        {
        AtSdhLine line = (AtSdhLine)AtListObjectGet(lineList, i);
        AtHdlcChannel hdlc = AtSdhLineDccChannelGet(line, layer);

        if (hdlc == NULL)
            continue;

        if (!AtListContainsObject(hdlcList, (AtObject) hdlc))
            AtListObjectAdd(hdlcList, (AtObject) hdlc);
        }

    AtObjectDelete((AtObject)lineList);
    return success;
    }

static eBool HdlcListUpdateFromDccIdString(AtList hdlcList, char *pStringDccIds)
    {
    char buf[128];
    uint32 length = AtStrlen(pStringDccIds);
    eBool success = cAtFalse;

    AtOsalMemInit(buf, 0, sizeof(buf));

    /* Sub string with format "id1s.section or id1s.line" */
    if (AtStrrchr(pStringDccIds, 'a') != NULL)
        {
        if ((length >= 9) && (AtStrncmp(&pStringDccIds[length - 4], ".all", 4) == 0))
            {
            AtStrncpy(buf, &pStringDccIds[0], (length - 4));
            success = ListUpdate(hdlcList, buf, cAtSdhLineDccLayerAll);
            }
        }
    else if (AtStrrchr(pStringDccIds, 'l') != NULL)
        {
        if ((length >= 6) && (AtStrncmp(&pStringDccIds[length - 5], ".line", 5) == 0))
            {
            AtStrncpy(buf, &pStringDccIds[0], (length - 5));
            success = ListUpdate(hdlcList, buf, cAtSdhLineDccLayerLine);
            }
        }

    else if (AtStrrchr(pStringDccIds, 's') != NULL)
        {
        if ((length >= 9) && (AtStrncmp(&pStringDccIds[length - 8], ".section", 8) == 0))
            {
            AtStrncpy(buf, &pStringDccIds[0], (length - 8));
            success = ListUpdate(hdlcList, buf, cAtSdhLineDccLayerSection);
            }
        }

    return success;
    }

static AtList DccHdlcListFromString(char *pStringDccIds)
    {
    AtList hdlcList = AtListCreate(0);
    AtTokenizer tokenizer = AtTokenizerSharedTokenizer(pStringDccIds, cOrChar);

    while (AtTokenizerHasNextString(tokenizer))
        {
        char *pSubStrIdList = AtTokenizerNextString(tokenizer);

        if (!pSubStrIdList)
            {
            AtObjectDelete((AtObject)hdlcList);
            return NULL;
            }

        if (AtStrrchr(pSubStrIdList, '.') == NULL)
            {
            AtObjectDelete((AtObject)hdlcList);
            return NULL;
            }

        if (!HdlcListUpdateFromDccIdString(hdlcList, pSubStrIdList))
            {
            AtObjectDelete((AtObject)hdlcList);
            return NULL;
            }
        }

    return hdlcList;
    }

static uint32 LayerGet(char *pLayerStr)
    {
    eBool blResult;
    eAtSdhLineDccLayer layer = cAtSdhLineDccLayerUnknown;
    mAtStrToEnum(cAtSdhLineDccLayerStr, cAtSdhLineDccLayerVal, pLayerStr, layer, blResult);
    if (!blResult)
        {
        layer = cAtSdhLineDccLayerUnknown;
        AtPrintc(cSevCritical, "ERROR: Invalid parameter <layer> , expected section|line\r\n");
        }

    return layer;
    }

static uint32 Layers(char *pLayerStrs, eAtSdhLineDccLayer *layerList, uint32 numberLayers)
    {
    AtTokenizer tokenizer = AtTokenizerSharedTokenizer(pLayerStrs, cOrChar);
    uint32 numberValid = 0;

    while (AtTokenizerHasNextString(tokenizer))
        {
        eAtSdhLineDccLayer layer;
        char *pSubStr = AtTokenizerNextString(tokenizer);
        if (!pSubStr)
            {
            AtPrintc(cSevCritical, "ERROR: Invalid parameter <layer> , expected section|line or all\r\n");
            return 0;
            }

        layer = LayerGet(pSubStr);
        if (layer != cAtSdhLineDccLayerUnknown)
            {
            layerList[numberValid] = layer;
            numberValid++;

            if (numberValid >= numberLayers)
                return numberValid;
            }
        }

    return numberValid;
    }

static eBool DccEnable(char argc, char **argv, eBool enable, EnableAttributeSetFunc enableFunc)
    {
    uint32 i;
    AtList hdlcList = DccHdlcListFromString(argv[0]);
    uint32 numberHdlc = AtListLengthGet(hdlcList);
    eBool success = cAtTrue;

    AtUnused(argc);
    if (numberHdlc == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No DCC HDLC, the ID List may be wrong, expect 1.section or 1.line or 1.section|line \r\n");
        AtObjectDelete((AtObject)hdlcList);
        return cAtFalse;
        }


    for (i = 0; i < numberHdlc; i++)
        {
        eAtRet ret;
        AtChannel hdlcChannel = (AtChannel)AtListObjectGet(hdlcList, i);
        ret = enableFunc(hdlcChannel, enable);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: can not enable/disable on %s, ret = %s\r\n",
                     AtChannelIdString((AtChannel)hdlcChannel),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    AtObjectDelete((AtObject)hdlcList);
    return success;
    }

AtHdlcChannel *CliDccHdlcArrayFromString(char *pStringDccIds, uint32 *numChannels)
    {
    AtList channels = DccHdlcListFromString(pStringDccIds);
    uint32 numObjects;
    AtObject *objects = CliSharedObjectListGet(&numObjects);
    *numChannels = CliListToArray(channels, objects, numObjects);
    AtObjectDelete((AtObject)channels);
    return (AtHdlcChannel *)objects;
    }

AtList CliPwDccHdlcListFromString(char *pStringDccIds)
    {
    uint32 i;
    AtList dccPwList = NULL;
    AtList hdlcList = DccHdlcListFromString(pStringDccIds);
    uint32 numberHdlc = AtListLengthGet(hdlcList);

    if (numberHdlc == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No DCC HDLC, the ID List may be wrong, expect 1.section or 1.line or 1.section|line \r\n");
        AtObjectDelete((AtObject)hdlcList);
        return NULL;
        }

    dccPwList = AtListCreate(0);
    for (i = 0; i < numberHdlc; i++)
        {
        AtHdlcChannel hdlcChannel = (AtHdlcChannel)AtListObjectGet(hdlcList, i);
        AtPw pw = AtChannelBoundPwGet((AtChannel) hdlcChannel);
        if (pw)
            AtListObjectAdd(dccPwList, (AtObject) pw);
        }

    AtObjectDelete((AtObject)hdlcList);
    return dccPwList;
    }

AtPw* CliPwDccArrayFromStringGet(char *pStringDccIds, uint32 *numberPws)
    {
    uint32 i;
    uint32 numValidPw;
    uint32 bufferSize;
    AtPw* pws = (AtPw *)CliSharedChannelListGet(&bufferSize);
    AtList dccPws = CliPwDccHdlcListFromString(pStringDccIds);
    uint32 numDccPws = AtListLengthGet(dccPws);

    *numberPws = 0;
    if (numDccPws == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No DCC HDLC, the ID List may be wrong, expect 1.section or 1.line or 1.section|line \r\n");
        AtObjectDelete((AtObject)dccPws);
        return NULL;
        }

    numValidPw = 0;
    for (i = 0; i < numDccPws; i++)
    	{
		AtPw pw = (AtPw) AtListObjectGet(dccPws, i);
		if (numValidPw >= bufferSize)
			break;

		pws[numValidPw++] = pw;
    	}

    AtObjectDelete((AtObject)dccPws);
    *numberPws = numValidPw;
    return pws;
    }

AtList CliAtDccHdlcListFromString(char *pStringDccIds)
    {
    return DccHdlcListFromString(pStringDccIds);
    }

eBool CmdSdhLineDccHdlcFlagSet(char argc, char **argv)
    {
    uint32 numChannels;
    AtHdlcChannel *channels = CliDccHdlcArrayFromString(argv[0], &numChannels);
    char *numFlagsString = NULL;

    if (argc > 2)
        numFlagsString = argv[2];

    AtUnused(argc);
    return CliAtHdlcChannelFlagSet(channels, numChannels, argv[1], numFlagsString);
    }

eBool CmdSdhLineDccHdlcIdlePatternSet(char argc, char **argv)
    {
    uint32 numChannels, channel_i;
    AtHdlcChannel *channels;
    uint8 idlePattern;
    eBool success = cAtTrue;

    /* Get channels */
    AtUnused(argc);
    channels = CliDccHdlcArrayFromString(argv[0], &numChannels);
    if (numChannels == 0)
        return cAtFalse;

    idlePattern = (uint8)AtStrtoul(argv[1], NULL, 16);
    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        eAtRet ret = AtHdlcChannelIdlePatternSet(channels[channel_i], idlePattern);
        if (ret != cAtOk)
            {
            AtPrintc(cSevWarning,
                     "Can not change Idle pattern for HDLC channel %s, ret = %s\n",
                     CliChannelIdStringGet((AtChannel)channels[channel_i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdSdhLineDccHdlcMtuSet(char argc, char **argv)
    {
    uint32 numChannels;
    AtHdlcChannel *channels = CliDccHdlcArrayFromString(argv[0], &numChannels);
    AtUnused(argc);
    return CliAtHdlcChannelMtuSet(channels, numChannels, argv[1]);
    }

eBool CmdSdhLineDccHdlcFcsSet(char argc, char **argv)
    {
    uint32 numChannels;
    AtHdlcChannel *channels = CliDccHdlcArrayFromString(argv[0], &numChannels);
    AtUnused(argc);
    return CliAtHdlcChannelFcsModeSet(channels, numChannels, argv[1]);
    }

eBool CmdSdhLineDccHdlcFcsBitOrderSet(char argc, char **argv)
    {
    uint32 numChannels;
    AtHdlcChannel *channels = CliDccHdlcArrayFromString(argv[0], &numChannels);
    AtUnused(argc);
    return CliAtHdlcChannelFcsCalculationMode(channels, numChannels, argv[1]);
    }

eBool CmdSdhLineDccHdlcShow(char argc, char **argv)
    {
    uint32 i;
    AtList hdlcList = DccHdlcListFromString(argv[0]);
    uint32 numberHdlc = AtListLengthGet(hdlcList);
    tTab *tabPtr;
    const char *heading[] = {"ChannelId", "TxEnable", "RxEnable", "Flag", "NumFlags", "IdlePattern", "MTU", "FcsMode", "fcsBitOrder", "intrMask", "Scramble", "stuffing"};

    AtUnused(argc);
    if (numberHdlc == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No DCC HDLC, the ID List may be wrong, expect 1.section or 1.line or 1.section|line \r\n");
        AtObjectDelete((AtObject)hdlcList);
        return cAtFalse;
        }

    tabPtr = TableAlloc(numberHdlc, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        AtObjectDelete((AtObject)hdlcList);
        return cAtFalse;
        }

    for (i = 0; i < numberHdlc; i++)
        {
        const char *string;
        uint32 intrMaskVal;
        uint32 colum = 0;
        eBool enabled;
        AtHdlcChannel hdlcChannel = (AtHdlcChannel)AtListObjectGet(hdlcList, i);

        StrToCell(tabPtr, i, colum++, AtChannelIdString((AtChannel)hdlcChannel));
        StrToCell(tabPtr, i, colum++, CliBoolToString(AtChannelTxIsEnabled((AtChannel)hdlcChannel)));
        StrToCell(tabPtr, i, colum++, CliBoolToString(AtChannelRxIsEnabled((AtChannel)hdlcChannel)));
        StrToCell(tabPtr, i, colum++, CliNumber2String(AtHdlcChannelFlagGet(hdlcChannel), "0x%02x"));
        StrToCell(tabPtr, i, colum++, CliNumber2String(AtHdlcChannelNumFlagsGet(hdlcChannel), "%u"));
        StrToCell(tabPtr, i, colum++, CliNumber2String(AtHdlcChannelIdlePatternGet(hdlcChannel), "0x%02x"));
        StrToCell(tabPtr, i, colum++, CliNumber2String(AtHdlcChannelMtuGet(hdlcChannel), "%u"));

        string = CliHdlcFcsModeToString(AtHdlcChannelFcsModeGet(hdlcChannel));
        StrToCell(tabPtr, i, colum++, string ? string : "Error");
        string = CliAtHdlcFcsCalculationMode2Str(AtHdlcChannelFcsCalculationModeGet(hdlcChannel));
        ColorStrToCell(tabPtr, i, colum++, string ? string : "Error", string ? cSevNormal : cSevCritical);

        /* Print interrupt mask values */
        intrMaskVal = AtChannelInterruptMaskGet((AtChannel)hdlcChannel);
        ColorStrToCell(tabPtr, i, colum++, CliHdlcChannelAlarm2String(intrMaskVal), cSevNormal);

        enabled = AtHdlcChannelScrambleIsEnabled(hdlcChannel);
        ColorStrToCell(tabPtr, i, colum++, CliBoolToString(enabled), enabled ? cSevInfo : cSevCritical);

        string = CliAtHdlcStuffMode2String(AtHdlcChannelStuffModeGet(hdlcChannel));
        ColorStrToCell(tabPtr, i, colum++, string ? string : "error", string ? cSevNormal : cSevCritical);
        }

    AtObjectDelete((AtObject)hdlcList);
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdSdhLineDccHdlcCountersShow(char argc, char **argv)
    {
    uint32 i;
    tTab *tabPtr = NULL;
    AtList hdlcList = DccHdlcListFromString(argv[0]);
    uint32 numberHdlc = AtListLengthGet(hdlcList);
    uint32 (*CounterGet)(AtChannel self, uint16 counterType) = AtChannelCounterGet;
    eBool silent = cAtFalse;
    eAtHistoryReadingMode readingMode = cAtHistoryReadingModeUnknown;
    const char *heading[] = {
                            "ID",

                            "txByte",
                            "txPacket",
                            "txGoodByte",
                            "txGoodPacket",
                            "txIdleByte",
                            "txAbortPacket",
                            "txMTUPacket",
                            "txDiscardPacket",

                            "rxByte",
                            "rxPacket",
                            "rxGoodByte",
                            "rxGoodPacket",
                            "rxIdleByte",
                            "rxMRUPacket",
                            "rxAbort",
                            "rxFcsErr",
                            "rxDiscardPacket"
                            };


    AtUnused(argc);
    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid parameter <readingMode> expected r2c/ro\r\n");
        return cAtFalse;
        }

    if (argc >= 2)
        silent = CliHistoryReadingShouldSilent(readingMode, argv[2]);

    /* Calculate number of tables need to show counters of all input Channels */

    if (numberHdlc == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No DCC HDLC, the ID List may be wrong, expect 1.section or 1.line or 1.section|line \r\n");
        AtObjectDelete((AtObject)hdlcList);
        return cAtFalse;
        }
    
    if (!silent)
        {
        tabPtr = TableAlloc(numberHdlc, mCount(heading), heading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "Cannot allocate table\r\n");
            AtObjectDelete((AtObject)hdlcList);
            return cAtFalse;
            }
        }

    if (readingMode == cAtHistoryReadingModeReadToClear)
        CounterGet = AtChannelCounterClear;

    for (i = 0; i < numberHdlc; i++)
        {
        uint32 colum = 0;
        AtChannel hdlcChannel = (AtChannel)AtListObjectGet(hdlcList, i);
        StrToCell(tabPtr, i, colum++, AtChannelIdString(hdlcChannel));
        CliChannelCounterPrintToCell(hdlcChannel, CounterGet, cAtHdlcChannelCounterTypeTxBytes, cAtCounterTypeGood, tabPtr, i, colum++);
        CliChannelCounterPrintToCell(hdlcChannel, CounterGet, cAtHdlcChannelCounterTypeTxPackets, cAtCounterTypeGood, tabPtr, i, colum++);
        CliChannelCounterPrintToCell(hdlcChannel, CounterGet, cAtHdlcChannelCounterTypeTxGoodBytes, cAtCounterTypeGood, tabPtr, i, colum++);
        CliChannelCounterPrintToCell(hdlcChannel, CounterGet, cAtHdlcChannelCounterTypeTxGoodPackets, cAtCounterTypeGood, tabPtr, i, colum++);
        CliChannelCounterPrintToCell(hdlcChannel, CounterGet, cAtHdlcChannelCounterTypeTxIdleBytes, cAtCounterTypeGood, tabPtr, i, colum++);
        CliChannelCounterPrintToCell(hdlcChannel, CounterGet, cAtHdlcChannelCounterTypeTxAborts, cAtCounterTypeError, tabPtr, i, colum++);
        CliChannelCounterPrintToCell(hdlcChannel, CounterGet, cAtHdlcChannelCounterTypeTxMTUPackets, cAtCounterTypeError, tabPtr, i, colum++);
        CliChannelCounterPrintToCell(hdlcChannel, CounterGet, cAtHdlcChannelCounterTypeTxDiscardFrames, cAtCounterTypeError, tabPtr, i, colum++);

        CliChannelCounterPrintToCell(hdlcChannel, CounterGet, cAtHdlcChannelCounterTypeRxBytes, cAtCounterTypeGood, tabPtr, i, colum++);
        CliChannelCounterPrintToCell(hdlcChannel, CounterGet, cAtHdlcChannelCounterTypeRxPackets, cAtCounterTypeGood, tabPtr, i, colum++);
        CliChannelCounterPrintToCell(hdlcChannel, CounterGet, cAtHdlcChannelCounterTypeRxGoodBytes, cAtCounterTypeGood, tabPtr, i, colum++);
        CliChannelCounterPrintToCell(hdlcChannel, CounterGet, cAtHdlcChannelCounterTypeRxGoodPackets, cAtCounterTypeGood, tabPtr, i, colum++);
        CliChannelCounterPrintToCell(hdlcChannel, CounterGet, cAtHdlcChannelCounterTypeRxIdleBytes, cAtCounterTypeGood, tabPtr, i, colum++);
        CliChannelCounterPrintToCell(hdlcChannel, CounterGet, cAtHdlcChannelCounterTypeRxMRUPackets, cAtCounterTypeError, tabPtr, i, colum++);
        CliChannelCounterPrintToCell(hdlcChannel, CounterGet, cAtHdlcChannelCounterTypeRxAborts, cAtCounterTypeError, tabPtr, i, colum++);
        CliChannelCounterPrintToCell(hdlcChannel, CounterGet, cAtHdlcChannelCounterTypeRxFcsErrors, cAtCounterTypeError, tabPtr, i, colum++);
        CliChannelCounterPrintToCell(hdlcChannel, CounterGet, cAtHdlcChannelCounterTypeRxDiscardFrames, cAtCounterTypeError, tabPtr, i, colum++);
        }

    AtObjectDelete((AtObject)hdlcList);
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdSdhLineDccHdlcCreate(char argc, char **argv)
    {
    uint32 numberOfLines, i, j, numberLayers;
    eBool success = cAtTrue;
    AtList lineList = AtListCreate(0);
    eAtSdhLineDccLayer dccLayer[cAtSdhLineDccLayerNumbers];

    /* Get list of lines */
    AtUnused(argc);
    CliSdhLineListFromString(lineList, argv[0]);
    numberOfLines = AtListLengthGet(lineList);
    if (numberOfLines == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No line, the ID List may be wrong, expect 1,2-3,...\r\n");
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    numberLayers = Layers(argv[1], dccLayer, cAtSdhLineDccLayerNumbers);
    if (numberLayers == 0)
        {
        AtPrintc(cSevCritical, "ERROR: can not paser dcc layer. Input %s\r\n", argv[1]);
        AtObjectDelete((AtObject)lineList);
        return cAtFalse;
        }

    /* Apply configure for all input lines */
    for (i = 0; i < numberOfLines; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtSdhLine line = (AtSdhLine)AtListObjectGet(lineList, i);
        for (j = 0; j < numberLayers; j++)
            {
            AtHdlcChannel dcc = AtSdhLineDccChannelCreate(line, dccLayer[j]);
            if (dcc == NULL)
                {
                AtPrintc(cSevCritical,
                         "ERROR: Cannot create dcc on  line %u, layer %s!. It may be existed!\r\n",
                         AtChannelIdGet((AtChannel) line) + 1,
                         argv[1]);
                success = cAtFalse;
                }
            }
        }

    AtObjectDelete((AtObject)lineList);
    return success;
    }

eBool CmdSdhLineDccHdlcDelete(char argc, char **argv)
    {
    uint32 i;
    AtList hdlcList = DccHdlcListFromString(argv[0]);
    uint32 numberHdlc = AtListLengthGet(hdlcList);
    eBool success = cAtTrue;

    if (numberHdlc == 0)
        {
        AtPrintc(cSevWarning, "ERROR: No DCC HDLC, the ID List may be wrong, expect 1.section or 1.line or 1.section|line \r\n");
        AtObjectDelete((AtObject)hdlcList);
        return cAtTrue;
        }

    AtUnused(argc);
    for (i = 0; i < numberHdlc; i++)
        {
        AtHdlcChannel dcc = (AtHdlcChannel)AtListObjectGet(hdlcList, i);
        AtSdhLine line = (AtSdhLine) AtEncapChannelBoundPhyGet((AtEncapChannel) dcc);
        eAtRet ret = AtSdhLineDccChannelDelete(line, dcc);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot delete DCC on line %s\r\n",
                     AtChannelIdString((AtChannel) line));
            success = cAtFalse;
            }
        }

    AtObjectDelete((AtObject)hdlcList);
    return success;
    }

eBool CmdSdhLineDccHdlcInit(char argc, char **argv)
    {
    uint32 i;
    AtList hdlcList = DccHdlcListFromString(argv[0]);
    uint32 numberHdlc = AtListLengthGet(hdlcList);
    eBool success = cAtTrue;

    if (numberHdlc == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No DCC HDLC, the ID List may be wrong, expect 1.section or 1.line or 1.section|line \r\n");
        AtObjectDelete((AtObject)hdlcList);
        return cAtFalse;
        }

    AtUnused(argc);
    for (i = 0; i < numberHdlc; i++)
        {
        AtPw pw;
        AtHdlcChannel dcc = (AtHdlcChannel)AtListObjectGet(hdlcList, i);
        eAtRet ret = AtChannelInit((AtChannel) dcc);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot Init DCC with return code %s\r\n",
                     AtChannelIdString((AtChannel) dcc));
            success = cAtFalse;
            }

        pw = AtChannelBoundPwGet((AtChannel) dcc);
        if (pw == NULL)
            pw = (AtPw)AtModulePwHdlcCreate(PwModule((AtChannel) dcc), (AtHdlcChannel) dcc);

        ret = AtChannelInit((AtChannel) pw);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot Init DCC with return code %s\r\n",
                     AtChannelIdString((AtChannel) dcc));
            success = cAtFalse;
            }
        }

    AtObjectDelete((AtObject)hdlcList);
    return success;
    }

eBool CmdSdhLineDccHdlcInterruptsShow(char argc, char **argv)
    {
    uint32 numChannels;
    AtHdlcChannel *channels = CliDccHdlcArrayFromString(argv[0], &numChannels);

    if (numChannels == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No DCC HDLC, the ID List may be wrong, expect 1.section or 1.line or 1.section|line \r\n");
        return cAtFalse;
        }

    return CliAtHdlcChannelInterruptShow(argc, argv, channels, numChannels);
    }

eBool CmdSdhLineDccHdlcAlarmShow(char argc, char **argv)
    {
    uint32 numChannels;
    AtHdlcChannel *channels = CliDccHdlcArrayFromString(argv[0], &numChannels);
    AtUnused(argc);

    if (numChannels == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No DCC HDLC, the ID List may be wrong, expect 1.section or 1.line or 1.section|line \r\n");
        return cAtFalse;
        }

    return CliAtHdlcChannelAlarmShow(channels, numChannels);
    }

eBool CmdSdhLineDccHdlcAlarmCapture(char argc, char **argv)
    {
    eAtRet ret = cAtOk;
    uint32 i;
    static tAtChannelEventListener intrListener;
    eBool captureEnabled = cAtTrue;
    eBool convertSuccess;
    eBool success = cAtTrue;
    uint32 numChannels;
    AtHdlcChannel *channels;
    AtUnused(argc);

    /* Get list of DCC/HDLC channel */
    channels = CliDccHdlcArrayFromString(argv[0], &numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No DCC HDLC, the ID List may be wrong, expect 1.section or 1.line or 1.section|line \r\n");
        return cAtFalse;
        }

    AtOsalMemInit(&intrListener, 0, sizeof(intrListener));
    intrListener.AlarmChangeState = CliAtHdlcChannelAlarmChangeState;

    /* Enabling */
    mAtStrToBool(argv[1], captureEnabled, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enabling mode, expected en/dis\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtChannel dccChannel = (AtChannel)channels[i];

        if (captureEnabled)
            ret = AtChannelEventListenerAdd(dccChannel, &intrListener);
        else
            ret = AtChannelEventListenerRemove(dccChannel, &intrListener);

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot %s alarm listener for %s, ret = %s\r\n",
                     captureEnabled ? "register" : "deregister",
                     AtChannelIdString(dccChannel),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdSdhLineDccEnable(char argc, char **argv)
    {
    return DccEnable(argc, argv, cAtTrue, AtChannelEnable);
    }

eBool CmdSdhLineDccDisable(char argc, char **argv)
    {
    return DccEnable(argc, argv, cAtFalse, AtChannelEnable);
    }

eBool CmdSdhLineDccTxEnable(char argc, char **argv)
    {
    return DccEnable(argc, argv, cAtTrue, AtChannelTxEnable);
    }

eBool CmdSdhLineDccTxDisable(char argc, char **argv)
    {
    return DccEnable(argc, argv, cAtFalse, AtChannelTxEnable);
    }

eBool CmdSdhLineDccRxEnable(char argc, char **argv)
    {
    return DccEnable(argc, argv, cAtTrue, AtChannelRxEnable);
    }

eBool CmdSdhLineDccRxDisable(char argc, char **argv)
    {
    return DccEnable(argc, argv, cAtFalse, AtChannelRxEnable);
    }

eBool CmdSdhLineDccHdlcInterruptMsk(char argc, char **argv)
    {
    uint32 numChannels;
    AtHdlcChannel *channels = CliDccHdlcArrayFromString(argv[0], &numChannels);
    AtUnused(argc);

    if (numChannels == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No DCC HDLC, the ID List may be wrong, expect 1.section or 1.line or 1.section|line \r\n");
        return cAtFalse;
        }

    return CliAtHdlcChannelIntrMaskSet(argv[1], argv[2], channels, numChannels);
    }

eBool CmdSdhLineDccScrambleEnable(char argc, char **argv)
    {
    return DccEnable(argc, argv, cAtTrue, (EnableAttributeSetFunc)AtHdlcChannelScrambleEnable);
    }

eBool CmdSdhLineDccScrambleDisable(char argc, char **argv)
    {
    return DccEnable(argc, argv, cAtFalse, (EnableAttributeSetFunc)AtHdlcChannelScrambleEnable);
    }

eBool CmdSdhLineDccStuffModeSet(char argc, char **argv)
    {
    uint32 numChannels;
    AtHdlcChannel *channels = CliDccHdlcArrayFromString(argv[0], &numChannels);
    AtUnused(argc);
    return CliAtHdlcChannelStuffModeSet(channels, numChannels, argv[1]);
    }

eBool CmdSdhLineDccHdlcHierarchy(char argc, char **argv)
    {
    uint32 numberOfLines, i;
    uint32 numChannelToRest = 1;
    AtChannelHierarchyDisplayer displayer;
    AtList lineList = AtListCreate(0);
    AtUnused(argc);

    CliSdhLineListFromString(lineList, argv[0]);
    numberOfLines = AtListLengthGet(lineList);
    displayer = AtSdhDccHierarchyDisplayerGet(cAtChannelHierarchyModeAlarm);

    for (i = 0; i < numberOfLines; i = AtCliNextIdWithSleep(i, numChannelToRest))
        {
        AtChannel line = (AtChannel)AtListObjectGet(lineList, i);
        AtChannelHierarchyShow(displayer, line, "");
        }

    AtObjectDelete((AtObject)lineList);
    return cAtTrue;
    }

eBool CmdSdhLineDccHdlcDebugShow(char argc, char **argv)
    {
    uint32 i;
    AtList hdlcList = DccHdlcListFromString(argv[0]);
    uint32 numHdlcs = AtListLengthGet(hdlcList);

    AtUnused(argc);

    if (numHdlcs == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No DCC HDLC, the ID List may be wrong, expect 1.section or 1.line or 1.section|line \r\n");
        AtObjectDelete((AtObject)hdlcList);
        return cAtFalse;
        }

    for (i = 0; i < numHdlcs; i++)
        {
        AtChannel hdlcChannel = (AtChannel)AtListObjectGet(hdlcList, i);
        AtChannelDebug(hdlcChannel);
        }

    AtObjectDelete((AtObject)hdlcList);

    return cAtTrue;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : CliAtSdhLineDcc.c
 *
 * Created Date: Sep 9, 2016
 *
 * Description : CLIs to control DCC
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTokenizer.h"
#include "AtHdlcChannel.h"
#include "AtSdhLine.h"
#include "CliAtSdhLineDcc.h"
#include "CliAtModuleSdh.h"
#include "../encap/CliAtModuleEncap.h"
#include "AtCliModule.h"
#include "../prbs/CliAtPrbsEngine.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtList PrbsEnginesGet(char *idString)
    {
	uint32 channel_i;
	AtList engines = AtListCreate(0);
	AtList hdlcList = CliAtDccHdlcListFromString(idString);
	uint32 numberHdlc = AtListLengthGet(hdlcList);

	if (numberHdlc == 0)
		{
		AtPrintc(cSevCritical, "ERROR: No DCC HDLC, the ID List may be wrong, expect <lineId>.section or <lineId>.line or <lineId>.section|line \r\n");
		AtObjectDelete((AtObject)hdlcList);
		return engines;
		}

    for (channel_i = 0; channel_i < numberHdlc; channel_i++)
        {
    	AtHdlcChannel hdlcChannel = (AtHdlcChannel)AtListObjectGet(hdlcList, channel_i);
        AtPrbsEngine engine = AtChannelPrbsEngineGet((AtChannel)hdlcChannel);
        if (engine)
            AtListObjectAdd(engines, (AtObject)engine);
        }

    AtObjectDelete((AtObject)hdlcList);

    return engines;
    }

eBool CmdSdhLineDccHdlcPrbsEnable(char argc, char **argv)
    {
	AtList engines = PrbsEnginesGet(argv[0]);
	AtUnused(argc);
	CliAtPrbsEngineEnable(engines, cAtTrue);
	AtObjectDelete((AtObject)engines);
	return cAtTrue;
    }

eBool CmdSdhLineDccHdlcPrbsDisable(char argc, char **argv)
    {
	AtList engines = PrbsEnginesGet(argv[0]);
	AtUnused(argc);
	CliAtPrbsEngineEnable(engines, cAtFalse);
	AtObjectDelete((AtObject)engines);
	return cAtTrue;
    }

eBool CmdSdhLineDccHdlcPrbsMode(char argc, char **argv)
	{
	AtList engines = PrbsEnginesGet(argv[0]);
	AtUnused(argc);
	CliAtPrbsEngineModeSet(engines, argv[1]);
	AtObjectDelete((AtObject)engines);
	return cAtTrue;
	}

eBool CmdSdhLineDccHdlcPrbsInvertEnable(char argc, char **argv)
	{
	AtList engines = PrbsEnginesGet(argv[0]);
	AtUnused(argc);
	CliAtPrbsEngineInvert(engines, cAtTrue);
	AtObjectDelete((AtObject)engines);
	return cAtTrue;
	}

eBool CmdSdhLineDccHdlcPrbsInvertDisable(char argc, char **argv)
	{
	AtList engines = PrbsEnginesGet(argv[0]);

	AtUnused(argc);
	CliAtPrbsEngineInvert(engines, cAtFalse);
	AtObjectDelete((AtObject)engines);
	return cAtTrue;
	}

eBool CmdSdhLineDccHdlcPrbsForce(char argc, char **argv)
	{
	AtList engines = PrbsEnginesGet(argv[0]);

	AtUnused(argc);
	CliAtPrbsEngineErrorForce(engines, cAtTrue);
	AtObjectDelete((AtObject)engines);

	return cAtTrue;
	}

eBool CmdSdhLineDccHdlcPrbsUnforce(char argc, char **argv)
	{
	AtList engines = PrbsEnginesGet(argv[0]);

	AtUnused(argc);
	CliAtPrbsEngineErrorForce(engines, cAtFalse);
	AtObjectDelete((AtObject)engines);
	return cAtTrue;
	}

eBool CmdSdhLineDccHdlcPrbsForceSingle(char argc, char **argv)
	{
	AtList engines = PrbsEnginesGet(argv[0]);

	AtUnused(argc);
	CliAtPrbsEngineErrorInject(engines, (argc > 1) ? (uint32)AtStrToDw(argv[1]) : 1);
	AtObjectDelete((AtObject)engines);
	return cAtTrue;
	}

eBool CmdSdhLineDccHdlcPrbsForceBer(char argc, char **argv)
	{
	AtList engines = PrbsEnginesGet(argv[0]);

	AtUnused(argc);
	CliAtPrbsEngineTxErrorRateForce(engines, argv[1]);
	AtObjectDelete((AtObject)engines);

	return cAtTrue;
	}

eBool CmdSdhLineDccHdlcPrbsFixPattern(char argc, char **argv)
	{
	AtList engines = PrbsEnginesGet(argv[0]);

	AtUnused(argc);
	CliAtPrbsEngineTxFixedPatternSet(engines, AtStrToDw(argv[1]));
	CliAtPrbsEngineRxFixedPatternSet(engines,  AtStrToDw(argv[2]));
	AtObjectDelete((AtObject)engines);
	return cAtTrue;
	}

eBool CmdSdhLineDccHdlcPrbsBitOrder(char argc, char **argv)
	{
	AtList engines = PrbsEnginesGet(argv[0]);

	AtUnused(argc);
	CliAtPrbsEngineBitOrderSet(engines, argv[1]);
	AtObjectDelete((AtObject)engines);

	return cAtTrue;
	}

eBool CmdSdhLineDccHdlcPrbsSide(char argc, char **argv)
	{
	AtList engines = PrbsEnginesGet(argv[0]);

	AtUnused(argc);
	CliAtPrbsEngineSideSet(engines, argv[1]);
	AtObjectDelete((AtObject)engines);

	return cAtTrue;
	}

eBool CmdSdhLineDccHdlcPrbsSideGenerating(char argc, char **argv)
	{
	AtList engines = PrbsEnginesGet(argv[0]);

	AtUnused(argc);
	CliAtPrbsEngineGeneratingSideSet(engines, argv[1]);
	AtObjectDelete((AtObject)engines);

	return cAtTrue;
	}

eBool CmdSdhLineDccHdlcPrbsSideMonitoring(char argc, char **argv)
	{
	AtList engines = PrbsEnginesGet(argv[0]);

	AtUnused(argc);
	CliAtPrbsEngineMonitoringSideSet(engines, argv[1]);
	AtObjectDelete((AtObject)engines);

	return cAtTrue;
	}

eBool CmdSdhLineDccHdlcPrbsTxEnable(char argc, char **argv)
	{
	AtList engines = PrbsEnginesGet(argv[0]);

	AtUnused(argc);
	CliAtPrbsEngineTxEnable(engines, cAtTrue);
	AtObjectDelete((AtObject)engines);
	return cAtTrue;
	}

eBool CmdSdhLineDccHdlcPrbsTxDisable(char argc, char **argv)
	{
	AtList engines = PrbsEnginesGet(argv[0]);

	AtUnused(argc);
	CliAtPrbsEngineTxEnable(engines, cAtFalse);
	AtObjectDelete((AtObject)engines);
	return cAtTrue;
	}

eBool CmdSdhLineDccHdlcPrbsRxEnable(char argc, char **argv)
	{
	AtList engines = PrbsEnginesGet(argv[0]);

	AtUnused(argc);
	CliAtPrbsEngineRxEnable(engines, cAtTrue);
	AtObjectDelete((AtObject)engines);
	return cAtTrue;
	}

eBool CmdSdhLineDccHdlcPrbsRxDisable(char argc, char **argv)
	{
	AtList engines = PrbsEnginesGet(argv[0]);
	AtUnused(argc);
	CliAtPrbsEngineRxEnable(engines, cAtFalse);
	AtObjectDelete((AtObject)engines);
	return cAtTrue;
	}

eBool CmdShowSdhLineDccHdlcPrbs(char argc, char **argv)
	{
	AtList engines = PrbsEnginesGet(argv[0]);
	eBool silent = cAtFalse;

	if (argc > 2)
		silent = CliIsSilentIndication(argv[2]);

	AtUnused(argc);
	CliAtPrbsEngineShow(engines, silent);
	AtObjectDelete((AtObject)engines);
	return cAtTrue;
	}

eBool CmdShowSdhLineDccHdlcPrbsBer(char argc, char **argv)
	{
	AtList engines = PrbsEnginesGet(argv[0]);
	CliAtPrbsEngineBitErrorRateShow(engines);
	AtUnused(argc);
	AtObjectDelete((AtObject)engines);
	return cAtTrue;
	}

eBool CmdShowSdhLineDccHdlcPrbsCounters(char argc, char **argv)
	{
	AtList engines = PrbsEnginesGet(argv[0]);

	AtUnused(argc);
	CliAtPrbsEngineCounterGet(engines, argv[1], (argc >= 2) ? argv[2] : NULL);
	AtObjectDelete((AtObject)engines);
	return cAtTrue;
	}

eBool CmdDebugSdhLineDccHdlcPrbs(char argc, char **argv)
	{
	AtList engines = PrbsEnginesGet(argv[0]);

	AtUnused(argc);
	CliAtPrbsEngineDebug(engines);
	AtObjectDelete((AtObject)engines);
	return cAtTrue;
	}

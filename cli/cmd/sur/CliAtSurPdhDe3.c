/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Sur-Pdh-De3
 *
 * File        : CliAtSurPdhDe3.c
 *
 * Created Date: Mar 22, 2015
 *
 * Description :N/A
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtPdhDe3.h"
#include "CliAtModuleSur.h"
#include "CliAtSurEngine.h"

/*--------------------------- Define -----------------------------------------*/
static const uint32 cAtSurEnginePdhDe3PmParamVal[] =
    {
     cAtSurEnginePdhDe3PmParamCvL,
     cAtSurEnginePdhDe3PmParamEsL,
     cAtSurEnginePdhDe3PmParamSesL,
     cAtSurEnginePdhDe3PmParamLossL,
     cAtSurEnginePdhDe3PmParamCvpP,
     cAtSurEnginePdhDe3PmParamCvcpP,
     cAtSurEnginePdhDe3PmParamEspP,
     cAtSurEnginePdhDe3PmParamEscpP,
     cAtSurEnginePdhDe3PmParamSespP,
     cAtSurEnginePdhDe3PmParamSescpP,
     cAtSurEnginePdhDe3PmParamSasP,
     cAtSurEnginePdhDe3PmParamAissP,
     cAtSurEnginePdhDe3PmParamUaspP,
     cAtSurEnginePdhDe3PmParamUascpP,
     cAtSurEnginePdhDe3PmParamCvcpPfe,
     cAtSurEnginePdhDe3PmParamEscpPfe,
     cAtSurEnginePdhDe3PmParamEsbcpPfe,
     cAtSurEnginePdhDe3PmParamSescpPfe,
     cAtSurEnginePdhDe3PmParamSascpPfe,
     cAtSurEnginePdhDe3PmParamUascpPfe,
     cAtSurEnginePdhDe3PmParamEsaL,
     cAtSurEnginePdhDe3PmParamEsbL,
     cAtSurEnginePdhDe3PmParamEsapP,
     cAtSurEnginePdhDe3PmParamEsacpP,
     cAtSurEnginePdhDe3PmParamEsbpP,
     cAtSurEnginePdhDe3PmParamEsbcpP,
     cAtSurEnginePdhDe3PmParamFcP,
     cAtSurEnginePdhDe3PmParamEsacpPfe,
     cAtSurEnginePdhDe3PmParamFccpPfe
    };

static const char *cAtSurEnginePdhDe3PmParamUpperStr[] =
    {
     "CV-L",
     "ES-L",
     "SES-L",
     "LOSS-L",
     "CVP-P",
     "CVCP-P",
     "ESP-P",
     "ESCP-P",
     "SESP-P",
     "SESCP-P",
     "SAS-P",
     "AISS-P",
     "UASP-P",
     "UASCP-P",
     "CVCP-PFE",
     "ESCP-PFE",
     "ESBCP-PFE",
     "SESCP-PFE",
     "SASCP-PFE",
     "UASCP-PFE",
     "ESA-L",
     "ESB-L",
     "ESAP-P",
     "ESACP-P",
     "ESBP-P",
     "ESBCP-P",
     "FC-P",
     "ESACP-PFE",
     "FCCP-PFE"
    };

static const char *cAtSurEnginePdhDe3PmParamStr[] =
    {
     "cvl",
     "esl",
     "sesl",
     "lossl",
     "cvpp",
     "cvcpp",
     "espp",
     "escpp",
     "sespp",
     "sescpp",
     "sasp",
     "aissp",
     "uaspp",
     "uascpp",
     "cvcppfe",
     "escppfe",
     "esbcppfe",
     "sescppfe",
     "sascppfe",
     "uascppfe",
     "esal",
     "esbl",
     "esapp",
     "esacpp",
     "esbpp",
     "esbcpp",
     "fcp",
     "esacppfe",
     "fccppfe"
    };

static const uint32 cAtPdhDe3AlarmTypeVal[] =
    {
    cAtPdhDe3AlarmLos,
    cAtPdhDe3AlarmLof,
    cAtPdhDe3AlarmAis,
    cAtPdhDe3AlarmRai
    };

static const char *cAtPdhDe3AlarmTypeStr[] =
    {
    "los", "lof", "ais", "rai"
    };

static const char *cAtPdhDe3AlarmTypeUpperStr[] =
    {
    "LOS", "LOF", "AIS", "RAI"
    };

/*--------------------------- Macros -----------------------------------------*/
#define mParamCounter(paramName)    return (counters)->paramName;

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/


/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool Enable(char argc, char **argv, eBool enable, CliSurEnableFunc enableFunc)
    {
    return CliAtSurEngineEnable(argc, argv, enable, CliDe3ListFromString, enableFunc);
    }

static int32 De3ParamCounterGet(void* allCounters, uint32 paramType)
    {
    tAtSurEnginePdhDe3PmCounters *counters = (tAtSurEnginePdhDe3PmCounters *)allCounters;
    switch (paramType)
        {
        case cAtSurEnginePdhDe3PmParamCvL:
            mParamCounter(cvL)
        case cAtSurEnginePdhDe3PmParamEsL:
            mParamCounter(esL)
        case cAtSurEnginePdhDe3PmParamSesL:
            mParamCounter(sesL)
        case cAtSurEnginePdhDe3PmParamLossL:
            mParamCounter(lossL)
        case cAtSurEnginePdhDe3PmParamCvpP:
            mParamCounter(cvpP)
        case cAtSurEnginePdhDe3PmParamCvcpP:
            mParamCounter(cvcpP)
        case cAtSurEnginePdhDe3PmParamEspP:
            mParamCounter(espP)
        case cAtSurEnginePdhDe3PmParamEscpP:
            mParamCounter(escpP)
        case cAtSurEnginePdhDe3PmParamSespP:
            mParamCounter(sespP)
        case cAtSurEnginePdhDe3PmParamSescpP:
            mParamCounter(sescpP)
        case cAtSurEnginePdhDe3PmParamSasP:
            mParamCounter(sasP)
        case cAtSurEnginePdhDe3PmParamAissP:
            mParamCounter(aissP)
        case cAtSurEnginePdhDe3PmParamUaspP:
            mParamCounter(uaspP)
        case cAtSurEnginePdhDe3PmParamUascpP:
            mParamCounter(uascpP)
        case cAtSurEnginePdhDe3PmParamCvcpPfe:
            mParamCounter(cvcpPfe)
        case cAtSurEnginePdhDe3PmParamEscpPfe:
            mParamCounter(escpPfe)
        case cAtSurEnginePdhDe3PmParamSescpPfe:
            mParamCounter(sescpPfe)
        case cAtSurEnginePdhDe3PmParamSascpPfe:
            mParamCounter(sascpPfe)
        case cAtSurEnginePdhDe3PmParamUascpPfe:
            mParamCounter(uascpPfe)
        case cAtSurEnginePdhDe3PmParamEsbcpPfe:
            mParamCounter(esbcpPfe)
        case cAtSurEnginePdhDe3PmParamEsaL:
            mParamCounter(esaL)
        case cAtSurEnginePdhDe3PmParamEsbL:
            mParamCounter(esbL)
        case cAtSurEnginePdhDe3PmParamEsapP:
            mParamCounter(esapP)
        case cAtSurEnginePdhDe3PmParamEsacpP:
            mParamCounter(esacpP)
        case cAtSurEnginePdhDe3PmParamEsbpP:
            mParamCounter(esbpP)
        case cAtSurEnginePdhDe3PmParamEsbcpP:
            mParamCounter(esbcpP)
        case cAtSurEnginePdhDe3PmParamFcP:
            mParamCounter(fcP)
        case cAtSurEnginePdhDe3PmParamEsacpPfe:
            mParamCounter(esacpPfe)
        case cAtSurEnginePdhDe3PmParamFccpPfe :
            mParamCounter(fccpPfe)
        default:
            return 0;
        }
    }

static eBool RegistersShow(char argc, char **argv, eAtPmRegisterType regType)
    {
    tAtSurEnginePdhDe3PmCounters counters;
    return CliAtSurEnginePmParamRegistersShow(argc, argv,
                                              cAtSurEnginePdhDe3PmParamUpperStr,
                                              cAtSurEnginePdhDe3PmParamVal,
                                              mCount(cAtSurEnginePdhDe3PmParamVal),
                                              (void *)&counters,
                                              regType,
                                              De3ParamCounterGet,
                                              CliDe3ListFromString);
    }

static void FailureNotify(AtSurEngine surEngine,
                          void *listener,
                          uint32 failureType,
                          uint32 currentStatus)
    {
    CliAtSurEngineFailureNotify(surEngine,
                                listener,
                                failureType,
                                currentStatus,
                                cAtPdhDe3AlarmTypeUpperStr,
                                cAtPdhDe3AlarmTypeVal,
                                mCount(cAtPdhDe3AlarmTypeVal));
    }

static void TcaNotify(AtSurEngine surEngine,
                      void *listener,
                      AtPmParam param,
                      AtPmRegister pmRegister)
    {
    CliAtSurEngineTcaNotify(surEngine,
                            listener,
                            param,
                            pmRegister,
                            cAtSurEnginePdhDe3PmParamUpperStr,
                            cAtSurEnginePdhDe3PmParamVal,
                            mCount(cAtSurEnginePdhDe3PmParamVal));
    }

static void AutotestFailureChangeState(AtSurEngine engine, void *userData, uint32 changedAlarms, uint32 currentStatus)
    {
    AtChannelObserver observer = userData;
    AtUnused(engine);
    AtChannelObserverFailuresUpdate(observer, changedAlarms, currentStatus);
    }

static void AutotestTcaChangeState(AtSurEngine engine, void *userData, AtPmParam param, AtPmRegister pmRegister)
    {
    AtChannelObserver observer = userData;
    uint32 changedParams = AtPmParamTypeGet(param);
    AtUnused(engine);
    AtUnused(pmRegister);
    AtChannelObserverTcaUpdate(observer, changedParams);
    }

static tAtSurEngineListener *Listener(void)
    {
    static tAtSurEngineListener listener;
    static tAtSurEngineListener *pListener = NULL;

    if (pListener)
        return pListener;

    AtOsalMemInit(&listener, 0, sizeof(listener));
    if (AutotestIsEnabled())
        {
        listener.FailureNotify = AutotestFailureChangeState;
        listener.TcaNotify     = AutotestTcaChangeState;
        }
    else
        {
        listener.FailureNotify = FailureNotify;
        listener.TcaNotify     = TcaNotify;
        }

    pListener = &listener;
    return pListener;
    }

eBool CmdPdhDe3SurInit(char argc, char **argv)
    {
    return CliAtSurEngineInit(argc, argv, CliDe3ListFromString);
    }

eBool CmdPdhDe3SurEnable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue, AtSurEngineEnable);
    }

eBool CmdPdhDe3SurDisable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse, AtSurEngineEnable);
    }

eBool CmdPdhDe3SurPmEnable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue, AtSurEnginePmEnable);
    }

eBool CmdPdhDe3SurPmDisable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse, AtSurEnginePmEnable);
    }

eBool CmdPdhDe3NotificationEnable(char argc, char **argv)
    {
    return CliAtSurEngineNotificationEnable(argc, argv, cAtTrue, Listener(), CliDe3ListFromString,
                                            CliAtPdhDe3ObserverGet, CliAtPdhDe3ObserverDelete);
    }

eBool CmdPdhDe3NotificationDisable(char argc, char **argv)
    {
    return CliAtSurEngineNotificationEnable(argc, argv, cAtFalse, Listener(), CliDe3ListFromString,
                                            CliAtPdhDe3Observer, CliAtPdhDe3ObserverDelete);
    }

eBool CmdPdhDe3FailureNotificationMaskSet(char argc, char **argv)
    {
    return CliAtSurEngineNotificationMaskSet(argc,
                                             argv,
                                             cAtPdhDe3AlarmTypeStr,
                                             cAtPdhDe3AlarmTypeVal,
                                             mCount(cAtPdhDe3AlarmTypeVal),
                                             AtSurEngineFailureNotificationMaskSet,
                                             CliDe3ListFromString);
    }

eBool CmdPdhDe3SurEngineShow(char argc, char **argv)
    {
    return CliAtSurEngineShow(argc, argv, CliDe3ListFromString);
    }

eBool CmdPdhDe3FailureNotificationMaskShow(char argc, char **argv)
    {
    return CliAtSurEngingFailureNotificationMaskShow(argc, argv,
                                                     cAtPdhDe3AlarmTypeUpperStr,
                                                     cAtPdhDe3AlarmTypeVal,
                                                     mCount(cAtPdhDe3AlarmTypeVal),
                                                     CliDe3ListFromString);
    }

eBool CmdPdhDe3FailureShow(char argc, char **argv)
    {
    return CliAtSurEngineFailuresShow(argc, argv,
                                      cAtPdhDe3AlarmTypeUpperStr,
                                      cAtPdhDe3AlarmTypeVal,
                                      mCount(cAtPdhDe3AlarmTypeVal),
                                      CliDe3ListFromString);
    }

eBool CmdPdhDe3FailureHistoryShow(char argc, char **argv)
    {
    return CliAtSurEngineFailuresHistoryShow(argc, argv,
                                             cAtPdhDe3AlarmTypeUpperStr,
                                             cAtPdhDe3AlarmTypeVal,
                                             mCount(cAtPdhDe3AlarmTypeVal),
                                             CliDe3ListFromString);
    }

eBool CmdPdhDe3FailureCaptureShow(char argc, char **argv)
    {
    return CliAtSurEngineFailuresCaptureShow(argc, argv,
                                             cAtPdhDe3AlarmTypeUpperStr,
                                             cAtPdhDe3AlarmTypeVal,
                                             mCount(cAtPdhDe3AlarmTypeVal),
                                             CliDe3ListFromString,
                                             CliAtPdhDe3ObserverGet,
                                             CliAtPdhDe3ObserverDelete);
    }

eBool CmdPdhDe3PmParamCurrentPeriodRegisterShow(char argc, char **argv)
    {
    return RegistersShow(argc, argv, cAtPmCurrentPeriodRegister);
    }

eBool CmdPdhDe3PmParamCurrentDayRegisterShow(char argc, char **argv)
    {
    return RegistersShow(argc, argv, cAtPmCurrentDayRegister);
    }

eBool CmdPdhDe3PmParamPreviousPeriodRegisterShow(char argc, char **argv)
    {
    return RegistersShow(argc, argv, cAtPmPreviousPeriodRegister);
    }

eBool CmdPdhDe3PmParamPreviousDayRegisterShow(char argc, char **argv)
    {
    return RegistersShow(argc, argv, cAtPmPreviousDayRegister);
    }

eBool CmdPdhDe3PmParamRecentPeriodRegisterShow(char argc, char **argv)
    {
    return CliAtSurEnginePmParamRecentPeriodsShow(argc, argv,
                                                  cAtSurEnginePdhDe3PmParamUpperStr,
                                                  cAtSurEnginePdhDe3PmParamVal,
                                                  mCount(cAtSurEnginePdhDe3PmParamVal),
                                                  CliDe3ListFromString);
    }

static uint32 CliSurPdhDe3PmParamFromString(char *pStr)
    {
    uint32 paramType;
    eBool convertSuccess = cAtTrue;
    mAtStrToEnum(cAtSurEnginePdhDe3PmParamStr, cAtSurEnginePdhDe3PmParamVal,  pStr,  paramType, convertSuccess);
    if(!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Unknown type %s, expect: ", pStr);
        CliExpectedValuesPrint(cSevCritical, cAtSurEnginePdhDe3PmParamStr, mCount(cAtSurEnginePdhDe3PmParamVal));
        AtPrintc(cSevCritical, "\r\n");
        return 0;
        }

    return paramType;
    }

eBool CmdSurThresholProfileDe3Set(char argc, char **argv)
    {
    return CliSurThresholdProfileParamValueSet(argc, argv,
                                               CliSurPdhDe3PmParamFromString,
                                               (TcaThresholdFuncSet)AtPmThresholdProfilePdhDe3PeriodTcaThresholdSet);
    }

eBool CmdSurThresholProfileDe3Show(char argc, char **argv)
    {
    return CliSurThresholProfileShow(argc,
                                     argv,
                                     cAtSurEnginePdhDe3PmParamUpperStr,
                                     cAtSurEnginePdhDe3PmParamVal,
                                     mCount(cAtSurEnginePdhDe3PmParamVal),
                                     (TcaThresholdFuncGet)AtPmThresholdProfilePdhDe3PeriodTcaThresholdGet);
    }

eBool CmdPdhDe3SurveillanceThresoldProfileSet(char argc, char **argv)
    {
    return CliAtSurEngineThresholdProfileSet(argc, argv, CliDe3ListFromString);
    }

eBool CmdPdhDe3SurveillanceThresoldProfileShow(char argc, char **argv)
    {
    return CliAtSurEngineThresholdProfileShow(argc, argv, CliDe3ListFromString);
    }

eBool CmdPdhDe3TcaNotificationMaskSet(char argc, char **argv)
    {
    return CliAtSurEngineNotificationMaskSet(argc, argv,
                                             cAtSurEnginePdhDe3PmParamStr,
                                             cAtSurEnginePdhDe3PmParamVal,
                                             mCount(cAtSurEnginePdhDe3PmParamVal),
                                             AtSurEngineTcaNotificationMaskSet,
                                             CliDe3ListFromString);
    }

eBool CmdPdhDe3TcaNotificationMaskShow(char argc, char **argv)
    {
    return CliAtSurEngingTcaNotificationMaskShow(argc, argv,
                                                 cAtSurEnginePdhDe3PmParamUpperStr,
                                                 cAtSurEnginePdhDe3PmParamVal,
                                                 mCount(cAtSurEnginePdhDe3PmParamVal),
                                                 CliDe3ListFromString);
    }

eBool CmdPdhDe3TcaHistoryShow(char argc, char **argv)
    {
    return CliAtSurEngineTcaHistoryShow(argc, argv,
                                        cAtSurEnginePdhDe3PmParamUpperStr,
                                        cAtSurEnginePdhDe3PmParamVal,
                                        mCount(cAtSurEnginePdhDe3PmParamVal),
                                        CliDe3ListFromString);
    }

eBool CmdPdhDe3TcaCaptureShow(char argc, char **argv)
    {
    return CliAtSurEngineTcaCaptureShow(argc, argv,
                                        cAtSurEnginePdhDe3PmParamUpperStr,
                                        cAtSurEnginePdhDe3PmParamVal,
                                        mCount(cAtSurEnginePdhDe3PmParamVal),
                                        CliDe3ListFromString,
                                        CliAtPdhDe3ObserverGet,
                                        CliAtPdhDe3ObserverDelete);
    }

eBool CmdPdhDe3PmParamPeriodThresholdShow(char argc, char **argv)
    {
    return CliAtSurEngineParamPeriodThresholdShow(argc, argv,
                                      cAtSurEnginePdhDe3PmParamUpperStr,
                                      cAtSurEnginePdhDe3PmParamVal,
                                      mCount(cAtSurEnginePdhDe3PmParamVal),
                                      CliDe3ListFromString);
    }

eBool CmdPdhDe3PmParamDayThresholdShow(char argc, char **argv)
    {
    return CliAtSurEngineParamDayThresholdShow(argc, argv,
                                      cAtSurEnginePdhDe3PmParamUpperStr,
                                      cAtSurEnginePdhDe3PmParamVal,
                                      mCount(cAtSurEnginePdhDe3PmParamVal),
                                      CliDe3ListFromString);
    }

# @group CliAtSurPdhDe3 "PDH DS3/E3 Surveillance Monitoring"
# @ingroup CliAtModuleSur "Surveillance Module"

4 pdh de3 surveillance init CmdPdhDe3SurInit 1 de3List=1
/*
    Syntax     : pdh de3 surveillance init <de3List>
    Parameter  : de3List - List of PDH DS3s/E3s
    Description: Initialize surveillance engine  
    Example    : * For DS3/E3 LIUs:
                   pdh de3 surveillance init 1-4 
                 * For DS3/E3 mapped to VC3:
                   pdh de3 surveillance init 1.1.1-1.1.3 
*/ 

4 pdh de3 surveillance enable CmdPdhDe3SurEnable 1 de3List=1
/*
    Syntax     : pdh de3 surveillance enable <de3List>
    Parameter  : de3List     - List of PDH DS3s/E3s
    Description: Enable surveillance engine 
    Example    : * For DS3/E3 LIUs:
                   pdh de3 surveillance enable 1-4 
                 * For DS3/E3 mapped to VC3:
                   pdh de3 surveillance enable 1.1.1-1.1.3 
*/ 

4 pdh de3 surveillance disable CmdPdhDe3SurDisable 1 de3List=1
/*
    Syntax     : pdh de3 surveillance disable <de3List>
    Parameter  : de3List     - List of PDH DS3s/E3s
    Description: Disable surveillance engine 
    Example    : * For DS3/E3 LIUs:
                   pdh de3 surveillance disable 1-4 
                 * For DS3/E3 mapped to VC3:
                   pdh de3 surveillance disable 1.1.1-1.1.3 
*/

5 pdh de3 surveillance pm enable CmdPdhDe3SurPmEnable 1 de3List=1
/*
    Syntax     : pdh de3 surveillance pm enable <de3List>
    Parameter  : de3List     - List of PDH DS3s/E3s
    Description: Enable PM monitoring of DS3/E3 surveillance engine.
    Example    : * For DS3/E3 LIUs:
                   pdh de3 surveillance pm enable 1-4 
                 * For DS3/E3 mapped to VC3:
                   pdh de3 surveillance pm enable 1.1.1-1.1.3 
*/ 

5 pdh de3 surveillance pm disable CmdPdhDe3SurPmDisable 1 de3List=1
/*
    Syntax     : pdh de3 surveillance pm disable <de3List>
    Parameter  : de3List     - List of PDH DS3s/E3s
    Description: Disable PM monitoring of DS3/E3 surveillance engine.
    Example    : * For DS3/E3 LIUs:
                   pdh de3 surveillance pm disable 1-4 
                 * For DS3/E3 mapped to VC3:
                   pdh de3 surveillance pm disable 1.1.1-1.1.3 
*/

5 pdh de3 surveillance notification enable CmdPdhDe3NotificationEnable 1 de3List=1
/*
    Syntax     : pdh de3 surveillance notification enable <de3List> 
    Parameter  : de3List - List of PDH DS3s/E3s                                  
    Description: Listen on Failures and TCA
    Example    : * For DS3/E3 LIUs:
                   pdh de3 surveillance notification enable 1-4 
                 * For DS3/E3 mapped to VC3:
                   pdh de3 surveillance notification enable 1.1.1-1.1.3 
*/

5 pdh de3 surveillance notification disable CmdPdhDe3NotificationDisable 1 de3List=1
/*
    Syntax     : pdh de3 surveillance notification disable <de3List> 
    Parameter  : de3List     - List of PDH DS3s/E3s          
                               + For DS3/E3 LIU: 1..N where N is maximum number of DS3s/E3s
                               + For DS3/E3 mapped to SDH VC, its ID is the same as VC-1x ID.
                               The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>                        
    Description: Stop listening on Failures and TCA 
    Example    : * For DS3/E3 LIUs:
                   pdh de3 surveillance notification disable 1-4 
                 * For DS3/E3 mapped to VC3:
                   pdh de3 surveillance notification disable 1.1.1-1.1.3 
*/

4 pdh de3 failure notificationmask CmdPdhDe3FailureNotificationMaskSet 3 de3List=1 failure=ais enable=en
/*
    Syntax     : pdh de3 failure notificationmask <de3List> <failures> <en/dis>
    Parameter  : de3List   - List of PDH DS3s/E3s
                 failures  - ORed of failures separated by '|'.
                             + los: LOS
                             + lof: LOF
                             + ais: AIS
                             + rai: RAI
                 enable    - Enabling
                             + en: Enable
                             + dis: Disable
    Description: Set failure notification mask
    Example    : * For DS3/E3 LIUs:
                   pdh de3 failure notificationmask 1 ais|lof en                   
                 * For DS3/E3 mapped to VC3:
                   pdh de3 failure notificationmask 1.1.1-1.1.3 ais|lof dis                   
*/

4 show pdh de3 surveillance CmdPdhDe3SurEngineShow 1 de3List=1  
/*
    Syntax     : show pdh de3 surveillance <de3List>
    Parameter  : de3List     - List of PDH DS3s/E3s
    Description: Show configuration information of surveillance engine
    Example    : * For DS3/E3 LIUs:
                   show pdh de3 surveillance 1
                 * For DS3/E3 mapped to VC3:
                   show pdh de3 surveillance 1.1.1-1.1.3
*/

5 show pdh de3 failure notificationmask CmdPdhDe3FailureNotificationMaskShow 1 de3List=1 
/*
    Syntax     : show pdh de3 failure notificationmask <de3List> 
    Parameter  : de3List      - List of PDH DS3s/E3s
    Description: Show failure notification mask
    Example    : * For DS3/E3 LIUs:
                   show pdh de3 failure notificationmask 1
                 * For DS3/E3 mapped to VC3:
                   show pdh de3 failure notificationmask 1.1.1-1.1.3 
*/

5 show pdh de3 failure capture CmdPdhDe3FailureCaptureShow 2 de3List=1 postAction=ro
/*
    Syntax     : show pdh de3 failure capture <de3List> <ro/r2c> [silent]
    Parameter  : de3List  - Input DS3/E3 identifier. Format of one ID item is as following
                            + For DS3/E3 LIU: 1..N where N is maximum number of DS3s/E3s
                 postAction - [ro, r2c] read only / read to clear
                            + ro: do not flush list of captured alarms after displaying
                            + r2c: flush list of captured alarms after displaying
                silent      - If "silent" is specified, there would be no
                              output with "r2c" mode
    Description: Show captured failures
    Example    : show pdh de3 failure capture 1 ro
                 show pdh de3 failure capture 1 r2c
                 show pdh de3 failure capture 1 r2c silent
*/

4 show pdh de3 failure CmdPdhDe3FailureShow 1 de3List=1
/*
    Syntax     : show pdh de3 failure <de3List>
    Parameter  : de3List     - List of PDH DS3s/E3s
    Description: Show current failure status
    Example    : * For DS3/E3 LIUs:
                   show pdh de3 failure 1-16
                 * For DS3/E3 mapped to VC3:                 
                   show pdh de3 failure 1.1.1-1.1.3
*/

5 show pdh de3 failure history CmdPdhDe3FailureHistoryShow -1 de3List readingMode
/*
    Syntax     : show pdh de3 failure history  <de3List> <ro/r2c> [silent]
    Parameter  : de3List     - List of PDH DS3s/E3s
                 readingMode - [ro, r2c] Reading mode
                               + ro : Read-only
                               + r2c: Read-to-clear
                 silent      - If "silent" is specified, there would be no 
                               output with "r2c" mode
    Description: Show failure history
    Example    : * For DS3/E3 LIUs:
                   show pdh de3 failure history 1-16 r2c
                   show pdh de3 failure history 1-16 r2c silent
                 * For DS3/E3 mapped to VC3:
                   show pdh de3 failure history 1.1.1-1.1.3 ro
*/

6 show pdh de3 pm current period CmdPdhDe3PmParamCurrentPeriodRegisterShow -1 de3List=1 readingMode
/*
    Syntax     : show pdh de3 pm current period <de3List> <ro/r2c> [silent]
    Parameter  : de3List     - List of PDH DS3s/E3s
                 readingMode - [ro, r2c] Reading mode
                               + ro : Read-only
                               + r2c: Read-to-clear
                 silent      - If "silent" is specified, there would be no 
                               output with "r2c" mode
    Description: Show current period
    Example    : * For DS3/E3 LIUs:
                   show pdh de3 pm current period 1-16 r2c
                   show pdh de3 pm current period 1-16 r2c silent
                 * For DS3/E3 mapped to VC3:
                   show pdh de3 pm current period 1.1.1-1.1.3 ro
*/

6 show pdh de3 pm current day CmdPdhDe3PmParamCurrentDayRegisterShow -1 de3List=1 readingMode
/*
    Syntax     : show pdh de3 pm current day  <de3List> <ro/r2c> [silent]
    Parameter  : de3List     - List of PDH DS3s/E3s
                 readingMode - [ro, r2c] Reading mode
                               + ro : Read-only
                               + r2c: Read-to-clear
                 silent      - If "silent" is specified, there would be no 
                               output with "r2c" mode
    Description: Show current day
    Example    : * For DS3/E3 LIUs:
                   show pdh de3 pm current day 1-16 r2c
                   show pdh de3 pm current day 1-16 r2c silent
                 * For DS3/E3 mapped to VC3:
                   show pdh de3 pm current day 1.1.1-1.1.3 r2c
                   show pdh de3 pm current day 1.1.1-1.1.3 r2c silent
*/

6 show pdh de3 pm previous period CmdPdhDe3PmParamPreviousPeriodRegisterShow -1 de3List=1 readingMode
/*
    Syntax     : show pdh de3 pm previous period  <de3List> <ro/r2c> [silent]
    Parameter  : de3List     - List of PDH DS3s/E3s
                 readingMode - [ro, r2c] Reading mode
                               + ro : Read-only
                               + r2c: Read-to-clear
                 silent      - If "silent" is specified, there would be no 
                               output with "r2c" mode
    Description: Show previous period
    Example    : * For DS3/E3 LIUs:
                   show pdh de3 pm previous period 1-16 r2c
                   show pdh de3 pm previous period 1-16 r2c silent
                 * For DS3/E3 mapped to VC3:
                   show pdh de3 pm previous period 1.1.1-1.1.3 r2c
                   show pdh de3 pm previous period 1.1.1-1.1.3 r2c silent
*/

6 show pdh de3 pm previous day CmdPdhDe3PmParamPreviousDayRegisterShow -1 de3List=1 readingMode
/*
    Syntax     : show pdh de3 pm previous day <de3List> <ro/r2c> [silent]
    Parameter  : de3List - List of PDH DS3s/E3s
                 readingMode - [ro, r2c] Reading mode
                               + ro : Read-only
                               + r2c: Read-to-clear
                 silent      - If "silent" is specified, there would be no 
                               output with "r2c" mode
    Description: Show previous day
    Example    : * For DS3/E3 LIUs:
                   show pdh de3 pm previous day 1-16 r2c
                   show pdh de3 pm previous day 1-16 r2c silent
                 * For DS3/E3 mapped to VC3:
                   show pdh de3 pm previous day 1.1.1-1.1.3 r2c
                   show pdh de3 pm previous day 1.1.1-1.1.3 r2c silent
*/

6 show pdh de3 pm recent period CmdPdhDe3PmParamRecentPeriodRegisterShow -1 de3List=1 recentPeriod=1 readingMode
/*
    Syntax     : show pdh de3 pm recent period <de3List> <recentPeriods>readingMode <ro/r2c> [silent]
    Parameter  : de3List      - List of PDH DS3s/E3s
                 recentPeriod - Recent period
                 readingMode  - Reading mode
                                + r2c: Read to Clear
                                + ro: Read only
                 silent      - If "silent" is specified, there would be no 
                               output with "r2c" mode
    Description: Show recent periods
    Example    : * For DS3/E3 LIUs:
                   show pdh de3 pm recent period 1-16 1 r2c
                   show pdh de3 pm recent period 1-16 1 r2c silent
                 * For DS3/E3 mapped to VC3:
                   show pdh de3 pm recent period 1.1.1-1.1.3 1 r2c
                   show pdh de3 pm recent period 1.1.1-1.1.3 1 r2c silent
*/

5 pdh de3 surveillance threshold profile CmdPdhDe3SurveillanceThresoldProfileSet 2 de3List=1 profileId
/*
    Syntax     : pdh de3 surveillance threshold profile <de3List> <profileId> 
    Parameter  : de3List      - List of PDH DS3s/E3s
                 profileId    - profile Id                    
    Description: Set Threshold Profile
    Example    : pdh de3 surveillance threshold profile 1-4 2
*/

6 show pdh de3 surveillance threshold profile CmdPdhDe3SurveillanceThresoldProfileShow 1 de3List=1
/*
    Syntax     : show pdh de3 surveillance threshold profile <de3List>
    Parameter  : de3List      - List of PDH DS3s/E3s        
    Description: Show Threshold Profile
    Example    : show pdh de3 surveillance threshold profile 1-4
*/

4 pdh de3 pm notificationmask CmdPdhDe3TcaNotificationMaskSet 3 de3List=1 parameters enable=en
/*
    Syntax     : pdh de3 pm notificationmask <de3List> <parameters> <en/dis>
    Parameter  : de3List    - List of PDH DS3/E3
                 parameters - ORed of parameters separated by '|'.
                              + cvl      : CV-L
                              + esl      : ES-L
                              + sesl     : SES-L
                              + lossl    : LOSS-L
                              + cvpp     : CVP-P
                              + cvcpp    : CVCP-P
                              + espp     : ESP-P
                              + escpp    : ESCP-P
                              + sespp    : SESP-P
                              + sescpp   : SESCP-P
                              + sasp     : SAS-P
                              + aissp    : AISS-P
                              + uaspp    : UASP-P
                              + uascpp   : UASCP-P
                              + cvcppfe  : CVCP-PFE
                              + escppfe  : ESCP-PFE
                              + esbcppfe : ESBCP-PFE
                              + sescppfe : SESCP-PFE
                              + sascppfe : SASCP-PFE
                              + uascppfe : UASCP-PFE
                              + esal     : ESA-L
                              + esbl     : ESB-L
                              + esapp    : ESAP-P
                              + esacpp   : ESACP-P
                              + esbpp    : ESBP-P
                              + esbcpp   : ESBCP-P
                              + fcp      : FC-P
                              + esacppfe : ESACP-PFE
                              + fccppfe  : FCCP-PFE
                 enable    - [en, dis] Enabling
    Description: Set pm notification mask
    Example    : * For DS3/E3 LIUs:
                   pdh de3 pm notificationmask 1 cvl|esl en                   
                 * For DS3/E3 mapped to VC3:
                   pdh de3 pm notificationmask 1.1.1-1.1.3 cvl|esl dis                         
*/

5 show pdh de3 pm notificationmask CmdPdhDe3TcaNotificationMaskShow 1 de3List=1 
/*
    Syntax     : show pdh de3 pm notificationmask <de3List> 
    Parameter  : de3List   - List of PDH DS3/E3
    Description: Show pm notification mask
    Example    : * For DS3/E3 LIUs:
                   show pdh de3 pm notificationmask 1
                 * For DS3/E3 mapped to VC3:
                   show pdh de3 pm notificationmask 1.1.1-1.1.3 
*/

5 show pdh de3 pm history CmdPdhDe3TcaHistoryShow -1 de3List=1 readingMode=ro
/*
    Syntax     : show pdh de3 pm history  <de3List> <ro/r2c> [<silent>]
    Parameter  : de3List     - List of PDH DS3/E3
                 readingMode - [ro, r2c] Reading mode
                               + ro : Read-only
                               + r2c: Read-to-clear
                 silent      - If "silent" is specified, there would be no 
                               output with "r2c" mode
    Description: Show pm history
    Example    : * For DS3/E3 LIUs:
                   show pdh de3 pm history 1-16 r2c
                   show pdh de3 pm history 1-16 r2c silent
                 * For DS3/E3 mapped to VC3:
                   show pdh de3 pm history 1.1.1-1.1.3 ro
*/

5 show pdh de3 pm capture CmdPdhDe3TcaCaptureShow 2 de3List=1 postAction=ro
/*
    Syntax     : show pdh de3 pm capture <de3List> <ro/r2c> [silent]
    Parameter  : de3List    - Input DS3/E3 identifier. Format of one ID item is as following
                            + For DS3/E3 LIU: 1..N where N is maximum number of DS3s/E3s
                 postAction - [ro, r2c] read only / read to clear
                            + ro: do not flush list of captured tca after displaying
                            + r2c: flush list of captured tca after displaying
                 silent     - If "silent" is specified, there would be no
                              output with "r2c" mode
    Description: Show captured PM TCA
    Example    : show pdh de3 pm capture 1 ro
                 show pdh de3 pm capture 1 r2c
                 show pdh de3 pm capture 1 r2c silent
*/

6 show pdh de3 pm threshold period CmdPdhDe3PmParamPeriodThresholdShow 1 de3List=1
/*
    Syntax     : show pdh de3 pm threshold period <de3List>
    Parameter  : de3List     - List of PDH DS3/E3
    Description: Show period threshold
    Example    : show pdh de3 pm threshold period 1-16
*/

6 show pdh de3 pm threshold day CmdPdhDe3PmParamDayThresholdShow 1 de3List=1
/*
    Syntax     : show pdh de3 pm threshold day <de3List>
    Parameter  : de3List     - List of PDH DS3/E3
    Description: Show day threshold
    Example    : show pdh de3 pm threshold day 1-16
*/

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : CliAtSurEngine.c
 *
 * Created Date: Sep 13, 2015
 *
 * Description : CLI common implementation of SUR engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtTokenizer.h"
#include "CliAtModuleSur.h"
#include "CliAtSurEngine.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumChannelsPerTable 10

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtChannel *ChannelsParse(char *idString, ChannelsIdParseFunc idParseFunc, uint32 *numChannels)
    {
    AtChannel* channels = CliSharedChannelListGet(numChannels);

    *numChannels = idParseFunc(idString, channels, *numChannels);
    if (*numChannels == 0)
        {
        AtPrintc(cSevWarning, "No engine\r\n");
        return NULL;
        }

    return channels;
    }

static eBool ParamThresholdShow(char argc, char **argv,
                                const char *paramTypeStr[],
                                const uint32 *paramTypeVal,
                                uint32 numParams,
                                eAtPmRegisterType regType,
                                ChannelsIdParseFunc idParseFunc)
    {
    uint32 channel_i;
    tTab *tabPtr = NULL;
    const char *pHeading[] = {"Param", "", "", "", "", "", "", "", "", "", ""};
    uint32 numChannels;
    AtChannel* channels;
    uint32 row_i, numRows = numParams;
    uint32 column = 0;
    uint32 (*ThresholdGet)(AtPmParam self) = NULL;
    
    AtUnused(argc);

    /* Determine what function to use */
    switch ((uint32)regType)
        {
        case cAtPmCurrentPeriodRegister :
        case cAtPmPreviousPeriodRegister:
            ThresholdGet = AtPmParamPeriodThresholdGet;
            break;

        case cAtPmCurrentDayRegister    :
        case cAtPmPreviousDayRegister   :
            ThresholdGet = AtPmParamDayThresholdGet;
            break;

        default:
            AtPrintc(cSevCritical, "ERROR: not support threshold type\r\n");
            return cAtFalse;
        }

    /* Have a list of channels */
    channels = ChannelsParse(argv[0], idParseFunc, &numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No channel to show. ID format may be wrong\r\n");
        return cAtTrue;
        }

    tabPtr = TableAlloc(numRows, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Put all of parameters as title */
    for (row_i = 0; row_i < numRows; row_i++)
        StrToCell(tabPtr, row_i, 0, paramTypeStr[row_i]);

    for (channel_i = 0; channel_i < numChannels; channel_i = AtCliNextIdWithSleep(channel_i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtSurEngine engine = AtChannelSurEngineGet(channels[channel_i]);

        column = (channel_i % cNumChannelsPerTable) + 1;

        /* Enough one table to print */
        if ((channel_i > 0) && ((channel_i % cNumChannelsPerTable) == 0))
            {
            TablePrint(tabPtr);
            AtPrintc(cSevInfo, "Continue...\r\n");
            }

        /* Channel ID */
        TableColumnHeaderSet(tabPtr, column, CliChannelIdStringGet(channels[channel_i]));

        /* Put all thresholds of all parameters */
        for (row_i = 0; row_i < numRows; row_i++)
            {
            AtPmParam param = AtSurEnginePmParamGet(engine, paramTypeVal[row_i]);
            uint32 threshold = ThresholdGet(param);

            StrToCell(tabPtr, row_i, column, CliNumber2String(threshold, "%u"));
            }
        }

    /* There is always one last table and it may contain less than number of
     * columns to display for one table */
    SubTablePrint(tabPtr, 0, numRows - 1, 0, column);
    TableFree(tabPtr);

    return cAtTrue;
    }

char **CliSurTableHeadingCreate(const char *fisrtTitle, const char *remainingTitleNames[], uint32 numTitles)
    {
    char **pHeading;
    uint32 i;

    /* Need memory to hold all of title types */
    pHeading = AtOsalMemAlloc(sizeof(char *) * (numTitles));
    if (pHeading == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Build table heading fail\r\n");
        return NULL;
        }

    /* Build title */
    if (fisrtTitle)
    	pHeading[0] = AtStrdup(fisrtTitle);
    else
    	pHeading[0] = AtStrdup("channel");

    for (i = 0; i < (numTitles - 1); i++)
        {
        uint32 j;

        /* Fill all remaining titles */
        pHeading[i + 1] = AtStrdup(remainingTitleNames[i]);
        if (pHeading[i + 1])
            continue;

        /* Need to clean up  */
        for (j = 0; j < i; j++)
            AtOsalMemFree(pHeading[j]);
        AtOsalMemFree(pHeading);
        return NULL;
        }

    return pHeading;
    }

void CliSurTableHeadingDelete(char **pHeading, uint32 numTitles)
    {
    uint32 i;

    for (i = 0; i < numTitles; i++)
        AtOsalMemFree(pHeading[i]);

    AtOsalMemFree(pHeading);
    }

static eBool FailuresMaskShow(char argc, char **argv,
                              const char *failureTypeStr[],
                              const uint32 *failureTypeVal,
                              uint32 numFailures,
                              FailureFuncGet failureGetFunc,
                              ChannelsIdParseFunc idParseFunc,
                              eBool silent)
    {
    uint32 channel_i, failure_i;
    tTab *tabPtr = NULL;
    char **pHeading;
    uint32 numChannels;
    AtChannel* channels = ChannelsParse(argv[0], idParseFunc, &numChannels);

    AtUnused(argc);

    pHeading = CliSurTableHeadingCreate(NULL, failureTypeStr, numFailures + 1);
    if (pHeading == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Build table heading fail\r\n");
        return cAtFalse;
        }

    if (!silent)
        {
        /* Create table with titles */
        tabPtr = TableAlloc(numChannels, numFailures + 1, AtCast(const char **, pHeading));
        if (tabPtr == NULL)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
            CliSurTableHeadingDelete(pHeading, numFailures + 1);
            return cAtFalse;
            }
        }

    /* Make table content */
    for (channel_i = 0; channel_i < numChannels; channel_i = AtCliNextIdWithSleep(channel_i, AtCliLimitNumRestTdmChannelGet()))
        {
        uint32 raised;
        uint32 failure;
        AtChannel channel = channels[channel_i];
        AtSurEngine engine = AtChannelSurEngineGet(channel);

        /* Put channel ID */
        StrToCell(tabPtr, channel_i, 0, (char *)CliChannelIdStringGet(channel));

        /* Show failure */
        failure = failureGetFunc(engine);
        for (failure_i = 0; failure_i < numFailures; failure_i++)
            {
            raised = (failure & failureTypeVal[failure_i]);
            ColorStrToCell(tabPtr, channel_i, failure_i + 1, raised ? "set" : "clear", raised ? cSevCritical : cSevInfo);
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    CliSurTableHeadingDelete(pHeading,  numFailures + 1);

    return cAtTrue;
    }

static eBool FailuresShow(char argc, char **argv,
                          const char *failureTypeStr[],
                          const uint32 *failureTypeVal,
                          uint32 numFailures,
                          FailureFuncGet failureGetFunc,
                          ChannelsIdParseFunc idParseFunc,
                          eBool silent)
    {
    return FailuresMaskShow(argc, argv, failureTypeStr, failureTypeVal, numFailures, failureGetFunc, idParseFunc, silent);
    }

static eBool FailuresCaptureShow(char argc, char **argv,
                                 const char *failureTypeStr[],
                                 const uint32 *failureTypeVal,
                                 uint32 numFailures,
                                 ChannelsIdParseFunc idParseFunc,
                                 AtChannelObserver (*ObserverGet)(AtChannel),
                                 void (*ObserverDelete)(AtChannel))
    {
    tTab *tabPtr = NULL;
    char **pHeading;
    uint32 numChannels, i;
    AtChannel* channels;
    eBool readMode;
    eBool silent = cAtFalse;

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    /* Get the shared buffer to hold all of objects, then call the parse function */
    channels = ChannelsParse(argv[0], idParseFunc, &numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevNormal, "No channel to filter\r\n");
        return cAtTrue;
        }

    /* Get reading action mode */
    readMode = CliHistoryReadingModeGet(argv[1]);
    if (readMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    if (argc > 2 )
        silent = CliHistoryReadingShouldSilent(readMode, argv[2]);

    if (!silent)
        {
        /* Create table with titles */
        pHeading = CliSurTableHeadingCreate(NULL, failureTypeStr, numFailures + 1);

        tabPtr = TableAlloc(numChannels, numFailures + 1, AtCast(const char **, pHeading));
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
            return cAtFalse;
            }
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtChannel channel = channels[i];
        AtChannelObserver observer = ObserverGet(channel);
        uint8 column = 0;
        uint32 current;
        uint32 history;
        uint8 j;

        StrToCell(tabPtr, i, column++, CliChannelIdStringGet(channel));

        current = AtChannelObserverFailureGet(observer);
        if (readMode == cAtHistoryReadingModeReadToClear)
            history = AtChannelObserverFailureHistoryClear(observer);
        else
            history = AtChannelObserverFailureHistoryGet(observer);

        for (j = 0; j < numFailures; j++)
            CliCapturedAlarmToCell(tabPtr, i, column++, history, current, failureTypeVal[j]);

        ObserverDelete(channel);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    CliSurTableHeadingDelete(pHeading, numFailures + 1);

    return cAtTrue;
    }

eBool CliAtSurEngineInit(char argc, char **argv, ChannelsIdParseFunc idParseFunc)
    {
    uint32 channel_i;
    eBool success = cAtTrue;
    uint32 numChannels;
    AtChannel* channels = ChannelsParse(argv[0], idParseFunc, &numChannels);

    AtUnused(argc);

    for (channel_i = 0; channel_i < numChannels; channel_i = AtCliNextIdWithSleep(channel_i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtChannel channel = channels[channel_i];
        AtSurEngine engine = AtChannelSurEngineGet(channel);
        eAtRet ret = AtSurEngineInit(engine);

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Initialize engine of channel %s fail, ret = %s\r\n",
                     CliChannelIdStringGet(channel),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CliAtSurEngineEnable(char argc, char **argv,
                           eBool enable,
                           ChannelsIdParseFunc idParseFunc,
                           CliSurEnableFunc enableFunc)
    {
    uint32 channel_i;
    eBool success = cAtTrue;
    uint32 numChannels;
    AtChannel* channels = ChannelsParse(argv[0], idParseFunc, &numChannels);

    AtUnused(argc);

    for (channel_i = 0; channel_i < numChannels; channel_i = AtCliNextIdWithSleep(channel_i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtChannel channel = channels[channel_i];
        AtSurEngine engine = AtChannelSurEngineGet(channel);
        eAtRet ret = enableFunc(engine, enable);

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: %s Surveillance engine fail on channel %s, ret = %s\r\n",
                     enable ? "Enable" : "Disable",
                     CliChannelIdStringGet(channel),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CliAtSurEngingFailureNotificationMaskShow(char argc, char **argv,
                                             const char *failureTypeStr[],
                                             const uint32 *failureTypeVal,
                                             uint32 numFailures,
                                             ChannelsIdParseFunc idParseFunc)
    {
    static const eBool silent = cAtFalse;
    return FailuresMaskShow(argc, argv,
                            failureTypeStr,
                            failureTypeVal,
                            numFailures,
                            AtSurEngineFailureNotificationMaskGet,
                            idParseFunc,
                            silent);
    }

eBool CliAtSurEngineShow(char argc, char **argv, ChannelsIdParseFunc idParseFunc)
    {
    uint32 channel_i;
    tTab *tabPtr;
    const char * heading[] = {"channel", "enabled", "PM enabled"};
    uint32 numChannels;
    AtChannel* channels = ChannelsParse(argv[0], idParseFunc, &numChannels);

    AtUnused(argc);

    /* Create table with titles */
    tabPtr = TableAlloc(numChannels, mCount(heading), (const char **) heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (channel_i = 0; channel_i < numChannels; channel_i = AtCliNextIdWithSleep(channel_i, AtCliLimitNumRestTdmChannelGet()))
        {
        eBool enabled;
        AtSurEngine engine = AtChannelSurEngineGet(channels[channel_i]);
        uint8 column = 0;

        StrToCell(tabPtr, channel_i, column++, (char *) CliChannelIdStringGet(channels[channel_i]));

        enabled = AtSurEngineIsEnabled(engine);
        ColorStrToCell(tabPtr, channel_i, column++, enabled ? "Enable" : "Disable", enabled ? cSevInfo : cSevCritical);

        enabled = AtSurEnginePmIsEnabled(engine);
        ColorStrToCell(tabPtr, channel_i, column++, enabled ? "Enable" : "Disable", enabled ? cSevInfo : cSevCritical);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CliAtSurEnginePmParamRegistersShow(char argc, char **argv,
                                         const char *paramTypeStr[],
                                         const uint32 *paramTypeVal,
                                         uint32 numParams,
                                         void* allPmCounters, /* Buffer from caller, please */
                                         eAtPmRegisterType regType,
                                         CliParamCounterParser counterParser,
                                         ChannelsIdParseFunc idParseFunc)
    {
    uint32 channel_i;
    tTab *tabPtr = NULL;
    const char *pHeading[] = {"Param", "", "", "", "", "", "", "", "", "", ""};
    uint32 numChannels;
    AtChannel* channels;
    uint32 row_i, numRows = numParams;
    uint32 column = 0;
    eAtHistoryReadingMode readingMode;
    eAtRet (*CounterGet)(AtSurEngine, eAtPmRegisterType, void*);
    tAtOsalCurTime curTime;
    tAtOsalCurTime startTime;
    static uint32 maxElapsedTime = 0;
    uint32 totalElapsedTime = 0;
    static char channelDesc[32];
    eBool silent = cAtFalse;

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    readingMode = CliHistoryReadingModeGet(argv[1]);

    /* Have a list of channels */
    channels = ChannelsParse(argv[0], idParseFunc, &numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No channel to show. ID format may be wrong\r\n");
        return cAtTrue;
        }

    /* Reading mode must be valid */
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Invalid reading action mode. Expected: %s\r\n",
                 CliValidHistoryReadingModes());
        return cAtFalse;
        }

    if (readingMode == cAtHistoryReadingModeReadToClear)
        CounterGet = AtSurEngineAllCountersClear;
    else
        CounterGet = AtSurEngineAllCountersGet;

    silent = CliHistoryReadingShouldSilent(readingMode, (argc > 2 ) ? argv[2] : NULL);
    if (!silent)
        {
        tabPtr = TableAlloc(numRows + 1, mCount(pHeading), pHeading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
            return cAtFalse;
            }
        }

    /* Put all of parameters as title */
    for (row_i = 0; row_i < numRows; row_i++)
        StrToCell(tabPtr, row_i, 0, paramTypeStr[row_i]);
    StrToCell(tabPtr, row_i, 0, "Elapsed (us)");

    for (channel_i = 0; channel_i < numChannels; channel_i = AtCliNextIdWithSleep(channel_i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtSurEngine engine = AtChannelSurEngineGet(channels[channel_i]);
        uint32 invalidFlags;
        eAtRet ret;
        uint32 elapsedTime;

        AtOsalCurTimeGet(&startTime);
        ret = CounterGet(engine, regType, allPmCounters);
        AtOsalCurTimeGet(&curTime);

        if (ret != cAtOk)
            {
            const char *errorString = AtRet2String(ret);
            for (row_i = 0; row_i < numRows; row_i++)
                ColorStrToCell(tabPtr, row_i, column, errorString, cSevCritical);
            break;
            }

        column = (channel_i % cNumChannelsPerTable) + 1;

        /* Enough one table to print */
        if ((channel_i > 0) && ((channel_i % cNumChannelsPerTable) == 0))
            {
            TablePrint(tabPtr);
            AtPrintc(cSevInfo, "Continue...\r\n");
            }

        /* Channel ID */
        TableColumnHeaderSet(tabPtr, column, CliChannelIdStringGet(channels[channel_i]));

        /* Put all counters */
        invalidFlags = AtSurEnginePmInvalidFlagsGet(engine, regType);
        for (row_i = 0; row_i < numRows; row_i++)
            {
            int32 regValue = counterParser(allPmCounters, paramTypeVal[row_i]);
            uint32 valid = ~invalidFlags & paramTypeVal[row_i];

            if (valid)
                ColorStrToCell(tabPtr, row_i, column, CliNumber2String(regValue, "%d"), regValue ? cSevCritical : cSevNormal);
            else
                ColorStrToCell(tabPtr, row_i, column, CliNumber2String(regValue, "%d (invalid)"), cSevCritical);
            }

        /* Elapsed time to get full counters. */
        elapsedTime = AtOsalDifferenceTimeInUs(&curTime, &startTime);
        StrToCell(tabPtr, row_i, column, CliNumber2String(elapsedTime, "%d"));

        if (elapsedTime > maxElapsedTime)
            {
            AtChannel channel = AtSurEngineChannelGet(engine);
            maxElapsedTime = elapsedTime;
            AtSprintf(channelDesc, "%s.%s", AtChannelTypeString(channel), AtChannelIdString(channel));
            }
        totalElapsedTime += elapsedTime;
        }

    /* There is always one last table and it may contain less than number of
     * columns to display for one table */
    SubTablePrint(tabPtr, 0, numRows, 0, column);
    TableFree(tabPtr);

    AtPrintc(cSevInfo, "\r\nNote: Totally %d channels", numChannels);
    AtPrintc(cSevInfo, "\r\n      Total time %d (us)", totalElapsedTime);
    AtPrintc(cSevInfo, "\r\n      Average time %d (us)", totalElapsedTime / numChannels);
    AtPrintc(cSevInfo, "\r\n      Maximum time %d (us) at channel %s\r\n", maxElapsedTime, channelDesc);

    return cAtTrue;
    }

eBool CliAtSurEnginePmParamRecentPeriodsShow(char argc, char **argv,
                                             const char *paramTypeStr[],
                                             const uint32 *paramTypeVal,
                                             uint32 numParams,
                                             ChannelsIdParseFunc idParseFunc)
    {
    uint32 channel_i;
    tTab *tabPtr = NULL;
    uint32 row_i;
    const char *pHeading[] = {"Param", "", "", "", "", "", "", "", "", "", ""};
    uint32 numChannels;
    AtChannel* channels;
    uint32 recentPeriod;
    uint32 column = 0;
    uint32 numRows = numParams;
    eAtHistoryReadingMode readingMode;
    int32 (*RegisterReadFunc)(AtPmRegister self) = AtPmRegisterValue;
    eBool silent;

    if (argc < 3)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    recentPeriod = AtStrToDw(argv[1]);
    readingMode = CliHistoryReadingModeGet(argv[2]);

    /* Have a list of channels */
    channels = ChannelsParse(argv[0], idParseFunc, &numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevWarning, "WARNING: no channels to display, ID list may be invalid\r\n");
        return cAtTrue;
        }

    /* Reading mode must be valid */
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Invalid reading action mode. Expected: %s\r\n",
                 CliValidHistoryReadingModes());
        return cAtFalse;
        }
    if (readingMode == cAtHistoryReadingModeReadToClear)
        RegisterReadFunc = AtPmRegisterReset;

    silent = CliHistoryReadingShouldSilent(readingMode, (argc > 3 ) ? argv[3] : NULL);
    if (!silent)
        {
    tabPtr = TableAlloc(numRows, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }
        }

    /* Put all of parameters as title */
    for (row_i = 0; row_i < numRows; row_i++)
        StrToCell(tabPtr, row_i, 0, paramTypeStr[row_i]);

    for (channel_i = 0; channel_i < numChannels; channel_i = AtCliNextIdWithSleep(channel_i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtSurEngine engine = AtChannelSurEngineGet(channels[channel_i]);

        column = (channel_i % cNumChannelsPerTable) + 1;

        /* Enough one table to print */
        if ((channel_i > 0) && ((channel_i % cNumChannelsPerTable) == 0))
            {
            TablePrint(tabPtr);
            AtPrintc(cSevInfo, "Continue...\r\n");
            }

        /* Channel ID */
        TableColumnHeaderSet(tabPtr, column, CliChannelIdStringGet(channels[channel_i]));

        for (row_i = 0; row_i < numRows; row_i++)
            {
            AtPmRegister pmRegsiter;
            uint32 paramType = paramTypeVal[row_i];
            AtPmParam pmParam;
            eBool valid;
            int32 regValue;

            /* Parameter may be invalid */
            pmParam = AtSurEnginePmParamGet(engine, paramType);
            if (pmParam == NULL)
                {
                ColorStrToCell(tabPtr, row_i, column, "error", cSevCritical);
                continue;
                }

            /* Register may also not exist */
            pmRegsiter = AtPmParamRecentRegister(pmParam, (uint8)(recentPeriod - 1));
            if (pmRegsiter == NULL)
                {
                ColorStrToCell(tabPtr, row_i, column, "error", cSevCritical);
                continue;
                }

            /* Register value may be also invalid. Need to handle properly */
            regValue = RegisterReadFunc(pmRegsiter);
            valid = AtPmRegisterIsValid(pmRegsiter);
            if (valid)
                ColorStrToCell(tabPtr, row_i, column, CliNumber2String(regValue, "%d"), regValue ? cSevCritical : cSevNormal);
            else
                ColorStrToCell(tabPtr, row_i, column, "xxx", cSevCritical);
            }
        }

    /* There is always one last table and it may contain less than number of
     * columns to display for one table */
    SubTablePrint(tabPtr, 0, numRows - 1, 0, column);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CliAtSurEnginePmParamRecentRegisterReset(char argc, char **argv,
                                               const char *paramTypeStr[],
                                               const uint32 *paramTypeVal,
                                               uint32 numParams,
                                               ChannelsIdParseFunc idParseFunc)
    {
    uint32 channel_i;
    eBool success = cAtTrue;
    uint32 numChannels;
    AtChannel* channels = ChannelsParse(argv[0], idParseFunc, &numChannels);
    AtTokenizer paramParser = AtTokenizerSharedTokenizer(argv[1], ",");
    AtIdParser periodParser = AtIdParserNew(argv[2], NULL, NULL);

    AtUnused(argc);

    /* The threshold */
    for (channel_i = 0; channel_i < numChannels; channel_i = AtCliNextIdWithSleep(channel_i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtSurEngine surEngine = AtChannelSurEngineGet(channels[channel_i]);
        AtPmParam param;
        AtPmRegister pmRegister;

        /* Ignore not existing engine */
        if (surEngine == NULL)
            {
            AtPrintc(cSevWarning,
                     "WARNING: Ignore engine of %s which does not have Surveillance engine\r\n",
                     CliChannelIdStringGet(channels[channel_i]));
            continue;
            }

        /* Handle all input parameters */
        AtTokenizerRestart(paramParser);
        while(AtTokenizerHasNextString(paramParser))
            {
            char *paramString = AtTokenizerNextString(paramParser);
            eBool convertSuccess = cAtTrue;
            uint32 paramType;

            /* Convert parameter type */
            paramType = CliStringToEnum(paramString, paramTypeStr, paramTypeVal, numParams, &convertSuccess);
            if (!convertSuccess)
                {
                AtPrintc(cSevWarning, "WARNING: Unknown parameter %s and it is ignored, expected: ", paramString);
                CliExpectedValuesPrint(cSevWarning, paramTypeStr, numParams);
                AtPrintc(cSevWarning, "\r\n");
                continue;
                }

            /* Parameter may be not supported */
            param = AtSurEnginePmParamGet(surEngine, paramType);
            if (param == NULL)
                {
                AtPrintc(cSevWarning,
                         "WARNING: ignore parameter %s which may not exist\r\n",
                         paramString);
                continue;
                }

            /* Reset all of input recent periods */
            AtIdParserRestart(periodParser);
            while (AtIdParserHasNextNumber(periodParser))
                {
                uint32 period;

                period = AtIdParserNextNumber(periodParser);
                if (period == 0)
                    {
                    AtPrintc(cSevWarning, "WARNING: Recent period start from 1\r\n");
                    continue;
                    }

                pmRegister = AtPmParamRecentRegister(param, (uint8)(period - 1));
                if (pmRegister == NULL)
                    {
                    AtPrintc(cSevWarning,
                             "WARNING: Ignore period %u which does not exist\r\n",
                             period);
                    continue;
                    }

                AtPmRegisterReset(pmRegister);
                }
            }
        }

    AtObjectDelete((AtObject)periodParser);

    return success;
    }

eBool CliAtSurEngineNotificationMaskSet(char argc, char **argv,
                                        const char *typeStr[],
                                        const uint32 *typeVal,
                                        uint32 numTypes,
                                        eAtRet (*NotificationMaskSet)(AtSurEngine, uint32, uint32),
                                        ChannelsIdParseFunc idParseFunc)
    {
    uint32 channel_i;
    eBool success = cAtTrue;
    uint32 numChannels;
    AtChannel* channels = ChannelsParse(argv[0], idParseFunc, &numChannels);
    AtTokenizer typeParser = AtTokenizerSharedTokenizer(argv[1], "|");
    uint32 interruptMask = 0, enabledMask;
    eBool enabled = CliBoolFromString(argv[2]);

    AtUnused(argc);

    /* Parse masks */
    while (AtTokenizerHasNextString(typeParser))
        {
        char *alarmTypeStr = AtTokenizerNextString(typeParser);
        eBool convertSuccess = cAtFalse;
        uint32 alarmMask = CliStringToEnum(alarmTypeStr, typeStr, typeVal, numTypes, &convertSuccess);

        if (!convertSuccess)
            {
            AtPrintc(cSevCritical, "ERROR: Unknown type %s, expect: ", alarmTypeStr);
            CliExpectedValuesPrint(cSevCritical, typeStr, numTypes);
            AtPrintc(cSevCritical, "\r\n");
            return cAtFalse;
            }

        interruptMask = interruptMask | alarmMask;
        }

    enabledMask = enabled ? interruptMask : 0;

    for (channel_i = 0; channel_i < numChannels; channel_i = AtCliNextIdWithSleep(channel_i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtChannel channel = channels[channel_i];
        AtSurEngine engine = AtChannelSurEngineGet(channel);
        eAtRet ret = NotificationMaskSet(engine, interruptMask, enabledMask);

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: %s notification fail on %s, ret = %s\r\n",
                     enabled ? "Enable" : "Disable",
                     CliChannelIdStringGet(channel),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CliAtSurEngineFailuresShow(char argc, char **argv,
                                 const char *failureTypeStr[],
                                 const uint32 *failureTypeVal,
                                 uint32 numFailures,
                                 ChannelsIdParseFunc idParseFunc)
    {
    static const eBool silent = cAtFalse;
    return FailuresShow(argc,
                        argv,
                        failureTypeStr,
                        failureTypeVal,
                        numFailures,
                        AtSurEngineCurrentFailuresGet,
                        idParseFunc,
                        silent);
    }

eBool CliAtSurEngineFailuresHistoryShow(char argc, char **argv,
                                        const char *failureTypeStr[],
                                        const uint32 *failureTypeVal,
                                        uint32 numFailures,
                                        ChannelsIdParseFunc idParseFunc)
    {
    eAtHistoryReadingMode readingMode;
    eBool silent = cAtFalse;
    uint32 (*FailureChangedHistory)(AtSurEngine self) = NULL;

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    /* Get reading action mode */
    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    silent = CliHistoryReadingShouldSilent(readingMode, (argc > 2 ) ? argv[2] : NULL);
    if (readingMode == cAtHistoryReadingModeReadOnly)
        FailureChangedHistory = AtSurEngineFailureChangedHistoryGet;
    else
        FailureChangedHistory = AtSurEngineFailureChangedHistoryClear;

    return FailuresShow(argc, argv,
                        failureTypeStr,
                        failureTypeVal,
                        numFailures,
                        FailureChangedHistory,
                        idParseFunc,
                        silent);
    }

void CliAtSurEngineFailureNotify(AtSurEngine surEngine,
                                 void *listener,
                                 uint32 failureType,
                                 uint32 currentStatus,
                                 const char *failureTypeStr[],
                                 const uint32 *failureTypeVal,
                                 uint32 numFailures)
    {
    uint32 alarm_i;
    AtChannel channel = AtSurEngineChannelGet(surEngine);
    const char *moduleName = AtModuleTypeString(AtChannelModuleGet(channel));

    AtUnused(listener);

    AtPrintc(cSevNormal, "\r\n");
    AtPrintc(cSevNormal, "%s [%s] Failure changed at channel %s",
             AtOsalDateTimeInUsGet(),
             moduleName,
             CliChannelIdStringGet(channel));

    for (alarm_i = 0; alarm_i < numFailures; alarm_i++)
        {
        uint32 alarmType = failureTypeVal[alarm_i];
        if ((failureType & alarmType) == 0)
            continue;

        AtPrintc(cSevNormal, " * [%s:", failureTypeStr[alarm_i]);
        if (currentStatus & alarmType)
            AtPrintc(cSevCritical, "set");
        else
            AtPrintc(cSevInfo, "clear");
        AtPrintc(cSevNormal, "]");
        }

    AtPrintc(cSevInfo, "\r\n");
    }

eBool CliAtSurEngineNotificationEnable(char argc, char **argv,
                                       eBool enable,
                                       const tAtSurEngineListener *listener,
                                       ChannelsIdParseFunc idParseFunc,
                                       AtChannelObserver (*ObserverGet)(AtChannel),
                                       void (*ObserverDelete)(AtChannel))
    {
    uint32 numChannels;
    AtChannel* channels = ChannelsParse(argv[0], idParseFunc, &numChannels);
    uint32 channel_i;
    eBool success = cAtTrue;

    AtUnused(argc);

    for (channel_i = 0; channel_i < numChannels; channel_i = AtCliNextIdWithSleep(channel_i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtSurEngine surEngine = AtChannelSurEngineGet(channels[channel_i]);
        eAtRet ret;

        /* Ignore not existing engine */
        if (surEngine == NULL)
            {
            AtPrintc(cSevWarning,
                     "WARNING: Ignore engine of %s which does not have Surveillance engine\r\n",
                     CliChannelIdStringGet(channels[channel_i]));
            continue;
            }

        if (enable)
            {
            if (AutotestIsEnabled())
                ret = AtSurEngineListenerAdd(surEngine, ObserverGet(channels[channel_i]), listener);
            else
                ret = AtSurEngineListenerAdd(surEngine, NULL, listener);
            }
        else
            {
            if (AutotestIsEnabled())
                {
                ret = AtSurEngineListenerRemove(surEngine, ObserverGet(channels[channel_i]), listener);
                ObserverDelete(channels[channel_i]);
                }
            else
                ret = AtSurEngineListenerRemove(surEngine, NULL, listener);
            }

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Listen handling fail on channel %s, ret = %s\r\n",
                     CliChannelIdStringGet(channels[channel_i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CliAtSurEngineThresholdProfileSet(char argc, char **argv, ChannelsIdParseFunc idParseFunc)
    {
    uint32 channel_i;
    eBool success = cAtTrue;
    uint32 numChannels;
    AtChannel* channels = ChannelsParse(argv[0], idParseFunc, &numChannels);
    uint32 profileId = AtStrtoul(argv[1], NULL, 10);
    AtModuleSur moduleSur = CliModuleSur();
    AtPmThresholdProfile profile = AtModuleSurThresholdProfileGet(moduleSur, profileId - 1);

    AtUnused(argc);
    if (profile == NULL)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Profile %u is not exits, expected input 1 to %u,\r\n",
                 profileId,
                 AtModuleSurNumThresholdProfiles(moduleSur));
        return cAtFalse;
        }

    for (channel_i = 0; channel_i < numChannels; channel_i = AtCliNextIdWithSleep(channel_i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret;
        AtChannel channel = channels[channel_i];
        AtSurEngine engine = AtChannelSurEngineGet(channel);
        if (engine == NULL)
        	{
            AtPrintc(cSevCritical,
                     "ERROR: Channel %s can not gen Sur Engine\r\n",
                     CliChannelIdStringGet(channel));
            success = cAtFalse;
        	continue;
        	}

        ret = AtSurEngineThresholdProfileSet(engine, profile);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Set Profile %s for Surveillance engine fail on channel %s, ret = %s\r\n",
                     argv[1],
                     CliChannelIdStringGet(channel),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CliAtSurEngineThresholdProfileShow(char argc, char **argv, ChannelsIdParseFunc idParseFunc)
    {
    uint32 channel_i;
    uint32 numChannels;
    AtChannel* channels = ChannelsParse(argv[0], idParseFunc, &numChannels);
    const char *pHeading[] = {"engineId", "profileId"};
    tTab *tabPtr = NULL;

    AtUnused(argc);
    if (numChannels == 0)
		{
		AtPrintc(cSevCritical, "No channel for show with input %s\r\n", argv[0]);
		return cAtFalse;
		}

    tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
    if (tabPtr == NULL)
        {
        AtPrintc(cSevCritical, "Cannot create table\r\n");
        return cAtFalse;
        }

    for (channel_i = 0; channel_i < numChannels; channel_i = AtCliNextIdWithSleep(channel_i, AtCliLimitNumRestTdmChannelGet()))
        {
        uint32 column = 0;
        AtSurEngine engine = AtChannelSurEngineGet(channels[channel_i]);
        AtPmThresholdProfile profile = AtSurEngineThresholdProfileGet(engine);

        /* Put channel ID */
        StrToCell(tabPtr, channel_i, column++, (char *)CliChannelIdStringGet(channels[channel_i]));
        if (profile)
        	StrToCell(tabPtr, channel_i, column++, CliNumber2String(AtPmThresholdProfileIdGet(profile) + 1, "%d"));
        else
        	StrToCell(tabPtr, channel_i, column++, "None");
        }

    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);
    return cAtTrue;
    }

eBool CliAtSurEngingTcaNotificationMaskShow(char argc, char **argv,
                                             const char *pmTypeStr[],
                                             const uint32 *pmTypeVal,
                                             uint32 numPmTypes,
                                             ChannelsIdParseFunc idParseFunc)
    {
    static const eBool silent = cAtFalse;
    return FailuresMaskShow(argc, argv,
                            pmTypeStr,
                            pmTypeVal,
                            numPmTypes,
                            AtSurEngineTcaNotificationMaskGet,
                            idParseFunc,
                            silent);
    }

eBool CliAtSurEngineTcaHistoryShow(char argc, char **argv,
                                        const char *pmTypeStr[],
                                        const uint32 *pmTypeVal,
                                        uint32 numPmTypes,
                                        ChannelsIdParseFunc idParseFunc)
    {
    eAtHistoryReadingMode readingMode;
    eBool silent = cAtFalse;
    uint32 (*TcaChangedHistory)(AtSurEngine self) = NULL;

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    /* Get reading action mode */
    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    if (readingMode == cAtHistoryReadingModeReadOnly)
        TcaChangedHistory = AtSurEngineTcaChangedHistoryGet;
    else
        TcaChangedHistory = AtSurEngineTcaChangedHistoryClear;

    if (argc > 2)
        silent = CliHistoryReadingShouldSilent(readingMode, argv[2]);

    return FailuresShow(argc, argv,
                        pmTypeStr,
                        pmTypeVal,
                        numPmTypes,
                        TcaChangedHistory,
                        idParseFunc,
                        silent);
    }

void CliAtSurEngineTcaNotify(AtSurEngine surEngine,
                             void *listener,
                             AtPmParam param,
                             AtPmRegister pmRegister,
                             const char *paramTypeStr[],
                             const uint32 *paramTypeVal,
                             uint32 numParams)
    {
    uint32 param_i;
    AtChannel channel = AtSurEngineChannelGet(surEngine);
    const char *moduleName = AtModuleTypeString(AtChannelModuleGet(channel));

    AtUnused(listener);
    AtUnused(pmRegister);

    AtPrintc(cSevNormal, "\r\n");
    AtPrintc(cSevNormal, "%s [%s] Threshold crossing alarms at channel %s",
             AtOsalDateTimeInUsGet(),
             moduleName,
             CliChannelIdStringGet(channel));

    for (param_i = 0; param_i < numParams; param_i++)
        {
        uint32 paramType = paramTypeVal[param_i];
        if ((AtPmParamTypeGet(param) & paramType) == 0)
            continue;

        AtPrintc(cSevNormal, " * [");
        AtPrintc(cSevCritical, "%s", paramTypeStr[param_i]);
        AtPrintc(cSevNormal, "]");
        }

    AtPrintc(cSevInfo, "\r\n");
    }

eBool CliAtSurEngineParamPeriodThresholdShow(char argc, char **argv,
                                             const char *paramTypeStr[],
                                             const uint32 *paramTypeVal,
                                             uint32 numParams,
                                             ChannelsIdParseFunc idParseFunc)
    {
    return ParamThresholdShow(argc, argv, paramTypeStr, paramTypeVal, numParams, cAtPmCurrentPeriodRegister, idParseFunc);
    }

eBool CliAtSurEngineParamDayThresholdShow(char argc, char **argv,
                                          const char *paramTypeStr[],
                                          const uint32 *paramTypeVal,
                                          uint32 numParams,
                                          ChannelsIdParseFunc idParseFunc)
    {
    return ParamThresholdShow(argc, argv, paramTypeStr, paramTypeVal, numParams, cAtPmCurrentDayRegister, idParseFunc);
    }

eBool CliAtSurEngineFailuresCaptureShow(char argc, char **argv,
                                        const char *failureTypeStr[],
                                        const uint32 *failureTypeVal,
                                        uint32 numFailures,
                                        ChannelsIdParseFunc idParseFunc,
                                        AtChannelObserver (*ObserverGet)(AtChannel),
                                        void (*ObserverDelete)(AtChannel))
    {
    return FailuresCaptureShow(argc, argv, failureTypeStr, failureTypeVal, numFailures, idParseFunc, ObserverGet, ObserverDelete);
    }

eBool CliAtSurEngineTcaCaptureShow(char argc, char **argv,
                                   const char *pmTypeStr[],
                                   const uint32 *pmTypeVal,
                                   uint32 numPmTypes,
                                   ChannelsIdParseFunc idParseFunc,
                                   AtChannelObserver (*ObserverGet)(AtChannel),
                                   void (*ObserverDelete)(AtChannel))
    {
    eAtHistoryReadingMode readingMode;
    eBool silent = cAtFalse;
    uint32 (*CapturedTcaGet)(AtChannelObserver) = NULL;
    uint32 numChannels, i;
    AtChannel* channels = ChannelsParse(argv[0], idParseFunc, &numChannels);
    tTab *tabPtr = NULL;
    char **pHeading;

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    /* Get reading action mode */
    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    if (readingMode == cAtHistoryReadingModeReadOnly)
        CapturedTcaGet = AtChannelObserverTcaHistoryGet;
    else
        CapturedTcaGet = AtChannelObserverTcaHistoryClear;

    if (argc > 2)
        silent = CliHistoryReadingShouldSilent(readingMode, argv[2]);

    if (!silent)
        {
        /* Create table with titles */
        pHeading = CliSurTableHeadingCreate(NULL, pmTypeStr, numPmTypes + 1);

        tabPtr = TableAlloc(numChannels, numPmTypes + 1, AtCast(const char **, pHeading));
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
            return cAtFalse;
            }
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtChannel channel = channels[i];
        AtChannelObserver observer = ObserverGet(channel);
        uint8 column = 0;
        uint32 history;
        uint8 j;

        StrToCell(tabPtr, i, column++, CliChannelIdStringGet(channel));

        history = CapturedTcaGet(observer);

        for (j = 0; j < numPmTypes; j++)
            CliCapturedAlarmToCell(tabPtr, i, column++, history, history, pmTypeVal[j]);

        ObserverDelete(channel);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    CliSurTableHeadingDelete(pHeading, numPmTypes + 1);

    return cAtTrue;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : CliAtModuleSur.h
 *
 * Created Date: Mar 22, 2015
 *
 * Description : SUR Module CLIs
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef CLIATSUR_H_
#define CLIATSUR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCli.h"
#include "AtCliModule.h"
#include "../../cmd/textui/CliAtTextUI.h"
#include "AtPmThresholdProfile.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef uint32 (*FailureFuncGet)(AtSurEngine self);
typedef eAtRet (*CliPmThresholdSet)(AtPmParam, uint32);
typedef AtPmRegister (*CliPmRecentRegiserGet)(AtPmParam, uint8);
typedef eAtRet (*CliPmThresholdEntrySet)(AtSurEngine, uint32);
typedef uint32 (*ParamFromStringFuncGet)(char*);
typedef const char * (*ParamToStringFunc)(uint32);
typedef eAtRet (*TcaThresholdFuncSet)(AtPmThresholdProfile, uint32, uint32);
typedef uint32 (*TcaThresholdFuncGet)(AtPmThresholdProfile, uint32);
/*--------------------------- Forward declarations ---------------------------*/
char **CliSurTableHeadingCreate(const char *fisrtTitle, const char *remainingTitleNames[], uint32 numTitles);
void CliSurTableHeadingDelete(char **pHeading, uint32 numTitles);

/*--------------------------- Entries ----------------------------------------*/
AtModuleSur CliModuleSur(void);

eBool CliSurThresholdProfileParamValueSet(char argc,
                                          char **argv,
                                          ParamFromStringFuncGet paramGet,
                                          TcaThresholdFuncSet thresholdSet);
eBool CliSurThresholProfileShow(char argc,
                                char **argv,
                                const char *pramaTypeStr[],
                                const uint32 *paramTypeVal,
                                uint32 numParams,
                                TcaThresholdFuncGet thresholdGetFunc);

const char **CliAtSdhPathFailureTypeStr(uint32 *numFailures);
const uint32 *CliAtSdhPathFailureTypeVal(uint32 *numFailures);
const char **CliAtSdhLineFailureTypeStr(uint32 *numFailures);
const uint32 *CliAtSdhLineFailureTypeVal(uint32 *numFailures);

uint32 CliTdmPwsFromString(char *idString, AtChannel* channels, uint32 bufferSize);

#endif /* CLIATSUR_H_ */

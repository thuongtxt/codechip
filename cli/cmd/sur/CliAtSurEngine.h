/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : SUR
 *
 * File        : CliAtSurEngine.h
 *
 * Created Date: Sep 9, 2015
 *
 * Description : SUR CLI engine: static inclusion methods.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLIATSURENGINE_H_
#define _CLIATSURENGINE_H_

/*--------------------------- Include files ----------------------------------*/
#include "AtSurEngine.h"
#include "../common/AtChannelObserver.h"
#include "../common/CliAtChannelObserver.h"

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/
typedef uint32 (*ChannelsIdParseFunc)(char *, AtChannel *, uint32);
typedef AtPmRegister (*CliPmRegisterGet)(AtPmParam);

typedef int32 (*CliParamCounterParser)(void* allCounters, uint32 paramType);
typedef eAtRet (*CliSurEnableFunc)(AtSurEngine, eBool);

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool CliAtSurEngineInit(char argc, char **argv, ChannelsIdParseFunc idParseFunc);
eBool CliAtSurEngineEnable(char argc, char **argv,
                           eBool enable,
                           ChannelsIdParseFunc idParseFunc,
                           CliSurEnableFunc enableFunc);

eBool CliAtSurEngineShow(char argc, char **argv, ChannelsIdParseFunc idParseFunc);
eBool CliAtSurEngineFailuresShow(char argc, char **argv,
                                 const char *pmTypeStr[],
                                 const uint32 *pmTypeVal,
                                 uint32 numPmTypes,
                                 ChannelsIdParseFunc idParseFunc);
eBool CliAtSurEngineFailuresHistoryShow(char argc, char **argv,
                                        const char *pmTypeStr[],
                                        const uint32 *pmTypeVal,
                                        uint32 numPmTypes,
                                        ChannelsIdParseFunc idParseFunc);
eBool CliAtSurEngingFailureNotificationMaskShow(char argc, char **argv,
                                             const char *pmTypeStr[],
                                             const uint32 *pmTypeVal,
                                             uint32 numPmTypes,
                                             ChannelsIdParseFunc idParseFunc);
eBool CliAtSurEngineTcaHistoryShow(char argc, char **argv,
                                   const char *pmTypeStr[],
                                   const uint32 *pmTypeVal,
                                   uint32 numPmTypes,
                                   ChannelsIdParseFunc idParseFunc);
eBool CliAtSurEngingTcaNotificationMaskShow(char argc, char **argv,
                                             const char *pmTypeStr[],
                                             const uint32 *pmTypeVal,
                                             uint32 numPmTypes,
                                             ChannelsIdParseFunc idParseFunc);
eBool CliAtSurEngineNotificationMaskSet(char argc, char **argv,
                                        const char *typeStr[],
                                        const uint32 *typeVal,
                                        uint32 numTypes,
                                        eAtRet (*NotificationMaskSet)(AtSurEngine, uint32, uint32),
                                        ChannelsIdParseFunc idParseFunc);
eBool CliAtSurEnginePmParamRegistersShow(char argc, char **argv,
                                         const char *paramTypeStr[],
                                         const uint32 *paramTypeVal,
                                         uint32 numParams,
                                         void* allPmCounters,
                                         eAtPmRegisterType regType,
                                         CliParamCounterParser getCounter,
                                         ChannelsIdParseFunc idParseFunc);
eBool CliAtSurEnginePmParamRecentPeriodsShow(char argc, char **argv,
                                             const char *paramTypeStr[],
                                             const uint32 *paramTypeVal,
                                             uint32 numParams,
                                             ChannelsIdParseFunc idParseFunc);

eBool CliAtSurEngineTcaThresholdSet(char argc, char **argv,
                                    const char *paramTypeStr[],
                                    const uint32 *paramTypeVal,
                                    uint32 numParams,
                                    ChannelsIdParseFunc idParseFunc);
eBool CliAtSurEnginePmParamRecentRegisterReset(char argc, char **argv,
                                               const char *paramTypeStr[],
                                               const uint32 *paramTypeVal,
                                               uint32 numParams,
                                               ChannelsIdParseFunc idParseFunc);
eBool CliAtSurEngineKThresholdEntrySet(char argc, char **argv, ChannelsIdParseFunc idParseFunc);

eBool CliAtSurEngineNotificationEnable(char argc, char **argv,
                                       eBool enable,
                                       const tAtSurEngineListener *listener,
                                       ChannelsIdParseFunc idParseFunc,
                                       AtChannelObserver (*ObserverGet)(AtChannel),
                                       void (*ObserverDelete)(AtChannel));
void CliAtSurEngineFailureNotify(AtSurEngine surEngine,
                                 void *listener,
                                 uint32 failureType,
                                 uint32 currentStatus,
                                 const char *failureTypeStr[],
                                 const uint32 *failureTypeVal,
                                 uint32 numFailures);
void CliAtSurEngineTcaNotify(AtSurEngine surEngine,
                             void *listener,
                             AtPmParam param,
                             AtPmRegister pmRegister,
                             const char *paramTypeStr[],
                             const uint32 *paramTypeVal,
                             uint32 numParams);

/* Threshold Profile*/
eBool CliAtSurEngineThresholdProfileSet(char argc, char **argv, ChannelsIdParseFunc idParseFunc);
eBool CliAtSurEngineThresholdProfileShow(char argc, char **argv, ChannelsIdParseFunc idParseFunc);

/* Thresholds */
eBool CliAtSurEngineParamPeriodThresholdShow(char argc, char **argv,
                                             const char *paramTypeStr[],
                                             const uint32 *paramTypeVal,
                                             uint32 numParams,
                                             ChannelsIdParseFunc idParseFunc);
eBool CliAtSurEngineParamDayThresholdShow(char argc, char **argv,
                                          const char *paramTypeStr[],
                                          const uint32 *paramTypeVal,
                                          uint32 numParams,
                                          ChannelsIdParseFunc idParseFunc);
eBool CliAtSurEngineFailuresCaptureShow(char argc, char **argv,
                                        const char *failureTypeStr[],
                                        const uint32 *failureTypeVal,
                                        uint32 numFailures,
                                        ChannelsIdParseFunc idParseFunc,
                                        AtChannelObserver (*ObserverGet)(AtChannel),
                                        void (*ObserverDelete)(AtChannel));
eBool CliAtSurEngineTcaCaptureShow(char argc, char **argv,
                                   const char *pmTypeStr[],
                                   const uint32 *pmTypeVal,
                                   uint32 numPmTypes,
                                   ChannelsIdParseFunc idParseFunc,
                                   AtChannelObserver (*ObserverGet)(AtChannel),
                                   void (*ObserverDelete)(AtChannel));

#ifdef __cplusplus
}
#endif

#endif /* _CLIATSURENGINE_H_ */

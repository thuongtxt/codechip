/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Sur-Pdh-De1
 *
 * File        : CliAtSurPdhDe1.c
 *
 * Created Date: Mar 22, 2015
 *
 * Description :N/A
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtPdhDe1.h"
#include "CliAtModuleSur.h"
#include "CliAtSurEngine.h"

/*--------------------------- Define -----------------------------------------*/
static const uint32 cPdhDe1PmParamVal[] =
    {
     cAtSurEnginePdhDe1PmParamCvL,
     cAtSurEnginePdhDe1PmParamEsL,
     cAtSurEnginePdhDe1PmParamSesL,
     cAtSurEnginePdhDe1PmParamLossL,
     cAtSurEnginePdhDe1PmParamEsLfe,
     cAtSurEnginePdhDe1PmParamCvP,
     cAtSurEnginePdhDe1PmParamEsP,
     cAtSurEnginePdhDe1PmParamSesP,
     cAtSurEnginePdhDe1PmParamAissP,
     cAtSurEnginePdhDe1PmParamSasP,
     cAtSurEnginePdhDe1PmParamCssP,
     cAtSurEnginePdhDe1PmParamUasP,
     cAtSurEnginePdhDe1PmParamSefsPfe,
     cAtSurEnginePdhDe1PmParamEsPfe,
     cAtSurEnginePdhDe1PmParamSesPfe,
     cAtSurEnginePdhDe1PmParamCssPfe,
     cAtSurEnginePdhDe1PmParamFcPfe,
     cAtSurEnginePdhDe1PmParamUasPfe,
     cAtSurEnginePdhDe1PmParamFcP
    };

static const char *cPdhDe1PmParamUpperStr[] =
    {
    "CV-L",
    "ES-L",
    "SES-L",
    "LOSS-L",
    "ES-LFE",
    "CV-P",
    "ES-P",
    "SES-P",
    "AISS-P",
    "SAS-P",
    "CSS-P",
    "UAS-P",
    "SEFS-PFE",
    "ES-PFE",
    "SES-PFE",
    "CSS-PFE",
    "FC-PFE",
    "UAS-PFE",
    "FC-P"
    };

static const char *cPdhDe1PmParamStr[] =
    {
    "cvl",
    "esl",
    "sesl",
    "lossl",
    "eslfe",
    "cvp",
    "esp",
    "sesp",
    "aissp",
    "sasp",
    "cssp",
    "uasp",
    "sefspfe",
    "espfe",
    "sespfe",
    "csspfe",
    "fcpfe",
    "uaspfe",
    "fcp"
    };

static const uint32 cAtPdhDe1AlarmVal[] =
    {
    cAtPdhDe1AlarmLos,
    cAtPdhDe1AlarmLof,
    cAtPdhDe1AlarmAis,
    cAtPdhDe1AlarmRai,
    cAtPdhDe1AlarmAisCi,
    cAtPdhDe1AlarmRaiCi
    };

static const char *cAtPdhDe1AlarmStr[] =
    {
    "los",
    "lof",
    "ais",
    "rai",
    "ais-ci",
    "rai-ci"
    };

static const char *cAtPdhDe1AlarmUpperStr[] =
    {
    "LOS",
    "LOF",
    "AIS",
    "RAI",
    "AIS-CI",
    "RAI-CI"
    };

/*--------------------------- Macros -----------------------------------------*/
#define mParamCounter(paramName)    return (counters)->paramName;
/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool Enable(char argc, char **argv, eBool enable, CliSurEnableFunc enableFunc)
    {
    return CliAtSurEngineEnable(argc, argv, enable, De1ListFromString, enableFunc);
    }

static int32 De1ParamCounterGet(void* allCounters, uint32 paramType)
    {
    tAtSurEnginePdhDe1PmCounters *counters = (tAtSurEnginePdhDe1PmCounters *)allCounters;
    switch (paramType)
        {
        case cAtSurEnginePdhDe1PmParamCvL:
            mParamCounter(cvL)
        case cAtSurEnginePdhDe1PmParamEsL:
            mParamCounter(esL)
        case cAtSurEnginePdhDe1PmParamSesL:
            mParamCounter(sesL)
        case cAtSurEnginePdhDe1PmParamLossL:
            mParamCounter(lossL)
        case cAtSurEnginePdhDe1PmParamEsLfe:
            mParamCounter(esLfe)
        case cAtSurEnginePdhDe1PmParamCvP:
            mParamCounter(cvP)
        case cAtSurEnginePdhDe1PmParamEsP:
            mParamCounter(esP)
        case cAtSurEnginePdhDe1PmParamSesP:
            mParamCounter(sesP)
        case cAtSurEnginePdhDe1PmParamAissP:
            mParamCounter(aissP)
        case cAtSurEnginePdhDe1PmParamSasP:
            mParamCounter(sasP)
        case cAtSurEnginePdhDe1PmParamCssP:
            mParamCounter(cssP)
        case cAtSurEnginePdhDe1PmParamUasP:
            mParamCounter(uasP)
        case cAtSurEnginePdhDe1PmParamSefsPfe:
            mParamCounter(sefsPfe)
        case cAtSurEnginePdhDe1PmParamEsPfe:
            mParamCounter(esPfe)
        case cAtSurEnginePdhDe1PmParamSesPfe:
            mParamCounter(sesPfe)
        case cAtSurEnginePdhDe1PmParamCssPfe:
            mParamCounter(cssPfe)
        case cAtSurEnginePdhDe1PmParamFcPfe:
            mParamCounter(fcPfe)
        case cAtSurEnginePdhDe1PmParamUasPfe:
            mParamCounter(uasPfe)
        case cAtSurEnginePdhDe1PmParamFcP:
            mParamCounter(fcP)
        default:
            return 0;
        }
    }

static eBool RegistersShow(char argc, char **argv, eAtPmRegisterType regType)
    {
    tAtSurEnginePdhDe1PmCounters counters;
    return CliAtSurEnginePmParamRegistersShow(argc, argv,
                                      cPdhDe1PmParamUpperStr,
                                      cPdhDe1PmParamVal,
                                      mCount(cPdhDe1PmParamVal),
                                      (void *)&counters,
                                      regType,
                                      De1ParamCounterGet,
                                      De1ListFromString);
    }

static void FailureNotify(AtSurEngine surEngine,
                          void *listener,
                          uint32 failureType,
                          uint32 currentStatus)
    {
    CliAtSurEngineFailureNotify(surEngine,
                                listener,
                                failureType,
                                currentStatus,
                                cAtPdhDe1AlarmUpperStr,
                                cAtPdhDe1AlarmVal,
                                mCount(cAtPdhDe1AlarmVal));
    }

static void TcaNotify(AtSurEngine surEngine,
                      void *listener,
                      AtPmParam param,
                      AtPmRegister pmRegister)
    {
    CliAtSurEngineTcaNotify(surEngine,
                            listener,
                            param,
                            pmRegister,
                            cPdhDe1PmParamUpperStr,
                            cPdhDe1PmParamVal,
                            mCount(cPdhDe1PmParamVal));
    }

static void AutotestFailureChangeState(AtSurEngine engine, void *userData, uint32 changedAlarms, uint32 currentStatus)
    {
    AtChannelObserver observer = userData;
    AtUnused(engine);
    AtChannelObserverFailuresUpdate(observer, changedAlarms, currentStatus);
    }

static void AutotestTcaChangeState(AtSurEngine engine, void *userData, AtPmParam param, AtPmRegister pmRegister)
    {
    AtChannelObserver observer = userData;
    uint32 changedParams = AtPmParamTypeGet(param);
    AtUnused(engine);
    AtUnused(pmRegister);
    AtChannelObserverTcaUpdate(observer, changedParams);
    }

static tAtSurEngineListener *Listener(void)
    {
    static tAtSurEngineListener listener;
    static tAtSurEngineListener *pListener = NULL;

    if (pListener)
        return pListener;

    AtOsalMemInit(&listener, 0, sizeof(listener));
    if (AutotestIsEnabled())
        {
        listener.FailureNotify = AutotestFailureChangeState;
        listener.TcaNotify     = AutotestTcaChangeState;
        }
    else
        {
        listener.FailureNotify = FailureNotify;
        listener.TcaNotify     = TcaNotify;
        }

    pListener = &listener;
    return pListener;
    }

eBool CmdPdhDe1SurInit(char argc, char **argv)
    {
    return CliAtSurEngineInit(argc, argv, De1ListFromString);
    }

eBool CmdPdhDe1SurEnable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue, AtSurEngineEnable);
    }

eBool CmdPdhDe1SurDisable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse, AtSurEngineEnable);
    }

eBool CmdPdhDe1SurPmEnable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue, AtSurEnginePmEnable);
    }

eBool CmdPdhDe1SurPmDisable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse, AtSurEnginePmEnable);
    }

eBool CmdPdhDe1NotificationEnable(char argc, char **argv)
    {
    return CliAtSurEngineNotificationEnable(argc, argv, cAtTrue, Listener(), De1ListFromString,
                                            CliAtPdhDe1ObserverGet, CliAtPdhDe1ObserverDelete);
    }

eBool CmdPdhDe1NotificationDisable(char argc, char **argv)
    {
    return CliAtSurEngineNotificationEnable(argc, argv, cAtFalse, Listener(), De1ListFromString,
                                            CliAtPdhDe1Observer, CliAtPdhDe1ObserverDelete);
    }

eBool CmdPdhDe1FailureNotificationMaskSet(char argc, char **argv)
    {
    return CliAtSurEngineNotificationMaskSet(argc, argv,
                                             cAtPdhDe1AlarmStr, cAtPdhDe1AlarmVal, mCount(cAtPdhDe1AlarmVal),
                                             AtSurEngineFailureNotificationMaskSet,
                                             De1ListFromString);
    }

eBool CmdPdhDe1SurEngineShow(char argc, char **argv)
    {
    return CliAtSurEngineShow(argc, argv, De1ListFromString);
    }

eBool CmdPdhDe1FailureNotificationMaskShow(char argc, char **argv)
    {
    return CliAtSurEngingFailureNotificationMaskShow(argc, argv,
                                                     cAtPdhDe1AlarmUpperStr,
                                                     cAtPdhDe1AlarmVal,
                                                     mCount(cAtPdhDe1AlarmVal),
                                                     De1ListFromString);
    }

eBool CmdPdhDe1FailureShow(char argc, char **argv)
    {
    return CliAtSurEngineFailuresShow(argc, argv,
                                      cAtPdhDe1AlarmUpperStr,
                                      cAtPdhDe1AlarmVal,
                                      mCount(cAtPdhDe1AlarmVal),
                                      De1ListFromString);
    }

eBool CmdPdhDe1FailureHistoryShow(char argc, char **argv)
    {
    return CliAtSurEngineFailuresHistoryShow(argc, argv,
                                             cAtPdhDe1AlarmUpperStr,
                                             cAtPdhDe1AlarmVal,
                                             mCount(cAtPdhDe1AlarmVal),
                                             De1ListFromString);
    }

eBool CmdPdhDe1FailureCaptureShow(char argc, char **argv)
    {
    return CliAtSurEngineFailuresCaptureShow(argc, argv,
                                             cAtPdhDe1AlarmUpperStr,
                                             cAtPdhDe1AlarmVal,
                                             mCount(cAtPdhDe1AlarmVal),
                                             De1ListFromString,
                                             CliAtPdhDe1ObserverGet,
                                             CliAtPdhDe1ObserverDelete);
    }

eBool CmdPdhDe1PmParamCurrentPeriodRegisterShow(char argc, char **argv)
    {
    return RegistersShow(argc, argv, cAtPmCurrentPeriodRegister);
    }

eBool CmdPdhDe1PmParamPreviousPeriodRegisterShow(char argc, char **argv)
    {
    return RegistersShow(argc, argv, cAtPmPreviousPeriodRegister);
    }

eBool CmdPdhDe1PmParamCurrentDayRegisterShow(char argc, char **argv)
    {
    return RegistersShow(argc, argv, cAtPmCurrentDayRegister);
    }

eBool CmdPdhDe1PmParamPreviousDayRegisterShow(char argc, char **argv)
    {
    return RegistersShow(argc, argv, cAtPmPreviousDayRegister);
    }

eBool CmdPdhDe1PmParamRecentPeriodRegisterShow(char argc, char **argv)
    {
    return CliAtSurEnginePmParamRecentPeriodsShow(argc, argv,
                                                  cPdhDe1PmParamUpperStr,
                                                  cPdhDe1PmParamVal,
                                                  mCount(cPdhDe1PmParamVal),
                                                  De1ListFromString);
    }

static uint32 CliSurPdhDe1PmParamFromString(char *pStr)
    {
    uint32 paramType;
    eBool convertSuccess = cAtTrue;
    mAtStrToEnum(cPdhDe1PmParamStr, cPdhDe1PmParamVal,  pStr,  paramType, convertSuccess);
    if(!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Unknown type %s, expect: ", pStr);
        CliExpectedValuesPrint(cSevCritical, cPdhDe1PmParamStr, mCount(cPdhDe1PmParamVal));
        AtPrintc(cSevCritical, "\r\n");
        return 0;
        }

    return paramType;
    }

eBool CmdSurThresholProfileDe1Set(char argc, char **argv)
    {
    return CliSurThresholdProfileParamValueSet(argc, argv,
                                               CliSurPdhDe1PmParamFromString,
                                               (TcaThresholdFuncSet)AtPmThresholdProfilePdhDe1PeriodTcaThresholdSet);
    }

eBool CmdSurThresholProfileDe1Show(char argc, char **argv)
    {
    return CliSurThresholProfileShow(argc,
                                     argv,
                                     cPdhDe1PmParamUpperStr,
                                     cPdhDe1PmParamVal,
                                     mCount(cPdhDe1PmParamVal),
                                     (TcaThresholdFuncGet)AtPmThresholdProfilePdhDe1PeriodTcaThresholdGet);
    }

eBool CmdPdhDe1SurveillanceThresoldProfileSet(char argc, char **argv)
    {
    return CliAtSurEngineThresholdProfileSet(argc, argv, De1ListFromString);
    }

eBool CmdPdhDe1SurveillanceThresoldProfileShow(char argc, char **argv)
    {
    return CliAtSurEngineThresholdProfileShow(argc, argv, De1ListFromString);
    }

eBool CmdPdhDe1TcaNotificationMaskSet(char argc, char **argv)
    {
    return CliAtSurEngineNotificationMaskSet(argc, argv,
                                             cPdhDe1PmParamStr, cPdhDe1PmParamVal, mCount(cPdhDe1PmParamVal),
                                             AtSurEngineTcaNotificationMaskSet,
                                             De1ListFromString);
    }

eBool CmdPdhDe1TcaNotificationMaskShow(char argc, char **argv)
    {
    return CliAtSurEngingTcaNotificationMaskShow(argc, argv,
                                                  cPdhDe1PmParamUpperStr,
                                                  cPdhDe1PmParamVal,
                                                  mCount(cPdhDe1PmParamVal),
                                                  De1ListFromString);
    }

eBool CmdPdhDe1TcaHistoryShow(char argc, char **argv)
    {
    return CliAtSurEngineTcaHistoryShow(argc, argv,
                                        cPdhDe1PmParamUpperStr,
                                        cPdhDe1PmParamVal,
                                        mCount(cPdhDe1PmParamVal),
                                        De1ListFromString);
    }

eBool CmdPdhDe1TcaCaptureShow(char argc, char **argv)
    {
    return CliAtSurEngineTcaCaptureShow(argc, argv,
                                        cPdhDe1PmParamUpperStr,
                                        cPdhDe1PmParamVal,
                                        mCount(cPdhDe1PmParamVal),
                                        De1ListFromString,
                                        CliAtPdhDe1ObserverGet,
                                        CliAtPdhDe1ObserverDelete);
    }

eBool CmdPdhDe1PmParamPeriodThresholdShow(char argc, char **argv)
    {
    return CliAtSurEngineParamPeriodThresholdShow(argc, argv,
                                      cPdhDe1PmParamUpperStr,
                                      cPdhDe1PmParamVal,
                                      mCount(cPdhDe1PmParamVal),
                                      De1ListFromString);
    }

eBool CmdPdhDe1PmParamDayThresholdShow(char argc, char **argv)
    {
    return CliAtSurEngineParamDayThresholdShow(argc, argv,
                                      cPdhDe1PmParamUpperStr,
                                      cPdhDe1PmParamVal,
                                      mCount(cPdhDe1PmParamVal),
                                      De1ListFromString);
    }

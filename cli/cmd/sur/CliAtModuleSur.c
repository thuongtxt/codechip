/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2011 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : CliAtModuleSur.c
 *
 * Created Date: Mar 21, 2015
 *
 * Description :  Surveillance Module CLI implementation
 *
 * Notes       :
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "CliAtModuleSur.h"
#include "AtSurEngine.h"
#include "../sdh/CliAtModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/
#define cHeadingLenMax  32
#define cHeadingStrMax  30
#define sChannelStr "channel"

/*--------------------------- Macros-----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const uint32 cAtSurPeriodExpireMethodVal[] = {cAtSurPeriodExpireMethodManual,
                                                     cAtSurPeriodExpireMethodAuto};
static const char *cAtSurPeriodExpireMethodStr[] = {"manual", "auto"};

static const uint32 cAtSurTickSourceVal[] = {cAtSurTickSourceInternal, cAtSurTickSourceExternal};
static const char *cAtSurTickSourceStr[] = {"internal", "external"};

/*--------------------------- Forward Declare --------------------------------*/

/*--------------------------- Implements -----------------------------------------*/
static eBool DebugEnable(char argc, char **argv, eBool enable)
    {
    eAtRet ret;

    AtUnused(argc);
    AtUnused(argv);

    ret = AtModuleDebugEnable((AtModule)CliModuleSur(), enable);
    if (ret == cAtOk)
        return cAtTrue;

    AtPrintc(cSevCritical, "ERROR: %s debugging fail, ret = %s\r\n",
             enable ? "Enable" : "Disable", AtRet2String(ret));
    return cAtFalse;
    }

static eBool InterruptEnable(char argc, char **argv, eBool enable)
    {
    eAtRet ret = AtModuleInterruptEnable((AtModule)CliModuleSur(), enable);
    AtUnused(argv);
    AtUnused(argc);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "Cannot %s interrupt, ret = %s\n", enable ? "enable" : "disable", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

static eBool ExpireNotificationEnable(char argc, char **argv, eBool enable)
    {
    eAtRet ret = AtModuleSurExpireNotificationEnable(CliModuleSur(), enable);
    AtUnused(argv);
    AtUnused(argc);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "Cannot %s expire notification, ret = %s\n", enable ? "enable" : "disable", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

static eBool FailureTimerSet(char argc, char **argv, eAtRet (*TimerSetFunc)(AtModuleSur, uint32))
    {
    uint32 value = AtStrToDw(argv[0]);
    eAtRet ret = TimerSetFunc(CliModuleSur(), value);
    if (ret == cAtOk)
        return cAtTrue;

    AtUnused(argc);

    AtPrintc(cSevCritical, "ERROR: Changing timer fail with ret = %s\r\n", AtRet2String(ret));
    return cAtFalse;
    }

static uint32 ProfilesFromString(char *pStrIdList, AtPmThresholdProfile *profiles, uint32 bufferSize)
    {
    uint32 *idBuffer, numIds, i;
    uint32 numValidProfiles = 0;
    AtModuleSur moduleSur = CliModuleSur();

    if (pStrIdList == NULL)
        return 0;

    /* It is just a list of flat ID */
    idBuffer = CliSharedIdBufferGet(&numIds);
    numIds = CliIdListFromString(pStrIdList, idBuffer, numIds);
    if (numIds > bufferSize)
        numIds = bufferSize;

    /* Just have valid profile */
    for (i = 0; i < numIds; i++)
        {
        AtPmThresholdProfile profile = AtModuleSurThresholdProfileGet(moduleSur, idBuffer[i] - 1);
        if (profile)
            {
            profiles[numValidProfiles] = profile;
            numValidProfiles = numValidProfiles + 1;
            }
        else
            AtPrintc(cSevWarning,  "Profile %u does not exist\r\n", idBuffer[i]);
        }

    if (numValidProfiles == 0)
        {
        AtPrintc(cSevWarning,
                 "WARNING: No Profile is found with input %s, expected: 1 or 1-%u, ...\r\n",
                 pStrIdList,
                 AtModuleSurNumThresholdProfiles(moduleSur));
        }

    return numValidProfiles;
    }

static void ExpireNotify(AtModuleSur module, void* userContext)
    {
    AtUnused(module);
    AtUnused(userContext);
    AtPrintc(cSevNormal, "\r\n%s [SUR] Current period expired\r\n",
             AtOsalDateTimeInUsGet());
    AtPrintc(cSevNormal, "\r\n");
    }

static eBool InterruptCapture(char argc, char **argv, eBool enable)
    {
    static tAtModuleSurEventListener listener;
    eAtRet ret;
    AtUnused(argv);
    AtUnused(argc);

    listener.ExpireNotify = ExpireNotify;
    if (enable)
        ret = AtModuleEventListenerAdd((AtModule)CliModuleSur(), &listener, NULL);
    else
        ret = AtModuleEventListenerRemove((AtModule)CliModuleSur(), &listener);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "Cannot %s interrupt, ret = %s\n", enable ? "enable" : "disable", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

AtModuleSur CliModuleSur(void)
    {
    return (AtModuleSur)AtDeviceModuleGet(CliDevice(), cAtModuleSur);
    }

eBool CmdAtModuleSurDebugEnable(char argc, char **argv)
    {
    return DebugEnable(argc, argv, cAtTrue);
    }

eBool CmdAtModuleSurDebugDisable(char argc, char **argv)
    {
    return DebugEnable(argc, argv, cAtFalse);
    }

eBool CmdAtModuleSurDebug(char argc, char **argv)
    {
    eAtRet ret = AtModuleDebug((AtModule)CliModuleSur());
    AtUnused(argv);
    AtUnused(argc);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "Cannot show debugging, ret = %s\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdSurInterruptEnable(char argc, char **argv)
    {
    return InterruptEnable(argc, argv, cAtTrue);
    }

eBool CmdSurInterruptDisable(char argc, char **argv)
    {
    return InterruptEnable(argc, argv, cAtFalse);
    }

eBool CmdSurPeriodExpire(char argc, char **argv)
    {
    eAtRet ret = AtModuleSurPeriodExpire(CliModuleSur());
    if (ret == cAtOk)
        return cAtTrue;

    AtUnused(argc);
    AtUnused(argv);

    AtPrintc(cSevCritical, "ERROR: Handle period expire fail with ret = %s\r\n", AtRet2String(ret));
    return cAtFalse;
    }

eBool CmdSurPeriodAsyncExpire(char argc, char **argv)
    {
    eAtRet ret;

    AtUnused(argc);
    AtUnused(argv);

    ret = AtModuleSurPeriodAsyncExpire(CliModuleSur());
    if (ret == cAtOk)
        {
        AtPrintc(cSevInfo, "Period expiring is complete\r\n");
        return cAtTrue;
        }

    if (ret == cAtErrorAgain)
        {
        AtPrintc(cSevWarning, "Period expiring is in progress, please keep running this operation till success\r\n");
        return cAtTrue;
        }

    AtPrintc(cSevCritical, "ERROR: Handle period expire fail with ret = %s\r\n", AtRet2String(ret));
    return cAtFalse;
    }

eBool CmdAtModuleSurShow(char argc, char **argv)
    {
    tTab *tabPtr;
    uint32 row = 0;
    const char *heading[] = {"Attribute", "Value"};
    AtModuleSur surModule = CliModuleSur();
    uint32 numRows = 7;
    uint32 value;
    eBool enabled;
    const char *stringValue;

    AtUnused(argc);
    AtUnused(argv);

    /* Create table */
    tabPtr = TableAlloc(numRows, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    enabled = AtModuleInterruptIsEnabled((AtModule)surModule);
    StrToCell(tabPtr, row, 0, "Interrupt");
    StrToCell(tabPtr, row, 1, CliBoolToString(enabled));
    row++;

    value = AtModuleSurPeriodElapsedSecondsGet(surModule);
    StrToCell(tabPtr, row, 0, "Elapsed time (s)");
    StrToCell(tabPtr, row, 1, CliNumber2String(value, "%u"));
    row++;

    value = AtModuleSurFailureHoldOffTimerGet(surModule);
    StrToCell(tabPtr, row, 0, "Failure hold-off timer (ms)");
    StrToCell(tabPtr, row, 1, CliNumber2String(value, "%u"));
    row++;

    value = AtModuleSurFailureHoldOnTimerGet(surModule);
    StrToCell(tabPtr, row, 0, "Failure hold-on timer (ms)");
    StrToCell(tabPtr, row, 1, CliNumber2String(value, "%u"));
    row++;

    value = AtModuleSurExpireMethodGet(surModule);
    stringValue = CliEnumToString(value,
                                  cAtSurPeriodExpireMethodStr,
                                  cAtSurPeriodExpireMethodVal,
                                  mCount(cAtSurPeriodExpireMethodVal), NULL);
    StrToCell(tabPtr, row, 0, "Period expire method");
    ColorStrToCell(tabPtr, row, 1, stringValue ? stringValue : "error", stringValue ? cSevNormal : cSevCritical);
    row++;

    enabled = AtModuleSurExpireNotificationIsEnabled(surModule);
    StrToCell(tabPtr, row, 0, "Period expire notification");
    StrToCell(tabPtr, row, 1, CliBoolToString(enabled));
    row++;

    value = AtModuleSurTickSourceGet(surModule);
    stringValue = CliEnumToString(value,
                                  cAtSurTickSourceStr,
                                  cAtSurTickSourceVal,
                                  mCount(cAtSurTickSourceVal), NULL);
    StrToCell(tabPtr, row, 0, "Tick source");
    ColorStrToCell(tabPtr, row, 1, stringValue ? stringValue : "error", stringValue ? cSevNormal : cSevCritical);
    row++;

    /* Will help when number of rows need to be added */
    AtAssert(row == numRows);

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CliSurThresholdProfileParamValueSet(char argc, char **argv, ParamFromStringFuncGet paramGet, TcaThresholdFuncSet thresholdSet)
    {
    uint32 i;
    AtPmThresholdProfile *profiles;
    uint32 numProfiles;
    uint32 threshold;
    uint32 param;
    eBool success = cAtTrue;

    /* Get the shared ID buffer */
    AtUnused(argc);
    profiles = (AtPmThresholdProfile*) CliSharedChannelListGet(&numProfiles);
    numProfiles = ProfilesFromString(argv[0], profiles, numProfiles);
    if (numProfiles == 0)
          return cAtFalse;

    if ((param = paramGet(argv[1])) == 0)
        return cAtFalse;

    threshold = AtStrtoul(argv[2], NULL, 10);
    for (i = 0; i < numProfiles; i++)
        {
        AtPmThresholdProfile profile = profiles[i];
        eAtRet ret = thresholdSet(profile, paramGet(argv[1]), threshold);
        if (ret != cAtOk)
            {
            AtPrintc(cSevWarning,
                     "TCA Threshold for Profile %u cannot be set new threshold %u, ret = %s\r\n",
                     AtPmThresholdProfileIdGet(profile) + 1,
                     threshold,
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CliSurThresholProfileShow(char argc,
                                 char **argv,
                                 const char *pramaTypeStr[],
                                 const uint32 *paramTypeVal,
                                 uint32 numParams,
                                 TcaThresholdFuncGet thresholdGetFunc)
    {
    AtPmThresholdProfile *profiles;
    uint32 numProfiles;
    uint32 profile_i, param_i;
    tTab *tabPtr;
    char **pHeading;
    /* Get the shared ID buffer */
    AtUnused(argc);
    profiles = (AtPmThresholdProfile*) CliSharedChannelListGet(&numProfiles);
    numProfiles = ProfilesFromString(argv[0], profiles, numProfiles);
    if (numProfiles == 0)
          return cAtFalse;

    pHeading = CliSurTableHeadingCreate("profileId", pramaTypeStr, numParams + 1);
    if (pHeading == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Build table heading fail\r\n");
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numProfiles, numParams + 1, AtCast(const char **, pHeading));
    if (tabPtr == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        CliSurTableHeadingDelete(pHeading, numParams + 1);
        return cAtFalse;
        }

    /* Make table content */
    for (profile_i = 0; profile_i < numProfiles; profile_i = AtCliNextIdWithSleep(profile_i, AtCliLimitNumRestTdmChannelGet()))
        {
        uint32 threshold;
        AtPmThresholdProfile profile = profiles[profile_i];

        /* Put channel ID */
        StrToCell(tabPtr, profile_i, 0, CliNumber2String(AtPmThresholdProfileIdGet(profile) + 1, "%u"));

        /* Show threshold */
        for (param_i = 0; param_i < numParams; param_i++)
            {
            threshold = thresholdGetFunc(profile, paramTypeVal[param_i]);
            StrToCell(tabPtr, profile_i, param_i + 1, CliNumber2String(threshold, "%u"));
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    CliSurTableHeadingDelete(pHeading, numParams + 1);

    return cAtTrue;
    }

eBool CmdAtModuleSurFailureHoldOffTimerSet(char argc, char **argv)
    {
    return FailureTimerSet(argc, argv, AtModuleSurFailureHoldOffTimerSet);
    }

eBool CmdAtModuleSurFailureHoldOnTimerSet(char argc, char **argv)
    {
    return FailureTimerSet(argc, argv, AtModuleSurFailureHoldOnTimerSet);
    }

eBool CmdAtModuleSurExpireMethodSet(char argc, char **argv)
    {
    eAtRet ret;
    eBool convertSuccess = cAtFalse;
    eAtSurPeriodExpireMethod method;

    AtUnused(argc);

    /* Get method */
    method = CliStringToEnum(argv[0],
                             cAtSurPeriodExpireMethodStr,
                             cAtSurPeriodExpireMethodVal,
                             mCount(cAtSurPeriodExpireMethodVal),
                             &convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: please input valid methods. Expect: ");
        CliExpectedValuesPrint(cSevCritical, cAtSurPeriodExpireMethodStr, mCount(cAtSurPeriodExpireMethodVal));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    /* Apply */
    ret = AtModuleSurExpireMethodSet(CliModuleSur(), method);
    if (ret == cAtOk)
        return cAtTrue;

    AtPrintc(cSevCritical, "ERROR: Configure expire method fail with ret = %s\r\n", AtRet2String(ret));
    return cAtFalse;
    }

eBool CmdAtModuleSurTickSourceSet(char argc, char **argv)
    {
    eBool convertSuccess = cAtTrue;
    eAtSurTickSource source = CliStringToEnum(argv[0], cAtSurTickSourceStr, cAtSurTickSourceVal, mCount(cAtSurTickSourceVal), &convertSuccess);
    eAtRet ret;

    AtUnused(argc);

    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Please input valid tick source. Expect: ");
        CliExpectedValuesPrint(cSevCritical, cAtSurTickSourceStr, mCount(cAtSurTickSourceVal));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    ret = AtModuleSurTickSourceSet(CliModuleSur(), source);
    if (ret == cAtOk)
        return cAtTrue;

    AtPrintc(cSevCritical, "ERROR: Change tick source fail with ret = %s\r\n", AtRet2String(ret));
    return cAtFalse;
    }

eBool CmdAtModuleSurExpireNotificationEnable(char argc, char **argv)
    {
    return ExpireNotificationEnable(argc, argv, cAtTrue);
    }

eBool CmdAtModuleSurExpireNotificationDisable(char argc, char **argv)
    {
    return ExpireNotificationEnable(argc, argv, cAtFalse);
    }

eBool CmdSurInterruptCaptureEnable(char argc, char **argv)
    {
    return InterruptCapture(argc, argv, cAtTrue);
    }

eBool CmdSurInterruptCaptureDisable(char argc, char **argv)
    {
    return InterruptCapture(argc, argv, cAtFalse);
    }


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Sur-PW
 *
 * File        : CliAtSurPw.c
 *
 * Created Date: Mar 22, 2015
 *
 * Description :N/A
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDevice.h"
#include "AtCli.h"
#include "AtPw.h"
#include "CliAtModuleSur.h"
#include "CliAtSurEngine.h"

/*--------------------------- Define -----------------------------------------*/
static const uint32 cAtSurEnginePwPmParamVal[] =
    {
    cAtSurEnginePwPmParamEs,
    cAtSurEnginePwPmParamSes,
    cAtSurEnginePwPmParamUas,
    cAtSurEnginePwPmParamFeEs,
    cAtSurEnginePwPmParamFeSes,
    cAtSurEnginePwPmParamFeUas,
    cAtSurEnginePwPmParamFc
    };

static const char *cAtSurEnginePwPmParamUpperStr[] =
    {
    "ES", "SES", "UAS", "FE-ES", "FE-SES", "FE-UAS", "FC"
    };

static const char *cAtSurEnginePwPmParamStr[] =
    {
    "es", "ses", "uas", "fees", "feses", "feuas", "fc"
    };

static const uint32 cAtPwAlarmTypeVal[] =
    {
    cAtPwAlarmTypeLops,
    cAtPwAlarmTypeJitterBufferOverrun,
    cAtPwAlarmTypeJitterBufferUnderrun,
    cAtPwAlarmTypeRBit,
    cAtPwAlarmTypeLBit,
    cAtPwAlarmTypeStrayPacket,
    cAtPwAlarmTypeMalformedPacket,
    cAtPwAlarmTypeMissingPacket
    };

static const char *cAtPwAlarmTypeStr[] =
    {
    "lops",
    "overrun",
    "underrun",
    "rbit",
    "lbit",
    "stray",
    "malformed",
    "missing"
    };

static const char *cAtPwAlarmTypeUpperStr[] =
    {
     "lops",
     "jitterOverrun",
     "jitterUnderun",
     "RBit",
     "LBit",
     "StrayPkt",
     "MailformPkt",
     "MissingPkt"
    };

/*--------------------------- Macros -----------------------------------------*/
#define mParamCounter(paramName)    return (counters)->paramName;

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 PwsFromString(char *idString, AtChannel* channels, uint32 bufferSize)
    {
    uint32 numValidPws, numPws, pw_i;
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
    uint32 *idList = CliSharedIdBufferGet(&numPws);

    numPws = CliIdListFromString(idString, idList, numPws);
    numPws = (numPws > bufferSize) ? bufferSize : numPws;
    if (numPws == 0)
        return 0;

    /* Put all valid PWs */
    numValidPws = 0;
    for (pw_i = 0; pw_i < numPws; pw_i++)
        {
        AtPw pw = AtModulePwGetPw(pwModule, (uint32) (idList[pw_i] - 1));
        if (pw == NULL)
            continue;

        channels[numValidPws] = (AtChannel)pw;
        numValidPws = numValidPws + 1;
        }

    return numValidPws;
    }

static eBool Enable(char argc, char **argv, eBool enable, CliSurEnableFunc enableFunc)
    {
    return CliAtSurEngineEnable(argc, argv, enable, PwsFromString, enableFunc);
    }

static int32 PwParamCounterGet(void* allCounters, uint32 paramType)
    {
    tAtSurEnginePwPmCounters *counters = (tAtSurEnginePwPmCounters *)allCounters;
    switch (paramType)
        {
        case cAtSurEnginePwPmParamEs :
            mParamCounter(es)
        case cAtSurEnginePwPmParamSes:
            mParamCounter(ses)
        case cAtSurEnginePwPmParamUas:
            mParamCounter(uas)
        case cAtSurEnginePwPmParamFeEs :
            mParamCounter(esfe)
        case cAtSurEnginePwPmParamFeSes:
            mParamCounter(sesfe)
        case cAtSurEnginePwPmParamFeUas:
            mParamCounter(uasfe)
        case cAtSurEnginePwPmParamFc:
            mParamCounter(fc)
        default:
            return 0;
        }
    }

static eBool RegistersShow(char argc, char **argv, eAtPmRegisterType regType)
    {
    tAtSurEnginePwPmCounters counters;
    return CliAtSurEnginePmParamRegistersShow(argc, argv,
                                              cAtSurEnginePwPmParamUpperStr,
                                              cAtSurEnginePwPmParamVal,
                                              mCount(cAtSurEnginePwPmParamVal),
                                              (void *)&counters,
                                              regType,
                                              PwParamCounterGet,
                                              PwsFromString);
    }

static void FailureNotify(AtSurEngine surEngine,
                          void *listener,
                          uint32 failureType,
                          uint32 currentStatus)
    {
    CliAtSurEngineFailureNotify(surEngine,
                                listener,
                                failureType,
                                currentStatus,
                                cAtPwAlarmTypeUpperStr,
                                cAtPwAlarmTypeVal,
                                mCount(cAtPwAlarmTypeVal));
    }

static void TcaNotify(AtSurEngine surEngine,
                      void *listener,
                      AtPmParam param,
                      AtPmRegister pmRegister)
    {
    CliAtSurEngineTcaNotify(surEngine,
                            listener,
                            param,
                            pmRegister,
                            cAtSurEnginePwPmParamUpperStr,
                            cAtSurEnginePwPmParamVal,
                            mCount(cAtSurEnginePwPmParamVal));
    }

static void AutotestFailureChangeState(AtSurEngine engine, void *userData, uint32 changedAlarms, uint32 currentStatus)
    {
    AtChannelObserver observer = userData;
    AtUnused(engine);
    AtChannelObserverFailuresUpdate(observer, changedAlarms, currentStatus);
    }

static void AutotestTcaChangeState(AtSurEngine engine, void *userData, AtPmParam param, AtPmRegister pmRegister)
    {
    AtChannelObserver observer = userData;
    uint32 changedParams = AtPmParamTypeGet(param);
    AtUnused(engine);
    AtUnused(pmRegister);
    AtChannelObserverTcaUpdate(observer, changedParams);
    }

static tAtSurEngineListener *Listener(void)
    {
    static tAtSurEngineListener listener;
    static tAtSurEngineListener *pListener = NULL;

    if (pListener)
        return pListener;

    AtOsalMemInit(&listener, 0, sizeof(listener));
    if (AutotestIsEnabled())
        {
        listener.FailureNotify = AutotestFailureChangeState;
        listener.TcaNotify     = AutotestTcaChangeState;
        }
    else
        {
        listener.FailureNotify = FailureNotify;
        listener.TcaNotify     = TcaNotify;
        }

    pListener = &listener;
    return pListener;
    }

eBool CmdPwSurInit(char argc, char **argv)
    {
    return CliAtSurEngineInit(argc, argv, PwsFromString);
    }

eBool CmdPwSurEnable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue, AtSurEngineEnable);
    }

eBool CmdPwSurDisable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse, AtSurEngineEnable);
    }

eBool CmdPwSurPmEnable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue, AtSurEnginePmEnable);
    }

eBool CmdPwSurPmDisable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse, AtSurEnginePmEnable);
    }

eBool CmdPwFailureNotificationMaskSet(char argc, char **argv)
    {
    return CliAtSurEngineNotificationMaskSet(argc, argv,
                                             cAtPwAlarmTypeStr,
                                             cAtPwAlarmTypeVal,
                                             mCount(cAtPwAlarmTypeVal),
                                             AtSurEngineFailureNotificationMaskSet,
                                             PwsFromString);
    }

eBool CmdPwSurEngineShow(char argc, char **argv)
    {
    return CliAtSurEngineShow(argc, argv, PwsFromString);
    }

eBool CmdPwFailureNotificationMaskShow(char argc, char **argv)
    {
    return CliAtSurEngingFailureNotificationMaskShow(argc, argv,
                                                  cAtPwAlarmTypeUpperStr,
                                                  cAtPwAlarmTypeVal,
                                                  mCount(cAtPwAlarmTypeVal),
                                                  PwsFromString);
    }

eBool CmdPwFailureShow(char argc, char **argv)
    {
    return CliAtSurEngineFailuresShow(argc, argv,
                                      cAtPwAlarmTypeUpperStr,
                                      cAtPwAlarmTypeVal,
                                      mCount(cAtPwAlarmTypeVal),
                                      PwsFromString);
    }

eBool CmdPwFailureHistoryShow(char argc, char **argv)
    {
    return CliAtSurEngineFailuresHistoryShow(argc, argv,
                                             cAtPwAlarmTypeUpperStr,
                                             cAtPwAlarmTypeVal,
                                             mCount(cAtPwAlarmTypeVal),
                                             PwsFromString);
    }

eBool CmdPwFailureCaptureShow(char argc, char **argv)
    {
    return CliAtSurEngineFailuresCaptureShow(argc, argv,
                                             cAtPwAlarmTypeUpperStr,
                                             cAtPwAlarmTypeVal,
                                             mCount(cAtPwAlarmTypeVal),
                                             PwsFromString,
                                             CliAtPwObserverGet,
                                             CliAtPwObserverDelete);
    }

eBool CmdPwPmParamCurrentPeriodRegisterShow(char argc, char **argv)
    {
    return RegistersShow(argc, argv, cAtPmCurrentPeriodRegister);
    }

eBool CmdPwPmParamPreviousPeriodRegisterShow(char argc, char **argv)
    {
    return RegistersShow(argc, argv, cAtPmPreviousPeriodRegister);
    }

eBool CmdPwPmParamCurrentDayRegisterShow(char argc, char **argv)
    {
    return RegistersShow(argc, argv, cAtPmCurrentDayRegister);
    }

eBool CmdPwPmParamPreviousDayRegisterShow(char argc, char **argv)
    {
    return RegistersShow(argc, argv, cAtPmPreviousDayRegister);
    }

eBool CmdPwPmParamRecentPeriodRegisterShow(char argc, char **argv)
    {
    return CliAtSurEnginePmParamRecentPeriodsShow(argc, argv,
                                                  cAtSurEnginePwPmParamUpperStr,
                                                  cAtSurEnginePwPmParamVal,
                                                  mCount(cAtSurEnginePwPmParamVal),
                                                  PwsFromString);
    }

eBool CmdPwNotificationEnable(char argc, char **argv)
    {
    return CliAtSurEngineNotificationEnable(argc, argv, cAtTrue, Listener(), PwsFromString,
                                            CliAtPwObserverGet, CliAtPwObserverDelete);
    }

eBool CmdPwNotificationDisable(char argc, char **argv)
    {
    return CliAtSurEngineNotificationEnable(argc, argv, cAtFalse, Listener(), PwsFromString,
                                            CliAtPwObserver, CliAtPwObserverDelete);
    }

static uint32 CliSurPwPmParamFromString(char *pStr)
    {
    uint32 paramType;
    eBool convertSuccess = cAtTrue;
    mAtStrToEnum(cAtSurEnginePwPmParamStr, cAtSurEnginePwPmParamVal,  pStr,  paramType, convertSuccess);
    if(!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Unknown type %s, expect: ", pStr);
        CliExpectedValuesPrint(cSevCritical, cAtSurEnginePwPmParamStr, mCount(cAtSurEnginePwPmParamVal));
        AtPrintc(cSevCritical, "\r\n");
        return 0;
        }

    return paramType;
    }

uint32 CliTdmPwsFromString(char *idString, AtChannel* channels, uint32 bufferSize)
    {
    return PwsFromString(idString, channels, bufferSize);
    }

eBool CmdSurThresholProfilePwSet(char argc, char **argv)
    {
    return CliSurThresholdProfileParamValueSet(argc, argv,
                                               CliSurPwPmParamFromString,
                                               (TcaThresholdFuncSet)AtPmThresholdProfilePwPeriodTcaThresholdSet);
    }

eBool CmdSurThresholProfilePwShow(char argc, char **argv)
    {
    return CliSurThresholProfileShow(argc,
                                     argv,
                                     cAtSurEnginePwPmParamUpperStr,
                                     cAtSurEnginePwPmParamVal,
                                     mCount(cAtSurEnginePwPmParamVal),
                                     (TcaThresholdFuncGet)AtPmThresholdProfilePwPeriodTcaThresholdGet);
    }

eBool CmdPwSurveillanceThresoldProfileSet(char argc, char **argv)
    {
    return CliAtSurEngineThresholdProfileSet(argc, argv, PwsFromString);
    }

eBool CmdPwSurveillanceThresoldProfileShow(char argc, char **argv)
    {
    return CliAtSurEngineThresholdProfileShow(argc, argv, PwsFromString);
    }

eBool CmdPwTcaNotificationMaskSet(char argc, char **argv)
    {
    return CliAtSurEngineNotificationMaskSet(argc, argv,
                                             cAtSurEnginePwPmParamStr,
                                             cAtSurEnginePwPmParamVal,
                                             mCount(cAtSurEnginePwPmParamVal),
                                             AtSurEngineTcaNotificationMaskSet,
                                             PwsFromString);
    }

eBool CmdPwTcaNotificationMaskShow(char argc, char **argv)
    {
    return CliAtSurEngingTcaNotificationMaskShow(argc, argv,
                                                 cAtSurEnginePwPmParamUpperStr,
                                                  cAtSurEnginePwPmParamVal,
                                                  mCount(cAtSurEnginePwPmParamVal),
                                                  PwsFromString);
    }

eBool CmdPwTcaHistoryShow(char argc, char **argv)
    {
    return CliAtSurEngineTcaHistoryShow(argc, argv,
                                        cAtSurEnginePwPmParamUpperStr,
                                        cAtSurEnginePwPmParamVal,
                                        mCount(cAtSurEnginePwPmParamVal),
                                        PwsFromString);
    }

eBool CmdPwTcaCaptureShow(char argc, char **argv)
    {
    return CliAtSurEngineTcaCaptureShow(argc, argv,
                                        cAtSurEnginePwPmParamUpperStr,
                                        cAtSurEnginePwPmParamVal,
                                        mCount(cAtSurEnginePwPmParamVal),
                                        PwsFromString,
                                        CliAtPwObserverGet,
                                        CliAtPwObserverDelete);
    }

eBool CmdPwPmParamPeriodThresholdShow(char argc, char **argv)
    {
    return CliAtSurEngineParamPeriodThresholdShow(argc, argv,
                                      cAtSurEnginePwPmParamUpperStr,
                                      cAtSurEnginePwPmParamVal,
                                      mCount(cAtSurEnginePwPmParamVal),
                                      PwsFromString);
    }

eBool CmdPwPmParamDayThresholdShow(char argc, char **argv)
    {
    return CliAtSurEngineParamDayThresholdShow(argc, argv,
                                      cAtSurEnginePwPmParamUpperStr,
                                      cAtSurEnginePwPmParamVal,
                                      mCount(cAtSurEnginePwPmParamVal),
                                      PwsFromString);
    }

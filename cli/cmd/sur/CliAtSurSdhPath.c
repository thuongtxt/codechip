/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Sur SDH-PATH
 *
 * File        : CliAtSurSdhPath.c
 *
 * Created Date: Mar 22, 2015
 *
 * Description :N/A
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../sdh/CliAtModuleSdh.h"
#include "AtCli.h"
#include "AtDevice.h"
#include "AtModuleSdh.h"
#include "AtSdhPath.h"
#include "CliAtModuleSur.h"
#include "CliAtSurEngine.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mParamCounter(paramName)    return (counters)->paramName;

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char *cAtSdhPathAlarmTypeStr[] =
    {
    "ais",
    "lop",
    "tim",
    "uneq",
    "plm",
    "ber-sd",
    "ber-sf",
    "lom",
    "rfi",
    "rfi-s",
    "rfi-c",
    "rfi-p"
    };

static const char *cAtSdhPathAlarmTypeUpperStr[] =
    {
    "AIS",
    "LOP",
    "TIM",
    "UNEQ",
    "PLM",
    "BER-SD",
    "BER-SF",
    "LOM",
    "RFI",
    "RFI-S",
    "RFI-C",
    "RFI-P"
    };

static const uint32 cAtSdhPathAlarmTypeVal[] =
    {
    cAtSdhPathAlarmAis,
    cAtSdhPathAlarmLop,
    cAtSdhPathAlarmTim,
    cAtSdhPathAlarmUneq,
    cAtSdhPathAlarmPlm,
    cAtSdhPathAlarmBerSd,
    cAtSdhPathAlarmBerSf,
    cAtSdhPathAlarmLom,
    cAtSdhPathAlarmRfi,
    cAtSdhPathAlarmRfiS,
    cAtSdhPathAlarmRfiC,
    cAtSdhPathAlarmRfiP
    };

static const uint32 cAtSurEngineSdhPathPmParamVal[] =
    {
     cAtSurEngineSdhPathPmParamPpjcPdet,
     cAtSurEngineSdhPathPmParamNpjcPdet,
     cAtSurEngineSdhPathPmParamPpjcPgen,
     cAtSurEngineSdhPathPmParamNpjcPgen,
     cAtSurEngineSdhPathPmParamPjcDiff,
     cAtSurEngineSdhPathPmParamPjcsPdet,
     cAtSurEngineSdhPathPmParamPjcsPgen,
     cAtSurEngineSdhPathPmParamCvP,
     cAtSurEngineSdhPathPmParamEsP,
     cAtSurEngineSdhPathPmParamSesP,
     cAtSurEngineSdhPathPmParamUasP,
     cAtSurEngineSdhPathPmParamFcP,
     cAtSurEngineSdhPathPmParamCvPfe,
     cAtSurEngineSdhPathPmParamEsPfe,
     cAtSurEngineSdhPathPmParamSesPfe,
     cAtSurEngineSdhPathPmParamUasPfe,
     cAtSurEngineSdhPathPmParamFcPfe,
     cAtSurEngineSdhPathPmParamEfsP,
     cAtSurEngineSdhPathPmParamEfsPfe,
     cAtSurEngineSdhPathPmParamAsP,
     cAtSurEngineSdhPathPmParamAsPfe,
     cAtSurEngineSdhPathPmParamEbP,
     cAtSurEngineSdhPathPmParamBbeP,
     cAtSurEngineSdhPathPmParamEsrP,
     cAtSurEngineSdhPathPmParamSesrP,
     cAtSurEngineSdhPathPmParamBberP
    };

static const char *cAtSurEngineSdhPathPmParamUpperStr[] =
    {
    "PPJC-PDet",
    "NPJC-PDet",
    "PPJC-PGen",
    "NPJC-PGen",
    "PJC-Diff",
    "PJCS-PDet",
    "PJCS-PGen",
    "CV-P",
    "ES-P",
    "SES-P",
    "UAS-P",
    "FC-P",
    "CV-PFE",
    "ES-PFE",
    "SES-PFE",
    "UAS-PFE",
    "FC-PFE",
    "EFS-P",
    "EFS-PFE",
    "AS-P",
    "AS-PFE",
    "EB-P",
    "BBE-P",
    "ESR-P",
    "SESR-P",
    "BBER-P"
    };

static const char *cAtSurEngineSdhPathPmParamStr[] =
    {
    "ppjcpdet",
    "npjcpdet",
    "ppjcpgen",
    "npjcpgen",
    "pjcdiff",
    "pjcspdet",
    "pjcspgen",
    "cvp",
    "esp",
    "sesp",
    "uasp",
    "fcp",
    "cvpfe",
    "espfe",
    "sespfe",
    "uaspfe",
    "fcpfe",
    "efsp",
    "efspfe",
    "asp",
    "aspfe",
    "ebp",
    "bbep",
    "esrp",
    "sesrp",
    "bberp"
    };

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool Enable(char argc, char **argv, eBool enable, CliSurEnableFunc enableFunc)
    {
    return CliAtSurEngineEnable(argc, argv, enable, CliAtModuleSdhChanelsFromString, enableFunc);
    }

static int32 PathParamCounterGet(void* allCounters, uint32 paramType)
    {
    tAtSurEngineSdhPathPmCounters *counters = (tAtSurEngineSdhPathPmCounters *)allCounters;
    switch (paramType)
        {
        case cAtSurEngineSdhPathPmParamPpjcPdet:
            mParamCounter(ppjcPdet)
        case cAtSurEngineSdhPathPmParamNpjcPdet:
            mParamCounter(npjcPdet)
        case cAtSurEngineSdhPathPmParamPpjcPgen:
            mParamCounter(ppjcPgen)
        case cAtSurEngineSdhPathPmParamNpjcPgen:
            mParamCounter(npjcPgen)
        case cAtSurEngineSdhPathPmParamPjcDiff:
            mParamCounter(pjcDiff)
        case cAtSurEngineSdhPathPmParamPjcsPdet:
            mParamCounter(pjcsPdet)
        case cAtSurEngineSdhPathPmParamPjcsPgen:
            mParamCounter(pjcsPgen)
        case cAtSurEngineSdhPathPmParamCvP:
            mParamCounter(cvP)
        case cAtSurEngineSdhPathPmParamEsP:
            mParamCounter(esP)
        case cAtSurEngineSdhPathPmParamSesP:
            mParamCounter(sesP)
        case cAtSurEngineSdhPathPmParamUasP:
            mParamCounter(uasP)
        case cAtSurEngineSdhPathPmParamFcP:
            mParamCounter(fcP)
        case cAtSurEngineSdhPathPmParamCvPfe:
            mParamCounter(cvPfe)
        case cAtSurEngineSdhPathPmParamEsPfe:
            mParamCounter(esPfe)
        case cAtSurEngineSdhPathPmParamSesPfe:
            mParamCounter(sesPfe)
        case cAtSurEngineSdhPathPmParamUasPfe:
            mParamCounter(uasPfe)
        case cAtSurEngineSdhPathPmParamFcPfe:
            mParamCounter(fcPfe)
        case cAtSurEngineSdhPathPmParamEfsP:
            mParamCounter(efsP)
        case cAtSurEngineSdhPathPmParamEfsPfe:
            mParamCounter(efsPfe)
        case cAtSurEngineSdhPathPmParamAsP:
            mParamCounter(asP)
        case cAtSurEngineSdhPathPmParamAsPfe:
            mParamCounter(asPfe)
        case cAtSurEngineSdhPathPmParamEbP:
            mParamCounter(ebP)
        case cAtSurEngineSdhPathPmParamBbeP:
            mParamCounter(bbeP)
        case cAtSurEngineSdhPathPmParamEsrP:
            mParamCounter(esrP)
        case cAtSurEngineSdhPathPmParamSesrP:
            mParamCounter(sesrP)
        case cAtSurEngineSdhPathPmParamBberP:
            mParamCounter(bberP)
        default:
            return 0;
        }
    }

static eBool RegistersShow(char argc, char **argv, eAtPmRegisterType regType)
    {
    tAtSurEngineSdhPathPmCounters counters;
    return CliAtSurEnginePmParamRegistersShow(argc, argv,
                                              cAtSurEngineSdhPathPmParamUpperStr,
                                              cAtSurEngineSdhPathPmParamVal,
                                              mCount(cAtSurEngineSdhPathPmParamVal),
                                              (void *)&counters,
                                              regType,
                                              PathParamCounterGet,
                                              CliAtModuleSdhChanelsFromString);
    }

static void FailureNotify(AtSurEngine surEngine,
                          void *listener,
                          uint32 failureType,
                          uint32 currentStatus)
    {
    CliAtSurEngineFailureNotify(surEngine,
                                listener,
                                failureType,
                                currentStatus,
                                cAtSdhPathAlarmTypeUpperStr,
                                cAtSdhPathAlarmTypeVal,
                                mCount(cAtSdhPathAlarmTypeVal));
    }

static void TcaNotify(AtSurEngine surEngine,
                      void *listener,
                      AtPmParam param,
                      AtPmRegister pmRegister)
    {
    CliAtSurEngineTcaNotify(surEngine,
                            listener,
                            param,
                            pmRegister,
                            cAtSurEngineSdhPathPmParamUpperStr,
                            cAtSurEngineSdhPathPmParamVal,
                            mCount(cAtSurEngineSdhPathPmParamVal));
    }

static void AutotestFailureChangeState(AtSurEngine engine, void *userData, uint32 changedAlarms, uint32 currentStatus)
    {
    AtChannelObserver observer = userData;
    AtUnused(engine);
    AtChannelObserverFailuresUpdate(observer, changedAlarms, currentStatus);
    }

static void AutotestTcaChangeState(AtSurEngine engine, void *userData, AtPmParam param, AtPmRegister pmRegister)
    {
    AtChannelObserver observer = userData;
    uint32 changedParams = AtPmParamTypeGet(param);
    AtUnused(engine);
    AtUnused(pmRegister);
    AtChannelObserverTcaUpdate(observer, changedParams);
    }

static tAtSurEngineListener *Listener(void)
    {
    static tAtSurEngineListener listener;
    static tAtSurEngineListener *pListener = NULL;

    if (pListener)
        return pListener;

    AtOsalMemInit(&listener, 0, sizeof(listener));
    if (AutotestIsEnabled())
        {
        listener.FailureNotify = AutotestFailureChangeState;
        listener.TcaNotify     = AutotestTcaChangeState;
        }
    else
        {
        listener.FailureNotify = FailureNotify;
        listener.TcaNotify     = TcaNotify;
        }

    pListener = &listener;
    return pListener;
    }

const char **CliAtSdhPathFailureTypeStr(uint32 *numFailures)
    {
    *numFailures = mCount(cAtSdhPathAlarmTypeStr);
    return cAtSdhPathAlarmTypeStr;
    }

const uint32 *CliAtSdhPathFailureTypeVal(uint32 *numFailures)
    {
    *numFailures = mCount(cAtSdhPathAlarmTypeVal);
    return cAtSdhPathAlarmTypeVal;
    }

eBool CmdSdhPathSurInit(char argc, char **argv)
    {
    return CliAtSurEngineInit(argc, argv, CliAtModuleSdhChanelsFromString);
    }

eBool CmdSdhPathSurEnable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue, AtSurEngineEnable);
    }

eBool CmdSdhPathSurDisable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse, AtSurEngineEnable);
    }

eBool CmdSdhPathSurPmEnable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue, AtSurEnginePmEnable);
    }

eBool CmdSdhPathSurPmDisable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse, AtSurEnginePmEnable);
    }

eBool CmdSdhPathNotificationEnable(char argc, char **argv)
    {
    return CliAtSurEngineNotificationEnable(argc, argv, cAtTrue, Listener(), CliAtModuleSdhChanelsFromString,
                                            CliAtSdhPathObserverGet, CliAtSdhPathObserverDelete);
    }

eBool CmdSdhPathNotificationDisable(char argc, char **argv)
    {
    return CliAtSurEngineNotificationEnable(argc, argv, cAtFalse, Listener(), CliAtModuleSdhChanelsFromString,
                                            CliAtSdhPathObserver, CliAtSdhPathObserverDelete);
    }

eBool CmdSdhPathPmParamCurrentPeriodRegisterShow(char argc, char **argv)
    {
    return RegistersShow(argc, argv, cAtPmCurrentPeriodRegister);
    }

eBool CmdSdhPathPmParamPreviousPeriodRegisterShow(char argc, char **argv)
    {
    return RegistersShow(argc, argv, cAtPmPreviousPeriodRegister);
    }

eBool CmdSdhPathPmParamCurrentDayRegisterShow(char argc, char **argv)
    {
    return RegistersShow(argc, argv, cAtPmCurrentDayRegister);
    }

eBool CmdSdhPathPmParamPreviousDayRegisterShow(char argc, char **argv)
    {
    return RegistersShow(argc, argv, cAtPmPreviousDayRegister);
    }

eBool CmdSdhPathPmParamRecentPeriodRegisterShow(char argc, char **argv)
    {
    return CliAtSurEnginePmParamRecentPeriodsShow(argc, argv,
                                                  cAtSurEngineSdhPathPmParamUpperStr,
                                                  cAtSurEngineSdhPathPmParamVal,
                                                  mCount(cAtSurEngineSdhPathPmParamVal),
                                                  CliAtModuleSdhChanelsFromString);
    }

eBool CmdSdhPathFailureNotificationMaskSet(char argc, char **argv)
    {
    return CliAtSurEngineNotificationMaskSet(argc, argv,
                                             cAtSdhPathAlarmTypeStr,
                                             cAtSdhPathAlarmTypeVal,
                                             mCount(cAtSdhPathAlarmTypeVal),
                                             AtSurEngineFailureNotificationMaskSet,
                                             CliAtModuleSdhChanelsFromString);
    }

eBool CmdSdhPathSurEngineShow(char argc, char **argv)
    {
    return CliAtSurEngineShow(argc, argv, CliAtModuleSdhChanelsFromString);
    }

eBool CmdSdhPathFailureNotificationMaskShow(char argc, char **argv)
    {
    return CliAtSurEngingFailureNotificationMaskShow(argc, argv,
                                                  cAtSdhPathAlarmTypeUpperStr,
                                                  cAtSdhPathAlarmTypeVal,
                                                  mCount(cAtSdhPathAlarmTypeVal),
                                                  CliAtModuleSdhChanelsFromString);
    }

eBool CmdSdhPathFailureShow(char argc, char **argv)
    {
    return CliAtSurEngineFailuresShow(argc, argv,
                                      cAtSdhPathAlarmTypeUpperStr,
                                      cAtSdhPathAlarmTypeVal,
                                      mCount(cAtSdhPathAlarmTypeVal),
                                      CliAtModuleSdhChanelsFromString);
    }

eBool CmdSdhPathFailureHistoryShow(char argc, char **argv)
    {
    return CliAtSurEngineFailuresHistoryShow(argc, argv,
                                             cAtSdhPathAlarmTypeUpperStr,
                                             cAtSdhPathAlarmTypeVal,
                                             mCount(cAtSdhPathAlarmTypeVal),
                                             CliAtModuleSdhChanelsFromString);
    }

static uint32 CliSurSdhPathPmParamFromString(char *pStr)
    {
    uint32 paramType;
    eBool convertSuccess = cAtTrue;
    mAtStrToEnum(cAtSurEngineSdhPathPmParamStr, cAtSurEngineSdhPathPmParamVal,  pStr,  paramType, convertSuccess);
    if(!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Unknown type %s, expect: ", pStr);
        CliExpectedValuesPrint(cSevCritical, cAtSurEngineSdhPathPmParamStr, mCount(cAtSurEngineSdhPathPmParamVal));
        AtPrintc(cSevCritical, "\r\n");
        return 0;
        }

    return paramType;
    }

eBool CmdSurThresholProfileHoPathSet(char argc, char **argv)
    {
    return CliSurThresholdProfileParamValueSet(argc, argv, (ParamFromStringFuncGet)CliSurSdhPathPmParamFromString, (TcaThresholdFuncSet)AtPmThresholdProfileSdhHoPathPeriodTcaThresholdSet);
    }

eBool CmdSurThresholProfileLoPathSet(char argc, char **argv)
    {
    return CliSurThresholdProfileParamValueSet(argc, argv,
                                               CliSurSdhPathPmParamFromString,
                                               (TcaThresholdFuncSet)AtPmThresholdProfileSdhLoPathPeriodTcaThresholdSet);
    }

eBool CmdSurThresholProfileHoPathShow(char argc, char **argv)
    {
    return CliSurThresholProfileShow(argc,
                                     argv,
                                     cAtSurEngineSdhPathPmParamUpperStr,
                                     cAtSurEngineSdhPathPmParamVal,
                                     mCount(cAtSurEngineSdhPathPmParamVal),
                                     (TcaThresholdFuncGet)AtPmThresholdProfileSdhHoPathPeriodTcaThresholdGet);
    }

eBool CmdSurThresholProfileLoPathShow(char argc, char **argv)
    {
    return CliSurThresholProfileShow(argc,
                                     argv,
                                     cAtSurEngineSdhPathPmParamUpperStr,
                                     cAtSurEngineSdhPathPmParamVal,
                                     mCount(cAtSurEngineSdhPathPmParamVal),
                                     (TcaThresholdFuncGet)AtPmThresholdProfileSdhLoPathPeriodTcaThresholdGet);
    }

eBool CmdSdhPathSurveillanceThresoldProfileSet(char argc, char **argv)
    {
    return CliAtSurEngineThresholdProfileSet(argc, argv, CliAtModuleSdhChanelsFromString);
    }

eBool CmdSdhPathSurveillanceThresoldProfileShow(char argc, char **argv)
    {
    return CliAtSurEngineThresholdProfileShow(argc, argv, CliAtModuleSdhChanelsFromString);
    }

eBool CmdSdhPathTcaNotificationMaskSet(char argc, char **argv)
    {
    return CliAtSurEngineNotificationMaskSet(argc, argv,
                                             cAtSurEngineSdhPathPmParamStr,
                                             cAtSurEngineSdhPathPmParamVal,
                                             mCount(cAtSurEngineSdhPathPmParamVal),
                                             AtSurEngineTcaNotificationMaskSet,
                                             CliAtModuleSdhChanelsFromString);
    }

eBool CmdSdhPathTcaNotificationMaskShow(char argc, char **argv)
    {
    return CliAtSurEngingTcaNotificationMaskShow(argc, argv,
                                                 cAtSurEngineSdhPathPmParamUpperStr,
                                                 cAtSurEngineSdhPathPmParamVal,
                                                  mCount(cAtSurEngineSdhPathPmParamVal),
                                                  CliAtModuleSdhChanelsFromString);
    }

eBool CmdSdhPathTcaHistoryShow(char argc, char **argv)
    {
    return CliAtSurEngineTcaHistoryShow(argc, argv,
                                        cAtSurEngineSdhPathPmParamUpperStr,
                                        cAtSurEngineSdhPathPmParamVal,
                                        mCount(cAtSurEngineSdhPathPmParamVal),
                                        CliAtModuleSdhChanelsFromString);
    }

eBool CmdSdhPathPmParamPeriodThresholdShow(char argc, char **argv)
    {
    return CliAtSurEngineParamPeriodThresholdShow(argc, argv,
                                      cAtSurEngineSdhPathPmParamUpperStr,
                                      cAtSurEngineSdhPathPmParamVal,
                                      mCount(cAtSurEngineSdhPathPmParamVal),
                                      CliAtModuleSdhChanelsFromString);
    }

eBool CmdSdhPathPmParamDayThresholdShow(char argc, char **argv)
    {
    return CliAtSurEngineParamDayThresholdShow(argc, argv,
                                      cAtSurEngineSdhPathPmParamUpperStr,
                                      cAtSurEngineSdhPathPmParamVal,
                                      mCount(cAtSurEngineSdhPathPmParamVal),
                                      CliAtModuleSdhChanelsFromString);
    }

eBool CmdSdhPathFailureCaptureShow(char argc, char **argv)
    {
    return CliAtSurEngineFailuresCaptureShow(argc, argv,
                                             cAtSdhPathAlarmTypeUpperStr,
                                             cAtSdhPathAlarmTypeVal,
                                             mCount(cAtSdhPathAlarmTypeVal),
                                             CliAtModuleSdhChanelsFromString,
                                             CliAtSdhPathObserverGet,
                                             CliAtSdhPathObserverDelete);
    }

eBool CmdSdhPathTcaCaptureShow(char argc, char **argv)
    {
    return CliAtSurEngineTcaCaptureShow(argc, argv,
                                        cAtSurEngineSdhPathPmParamUpperStr,
                                        cAtSurEngineSdhPathPmParamVal,
                                        mCount(cAtSurEngineSdhPathPmParamVal),
                                        CliAtModuleSdhChanelsFromString,
                                        CliAtSdhPathObserverGet,
                                        CliAtSdhPathObserverDelete);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Sur-Sdh-Line
 *
 * File        : CliAtSurSdhLine.c
 *
 * Created Date: Mar 22, 2015
 *
 * Description :N/A
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "AtModuleSdh.h"
#include "AtSdhLine.h"
#include "CliAtModuleSur.h"
#include "CliAtSurEngine.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mParamCounter(paramName)    return (counters)->paramName;

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const uint32 cAtSurEngineSdhLinePmParamVal[] =
    {
    cAtSurEngineSdhLinePmParamSefsS,
    cAtSurEngineSdhLinePmParamCvS,
    cAtSurEngineSdhLinePmParamEsS,
    cAtSurEngineSdhLinePmParamSesS,
    cAtSurEngineSdhLinePmParamCvL,
    cAtSurEngineSdhLinePmParamEsL,
    cAtSurEngineSdhLinePmParamSesL,
    cAtSurEngineSdhLinePmParamUasL,
    cAtSurEngineSdhLinePmParamFcL,
    cAtSurEngineSdhLinePmParamCvLfe,
    cAtSurEngineSdhLinePmParamEsLfe,
    cAtSurEngineSdhLinePmParamSesLfe,
    cAtSurEngineSdhLinePmParamUasLfe,
    cAtSurEngineSdhLinePmParamFcLfe
    };

static const char *cAtSurEngineSdhLinePmParamUpperStr[] =
    {
    "SEFS-S",
    "CV-S",
    "ES-S",
    "SES-S",
    "CV-L",
    "ES-L",
    "SES-L",
    "UAS-L",
    "FC-L",
    "CV-LFE",
    "ES-LFE",
    "SES-LFE",
    "UAS-LFE",
    "FC-LFE"
    };

static const char *cAtSurEngineSdhLinePmParamStr[] =
    {
    "sefss",
    "cvs",
    "ess",
    "sess",
    "cvl",
    "esl",
    "sesl",
    "uasl",
    "fcl",
    "cvlfe",
    "eslfe",
    "seslfe",
    "uaslfe",
    "fclfe"
    };

static const char *cAtSdhLineAlarmTypeStr[] =
    {
    "los",
    "lof",
    "tim",
    "ais",
    "rfi",
    "ber-sd",
    "ber-sf",
    };

static const char *cAtSdhLineAlarmTypeUpperStr[] =
    {
    "LOS",
    "LOF",
    "TIM",
    "AIS",
    "RFI",
    "BER-SD",
    "BER-SF",
    };

static const uint32 cAtSdhLineAlarmTypeVal[] =
    {
    cAtSdhLineAlarmLos,
    cAtSdhLineAlarmLof,
    cAtSdhLineAlarmTim,
    cAtSdhLineAlarmAis,
    cAtSdhLineAlarmRfi,
    cAtSdhLineAlarmBerSd,
    cAtSdhLineAlarmBerSf,
    };

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 LinesFromString(char *pStrIdList, AtChannel *channels, uint32 channelBufferSize)
    {
    return CliAtSdhLinesFromString(pStrIdList, channels, channelBufferSize);
    }

static eBool Enable(char argc, char **argv, eBool enable, CliSurEnableFunc enableFunc)
    {
    return CliAtSurEngineEnable(argc, argv, enable, LinesFromString, enableFunc);
    }

static int32 LineParamCounterGet(void* allCounters, uint32 paramType)
    {
    tAtSurEngineSdhLinePmCounters *counters = (tAtSurEngineSdhLinePmCounters *)allCounters;
    switch (paramType)
        {
        case cAtSurEngineSdhLinePmParamSefsS :
            mParamCounter(sefsS)
        case cAtSurEngineSdhLinePmParamCvS   :
            mParamCounter(cvS)
        case cAtSurEngineSdhLinePmParamEsS   :
            mParamCounter(esS)
        case cAtSurEngineSdhLinePmParamSesS  :
            mParamCounter(sesS)
        case cAtSurEngineSdhLinePmParamCvL   :
            mParamCounter(cvL)
        case cAtSurEngineSdhLinePmParamEsL   :
            mParamCounter(esL)
        case cAtSurEngineSdhLinePmParamSesL  :
            mParamCounter(sesL)
        case cAtSurEngineSdhLinePmParamUasL  :
            mParamCounter(uasL)
        case cAtSurEngineSdhLinePmParamFcL   :
            mParamCounter(fcL)
        case cAtSurEngineSdhLinePmParamCvLfe :
            mParamCounter(cvLfe)
        case cAtSurEngineSdhLinePmParamEsLfe :
            mParamCounter(esLfe)
        case cAtSurEngineSdhLinePmParamSesLfe:
            mParamCounter(sesLfe)
        case cAtSurEngineSdhLinePmParamUasLfe:
            mParamCounter(uasLfe)
        case cAtSurEngineSdhLinePmParamFcLfe :
            mParamCounter(fcLfe)
        default:
            return 0;
        }
    }

static eBool RegistersShow(char argc, char **argv, eAtPmRegisterType regType)
    {
    tAtSurEngineSdhLinePmCounters counters;
    return CliAtSurEnginePmParamRegistersShow(argc, argv,
                                      cAtSurEngineSdhLinePmParamUpperStr,
                                      cAtSurEngineSdhLinePmParamVal,
                                      mCount(cAtSurEngineSdhLinePmParamVal),
                                      (void *)&counters,
                                      regType,
                                      LineParamCounterGet,
                                      LinesFromString);
    }

static void FailureNotify(AtSurEngine surEngine,
                          void *listener,
                          uint32 failureType,
                          uint32 currentStatus)
    {
    CliAtSurEngineFailureNotify(surEngine,
                                listener,
                                failureType,
                                currentStatus,
                                cAtSdhLineAlarmTypeUpperStr,
                                cAtSdhLineAlarmTypeVal,
                                mCount(cAtSdhLineAlarmTypeVal));
    }

static void TcaNotify(AtSurEngine surEngine,
                      void *listener,
                      AtPmParam param,
                      AtPmRegister pmRegister)
    {
    CliAtSurEngineTcaNotify(surEngine,
                            listener,
                            param,
                            pmRegister,
                            cAtSurEngineSdhLinePmParamUpperStr,
                            cAtSurEngineSdhLinePmParamVal,
                            mCount(cAtSurEngineSdhLinePmParamVal));
    }

static void AutotestFailureChangeState(AtSurEngine engine, void *userData, uint32 changedAlarms, uint32 currentStatus)
    {
    AtChannelObserver observer = userData;
    AtUnused(engine);
    AtChannelObserverFailuresUpdate(observer, changedAlarms, currentStatus);
    }

static void AutotestTcaChangeState(AtSurEngine engine, void *userData, AtPmParam param, AtPmRegister pmRegister)
    {
    AtChannelObserver observer = userData;
    uint32 changedParams = AtPmParamTypeGet(param);
    AtUnused(engine);
    AtUnused(pmRegister);
    AtChannelObserverTcaUpdate(observer, changedParams);
    }

static tAtSurEngineListener *Listener(void)
    {
    static tAtSurEngineListener listener;
    static tAtSurEngineListener *pListener = NULL;

    if (pListener)
        return pListener;

    AtOsalMemInit(&listener, 0, sizeof(listener));
    if (AutotestIsEnabled())
        {
        listener.FailureNotify = AutotestFailureChangeState;
        listener.TcaNotify     = AutotestTcaChangeState;
        }
    else
        {
        listener.FailureNotify = FailureNotify;
        listener.TcaNotify     = TcaNotify;
        }

    pListener = &listener;
    return pListener;
    }

const char **CliAtSdhLineFailureTypeStr(uint32 *numFailures)
    {
    *numFailures = mCount(cAtSdhLineAlarmTypeStr);
    return cAtSdhLineAlarmTypeStr;
    }

const uint32 *CliAtSdhLineFailureTypeVal(uint32 *numFailures)
    {
    *numFailures = mCount(cAtSdhLineAlarmTypeVal);
    return cAtSdhLineAlarmTypeVal;
    }

eBool CmdSdhLineSurInit(char argc, char **argv)
    {
    return CliAtSurEngineInit(argc, argv, LinesFromString);
    }

eBool CmdSdhLineSurEnable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue, AtSurEngineEnable);
    }

eBool CmdSdhLineSurDisable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse, AtSurEngineEnable);
    }

eBool CmdSdhLineSurPmEnable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue, AtSurEnginePmEnable);
    }

eBool CmdSdhLineSurPmDisable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse, AtSurEnginePmEnable);
    }

eBool CmdSdhLineNotificationEnable(char argc, char **argv)
    {
    return CliAtSurEngineNotificationEnable(argc, argv, cAtTrue, Listener(), LinesFromString,
                                            CliAtSdhLineObserverGet,
                                            CliAtSdhLineObserverDelete);
    }

eBool CmdSdhLineNotificationDisable(char argc, char **argv)
    {
    return CliAtSurEngineNotificationEnable(argc, argv, cAtFalse, Listener(), LinesFromString,
                                            CliAtSdhLineObserver,
                                            CliAtSdhLineObserverDelete);
    }

eBool CmdSdhLineFailureNotificationMaskSet(char argc, char **argv)
    {
    return CliAtSurEngineNotificationMaskSet(argc, argv,
                                             cAtSdhLineAlarmTypeStr,
                                             cAtSdhLineAlarmTypeVal,
                                             mCount(cAtSdhLineAlarmTypeVal),
                                             AtSurEngineFailureNotificationMaskSet,
                                             LinesFromString);
    }

eBool CmdSdhLineSurEngineShow(char argc, char **argv)
    {
    return CliAtSurEngineShow(argc, argv, LinesFromString);
    }

eBool CmdSdhLineFailureNotificationMaskShow(char argc, char **argv)
    {
    return CliAtSurEngingFailureNotificationMaskShow(argc, argv,
                                                  cAtSdhLineAlarmTypeUpperStr,
                                                  cAtSdhLineAlarmTypeVal,
                                                  mCount(cAtSdhLineAlarmTypeVal),
                                                  LinesFromString);
    }

eBool CmdSdhLineFailureShow(char argc, char **argv)
    {
    return CliAtSurEngineFailuresShow(argc, argv,
                                      cAtSdhLineAlarmTypeUpperStr,
                                      cAtSdhLineAlarmTypeVal,
                                      mCount(cAtSdhLineAlarmTypeStr),
                                      LinesFromString);
    }

eBool CmdSdhLineFailureHistoryShow(char argc, char **argv)
    {
    return CliAtSurEngineFailuresHistoryShow(argc, argv,
                                             cAtSdhLineAlarmTypeUpperStr,
                                             cAtSdhLineAlarmTypeVal,
                                             mCount(cAtSdhLineAlarmTypeStr),
                                             LinesFromString);
    }

eBool CmdSdhLineFailureCaptureShow(char argc, char **argv)
    {
    return CliAtSurEngineFailuresCaptureShow(argc, argv,
                                             cAtSdhLineAlarmTypeUpperStr,
                                             cAtSdhLineAlarmTypeVal,
                                             mCount(cAtSdhLineAlarmTypeStr),
                                             LinesFromString,
                                             CliAtSdhLineObserverGet,
                                             CliAtSdhLineObserverDelete);
    }

eBool CmdSdhLinePmParamCurrentPeriodRegisterShow(char argc, char **argv)
    {
    return RegistersShow(argc, argv, cAtPmCurrentPeriodRegister);
    }

eBool CmdSdhLinePmParamPreviousPeriodRegisterShow(char argc, char **argv)
    {
    return RegistersShow(argc, argv, cAtPmPreviousPeriodRegister);
    }

eBool CmdSdhLinePmParamCurrentDayRegisterShow(char argc, char **argv)
    {
    return RegistersShow(argc, argv, cAtPmCurrentDayRegister);
    }

eBool CmdSdhLinePmParamPreviousDayRegisterShow(char argc, char **argv)
    {
    return RegistersShow(argc, argv, cAtPmPreviousDayRegister);
    }

eBool CmdSdhLinePmParamRecentPeriodRegisterShow(char argc, char **argv)
    {
    return CliAtSurEnginePmParamRecentPeriodsShow(argc, argv,
                                                  cAtSurEngineSdhLinePmParamUpperStr,
                                                  cAtSurEngineSdhLinePmParamVal,
                                                  mCount(cAtSurEngineSdhLinePmParamVal),
                                                  LinesFromString);

    }

static uint32 CliSurSdhLinePmParamFromString(char *pStr)
    {
    uint32 paramType;
    eBool convertSuccess = cAtTrue;
    mAtStrToEnum(cAtSurEngineSdhLinePmParamStr, cAtSurEngineSdhLinePmParamVal,  pStr,  paramType, convertSuccess);
    if(!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Unknown type %s, expect: ", pStr);
        CliExpectedValuesPrint(cSevCritical, cAtSurEngineSdhLinePmParamStr, mCount(cAtSurEngineSdhLinePmParamVal));
        AtPrintc(cSevCritical, "\r\n");
        return 0;
        }

    return paramType;
    }

eBool CmdSurThresholProfileLineSet(char argc, char **argv)
    {
    return CliSurThresholdProfileParamValueSet(argc, argv,
                                               CliSurSdhLinePmParamFromString,
                                               (TcaThresholdFuncSet)AtPmThresholdProfileSdhLinePeriodTcaThresholdSet);
    }

eBool CmdSurThresholProfileLineShow(char argc, char **argv)
    {
    return CliSurThresholProfileShow(argc,
                                     argv,
                                     cAtSurEngineSdhLinePmParamUpperStr,
                                     cAtSurEngineSdhLinePmParamVal,
                                     mCount(cAtSurEngineSdhLinePmParamVal),
                                     (TcaThresholdFuncGet)AtPmThresholdProfileSdhLinePeriodTcaThresholdGet);
    }

eBool CmdSdhLineSurveillanceThresoldProfileSet(char argc, char **argv)
    {
    return CliAtSurEngineThresholdProfileSet(argc, argv, LinesFromString);
    }

eBool CmdSdhLineSurveillanceThresoldProfileShow(char argc, char **argv)
    {
    return CliAtSurEngineThresholdProfileShow(argc, argv, LinesFromString);
    }

eBool CmdSdhLineTcaNotificationMaskSet(char argc, char **argv)
    {
    return CliAtSurEngineNotificationMaskSet(argc, argv,
                                             cAtSurEngineSdhLinePmParamStr,
                                             cAtSurEngineSdhLinePmParamVal,
                                             mCount(cAtSurEngineSdhLinePmParamVal),
                                             AtSurEngineTcaNotificationMaskSet,
                                             LinesFromString);
    }

eBool CmdSdhLineTcaNotificationMaskShow(char argc, char **argv)
    {
    return CliAtSurEngingTcaNotificationMaskShow(argc, argv,
                                                 cAtSurEngineSdhLinePmParamUpperStr,
                                                 cAtSurEngineSdhLinePmParamVal,
                                                 mCount(cAtSurEngineSdhLinePmParamVal),
                                                 LinesFromString);
    }

eBool CmdSdhLineTcaHistoryShow(char argc, char **argv)
    {
    return CliAtSurEngineTcaHistoryShow(argc, argv,
                                        cAtSurEngineSdhLinePmParamUpperStr,
                                        cAtSurEngineSdhLinePmParamVal,
                                        mCount(cAtSurEngineSdhLinePmParamVal),
                                        LinesFromString);
    }

eBool CmdSdhLinePmParamPeriodThresholdShow(char argc, char **argv)
    {
    return CliAtSurEngineParamPeriodThresholdShow(argc, argv,
                                      cAtSurEngineSdhLinePmParamUpperStr,
                                      cAtSurEngineSdhLinePmParamVal,
                                      mCount(cAtSurEngineSdhLinePmParamVal),
                                      LinesFromString);
    }

eBool CmdSdhLinePmParamDayThresholdShow(char argc, char **argv)
    {
    return CliAtSurEngineParamDayThresholdShow(argc, argv,
                                      cAtSurEngineSdhLinePmParamUpperStr,
                                      cAtSurEngineSdhLinePmParamVal,
                                      mCount(cAtSurEngineSdhLinePmParamVal),
                                      LinesFromString);
    }

eBool CmdSdhLineTcaCaptureShow(char argc, char **argv)
    {
    return CliAtSurEngineTcaCaptureShow(argc, argv,
                                        cAtSurEngineSdhLinePmParamUpperStr,
                                        cAtSurEngineSdhLinePmParamVal,
                                        mCount(cAtSurEngineSdhLinePmParamVal),
                                        LinesFromString,
                                        CliAtSdhLineObserverGet,
                                        CliAtSdhLineObserverDelete);
    }


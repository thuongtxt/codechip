/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PTP
 *
 * File        : CliAtPtpPsnGroup.c
 *
 * Created Date: Aug 2, 2018
 *
 * Description : PTP PSN multicast group CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtModulePtp.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool AddressAttributeSet(char *idListStr, uint8 *address,
                                 AtPtpPsnGroup (*groupGet)(AtModulePtp, uint32))
    {
    uint8 i;
    uint32 bufferSize;
    uint32 numGroups;
    AtPtpPsnGroup *groupList;
    eBool success = cAtTrue;

    /* Get the shared ID buffer */
    groupList = (AtPtpPsnGroup*)CliSharedObjectListGet(&bufferSize);
    if ((numGroups = CliAtPtpPsnGroupListFromString(idListStr, groupList, bufferSize, groupGet)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid group ID list or group not exist. Expected: 1, 1-2 ...\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numGroups; i++)
        {
        AtPtpPsnGroup group = (AtPtpPsnGroup)groupList[i];
        eAtRet ret;

        ret = AtPtpPsnGroupAddressSet(group, address);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Set address of %s fail with ret = %s\r\n",
                     AtObjectToString((AtObject)group),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static eBool PortHandle(char *groupIdListStr, char *portIdListStr,
                        AtPtpPsnGroup (*groupGet)(AtModulePtp, uint32),
                        eAtModulePtpRet (*portHandle)(AtPtpPsnGroup, AtPtpPort))
    {
    uint8 i, j;
    uint32 bufferSize;
    uint32 numGroups;
    AtPtpPsnGroup *groupList;
    eBool success = cAtTrue;
    AtChannel *portList;
    uint32 numPorts;

    /* Get the shared ID buffer */
    groupList = (AtPtpPsnGroup*)CliSharedObjectListGet(&bufferSize);
    if ((numGroups = CliAtPtpPsnGroupListFromString(groupIdListStr, groupList, bufferSize, groupGet)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid group ID list or group not exist. Expected: 1, 1-2 ...\r\n");
        return cAtFalse;
        }

    portList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = CliAtPtpPortListFromString(portIdListStr, portList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid port ID list or port not exist. Expected: 1, 1-2 ...\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numGroups; i++)
        {
        for (j = 0; j < numPorts; j++)
            {
            eAtRet ret = portHandle(groupList[i], (AtPtpPort)portList[j]);
            if (ret != cAtOk)
                {
                AtPrintc(cSevCritical, "ERROR: Manipulate %s with %s fail with ret = %s\r\n",
                         AtObjectToString((AtObject)groupList[i]),
                         CliChannelIdStringGet(portList[j]),
                         AtRet2String(ret));
                success = cAtFalse;
                }
            }
        }

    return success;
    }

static const char *PortListString(AtPtpPsnGroup group)
    {
    static char buff[64];
    AtIterator iter;
    AtChannel port;

    AtOsalMemInit(buff, 0, sizeof(buff));

    iter = AtPtpPsnGroupPortIteratorCreate(group);
    port = (AtChannel)AtIteratorNext(iter);
    if (port == NULL)
        {
        AtObjectDelete((AtObject)iter);
        return "none";
        }

    AtSprintf(buff, "%d", AtChannelIdGet(port) + 1);
    while ((port = (AtChannel)AtIteratorNext(iter)) != NULL)
        AtSprintf(&buff[AtStrlen(buff)], ",%d", AtChannelIdGet(port) + 1);
    AtObjectDelete((AtObject)iter);

    return buff;
    }

static eBool CliAtPtpPsnGroupShow(char argc, char **argv,
                                  AtPtpPsnGroup (*groupGet)(AtModulePtp, uint32),
                                  char *addr2StringFunc(const uint8 *))
    {
    tTab *tabPtr = NULL;
    uint8 i;
    uint32 bufferSize;
    AtPtpPsnGroup *groupList;
    uint32 numGroups;
    uint8 row = 0;
    const char *heading[] = {"Description", "Address", "Joined Ports"};

    AtUnused(argc);

    /* Get the shared ID buffer */
    groupList = (AtPtpPsnGroup*)CliSharedObjectListGet(&bufferSize);
    if ((numGroups = (uint8)CliAtPtpPsnGroupListFromString(argv[0], groupList, bufferSize, groupGet)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 ...\r\n");
        return cAtTrue;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numGroups, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numGroups; i++)
        {
        uint8 ipAddr[cAtIpv6AddressLen];
        AtPtpPsnGroup group = groupList[i];
        eAtRet ret;
        uint8 column = 0;

        /* Description */
        StrToCell(tabPtr, i, column++, AtObjectToString((AtObject)group));

        /* Address */
        ret = AtPtpPsnGroupAddressGet(group, ipAddr);
        ColorStrToCell(tabPtr, i, column++, (ret == cAtOk) ? addr2StringFunc(ipAddr) : "Error",
                                            (ret == cAtOk) ? cSevNormal : cSevCritical);

        /* Port list, */
        StrToCell(tabPtr, row++, column, PortListString(group));
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtPtpPsnGroupEthMacAddressSet(char argc, char **argv)
    {
    uint8 mac[cAtMacAddressLen] = {0};
    AtUnused(argc);

    if (!CliAtModuleEthMacAddrStr2Byte(argv[1], mac))
        {
        AtPrintc(cSevCritical, "Invalid MAC address\r\n");
        return cAtFalse;
        }

    return AddressAttributeSet(argv[0], mac, AtModulePtpL2PsnGroupGet);
    }

eBool CmdAtPtpPsnGroupEthMacShow(char argc, char **argv)
    {
    return CliAtPtpPsnGroupShow(argc, argv, AtModulePtpL2PsnGroupGet, CliEthPortMacAddressByte2Str);
    }

eBool CmdAtPtpPsnGroupIpV4AddressSet(char argc, char **argv)
    {
    uint8 ipv4Addr[cAtIpv4AddressLen] = {0};
    AtUnused(argc);

    if (!CliIpv4StringToArray(argv[1], ipv4Addr))
        {
        AtPrintc(cSevCritical, "Invalid IPv4 address\r\n");
        return cAtFalse;
        }

    return AddressAttributeSet(argv[0], ipv4Addr, AtModulePtpIpV4PsnGroupGet);
    }

eBool CmdAtPtpPsnGroupIpV4PortAdd(char argc, char **argv)
    {
    AtUnused(argc);
    return PortHandle(argv[0], argv[1], AtModulePtpIpV4PsnGroupGet, AtPtpPsnGroupPortAdd);
    }

eBool CmdAtPtpPsnGroupIpV4PortRemove(char argc, char **argv)
    {
    AtUnused(argc);
    return PortHandle(argv[0], argv[1], AtModulePtpIpV4PsnGroupGet, AtPtpPsnGroupPortRemove);
    }

eBool CmdAtPtpPsnGroupIpV4Show(char argc, char **argv)
    {
    return CliAtPtpPsnGroupShow(argc, argv, AtModulePtpIpV4PsnGroupGet, CliIpV4ArrayToString);
    }

eBool CmdAtPtpPsnGroupIpV6AddressSet(char argc, char **argv)
    {
    uint8 ipv6Addr[cAtIpv6AddressLen] = {0};
    AtUnused(argc);

    if (!CliIpV6StringToArray(argv[1], ipv6Addr))
        {
        AtPrintc(cSevCritical, "Invalid IPv6 address\r\n");
        return cAtFalse;
        }

    return AddressAttributeSet(argv[0], ipv6Addr, AtModulePtpIpV6PsnGroupGet);
    }

eBool CmdAtPtpPsnGroupIpV6PortAdd(char argc, char **argv)
    {
    AtUnused(argc);
    return PortHandle(argv[0], argv[1], AtModulePtpIpV6PsnGroupGet, AtPtpPsnGroupPortAdd);
    }

eBool CmdAtPtpPsnGroupIpV6PortRemove(char argc, char **argv)
    {
    AtUnused(argc);
    return PortHandle(argv[0], argv[1], AtModulePtpIpV6PsnGroupGet, AtPtpPsnGroupPortRemove);
    }

eBool CmdAtPtpPsnGroupIpV6Show(char argc, char **argv)
    {
    return CliAtPtpPsnGroupShow(argc, argv, AtModulePtpIpV6PsnGroupGet, CliIpV6ArrayToString);
    }


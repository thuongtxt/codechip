/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PTP
 *
 * File        : CliAtModulePtp.c
 *
 * Created Date: Jul 2, 2018
 *
 * Description : Module PTP CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtModulePtp.h"
#include "AtCliModule.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char * cAtPtpDeviceTypeStr[] = {
                                     "ordinary",
                                     "boundary",
                                     "transparent"
                                     };

static const uint32 cAtPtpDeviceTypeVal[]  = {
                                             cAtPtpDeviceTypeOrdinary,
                                             cAtPtpDeviceTypeBoundary,
                                             cAtPtpDeviceTypeTransparent
                                            };

static const uint32 cAtPtpPpsSourceVal[] = {
                                            cAtPtpPpsSourceInternal,
                                            cAtPtpPpsSourceExternal
                                           };
static const char *cAtPtpPpsSourceStr[] = {"internal", "external"};

static const uint32 cAtPtpCorrectionModeVal[] = {
                                            cAtPtpCorrectionModeStandard,
                                            cAtPtpCorrectionModeAssist
                                           };
static const char *cAtPtpCorrectionModeStr[] = {"standard", "assistant"};

static const uint32 cAtPtpT1T3CaptureModeVal[] = {
                                            cAtPtpT1T3TimestampCaptureModeNone,
                                            cAtPtpT1T3TimestampCaptureModeCpu,
                                            cAtPtpT1T3TimestampCaptureModePacket
                                           };
static const char *cAtPtpT1T3CaptureModeStr[] = {"none", "cpu", "packet"};

static const uint32 cAtPtpMsgTypeVal[] = {
                                          cAtPtpMsgTypeSync,
                                          cAtPtpMsgTypeFollowUp,
                                          cAtPtpMsgTypeDelayRequest,
                                          cAtPtpMsgTypeDelayRespond
                                          };
static const char *cAtPtpMsgTypeStr[] = {"sync", "follow-up", "delay-req", "delay-resp"};


static const uint32 cAtPtpAlarmTypeVal[] = {
                                            cAtModulePtpAlarmTypeCpuQueu
                                            };
static const char *cAtPtpAlarmTypeStr[] = {"cpuqueue"};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static AtModulePtp CliModulePtp(void)
    {
    return (AtModulePtp)AtDeviceModuleGet(CliDevice(), cAtModulePtp);
    }

static const char **CliPtpAlarmTypeStr(uint8 *numAlarms)
    {
    if (numAlarms)
        *numAlarms = mCount(cAtPtpAlarmTypeStr);
    return cAtPtpAlarmTypeStr;
    }

static const uint32 *CliPtpAlarmTypeVal(uint8 *numAlarms)
    {
    if (numAlarms)
        *numAlarms = mCount(cAtPtpAlarmTypeVal);
    return cAtPtpAlarmTypeVal;
    }

static const char *Alarm2String(uint32 alarms)
    {
    uint16 i;
    static char alarmString[64];
    uint8 numAlarms;
    const char **alarmTypeStr = CliPtpAlarmTypeStr(&numAlarms);
    const uint32 *alarmTypeVal = CliPtpAlarmTypeVal(&numAlarms);

    /* Initialize string */
    alarmString[0] = '\0';
    if (alarms == 0)
        AtSprintf(alarmString, "None");

    /* There are alarms */
    else
        {
        for (i = 0; i < numAlarms; i++)
            {
            if (alarms & (alarmTypeVal[i]))
                AtSprintf(alarmString, "%s%s|", alarmString, alarmTypeStr[i]);
            }

        if (AtStrlen(alarmString) == 0)
            return "None";

        /* Remove the last '|' */
        alarmString[AtStrlen(alarmString) - 1] = '\0';
        }

    return alarmString;
    }

static uint32 IntrMaskFromString(char *pIntrStr)
    {
    uint8 numAlarms = 0;
    const char **alarmTypeStrings = CliPtpAlarmTypeStr(&numAlarms);
    const uint32 *alarmTypes = CliPtpAlarmTypeVal(&numAlarms);
    return CliMaskFromString(pIntrStr, alarmTypeStrings, alarmTypes, numAlarms);
    }

static eBool StatusShow(char argc, char **argv, uint32 (*AlarmGetFunc)(AtModule self), eBool silent)
    {
    uint32 j;
    uint32 alarmStat;
    tTab *tabPtr = NULL;
    const char *pHeading[] = {"CPUQUEUE"};
    uint8 numAlarms;
    const uint32 *alarmTypeVal = CliPtpAlarmTypeVal(&numAlarms);
    AtUnused(argc);
    AtUnused(argv);

    /* Create table with titles */
    if (!silent)
        {
        tabPtr = TableAlloc(1, mCount(pHeading), pHeading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
            return cAtFalse;
            }
        }

    /* Print alarm status */
    alarmStat = AlarmGetFunc((AtModule) CliModulePtp());
    for (j = 0; j < numAlarms; j++)
        {
        if (alarmStat & alarmTypeVal[j])
            ColorStrToCell(tabPtr, 0, j, "set", cSevCritical);
        else
            ColorStrToCell(tabPtr, 0, j, "clear", cSevInfo);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

static eBool InterruptEnable(char argc, char **argv, eBool enable)
    {
    eAtRet ret = AtModuleInterruptEnable((AtModule)CliModulePtp(), enable);
    AtUnused(argv);
    AtUnused(argc);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "Cannot %s interrupt, ret = %s\n", enable ? "enable" : "disable", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

static void ErrorReporting(AtModule module, uint32 changedAlarms, uint32 currentStatus, void* userContext)
    {
    AtUnused(userContext);
    AtUnused(currentStatus);
    AtUnused(module);
    AtPrintc(cSevNormal, "\r\n%s Module PTP alarm raised:", AtOsalDateTimeInUsGet());
    AtPrintc(cSevNormal, "\r\n");
    if (changedAlarms & cAtModulePtpAlarmTypeCpuQueu)
        {
        AtPrintc(cSevNormal, "CPU QUEUE message contents:");
        AtPrintc(cSevNormal, "\r\n");
        AtCliExecute("show ptp t1t3 capture content");
        }
    }

static eBool InterruptCapture(char argc, char **argv, eBool enable)
    {
    static tAtModuleAlarmListener listener;
    eAtRet ret;
    AtUnused(argv);
    AtUnused(argc);

    listener.AlarmChangeStateWithUserData = ErrorReporting;
    if (enable)
        ret = AtModuleEventListenerAdd((AtModule)CliModulePtp(), &listener, NULL);
    else
        ret = AtModuleEventListenerRemove((AtModule)CliModulePtp(), &listener);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "Cannot %s interrupt, ret = %s\n", enable ? "enable" : "disable", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

static uint32 PortListFromString(char *pStrIdList, AtChannel *channel, uint32 bufferSize)
    {
    uint32 numPorts, i;
    uint32 *idBuf;
    uint32 numChannels = 0;
    AtModulePtp ptpModule = CliModulePtp();

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numPorts = CliIdListFromString(pStrIdList, idBuf, bufferSize)) == 0)
        return numChannels;

    for (i = 0; i < numPorts; i++)
        {
        channel[numChannels] = (AtChannel)AtModulePtpPortGet(ptpModule, idBuf[i] - 1);
        if (channel[numChannels])
            numChannels = numChannels + 1;

       if (numChannels >= bufferSize)
           break;
        }

    return numChannels;
    }

static eBool Enable(char argc, char **argv, eBool enable, eAtModulePtpRet (*EnableFunc)(AtModulePtp, eBool))
    {
    eAtRet ret;
    AtUnused(argc);
    AtUnused(argv);

    ret = EnableFunc(CliModulePtp(), enable);
    return (ret != cAtOk) ? cAtFalse : cAtTrue;
    }

uint32 CliAtPtpPortListFromString(char *pStrIdList, AtChannel *channel, uint32 bufferSize)
    {
    AtList portIdStrings = AtCliIdStrListSeparateByComma(pStrIdList);
    AtIterator iterator = AtListIteratorGet(portIdStrings);
    char *strId;
    uint32 numPorts = 0;

    AtIteratorRestart(iterator);
    while ((strId = (char*)AtIteratorNext(iterator)) != NULL)
        {
        numPorts += PortListFromString(strId, &channel[numPorts], bufferSize - numPorts);
        }

    AtObjectDelete((AtObject)portIdStrings);
    return numPorts;
    }

uint32 CliAtPtpPsnGroupListFromString(char *pStrIdList, AtPtpPsnGroup *groupList, uint32 bufferSize,
                                      AtPtpPsnGroup (*GroupGet)(AtModulePtp, uint32))
    {
    uint32 numberIds, i;
    uint32 *idBuf;
    uint32 numGroups = 0;
    AtModulePtp ptpModule = CliModulePtp();

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numberIds = CliIdListFromString(pStrIdList, idBuf, bufferSize)) == 0)
        return numGroups;

    for (i = 0; i < numberIds; i++)
        {
        groupList[numGroups] = GroupGet(ptpModule, idBuf[i] - 1);
        if (groupList[numGroups])
            numGroups = numGroups + 1;

       if (numGroups >= bufferSize)
           break;
        }

    return numGroups;
    }

eBool CmdAtModulePtpShow(char argc, char **argv)
    {
    eAtRet ret;
    tTab *tabPtr;
    const char *heading[] = {"Attribute", "Value"};
    const char *colTitle[] = {"numPorts", "Device Type", "Device MAC", "Device MAC enabled",
                              "Time-of-Day (s)", "PPS Route Trip Delay (ns)", "PPS source",
                              "Correction Mode", "L2 MAC Groups", "IPv4 Groups", "IPv6 Groups",
                              "T1T3 Capture Mode", "Interrupt Mask", "Timestamp Bypass"};
    AtModulePtp ptpModule = CliModulePtp();
    uint32 value;
    uint8 deviceMacAddr[cAtMacAddressLen];
    uint32 row_i = 0;
    char buf[100];
    eBool blResult;
    const char *stringValue;
    uint32 intrMaskVal = 0;
    eBool timestampBypass = cAtFalse;

    AtUnused(argc);
    AtUnused(argv);

    /* Initialize table */
    tabPtr = TableAlloc(mCount(colTitle), mCount(heading), heading);
    if (tabPtr == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    StrToCell(tabPtr, row_i, 0, colTitle[row_i]);
    StrToCell(tabPtr, row_i, 1, CliNumber2String(AtModulePtpNumPortsGet(ptpModule), "%u"));
    row_i = row_i + 1;

    StrToCell(tabPtr, row_i, 0, colTitle[row_i]);
    value = AtModulePtpDeviceTypeGet(ptpModule);
    if (value == cAtPtpDeviceTypeUnknown)
        ColorStrToCell(tabPtr, row_i, 1, "Error", cSevCritical);
    else
        {
        /* Convert enum to string */
        mAtEnumToStr(cAtPtpDeviceTypeStr,
                     cAtPtpDeviceTypeVal,
                     value,
                     buf,
                     blResult);
        StrToCell(tabPtr, row_i, 1, (blResult) ? buf : "Error");
        }
    row_i = row_i + 1;

    StrToCell(tabPtr, row_i, 0, colTitle[row_i]);
    ret = AtModulePtpDeviceMacAddressGet(ptpModule, deviceMacAddr);
    if (ret == cAtOk)
        StrToCell(tabPtr, row_i, 1, CliEthPortMacAddressByte2Str(deviceMacAddr));
    else
        StrToCell(tabPtr, row_i, 1, AtRet2String(ret));
    row_i = row_i + 1;

    StrToCell(tabPtr, row_i, 0, colTitle[row_i]);
    StrToCell(tabPtr, row_i, 1, CliBoolToString(AtModulePtpDeviceMacAddressIsEnabled(ptpModule)));
    row_i = row_i + 1;

    StrToCell(tabPtr, row_i, 0, colTitle[row_i]);
    StrToCell(tabPtr, row_i, 1, AtNumberUInt64DigitGroupingString(AtModulePtpTimeOfDayGet(ptpModule)));
    row_i = row_i + 1;

    StrToCell(tabPtr, row_i, 0, colTitle[row_i]);
    StrToCell(tabPtr, row_i, 1, AtNumberDigitGroupingString(AtModulePtpPpsRouteTripDelayGet(ptpModule)));
    row_i = row_i + 1;

    value = AtModulePtpPpsSourceGet(ptpModule);
    stringValue = CliEnumToString(value,
                                  cAtPtpPpsSourceStr,
                                  cAtPtpPpsSourceVal,
                                  mCount(cAtPtpPpsSourceVal), NULL);
    StrToCell(tabPtr, row_i, 0, colTitle[row_i]);
    ColorStrToCell(tabPtr, row_i, 1, stringValue ? stringValue : "Error", stringValue ? cSevNormal : cSevCritical);
    row_i = row_i + 1;

    value = AtModulePtpCorrectionModeGet(ptpModule);
    stringValue = CliEnumToString(value,
                                  cAtPtpCorrectionModeStr,
                                  cAtPtpCorrectionModeVal,
                                  mCount(cAtPtpCorrectionModeVal), NULL);
    StrToCell(tabPtr, row_i, 0, colTitle[row_i]);
    ColorStrToCell(tabPtr, row_i, 1, stringValue ? stringValue : "Error", stringValue ? cSevNormal : cSevCritical);
    row_i = row_i + 1;

    StrToCell(tabPtr, row_i, 0, colTitle[row_i]);
    StrToCell(tabPtr, row_i, 1, CliNumber2String(AtModulePtpNumL2PsnGroupsGet(ptpModule), "%u"));
    row_i = row_i + 1;

    StrToCell(tabPtr, row_i, 0, colTitle[row_i]);
    StrToCell(tabPtr, row_i, 1, CliNumber2String(AtModulePtpNumIpV4PsnGroupsGet(ptpModule), "%u"));
    row_i = row_i + 1;

    StrToCell(tabPtr, row_i, 0, colTitle[row_i]);
    StrToCell(tabPtr, row_i, 1, CliNumber2String(AtModulePtpNumIpV6PsnGroupsGet(ptpModule), "%u"));
    row_i = row_i + 1;

    /* T1T3 capture mode */
    value = AtModulePtpT1T3CaptureModeGet(ptpModule);
    stringValue = CliEnumToString(value,
                                  cAtPtpT1T3CaptureModeStr,
                                  cAtPtpT1T3CaptureModeVal,
                                  mCount(cAtPtpT1T3CaptureModeVal), NULL);
    StrToCell(tabPtr, row_i, 0, colTitle[row_i]);
    ColorStrToCell(tabPtr, row_i, 1, stringValue ? stringValue : "Error", stringValue ? cSevNormal : cSevCritical);
    row_i = row_i + 1;

    /* Interrupt mask */
    intrMaskVal = AtModuleInterruptMaskGet((AtModule)ptpModule);
    StrToCell(tabPtr, row_i, 0, colTitle[row_i]);
    ColorStrToCell(tabPtr, row_i, 1, Alarm2String(intrMaskVal), cSevNormal);
    row_i = row_i + 1;

    /* Timestamp bypass */
    timestampBypass = AtModulePtpTimestampBypassIsEnabled(ptpModule);
    StrToCell(tabPtr, row_i, 0, colTitle[row_i]);
    StrToCell(tabPtr, row_i, 1, timestampBypass ? "en" : "dis");
    row_i = row_i + 1;

    /* Show table */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtModulePtpDeviceTypeSet(char argc, char **argv)
    {
    eAtRet ret;
    eAtPtpDeviceType deviceType;
    eBool blResult;
    AtUnused(argc);

    /* Get clock type from string */
    mAtStrToEnum(cAtPtpDeviceTypeStr,
                 cAtPtpDeviceTypeVal,
                 argv[0],
                 deviceType,
                 blResult);
    if (!blResult)
        {
        AtPrintc(cSevCritical, "Invalid PTP clock type\r\n");
        return cAtFalse;
        }

    ret = AtModulePtpDeviceTypeSet(CliModulePtp(), deviceType);
    if (ret != cAtOk)
        AtPrintc(cSevCritical, "ERROR: Set device type failed with ret = %s\r\n",
                 AtRet2String(ret));

    return (ret == cAtOk) ? cAtTrue : cAtFalse;
    }

eBool CmdAtModulePtpDeviceMacAddressSet(char argc, char **argv)
    {
    uint8 mac[cAtMacAddressLen];
    eAtRet ret = cAtOk;
    AtUnused(argc);

    if (!CliAtModuleEthMacAddrStr2Byte(argv[0], mac))
        {
        AtPrintc(cSevCritical, "Invalid Destination MAC\r\n");
        return cAtFalse;
        }

    ret = AtModulePtpDeviceMacAddressSet(CliModulePtp(), mac);
    return (ret != cAtOk) ? cAtFalse : cAtTrue;
    }

eBool CmdAtModulePtpDebug(char argc, char **argv)
    {
    eAtRet ret = AtModuleDebug((AtModule)CliModulePtp());
    AtUnused(argv);
    AtUnused(argc);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "Cannot show debugging, ret = %s\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAtModulePtpPpsSourceSet(char argc, char **argv)
    {
    eBool convertSuccess = cAtTrue;
    eAtPtpPpsSource source = CliStringToEnum(argv[0], cAtPtpPpsSourceStr, cAtPtpPpsSourceVal, mCount(cAtPtpPpsSourceVal), &convertSuccess);
    eAtRet ret;

    AtUnused(argc);

    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Please input valid PPS source. Expect: ");
        CliExpectedValuesPrint(cSevCritical, cAtPtpPpsSourceStr, mCount(cAtPtpPpsSourceVal));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    ret = AtModulePtpPpsSourceSet(CliModulePtp(), source);
    if (ret == cAtOk)
        return cAtTrue;

    AtPrintc(cSevCritical, "ERROR: Change PPS source fail with ret = %s\r\n", AtRet2String(ret));
    return cAtFalse;
    }

eBool CmdAtModulePtpCorrectionModeSet(char argc, char **argv)
    {
    eBool convertSuccess = cAtTrue;
    eAtPtpCorrectionMode mode = CliStringToEnum(argv[0], cAtPtpCorrectionModeStr,
                                             cAtPtpCorrectionModeVal, mCount(cAtPtpCorrectionModeVal),
                                             &convertSuccess);
    eAtRet ret;

    AtUnused(argc);

    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Please input valid correction mode. Expect: ");
        CliExpectedValuesPrint(cSevCritical, cAtPtpCorrectionModeStr, mCount(cAtPtpCorrectionModeVal));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    ret = AtModulePtpCorrectionModeSet(CliModulePtp(), mode);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Change correction mode fail with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAtModulePtpDeviceMacAddressEnable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue, AtModulePtpDeviceMacAddressEnable);
    }

eBool CmdAtModulePtpDeviceMacAddressDisable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse, AtModulePtpDeviceMacAddressEnable);
    }

eBool CmdAtModulePtpT1T3CaptureModeSet(char argc, char **argv)
    {
    eBool convertSuccess = cAtTrue;
    eAtPtpT1T3TimeStampCaptureMode mode = CliStringToEnum(argv[0], cAtPtpT1T3CaptureModeStr,
                                                          cAtPtpT1T3CaptureModeVal, mCount(cAtPtpT1T3CaptureModeVal),
                                                          &convertSuccess);
    eAtRet ret;

    AtUnused(argc);

    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Please input valid correction mode. Expect: ");
        CliExpectedValuesPrint(cSevCritical, cAtPtpT1T3CaptureModeStr, mCount(cAtPtpT1T3CaptureModeVal));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    ret = AtModulePtpT1T3CaptureModeSet(CliModulePtp(), mode);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Change correction mode fail with ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAtModulePtpT1T3CaptureContentShow(char argc, char **argv)
    {
    uint32 numMsg, msg_i;
    tTab *tabPtr;
    const char *heading[] = {"Port Description", "PtpMsgType", "SequenceId", "Timestamp(sec.nano)"};
    char buf[200];
    tAtPtpT1T3TimestampContent *timestampBuffer = NULL;
    tAtPtpT1T3TimestampContent *timestampPointer = NULL;
    static const uint32 cMaxNumMsgEntry = 16;
    AtModulePtp module = (AtModulePtp)AtDeviceModuleGet(CliDevice(), cAtModulePtp);
    AtPtpPort port = NULL;

    AtUnused(argc);
    AtUnused(argv);

    /* Initialize structure */
    AtOsalMemInit(buf, 0x0, sizeof(buf));
    timestampBuffer = AtOsalMemAlloc(sizeof(tAtPtpT1T3TimestampContent) * cMaxNumMsgEntry); /*16 messages*/
    numMsg = AtModulePtpT1T3CaptureContentGet(module, timestampBuffer, cMaxNumMsgEntry);

    /* Create table with titles */
    tabPtr = TableAlloc(numMsg, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (msg_i = 0; msg_i < numMsg; msg_i++)
        {
        uint8 column = 0;
        timestampPointer = &timestampBuffer[msg_i];
        port = timestampPointer->egressPort;

        /* Description */
        StrToCell(tabPtr, msg_i, column++, AtObjectToString((AtObject)port));

        /* msg type */
        StrToCell(tabPtr, msg_i, column++, CliEnumToString(timestampPointer->ptpMsgType,
                                                           cAtPtpMsgTypeStr,
                                                           cAtPtpMsgTypeVal,
                                                           mCount(cAtPtpMsgTypeVal), NULL));

        /* Sequence ID */
        AtSprintf(buf, "%d", timestampPointer->sequenceNumber);
        StrToCell(tabPtr, msg_i, column++, buf);

        /* time stamp */
        AtSprintf(buf, "%llu.%u", timestampPointer->seconds, timestampPointer->nanoSeconds);
        StrToCell(tabPtr, msg_i, column++, buf);
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtOsalMemFree(timestampBuffer);

    return cAtTrue;
    }

eBool CmdPtpInterruptEnable(char argc, char **argv)
    {
    return InterruptEnable(argc, argv, cAtTrue);
    }

eBool CmdPtpInterruptDisable(char argc, char **argv)
    {
    return InterruptEnable(argc, argv, cAtFalse);
    }

/*
 * SET interrupt mask
 */
eBool CmdPtpIntrMsk(char argc, char **argv)
    {
    uint32 intrMaskVal;
    eBool enable = cAtFalse;
    eBool success = cAtTrue;
    eBool convertSuccess = cAtFalse;
    eAtRet ret;
    AtUnused(argc);

    /* Get interrupt mask value from input string */
    if ((intrMaskVal = IntrMaskFromString(argv[0])) == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Interrupt mask is invalid\r\n");
        return cAtFalse;
        }

    /* Enabling */
    mAtStrToBool(argv[1], enable, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Expected: en/dis\r\n");
        return cAtFalse;
        }

    ret = AtModuleInterruptMaskSet((AtModule)CliModulePtp(), intrMaskVal, enable ? intrMaskVal : 0x0);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot configure interrupt mask , ret = %s\r\n",
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

eBool CmdPtpShowAlarm(char argc, char **argv)
    {
    return StatusShow(argc, argv, AtModuleAlarmGet, cAtFalse);
    }

/*
 * Show interrupt
 */
eBool CmdPtpShowInterrupt(char argc, char **argv)
    {
    eAtHistoryReadingMode readingMode;
    eBool silent = cAtFalse;

    if (argc < 1)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    readingMode = CliHistoryReadingModeGet(argv[0]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    silent = CliHistoryReadingShouldSilent(readingMode, (argc > 1) ? argv[1] : NULL);

    if (readingMode == cAtHistoryReadingModeReadToClear)
        return StatusShow(argc, argv, AtModuleAlarmInterruptClear, silent);
    else
        return StatusShow(argc, argv, AtModuleAlarmInterruptGet, silent);
    }

eBool CmdPtpInterruptCaptureEnable(char argc, char **argv)
    {
    return InterruptCapture(argc, argv, cAtTrue);
    }

eBool CmdPtpInterruptCaptureDisable(char argc, char **argv)
    {
    return InterruptCapture(argc, argv, cAtFalse);
    }

eBool CmdAtPtpExpectVlanSet(char argc, char **argv)
    {
    tAtEthVlanDesc desc;
    tAtEthVlanTag *cVlan;
    eBool success = cAtTrue;
    eAtRet ret = cAtOk;
    uint8 index = 0;

    AtUnused(argc);

    AtOsalMemInit(&desc, 0, sizeof(desc));

    /* Get entry ID */
    index = (uint8)AtStrtoul(argv[0], NULL, 10);
    if (index == 0)
        {
        AtPrintc(cSevCritical, "Invalid entry index. Expected: 1, 2...\r\n");
        return cAtFalse;
        }
    index = (uint8)CliId2DriverId(index);

    /* Get S-VLAN and C-VLAN */
    cVlan = NULL;
    if (!CliEthFlowVlanGet(argv[1], NULL, &desc))
        return cAtFalse;
    if (desc.numberOfVlans > 0)
        cVlan = &(desc.vlans[0]);

    ret = AtModulePtpExpectedVlanSet(CliModulePtp(), index, cVlan);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Can not configure for %s, ret = %s\r\n",
                 AtObjectToString((AtObject)CliModulePtp()),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

eBool CmdAtPtpExpectedVlanShow(char argc, char **argv)
    {
    const char *pHeading[] = { "Index", "vlan" };
    tTab *tabPtr;
    static char idString[32];
    uint32 numIndex, i;
    AtUnused(argc);
    AtUnused(argv);
    /* Create table with titles */
    numIndex = (uint8)AtModulePtpNumExpectedVlan(CliModulePtp());
    tabPtr = TableAlloc(numIndex, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numIndex; i++)
        {
        tAtEthVlanTag cVlan = {0, 0, 0};

        AtSprintf(idString, "%d", (i + 1));
        StrToCell(tabPtr, i, 0, idString);
        StrToCell(tabPtr, i, 1, CliVlans2Str(AtModulePtpExpectedVlanGet(CliModulePtp(), (uint8)i, &cVlan), NULL));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtModulePtpTimeOfDaySet(char argc, char **argv)
    {
    eAtRet ret;
    uint64 seconds = 0;

    AtUnused(argc);
    seconds = AtStrtoull(argv[0], NULL, 10);
    ret = AtModulePtpTimeOfDaySet(CliModulePtp(), seconds);
    if (ret != cAtOk)
        AtPrintc(cSevCritical, "ERROR: Set device ToD failed with ret = %s\r\n",
                 AtRet2String(ret));

    return (ret == cAtOk) ? cAtTrue : cAtFalse;
    }

eBool CmdPtpTimestampBypassEnable(char argc, char **argv)
    {
    eAtRet ret = cAtOk;
    AtUnused(argc);
    AtUnused(argv);
    ret = AtModulePtpTimestampBypassEnable(CliModulePtp(), cAtTrue);
    if (ret != cAtOk)
        AtPrintc(cSevCritical, "ERROR: Set PTP timestamp bypass enabled failed with ret = %s\r\n",
                 AtRet2String(ret));

    return cAtTrue;
    }

eBool CmdPtpTimestampBypassDisable(char argc, char **argv)
    {
    eAtRet ret = cAtOk;
    AtUnused(argc);
    AtUnused(argv);
    ret = AtModulePtpTimestampBypassEnable(CliModulePtp(), cAtFalse);
    if (ret != cAtOk)
        AtPrintc(cSevCritical, "ERROR: Set PTP timestamp bypass disabled failed with ret = %s\r\n",
                 AtRet2String(ret));

    return cAtTrue;
    }

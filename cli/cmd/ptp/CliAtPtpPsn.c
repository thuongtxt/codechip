/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PTP
 *
 * File        : CliAtPtpPsn.c
 *
 * Created Date: Jul 4, 2018
 *
 * Description : PTP PSN CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtModulePtp.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mPutAddressToCell(ret, addressString)                                   \
    do {                                                                        \
    if (ret == cAtOk)                                                           \
        StrToCell(tabPtr, row++, column, addressString);                        \
    else                                                                        \
        ColorStrToCell(tabPtr, row++, column,                                   \
                       (ret == cAtErrorModeNotSupport) ? sAtNotSupported : AtRet2String(ret), \
                       (ret == cAtErrorModeNotSupport) ? cSevNormal : cSevCritical);\
    }while(0)

/*--------------------------- Local typedefs ---------------------------------*/
typedef eAtRet (*AddressSetFunc)(AtPtpPsn self, const uint8 *address);
typedef eAtRet (*BoolAttributeSetFunc)(AtPtpPsn self, eBool enable);
typedef eAtRet (*AddressSetFuncWithIndex)(AtPtpIpPsn self, uint32 entryIndex, const uint8 *address);
typedef eAtRet (*BoolAttributeSetFuncWithIndex)(AtPtpIpPsn self, uint32 entryIndex, eBool enable);
typedef eAtRet (*CommonAddressSetFuncWithIndex)(AtPtpPsn self, uint32 entryIndex, const uint8 *address);
typedef eAtRet (*CommonBoolAttributeSetFuncWithIndex)(AtPtpPsn self, uint32 entryIndex, eBool enable);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char * cAtBoolStr[] = {
                            "en",
                            "dis"
                            };

static const eBool cAtBoolVal[]  = {
                             cAtTrue,
                             cAtFalse
                             };


/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool AddressAttributeSet(char *idListStr, eBool enable, uint8 *address,
                                 eAtPtpPsnType psnType,
                                 BoolAttributeSetFunc enableFunc,
                                 AddressSetFunc addressFunc)
    {
    uint8 i;
    uint32 bufferSize;
    uint32 numPorts;
    AtChannel *channelList;
    eBool success = cAtTrue;

    /* Get the shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliAtPtpPortListFromString(idListStr, channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 ...\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numPorts; i++)
        {
        AtPtpPort port = (AtPtpPort)channelList[i];
        AtPtpPsn psn = AtPtpPortPsnGet(port, psnType);
        eAtRet ret;

        ret = enableFunc(psn, enable);
        if (ret != cAtOk)
            AtPrintc(cSevCritical, "ERROR: Enable expected address of %s fail with ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)port),
                     AtRet2String(ret));

        if (ret == cAtOk && addressFunc != NULL)
            {
            ret = addressFunc(psn, address);
            if (ret != cAtOk)
                AtPrintc(cSevCritical, "ERROR: Set expected address of %s fail with ret = %s\r\n",
                         CliChannelIdStringGet((AtChannel)port),
                         AtRet2String(ret));
            }

        if (ret == cAtOk)
            continue;

        success = cAtFalse;
        }

    return success;
    }

static eBool AddressEntryAttributeSet(char *idListStr, char *entryIdStr, eBool enable, uint8 *address,
                                          eAtPtpPsnType psnType,
                                          CommonBoolAttributeSetFuncWithIndex enableFunc,
                                          CommonAddressSetFuncWithIndex addressFunc)
    {
    uint8 i;
    uint32 bufferSize;
    uint32 numPorts;
    AtChannel *channelList;
    eBool success = cAtTrue;
    uint32 entryIndex;

    /* Get the shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliAtPtpPortListFromString(idListStr, channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 ...\r\n");
        return cAtFalse;
        }

    /* Get entry ID */
    entryIndex = AtStrtoul(entryIdStr, NULL, 10);
    if (entryIndex == 0)
        {
        AtPrintc(cSevCritical, "Invalid entry index. Expected: 1, 2...\r\n");
        return cAtFalse;
        }
    entryIndex = CliId2DriverId(entryIndex);

    for (i = 0; i < numPorts; i++)
        {
        AtPtpPort port = (AtPtpPort)channelList[i];
        AtPtpPsn psn = AtPtpPortPsnGet(port, psnType);
        eAtRet ret;

        ret = enableFunc(psn, entryIndex, enable);
        if (ret != cAtOk)
            AtPrintc(cSevCritical, "ERROR: Enable expected address of %s fail with ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)port),
                     AtRet2String(ret));

        if (ret == cAtOk && addressFunc != NULL)
            {
            ret = addressFunc(psn, entryIndex, address);
            if (ret != cAtOk)
                AtPrintc(cSevCritical, "ERROR: Set expected address of %s fail with ret = %s\r\n",
                         CliChannelIdStringGet((AtChannel)port),
                         AtRet2String(ret));
            }

        if (ret == cAtOk)
            continue;

        success = cAtFalse;
        }

    return success;
    }

static eBool EthMacAddressSet(char argc, char **argv, BoolAttributeSetFunc enableFunc, AddressSetFunc addressFunc)
    {
    uint8 mac[cAtMacAddressLen] = {0};
    eBool enabled;
    eBool blResult;
    AddressSetFunc addressSetFunc = NULL;
    AtUnused(argc);

    mAtStrToEnum(cAtBoolStr,
                 cAtBoolVal,
                 argv[1],
                 enabled,
                 blResult);
    if (!blResult)
        {
        AtPrintc(cSevCritical, "Invalid enable mode. Expected: en/dis.\r\n");
        return cAtFalse;
        }

    if (argc > 2)
        {
        if (!CliAtModuleEthMacAddrStr2Byte(argv[2], mac))
            {
            AtPrintc(cSevCritical, "Invalid MAC address\r\n");
            return cAtFalse;
            }

        addressSetFunc = addressFunc;
        }

    return AddressAttributeSet(argv[0], enabled, mac, cAtPtpPsnTypeLayer2, enableFunc, addressSetFunc);
    }

static eBool EthMacAddressEntrySet(char argc, char **argv, CommonBoolAttributeSetFuncWithIndex enableFunc, CommonAddressSetFuncWithIndex addressFunc)
    {
    uint8 mac[cAtMacAddressLen] = {0};
    eBool enabled;
    eBool blResult;
    CommonAddressSetFuncWithIndex addressSetFunc = NULL;
    char entryIndexStr[] = "1";
    AtUnused(argc);

    mAtStrToEnum(cAtBoolStr,
                 cAtBoolVal,
                 argv[1],
                 enabled,
                 blResult);
    if (!blResult)
        {
        AtPrintc(cSevCritical, "Invalid enable mode. Expected: en/dis.\r\n");
        return cAtFalse;
        }

    if (argc > 2)
        {
        if (!CliAtModuleEthMacAddrStr2Byte(argv[2], mac))
            {
            AtPrintc(cSevCritical, "Invalid MAC address\r\n");
            return cAtFalse;
            }

        addressSetFunc = addressFunc;
        }

    return AddressEntryAttributeSet(argv[0], entryIndexStr, enabled, mac, cAtPtpPsnTypeLayer2, enableFunc, addressSetFunc);
    }

static eBool IpV4AddressSet(char argc, char **argv, BoolAttributeSetFunc enableFunc, AddressSetFunc addressFunc)
    {
    uint8 ipv4Addr[cAtIpv4AddressLen] = {0};
    eBool enabled;
    eBool blResult;
    AddressSetFunc addressSetFunc = NULL;
    AtUnused(argc);

    mAtStrToEnum(cAtBoolStr,
                 cAtBoolVal,
                 argv[1],
                 enabled,
                 blResult);
    if (!blResult)
        {
        AtPrintc(cSevCritical, "Invalid enable mode. Expected: en/dis.\r\n");
        return cAtFalse;
        }

    if (argc > 2)
        {
        if (!CliIpv4StringToArray(argv[2], ipv4Addr))
            {
            AtPrintc(cSevCritical, "Invalid IPv4 address\r\n");
            return cAtFalse;
            }

        addressSetFunc = addressFunc;
        }

    return AddressAttributeSet(argv[0], enabled, ipv4Addr, cAtPtpPsnTypeIpV4, enableFunc, addressSetFunc);
    }

static eBool IpV6AddressSet(char argc, char **argv, BoolAttributeSetFunc enableFunc, AddressSetFunc addressFunc)
    {
    uint8 ipv6Addr[cAtIpv6AddressLen] = {0};
    eBool enabled;
    eBool blResult;
    AddressSetFunc addressSetFunc = NULL;
    AtUnused(argc);

    mAtStrToEnum(cAtBoolStr,
                 cAtBoolVal,
                 argv[1],
                 enabled,
                 blResult);
    if (!blResult)
        {
        AtPrintc(cSevCritical, "Invalid enable mode. Expected: en/dis.\r\n");
        return cAtFalse;
        }

    if (argc > 2)
        {
        if (!CliIpV6StringToArray(argv[2], ipv6Addr))
            {
            AtPrintc(cSevCritical, "Invalid IPv6 address\r\n");
            return cAtFalse;
            }

        addressSetFunc = addressFunc;
        }

    return AddressAttributeSet(argv[0], enabled, ipv6Addr, cAtPtpPsnTypeIpV6, enableFunc, addressSetFunc);
    }


static eBool CommonIpAddressAttributeSet(char *idListStr, char *entryIdStr, eBool enable, uint8 *address,
                                          eAtPtpPsnType psnType,
                                          CommonBoolAttributeSetFuncWithIndex enableFunc,
                                          CommonAddressSetFuncWithIndex addressFunc)
    {
    uint8 i;
    uint32 bufferSize;
    uint32 numPorts;
    AtChannel *channelList;
    eBool success = cAtTrue;
    uint32 entryIndex;

    /* Get the shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliAtPtpPortListFromString(idListStr, channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 ...\r\n");
        return cAtFalse;
        }

    /* Get entry ID */
    entryIndex = AtStrtoul(entryIdStr, NULL, 10);
    if (entryIndex == 0)
        {
        AtPrintc(cSevCritical, "Invalid entry index. Expected: 1, 2...\r\n");
        return cAtFalse;
        }
    entryIndex = CliId2DriverId(entryIndex);

    for (i = 0; i < numPorts; i++)
        {
        AtPtpPort port = (AtPtpPort)channelList[i];
        AtPtpPsn psn = AtPtpPortPsnGet(port, psnType);
        eAtRet ret;

        ret = enableFunc(psn, entryIndex, enable);
        if (ret != cAtOk)
            AtPrintc(cSevCritical, "ERROR: Enable expected address of %s fail with ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)port),
                     AtRet2String(ret));

        if (ret == cAtOk && addressFunc != NULL)
            {
            ret = addressFunc(psn, entryIndex, address);
            if (ret != cAtOk)
                AtPrintc(cSevCritical, "ERROR: Set expected address of %s fail with ret = %s\r\n",
                         CliChannelIdStringGet((AtChannel)port),
                         AtRet2String(ret));
            }

        if (ret == cAtOk)
            continue;

        success = cAtFalse;
        }

    return success;
    }

static eBool CommonIpV4AddressSet(char argc, char **argv, CommonBoolAttributeSetFuncWithIndex enableFunc, CommonAddressSetFuncWithIndex addressFunc)
    {
    uint8 ipv4Addr[cAtIpv4AddressLen] = {0};
    eBool enabled;
    eBool blResult;
    CommonAddressSetFuncWithIndex addressSetFunc = NULL;
    AtUnused(argc);

    mAtStrToEnum(cAtBoolStr,
                 cAtBoolVal,
                 argv[2],
                 enabled,
                 blResult);
    if (!blResult)
        {
        AtPrintc(cSevCritical, "Invalid enable mode. Expected: en/dis.\r\n");
        return cAtFalse;
        }

    if (argc > 3)
        {
        if (!CliIpv4StringToArray(argv[3], ipv4Addr))
            {
            AtPrintc(cSevCritical, "Invalid IPv4 address\r\n");
            return cAtFalse;
            }

        addressSetFunc = addressFunc;
        }

    return CommonIpAddressAttributeSet(argv[0], argv[1], enabled, ipv4Addr, cAtPtpPsnTypeIpV4, enableFunc, addressSetFunc);
    }

static eBool CommonIpV6AddressSet(char argc, char **argv, CommonBoolAttributeSetFuncWithIndex enableFunc, CommonAddressSetFuncWithIndex addressFunc)
    {
    uint8 ipv6Addr[cAtIpv6AddressLen] = {0};
    eBool enabled;
    eBool blResult;
    CommonAddressSetFuncWithIndex addressSetFunc = NULL;
    AtUnused(argc);

    mAtStrToEnum(cAtBoolStr,
                 cAtBoolVal,
                 argv[2],
                 enabled,
                 blResult);
    if (!blResult)
        {
        AtPrintc(cSevCritical, "Invalid enable mode. Expected: en/dis.\r\n");
        return cAtFalse;
        }

    if (argc > 3)
        {
        if (!CliIpV6StringToArray(argv[3], ipv6Addr))
            {
            AtPrintc(cSevCritical, "Invalid IPv6 address\r\n");
            return cAtFalse;
            }

        addressSetFunc = addressFunc;
        }

    return CommonIpAddressAttributeSet(argv[0], argv[1], enabled, ipv6Addr, cAtPtpPsnTypeIpV6, enableFunc, addressSetFunc);
    }


static eBool IpUnicastAddressAttributeSet(char *idListStr, char *entryIdStr, eBool enable, uint8 *address,
                                          eAtPtpPsnType psnType,
                                          BoolAttributeSetFuncWithIndex enableFunc,
                                          AddressSetFuncWithIndex addressFunc)
    {
    uint8 i;
    uint32 bufferSize;
    uint32 numPorts;
    AtChannel *channelList;
    eBool success = cAtTrue;
    uint32 entryIndex;

    /* Get the shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliAtPtpPortListFromString(idListStr, channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 ...\r\n");
        return cAtFalse;
        }

    /* Get entry ID */
    entryIndex = AtStrtoul(entryIdStr, NULL, 10);
    if (entryIndex == 0)
        {
        AtPrintc(cSevCritical, "Invalid entry index. Expected: 1, 2...\r\n");
        return cAtFalse;
        }
    entryIndex = CliId2DriverId(entryIndex);

    for (i = 0; i < numPorts; i++)
        {
        AtPtpPort port = (AtPtpPort)channelList[i];
        AtPtpPsn psn = AtPtpPortPsnGet(port, psnType);
        eAtRet ret;

        ret = enableFunc((AtPtpIpPsn)psn, entryIndex, enable);
        if (ret != cAtOk)
            AtPrintc(cSevCritical, "ERROR: Enable expected address of %s fail with ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)port),
                     AtRet2String(ret));

        if (ret == cAtOk && addressFunc != NULL)
            {
            ret = addressFunc((AtPtpIpPsn)psn, entryIndex, address);
            if (ret != cAtOk)
                AtPrintc(cSevCritical, "ERROR: Set expected address of %s fail with ret = %s\r\n",
                         CliChannelIdStringGet((AtChannel)port),
                         AtRet2String(ret));
            }

        if (ret == cAtOk)
            continue;

        success = cAtFalse;
        }

    return success;
    }

static eBool IpV4UnicastAddressSet(char argc, char **argv, BoolAttributeSetFuncWithIndex enableFunc, AddressSetFuncWithIndex addressFunc)
    {
    uint8 ipv4Addr[cAtIpv4AddressLen] = {0};
    eBool enabled;
    eBool blResult;
    AddressSetFuncWithIndex addressSetFunc = NULL;
    AtUnused(argc);

    mAtStrToEnum(cAtBoolStr,
                 cAtBoolVal,
                 argv[2],
                 enabled,
                 blResult);
    if (!blResult)
        {
        AtPrintc(cSevCritical, "Invalid enable mode. Expected: en/dis.\r\n");
        return cAtFalse;
        }

    if (argc > 3)
        {
        if (!CliIpv4StringToArray(argv[3], ipv4Addr))
            {
            AtPrintc(cSevCritical, "Invalid IPv4 address\r\n");
            return cAtFalse;
            }

        addressSetFunc = addressFunc;
        }

    return IpUnicastAddressAttributeSet(argv[0], argv[1], enabled, ipv4Addr, cAtPtpPsnTypeIpV4, enableFunc, addressSetFunc);
    }

static eBool IpV6UnicastAddressSet(char argc, char **argv, BoolAttributeSetFuncWithIndex enableFunc, AddressSetFuncWithIndex addressFunc)
    {
    uint8 ipv6Addr[cAtIpv6AddressLen] = {0};
    eBool enabled;
    eBool blResult;
    AddressSetFuncWithIndex addressSetFunc = NULL;
    AtUnused(argc);

    mAtStrToEnum(cAtBoolStr,
                 cAtBoolVal,
                 argv[2],
                 enabled,
                 blResult);
    if (!blResult)
        {
        AtPrintc(cSevCritical, "Invalid enable mode. Expected: en/dis.\r\n");
        return cAtFalse;
        }

    if (argc > 3)
        {
        if (!CliIpV6StringToArray(argv[3], ipv6Addr))
            {
            AtPrintc(cSevCritical, "Invalid IPv6 address\r\n");
            return cAtFalse;
            }

        addressSetFunc = addressFunc;
        }

    return IpUnicastAddressAttributeSet(argv[0], argv[1], enabled, ipv6Addr, cAtPtpPsnTypeIpV6, enableFunc, addressSetFunc);
    }

static void HeadingFree(char** heading, uint8 numItem)
    {
    uint8 i;
    for (i = 0; i < numItem; i++)
        AtOsalMemFree(heading[i]);
    AtOsalMemFree(heading);
    }

static uint32 PsnListFromPortString(char *portIdList, AtPtpPsn *psnList, uint32 bufferSize, eAtPtpPsnType psnType)
    {
    uint32 numPorts;
    AtChannel *channelList;
    uint8 i;
    uint32 numPsn = 0;

    /* Get the shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliAtPtpPortListFromString(portIdList, channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 ...\r\n");
        return 0;
        }

    for (i = 0; i < numPorts; i++)
        {
        AtPtpPsn psn = AtPtpPortPsnGet((AtPtpPort)channelList[i], psnType);

        if (psn == NULL)
            continue;

        psnList[numPsn++] = psn;
        if (numPsn >= bufferSize)
            return numPsn;
        }

    return numPsn;
    }

static eBool CliAtPtpPsnShow(char argc, char **argv, eAtPtpPsnType ipPsnType, char *addr2StringFunc(const uint8 *))
    {
    tTab *tabPtr = NULL;
    char **heading;
    uint8 i, j;
    uint32 bufferSize;
    AtPtpPsn *psnList;
    uint32 numPsn;
    uint32 maxUnicastDest = 0, maxUnicastSrc = 0, maxOtherAddr = 0;
    uint32 numRows;
    uint8 row = 0;

    AtUnused(argc);

    /* Get the shared ID buffer */
    psnList = (AtPtpPsn*)CliSharedObjectListGet(&bufferSize);
    if ((numPsn = (uint8)PsnListFromPortString(argv[0], psnList, bufferSize, ipPsnType)) == 0)
        {
        AtPrintc(cSevInfo, "No PSN to be showed\r\n");
        return cAtTrue;
        }

    /* Allocate memory */
    heading = (void*)AtOsalMemAlloc((numPsn + 1UL) * sizeof(char *));
    if (heading == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate memory for table heading\r\n");
        return cAtFalse;
        }

    heading[0] = (char*)AtOsalMemAlloc(16);
    if (heading[0] == NULL)
        {
        AtOsalMemFree(heading);
        return cAtFalse;
        }

    AtOsalMemInit((char*)heading[0], 0, 16);
    for (i = 1; i <= numPsn; i++)
        {
        heading[i] = (char*)AtOsalMemAlloc(3);
        if (heading[i] == NULL)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot allocate memory for table heading\r\n");
            HeadingFree((char**)heading, (uint8)(numPsn + 1));
            return cAtFalse;
            }

        AtOsalMemInit((char*)heading[i], 0, 3);
        }

    /* Create table with titles */
    AtSprintf((void*)heading[0], "%s", "Port Id");
    for (i = 1; i <= numPsn; i++)
        AtSprintf((void*)heading[i], "%s", AtChannelIdString((AtChannel)AtPtpPsnPortGet(psnList[i - 1])));

    for (i = 0; i < numPsn; i++)
        {
        AtPtpPsn psn = psnList[i];
        uint32 numUnicastAddr;

        numUnicastAddr = AtPtpPsnMaxNumDestAddressEntryGet(psn);
        if (numUnicastAddr > maxUnicastDest)
            maxUnicastDest = numUnicastAddr;

        numUnicastAddr = AtPtpPsnMaxNumSourceAddressEntryGet(psn);
        if (numUnicastAddr > maxUnicastSrc)
            maxUnicastSrc = numUnicastAddr;

        maxOtherAddr = 2; /* Anycast dest. + Anycast source */
        }
    numRows = maxUnicastDest * 2U + maxUnicastSrc * 2U + maxOtherAddr;

    tabPtr = TableAlloc(numRows, numPsn + 1UL, (void *)heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        HeadingFree((char**)heading, (uint8)(numPsn + 1));
        return cAtFalse;
        }

    row = 0;
    for (i = 0; i < maxUnicastDest; i++)
        {
        char buf[32] = {0};

        AtSprintf(buf, "DA #%d", i + 1U);
        StrToCell(tabPtr, row++, 0, buf);
        AtSprintf(buf, "DA #%d Enable", i + 1U);
        StrToCell(tabPtr, row++, 0, buf);
        }

    StrToCell(tabPtr, row++, 0, "Anycast DA Enable");

    for (i = 0; i < maxUnicastSrc; i++)
        {
        char buf[32] = {0};

        AtSprintf(buf, "SA #%d", i + 1U);
        StrToCell(tabPtr, row++, 0, buf);
        AtSprintf(buf, "SA #%d Enable", i + 1U);
        StrToCell(tabPtr, row++, 0, buf);
        }

    StrToCell(tabPtr, row++, 0, "Anycast SA Enable");

    for (i = 0; i < numPsn; i++)
        {
        uint8 ipAddr[cAtIpv6AddressLen];
        eBool enabled;
        AtPtpPsn psn = psnList[i];
        eAtRet ret;
        uint8 column = (uint8)(i + 1);

        row = 0;
        for (j = 0; j < maxUnicastDest; j++)
            {
            if (psn)
                {
                /* Unicast dest. address */
                ret = AtPtpPsnExpectedDestAddressEntryGet(psn, j, ipAddr);
                mPutAddressToCell(ret, addr2StringFunc(ipAddr));

                /* Unicast expected dest. address enable */
                enabled = AtPtpPsnExpectedDestAddressEntryIsEnabled(psn, j);
                StrToCell(tabPtr, row++, column, CliBoolToString(enabled));
                }
            else
                {
                StrToCell(tabPtr, row++, column, sAtNotApplicable);
                StrToCell(tabPtr, row++, column, sAtNotApplicable);
                }
            }

        if (psn)
            {
            /* Anycast expected dest. address enable */
            enabled = AtPtpPsnExpectedAnycastDestAddressIsEnabled(psn);
            StrToCell(tabPtr, row++, column, CliBoolToString(enabled));
            }
        else
            {
            StrToCell(tabPtr, row++, column, sAtNotApplicable);
            }

        for (j = 0; j < maxUnicastSrc; j++)
            {
            if (psn)
                {
                /* Unicast source address */
                ret = AtPtpPsnExpectedSourceAddressEntryGet(psn, j, ipAddr);
                mPutAddressToCell(ret, (void*)addr2StringFunc(ipAddr));

                /* Unicast expected source address enable */
                enabled = AtPtpPsnExpectedSourceAddressEntryIsEnabled(psn, j);
                StrToCell(tabPtr, row++, column, CliBoolToString(enabled));
                }
            else
                {
                StrToCell(tabPtr, row++, column, sAtNotApplicable);
                StrToCell(tabPtr, row++, column, sAtNotApplicable);
                }
            }

        if (psn)
            {
            /* Anycast expected source address enable */
            enabled = AtPtpPsnExpectedAnycastSourceAddressIsEnabled(psn);
            StrToCell(tabPtr, row++, column, CliBoolToString(enabled));
            }
        else
            {
            StrToCell(tabPtr, row++, column, sAtNotApplicable);
            }
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    /* Free memory */
    HeadingFree((char**)heading, (uint8)(numPsn + 1));

    return cAtTrue;
    }

static eBool CliAtPtpIpPsnShow(char argc, char **argv, eAtPtpPsnType ipPsnType, char *addr2StringFunc(const uint8 *))
    {
    tTab *tabPtr = NULL;
    char **heading;
    uint8 i, j;
    uint32 bufferSize;
    AtPtpPsn *psnList;
    uint32 numPsn;
    uint32 maxUnicastDest = 0, maxUnicastSrc = 0, maxOtherAddr = 0;
    uint32 numRows;
    uint8 row = 0;

    AtUnused(argc);

    /* Get the shared ID buffer */
    psnList = (AtPtpPsn*)CliSharedObjectListGet(&bufferSize);
    if ((numPsn = (uint8)PsnListFromPortString(argv[0], psnList, bufferSize, ipPsnType)) == 0)
        {
        AtPrintc(cSevInfo, "No PSN to be showed\r\n");
        return cAtTrue;
        }

    /* Allocate memory */
    heading = (void*)AtOsalMemAlloc((numPsn + 1UL) * sizeof(char *));
    if (heading == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate memory for table heading\r\n");
        return cAtFalse;
        }

    heading[0] = (char*)AtOsalMemAlloc(16);
    if (heading[0] == NULL)
        {
        AtOsalMemFree(heading);
        return cAtFalse;
        }

    AtOsalMemInit((char*)heading[0], 0, 16);
    for (i = 1; i <= numPsn; i++)
        {
        heading[i] = (char*)AtOsalMemAlloc(3);
        if (heading[i] == NULL)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot allocate memory for table heading\r\n");
            HeadingFree((char**)heading, (uint8)(numPsn + 1));
            return cAtFalse;
            }

        AtOsalMemInit((char*)heading[i], 0, 3);
        }

    /* Create table with titles */
    AtSprintf((void*)heading[0], "%s", "Port Id");
    for (i = 1; i <= numPsn; i++)
        AtSprintf((void*)heading[i], "%s", AtChannelIdString((AtChannel)AtPtpPsnPortGet(psnList[i - 1])));

    for (i = 0; i < numPsn; i++)
        {
        AtPtpPsn psn = psnList[i];
        uint32 numUnicastAddr;

        numUnicastAddr = AtPtpIpPsnMaxNumUnicastDestAddressEntryGet((AtPtpIpPsn)psn);
        if (numUnicastAddr > maxUnicastDest)
            maxUnicastDest = numUnicastAddr;

        numUnicastAddr = AtPtpIpPsnMaxNumUnicastSourceAddressEntryGet((AtPtpIpPsn)psn);
        if (numUnicastAddr > maxUnicastSrc)
            maxUnicastSrc = numUnicastAddr;

        maxOtherAddr = 4; /* 2 x Multicast dest. + Anycast dest. + Anycast source */
        }
    numRows = maxUnicastDest * 2U + maxUnicastSrc * 2U + maxOtherAddr;

    tabPtr = TableAlloc(numRows, numPsn + 1UL, (void *)heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        HeadingFree((char**)heading, (uint8)(numPsn + 1));
        return cAtFalse;
        }

    row = 0;
    for (i = 0; i < maxUnicastDest; i++)
        {
        char buf[32] = {0};

        AtSprintf(buf, "Unicast DA #%d", i + 1U);
        StrToCell(tabPtr, row++, 0, buf);
        AtSprintf(buf, "Unicast DA #%d Enable", i + 1U);
        StrToCell(tabPtr, row++, 0, buf);
        }

    StrToCell(tabPtr, row++, 0, "Multicast DA");
    StrToCell(tabPtr, row++, 0, "Multicast DA Enable");
    StrToCell(tabPtr, row++, 0, "Anycast DA Enable");

    for (i = 0; i < maxUnicastSrc; i++)
        {
        char buf[32] = {0};

        AtSprintf(buf, "Unicast SA #%d", i + 1U);
        StrToCell(tabPtr, row++, 0, buf);
        AtSprintf(buf, "Unicast SA #%d Enable", i + 1U);
        StrToCell(tabPtr, row++, 0, buf);
        }

    StrToCell(tabPtr, row++, 0, "Anycast SA Enable");

    for (i = 0; i < numPsn; i++)
        {
        uint8 ipAddr[cAtIpv6AddressLen];
        eBool enabled;
        AtPtpPsn psn = psnList[i];
        eAtRet ret;
        uint8 column = (uint8)(i + 1);

        row = 0;
        for (j = 0; j < maxUnicastDest; j++)
            {
            if (psn)
                {
                /* Unicast dest. address */
                ret = AtPtpIpPsnExpectedUnicastDestAddressEntryGet((AtPtpIpPsn)psn, j, ipAddr);
                mPutAddressToCell(ret, addr2StringFunc(ipAddr));

                /* Unicast expected dest. address enable */
                enabled = AtPtpIpPsnExpectedUnicastDestAddressEntryIsEnabled((AtPtpIpPsn)psn, j);
                StrToCell(tabPtr, row++, column, CliBoolToString(enabled));
                }
            else
                {
                StrToCell(tabPtr, row++, column, sAtNotApplicable);
                StrToCell(tabPtr, row++, column, sAtNotApplicable);
                }
            }

        if (psn)
            {
            /* Multicast dest. address */
            ret = AtPtpPsnExpectedMcastDestAddressGet(psn, ipAddr);
            mPutAddressToCell(ret, (void*)addr2StringFunc(ipAddr));

            /* Multicast expected dest. address enable */
            enabled = AtPtpPsnExpectedMcastDestAddressIsEnabled(psn);
            StrToCell(tabPtr, row++, column, CliBoolToString(enabled));

            /* Anycast expected dest. address enable */
            enabled = AtPtpPsnExpectedAnycastDestAddressIsEnabled(psn);
            StrToCell(tabPtr, row++, column, CliBoolToString(enabled));
            }
        else
            {
            StrToCell(tabPtr, row++, column, sAtNotApplicable);
            StrToCell(tabPtr, row++, column, sAtNotApplicable);
            StrToCell(tabPtr, row++, column, sAtNotApplicable);
            }

        for (j = 0; j < maxUnicastSrc; j++)
            {
            if (psn)
                {
                /* Unicast source address */
                ret = AtPtpIpPsnExpectedUnicastSourceAddressEntryGet((AtPtpIpPsn)psn, j, ipAddr);
                mPutAddressToCell(ret, (void*)addr2StringFunc(ipAddr));

                /* Unicast expected source address enable */
                enabled = AtPtpIpPsnExpectedUnicastSourceAddressEntryIsEnabled((AtPtpIpPsn)psn, j);
                StrToCell(tabPtr, row++, column, CliBoolToString(enabled));
                }
            else
                {
                StrToCell(tabPtr, row++, column, sAtNotApplicable);
                StrToCell(tabPtr, row++, column, sAtNotApplicable);
                }
            }

        if (psn)
            {
            /* Anycast expected source address enable */
            enabled = AtPtpPsnExpectedAnycastSourceAddressIsEnabled(psn);
            StrToCell(tabPtr, row++, column, CliBoolToString(enabled));
            }
        else
            {
            StrToCell(tabPtr, row++, column, sAtNotApplicable);
            }
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    /* Free memory */
    HeadingFree((char**)heading, (uint8)(numPsn + 1));

    return cAtTrue;
    }

eBool CmdAtPtpPortPsnEthExpectedUnicastDestMacSet(char argc, char **argv)
    {
    return EthMacAddressSet(argc, argv,
                            AtPtpPsnExpectedUnicastDestAddressEnable,
                            AtPtpPsnExpectedUnicastDestAddressSet);
    }

eBool CmdAtPtpPortPsnEthExpectedMulticastDestMacSet(char argc, char **argv)
    {
    return EthMacAddressSet(argc, argv,
                            AtPtpPsnExpectedMcastDestAddressEnable,
                            AtPtpPsnExpectedMcastDestAddressSet);
    }

eBool CmdAtPtpPortPsnEthExpectedAnycastDestMacSet(char argc, char **argv)
    {
    return EthMacAddressSet(argc, argv,
                            AtPtpPsnExpectedAnycastDestAddressEnable,
                            NULL);
    }

eBool CmdAtPtpPortPsnEthExpectedUnicastSourceMacSet(char argc, char **argv)
    {
    return EthMacAddressSet(argc, argv,
                            AtPtpPsnExpectedUnicastSourceAddressEnable,
                            AtPtpPsnExpectedUnicastSourceAddressSet);
    }

eBool CmdAtPtpPortPsnEthExpectedAnycastSourceMacSet(char argc, char **argv)
    {
    return EthMacAddressSet(argc, argv,
                            AtPtpPsnExpectedAnycastSourceAddressEnable,
                            NULL);
    }

eBool CmdAtPtpPortEthPsnShow(char argc, char **argv)
    {
    tTab *tabPtr = NULL;
    char **heading;
    uint8 i;
    uint32 bufferSize;
    uint32 numPorts;
    AtChannel *channelList;
    const char *colStr[] = {"Unicast DA", "Unicast DA Enable", "Multicast DA",
                            "Multicast DA Enable", "Anycast DA Enable",
                            "Unicast SA", "Unicast SA Enable", "Anycast SA Enable"};
    uint8 row = 0;

    AtUnused(argc);

    /* Get the shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliAtPtpPortListFromString(argv[0], channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 ...\r\n");
        return cAtFalse;
        }

    /* Allocate memory */
    heading = (void*)AtOsalMemAlloc((numPorts + 1UL) * sizeof(char *));
    if (heading == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate memory for table heading\r\n");
        return cAtFalse;
        }

    heading[0] = (char*)AtOsalMemAlloc(16);
    if (heading[0] == NULL)
        {
        AtOsalMemFree(heading);
        return cAtFalse;
        }

    AtOsalMemInit((char*)heading[0], 0, 16);
    for (i = 1; i <= numPorts; i++)
        {
        heading[i] = (char*)AtOsalMemAlloc(3);
        if (heading[i] == NULL)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot allocate memory for table heading\r\n");
            HeadingFree((char**)heading, (uint8)(numPorts + 1));
            return cAtFalse;
            }

        AtOsalMemInit((char*)heading[i], 0, 3);
        }

    /* Create table with titles */
    AtSprintf((void*)heading[0], "%s", "Port Id");
    for (i = 1; i <= numPorts; i++)
        AtSprintf((void*)heading[i], "%s", AtChannelIdString(channelList[i - 1]));

    tabPtr = TableAlloc(mCount(colStr), numPorts + 1UL, (void *)heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        return cAtFalse;
        }

    row = 0;
    for (i = 0; i < mCount(colStr); i++)
        StrToCell(tabPtr, row++, 0, colStr[i]);

    for (i = 0; i < numPorts; i++)
        {
        uint8 macAddr[cAtMacAddressLen];
        eBool enabled;
        AtPtpPort port = (AtPtpPort)channelList[i];
        AtPtpPsn psn = AtPtpPortPsnGet(port, cAtPtpPsnTypeLayer2);
        eAtRet ret;
        uint8 column = (uint8)(i + 1);

        row = 0;

        /* Unicast dest. address */
        ret = AtPtpPsnExpectedUnicastDestAddressGet(psn, macAddr);
        mPutAddressToCell(ret, (void*)CliEthPortMacAddressByte2Str(macAddr));

        /* Unicast expected dest. address enable */
        enabled = AtPtpPsnExpectedUnicastDestAddressIsEnabled(psn);
        StrToCell(tabPtr, row++, column, CliBoolToString(enabled));

        /* Multicast dest. address */
        ret = AtPtpPsnExpectedMcastDestAddressGet(psn, macAddr);
        mPutAddressToCell(ret, (void*)CliEthPortMacAddressByte2Str(macAddr));

        /* Multicast expected dest. address enable */
        enabled = AtPtpPsnExpectedMcastDestAddressIsEnabled(psn);
        StrToCell(tabPtr, row++, column, CliBoolToString(enabled));

        /* Anycast expected dest. address enable */
        enabled = AtPtpPsnExpectedAnycastDestAddressIsEnabled(psn);
        StrToCell(tabPtr, row++, column, CliBoolToString(enabled));

        /* Unicast source address */
        ret = AtPtpPsnExpectedUnicastSourceAddressGet(psn, macAddr);
        mPutAddressToCell(ret, (void*)CliEthPortMacAddressByte2Str(macAddr));

        /* Unicast expected source address enable */
        enabled = AtPtpPsnExpectedUnicastSourceAddressIsEnabled(psn);
        StrToCell(tabPtr, row++, column, CliBoolToString(enabled));

        /* Anycast expected source address enable */
        enabled = AtPtpPsnExpectedAnycastSourceAddressIsEnabled(psn);
        StrToCell(tabPtr, row++, column, CliBoolToString(enabled));
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    /* Free memory */
    HeadingFree((char**)heading, (uint8)(numPorts + 1));

    return cAtTrue;
    }

eBool CmdAtPtpPortIpV4PsnExpectedUnicastDestAddressSet(char argc, char **argv)
    {
    return IpV4UnicastAddressSet(argc, argv,
                                 AtPtpIpPsnExpectedUnicastDestAddressEntryEnable,
                                 AtPtpIpPsnExpectedUnicastDestAddressEntrySet);
    }

eBool CmdAtPtpPortIpV4PsnExpectedMcastDestAddressSet(char argc, char **argv)
    {
    return IpV4AddressSet(argc, argv,
                          AtPtpPsnExpectedMcastDestAddressEnable,
                          AtPtpPsnExpectedMcastDestAddressSet);
    }

eBool CmdAtPtpPortIpV4PsnExpectedAnycastDestAddressSet(char argc, char **argv)
    {
    return IpV4AddressSet(argc, argv,
                          AtPtpPsnExpectedAnycastDestAddressEnable,
                          NULL);
    }

eBool CmdAtPtpPortIpV4PsnExpectedUnicastSourceAddressSet(char argc, char **argv)
    {
    return IpV4UnicastAddressSet(argc, argv,
                                 AtPtpIpPsnExpectedUnicastSourceAddressEntryEnable,
                                 AtPtpIpPsnExpectedUnicastSourceAddressEntrySet);
    }

eBool CmdAtPtpPortIpV4PsnExpectedAnycastSourceAddressSet(char argc, char **argv)
    {
    return IpV4AddressSet(argc, argv,
                          AtPtpPsnExpectedAnycastSourceAddressEnable,
                          NULL);
    }

eBool CmdAtPtpPortIpV4PsnShow(char argc, char **argv)
    {
    return CliAtPtpIpPsnShow(argc, argv, cAtPtpPsnTypeIpV4, CliIpV4ArrayToString);
    }

eBool CmdAtPtpPortIpV6PsnExpectedUnicastDestAddressSet(char argc, char **argv)
    {
    return IpV6UnicastAddressSet(argc, argv,
                                 AtPtpIpPsnExpectedUnicastDestAddressEntryEnable,
                                 AtPtpIpPsnExpectedUnicastDestAddressEntrySet);
    }

eBool CmdAtPtpPortIpV6PsnExpectedMcastDestAddressSet(char argc, char **argv)
    {
    return IpV6AddressSet(argc, argv,
                          AtPtpPsnExpectedMcastDestAddressEnable,
                          AtPtpPsnExpectedMcastDestAddressSet);
    }

eBool CmdAtPtpPortIpV6PsnExpectedAnycastDestAddressSet(char argc, char **argv)
    {
    return IpV6AddressSet(argc, argv,
                          AtPtpPsnExpectedAnycastDestAddressEnable,
                          NULL);
    }

eBool CmdAtPtpPortIpV6PsnExpectedUnicastSourceAddressSet(char argc, char **argv)
    {
    return IpV6UnicastAddressSet(argc, argv,
                                 AtPtpIpPsnExpectedUnicastSourceAddressEntryEnable,
                                 AtPtpIpPsnExpectedUnicastSourceAddressEntrySet);
    }

eBool CmdAtPtpPortIpV6PsnExpectedAnycastSourceAddressSet(char argc, char **argv)
    {
    return IpV6AddressSet(argc, argv,
                          AtPtpPsnExpectedAnycastSourceAddressEnable,
                          NULL);
    }

eBool CmdAtPtpPortIpv6PsnShow(char argc, char **argv)
    {
    return CliAtPtpIpPsnShow(argc, argv, cAtPtpPsnTypeIpV6, CliIpV6ArrayToString);
    }

eBool CmdAtPtpPortPsnEthExpectedDestMacSet(char argc, char **argv)
    {
    return EthMacAddressEntrySet(argc, argv,
                            AtPtpPsnExpectedDestAddressEntryEnable,
                            AtPtpPsnExpectedDestAddressEntrySet);
    }

eBool CmdAtPtpPortPsnEthExpectedSourceMacSet(char argc, char **argv)
    {
    return EthMacAddressEntrySet(argc, argv,
                            AtPtpPsnExpectedSourceAddressEntryEnable,
                            AtPtpPsnExpectedSourceAddressEntrySet);
    }

eBool CmdAtPtpPortIpV4PsnExpectedDestAddressSet(char argc, char **argv)
    {
    return CommonIpV4AddressSet(argc, argv,
                                 AtPtpPsnExpectedDestAddressEntryEnable,
                                 AtPtpPsnExpectedDestAddressEntrySet);
    }

eBool CmdAtPtpPortIpV4PsnExpectedSourceAddressSet(char argc, char **argv)
    {
    return CommonIpV4AddressSet(argc, argv,
                                 AtPtpPsnExpectedSourceAddressEntryEnable,
                                 AtPtpPsnExpectedSourceAddressEntrySet);
    }

eBool CmdAtPtpPortIpV6PsnExpectedDestAddressSet(char argc, char **argv)
    {
    return CommonIpV6AddressSet(argc, argv,
                                 AtPtpPsnExpectedDestAddressEntryEnable,
                                 AtPtpPsnExpectedDestAddressEntrySet);
    }

eBool CmdAtPtpPortIpV6PsnExpectedSourceAddressSet(char argc, char **argv)
    {
    return CommonIpV6AddressSet(argc, argv,
                                 AtPtpPsnExpectedSourceAddressEntryEnable,
                                 AtPtpPsnExpectedSourceAddressEntrySet);
    }

eBool CmdAtPtpPortEthAnyPsnShow(char argc, char **argv)
    {
    return CliAtPtpPsnShow(argc, argv, cAtPtpPsnTypeLayer2, CliEthPortMacAddressByte2Str);
    }

eBool CmdAtPtpPortIpV4AnyPsnShow(char argc, char **argv)
    {
    return CliAtPtpPsnShow(argc, argv, cAtPtpPsnTypeIpV4, CliIpV4ArrayToString);
    }

eBool CmdAtPtpPortIpV6AnyPsnShow(char argc, char **argv)
    {
    return CliAtPtpPsnShow(argc, argv, cAtPtpPsnTypeIpV6, CliIpV6ArrayToString);
    }

eBool CmdAtPtpPortExpectVlanSet(char argc, char **argv)
    {
    tAtEthVlanDesc desc;
    tAtEthVlanTag *sVlan, *cVlan;
    eBool success = cAtTrue;
    uint32 bufferSize;
    uint32 numPorts, i;
    AtChannel *channelList;
    AtUnused(argc);

    /* Get the shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliAtPtpPortListFromString(argv[0], channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 ...\r\n");
        return cAtFalse;
        }

    AtOsalMemInit(&desc, 0, sizeof(desc));

    /* Get S-VLAN and C-VLAN */
    sVlan = cVlan = NULL;
    if (!CliEthFlowVlanGet(argv[1], argv[2], &desc))
        return cAtFalse;
    if (desc.numberOfVlans > 0)
        {
        cVlan = &(desc.vlans[0]);
        if (desc.numberOfVlans == 2)
            sVlan = &(desc.vlans[1]);
        }

    for (i = 0; i < numPorts; i++)
        {
        eAtRet ret = cAtOk;
        AtPtpPort port = (AtPtpPort)channelList[i];
        AtPtpPsn psn = AtPtpPortPsnGet(port, cAtPtpPsnTypeLayer2);

        ret = AtPtpPsnExpectedCVlanSet(psn, cVlan);
        ret |= AtPtpPsnExpectedSVlanSet(psn, sVlan);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Can not configure for %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)port),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtPtpPortExpectedVlanShow(char argc, char **argv)
    {
    const char *pHeading[] = { "Port", "vlan" };
    tTab *tabPtr;
    static char idString[32];
    uint32 bufferSize;
    uint32 numPorts, i;
    AtChannel *channelList;
    AtUnused(argc);

    /* Get the shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliAtPtpPortListFromString(argv[0], channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 ...\r\n");
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numPorts, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numPorts; i++)
        {
        tAtEthVlanTag cVlan = {0, 0, 0};
        tAtEthVlanTag sVlan = {0, 0, 0};
        AtPtpPort port = (AtPtpPort)channelList[i];
        AtPtpPsn psn = AtPtpPortPsnGet(port, cAtPtpPsnTypeLayer2);

        AtSprintf(idString, "%s", AtObjectToString((AtObject)port));
        StrToCell(tabPtr, i, 0, idString);
        StrToCell(tabPtr, i, 1, CliVlans2Str(AtPtpPsnExpectedCVlanGet(psn, &cVlan), AtPtpPsnExpectedSVlanGet(psn, &sVlan)));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PTP
 *
 * File        : CliAtPtpPort.c
 *
 * Created Date: Jul 2, 2018
 *
 * Description : PTP port CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtModulePtp.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mPutCounter(counterType, counterKind)                                   \
        CliCounterTypePrintToCell(tabPtr, counter_i++, port_i + 1UL,            \
                                  CounterGet(port, cAtPtpPortCounterType##counterType), \
                                  counterKind, #counterType)

/*--------------------------- Local typedefs ---------------------------------*/
typedef eAtRet (*PortAttributeSetFunc)(AtPtpPort self, uint32 value);
typedef eAtRet (*PortAddressSetFunc)(AtPtpPort self, uint8 *address);
typedef eAtRet (*PortBoolAttributeSetFunc)(AtPtpPort self, eBool enable);
typedef eAtRet (*EnableFunc)(AtChannel self, eBool enabled);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char * cAtPtpPortStateStr[] = {
                                     "master",
                                     "slave",
                                     "passive"
                                     };

static const uint32 cAtPtpPortStateVal[]  = {
                                             cAtPtpPortStateMaster,
                                             cAtPtpPortStateSlave,
                                             cAtPtpPortStatePassive
                                            };

static const char * cAtPtpPortStepModeStr[] = {
                                     "one-step",
                                     "two-step"
                                     };

static const uint32 cAtPtpPortStepModeVal[]  = {
                                             cAtPtpStepModeOneStep,
                                             cAtPtpStepModeTwoStep
                                            };

static const char * cAtPtpTransportTypeStr[] = {
                                     "unknown",
                                     "l2tp",
                                     "ipv4",
                                     "ipv6",
                                     "ipv4vpn",
                                     "ipv6vpn",
                                     "any"
                                     };

static const uint32 cAtPtpTransportTypeVal[]  = {
                                           cAtPtpTransportTypeUnknown,
                                           cAtPtpTransportTypeL2TP,
                                           cAtPtpTransportTypeIpV4,
                                           cAtPtpTransportTypeIpV6,
                                           cAtPtpTransportTypeIpV4Vpn,
                                           cAtPtpTransportTypeIpV6Vpn,
                                           cAtPtpTransportTypeAny
                                          };


/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static eBool Enable(char argc, char **argv, eBool enable, EnableFunc enableFunc)
    {
    uint8 i;
    uint32 bufferSize;
    uint32 numPorts;
    AtChannel *channelList;
    eBool success = cAtTrue;
    AtUnused(argc);

    /* Get the shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliAtPtpPortListFromString(argv[0], channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 ...\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numPorts; i++)
        {
        AtChannel port = channelList[i];
        eAtRet ret;

        ret = enableFunc(port, enable);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical, "ERROR: %s %s fail with ret = %s\r\n",
                 enable ? "Enable" : "Disable",
                 CliChannelIdStringGet((AtChannel)port),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

static eBool AttributeSet(char *idListStr, uint32 value, PortAttributeSetFunc setFunc)
    {
    uint8 i;
    uint32 bufferSize;
    uint32 numPorts;
    AtChannel *channelList;
    eBool success = cAtTrue;

    /* Get the shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliAtPtpPortListFromString(idListStr, channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 ...\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numPorts; i++)
        {
        AtPtpPort port = (AtPtpPort)channelList[i];
        eAtRet ret;

        ret = setFunc(port, value);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical, "ERROR: %s fail with ret = %s\r\n",
                 CliChannelIdStringGet((AtChannel)port),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

static void HeadingFree(char** heading, uint8 numItem)
    {
    uint8 i;
    for (i = 0; i < numItem; i++)
        AtOsalMemFree(heading[i]);
    AtOsalMemFree(heading);
    }

static eBool PortCounterGet(AtPtpPort *ports, uint32 numPorts, char argc, char **argv)
    {
    uint32 port_i;
    tTab *tabPtr = NULL;
    char **heading;
    eBool silent = cAtFalse;
    eAtHistoryReadingMode readingMode;
    uint32 (*CounterGet)(AtChannel self, uint16 counterType);
    AtUnused(argc);

    /* Get counter mode */
    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    CounterGet = AtChannelCounterGet;
    if (readingMode == cAtHistoryReadingModeReadToClear)
        CounterGet = AtChannelCounterClear;

    /* Allocate memory */
    heading = (void*)AtOsalMemAlloc((numPorts + 1UL) * sizeof(char *));
    if (heading == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate memory for table heading\r\n");
        return cAtFalse;
        }

    heading[0] = (char*)AtOsalMemAlloc(16);
    if (heading[0] == NULL)
        {
        AtOsalMemFree(heading);
        return cAtFalse;
        }

    AtOsalMemInit((char*)heading[0], 0, 16);
    for (port_i = 1; port_i <= numPorts; port_i++)
        {
        heading[port_i] = (char*)AtOsalMemAlloc(3);
        if (heading[port_i] == NULL)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot allocate memory for table heading\r\n");
            HeadingFree((char**)heading, (uint8)(numPorts + 1));
            return cAtFalse;
            }

        AtOsalMemInit((char*)heading[port_i], 0, 3);
        }

    /* Create table with titles */
    AtSprintf((void*)heading[0], "%s", "Port Id");
    for (port_i = 1; port_i <= numPorts; port_i++)
        AtSprintf((void*)heading[port_i], "%s", AtChannelIdString((AtChannel)ports[port_i- 1]));

    if (argc > 2)
        silent = CliHistoryReadingShouldSilent(readingMode, argv[2]);

    if (!silent)
        {
        tabPtr = TableAlloc(0, numPorts + 1UL, (void *)heading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "Cannot allocate table\r\n");
            return cAtFalse;
            }
        }

    for (port_i = 0; port_i < numPorts; port_i++)
        {
        uint32 counter_i = 0;
        AtChannel port = (AtChannel)ports[port_i];

        mPutCounter(TxPackets,          cAtCounterTypeNeutral);
        mPutCounter(TxSyncPackets,      cAtCounterTypeNeutral);
        mPutCounter(TxFollowUpPackets,  cAtCounterTypeNeutral);
        mPutCounter(TxDelayReqPackets,  cAtCounterTypeNeutral);
        mPutCounter(TxDelayRespPackets, cAtCounterTypeNeutral);

        mPutCounter(RxPackets,          cAtCounterTypeNeutral);
        mPutCounter(RxSyncPackets,      cAtCounterTypeNeutral);
        mPutCounter(RxFollowUpPackets,  cAtCounterTypeNeutral);
        mPutCounter(RxDelayReqPackets,  cAtCounterTypeNeutral);
        mPutCounter(RxDelayRespPackets, cAtCounterTypeNeutral);
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    /* Free memory */
    HeadingFree((char**)heading, (uint8)(numPorts + 1));

    return cAtTrue;
    }

#if 0
static eBool BoolAttributeSet(char *idListStr, eBool value, PortBoolAttributeSetFunc setFunc)
    {
    uint8 i;
    uint32 bufferSize;
    uint32 numPorts;
    AtChannel *channelList;
    eBool success = cAtTrue;

    /* Get the shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliAtPtpPortListFromString(idListStr, channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 ...\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numPorts; i++)
        {
        AtPtpPort port = (AtPtpPort)channelList[i];
        eAtRet ret;

        ret = setFunc(port, value);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical, "ERROR: %s fail with ret = %s\r\n",
                 CliChannelIdStringGet((AtChannel)port),
                 AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }
#endif


eBool CmdAtPtpPortEnable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue, AtChannelEnable);
    }

eBool CmdAtPtpPortDisable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse, AtChannelEnable);
    }

eBool CmdAtPtpPortStateSet(char argc, char **argv)
    {
    uint32 state;
    eBool blResult;
    AtUnused(argc);

    mAtStrToEnum(cAtPtpPortStateStr,
                 cAtPtpPortStateVal,
                 argv[1],
                 state,
                 blResult);
    if (!blResult)
        {
        AtPrintc(cSevCritical, "Invalid PTP port state. Expected: master, slave...\r\n");
        return cAtFalse;
        }

    return AttributeSet(argv[0], state, AtPtpPortStateSet);
    }

eBool CmdAtPtpPortStepModeSet(char argc, char **argv)
    {
    uint32 stepMode;
    eBool blResult;
    AtUnused(argc);

    mAtStrToEnum(cAtPtpPortStepModeStr,
                 cAtPtpPortStepModeVal,
                 argv[1],
                 stepMode,
                 blResult);
    if (!blResult)
        {
        AtPrintc(cSevCritical, "Invalid PTP port step mode. Expected: one-step or two-step.\r\n");
        return cAtFalse;
        }

    return AttributeSet(argv[0], stepMode, AtPtpPortStepModeSet);
    }

eBool CmdAtPtpPortTransportSet(char argc, char **argv)
    {
    uint32 transportType;
    eBool blResult;
    AtUnused(argc);

    mAtStrToEnum(cAtPtpTransportTypeStr,
                 cAtPtpTransportTypeVal,
                 argv[1],
                 transportType,
                 blResult);
    if (!blResult)
        {
        AtPrintc(cSevCritical, "Invalid PTP transport protocol. Expected: l2tp, ipv4, ipv6, ...\r\n");
        return cAtFalse;
        }

    return AttributeSet(argv[0], transportType, AtPtpPortTransportTypeSet);
    }

eBool CmdAtPtpPortShow(char argc, char **argv)
    {
    uint32 bufferSize;
    uint8 numPorts, port_i;
    eBool blResult = cAtFalse;
    AtChannel *channelList;
    tTab *tabPtr;
    const char *heading[] = {"Description", "Enabled", "State", "StepMode", "Transport", "AsymmetryDelay (ns)", "TxDelayAdj(ns)", "RxDelayAdj(ns)"};
    char buf[100];
    AtUnused(argc);

    /* Initialize structure */
    AtOsalMemInit(buf, 0x0, sizeof(buf));

    /* Get the shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliAtPtpPortListFromString(argv[0], channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 \r\n");
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numPorts, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        return cAtFalse;
        }

    /* Make table content */
    for (port_i = 0; port_i < numPorts; port_i++)
        {
        AtPtpPort port = (AtPtpPort)channelList[port_i];
        uint8 column = 0;
        eAtPtpPortState state;
        eAtPtpStepMode stepMode;
        eAtPtpPsnType psnType;

        /* Description */
        StrToCell(tabPtr, port_i, column++, AtObjectToString((AtObject)port));

        /* Enabling status */
        StrToCell(tabPtr, port_i, column++, CliBoolToString(AtChannelIsEnabled((AtChannel)port)));

        /* Port state */
        state = AtPtpPortStateGet(port);
        mAtEnumToStr(cAtPtpPortStateStr,
                     cAtPtpPortStateVal,
                     state, buf, blResult);
        ColorStrToCell(tabPtr, port_i, column++, blResult ? buf : "NA", blResult ? cSevNormal : cSevCritical);

        /* Step mode */
        stepMode = AtPtpPortStepModeGet(port);
        mAtEnumToStr(cAtPtpPortStepModeStr,
                     cAtPtpPortStepModeVal,
                     stepMode, buf, blResult);
        ColorStrToCell(tabPtr, port_i, column++, blResult ? buf : "NA", blResult ? cSevNormal : cSevCritical);

        /* PSN Type */
        psnType = AtPtpPortTransportTypeGet(port);
        mAtEnumToStr(cAtPtpTransportTypeStr,
                     cAtPtpTransportTypeVal,
                     psnType, buf, blResult);
        ColorStrToCell(tabPtr, port_i, column++, blResult ? buf : "NA", blResult ? cSevNormal : cSevCritical);

        /* Asymmetry Link Delay */
        StrToCell(tabPtr, port_i, column++, CliNumber2String(AtPtpPortAsymmetryDelayGet(port), "%d"));

        StrToCell(tabPtr, port_i, column++, CliNumber2String(AtPtpPortTxDelayAdjustGet(port), "%d"));
        StrToCell(tabPtr, port_i, column++, CliNumber2String(AtPtpPortRxDelayAdjustGet(port), "%d"));
        }

    /* Show */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtPtpPortCountersShow(char argc, char **argv)
    {
    uint32 numPorts;
    AtChannel *ports = CliSharedChannelListGet(&numPorts);

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    /* Get Port List */
    numPorts = CliAtPtpPortListFromString(argv[0], ports, numPorts);
    if (ports == NULL)
        {
        AtPrintc(cSevInfo, "No port to display, the ID list may be wrong, expect: 1,2-3,...\r\n");
        return cAtTrue;
        }

    return PortCounterGet((AtPtpPort *)ports, numPorts, argc, argv);
    }

eBool CmdAtPtpPortTxDelayAdjustSet(char argc, char **argv)
    {
    uint32 nanoSeconds;
    AtUnused(argc);
    nanoSeconds = AtStrtoul(argv[1], NULL, 10);
    return AttributeSet(argv[0], nanoSeconds, AtPtpPortTxDelayAdjustSet);
    }

eBool CmdAtPtpPortRxDelayAdjustSet(char argc, char **argv)
    {
    uint32 nanoSeconds;
    AtUnused(argc);
    nanoSeconds = AtStrtoul(argv[1], NULL, 10);
    return AttributeSet(argv[0], nanoSeconds, AtPtpPortRxDelayAdjustSet);
    }

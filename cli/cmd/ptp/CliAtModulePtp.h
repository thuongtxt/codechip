/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PTP
 * 
 * File        : CliAtModulePtp.h
 * 
 * Created Date: Jul 2, 2018
 *
 * Description : Module PTP CLIs
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLIATMODULEPTP_H_
#define _CLIATMODULEPTP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCli.h"
#include "AtOsal.h"
#include "AtCommon.h"
#include "AtClasses.h"
#include "AtNumber.h"
#include "AtDevice.h"
#include "AtModulePtp.h"
#include "AtPtpPort.h"
#include "AtPtpPsn.h"
#include "AtPtpPsnGroup.h"

#include "../eth/CliAtModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 CliAtPtpPortListFromString(char *pStrIdList, AtChannel *channel, uint32 bufferSize);
uint32 CliAtPtpPsnGroupListFromString(char *pStrIdList, AtPtpPsnGroup *groupList, uint32 bufferSize,
                                      AtPtpPsnGroup (*GroupGet)(AtModulePtp, uint32));

#ifdef __cplusplus
}
#endif
#endif /* _CLIATMODULEPTP_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : CliAtPrbsEngineSerdesEthPort.c
 *
 * Created Date: Aug 24, 2013
 *
 * Description : PRBS CLI
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtPrbsEngine.h"
#include "../eth/CliAtModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtList PrbsEnginesGet(char *idString)
    {
    uint32 i;
    AtList engines = AtListCreate(0);
    uint32 bufferSize;
    uint32 numPorts;
    AtChannel *channelList;

    /* Get the shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliEthPortListFromString(idString, channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 or 1.1 ...\r\n");
        return engines;
        }

    for (i = 0; i < numPorts; i++)
        {
        AtChannel port = channelList[i];
        AtPrbsEngine engine = AtChannelPrbsEngineGet(port);
        if (engine)
            AtListObjectAdd(engines, (AtObject)engine);
        }

    return engines;
    }

static eBool Invert(char argc, char **argv, eBool invert)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineInvert(engines, invert);
	AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

static eBool ErrorForce(char argc, char **argv, eBool force)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineErrorForce(engines, force);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

static eBool Enable(char argc, char **argv, eBool enable)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eAtDirection direction;
    eBool success = cAtFalse;

    if (argc < 1)
        {
        AtPrintc(cSevCritical, "Not enough parameters\r\n");
        return cAtFalse;
        }

    if (argc == 1)
        {
        success = CliAtPrbsEngineEnable(engines, enable);
        AtObjectDelete((AtObject)engines);
        return success;
        }

    direction = CliDirectionFromStr(argv[1]);

    switch(direction)
        {
        case cAtDirectionTx:
            success = CliAtPrbsEngineTxEnable(engines, enable);
            break;
        case cAtDirectionRx:
            success = CliAtPrbsEngineRxEnable(engines, enable);
            break;
        case cAtDirectionAll:
            success = CliAtPrbsEngineEnable(engines, enable);
            break;

        case cAtDirectionInvalid:
            AtPrintc(cSevCritical, "ERROR: Invalid direction parameter. Expected: %s\r\n", CliValidDirections());
            return cAtFalse;

        default:
            break;
        }

    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineEthPortModeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eAtDirection direction;
    eBool success = cAtFalse;

    if (argc < 1)
        {
        AtPrintc(cSevCritical, "Not enough parameters\r\n");
        return cAtFalse;
        }

    if (argc == 2)
        {
        success = CliAtPrbsEngineModeSet(engines, argv[1]);
        AtObjectDelete((AtObject)engines);
        return success;
        }

    direction = CliDirectionFromStr(argv[2]);

    switch(direction)
        {
        case cAtDirectionTx:
            success = CliAtPrbsEngineTxModeSet(engines, argv[1]);
            break;
        case cAtDirectionRx:
            success = CliAtPrbsEngineRxModeSet(engines, argv[1]);
            break;
        case cAtDirectionAll:
            success = CliAtPrbsEngineModeSet(engines, argv[1]);
            break;

        case cAtDirectionInvalid:
            AtPrintc(cSevCritical, "ERROR: Invalid direction parameter. Expected: %s\r\n", CliValidDirections());
            return cAtFalse;

        default:
            break;
        }

    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineEthPortInvertEnable(char argc, char **argv)
    {
    return Invert(argc, argv, cAtTrue);
    }

eBool CmdAtPrbsEngineEthPortInvertDisable(char argc, char **argv)
    {
    return Invert(argc, argv, cAtFalse);
    }

eBool CmdAtPrbsEngineEthPortErrorForce(char argc, char **argv)
    {
    return ErrorForce(argc, argv, cAtTrue);
    }

eBool CmdAtPrbsEngineEthPortErrorUnForce(char argc, char **argv)
    {
    return ErrorForce(argc, argv, cAtFalse);
    }

eBool CmdAtPrbsEngineEthPortEnable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue);
    }

eBool CmdAtPrbsEngineEthPortDisable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse);
    }

eBool CmdAtPrbsEngineEthPortCounterGet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineCounterGet(engines, argv[1], (argc > 2) ? argv[2] : NULL);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineEthPortDebug(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineDebug(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineEthPortShow(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool silentMode = cAtFalse;
    eBool success = CliAtPrbsEngineShow(engines, silentMode);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineEthPortStart(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineStart(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineEthPortStop(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineStop(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineEthPortDurationSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineDurationSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineEthPortBandwidthShow(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool silentMode = cAtFalse;
    eBool success = CliAtPrbsEngineBandwidthShowWithSilent(engines, cAtTrue, silentMode);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineEthPortPayloadLengthModeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success;
    AtUnused(argc);
    success = CliAtPrbsEnginePayloadLengthModeSet(engines, argv[1]);

    AtObjectDelete((AtObject) engines);
    return success;
    }

eBool CmdAtPrbsEngineEthPortPayloadLengthMinSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    uint32 value = AtStrToDw(argv[1]);
    eBool success;
    AtUnused(argc);
    success = CliAtPrbsEngineMinLengthSet(engines, value);

    AtObjectDelete((AtObject) engines);
    return success;
    }

eBool CmdAtPrbsEngineEthPortPayloadLengthMaxSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    uint32 value = AtStrToDw(argv[1]);
    eBool success;
    AtUnused(argc);
    success = CliAtPrbsEngineMaxLengthSet(engines, value);

    AtObjectDelete((AtObject) engines);
    return success;
    }

eBool CmdAtPrbsEngineEthPortPayloadLengthBurstSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    uint32 value = AtStrToDw(argv[1]);
    eBool success;
    AtUnused(argc);
    success = CliAtPrbsEngineBurstSizeSet(engines, value);

    AtObjectDelete((AtObject) engines);
    return success;
    }

eBool CmdAtPrbsEngineEthPortBandwidthPercentageSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    uint32 value = AtStrToDw(argv[1]);
    eBool success;
    AtUnused(argc);
    success = CliAtPrbsEnginePercentageBandwidthSet(engines, value);

    AtObjectDelete((AtObject) engines);
    return success;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : CliAtPrbsEngineDe1.c
 *
 * Created Date: Aug 24, 2013
 *
 * Description : PRBS CLI
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtPrbsEngine.h"
#include "AtPdhChannel.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtList PrbsEnginesGet(char *idString)
    {
    uint32 numChannels, i;
    AtChannel *channelList = CliSharedChannelListGet(&numChannels);
    AtList engines = AtListCreate(0);
    numChannels = De1ListFromString(idString, channelList, numChannels);

    for (i = 0; i < numChannels; i++)
        {
        AtPrbsEngine engine = AtChannelPrbsEngineGet(channelList[i]);
        if (engine)
            AtListObjectAdd(engines, (AtObject)engine);
        }

    return engines;
    }

static AtList LinePrbsEnginesGet(char *idString)
    {
    uint32 numChannels, i;
    AtChannel *channelList = CliSharedChannelListGet(&numChannels);
    AtList engines = AtListCreate(0);
    numChannels = De1ListFromString(idString, channelList, numChannels);

    for (i = 0; i < numChannels; i++)
        {
        AtPrbsEngine engine = AtPdhChannelLinePrbsEngineGet((AtPdhChannel)channelList[i]);
        if (engine)
            AtListObjectAdd(engines, (AtObject)engine);
        }

    return engines;
    }

static eBool Invert(char argc, char **argv, eBool invert)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineInvert(engines, invert);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

static eBool ErrorForce(char argc, char **argv, eBool force, AtList (*EngineGet)(char *idString))
    {
    AtList engines = EngineGet(argv[0]);
    eBool success = CliAtPrbsEngineErrorForce(engines, force);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

static eBool Enable(char argc, char **argv, eBool enable, AtList (*EngineGet)(char *idString))
    {
    AtList engines = EngineGet(argv[0]);
    eBool success = CliAtPrbsEngineEnable(engines, enable);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineDe1ModeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineModeSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineDe1TransmitModeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineTxModeSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineDe1ExpectModeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineRxModeSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }


eBool CmdAtPrbsEngineDe1DataModeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineDataModeSet(engines, argv[1], cAtTrue, cAtTrue);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineDe1TransmitDataModeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineDataModeSet(engines, argv[1], cAtTrue, cAtFalse);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineDe1ExpectDataModeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineDataModeSet(engines, argv[1], cAtFalse, cAtTrue);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineDe1InvertEnable(char argc, char **argv)
    {
    return Invert(argc, argv, cAtTrue);
    }

eBool CmdAtPrbsEngineDe1InvertDisable(char argc, char **argv)
    {
    return Invert(argc, argv, cAtFalse);
    }

eBool CmdAtPrbsEngineDe1ErrorForce(char argc, char **argv)
    {
    return ErrorForce(argc, argv, cAtTrue, PrbsEnginesGet);
    }

eBool CmdAtPrbsEngineDe1ErrorUnForce(char argc, char **argv)
    {
    return ErrorForce(argc, argv, cAtFalse, PrbsEnginesGet);
    }

eBool CmdAtPrbsEngineDe1Enable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue, PrbsEnginesGet);
    }

eBool CmdAtPrbsEngineDe1Disable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse, PrbsEnginesGet);
    }

eBool CmdAtPrbsEngineDe1Show(char argc, char **argv)
    {
    return CliAtPrbsEngineShowWithHandler(argc, argv, PrbsEnginesGet);
    }

eBool CmdAtPrbsEngineDe1TxErrorRateForce(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineTxErrorRateForce(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineDe1FixPatternSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    uint32 transmitPattern = AtStrToDw(argv[1]);
    uint32 expectedPattern = (argc == 2)? transmitPattern: AtStrToDw(argv[2]);
    eBool success;

    AtUnused(argc);
    success  = CliAtPrbsEngineTxFixedPatternSet(engines, transmitPattern);
    success &= CliAtPrbsEngineRxFixedPatternSet(engines, expectedPattern);

    AtObjectDelete((AtObject) engines);
    return success;
    }

eBool CmdAtPrbsEngineDe1TransmitFixPatternSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    uint32 transmitPattern = AtStrToDw(argv[1]);
    eBool success = CliAtPrbsEngineTxFixedPatternSet(engines, transmitPattern);

    AtUnused(argc);
    AtObjectDelete((AtObject) engines);

    return success;
    }

eBool CmdAtPrbsEngineDe1ExpectFixPatternSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    uint32 expectedPattern = AtStrToDw(argv[1]);
    eBool success = CliAtPrbsEngineRxFixedPatternSet(engines, expectedPattern);

    AtUnused(argc);
    AtObjectDelete((AtObject) engines);

    return success;
    }

eBool CmdAtPrbsEngineDe1BitErrorRateShow(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineBitErrorRateShow(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineDe1SideSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineSideSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineDe1GeneratingSideSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineGeneratingSideSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineDe1MonitoringSideSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineMonitoringSideSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineDe1CounterGet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineCounterGet(engines, argv[1], (argc > 2) ? argv[2] : NULL);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineDe1ErrorInject(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineErrorInject(engines, (argc > 1) ? (uint32)AtStrToDw(argv[1]) : 1);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineDe1BitOrderSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineBitOrderSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }


eBool CmdAtPrbsEngineLineDe1ErrorForce(char argc, char **argv)
    {
    return ErrorForce(argc, argv, cAtTrue, LinePrbsEnginesGet);
    }

eBool CmdAtPrbsEngineLineDe1ErrorUnForce(char argc, char **argv)
    {
    return ErrorForce(argc, argv, cAtFalse, LinePrbsEnginesGet);
    }

eBool CmdAtPrbsEngineLineDe1Enable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue, LinePrbsEnginesGet);
    }

eBool CmdAtPrbsEngineLineDe1Disable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse, LinePrbsEnginesGet);
    }

eBool CmdAtPrbsEngineLineDe1Show(char argc, char **argv)
    {
    return CliAtPrbsEngineShowWithHandler(argc, argv, LinePrbsEnginesGet);
    }

eBool CmdAtPrbsEngineLineDe1Start(char argc, char **argv)
    {
    AtList engines = LinePrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineStart(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineLineDe1Stop(char argc, char **argv)
    {
    AtList engines = LinePrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineStop(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineLineDe1DurationSet(char argc, char **argv)
    {
    AtList engines = LinePrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineDurationSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineDe1Start(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineStart(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineDe1Stop(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineStop(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineDe1DurationSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineDurationSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineDe1DelayEnable(char argc, char **argv)
	{
	AtList engines = PrbsEnginesGet(argv[0]);
	eBool success = CliAtPrbsEngineDelayEnable(engines, cAtTrue);

	AtUnused(argc);
	AtObjectDelete((AtObject)engines);
	return success;
	}

eBool CmdAtPrbsEngineDe1DelayDisable(char argc, char **argv)
	{
	AtList engines = PrbsEnginesGet(argv[0]);
	eBool success = CliAtPrbsEngineDelayEnable(engines, cAtFalse);

	AtUnused(argc);
	AtObjectDelete((AtObject)engines);
	return success;
	}

eBool CmdAtPrbsEngineDe1DelayGet(char argc, char **argv)
	{
	AtList engines = PrbsEnginesGet(argv[0]);
	eBool success = CliAtPrbsEngineDelayShow(engines, argv[1], (argc > 2) ? argv[2] : NULL);
	AtUnused(argc);
	AtObjectDelete((AtObject)engines);
	return success;
	}

eBool CmdAtPrbsEngineDe1DisruptionMaxExpectationTimeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineDisruptionMaxExpectationTimeSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineDe1DisruptionGet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineDisruptionShow(engines, argv[1], (argc > 2) ? argv[2] : NULL);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

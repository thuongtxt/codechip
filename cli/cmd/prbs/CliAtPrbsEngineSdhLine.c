/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : CliAtPrbsEngineSdhLine.c
 *
 * Created Date: April 07, 2015
 *
 * Description : PRBS CLI
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "CliAtPrbsEngine.h"
#include "AtSdhLine.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 LineListFromString(char *pStrIdList,
                                       AtChannel *channels,
                                       uint32 bufferSize)
    {
    uint32 *idBuffer, numChannels, i;

    if (pStrIdList == NULL)
        return 0;

    /* It is just a list of flat ID */
    idBuffer = CliSharedIdBufferGet(&numChannels);
    numChannels = CliIdListFromString(pStrIdList, idBuffer, numChannels);
    if (numChannels > bufferSize)
        numChannels = bufferSize;
    for (i = 0; i < numChannels; i++)
        channels[i] = (AtChannel) AtModuleSdhLineGet(CliModuleSdh(), (uint8)(idBuffer[i] - 1));

    return numChannels;
    }

static AtList LinePrbsEnginesGet(char *idString)
    {
    uint32 numChannels, i;
    AtChannel *channelList = CliSharedChannelListGet(&numChannels);
    AtList engines = AtListCreate(0);
    numChannels = LineListFromString(idString, channelList, numChannels);

    for (i = 0; i < numChannels; i++)
        {
        AtPrbsEngine engine = AtSdhLinePrbsEngineGet((AtSdhLine)channelList[i]);
        if (engine)
            AtListObjectAdd(engines, (AtObject)engine);
        }

    return engines;
    }

static eBool ErrorForce(char argc, char **argv, eBool force, AtList (*EngineGet)(char *idString))
    {
    AtList engines = EngineGet(argv[0]);
    eBool success = CliAtPrbsEngineErrorForce(engines, force);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

static eBool Enable(char argc, char **argv, eBool enable, AtList (*EngineGet)(char *idString))
    {
    AtList engines = EngineGet(argv[0]);
    eBool success = CliAtPrbsEngineEnable(engines, enable);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineLineSdhErrorForce(char argc, char **argv)
    {
    return ErrorForce(argc, argv, cAtTrue, LinePrbsEnginesGet);
    }

eBool CmdAtPrbsEngineLineSdhErrorUnForce(char argc, char **argv)
    {
    return ErrorForce(argc, argv, cAtFalse, LinePrbsEnginesGet);
    }

eBool CmdAtPrbsEngineLineSdhEnable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue, LinePrbsEnginesGet);
    }

eBool CmdAtPrbsEngineLineSdhDisable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse, LinePrbsEnginesGet);
    }

eBool CmdAtPrbsEngineLineSdhShow(char argc, char **argv)
    {
    return CliAtPrbsEngineShowWithHandler(argc, argv, LinePrbsEnginesGet);
    }

eBool CmdAtPrbsEngineLineSdhStart(char argc, char **argv)
    {
    AtList engines = LinePrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineStart(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineLineSdhStop(char argc, char **argv)
    {
    AtList engines = LinePrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineStop(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineLineSdhDurationSet(char argc, char **argv)
    {
    AtList engines = LinePrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineDurationSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : CliAtPrbsEngineDe3.c
 *
 * Created Date: April 07, 2015
 *
 * Description : PRBS CLI
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "CliAtPrbsEngine.h"
#include "AtPdhChannel.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtList PrbsEnginesGet(char *idString)
    {
    uint32 numChannels, i;
    AtChannel *channelList = CliSharedChannelListGet(&numChannels);
    AtList engines = AtListCreate(0);
    numChannels = CliDe3ListFromString(idString, channelList, numChannels);

    for (i = 0; i < numChannels; i++)
        {
        AtPrbsEngine engine = AtChannelPrbsEngineGet(channelList[i]);
        if (engine)
            AtListObjectAdd(engines, (AtObject)engine);
        }

    return engines;
    }

static AtList LinePrbsEnginesGet(char *idString)
    {
    uint32 numChannels, i;
    AtChannel *channelList = CliSharedChannelListGet(&numChannels);
    AtList engines = AtListCreate(0);
    numChannels = CliDe3ListFromString(idString, channelList, numChannels);

    for (i = 0; i < numChannels; i++)
        {
        AtPrbsEngine engine = AtPdhChannelLinePrbsEngineGet((AtPdhChannel)channelList[i]);
        if (engine)
            AtListObjectAdd(engines, (AtObject)engine);
        }

    return engines;
    }

static eBool Invert(char argc, char **argv, eBool invert)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineInvert(engines, invert);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

static eBool ErrorForce(char argc, char **argv, eBool force, AtList (*EngineGet)(char *idString))
    {
    AtList engines = EngineGet(argv[0]);
    eBool success = CliAtPrbsEngineErrorForce(engines, force);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

static eBool Enable(char argc, char **argv, eBool enable, AtList (*EngineGet)(char *idString))
    {
    AtList engines = EngineGet(argv[0]);
    eBool success = CliAtPrbsEngineEnable(engines, enable);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineDe3ModeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineModeSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    AtUnused(argc);
    return success;
    }

eBool CmdAtPrbsEngineDe3TransmitModeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineTxModeSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    AtUnused(argc);
    return success;
    }

eBool CmdAtPrbsEngineDe3ExpectModeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineRxModeSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    AtUnused(argc);
    return success;
    }

eBool CmdAtPrbsEngineDe3DataModeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineDataModeSet(engines, argv[1], cAtTrue, cAtTrue);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    AtUnused(argc);
    return success;
    }

eBool CmdAtPrbsEngineDe3TransmitDataModeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineDataModeSet(engines, argv[1], cAtTrue, cAtFalse);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    AtUnused(argc);
    return success;
    }

eBool CmdAtPrbsEngineDe3ExpectDataModeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineDataModeSet(engines, argv[1], cAtFalse, cAtTrue);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    AtUnused(argc);
    return success;
    }

eBool CmdAtPrbsEngineDe3InvertEnable(char argc, char **argv)
    {
    return Invert(argc, argv, cAtTrue);
    }

eBool CmdAtPrbsEngineDe3InvertDisable(char argc, char **argv)
    {
    return Invert(argc, argv, cAtFalse);
    }

eBool CmdAtPrbsEngineDe3ErrorForce(char argc, char **argv)
    {
    return ErrorForce(argc, argv, cAtTrue, PrbsEnginesGet);
    }

eBool CmdAtPrbsEngineDe3ErrorUnForce(char argc, char **argv)
    {
    return ErrorForce(argc, argv, cAtFalse, PrbsEnginesGet);
    }

eBool CmdAtPrbsEngineDe3Enable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue, PrbsEnginesGet);
    }

eBool CmdAtPrbsEngineDe3Disable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse, PrbsEnginesGet);
    }

eBool CmdAtPrbsEngineDe3Show(char argc, char **argv)
    {
    return CliAtPrbsEngineShowWithHandler(argc, argv, PrbsEnginesGet);
    }

eBool CmdAtPrbsEngineDe3TxErrorRateForce(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineTxErrorRateForce(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineDe3FixPatternSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    uint32 transmitPattern = AtStrToDw(argv[1]);
    uint32 expectedPattern = (argc == 2)?transmitPattern:AtStrToDw(argv[2]);
    eBool success;

    AtUnused(argc);
    success = CliAtPrbsEngineTxFixedPatternSet(engines, transmitPattern);
    success &= CliAtPrbsEngineRxFixedPatternSet(engines, expectedPattern);

    AtObjectDelete((AtObject) engines);
    return success;
    }

eBool CmdAtPrbsEngineDe3TransmitFixPatternSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    uint32 transmitPattern = AtStrToDw(argv[1]);
    eBool success = CliAtPrbsEngineTxFixedPatternSet(engines, transmitPattern);

    AtUnused(argc);
    AtObjectDelete((AtObject) engines);

    return success;
    }

eBool CmdAtPrbsEngineDe3ExpectFixPatternSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    uint32 expectedPattern = AtStrToDw(argv[1]);
    eBool success = CliAtPrbsEngineRxFixedPatternSet(engines, expectedPattern);

    AtUnused(argc);
    AtObjectDelete((AtObject) engines);

    return success;
    }

eBool CmdAtPrbsEngineDe3BitErrorRateShow(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineBitErrorRateShow(engines);

    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineDe3GeneratingSideSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineGeneratingSideSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineDe3MonitoringSideSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineMonitoringSideSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineDe3CounterGet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineCounterGet(engines, argv[1], (argc > 2) ? argv[2] : NULL);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineDe3ErrorInject(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineErrorInject(engines, (argc > 1) ? (uint32)AtStrToDw(argv[1]) : 1);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineDe3BitOrderSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineBitOrderSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineLineDe3ErrorForce(char argc, char **argv)
    {
    return ErrorForce(argc, argv, cAtTrue, LinePrbsEnginesGet);
    }

eBool CmdAtPrbsEngineLineDe3ErrorUnForce(char argc, char **argv)
    {
    return ErrorForce(argc, argv, cAtFalse, LinePrbsEnginesGet);
    }

eBool CmdAtPrbsEngineLineDe3Enable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue, LinePrbsEnginesGet);
    }

eBool CmdAtPrbsEngineLineDe3Disable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse, LinePrbsEnginesGet);
    }

eBool CmdAtPrbsEngineLineDe3Show(char argc, char **argv)
    {
    return CliAtPrbsEngineShowWithHandler(argc, argv, LinePrbsEnginesGet);
    }

eBool CmdAtPrbsEngineDe3Start(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineStart(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineDe3Stop(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineStop(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineDe3DurationSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineDurationSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineLineDe3Start(char argc, char **argv)
    {
    AtList engines = LinePrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineStart(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineLineDe3Stop(char argc, char **argv)
    {
    AtList engines = LinePrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineStop(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineLineDe3DurationSet(char argc, char **argv)
    {
    AtList engines = LinePrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineDurationSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }


eBool CmdAtPrbsEngineDe3DelayEnable(char argc, char **argv)
	{
	AtList engines = PrbsEnginesGet(argv[0]);
	eBool success = CliAtPrbsEngineDelayEnable(engines, cAtTrue);

	AtUnused(argc);
	AtObjectDelete((AtObject)engines);
	return success;
	}

eBool CmdAtPrbsEngineDe3DelayDisable(char argc, char **argv)
	{
	AtList engines = PrbsEnginesGet(argv[0]);
	eBool success = CliAtPrbsEngineDelayEnable(engines, cAtFalse);

	AtUnused(argc);
	AtObjectDelete((AtObject)engines);
	return success;
	}

eBool CmdAtPrbsEngineDe3DelayGet(char argc, char **argv)
	{
	AtList engines = PrbsEnginesGet(argv[0]);
	eBool success = CliAtPrbsEngineDelayShow(engines, argv[1], (argc > 2) ? argv[2] : NULL);
	AtUnused(argc);
	AtObjectDelete((AtObject)engines);
	return success;
	}


eBool CmdAtPrbsEngineDe3DisruptionMaxExpectationTimeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineDisruptionMaxExpectationTimeSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }


eBool CmdAtPrbsEngineDe3DisruptionGet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineDisruptionShow(engines, argv[1], (argc > 2) ? argv[2] : NULL);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : CliAtPrbsEngineNxDs0.c
 *
 * Created Date: Aug 24, 2013
 *
 * Description : PRBS CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtPrbsEngine.h"
#include "AtPdhDe1.h"

/*--------------------------- Define -----------------------------------------*/
#define cNxDs0IdGroupSeparateChar "|"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtList PrbsEnginesGet(char *de1IdString, char *timeSlotString)
    {
    uint32 numChannels, i;
    uint32 numDs0, bitmap, *ds0List;
    AtChannel *channelList;
    AtList engines = AtListCreate(0);

    /* Get DE1 ID */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(de1IdString, channelList, numChannels);

    /* Get DS0 list */
    ds0List = CliSharedIdBufferGet(&numDs0);
    numDs0  = CliIdListFromString(timeSlotString, ds0List, numDs0);

    /* Create bitmap from DS0 list */
    bitmap = 0;
    for (i = 0; i < numDs0; i++)
        bitmap |= cBit0 << ds0List[i];

    for (i = 0; i < numChannels; i++)
        {
        AtPrbsEngine engine;
        AtPdhNxDS0 nxDs0;

        /* Check if DS0 has already been created */
        nxDs0 = AtPdhDe1NxDs0Get((AtPdhDe1)channelList[i], bitmap);
        if (nxDs0 == NULL)
            {
            AtPrintc(cSevWarning,
                     "WARNING: NxDS0 0x%08x of %s has not been created\r\n",
                     bitmap,
                     CliChannelIdStringGet(channelList[i]));
            continue;
            }

        /* Delete it */
        engine = AtChannelPrbsEngineGet((AtChannel)nxDs0);
        if (engine)
            AtListObjectAdd(engines, (AtObject)engine);
        }

    return engines;
    }

static eBool HelperCmdAtPrbsEngineNxDs0Invert(char argc, char **argv, eBool invert)
    {
    AtList engines = PrbsEnginesGet(argv[0], argv[1]);
    eBool success = CliAtPrbsEngineInvert(engines, invert);
	AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

static eBool HelperCmdAtPrbsEngineNxDs0ErrorForce(char argc, char **argv, eBool force)
    {
    AtList engines = PrbsEnginesGet(argv[0], argv[1]);
    eBool success = CliAtPrbsEngineErrorForce(engines, force);
	AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

static eBool HelperCmdAtPrbsEngineNxDs0Enable(char argc, char **argv, eBool enable)
    {
    AtList engines = PrbsEnginesGet(argv[0], argv[1]);
    eBool success = CliAtPrbsEngineEnable(engines, enable);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineNxDs0ModeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0], argv[1]);
    eBool success = CliAtPrbsEngineModeSet(engines, argv[2]);
	AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineNxDs0TransmitModeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0], argv[1]);
    eBool success = CliAtPrbsEngineTxModeSet(engines, argv[2]);
	AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineNxDs0ExpectModeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0], argv[1]);
    eBool success = CliAtPrbsEngineRxModeSet(engines, argv[2]);
	AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineNxDs0DataModeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0], argv[1]);
    eBool success = CliAtPrbsEngineDataModeSet(engines, argv[2], cAtTrue, cAtTrue);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineNxDs0TransmitDataModeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0], argv[1]);
    eBool success = CliAtPrbsEngineDataModeSet(engines, argv[2], cAtTrue, cAtFalse);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineNxDs0ExpectDataModeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0], argv[1]);
    eBool success = CliAtPrbsEngineDataModeSet(engines, argv[2], cAtFalse, cAtTrue);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineNxDs0InvertEnable(char argc, char **argv)
    {
    return HelperCmdAtPrbsEngineNxDs0Invert(argc, argv, cAtTrue);
    }

eBool CmdAtPrbsEngineNxDs0InvertDisable(char argc, char **argv)
    {
    return HelperCmdAtPrbsEngineNxDs0Invert(argc, argv, cAtFalse);
    }

eBool CmdAtPrbsEngineNxDs0ErrorForce(char argc, char **argv)
    {
    return HelperCmdAtPrbsEngineNxDs0ErrorForce(argc, argv, cAtTrue);
    }

eBool CmdAtPrbsEngineNxDs0ErrorUnForce(char argc, char **argv)
    {
    return HelperCmdAtPrbsEngineNxDs0ErrorForce(argc, argv, cAtFalse);
    }

eBool CmdAtPrbsEngineNxDs0Enable(char argc, char **argv)
    {
    return HelperCmdAtPrbsEngineNxDs0Enable(argc, argv, cAtTrue);
    }

eBool CmdAtPrbsEngineNxDs0Disable(char argc, char **argv)
    {
    return HelperCmdAtPrbsEngineNxDs0Enable(argc, argv, cAtFalse);
    }

eBool CmdAtPrbsEngineNxDs0Show(char argc, char **argv)
    {
    AtList engines;
    eBool success = cAtTrue;
    eBool silent = cAtFalse;

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }
        
    if (argc > 2)
        silent = CliIsSilentIndication(argv[2]);

    engines = PrbsEnginesGet(argv[0], argv[1]);
    success = CliAtPrbsEngineShow(engines, silent);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);

    return success;
    }

eBool CmdAtPrbsEngineNxDs0SideSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0], argv[1]);
    eBool success = CliAtPrbsEngineSideSet(engines, argv[2]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineNxDs0GeneratingSideSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0], argv[1]);
    eBool success = CliAtPrbsEngineGeneratingSideSet(engines, argv[2]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineNxDs0MonitoringSideSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0], argv[1]);
    eBool success = CliAtPrbsEngineMonitoringSideSet(engines, argv[2]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineNxDs0CounterGet(char argc, char **argv)
    {
    AtList engines;
    eBool success;

    if (argc < 3)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    engines = PrbsEnginesGet(argv[0], argv[1]);
    success = CliAtPrbsEngineCounterGet(engines, argv[2], (argc > 3) ? argv[3] : NULL);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineNxDs0ErrorInject(char argc, char **argv)
    {
    AtList engines;
    eBool success;

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    engines = PrbsEnginesGet(argv[0], argv[1]);
    success = CliAtPrbsEngineErrorInject(engines, (argc > 2) ? (uint32)AtStrToDw(argv[2]) : 1);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineNxDs0TxErrorRateForce(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0], argv[1]);
    eBool success = CliAtPrbsEngineTxErrorRateForce(engines, argv[2]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineNxDs0FixPatternSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0], argv[1]);
    uint32 transmitPattern = AtStrToDw(argv[2]);
    uint32 expectedPattern = (argc==3) ? transmitPattern: AtStrToDw(argv[3]);
    eBool success;
    AtUnused(argc);
    success  = CliAtPrbsEngineTxFixedPatternSet(engines, transmitPattern);
    success &= CliAtPrbsEngineRxFixedPatternSet(engines, expectedPattern);

    AtObjectDelete((AtObject) engines);
    return success;
    }

eBool CmdAtPrbsEngineNxDs0TransmitFixPatternSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0], argv[1]);
    uint32 transmitPattern = AtStrToDw(argv[2]);
    eBool success = CliAtPrbsEngineTxFixedPatternSet(engines, transmitPattern);

    AtUnused(argc);
    AtObjectDelete((AtObject) engines);

    return success;
    }

eBool CmdAtPrbsEngineNxDs0ExpectFixPatternSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0], argv[1]);
    uint32 expectedPattern = AtStrToDw(argv[2]);
    eBool success = CliAtPrbsEngineRxFixedPatternSet(engines, expectedPattern);

    AtUnused(argc);
    AtObjectDelete((AtObject) engines);

    return success;
    }

eBool CmdAtPrbsEngineNxDs0BitErrorRateShow(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0], argv[1]);
    eBool success = CliAtPrbsEngineBitErrorRateShow(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineNxDs0BitOrderSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0], argv[1]);
    eBool success = CliAtPrbsEngineBitOrderSet(engines, argv[2]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineNxDs0Start(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0], argv[1]);
    eBool success = CliAtPrbsEngineStart(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineNxDs0Stop(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0], argv[1]);
    eBool success = CliAtPrbsEngineStop(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineNxDs0DurationSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0], argv[1]);
    eBool success = CliAtPrbsEngineDurationSet(engines, argv[2]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineNxDs0DelayEnable(char argc, char **argv)
	{
	AtList engines = PrbsEnginesGet(argv[0], argv[1]);
	eBool success = CliAtPrbsEngineDelayEnable(engines, cAtTrue);

	AtUnused(argc);
	AtObjectDelete((AtObject)engines);
	return success;
	}

eBool CmdAtPrbsEngineNxDs0DelayDisable(char argc, char **argv)
	{
	AtList engines = PrbsEnginesGet(argv[0], argv[1]);
	eBool success = CliAtPrbsEngineDelayEnable(engines, cAtFalse);

	AtUnused(argc);
	AtObjectDelete((AtObject)engines);
	return success;
	}

eBool CmdAtPrbsEngineNxDs0DelayGet(char argc, char **argv)
	{
	AtList engines = PrbsEnginesGet(argv[0], argv[1]);
	eBool success = CliAtPrbsEngineDelayShow(engines, argv[2], (argc > 3) ? argv[3] : NULL);
	AtUnused(argc);
	AtObjectDelete((AtObject)engines);
	return success;
	}

eBool CmdAtPrbsEngineNxDs0DisruptionMaxExpectationTimeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0], argv[1]);
    eBool success = CliAtPrbsEngineDisruptionMaxExpectationTimeSet(engines, argv[2]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }


eBool CmdAtPrbsEngineNxDs0DisruptionGet(char argc, char **argv)
    {
    AtList engines = NULL;
    eBool success = cAtFalse;
    char *timeSlotString = (AtStrcmp(argv[1], "r2c") ==0 || AtStrcmp(argv[0], "ro") ==0)?  NULL : argv[1];
    char *silentStr = (argc > 2) ? argv[2] : NULL;
    if (timeSlotString)
        silentStr = (argc > 3) ? argv[3] : NULL;

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }
    engines = PrbsEnginesGet(argv[0], argv[1]);
    success = CliAtPrbsEngineDisruptionShow(engines, argv[1], silentStr);
    AtObjectDelete((AtObject)engines);
    return success;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : CliAtPrbsEngine.c
 *
 * Created Date: Aug 24, 2013
 *
 * Description : PRBS engine CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "CliAtPrbsEngine.h"
#include "AtCliModule.h"
#include "../textui/CliAtTextUI.h"
#include "../../driver/src/implement/default/att/ThaAttModulePrbs.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef eAtRet (*AttributeSetFunc)(AtPrbsEngine self, uint32 value);
typedef eAtRet (*AttributeNoPramSetFunc)(AtPrbsEngine self);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char *cAtPrbsModeStr[] = {"prbs7",
                                       "prbs9",
                                       "prbs15",
                                       "prbs23",
                                       "prbs31",
                                       "sequence",
                                       "fixedpattern1byte",
                                       "fixedpattern2bytes",
                                       "fixedpattern3bytes",
                                       "fixedpattern4bytes",
                                       "prbs11",
                                       "prbs20",
                                       "prbs20qrss",
                                       "prbs20r",
                                       "prbs7"
                                       };

static const uint32 cAtPrbsModeVal[] = {cAtPrbsModePrbs7,
                                        cAtPrbsModePrbs9,
                                        cAtPrbsModePrbs15,
                                        cAtPrbsModePrbs23,
                                        cAtPrbsModePrbs31,
                                        cAtPrbsModePrbsSeq,
                                        cAtPrbsModePrbsFixedPattern1Byte,
                                        cAtPrbsModePrbsFixedPattern2Bytes,
                                        cAtPrbsModePrbsFixedPattern3Bytes,
                                        cAtPrbsModePrbsFixedPattern4Bytes,
                                        cAtPrbsModePrbs11,
                                        cAtPrbsModePrbs20StdO151,
                                        cAtPrbsModePrbs20QrssStdO151,
                                        cAtPrbsModePrbs20rStdO153,
                                        cAtPrbsModePrbs7
                                        };

static const char *cAtPrbsDataModeStr[] = {"64kb",
                                           "56kb"
                                           };

static const uint32 cAtPrbsDataModeVal[] = {cAtPrbsDataMode64kb,
                                            cAtPrbsDataMode56kb
                                            };

static const char *cAtPrbsBerRateStr[] =
    {
    "off",
    "1e-2",
    "1e-3",
    "1e-4",
    "1e-5",
    "1e-6",
    "1e-7",
    "1e-8",
    "1e-9",
    "1e-10"
    };

static const uint32 cAtPrbsBerRateVal[] =
    {
    0,
    cAtBerRate1E2,  /**< Error rate is 10-2 */
    cAtBerRate1E3,  /**< Error rate is 10-3 */
    cAtBerRate1E4,  /**< Error rate is 10-4 */
    cAtBerRate1E5,  /**< Error rate is 10-5 */
    cAtBerRate1E6,  /**< Error rate is 10-6 */
    cAtBerRate1E7,  /**< Error rate is 10-7 */
    cAtBerRate1E8,  /**< Error rate is 10-8 */
    cAtBerRate1E9,  /**< Error rate is 10-9 */
    cAtBerRate1E10  /**< Error rate is 10-10 */
    };

static const eAtPrbsEngineAlarmType cAtPrbsAlarmTypeVal[] =
    {
    cAtPrbsEngineAlarmTypeNone,
    cAtPrbsEngineAlarmTypeError,
    cAtPrbsEngineAlarmTypeLossSync
    };

static const char *cAtPrbsSideStr[] = {"tdm", "psn"};
static uint32 cAtPrbsSideVal[] = {cAtPrbsSideTdm, cAtPrbsSidePsn};

static const char *cAtPrbsBitOrderStr[] = {"msb", "lsb", sAtNotApplicable};
static const uint32 cAtPrbsBitOrderVal[] = {cAtPrbsBitOrderMsb, cAtPrbsBitOrderLsb, cAtPrbsBitOrderNotApplicable};

static const char *cAtPrbsLengthModeStr[] = {"fix", "increase", "decrease", "random"};
static const uint32 cAtPrbsLengthModeVal[] = {cAtPrbsLengthModeFixed, cAtPrbsLengthModeIncreased, cAtPrbsLengthModeDecreased, cAtPrbsLengthModeRandom};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *SideString(eAtPrbsSide option)
    {
    if (option == cAtPrbsSideTdm) return "tdm";
    if (option == cAtPrbsSidePsn) return "psn";

    return "unknown";
    }

static eBool AttributeSet(AtList engines, uint32 value, AttributeSetFunc Func)
    {
    uint32 i;
    eBool success = cAtTrue;

    if (AtListLengthGet(engines) == 0)
        {
        AtPrintc(cSevWarning, "No engine to configure\r\n");
        return cAtTrue;
        }

    for (i = 0; i < AtListLengthGet(engines); i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtPrbsEngine engine = (AtPrbsEngine)AtListObjectGet(engines, i);
        eAtRet ret = Func(engine, value);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Configure PRBS engine of %s fail with ret = %s\r\n",
                     AtPrbsEngineChannelDescription(engine),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static eBool AttributeNoParamSet(AtList engines, AttributeNoPramSetFunc Func)
    {
    uint32 i;
    eBool success = cAtTrue;

    if (AtListLengthGet(engines) == 0)
        {
        AtPrintc(cSevWarning, "No engine to configure\r\n");
        return cAtTrue;
        }

    for (i = 0; i < AtListLengthGet(engines); i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtPrbsEngine engine = (AtPrbsEngine)AtListObjectGet(engines, i);
        eAtRet ret = Func(engine);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Configure PRBS engine of %s fail with ret = %s\r\n",
                     AtPrbsEngineChannelDescription(engine),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static eBool ModeSet(AtList engines, const char *modeString, AttributeSetFunc Func)
    {
    eBool convertSuccess = cAtFalse;
    eAtPrbsMode mode = CliStringToEnum(modeString, cAtPrbsModeStr, cAtPrbsModeVal, mCount(cAtPrbsModeVal), &convertSuccess);

    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid PRBS mode, expect: ");
        CliExpectedValuesPrint(cSevCritical, cAtPrbsModeStr, mCount(cAtPrbsModeStr));
        AtPrintc(cSevCritical, "\r\n");
        }

    return AttributeSet(engines, mode, Func);
    }

static eBool DataModeSet(AtList engines, const char *modeString, AttributeSetFunc Func)
    {
    eBool convertSuccess = cAtFalse;
    eAtPrbsDataMode mode = CliStringToEnum(modeString, cAtPrbsDataModeStr, cAtPrbsDataModeVal, mCount(cAtPrbsDataModeVal), &convertSuccess);

    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid PRBS data mode, expect: ");
        CliExpectedValuesPrint(cSevCritical, cAtPrbsDataModeStr, mCount(cAtPrbsDataModeStr));
        AtPrintc(cSevCritical, "\r\n");
        }

    return AttributeSet(engines, mode, Func);
    }

static eBool PayloadLengthModeSet(AtList engines, const char *modeString, AttributeSetFunc Func)
    {
    eBool convertSuccess = cAtFalse;
    eAtPrbsMode mode = CliStringToEnum(modeString, cAtPrbsLengthModeStr, cAtPrbsLengthModeVal, mCount(cAtPrbsLengthModeVal), &convertSuccess);

    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid PRBS mode, expect: ");
        CliExpectedValuesPrint(cSevCritical, cAtPrbsLengthModeStr, mCount(cAtPrbsLengthModeStr));
        AtPrintc(cSevCritical, "\r\n");
        }

    return AttributeSet(engines, mode, Func);
    }

static eBool BitOrderSet(AtList engines, const char *bitOrderString, AttributeSetFunc Func)
    {
    eBool convertSuccess = cAtFalse;
    eAtPrbsBitOrder order = CliStringToEnum(bitOrderString, cAtPrbsBitOrderStr, cAtPrbsBitOrderVal, mCount(cAtPrbsBitOrderVal), &convertSuccess);

    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid bit order mode, expect: ");
        CliExpectedValuesPrint(cSevCritical, cAtPrbsBitOrderStr, mCount(cAtPrbsBitOrderStr));
        AtPrintc(cSevCritical, "\r\n");
        }

    return AttributeSet(engines, order, Func);
    }

static uint64 CounterValueFromCounterType(AtPrbsEngine self, eBool r2c, uint16 counterType, eBool isLongCounter)
    {
    if (isLongCounter)
        {
        if (r2c)
            return AtPrbsEngineCounter64BitClear(self, counterType);
        return AtPrbsEngineCounter64BitGet(self, counterType);
        }
    else
        {
        if (r2c)
            return AtPrbsEngineCounterClear(self, counterType);
        return AtPrbsEngineCounterGet(self, counterType);
        }
    }

static void PutGating(tTab *tabPtr, uint32 row, uint32 column, AtPrbsEngine engine)
    {
    char buffer[32];
    uint32 durationMs, elapseTimeMs;
    AtTimer timer = AtPrbsEngineGatingTimer(engine);

    if (timer == NULL)
        {
        StrToCell(tabPtr, row, column, sAtNotSupported);
        return;
        }

    if (!AtTimerIsStarted(timer))
        {
        StrToCell(tabPtr, row, column, "not-started");
        return;
        }

    if (!AtTimerIsRunning(timer))
        {
        StrToCell(tabPtr, row, column, "not-running");
        return;
        }

    durationMs = AtTimerDurationGet(timer);

    if (durationMs == 0)
        {
        ColorStrToCell(tabPtr, row, column, "none-stop", cSevInfo);
        return;
        }

    elapseTimeMs = AtTimerElapsedTimeGet(timer);
    if (elapseTimeMs == cInvalidUint32)
        {
        ColorStrToCell(tabPtr, row, column, CliNumber2String(AtTimerDurationGet(timer) / 1000, "running/%u(s)"), cSevInfo);
        return;
        }

    AtSnprintf(buffer, sizeof(buffer) - 1, "%u(s)/%u(s)", elapseTimeMs / 1000, durationMs / 1000);
    ColorStrToCell(tabPtr, row, column, buffer, cSevInfo);
    }

static eAtRet GatingStart(AtPrbsEngine self)
    {
    return AtTimerStart(AtPrbsEngineGatingTimer(self));
    }

static eAtRet GatingStop(AtPrbsEngine self)
    {
    return AtTimerStop(AtPrbsEngineGatingTimer(self));
    }

static eAtRet GatingDurationSet(AtPrbsEngine self, uint32 value)
    {
    return AtTimerDurationSet(AtPrbsEngineGatingTimer(self), value);
    }

static eBool IsActive(AtPrbsEngine engine)
    {
    eBool rxEnabled = AtPrbsEngineRxIsEnabled(engine);
    eBool gatingStarted = cAtFalse;
    AtTimer timer = AtPrbsEngineGatingTimer(engine);
    if (timer)
        gatingStarted = AtTimerIsStarted(timer);

    return (rxEnabled || gatingStarted) ? cAtTrue : cAtFalse;
    }

static eAtSevLevel ColorCorrect(eBool shouldDisplayColor, eAtSevLevel color)
    {
    return shouldDisplayColor ? color : cSevNormal;
    }

static eBool PutInvert(tTab *tabPtr, uint32 row, uint32 column, AtPrbsEngine engine)
    {
    uint32 bufferSize;
    char *buffer = CliSharedCharBufferGet(&bufferSize);

    if (mBoolToBin(AtPrbsEngineTxIsInverted(engine)) == mBoolToBin(AtPrbsEngineRxIsInverted(engine)))
        return StrToCell(tabPtr, row, column, CliBoolToString(AtPrbsEngineIsInverted(engine)));

    AtSnprintf(buffer, bufferSize, "TX: %s, RX: %s",
              CliBoolToString(AtPrbsEngineTxIsInverted(engine)),
              CliBoolToString(AtPrbsEngineRxIsInverted(engine)));

    return StrToCell(tabPtr, row, column, buffer);
    }

static void CliPrbsCounterPrintToCell(tTab *tabPtr,
                           uint32 row,
                           uint32 column,
                           uint64 counterValue,
                           eAtCounterType counterType,
                           eBool isValidCounter,
                           eBool isLongCounter)
    {
    if (isLongCounter)
        CliCounter64BitPrintToCell(tabPtr, row, column, counterValue, counterType, isValidCounter);
    else
        CliCounterPrintToCell(tabPtr, row, column, (uint32)counterValue, counterType, isValidCounter);
    }

eBool CliAtPrbsEngineDelayEnable(AtList engines, eBool enable)
    {
	if (enable)
		return AttributeNoParamSet(engines, AtPrbsEngineDelayEnable);
    return AttributeNoParamSet(engines, AtPrbsEngineDelayDisable);
    }

static eBool CliAtPrbsEngineLongCounterGet(AtList engines, char *readingModeStr, char *verboseMode, eBool isLongCounter)
    {
    uint32 engine_i;
    tTab *tabPtr = NULL;
    eBool silent = cAtFalse;
    eAtHistoryReadingMode readingMode = cAtHistoryReadingModeUnknown;
    eBool r2c;
    const char *heading[] =
        {
        "Channel",
        "engineId",
        "txFrame",
        "TxBit",
        "rxFrame",
        "RxBit",
        "RxBitError",
        "RxBitLastSync",
        "RxBitErrorLastSync",
        "RxSync",
        "RxLoss",
        "RxLostFrame",
        "RxErrorFrame"
        };

    if (AtListLengthGet(engines) == 0)
        {
        AtPrintc(cSevWarning, "No engine to display\r\n");
        return cAtTrue;
        }

    readingMode = CliHistoryReadingModeGet(readingModeStr);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }
    r2c = (readingMode == cAtHistoryReadingModeReadToClear) ? cAtTrue : cAtFalse;

    silent = CliHistoryReadingShouldSilent(readingMode, verboseMode);

    if (!silent)
        {
        tabPtr = TableAlloc(AtListLengthGet(engines), mCount(heading), heading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "Cannot create table\r\n");
            return cAtFalse;
            }
        }

    for (engine_i = 0; engine_i < AtListLengthGet(engines); engine_i = AtCliNextIdWithSleep(engine_i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtPrbsEngine engine = (AtPrbsEngine)AtListObjectGet(engines, engine_i);
        eBool isSupportedCounter = cAtTrue;
        uint64 counterValue = 0;
        uint8 column = 0;

        AtPrbsEngineAllCountersLatchAndClear(engine, r2c);
        StrToCell(tabPtr, engine_i, column++, AtPrbsEngineChannelDescription(engine));
        StrToCell(tabPtr, engine_i, column++, CliNumber2String(AtPrbsEngineIdGet(engine) + 1, "%u"));

        isSupportedCounter = AtPrbsEngineCounterIsSupported(engine, cAtPrbsEngineCounterTxFrame);
        if (isSupportedCounter)
            counterValue = CounterValueFromCounterType(engine, r2c, cAtPrbsEngineCounterTxFrame, isLongCounter);
        CliPrbsCounterPrintToCell(tabPtr, engine_i, column++, counterValue, isSupportedCounter ? cAtCounterTypeGood : cAtCounterTypeNA, cAtTrue, isLongCounter);

        isSupportedCounter = AtPrbsEngineCounterIsSupported(engine, cAtPrbsEngineCounterTxBit);
        if (isSupportedCounter)
            counterValue = CounterValueFromCounterType(engine, r2c, cAtPrbsEngineCounterTxBit, isLongCounter);
        CliPrbsCounterPrintToCell(tabPtr, engine_i, column++, counterValue, isSupportedCounter ? cAtCounterTypeGood : cAtCounterTypeNA, cAtTrue, isLongCounter);

        isSupportedCounter = AtPrbsEngineCounterIsSupported(engine, cAtPrbsEngineCounterRxFrame);
        if (isSupportedCounter)
            counterValue = CounterValueFromCounterType(engine, r2c, cAtPrbsEngineCounterRxFrame, isLongCounter);
        CliPrbsCounterPrintToCell(tabPtr, engine_i, column++, counterValue, isSupportedCounter ? cAtCounterTypeGood : cAtCounterTypeNA, cAtTrue, isLongCounter);

        isSupportedCounter = AtPrbsEngineCounterIsSupported(engine, cAtPrbsEngineCounterRxBit);
        if (isSupportedCounter)
            counterValue = CounterValueFromCounterType(engine, r2c, cAtPrbsEngineCounterRxBit, isLongCounter);
        CliPrbsCounterPrintToCell(tabPtr, engine_i, column++, counterValue, isSupportedCounter ? cAtCounterTypeGood : cAtCounterTypeNA, cAtTrue, isLongCounter);

        isSupportedCounter = AtPrbsEngineCounterIsSupported(engine, cAtPrbsEngineCounterRxBitError);
        if (isSupportedCounter)
            counterValue = CounterValueFromCounterType(engine, r2c, cAtPrbsEngineCounterRxBitError, isLongCounter);
        CliPrbsCounterPrintToCell(tabPtr, engine_i, column++, counterValue, isSupportedCounter ? cAtCounterTypeError : cAtCounterTypeNA, cAtTrue, isLongCounter);

        isSupportedCounter = AtPrbsEngineCounterIsSupported(engine, cAtPrbsEngineCounterRxBitLastSync);
        if (isSupportedCounter)
            counterValue = CounterValueFromCounterType(engine, r2c, cAtPrbsEngineCounterRxBitLastSync, isLongCounter);
        CliPrbsCounterPrintToCell(tabPtr, engine_i, column++, counterValue, isSupportedCounter ? cAtCounterTypeGood : cAtCounterTypeNA, cAtTrue, isLongCounter);

        isSupportedCounter = AtPrbsEngineCounterIsSupported(engine, cAtPrbsEngineCounterRxBitErrorLastSync);
        if (isSupportedCounter)
            counterValue = CounterValueFromCounterType(engine, r2c, cAtPrbsEngineCounterRxBitErrorLastSync, isLongCounter);
        CliPrbsCounterPrintToCell(tabPtr, engine_i, column++, counterValue, isSupportedCounter ? cAtCounterTypeError : cAtCounterTypeNA, cAtTrue, isLongCounter);

        isSupportedCounter = AtPrbsEngineCounterIsSupported(engine, cAtPrbsEngineCounterRxSync);
        if (isSupportedCounter)
            counterValue = CounterValueFromCounterType(engine, r2c, cAtPrbsEngineCounterRxSync, isLongCounter);
        CliPrbsCounterPrintToCell(tabPtr, engine_i, column++, counterValue, isSupportedCounter ? cAtCounterTypeGood : cAtCounterTypeNA, cAtTrue, isLongCounter);

        isSupportedCounter = AtPrbsEngineCounterIsSupported(engine, cAtPrbsEngineCounterRxBitLoss);
        if (isSupportedCounter)
            counterValue = CounterValueFromCounterType(engine, r2c, cAtPrbsEngineCounterRxBitLoss, isLongCounter);
        CliPrbsCounterPrintToCell(tabPtr, engine_i, column++, counterValue, isSupportedCounter ? cAtCounterTypeError : cAtCounterTypeNA, cAtTrue, isLongCounter);

        isSupportedCounter = AtPrbsEngineCounterIsSupported(engine, cAtPrbsEngineCounterRxLostFrame);
        if (isSupportedCounter)
            counterValue = CounterValueFromCounterType(engine, r2c, cAtPrbsEngineCounterRxLostFrame, isLongCounter);
        CliPrbsCounterPrintToCell(tabPtr, engine_i, column++, counterValue, isSupportedCounter ? cAtCounterTypeError : cAtCounterTypeNA, cAtTrue, isLongCounter);

        isSupportedCounter = AtPrbsEngineCounterIsSupported(engine, cAtPrbsEngineCounterRxErrorFrame);
        if (isSupportedCounter)
            counterValue = CounterValueFromCounterType(engine, r2c, cAtPrbsEngineCounterRxErrorFrame, isLongCounter);
        CliPrbsCounterPrintToCell(tabPtr, engine_i, column++, counterValue, isSupportedCounter ? cAtCounterTypeError : cAtCounterTypeNA, cAtTrue, isLongCounter);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

char *CliPrbsAlarmToString(uint32 alarm)
    {
    static char buf[32];

    if (alarm == 0)
        {
        AtSprintf(buf, "none");
        return buf;
        }

    AtOsalMemInit(buf, 0, sizeof(buf));
    if (alarm & cAtPrbsEngineAlarmTypeError)
        {
        AtStrcat(buf, "|");
        AtStrcat(buf, "error");
        }
    if (alarm & cAtPrbsEngineAlarmTypeLossSync)
        {
        AtStrcat(buf, "|");
        AtStrcat(buf, "lossSync");
        }

    return &buf[1]; /* Get rid of the first '|' */
    }

eBool CliAtPrbsEngineModeSet(AtList engines, const char *modeString)
    {
    return ModeSet(engines, modeString, (AttributeSetFunc)AtPrbsEngineModeSet);
    }

eBool CliAtPrbsEngineDataModeSet(AtList engines, const char *modeString, eBool isTx, eBool isRx)
    {
    if (isTx==cAtTrue && isRx==cAtTrue)
        return DataModeSet(engines, modeString, (AttributeSetFunc)AtPrbsEngineDataModeSet);
    else if (isTx==cAtTrue)
        return DataModeSet(engines, modeString, (AttributeSetFunc)AtPrbsEngineTxDataModeSet);
    return DataModeSet(engines, modeString, (AttributeSetFunc)AtPrbsEngineRxDataModeSet);
    }

eBool CliAtPrbsEngineInvertWithHandler(AtList engines, eBool invert, eAtModulePrbsRet (*Invert)(AtPrbsEngine self, eBool invert))
    {
    uint32 i;
    eBool success = cAtTrue;

    if (AtListLengthGet(engines) == 0)
        {
        AtPrintc(cSevWarning, "No engine to configure\r\n");
        return cAtTrue;
        }

    for (i = 0; i < AtListLengthGet(engines); i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtPrbsEngine engine = (AtPrbsEngine)AtListObjectGet(engines, i);
        eAtRet ret = Invert(engine, invert);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot %s inverting for PRBS engine %s, ret = %s\r\n",
                     invert ? "enable" : "disable",
                     AtPrbsEngineChannelDescription(engine),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CliAtPrbsEngineInvert(AtList engines, eBool invert)
    {
    return CliAtPrbsEngineInvertWithHandler(engines, invert, AtPrbsEngineInvert);
    }

eBool CliAtPrbsEngineErrorForce(AtList engines, eBool force)
    {
    return AttributeSet(engines, force, (AttributeSetFunc)AtPrbsEngineErrorForce);
    }

eBool CliAtPrbsEngineEnable(AtList engines, eBool enable)
    {
    return AttributeSet(engines, enable, (AttributeSetFunc)AtPrbsEngineEnable);
    }

eBool CliAtPrbsEngineCounterGet(AtList engines, char *readingModeStr, char *verboseMode)
    {
    return CliAtPrbsEngineLongCounterGet(engines, readingModeStr, verboseMode, cAtTrue);
    }

eBool CliAtPrbsEngineCounter64BitGet(AtList engines, char *readingModeStr, char *verboseMode)
    {
    return CliAtPrbsEngineLongCounterGet(engines, readingModeStr, verboseMode, cAtTrue);
    }

eBool CliAtPrbsEngineDebug(AtList engines)
    {
    uint32 engine_i;

    if (AtListLengthGet(engines) == 0)
        {
        AtPrintc(cSevWarning, "No engine to display\r\n");
        return cAtTrue;
        }

    for (engine_i = 0; engine_i < AtListLengthGet(engines); engine_i = AtCliNextIdWithSleep(engine_i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtPrbsEngine engine = (AtPrbsEngine)AtListObjectGet(engines, engine_i);
        AtPrintc(cSevInfo, "* Engine: %s\r\n", AtPrbsEngineChannelDescription(engine));
        AtPrbsEngineDebug(engine);
        }

    return cAtTrue;
    }

eBool CliAtPrbsEngineTxModeSet(AtList engines, const char *modeString)
    {
    return ModeSet(engines, modeString, (AttributeSetFunc)AtPrbsEngineTxModeSet);
    }

eBool CliAtPrbsEngineRxModeSet(AtList engines, const char *modeString)
    {
    return ModeSet(engines, modeString, (AttributeSetFunc)AtPrbsEngineRxModeSet);
    }

eBool CliAtPrbsEngineTxEnable(AtList engines, eBool enable)
    {
    return AttributeSet(engines, enable, (AttributeSetFunc)AtPrbsEngineTxEnable);
    }

eBool CliAtPrbsEngineRxEnable(AtList engines, eBool enable)
    {
    return AttributeSet(engines, enable, (AttributeSetFunc)AtPrbsEngineRxEnable);
    }

eBool CliAtPrbsEngineShowWithSilent(AtList engines, eBool printEngineIdFirst, eBool silentMode)
    {
    uint32 engine_i;
    const char *aString;
    const char *pHeading[] = {"channel",
                              "engineId",
                              "txMode", "rxMode", "txFixPattern", "rxFixPattern",
                              "invert", "forceError", "txEnabled", "rxEnabled",
                              "txBitOrder", "rxBitOrder", "alarm", "sticky", "genSide", "monSide",
                              "gating", "genData", "monData", "isRaw"};
    tTab *tabPtr = NULL;
    eBool convertSuccess, boolVal;

    if (printEngineIdFirst)
        {
        pHeading[0] = "engineId";
        pHeading[1] = "channel";
        }
    
    if (AtListLengthGet(engines) == 0)
        {
        AtPrintc(cSevWarning, "No engine to display\r\n");
        return cAtTrue;
        }

    if (!silentMode)
        {
        tabPtr = TableAlloc(AtListLengthGet(engines), mCount(pHeading), pHeading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "Cannot create table\r\n");
            return cAtFalse;
            }
        }

    for (engine_i = 0; engine_i < AtListLengthGet(engines); engine_i = AtCliNextIdWithSleep(engine_i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtPrbsEngine engine = (AtPrbsEngine)AtListObjectGet(engines, engine_i);
        uint32 alarm = AtPrbsEngineAlarmGet(engine);
        uint32 sticky = AtPrbsEngineAlarmHistoryClear(engine);
        uint8 column = 0;
        eBool isActive;

        if (printEngineIdFirst)
            {
            StrToCell(tabPtr, engine_i, column++, CliNumber2String(AtPrbsEngineIdGet(engine) + 1, "%d"));
            StrToCell(tabPtr, engine_i, column++, AtPrbsEngineChannelDescription(engine));
            }
        else
            {
            StrToCell(tabPtr, engine_i, column++, AtPrbsEngineChannelDescription(engine));
            StrToCell(tabPtr, engine_i, column++, CliNumber2String(AtPrbsEngineIdGet(engine) + 1, "%d"));
            }

        aString = CliEnumToString(AtPrbsEngineTxModeGet(engine), cAtPrbsModeStr, cAtPrbsModeVal, mCount(cAtPrbsModeVal), &convertSuccess);
        ColorStrToCell(tabPtr, engine_i, column++, convertSuccess ? aString : "error", convertSuccess ? cSevNormal : cSevCritical);

        aString = CliEnumToString(AtPrbsEngineRxModeGet(engine), cAtPrbsModeStr, cAtPrbsModeVal, mCount(cAtPrbsModeVal), &convertSuccess);
        ColorStrToCell(tabPtr, engine_i, column++, convertSuccess ? aString : "error", convertSuccess ? cSevNormal : cSevCritical);

        /* TX fixed pattern */
        if (AtPrbsModeIsFixedPattern(AtPrbsEngineTxModeGet(engine)))
            StrToCell(tabPtr, engine_i, column++, CliNumber2String(AtPrbsEngineTxFixedPatternGet(engine), "0x%08x"));
        else
            StrToCell(tabPtr, engine_i, column++, sAtNotApplicable);

        /* RX fixed pattern */
        if (AtPrbsModeIsFixedPattern(AtPrbsEngineRxModeGet(engine)))
            StrToCell(tabPtr, engine_i, column++, CliNumber2String(AtPrbsEngineRxFixedPatternGet(engine),"0x%08x"));
        else
            StrToCell(tabPtr, engine_i, column++, sAtNotApplicable);

        PutInvert(tabPtr, engine_i, column++, engine);

        boolVal = AtPrbsEngineErrorIsForced(engine);
        ColorStrToCell(tabPtr, engine_i, column++, CliBoolToString(boolVal), boolVal ? cSevCritical : cSevNormal);

        boolVal = AtPrbsEngineTxIsEnabled(engine);
        ColorStrToCell(tabPtr, engine_i, column++, CliBoolToString(boolVal), boolVal ? cSevInfo : cSevCritical);

        boolVal = AtPrbsEngineRxIsEnabled(engine);
        ColorStrToCell(tabPtr, engine_i, column++, CliBoolToString(boolVal), boolVal ? cSevInfo : cSevCritical);

        aString = CliEnumToString(AtPrbsEngineTxBitOrderGet(engine), cAtPrbsBitOrderStr, cAtPrbsBitOrderVal, mCount(cAtPrbsBitOrderVal), &convertSuccess);
        ColorStrToCell(tabPtr, engine_i, column++, convertSuccess ? aString : "error", convertSuccess ? cSevNormal : cSevCritical);

        aString = CliEnumToString(AtPrbsEngineRxBitOrderGet(engine), cAtPrbsBitOrderStr, cAtPrbsBitOrderVal, mCount(cAtPrbsBitOrderVal), &convertSuccess);
        ColorStrToCell(tabPtr, engine_i, column++, convertSuccess ? aString : "error", convertSuccess ? cSevNormal : cSevCritical);

        isActive = IsActive(engine);
        ColorStrToCell(tabPtr, engine_i, column++, CliPrbsAlarmToString(alarm), ColorCorrect(isActive, alarm ? cSevCritical : cSevInfo));
        ColorStrToCell(tabPtr, engine_i, column++, CliPrbsAlarmToString(sticky), ColorCorrect(isActive, sticky ? cSevCritical : cSevInfo));

        ColorStrToCell(tabPtr, engine_i, column++, SideString(AtPrbsEngineGeneratingSideGet(engine)), cSevInfo);
        ColorStrToCell(tabPtr, engine_i, column++, SideString(AtPrbsEngineMonitoringSideGet(engine)), cSevInfo);

        PutGating(tabPtr, engine_i, column++, engine);

        aString = CliEnumToString(AtPrbsEngineTxDataModeGet(engine), cAtPrbsDataModeStr, cAtPrbsDataModeVal, mCount(cAtPrbsDataModeVal), &convertSuccess);
        StrToCell(tabPtr, engine_i, column++, convertSuccess ? aString : "N/A");

        aString = CliEnumToString(AtPrbsEngineRxDataModeGet(engine), cAtPrbsDataModeStr, cAtPrbsDataModeVal, mCount(cAtPrbsDataModeVal), &convertSuccess);
        StrToCell(tabPtr, engine_i, column++, convertSuccess ? aString : "N/A");

        StrToCell(tabPtr, engine_i, column++, AtPrbsEngineIsRawPrbs(engine) ? "en" : "dis");
        }

    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CliAtPrbsEngineShow(AtList engines, eBool silentMode)
    {
    return CliAtPrbsEngineShowWithSilent(engines, cAtFalse, silentMode);
    }

eBool CliAtPrbsEngineTxErrorRateForce(AtList engines, char* pErrorRateStr)
    {
    uint32 i, errorRate;
    eBool success = cAtTrue;

    if (AtListLengthGet(engines) == 0)
        {
        AtPrintc(cSevWarning, "No engine to configure\r\n");
        return cAtTrue;
        }

    errorRate = CliStringToEnum(pErrorRateStr, cAtPrbsBerRateStr, cAtPrbsBerRateVal, mCount(cAtPrbsBerRateVal), &success);
    if (success == cAtFalse)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Please input correct parameter. Expect: ");
        CliExpectedValuesPrint(cSevCritical,
                               cAtPrbsBerRateStr,
                               mCount(cAtPrbsBerRateStr));
        AtPrintc(cSevCritical, "\r\n");
        return cAtFalse;
        }

    for (i = 0; i < AtListLengthGet(engines); i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        eAtRet ret;
        AtPrbsEngine engine = (AtPrbsEngine) AtListObjectGet(engines, i);

        if (errorRate == 0)
            {
            ret = AtPrbsEngineErrorForce(engine, cAtFalse);
            if (ret != cAtOk)
                {
                AtPrintc(cSevCritical,
                         "ERROR: Cannot disable forcing error for PRBS engine %s, ret = %s\r\n",
                         AtPrbsEngineChannelDescription(engine),
                         AtRet2String(ret));
                success = cAtFalse;
                }
            }
        else
            {
            ret = AtPrbsEngineTxErrorRateSet(engine, errorRate);
            if (ret != cAtOk)
                {
                AtPrintc(cSevCritical,
                         "ERROR: Cannot set Bit-Error-Rate for PRBS engine %s, ret = %s\r\n",
                         AtPrbsEngineChannelDescription(engine),
                         AtRet2String(ret));
                success = cAtFalse;
                }

            ret = AtPrbsEngineErrorForce(engine, cAtTrue);
            if (ret != cAtOk)
                {
                AtPrintc(cSevCritical,
                         "ERROR: Cannot enable forcing error for PRBS engine %s, ret = %s\r\n",
                         AtPrbsEngineChannelDescription(engine),
                         AtRet2String(ret));
                success = cAtFalse;
                }
            }
        }

    return success;
    }

eBool CliAtPrbsEngineBitErrorRateShow(AtList engines)
    {
    tTab *tabPtr;
    uint32 engine_i;
    const char *aString;
    const char *pHeading[] =
        {
        "channel",  "Tx-ErrorRate", "Rx-ErrorRate"
        };
    eBool convertSuccess;

    if (AtListLengthGet(engines) == 0)
        {
        AtPrintc(cSevWarning, "No engine to display\r\n");
        return cAtTrue;
        }

    tabPtr = TableAlloc(AtListLengthGet(engines), mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot create table\r\n");
        return cAtFalse;
        }

    for (engine_i = 0; engine_i < AtListLengthGet(engines); engine_i = AtCliNextIdWithSleep(engine_i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtPrbsEngine engine = (AtPrbsEngine) AtListObjectGet(engines, engine_i);
        uint8 column = 0;

        StrToCell(tabPtr, engine_i, column++, AtPrbsEngineChannelDescription(engine));

        aString = CliEnumToString(AtPrbsEngineTxErrorRateGet(engine),
                                  cAtPrbsBerRateStr,
                                  cAtPrbsBerRateVal,
                                  mCount(cAtPrbsBerRateVal),
                                  &convertSuccess);
        ColorStrToCell(tabPtr,
                       engine_i,
                       column++,
                       convertSuccess ? aString : "error",
                       convertSuccess ? cSevNormal : cSevCritical);

        aString = CliEnumToString(AtPrbsEngineRxErrorRateGet(engine),
                                  cAtPrbsBerRateStr,
                                  cAtPrbsBerRateVal,
                                  mCount(cAtPrbsBerRateVal),
                                  &convertSuccess);
        ColorStrToCell(tabPtr,
                       engine_i,
                       column++,
                       convertSuccess ? aString : "error",
                       convertSuccess ? cSevNormal : cSevCritical);
        }

    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CliAtPrbsEngineTxFixedPatternSet(AtList engines, uint32 fixPattern)
    {
    return AttributeSet(engines, fixPattern, (AttributeSetFunc)AtPrbsEngineTxFixedPatternSet);
    }

eBool CliAtPrbsEngineRxFixedPatternSet(AtList engines, uint32 fixPattern)
    {
    return AttributeSet(engines, fixPattern, (AttributeSetFunc)AtPrbsEngineRxFixedPatternSet);
    }

static eBool SideSet(AtList engines, const char *sideString, AttributeSetFunc Func)
    {
    eBool convertSuccess = cAtFalse;
    eAtPrbsSide side = CliStringToEnum(sideString, cAtPrbsSideStr, cAtPrbsSideVal, mCount(cAtPrbsSideVal), &convertSuccess);

    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid PRBS side, expect: ");
        CliExpectedValuesPrint(cSevCritical, cAtPrbsSideStr, mCount(cAtPrbsSideStr));
        AtPrintc(cSevCritical, "\r\n");
        }

    return AttributeSet(engines, side, Func);
    }

eBool CliAtPrbsEngineSideSet(AtList engines, const char *sideString)
    {
    return SideSet(engines, sideString, (AttributeSetFunc)AtPrbsEngineSideSet);
    }

eBool CliAtPrbsEngineGeneratingSideSet(AtList engines, const char *sideString)
    {
    return SideSet(engines, sideString, (AttributeSetFunc)AtPrbsEngineGeneratingSideSet);
    }

eBool CliAtPrbsEngineMonitoringSideSet(AtList engines, const char *sideString)
    {
    return SideSet(engines, sideString, (AttributeSetFunc)AtPrbsEngineMonitoringSideSet);
    }

eBool CliAtPrbsEngineErrorInject(AtList engines, uint32 numErrors)
    {
    return AttributeSet(engines, numErrors, (AttributeSetFunc)AtPrbsEngineTxErrorInject);
    }

eBool CliAtPrbsEngineBitOrderSet(AtList engines, const char *bitOrderString)
    {
    return BitOrderSet(engines, bitOrderString, (AttributeSetFunc)AtPrbsEngineBitOrderSet);
    }

eBool CliAtPrbsEngineShowWithHandler(char argc, char **argv, EngineFuncGet engineGet)
    {
    AtList engines;
    eBool success = cAtTrue;
    eBool silent = cAtFalse;

    if (argc > 1)
        silent = CliIsSilentIndication(argv[1]);

    engines = engineGet(argv[0]);
    success = CliAtPrbsEngineShow(engines, silent);
    AtObjectDelete((AtObject)engines);

    return success;
    }

eBool CmdAtPrbsEngineDebug(char argc, char **argv)
    {
    AtList engines = CliPrbsEnginesGet(argv[0]);
    eBool ret = CliAtPrbsEngineDebug(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return ret;
    }

uint32 CliPrbsModeStringToEnum(const char *modeString)
    {
    eBool convertSuccess = cAtFalse;
    eAtPrbsMode mode = CliStringToEnum(modeString, cAtPrbsModeStr, cAtPrbsModeVal, mCount(cAtPrbsModeVal), &convertSuccess);

    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid PRBS mode, expect: ");
        CliExpectedValuesPrint(cSevCritical, cAtPrbsModeStr, mCount(cAtPrbsModeStr));
        AtPrintc(cSevCritical, "\r\n");
        return cAtPrbsModeInvalid;
        }

    return (uint32) mode;
    }

eBool CliAtPrbsEngineStart(AtList engines)
    {
    return AttributeNoParamSet(engines, GatingStart);
    }

eBool CliAtPrbsEngineStop(AtList engines)
    {
    return AttributeNoParamSet(engines, GatingStop);
    }

eBool CliAtPrbsEngineDurationSet(AtList engines, const char *durInMs)
    {
    uint32 durationInMs = AtStrtoul(durInMs, NULL, 10);
    return AttributeSet(engines, durationInMs, GatingDurationSet);
    }

eBool CliAtPrbsEngineDisruptionMaxExpectationTimeSet(AtList engines, const char *durInMs)
    {
    uint32 durationInMs = AtStrtoul(durInMs, NULL, 10);
    return AttributeSet(engines, durationInMs, AtPrbsEngineDisruptionMaxExpectedTimeSet);
    }

eBool CliAtPrbsEngineGatingShow(AtList engines)
    {
    uint32 engine_i;
    const char *pHeading[] = {"channel", "engineId", "controllable", "duration(ms)", "elapsedTime(ms)", "started", "running"};
    tTab *tabPtr = NULL;

    if (AtListLengthGet(engines) == 0)
        {
        AtPrintc(cSevWarning, "No engine to display\r\n");
        return cAtTrue;
        }

    tabPtr = TableAlloc(AtListLengthGet(engines), mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot create table\r\n");
        return cAtFalse;
        }

    for (engine_i = 0; engine_i < AtListLengthGet(engines); engine_i++)
        {
        AtPrbsEngine engine = (AtPrbsEngine)AtListObjectGet(engines, engine_i);
        AtTimer timer = AtPrbsEngineGatingTimer(engine);
        uint8 column = 0;
        eBool boolVal;
        uint32 value;

        StrToCell(tabPtr, engine_i, column++, AtPrbsEngineChannelDescription(engine));
        StrToCell(tabPtr, engine_i, column++, CliNumber2String(AtPrbsEngineIdGet(engine) + 1, "%d"));

        /* Controllable */
        boolVal = timer ? cAtTrue : cAtFalse;
        ColorStrToCell(tabPtr, engine_i, column++, boolVal ? "yes" : "no", boolVal ? cSevInfo : cSevCritical);

        /* Not controllable, just invalidate all fields */
        if (!boolVal)
            {
            for (; column < mCount(pHeading); column++)
                StrToCell(tabPtr, engine_i, column, sAtNotCare);
            continue;
            }

        StrToCell(tabPtr, engine_i, column++, CliNumber2String(AtTimerDurationGet(timer), "%d"));

        value = AtTimerElapsedTimeGet(timer);
        if (value == cInvalidUint32)
            StrToCell(tabPtr, engine_i, column++, sAtNotSupported);
        else
            StrToCell(tabPtr, engine_i, column++, CliNumber2String(AtTimerElapsedTimeGet(timer), "%d"));

        boolVal = AtTimerIsStarted(timer);
        ColorStrToCell(tabPtr, engine_i, column++, boolVal ? "yes" : "no", boolVal ? cSevInfo : cSevNormal);

        boolVal = AtTimerIsRunning(timer);
        ColorStrToCell(tabPtr, engine_i, column++, boolVal ? "yes" : "no", boolVal ? cSevInfo : cSevNormal);
        }
    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CliAtPrbsEngineAlarmHistoryShow(AtList engines, char *readingModeStr, char *verboseMode)
    {
    uint32 engine_i;
    eBool r2c;
    eBool silent = cAtFalse;
    eAtHistoryReadingMode readingMode = cAtHistoryReadingModeUnknown;
    const char *pHeading[] = {"Channel", "Engine ID", "Error", "Loss"};
    tTab *tabPtr = NULL;

    if (AtListLengthGet(engines) == 0)
        {
        AtPrintc(cSevWarning, "No engine to display\r\n");
        return cAtTrue;
        }

    readingMode = CliHistoryReadingModeGet(readingModeStr);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    r2c = (readingMode == cAtHistoryReadingModeReadToClear) ? cAtTrue : cAtFalse;
    silent = CliHistoryReadingShouldSilent(readingMode, verboseMode);
    
    if (!silent)
        {
        tabPtr = TableAlloc(AtListLengthGet(engines), mCount(pHeading), pHeading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "Cannot create table\r\n");
            return cAtFalse;
            }
        }

    for (engine_i = 0; engine_i < AtListLengthGet(engines); engine_i = AtCliNextIdWithSleep(engine_i, AtCliLimitNumRestTdmChannelGet()))
        {
        uint32 alarm = 0;
        uint8 column = 0;
        uint32 numAlarms;
        uint32 j;
        AtPrbsEngine engine = (AtPrbsEngine)AtListObjectGet(engines, engine_i);

        if (r2c)
            alarm = AtPrbsEngineAlarmHistoryClear(engine);
        else
            alarm = AtPrbsEngineAlarmHistoryGet(engine);

        StrToCell(tabPtr, engine_i, column++, CliChannelIdStringGet(AtPrbsEngineChannelGet(engine)));
        StrToCell(tabPtr, engine_i, column++, CliNumber2String(AtPrbsEngineIdGet(engine) + 1, "%d"));

        numAlarms = mCount(cAtPrbsAlarmTypeVal);
        for (j = 0; j < numAlarms; j++)
           {
           if (alarm & cAtPrbsAlarmTypeVal[j])
               ColorStrToCell(tabPtr, engine_i, column++, "set", cSevCritical);
           else
               ColorStrToCell(tabPtr, engine_i, column++, "clear", cSevInfo);
           }
        }

    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

const char *CliPrbsModeToString(eAtPrbsMode mode)
    {
    return CliEnumToString(mode,
                           cAtPrbsModeStr,
                           cAtPrbsModeVal,
                           mCount(cAtPrbsModeVal),
                           NULL);
    }

eBool CliAtPrbsEngineDelayShow(AtList engines, char *readingModeStr, char *verboseMode)
    {
	AtModulePrbs prbsModule;
	uint32 engine_i;
	eBool r2c = cAtFalse;
	eBool silent = cAtFalse;
    tTab *tabPtr = NULL;
    eBool success = cAtTrue;
    eAtHistoryReadingMode readingMode = cAtHistoryReadingModeUnknown;
    float delay = 0;
    char buf[80];
    const char *pHeading[] = {"Channel", "Engine ID", "MaxDelay(ms)", "MinDelay(ms)",  "AverageDelay(ms)"};

    prbsModule = (AtModulePrbs)AtDeviceModuleGet(CliDevice(), cAtModulePrbs);
    if (AtListLengthGet(engines) == 0)
            {
            AtPrintc(cSevWarning, "No engine to display\r\n");
            return cAtTrue;
            }

	readingMode = CliHistoryReadingModeGet(readingModeStr);
	if (readingMode == cAtHistoryReadingModeUnknown)
		{
		AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
		return cAtFalse;
		}

	r2c = (readingMode == cAtHistoryReadingModeReadToClear) ? cAtTrue : cAtFalse;
	silent = CliHistoryReadingShouldSilent(readingMode, verboseMode);

	if (!silent)
		{
		tabPtr = TableAlloc(AtListLengthGet(engines), mCount(pHeading), pHeading);
		if (!tabPtr)
			{
			AtPrintc(cSevCritical, "Cannot create table\r\n");
			return cAtFalse;
			}
		}

    tabPtr = TableAlloc(AtListLengthGet(engines), mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (engine_i = 0; engine_i < AtListLengthGet(engines); engine_i = AtCliNextIdWithSleep(engine_i, AtCliLimitNumRestTdmChannelGet()))
        {
    	AtPrbsEngine engine = (AtPrbsEngine)AtListObjectGet(engines, engine_i);
        uint8 column = 0;

        StrToCell(tabPtr, engine_i, column++, CliChannelIdStringGet(AtPrbsEngineChannelGet(engine)));
		StrToCell(tabPtr, engine_i, column++, CliNumber2String(AtPrbsEngineIdGet(engine) + 1, "%d"));

        delay = AtPrbsEngineMaxDelayGet(engine, r2c);
        AtSprintf(buf, "%.6f", delay);
        StrToCell(tabPtr, engine_i, column++, buf);

        delay = AtPrbsEngineMinDelayGet(engine, r2c);
		AtSprintf(buf, "%.6f", delay);
		StrToCell(tabPtr, engine_i, column++, buf);

		if (!ThaAttModulePrbsNeedDelayAverage((ThaAttModulePrbs)prbsModule))
			AtSprintf(buf, "%s", "N/A");
		else
			{
			delay = AtPrbsEngineAverageDelayGet(engine, r2c);
			AtSprintf(buf, "%.6f", delay);
			}
		StrToCell(tabPtr, engine_i, column++, buf);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return success;
    }

eBool CliAtPrbsEngineDisruptionShow(AtList engines, char *readingModeStr, char *verboseMode)
    {
    uint32 engine_i;
    eBool r2c = cAtFalse;
    eBool silent = cAtFalse;
    tTab *tabPtr = NULL;
    eBool success = cAtTrue;
    eAtHistoryReadingMode readingMode = cAtHistoryReadingModeUnknown;
    char buf[80];
    const char *pHeading[] = {"Channel", "Engine ID", "MaxExpectedDisruptionTime(ms)", "CurDisruptionTime(ms)"};

    if (AtListLengthGet(engines) == 0)
        {
        AtPrintc(cSevWarning, "No engine to display\r\n");
        return cAtTrue;
        }

    readingMode = CliHistoryReadingModeGet(readingModeStr);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    r2c = (readingMode == cAtHistoryReadingModeReadToClear) ? cAtTrue : cAtFalse;
    silent = CliHistoryReadingShouldSilent(readingMode, verboseMode);

    if (!silent)
        {
        tabPtr = TableAlloc(AtListLengthGet(engines), mCount(pHeading), pHeading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "Cannot create table\r\n");
            return cAtFalse;
            }
        }

    tabPtr = TableAlloc(AtListLengthGet(engines), mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (engine_i = 0; engine_i < AtListLengthGet(engines); engine_i = AtCliNextIdWithSleep(engine_i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtPrbsEngine engine = (AtPrbsEngine)AtListObjectGet(engines, engine_i);
        uint8 column = 0;
        uint32 time = 0;

        StrToCell(tabPtr, engine_i, column++, AtPrbsEngineChannelDescription(engine));
        StrToCell(tabPtr, engine_i, column++, CliNumber2String(AtPrbsEngineIdGet(engine) + 1, "%d"));

        time = AtPrbsEngineDisruptionMaxExpectedTimeGet(engine);
        AtSprintf(buf, "%d", time);
        StrToCell(tabPtr, engine_i, column++, buf);

        time = AtPrbsEngineDisruptionCurTimeGet(engine, r2c);
        AtSprintf(buf, "%d", time);
        StrToCell(tabPtr, engine_i, column++, buf);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return success;
    }

eBool CliAtPrbsEngineBandwidthShowWithSilent(AtList engines, eBool printEngineIdFirst, eBool silentMode)
    {
    uint32 engine_i;
    const char *aString;
    const char *pHeading[] = {"channel",
                              "engineId",
                              "bandwidth control", "min lenngth", "max length", "len mode",
                              "tx percentage bandwith", "burst size"};
    tTab *tabPtr = NULL;
    eBool convertSuccess;
    char cellString[128];

    if (printEngineIdFirst)
        {
        pHeading[0] = "engineId";
        pHeading[1] = "channel";
        }

    if (AtListLengthGet(engines) == 0)
        {
        AtPrintc(cSevWarning, "No engine to display\r\n");
        return cAtTrue;
        }

    if (!silentMode)
        {
        tabPtr = TableAlloc(AtListLengthGet(engines), mCount(pHeading), pHeading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "Cannot create table\r\n");
            return cAtFalse;
            }
        }

    for (engine_i = 0; engine_i < AtListLengthGet(engines); engine_i = AtCliNextIdWithSleep(engine_i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtPrbsEngine engine = (AtPrbsEngine)AtListObjectGet(engines, engine_i);
        eBool isBandwidthSupported = AtPrbsEngineBandwidthControlIsSupported(engine);
        uint32 minLength = AtPrbsEngineMinLengthGet(engine);
        uint32 maxLength = AtPrbsEngineMaxLengthGet(engine);
        uint32 lengthMode   = AtPrbsEnginePayloadLengthModeGet(engine);
        uint32 percentageBandwidth = AtPrbsEnginePercentageBandwidthGet(engine);
        uint32 burstSize = AtPrbsEngineBurstSizeGet(engine);
        uint8 column = 0;

        if (printEngineIdFirst)
            {
            StrToCell(tabPtr, engine_i, column++, CliNumber2String(AtPrbsEngineIdGet(engine) + 1, "%d"));
            StrToCell(tabPtr, engine_i, column++, AtPrbsEngineChannelDescription(engine));
            }
        else
            {
            StrToCell(tabPtr, engine_i, column++, AtPrbsEngineChannelDescription(engine));
            StrToCell(tabPtr, engine_i, column++, CliNumber2String(AtPrbsEngineIdGet(engine) + 1, "%d"));
            }

        /*bandwidth control is supported */
        StrToCell(tabPtr, engine_i, column++, isBandwidthSupported ? "yes" : "no");

        /*min length */
        AtSprintf(cellString, "%d", minLength);
        StrToCell(tabPtr, engine_i, column++, isBandwidthSupported ? cellString : "NA");

        /*max length */
        AtSprintf(cellString, "%d", maxLength);
        StrToCell(tabPtr, engine_i, column++, isBandwidthSupported ? cellString : "NA");

        /*length mode */
        aString = CliEnumToString(lengthMode, cAtPrbsLengthModeStr, cAtPrbsLengthModeVal, mCount(cAtPrbsLengthModeVal), &convertSuccess);
        ColorStrToCell(tabPtr, engine_i, column++, convertSuccess ? aString : "error", convertSuccess ? cSevNormal : cSevCritical);

        /* percentage bandwidth */
        AtSprintf(cellString, "%d", percentageBandwidth);
        StrToCell(tabPtr, engine_i, column++, cellString);

        /* burst size */
        AtSprintf(cellString, "%d", burstSize);
        StrToCell(tabPtr, engine_i, column++, cellString);
        }

    /* Print table */
    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CliAtPrbsEnginePayloadLengthModeSet(AtList engines, const char *modeString)
    {
    return PayloadLengthModeSet(engines, modeString, (AttributeSetFunc)AtPrbsEnginePayloadLengthModeSet);
    }

eBool CliAtPrbsEngineBurstSizeSet(AtList engines, uint32 burstSize)
    {
    return AttributeSet(engines, burstSize, (AttributeSetFunc)AtPrbsEngineBurstSizeSet);
    }

eBool CliAtPrbsEnginePercentageBandwidthSet(AtList engines, uint32 percentageBandwidth)
    {
    return AttributeSet(engines, percentageBandwidth, (AttributeSetFunc)AtPrbsEnginePercentageBandwidthSet);
    }

eBool CliAtPrbsEngineMinLengthSet(AtList engines, uint32 length)
    {
    return AttributeSet(engines, length, (AttributeSetFunc)AtPrbsEngineMinLengthSet);
    }

eBool CliAtPrbsEngineMaxLengthSet(AtList engines, uint32 length)
    {
    return AttributeSet(engines, length, (AttributeSetFunc)AtPrbsEngineMaxLengthSet);
    }

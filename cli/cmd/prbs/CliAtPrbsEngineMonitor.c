/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : CliAtPrbsEngineMonitor.c
 *
 * Created Date: Feb 29, 2016
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "CliAtPrbsEngine.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tCliBertMonitorEngine *CliBertMonitorEngine;
typedef struct tCliBertMonitorEngine
    {
    tAtObject super;

    AtPrbsEngine engine;
    long long rxBits;
    long long rxLosSyncBits;
    long long rxSyncBits;
    long long rxErrorBits;
    long long txBits;
    long long syncCount;
    uint32 prevousLossyncStiky;

    }tCliBertMonitorEngine;


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static volatile eBool isBertStarted = cAtFalse;
static AtList bertMonitorEngines = NULL;
static AtOsalMutex mutex = NULL;
static tAtOsalCurTime startTime;
static tAtOsalCurTime stopTime;

/*--------------------------- Forward declarations ---------------------------*/
CliBertMonitorEngine CliBertMonitorEngineNew(AtPrbsEngine engine);
void BertMonHandler(void);

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return (uint32)sizeof(tCliBertMonitorEngine);
    }

static CliBertMonitorEngine CliBertMonitorEngineInit(CliBertMonitorEngine monEngine, AtPrbsEngine engine)
    {
    AtObjectInit((AtObject)monEngine);
    monEngine->prevousLossyncStiky = cAtPrbsEngineAlarmTypeLossSync;
    monEngine->engine = engine;
    return monEngine;
    }

CliBertMonitorEngine CliBertMonitorEngineNew(AtPrbsEngine engine)
    {
    CliBertMonitorEngine monEngine = (CliBertMonitorEngine)AtOsalMemAlloc(ObjectSize());
    AtOsalMemInit(monEngine, 0 , ObjectSize());

    return CliBertMonitorEngineInit(monEngine, engine);
    }

static void BertCounterClear(AtPrbsEngine engine)
    {
    AtPrbsEngineAlarmHistoryClear(engine);
    AtPrbsEngineAllCountersLatch(engine);

    AtPrbsEngineCounterClear(engine, cAtPrbsEngineCounterTxBit);
    AtPrbsEngineCounterClear(engine, cAtPrbsEngineCounterRxBit);
    AtPrbsEngineCounterClear(engine, cAtPrbsEngineCounterRxBitError);
    AtPrbsEngineCounterClear(engine, cAtPrbsEngineCounterRxSync);
    AtPrbsEngineCounterClear(engine, cAtPrbsEngineCounterRxBitLoss);
    }

static AtList PrbsMonEnginesGet(char *idString)
    {
    AtModulePrbs modulePrbs = (AtModulePrbs)AtDeviceModuleGet(CliDevice(), cAtModulePrbs);
    uint32 numberOfEngines, i;
    AtList engines = AtListCreate(0);
    uint32 *engineIds;
    uint32 maxEngine = AtModulePrbsMaxNumEngines(modulePrbs);

    /* Get list of line */
    engineIds = CliSharedIdBufferGet(&numberOfEngines);
    numberOfEngines = CliIdListFromString(idString, engineIds, numberOfEngines);
    numberOfEngines = numberOfEngines < maxEngine ? numberOfEngines : maxEngine;

    for (i = 0; i < numberOfEngines; i++)
        {
        AtPrbsEngine engine = AtModulePrbsEngineGet(modulePrbs, engineIds[i] - 1);
        if (engine)
            {
            CliBertMonitorEngine monEngine;
            BertCounterClear(engine);
            monEngine = CliBertMonitorEngineNew(engine);
            AtListObjectAdd(engines, (AtObject)monEngine);
            }
        }

    return engines;
    }

static void SimpleCliCounterPrintToCell(tTab *tabPtr,
                           uint32 row,
                           uint32 column,
                           long long counterValue)
    {
    static char buf[128];

    /* Put to cell with correct color */
    AtSprintf(buf, "%lld", counterValue);

    ColorStrToCell(tabPtr, row, column, buf, cSevNormal);
    }

static eBool CliAtPrbsEngineCounterMonitorShow(AtList monengines)
    {
    uint32 engine_i;
    tTab *tabPtr;
    const char *heading[] =
        {
        "Channel",
        "engineId",
        "TxBits",
        "RxBits",
        "RxBitErrorBits",
        "RxSyncBits",
        "RxLossBits",
        "RxSyncCounts",
        "Elapse-time(s)",
        "RxBitsRate(kbits/s)"
        };

    if (AtListLengthGet(monengines) == 0)
        {
        AtPrintc(cSevWarning, "No engine to display\r\n");
        return cAtTrue;
        }

    tabPtr = TableAlloc(AtListLengthGet(monengines), mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot create table\r\n");
        return cAtFalse;
        }

    for (engine_i = 0; engine_i < AtListLengthGet(monengines); engine_i++)
        {
        CliBertMonitorEngine monEngine = (CliBertMonitorEngine)(void*)AtListObjectGet(monengines, engine_i);
        AtPrbsEngine engine = monEngine->engine;
        double elapsetime = 0;
        long long counterValue = 0;
        double rate = 0;
        uint8 column = 0;

        StrToCell(tabPtr, engine_i, column++, (char *)CliChannelIdStringGet(AtPrbsEngineChannelGet(engine)));
        StrToCell(tabPtr, engine_i, column++, CliNumber2String(AtPrbsEngineIdGet(engine) + 1, "%u"));

        counterValue = monEngine->txBits;
        SimpleCliCounterPrintToCell(tabPtr, engine_i, column++, counterValue);

        counterValue = monEngine->rxBits;
        SimpleCliCounterPrintToCell(tabPtr, engine_i, column++, counterValue);

        counterValue = monEngine->rxErrorBits;
        SimpleCliCounterPrintToCell(tabPtr, engine_i, column++, counterValue);

        counterValue = monEngine->rxSyncBits;
        SimpleCliCounterPrintToCell(tabPtr, engine_i, column++, counterValue);

        counterValue = monEngine->rxLosSyncBits;
        SimpleCliCounterPrintToCell(tabPtr, engine_i, column++, counterValue);

        counterValue = monEngine->syncCount;
        SimpleCliCounterPrintToCell(tabPtr, engine_i, column++, counterValue);

        counterValue = AtOsalDifferenceTimeInMs(&stopTime, &startTime)/1000;
        elapsetime = AtOsalDifferenceTimeInMs(&stopTime, &startTime)/1000;
        SimpleCliCounterPrintToCell(tabPtr, engine_i, column++, counterValue);

        rate = (double)(((double)monEngine->rxBits / elapsetime)/1000);
        counterValue = (long long)rate;
        SimpleCliCounterPrintToCell(tabPtr, engine_i, column++, counterValue);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

static void BertCounterCollect(AtList engines)
    {
    uint32 engine_i;

    for (engine_i = 0; engine_i < AtListLengthGet(engines); engine_i++)
        {
        CliBertMonitorEngine monEngine = (CliBertMonitorEngine)(void*)AtListObjectGet(engines, engine_i);
        AtPrbsEngine engine = monEngine->engine;
        uint32 sticky = AtPrbsEngineAlarmHistoryClear(engine);

        AtPrbsEngineAllCountersLatch(engine);

        monEngine->txBits += AtPrbsEngineCounterClear(engine, cAtPrbsEngineCounterTxBit);
        monEngine->rxBits += AtPrbsEngineCounterClear(engine, cAtPrbsEngineCounterRxBit);
        monEngine->rxErrorBits += AtPrbsEngineCounterClear(engine, cAtPrbsEngineCounterRxBitError);
        monEngine->rxSyncBits += AtPrbsEngineCounterClear(engine, cAtPrbsEngineCounterRxSync);
        monEngine->rxLosSyncBits += AtPrbsEngineCounterClear(engine, cAtPrbsEngineCounterRxBitLoss);
        monEngine->syncCount += (monEngine->prevousLossyncStiky == 0 && sticky != 0) ? 1 : 0;
        monEngine->prevousLossyncStiky = sticky;
        }
    }

static eBool CliBertStart(char *idString)
    {
    if (!isBertStarted)
        {
        if (mutex == NULL)
            mutex = AtOsalMutexCreate();
        if (bertMonitorEngines == NULL)
            bertMonitorEngines = PrbsMonEnginesGet(idString);

        AtOsalCurTimeGet(&startTime);
        AtOsalCurTimeGet(&stopTime);
        isBertStarted = cAtTrue;
        return cAtTrue;
        }
    else
        return cAtFalse;
    }

static void CliBertStop(void)
    {
    uint32 engine_i;
    if (mutex != NULL)
        {
        AtOsalMutexLock(mutex);
        isBertStarted = cAtFalse;

        AtOsalCurTimeGet(&stopTime);
        CliAtPrbsEngineCounterMonitorShow(bertMonitorEngines);

        for (engine_i = 0; engine_i < AtListLengthGet(bertMonitorEngines); engine_i++)
            {
            AtObject monEngine = AtListObjectGet(bertMonitorEngines, engine_i);
            AtObjectDelete(monEngine);
            }

        AtObjectDelete((AtObject)bertMonitorEngines);
        bertMonitorEngines = NULL;

        AtOsalMutexUnLock(mutex);
        AtOsalMutexDestroy(mutex);
        mutex = NULL;
        }
    }

void BertMonHandler(void)
    {
    if (isBertStarted)
        {
        AtOsalMutexLock(mutex);
        BertCounterCollect(bertMonitorEngines);
        AtOsalMutexUnLock(mutex);
        }
    }

eBool CmdAtPrbsEngineMonitorStart(char argc, char **argv)
    {
    AtUnused(argc);

    return CliBertStart(argv[0]);
    }

eBool CmdAtPrbsEngineMonitorStop(char argc, char **argv)
    {

    AtUnused(argc);
    AtUnused(argv);
    CliBertStop();

    return cAtTrue;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : CliAtSdhVcPrbs.c
 *
 * Created Date: Aug 24, 2013
 *
 * Description : PRBS CLI
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtPrbsEngine.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtList PrbsEnginesGet(char *idString)
    {
    uint32 numberPaths, i;
    uint32 bufferSize;
    AtList engines = AtListCreate(0);
    AtChannel *channelList = CliSharedChannelListGet(&bufferSize);
    numberPaths = CliChannelListFromString(idString, channelList, bufferSize);

    for (i = 0; i < numberPaths; i++)
        {
        AtPrbsEngine engine = AtChannelPrbsEngineGet(channelList[i]);
        if (engine)
            AtListObjectAdd(engines, (AtObject)engine);
        }

    return engines;
    }

static eBool Invert(char argc, char **argv, eBool invert)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineInvert(engines, invert);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

static eBool ErrorForce(char argc, char **argv, eBool force)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineErrorForce(engines, force);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

static eBool Enable(char argc, char **argv, eBool enable)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineEnable(engines, enable);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineVcModeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineModeSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineVcTransmitModeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineTxModeSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineVcExpectModeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineRxModeSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineVcDataModeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineDataModeSet(engines, argv[1], cAtTrue, cAtTrue);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineVcTransmitDataModeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineDataModeSet(engines, argv[1], cAtTrue, cAtTrue);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineVcExpectDataModeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineDataModeSet(engines, argv[1], cAtFalse, cAtTrue);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineVcInvertEnable(char argc, char **argv)
    {
    return Invert(argc, argv, cAtTrue);
    }

eBool CmdAtPrbsEngineVcInvertDisable(char argc, char **argv)
    {
    return Invert(argc, argv, cAtFalse);
    }

eBool CmdAtPrbsEngineVcErrorForce(char argc, char **argv)
    {
    return ErrorForce(argc, argv, cAtTrue);
    }

eBool CmdAtPrbsEngineVcErrorUnForce(char argc, char **argv)
    {
    return ErrorForce(argc, argv, cAtFalse);
    }

eBool CmdAtPrbsEngineVcEnable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue);
    }

eBool CmdAtPrbsEngineVcDisable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse);
    }

eBool CmdAtPrbsEngineVcShow(char argc, char **argv)
    {
    return CliAtPrbsEngineShowWithHandler(argc, argv, PrbsEnginesGet);
    }

eBool CmdAtPrbsEngineVcSideSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineSideSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineVcGeneratingSideSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineGeneratingSideSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineVcMonitoringSideSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineMonitoringSideSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineVcCounterGet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineCounterGet(engines, argv[1], (argc > 2) ? argv[2] : NULL);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineVcErrorInject(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineErrorInject(engines, (argc > 1) ? (uint32)AtStrToDw(argv[1]) : 1);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineVcFixPatternSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    uint32 transmitPattern = AtStrToDw(argv[1]);
    uint32 expectedPattern = (argc==2)? transmitPattern: AtStrToDw(argv[2]);
    eBool success = cAtFalse;

    AtUnused(argc);
    success  = CliAtPrbsEngineTxFixedPatternSet(engines, transmitPattern);
    success &= CliAtPrbsEngineRxFixedPatternSet(engines, expectedPattern);

    AtObjectDelete((AtObject) engines);
    return success;
    }

eBool CmdAtPrbsEngineVcTransmitFixPatternSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    uint32 transmitPattern = AtStrToDw(argv[1]);
    eBool success = CliAtPrbsEngineTxFixedPatternSet(engines, transmitPattern);

    AtUnused(argc);
    AtObjectDelete((AtObject) engines);

    return success;
    }

eBool CmdAtPrbsEngineVcExpectFixPatternSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    uint32 expectedPattern = AtStrToDw(argv[1]);
    eBool success = CliAtPrbsEngineRxFixedPatternSet(engines, expectedPattern);

    AtUnused(argc);
    AtObjectDelete((AtObject) engines);

    return success;
    }

eBool CmdAtPrbsEngineVcBitErrorRateShow(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineBitErrorRateShow(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineVcTxErrorRateForce(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineTxErrorRateForce(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineVcBitOrderSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineBitOrderSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineVcStart(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineStart(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineVcStop(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineStop(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineVcDurationSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineDurationSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineVcDelayEnable(char argc, char **argv)
	{
	AtList engines = PrbsEnginesGet(argv[0]);
	eBool success = CliAtPrbsEngineDelayEnable(engines, cAtTrue);

	AtUnused(argc);
	AtObjectDelete((AtObject)engines);
	return success;
	}

eBool CmdAtPrbsEngineVcDelayDisable(char argc, char **argv)
	{
	AtList engines = PrbsEnginesGet(argv[0]);
	eBool success = CliAtPrbsEngineDelayEnable(engines, cAtFalse);

	AtUnused(argc);
	AtObjectDelete((AtObject)engines);
	return success;
	}

eBool CmdAtAttSdhPathDelayGet(char argc, char **argv)
	{
	AtList engines = PrbsEnginesGet(argv[0]);
	eBool success = CliAtPrbsEngineDelayShow(engines, argv[1], (argc > 2) ? argv[2] : NULL);
	AtUnused(argc);
	AtObjectDelete((AtObject)engines);
	return success;
	}

eBool CmdAtPrbsEngineVcDisruptionMaxExpectationTimeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineDisruptionMaxExpectationTimeSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtAttSdhPathDisruptionGet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineDisruptionShow(engines, argv[1], (argc > 2) ? argv[2] : NULL);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

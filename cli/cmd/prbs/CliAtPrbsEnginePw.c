/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : CliAtPrbsEnginePw.c
 *
 * Created Date: Mar 12, 2015
 *
 * Description : PRBS CLIs implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtPrbsEngine.h"
#include "AtDevice.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtList PrbsEnginesGet(char *idString)
    {
    uint32 bufferSize;
    uint32      *idBuf;
    uint32 numChannels, i;
    AtList engines = AtListCreate(0);
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numChannels = CliIdListFromString(idString, idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    for (i = 0; i < numChannels; i++)
        {
        AtPrbsEngine engine;
        AtChannel pw = (AtChannel)AtModulePwGetPw(pwModule, idBuf[i] - 1);
        if (pw == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: Ignore not exist PW %d\r\n", idBuf[i]);
            continue;
            }

        engine = AtChannelPrbsEngineGet(pw);
        if (engine)
            AtListObjectAdd(engines, (AtObject)engine);
        }

    return engines;
    }

static eBool Enable(char argc, char **argv, eBool enable)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineEnable(engines, enable);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEnginePwModeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineModeSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEnginePwEnable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue);
    }

eBool CmdAtPrbsEnginePwDisable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse);
    }

eBool CmdAtPrbsEnginePwShow(char argc, char **argv)
    {
    return CliAtPrbsEngineShowWithHandler(argc, argv, PrbsEnginesGet);
    }

eBool CmdAtPrbsEnginePwSideSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineSideSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

static eBool ErrorForce(char argc, char **argv, eBool force)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineErrorForce(engines, force);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEnginePwErrorForce(char argc, char **argv)
    {
    return ErrorForce(argc, argv, cAtTrue);
    }

eBool CmdAtPrbsEnginePwErrorUnForce(char argc, char **argv)
    {
    return ErrorForce(argc, argv, cAtFalse);
    }

eBool CmdAtPrbsEnginePwCounterGet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineCounterGet(engines, argv[1], (argc > 2) ? argv[2] : NULL);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEnginePwErrorInject(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineErrorInject(engines, (argc > 1) ? (uint32)AtStrToDw(argv[1]) : 1);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEnginePwStart(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineStart(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEnginePwStop(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineStop(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEnginePwDurationSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineDurationSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : CliAtPrbsEngine.h
 * 
 * Created Date: Aug 24, 2013
 *
 * Description : PRBS engine CLIs
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLIATPRBSENGINE_H_
#define _CLIATPRBSENGINE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtList.h"
#include "AtCli.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef AtList (*EngineFuncGet)(char *idString);

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool CliAtPrbsEngineModeSet(AtList engines, const char *modeString);
eBool CliAtPrbsEngineDataModeSet(AtList engines, const char *modeString, eBool isTx, eBool isRx);
eBool CliAtPrbsEngineTxModeSet(AtList engines, const char *modeString);
eBool CliAtPrbsEngineRxModeSet(AtList engines, const char *modeString);
eBool CliAtPrbsEngineInvert(AtList engines, eBool invert);
eBool CliAtPrbsEngineInvertWithHandler(AtList engines, eBool invert, eAtModulePrbsRet (*Invert)(AtPrbsEngine self, eBool invert));
eBool CliAtPrbsEngineErrorForce(AtList engines, eBool force);
eBool CliAtPrbsEngineEnable(AtList engines, eBool enable);
eBool CliAtPrbsEngineDelayEnable(AtList engines, eBool enable);
eBool CliAtPrbsEngineShow(AtList engines, eBool silentMode);
eBool CliAtPrbsEngineShowWithSilent(AtList engines, eBool printEngineIdFirst, eBool silentMode);
eBool CliAtPrbsEngineCounterGet(AtList engines, char *readingModeStr, char *verboseMode);
eBool CliAtPrbsEngineCounter64BitGet(AtList engines, char *readingModeStr, char *verboseMode);
eBool CliAtPrbsEngineDebug(AtList engines);
eBool CliAtPrbsEngineTxModeSet(AtList engines, const char *modeString);
eBool CliAtPrbsEngineRxModeSet(AtList engines, const char *modeString);
eBool CliAtPrbsEngineTxEnable(AtList engines, eBool enable);
eBool CliAtPrbsEngineRxEnable(AtList engines, eBool enable);
eBool CliAtPrbsEngineSideSet(AtList engines, const char *modeString);
eBool CliAtPrbsEngineErrorInject(AtList engines, uint32 numErrors);
eBool CliAtPrbsEngineBitOrderSet(AtList engines, const char *bitOrderString);
eBool CliAtPrbsEngineShowWithHandler(char argc, char **argv, EngineFuncGet engineGet);
eBool CliAtPrbsEngineAlarmHistoryShow(AtList engines, char *readingModeStr, char *verboseMode);
eBool CliAtPrbsEngineDelayShow(AtList engines, char *readingModeStr, char *verboseMode);
eBool CliAtPrbsEngineDisruptionShow(AtList engines, char *readingModeStr, char *verboseMode);

/* ADDED */
eBool CliAtPrbsEngineTxErrorRateForce(AtList engines, char* pErrorRateStr);
eBool CliAtPrbsEngineBitErrorRateShow(AtList engines);
eBool CliAtPrbsEngineTxFixedPatternSet(AtList engines, uint32 fixPattern);
eBool CliAtPrbsEngineRxFixedPatternSet(AtList engines, uint32 fixPattern);
eBool CliAtPrbsEngineGeneratingSideSet(AtList engines, const char *sideString);
eBool CliAtPrbsEngineMonitoringSideSet(AtList engines, const char *sideString);

/* Bandwidth control */
eBool CliAtPrbsEngineBandwidthShowWithSilent(AtList engines, eBool printEngineIdFirst, eBool silentMode);
eBool CliAtPrbsEnginePayloadLengthModeSet(AtList engines, const char *modeString);
eBool CliAtPrbsEnginePercentageBandwidthSet(AtList engines, uint32 percentageBandwidth);
eBool CliAtPrbsEngineMinLengthSet(AtList engines, uint32 length);
eBool CliAtPrbsEngineMaxLengthSet(AtList engines, uint32 length);
eBool CliAtPrbsEngineBurstSizeSet(AtList engines, uint32 burstSize);

/* Gating */
eBool CliAtPrbsEngineStart(AtList engines);
eBool CliAtPrbsEngineStop(AtList engines);
eBool CliAtPrbsEngineDurationSet(AtList engines, const char *durInMs);
eBool CliAtPrbsEngineDisruptionMaxExpectationTimeSet(AtList engines, const char *durInMs);
eBool CliAtPrbsEngineGatingShow(AtList engines);

/* Helper */
AtList CliPrbsEnginesGet(char *idString);

#endif /* _CLIATPRBSENGINE_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : CliAtPrbsEngineSerdesSdhLine.c
 *
 * Created Date: Aug 24, 2013
 *
 * Description : PRBS CLI
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtPrbsEngine.h"
#include "../sdh/CliAtModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtList PrbsEnginesGet(char *idString)
    {
    uint32 i;
    AtList engines = AtListCreate(0);
    AtList serdesControllers = CliAtSdhLineSerdesControllers(idString);

    for (i = 0; i < AtListLengthGet(serdesControllers); i++)
        {
        AtSerdesController serdes = (AtSerdesController)AtListObjectGet(serdesControllers, i);
        AtPrbsEngine engine = AtSerdesControllerPrbsEngine(serdes);
        if (engine)
            AtListObjectAdd(engines, (AtObject)engine);
        }

    AtObjectDelete((AtObject)serdesControllers);

    return engines;
    }

static eBool Invert(char argc, char **argv, eBool invert)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineInvert(engines, invert);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

static eBool ErrorForce(char argc, char **argv, eBool force)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineErrorForce(engines, force);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

static eBool Enable(char argc, char **argv, eBool enable)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineEnable(engines, enable);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineSerdesSdhLineModeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineModeSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineSerdesSdhLineInvertEnable(char argc, char **argv)
    {
    return Invert(argc, argv, cAtTrue);
    }

eBool CmdAtPrbsEngineSerdesSdhLineInvertDisable(char argc, char **argv)
    {
    return Invert(argc, argv, cAtFalse);
    }

eBool CmdAtPrbsEngineSerdesSdhLineErrorForce(char argc, char **argv)
    {
    return ErrorForce(argc, argv, cAtTrue);
    }

eBool CmdAtPrbsEngineSerdesSdhLineErrorUnForce(char argc, char **argv)
    {
    return ErrorForce(argc, argv, cAtFalse);
    }

eBool CmdAtPrbsEngineSerdesSdhLineEnable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue);
    }

eBool CmdAtPrbsEngineSerdesSdhLineDisable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse);
    }

eBool CmdAtPrbsEngineSerdesSdhLineShow(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool silentMode = cAtFalse;
    eBool success = CliAtPrbsEngineShow(engines, silentMode);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineSerdesSdhLineStart(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineStart(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineSerdesSdhLineStop(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineStop(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineSerdesSdhLineDurationSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineDurationSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

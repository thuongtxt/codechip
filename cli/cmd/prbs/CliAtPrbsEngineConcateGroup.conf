# @group CliAtPrbsEngineConcateGroup "ConcateGroup"
# @ingroup CliAtModulePrbs "PRBS module"

4 prbs engine create vcg CmdAtModulePrbsVcgPrbsEngineCreate 1 vcgId=1
/*
    Syntax     : prbs engine create vcg <vcgId>
    Parameter  : vcgId  - VCG Id
    Description: Create PRBS engine on this VCG
    Example    : prbs engine create vcg 1
*/

4 prbs engine delete vcg CmdAtModulePrbsVcgPrbsEngineDelete 1 vcgId=1
/*
    Syntax     : prbs engine delete vcg <vcgId>
    Parameter  : vcgId  - VCG Id
    Description: Delete PRBS engine on this VCG
    Example    : prbs engine delete vcg 1
*/

3 vcg prbs mode CmdAtPrbsEngineVcgModeSet 2 vcgList=1 mode
/*
    Syntax     : vcg prbs mode <vcgList> <prbs7/prbs9/prbs15/prbs23/prbs31/sequence/fixedpattern/prbs20/prbs20qrss/all0s/all1s/alt01/1in8>
    Parameter  : vcgList - List of VCGs
                 mode    - PRBS mode: prbs7/prbs9/prbs15/prbs23/prbs31/sequence/fixedpattern/prbs20/prbs20qrss/all0s/all1s/alt01/1in8
    Description: Set PRBS mode
    Example    : vcg prbs mode 1-16 prbs15
*/

3 vcg prbs enable CmdAtPrbsEngineVcgEnable 1 vcgList=1
/*
    Syntax     : vcg prbs enable <vcgList>
    Parameter  : vcgList - List of VCGs
    Description: Enable PRBS engine
    Example    : vcg prbs enable 1-16
*/

3 vcg prbs disable CmdAtPrbsEngineVcgDisable 1 vcgList=1
/*
    Syntax     : vcg prbs disable <vcgList>
    Parameter  : vcgList - List of VCGs
    Description: Disable PRBS engine
    Example    : vcg prbs disable 1-16
*/

3 show vcg prbs CmdAtPrbsEngineVcgShow 1 vcgList=1
/*
    Syntax     : show vcg prbs <vcgList> [silent]
    Parameter  : vcgList - List of VCGs
                 silent  - If "silent" is specified, there would be no
                           output with "r2c" mode
    Description: Show PRBS engines
    Example    : show vcg prbs 1-16
*/

4 show vcg prbs counters CmdAtPrbsEngineVcgCounterGet -1 vcgList=1 readingMode=r2c
/*
    Syntax     : show vcg prbs counters <vcgList> <ro/r2c> [silent]
    Parameter  : vcgList - List of VCGs
                 counterReadMode - read only or read to clear counters.
                 silent          - If "silent" is specified, there would be no 
                                   output with "r2c" mode
    Description: Show PRBS engines counters
    Example    : show vcg prbs counters 1 r2c
                 show vcg prbs counters 1 r2c silent
*/

3 vcg prbs invert CmdAtPrbsEngineVcgInvertEnable 1 vcgList=1
/*
    Syntax     : vcg prbs invert <vcgList>
    Parameter  : vcgList - List of VCGs
    Description: PRBS invert
    Example    : vcg prbs invert 1-8
*/

3 vcg prbs noinvert CmdAtPrbsEngineVcgInvertDisable 1 vcgList=1
/*
    Syntax     : vcg prbs noinvert <vcgList>
    Parameter  : vcgList - List of VCGs
    Description: PRBS no invert
    Example    : vcg prbs noinvert 1-8
*/

4 vcg prbs error force CmdAtPrbsEngineVcgErrorForce 1 vcgList=1
/*
    Syntax     : vcg prbs error force <vcgList>
    Parameter  : vcgList - List of VCGs
    Description: Force PRBS error
    Example    : vcg prbs error force 1-8
*/

4 vcg prbs error unforce CmdAtPrbsEngineVcgErrorUnForce 1 vcgList=1
/*
    Syntax     : vcg prbs error unforce <vcgList>
    Parameter  : vcgList - List of VCGs
    Description: Unforce PRBS error
    Example    : vcg prbs error unforce 1-8
*/

3 vcg prbs ber CmdAtPrbsEngineVcgTxErrorRateForce 2 vcgList=1 ber=10e-3
/*
    Syntax     : vcg prbs ber <vcgList> <ber>
    Parameter  : vcgList - List of VCGs
                 ber -[off, 10e-2, 10e-3, 10e-4, 10e-5, 10e-6, 10e-7, 10e-8, 10e-9, 10e-10]
    Description: PRBS force Bit-Error-Rate
    Example    : vcg prbs ber 1-8 10e-3
*/

4 show vcg prbs ber CmdAtPrbsEngineVcgBitErrorRateShow 1 vcgList=1
/*
    Syntax     : show vcg prbs ber <vcgList>
    Parameter  : vcgList - List of VCGs
    Description: Show BER of PRBS
    Example    : show vcg prbs ber 1-8
*/

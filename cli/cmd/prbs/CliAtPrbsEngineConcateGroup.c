/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : CliAtPrbsEngineConcate.c
 *
 * Created Date: Aug 10, 2016
 *
 * Description : PRBS for Concate Group
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "AtPdhDe1.h"
#include "AtModuleConcate.h"
#include "CliAtPrbsEngine.h"
#include "AtCliModule.h"
#include "../../../driver/src/generic/prbs/AtModulePrbsInternal.h"
#include "../textui/CliAtTextUI.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModulePrbs PrbsModule(void)
    {
    return (AtModulePrbs)AtDeviceModuleGet(CliDevice(), cAtModulePrbs);
    }

static AtList PrbsEnginesGet(char *idString)
    {
    uint32 bufferSize, vcg_i;
    uint32 *vcgIdList = CliSharedIdBufferGet(&bufferSize);
    AtList engines = AtListCreate(0);
    uint32 numberOfVcg = CliIdListFromString(idString, vcgIdList, bufferSize);

    for (vcg_i = 0; vcg_i < numberOfVcg; vcg_i++)
        {
        AtPrbsEngine engine;
        AtConcateGroup vcg = AtModuleConcateGroupGet(CliAtConcateModule(), vcgIdList[vcg_i] - 1);
        if (vcg == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: VCG %d has not been created, it is ignored\r\n", vcgIdList[vcg_i]);
            continue;
            }

        engine = AtChannelPrbsEngineGet((AtChannel)vcg);
        if (engine == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: VCG %d was not created prbs engine\r\n", vcgIdList[vcg_i]);
            continue;
            }

        AtListObjectAdd(engines, (AtObject)engine);
        }

    return engines;
    }

static eBool Enable(char argc, char **argv, eBool enable, AtList (*EngineGet)(char *idString))
    {
    AtList engines = EngineGet(argv[0]);
    eBool success = CliAtPrbsEngineEnable(engines, enable);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

static eBool EngineShow(char argc, char **argv, AtList (*EngineGet)(char *idString))
    {
    AtList engines = EngineGet(argv[0]);
    eBool silent = cAtFalse, success;

    if (argc > 1)
        silent = CliIsSilentIndication(argv[1]);

    success = CliAtPrbsEngineShow(engines, silent);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

static eBool ErrorForce(char argc, char **argv, eBool force, AtList (*EngineGet)(char *idString))
    {
    AtList engines = EngineGet(argv[0]);
    eBool success = CliAtPrbsEngineErrorForce(engines, force);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

static eBool Invert(char argc, char **argv, eBool invert)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineInvert(engines, invert);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtModulePrbsVcgPrbsEngineCreate(char argc, char **argv)
    {
    uint32 bufferSize, vcg_i;
    uint32 *vcgIdList = CliSharedIdBufferGet(&bufferSize);
    uint32 numberOfVcg = CliIdListFromString(argv[0], vcgIdList, bufferSize);
    eBool success = cAtTrue;

    AtUnused(argc);

    for (vcg_i = 0; vcg_i < numberOfVcg; vcg_i++)
        {
        AtPrbsEngine newPrbsEngine;
        AtConcateGroup vcg = AtModuleConcateGroupGet(CliAtConcateModule(), vcgIdList[vcg_i] - 1);
        if (vcg == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: VCG %d has not been created, it is ignored\r\n", vcgIdList[vcg_i]);
            continue;
            }

        newPrbsEngine = AtModulePrbsConcateGroupPrbsEngineCreate(PrbsModule(), vcg);
        if (!newPrbsEngine)
            {
            AtPrintc(cSevCritical, "ERROR: Can not prbs engine on VCG#%d\r\n", vcgIdList[vcg_i]);
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtModulePrbsVcgPrbsEngineDelete(char argc, char **argv)
    {
    uint32 bufferSize, vcg_i;
    uint32 *vcgIdList = CliSharedIdBufferGet(&bufferSize);
    uint32 numberOfVcg = CliIdListFromString(argv[0], vcgIdList, bufferSize);
    eBool success = cAtTrue;

    AtUnused(argc);

    for (vcg_i = 0; vcg_i < numberOfVcg; vcg_i++)
        {
        eAtRet ret;
        AtPrbsEngine prbsEngine;
        AtConcateGroup vcg = AtModuleConcateGroupGet(CliAtConcateModule(), vcgIdList[vcg_i] - 1);
        if (vcg == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: VCG %d has not been created, it is ignored\r\n", vcgIdList[vcg_i]);
            continue;
            }

        prbsEngine = AtChannelPrbsEngineGet((AtChannel) vcg);
        if (prbsEngine == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: VCG %d has no PRBS engine \r\n", vcgIdList[vcg_i]);
            continue;
            }

        ret = AtModulePrbsConcateGroupPrbsEngineDelete(PrbsModule(), prbsEngine);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: can not delete PRBS Engine on VCG#%s with ret = %s\r\n", argv[0], AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtPrbsEngineVcgModeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineModeSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineVcgEnable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue, PrbsEnginesGet);
    }

eBool CmdAtPrbsEngineVcgDisable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse, PrbsEnginesGet);
    }

eBool CmdAtPrbsEngineVcgShow(char argc, char **argv)
    {
    return EngineShow(argc, argv, PrbsEnginesGet);
    }

eBool CmdAtPrbsEngineVcgCounterGet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineCounterGet(engines, argv[1], (argc >= 2) ? argv[2] : NULL);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineVcgErrorForce(char argc, char **argv)
    {
    return ErrorForce(argc, argv, cAtTrue, PrbsEnginesGet);
    }

eBool CmdAtPrbsEngineVcgErrorUnForce(char argc, char **argv)
    {
    return ErrorForce(argc, argv, cAtFalse, PrbsEnginesGet);
    }

eBool CmdAtPrbsEngineVcgInvertEnable(char argc, char **argv)
    {
    return Invert(argc, argv, cAtTrue);
    }

eBool CmdAtPrbsEngineVcgInvertDisable(char argc, char **argv)
    {
    return Invert(argc, argv, cAtFalse);
    }

eBool CmdAtPrbsEngineVcgTxErrorRateForce(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineTxErrorRateForce(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineVcgBitErrorRateShow(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineBitErrorRateShow(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }


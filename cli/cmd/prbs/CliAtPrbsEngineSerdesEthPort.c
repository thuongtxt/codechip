/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : CliAtPrbsEngineSerdesEthPort.c
 *
 * Created Date: Aug 24, 2013
 *
 * Description : PRBS CLI
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtPrbsEngine.h"
#include "../eth/CliAtModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtList PrbsEnginesGet(char *idString)
    {
    uint32 i;
    AtList engines = AtListCreate(0);
    AtList serdesControllers = CliAtEthPortSerdesControllers(idString);

    for (i = 0; i < AtListLengthGet(serdesControllers); i++)
        {
        AtSerdesController serdes = (AtSerdesController)AtListObjectGet(serdesControllers, i);
        AtPrbsEngine engine = AtSerdesControllerPrbsEngine(serdes);
        if (engine)
            AtListObjectAdd(engines, (AtObject)engine);
        }

    AtObjectDelete((AtObject)serdesControllers);

    return engines;
    }

static eBool Invert(char argc, char **argv, eBool invert)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineInvert(engines, invert);
	AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

static eBool ErrorForce(char argc, char **argv, eBool force)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineErrorForce(engines, force);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

static eBool Enable(char argc, char **argv, eBool enable)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eAtDirection direction;
    eBool success = cAtFalse;

    if (argc < 1)
        {
        AtPrintc(cSevCritical, "Not enough parameters\r\n");
        return cAtFalse;
        }

    if (argc == 1)
        {
        success = CliAtPrbsEngineEnable(engines, enable);
        AtObjectDelete((AtObject)engines);
        return success;
        }

    direction = CliDirectionFromStr(argv[1]);

    switch(direction)
        {
        case cAtDirectionTx:
            success = CliAtPrbsEngineTxEnable(engines, enable);
            break;
        case cAtDirectionRx:
            success = CliAtPrbsEngineRxEnable(engines, enable);
            break;
        case cAtDirectionAll:
            success = CliAtPrbsEngineEnable(engines, enable);
            break;

        case cAtDirectionInvalid:
            AtPrintc(cSevCritical, "ERROR: Invalid direction parameter. Expected: %s\r\n", CliValidDirections());
            return cAtFalse;

        default:
            break;
        }

    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineSerdesEthPortModeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eAtDirection direction;
    eBool success = cAtFalse;

    if (argc < 1)
        {
        AtPrintc(cSevCritical, "Not enough parameters\r\n");
        return cAtFalse;
        }

    if (argc == 2)
        {
        success = CliAtPrbsEngineModeSet(engines, argv[1]);
        AtObjectDelete((AtObject)engines);
        return success;
        }

    direction = CliDirectionFromStr(argv[2]);

    switch(direction)
        {
        case cAtDirectionTx:
            success = CliAtPrbsEngineTxModeSet(engines, argv[1]);
            break;
        case cAtDirectionRx:
            success = CliAtPrbsEngineRxModeSet(engines, argv[1]);
            break;
        case cAtDirectionAll:
            success = CliAtPrbsEngineModeSet(engines, argv[1]);
            break;

        case cAtDirectionInvalid:
            AtPrintc(cSevCritical, "ERROR: Invalid direction parameter. Expected: %s\r\n", CliValidDirections());
            return cAtFalse;

        default:
            break;
        }

    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineSerdesEthPortInvertEnable(char argc, char **argv)
    {
    return Invert(argc, argv, cAtTrue);
    }

eBool CmdAtPrbsEngineSerdesEthPortInvertDisable(char argc, char **argv)
    {
    return Invert(argc, argv, cAtFalse);
    }

eBool CmdAtPrbsEngineSerdesEthPortErrorForce(char argc, char **argv)
    {
    return ErrorForce(argc, argv, cAtTrue);
    }

eBool CmdAtPrbsEngineSerdesEthPortErrorUnForce(char argc, char **argv)
    {
    return ErrorForce(argc, argv, cAtFalse);
    }

eBool CmdAtPrbsEngineSerdesEthPortEnable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue);
    }

eBool CmdAtPrbsEngineSerdesEthPortDisable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse);
    }

eBool CmdAtPrbsEngineSerdesEthPortCounterGet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineCounterGet(engines, argv[1], (argc > 2) ? argv[2] : NULL);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineSerdesEthPortDebug(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineDebug(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineSerdesEthPortShow(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool silentMode = cAtFalse;
    eBool success = CliAtPrbsEngineShow(engines, silentMode);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineSerdesEthPortStart(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineStart(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineSerdesEthPortStop(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineStop(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineSerdesEthPortDurationSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineDurationSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

# @group CliAtPrbsEngineDe1 "DS1/E1"
# @ingroup CliAtModulePrbs "PRBS module"

4 pdh de1 prbs mode CmdAtPrbsEngineDe1ModeSet 2 de1List=1 mode
/*
    Syntax     : pdh de1 prbs mode <de1List> <prbs7/prbs9/prbs15/prbs23/prbs31/sequence/fixedpattern/prbs20/prbs20qrss/all0s/all1s/alt01/1in8>
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
                 mode    - PRBS mode: prbs7/prbs9/prbs15/prbs23/prbs31/sequence/fixedpattern/prbs20/prbs20qrss/all0s/all1s/alt01/1in8
    Description: Set PRBS mode
    Example    : * For DS1/E1 LIUs: 
                   pdh de1 prbs mode 1-16 prbs15
                 * For DS1/E1 mapped to VC1x:
                   pdh de1 prbs mode 1.1.1.1.1-1.1.1.1.3 prbs15
*/

5 pdh de1 prbs transmit mode CmdAtPrbsEngineDe1TransmitModeSet 2 de1List=1 mode
/*
    Syntax     : pdh de1 prbs mode <de1List> <prbs7/prbs9/prbs15/prbs23/prbs31/sequence/fixedpattern/prbs20/prbs20qrss/all0s/all1s/alt01/1in8>
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
                 mode    - PRBS mode: prbs7/prbs9/prbs15/prbs23/prbs31/sequence/fixedpattern/prbs20/prbs20qrss/all0s/all1s/alt01/1in8
    Description: Set PRBS mode for TX side
    Example    : * For DS1/E1 LIUs: 
                   pdh de1 prbs transmit mode 1-16 prbs15
                 * For DS1/E1 mapped to VC1x:
                   pdh de1 prbs transmit mode 1.1.1.1.1-1.1.1.1.3 prbs15
*/

5 pdh de1 prbs expect mode CmdAtPrbsEngineDe1ExpectModeSet 2 de1List=1 mode
/*
    Syntax     : pdh de1 prbs mode <de1List> <prbs7/prbs9/prbs15/prbs23/prbs31/sequence/fixedpattern/prbs20/prbs20qrss/all0s/all1s/alt01/1in8>
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
                 mode    - PRBS mode: prbs7/prbs9/prbs15/prbs23/prbs31/sequence/fixedpattern/prbs20/prbs20qrss/all0s/all1s/alt01/1in8
    Description: Set expected PRBS mode for RX side
    Example    : * For DS1/E1 LIUs: 
                   pdh de1 prbs expect mode 1-16 prbs15
                 * For DS1/E1 mapped to VC1x:
                   pdh de1 prbs expect mode 1.1.1.1.1-1.1.1.1.3 prbs15
*/

4 pdh de1 prbs datamode CmdAtPrbsEngineDe1DataModeSet 2 de1List=1 datamode
/*
    Syntax     : pdh de1 prbs datamode <de1List> <64kb/56kb>
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
                 mode    - PRBS Data mode: 64kb/56kb
    Description: Set PRBS mode
    Example    : * For DS1/E1 LIUs: 
                   pdh de1 prbs datamode 1-16 prbs15
                 * For DS1/E1 mapped to VC1x:
                   pdh de1 prbs datamode 1.1.1.1.1-1.1.1.1.3 prbs15
*/

5 pdh de1 prbs transmit datamode CmdAtPrbsEngineDe1TransmitDataModeSet 2 de1List=1 datamode
/*
    Syntax     : pdh de1 prbs datamode <de1List> <64kb/56kb>
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
                 mode    - PRBS data mode: 64kb/56kb
    Description: Set PRBS data mode for TX side
    Example    : * For DS1/E1 LIUs: 
                   pdh de1 prbs transmit datamode 1-16 prbs15
                 * For DS1/E1 mapped to VC1x:
                   pdh de1 prbs transmit datamode 1.1.1.1.1-1.1.1.1.3 prbs15
*/

5 pdh de1 prbs expect datamode CmdAtPrbsEngineDe1ExpectDataModeSet 2 de1List=1 datamode
/*
    Syntax     : pdh de1 prbs datamode <de1List> <64kb/56kb>
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
                 mode    - PRBS mode: 64kb/56kb
    Description: Set expected PRBS data mode for RX side
    Example    : * For DS1/E1 LIUs: 
                   pdh de1 prbs expect datamode 1-16 prbs15
                 * For DS1/E1 mapped to VC1x:
                   pdh de1 prbs expect datamode 1.1.1.1.1-1.1.1.1.3 prbs15
*/

4 pdh de1 prbs invert CmdAtPrbsEngineDe1InvertEnable 1 de1List=1
/*
    Syntax     : pdh de1 prbs invert <de1List>
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
    Description: PRBS invert
    Example    : * For DS1/E1 LIUs: 
                   pdh de1 prbs invert 1-16
                 * For DS1/E1 mapped to VC1x:
                   pdh de1 prbs invert 1.1.1.1.1-1.1.1.1.3
*/

4 pdh de1 prbs noinvert CmdAtPrbsEngineDe1InvertDisable 1 de1List=1
/*
    Syntax     : pdh de1 prbs noinvert <de1List>
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
    Description: PRBS no invert
    Example    : * For DS1/E1 LIUs: 
                   pdh de1 prbs noinvert 1-16
                 * For DS1/E1 mapped to VC1x:
                   pdh de1 prbs noinvert 1.1.1.1.1-1.1.1.1.3
*/

4 pdh de1 prbs force CmdAtPrbsEngineDe1ErrorForce 1 de1List=1
/*
    Syntax     : pdh de1 prbs force <de1List>
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
    Description: Force PRBS error
    Example    : * For DS1/E1 LIUs: 
                   pdh de1 prbs force 1-16
                 * For DS1/E1 mapped to VC1x:
                   pdh de1 prbs force 1.1.1.1.1-1.1.1.1.3
*/

4 pdh de1 prbs unforce CmdAtPrbsEngineDe1ErrorUnForce 1 de1List=1
/*
    Syntax     : pdh de1 prbs unforce <de1List>
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
    Description: Unforce PRBS error
    Example    : * For DS1/E1 LIUs: 
                   pdh de1 prbs unforce 1-16
                 * For DS1/E1 mapped to VC1x:
                   pdh de1 prbs unforce 1.1.1.1.1-1.1.1.1.3
*/

4 pdh de1 prbs enable CmdAtPrbsEngineDe1Enable 1 de1List=1
/*
    Syntax     : pdh de1 prbs enable <de1List>
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
    Description: Enable PRBS engine
    Example    : * For DS1/E1 LIUs: 
                   pdh de1 prbs enable 1-16
                 * For DS1/E1 mapped to VC1x:
                   pdh de1 prbs enable 1.1.1.1.1-1.1.1.1.3
*/

4 pdh de1 prbs disable CmdAtPrbsEngineDe1Disable 1 de1List=1
/*
    Syntax     : pdh de1 prbs disable <de1List>
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
    Description: Disable PRBS engine
    Example    : * For DS1/E1 LIUs: 
                   pdh de1 prbs disable 1-16
                 * For DS1/E1 mapped to VC1x:
                   pdh de1 prbs disable 1.1.1.1.1-1.1.1.1.3
*/

4 show pdh de1 prbs CmdAtPrbsEngineDe1Show -1 de1List=1 
/*
    Syntax     : show pdh de1 prbs <de1List> [silent]
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
                 silent    - If "silent" is specified, there would be no output
    Description: Show PRBS engines
    Example    : * For DS1/E1 LIUs: 
                   show pdh de1 prbs 1-16
                   show pdh de1 prbs 1-16 silent
                 * For DS1/E1 mapped to VC1x:
                   show pdh de1 prbs 1.1.1.1.1-1.1.1.1.3
                   show pdh de1 prbs 1.1.1.1.1-1.1.1.1.3 silent
*/

4 pdh de1 prbs side CmdAtPrbsEngineDe1SideSet 2 de1List=1 side
/*
    Syntax     : pdh de1 prbs side <de1List> <tdm/psn>
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
                 side    - Generate/monitor side
                           + tdm: PRBS will generate and monitor at TDM side
                           + psn: PRBS will generate and monitor at PSN side
    Description: Set side to generate and monitor
    Example    : * For DS1/E1 LIUs:
                   pdh de1 prbs side 1-16 tdm
                 * For DS1/E1 mapped to VC1x:
                   pdh de1 prbs side 1.1.1.1.1-1.1.1.1.3 psn
*/

5 show pdh de1 prbs counters CmdAtPrbsEngineDe1CounterGet -1 de1List=1 readingMode=r2c
/*
    Syntax     : show pdh de1 prbs counters <de1List> <ro/r2c> [silent]
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
                 counterReadMode - read only or read to clear counters.
                 silent          - If "silent" is specified, there would be no 
                                   output with "r2c" mode
    Description: Show PRBS engines counters
    Example    : show pdh de1 prbs counters 1 r2c
                 show pdh de1 prbs counters 1 r2c silent
*/

5 pdh de1 prbs force single CmdAtPrbsEngineDe1ErrorInject -1 de1List=1 numForcedErrors=1
/*
    Syntax     : pdh de1 prbs force single <de1List> [<numForcedErrors>]
    Parameter  : de1List         - Input DS1/E1 identifier. Format of one ID item is as following
                                   + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                                   + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                                     The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
                 numForcedErrors - Num forced errors
    Description: Force single error
    Example    : * For DS1/E1 LIUs: 
                   pdh de1 prbs force single 1-16
                 * For DS1/E1 mapped to VC1x:
                   pdh de1 prbs force single 1.1.1.1.1-1.1.1.1.3
*/

5 pdh de1 prbs force ber CmdAtPrbsEngineDe1TxErrorRateForce 2 de1List=1 ber=10e-3
/*
    Syntax     : pdh de1 prbs force ber <de1List> <ber>
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
                 ber -[off, 10e-2, 10e-3, 10e-4, 10e-5, 10e-6, 10e-7, 10e-8, 10e-9, 10e-10]
    Description: PRBS force Bit-Error-Rate
    Example    : * For DS1/E1 LIUs: 
                   pdh de1 prbs force ber 1-16 10e-3
                 * For DS1/E1 mapped to VC1x:
                   pdh de1 prbs force ber 1.1.1.1.1-1.1.1.1.3 10e-3
*/

4 pdh de1 prbs fixpattern CmdAtPrbsEngineDe1FixPatternSet 2 de1List=1 pattern=0x12345678
/*
    Syntax     : pdh de1 prbs fixpattern <de1List> <tx> <expect>
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
                 pattern -[0x00000000, 0xFFFFFFFF] transmiting and expected pattern 
    Description: Set Fix pattern value for both TX and RX sides
    Example    : * For DS1/E1 LIUs: 
                   pdh de1 prbs fixpattern 1-16 0x12345678 0x12345678
                 * For DS1/E1 mapped to VC1x:
                   pdh de1 prbs fixpattern 1.1.1.1.1-1.1.1.1.3 0x12345678 0x12345678
*/

5 pdh de1 prbs transmit fixpattern CmdAtPrbsEngineDe1TransmitFixPatternSet 2 de1List=1 tx=0x12345678
/*
    Syntax     : pdh de1 prbs transmit fixpattern <de1List> <tx> <expect>
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
                 tx -[0x00000000, 0xFFFFFFFF] transmiting pattern 
    Description: Set Transmit Fix pattern value
    Example    : * For DS1/E1 LIUs: 
                   pdh de1 prbs transmit fixpattern 1-16 0x12345678
                 * For DS1/E1 mapped to VC1x:
                   pdh de1 prbs transmit fixpattern 1.1.1.1.1-1.1.1.1.3 0x12345678
*/

5 pdh de1 prbs expect fixpattern CmdAtPrbsEngineDe1ExpectFixPatternSet 2 de1List=1 expect=0x12345678
/*
    Syntax     : pdh de1 prbs expect fixpattern <de1List> <tx> <expect>
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>                 
                 expect -[0x00000000, 0xFFFFFFFF] expected pattern 
    Description: Set Expect Fix pattern value
    Example    : * For DS1/E1 LIUs: 
                   pdh de1 prbs expect fixpattern 1-16 0x12345678 0x12345678
                 * For DS1/E1 mapped to VC1x:
                   pdh de1 prbs expect fixpattern 1.1.1.1.1-1.1.1.1.3 0x12345678 0x12345678
*/

5 show pdh de1 prbs ber CmdAtPrbsEngineDe1BitErrorRateShow 1 de1List=1
/*
    Syntax     : show pdh de1 prbs ber <de1List>
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu> 
    Description: Show BER of PRBS
    Example    : * For DS1/E1 LIUs: 
                   show pdh de1 prbs ber 1-16
                 * For DS1/E1 mapped to VC1x:
                   show pdh de1 prbs ber 1.1.1.1.1-1.1.1.1.3
*/

5 pdh de1 prbs side generating CmdAtPrbsEngineDe1GeneratingSideSet 2 de1List=1 side=tdm
/*
    Syntax     : pdh de1 prbs side generating <de1List> <tdm/psn>
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
                 side    - PRBS side: tdm/psn
    Description: Set PRBS generating side
    Example    : * For DS1/E1 LIUs: 
                   pdh de1 prbs side generating  1-16 tdm
                 * For DS1/E1 mapped to VC1x:
                   pdh de1 prbs side generating 1.1.1.1.1-1.1.1.1.3 tdm
*/

5 pdh de1 prbs side monitoring CmdAtPrbsEngineDe1MonitoringSideSet 2 de1List=1 side=tdm
/*
    Syntax     : pdh de1 prbs side monitoring <de1List> <tdm/psn>
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The moneral format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
                 side    - PRBS side: tdm/psn
    Description: Set PRBS monitoring side
    Example    : * For DS1/E1 LIUs: 
                   pdh de1 prbs side monitoring 1-16 tdm
                 * For DS1/E1 mapped to VC1x:
                   pdh de1 prbs side monitoring 1.1.1.1.1-1.1.1.1.3 tdm
*/

4 pdh de1 prbs bitorder CmdAtPrbsEngineDe1BitOrderSet 2 de1List=1 bitOrder
/*
    Syntax     : pdh de1 prbs bitorder <de1List> <msb/lsb>
    Parameter  : de1List  - Input DS1/E1 identifier. Format of one ID item is as following
                            + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                            + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                              The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
                 bitOrder - Bit order: msb/lsb
    Description: Set PRBS bit order
    Example    : * For DS1/E1 LIUs: 
                   pdh de1 prbs bitorder 1-16 msb
                 * For DS1/E1 mapped to VC1x:
                   pdh de1 prbs bitorder 1.1.1.1.1-1.1.1.1.3 msb
*/

5 pdh de1 line prbs force CmdAtPrbsEngineLineDe1ErrorForce 1 de1List=1
/*
    Syntax     : pdh de1 line prbs force <de1List>
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
    Description: Force Line PRBS error
    Example    : * For DS1/E1 LIUs: 
                   pdh de1 line prbs force 1-16
                 * For DS1/E1 mapped to VC1x:
                   pdh de1 line prbs force 1.1.1.1.1-1.1.1.1.3
*/

5 pdh de1 line prbs unforce CmdAtPrbsEngineLineDe1ErrorUnForce 1 de1List=1
/*
    Syntax     : pdh de1 line prbs unforce <de1List>
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
    Description: Unforce Line PRBS error
    Example    : * For DS1/E1 LIUs: 
                   pdh de1 line prbs unforce 1-16
                 * For DS1/E1 mapped to VC1x:
                   pdh de1 line prbs unforce 1.1.1.1.1-1.1.1.1.3
*/

5 pdh de1 line prbs enable CmdAtPrbsEngineLineDe1Enable 1 de1List=1
/*
    Syntax     : pdh de1 line prbs enable <de1List>
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
    Description: Enable Line PRBS engine
    Example    : * For DS1/E1 LIUs: 
                   pdh de1 line prbs enable 1-16
                 * For DS1/E1 mapped to VC1x:
                   pdh de1 line prbs enable 1.1.1.1.1-1.1.1.1.3
*/

5 pdh de1 line prbs disable CmdAtPrbsEngineLineDe1Disable 1 de1List=1
/*
    Syntax     : pdh de1 line prbs disable <de1List>
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
    Description: Disable Line PRBS engine
    Example    : * For DS1/E1 LIUs: 
                   pdh de1 line prbs disable 1-16
                 * For DS1/E1 mapped to VC1x:
                   pdh de1 line prbs disable 1.1.1.1.1-1.1.1.1.3
*/

5 show pdh de1 line prbs CmdAtPrbsEngineLineDe1Show 1 de1List=1
/*
    Syntax     : show pdh de1 line prbs <de1List>
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
    Description: Show Line PRBS engines
    Example    : * For DS1/E1 LIUs: 
                   show pdh de1 line prbs 1-16
                 * For DS1/E1 mapped to VC1x:
                   show pdh de1 line prbs 1.1.1.1.1-1.1.1.1.3
*/

5 pdh de1 line prbs start CmdAtPrbsEngineLineDe1Start 1 de1List=1
/*
    Syntax     : pdh de1 line prbs start <de1List>
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
    Description: start PRBS engine
    Example    : * For DS1/E1 LIUs: 
                   pdh de1 line prbs start 1-16
                 * For DS1/E1 mapped to VC1x:
                   pdh de1 line prbs start 1.1.1.1.1-1.1.1.1.3
*/

5 pdh de1 line prbs stop CmdAtPrbsEngineLineDe1Stop 1 de1List=1
/*
    Syntax     : pdh de1 line prbs stop <de1List>
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
    Description: Stop PRBS engine
    Example    : * For DS1/E1 LIUs: 
                   pdh de1 line prbs stop 1-16
                 * For DS1/E1 mapped to VC1x:
                   pdh de1 line prbs stop 1.1.1.1.1-1.1.1.1.3
*/

5 pdh de1 line prbs duration CmdAtPrbsEngineLineDe1DurationSet 2 de1List=1 durInMs=500
/*
    Syntax     : pdh de1 line prbs duration <de1List> <durInMs>
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
                 durInMs    - Duration in milisecond
    Description: Set duration
    Example    : * For DS1/E1 LIUs: 
                   pdh de1 line prbs duration 1-16 500
                 * For DS1/E1 mapped to VC1x:
                   pdh de1 line prbs duration 1.1.1.1.1-1.1.1.1.3 500
*/

4 pdh de1 prbs start CmdAtPrbsEngineDe1Start 1 de1List=1
/*
    Syntax     : pdh de1 prbs start <de1List>
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
    Description: start PRBS engine
    Example    : * For DS1/E1 LIUs: 
                   pdh de1 prbs start 1-16
                 * For DS1/E1 mapped to VC1x:
                   pdh de1 prbs start 1.1.1.1.1-1.1.1.1.3
*/

4 pdh de1 prbs stop CmdAtPrbsEngineDe1Stop 1 de1List=1
/*
    Syntax     : pdh de1 prbs stop <de1List>
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
    Description: Stop PRBS engine
    Example    : * For DS1/E1 LIUs: 
                   pdh de1 prbs stop 1-16
                 * For DS1/E1 mapped to VC1x:
                   pdh de1 prbs stop 1.1.1.1.1-1.1.1.1.3
*/

4 pdh de1 prbs duration CmdAtPrbsEngineDe1DurationSet 2 de1List=1 durInMs=400
/*
    Syntax     : pdh de1 prbs duration <de1List> <durInMs>
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
                 durInMs    - Duration in milisecond
    Description: Set duration
    Example    : * For DS1/E1 LIUs: 
                   pdh de1 prbs duration 1-16 400
                 * For DS1/E1 mapped to VC1x:
                   pdh de1 prbs duration 1.1.1.1.1-1.1.1.1.3 400
*/

5 pdh de1 prbs delay enable CmdAtPrbsEngineDe1DelayEnable 1 de1List=1
/*
    Syntax     : pdh de1 delay enable <de1List> 
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
    Description: Enable Delay
    Example    : * For DS1/E1 LIUs: 
                   pdh de1 prbs delay enable 1-16 
                 * For DS1/E1 mapped to VC1x:
                   pdh de1 prbs delay enable 1.1.1.1.1-1.1.1.1.3 
*/

5 pdh de1 prbs delay disable CmdAtPrbsEngineDe1DelayDisable 1 de1List=1
/*
    Syntax     : pdh de1 delay disable <de1List> 
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
    Description: Disable Delay
    Example    : * For DS1/E1 LIUs: 
                   pdh de1 prbs delay disable 1-16 
                 * For DS1/E1 mapped to VC1x:
                   pdh de1 prbs delay disable 1.1.1.1.1-1.1.1.1.3 
*/

5 show pdh de1 prbs delay CmdAtPrbsEngineDe1DelayGet -1 de1List=1 r2c silent
/*
    Syntax     : show pdh de1 prbs duration <de1List>
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
    Description: Show PRBS delay
    Example    : * For DS1/E1 LIUs: 
                   show pdh de1 prbs delay 1-16
                 * For DS1/E1 mapped to VC1x:
                   show pdh de1 prbs delay 1.1.1.1.1-1.1.1.1.3
*/

5 pdh de1 prbs disruption max_expect_time CmdAtPrbsEngineDe1DisruptionMaxExpectationTimeSet 2 de1List=1 durInMs=400
/*
    Syntax     : pdh de1 prbs max_expect_time <de1List> <durInMs>
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
                 durInMs    - Duration in milisecond
    Description: Set duration
    Example    : * For DS1/E1 LIUs: 
                   pdh de1 prbs max_expect_time 1-16 400
                 * For DS1/E1 mapped to VC1x:
                   pdh de1 prbs max_expect_time 1.1.1.1.1-1.1.1.1.3 400
*/

5 show pdh de1 prbs disruption CmdAtPrbsEngineDe1DisruptionGet -1 de1List=1 r2c silent
/*
    Syntax     : show pdh de1 prbs disruption <de1List>
    Parameter  : de1List - Input DS1/E1 identifier. Format of one ID item is as following
                           + For DS1/E1 LIU: 1..N where N is maximum number of DS1s/E1s
                           + For DS1/E1 mapped to SDH VC, its ID is the same as VC-1x ID.
                             The general format is: <line>.<aug1>.<au3Tug3>.<tug2>.<tu>
    Description: Show PRBS disruption
    Example    : * For DS1/E1 LIUs: 
                   show pdh de1 prbs disruption 1-16
                 * For DS1/E1 mapped to VC1x:
                   show pdh de1 prbs disruption 1.1.1.1.1-1.1.1.1.3
*/
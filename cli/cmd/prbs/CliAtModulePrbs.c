/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : CliAtModulePrbs.c
 *
 * Created Date: Apr 17, 2015
 *
 * Description : PRBS command
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "AtDevice.h"
#include "AtPdhDe1.h"
#include "CliAtPrbsEngine.h"
#include "AtCliModule.h"
#include "../textui/CliAtTextUI.h"

/*--------------------------- Define -----------------------------------------*/
#define cCliSharedChannelBufferSize 2048

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtList NxDs0List(char *de1IdString, char *timeSlotString)
    {
    uint32 numChannels, i;
    uint32 numDs0, bitmap, *ds0List;
    AtChannel *channelList;
    AtList nxDs0List = AtListCreate(0);

    /* Get DE1 ID */
    channelList = CliSharedChannelListGet(&numChannels);
    numChannels = De1ListFromString(de1IdString, channelList, numChannels);

    /* Get DS0 list */
    ds0List = &CliSharedIdBufferGet(&numDs0)[cCliSharedChannelBufferSize / 2];
    numDs0  = CliIdListFromString(timeSlotString, ds0List, numDs0);

    /* Create bitmap from DS0 list */
    bitmap = 0;
    for (i = 0; i < numDs0; i++)
        bitmap |= cBit0 << ds0List[i];

    for (i = 0; i < numChannels; i++)
        {
        AtPdhNxDS0 nxDs0;

        /* Check if DS0 has already been created */
        nxDs0 = AtPdhDe1NxDs0Get((AtPdhDe1)channelList[i], bitmap);
        if (nxDs0 == NULL)
            {
            AtPrintc(cSevWarning,
                     "WARNING: NxDS0 0x%08x of %s has not been created\r\n",
                     bitmap,
                     CliChannelIdStringGet(channelList[i]));
            continue;
            }

        AtListObjectAdd(nxDs0List, (AtObject)nxDs0);
        }

    return nxDs0List;
    }

static AtList PrbsEnginesGet(char *idString)
    {
    AtList engines = AtListCreate(0);
    uint32 bufferSize;
    uint32 *idBuf;
    uint32 numEngines;
    AtModulePrbs prbsModule;
    uint16 prbsEngineId;

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numEngines = CliIdListFromString(idString, idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid PRBS engine list, expected: 1 or 1-16, ...\n");
        return NULL;
        }

    prbsModule = (AtModulePrbs)AtDeviceModuleGet(CliDevice(), cAtModulePrbs);
    for (prbsEngineId = 0; prbsEngineId < numEngines; prbsEngineId++)
        {
        AtPrbsEngine engine = (AtPrbsEngine)AtModulePrbsEngineGet(prbsModule, idBuf[prbsEngineId] - 1);
        if (engine)
            AtListObjectAdd(engines, (AtObject)engine);
        }

    return engines;
    }

static eBool Invert(char argc, char **argv, eBool invert, eAtModulePrbsRet (*invertHandler)(AtPrbsEngine, eBool))
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineInvertWithHandler(engines, invert, invertHandler);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

static eBool ErrorForce(char argc, char **argv, eBool force)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineErrorForce(engines, force);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

static eBool Enable(char argc, char **argv, eBool enable)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineEnable(engines, enable);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

static eBool ModeSet(char argc, char **argv, eBool (*ModeSetFunc)(AtList engines, const char *modeString))
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = ModeSetFunc(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

AtList CliPrbsEnginesGet(char *idString)
    {
    return PrbsEnginesGet(idString);
    }

eBool CmdAtModulePrbsEnginePdhDe1Create(char argc, char **argv)
    {
    uint16       prbsEngineId;
    uint32       *idBuf;
    uint32       bufferSize;
    uint32       numEngines;
    AtModulePrbs prbsModule;
    uint32       numChannels;
    AtChannel    *channelList = CliSharedChannelListGet(&numChannels);
    eBool        success = cAtTrue;

    AtUnused(argc);

    /* Get DE1 list first due to buffer using issue */
    numChannels = De1ListFromString(argv[1], channelList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid DS1/E1s, expected: 1 or 1.1.1 ...\n");
        return cAtFalse;
        }

    /* Get PRBS engine list */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numEngines = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid PRBS engine list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    if (numEngines != numChannels)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Number of prbs engines (%d) and DE1 channel (%d) must be equal\r\n",
                 numEngines, numChannels);
        return cAtFalse;
        }

    prbsModule = (AtModulePrbs)AtDeviceModuleGet(CliDevice(), cAtModulePrbs);
    for (prbsEngineId = 0; prbsEngineId < numEngines; prbsEngineId = (uint16)AtCliNextIdWithSleep(prbsEngineId, AtCliLimitNumRestTdmChannelGet()))
        {
        if (AtModulePrbsDe1PrbsEngineCreate(prbsModule, idBuf[prbsEngineId] - 1, (AtPdhDe1)channelList[prbsEngineId]) == NULL)
            {
            AtPrintc(cSevCritical, "ERROR: PRBS engine %u cannot be created or it is existing\r\n", idBuf[prbsEngineId]);
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtModulePrbsEnginePdhDe3Create(char argc, char **argv)
    {
    uint16       prbsEngineId;
    uint32       *idBuf, *myIdBuf;
    uint32       bufferSize;
    uint32       numEngines;
    AtModulePrbs prbsModule;
    uint32       numChannels;
    AtChannel    *channelList = CliSharedChannelListGet(&numChannels);
    eBool        success = cAtTrue;

    AtUnused(argc);

    /* Channels need to be get first because of side affect that alter content
     * of shared ID buffer */
    numChannels = CliDe3ListFromString(argv[1], channelList, numChannels);

    /* Get PRBS engine list */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numEngines = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid PRBS engine list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }
    myIdBuf = AtOsalMemAlloc(sizeof(uint32) * numEngines);
    AtOsalMemCpy(myIdBuf, (void*)idBuf, sizeof(uint32) * numEngines);

    if (numEngines != numChannels)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Number of prbs engines (%d) and DE3 channel (%d) must be equal\r\n",
                 numEngines, numChannels);
        AtOsalMemFree(myIdBuf);
        return cAtFalse;
        }

    prbsModule = (AtModulePrbs)AtDeviceModuleGet(CliDevice(), cAtModulePrbs);
    for (prbsEngineId = 0; prbsEngineId < numEngines; prbsEngineId = (uint16)AtCliNextIdWithSleep(prbsEngineId, AtCliLimitNumRestTdmChannelGet()))
        {
        if (AtModulePrbsDe3PrbsEngineCreate(prbsModule, myIdBuf[prbsEngineId] - 1, (AtPdhDe3)channelList[prbsEngineId]) == NULL)
            {
            AtPrintc(cSevCritical, "ERROR: PRBS engine %u cannot be created or it is existing\r\n", idBuf[prbsEngineId]);
            success = cAtFalse;
            }
        }

    AtOsalMemFree(myIdBuf);
    return success;
    }

eBool CmdAtModulePrbsEnginePdhNxDs0Create(char argc, char **argv)
    {
    uint16       prbsEngineId;
    uint32       *idBuf, *myIdBuf;
    uint32       bufferSize;
    uint32       numEngines;
    AtModulePrbs prbsModule;
    uint32       numChannels;
    AtList       nxDs0List;
    eBool        success = cAtTrue;

    AtUnused(argc);

    /* Channels need to be get first because of side affect that alter content
     * of shared ID buffer */
    nxDs0List = NxDs0List(argv[1], argv[2]);

    /* Get PRBS engine list */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numEngines = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid PRBS engine list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }
    myIdBuf = AtOsalMemAlloc(sizeof(uint32) * numEngines);
    AtOsalMemCpy(myIdBuf, (void*)idBuf, sizeof(uint32) * numEngines);

    numChannels = AtListLengthGet(nxDs0List);
    if (numEngines != numChannels)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Number of prbs engines (%d) and NxDs0 channel (%d) must be equal\r\n",
                 numEngines, numChannels);
        AtObjectDelete((AtObject)nxDs0List);
        AtOsalMemFree(myIdBuf);
        return cAtFalse;
        }

    prbsModule = (AtModulePrbs)AtDeviceModuleGet(CliDevice(), cAtModulePrbs);
    for (prbsEngineId = 0; prbsEngineId < numEngines; prbsEngineId = (uint16)AtCliNextIdWithSleep(prbsEngineId, AtCliLimitNumRestTdmChannelGet()))
        {
        if (AtModulePrbsNxDs0PrbsEngineCreate(prbsModule, myIdBuf[prbsEngineId] - 1, (AtPdhNxDS0)AtListObjectGet(nxDs0List, prbsEngineId)) == NULL)
            {
            AtPrintc(cSevCritical, "ERROR: PRBS engine %u cannot be created or it is existing\r\n", idBuf[prbsEngineId]);
            success = cAtFalse;
            }
        }

    AtOsalMemFree(myIdBuf);
    AtObjectDelete((AtObject)nxDs0List);

    return success;
    }

eBool CmdAtModulePrbsEngineSdhPathCreate(char argc, char **argv)
    {
    uint16       prbsEngineId;
    uint32       *idBuf;
    uint32       bufferSize;
    uint32       numEngines;
    AtModulePrbs prbsModule;
    uint32       numberPaths;
    AtChannel    *channelList = CliSharedChannelListGet(&bufferSize);
    eBool        success = cAtTrue;

    AtUnused(argc);

    /* Channels need to be get first because of side affect that alter content
     * of shared ID buffer */
    numberPaths = CliChannelListFromString(argv[1], channelList, bufferSize);

    /* Get PRBS engine list */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numEngines = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid PRBS engine list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    if (numEngines != numberPaths)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Number of prbs engines (%d) and SDH path channel (%d) must be equal\r\n",
                 numEngines, numberPaths);
        return cAtFalse;
        }

    prbsModule = (AtModulePrbs)AtDeviceModuleGet(CliDevice(), cAtModulePrbs);
    for (prbsEngineId = 0; prbsEngineId < numEngines; prbsEngineId = (uint16)AtCliNextIdWithSleep(prbsEngineId, AtCliLimitNumRestTdmChannelGet()))
        {
        if (AtModulePrbsSdhVcPrbsEngineCreate(prbsModule, idBuf[prbsEngineId] - 1, (AtSdhChannel)channelList[prbsEngineId]) == NULL)
            {
            AtPrintc(cSevCritical, "ERROR: PRBS engine %u cannot be created or it is existing\r\n", idBuf[prbsEngineId]);
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtModulePrbsEngineEthPortCreate(char argc, char **argv)
    {
    uint16       prbsEngineId;
    uint32       *idBuf;
    uint32       bufferSize;
    uint32       numEngines;
    AtModulePrbs prbsModule;
    uint32       numPorts;
    AtChannel    *channelList;
    eBool        success = cAtTrue;

    AtUnused(argc);

    /* Channels need to be get first because of side affect that alter content
     * of shared ID buffer */
    channelList = CliSharedChannelListGet(&bufferSize);
    if ((numPorts = (uint8)CliEthPortListFromString(argv[1], channelList, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "Invalid Port ID list or Port not exist. Expected: 1, 1-2 or 1.1 ...\r\n");
        return cAtFalse;
        }

    /* Get PRBS engine list */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numEngines = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid PRBS engine list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    if (numEngines != numPorts)
        {
        AtPrintc(cSevCritical,
                 "ERROR: Number of prbs engines (%d) and Ethernet port (%d) must be equal\r\n",
                 numEngines, numPorts);
        return cAtFalse;
        }

    prbsModule = (AtModulePrbs)AtDeviceModuleGet(CliDevice(), cAtModulePrbs);
    for (prbsEngineId = 0; prbsEngineId < numEngines; prbsEngineId = (uint16)AtCliNextIdWithSleep(prbsEngineId, AtCliLimitNumRestTdmChannelGet()))
        {
        if (AtModulePrbsEthPrbsEngineCreate(prbsModule, idBuf[prbsEngineId] - 1, (AtEthPort)channelList[prbsEngineId]) == NULL)
            {
            AtPrintc(cSevCritical, "ERROR: PRBS engine %u cannot be created or it is existing\r\n", idBuf[prbsEngineId]);
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtPrbsEngineShow(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool silentMode = cAtFalse;
    eBool success = CliAtPrbsEngineShowWithSilent(engines, cAtTrue, silentMode);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtModulePrbsEngineDelete(char argc, char **argv)
    {
    uint16 prbsEngineId;
    AtModulePrbs prbsModule;
    uint32 numEngines;
    uint32 bufferSize;
    uint32 *idBuf;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numEngines = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid PRBS engine list, expected: 1 or 1-16, ...\n");
        return cAtFalse;
        }

    prbsModule = (AtModulePrbs)AtDeviceModuleGet(CliDevice(), cAtModulePrbs);
    for (prbsEngineId = 0; prbsEngineId < numEngines; prbsEngineId = (uint16)AtCliNextIdWithSleep(prbsEngineId, AtCliLimitNumRestTdmChannelGet()))
        {
        uint32 ret;
        uint32 engineId = idBuf[prbsEngineId] - 1;
        AtPrbsEngine engine = AtModulePrbsEngineGet(prbsModule, engineId);

        if (engine == NULL)
            {
            AtPrintc(cSevWarning, "Engine %d does not exist or it has not been creted yet, ignoring...\r\n", idBuf[prbsEngineId]);
            continue;
            }

        ret = AtModulePrbsEngineDelete(prbsModule, engineId);
        if (ret == cAtOk)
            continue;

        AtPrintc(cSevCritical, "ERROR: Delete PRBS engine %u fail with ret = %s\r\n", idBuf[prbsEngineId], AtRet2String(ret));
        success = cAtFalse;
        }

    return success;
    }

eBool CmdAtPrbsEngineModeSet(char argc, char **argv)
    {
	return ModeSet(argc, argv, CliAtPrbsEngineModeSet);
    }

eBool CmdAtPrbsEngineRxModeSet(char argc, char **argv)
    {
	return ModeSet(argc, argv, CliAtPrbsEngineRxModeSet);
    }

eBool CmdAtPrbsEngineTxModeSet(char argc, char **argv)
    {
	return ModeSet(argc, argv, CliAtPrbsEngineTxModeSet);
    }

eBool CmdAtPrbsEngineInvertEnable(char argc, char **argv)
    {
    return Invert(argc, argv, cAtTrue, AtPrbsEngineInvert);
    }

eBool CmdAtPrbsEngineInvertDisable(char argc, char **argv)
    {
    return Invert(argc, argv, cAtFalse, AtPrbsEngineInvert);
    }

eBool CmdAtPrbsEngineErrorForce(char argc, char **argv)
    {
    return ErrorForce(argc, argv, cAtTrue);
    }

eBool CmdAtPrbsEngineErrorUnForce(char argc, char **argv)
    {
    return ErrorForce(argc, argv, cAtFalse);
    }

eBool CmdAtPrbsEngineEnable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtTrue);
    }

eBool CmdAtPrbsEngineTxEnable(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineTxEnable(engines, cAtTrue);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineTxDisable(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineTxEnable(engines, cAtFalse);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineRxEnable(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineRxEnable(engines, cAtTrue);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineRxDisable(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineRxEnable(engines, cAtFalse);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineDisable(char argc, char **argv)
    {
    return Enable(argc, argv, cAtFalse);
    }

eBool CmdAtPrbsEngineTxErrorRateForce(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineTxErrorRateForce(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineFixPatternSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    uint32 transmitPattern = AtStrToDw(argv[1]);
    uint32 expectedPattern = AtStrToDw(argv[2]);
    eBool success;
    AtUnused(argc);
    success  = CliAtPrbsEngineTxFixedPatternSet(engines, transmitPattern);
    success &= CliAtPrbsEngineRxFixedPatternSet(engines, expectedPattern);

    AtObjectDelete((AtObject) engines);
    return success;
    }

eBool CmdAtPrbsEngineBitErrorRateShow(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineBitErrorRateShow(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineSideSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineSideSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineGeneratingSideSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineGeneratingSideSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineMonitoringSideSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineMonitoringSideSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineCounterGet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineCounter64BitGet(engines, argv[1], (argc > 2) ? argv[2] : NULL);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineErrorInject(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineErrorInject(engines, (argc > 1) ? (uint32)AtStrToDw(argv[1]) : 1);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineBitOrderSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineBitOrderSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineStart(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineStart(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineStop(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineStop(engines);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineDurationSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success = CliAtPrbsEngineDurationSet(engines, argv[1]);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEngineTxInvertEnable(char argc, char **argv)
    {
    return Invert(argc, argv, cAtTrue, AtPrbsEngineTxInvert);
    }

eBool CmdAtPrbsEngineTxInvertDisable(char argc, char **argv)
    {
    return Invert(argc, argv, cAtFalse, AtPrbsEngineTxInvert);
    }

eBool CmdAtPrbsEngineRxInvertEnable(char argc, char **argv)
    {
    return Invert(argc, argv, cAtTrue, AtPrbsEngineRxInvert);
    }

eBool CmdAtPrbsEngineRxInvertDisable(char argc, char **argv)
    {
    return Invert(argc, argv, cAtFalse, AtPrbsEngineRxInvert);
    }

eBool CmdAtPrbsEngineTxFixPatternSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    uint32 transmitPattern = AtStrToDw(argv[1]);
    eBool success;
    AtUnused(argc);
    success  = CliAtPrbsEngineTxFixedPatternSet(engines, transmitPattern);

    AtObjectDelete((AtObject) engines);
    return success;
    }

eBool CmdAtPrbsEngineRxFixPatternSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    uint32 expectedPattern = AtStrToDw(argv[1]);
    eBool success;
    AtUnused(argc);
    success = CliAtPrbsEngineRxFixedPatternSet(engines, expectedPattern);

    AtObjectDelete((AtObject) engines);
    return success;
    }

eBool CmdAtPrbsEngineBandwidthShow(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool silentMode = cAtFalse;
    eBool success = CliAtPrbsEngineBandwidthShowWithSilent(engines, cAtTrue, silentMode);
    AtUnused(argc);
    AtObjectDelete((AtObject)engines);
    return success;
    }

eBool CmdAtPrbsEnginePayloadLengthModeSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    eBool success;
    AtUnused(argc);
    success = CliAtPrbsEnginePayloadLengthModeSet(engines, argv[1]);

    AtObjectDelete((AtObject) engines);
    return success;
    }

eBool CmdAtPrbsEnginePayloadLengthMinSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    uint32 value = AtStrToDw(argv[1]);
    eBool success;
    AtUnused(argc);
    success = CliAtPrbsEngineMinLengthSet(engines, value);

    AtObjectDelete((AtObject) engines);
    return success;
    }

eBool CmdAtPrbsEnginePayloadLengthMaxSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    uint32 value = AtStrToDw(argv[1]);
    eBool success;
    AtUnused(argc);
    success = CliAtPrbsEngineMaxLengthSet(engines, value);

    AtObjectDelete((AtObject) engines);
    return success;
    }

eBool CmdAtPrbsEnginePayloadLengthBurstSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    uint32 value = AtStrToDw(argv[1]);
    eBool success;
    AtUnused(argc);
    success = CliAtPrbsEngineBurstSizeSet(engines, value);

    AtObjectDelete((AtObject) engines);
    return success;
    }

eBool CmdAtPrbsEngineBandwidthPercentageSet(char argc, char **argv)
    {
    AtList engines = PrbsEnginesGet(argv[0]);
    uint32 value = AtStrToDw(argv[1]);
    eBool success;
    AtUnused(argc);
    success = CliAtPrbsEnginePercentageBandwidthSet(engines, value);

    AtObjectDelete((AtObject) engines);
    return success;
    }

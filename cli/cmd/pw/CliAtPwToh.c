/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : CliAtPwToh.c
 *
 * Created Date: Mar 7, 2014
 *
 * Description : TOH PW
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtModulePw.h"
#include "AtModulePw.h"
#include "AtPwToh.h"
#include "AtDevice.h"
#include "AtTokenizer.h"
#include "AtPw.h"
#include "AtDriver.h"

/*--------------------------- Define -----------------------------------------*/
#define cTohStringFormat ","
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char *cAtTohStr[] = {"A1", "A2", "J0", "B1", "E1", "F1", "D1", "D2", "D3","H1", "H2", "H3",
                                  "B2", "K1", "K2", "D4", "D5", "D6", "D7", "D8", "D9",
                                  "D10", "D11", "D12", "S1", "M1", "E2"};

static const uint32 cAtTohVal[]  = {cAtSdhLineRsOverheadByteA1,
                                    cAtSdhLineRsOverheadByteA2,
                                    cAtSdhLineRsOverheadByteJ0,
                                    cAtSdhLineRsOverheadByteB1,
                                    cAtSdhLineRsOverheadByteE1,
                                    cAtSdhLineRsOverheadByteF1,
                                    cAtSdhLineRsOverheadByteD1,
                                    cAtSdhLineRsOverheadByteD2,
                                    cAtSdhLineRsOverheadByteD3,
                                    cAtSdhLineRsOverheadByteH1,
                                    cAtSdhLineRsOverheadByteH2,
                                    cAtSdhLineRsOverheadByteH3,

                                    /* MSOH */
                                    cAtSdhLineMsOverheadByteB2,
                                    cAtSdhLineMsOverheadByteK1,
                                    cAtSdhLineMsOverheadByteK2,
                                    cAtSdhLineMsOverheadByteD4,
                                    cAtSdhLineMsOverheadByteD5,
                                    cAtSdhLineMsOverheadByteD6,
                                    cAtSdhLineMsOverheadByteD7,
                                    cAtSdhLineMsOverheadByteD8,
                                    cAtSdhLineMsOverheadByteD9,
                                    cAtSdhLineMsOverheadByteD10,
                                    cAtSdhLineMsOverheadByteD11,
                                    cAtSdhLineMsOverheadByteD12,
                                    cAtSdhLineMsOverheadByteS1,
                                    cAtSdhLineMsOverheadByteM1,
                                    cAtSdhLineMsOverheadByteE2
                                    };

#define mRs(name) cAtSdhLineRsOverheadByte##name
#define mMs(name) cAtSdhLineMsOverheadByte##name
static const eAtSdhLineOverheadByte m_TohTable[9][3] = {{mRs(A1) , mRs(A2) , mRs(J0)},
													   {mRs(B1) , mRs(E1) , mRs(F1)},
													   {mRs(D1) , mRs(D2) , mRs(D3)},
													   {mRs(H1) , mRs(H2) , mRs(H3)},
													   {mMs(B2) , mMs(K1) , mMs(K2)},
													   {mMs(D4) , mMs(D5) , mMs(D6)},
													   {mMs(D7) , mMs(D8) , mMs(D9)},
													   {mMs(D10), mMs(D11), mMs(D12)},
													   {mMs(S1) , mMs(M1) , mMs(E2)}};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 AllTohBytesGetFromString(char* string, uint32 *numBytes)
    {
    uint32 tohBitMap = 0;
    AtTokenizer tokenizer = AtTokenizerSharedTokenizer((void *)string, cTohStringFormat);
    uint32 numItems = 0;
    char* ohByteString;
    eAtSdhLineOverheadByte ohByte = cAtSdhLineRsOverheadByteA1;
    eBool convertSuccess;

    while((AtTokenizerHasNextString(tokenizer)) && (numItems < 324))
        {
        ohByteString = AtTokenizerNextString(tokenizer);
        ohByte = CliStringToEnum(ohByteString, cAtTohStr, cAtTohVal, mCount(cAtTohVal), &convertSuccess);
        if (!convertSuccess)
            continue;

        tohBitMap |= ohByte;
        numItems++;
        }

    *numBytes = (uint8)numItems;
    return tohBitMap;
    }

static eBool Carry(char argc, char **argv, eBool enable)
    {
    AtList      pwList;
    uint16      pw_i, sts_i;
    uint32      *idBuf;
    uint32      bufferSize;
    uint32      numPws, numSts;
    eAtRet      ret = cAtOk;
    uint32      numTohByte;
    AtPw        pw;
    uint32      ohBitMap;
	AtUnused(argc);

    pwList = CliPwListFromString(argv[0]);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    numPws = AtListLengthGet(pwList);

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numSts = CliIdListFromString(argv[1], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    ohBitMap = AllTohBytesGetFromString(argv[2], &numTohByte);
    if (numTohByte == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid TOH list\n");
        AtObjectDelete((AtObject)pwList);
        return cAtFalse;
        }

    for (pw_i = 0; pw_i < numPws; pw_i++)
        {
        pw = (AtPw)AtListObjectGet(pwList, pw_i);
        if (AtPwTypeGet(pw) != cAtPwTypeToh)
            {
            AtPrintc(cSevWarning, "WARNING: %s is not TOH\r\n", CliChannelIdStringGet((AtChannel)pw));
            continue;
            }

        for (sts_i = 0; sts_i < numSts; sts_i++)
            {
            ret = AtPwTohByteEnable((AtPwToh)pw, (uint8)(idBuf[sts_i] - 1), ohBitMap, enable);
            if (ret != cAtOk)
                AtPrintc(cSevCritical, "ERROR: Can not configure for %s, ret = %s\r\n",
                         CliChannelIdStringGet((AtChannel)pw),
                         AtRet2String(ret));
            }
        }

    AtObjectDelete((AtObject)pwList);
    return (ret != cAtOk) ? cAtFalse : cAtTrue;
    }

eBool CmdPwTohByteEnable(char argc, char **argv)
    {
    return Carry(argc, argv, cAtTrue);
    }

eBool CmdPwTohByteDisable(char argc, char **argv)
    {
    return Carry(argc, argv, cAtFalse);
    }

static char* StsOhBytesToString(AtPwToh pw, uint8 stsId)
    {
    uint32 ohBitMap;
    uint8 numBytes = AtPwTohEnabledBytesByStsGet(pw, stsId, &ohBitMap);
    uint8 i;
    static char string[128];
    eBool convertSuccess;

    if (numBytes == 0)
        {
        AtSprintf(string, "none");
        return string;
        }

    AtOsalMemInit(string, 0, sizeof(string));

    for (i = 0; i < 32; i++)
        {
        char temp[5];

        if ((ohBitMap & (cBit0 << i)) == 0)
            continue;

        AtOsalMemInit(temp, 0, sizeof(temp));
        mAtEnumToStr(cAtTohStr, cAtTohVal, (cBit0 << i), temp, convertSuccess);
        if (convertSuccess)
        	{
        	AtStrcat(string, temp);
        	AtStrcat(string, ",");
        	}
        }

    string[AtStrlen(string) - 1] = '\0';
    return string;
    }

eBool CmdPwTohShow(char argc, char **argv)
    {
    AtList       pwList;
    tTab         *tabPtr;
    uint32       numPws, i, rowIndex;
    uint8        j;
    uint32       numSts;
    AtPwToh      pwToh;
    char         strBuf[32];
    const char   *heading[] = {"PW", "Carried bytes"};
	AtUnused(argc);

    pwList = CliPwListFromString(argv[0]);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    numPws = AtListLengthGet(pwList);

    /* Allocate memory */
    tabPtr = TableAlloc(12, 2, heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        AtObjectDelete((AtObject)pwList);
        return cAtFalse;
        }

    for (i = 0; i < numPws; i++)
        {
        AtSdhLine line;
        pwToh = (AtPwToh)AtListObjectGet(pwList, i);
        if (AtPwTypeGet((AtPw)pwToh) != cAtPwTypeToh)
            {
            AtPrintc(cSevWarning, "WARNING: %s is not TOH\r\n", CliChannelIdStringGet((AtChannel)pwToh));
            continue;
            }

        line = (AtSdhLine)AtPwBoundCircuitGet((AtPw)pwToh);
        TableColumnHeaderSet(tabPtr, 0, (char*)CliChannelIdStringGet((AtChannel)pwToh));

        numSts = (AtSdhLineRateGet(line) == cAtSdhLineRateStm1) ? 3 : 12;
        rowIndex = 0;
        for (j = 0; j < numSts; j++)
            {
            AtSprintf(strBuf, "sts.%u", j + 1);
            StrToCell(tabPtr, rowIndex,   0, strBuf);
            StrToCell(tabPtr, rowIndex++, 1, StsOhBytesToString(pwToh, (uint8)j));
            }

        AtPrintc(cSevInfo, "\r\nPW: %u\r\n", AtChannelIdGet((AtChannel)pwToh));
        SubTablePrint(tabPtr, 0, numSts-1, 0, 1);
        }

    TableFree(tabPtr);
    AtObjectDelete((AtObject)pwList);

    return cAtTrue;
    }

static eAtRet TohByteRowColGet(eAtSdhLineOverheadByte ohByte, uint8 sts1, uint8 *row, uint8 *col, uint8 numStsInLine)
    {
    uint8 row_i, col_i;

    for (row_i = 0; row_i < 9; row_i++)
        {
        for (col_i = 0; col_i < 3; col_i++)
            {
            if (ohByte != (uint32)(m_TohTable[row_i][col_i]))
                continue;

            *row = row_i;
            *col = (uint8)(col_i * numStsInLine + sts1);
            return cAtOk;
            }
        }

    *row = 0xFF;
    *col = 0xFF;

    return cAtError;
    }

eBool CmdPwTohBytesTableShow(char argc, char **argv)
    {
    AtList       pwList;
    tTab         *tabPtr;
    uint32       numPws, pw_i, sts_i, bit_i, col_i, row_i;
    uint32       numSts;
    AtPwToh      pwToh;
    const char   *heading[] = {"  ", "1","2","3","4","5","6","7","8","9","10",
                               "11","12","13","14","15","16","17","18","19","20",
                               "21","22","23","24","25","26","27","28","29","30",
                               "31","32","33","34","35","36"};
    uint8       row, col;
	AtUnused(argc);

    pwList = CliPwListFromString(argv[0]);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    numPws = AtListLengthGet(pwList);

    /* Allocate memory */
    tabPtr = TableAlloc(10, 36 + 1, heading); /* plus one col for row number */
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        AtObjectDelete((AtObject)pwList);
        return cAtFalse;
        }

    for (pw_i = 0; pw_i < numPws; pw_i++)
        {
        AtSdhLine line;
        eAtSdhLineOverheadByte ohByte;
        char strBuf[16];
        char temp[4];
        uint32 tohBitmap;

        pwToh = (AtPwToh)AtListObjectGet(pwList, pw_i);
        if (AtPwTypeGet((AtPw)pwToh) != cAtPwTypeToh)
            {
            AtPrintc(cSevWarning, "WARNING: %s is not TOH\r\n", CliChannelIdStringGet((AtChannel)pwToh));
            continue;
            }

        line = (AtSdhLine)AtPwBoundCircuitGet((AtPw)pwToh);
        numSts = AtSdhChannelNumSts((AtSdhChannel)line);

        for (col_i = 0; col_i < 36; col_i++)
            {
            AtSprintf(strBuf, "%u", (col_i % numSts) + 1);
            TableColumnHeaderSet(tabPtr, col_i + 1, strBuf);

            AtSprintf(strBuf, "%3u", col_i / numSts);
            ColorStrToCell(tabPtr, 0, col_i + 1, strBuf, cSevDebug);
            }

        for (row_i = 0; row_i < 9; row_i++)
            {
            AtSprintf(strBuf, "%u", row_i);
            ColorStrToCell(tabPtr, row_i + 1, 0, strBuf, cSevDebug);
            }

        for (sts_i = 0; sts_i < numSts; sts_i++)
            {
            AtPwTohEnabledBytesByStsGet(pwToh, (uint8)sts_i, &tohBitmap);
            if (!tohBitmap)
                continue;

            for (bit_i = 0; bit_i < 32; bit_i++)
                {
                eBool convertSuccess;

                if ((tohBitmap & (cBit0 << bit_i)) == 0)
                    continue;

                ohByte = cBit0 << bit_i;
                mAtEnumToStr(cAtTohStr, cAtTohVal, ohByte, temp, convertSuccess);
				if (convertSuccess)
                	AtSprintf(strBuf, "%3s", temp);
                else
                	AtSprintf(strBuf, "xxx");

                TohByteRowColGet(ohByte, (uint8)sts_i, &row, &col, (uint8)numSts);
                ColorStrToCell(tabPtr, (uint32)(row + 1), (uint32)(col + 1), strBuf, cSevInfo);
                }
            }

        AtPrintc(cSevInfo, "\r\nPW: %u\r\n", AtChannelIdGet((AtChannel)pwToh) + 1);
        SubTablePrint(tabPtr, 0, 9, 0, numSts * 3);
        }

    TableFree(tabPtr);
    AtObjectDelete((AtObject)pwList);

    return cAtTrue;
    }

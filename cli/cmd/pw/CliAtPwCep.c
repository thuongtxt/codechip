/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : CliAtPwCep.c
 *
 * Created Date: Mar 18, 2013
 *
 * Description : CEP CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtModulePw.h"
#include "AtModulePw.h"
#include "AtPwCep.h"
#include "AtDevice.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef eAtRet (*EnableFunc)(AtPwCep, eBool);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool AutoCwBitEnabling(char argc, char **argv, eBool enable, EnableFunc enableFunc)
    {
    AtPw        pw;
    uint16      i;
    uint32      *idBuf;
    uint32      bufferSize;
    uint32      numPws;
    AtModulePw  pwModule;
    eAtRet      ret = cAtOk;
	AtUnused(argc);

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numPws = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    pwModule = (AtModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);

    for (i = 0; i < numPws; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        pw = AtModulePwGetPw(pwModule, (uint16)(idBuf[i] - 1));
        if (pw == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: PW %u does not exist\r\n", idBuf[i]);
            continue;
            }

        if (AtPwTypeGet(pw) != cAtPwTypeCEP)
            {
            AtPrintc(cSevWarning, "WARNING: PW %u is not CEP\r\n", idBuf[i]);
            continue;
            }

        ret = enableFunc((AtPwCep)pw, enable);
        if (ret != cAtOk)
            AtPrintc(cSevCritical, "ERROR: Can not configure for %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)pw),
                     AtRet2String(ret));
        }

    return (ret != cAtOk) ? cAtFalse : cAtTrue;
    }

static uint32 NumberOfRowForEquippedChannelsGet(AtList pwList)
    {
    uint32 numRow = 0;
    AtIterator iterator = AtListIteratorCreate(pwList);
    AtPw pw;
    uint8 numEquipped;

    while ((pw = (AtPw)AtIteratorNext(iterator)) != NULL)
        {
        if (AtPwTypeGet(pw) != cAtPwTypeCEP)
            continue;

        if (AtPwCepModeGet((AtPwCep)pw) != cAtPwCepModeFractional)
            continue;

        numEquipped = AtPwCepNumEquippedChannelsGet((AtPwCep)pw);
        if (numRow < numEquipped)
            numRow = numEquipped;
        }

    AtObjectDelete((AtObject)iterator);
    return numRow;
    }

static eBool Equip(char argc, char **argv, eBool equip)
    {
    AtPw        pw;
    uint16      i;
    AtChannel   *attachCircuitList;
    uint32      bufferSize;
    uint32      numCircuits;
    AtModulePw  pwModule;
    eBool       success = cAtTrue;
    uint32      pwId;
	AtUnused(argc);

    /* Get the shared Channel buffer */
    attachCircuitList = CliSharedChannelListGet(&bufferSize);
    numCircuits = CliChannelListFromString(argv[1], attachCircuitList, bufferSize);
    if (numCircuits == 0)
        {
        AtPrintc(cSevCritical, "No circuit to bind.\r\n");
        return cAtTrue;
        }

    pwId = AtStrToDw(argv[0]);
    pwModule = (AtModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);

    pw = AtModulePwGetPw(pwModule, (uint16)(pwId - 1));
    if (pw == NULL)
        {
        AtPrintc(cSevWarning, "ERROR: Pw not exist %u\r\n", pwId);
        return cAtFalse;
        }

    for (i = 0; i < numCircuits; i++)
        {
        eAtRet ret = AtPwCepEquip((AtPwCep)pw, attachCircuitList[i], equip);

        /* Let user know if any error */
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Can not equip/un-equip circuit %s for PW %s, ret = %s\r\n",
                     CliChannelIdStringGet(attachCircuitList[i]),
                     CliChannelIdStringGet((AtChannel)pw),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdPwCepAutoNBitEn(char argc, char **argv)
    {
    return AutoCwBitEnabling(argc, argv, cAtTrue, (EnableFunc)AtPwCepCwAutoNBitEnable);
    }

eBool CmdPwCepAutoNBitDis(char argc, char **argv)
    {
    return AutoCwBitEnabling(argc, argv, cAtFalse, (EnableFunc)AtPwCepCwAutoNBitEnable);
    }

eBool CmdPwCepAutoPBitEn(char argc, char **argv)
    {
    return AutoCwBitEnabling(argc, argv, cAtTrue, (EnableFunc)AtPwCepCwAutoPBitEnable);
    }

eBool CmdPwCepAutoPBitDis(char argc, char **argv)
    {
    return AutoCwBitEnabling(argc, argv, cAtFalse, (EnableFunc)AtPwCepCwAutoPBitEnable);
    }

eBool CmdPwCepEparEn(char argc, char **argv)
    {
    return AutoCwBitEnabling(argc, argv, cAtTrue, (EnableFunc)AtPwCepEparEnable);
    }

eBool CmdPwCepEparDis(char argc, char **argv)
    {
    return AutoCwBitEnabling(argc, argv, cAtFalse, (EnableFunc)AtPwCepEparEnable);
    }

eBool CmdPwCepEbmEn(char argc, char **argv)
    {
    return AutoCwBitEnabling(argc, argv, cAtTrue, (EnableFunc)AtPwCepCwEbmEnable);
    }

eBool CmdPwCepEbmDis(char argc, char **argv)
    {
    return AutoCwBitEnabling(argc, argv, cAtFalse, (EnableFunc)AtPwCepCwEbmEnable);
    }

eBool CmdPwCepEquip(char argc, char **argv)
    {
    return Equip(argc, argv, cAtTrue);
    }

eBool CmdPwCepUnEquip(char argc, char **argv)
    {
    return Equip(argc, argv, cAtFalse);
    }

static void HeadingFree(char** heading, uint32 numItem)
    {
    uint32 i;
    for (i = 0; i < numItem; i++)
        AtOsalMemFree(heading[i]);

    AtOsalMemFree(heading);
    }

eBool CmdPwCepEquippedChannelsGet(char argc, char** argv)
    {
    AtList        pwList;
    tTab          *tabPtr;
    char          **heading;
    uint32        numPws, i, numRow, rowIndex;
    AtIterator    chnIterator;
	AtUnused(argc);

    pwList = CliPwListFromString(argv[0]);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    numPws = AtListLengthGet(pwList);

    /* Allocate memory */
    heading = (void*)AtOsalMemAlloc(sizeof(char*) * numPws);
    if (heading == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate memory for table heading\r\n");
        return cAtFalse;
        }
    for (i = 0; i < numPws; i++)
        {
        heading[i] = (char*)AtOsalMemAlloc(sizeof(char) * 32);
        if (heading[i] == NULL)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot allocate memory for table heading\r\n");
            HeadingFree((char**)heading, numPws);
            return cAtFalse;
            }
        }

    /* Create table with titles */
    for (i = 0; i < numPws; i++)
        AtSprintf((void*)heading[i], "PW %u", AtChannelIdGet((AtChannel)AtListObjectGet(pwList, i)) + 1);

    numRow = NumberOfRowForEquippedChannelsGet(pwList);
    tabPtr = TableAlloc(numRow, numPws, (void *)heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        AtObjectDelete((AtObject)pwList);
        return cAtFalse;
        }

    for (i = 0; i < numPws; i++)
        {
        AtPw pw;
        AtChannel channel;
        rowIndex = 0;

        pw = (AtPw)AtListObjectGet(pwList, i);
        if (AtPwTypeGet(pw) != cAtPwTypeCEP)
            {
            AtPrintc(cSevWarning, "WARNING: %s is not CEP\r\n", CliChannelIdStringGet((AtChannel)pw));
            continue;
            }

        chnIterator = AtPwCepEquippedChannelsIteratorCreate((AtPwCep)pw);
        while ((channel = (AtChannel)AtIteratorNext(chnIterator)) != NULL)
            StrToCell(tabPtr, rowIndex++, i, (char*)CliChannelIdStringGet(channel));

        AtObjectDelete((AtObject)chnIterator);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtObjectDelete((AtObject)pwList);

    /* Free the heading */
    for (i = 0; i < numPws; i++)
        AtOsalMemFree(heading[i]);
    AtOsalMemFree(heading);

    return cAtTrue;
    }

# @group CliAtPwPsn "Packet switch network (PSN)"
# @ingroup CliAtModulePw "Pseudowire module"

3 pw mpls innerlabel CmdPwMplsInnerLabelSet 2 pwList=1 label=1.1.1
/*
    Syntax     : pw mpls innerlabel <pwList> <label>.<exp>.<ttl>
    Parameter  : pwList - List of PW IDs
                 label  - MPLS label. Format: <label>.<exp>.<ttl>
    Description: Set MPLS inner label
    Example    : pw mpls innerlabel 1 1.1.1
*/

3 pw mpls expectedlabel CmdPwMplsExpectedLabelSet 2 pwList=1 expectedlabel=1
/*
    Syntax     : pw mpls expectedlabel <pwList> <expectedlabel>
    Parameter  : pwList         - List of PW IDs
                 expectedlabel  - MPLS expected label
    Description: Set MPLS expected label
    Example    : pw mpls expectedlabel 1 1
*/

4 pw mpls outerlabel add CmdPwMplsOuterLabelAdd 2 pwList=1 label=1.1.1
/*
    Syntax     : pw mpls outerlabel add <pwList> <label> <exp> <ttl>
    Parameter  : pwList - List of PW IDs
                 label  - MPLS label. Format: <label>.<exp>.<ttl>
    Description: Add MPLS outer label
    Example    : pw mpls outerlabel add 1 1.1.1
*/

4 pw mpls outerlabel remove CmdPwMplsOuterLabelRemove 2 pwList=1 label=1.1.1
/*
    Syntax     : pw mpls outerlabel remove <pwList> <label>.<exp>.<ttl>
    Parameter  : pwList - List of PW IDs
                 label  - MPLS label. Format: <label>.<exp>.<ttl>
    Description: Remove MPLS outer label
    Example    : pw mpls outerlabel remove 1 1.1.1
*/

4 pw mef ecid transmit CmdPwMefTxEcIdSet 2 pwList=1 ecId=1
/*
    Syntax     : pw mef ecid transmit <pwList> <txEcId>
    Parameter  : pwList - List of PW IDs
                 ecId   - Tx ECID
    Description: Set TX ECID
    Example    : pw mef ecid transmit 1 1
*/

4 pw mef ecid expect CmdPwMefExpectedEcIdSet 2 pwList=1 ecId=1
/*
    Syntax     : pw mef rxecid <pwList> <rxEcId>
    Parameter  : pwList - List of PW IDs
                 ecId   - Expected ECID
    Description: Set expected ECID
    Example    : pw mef ecid expect 1 1
*/    

3 pw mef dmac CmdPwMefDestMacSet 2 pwList=1 dmac=c0.ca.c0.ca.c0.ca
/*
    Syntax     : pw mef dmac <pwList> <dmac>
    Parameter  : pwList - List of PW IDs
                 dmac   - Dest MAC address
    Description: Set dest MAC
    Example    : pw mef dmac 1 c0.ca.c0.ca.c0.ca
*/

3 pw mef smac CmdPwMefSourceMacSet 2 pwList=1 smac=c0.ca.c0.ca.c0.ca
/*
    Syntax     : pw mef smac <pwList> <smac>
    Parameter  : pwList - List of PW IDs
                 smac   - Source MAC address
    Description: Set source MAC
    Example    : pw mef smac 1 c0.ca.c0.ca.c0.ca
*/

3 pw mef expectedmac CmdPwMefExpectedMacSet 2 pwList=1 expectedmac=c0.ca.c0.ca.c0.ca
/*
    Syntax     : pw mef expectedmac <pwList> <expectedmac>
    Parameter  : pwList      - List of PW IDs
                 expectedmac - Expected MAC address
    Description: Set expected MAC
    Example    : pw mef expectedmac 1 c0.ca.c0.ca.c0.ca
*/

4 pw mef vlan add CmdPwMefVlanAdd 3 pwList=1 vlanType=0x8100 vlanTag=1.1.1
/*
    Syntax     : pw mef vlan add <pwList> <vlanType> <vlanTag>
    Parameter  : pwList   - List of PW IDs
                 vlanType - VLAN type
                 vlanTag  - VLAN tag
    Description: Add VLAN
    Example    : pw mef vlan add 1 0x8100 1.1.1
*/

4 pw mef vlan remove CmdPwMefVlanRemove 3 pwList=1 vlanType=0x8100 vlanTag=1.1.1
/*
    Syntax     : pw mef vlan remove <pwList> <vlanType> <vlanTag>
    Parameter  : pwList   - List of PW IDs
                 vlanType - VLAN type
                 vlanTag  - VLAN tag
    Description: remove VLAN
    Example    : pw mef vlan remove 1 0x8100 1.1.1
*/

3 pw udp source CmdPwUdpSourcePortSet 2 pwList=1 srcPort=1
/*
    Syntax     : pw udp source <pwList> <srcPort|unused>
    Parameter  : pwList  - List of PW IDs
                 srcPort - Source Port - PW label or unused (0x85e).
                 Note, if srcPort is used as PW label, the destPort must be set to unused (0x85e)
    Description: Set Source Port for UDP Header
    Example    : pw udp source 1 1
                 pw udp source 1 unused
*/  

3 pw udp expect CmdPwUdpExpectedPortSet 2 pwList=1 expectedPort=1
/*
    Syntax     : pw udp expect <pwList> <expectedPort>
    Parameter  : pwList       - List of PW IDs
                 expectedPort - Expected Port
    Description: Set Expected Port for UDP protocol
    Example    : pw udp expect 1 1
*/  

3 pw udp dest CmdPwUdpDestPortSet 2 pwList=1 destPort=1
/*
    Syntax     : pw udp dest <pwList> <destPort|unused>
    Parameter  : pwList   - List of PW IDs
                 destPort - Dest Port - PW label or unused (0x85e)
                 Note, if destPort is used as PW label, the srcPort must be set to unused (0x85e)
    Description: Set Dest Port UDP Header
    Example    : pw udp dest 1 1
                 pw udp dest 1 unused
*/  

3 pw ipv4 tos CmdPwIPv4TosSet 2 pwList=1 tos=1
/*
    Syntax     : pw ipv4 tos <pwList> <tos>
    Parameter  : pwList - List of PW IDs
                 tos    - Type of Service of IPv4 Header
    Description: Set Type of Service IPv4 Header
    Example    : pw ipv4 tos 1 1
*/ 

3 pw ipv4 ttl CmdPwIPv4TtlSet 2 pwList=1 ttl=1
/*
    Syntax     : pw ipv4 ttl <pwList> <ttl>
    Parameter  : pwList - List of PW IDs
                 ttl    - Value Time to live of IPv4 Header
    Description: Set value Time to live IPv4 Header
    Example    : pw ipv4 ttl 1 1
*/ 

3 pw ipv4 flag CmdPwIPv4FlagsSet 2 pwList=1 flags=1
/*
    Syntax     : pw ipv4 flag <pwList> <flags>
    Parameter  : pwList - List of PW IDs
                 flags  - Flags
    Description: Set value Flags IPv4 Header
    Example    : pw ipv4 flag 1 1
*/

3 pw ipv4 source CmdAtPwIpV4PsnSourceAddressSet 2 pwList=1 ipAddress
/*
    Syntax     : pw ipv4 source <pwList> <ipAddress>
    Parameter  : pwList    - List of PW IDs
                 ipAddress - Source IP Address
    Description: Set source IP address
    Example    : pw ipv4 source 1 172.33.43.44
*/ 

3 pw ipv4 dest CmdAtPwIpV4PsnDestAddressSet 2 pwList=1 ipAddress
/*
    Syntax     : pw ipv4 dest <pwList> <ipAddress>
    Parameter  : pwList    - List of PW IDs
                 ipAddress - Destination IP Address
    Description: Set destination IP address
    Example    : pw ipv4 dest 1 172.33.43.44
*/ 

3 pw ipv6 trafficclass CmdPwIPv6TrafficClassSet 2 pwList=1 trafficClass=1
/*
    Syntax     : pw ipv6 trafficclass <pwList> <trafficClass>
    Parameter  : pwList       - List of PW IDs
                 trafficClass - Traffic Class of IPv6 Header
    Description: Set Traffic Class for IPv6 Header
    Example    : pw ipv6 trafficclass 1 1
*/  

3 pw ipv6 flowlabel CmdPwIPv6FlowLabelSet 2 pwList=1 flowlabel=1
/*
    Syntax     : pw ipv6 flowlabel <pwList> <flowlabel>
    Parameter  : pwList    - List of PW IDs
                 flowlabel - Flow Label of IPv6 Header
    Description: Set Flow Label for IPv6 Header
    Example    : pw ipv6 flowlabel 1 1
*/  

3 pw ipv6 hoplimit CmdPwIPv6HopLimitSet 2 pwList=1 hoplimit=1
/*
    Syntax     : pw ipv6 hoplimit <pwList> <hoplimit>
    Parameter  : pwList   - List of PW IDs
                 hoplimit - Hop Limit of IPv6 Header
    Description: Set Hop Limit for IPv6 Header
    Example    : pw ipv6 hoplimit 1 1
*/ 

3 pw ipv6 source CmdAtPwIpV6PsnSourceAddressSet 2 pwList=1 ipAddress
/*
    Syntax     : pw ipv6 source <pwList> <ipAddress>
    Parameter  : pwList    - List of PW IDs
                 ipAddress - Source IP Address
    Description: Set source IP address
    Example    : pw ipv6 source 1 172.33.43.44
*/ 

3 pw ipv6 dest CmdAtPwIpV6PsnDestAddressSet 2 pwList=1 ipAddress
/*
    Syntax     : pw ipv6 dest <pwList> <ipAddress>
    Parameter  : pwList    - List of PW IDs
                 ipAddress - Destination IP Address
    Description: Set destination IP address
    Example    : pw ipv6 dest 1 172.33.43.44
*/ 

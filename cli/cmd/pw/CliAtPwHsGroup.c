/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : CliAtPwHwGroup.c
 *
 * Created Date: Mar 18, 2015
 *
 * Description : PW CLIs
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtModulePw.h"
#include "AtDevice.h"
#include "AtModulePw.h"
#include "AtPw.h"
#include "AtList.h"
#include "AtPwGroup.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static const char * cAtPwGroupLabelSetStr[] = { "invalid",
                                                "primary",
                                                "backup"
                                                };

static const eAtPwGroupLabelSet cAtPwGroupLabelSetVal[]  = {cAtPwGroupLabelSetUnknown,
                                                            cAtPwGroupLabelSetPrimary,
                                                            cAtPwGroupLabelSetBackup
                                                            };
/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool PwGroupLabelSetFromString(char *pLabelString, eAtPwGroupLabelSet *pLabel)
    {
    eBool convertSuccess = cAtFalse;
    eAtPwGroupLabelSet labelSet = cAtPwGroupLabelSetUnknown;

    mAtStrToEnum(cAtPwGroupLabelSetStr, cAtPwGroupLabelSetVal, pLabelString, labelSet, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid PwGroupLabelSet \r\n");
        return cAtFalse;
        }

    *pLabel = labelSet;
    return cAtTrue;
    }

uint32 PwGroupToPwIdList(AtPwGroup group, uint32 *pwIds, uint32 bufsize)
    {
    uint32 pw_i, numPw = AtPwGroupNumPws(group);

    /* Clear memory */
    AtOsalMemInit(pwIds, 0, sizeof(uint32)*bufsize);
    numPw = mMin(numPw, bufsize);

    for (pw_i = 0; pw_i < numPw; pw_i++)
        {
        AtPw pw = AtPwGroupPwAtIndex(group, pw_i);
        pwIds[pw_i] = AtChannelIdGet((AtChannel)pw) + 1;
        }

    return numPw;
    }

eBool CmdPwHsGroupCreate(char argc, char **argv)
    {
    return HelperPwCreate(argc, argv, (PwCreateFunc) AtModulePwHsGroupCreate, "PwHs Group");
    }

eBool CmdPwHsGroupDelete(char argc, char **argv)
    {
    return CliPwGroupDelete(argc, argv, (PwGroupDeleteFunc)AtModulePwHsGroupDelete, "PW HS Group");
    }

eBool CmdPwHsGroupEnable(char argc, char **argv)
    {
    AtUnused(argc);
     return CliPwGroupBoolAttributeSet(argv[0],
                                       cAtTrue,
                                       (AtPwGroup (*)(AtModulePw, uint32))AtModulePwHsGroupGet,
                                       (PwGroupAttributeSet)AtPwGroupEnable);
    }

eBool CmdPwHsGroupDisable(char argc, char **argv)
    {
    AtUnused(argc);
     return CliPwGroupBoolAttributeSet(argv[0],
                                       cAtFalse,
                                       (AtPwGroup (*)(AtModulePw, uint32))AtModulePwHsGroupGet,
                                       (PwGroupAttributeSet)AtPwGroupEnable);
    }


eBool CmdPwHsGroupAdd(char argc, char **argv)
    {
    return CliPwGroupResourceManager(argc, argv, AtModulePwHsGroupGet, AtPwGroupPwAdd);
    }

eBool CmdPwHsGroupRemove(char argc, char **argv)
    {
    return CliPwGroupResourceManager(argc, argv, AtModulePwHsGroupGet, AtPwGroupPwRemove);
    }

eBool CmdPwHsGroupTxLabelSetSelect(char argc, char **argv)
    {
    eAtPwGroupLabelSet label;
    AtUnused(argc);
    if (!PwGroupLabelSetFromString(argv[1], &label))
        return cAtFalse;

    return CliPwGroupBoolAttributeSet(argv[0], label, (PwGroupGetFunc)AtModulePwHsGroupGet, (PwGroupAttributeSet)AtPwGroupTxLabelSetSelect);
    }

eBool CmdPwHsGroupRxLabelSetSelect(char argc, char **argv)
    {
    eAtPwGroupLabelSet label;
    AtUnused(argc);
    if (!PwGroupLabelSetFromString(argv[1], &label))
        return cAtFalse;

    return CliPwGroupBoolAttributeSet(argv[0], label, (PwGroupGetFunc)AtModulePwHsGroupGet, (PwGroupAttributeSet)AtPwGroupRxLabelSetSelect);
    }

eBool CmdPwHsGroupShow(char argc, char **argv)
    {
    uint16          group_i;
    uint32          numPwGroups;
    AtList          pwGroupList;
    char            buf[30];
    char            *pwStringBuf;
    uint32          stringBufLen = 64000;
    tTab            *tabPtr;
    const char      *heading[] = {"GroupId", "Enable", "Pw List", "Tx label", "Rx label"};
    uint32 *idBuf, bufferSize;
    AtUnused(argc);

    /* Get the shared ID buffer */
    pwGroupList = CliPwGroupListFromString(argv[0], AtModulePwHsGroupGet);
    numPwGroups = AtListLengthGet(pwGroupList);
    if (numPwGroups == 0)
        {
        AtPrintc(cSevWarning, "WARNING: Invalid pwGroup with %s\r\n", argv[0]);
        AtObjectDelete((AtObject)pwGroupList);
        return cAtFalse;
        }

    /* Allocate Table */
    tabPtr = TableAlloc(numPwGroups, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        AtObjectDelete((AtObject)pwGroupList);
        return cAtFalse;
        }

    idBuf = CliSharedIdBufferGet(&bufferSize);

    /* Decorating Table */
    /* Allocate 64K bytes for pw ID string list */
    pwStringBuf = (char*)AtOsalMemAlloc(stringBufLen);
    if (!pwStringBuf)
        {
        AtPrintc(cSevCritical, "Cannot allocate buffer to store pw list\r\n");
        AtObjectDelete((AtObject)pwGroupList);
        return cAtFalse;
        }

    for (group_i = 0; group_i < numPwGroups; group_i = (uint16)AtCliNextIdWithSleep(group_i, AtCliLimitNumRestPwGet()))
        {
        uint32     numPw;
        eBool      blResult;
        uint32     column = 0;
        AtPwGroup  pwGroup = (AtPwGroup)AtListObjectGet(pwGroupList, group_i);

        StrToCell(tabPtr, group_i, column++, CliNumber2String(AtPwGroupIdGet(pwGroup) + 1, "%d"));

        ColorStrToCell(tabPtr, group_i, column++, "N/A", cSevInfo);

        numPw =  AtPwGroupNumPws(pwGroup);

        /* Clear memory */
        AtOsalMemInit(pwStringBuf, 0, stringBufLen);
        numPw = PwGroupToPwIdList(pwGroup, idBuf, bufferSize);
        if (numPw == 0)
            AtSprintf(pwStringBuf, "%s", "none");
        else
            IdDwordListToString(pwStringBuf, stringBufLen, idBuf, numPw);

        ColorStrToCell(tabPtr, group_i, column++,(char *)pwStringBuf, (numPw == 0) ? cSevCritical : cSevInfo);

        mAtEnumToStr(cAtPwGroupLabelSetStr, cAtPwGroupLabelSetVal, AtPwGroupTxSelectedLabelGet(pwGroup), buf, blResult);
        StrToCell(tabPtr, group_i, column++, (blResult == cAtTrue) ? buf : "error");

        mAtEnumToStr(cAtPwGroupLabelSetStr, cAtPwGroupLabelSetVal, AtPwGroupRxSelectedLabelGet(pwGroup), buf, blResult);
        StrToCell(tabPtr, group_i, column++, (blResult == cAtTrue) ? buf : "error");
        }

    AtObjectDelete((AtObject)pwGroupList);
    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtOsalMemFree(pwStringBuf);

    return cAtTrue;
    }

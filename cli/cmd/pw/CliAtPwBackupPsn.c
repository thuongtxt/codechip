/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : CliAtPwBackupPsn.c
 *
 * Created Date: MAY 08, 2015
 *
 * Description : PW PSN CLIs
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtModulePw.h"
#include "AtModulePw.h"
#include "AtPw.h"
#include "AtPwPsn.h"
#include "AtDevice.h"
#include "../eth/CliAtModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
static eBool MplsLabelSet(char argc, char **argv, PwPsnMplsLabelSet mplsLabelSetFunc)
    {
    return CliPwPsnMplsLabelSet(argc, argv, AtPwBackupPsnSet, AtPwBackupPsnGet, mplsLabelSetFunc);
    }

static eBool PsnScalarAttributeSet(char *pwIdList,
                                   eAtPwPsnType pwPsnType,
                                   uint32 value,
                                   ScalarAttributeSet attributeSet)
    {
    return CliPsnScalarAttributeSet(pwIdList, pwPsnType, value, AtPwBackupPsnSet, AtPwBackupPsnGet, attributeSet);
    }

static eBool IpAddressSet(char argc,
                          char **argv,
                          eAtPwPsnType pwPsnType,
                          ArrayAttributeSet AddressSetFunc)
    {
    return CliPwPsnIpAddressSet(argc, argv, pwPsnType, AtPwBackupPsnSet, AtPwBackupPsnGet, AddressSetFunc);
    }

eBool CmdPwBackupPsnSet(char argc, char **argv)
    {
    return CliPwBackupPsnSet(argc, argv, AtPwBackupPsnSet, AtPwBackupPsnGet);
    }

eBool CmdPwBackupEthHeaderSet(char argc, char **argv)
    {
    uint32 numPws;
    AtPw *pwList = CliPwArrayFromStringGet(argv[0], &numPws);

    AtUnused(argc);

    if (numPws == 0)
        {
        CliPwPrintHelpString();
        return cAtFalse;
        }

    return CliPwEthHeaderSet(pwList, numPws, argv[1], argv[2], argv[3], AtPwBackupEthHeaderSet);
    }

eBool CmdPwBackupMplsInnerLabelSet(char argc, char **argv)
    {
    return MplsLabelSet(argc, argv, (PwPsnMplsLabelSet)AtPwMplsPsnInnerLabelSet);
    }

eBool CmdPwBackupMplsOuterLabelAdd(char argc, char **argv)
    {
    return MplsLabelSet(argc, argv, (PwPsnMplsLabelSet)AtPwMplsPsnOuterLabelAdd);
    }

eBool CmdPwBackupMplsOuterLabelRemove(char argc, char **argv)
    {
    return MplsLabelSet(argc, argv, (PwPsnMplsLabelSet)AtPwMplsPsnOuterLabelRemove);
    }

eBool CmdPwBackupMplsExpectedLabelSet(char argc, char **argv)
    {
    AtUnused(argc);
    return PsnScalarAttributeSet(argv[0], cAtPwPsnTypeMpls, AtStrToDw(argv[1]), (eAtRet (*)(AtPwPsn, uint32))AtPwMplsPsnExpectedLabelSet);
    }

eBool CmdPwBackupMefTxEcIdSet(char argc, char **argv)
    {
    AtUnused(argc);
    return PsnScalarAttributeSet(argv[0], cAtPwPsnTypeMef, AtStrToDw(argv[1]), (eAtRet (*)(AtPwPsn, uint32))AtPwMefPsnTxEcIdSet);
    }

eBool CmdPwBackupMefExpectedEcIdSet(char argc, char **argv)
    {
    AtUnused(argc);
    return PsnScalarAttributeSet(argv[0], cAtPwPsnTypeMef, AtStrToDw(argv[1]), (eAtRet (*)(AtPwPsn, uint32))AtPwMefPsnExpectedEcIdSet);
    }

static eBool UdpPortIsValid(const char * udpPortString)
    {
    return (AtStrToDw(udpPortString) > cBit15_0) ? cAtFalse : cAtTrue;
    }

eBool CmdPwBackupUdpSourcePortSet(char argc, char **argv)
    {
    AtUnused(argc);
    if (UdpPortIsValid(argv[1]) == cAtFalse)
    	{
    	AtPrintc(cSevCritical, "ERROR: UDP source port is invalid, expect 16-bits number\r\n");
        return cAtFalse;
        }
        
    return PsnScalarAttributeSet(argv[0], cAtPwPsnTypeUdp, CliPwUdpPortGet(argv[1]), (eAtRet (*)(AtPwPsn, uint32))AtPwUdpPsnSourcePortSet);
    }

eBool CmdPwBackupUdpExpectedPortSet(char argc, char **argv)
    {
    AtUnused(argc);
    if (UdpPortIsValid(argv[1]) == cAtFalse)
    	{
    	AtPrintc(cSevCritical, "ERROR: UDP expected port is invalid, expect 16-bits number\r\n");
        return cAtFalse;
        }
        
    return PsnScalarAttributeSet(argv[0], cAtPwPsnTypeUdp, CliPwUdpPortGet(argv[1]), (eAtRet (*)(AtPwPsn, uint32))AtPwUdpPsnExpectedPortSet);
    }

eBool CmdPwBackupUdpDestPortSet(char argc, char **argv)
    {
    AtUnused(argc);
    if (UdpPortIsValid(argv[1]) == cAtFalse)
    	{
    	AtPrintc(cSevCritical, "ERROR: UDP destination port is invalid, expect 16-bits number\r\n");
        return cAtFalse;
        }
        
    return PsnScalarAttributeSet(argv[0], cAtPwPsnTypeUdp, CliPwUdpPortGet(argv[1]), (eAtRet (*)(AtPwPsn, uint32))AtPwUdpPsnDestPortSet);
    }

eBool CmdPwBackupIPv4TosSet(char argc, char **argv)
    {
    AtUnused(argc);
    return PsnScalarAttributeSet(argv[0], cAtPwPsnTypeIPv4, AtStrToDw(argv[1]), (eAtRet (*)(AtPwPsn, uint32))AtPwIpV4PsnTypeOfServiceSet);
    }

eBool CmdPwBackupIPv4TtlSet(char argc, char **argv)
    {
    AtUnused(argc);
    return PsnScalarAttributeSet(argv[0], cAtPwPsnTypeIPv4, AtStrToDw(argv[1]), (eAtRet (*)(AtPwPsn, uint32))AtPwIpV4PsnTimeToLiveSet);
    }

eBool CmdPwBackupIPv4FlagsSet(char argc, char **argv)
    {
    AtUnused(argc);
    return PsnScalarAttributeSet(argv[0], cAtPwPsnTypeIPv4, AtStrToDw(argv[1]), (eAtRet (*)(AtPwPsn, uint32))AtPwIpV4PsnFlagsSet);
    }

eBool CmdPwBackupIPv6TrafficClassSet(char argc, char **argv)
    {
    AtUnused(argc);
    return PsnScalarAttributeSet(argv[0], cAtPwPsnTypeIPv6, AtStrToDw(argv[1]), (eAtRet (*)(AtPwPsn, uint32))AtPwIpV6PsnTrafficClassSet);
    }

eBool CmdPwBackupIPv6FlowLabelSet(char argc, char **argv)
    {
    AtUnused(argc);
    return PsnScalarAttributeSet(argv[0], cAtPwPsnTypeIPv6, AtStrToDw(argv[1]), (eAtRet (*)(AtPwPsn, uint32))AtPwIpV6PsnFlowLabelSet);
    }

eBool CmdPwBackupIPv6HopLimitSet(char argc, char **argv)
    {
    AtUnused(argc);
    return PsnScalarAttributeSet(argv[0], cAtPwPsnTypeIPv6, AtStrToDw(argv[1]), (eAtRet (*)(AtPwPsn, uint32))AtPwIpV6PsnHopLimitSet);
    }

eBool CmdPwBackupIpV4PsnSourceAddressSet(char argc, char **argv)
    {
    return IpAddressSet(argc, argv, cAtPwPsnTypeIPv4, (ArrayAttributeSet)AtPwIpPsnSourceAddressSet);
    }

eBool CmdPwBackupIpV4PsnDestAddressSet(char argc, char **argv)
    {
    return IpAddressSet(argc, argv, cAtPwPsnTypeIPv4, (ArrayAttributeSet)AtPwIpPsnDestAddressSet);
    }

eBool CmdPwBackupIpV6PsnSourceAddressSet(char argc, char **argv)
    {
    return IpAddressSet(argc, argv, cAtPwPsnTypeIPv6, (ArrayAttributeSet)AtPwIpPsnSourceAddressSet);
    }

eBool CmdPwBackupIpV6PsnDestAddressSet(char argc, char **argv)
    {
    return IpAddressSet(argc, argv, cAtPwPsnTypeIPv6, (ArrayAttributeSet)AtPwIpPsnDestAddressSet);
    }

eBool CmdPwBackupMefVlanAdd(char argc, char **argv)
    {
    return CliPwPsnMefVlanSet(argc, argv, AtPwBackupPsnSet, AtPwBackupPsnGet, (PwPsnMefVlanSet)AtPwMefPsnVlanTagAdd);
    }

eBool CmdPwBackupMefVlanRemove(char argc, char **argv)
    {
    return CliPwPsnMefVlanSet(argc, argv, AtPwBackupPsnSet, AtPwBackupPsnGet, (PwPsnMefVlanSet)AtPwMefPsnVlanTagRemove);
    }

eBool CmdPwBackupMefDestMacSet(char argc, char **argv)
    {
    return CliPwPsnMefMacAddressSet(argc, argv, AtPwBackupPsnSet, AtPwBackupPsnGet, (ArrayAttributeSet)AtPwMefPsnDestMacSet);
    }

eBool CmdPwBackupMefSourceMacSet(char argc, char **argv)
    {
    return CliPwPsnMefMacAddressSet(argc, argv,  AtPwBackupPsnSet, AtPwBackupPsnGet, (ArrayAttributeSet)AtPwMefPsnSourceMacSet);
    }

eBool CmdPwBackupMefExpectedMacSet(char argc, char **argv)
    {
    return CliPwPsnMefMacAddressSet(argc, argv, AtPwBackupPsnSet, AtPwBackupPsnGet, (ArrayAttributeSet)AtPwMefPsnExpectedMacSet);
    }

eBool CmdPwBackupPsnIpv4Show(char argc, char **argv)
    {
    return CliPwPsnIpv4Show(argc, argv, AtPwBackupPsnGet);
    }

eBool CmdPwBackupPsnIpv6Show(char argc, char **argv)
    {
    return CliPwPsnIpv6Show(argc, argv, AtPwBackupPsnGet);
    }

eBool CmdPwBackupPsnMplsShow(char argc, char **argv)
    {
    return CliPwPsnMplsShow(argc, argv, AtPwBackupPsnGet);
    }

eBool CmdPwBackupPsnMefShow(char argc, char **argv)
    {
    return CliPwPsnMefShow(argc, argv, AtPwBackupPsnGet);
    }

eBool CmdPwBackupPsnUdpShow(char argc, char **argv)
    {
    return CliPwPsnUdpShow(argc, argv, AtPwBackupPsnGet);
    }

eBool CmdPwBackupPsnShow(char argc, char **argv)
    {
    return CliPwPsnShow(argc,
                        argv,
                        AtPwBackupPsnGet,
                        AtPwBackupEthDestMacGet,
                        AtPwBackupEthSrcMacGet,
                        AtPwBackupEthCVlanGet,
                        AtPwBackupEthSVlanGet,
                        AtPwEthCVlanTpidGet,
                        AtPwEthSVlanTpidGet);
    }

eBool CmdPwBackupEthSourceMacSet(char argc, char **argv)
    {
    AtList pwList = CliPwListFromString(argv[0]);
    
    AtUnused(argc);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    return CliPwEthMacSet(pwList, argv[1], AtPwBackupEthSrcMacSet);
    }

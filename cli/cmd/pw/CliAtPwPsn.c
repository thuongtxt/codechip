/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : CliAtPwPsn.c
 *
 * Created Date: Mar 18, 2013
 *
 * Description : PW PSN CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtModulePw.h"
#include "AtModulePw.h"
#include "AtPw.h"
#include "AtPwPsn.h"
#include "AtDevice.h"
#include "../eth/CliAtModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#define cIpAddressMaxLen 16
#define cMacAddressLen 6

/*--------------------------- Macros -----------------------------------------*/
#define mPutConfigToCell(value)                                                \
    AtSprintf(buf, "%u", value);                                               \
    StrToCell(tabPtr, i, column++, buf);

#define mPutHexValueToCell(value)                                              \
    AtSprintf(buf, "0x%x", value);                                             \
    StrToCell(tabPtr, i, column++, buf);

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char * cAtPsnTypeStr[] = {
                                 "mpls",
                                 "mef",
                                 "udp",
                                 "ipv4",
                                 "ipv6"
                                 };

static const eAtPwPsnType cAtPsnTypeVal[]  = {
                                              cAtPwPsnTypeMpls,
                                              cAtPwPsnTypeMef,
                                              cAtPwPsnTypeUdp,
                                              cAtPwPsnTypeIPv4,
                                              cAtPwPsnTypeIPv6
                                              };

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void FreeStringArray(char **pItemList, uint32 numItems)
    {
    uint32 itemIndex;

    if (pItemList == NULL)
        return;

    for (itemIndex = 0; itemIndex < numItems; itemIndex++)
        AtOsalMemFree(pItemList[itemIndex]);
    AtOsalMemFree(pItemList);
    }

static eBool MplsLabelFromString(const char *pMplsLabel, tAtPwMplsLabel *label)
    {
    char    **pItemList;
    uint32  numItems = 0;
    uint32  ttl;
    tAtPwMplsLabel *returnLabel = NULL;

    if (pMplsLabel == NULL)
        return cAtFalse;

    pItemList = StrTokenGet(pMplsLabel, cFormatCheck, &numItems);
    if (numItems != 3)
        {
        FreeStringArray(pItemList, (uint8)numItems);
        return cAtFalse;
        }

    ttl = (uint32)AtStrToDw(pItemList[2]);
    if (ttl > cBit7_0)
        {
        FreeStringArray(pItemList, (uint8)numItems);
        return cAtFalse;
        }

    returnLabel = AtPwMplsLabelMake((uint32)AtStrToDw(pItemList[0]), (uint8)AtStrToDw(pItemList[1]), (uint8)ttl, label);

    FreeStringArray(pItemList, (uint8)numItems);

    return returnLabel ? cAtTrue : cAtFalse;
    }

eBool CliPsnScalarAttributeSet(char *pwIdList,
                               eAtPwPsnType pwPsnType,
                               uint32 value,
                               PwPsnSet pwPsnSetFunc,
                               PwPsnGet pwPsnGetFunc,
                               ScalarAttributeSet attributeSet)
    {
    uint16 i;
    uint32 numPws;
    AtPwPsn pwPsn;
    AtPwPsn topPwPsn;
    eBool success = cAtTrue;
    AtPw *pwList;

    pwList = CliPwArrayFromStringGet(pwIdList, &numPws);
    if (numPws == 0)
        {
        CliPwPrintHelpString();
        return cAtFalse;
        }

    for (i = 0; i < numPws; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        eBool psnExist = cAtFalse;

        /* Search PSN */
        topPwPsn = pwPsnGetFunc(pwList[i]);
        topPwPsn = (AtPwPsn)AtObjectClone((AtObject)topPwPsn);
        pwPsn    = topPwPsn;
        while (pwPsn != NULL)
            {
            eAtRet ret = cAtOk;

            if (pwPsnType ==  AtPwPsnTypeGet(pwPsn))
                {
                ret |= attributeSet(pwPsn, value);
                if (ret == cAtOk)
                    ret |= pwPsnSetFunc(pwList[i], topPwPsn);

                psnExist = cAtTrue;
                if (ret != cAtOk)
                    {
                    AtPrintc(cSevCritical,
                             "ERROR: Cannot set PSN attribute for %s, ret = %s\r\n",
                             CliChannelIdStringGet((AtChannel)pwList[i]),
                             AtRet2String(ret));
                    success = cAtFalse;
                    }
                }

            pwPsn = AtPwPsnLowerPsnGet(pwPsn);
            }

        AtObjectDelete((AtObject)topPwPsn);
        /* PSN does not exist */
        if (!psnExist)
            {
            AtPrintc(cSevCritical, "ERROR: This PSN has not been set for %s\r\n", CliChannelIdStringGet((AtChannel)pwList[i]));
            success = cAtFalse;
            }
        }

    return success;
    }

static eBool PsnScalarAttributeSet(char *pwIdList,
                                   eAtPwPsnType pwPsnType,
                                   uint32 value,
                                   ScalarAttributeSet attributeSet)
    {
    return CliPsnScalarAttributeSet(pwIdList, pwPsnType, value, AtPwPsnSet, AtPwPsnGet, attributeSet);
    }

uint16 CliPwUdpPortGet(char *udpPortString)
    {
    static const uint16 cUnusedPort = 0x85e;

    if ((udpPortString == NULL) ||
        (AtStrcmp(udpPortString, "unused") == 0) ||
        (AtStrcmp(udpPortString, "unuse")  == 0) ||
        (AtStrcmp(udpPortString, "none")   == 0))
        return cUnusedPort;

    return (uint16)AtStrToDw(udpPortString);
    }

static eBool IpAddressLenIsValid(eAtPwPsnType ipVersion, uint32 len)
    {
    if ((ipVersion == cAtPwPsnTypeIPv4) && (len == 4))  return cAtTrue;
    if ((ipVersion == cAtPwPsnTypeIPv6) && (len == 16)) return cAtTrue;

    return cAtFalse;
    }

eBool CliPwPsnIpAddressSet(char argc,
                      char **argv,
                      eAtPwPsnType pwPsnType,
                      PwPsnSet pwPsnSetFunc,
                      PwPsnGet pwPsnGetFunc,
                      ArrayAttributeSet AddressSetFunc)
    {
    AtPw pw;
    uint16 i;
    uint32 *idBuf;
    uint32 bufferSize;
    uint32 numPws;
    AtModulePw pwModule;
    AtPwPsn pwPsn;
    AtPwPsn topPwPsn;
    eBool success = cAtTrue;
    uint8 address[cIpAddressMaxLen];
    uint32 numBytes;
	AtUnused(argc);

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numPws = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    /* Get IP */
    numBytes = AtString2Bytes(argv[1], address, cIpAddressMaxLen, 10);
    if (!IpAddressLenIsValid(pwPsnType, numBytes))
        {
        AtPrintc(cSevCritical, "ERROR: Invalid IP address\r\n");
        return cAtFalse;
        }

    /* Apply this IP for all input PWs */
    pwModule = (AtModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
    for (i = 0; i < numPws; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        eBool psnExist = cAtFalse;

        /* Ignore PW that has not been created */
        pw = AtModulePwGetPw(pwModule, (uint16)(idBuf[i] - 1));
        if (pw == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: Ignore not exist PW %u\r\n", idBuf[i]);
            continue;
            }

        /* Search PSN */
        topPwPsn = pwPsnGetFunc(pw);
        pwPsn    = topPwPsn;
        while (pwPsn != NULL)
            {
            eAtRet ret = cAtOk;

            if (pwPsnType == AtPwPsnTypeGet(pwPsn))
                {
                ret |= AddressSetFunc(pwPsn, address);
                ret |= pwPsnSetFunc(pw, topPwPsn);
                psnExist = cAtTrue;
                if (ret != cAtOk)
                    {
                    AtPrintc(cSevCritical,
                             "ERROR: Cannot set IP Address for %s, ret = %s\r\n",
                             CliChannelIdStringGet((AtChannel)pw),
                             AtRet2String(ret));
                    success = cAtFalse;
                    }
                }

            pwPsn = AtPwPsnLowerPsnGet(pwPsn);
            }

        /* PSN does not exist */
        if (!psnExist)
            {
            AtPrintc(cSevCritical, "ERROR: This IP PSN has not been set for %s\r\n", CliChannelIdStringGet((AtChannel)pw));
            success = cAtFalse;
            }
        }

    return success;
    }

static eBool IpAddressSet(char argc,
                          char **argv,
                          eAtPwPsnType pwPsnType,
                          ArrayAttributeSet AddressSetFunc)
    {
    return CliPwPsnIpAddressSet(argc, argv, pwPsnType, AtPwPsnSet, AtPwPsnGet, AddressSetFunc);
    }

static AtPwPsn PsnLayerGet(AtPwPsn psn, eAtPwPsnType psnType)
    {
    AtPwPsn currentPsn = psn;

    while (currentPsn != NULL)
        {
        if (AtPwPsnTypeGet(currentPsn) == psnType)
            return currentPsn;

        currentPsn = AtPwPsnLowerPsnGet(currentPsn);
        }

    return NULL;
    }

static AtList PwsWithPsnType(char *pwIdListString, eAtPwPsnType psnType)
    {
    AtList     validPws;
    uint32     numPws, i;
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
    AtPw       *pwList;

    pwList = CliPwArrayFromStringGet(pwIdListString, &numPws);
    if (numPws == 0)
        {
        CliPwPrintHelpString();
        return cAtFalse;
        }

    validPws = AtListCreate(AtModulePwMaxPwsGet(pwModule));
    if (validPws == NULL)
        return NULL;

    /* Put all valid PWs */
    for (i = 0; i < numPws; i++)
        {
        AtPw    pw;
        AtPwPsn pwPsn;

        pw       = pwList[i];
        pwPsn    = AtPwPsnGet(pw);
        pwPsn    = PsnLayerGet(pwPsn, psnType);
        if ((pw == NULL) || (pwPsn == NULL))
            continue;

        AtListObjectAdd(validPws, (AtObject)pw);
        }

    /* Return NULL if nothing in this list */
    if (AtListLengthGet(validPws) == 0)
        {
        AtObjectDelete((AtObject)validPws);
        return NULL;
        }

    return validPws;
    }

eBool CliPwPsnMplsLabelSet(char argc,
                      char **argv,
                      PwPsnSet pwPsnSetFunc,
                      PwPsnGet pwPsnGetFunc,
                      PwPsnMplsLabelSet mplsLabelSetFunc)
    {
    uint16 i;
    uint32 numPws;
    tAtPwMplsLabel label;
    eBool success = cAtTrue;
    AtPw *pwList;
	AtUnused(argc);

    pwList = CliPwArrayFromStringGet(argv[0], &numPws);
    if (numPws == 0)
        {
        CliPwPrintHelpString();
        return cAtFalse;
        }

    AtOsalMemInit(&label, 0, sizeof(tAtPwMplsLabel));
    if (!MplsLabelFromString(argv[1], &label))
        {
        AtPrintc(cSevCritical, "ERROR: Invalid MPLS label\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numPws; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        eBool mplsExist;
        AtPwPsn topPwPsn, pwPsn;

        /* Search MPLS layer */
        topPwPsn  = (AtPwPsn)AtObjectClone((AtObject)pwPsnGetFunc(pwList[i]));
        pwPsn     = topPwPsn;
        mplsExist = cAtFalse;
        while (pwPsn != NULL)
            {
            eAtRet ret;

            /* Apply input configuration */
            if (AtPwPsnTypeGet(pwPsn) == cAtPwPsnTypeMpls)
                {
                ret  = mplsLabelSetFunc((AtPwMplsPsn)pwPsn, &label);
                if (ret == cAtOk)
                    {
                    ret |= pwPsnSetFunc(pwList[i], topPwPsn);
                    mplsExist = cAtTrue;
                    }

                if (ret != cAtOk)
                    {
                    AtPrintc(cSevCritical,
                             "ERROR: Cannot set MPLS label for %s, ret = %s\r\n",
                             CliChannelIdStringGet((AtChannel)pwList[i]),
                             AtRet2String(ret));
                    success = cAtFalse;
                    }
                }

            pwPsn = AtPwPsnLowerPsnGet(pwPsn);
            }

        /* Make sure that new MPLS configuration is applied */
        if (!mplsExist)
            {
            AtPrintc(cSevCritical,
                     "ERROR: MPLS layer has not been set for %s\r\n",
                     CliChannelIdStringGet((AtChannel)pwList[i]));
            success = cAtFalse;
            }

        AtObjectDelete((AtObject)topPwPsn);
        }

    return success;
    }

static eBool MplsLabelSet(char argc, char **argv, PwPsnMplsLabelSet mplsLabelSetFunc)
    {
    return CliPwPsnMplsLabelSet(argc, argv, AtPwPsnSet, AtPwPsnGet, mplsLabelSetFunc);
    }

eBool CmdPwMplsInnerLabelSet(char argc, char **argv)
    {
    return MplsLabelSet(argc, argv, (PwPsnMplsLabelSet)AtPwMplsPsnInnerLabelSet);
    }

eBool CmdPwMplsOuterLabelAdd(char argc, char **argv)
    {
    return MplsLabelSet(argc, argv, (PwPsnMplsLabelSet)AtPwMplsPsnOuterLabelAdd);
    }

eBool CmdPwMplsOuterLabelRemove(char argc, char **argv)
    {
    return MplsLabelSet(argc, argv, (PwPsnMplsLabelSet)AtPwMplsPsnOuterLabelRemove);
    }

eBool CmdPwMplsExpectedLabelSet(char argc, char **argv)
    {
	AtUnused(argc);
    return PsnScalarAttributeSet(argv[0], cAtPwPsnTypeMpls, AtStrToDw(argv[1]), (eAtRet (*)(AtPwPsn, uint32))AtPwMplsPsnExpectedLabelSet);
    }

eBool CmdPwMefTxEcIdSet(char argc, char **argv)
    {
	AtUnused(argc);
    return PsnScalarAttributeSet(argv[0], cAtPwPsnTypeMef, AtStrToDw(argv[1]), (eAtRet (*)(AtPwPsn, uint32))AtPwMefPsnTxEcIdSet);
    }

eBool CmdPwMefExpectedEcIdSet(char argc, char **argv)
    {
	AtUnused(argc);
    return PsnScalarAttributeSet(argv[0], cAtPwPsnTypeMef, AtStrToDw(argv[1]), (eAtRet (*)(AtPwPsn, uint32))AtPwMefPsnExpectedEcIdSet);
    }

static eBool UdpPortIsValid(const char * udpPortString)
    {
    return (AtStrToDw(udpPortString) > cBit15_0) ? cAtFalse : cAtTrue;
    }

eBool CmdPwUdpSourcePortSet(char argc, char **argv)
    {
	AtUnused(argc);
	if (UdpPortIsValid(argv[1]) == cAtFalse)
	    {
	    AtPrintc(cSevCritical, "ERROR: UDP source port is invalid, expect 16-bits number\r\n");
	    return cAtFalse;
	    }

    return PsnScalarAttributeSet(argv[0], cAtPwPsnTypeUdp, CliPwUdpPortGet(argv[1]), (eAtRet (*)(AtPwPsn, uint32))AtPwUdpPsnSourcePortSet);
    }

eBool CmdPwUdpExpectedPortSet(char argc, char **argv)
    {
	AtUnused(argc);
    if (UdpPortIsValid(argv[1]) == cAtFalse)
        {
        AtPrintc(cSevCritical, "ERROR: UDP expected port is invalid, expect 16-bits number\r\n");
        return cAtFalse;
        }

    return PsnScalarAttributeSet(argv[0], cAtPwPsnTypeUdp, CliPwUdpPortGet(argv[1]), (eAtRet (*)(AtPwPsn, uint32))AtPwUdpPsnExpectedPortSet);
    }

eBool CmdPwUdpDestPortSet(char argc, char **argv)
    {
	AtUnused(argc);
    if (UdpPortIsValid(argv[1]) == cAtFalse)
        {
        AtPrintc(cSevCritical, "ERROR: UDP destination port is invalid, expect 16-bits number\r\n");
        return cAtFalse;
        }

    return PsnScalarAttributeSet(argv[0], cAtPwPsnTypeUdp, CliPwUdpPortGet(argv[1]), (eAtRet (*)(AtPwPsn, uint32))AtPwUdpPsnDestPortSet);
    }

eBool CmdPwIPv4TosSet(char argc, char **argv)
    {
	AtUnused(argc);
    return PsnScalarAttributeSet(argv[0], cAtPwPsnTypeIPv4, AtStrToDw(argv[1]), (eAtRet (*)(AtPwPsn, uint32))AtPwIpV4PsnTypeOfServiceSet);
    }

eBool CmdPwIPv4TtlSet(char argc, char **argv)
    {
	AtUnused(argc);
    return PsnScalarAttributeSet(argv[0], cAtPwPsnTypeIPv4, AtStrToDw(argv[1]), (eAtRet (*)(AtPwPsn, uint32))AtPwIpV4PsnTimeToLiveSet);
    }

eBool CmdPwIPv4FlagsSet(char argc, char **argv)
    {
	AtUnused(argc);
    return PsnScalarAttributeSet(argv[0], cAtPwPsnTypeIPv4, AtStrToDw(argv[1]), (eAtRet (*)(AtPwPsn, uint32))AtPwIpV4PsnFlagsSet);
    }
    
eBool CmdPwIPv6TrafficClassSet(char argc, char **argv)
    {
	AtUnused(argc);
    return PsnScalarAttributeSet(argv[0], cAtPwPsnTypeIPv6, AtStrToDw(argv[1]), (eAtRet (*)(AtPwPsn, uint32))AtPwIpV6PsnTrafficClassSet);
    }

eBool CmdPwIPv6FlowLabelSet(char argc, char **argv)
    {
	AtUnused(argc);
    return PsnScalarAttributeSet(argv[0], cAtPwPsnTypeIPv6, AtStrToDw(argv[1]), (eAtRet (*)(AtPwPsn, uint32))AtPwIpV6PsnFlowLabelSet);
    }

eBool CmdPwIPv6HopLimitSet(char argc, char **argv)
    {
	AtUnused(argc);
    return PsnScalarAttributeSet(argv[0], cAtPwPsnTypeIPv6, AtStrToDw(argv[1]), (eAtRet (*)(AtPwPsn, uint32))AtPwIpV6PsnHopLimitSet);
    }

eBool CmdAtPwIpV4PsnSourceAddressSet(char argc, char **argv)
    {
    return IpAddressSet(argc, argv, cAtPwPsnTypeIPv4, (ArrayAttributeSet)AtPwIpPsnSourceAddressSet);
    }

eBool CmdAtPwIpV4PsnDestAddressSet(char argc, char **argv)
    {
    return IpAddressSet(argc, argv, cAtPwPsnTypeIPv4, (ArrayAttributeSet)AtPwIpPsnDestAddressSet);
    }

eBool CmdAtPwIpV6PsnSourceAddressSet(char argc, char **argv)
    {
    return IpAddressSet(argc, argv, cAtPwPsnTypeIPv6, (ArrayAttributeSet)AtPwIpPsnSourceAddressSet);
    }

eBool CmdAtPwIpV6PsnDestAddressSet(char argc, char **argv)
    {
    return IpAddressSet(argc, argv, cAtPwPsnTypeIPv6, (ArrayAttributeSet)AtPwIpPsnDestAddressSet);
    }

eBool CliPwPsnIpv4Show(char argc, char **argv, AtPwPsn (*PsnGet)(AtPw self))
    {
    AtList        pwList;
    uint16        i;
    uint16        column;
    uint32        numPws;
    char          buf[30];
    uint8         ipv4AddrBuf[4];
    tTab          *tabPtr;
    const char    *heading[] = {"PW", "sourceAddress", "destAddress", "timeToLive", "typeOfService", "Flags"};
    AtUnused(argc);

    pwList = PwsWithPsnType(argv[0], cAtPwPsnTypeIPv4);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    numPws = AtListLengthGet(pwList);

    tabPtr = TableAlloc(numPws, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        AtObjectDelete((AtObject)pwList);
        return cAtFalse;
        }

    for (i = 0; i < numPws; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        AtPw          pw;
        AtPwPsn       psn;
        AtPwIpV4Psn   ipv4Psn;

        column = 0;
        pw = (AtPw)AtListObjectGet(pwList, i);

        StrToCell(tabPtr, i, column++, (char *)CliChannelIdStringGet((AtChannel)pw));

        /* Get the corresponding PSN layer and convert it to string */
        psn     = PsnGet(pw);
        ipv4Psn = (AtPwIpV4Psn)PsnLayerGet(psn, cAtPwPsnTypeIPv4);

        AtPwIpPsnSourceAddressGet((AtPwIpPsn)ipv4Psn, ipv4AddrBuf);
        StrToCell(tabPtr, i , column++, CliAtModuleEthIPv4AddrByte2Str(ipv4AddrBuf));

        AtPwIpPsnDestAddressGet((AtPwIpPsn)ipv4Psn, ipv4AddrBuf);
        StrToCell(tabPtr, i , column++, CliAtModuleEthIPv4AddrByte2Str(ipv4AddrBuf));

        mPutConfigToCell(AtPwIpV4PsnTimeToLiveGet(ipv4Psn));
        mPutHexValueToCell(AtPwIpV4PsnTypeOfServiceGet(ipv4Psn));
        mPutHexValueToCell(AtPwIpV4PsnFlagsGet(ipv4Psn));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtObjectDelete((AtObject)pwList);

    return cAtTrue;
    }

eBool CmdPwPsnIpv4Show(char argc, char **argv)
    {
    return CliPwPsnIpv4Show(argc, argv, AtPwPsnGet);
    }

eBool CliPwPsnIpv6Show(char argc, char **argv, AtPwPsn (*PsnGet)(AtPw self))
    {
    AtList        pwList;
    uint16        i;
    uint16        column;
    uint32        numPws;
    char          buf[30];
    uint8         ipv6AddrBuf[16];
    tTab          *tabPtr;
    const char    *heading[] = {"PW", "sourceAddress", "destAddress", "flowLabel", "hopLimit", "trafficClass"};
    AtUnused(argc);

    pwList = PwsWithPsnType(argv[0], cAtPwPsnTypeIPv6);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    numPws = AtListLengthGet(pwList);

    tabPtr = TableAlloc(numPws, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        AtObjectDelete((AtObject)pwList);
        return cAtFalse;
        }

    for (i = 0; i < numPws; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        AtPw          pw;
        AtPwPsn       psn;
        AtPwIpV6Psn   ipv6Psn;

        column = 0;
        pw = (AtPw)AtListObjectGet(pwList, i);

        StrToCell(tabPtr, i, column++, (char *)CliChannelIdStringGet((AtChannel)pw));

        /* Get the corresponding PSN layer and convert it to string */
        psn     = PsnGet(pw);
        ipv6Psn = (AtPwIpV6Psn)PsnLayerGet(psn, cAtPwPsnTypeIPv6);

        AtPwIpPsnSourceAddressGet((AtPwIpPsn)ipv6Psn, ipv6AddrBuf);
        StrToCell(tabPtr, i , column++, CliAtModuleEthIPv6AddrByte2Str(ipv6AddrBuf));

        AtPwIpPsnDestAddressGet((AtPwIpPsn)ipv6Psn, ipv6AddrBuf);
        StrToCell(tabPtr, i , column++, CliAtModuleEthIPv6AddrByte2Str(ipv6AddrBuf));

        mPutConfigToCell(AtPwIpV6PsnFlowLabelGet(ipv6Psn));
        mPutConfigToCell(AtPwIpV6PsnHopLimitGet(ipv6Psn));
        mPutHexValueToCell(AtPwIpV6PsnTrafficClassGet(ipv6Psn));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtObjectDelete((AtObject)pwList);

    return cAtTrue;
    }

eBool CmdPwPsnIpv6Show(char argc, char **argv)
    {
    return CliPwPsnIpv6Show(argc, argv, AtPwPsnGet);
    }

eBool CliPwPsnMplsShow(char argc, char **argv, AtPwPsn (*PsnGet)(AtPw self))
    {
    AtList         pwList;
    uint16         pw_i;
    uint8          label_i;
    uint16         column;
    uint32         numPws;
    tAtPwMplsLabel mplsLabel;
    static char    buf[30];
    static char    outerLabelsStringBuf[128];
    uint8          numMplsOuterLabel;
    tTab           *tabPtr;
    const char     *heading[] = {"PW", "expectedLabel", "innerLabel", "outerLabels"};
    AtUnused(argc);

    pwList = PwsWithPsnType(argv[0], cAtPwPsnTypeMpls);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    numPws = AtListLengthGet(pwList);

    tabPtr = TableAlloc(numPws, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        AtObjectDelete((AtObject)pwList);
        return cAtFalse;
        }

    for (pw_i = 0; pw_i < numPws; pw_i = (uint16)AtCliNextIdWithSleep(pw_i, AtCliLimitNumRestPwGet()))
        {
        AtPw           pw;
        AtPwPsn        psn;
        AtPwMplsPsn    mplsPsn;

        column = 0;
        pw = (AtPw)AtListObjectGet(pwList, pw_i);

        StrToCell(tabPtr, pw_i, column++, (char *)CliChannelIdStringGet((AtChannel)pw));

        psn     = PsnGet(pw);
        mplsPsn = (AtPwMplsPsn)PsnLayerGet(psn, cAtPwPsnTypeMpls);

        /* Add MPLS inner label string to cell */
        AtSprintf(buf, "%d", AtPwMplsPsnExpectedLabelGet(mplsPsn));
        StrToCell(tabPtr, pw_i, column++, buf);

        AtPwMplsPsnInnerLabelGet(mplsPsn, &mplsLabel);
        AtSprintf(buf, "%d.%d.%d", mplsLabel.label, mplsLabel.experimental, mplsLabel.timeToLive);
        StrToCell(tabPtr, pw_i, column++, buf);

        /* Add MPLS outer label string to cell */
        numMplsOuterLabel = AtPwMplsPsnNumberOfOuterLabelsGet(mplsPsn);

        if (numMplsOuterLabel == 0)
            {
            StrToCell(tabPtr, pw_i, column++, "none");
            continue;
            }

        AtOsalMemInit(outerLabelsStringBuf, 0, sizeof(outerLabelsStringBuf));
        for (label_i = 0; label_i < numMplsOuterLabel; label_i++)
            {
            tAtPwMplsLabel label;

            AtPwMplsPsnOuterLabelGetByIndex(mplsPsn, label_i, &label);
            AtStrcat(outerLabelsStringBuf, ",");
            AtSprintf(buf, "%d.%d.%d", label.label, label.experimental, label.timeToLive);
            AtStrcat(outerLabelsStringBuf, buf);
            }

        StrToCell(tabPtr, pw_i, column++, &outerLabelsStringBuf[1]);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtObjectDelete((AtObject)pwList);

    return cAtTrue;
    }

eBool CmdPwPsnMplsShow(char argc, char **argv)
    {
    return CliPwPsnMplsShow(argc, argv, AtPwPsnGet);
    }

static char *VlanTag2Str(const tAtEthVlanTag *vlanTag)
    {
    static char vlanTagBuf[16];
    AtSprintf(vlanTagBuf, "%d.%d.%d", vlanTag->priority, vlanTag->cfi, vlanTag->vlanId);

    return vlanTagBuf;
    }

eBool CliPwPsnMefShow(char argc, char **argv, AtPwPsn (*PsnGet)(AtPw self))
    {
    AtList        pwList;
    uint16        i;
    uint32        numPws;
    static char   buf[30];
    tTab          *tabPtr;
    const char    *heading[] = {"PW", "txEcId", "expectedEcId", "DMAC", "SMAC", "ExpectedMAC", "VLANs", "LowerPSN"};
	AtUnused(argc);

    pwList = PwsWithPsnType(argv[0], cAtPwPsnTypeMef);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    numPws = AtListLengthGet(pwList);

    tabPtr = TableAlloc(numPws, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        AtObjectDelete((AtObject)pwList);
        return cAtFalse;
        }

    for (i = 0; i < numPws; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
    	eAtRet        ret;
        eBool         blResult;
        AtPwPsn       psn;
        AtPwMefPsn    mefPsn;
        uint8         numVlans, vlanIndex;
        tAtEthVlanTag vlanTag;
        uint16        vlanType;
        uint16        column = 0;
        AtPw          pw = (AtPw)AtListObjectGet(pwList, i);

        StrToCell(tabPtr, i, column++, (char *)CliChannelIdStringGet((AtChannel)pw));

        /* Get the corresponding PSN layer and convert it to string */
        psn     = PsnGet(pw);
        mefPsn  = (AtPwMefPsn)PsnLayerGet(psn, cAtPwPsnTypeMef);

        mPutConfigToCell(AtPwMefPsnTxEcIdGet(mefPsn));
        mPutConfigToCell(AtPwMefPsnExpectedEcIdGet(mefPsn));

        AtOsalMemInit(buf, 0, sizeof(buf));
        ret = AtPwMefPsnDestMacGet(mefPsn, (void *)buf);
        StrToCell(tabPtr, i , column++, (ret == cAtOk) ? CliEthPortMacAddressByte2Str((void *)buf) : "N/A");

        AtOsalMemInit(buf, 0, sizeof(buf));
        ret = AtPwMefPsnSourceMacGet(mefPsn, (void *)buf);
        StrToCell(tabPtr, i , column++, (ret == cAtOk) ? CliEthPortMacAddressByte2Str((void *)buf) : "N/A");

        AtOsalMemInit(buf, 0, sizeof(buf));
        ret = AtPwMefPsnExpectedMacGet(mefPsn, (void *)buf);
        StrToCell(tabPtr, i , column++, (ret == cAtOk) ? CliEthPortMacAddressByte2Str((void *)buf) : "N/A");

        if (ret != cAtOk)
            StrToCell(tabPtr, i , column++, "N/A");
        else
            {
            numVlans = AtPwMefPsnNumVlanTags(mefPsn);
            if (numVlans > 0)
                {
                char *vlanStrBuffer = AtOsalMemAlloc(numVlans * 32UL);
                AtOsalMemInit(vlanStrBuffer, 0, numVlans * 32UL);
                for (vlanIndex = 0; vlanIndex < numVlans; vlanIndex++)
                    {
                    AtPwMefPsnVlanTagAtIndex(mefPsn, vlanIndex, &vlanType, &vlanTag);

                    AtSprintf(buf, " - %x|%s", vlanType, VlanTag2Str(&vlanTag));
                    AtStrcat(vlanStrBuffer, buf);
                    }
                StrToCell(tabPtr, i, column++, &vlanStrBuffer[3]);
                AtOsalMemFree(vlanStrBuffer);
                }
            else
                StrToCell(tabPtr, i, column++, "none");

            if (AtPwPsnLowerPsnGet((AtPwPsn)mefPsn))
                {
                mAtEnumToStr(cAtPsnTypeStr, cAtPsnTypeVal, AtPwPsnTypeGet(AtPwPsnLowerPsnGet((AtPwPsn)mefPsn)), buf, blResult);
                StrToCell(tabPtr, i, column++, (blResult == cAtTrue) ? buf : "error");
                }
            else
                StrToCell(tabPtr, i, column++, "none");
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtObjectDelete((AtObject)pwList);

    return cAtTrue;
    }

eBool CmdPwPsnMefShow(char argc, char **argv)
    {
    return CliPwPsnMefShow(argc, argv, AtPwPsnGet);
    }
 
eBool CliPwPsnUdpShow(char argc, char **argv, AtPwPsn (*PsnGet)(AtPw self))
    {
    AtList        pwList;
    uint16        i;
    uint16        column;
    uint32        numPws;
    static char   buf[30];
    tTab          *tabPtr;
    const char    *heading[] = {"PW", "expectedPort", "sourcePort", "destPort"};
    AtUnused(argc);

    pwList = PwsWithPsnType(argv[0], cAtPwPsnTypeUdp);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    numPws = AtListLengthGet(pwList);

    tabPtr = TableAlloc(numPws, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        AtObjectDelete((AtObject)pwList);
        return cAtFalse;
        }

    for (i = 0; i < numPws; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        AtPw        pw;
        AtPwPsn     psn;
        AtPwUdpPsn  udpPsn;

        column = 0;
        pw = (AtPw)AtListObjectGet(pwList, i);

        StrToCell(tabPtr, i, column++, (char *)CliChannelIdStringGet((AtChannel)pw));

        /* Get the corresponding PSN layer and convert it to string */
        psn     = PsnGet(pw);
        udpPsn  = (AtPwUdpPsn)PsnLayerGet(psn, cAtPwPsnTypeUdp);

        mPutConfigToCell(AtPwUdpPsnExpectedPortGet(udpPsn));
        mPutConfigToCell(AtPwUdpPsnSourcePortGet(udpPsn));
        mPutConfigToCell(AtPwUdpPsnDestPortGet(udpPsn));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtObjectDelete((AtObject)pwList);

    return cAtTrue;
    }

eBool CmdPwPsnUdpShow(char argc, char **argv)
    {
    return CliPwPsnUdpShow(argc, argv, AtPwPsnGet);
    }

eBool CliPwPsnMefMacAddressSet(char argc,
                    char **argv,
                    PwPsnSet pwPsnSetFunc,
                    PwPsnGet pwPsnGetFunc,
                    ArrayAttributeSet AddressSetFunc)
    {
    AtList pwList;
    uint16 i;
    uint32 numPws;
    AtPwPsn pwPsn;
    AtPwPsn topPwPsn;
    eBool success = cAtTrue;
    uint8 macAddress[cMacAddressLen];
    uint32 numBytes;
    AtUnused(argc);

    pwList = PwsWithPsnType(argv[0], cAtPwPsnTypeMef);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    numPws = AtListLengthGet(pwList);
    numBytes = AtString2Bytes(argv[1], macAddress, cMacAddressLen, 16);
    if (numBytes != 6)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid MAC address\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numPws; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        AtPw pw = (AtPw)AtListObjectGet(pwList, i);

        /* Search PSN */
        topPwPsn = pwPsnGetFunc(pw);
        pwPsn    = topPwPsn;
        while (pwPsn != NULL)
            {
            eAtRet ret = cAtOk;

            if (AtPwPsnTypeGet(pwPsn) == cAtPwPsnTypeMef)
                {
                ret |= AddressSetFunc(pwPsn, macAddress);
                ret |= pwPsnSetFunc(pw, topPwPsn);
                if (ret != cAtOk)
                    {
                    AtPrintc(cSevCritical,
                             "ERROR: Cannot set IP Address for %s, ret = %s\r\n",
                             CliChannelIdStringGet((AtChannel)pw),
                             AtRet2String(ret));
                    success = cAtFalse;
                    }
                }

            pwPsn = AtPwPsnLowerPsnGet(pwPsn);
            }
        }

    AtObjectDelete((AtObject)pwList);
    return success;
    }

eBool CmdPwMefDestMacSet(char argc, char **argv)
    {
    return CliPwPsnMefMacAddressSet(argc, argv, AtPwPsnSet, AtPwPsnGet, (ArrayAttributeSet)AtPwMefPsnDestMacSet);
    }

eBool CmdPwMefSourceMacSet(char argc, char **argv)
    {
    return CliPwPsnMefMacAddressSet(argc, argv,  AtPwPsnSet, AtPwPsnGet, (ArrayAttributeSet)AtPwMefPsnSourceMacSet);
    }

eBool CmdPwMefExpectedMacSet(char argc, char **argv)
    {
    return CliPwPsnMefMacAddressSet(argc, argv, AtPwPsnSet, AtPwPsnGet, (ArrayAttributeSet)AtPwMefPsnExpectedMacSet);
    }

static eBool VlanGet(char *pStrVlan, tAtEthVlanTag *vlanTag)
    {
    uint32 vlanBuf[10];
    uint32 numItem;

    if (pStrVlan != NULL)
        {
        static char minFormat[] = "0.0.0";
        static char maxFormat[] = "7.1.4095";

        if (!AtOsalMemCmp(pStrVlan, "none", 4))
            return cAtTrue;

        IdComplexNumParse(pStrVlan,
                          minFormat,
                          maxFormat,
                          10,
                          vlanBuf,
                          &numItem);
        if (numItem == 3)
            AtEthVlanTagConstruct((uint8)vlanBuf[0], (uint8)vlanBuf[1], (uint16)vlanBuf[2], vlanTag);
        else
            return cAtFalse;
        }

    return cAtTrue;
    }

eBool CliPwPsnMefVlanSet(char argc,
              char **argv,
              PwPsnSet pwPsnSetFunc,
              PwPsnGet pwPsnGetFunc,
              PwPsnMefVlanSet VlanSetFunc)
    {
    AtList pwList;
    uint16 i;
    uint32 numPws;
    AtPwPsn topPwPsn;
    eBool success = cAtTrue;
    tAtEthVlanTag vlanTag;
    uint16 vlanType;
    AtUnused(argc);

    pwList = PwsWithPsnType(argv[0], cAtPwPsnTypeMef);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }
    numPws = AtListLengthGet(pwList);

    vlanType = (uint16)AtStrToDw(argv[1]);
    if (VlanGet(argv[2], &vlanTag) == cAtFalse)
		{
		AtPrintc(cSevCritical, "Invalid VLAN, expected %s\r\n", CliExpectedVlanFormat());
		return cAtFalse;
		}

    for (i = 0; i < numPws; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        AtPw pw = (AtPw)AtListObjectGet(pwList, i);
        AtPwPsn pwPsn;

        /* Search PSN */
        topPwPsn = pwPsnGetFunc(pw);
        pwPsn    = topPwPsn;
        while (pwPsn != NULL)
            {
            eAtRet ret = cAtOk;

            if (AtPwPsnTypeGet(pwPsn) == cAtPwPsnTypeMef)
                {
                ret |= VlanSetFunc((AtPwMefPsn)pwPsn, vlanType, &vlanTag);
                ret |= pwPsnSetFunc(pw, topPwPsn);
                if (ret != cAtOk)
                    {
                    AtPrintc(cSevCritical,
                             "ERROR: Cannot set IP Address for %s, ret = %s\r\n",
                             CliChannelIdStringGet((AtChannel)pw),
                             AtRet2String(ret));
                    success = cAtFalse;
                    }
                }

            pwPsn = AtPwPsnLowerPsnGet(pwPsn);
            }
        }

    AtObjectDelete((AtObject)pwList);
    return success;
    }

eBool CmdPwMefVlanAdd(char argc, char **argv)
	{
	return CliPwPsnMefVlanSet(argc, argv, AtPwPsnSet, AtPwPsnGet, (PwPsnMefVlanSet)AtPwMefPsnVlanTagAdd);
	}

eBool CmdPwMefVlanRemove(char argc, char **argv)
	{
	return CliPwPsnMefVlanSet(argc, argv, AtPwPsnSet, AtPwPsnGet, (PwPsnMefVlanSet)AtPwMefPsnVlanTagRemove);
	}

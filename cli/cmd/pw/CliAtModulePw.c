/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Pseudowire
 *
 * File        : CliAtModulePw.c
 *
 * Created Date: Nov 19, 2012
 *
 * Description : PW module CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtModulePw.h"
#include "../framerelay/CliAtFr.h"
#include "AtModulePw.h"
#include "AtDevice.h"
#include "../encap/CliAtModuleEncap.h"
#include "../ppp/CliAtModulePpp.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char * cAtPwAtmPwStr[] = {
                                     "vcc1to1",
                                     "vpc1to1",
                                     "nto1"
                                     };

static const eAtAtmPwType cAtPwAtmPwType[]  = {
                                              cAtAtmPwTypeVcc1To1,
                                              cAtAtmPwTypeVpc1To1,
                                              cAtAtmPwTypeNTo1
                                              };

static const char * cAtPwDcrClockSourceStr[] = {
                                               "prc",
                                               "system",
                                               "ext1",
                                               "ext2"
                                              };

static const eAtPwDcrClockSource cAtPwDcrClockSourceVal[] =
                                              {
                                              cAtPwDcrClockSourcePrc,
                                              cAtPwDcrClockSourceSystem,
                                              cAtPwDcrClockSourceExt1,
                                              cAtPwDcrClockSourceExt2
                                              };

static const char * cAtPwCESoPModeStr[] = {"basic", "cas"};
static const eAtPwCESoPMode cAtPwCESoPModeVal[] = {cAtPwCESoPModeBasic, cAtPwCESoPModeWithCas};

static const char * cAtPwCepModeStr[] = {"basic", "fractional"};
static const eAtPwCepMode cAtPwCepModeVal[] = {cAtPwCepModeBasic, cAtPwCepModeFractional};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eBool HelperPwCreate(char argc, char **argv, PwCreateFunc pwCreateFunc, const char *name)
    {
    uint16       pw_i;
    uint32      *idBuf;
    uint32      bufferSize;
    uint32      numPws;
    AtModulePw  pwModule;
	AtUnused(argc);

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numPws = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid %s list, expected: 1 or 1-252, ...\n", name);
        return cAtFalse;
        }

    pwModule = (AtModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);

    for (pw_i = 0; pw_i < numPws; pw_i = (uint16)AtCliNextIdWithSleep(pw_i, AtCliLimitNumRestPwGet()))
        {
        if (pwCreateFunc(pwModule, idBuf[pw_i] - 1) == NULL)
            AtPrintc(cSevCritical, "ERROR: %s %u cannot be created or it is existing\r\n", name, idBuf[pw_i]);
        }

    return cAtTrue;
    }

static AtPw CESoPBasicCreate(AtModulePw pw, uint32 pwId)
    {
    return (AtPw)AtModulePwCESoPCreate(pw, (uint16)pwId, cAtPwCESoPModeBasic);
    }

static AtPw CepBasicCreate(AtModulePw pw, uint32 pwId)
    {
    return (AtPw)AtModulePwCepCreate(pw, (uint16)pwId, cAtPwCepModeBasic);
    }

static AtPw CepFractionalCreate(AtModulePw pw, uint32 pwId)
    {
    return (AtPw)AtModulePwCepCreate(pw, (uint16)pwId, cAtPwCepModeFractional);
    }

static AtPw CESoPWithCasCreate(AtModulePw pw, uint32 pwId)
    {
    return (AtPw)AtModulePwCESoPCreate(pw, (uint16)pwId, cAtPwCESoPModeWithCas);
    }

static AtModulePw PwModule(void)
    {
    return (AtModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
    }

static eBool InterruptEnable(char argc, char **argv, eBool enable)
    {
    eAtRet ret = AtModuleInterruptEnable((AtModule)PwModule(), enable);
    AtUnused(argv);
    AtUnused(argc);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "Cannot %s interrupt, ret = %s\n", enable ? "enable" : "disable", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdPwCepCreate(char argc, char **argv)
    {
    eBool convertSuccess = cAtTrue;
    eAtPwCepMode mode = cAtPwCepModeUnknown;

    /* Get CESoP mode */
    mAtStrToEnum(cAtPwCepModeStr, cAtPwCepModeVal, argv[1], mode, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid CEP mode, expect: basic|fractional\r\n");
        return cAtFalse;
        }

    if (mode == cAtPwCepModeBasic)
        return HelperPwCreate(argc, argv, (PwCreateFunc)CepBasicCreate,   "PW");
    else
        return HelperPwCreate(argc, argv, (PwCreateFunc)CepFractionalCreate,  "PW");
    }

eBool CmdPwSatopCreate(char argc, char **argv)
    {
    return HelperPwCreate(argc, argv, (PwCreateFunc)AtModulePwSAToPCreate,  "PW");
    }

eBool CmdPwCesopCreate(char argc, char **argv)
    {
    eBool convertSuccess = cAtTrue;
    eAtPwCESoPMode mode = cAtPwCESoPModeUnknown;

    /* Get CESoP mode */
    mAtStrToEnum(cAtPwCESoPModeStr, cAtPwCESoPModeVal, argv[1], mode, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid CESoP mode, expect: basic|cas\r\n");
        return cAtFalse;
        }

    if (mode == cAtPwCESoPModeBasic)
        return HelperPwCreate(argc, argv, (PwCreateFunc)CESoPBasicCreate, "PW");
    else
        return HelperPwCreate(argc, argv, (PwCreateFunc)CESoPWithCasCreate, "PW");
    }

eBool CmdPwHdlcCreate(char argc, char **argv)
    {
    AtHdlcChannel* channelBuffer;
    uint32 numChannels;
    AtModulePw  pwModule = (AtModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
    uint32 channel_i;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get the shared ID buffer */
    channelBuffer = (AtHdlcChannel *)(CliSharedChannelListGet(&numChannels));
    numChannels = CliAtModuleEncapHdlcChannelsFromString(argv[0], channelBuffer, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No HDLC channels, flat ID list may be wrong or no HDLC encapsulation is found\r\n");
        return cAtTrue;
        }

    for (channel_i = 0; channel_i < numChannels; channel_i = (uint16)AtCliNextIdWithSleep(channel_i, AtCliLimitNumRestPwGet()))
        {
        if (AtModulePwHdlcCreate(pwModule, channelBuffer[channel_i]) == NULL)
            {
            AtPrintc(cSevCritical,
                     "ERROR: HDLC PW %s cannot be created or it is existing\r\n",
                     CliChannelIdStringGet((AtChannel)channelBuffer[channel_i]));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdPwPppCreate(char argc, char **argv)
    {
    AtPppLink* linkList;
    uint32 numChannels;
    AtModulePw  pwModule = (AtModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
    uint32 linkIdx;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get the shared ID buffer */
    linkList = (AtPppLink *)((void**)CliSharedChannelListGet(&numChannels));
    numChannels = CliPppLinkIdListFromString(argv[0], linkList, numChannels);
    if (numChannels == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No HDLC channels, flat ID list may be wrong or no HDLC encapsulation is found\r\n");
        return cAtFalse;
        }

    for (linkIdx = 0; linkIdx < numChannels; linkIdx = (uint16)AtCliNextIdWithSleep(linkIdx, AtCliLimitNumRestPwGet()))
        {
        if (AtModulePwPppCreate(pwModule, linkList[linkIdx]) == NULL)
            {
            AtPrintc(cSevCritical,
                     "ERROR: PPP PW %s cannot be created or it is existing\r\n",
                     CliChannelIdStringGet((AtChannel)linkList[linkIdx]));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdPwMlpppCreate(char argc, char **argv)
    {
    uint32* pBundleList;
    uint32 numBundles;
    AtModulePw  pwModule = (AtModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
    uint32 bundle_i;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get the shared ID buffer */
    pBundleList  = CliSharedIdBufferGet(&numBundles);
    numBundles = CliBundleIdListFromString(argv[0], pBundleList, numBundles);
    if (numBundles == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No MLPPP Bundle, flat ID list may be wrong or no HDLC encapsulation is found\r\n");
        return cAtFalse;
        }

    for (bundle_i = 0; bundle_i < numBundles; bundle_i = (uint16)AtCliNextIdWithSleep(bundle_i, AtCliLimitNumRestPwGet()))
        {
        AtMpBundle mpBundle = CliMpBundleGet(pBundleList[bundle_i]);
        if (AtModulePwMlpppCreate(pwModule, mpBundle) == NULL)
            {
            AtPrintc(cSevCritical,
                     "ERROR: MLPPP PW %s cannot be created or it is existing\r\n",
                     CliChannelIdStringGet((AtChannel)mpBundle));
            success = cAtFalse;
            }
        }

    return success;
    }

static uint32 VirtualCircuitFromPrefixString(char *pStrIdList, AtChannel *channelList, uint32 bufferSize)
    {
    uint32 numChannels = 0;
    char *pDupString;
    char *idType, *idString;
    pDupString = IdTypeAndIdListGet(pStrIdList, &idType, &idString);
    if ((idType == NULL) || (idString == NULL))
        {
        AtOsalMemFree(pDupString);
        return cAtFalse;
        }

    if (AtStrcmp(idType, sAtChannelTypeFrameRelay) == 0)
        numChannels = CliAtModuleEncapFrVirtualCircuitFromNonPrefixString(idString, channelList, bufferSize);

    else if (AtStrcmp(idType, sAtChannelTypeMultiLinkFrameRelay) == 0)
        numChannels = CliAtModuleEncapMfrVirtualCircuitFromNonPrefixString(idString, channelList, bufferSize);

    AtOsalMemFree(pDupString);

    return numChannels;
    }

eBool CmdPwFrVirtualcircuitCreate(char argc, char **argv)
    {
    AtChannel* channelBuffer;
    uint32 numChannels = 0, bufferSize;
    AtModulePw  pwModule = (AtModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
    uint32 channel_i;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get the shared ID buffer */
    channelBuffer = (AtChannel*)((void**)CliSharedChannelListGet(&bufferSize));
    numChannels = VirtualCircuitFromPrefixString(argv[0], channelBuffer, bufferSize);
    if (numChannels == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No Virtual Circuit, flat ID list may be wrong or no HDLC encapsulation is found\r\n");
        return cAtFalse;
        }

    for (channel_i = 0; channel_i < numChannels; channel_i = (uint16)AtCliNextIdWithSleep(channel_i, AtCliLimitNumRestPwGet()))
        {
        if (AtModulePwFrVcCreate(pwModule, (AtFrVirtualCircuit)channelBuffer[channel_i]) == NULL)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Vitual Circuit PW (dlci=%d) cannot be created or it is existing\r\n",
                     AtChannelIdGet(channelBuffer[channel_i]));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdPwTohCreate(char argc, char **argv)
    {
    return HelperPwCreate(argc, argv, (PwCreateFunc)AtModulePwTohCreate, "PW");
    }

eBool CmdPwAtmCreate(char argc, char **argv)
    {
    uint16 i;
    eBool blResult;
    uint32 *idBuf;
    uint32 bufferSize;
    uint32 numPws;
    eAtAtmPwType atmPwtype = cAtAtmPwTypeVcc1To1;
	AtUnused(argc);

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numPws = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    mAtStrToEnum(cAtPwAtmPwStr,
                 cAtPwAtmPwType,
                 argv[1],
                 atmPwtype,
                 blResult);
    if (!blResult)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid ATM PW type");
        return cAtFalse;
        }

    for (i = 0; i < numPws; i++)
        {
        if (AtModulePwAtmCreate(PwModule(), (uint16)(idBuf[i] - 1), atmPwtype) == NULL)
            AtPrintc(cSevCritical, "ERROR: PW %u cannot be created\r\n", idBuf[i]);
        }

    return cAtTrue;
    }

eBool CmdPwDelete(char argc, char **argv)
    {
    uint16      i;
    AtPw        *pws;
    uint32      numPws;
    eAtRet      ret = cAtOk;

	AtUnused(argc);

    pws = CliPwArrayFromStringGet(argv[0], &numPws);
    if (numPws == 0)
        {
        CliPwPrintHelpString();
        return cAtFalse;
        }

    for (i = 0; i < numPws; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        ret = AtModulePwDeletePwObject(PwModule(), pws[i]);
        if (ret != cAtOk)
            AtPrintc(cSevCritical, "Can not delete PW %s, ret = %s\r\n", CliChannelIdStringGet((AtChannel)pws[i]), AtRet2String(ret));
        }

    return cAtTrue;
    }

eBool CmdAtModulePwDebug(char argc, char **argv)
    {
	AtUnused(argv);
	AtUnused(argc);
    AtModuleDebug(AtDeviceModuleGet(CliDevice(), cAtModulePw));
    return cAtTrue;
    }

eBool CmdAtModulePwDcrClockSourceSet(char argc, char **argv)
    {
    eAtPwDcrClockSource clockSource = cAtPwDcrClockSourceUnknown;
    eBool convertSuccess;
    eAtRet ret;
	AtUnused(argc);

    mAtStrToEnum(cAtPwDcrClockSourceStr,
                 cAtPwDcrClockSourceVal,
                 argv[0],
                 clockSource,
                 convertSuccess);

    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid clock source. Expect: prc, system, ext1, ext2\r\n");
        return cAtFalse;
        }

    ret = AtModulePwDcrClockSourceSet(PwModule(), clockSource);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot change DCR clock source, ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAtModulePwDcrClockFrequencySet(char argc, char **argv)
    {
    uint32 frequency;
    eAtRet ret;
	AtUnused(argc);

    frequency = AtStrToDw(argv[0]);
    ret = AtModulePwDcrClockFrequencySet(PwModule(), frequency);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot change DCR clock frequency, ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAtModulePwDcrRtpTimestampFrequencySet(char argc, char **argv)
    {
    uint32 frequency;
    eAtRet ret;
	AtUnused(argc);

    frequency = AtStrToDw(argv[0]);
    ret = AtModulePwRtpTimestampFrequencySet(PwModule(), frequency);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot change RTP Time-stamp frequency, ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

static eBool RtpTimestampFrequencyIsValid(uint32 value)
    {
    return (value == 0xFFFFFFFF) ? cAtFalse : cAtTrue;
    }

eBool CmdAtModulePwShow(char argc, char **argv)
    {
    tTab *pTab;
    static char buf[64];
    eBool convertSuccess;
    eAtPwDcrClockSource clockSource;
    uint32 freePw;
    uint32 rtpTimeStampFrequency;
    const char *heading[] = {"Attribute", "Value"};
    eBool enabled;
    uint32 row = 0;
    AtModulePw modulePw = PwModule();
	AtUnused(argv);
	AtUnused(argc);

    if (modulePw == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Module does not exist\r\n");
        return cAtFalse;
        }

    /* Initialize table */
    pTab = TableAlloc(13, mCount(heading), heading);
    if (pTab == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    enabled = AtModuleInterruptIsEnabled((AtModule)modulePw);
    StrToCell(pTab, row, 0, "Interrupt");
    StrToCell(pTab, row, 1, CliBoolToString(enabled));

    /* Number of PWs */
    row++;
    AtSprintf(buf, "%u", AtModulePwMaxPwsGet(modulePw));
    StrToCell(pTab, row, 0, "numPws");
    StrToCell(pTab, row, 1, buf);

    /* DCR clock source */
    row++;
    clockSource = AtModulePwDcrClockSourceGet(modulePw);
    mAtEnumToStr(cAtPwDcrClockSourceStr, cAtPwDcrClockSourceVal, clockSource, buf, convertSuccess);
    StrToCell(pTab, row, 0, "dcrClockSource");
    if (convertSuccess)
        StrToCell(pTab, row, 1, buf);
    else
        ColorStrToCell(pTab, row, 1, "Error", cSevCritical);

    /* DCR frequency */
    row++;
    AtSprintf(buf, "%u KHz", AtModulePwDcrClockFrequencyGet(modulePw));
    StrToCell(pTab, row, 0, "dcrClockFrequency");
    StrToCell(pTab, row, 1, buf);

    /* DCR RTP timestamp frequency */
    row++;
    AtSprintf(buf, "%u KHz", AtModulePwRtpTimestampFrequencyGet(modulePw));
    StrToCell(pTab, row, 0, "dcrRtpTimestampFrequency");
    StrToCell(pTab, row, 1, buf);

    /* CEP DCR RTP timestamp frequency */
    row++;
    StrToCell(pTab, row, 0, "cepDcrRtpTimestampFrequency");
    rtpTimeStampFrequency = AtModulePwCepRtpTimestampFrequencyGet(modulePw);
    if (RtpTimestampFrequencyIsValid(rtpTimeStampFrequency))
        AtSprintf(buf, "%d KHz", rtpTimeStampFrequency);
    else
        AtSprintf(buf, "N/A");
    StrToCell(pTab, row, 1, buf);

    /* CES DCR RTP timestamp frequency */
    row++;
    StrToCell(pTab, row, 0, "cesDcrRtpTimestampFrequency");
    rtpTimeStampFrequency = AtModulePwCesRtpTimestampFrequencyGet(modulePw);
    if (RtpTimestampFrequencyIsValid(rtpTimeStampFrequency))
        AtSprintf(buf, "%d KHz", rtpTimeStampFrequency);
    else
        AtSprintf(buf, "N/A");
    StrToCell(pTab, row, 1, buf);

    /* Next free PW */
    row++;
    freePw = AtModulePwFreePwGet(modulePw);
    if (freePw >= AtModulePwMaxPwsGet(modulePw))
        AtSprintf(buf, "None");
    else
        AtSprintf(buf, "%u", freePw + 1);
    StrToCell(pTab, row, 0, "nextFreePwId");
    StrToCell(pTab, row, 1, buf);

    /* Idle code */
    row++;
    AtSprintf(buf, "0x%x", AtModulePwIdleCodeGet(modulePw));
    StrToCell(pTab, row, 0, "idleCode");
    StrToCell(pTab, row, 1, buf);

    /* Jitter buffer centering */
    row++;
    enabled = AtModulePwJitterBufferCenteringIsEnabled(modulePw);
    StrToCell(pTab, row, 0, "Jitter buffer centering");
    StrToCell(pTab, row, 1, CliBoolToString(enabled));

    /* Jitter buffer RAM resource */
    row++;
    StrToCell(pTab, row, 0, "Jitter buffer max num blocks");
    StrToCell(pTab, row, 1, CliNumber2String(AtModulePwJitterBufferMaxBlocksGet(modulePw), "%u"));

    row++;
    StrToCell(pTab, row, 0, "Jitter buffer block size");
    AtSprintf(buf, "%u (bytes)", AtModulePwJitterBufferBlockSizeInBytesGet(modulePw));
    StrToCell(pTab, row, 1, buf);

    row++;
    StrToCell(pTab, row, 0, "Jitter buffer num free blocks");
    StrToCell(pTab, row, 1, CliNumber2String(AtModulePwJitterBufferFreeBlocksGet(modulePw), "%u"));

    /* Show table */
    TablePrint(pTab);
    TableFree(pTab);

    return cAtTrue;
    }

eBool CmdAtModulePwLock(char argc, char **argv)
    {
	AtUnused(argv);
	AtUnused(argc);
    AtModuleLock((AtModule)PwModule());
    return cAtTrue;
    }

eBool CmdAtModulePwUnLock(char argc, char **argv)
    {
	AtUnused(argv);
	AtUnused(argc);
    AtModuleUnLock((AtModule)PwModule());
    return cAtTrue;
    }

eBool CmdAtModulePwIdleCodeSet(char argc, char **argv)
    {
    /* PW Module IDLE code */
    if (argc == 1)
        {
        eAtRet ret = AtModulePwIdleCodeSet(PwModule(), (uint8)AtStrToDw(argv[0]));
        if (ret != cAtOk)
            AtPrintc(cSevCritical, "ERROR: Set IDLE fail with ret = %s\r\n", AtRet2String(ret));
        return (ret == cAtOk) ? cAtTrue : cAtFalse;
        }

    /* PW IDLE code */
    if (argc == 2)
        {
        return CmdAtPwIdleCodeSet(argc, argv);
        }

    AtPrintc(cSevCritical, "ERROR: Invalid number of arguments\r\n");
    return cAtFalse;
    }

eBool CmdAtModulePwCepDcrRtpTimestampFrequencySet(char argc, char **argv)
    {
    uint32 frequency;
    eAtRet ret;

    AtUnused(argc);

    frequency = AtStrToDw(argv[0]);
    ret = AtModulePwCepRtpTimestampFrequencySet(PwModule(), frequency);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot change RTP Time-stamp frequency, ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdAtModulePwCesDcrRtpTimestampFrequencySet(char argc, char **argv)
    {
    uint32 frequency;
    eAtRet ret;

    AtUnused(argc);
    frequency = AtStrToDw(argv[0]);
    ret = AtModulePwCesRtpTimestampFrequencySet(PwModule(), frequency);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot change RTP Time-stamp frequency, ret = %s\r\n", AtRet2String(ret));
        return cAtFalse;
        }

    return cAtTrue;
    }

eBool CmdPwInterruptEnable(char argc, char **argv)
    {
    return InterruptEnable(argc, argv, cAtTrue);
    }

eBool CmdPwInterruptDisable(char argc, char **argv)
    {
    return InterruptEnable(argc, argv, cAtFalse);
    }

eBool CmdPwJitterCenteringEnable(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    return AtModulePwJitterBufferCenteringEnable(PwModule(), cAtTrue) == cAtOk ? cAtTrue:cAtFalse;
    }

eBool CmdPwJitterCenteringDisable(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    return AtModulePwJitterBufferCenteringEnable(PwModule(), cAtFalse) == cAtOk ? cAtTrue:cAtFalse;
    }

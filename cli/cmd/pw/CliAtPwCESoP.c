/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : CliAtPwCESoP.c
 *
 * Created Date: Mar 18, 2013
 *
 * Description : CESoP CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtModulePw.h"
#include "AtModulePw.h"
#include "AtPwCESoP.h"
#include "AtDevice.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char * cCmdCESoPCasModeStr[]={"e1-cas", "ds1-cas"};
static const eAtPwCESoPCasMode cCmdCESoPCasModeVal[]={cAtPwCESoPCasModeE1, cAtPwCESoPCasModeDs1};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool AutoMBitEnable(char argc, char **argv, eAtModulePwRet (*EnableFunc)(AtPwCESoP , eBool), eBool enable)
    {
    AtPw        pw;
    uint16      i;
    uint32      *idBuf;
    uint32      bufferSize;
    uint32      numPws;
    AtModulePw  pwModule;
    eAtRet      ret = cAtOk;
	AtUnused(argc);

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numPws = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    pwModule = (AtModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);

    for (i = 0; i < numPws; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        pw = AtModulePwGetPw(pwModule, (uint16)(idBuf[i] - 1));
        if (pw == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: PW %u does not exist\r\n", idBuf[i]);
            continue;
            }

        if (AtPwTypeGet(pw) != cAtPwTypeCESoP)
            {
            AtPrintc(cSevWarning, "WARNING: PW %u is not CESoP\r\n", idBuf[i]);
            continue;
            }

        ret = EnableFunc((AtPwCESoP)pw, enable);
        if (ret != cAtOk)
            AtPrintc(cSevCritical, "ERROR: Can not configure for %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)pw),
                     AtRet2String(ret));
        }

    return (ret != cAtOk) ? cAtFalse : cAtTrue;
    }

static eAtModulePwRet CwAutoMBitEnable(AtPwCESoP self, eBool enable)
    {
    eAtRet ret = AtPwCESoPCwAutoTxMBitEnable(self, enable);
    if (AtPwCESoPCwAutoRxMBitIsConfigurable(self))
        ret |= AtPwCESoPCwAutoRxMBitEnable(self, enable);
    return ret;
    }

eBool CmdPwAutoMBitEn(char argc, char **argv)
    {
    return AutoMBitEnable(argc, argv, CwAutoMBitEnable, cAtTrue);
    }

eBool CmdPwAutoMBitDis(char argc, char **argv)
    {
    return AutoMBitEnable(argc, argv, CwAutoMBitEnable, cAtFalse);
    }

eBool CmdPwCESoPAutoTxMbitEn(char argc, char **argv)
    {
    return AutoMBitEnable(argc, argv, AtPwCESoPCwAutoTxMBitEnable, cAtTrue);
    }

eBool CmdPwCESoPAutoTxMbitDis(char argc, char **argv)
    {
    return AutoMBitEnable(argc, argv, AtPwCESoPCwAutoTxMBitEnable, cAtFalse);
    }

eBool CmdPwCESoPAutoRxMbitEn(char argc, char **argv)
    {
    return AutoMBitEnable(argc, argv, AtPwCESoPCwAutoRxMBitEnable, cAtTrue);
    }

eBool CmdPwCESoPAutoRxMbitDis(char argc, char **argv)
    {
    return AutoMBitEnable(argc, argv, AtPwCESoPCwAutoRxMBitEnable, cAtFalse);
    }

eBool CmdPwCESoPCasIdleCode1Set(char argc, char **argv)
    {
    AtUnused(argc);
    return CliPwAttributeSet(argv[0], AtStrtoul(argv[1], NULL, 16), (PwAttributeSetFunc)(AtPwCESoPCasIdleCode1Set));
    }

eBool CmdPwCESoPCasIdleCode2Set(char argc, char **argv)
    {
    AtUnused(argc);
    return CliPwAttributeSet(argv[0], AtStrtoul(argv[1], NULL, 16), (PwAttributeSetFunc)(AtPwCESoPCasIdleCode2Set));
    }

eBool CmdPwCESoPCasAutoIdleEnable(char argc, char **argv)
    {
    AtUnused(argc);
    return CliPwAttributeSet(argv[0], cAtTrue, (PwAttributeSetFunc)(AtPwCESoPCasAutoIdleEnable));
    }

eBool CmdPwCESoPCasAutoIdleDisable(char argc, char **argv)
    {
    AtUnused(argc);
    return CliPwAttributeSet(argv[0], cAtFalse, (PwAttributeSetFunc)(AtPwCESoPCasAutoIdleEnable));
    }

eBool CmdPwCESoPCasShow(char argc, char **argv)
    {
    AtPw*         pwList;
    uint16        i;
    uint16        column;
    uint32        numPws;
    tTab          *tabPtr;
    eBool         blResult = cAtFalse;
    const char    *heading[] = {"PW", "CAS_IDLE1", "CAS_IDLE2", "CAS_AUTO_IDLE", "RX CAS Mode"};
    AtUnused(argc);

    pwList = CliPwArrayFromStringGet(argv[0], &numPws);
    if (numPws == 0)
        {
        CliPwPrintHelpString();
        return cAtFalse;
        }

    tabPtr = TableAlloc(numPws, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numPws; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        eAtPwType pwType;
        AtPwCESoP pw;
        eAtPwCESoPCasMode casMode;
        uint8 abcd1, abcd2, autoEnabled;
        static char celString[32];

        column = 0;
        pw = (AtPwCESoP)pwList[i];
        pwType = AtPwTypeGet((AtPw)pw);
        if (pwType != cAtPwTypeCESoP)
            continue;

        AtSprintf(celString, "%s", CliChannelIdStringGet((AtChannel)pw));
        StrToCell(tabPtr, i, column++, celString);

        abcd1 = AtPwCESoPCasIdleCode1Get(pw);
        AtSprintf(celString, "0x%x", abcd1);
        StrToCell(tabPtr, i, column++, celString);

        abcd2 = AtPwCESoPCasIdleCode2Get(pw);
        AtSprintf(celString, "0x%x", abcd2);
        StrToCell(tabPtr, i, column++, celString);

        autoEnabled = AtPwCESoPCasAutoIdleIsEnabled(pw);
        mAtBoolToStr(autoEnabled, celString, blResult);
        StrToCell(tabPtr, i, column++, celString);

        casMode = AtPwCESoPRxCasModeGet(pw);
        mAtEnumToStr(cCmdCESoPCasModeStr,
                     cCmdCESoPCasModeVal,
                     casMode,
                     celString,
                     blResult);
        StrToCell(tabPtr, i, column++, (blResult) ? celString : "N/A");
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return blResult;
    }

eBool CmdPwCESoPRxCasMode(char argc, char **argv)
    {
    eBool convertSuccess = cAtFalse;
    eAtPwCESoPCasMode casMode = cAtPwCESoPCasModeUnknown;
    AtUnused(argc);

    mAtStrToEnum(cCmdCESoPCasModeStr, cCmdCESoPCasModeVal, argv[1], casMode, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid CESoP CAS mode\r\n");
        return cAtFalse;
        }

    return CliPwAttributeSet(argv[0], casMode, (PwAttributeSetFunc)(AtPwCESoPRxCasModeSet));
    }

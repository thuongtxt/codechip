/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : CliAtPwHdlc.c
 *
 * Created Date: May 23, 2019
 *
 * Description : PW HDLC CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtModulePw.h"
#include "AtPwHdlc.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mIsPwVirtualCircuitType(_type) ((_type == cAtPwTypeFr) ? cAtTrue : cAtFalse)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char * cAtPwHdlcPayloadTypeStr[] = {"full", "pdu"};
static const uint32 cAtPwHdlcPayloadTypeVal[] = {cAtPwHdlcPayloadTypeFull, cAtPwHdlcPayloadTypePdu};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eBool CmdPwHdlcPayloadTypeSet(char argc, char **argv)
    {
    eBool succes;
    uint32 payloadType = CliStringToEnum(argv[1], cAtPwHdlcPayloadTypeStr, cAtPwHdlcPayloadTypeVal, sizeof(cAtPwHdlcPayloadTypeVal), &succes);

    AtUnused(argc);
    if (!succes)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid HDLC payload type mode. Expected: full/pdu\r\n");
        return cAtFalse;
        }

    return CliPwAttributeSet(argv[0], payloadType, (PwAttributeSetFunc)AtPwHdlcPayloadTypeSet);
    }

eBool CmdPwHdlcGet(char argc, char **argv)
    {
    AtPw          pw;
    AtPw*         pwList;
    uint16        i;
    uint16        column;
    eBool         blResult;
    uint32        numPws;
    static char   buf[32];
    tTab          *tabPtr;
    const char    *heading[] = {"PW", "payloadType"};
    AtUnused(argc);

    pwList = CliPwArrayFromStringGet(argv[0], &numPws);
    if (numPws == 0)
        {
        CliPwPrintHelpString();
        return cAtFalse;
        }

    tabPtr = TableAlloc(numPws, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numPws; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        eAtPwType pwType;
        static char idString[32];

        column = 0;
        pw = (AtPw)pwList[i];
        pwType = AtPwTypeGet(pw);

        if (mIsPwVirtualCircuitType(pwType))
            AtSprintf(idString, "%s.%d", AtChannelTypeString((AtChannel)pw), AtChannelIdGet((AtChannel)pw));
        else
            AtSprintf(idString, "%s", CliChannelIdStringGet((AtChannel)pw));

        StrToCell(tabPtr, i, column++, idString);

        mAtEnumToStr(cAtPwHdlcPayloadTypeStr, cAtPwHdlcPayloadTypeVal, AtPwHdlcPayloadTypeGet((AtPwHdlc)pw), buf, blResult);
        StrToCell(tabPtr, i, column++, (blResult) ? buf : sAtNotApplicable);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

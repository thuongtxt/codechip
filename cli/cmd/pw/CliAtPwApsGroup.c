/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : CliAtPwApsGroup.c
 *
 * Created Date: Mar 18, 2015
 *
 * Description : PW CLIs
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtModulePw.h"
#include "AtDevice.h"
#include "AtModulePw.h"
#include "AtPw.h"
#include "AtList.h"
#include "AtPwGroup.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
AtList CliPwGroupListFromString(char *groupIdListString, PwGroupGetFunc PwGetPwFunc)
    {
    AtList validGroups;
    uint32 *idBuf;
    uint32 bufferSize, numGroups, i;
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);

    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numGroups = CliIdListFromString(groupIdListString, idBuf, bufferSize)) == 0)
        return NULL;

    validGroups = AtListCreate(0);
    if (validGroups == NULL)
        return NULL;

    /* Put all valid PWs */
    for (i = 0; i < numGroups; i++)
        {
        AtObject group;

        group = (AtObject)PwGetPwFunc(pwModule, (uint16)(idBuf[i] - 1));
        if (group)
            AtListObjectAdd(validGroups, group);
        }

    /* Return NULL if nothing in this list */
    if (AtListLengthGet(validGroups) == 0)
        {
        AtObjectDelete((AtObject)validGroups);
        return NULL;
        }

    return validGroups;
    }

eBool CliPwGroupBoolAttributeSet(char *pwGroupIdList,
                                 uint32 value,
                                 PwGroupGetFunc PwGetPwFunc,
                                 PwGroupAttributeSet attributeSetFunc)
    {
    eBool success;
    uint16 group_i;
    AtList pwGroupList = CliPwGroupListFromString(pwGroupIdList, PwGetPwFunc);

    if (AtListLengthGet(pwGroupList) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: Invalid pwGroup with %s\r\n", pwGroupIdList);
        return cAtFalse;
        }

    success = cAtTrue;
    for (group_i = 0; group_i < AtListLengthGet(pwGroupList); group_i = (uint16)AtCliNextIdWithSleep(group_i, AtCliLimitNumRestPwGet()))
        {
        eAtRet ret = cAtOk;

        /* Ignore PW that has not been created */
        AtPwGroup pwGroup = (AtPwGroup)AtListObjectGet(pwGroupList, group_i);
        ret = attributeSetFunc(pwGroup, value);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Can not change attribute of %s, ret = %s\r\n",
                     AtObjectToString((AtObject)pwGroup),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    AtObjectDelete((AtObject)pwGroupList);
    return success;
    }

eBool CmdPwApsGroupCreate(char argc, char **argv)
    {
    return HelperPwCreate(argc, argv, (PwCreateFunc) AtModulePwApsGroupCreate, "PW APS Group");
    }

eBool CliPwGroupDelete(char argc, char **argv, PwGroupDeleteFunc pwGroupDeleteFunc, const char *name)
    {
    uint16      i;
    uint32      *idBuf;
    uint32      bufferSize;
    uint32      numGroups;
    AtModulePw  pwModule = (AtModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);

    AtUnused(argc);

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numGroups = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid PW group list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    for (i = 0; i < numGroups; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        eAtRet ret = pwGroupDeleteFunc(pwModule, (idBuf[i] - 1));
        if (ret != cAtOk)
            AtPrintc(cSevCritical, "ERROR: %s %u cannot be deleted\r\n", name, idBuf[i]);
        }

    return cAtTrue;
    }

eBool CmdPwApsGroupDelete(char argc, char **argv)
    {
    return CliPwGroupDelete(argc, argv, (PwGroupDeleteFunc)AtModulePwApsGroupDelete, "PW APS Group");
    }

eBool CmdPwApsGroupEnable(char argc, char **argv)
    {
    AtUnused(argc);
    return CliPwGroupBoolAttributeSet(argv[0],
                                      cAtTrue,
                                      (AtPwGroup (*)(AtModulePw, uint32))AtModulePwApsGroupGet,
                                      (eAtRet (*)(AtPwGroup, uint32))AtPwGroupEnable);
    }

eBool CmdPwApsGroupDisable(char argc, char **argv)
    {
    AtUnused(argc);
    return CliPwGroupBoolAttributeSet(argv[0], cAtFalse, (AtPwGroup (*)(AtModulePw, uint32))AtModulePwApsGroupGet, (eAtRet (*)(AtPwGroup, uint32))AtPwGroupEnable);
    }

eBool CliPwGroupResourceManager(char argc,
                                char **argv,
                                PwGroupGetFunc PwGetPwFunc,
                                PwGroupResourceManagerFunc ResourceManagerFunc)
    {
    AtPwGroup   pwGroup;
    AtList      pwList;
    AtList      pwGroupList;
    uint32      pw_i, ret;
    eBool       success = cAtTrue;
    AtUnused(argc);

    /* Get the shared ID buffer */
    pwGroupList = CliPwGroupListFromString(argv[0], PwGetPwFunc);
    if (AtListLengthGet(pwGroupList) != 1)
        {
        AtPrintc(cSevWarning, "WARNING: Invalid pwGroup with %s\r\n", argv[0]);
        AtObjectDelete((AtObject)pwGroupList);
        return cAtFalse;
        }

    /* Check If Invalid PW List */
    pwList = CliPwListFromString(argv[1]);
    if (AtListLengthGet(pwList) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No PW invalid PW list with input %s , expected: 1 or 1-252, ...\n", argv[1]);
        AtObjectDelete((AtObject)pwGroupList);
        AtObjectDelete((AtObject)pwList);
        return cAtFalse;
        }

    /* Apply the PWGroup resource manager */
    pwGroup = (AtPwGroup)AtListObjectGet(pwGroupList, 0);
    for (pw_i = 0; pw_i < AtListLengthGet(pwList); pw_i = (uint16)AtCliNextIdWithSleep(pw_i, AtCliLimitNumRestPwGet()))
        {
        AtPw pw = (AtPw)AtListObjectGet(pwList, pw_i);
        ret = ResourceManagerFunc(pwGroup, pw);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Can not manage resource of %s for %s, ret = %s\r\n",
                     AtObjectToString((AtObject)pwGroup),
                     CliChannelIdStringGet((AtChannel)pw),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    AtObjectDelete((AtObject)pwGroupList);
    AtObjectDelete((AtObject)pwList);

    return success;
    }

eBool CmdPwApsGroupAdd(char argc, char **argv)
    {
    return CliPwGroupResourceManager(argc, argv, AtModulePwApsGroupGet, AtPwGroupPwAdd);
    }

eBool CmdPwApsGroupRemove(char argc, char **argv)
    {
    return CliPwGroupResourceManager(argc, argv, AtModulePwApsGroupGet, AtPwGroupPwRemove);
    }

eBool CmdPwApsGroupShow(char argc, char **argv)
    {
    uint16 group_i;
    uint32 numPwGroups;
    AtList pwGroupList;
    uint32 stringBufLen = 64000;
    char   *pwStringBuf;
    uint32 *idBuf, bufferSize;
    tTab       *tabPtr;
    const char *heading[] = {"GoupId", "Enable", "Pw List"};
    AtUnused(argc);

    /* Get the shared ID buffer */
    pwGroupList = CliPwGroupListFromString(argv[0], AtModulePwApsGroupGet);
    numPwGroups = AtListLengthGet(pwGroupList);
    if (numPwGroups == 0)
        {
        AtPrintc(cSevWarning, "WARNING: Invalid pwGroup with %s\r\n", argv[0]);
        AtObjectDelete((AtObject)pwGroupList);
        return cAtFalse;
        }

    /* Allocate Table */
    tabPtr = TableAlloc(numPwGroups, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        AtObjectDelete((AtObject)pwGroupList);
        return cAtFalse;
        }

    idBuf = CliSharedIdBufferGet(&bufferSize);
    /* Allocate 64K bytes for pw ID string list */
    pwStringBuf = (char*)AtOsalMemAlloc(stringBufLen);
    if (!pwStringBuf)
        {
        AtPrintc(cSevCritical, "Cannot allocate buffer to store pw list\r\n");
        AtObjectDelete((AtObject)pwGroupList);
        return cAtFalse;
        }

    for (group_i = 0; group_i < numPwGroups; group_i = (uint16)AtCliNextIdWithSleep(group_i, AtCliLimitNumRestPwGet()))
        {
        uint32 numPw;
        uint32 column = 0;
        eBool enable;

        AtPwGroup pwGroup = (AtPwGroup)AtListObjectGet(pwGroupList, group_i);
        StrToCell(tabPtr, group_i, column++, CliNumber2String(AtPwGroupIdGet(pwGroup) + 1, "%d"));

        enable = AtPwGroupIsEnabled(pwGroup);
        ColorStrToCell(tabPtr, group_i, column++, CliBoolToString(enable), enable ? cSevInfo : cSevCritical);

        numPw =  AtPwGroupNumPws(pwGroup);

        /* Clear memory */
        AtOsalMemInit(pwStringBuf, 0, stringBufLen);
        numPw = PwGroupToPwIdList(pwGroup, idBuf, bufferSize);
        if (numPw == 0)
            AtSprintf(pwStringBuf, "%s", "none");
        else
            IdDwordListToString(pwStringBuf, stringBufLen, idBuf, numPw);

        ColorStrToCell(tabPtr, group_i, column++,(char *)pwStringBuf, (numPw == 0) ? cSevCritical : cSevInfo);
        }


    AtObjectDelete((AtObject)pwGroupList);
    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtOsalMemFree(pwStringBuf);

    return cAtTrue;
    }

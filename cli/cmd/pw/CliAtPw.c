/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : CliAtPw.c
 *
 * Created Date: Mar 18, 2013
 *
 * Description : PW CLIs
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "CliAtModulePw.h"
#include "AtDevice.h"
#include "AtModulePw.h"
#include "AtPw.h"
#include "AtPwCep.h"
#include "AtPwCESoP.h"
#include "AtPwCounters.h"
#include "AtList.h"
#include "../util/CliAtDebugger.h"
#include "../eth/CliAtModuleEth.h"
#include "../encap/CliAtModuleEncap.h"
#include "../sdh/CliAtSdhLineDcc.h"
#include "AtCliModule.h"
#include "AtEncapChannel.h"
#include "AtHdlcChannel.h"
#include "AtFrLink.h"
#include "AtEncapBundle.h"
#include "AtMfrBundle.h"
#include "../sdh/CliAtSdhLineDcc.h"
#include "../common/CliAtChannelObserver.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumPwsPerTable 10
#define cDlciMaxId		(4095)
#define cTwoLevelsIdVcMinFormatStr "1.0"

/*--------------------------- Macros -----------------------------------------*/
#define mPutConfigToCell(value)                                                \
    do                                                                         \
        {                                                                      \
        AtSprintf(buf, "%u", value);                                           \
        StrToCell(tabPtr, i, column++, buf);                                   \
        }while(0)

#define mIsPwVirtualCircuitType(_type) ((_type == cAtPwTypeFr) ? cAtTrue : cAtFalse)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static const char * cAtPwRtpTimeStampStr[] = {
                                             "invalid",
                                             "absolute",
                                             "differential"
                                            };

static const uint32 cAtPwRtpTimeStampVal[]  = {
                                                              cAtPwRtpTimeStampModeInvalid,
                                                              cAtPwRtpTimeStampModeAbsolute,
                                                              cAtPwRtpTimeStampModeDifferential
                                                              };

static const char * cAtPwCwSequenceStr[] = {"wrapzero",
                                            "skipzero",
                                            "dis"};

static const uint32 cAtPwCwSequenceVal[]  = {
                                                          cAtPwCwSequenceModeWrapZero,
                                                          cAtPwCwSequenceModeSkipZero,
                                                          cAtPwCwSequenceModeDisable
                                                          };

static const char * cAtPwCwLengthStr[] = {
                                         "fullpkt",
                                         "payload"
                                         };

static const uint32 cAtPwCwLengthVal[]  = {
                                                      cAtPwCwLengthModeFullPacket,
                                                      cAtPwCwLengthModePayload
                                                      };

static const char * cAtPwCwReplaceStr[] = {
                                         "goodpacket",
                                         "ais",
                                         "idlecode"
                                         };

static const uint32 cAtPwCwReplaceVal[]  = {
                                                        cAtPwPktReplaceModePreviousGoodPacket,
                                                        cAtPwPktReplaceModeAis,
                                                        cAtPwPktReplaceModeIdleCode
                                                      };

static const char *cAtPsnTypeStr[] = {
                                 "mpls",
                                 "mef",
                                 "udp",
                                 "ipv4",
                                 "ipv6"
                                 };

static const uint32 cAtPsnTypeVal[]  = {
                                              cAtPwPsnTypeMpls,
                                              cAtPwPsnTypeMef,
                                              cAtPwPsnTypeUdp,
                                              cAtPwPsnTypeIPv4,
                                              cAtPwPsnTypeIPv6
                                              };

static const char * cAtPwAlarmTypeStr[] = {
                                         "lbit",
                                         "rbit",
                                         "mbit",
                                         "lops",
                                         "overrun",
                                         "underrun",
                                         "missing",
                                         "excessive",
                                         "stray",
                                         "malformed",
                                         "misconnection",
                                         "all"
                                         };

static const char *cAtPwAlarmPrettyTypeStr[] = {
                                                "L-bit",
                                                "R-bit",
                                                "M-bit",
                                                "LOPS",
                                                "Overrun",
                                                "Underrun",
                                                "Missing",
                                                "Excessive",
                                                "Stray",
                                                "Malformed",
                                                "Misconnection"};


static const uint32 cAtPwAlarmTypeVal[]  = {
                                                    cAtPwAlarmTypeLBit,
                                                    cAtPwAlarmTypeRBit,
                                                    cAtPwAlarmTypeMBit,
                                                    cAtPwAlarmTypeLops,
                                                    cAtPwAlarmTypeJitterBufferOverrun,
                                                    cAtPwAlarmTypeJitterBufferUnderrun,
                                                    cAtPwAlarmTypeMissingPacket,
                                                    cAtPwAlarmTypeExcessivePacketLossRate,
                                                    cAtPwAlarmTypeStrayPacket,
                                                    cAtPwAlarmTypeMalformedPacket,
                                                    cAtpwAlarmTypeMisConnection,
                                                    cAtPwAlarmTypeAll
                                                  };

static const char * cAtPwCESoPModeStr[] = {"basic", "cas"};
static const uint32 cAtPwCESoPModeVal[] = {cAtPwCESoPModeBasic, cAtPwCESoPModeWithCas};

static const char * cAtPwCepModeStr[] = {"basic", "fractional"};
static const uint32 cAtPwCepModeVal[] = {cAtPwCepModeBasic, cAtPwCepModeFractional};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eBool CliPwAttributeSet(char *pwIdList, uint32 value, PwAttributeSetFunc attributeSetFunc)
    {
    uint16      i;
    AtPw        *pwList;
    uint32      numPws;
    eBool       success = cAtTrue;

    pwList = CliPwArrayFromStringGet(pwIdList, &numPws);
    if (numPws == 0)
        {
        CliPwPrintHelpString();
        return cAtFalse;
        }

    for (i = 0; i < numPws; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        eAtRet ret = cAtOk;
        AtPw pw = pwList[i];

        /* Apply input attribute */
        ret = attributeSetFunc(pw, value);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Can not change attribute of %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)pw),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

static char *TwoLevelsIdFrVcMaxFormat(void)
    {
    static char maxFormat[16];

    AtOsalMemInit(maxFormat, 0, sizeof(maxFormat));
    AtSprintf(maxFormat, "%u.%u", AtModuleEncapMaxChannelsGet(CliAtEncapModule()), cDlciMaxId);

    return maxFormat;
    }

static char *TwoLevelsIdMfrVcMaxFormat(void)
    {
    static char maxFormat[16];

    AtOsalMemInit(maxFormat, 0, sizeof(maxFormat));
    AtSprintf(maxFormat, "%u.%u", AtModuleEncapMaxBundlesGet(CliAtEncapModule()), cDlciMaxId);

    return maxFormat;
    }

static eBool IntegerAttributeSet(char argc,
                                 char **argv,
                                 eAtRet(*attributeSetFunc)(AtPw pw, uint32 value))
    {
	AtUnused(argc);
    return CliPwAttributeSet(argv[0], AtStrToDw(argv[1]), attributeSetFunc);
    }

static eBool PsnTypeListFromString(const char *pPsnType, eAtPwPsnType *pPsnTypeList, uint8 *psnLayerNum)
    {
    eBool   isValid = cAtFalse;
    char    **pItemList;
    uint32  numItem;
    uint32  itemIndex;

    if (pPsnType == NULL)
        return cAtFalse;

    pItemList = StrTokenGet(pPsnType, cFormatCheck, &numItem);

    for (itemIndex = 0; itemIndex < numItem; itemIndex ++)
        {
        mAtStrToEnum(cAtPsnTypeStr, cAtPsnTypeVal, pItemList[itemIndex], pPsnTypeList[itemIndex], isValid);
        if (!isValid)
            break;
        }

    for (itemIndex = 0; itemIndex < numItem; itemIndex++)
        AtOsalMemFree(pItemList[itemIndex]);
    AtOsalMemFree(pItemList);

    if (isValid)
        *psnLayerNum = (uint8)numItem;

    return isValid;
    }

static char *PsnExpectedLabelGet(AtPwPsn psn)
    {
    static char buf[32];
    eAtPwPsnType type = AtPwPsnTypeGet(psn);

    AtSprintf(buf, sAtNotApplicable);
    if (type == cAtPwPsnTypeMpls)
        AtSprintf(buf, "%u", AtPwMplsPsnExpectedLabelGet((AtPwMplsPsn)psn));
    if (type == cAtPwPsnTypeMef)
        AtSprintf(buf, "%u", AtPwMefPsnExpectedEcIdGet((AtPwMefPsn)psn));
    return buf;
    }

static char** BuildHeadingForPws(AtPw *pwList, uint32 currentPwIndex, uint32 numPwsPerTable)
    {
    AtPw   pw;
    uint32 i;
    char   **headings;

    headings = AtOsalMemAlloc((numPwsPerTable + 1) * sizeof(char *));
    if (headings == NULL)
        return NULL;

    headings[0] = AtOsalMemAlloc(8);
    AtSprintf(headings[0], "PW");

    for (i = 0; i < numPwsPerTable; i++)
        {
        pw = pwList[currentPwIndex + i];
        headings[i + 1] = AtOsalMemAlloc(8);
        AtSprintf(headings[i + 1], "%u", AtChannelIdGet((AtChannel)pw) + 1);
        }

    return headings;
    }

static void DestroyHeadings(char **heading, uint32 numColumns)
    {
    uint32 i;

    if (heading == NULL)
        return;

    for (i = 0; i < numColumns; i++)
        AtOsalMemFree(heading[i]);
    AtOsalMemFree(heading);
    }

static char* NumPktsInJitterBuffer(AtPw pw)
    {
    static char buf[64];
    uint32 numAdditionalBytes = AtPwNumCurrentAdditionalBytesInJitterBuffer(pw);
    uint32 numPktInBuf = AtPwNumCurrentPacketsInJitterBuffer(pw);

    if (numAdditionalBytes != 0)
        AtSprintf(buf, "%u + %u (bytes)", numPktInBuf, numAdditionalBytes);
    else
        AtSprintf(buf, "%u", numPktInBuf);

    return buf;
    }

static eBool IsTdmPw(eAtPwType pwType)
    {
    if ((pwType == cAtPwTypeSAToP) || (pwType == cAtPwTypeCESoP) || (pwType == cAtPwTypeCEP))
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsHdlcPw(eAtPwType pwType)
    {
    if ((pwType == cAtPwTypeHdlc) || (pwType == cAtPwTypePpp) || (pwType == cAtPwTypeFr))
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsKbytePw(eAtPwType pwType)
    {
    if (pwType == cAtPwTypeKbyte)
        return cAtTrue;

    return cAtFalse;
    }

eBool CliAtPwCounterShow(AtPw *pwList, uint32 numPws, eAtHistoryReadingMode readingMode, eBool silent, HeadingPwsBuild pwHeadingBuild)
    {
    const char *counterNames[] = {/* Common counters */
                                  "TxPackets", "TxPayloadBytes", "RxPackets",
                                  "RxPayloadBytes", "RxDiscardedPackets", "RxMalformedPackets",
                                  "RxReorderedPackets", "RxLostPackets", "RxOutOfSeqDropPackets",
                                  "RxStrayPackets", "RxOamPackets", "RxDuplicatedPackets",

                                  /* TDM counters */
                                  "TxLbitPackets", "TxRbitPackets", "RxLbitPackets",
                                  "RxRbitPackets", "RxJitBufOverrunEvents", "RxJitBufUnderrunEvents",
                                  "RxLops", "RxPacketsSentToTdm",

                                  /* CESoP counters */
                                  "TxMbitPackets", "RxMbitPackets",

                                  /* CEP counters */
                                  "TxNbitPackets", "TxPbitPackets",
                                  "RxNbitPackets", "RxPbitPackets",

                                  /* Other */
                                  "NumPktsInJitterBuffer"};
    uint32 numTables, table_i;
    eAtRet (*AllCountersGet)(AtChannel self, void *counters) = NULL;
    uint32  remainPws = numPws;

    /* Calculate number of tables need to show counters of all input PWs */
    numTables = numPws / cNumPwsPerTable;
    if ((numPws % cNumPwsPerTable) > 0)
        numTables = numTables + 1;

    /* Get corresponding function to read counter */
    if (readingMode == cAtHistoryReadingModeReadOnly)
        AllCountersGet = AtChannelAllCountersGet;
    else
        AllCountersGet = AtChannelAllCountersClear;

    for (table_i = 0; table_i < numTables; table_i = AtCliNextIdWithSleep(table_i, 1))
        {
        tTab *tabPtr = NULL;
        uint32 numPwsPerTable;
        uint16 pw_i;
        char **heading = NULL;
        uint8 counter_i = 0;
        uint32 currentPwIndex = table_i * cNumPwsPerTable;
        eBool cntIsSupported = cAtTrue;

        numPwsPerTable = remainPws;
        if (numPwsPerTable > cNumPwsPerTable)
            numPwsPerTable = cNumPwsPerTable;

        /* Create table */
        if (!silent)
            {
            heading = pwHeadingBuild(pwList, currentPwIndex, numPwsPerTable);
            tabPtr = TableAlloc(mCount(counterNames), numPwsPerTable + 1, (void *)heading);
            if (!tabPtr)
                {
                DestroyHeadings(heading, numPwsPerTable + 1);
                AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
                return cAtFalse;
                }
            }

        /* Build column of counter names */
        for (counter_i = 0; counter_i < mCount(counterNames); counter_i++)
            StrToCell(tabPtr, counter_i, 0, counterNames[counter_i]);

        /* Put counters of current PWs */
        for (pw_i = 0; pw_i < numPwsPerTable; pw_i++)
            {
            eBool countersValid = cAtTrue;
            eAtRet ret;
            AtPwTdmCounters tdmCounters;
            AtPwCESoPCounters cesopCounters;
            AtPwCepCounters cepCounters;
            eAtPwType pwType;
            eBool notApplicable;
            AtPw pw;
            AtPwCounters counters = NULL;

            /* Get PW */
            pw = pwList[currentPwIndex + pw_i];

            /* Read counters */
            ret = AllCountersGet((AtChannel)pw, &counters);
            countersValid = (ret == cAtOk) ? cAtTrue : cAtFalse;
            pwType = AtPwTypeGet(pw);
            counter_i = 0;

            /* Common counters */
            notApplicable = (IsHdlcPw(pwType)||IsKbytePw(pwType)) ? cAtTrue : cAtFalse;
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, AtPwCountersTxPacketsGet(counters), cAtCounterTypeGood, countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, AtPwCountersTxPayloadBytesGet(counters), cAtCounterTypeGood, countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, AtPwCountersRxPacketsGet(counters), cAtCounterTypeGood, countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, AtPwCountersRxPayloadBytesGet(counters), cAtCounterTypeGood, countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, AtPwCountersRxDiscardedPacketsGet(counters), cAtCounterTypeError, countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, notApplicable ? 0 : AtPwCountersRxMalformedPacketsGet(counters),    notApplicable ? cAtCounterTypeNA : cAtCounterTypeError,   countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, notApplicable ? 0 : AtPwCountersRxReorderedPacketsGet(counters),    notApplicable ? cAtCounterTypeNA : cAtCounterTypeNeutral, countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, notApplicable ? 0 : AtPwCountersRxLostPacketsGet(counters),         cAtCounterTypeError,   countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, notApplicable ? 0 : AtPwCountersRxOutOfSeqDropPacketsGet(counters), notApplicable ? cAtCounterTypeNA : cAtCounterTypeError,   countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, notApplicable ? 0 : AtPwCountersRxStrayPacketsGet(counters),        notApplicable ? cAtCounterTypeNA : cAtCounterTypeError,   countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, AtPwCountersRxOamPacketsGet(counters), cAtCounterTypeNeutral, countersValid);
            cntIsSupported = AtChannelCounterIsSupported((AtChannel)pw, cAtPwCounterTypeRxDuplicatedPackets);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, AtPwCountersRxDuplicatedPacketsGet(counters), (!cntIsSupported) ? cAtCounterTypeNA : cAtCounterTypeError, countersValid);

            /* TDM counters */
            notApplicable = IsTdmPw(pwType) ? cAtFalse : cAtTrue;
            tdmCounters = (AtPwTdmCounters)counters;
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, notApplicable ? 0 : AtPwTdmCountersTxLbitPacketsGet(tdmCounters),      notApplicable ? cAtCounterTypeNA : cAtCounterTypeError, countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, notApplicable ? 0 : AtPwTdmCountersTxRbitPacketsGet(tdmCounters),      notApplicable ? cAtCounterTypeNA : cAtCounterTypeError, countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, notApplicable ? 0 : AtPwTdmCountersRxLbitPacketsGet(tdmCounters),      notApplicable ? cAtCounterTypeNA : cAtCounterTypeError, countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, notApplicable ? 0 : AtPwTdmCountersRxRbitPacketsGet(tdmCounters),      notApplicable ? cAtCounterTypeNA : cAtCounterTypeError, countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, notApplicable ? 0 : AtPwTdmCountersRxJitBufOverrunGet(tdmCounters),    notApplicable ? cAtCounterTypeNA : cAtCounterTypeError, countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, notApplicable ? 0 : AtPwTdmCountersRxJitBufUnderrunGet(tdmCounters),   notApplicable ? cAtCounterTypeNA : cAtCounterTypeError, countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, notApplicable ? 0 : AtPwTdmCountersRxLopsGet(tdmCounters),             notApplicable                      ? cAtCounterTypeNA : cAtCounterTypeError, countersValid);
            cntIsSupported = AtChannelCounterIsSupported((AtChannel)pw, cAtPwTdmCounterTypeRxPacketsSentToTdm);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, notApplicable ? 0 : AtPwTdmCountersRxPacketsSentToTdmGet(tdmCounters), (notApplicable || !cntIsSupported) ? cAtCounterTypeNA : cAtCounterTypeGood , countersValid);

            /* CESoP counters */
            notApplicable = (pwType == cAtPwTypeCESoP) ? cAtFalse : cAtTrue;
            cesopCounters = (AtPwCESoPCounters)counters;
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, notApplicable ? 0 : AtPwCESoPCountersTxMbitPacketsGet(cesopCounters), notApplicable ? cAtCounterTypeNA : cAtCounterTypeError, countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, notApplicable ? 0 : AtPwCESoPCountersRxMbitPacketsGet(cesopCounters), notApplicable ? cAtCounterTypeNA : cAtCounterTypeError, countersValid);

            /* CEP counters */
            notApplicable = (pwType == cAtPwTypeCEP) ? cAtFalse : cAtTrue;
            cepCounters = (AtPwCepCounters)counters;
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, notApplicable ? 0 : AtPwCepCountersTxNbitPacketsGet(cepCounters), notApplicable ? cAtCounterTypeNA : cAtCounterTypeNeutral, countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, notApplicable ? 0 : AtPwCepCountersTxPbitPacketsGet(cepCounters), notApplicable ? cAtCounterTypeNA : cAtCounterTypeNeutral, countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, notApplicable ? 0 : AtPwCepCountersRxNbitPacketsGet(cepCounters), notApplicable ? cAtCounterTypeNA : cAtCounterTypeNeutral, countersValid);
            CliCounterPrintToCell(tabPtr, counter_i++, pw_i + 1UL, notApplicable ? 0 : AtPwCepCountersRxPbitPacketsGet(cepCounters), notApplicable ? cAtCounterTypeNA : cAtCounterTypeNeutral, countersValid);

            /* Other counters */
            notApplicable = IsHdlcPw(pwType) ? cAtTrue : cAtFalse;
            ColorStrToCell(tabPtr, counter_i++, pw_i + 1UL, notApplicable ? 0 : NumPktsInJitterBuffer(pw), notApplicable ? cAtCounterTypeNA : cSevInfo);
            }

        /* Show it */
        TablePrint(tabPtr);
        TableFree(tabPtr);
        DestroyHeadings(heading, numPwsPerTable + 1);

        /* For next PWs */
        remainPws = remainPws - numPwsPerTable;
        }

    return cAtTrue;
    }

static AtPwPsn CreatePsnByType(eAtPwPsnType type)
    {
    if (type == cAtPwPsnTypeMpls) return (AtPwPsn)AtPwMplsPsnNew();
    if (type == cAtPwPsnTypeMef)  return (AtPwPsn)AtPwMefPsnNew();
    if (type == cAtPwPsnTypeUdp)  return (AtPwPsn)AtPwUdpPsnNew();
    if (type == cAtPwPsnTypeIPv4) return (AtPwPsn)AtPwIpV4PsnNew();
    if (type == cAtPwPsnTypeIPv6) return (AtPwPsn)AtPwIpV6PsnNew();

    return NULL;
    }

static AtPwPsn BuildPsn(char *psnString)
    {
    eBool isTopPsn = cAtTrue;
    uint8 psnLayer_i;
    eAtPwPsnType psnType[4];
    uint8 numLayers = 0;
    AtPwPsn topPwPsn, currentPsn;

    if (AtStrcmp(psnString, "none") == 0)
        return NULL;

    if (PsnTypeListFromString(psnString, psnType, &numLayers) == cAtFalse)
        return NULL;

    topPwPsn = currentPsn = NULL;
    for (psnLayer_i = 0; psnLayer_i < numLayers; psnLayer_i++)
        {
        AtPwPsn newPsn;

        newPsn = CreatePsnByType(psnType[psnLayer_i]);

        /* Top PSN */
        if (isTopPsn)
            {
            topPwPsn   = newPsn;
            currentPsn = topPwPsn;
            isTopPsn   = cAtFalse;
            }

        /* Lower PSN */
        else
            {
            AtPwPsnLowerPsnSet(currentPsn, newPsn);
            currentPsn = newPsn;
            }
        }

    return topPwPsn;
    }

static uint32 PsnDefaultLabel(AtPw pw, char argc, char** argv, eBool* newPsnIsSetExpectedValue)
    {
    *newPsnIsSetExpectedValue = cAtFalse;

    if (argc <= 2)
        return AtChannelIdGet((AtChannel)pw) + 1;

    *newPsnIsSetExpectedValue = cAtTrue;
    return AtStrToDw(argv[2]);
    }

static void PsnMplsDefaultLabelSet(AtPwPsn psn, uint32 defaultLabel)
    {
    tAtPwMplsLabel label;

    AtPwMplsPsnExpectedLabelSet((AtPwMplsPsn)psn, defaultLabel);
    AtPwMplsPsnInnerLabelSet((AtPwMplsPsn)psn, AtPwMplsLabelMake(defaultLabel, 1, 255, &label));
    }

static eAtModulePwRet PwPsnMplsDefaultSet(AtPwPsn psn, AtPw pw, char argc, char** argv, eBool* newPsnIsSetExpectedValue)
    {
    uint32 defaultLabel = PsnDefaultLabel(pw, argc, argv, newPsnIsSetExpectedValue);

    PsnMplsDefaultLabelSet(psn, defaultLabel);
    return cAtOk;
    }

static void BackupPsnMplsDefaultLabelSet(AtPwPsn psn, uint32 defaultLabel)
    {
    tAtPwMplsLabel label;

    AtPwMplsPsnExpectedLabelSet((AtPwMplsPsn)psn, defaultLabel);
    AtPwMplsPsnInnerLabelGet((AtPwMplsPsn)psn, &label);
    label.label = defaultLabel;
    AtPwMplsPsnInnerLabelSet((AtPwMplsPsn)psn, &label);
    }

static eAtModulePwRet PwBackupPsnMplsDefaultSet(AtPwPsn psn, AtPw pw, char argc, char** argv, eBool* newPsnIsSetExpectedValue)
    {
    uint32 defaultLabel = PsnDefaultLabel(pw, argc, argv, newPsnIsSetExpectedValue);

    BackupPsnMplsDefaultLabelSet(psn, defaultLabel);
    return cAtOk;
    }

static eAtModulePwRet PwPsnMefDefaultSet(AtPwPsn psn, AtPw pw, char argc, char** argv, eBool* newPsnIsSetExpectedValue)
    {
    uint32 defaultLabel = PsnDefaultLabel(pw, argc, argv, newPsnIsSetExpectedValue);
    AtPwPsn lowerPsn = AtPwPsnLowerPsnGet(psn);

    if (AtPwPsnTypeGet(lowerPsn) == cAtPwPsnTypeMpls)
        {
        eAtRet ret = cAtOk;
        AtPwMefPsn mefPsn = (AtPwMefPsn)psn;
        uint8 defaultMac[cAtMacAddressLen] = {0,0,0,0,0,0};
        ret |= AtPwMefPsnDestMacSet(mefPsn, defaultMac);
        ret |= AtPwMefPsnSourceMacSet(mefPsn, defaultMac);
        PsnMplsDefaultLabelSet(lowerPsn, defaultLabel);
        }
    else
        {
        AtPwMefPsnExpectedEcIdSet((AtPwMefPsn)psn, defaultLabel);
        AtPwMefPsnTxEcIdSet((AtPwMefPsn)psn, defaultLabel);
        }

    return cAtOk;
    }

static eAtModulePwRet PwBackupPsnMefDefaultSet(AtPwPsn psn, AtPw pw, char argc, char** argv, eBool* newPsnIsSetExpectedValue)
    {
    uint32 defaultLabel = PsnDefaultLabel(pw, argc, argv, newPsnIsSetExpectedValue);
    AtPwPsn lowerPsn = AtPwPsnLowerPsnGet(psn);

    if (AtPwPsnTypeGet(lowerPsn) == cAtPwPsnTypeMpls)
        {
        BackupPsnMplsDefaultLabelSet(lowerPsn, defaultLabel);
        }
    else
        {
        AtPwMefPsnExpectedEcIdSet((AtPwMefPsn)psn, defaultLabel);
        AtPwMefPsnTxEcIdSet((AtPwMefPsn)psn, defaultLabel);
        }

    return cAtOk;
    }

static eAtModulePwRet PwPsnUdpDefaultSet(AtPwPsn psn, AtPw pw, char argc, char** argv, eBool* newPsnIsSetExpectedValue)
    {
    eAtModulePwRet ret;
    uint32 defaultLabel = PsnDefaultLabel(pw, argc, argv, newPsnIsSetExpectedValue);;
    ret = AtPwUdpPsnSourcePortSet((AtPwUdpPsn)psn, (uint16)defaultLabel);
    ret |= AtPwUdpPsnExpectedPortSet((AtPwUdpPsn)psn, (uint16)defaultLabel);
    return ret;
    }

static eAtModulePwRet PwPsnDefaultSet(AtPwPsn psn, AtPw pw, char argc, char** argv, eBool *newPsnIsSetExpectedValue)
    {
    eAtPwPsnType psnType = AtPwPsnTypeGet(psn);
    if (psnType == cAtPwPsnTypeMpls)
        return PwPsnMplsDefaultSet(psn, pw, argc, argv, newPsnIsSetExpectedValue);

    if (psnType == cAtPwPsnTypeMef)
        return PwPsnMefDefaultSet(psn, pw, argc, argv, newPsnIsSetExpectedValue);

    if (psnType == cAtPwPsnTypeUdp)
        return PwPsnUdpDefaultSet(psn, pw, argc, argv, newPsnIsSetExpectedValue);

    return cAtOk;
    }

static eAtModulePwRet PwBackupPsnDefaultSet(AtPwPsn psn, AtPw pw, char argc, char** argv, eBool *newPsnIsSetExpectedValue)
    {
    eAtPwPsnType psnType = AtPwPsnTypeGet(psn);
    if (psnType == cAtPwPsnTypeMpls)
        return PwBackupPsnMplsDefaultSet(psn, pw, argc, argv, newPsnIsSetExpectedValue);

    if (psnType == cAtPwPsnTypeMef)
        return PwBackupPsnMefDefaultSet(psn, pw, argc, argv, newPsnIsSetExpectedValue);

    if (psnType == cAtPwPsnTypeUdp)
        return PwPsnUdpDefaultSet(psn, pw, argc, argv, newPsnIsSetExpectedValue);

    return cAtOk;
    }

static eBool PwAlarmDefectShow(char argc, char **argv, uint32 (*AlarmGet)(AtChannel self), eBool silent)
    {
    uint16        i;
    uint16        column;
    uint32        numPws;
    tTab          *tabPtr = NULL;
    const char    *heading[] = {"PW", "lops", "jitterOverrun", "jitterUnderun", "LBit", "RBit", "MBit", "NBit", "PBit",
                                "MissingPkt", "ExcessivePktLossRate", "StrayPkt", "MailformPkt", "RemotePktLoss", "MisConnection"};
    AtPw          *pwList;
    static char   idString[32];
	AtUnused(argc);

    pwList = CliPwArrayFromStringGet(argv[0], &numPws);
    if (numPws == 0)
        {
        CliPwPrintHelpString();
        return cAtFalse;
        }

    if (!silent)
        {
    tabPtr = TableAlloc(numPws, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        return cAtFalse;
        }
        }

    for (i = 0; i < numPws; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        uint32 alarms = 0;
        eBool  alarmRaise;
        static const eAtPwAlarmType alarmTypes[] = {cAtPwAlarmTypeLops,
                                                    cAtPwAlarmTypeJitterBufferOverrun,
                                                    cAtPwAlarmTypeJitterBufferUnderrun,
                                                    cAtPwAlarmTypeLBit,
                                                    cAtPwAlarmTypeRBit,
                                                    cAtPwAlarmTypeMBit,
                                                    cAtPwAlarmTypeNBit,
                                                    cAtPwAlarmTypePBit,
                                                    cAtPwAlarmTypeMissingPacket,
                                                    cAtPwAlarmTypeExcessivePacketLossRate,
                                                    cAtPwAlarmTypeStrayPacket,
                                                    cAtPwAlarmTypeMalformedPacket,
                                                    cAtPwAlarmTypeRemotePacketLoss,
                                                    cAtpwAlarmTypeMisConnection};
        uint8 alarm_i;
        AtPw pw = pwList[i];
        eAtPwType pwType = AtPwTypeGet(pw);
        alarms = AlarmGet((AtChannel)pw);

        /* ID */
        column = 0;
        if (mIsPwVirtualCircuitType(pwType))
            AtSprintf(idString, "%s.%d", AtChannelTypeString((AtChannel)pw), AtChannelIdGet((AtChannel)pw));
        else
            AtSprintf(idString, "%s", CliChannelIdStringGet((AtChannel)pw));
        StrToCell(tabPtr, i, column++, idString);

        /* Alarm */
        for (alarm_i = 0; alarm_i < mCount(alarmTypes); alarm_i++)
            {
            if (!AtChannelAlarmIsSupported((AtChannel)pw, alarmTypes[alarm_i]))
                {
                ColorStrToCell(tabPtr, i, column++, sAtNotApplicable, cSevNormal);
                continue;
                }

            alarmRaise = (alarms & alarmTypes[alarm_i]) ? cAtTrue : cAtFalse;
            ColorStrToCell(tabPtr, i, column++, alarmRaise ? "set" : "clear", alarmRaise ? cSevCritical : cSevInfo);
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

static eBool HistoryShow(char argc, char **argv,
                         uint32 (*HistoryClearFunc)(AtChannel self),
                         uint32 (*HistoryGetFunc)(AtChannel self))
    {
    eAtHistoryReadingMode readingMode;
    eBool silent = cAtFalse;

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading action mode. Expected: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    silent = CliHistoryReadingShouldSilent(readingMode, (argc > 2 ) ? argv[2] : NULL);
    if (readingMode == cAtHistoryReadingModeReadToClear)
        return PwAlarmDefectShow(argc, argv, HistoryClearFunc, silent);
    else
        return PwAlarmDefectShow(argc, argv, HistoryGetFunc, silent);
    }

static eBool PwJitterBufferAndPayloadSizeSet(
        char argc, char **argv,
        eAtModulePwRet (*Set)(AtPw, uint32, uint32, uint16))
    {
    AtList pws;
    uint32 jitterBufferSize, jitterDelay;
    uint16 payloadSize;
    uint32 i;
    eBool success = cAtTrue;
    AtUnused(argc);

    /* Get list of PWs */
    pws = CliPwListFromString(argv[0]);
    if (AtListLengthGet(pws) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, list of PWs may be wrong, example: 1,2,5-9\r\n");
        AtObjectDelete((AtObject)pws);
        return cAtTrue;
        }

    /* Get parameters */
    jitterBufferSize = (uint32)AtStrToDw(argv[1]);
    jitterDelay      = (uint32)AtStrToDw(argv[2]);
    payloadSize      = (uint16)AtStrToDw(argv[3]);

    /* Apply for all input PWs */
    for (i = 0; i < AtListLengthGet(pws); i = AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        AtPw pw = (AtPw)AtListObjectGet(pws, i);
        eAtRet ret = Set(pw, jitterBufferSize, jitterDelay, payloadSize);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot configure %s, ret = %s\r\n", CliChannelIdStringGet((AtChannel)pw), AtRet2String(ret));
            success = cAtFalse;
            }
        }

    AtObjectDelete((AtObject)pws);
    return success;
    }

static const char* JitterBufferWatermark2String(AtPw pw, char *buffer, uint32 bufferSize)
    {
    if (!AtPwJitterBufferWatermarkIsSupported(pw))
        return sAtNotSupported;

    AtSnprintf(buffer, bufferSize, "%d/%d", AtPwJitterBufferWatermarkMinPackets(pw), AtPwJitterBufferWatermarkMaxPackets(pw));
    return buffer;
    }

static const char *Alarm2String(uint32 alarms, char *pAlarmString)
    {
    uint16 i;

    /* Initialize string */
    pAlarmString[0] = '\0';
    if (alarms == 0)
        AtSprintf(pAlarmString, "None");

    /* There are alarms */
    else
        {
        for (i = 0; i < mCount(cAtPwAlarmTypeVal) - 1; i++) /* Exclude 'all' */
            {
            if (alarms & (cAtPwAlarmTypeVal[i]))
                AtSprintf(pAlarmString, "%s%s|", pAlarmString, cAtPwAlarmTypeStr[i]);
            }

        if (AtStrlen(pAlarmString) == 0)
            return "None";

        /* Remove the last '|' */
        pAlarmString[AtStrlen(pAlarmString) - 1] = '\0';
        }

    return pAlarmString;
    }

static eBool ExpectedValueIsChanged(AtPwPsn newPsn, AtPwPsn currentPsn)
    {
    eAtPwPsnType pwType = AtPwPsnTypeGet(newPsn);
    if (pwType == cAtPwPsnTypeMef)
        return (AtPwMefPsnExpectedEcIdGet((AtPwMefPsn)newPsn) != AtPwMefPsnExpectedEcIdGet((AtPwMefPsn)currentPsn)) ? cAtTrue : cAtFalse;
    if (pwType == cAtPwPsnTypeMpls)
        return (AtPwMplsPsnExpectedLabelGet((AtPwMplsPsn)newPsn) != AtPwMplsPsnExpectedLabelGet((AtPwMplsPsn)currentPsn)) ? cAtTrue : cAtFalse;
    if (pwType == cAtPwPsnTypeUdp)
        return (AtPwUdpPsnExpectedPortGet((AtPwUdpPsn)newPsn) != AtPwUdpPsnExpectedPortGet((AtPwUdpPsn)currentPsn)) ? cAtTrue : cAtFalse;

    return cAtTrue;
    }

static eBool PwPsnIsChanged(AtPw pw, AtPwPsn newPsn, eBool newPsnIsSetExpectedValue, AtPwPsn (*PsnGet)(AtPw self))
    {
    AtPwPsn currentPsn = PsnGet(pw);
    AtPwPsn lowerPsnOfNew;
    AtPwPsn lowerPsnOfCurrent;

    if (AtPwPsnTypeGet(newPsn) != AtPwPsnTypeGet(currentPsn))
        return cAtTrue;

    if (newPsnIsSetExpectedValue)
        {
        if (ExpectedValueIsChanged(newPsn, currentPsn))
            return cAtTrue;
        }

    lowerPsnOfCurrent = AtPwPsnLowerPsnGet(currentPsn);
    lowerPsnOfNew = AtPwPsnLowerPsnGet(newPsn);

    while (lowerPsnOfCurrent || lowerPsnOfNew)
        {
        if (AtPwPsnTypeGet(lowerPsnOfNew) != AtPwPsnTypeGet(lowerPsnOfCurrent))
            return cAtTrue;

        lowerPsnOfCurrent = AtPwPsnLowerPsnGet(lowerPsnOfCurrent);
        lowerPsnOfNew = AtPwPsnLowerPsnGet(lowerPsnOfNew);
        }

    return cAtFalse;
    }

static void AutotestAlarmChangeState(AtChannel channel, uint32 changedAlarms, uint32 currentStatus, void *userData)
    {
    AtChannelObserver observer = userData;
    AtUnused(channel);
    AtChannelObserverDefectsUpdate(observer, changedAlarms, currentStatus);
    }

static uint32 TdmPwArrayFromString(char *pwIdListString, AtChannel *channels, uint32 bufferSize)
    {
    uint32 *idBuf;
    uint32 numPws, i;
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
    uint32 numChannels = 0;

    idBuf = CliSharedIdBufferGet(&bufferSize);
    numPws = CliIdListFromString(pwIdListString, idBuf, bufferSize);
    if (numPws == 0)
        return 0;

    /* Put all valid PWs */
    for (i = 0; i < numPws; i++)
        {
        AtPw pw = AtModulePwGetPw(pwModule, (idBuf[i] - 1));
        if (pw)
            channels[numChannels++] = (AtChannel)pw;
        }

    return numChannels;
    }

static uint32 HdlcPwArrayFromString(char *pwIdListString, AtChannel *channels, uint32 bufferSize)
    {
    uint32 *idBuf;
    uint32 numPws, i;
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(CliDevice(), cAtModuleEncap);
    uint32 numChannels = 0;

    idBuf = CliSharedIdBufferGet(&bufferSize);
    numPws = CliIdListFromString(pwIdListString, idBuf, bufferSize);
    if (numPws == 0)
        return 0;

    /* Put all valid PWs */
    for (i = 0; i < numPws; i++)
        {
        AtEncapChannel hdlcChannel = AtModuleEncapChannelGet(encapModule, (uint16)(idBuf[i] - 1));

        if (hdlcChannel && AtEncapChannelEncapTypeGet(hdlcChannel) == cAtEncapHdlc)
            {
            channels[numChannels] = (AtChannel)AtChannelBoundPwGet((AtChannel)hdlcChannel);
            if (channels[numChannels])
                numChannels++;
            }
        }

    return numChannels;
    }

static uint32 DccPwArrayFromString(char *pwIdListString, AtChannel *channels, uint32 bufferSize)
    {
    AtList hdlcList = CliAtDccHdlcListFromString(pwIdListString);
    uint32 numberHdlc = AtListLengthGet(hdlcList);
    uint32 i;
    uint32 numChannels = 0;

    if (numberHdlc == 0)
        {
        AtPrintc(cSevCritical, "ERROR: No DCC HDLC, the ID List may be wrong, expect 1.section or 1.line or 1.section|line \r\n");
        AtObjectDelete((AtObject)hdlcList);
        return 0;
        }

    for (i = 0; i < numberHdlc; i++)
        {
        AtHdlcChannel hdlcChannel = (AtHdlcChannel)AtListObjectGet(hdlcList, i);
        AtPw pw = AtChannelBoundPwGet((AtChannel) hdlcChannel);

        /* Try to get PW from HDLC Link */
        if (pw == NULL)
            {
            AtHdlcLink hdlcLink = AtHdlcChannelHdlcLinkGet(hdlcChannel);
            pw = AtChannelBoundPwGet((AtChannel)hdlcLink);
            }

        if (pw)
            {
            channels[numChannels] = (AtChannel)pw;
            numChannels++;
            if (numChannels == bufferSize)
                break;
            }
        }

    AtObjectDelete((AtObject)hdlcList);
    return numChannels;
    }

static uint32 PppPwArrayFromString(char *pwIdListString, AtChannel *channels, uint32 bufferSize)
    {
    uint32 *idBuf;
    uint32 numPws, i;
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(CliDevice(), cAtModuleEncap);
    uint32 numChannels = 0;

    idBuf = CliSharedIdBufferGet(&bufferSize);
    numPws = CliIdListFromString(pwIdListString, idBuf, bufferSize);
    if (numPws == 0)
        return 0;

    /* Put all valid PWs */
    for (i = 0; i < numPws; i++)
        {
        AtEncapChannel hdlcChannel = AtModuleEncapChannelGet(encapModule, (uint16)(idBuf[i] - 1));

        if (hdlcChannel == NULL)
            continue;

        if (AtEncapChannelEncapTypeGet(hdlcChannel) != cAtEncapHdlc)
            continue;

        if (AtHdlcChannelFrameTypeGet((AtHdlcChannel)hdlcChannel) == cAtHdlcFrmPpp)
            {
            AtPppLink pppLink = (AtPppLink)AtHdlcChannelHdlcLinkGet((AtHdlcChannel)hdlcChannel);
            channels[numChannels] = (AtChannel)AtChannelBoundPwGet((AtChannel)pppLink);
            if (channels[numChannels])
                numChannels++;
            }
        }

    return numChannels;
    }

static uint32 FrVcPwArrayFromString(char *idListString, AtChannel *channelList, uint32 bufferSize)
    {
    uint32 i;
    uint32 numChannels = 0;
    AtIdParser idParser = AtIdParserNew(idListString, cTwoLevelsIdVcMinFormatStr, TwoLevelsIdFrVcMaxFormat());

    AtUnused(bufferSize);
    for (i = 0; i < (AtIdParserNumNumbers(idParser) / 2); i++)
        {
        AtHdlcChannel hdlcChannel;
        AtFrVirtualCircuit frvc;
        AtFrLink link;

        hdlcChannel = (AtHdlcChannel)AtModuleEncapChannelGet(CliAtEncapModule(), (uint16)(CliId2DriverId(AtIdParserNextNumber(idParser))));
        if (hdlcChannel == NULL)
            continue;

        if (AtEncapChannelEncapTypeGet((AtEncapChannel)hdlcChannel) != cAtEncapHdlc)
            continue;

        if (AtHdlcChannelFrameTypeGet(hdlcChannel) != cAtHdlcFrmFr)
            continue;

        link = (AtFrLink)AtHdlcChannelHdlcLinkGet(hdlcChannel) ;
        if (link == NULL)
            continue;

        frvc = AtFrLinkVirtualCircuitGet(link, AtIdParserNextNumber(idParser));
        if (frvc != NULL)
            {
            channelList[numChannels] = (AtChannel)AtChannelBoundPwGet((AtChannel)frvc);
            if (channelList[numChannels])
                numChannels++;
            }
        }

    AtObjectDelete((AtObject)idParser);
    return numChannels;
    }

static uint32 MfrVcPwArrayFromString(char *idListString, AtChannel *channelList, uint32 bufferSize)
    {
    uint32 i;
    uint32 numChannels = 0;
    AtIdParser idParser = AtIdParserNew(idListString, cTwoLevelsIdVcMinFormatStr, TwoLevelsIdMfrVcMaxFormat());

    AtUnused(bufferSize);
    for (i = 0; i < (AtIdParserNumNumbers(idParser) / 2); i++)
        {
        AtFrVirtualCircuit mfrVc;
        AtMfrBundle bundle = (AtMfrBundle)AtModuleEncapBundleGet(CliAtEncapModule(), CliId2DriverId(AtIdParserNextNumber(idParser)));
        if (bundle == NULL)
            continue;

        if (AtEncapBundleTypeGet((AtEncapBundle)bundle) != cAtEncapBundleTypeMfr)
            continue;

        mfrVc = AtMfrBundleVirtualCircuitGet(bundle, AtIdParserNextNumber(idParser));
        if (mfrVc != NULL)
            {
            channelList[numChannels] = (AtChannel)AtChannelBoundPwGet((AtChannel)mfrVc);
            if (channelList[numChannels])
                numChannels++;
            }
        }

    AtObjectDelete((AtObject)idParser);
    return numChannels;
    }

static uint32 MpPwArrayFromString(char *idListString, AtChannel *channelList, uint32 bufferSize)
    {
    uint32 *idBuf;
    uint32 numPws, i;
    uint32 numChannels = 0;

    idBuf = CliSharedIdBufferGet(&bufferSize);
    numPws = CliIdListFromString(idListString, idBuf, bufferSize);
    if (numPws == 0)
        return 0;

    for (i = 0; i < numPws; i++)
        {
        AtHdlcBundle bundle = AtModuleEncapBundleGet(CliAtEncapModule(), CliId2DriverId(idBuf[i]));
        if (bundle == NULL)
            continue;

        if (AtEncapBundleTypeGet((AtEncapBundle) bundle) != cAtEncapBundleTypeMlppp)
            continue;

        channelList[numChannels] = (AtChannel)AtChannelBoundPwGet((AtChannel)bundle);
        if (channelList[numChannels])
            numChannels++;
        }

    return numChannels;
    }

static uint32 PwArrayFromString(char *pStrIdList, AtChannel *pwList, uint32 bufferSize)
    {
    char *pDupString;
    char *idType, *idString;
    uint32 numChannels = 0;

    /* Get ID type */
    pDupString = IdTypeAndIdListGet(pStrIdList, &idType, &idString);
    if ((idType == NULL) || (idString == NULL))
        {
        AtOsalMemFree(pDupString);
        return TdmPwArrayFromString(pStrIdList, pwList, bufferSize);
        }

    if (AtStrcmp(idType, sAtChannelTypeEncapHdlc) == 0)
        numChannels = HdlcPwArrayFromString(idString, pwList, bufferSize);

    else if (AtStrcmp(idType, sAtChannelTypePppLink) == 0)
        numChannels = PppPwArrayFromString(idString, pwList, bufferSize);

    else if (AtStrcmp(idType, sAtChannelTypeFrameRelay) == 0)
        numChannels = FrVcPwArrayFromString(idString, pwList, bufferSize);

    else if (AtStrcmp(idType, sAtChannelTypeMultiLinkFrameRelay) == 0)
        numChannels = MfrVcPwArrayFromString(idString, pwList, bufferSize);

    else if (AtStrcmp(idType, sAtChannelTypePppBundle) == 0)
        numChannels = MpPwArrayFromString(idString, pwList, bufferSize);

    else if (AtStrcmp(idType, sAtChannelTypeDcc) == 0)
        numChannels = DccPwArrayFromString(idString, pwList, bufferSize);

    /* Free memory */
    AtOsalMemFree(pDupString);

    return numChannels;
    }

static eBool NoParamSet(char argc, char **argv,
                        eAtRet (*functionHandler)(AtPw self),
                        const char *description)
    {
    AtList pwList;
    uint16 pw_i;
    uint32 numPws;
    eBool success = cAtTrue;

    AtUnused(argc);

    pwList = CliPwListFromString(argv[0]);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    numPws = AtListLengthGet(pwList);
    for (pw_i = 0; pw_i < numPws; pw_i = (uint16)AtCliNextIdWithSleep(pw_i, AtCliLimitNumRestPwGet()))
        {
        AtPw pw = (AtPw)AtListObjectGet(pwList, pw_i);
        eAtRet ret = functionHandler(pw);

        if (ret != cAtOk)
            {
            AtPrintc(cSevWarning, "ERROR: %s failed, ret = %s\r\n", description, AtRet2String(ret));
            success = cAtFalse;
            }
        }

    AtObjectDelete((AtObject)pwList);

    return success;
    }

AtList CliPwListFromString(char *pwIdListString)
    {
    AtList validPws;
    uint32 *idBuf;
    uint32 bufferSize, numPws, i;
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);

    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numPws = CliIdListFromString(pwIdListString, idBuf, bufferSize)) == 0)
        return NULL;

    validPws = AtListCreate(AtModulePwMaxPwsGet(pwModule));
    if (validPws == NULL)
        return NULL;

    /* Put all valid PWs */
    for (i = 0; i < numPws; i++)
        {
        AtPw pw;

        pw = AtModulePwGetPw(pwModule, (uint16)(idBuf[i] - 1));
        if (pw)
            AtListObjectAdd(validPws, (AtObject)pw);
        }

    /* Return NULL if nothing in this list */
    if (AtListLengthGet(validPws) == 0)
        {
        AtObjectDelete((AtObject)validPws);
        return NULL;
        }

    return validPws;
    }

eBool CliPwPsnSet(char argc, char **argv, eAtModulePwRet (*PsnSet)(AtPw self, AtPwPsn psn), AtPwPsn (*PsnGet)(AtPw self))
    {
    uint16 pw_i;
    uint32 numPws;
    AtPwPsn psn;
    AtPw *pwList;
    eBool success = cAtTrue;

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    pwList = CliPwArrayFromStringGet(argv[0], &numPws);
    if (numPws == 0)
        {
        CliPwPrintHelpString();
        return cAtFalse;
        }

    /* Build PSN */
    psn = BuildPsn(argv[1]);
    if ((AtStrcmp(argv[1], "none") != 0) && (psn == NULL))
        {
        AtPrintc(cSevCritical, "ERROR: Invalid PW PSN type, example: mpls.ipv4 or mpls or ipv4");
        return cAtFalse;
        }

    /* Apply this PSN for all input PWs */
    for (pw_i = 0; pw_i < numPws; pw_i = (uint16)AtCliNextIdWithSleep(pw_i, AtCliLimitNumRestPwGet()))
        {
        eAtRet ret;
        AtPw pw = pwList[pw_i];
        eBool newPsnIsSetExpectedValue = cAtFalse;

        /* Assign this new PSN to PW */
        ret = PwPsnDefaultSet(psn, pw, argc, argv, &newPsnIsSetExpectedValue);
        if (!PwPsnIsChanged(pw, psn, newPsnIsSetExpectedValue, PsnGet))
            continue;

        ret |= PsnSet(pw, psn);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot setup PSN for PW %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)pw),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    /* Delete this PSN */
    AtObjectDelete((AtObject)psn);

    return success;
    }

static eBool CliPwPrimPsnIsReady(AtPw pw, AtPwPsn bkPsn)
    {
    AtPwPsn priPsn = AtPwPsnGet(pw);

    if (priPsn == NULL)
        return cAtFalse;

    if (AtPwPsnTypeGet(priPsn) != AtPwPsnTypeGet(bkPsn))
        return cAtFalse;

    return cAtTrue;
    }

eBool CliPwBackupPsnSet(char argc, char **argv, eAtModulePwRet (*PsnSet)(AtPw self, AtPwPsn psn), AtPwPsn (*PsnGet)(AtPw self))
    {
    uint16 pw_i;
    uint32 *idBuf;
    uint32 bufferSize;
    uint32 numPws;
    AtModulePw pwModule;
    AtPwPsn psn;
    eBool success = cAtTrue;

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numPws = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    /* Build PSN */
    psn = BuildPsn(argv[1]);
    if ((AtStrcmp(argv[1], "none") != 0) && (psn == NULL))
        {
        AtPrintc(cSevCritical, "ERROR: Invalid PW PSN type, example: mpls.ipv4 or mpls or ipv4");
        return cAtFalse;
        }

    /* Apply this PSN for all input PWs */
    pwModule = (AtModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
    for (pw_i = 0; pw_i < numPws; pw_i = (uint16)AtCliNextIdWithSleep(pw_i, AtCliLimitNumRestPwGet()))
        {
        eAtRet ret = cAtOk;
        AtPw pw;
        eBool newPsnIsSetExpectedValue = cAtFalse;

        /* Only handle valid PWs */
        pw = AtModulePwGetPw(pwModule, (uint16)(idBuf[pw_i] - 1));
        if (pw == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: Ignore not exist PW %u\r\n", idBuf[pw_i]);
            continue;
            }

        /* Assign this new PSN to PW */
        if (!CliPwPrimPsnIsReady(pw, psn))
            {
            AtPrintc(cSevWarning, "WARNING: Ignore mismatched primary PSN %u\r\n", idBuf[pw_i]);
            continue;
            }
        else
            {
            AtPwPsn priPsn = AtPwPsnGet(pw), bkPsn = (AtPwPsn)AtObjectClone((AtObject)priPsn);
            ret = PwBackupPsnDefaultSet(bkPsn, pw, argc, argv, &newPsnIsSetExpectedValue);
            if (ret == cAtOk && PwPsnIsChanged(pw, bkPsn, newPsnIsSetExpectedValue, PsnGet))
                {
                ret |= PsnSet(pw, bkPsn);
                if (ret != cAtOk)
                    {
                    AtPrintc(cSevCritical, "ERROR: Cannot setup PSN for PW %s, ret = %s\r\n",
                             CliChannelIdStringGet((AtChannel)pw),
                             AtRet2String(ret));
                    success = cAtFalse;
                    }
                }
            /* Delete this PSN */
            AtObjectDelete((AtObject)bkPsn);
            }

        }

    /* Delete this PSN */
    AtObjectDelete((AtObject)psn);

    return success;
    }

eBool CliPwEthHeaderSet(AtPw *pwList, uint32 numPws, char *pMacStr, char *pCVlanStr, char *pSVlanStr, eAtModulePwRet (*EthHeaderSet)(AtPw, uint8 *, const tAtEthVlanTag *, const tAtEthVlanTag *))
    {
    uint16 i;
    tAtEthVlanDesc desc;
    uint8 mac[cAtMacAddressLen];
    tAtEthVlanTag *sVlan, *cVlan;
    eBool success = cAtTrue;

    AtOsalMemInit(&desc, 0, sizeof(desc));
    AtOsalMemInit(mac, 0, cAtMacAddressLen);

    /* Get MAC */
    if (!CliAtModuleEthMacAddrStr2Byte(pMacStr, mac))
        {
        AtPrintc(cSevWarning, "WARNING: MAC is invalid!!!\n");
        return cAtFalse;
        }

    /* Get S-VLAN and C-VLAN */
    sVlan = cVlan = NULL;
    if (!CliEthFlowVlanGet(pCVlanStr, pSVlanStr, &desc))
        {
        AtPrintc(cSevCritical, "ERROR: input VLAN (CVLAN: %s, SVLAN: %s may be invalid\r\n",
                 pCVlanStr ? pCVlanStr : "none",
                 pSVlanStr ? pSVlanStr : "none");
        return cAtFalse;
        }

    if (desc.numberOfVlans > 0)
        {
        cVlan = &(desc.vlans[0]);
        if (desc.numberOfVlans == 2)
            sVlan = &(desc.vlans[1]);
        }

    if ((cVlan == NULL) && (AtOsalMemCmp(pSVlanStr, "none", 4)))
        {
        AtPrintc(cSevCritical, "ERROR: Input Vlan invalid. Can not configure for Vlan\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numPws; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        eAtRet ret = cAtOk;
        AtPw pw = pwList[i];

        /* Apply new header */
        ret = EthHeaderSet(pw, mac, cVlan, sVlan);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Can not configure for %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)pw),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CliPwBoolAttributeSet(char argc, char **argv, eBool boolValue, PwBoolAttributeSet PwAttributeEnabling)
    {
    AtUnused(argc);
    return CliPwAttributeSet(argv[0], boolValue, (PwAttributeSetFunc)PwAttributeEnabling);
    }

char *CliPwPsnChainString(AtPwPsn psn)
    {
    eBool  blResult;
    static char psnChainStrBuf[32];
    static char psnLayerStrBuf[5];
    AtPwPsn currentPsn = psn;

    if (psn == NULL)
        {
        AtSprintf(psnChainStrBuf, "none");
        return psnChainStrBuf;
        }

    AtOsalMemInit(psnChainStrBuf, 0, sizeof(psnChainStrBuf));
    AtOsalMemInit(psnLayerStrBuf, 0, sizeof(psnLayerStrBuf));

    while (currentPsn != NULL)
        {
        mAtEnumToStr(cAtPsnTypeStr, cAtPsnTypeVal, AtPwPsnTypeGet(currentPsn), psnLayerStrBuf, blResult);
        if (!blResult)
            return psnChainStrBuf;

        currentPsn = AtPwPsnLowerPsnGet(currentPsn);
        AtStrcat(psnChainStrBuf, ".");
        AtStrcat(psnChainStrBuf, psnLayerStrBuf);
        }

    return &psnChainStrBuf[1];
    }

AtPw* CliPwArrayFromStringGet(char* arg, uint32 *numberPws)
    {
    uint32 bufferSize;
    AtPw* pws = (AtPw *)CliSharedChannelListGet(&bufferSize);
    *numberPws = PwArrayFromString(arg, (AtChannel *)((void**)pws), bufferSize);

    return pws;
    }

AtPw* CliPppPwArrayFromString(char* arg, uint32 *numberPws)
    {
    uint32 bufferSize;
    AtPw* pws = (AtPw *)CliSharedChannelListGet(&bufferSize);
    *numberPws = PppPwArrayFromString(arg, (AtChannel *)((void**)pws), bufferSize);

    return pws;
    }

AtPw* CliHdlcPwArrayFromString(char* arg, uint32 *numberPws)
    {
    uint32 bufferSize;
    AtPw* pws = (AtPw *)CliSharedChannelListGet(&bufferSize);
    *numberPws = HdlcPwArrayFromString(arg, (AtChannel *)((void**)pws), bufferSize);

    return pws;
    }

eBool CmdPwGet(char argc, char **argv)
    {
    AtPw          pw;
    AtPw*         pwList;
    uint16        i;
    uint16        column;
    eBool         blResult;
    uint32        numPws;
    AtPwPsn       psn;
    static char   buf[32];
    tTab          *tabPtr;
    uint8         queueId;
    const char    *heading[] = {"PW",

                                /* Common */
                                "circuit", "enabled", "payload", "jitterBuffer(us)", "reorder",
                                "suppress", "psn", "label", "controlWord", "rtp", "idleCode",
                                "interruptMask", "Queue", "LOPS replaceMode",

                                /* CEP */
                                "ebm",
                                "epar",
                                "cepMode",

                                /* CESoP */
                                "cesopMode"};
	AtUnused(argc);

    pwList = CliPwArrayFromStringGet(argv[0], &numPws);
    if (numPws == 0)
        {
        CliPwPrintHelpString();
        return cAtFalse;
        }

    tabPtr = TableAlloc(numPws, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numPws; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        eBool enabled;
        eAtPwType pwType;
        AtChannel attachedCircuit;
        uint32 intrMaskVal;
        static char alarmString[1024];
        static char idString[32];
        const char *stringValue;

        column = 0;
        pw = (AtPw)pwList[i];
        pwType = AtPwTypeGet(pw);

        if (mIsPwVirtualCircuitType(pwType))
            AtSprintf(idString, "%s.%d", AtChannelTypeString((AtChannel)pw), AtChannelIdGet((AtChannel)pw));
        else
            AtSprintf(idString, "%s", CliChannelIdStringGet((AtChannel)pw));

        StrToCell(tabPtr, i, column++, idString);

        /* Attachment circuit */
        attachedCircuit = AtPwBoundCircuitGet(pw);
        if (attachedCircuit == NULL )
            AtSprintf(buf, "none");
        else if (mIsPwVirtualCircuitType(pwType))
            AtSprintf(buf, "%s.%d", AtChannelTypeString((AtChannel)attachedCircuit), AtChannelIdGet((AtChannel)attachedCircuit));
        else
            AtSprintf(buf, "%s", CliChannelIdStringGet(attachedCircuit));
        StrToCell(tabPtr, i, column++, buf);

        /* Enabling */
        enabled = AtChannelIsEnabled((AtChannel)pw);
        mAtBoolToStr(enabled, buf, blResult);
        ColorStrToCell(tabPtr, i, column++, buf, enabled ? cSevInfo : cSevCritical);

        if (AtPwIsTdmPw(pw))
            {
        /* Payload */
        mPutConfigToCell(AtPwPayloadSizeGet(pw));

        /* Jitter */
        mPutConfigToCell(AtPwJitterBufferSizeGet(pw));

        /* Reodering */
        enabled = AtPwReorderingIsEnabled(pw);
        mAtBoolToStr(enabled, buf, blResult);
        ColorStrToCell(tabPtr, i, column++, buf, enabled ? cSevInfo : cSevCritical);
            }
        else
            {
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            }

        /* Suppress */
        enabled = AtPwSuppressIsEnabled(pw);
        mAtBoolToStr(enabled, buf, blResult);
        ColorStrToCell(tabPtr, i, column++, buf, enabled ? cSevInfo : cSevCritical);

        /* PSN */
        psn = AtPwPsnGet(pw);
        StrToCell(tabPtr, i, column++, CliPwPsnChainString(psn));

        /* Label */
        StrToCell(tabPtr, i, column++, PsnExpectedLabelGet(psn));

        /* Control word */
        enabled = AtPwCwIsEnabled(pw);
        mAtBoolToStr(enabled, buf, blResult);
        StrToCell(tabPtr, i, column++, buf);

        /* RTP */
        enabled = AtPwRtpIsEnabled(pw);
        mAtBoolToStr(enabled, buf, blResult);
        StrToCell(tabPtr, i, column++, buf);

        /* IDLE code */
        StrToCell(tabPtr, i, column++, CliNumber2String(AtPwIdleCodeGet(pw), "0x%02x"));

        /* Interrupt mask */
        intrMaskVal = AtChannelInterruptMaskGet((AtChannel)pw);
        StrToCell(tabPtr, i, column++, Alarm2String(intrMaskVal, alarmString));

        /* Queue */
        queueId = AtPwEthPortQueueGet(pw);
        StrToCell(tabPtr, i, column++, (queueId == cAtInvalidPwEthPortQueueId) ? sAtNotApplicable : CliNumber2String(queueId + 1, "%u"));

        /* LOPS replace mode */
        stringValue = CliEnumToString(AtPwLopsPktReplaceModeGet(pw), cAtPwCwReplaceStr, cAtPwCwReplaceVal, mCount(cAtPwCwReplaceVal), NULL);
        ColorStrToCell(tabPtr, i, column++, stringValue ? stringValue : "Error", stringValue ? cSevNormal : cSevCritical);

        /* CEP */
        if (pwType == cAtPwTypeCEP)
            {
            enabled = AtPwCepCwEbmIsEnabled((AtPwCep)pw);
            mAtBoolToStr(enabled, buf, blResult);
            ColorStrToCell(tabPtr, i, column++, buf, enabled ? cSevInfo : cSevCritical);

            enabled = AtPwCepEparIsEnabled((AtPwCep)pw);
            mAtBoolToStr(enabled, buf, blResult);
            ColorStrToCell(tabPtr, i, column++, buf, enabled ? cSevInfo : cSevCritical);

            mAtEnumToStr(cAtPwCepModeStr, cAtPwCepModeVal, AtPwCepModeGet((AtPwCep)pw), buf, blResult);
            StrToCell(tabPtr, i, column++, (blResult) ? buf : sAtNotApplicable);
            }
        else
            {
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            }
            
        /* CESoP */
        if (pwType == cAtPwTypeCESoP)
            {
            mAtEnumToStr(cAtPwCESoPModeStr, cAtPwCESoPModeVal, AtPwCESoPModeGet((AtPwCESoP)pw), buf, blResult);
            StrToCell(tabPtr, i, column++, (blResult) ? buf : sAtNotApplicable);
            }
        else
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CliPwPsnShow(char argc, char **argv,
                   AtPwPsn (*PsnGet)(AtPw self),
                   eAtModulePwRet (*DestMacGet)(AtPw self, uint8 *destMac),
                   eAtModulePwRet (*SourceMacGet)(AtPw self, uint8 *srcMac),
                   tAtEthVlanTag *(*CVlanGet)(AtPw self, tAtEthVlanTag *cVlan),
                   tAtEthVlanTag *(*SVlanGet)(AtPw self, tAtEthVlanTag *sVlan),
                   uint16 (*CVlanTpidGet)(AtPw self),
                   uint16 (*SVlanTpidGet)(AtPw self))
    {
    AtPw          *pwList;
    uint16        i;
    uint16        column;
    uint32        numPws;
    static uint8  mac[6];
    static char   buf[32];
    tAtEthVlanTag cVlan, sVlan;
    tTab          *tabPtr;
    const char    *heading[] = {"PW", "psn", "vlan", "ethPort", "DMAC", "SMAC", "CVlan TPID", "SVlan TPID", "ethFlow"};
	AtUnused(argc);

    pwList = CliPwArrayFromStringGet(argv[0], &numPws);
    if (numPws == 0)
        {
        CliPwPrintHelpString();
        return cAtFalse;
        }

    tabPtr = TableAlloc(numPws, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        return cAtFalse;
        }

    for (i = 0; i < numPws; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        AtPw    pw;
        AtPwPsn psn;
        AtEthPort ethPort;
        AtEthFlow ethFlow;
        eAtPwType type;
        char idString[128];
        uint16 vlanTpid;

        column = 0;
        pw = pwList[i];

        type = AtPwTypeGet(pw);

        if (mIsPwVirtualCircuitType(type))
            AtSprintf(idString, "%s.%d", AtChannelTypeString((AtChannel)pw), AtChannelIdGet((AtChannel)pw));
        else
            AtSprintf(idString, "%s", CliChannelIdStringGet((AtChannel)pw));

        StrToCell(tabPtr, i, column++, idString);

        /* PSN */
        psn = PsnGet(pw);

        StrToCell(tabPtr, i, column++, CliPwPsnChainString(psn));
        StrToCell(tabPtr, i, column++, CliVlans2Str(CVlanGet(pw, &cVlan), SVlanGet(pw, &sVlan)));
        ethPort = AtPwEthPortGet(pw);
        if (ethPort == NULL)
            StrToCell(tabPtr, i, column++, "none");
        else
            mPutConfigToCell(AtChannelIdGet((AtChannel)ethPort) + 1);

        AtOsalMemInit(mac, 0, sizeof(mac));
        DestMacGet(pw, mac);
        StrToCell(tabPtr, i, column++, AtBytes2String(mac, 6, 16));
        AtOsalMemInit(mac, 0, sizeof(mac));
        SourceMacGet(pw, mac);
        StrToCell(tabPtr, i, column++, AtBytes2String(mac, 6, 16));

        /* Vlan Tpid */
        if ((vlanTpid = CVlanTpidGet(pw)) != 0)
            AtSprintf(buf, "0x%04X", vlanTpid);
        else
            AtSprintf(buf, "N/A");
        StrToCell(tabPtr, i, column++, buf);

        if ((vlanTpid = SVlanTpidGet(pw)) != 0)
            AtSprintf(buf, "0x%04X", vlanTpid);
        else
            AtSprintf(buf, "N/A");
        StrToCell(tabPtr, i, column++, buf);

        ethFlow = AtPwEthFlowGet(pw);
        if (ethFlow == NULL)
            StrToCell(tabPtr, i, column++, "none");
        else
            mPutConfigToCell(AtChannelIdGet((AtChannel)ethFlow) + 1);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdPwPsnShow(char argc, char **argv)
    {
    return CliPwPsnShow(argc,
                        argv,
                        AtPwPsnGet,
                        AtPwEthDestMacGet,
                        AtPwEthSrcMacGet,
                        AtPwEthCVlanGet,
                        AtPwEthSVlanGet,
                        AtPwEthCVlanTpidGet,
                        AtPwEthSVlanTpidGet);
    }

eBool CmdPwRtpGet(char argc, char **argv)
    {
    AtList pwList;
    uint16 i;
    uint16 column;
    uint32 numPws;
    tTab *tabPtr;
    uint32 bufferSize;
    char *buf = CliSharedCharBufferGet(&bufferSize);
    const char *heading[] = {"PW", "enabled", "payloadType", "SSRC", "txPayloadType", "expPayloadType", "payloadTypeCheck", "txSsrc", "expSsrc", "ssrcCheck", "timeStampMode"};

	AtUnused(argc);

    pwList = CliPwListFromString(argv[0]);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    numPws = AtListLengthGet(pwList);

    tabPtr = TableAlloc(numPws, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        AtObjectDelete((AtObject)pwList);
        return cAtFalse;
        }

    for (i = 0; i < numPws; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        eBool rtpEnabled, payloadTypeCompared, ssrcCompared;
        AtPw pw = (AtPw)AtListObjectGet(pwList, i);
        eAtPwType pwType = AtPwTypeGet(pw);
        const char *stringValue;

        /* PW ID */
        column = 0;
        if (mIsPwVirtualCircuitType(pwType))
            AtSprintf(buf, "%s.%d", AtChannelTypeString((AtChannel)pw), AtChannelIdGet((AtChannel)pw));
        else
            AtSprintf(buf, "%s", CliChannelIdStringGet((AtChannel)pw));
        StrToCell(tabPtr, i, column++, buf);

        /* RTP enabling */
        rtpEnabled = AtPwRtpIsEnabled(pw);
        ColorStrToCell(tabPtr, i, column++, CliBoolToString(rtpEnabled), rtpEnabled ? cSevInfo : cSevCritical);

        /* FIXME: Payload type and SSRC. This is to be compatible with autotest module
         * which is still using old table header. The following 2 lines code
         * should be removed after autotest module is updated. */
        StrToCell(tabPtr, i, column++, CliNumber2String(AtPwRtpPayloadTypeGet(pw), "0x%x"));
        StrToCell(tabPtr, i, column++, CliNumber2String(AtPwRtpSsrcGet(pw), "0x%x"));

        /* Payload type */
        StrToCell(tabPtr, i, column++, CliNumber2String(AtPwRtpTxPayloadTypeGet(pw), "0x%x"));
        StrToCell(tabPtr, i, column++, CliNumber2String(AtPwRtpExpectedPayloadTypeGet(pw), "0x%x"));
        payloadTypeCompared = AtPwRtpPayloadTypeIsCompared(pw);
        ColorStrToCell(tabPtr, i, column++, CliBoolToString(payloadTypeCompared), payloadTypeCompared ? cSevInfo : cSevCritical);

        /* SSRC */
        StrToCell(tabPtr, i, column++, CliNumber2String(AtPwRtpTxSsrcGet(pw), "0x%x"));
        StrToCell(tabPtr, i, column++, CliNumber2String(AtPwRtpExpectedSsrcGet(pw), "0x%x"));
        ssrcCompared = AtPwRtpSsrcIsCompared(pw);
        ColorStrToCell(tabPtr, i, column++, CliBoolToString(ssrcCompared), ssrcCompared ? cSevInfo : cSevCritical);

        /* Timestamp */
        stringValue = CliEnumToString(AtPwRtpTimeStampModeGet(pw),
                                      cAtPwRtpTimeStampStr, cAtPwRtpTimeStampVal,
                                      mCount(cAtPwRtpTimeStampVal), NULL);
        ColorStrToCell(tabPtr, i, column++, stringValue ? stringValue : "Error", stringValue ? cSevNormal : cSevCritical);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtObjectDelete((AtObject)pwList);

    return cAtTrue;
    }

eBool CmdPwControlWordGet(char argc, char **argv)
    {
    AtPw          pw;
    AtList        pwList;
    uint16        i;
    uint16        column;
    uint32        numPws;
    tTab          *tabPtr;
    const char  *heading[] = {"PW", "enabled", "autoTxLbit", "autoRxLbit", "autoRbit", "autoTxMbit", "autoRxMbit",
                              "autoNbit", "autoPbit", "lengthMode", "replaceMode", "sequenceMode"};
	AtUnused(argc);

    pwList = CliPwListFromString(argv[0]);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    numPws = AtListLengthGet(pwList);

    tabPtr = TableAlloc(numPws, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        AtObjectDelete((AtObject)pwList);
        return cAtFalse;
        }

    for (i = 0; i < numPws; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        eBool cwEnabled;
        const char *stringValue;

        column = 0;
        pw = (AtPw)AtListObjectGet(pwList, i);

        StrToCell(tabPtr, i, column++, CliChannelIdStringGet((AtChannel)pw));

        /* CW enabling */
        cwEnabled = AtPwCwIsEnabled(pw);
        ColorStrToCell(tabPtr, i, column++, CliBoolToString(cwEnabled), cwEnabled ? cSevInfo : cSevCritical);

        /* CW configuration detail */
        cwEnabled = AtPwCwAutoTxLBitIsEnabled(pw);
        ColorStrToCell(tabPtr, i, column++, CliBoolToString(cwEnabled), cwEnabled ? cSevInfo : cSevCritical);

        cwEnabled = AtPwCwAutoRxLBitIsEnabled(pw);
        ColorStrToCell(tabPtr, i, column++, CliBoolToString(cwEnabled), cwEnabled ? cSevInfo : cSevCritical);

        cwEnabled = AtPwCwAutoRBitIsEnabled(pw);
        ColorStrToCell(tabPtr, i, column++, CliBoolToString(cwEnabled), cwEnabled ? cSevInfo : cSevCritical);

        if (AtPwTypeGet(pw) != cAtPwTypeCESoP)
            {
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            }
        else
            {
            AtPwCESoP cesop = (AtPwCESoP)pw;
            eBool enabled = AtPwCESoPCwAutoTxMBitIsEnabled(cesop);

            ColorStrToCell(tabPtr, i, column++, CliBoolToString(enabled), enabled ? cSevInfo : cSevCritical);

            if (AtPwCESoPCwAutoRxMBitIsConfigurable(cesop))
                {
                eBool rxEnabled = AtPwCESoPCwAutoRxMBitIsEnabled(cesop);
                ColorStrToCell(tabPtr, i, column++, CliBoolToString(rxEnabled), rxEnabled ? cSevInfo : cSevCritical);
                }
            else
                StrToCell(tabPtr, i, column++, sAtNotSupported);
            }

        if (AtPwTypeGet(pw) != cAtPwTypeCEP)
            {
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            StrToCell(tabPtr, i, column++, sAtNotApplicable);
            }
        else
            {
            cwEnabled = AtPwCepCwAutoNBitIsEnabled((AtPwCep)pw);
            ColorStrToCell(tabPtr, i, column++, CliBoolToString(cwEnabled), cwEnabled ? cSevInfo : cSevCritical);

            cwEnabled = AtPwCepCwAutoPBitIsEnabled((AtPwCep)pw);
            ColorStrToCell(tabPtr, i, column++, CliBoolToString(cwEnabled), cwEnabled ? cSevInfo : cSevCritical);
            }

        stringValue = CliEnumToString(AtPwCwLengthModeGet(pw), cAtPwCwLengthStr, cAtPwCwLengthVal, mCount(cAtPwCwLengthVal), NULL);
        ColorStrToCell(tabPtr, i, column++, stringValue ? stringValue : "Error", stringValue ? cSevNormal : cSevCritical);

        stringValue = CliEnumToString(AtPwCwPktReplaceModeGet(pw), cAtPwCwReplaceStr, cAtPwCwReplaceVal, mCount(cAtPwCwReplaceVal), NULL);
        ColorStrToCell(tabPtr, i, column++, stringValue ? stringValue : "Error", stringValue ? cSevNormal : cSevCritical);

        stringValue = CliEnumToString(AtPwCwSequenceModeGet(pw), cAtPwCwSequenceStr, cAtPwCwSequenceVal, mCount(cAtPwCwSequenceVal), NULL);
        ColorStrToCell(tabPtr, i, column++, stringValue ? stringValue : "Error", stringValue ? cSevNormal : cSevCritical);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtObjectDelete((AtObject)pwList);

    return cAtTrue;
    }

eBool CmdPwLopSetThresSet(char argc, char **argv)
    {
    return IntegerAttributeSet(argc, argv, (PwAttributeSetFunc)(AtPwLopsSetThresholdSet));
    }

eBool CmdPwLopClearThresSet(char argc, char **argv)
    {
    return IntegerAttributeSet(argc, argv, (PwAttributeSetFunc)(AtPwLopsClearThresholdSet));
    }

eBool CmdPwPayloadSizeSet(char argc, char **argv)
    {
    return IntegerAttributeSet(argc, argv, (PwAttributeSetFunc)(AtPwPayloadSizeSet));
    }

eBool CmdPwPrioritySet(char argc, char **argv)
    {
    return IntegerAttributeSet(argc, argv, (PwAttributeSetFunc)(AtPwPrioritySet));
    }

eBool CmdPwReorderEn(char argc, char **argv)
    {
    AtUnused(argc);
    return CliPwAttributeSet(argv[0], cAtTrue, (PwAttributeSetFunc)(AtPwReorderingEnable));
    }

eBool CmdPwReorderDis(char argc, char **argv)
    {
    AtUnused(argc);
    return CliPwAttributeSet(argv[0], cAtFalse, (PwAttributeSetFunc)(AtPwReorderingEnable));
    }

eBool CmdPwJitterBufferSet(char argc, char **argv)
    {
    AtUnused(argc);
    return CliPwAttributeSet(argv[0], (uint32)AtStrToDw(argv[1]), (PwAttributeSetFunc)(AtPwJitterBufferSizeSet));
    }

eBool CmdPwJitterDelaySet(char argc, char **argv)
    {
	AtUnused(argc);
	return CliPwAttributeSet(argv[0], (uint32)AtStrToDw(argv[1]), (PwAttributeSetFunc)(AtPwJitterBufferDelaySet));
    }

eBool CmdPwRtpEn(char argc, char **argv)
    {
    return CliPwBoolAttributeSet(argc, argv, cAtTrue, (PwBoolAttributeSet)AtPwRtpEnable);
    }

eBool CmdPwRtpDis(char argc, char **argv)
    {
    return CliPwBoolAttributeSet(argc, argv, cAtFalse, (PwBoolAttributeSet)AtPwRtpEnable);
    }

eBool CmdPwRtpPayloadTypeSet(char argc, char **argv)
    {
    return IntegerAttributeSet(argc, argv, (PwAttributeSetFunc)(AtPwRtpPayloadTypeSet));
    }

eBool CmdPwRtpSsrcSet(char argc, char **argv)
    {
    return IntegerAttributeSet(argc, argv, (PwAttributeSetFunc)(AtPwRtpSsrcSet));
    }

eBool CmdPwRtpTxSsrcSet(char argc, char **argv)
    {
    return IntegerAttributeSet(argc, argv, (PwAttributeSetFunc)(AtPwRtpTxSsrcSet));
    }

eBool CmdPwRtpExpectedSsrcSet(char argc, char **argv)
    {
    return IntegerAttributeSet(argc, argv, (PwAttributeSetFunc)(AtPwRtpExpectedSsrcSet));
    }

eBool CmdPwRtpSsrcCompare(char argc, char **argv)
    {
    return CliPwBoolAttributeSet(argc, argv, CliBoolFromString(argv[1]), (PwBoolAttributeSet)AtPwRtpSsrcCompare);
    }

eBool CmdPwRtpTxPayloadTypeSet(char argc, char **argv)
    {
    return IntegerAttributeSet(argc, argv, (PwAttributeSetFunc)(AtPwRtpTxPayloadTypeSet));
    }

eBool CmdPwRtpExpectedPayloadTypeSet(char argc, char **argv)
    {
    return IntegerAttributeSet(argc, argv, (PwAttributeSetFunc)(AtPwRtpExpectedPayloadTypeSet));
    }

eBool CmdPwRtpPayloadTypeCompare(char argc, char **argv)
    {
	return CliPwBoolAttributeSet(argc, argv, CliBoolFromString(argv[1]), (PwBoolAttributeSet)AtPwRtpPayloadTypeCompare);
    }

eBool CmdPwRtpTimeStampSet(char argc, char **argv)
    {
    eAtPwRtpTimeStampMode timeStamp = cAtPwRtpTimeStampModeInvalid;
    eBool convertSuccess = cAtTrue;
	AtUnused(argc);

    mAtStrToEnum(cAtPwRtpTimeStampStr, cAtPwRtpTimeStampVal, argv[1], timeStamp, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid RTP Time-Stamp Type\r\n");
        return cAtFalse;
        }

    return CliPwAttributeSet(argv[0], timeStamp, (PwAttributeSetFunc)(AtPwRtpTimeStampModeSet));
    }

eBool CmdPwSupressEn(char argc, char **argv)
    {
    return CliPwBoolAttributeSet(argc, argv, cAtTrue, (PwBoolAttributeSet)AtPwSuppressEnable);
    }

eBool CmdPwSupressDis(char argc, char **argv)
    {
    return CliPwBoolAttributeSet(argc, argv, cAtFalse, (PwBoolAttributeSet)AtPwSuppressEnable);
    }

eBool CmdPwControlWordEn(char argc, char **argv)
    {
    return CliPwBoolAttributeSet(argc, argv, cAtTrue, (PwBoolAttributeSet)AtPwCwEnable);
    }

eBool CmdPwControlWordDis(char argc, char **argv)
    {
    return CliPwBoolAttributeSet(argc, argv, cAtFalse, (PwBoolAttributeSet)AtPwCwEnable);
    }

eBool CmdPwAutoTxLbitEn(char argc, char **argv)
    {
    return CliPwBoolAttributeSet(argc, argv, cAtTrue, (PwBoolAttributeSet)AtPwCwAutoTxLBitEnable);
    }

eBool CmdPwAutoTxLbitDis(char argc, char **argv)
    {
    return CliPwBoolAttributeSet(argc, argv, cAtFalse, (PwBoolAttributeSet)AtPwCwAutoTxLBitEnable);
    }

eBool CmdPwAutoRxLbitEn(char argc, char **argv)
    {
    return CliPwBoolAttributeSet(argc, argv, cAtTrue, (PwBoolAttributeSet)AtPwCwAutoRxLBitEnable);
    }

eBool CmdPwAutoRxLbitDis(char argc, char **argv)
    {
    return CliPwBoolAttributeSet(argc, argv, cAtFalse, (PwBoolAttributeSet)AtPwCwAutoRxLBitEnable);
    }

eBool CmdPwAutoRBitEn(char argc, char **argv)
    {
    return CliPwBoolAttributeSet(argc, argv, cAtTrue, (PwBoolAttributeSet)AtPwCwAutoRBitEnable);
    }

eBool CmdPwAutoRBitDis(char argc, char **argv)
    {
    return CliPwBoolAttributeSet(argc, argv, cAtFalse, (PwBoolAttributeSet)AtPwCwAutoRBitEnable);
    }

eBool CmdPwEnable(char argc, char **argv)
    {
    return CliPwBoolAttributeSet(argc, argv, cAtTrue, (eAtRet (*)(AtPw, eBool))AtChannelEnable);
    }

eBool CmdPwDisable(char argc, char **argv)
    {
    return CliPwBoolAttributeSet(argc, argv, cAtFalse, (eAtRet (*)(AtPw, eBool))AtChannelEnable);
    }

eBool CmdPwCwSequenceSet(char argc, char **argv)
    {
    eBool convertSuccess = cAtFalse;
    eAtPwCwSequenceMode sequence = cAtPwCwSequenceModeInvalid;
	AtUnused(argc);

    /* Get sequence */
    mAtStrToEnum(cAtPwCwSequenceStr, cAtPwCwSequenceVal, argv[1], sequence, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid Control-Word Sequence Mode");
        return cAtFalse;
        }

    return CliPwAttributeSet(argv[0], sequence, (PwAttributeSetFunc)(AtPwCwSequenceModeSet));
    }

eBool CmdPwCwLengthSet(char argc, char **argv)
    {
    eBool convertSuccess = cAtFalse;
    eAtPwCwLengthMode length = cAtPwCwLengthModeInvalid;
	AtUnused(argc);

    mAtStrToEnum(cAtPwCwLengthStr, cAtPwCwLengthVal, argv[1], length, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid Control-Word Length Mode\r\n");
        return cAtFalse;
        }

    return CliPwAttributeSet(argv[0], length, (PwAttributeSetFunc)(AtPwCwLengthModeSet));
    }

eBool CmdPwCwReplaceModeSet(char argc, char **argv)
    {
    eBool convertSuccess = cAtFalse;
    eAtPwPktReplaceMode replaceMode = cAtPwPktReplaceModeInvalid;
	AtUnused(argc);

    mAtStrToEnum(cAtPwCwReplaceStr, cAtPwCwReplaceVal, argv[1], replaceMode, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid Control-Word Replace Mode");
        return cAtFalse;
        }

    return CliPwAttributeSet(argv[0], replaceMode, (PwAttributeSetFunc)(AtPwCwPktReplaceModeSet));
    }

eBool CmdPwLopsReplaceModeSet(char argc, char **argv)
    {
    eBool convertSuccess = cAtFalse;
    eAtPwPktReplaceMode replaceMode = cAtPwPktReplaceModeInvalid;
    AtUnused(argc);

    mAtStrToEnum(cAtPwCwReplaceStr, cAtPwCwReplaceVal, argv[1], replaceMode, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid LOPS Replace Mode");
        return cAtFalse;
        }

    return CliPwAttributeSet(argv[0], replaceMode, (PwAttributeSetFunc)(AtPwLopsPktReplaceModeSet));
    }

eBool CmdPwEthPortSet(char argc, char **argv)
    {
    uint16 i;
    uint32 numPws;
    AtModuleEth ethModule;
    AtEthPort port;
    eBool success = cAtTrue;
    AtPw *pwList;
	AtUnused(argc);

    pwList = CliPwArrayFromStringGet(argv[0], &numPws);
    if (numPws == 0)
        {
        CliPwPrintHelpString();
        return cAtFalse;
        }

    /* Get ETH port */
    ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    if (AtStrcmp(argv[1], "none") == 0)
        port = NULL;
    else
        {
        port = AtModuleEthPortGet(ethModule, (uint8)(AtStrToDw(argv[1]) - 1));
        if (port == NULL)
            return cAtFalse;
        }

    for (i = 0; i < numPws; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        eAtRet ret = cAtOk;

        /* Apply */
        ret = AtPwEthPortSet(pwList[i], port);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot set Ethernet port for %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)pwList[i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdPwEthHeaderSet(char argc, char **argv)
    {
    uint32 numPws;
    AtPw *pwList = CliPwArrayFromStringGet(argv[0], &numPws);

    AtUnused(argc);

    if (numPws == 0)
        {
        CliPwPrintHelpString();
        return cAtFalse;
        }

    return CliPwEthHeaderSet(pwList, numPws, argv[1], argv[2], argv[3], AtPwEthHeaderSet);
    }

eBool CmdPwCircuitBind(char argc, char **argv)
    {
    AtPw        pw;
    uint16      i;
    uint32      *idBuf;
    AtChannel   *attachCircuitList;
    uint32      bufferSize;
    uint32      numPws = 0, numCircuits = 0;
    AtModulePw  pwModule;
    eBool       success = cAtTrue;
	AtUnused(argc);

    /* Get the shared Channel buffer */
    attachCircuitList = CliSharedChannelListGet(&bufferSize);
    if (!AtStrcmp(argv[1], "none"))
        attachCircuitList = NULL;
    else
        {
        numCircuits = CliChannelListFromString(argv[1], attachCircuitList, bufferSize);
        if (numCircuits == 0)
            {
            AtPrintc(cSevCritical, "No circuit to bind.\r\n");
            return cAtTrue;
            }
        }

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numPws = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    /* Make sure the two lists have the same number of IDs */
    if (attachCircuitList && (numPws != numCircuits))
        {
        AtPrintc(cSevCritical,
                 "ERROR: Number of PWs (%d) and number of Circuits (%d) must be the same\r\n",
                 numPws, numCircuits);
        return cAtFalse;
        }

    pwModule = (AtModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
    for (i = 0; i < numPws; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        eAtRet ret;

        /* Ignore PW that has not been created */
        pw = AtModulePwGetPw(pwModule, (uint16)(idBuf[i] - 1));
        if (pw == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: Ignore not exist PW %u\r\n", idBuf[i]);
            continue;
            }

        /* Bind/unbind */
        if (attachCircuitList == NULL)
            ret = AtPwCircuitUnbind(pw);
        else
            ret = AtPwCircuitBind(pw, attachCircuitList[i]);

        /* Let user know if any error */
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Can not bind/unbind circuit for %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)pw),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdPwCircuitUnBind(char argc, char **argv)
    {
    AtPw pw;
    uint16 i;
    uint32 *idBuf;
    uint32 bufferSize;
    uint32 numPws;
    AtModulePw pwModule;
    eBool success = cAtTrue;
	AtUnused(argc);

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numPws = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    pwModule = (AtModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
    for (i = 0; i < numPws; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        eAtRet ret = cAtOk;

        /* Ignore PW that has not been created */
        pw = AtModulePwGetPw(pwModule, (uint16)(idBuf[i] - 1));
        if (pw == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: Ignore not exist PW %u\r\n", idBuf[i]);
            continue;
            }

        /* Unbind */
        ret = AtPwCircuitUnbind(pw);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Can not unbind %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)pw),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdPwCounterGet(char argc, char **argv)
    {
    eBool silent = cAtFalse;
    AtPw *pwList;
    eAtHistoryReadingMode readingMode;
    uint32 numPws;

    if (argc < 2)
        {
        AtPrintc(cSevCritical, "ERROR: Not enough parameter\r\n");
        return cAtFalse;
        }

    pwList = CliPwArrayFromStringGet(argv[0], &numPws);
    if (numPws == 0)
        {
        CliPwPrintHelpString();
        return cAtFalse;
        }

    /* Get reading mode */
    readingMode = CliHistoryReadingModeGet(argv[1]);
    if (readingMode == cAtHistoryReadingModeUnknown)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid reading mode, expect: %s\r\n", CliValidHistoryReadingModes());
        return cAtFalse;
        }

    if (argc > 2)
        silent = CliHistoryReadingShouldSilent(readingMode, argv[2]);

    return CliAtPwCounterShow(pwList, numPws, readingMode, silent, BuildHeadingForPws);
    }

eBool CmdPwPsnSet(char argc, char **argv)
    {
    return CliPwPsnSet(argc, argv, AtPwPsnSet, AtPwPsnGet);
    }

eBool CmdPwDebug(char argc, char **argv)
    {
    uint32      i;
    uint32      numPws;
    AtPw        *pwList;
	AtUnused(argc);

    pwList = CliPwArrayFromStringGet(argv[0], &numPws);
    if (numPws == 0)
        {
        CliPwPrintHelpString();
        return cAtFalse;
        }

    for (i = 0; i < numPws; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        AtChannelDebug((AtChannel)pwList[i]);

    return cAtTrue;
    }

eBool CmdPwAlarmShow(char argc, char **argv)
    {
    return PwAlarmDefectShow(argc, argv, AtChannelAlarmGet, cAtFalse);
    }

eBool CmdPwListenedAlarmShow(char argc, char **argv)
    {
    uint16        i;
    uint16        column;
    uint32        numPws;
    tTab          *tabPtr;
    const char    *heading[] = {"PW", "Count", "Matched", "lops", "jitterOverrun", "jitterUnderun", "LBit", "RBit", "MBit", "NBit", "PBit",
                                "MissingPkt", "ExcessivePktLossRate", "StrayPkt", "MailformPkt", "RemotePktLoss", "MisConnection"};
    AtList        pwList;
    uint32 (*ListenedDefectGet)(AtChannel self, uint32 * listenedCount);
    eAtHistoryReadingMode readingMode;
    uint32 counter;
    AtUnused(argc);

    pwList = CliPwListFromString(argv[0]);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    ListenedDefectGet = AtChannelListenedDefectGet;
    if (argc > 1)
        {
        /* Get reading action mode */
        readingMode = CliHistoryReadingModeGet(argv[1]);
        if (readingMode == cAtHistoryReadingModeUnknown)
            {
            AtPrintc(cSevCritical, "ERROR: Invalid reading mode. Expected %s\r\n", CliValidHistoryReadingModes());
            return cAtFalse;
            }

        if (readingMode == cAtHistoryReadingModeReadToClear)
            ListenedDefectGet = AtChannelListenedDefectClear;
        }

    numPws = AtListLengthGet(pwList);
    tabPtr = TableAlloc(numPws, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        AtObjectDelete((AtObject)pwList);
        return cAtFalse;
        }

    for (i = 0; i < numPws; i++)
        {
        AtPw   pw;
        uint32 alarms = 0;
        uint32 hwAlarms = 0;
        eBool  alarmRaise;
        static const eAtPwAlarmType alarmTypes[] = {cAtPwAlarmTypeLops,
                                                    cAtPwAlarmTypeJitterBufferOverrun,
                                                    cAtPwAlarmTypeJitterBufferUnderrun,
                                                    cAtPwAlarmTypeLBit,
                                                    cAtPwAlarmTypeRBit,
                                                    cAtPwAlarmTypeMBit,
                                                    cAtPwAlarmTypeNBit,
                                                    cAtPwAlarmTypePBit,
                                                    cAtPwAlarmTypeMissingPacket,
                                                    cAtPwAlarmTypeExcessivePacketLossRate,
                                                    cAtPwAlarmTypeStrayPacket,
                                                    cAtPwAlarmTypeMalformedPacket,
                                                    cAtPwAlarmTypeRemotePacketLoss,
                                                    cAtpwAlarmTypeMisConnection};
        uint8 alarm_i;

        pw = (AtPw)AtListObjectGet(pwList, i);
        alarms = ListenedDefectGet((AtChannel)pw, &counter);
        hwAlarms = AtChannelDefectGet((AtChannel)pw);

        /* ID */
        column = 0;
        StrToCell(tabPtr, i, column++, CliChannelIdStringGet((AtChannel)pw));

        StrToCell(tabPtr, i, column++, CliNumber2String(counter, "%u"));
        ColorStrToCell(tabPtr, i, column++, (alarms == hwAlarms) ? "true" : "false", (alarms == hwAlarms) ? cSevInfo : cSevCritical);

        /* Alarm */
        for (alarm_i = 0; alarm_i < mCount(alarmTypes); alarm_i++)
            {
            if (!AtChannelAlarmIsSupported((AtChannel)pw, alarmTypes[alarm_i]))
                {
                ColorStrToCell(tabPtr, i, column++, sAtNotApplicable, cSevNormal);
                continue;
                }

            alarmRaise = (alarms & alarmTypes[alarm_i]) ? cAtTrue : cAtFalse;
            ColorStrToCell(tabPtr, i, column++, alarmRaise ? "set" : "clear", alarmRaise ? cSevCritical : cSevInfo);
            }
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtObjectDelete((AtObject)pwList);

    return cAtTrue;
    }

eBool CmdPwDefectShow(char argc, char **argv)
    {
    return PwAlarmDefectShow(argc, argv, AtChannelDefectGet, cAtFalse);
    }

eBool CmdAlarmInterruptShow(char argc, char **argv)
    {
    return HistoryShow(argc, argv, AtChannelAlarmInterruptClear, AtChannelAlarmInterruptGet);
    }

eBool CmdDefectInterruptShow(char argc, char **argv)
    {
    return HistoryShow(argc, argv, AtChannelDefectInterruptClear, AtChannelDefectInterruptGet);
    }

eBool CmdPwJitterBufferGet(char argc, char **argv)
    {
    AtPw          pw;
    AtList        pwList;
    uint16        i;
    uint16        column;
    eBool         alarmRaise;
    uint32        numPws;
    uint32        pwAlarm;
    tTab          *tabPtr;
    const char    *heading[] = {"PW", "jitter(us)", "jitter(pkt)", "jitterDelay(us)", "jitterDelay(pkt)",
                                "NumPktsInJitterBuffer", "jitterOverrun", "jitterUnderun", "waterMark", "Ram blocks"};
	AtUnused(argc);

    pwList = CliPwListFromString(argv[0]);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    numPws = AtListLengthGet(pwList);

    tabPtr = TableAlloc(numPws, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        AtObjectDelete((AtObject)pwList);
        return cAtFalse;
        }

    for (i = 0; i < numPws; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        char buf[32];
        uint8 cellColor;
        column = 0;
        pw = (AtPw)AtListObjectGet(pwList, i);

        StrToCell(tabPtr, i, column++, CliChannelIdStringGet((AtChannel)pw));

        /* Jitter */
        mPutConfigToCell(AtPwJitterBufferSizeGet(pw));
        mPutConfigToCell(AtPwJitterBufferSizeInPacketGet(pw));
        mPutConfigToCell(AtPwJitterBufferDelayGet(pw));
        mPutConfigToCell(AtPwJitterBufferDelayInPacketGet(pw));

        /* Number of packets in buffer */
        cellColor = cSevInfo;
        if ((AtPwNumCurrentPacketsInJitterBuffer(pw)         == 0) &&
            (AtPwNumCurrentAdditionalBytesInJitterBuffer(pw) == 0))
            cellColor = cSevCritical;
        ColorStrToCell(tabPtr, i, column++, NumPktsInJitterBuffer(pw), cellColor);

        /* Jitter buffer status */
        pwAlarm = AtChannelAlarmGet((AtChannel)pw);
        alarmRaise = (pwAlarm & cAtPwAlarmTypeJitterBufferOverrun);
        ColorStrToCell(tabPtr, i, column++, alarmRaise ? "set" : "clear", alarmRaise ? cSevCritical : cSevInfo);

        alarmRaise = (pwAlarm & cAtPwAlarmTypeJitterBufferUnderrun);
        ColorStrToCell(tabPtr, i, column++, alarmRaise ? "set" : "clear", alarmRaise ? cSevCritical : cSevInfo);

        StrToCell(tabPtr, i, column++, JitterBufferWatermark2String(pw, buf, sizeof(buf)));

        /* Used ram blocks */
        mPutConfigToCell(AtPwJitterBufferNumBlocksGet(pw));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtObjectDelete((AtObject)pwList);

    return cAtTrue;
    }

eBool CmdAtPwJitterBufferCenter(char argc, char **argv)
    {
    return NoParamSet(argc, argv, AtPwJitterBufferCenter, "Force Jitter Center");
    }

static char* ExcessivePacketLossRateDefectThresholdString(AtPw pw)
    {
    static char   buf[32];
    uint32 threshold, timeInSec;

    threshold = AtPwExcessivePacketLossRateDefectThresholdGet(pw, &timeInSec);
    AtSprintf(buf, "%u/%us", threshold, timeInSec);

    return buf;
    }

eBool CmdPwThresholdGet(char argc, char **argv)
    {
    AtPw          pw;
    AtList        pwList;
    uint16        i;
    uint16        column;
    uint32        numPws;
    static char   buf[32];
    tTab          *tabPtr;
    const char    *heading[] = {"PW", "lopsSetThres", "lopsClrThres", "missingPacket", "excessiveLossRate", "strayPacket",
                                "malformedPacket", "remotePacketLoss", "bufferOverrun", "bufferUnderrun", "misConnection"};
    AtUnused(argc);

    pwList = CliPwListFromString(argv[0]);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    numPws = AtListLengthGet(pwList);

    tabPtr = TableAlloc(numPws, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        AtObjectDelete((AtObject)pwList);
        return cAtFalse;
        }

    for (i = 0; i < numPws; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        column = 0;
        pw = (AtPw)AtListObjectGet(pwList, i);

        StrToCell(tabPtr, i, column++, CliChannelIdStringGet((AtChannel)pw));

        /* Thresholds */
        mPutConfigToCell(AtPwLopsSetThresholdGet(pw));
        mPutConfigToCell(AtPwLopsClearThresholdGet(pw));
        mPutConfigToCell(AtPwMissingPacketDefectThresholdGet(pw));

        StrToCell(tabPtr, i, column++, ExcessivePacketLossRateDefectThresholdString(pw));

        mPutConfigToCell(AtPwStrayPacketDefectThresholdGet(pw));
        mPutConfigToCell(AtPwMalformedPacketDefectThresholdGet(pw));
        mPutConfigToCell(AtPwRemotePacketLossDefectThresholdGet(pw));
        mPutConfigToCell(AtPwJitterBufferOverrunDefectThresholdGet(pw));
        mPutConfigToCell(AtPwJitterBufferUnderrunDefectThresholdGet(pw));
        mPutConfigToCell(AtPwMisConnectionDefectThresholdGet(pw));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtObjectDelete((AtObject)pwList);

    return cAtTrue;
    }

eBool CmdPwConstrainGet(char argc, char **argv)
    {
    AtList        pwList;
    uint16        i;
    uint32        column;
    uint32        numPws;
    static char   buf[32];
    tTab          *tabPtr;
    const char    *heading[] = {"PW", "minPayload", "maxPayload", "minJitterBuffer",
                                "maxJitterBuffer", "minJitterDelay", "maxJitterDelay",
                                "minJitterDelay(pkt)", "minJitterBuffer(pkt)"};
	AtUnused(argc);

    pwList = CliPwListFromString(argv[0]);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    numPws = AtListLengthGet(pwList);

    tabPtr = TableAlloc(numPws, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        AtObjectDelete((AtObject)pwList);
        return cAtFalse;
        }

    for (i = 0; i < numPws; i++)
        {
        AtPw pw = (AtPw)AtListObjectGet(pwList, i);
        AtChannel circuit       = AtPwBoundCircuitGet(pw);
        uint16 payloadSize      = AtPwPayloadSizeGet(pw);
        uint32 jitterBufferSize = AtPwJitterBufferSizeGet(pw);

        column = 0;
        StrToCell(tabPtr, i, column++, CliChannelIdStringGet((AtChannel)pw));

        mPutConfigToCell(AtPwMinPayloadSize(pw, circuit, jitterBufferSize));
        mPutConfigToCell(AtPwMaxPayloadSize(pw, circuit, jitterBufferSize));
        mPutConfigToCell(AtPwMinJitterBufferSize(pw, circuit, (uint16)payloadSize));
        mPutConfigToCell(AtPwMaxJitterBufferSize(pw, circuit, (uint16)payloadSize));
        mPutConfigToCell(AtPwMinJitterDelay(pw, circuit, (uint16)payloadSize));
        mPutConfigToCell(AtPwMaxJitterDelay(pw, circuit, (uint16)payloadSize));
        mPutConfigToCell(AtPwMinJitterBufferDelayInPacketGet(pw, circuit, (uint16)payloadSize));
        mPutConfigToCell(AtPwMinJitterBufferSizeInPacketGet(pw, circuit, (uint16)payloadSize));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtObjectDelete((AtObject)pwList);

    return cAtTrue;
    }

eBool CmdPwJitterConstrainCalculate(char argc, char **argv)
    {
    AtList        pwList;
    uint16        i;
    uint16        column;
    uint32        numPws;
    static char   buf[32];
    tTab          *tabPtr;
    const char    *heading[] = {"PW", "minJitterBuffer", "maxJitterBuffer", "minJitterDelay", "maxJitterDelay"};
    uint16        payloadSize;
	AtUnused(argc);

    pwList = CliPwListFromString(argv[0]);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    numPws = AtListLengthGet(pwList);

    tabPtr = TableAlloc(numPws, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        AtObjectDelete((AtObject)pwList);
        return cAtFalse;
        }

    /* Get payload size */
    payloadSize = (uint16)AtStrToDw(argv[1]);

    for (i = 0; i < numPws; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        AtPw pw = (AtPw)AtListObjectGet(pwList, i);
        AtChannel circuit = AtPwBoundCircuitGet(pw);

        column = 0;
        StrToCell(tabPtr, i, column++, CliChannelIdStringGet((AtChannel)pw));

        mPutConfigToCell(AtPwMinJitterBufferSize(pw, circuit, (uint16)payloadSize));
        mPutConfigToCell(AtPwMaxJitterBufferSize(pw, circuit, (uint16)payloadSize));
        mPutConfigToCell(AtPwMinJitterDelay(pw, circuit, (uint16)payloadSize));
        mPutConfigToCell(AtPwMaxJitterDelay(pw, circuit, (uint16)payloadSize));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtObjectDelete((AtObject)pwList);

    return cAtTrue;
    }

eBool CmdPwPayloadSizeConstrainCalculate(char argc, char **argv)
    {
    AtList        pwList;
    uint16        i;
    uint16        column;
    uint32        numPws;
    static char   buf[32];
    tTab          *tabPtr;
    const char    *heading[] = {"PW", "minPayload", "maxPayload"};
    uint32        jitterBufferSize;
	AtUnused(argc);

    pwList = CliPwListFromString(argv[0]);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    numPws = AtListLengthGet(pwList);

    tabPtr = TableAlloc(numPws, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        AtObjectDelete((AtObject)pwList);
        return cAtFalse;
        }

    /* Get payload size */
    jitterBufferSize = AtStrToDw(argv[1]);

    for (i = 0; i < numPws; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        AtPw pw = (AtPw)AtListObjectGet(pwList, i);
        AtChannel circuit       = AtPwBoundCircuitGet(pw);

        column = 0;
        StrToCell(tabPtr, i, column++, CliChannelIdStringGet((AtChannel)pw));

        mPutConfigToCell(AtPwMinPayloadSize(pw, circuit, jitterBufferSize));
        mPutConfigToCell(AtPwMaxPayloadSize(pw, circuit, jitterBufferSize));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtObjectDelete((AtObject)pwList);

    return cAtTrue;
    }

eBool CmdPwJitterDelayConstrainCalculate(char argc, char **argv)
    {
    AtList        pwList;
    uint16        i;
    uint16        column;
    uint32        numPws;
    static char   buf[32];
    tTab          *tabPtr;
    const char    *heading[] = {"PW", "maxJitterDelay"};
    uint32        jitterBufferSize;
	AtUnused(argc);

    pwList = CliPwListFromString(argv[0]);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    numPws = AtListLengthGet(pwList);

    tabPtr = TableAlloc(numPws, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "Cannot allocate table\r\n");
        AtObjectDelete((AtObject)pwList);
        return cAtFalse;
        }

    /* Get payload size */
    jitterBufferSize = AtStrToDw(argv[1]);

    for (i = 0; i < numPws; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        AtPw pw = (AtPw)AtListObjectGet(pwList, i);

        column = 0;
        StrToCell(tabPtr, i, column++, CliChannelIdStringGet((AtChannel)pw));

        mPutConfigToCell(AtPwMaxJitterDelayForJitterBufferSize(pw, jitterBufferSize));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtObjectDelete((AtObject)pwList);

    return cAtTrue;
    }

eBool CmdPwJitterBufferInPacketUnitSet(char argc, char **argv)
    {
	AtUnused(argc);
	return CliPwAttributeSet(argv[0], (uint32)AtStrToDw(argv[1]), (PwAttributeSetFunc)(AtPwJitterBufferSizeInPacketSet));
    }

eBool CmdPwJitterDelayInPacketUnitSet(char argc, char** argv)
    {
    AtUnused(argc);
    return CliPwAttributeSet(argv[0], (uint32)AtStrToDw(argv[1]), (PwAttributeSetFunc)(AtPwJitterBufferDelayInPacketSet));
    }

eBool CmdAtPwJitterBufferAndPayloadSizeSet(char argc, char **argv)
    {
    return PwJitterBufferAndPayloadSizeSet(argc, argv, AtPwJitterBufferAndPayloadSizeSet);
    }

eBool CmdAtPwJitterBufferInPacketAndPayloadSizeSet(char argc, char **argv)
    {
    return PwJitterBufferAndPayloadSizeSet(argc, argv, (eAtModulePwRet (*)(AtPw, uint32, uint32, uint16))AtPwJitterBufferInPacketAndPayloadSizeSet);
    }

eBool CmdAtPwTxAlarmForce(char argc, char **argv)
    {
    uint16 i;
    eAtPwAlarmType alarmType = cAtPwAlarmTypeNone;
    eBool success            = cAtTrue;
    eBool convertSuccess     = cAtFalse;
    eBool forceEnable = cAtFalse;
    AtList pwList;
    eAtRet (*TxAlarmForce)(AtChannel, uint32);
	AtUnused(argc);

    /* Alarm type */
    mAtStrToEnum(cAtPwAlarmTypeStr, cAtPwAlarmTypeVal, argv[1], alarmType, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid Control-Word Replace Mode");
        return cAtFalse;
        }

    /* Enable/disable */
    mAtStrToBool(argv[2], forceEnable, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enable mode parameter. Expected: en/dis\r\n");
        return cAtFalse;
        }

    /* PW list */
    pwList = CliPwListFromString(argv[0]);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    /* Force/unforce alarms */
    TxAlarmForce = (forceEnable) ? AtChannelTxAlarmForce : AtChannelTxAlarmUnForce;
    for (i = 0; i < AtListLengthGet(pwList); i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        AtPw   pw  = (AtPw)AtListObjectGet(pwList, i);
        eAtRet ret = TxAlarmForce((AtChannel)pw, alarmType);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot %s alarm on %s, ret = %s\r\n",
                     forceEnable ? "force" : "unforce",
                     CliChannelIdStringGet((AtChannel)pw),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    AtObjectDelete((AtObject)pwList);

    return success;
    }

eBool CmdAtPwForcedAlarmGet(char argc, char **argv)
    {
    uint16        i;
    tTab          *tabPtr;
    const char    *heading[] = {"PW", "ForcedTxAlarms", "ForcedRxAlarms"};
    AtList        pwList;
    char          buf[32];
	AtUnused(argc);

    pwList = CliPwListFromString(argv[0]);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    tabPtr = TableAlloc(AtListLengthGet(pwList), mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot allocate table\r\n");
        AtObjectDelete((AtObject)pwList);
        return cAtFalse;
        }

    for (i = 0; i < AtListLengthGet(pwList); i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        AtPw   pw     = (AtPw)AtListObjectGet(pwList, i);
        uint32 alarms = AtChannelTxForcedAlarmGet((AtChannel)pw);

        StrToCell(tabPtr, i, 0, CliChannelIdStringGet((AtChannel)pw));

        Alarm2String(alarms, buf);
        ColorStrToCell(tabPtr, i, 1, buf, alarms ? cSevCritical : cSevInfo);
        StrToCell(tabPtr, i, 2, sAtNotApplicable);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtObjectDelete((AtObject)pwList);

    return cAtTrue;
    }

eBool CmdAtPwIdleCodeSet(char argc, char **argv)
    {
    AtUnused(argc);
    return CliPwAttributeSet(argv[0], AtStrToDw(argv[1]), (PwAttributeSetFunc)(AtPwIdleCodeSet));
    }

eBool CmdPwMissingPacketThresSet(char argc, char **argv)
    {
    return IntegerAttributeSet(argc, argv, (PwAttributeSetFunc)(AtPwMissingPacketDefectThresholdSet));
    }

eBool CmdPwExcessiveLossRateThresSet(char argc, char **argv)
    {
    AtPw        pw;
    uint16      i;
    uint32      *idBuf;
    uint32      bufferSize;
    uint32      numPws;
    AtModulePw  pwModule;
    eBool       success = cAtTrue;
    uint32      threshold;
    uint32      timeInSec;

    AtUnused(argc);

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numPws = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    threshold = AtStrToDw(argv[1]);
    timeInSec = AtStrToDw(argv[2]);
    pwModule = (AtModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
    for (i = 0; i < numPws; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        eAtRet ret = cAtOk;

        /* Ignore PW that has not been created */
        pw = AtModulePwGetPw(pwModule, (uint16)(idBuf[i] - 1));
        if (pw == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: Ignore not exist PW %u\r\n", idBuf[i]);
            continue;
            }

        /* Apply input attribute */
        ret = AtPwExcessivePacketLossRateDefectThresholdSet(pw, threshold, timeInSec);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Can not change attribute of %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)pw),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdPwStrayPacketThresSet(char argc, char **argv)
    {
    return IntegerAttributeSet(argc, argv, (PwAttributeSetFunc)(AtPwStrayPacketDefectThresholdSet));
    }

eBool CmdPwMalformedPacketThresSet(char argc, char **argv)
    {
    return IntegerAttributeSet(argc, argv, (PwAttributeSetFunc)(AtPwMalformedPacketDefectThresholdSet));
    }

eBool CmdPwRemotePacketLossThresSet(char argc, char **argv)
    {
    return IntegerAttributeSet(argc, argv, (PwAttributeSetFunc)(AtPwRemotePacketLossDefectThresholdSet));
    }

eBool CmdPwBufferUnderrunThresSet(char argc, char **argv)
    {
    return IntegerAttributeSet(argc, argv, (PwAttributeSetFunc)(AtPwJitterBufferUnderrunDefectThresholdSet));
    }

eBool CmdPwBufferOverrunThresSet(char argc, char **argv)
    {
    return IntegerAttributeSet(argc, argv, (PwAttributeSetFunc)(AtPwJitterBufferOverrunDefectThresholdSet));
    }

eBool CmdPwMisConnectionThresSet(char argc, char **argv)
    {
    return IntegerAttributeSet(argc, argv, (PwAttributeSetFunc)(AtPwMisConnectionDefectThresholdSet));
    }

eBool CmdPwLogShow(char argc, char** argv)
    {
    uint16      i;
    uint32      numPws;
    AtPw        *pwList;

    AtUnused(argc);

    pwList = CliPwArrayFromStringGet(argv[0], &numPws);
    if (numPws == 0)
        {
        CliPwPrintHelpString();
        return cAtFalse;
        }

    for (i = 0; i < numPws; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        AtChannelLogShow((AtChannel)pwList[i]);

    return cAtTrue;
    }

static eBool LogEnable(char argc, char** argv, eBool enable)
    {
    uint16      i;
    uint32      *idBuf;
    uint32      bufferSize;
    uint32      numPws;
    AtChannel   pw;
    AtModulePw  pwModule;
    eAtRet ret;
    eBool success = cAtTrue;

    AtUnused(argc);

    /* Get the shared ID buffer */
    idBuf = CliSharedIdBufferGet(&bufferSize);
    if ((numPws = CliIdListFromString(argv[0], idBuf, bufferSize)) == 0)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    pwModule = (AtModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
    for (i = 0; i < numPws; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        /* Ignore PW that has not been created */
        pw = (AtChannel)AtModulePwGetPw(pwModule, (uint16)(idBuf[i] - 1));
        if (pw == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: Ignore not exist PW %u\r\n", idBuf[i]);
            continue;
            }

        ret = AtChannelLogEnable(pw, enable);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot enable logging for PW %s, ret = %s\r\n",
                     CliChannelIdStringGet(pw),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdPwLogEnable(char argc, char** argv)
    {
    return LogEnable(argc, argv, cAtTrue);
    }

eBool CmdPwLogDisable(char argc, char** argv)
    {
    return LogEnable(argc, argv, cAtFalse);
    }

static void AlarmChangeState(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    uint8 alarm_i;

    AtPrintc(cSevNormal, "\r\n%s [PW] Defect changed at channel %s", AtOsalDateTimeInUsGet(), CliChannelIdStringGet(channel));
    for (alarm_i = 0; alarm_i < mCount(cAtPwAlarmPrettyTypeStr); alarm_i++)
        {
        eAtPwAlarmType alarmType = cAtPwAlarmTypeVal[alarm_i];
        if ((changedAlarms & alarmType) == 0)
            continue;

        AtPrintc(cSevNormal, " * [%s:", cAtPwAlarmPrettyTypeStr[alarm_i]);
        if (currentStatus & alarmType)
            AtPrintc(cSevCritical, "set");
        else
            AtPrintc(cSevInfo, "clear");
        AtPrintc(cSevNormal, "]");
        }
    AtPrintc(cSevInfo, "\r\n");
    }

static void AlarmChangeLogging(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    uint8 alarm_i;
    static char logBuffer[1024];
    char * logPtr = logBuffer;

    AtOsalMemInit(logBuffer, 0, sizeof(logBuffer));

    logPtr += AtSprintf(logPtr, "has defects changed at");
    for (alarm_i = 0; alarm_i < mCount(cAtPwAlarmPrettyTypeStr); alarm_i++)
        {
        eAtPwAlarmType alarmType = cAtPwAlarmTypeVal[alarm_i];
        if ((changedAlarms & alarmType) == 0)
            continue;

        logPtr += AtSprintf(logPtr, " * [%s:", cAtPwAlarmPrettyTypeStr[alarm_i]);
        if (currentStatus & alarmType)
            logPtr += AtSprintf(logPtr, "set");
        else
            logPtr += AtSprintf(logPtr, "clear");
        logPtr += AtSprintf(logPtr, "]");
        }
    CliChannelLog(channel, logBuffer);
    }

eBool CmdPwAlarmCapture(char argc, char **argv)
    {
    eAtRet ret;
    eBool convertSuccess;
    eBool success = cAtTrue;
    eBool captureEnabled = cAtFalse;
    AtList pwList;
    uint32 i;
    static tAtChannelEventListener pwListener;
    AtUnused(argc);

    /* PW list */
    pwList = CliPwListFromString(argv[0]);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    /* Enabling */
    mAtStrToBool(argv[1], captureEnabled, convertSuccess);
    if (!convertSuccess)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enabling mode, expected en/dis\r\n");
        return cAtFalse;
        }

    AtOsalMemInit(&pwListener, 0, sizeof(pwListener));
    if (AutotestIsEnabled())
        pwListener.AlarmChangeStateWithUserData = AutotestAlarmChangeState;
    else if (CliInterruptLogIsEnabled())
        pwListener.AlarmChangeState = AlarmChangeLogging;
    else
        pwListener.AlarmChangeState = AlarmChangeState;

    for (i = 0; i < AtListLengthGet(pwList); i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        AtChannel   pw  = (AtChannel)AtListObjectGet(pwList, i);
        if (captureEnabled)
            {
            if (AutotestIsEnabled())
                ret = AtChannelEventListenerAddWithUserData(pw, &pwListener, CliAtPwObserverGet(pw));
            else
                ret = AtChannelEventListenerAdd(pw, &pwListener);
            }
        else
            {
            ret = AtChannelEventListenerRemove(pw, &pwListener);

            if (AutotestIsEnabled())
                CliAtPwObserverDelete(pw);
            }

        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical,
                     "ERROR: Cannot %s alarm listener for %s, ret = %s\r\n",
                     captureEnabled ? "register" : "deregister",
                     CliChannelIdStringGet((AtChannel)pw),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    AtObjectDelete((AtObject)pwList);

    return success;
    }

static uint32 CliPwIntrMaskFromString(char *pIntrStr)
    {
    return CliMaskFromString(pIntrStr, cAtPwAlarmTypeStr, cAtPwAlarmTypeVal, mCount(cAtPwAlarmTypeVal));
    }

eBool CmdPwInterruptMaskSet(char argc, char **argv)
    {
    /* Declare variables */
    eAtRet  ret;
    uint32 intrMaskVal, i;
    eBool enable = cAtFalse, result;
    AtList pwList;
    AtUnused(argc);

    ret = cAtOk;

    /* PW list */
    pwList = CliPwListFromString(argv[0]);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    /* Get and Convert interrupt mask from string */
    intrMaskVal = CliPwIntrMaskFromString(argv[1]);

    /* Get enable mode */
    mAtStrToBool(argv[2], enable, result)
    if(!result)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid enable mode parameter\r\n"
                                 "Expected: en/dis\r\n");
        return cAtFalse;
        }

    /* SET Enabel/Disable */
    for (i = 0; i < AtListLengthGet(pwList); i = AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        AtChannel   pw  = (AtChannel)AtListObjectGet(pwList, i);
        ret = AtChannelInterruptMaskSet(pw, intrMaskVal, (enable) ? intrMaskVal : 0x0);
        if (ret != cAtOk)
           mWarnNotReturnCliIfNotOk(ret, pw, "Can not set interrupt mask");
        }

    AtObjectDelete((AtObject)pwList);
    return cAtTrue;
    }

eBool CmdPwEthPortQueueSet(char argc, char **argv)
    {
    AtUnused(argc);
    return CliPwAttributeSet(argv[0], (uint32)(AtStrToDw(argv[1]) - 1), (PwAttributeSetFunc)(AtPwEthPortQueueSet));
    }

eBool CmdAtPwAlarmCaptureShow(char argc, char **argv)
    {
    const char    *pHeading[] = {"PW", "lops", "jitterOverrun", "jitterUnderun", "LBit",
                                "RBit", "MBit", "NBit", "PBit", "MissingPkt", "ExcessivePktLossRate",
                                "StrayPkt", "MailformPkt", "RemotePktLoss", "MisConnection"};
    uint32 numChannels, i;
    eAtHistoryReadingMode readMode;
    AtChannel *channelList;
    tTab *tabPtr = NULL;
    eBool silent = cAtFalse;
    AtUnused(argc);

    channelList = (AtChannel *)CliPwArrayFromStringGet(argv[0], &numChannels);
    if (numChannels == 0)
        {
        CliPwPrintHelpString();
        return cAtFalse;
        }

    /* If list channel don't raised any event, just report not-changed */
    readMode = CliHistoryReadingModeGet(argv[1]);

    if (argc > 2)
        silent = CliHistoryReadingShouldSilent(readMode, argv[2]);

    if (!silent)
        {
        /* Create table with titles */
        tabPtr = TableAlloc(numChannels, mCount(pHeading), pHeading);
        if (!tabPtr)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
            return cAtFalse;
            }
        }

    for (i = 0; i < numChannels; i = AtCliNextIdWithSleep(i, AtCliLimitNumRestTdmChannelGet()))
        {
        AtChannel channel = channelList[i];
        AtChannelObserver observer = CliAtPwObserverGet(channel);
        uint8 column = 0;
        uint32 current;
        uint32 history;

        StrToCell(tabPtr, i, column++, CliChannelIdStringGet(channel));

        current = AtChannelObserverDefectGet(observer);
        if (readMode == cAtHistoryReadingModeReadToClear)
            history = AtChannelObserverDefectHistoryClear(observer);
        else
            history = AtChannelObserverDefectHistoryGet(observer);

        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPwAlarmTypeLops);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPwAlarmTypeJitterBufferOverrun);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPwAlarmTypeJitterBufferUnderrun);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPwAlarmTypeLBit);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPwAlarmTypeRBit);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPwAlarmTypeMBit);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPwAlarmTypeNBit);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPwAlarmTypePBit);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPwAlarmTypeMissingPacket);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPwAlarmTypeExcessivePacketLossRate);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPwAlarmTypeStrayPacket);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPwAlarmTypeMalformedPacket);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtPwAlarmTypeRemotePacketLoss);
        CliCapturedAlarmToCell(tabPtr, i, column++, history, current, cAtpwAlarmTypeMisConnection);

        /* Delete observer */
        CliAtPwObserverDelete(channel);
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CliPwEthMacSet(AtList pwList, char* macStr, MacSetFunc MacSet)
    {
    uint16 i;
    uint8 mac[cAtMacAddressLen];
    eBool success = cAtTrue;

    /* Get MAC */
    AtOsalMemInit(mac, 0, cAtMacAddressLen);
    if (!CliAtModuleEthMacAddrStr2Byte(macStr, mac))
        {
        AtPrintc(cSevWarning, "WARNING: MAC is invalid!!!\n");
        return cAtFalse;
        }

    for (i = 0; i < AtListLengthGet(pwList); i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        AtPw pw = (AtPw)AtListObjectGet(pwList, i);
        eAtRet ret = MacSet(pw, mac);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Can not configure SMAC for %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)pw),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    AtObjectDelete((AtObject)pwList);
    return success;
    }

void CliPwPrintHelpString(void)
    {
    AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, hdlc.1-2, ppp.2-3, fr.1.2-1.4 ...\n");
    }

const char **CliAtPwAlarmTypeStr(uint32 *numAlarms)
    {
    *numAlarms = mCount(cAtPwAlarmTypeStr);
    return cAtPwAlarmTypeStr;
    }

const uint32 *CliAtPwAlarmTypeVal(uint32 *numAlarms)
    {
    *numAlarms = mCount(cAtPwAlarmTypeVal);
    return cAtPwAlarmTypeVal;
    }

/* Not include the 'all' alarm which cause wrong interpretation for pw defect/failure profile */
const char **CliAtPwAlarmTypeStrForPwProfile(uint32 *numAlarms)
    {
    *numAlarms = mCount(cAtPwAlarmTypeStr) - 1;
    return cAtPwAlarmTypeStr;
    }

const uint32 *CliAtPwAlarmTypeValForPwProfile(uint32 *numAlarms)
    {
    *numAlarms = mCount(cAtPwAlarmTypeVal) - 1;
    return cAtPwAlarmTypeVal;
    }

eBool CmdPwEthSourceMacSet(char argc, char **argv)
    {
    AtList pwList = CliPwListFromString(argv[0]);

    AtUnused(argc);

    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    return CliPwEthMacSet(pwList, argv[1], AtPwEthSrcMacSet);
    }

eBool CmdAtPwCvlanTpidSet(char argc, char **argv)
    {
    AtUnused(argc);
    return CliPwAttributeSet(argv[0], (uint16)(AtStrToDw(argv[1])), (PwAttributeSetFunc)(AtPwEthCVlanTpidSet));
    }

eBool CmdAtPwSvlanTpidSet(char argc, char **argv)
    {
    AtUnused(argc);
    return CliPwAttributeSet(argv[0], (uint16)(AtStrToDw(argv[1])), (PwAttributeSetFunc)(AtPwEthSVlanTpidSet));
    }

eBool CmdAtPwExpectVlanSet(char argc, char **argv)
    {
    uint16 i;
    uint32 numPws;
    tAtEthVlanDesc desc;
    tAtEthVlanTag *sVlan, *cVlan;
    eBool success = cAtTrue;
    AtPw *pwList;
    AtUnused(argc);

    AtOsalMemInit(&desc, 0, sizeof(desc));

    pwList = CliPwArrayFromStringGet(argv[0], &numPws);
    if (numPws == 0)
        {
        CliPwPrintHelpString();
        return cAtFalse;
        }

    /* Get S-VLAN and C-VLAN */
    sVlan = cVlan = NULL;
    if (!CliEthFlowVlanGet(argv[1], argv[2], &desc))
        return cAtFalse;
    if (desc.numberOfVlans > 0)
        {
        cVlan = &(desc.vlans[0]);
        if (desc.numberOfVlans == 2)
            sVlan = &(desc.vlans[1]);
        }

    for (i = 0; i < numPws; i++)
        {
        eAtRet ret = cAtOk;

        ret  = AtPwEthExpectedCVlanSet(pwList[i], cVlan);
        ret |= AtPwEthExpectedSVlanSet(pwList[i], sVlan);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Can not configure for %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)pwList[i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    return success;
    }

eBool CmdAtPwExpectedVlanShow(char argc, char **argv)
    {
    const char *pHeading[] = { "PW", "vlan" };
    uint32 numPws, pw_i;
    AtPw *pwList;
    tTab *tabPtr;
    static char idString[32];
    AtUnused(argc);

    pwList = CliPwArrayFromStringGet(argv[0], &numPws);
    if (numPws == 0)
        {
        CliPwPrintHelpString();
        return cAtFalse;
        }

    /* Create table with titles */
    tabPtr = TableAlloc(numPws, mCount(pHeading), pHeading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    for (pw_i = 0; pw_i < numPws; pw_i++)
        {
        tAtEthVlanTag cVlan;
        tAtEthVlanTag sVlan;
        AtPw pw = pwList[pw_i];
        eAtPwType pwType = AtPwTypeGet(pw);

        if (mIsPwVirtualCircuitType(pwType))
            AtSprintf(idString, "%s.%d", AtChannelTypeString((AtChannel)pw), AtChannelIdGet((AtChannel)pw));
        else
            AtSprintf(idString, "%s", CliChannelIdStringGet((AtChannel)pw));
        StrToCell(tabPtr, pw_i, 0, idString);
        StrToCell(tabPtr, pw_i, 1, CliVlans2Str(AtPwEthExpectedCVlanGet(pw, &cVlan), AtPwEthExpectedSVlanGet(pw, &sVlan)));
        }

    TablePrint(tabPtr);
    TableFree(tabPtr);

    return cAtTrue;
    }

eBool CmdAtPwJitterBufferWatermarkReset(char argc, char **argv)
    {
    return NoParamSet(argc, argv, AtPwJitterBufferWatermarkReset, "Reset jitter buffer watermark");
    }

eBool CmdAtPwDebugTableShow(char argc, char **argv)
    {
    AtList pwList;
    uint16 pw_i;
    AtList debuggers;

    AtUnused(argc);

    /* PW list */
    pwList = CliPwListFromString(argv[0]);
    if (pwList == NULL)
        {
        AtPrintc(cSevWarning, "WARNING: No PW, they have not been created yet or invalid PW list, expected: 1 or 1-252, ...\n");
        return cAtFalse;
        }

    /* Create all of debuggers */
    debuggers = AtListCreate(AtListLengthGet(pwList));
    for (pw_i = 0; pw_i < AtListLengthGet(pwList); pw_i++)
        {
        AtChannel pw = (AtChannel)AtListObjectGet(pwList, pw_i);
        AtListObjectAdd(debuggers, (AtObject)AtChannelDebuggerCreate(pw));
        }

    CliAtChannelDebuggersShow(debuggers);

    /* Cleanup */
    AtListDeleteWithObjectHandler(debuggers, AtObjectDelete);
    AtObjectDelete((AtObject)pwList);

    return cAtTrue;
    }

eBool CmdPwEthFlowSet(char argc, char **argv)
    {
    uint16 i;
    uint32 numPws, numFlows;
    AtEthFlow flow;
    AtList flowList = NULL;
    eBool success = cAtTrue;
    AtPw *pwList;
    AtUnused(argc);

    pwList = CliPwArrayFromStringGet(argv[0], &numPws);
    if (numPws == 0)
        {
        CliPwPrintHelpString();
        return cAtFalse;
        }

    /* Get ETH flows */
    if (AtStrcmp(argv[1], "none") == 0)
        flow = NULL;
    else
        {
        flowList = CliEthFlowListFromString(argv[1], &numFlows);
        if (numFlows == 0)
            {
            AtPrintc(cSevCritical, "Invalid flow ID. Expected: 1, 2, ...\r\n");
            return cAtFalse;
            }

        if (numFlows != numPws)
            {
            AtPrintc(cSevCritical, "Number of flows is not equal to number of PWs\r\n");
            AtObjectDelete((AtObject)flowList);
            return cAtFalse;
            }
        }

    for (i = 0; i < numPws; i = (uint16)AtCliNextIdWithSleep(i, AtCliLimitNumRestPwGet()))
        {
        eAtRet ret = cAtOk;

        if (flowList)
            flow = (AtEthFlow)AtListObjectGet(flowList, i);

        /* Apply */
        ret = AtPwEthFlowSet(pwList[i], flow);
        if (ret != cAtOk)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot set Ethernet flow for %s, ret = %s\r\n",
                     CliChannelIdStringGet((AtChannel)pwList[i]),
                     AtRet2String(ret));
            success = cAtFalse;
            }
        }

    AtObjectDelete((AtObject)flowList);

    return success;
    }


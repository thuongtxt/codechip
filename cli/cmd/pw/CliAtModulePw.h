/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : CliAtModulePw.h
 * 
 * Created Date: Oct 10, 2013
 *
 * Description : PW CLIs
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLIATMODULEPW_H_
#define _CLIATMODULEPW_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCli.h"
#include "AtPw.h"
#include "AtCliModule.h"
#include "../../cmd/textui/CliAtTextUI.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef AtPw (*PwCreateFunc)(AtModulePw, uint32);
typedef eAtRet (*PwAttributeSetFunc)(AtPw pw, uint32 value);
typedef eAtRet (*PwBoolAttributeSet)(AtPw self, eBool value);
typedef AtPwPsn (*PwPsnGet)(AtPw self);
typedef eAtModulePwRet (*PwPsnSet)(AtPw self, AtPwPsn psn);
typedef eAtRet(*ScalarAttributeSet)(AtPwPsn self, uint32 value);
typedef eAtModulePwRet (*ArrayAttributeSet)(AtPwPsn self, const uint8 *array);
typedef eAtRet (*PwPsnMplsLabelSet)(AtPwMplsPsn self, const tAtPwMplsLabel *label);
typedef eAtRet (*PwPsnMefVlanSet)(AtPwMefPsn self, uint16 vlanType, const tAtEthVlanTag *tag);
typedef eAtRet (*PwGroupResourceManagerFunc)(AtPwGroup self, AtPw pw);
typedef AtPwGroup (*PwGroupGetFunc)(AtModulePw self, uint32 groupId);
typedef eAtRet (*PwGroupAttributeSet)(AtPwGroup self, uint32 value);
typedef eAtModulePwRet (*PwGroupDeleteFunc)(AtModulePw, uint32);
typedef char** (*HeadingPwsBuild)(AtPw *, uint32, uint32);
typedef eAtModulePwRet (*MacSetFunc)(AtPw, uint8 *);

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
extern AtList CliPwListFromString(char *pwIdListString);
AtPw* CliPwArrayFromStringGet(char* arg, uint32 *numberPws);
AtPw* CliPppPwArrayFromString(char* arg, uint32 *numberPws);
AtPw* CliHdlcPwArrayFromString(char* arg, uint32 *numberPws);

void CliPwPrintHelpString(void);

eBool CliPwPsnSet(char argc, char **argv, eAtModulePwRet (*PsnSet)(AtPw self, AtPwPsn psn), AtPwPsn (*PsnGet)(AtPw self));
eBool CliPwBackupPsnSet(char argc, char **argv, eAtModulePwRet (*PsnSet)(AtPw self, AtPwPsn psn), AtPwPsn (*PsnGet)(AtPw self));
eBool CliPwEthHeaderSet(AtPw *pwList, uint32 numPws, char *pMacStr, char *pSvlanStr, char *pCVlanStr, eAtModulePwRet (*EthHeaderSet)(AtPw, uint8 *, const tAtEthVlanTag *, const tAtEthVlanTag *));
eBool CliPwBoolAttributeSet(char argc, char **argv, eBool boolValue, PwBoolAttributeSet PwAttributeEnabling);
char *CliPwPsnChainString(AtPwPsn psn);
eBool CliPwPsnShow(char argc, char **argv,
                   AtPwPsn (*PsnGet)(AtPw self),
                   eAtModulePwRet (*DestMacGet)(AtPw self, uint8 *destMac),
                   eAtModulePwRet (*SourceMacGet)(AtPw self, uint8 *srcMac),
                   tAtEthVlanTag *(*CVlanGet)(AtPw self, tAtEthVlanTag *cVlan),
                   tAtEthVlanTag *(*SVlanGet)(AtPw self, tAtEthVlanTag *sVlan),
                   uint16 (*CVlanTpidGet)(AtPw self),
                   uint16 (*SVlanTpidGet)(AtPw self));

eBool CmdAtPwIdleCodeSet(char argc, char **argv);

eBool CliPwPsnMplsShow(char argc, char **argv, AtPwPsn (*PsnGet)(AtPw self));
eBool CliPwPsnMefShow(char argc, char **argv, AtPwPsn (*PsnGet)(AtPw self));
eBool CliPwPsnUdpShow(char argc, char **argv, AtPwPsn (*PsnGet)(AtPw self));
eBool CliPwPsnIpv6Show(char argc, char **argv, AtPwPsn (*PsnGet)(AtPw self));
eBool CliPwPsnIpv4Show(char argc, char **argv, AtPwPsn (*PsnGet)(AtPw self));
eBool CliPsnScalarAttributeSet(char *pwIdList, eAtPwPsnType pwPsnType, uint32 value, PwPsnSet pwPsnSetFunc, PwPsnGet pwPsnGetFunc, ScalarAttributeSet attributeSet);
uint16 CliPwUdpPortGet(char *udpPortString);
eBool CliPwPsnIpAddressSet(char argc, char **argv, eAtPwPsnType pwPsnType, PwPsnSet pwPsnSetFunc, PwPsnGet pwPsnGetFunc, ArrayAttributeSet AddressSetFunc);
eBool CliPwPsnMplsLabelSet(char argc, char **argv, PwPsnSet pwPsnSetFunc, PwPsnGet pwPsnGetFunc, PwPsnMplsLabelSet mplsLabelSetFunc);
eBool CliPwPsnMefMacAddressSet(char argc, char **argv, PwPsnSet pwPsnSetFunc, PwPsnGet pwPsnGetFunc, ArrayAttributeSet AddressSetFunc);
eBool CliPwPsnMefVlanSet(char argc, char **argv, PwPsnSet pwPsnSetFunc, PwPsnGet pwPsnGetFunc, PwPsnMefVlanSet VlanSetFunc);
eBool CliPwEthMacSet(AtList pwList, char* macStr, MacSetFunc MacSet);
eBool HelperPwCreate(char argc, char **argv, PwCreateFunc pwCreateFunc, const char *name);

eBool CliPwGroupBoolAttributeSet(char *pwGroupIdList, uint32 value, PwGroupGetFunc PwGetPwFunc, PwGroupAttributeSet attributeSetFunc);
eBool CliPwGroupResourceManager(char argc, char **argv, PwGroupGetFunc PwGetPwFunc, PwGroupResourceManagerFunc ResourceManagerFunc);
AtList CliPwGroupListFromString(char *pwIdListString, PwGroupGetFunc PwGetPwFunc);
eBool CliPwGroupDelete(char argc, char **argv, PwGroupDeleteFunc pwGroupDeleteFunc, const char *name);
uint32 PwGroupToPwIdList(AtPwGroup group, uint32 *pwIds, uint32 bufsize);
eBool CliPwAttributeSet(char *pwIdList, uint32 value, PwAttributeSetFunc attributeSetFunc);
void CliPwPrintHelpString(void);
eBool CliAtPwCounterShow(AtPw *pwList,
                         uint32 numPws,
                         eAtHistoryReadingMode readingMode,
                         eBool silent,
                         HeadingPwsBuild pwHeadingBuild);

const char **CliAtPwAlarmTypeStr(uint32 *numAlarms);
const uint32 *CliAtPwAlarmTypeVal(uint32 *numAlarms);
const char **CliAtPwAlarmTypeStrForPwProfile(uint32 *numAlarms);
const uint32 *CliAtPwAlarmTypeValForPwProfile(uint32 *numAlarms);

#endif /* _CLIATMODULEPW_H_ */


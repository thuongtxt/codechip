/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Util
 * 
 * File        : CliAtDebugger.h
 * 
 * Created Date: May 18, 2017
 *
 * Description : CLI common implementation for util module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLIATDEBUGGER_H_
#define _CLIATDEBUGGER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDebugger.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool CliAtDebuggerShow(AtDebugger debugger);
eBool CliAtChannelDebuggersShow(AtList channelDebuggers);

#ifdef __cplusplus
}
#endif
#endif /* _CLIATDEBUGGER_H_ */


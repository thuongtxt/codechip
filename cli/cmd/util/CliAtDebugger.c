/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Util
 *
 * File        : CliAtDebugger.c
 *
 * Created Date: May 18, 2017
 *
 * Description : CLI common implementation for util module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "CliAtDebugger.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static char **ChannelHeaderCreate(AtList channelDebuggers, uint32 *pNumColumns)
    {
    AtDebugger debugger = (AtDebugger)AtListObjectGet(channelDebuggers, 0);
    AtIterator iterator = AtDebuggerEntryIteratorCreate(debugger);
    uint32 numColumns = AtIteratorCount(iterator);
    uint32 memorySize = sizeof(char *) * numColumns;
    char **header = AtOsalMemAlloc(memorySize);
    uint32 column = 0;
    AtOsalMemInit(header, 0, memorySize);

    for (column = 0; column < numColumns; column++)
        {
        AtDebugEntry entry = (AtDebugEntry)AtIteratorNext(iterator);
        AtAssert(entry);
        header[column] = AtStrdup(AtDebuggerEntryNameGet(entry));
        }

    AtObjectDelete((AtObject)iterator);

    if (pNumColumns)
        *pNumColumns = numColumns;

    return header;
    }

eBool CliAtDebuggerShow(AtDebugger debugger)
    {
    tTab *tabPtr = NULL;
    const char *heading[] ={"Name", "Value", "Severity"};
    uint32 numEntries;
    uint32 row_i;
    AtIterator iterator;
    uint32 numRows;
    uint32 value;

    if (debugger == NULL)
        {
        AtPrintc(cSevWarning, "Debugger does not exist, nothing to display\r\n");
        return cAtTrue;
        }

    if (AtDebuggerNumEntries(debugger) == 0)
        {
        AtPrintc(cSevInfo, "No entries to display\r\n");
        return cAtTrue;
        }

    /* Create table */
    iterator = AtDebuggerEntryIteratorCreate(debugger);
    numEntries = AtIteratorCount(iterator);
    numRows = numEntries + 4; /* 4 last rows for displaying profiling information */
    tabPtr = TableAlloc(numRows, mCount(heading), heading);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Iterate and put all of entries */
    for (row_i = 0; row_i < numEntries; row_i++)
        {
        AtDebugEntry entry = (AtDebugEntry)AtIteratorNext(iterator);
        eAtSevLevel level = AtDebuggerEntrySeverityGet(entry);
        uint32 bufferSize;
        char * tmpStr = CliSharedCharBufferGet(&bufferSize);
        AtAssert(entry);

        AtSprintf(tmpStr, "%s", AtDebuggerEntryNameGet(entry));
        StrToCell(tabPtr, row_i, 0, AtStringTrim(tmpStr));
        StrToCell(tabPtr, row_i, 1, AtDebuggerEntryValueGet(entry));
        ColorStrToCell(tabPtr, row_i, 2, AtSevLevel2Str(level), level);
        }

    /* Put profile information */
    value = AtDebuggerNumInfoEntries(debugger);
    StrToCell(tabPtr, row_i, 0, "numInfoEntries");
    ColorStrToCell(tabPtr, row_i, 1, CliNumber2String(value, "%u"), (value == 0) ? cSevNormal : cSevInfo);
    StrToCell(tabPtr, row_i, 2, sAtNotCare);
    row_i = row_i + 1;

    value = AtDebuggerNumWarningEntries(debugger);
    StrToCell(tabPtr, row_i, 0, "numWarningEntries");
    ColorStrToCell(tabPtr, row_i, 1, CliNumber2String(value, "%u"), (value == 0) ? cSevInfo : cSevWarning);
    StrToCell(tabPtr, row_i, 2, sAtNotCare);
    row_i = row_i + 1;

    value = AtDebuggerNumCriticalEntries(debugger);
    StrToCell(tabPtr, row_i, 0, "numCriticalEntries");
    ColorStrToCell(tabPtr, row_i, 1, CliNumber2String(value, "%u"), (value == 0) ? cSevInfo : cSevCritical);
    StrToCell(tabPtr, row_i, 2, sAtNotCare);
    row_i = row_i + 1;

    StrToCell(tabPtr, row_i, 0, "numOtherSeverityEntries");
    StrToCell(tabPtr, row_i, 1, CliNumber2String(AtDebuggerNumOtherSeverityEntries(debugger), "%u"));
    StrToCell(tabPtr, row_i, 2, sAtNotCare);
    row_i = row_i + 1;

    AtAssert(row_i == numRows); /* Will be helpful when number of rows are manually increased */

    /* Display it */
    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtObjectDelete((AtObject)iterator);

    return cAtTrue;
    }

eBool CliAtChannelDebuggersShow(AtList channelDebuggers)
    {
    tTab *tabPtr = NULL;
    char **header;
    uint32 debugger_i;
    uint32 numColumns;
    uint32 numDebuggers = AtListLengthGet(channelDebuggers);

    if (numDebuggers == 0)
        {
        AtPrintc(cSevWarning, "Debugger does not exist, nothing to display\r\n");
        return cAtTrue;
        }

    /* Create table */
    header = ChannelHeaderCreate(channelDebuggers, &numColumns);
    tabPtr = TableAlloc(numDebuggers, numColumns, (const char **)(void *)header);
    if (!tabPtr)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return cAtFalse;
        }

    /* Iterate and put all of entries */
    for (debugger_i = 0; debugger_i < numDebuggers; debugger_i++)
        {
        AtDebugger debugger = (AtDebugger)AtListObjectGet(channelDebuggers, debugger_i);
        AtIterator iterator = AtDebuggerEntryIteratorCreate(debugger);
        uint32 column = 0;

        for (column = 0; column < numColumns; column++)
            {
            AtDebugEntry entry = (AtDebugEntry)AtIteratorNext(iterator);
            AtAssert(entry);
            ColorStrToCell(tabPtr, debugger_i, column, AtDebuggerEntryValueGet(entry), AtDebuggerEntrySeverityGet(entry));
            }

        AtObjectDelete((AtObject)iterator);
        }

    /* Display it */
    TablePrint(tabPtr);
    TableFree(tabPtr);
    AtStringArrayFree(header, numColumns);

    return cAtTrue;
    }

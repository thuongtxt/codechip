/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Test
 *
 * File        : ClishTestDdr.c
 *
 * Created Date: Mar 4, 2017
 *
 * Description : To work with DDR
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("test ddr", "Command to test DDR")

static eBool ReadAndExpectValue(AtHal hal,
                                uint32 address, uint32 expectedValue,
                                const char *filepath, uint32 lineNumber,
                                uint32 delayUsBeforeCheck)
    {
    uint32 actualValue;

    if (delayUsBeforeCheck)
        AtOsalUSleep(delayUsBeforeCheck);

    actualValue = AtHalRead(hal, address);
    if (actualValue != expectedValue)
        {
        AtPrintc(cSevCritical, "(%s: %d): Address = 0x%08x, expectedValue = 0x%08x, actualValue = 0x%08x\r\n",
                 filepath, lineNumber,
                 address, expectedValue, actualValue);
        return cAtFalse;
        }

    return cAtTrue;
    }

static void TestDdrRegs(uint32 repeatTime, uint32 delayUsBeforeCheck)
    {
    AtHal hal = AtDeviceIpCoreHalGet(CliDevice(), 0);
    uint32 time_i;

    AtDeviceDiagnosticModeEnable(CliDevice(), cAtTrue);
    AtHalWrite(hal, 0x00f20000, 0);
    AtHalWrite(hal, 0x00f22000, 0);

#define mCheckAddress(address, expectedValue) if (!ReadAndExpectValue(hal, address, expectedValue, AtSourceLocation, delayUsBeforeCheck)) return;

    for (time_i = 0; time_i < repeatTime; time_i++)
        {
        /* ddr test start 1-2 */
        mCheckAddress(0x00f20000, 0);
        AtHalWrite(hal, 0x00f20000, 0x00000001);
        mCheckAddress(0x00f20000, 1);
        AtHalWrite(hal, 0x00f20000, 0x00000003);
        mCheckAddress(0x00f20000, 0x00000003);
        mCheckAddress(0x00f22000, 0x00000000);
        AtHalWrite(hal, 0x00f22000, 0x00000001);
        mCheckAddress(0x00f22000, 0x00000001);
        AtHalWrite(hal, 0x00f22000, 0x00000003);
        mCheckAddress(0x00f22000, 0x00000003);

        /* ddr test stop 1-2 */
        mCheckAddress(0x00f20000, 0x00000003);
        AtHalWrite(hal, 0x00f20000, 0x00000001);
        mCheckAddress(0x00f20000, 0x00000001);
        mCheckAddress(0x00f20000, 0x00000001);
        AtHalWrite(hal, 0x00f20000, 0x00000000);
        mCheckAddress(0x00f22000, 0x00000003);
        AtHalWrite(hal, 0x00f22000, 0x00000001);
        mCheckAddress(0x00f22000, 0x00000001);
        mCheckAddress(0x00f22000, 0x00000001);
        AtHalWrite(hal, 0x00f22000, 0x00000000);
        }

    AtPrintc(cSevInfo, "No issues found\r\n");
    }

DEF_SDKCOMMAND(cliTestDdrRegs,
               "test ddr regs",
               "Test DDR related control registers",
               DEF_SDKPARAM("repeatTimes", "STRING", "Number of repeat times")
               DEF_SDKPARAM("delayTimeInUsBeforeCheck", "STRING", "Time to delay (us) before reading and check"),
               "${repeatTimes} ${delayTimeInUsBeforeCheck}")
    {
    uint32 repeatTime = AtStrToDw(lub_argv__get_arg(argv, 0));
    uint32 delayTimeInUsBeforeCheck = AtStrToDw(lub_argv__get_arg(argv, 1));
    TestDdrRegs(repeatTime, delayTimeInUsBeforeCheck);
    AtUnused(argv);
    AtUnused(context);
    return 0;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : ClishTest.c
 *
 * Created Date: Mar 7, 2014
 *
 * Description : Just for internal testing
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "commacro.h"
#include "AtDevice.h"
#include "AtPdhDe1.h"
#include "AtModuleSdh.h"
#include "AtSdhLine.h"
#include "AtModulePdh.h"
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
extern eAtRet AtZtePtnPdhDe1AutoTxAisEnable(AtPdhDe1 de1, eBool enable);
extern eBool AtZtePtnPdhDe1AutoTxAisIsEnabled(AtPdhDe1 de1);
extern AtDevice CliDevice(void);
extern void ArriveTextUIStart(void);
extern void ArriveTextUIFree(void);
extern void AtIdParserTest(void);
extern void AtTokenizerTest(void);
extern void AppInterruptMessageSend(void);

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("example", "Test example code")
TOP_SDKCOMMAND("example satop", "Test example code")
TOP_SDKCOMMAND("example satop liu", "Test example code")
TOP_SDKCOMMAND("test interrupt", "CLI to test interrupt")

static void TestTextUICliExecute(void)
    {
    if ((AtCliExecute("device show readwrite en en")   != 0) ||
        (AtCliExecute("device show readwrite dis dis") != 0))
        AtPrintc(cSevCritical, "ERROR: CLI execute fail\n");
    }

static void testDe1AutoTxAis(void)
    {
    AtPdhDe1 de1;
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(CliDevice(), cAtModuleSdh);
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(CliDevice(), cAtModulePdh);

    if (sdhModule)
        {
        AtSdhVc vc = AtSdhLineVc1xGet(AtModuleSdhLineGet(sdhModule, 0), 0, 0, 0, 0);
        de1 = (AtPdhDe1)AtSdhChannelMapChannelGet((AtSdhChannel)vc);
        }
    else
        de1 = AtModulePdhDe1Get(pdhModule, 0);

    if (de1 == NULL)
        {
        AtPrintc(cSevWarning, "%s: ignore test\r\n", __func__);
        return;
        }

    AtAssert(AtZtePtnPdhDe1AutoTxAisEnable(de1, cAtTrue) == cAtOk);
    AtAssert(AtZtePtnPdhDe1AutoTxAisIsEnabled(de1));
    AtAssert(AtZtePtnPdhDe1AutoTxAisEnable(de1, cAtFalse) == cAtOk);
    AtAssert(!AtZtePtnPdhDe1AutoTxAisIsEnabled(de1));
    }

DEF_SDKCOMMAND(cliTest,
               "test",
               "Test all of things",
               DEF_NULLPARAM,
               "")
    {
    AtUnused(argv);
    AtUnused(context);
    AtIdParserTest();
    testDe1AutoTxAis();
    AtTokenizerTest();
    TestTextUICliExecute();

    /* Other testing may be added */
    return 0;
    }

DEF_SDKCOMMAND(cliSendInterruptMessage,
               "test interrupt send",
               "Send interrupt message to HAL client",
               DEF_NULLPARAM,
               "")
    {
    AtUnused(argv);
    AtUnused(context);
    AppInterruptMessageSend();
    return 0;
    }

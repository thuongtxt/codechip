/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Test
 *
 * File        : ClishTestTextUI.c
 *
 * Created Date: Jun 30, 2016
 *
 * Description : CLI to test TextUI things
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtCliModule.h"
#include "AtCli.h"
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("test textui", "")
TOP_SDKCOMMAND("test textui listen", "")

static void WillExecuteCli(AtTextUI self, const char *cli, void *userData)
    {
    uint32 *value = (uint32 *)userData;
    *value = *value + 1;
    AtPrintc(cSevInfo, "Will execute CLI \"%s\", increase userdata to 1, it is now: %u\r\n", cli, *value);
    AtUnused(self);
    }

static void DidExecuteCliWithResult(AtTextUI self, const char *cli, int result, void *userData)
    {
    uint32 *value = (uint32 *)userData;
    *value = *value + 2;
    AtPrintc(cSevInfo, "Did execute CLI \"%s\", result = %d, increase userdata to 2, it is now: %u\r\n", cli, result, *value);
    AtUnused(self);
    }

static tAtTextUIListerner *TextUIListener(void)
    {
    static tAtTextUIListerner m_listener;
    static tAtTextUIListerner *pListener = NULL;

    if (pListener)
        return pListener;

    AtOsalMemInit(&m_listener, 0, sizeof(m_listener));
    m_listener.WillExecuteCli = WillExecuteCli;
    m_listener.DidExecuteCliWithResult = DidExecuteCliWithResult;
    pListener = &m_listener;

    return pListener;
    }

DEF_SDKCOMMAND(cliTestTextUIListenerStart,
               "test textui listen start",
               "Start text UI listener",
               DEF_NULLPARAM,
               "")
    {
    static uint32 userData = 0;
    AtTextUI textui = AtCliSharedTextUI();
    AtUnused(context);
    AtUnused(argv);
    AtTextUIListenerAdd(textui, TextUIListener(), &userData);
    return 0;
    }

DEF_SDKCOMMAND(cliTestTextUIListenerStop,
               "test textui listen stop",
               "Stop text UI listener",
               DEF_NULLPARAM,
               "")
    {
    AtTextUI textui = AtCliSharedTextUI();
    AtUnused(context);
    AtUnused(argv);
    AtTextUIListenerRemove(textui, TextUIListener());
    return 0;
    }


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : ClishTestPw.c
 *
 * Created Date: Oct 20, 2014
 *
 * Description : PW test CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <assert.h>
#include "../AtClish.h"
#include "AtDevice.h"
#include "AtModuleSdh.h"
#include "AtModulePw.h"
#include "AtModulePdh.h"
#include "AtSdhLine.h"
#include "AtSdhVc.h"
#include "AtSdhAug.h"
#include "AtSdhTug.h"
#include "AtPw.h"
#include "AtPdhDe1.h"
#include "AtZtePtn.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
extern AtDevice CliDevice(void);

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("test pw", "PW test CLI")

static AtModuleSdh SdhModule(void)
    {
    return (AtModuleSdh)AtDeviceModuleGet(CliDevice(), cAtModuleSdh);
    }

static AtModulePw PwModule(void)
    {
    return (AtModulePw)AtDeviceModuleGet(CliDevice(), cAtModulePw);
    }

static eAtRet MappingSetupForLine(uint8 lineId)
    {
    eAtRet ret = cAtOk;
    AtSdhLine line = AtModuleSdhLineGet(SdhModule(), lineId);
    AtSdhChannel vc4;
    uint8 tug3_i;

    ret |= AtSdhLineRateSet(line, cAtSdhLineRateStm1);

    ret |= AtSdhChannelMapTypeSet((AtSdhChannel)AtSdhLineAug1Get(line, 0), cAtSdhAugMapTypeAug1MapVc4);
    vc4 = (AtSdhChannel)AtSdhLineVc4Get(line, 0);
    ret |= AtSdhChannelMapTypeSet(vc4, cAtSdhVcMapTypeVc4Map3xTug3s);
    for (tug3_i = 0; tug3_i < AtSdhChannelNumberOfSubChannelsGet(vc4); tug3_i++)
        {
        AtSdhChannel tug3 = AtSdhChannelSubChannelGet(vc4, tug3_i);
        uint8 tug2_i;
        ret |= AtSdhChannelMapTypeSet(tug3, cAtSdhTugMapTypeTug3Map7xTug2s);
        for (tug2_i = 0; tug2_i < AtSdhChannelNumberOfSubChannelsGet(tug3); tug2_i++)
            {
            AtSdhChannel tug2 = AtSdhChannelSubChannelGet(tug3, tug2_i);
            uint8 tu1x_i;
            ret |= AtSdhChannelMapTypeSet(tug2, cAtSdhTugMapTypeTug2Map3xTu12s);
            for (tu1x_i = 0; tu1x_i < AtSdhChannelNumberOfSubChannelsGet(tug2); tu1x_i++)
                {
                AtSdhChannel tu1x = AtSdhChannelSubChannelGet(tug2, tu1x_i);
                AtSdhChannel vc1x = AtSdhChannelSubChannelGet(tu1x, 0);
                AtPdhChannel de1;
                ret |= AtSdhChannelMapTypeSet(vc1x, cAtSdhVcMapTypeVc1xMapDe1);
                de1 = (AtPdhChannel)AtSdhChannelMapChannelGet(vc1x);
                assert(de1);
                ret |= AtPdhChannelFrameTypeSet(de1, cAtPdhE1UnFrm);
                }
            }
        }

    return ret;
    }

static eAtRet MappingSetup(void)
    {
    uint8 line_i;
    eAtRet ret = cAtOk;

    for (line_i = 0; line_i < AtModuleSdhMaxLinesGet(SdhModule()); line_i++)
        ret |= MappingSetupForLine(line_i);

    return ret;
    }

static eAtRet PwSetup(uint32 pwId)
    {
    AtModulePw pwModule = PwModule();
    tAtZtePtnTag1 tag1;
    tAtZtePtnTag2 tag2;
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(CliDevice(), cAtModuleEth);
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(CliDevice(), cAtModuleSdh);
    uint8 mac[6] = {0xC0, 0xCA, 0xC0, 0xCA, 0xC0, 0xCA};
    eAtRet ret = cAtOk;
    AtPw pw = (AtPw)AtModulePwSAToPCreate(pwModule, pwId);
    uint8 lineId, aug1Id, tug3Id, tug2Id, tu1xId;
    AtChannel de1;
    AtSdhLine line;

    AtOsalMemInit(&tag1, 0, sizeof(tAtZtePtnTag1));
    AtOsalMemInit(&tag2, 0, sizeof(tAtZtePtnTag2));

    /* Tags */
    tag1.encapType = 10;
    tag2.cfi = 0;
    tag2.isMlpppIma = 0;
    tag2.priority = pwId % 8;
    tag2.stmPortId = pwId / 63;
    tag2.zteChannelId = pwId % 256;
    ret |= AtZtePwPtnHeaderSet(pw, mac, &tag1, &tag2);
    ret |= AtZtePtnPwExpectedTag1Set(pw, &tag1);
    ret |= AtZtePtnPwExpectedTag2Set(pw, &tag2);

    /* Common attributes */
    ret |= AtPwCwEnable(pw, cAtTrue);
    ret |= AtPwCwSequenceModeSet(pw, cAtPwCwSequenceModeWrapZero);
    ret |= AtPwCwLengthModeSet(pw, cAtPwCwLengthModePayload);
    ret |= AtPwRtpEnable(pw, cAtFalse);

    /* Jitter */
    ret |= AtPwJitterBufferSizeSet(pw, 10000);
    ret |= AtPwJitterBufferDelaySet(pw, 5000);
    ret |= AtPwPayloadSizeSet(pw, 256);
    ret |= AtPwReorderingEnable(pw, cAtTrue);
    ret |= AtPwSuppressEnable(pw, cAtTrue);
    ret |= AtPwCwPktReplaceModeSet(pw, cAtPwPktReplaceModeAis);
    ret |= AtPwLopsSetThresholdSet(pw, 5);
    ret |= AtPwLopsClearThresholdSet(pw, 5);

    /* Determine circuit to be bound */
    lineId = pwId / 63;
    aug1Id = 0;
    tug3Id = (pwId % 63) / 21;
    tug2Id = ((pwId % 63) % 21) / 3;
    tu1xId = pwId % 3;
    line = AtModuleSdhLineGet(sdhModule, lineId);
    de1 = AtSdhChannelMapChannelGet((AtSdhChannel)AtSdhLineVc1xGet(line, aug1Id, tug3Id, tug2Id, tu1xId));

    /* Activate */
    ret |= AtChannelEnable((AtChannel)pw, cAtTrue);
    ret |= AtPwEthPortSet(pw, AtModuleEthPortGet(ethModule, 0));
    ret |= AtPwCircuitBind(pw, de1);

    return ret;
    }

static eAtRet AllPwSetup(uint32 numPws)
    {
    uint32 pw_i;
    eAtRet ret = cAtOk;

    for (pw_i = 0; pw_i < numPws; pw_i++)
        ret |= PwSetup(pw_i);

    return ret;
    }

static void Setup(uint32 numPws)
    {
    tAtOsalCurTime startTime, curTime;

    AtOsalCurTimeGet(&startTime);
    assert(MappingSetup() == cAtOk);
    AtOsalCurTimeGet(&curTime);
    AtPrintc(cSevInfo, "Mapping takes %d(ms)\r\n", AtOsalDifferenceTimeInMs(&curTime, &startTime));

    AtOsalCurTimeGet(&startTime);
    assert(AllPwSetup(numPws) == cAtOk);
    AtOsalCurTimeGet(&curTime);
    AtPrintc(cSevInfo, "PW settup %d(ms)\r\n", AtOsalDifferenceTimeInMs(&curTime, &startTime));
    }

DEF_SDKCOMMAND(cliTestPwSetup,
               "test pw setup",
               "Test PW setup time",
               DEF_SDKPARAM_OPTION("NumPws", "STRING", "Num Pws"),
               "${NumPws}")
    {
    uint32 numPws = 252;

    if (lub_argv__get_count(argv))
        numPws = AtStrtol(lub_argv__get_arg(argv, 0), NULL, 10);

    AtPrintc(cSevInfo, "NumPw = %d\r\n", numPws);

    Setup(numPws);

    return 0;
    }

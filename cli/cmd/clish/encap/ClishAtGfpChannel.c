/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : ClishAtGfpChannel.c
 *
 * Created Date: Jun 9, 2014
 *
 * Description : CLISH for GFP channel
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
DEF_PTYPE("eAtGfpFrmMode", "select", "gfpf(1), gfpt(2)", "Frame mode")

TOP_SDKCOMMAND("encap gfp pti", "CLIs to control GFP PTI")
TOP_SDKCOMMAND("encap gfp pti monitor", "CLIs to control GFP PTI Monitor")
TOP_SDKCOMMAND("encap gfp upi", "CLIs to control GFP UPI")
TOP_SDKCOMMAND("encap gfp upi monitor", "CLIs to control GFP UPI Monitor")
TOP_SDKCOMMAND("encap gfp exi", "CLIs to control GFP EXI")
TOP_SDKCOMMAND("encap gfp exi monitor", "CLIs to control GFP EXI Monitor")
TOP_SDKCOMMAND("encap gfp pfi", "CLIs to control GFP PFI")
TOP_SDKCOMMAND("encap gfp fcs", "CLIs to control GFP FCS")
TOP_SDKCOMMAND("encap gfp fcs transmit", "CLIs to control GFP FCS Transmission")
TOP_SDKCOMMAND("encap gfp fcs monitor", "CLIs to control GFP FCS Monitor")
TOP_SDKCOMMAND("encap gfp csf", "CLIs to control GFP CSF")
TOP_SDKCOMMAND("encap gfp csf transmit", "CLIs to control GFP CSF Transmission")
TOP_SDKCOMMAND("encap gfp csf upi", "CLIs to control GFP CSF UPI Transmission")
TOP_SDKCOMMAND("encap gfp alarm", "CLIs to control GFP alarm capture")

DEF_SDKCOMMAND(cliAtGfpChannelTxPtiSet,
               "encap gfp pti transmit",
               "Set TX PTI\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("pti", "STRING", "TX PTI"),
               "${channelList} ${pti}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtGfpChannelExpectedPtiSet,
               "encap gfp pti expect",
               "Set expected PTI\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("pti", "STRING", "Expected PTI"),
               "${channelList} ${pti}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEncapGfpPtiMonitorEnable,
               "encap gfp pti monitor enable",
               "Enable PTI monitoring\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEncapGfpPtiMonitorDisable,
               "encap gfp pti monitor disable",
               "Disable PTI monitoring\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtGfpChannelTxUpiSet,
               "encap gfp upi transmit",
               "Set TX UPI\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("upi", "STRING", "TX UPI"),
               "${channelList} ${upi}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtGfpChannelExpectedUpiSet,
               "encap gfp upi expect",
               "Set expected UPI\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("upi", "STRING", "Expected UPI"),
               "${channelList} ${upi}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEncapGfpUpiMonitorEnable,
               "encap gfp upi monitor enable",
               "Enable UPI monitoring\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEncapGfpUpiMonitorDisable,
               "encap gfp upi monitor disable",
               "Disable UPI monitoring\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtGfpChannelTxExiSet,
               "encap gfp exi transmit",
               "Set TX EXI\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("exi", "STRING", "TX EXI"),
               "${channelList} ${exi}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtGfpChannelExpectedExiSet,
               "encap gfp exi expect",
               "Set expected EXI\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("exi", "STRING", "Expected EXI"),
               "${channelList} ${exi}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEncapGfpExiMonitorEnable,
               "encap gfp exi monitor enable",
               "Enable EXI monitoring\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEncapGfpExiMonitorDisable,
               "encap gfp exi monitor disable",
               "Disable EXI monitoring\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtGfpChannelTxPfiSet,
               "encap gfp pfi transmit",
               "Set TX PFI\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("pfi", "STRING", "TX PFI"),
               "${channelList} ${pfi}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEncapGfpShow,
               "show encap gfp",
               "Show GFP channel information\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEncapGfpTxFcsEnable,
               "encap gfp fcs transmit enable",
               "Enable FCS transmit\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEncapGfpTxFcsDisable,
               "encap gfp fcs transmit disable",
               "Disable FCS transmit\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEncapGfpFcsMonitorEnable,
               "encap gfp fcs monitor enable",
               "Enable FCS monitoring\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEncapGfpFcsMonitorDisable,
               "encap gfp fcs monitor disable",
               "Disable FCS monitoring\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEncapGfpFrameModeSet,
               "encap gfp frame",
               "Set GFP framing mode\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("frmMode", "eAtGfpFrmMode", ""),
               "${channelList} ${frmMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEncapGfpCounter,
               "show encap gfp counters",
               "Show GFP counters\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("counterReadMode", "HistoryReadingMode", ""),
               "${channelList} ${counterReadMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEncapGfpShowAlarm,
               "show encap gfp alarm",
               "Show GFP alarm information\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEncapGfpShowInterrupt,
               "show encap gfp interrupt",
               "Show GFP interrupt status\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("counterReadMode", "HistoryReadingMode", ""),
               "${channelList} ${counterReadMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEncapGfpPayloadScrambleEnable,
               "encap gfp scramble payload enable",
               "Enable Payload scrambling\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEncapGfpPayloadScrambleDisable,
               "encap gfp scramble payload disable",
               "Disable Payload scrambling\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEncapGfpCoreHeaderScrambleEnable,
               "encap gfp scramble coreheader enable",
               "Enable Core Header scrambling\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEncapGfpCoreHeaderScrambleDisable,
               "encap gfp scramble coreheader disable",
               "Disable Core Header scrambling\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDebugEncapGfp,
               "debug encap gfp",
               "To show information for debugging\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEncapGfpTxCsfEnable,
               "encap gfp csf transmit enable",
               "Enable CSF transmit\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEncapGfpTxCsfDisable,
               "encap gfp csf transmit disable",
               "Disable CSF transmit\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtGfpChannelTxCsfUpiSet,
               "encap gfp csf upi transmit",
               "Set TX CSF UPI\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("upi", "STRING", "TX CSF UPI"),
               "${channelList} ${upi}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtGfpChannelInterruptMask,
               "encap gfp interruptmask",
               "Set GFP interrupt mask\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("intrMask", "STRING", "Interrupt Mask")
               DEF_SDKPARAM("intrMaskEn", "eBool", ""),
               "${channelList} ${intrMask} ${intrMaskEn}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEncapGfpAlarmCapture,
               "encap gfp alarm capture",
               "Enable/disable alarm capturing\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("enable", "eBool", "Enable/disable alarm capturing"),
               "${channelList} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEncapGfpAlarmCaptureGet,
               "show encap gfp alarm capture",
               "Show captured alarms\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", "Read to clear without display table"),
               "${channelList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

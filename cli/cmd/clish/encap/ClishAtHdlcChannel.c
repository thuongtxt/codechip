/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : ClishAtHdlcChannel.c
 *
 * Created Date: Mar 21, 2016
 *
 * Description : CLI encapsulation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/
#define HexValue ".*"
#define ValueList "(("HexValue"[,])+"HexValue"|"HexValue")"
#define EncapHdlcDropPktType "(address-control-error|fcs-error)"
#define EncapHdlcDropPktMask "((("EncapHdlcDropPktType"[\\|])+"EncapHdlcDropPktType")|"EncapHdlcDropPktType")"

/*--------------------------- Macros -----------------------------------------*/
/* HDLC Defect */
#define HdlcDefectType "(fcs-error|undersize|oversize|decap-buffer-full|encap-buffer-full)"
#define HdlcDefectMask "((("HdlcDefectType"[\\|])+"HdlcDefectType")|"HdlcDefectType")"


/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("encap hdlc address", "Encapsulation Hdlc Address CLIs")
TOP_SDKCOMMAND("encap hdlc address insert", "Encapsulation Hdlc Address Insert CLIs")
TOP_SDKCOMMAND("encap hdlc address compare", "Encapsulation Hdlc Address Compare CLIs")
TOP_SDKCOMMAND("encap hdlc address bypass", "Encapsulation Hdlc Address Bypass CLIs")
TOP_SDKCOMMAND("encap hdlc control", "Encapsulation Hdlc Control CLIs")
TOP_SDKCOMMAND("encap hdlc control insert", "Encapsulation Hdlc Control Insert CLIs")
TOP_SDKCOMMAND("encap hdlc control compare", "Encapsulation Hdlc Control Compare CLIs")
TOP_SDKCOMMAND("encap hdlc control bypass", "Encapsulation Hdlc Control Bypass CLIs")
TOP_SDKCOMMAND("encap hdlc packet", "Encapsulation Hdlc Packet command CLIs")
TOP_SDKCOMMAND("encap hdlc packet drop", "Encapsulation Hdlc Packet Drop command CLIs")
TOP_SDKCOMMAND("encap laps", "Encapsulation LAPS CLIs")
TOP_SDKCOMMAND("encap laps sapi", "Encapsulation LAPS SAPI CLIs")
TOP_SDKCOMMAND("encap laps sapi monitor", "Encapsulation LAPS SAPI monitor CLIs")
TOP_SDKCOMMAND("show laps", "Show LAPS CLIs")
TOP_SDKCOMMAND("show laps link", "Show LAPS LINK CLIs")
TOP_SDKCOMMAND("show encap hdlc packet", "Show Encapsulation Hdlc Packet command CLIs")
TOP_SDKCOMMAND("show encap hdlc packet drop", "Show Encapsulation Hdlc Packet Drop command CLIs")

DEF_PTYPE("EncapHdlcDropPktMask", "regexp", EncapHdlcDropPktMask,
          "\nDrop condition masks type:\n"
          "- address-control-error    : Address/Control Error\n"
          "- fcs-error                : FCS Error\n"
          "Note, these masks can be ORed, example address-control-error|fcs-error\n")

DEF_PTYPE("ValueList", "regexp", ValueList,
          "List of value. Ex: 1 or 1,5,2,5\n ")

DEF_PTYPE("HdlcAlarmTypeMask", "regexp", HdlcDefectMask,
     "\nHDLC Alarm masks:\n"
     "- fcs-error          : FCS Error\n"
     "- undersize          : Packet Undersized\n"
     "- oversize           : Packet Oversized\n"
     "- decap-buffer-full  : Decapsulation buffer full\n"
     "- encap-buffer-full  : Encapsulation buffer full\n")


/* Enable Address Insert */
DEF_SDKCOMMAND(cliAtHdlcChannelAddrInsertEnable,
               "encap hdlc address insert enable",
               "Enable Address insertion\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

/* Disable Address Insert */
DEF_SDKCOMMAND(cliAtHdlcChannelAddrInsertDisable,
               "encap hdlc address insert disable",
               "Disable Address insertion\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

/* Enable Control Insert */
DEF_SDKCOMMAND(cliAtHdlcChannelCtrlInsertEnable,
               "encap hdlc control insert enable",
               "Enable Control insertion\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

/* Disable Control Insert */
DEF_SDKCOMMAND(cliAtHdlcChannelCtrlInsertDisable,
               "encap hdlc control insert disable",
               "Disable Control insertion\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

/* set Address transmit */
DEF_SDKCOMMAND(cliAtHdlcChannelAddrTransmit,
               "encap hdlc address transmit",
               "Set Address value to transmit\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("valueList", "ValueList", ""),
               "${channelList} ${valueList}")
    {
    mAtCliCall();
    }

/* set Control transmit */
DEF_SDKCOMMAND(cliAtHdlcChannelCtrlTransmit,
               "encap hdlc control transmit",
               "Set Control value to transmit\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("valueList", "ValueList", ""),
               "${channelList} ${valueList}")
    {
    mAtCliCall();
    }

/* Enable Address compare */
DEF_SDKCOMMAND(cliAtHdlcChannelAddrCompareEnable,
               "encap hdlc address compare enable",
               "Enable Address Comparison\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

/* Disable Address compare */
DEF_SDKCOMMAND(cliAtHdlcChannelAddrCompareDisable,
               "encap hdlc address compare disable",
               "Disable Address Comparison\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

/* Enable Control compare */
DEF_SDKCOMMAND(cliAtHdlcChannelCtrlCompareEnable,
               "encap hdlc control compare enable",
               "Enable Control Comparison\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

/* Disable Control compare */
DEF_SDKCOMMAND(cliAtHdlcChannelCtrlCompareDisable,
               "encap hdlc control compare disable",
               "Disable Control Comparison\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

/* set Address expect */
DEF_SDKCOMMAND(cliAtHdlcChannelAddressExpect,
               "encap hdlc address expect",
               "Set expected Address value\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("valueList", "ValueList", ""),
               "${channelList} ${valueList}")
    {
    mAtCliCall();
    }

/* set Control expect */
DEF_SDKCOMMAND(cliAtHdlcChannelControlExpect,
               "encap hdlc control expect",
               "Set exptected Control value\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("valueList", "ValueList", ""),
               "${channelList} ${valueList}")
    {
    mAtCliCall();
    }

/* Enable Address bypass */
DEF_SDKCOMMAND(cliAtHdlcChannelAddrBypassEnable,
               "encap hdlc address bypass enable",
               "Enable Address bypass\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

/* Disable Address bypass */
DEF_SDKCOMMAND(cliAtHdlcChannelAddrBypassDisable,
               "encap hdlc address bypass disable",
               "Disable Address bypass\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

/* Enable Control bypass */
DEF_SDKCOMMAND(cliAtHdlcChannelCtrlBypassEnable,
               "encap hdlc control bypass enable",
               "Enable Control bypass\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

/* Disable Control bypass */
DEF_SDKCOMMAND(cliAtHdlcChannelCtrlBypassDisable,
               "encap hdlc control bypass disable",
               "Disable Control bypass\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtLapsLinkTxSapiSet,
               "encap laps sapi transmit",
               "Set TX SAPI\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("sapi", "STRING", "TX SAPI"),
               "${channelList} ${sapi}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtLapsLinkExpectedSapiSet,
               "encap laps sapi expect",
               "Set Expected SAPI\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("sapi", "STRING", "TX SAPI"),
               "${channelList} ${sapi}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtLapsLinkSapiMonitorEnable,
               "encap laps sapi monitor enable",
               "Enable SAPI monitoring\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtLapsLinkSapiMonitorDisable,
               "encap laps sapi monitor disable",
               "Disable SAPI monitoring\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CLiAtLapsLinkShow,
               "show encap laps",
               "Show LAPS info \n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliEncapHdlcLkFlowBind,
               "encap hdlc link flowbind",
               "SET Flow binding\n",
               DEF_SDKPARAM("linkId", "HdlcLinkList", "")
               DEF_SDKPARAM("flowList", "EthFlowIds", "List of Ethernet flows"),
               "${linkId} ${flowList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CLiAtLapsLinkCountersShow,
               "show laps link counters",
               "Show LAPS Link Counters \n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("counterReadMode", "HistoryReadingMode", ""),
               "${channelList} ${counterReadMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHdlcChannelIdlePatternSet,
               "encap hdlc idlepattern",
               "Set HDLC Idle Pattern\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("idle", "BYTE_HEX", "Idle Pattern"),
               "${channelList} ${idle}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHdlcChannelAddressSizeSet,
               "encap hdlc address size",
               "Set HDLC address size\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("addressSize", "UINT", "Address Size"),
               "${addressSize} ${addressSize}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEncapHdlcDropPacketConditionMsk,
               "encap hdlc packet drop condition",
               "Set conditions that will drop packet\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("conditionsMask", "EncapHdlcDropPktMask", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${channelList} ${conditionsMask} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEncapHdlcDropPacketConditionShow,
               "show encap hdlc packet drop condition",
               "Show Condition that cause drop packet",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

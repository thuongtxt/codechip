/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : atcliencap.c
 *
 * Created Date: Oct 5, 2012
 *
 * Description : Cli encapsulation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/
#define CounterList "(("CounterType"[,])+"CounterType"|"CounterType")"
#define CounterType "(txGoodPkt|txAbortPkt|rxGoodPkt|rxAbortPkt|rxFcsErrPkt|rxAddrCtrlErrPkt|rxSapiErrPkt|rxErrPkt|all)"

/*--------------------------- Macros -----------------------------------------*/
/* Define regular expression for list of Channel IDs
 *
 * BNF format:
 * channel           ::= [1-8]
 * channnelLstElement ::= ((channel[-]channel)|channel)
 * channelLst        ::= ((lineLst[,]lineLstElement)|lineLstElement)
 *
 * Convert to regular expression:
 * lineLst: (lineLstElement[,])+lineLstElement|lineLstElement
 */
#define channelLstElement "(("regChannelno"[-]"regChannelno")|"regChannelno")"
#define channelLst "(("channelLstElement"[,])+"channelLstElement"|"channelLstElement")"
#define linkLstElement "(("regLinkno"[-]"regLinkno")|"regLinkno")"
#define linkLst "(("linkLstElement"[,])+"linkLstElement"|"linkLstElement")"
#define PhyLstElement "(("phyInf"[-]"phyInf")|"phyInf")"

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("encap", "Encapsulation CLIs")
TOP_SDKCOMMAND("encap channel", "Encapsulation Channel CLIs")
TOP_SDKCOMMAND("encap hdlc", "Encapsulation Hdlc CLIs")
TOP_SDKCOMMAND("encap hdlc link", "Encapsulation Hdlc Link CLIs")
TOP_SDKCOMMAND("encap hdlc link traffic", "Encapsulation Hdlc Link Traffic CLIs")
TOP_SDKCOMMAND("encap atmtc", "Encapsulation AtmTc CLIs")
TOP_SDKCOMMAND("encap gfp", "GFP CLIs")
TOP_SDKCOMMAND("encap gfp scramble", "CLIs to control GFP scrambling")
TOP_SDKCOMMAND("encap gfp scramble payload", "CLIs to control GFP payload scrambling")
TOP_SDKCOMMAND("encap gfp scramble coreheader", "CLIs to control GFP Core Header scrambling")
TOP_SDKCOMMAND("encap hdlc addresscontrol", "Address/control CLIs")
TOP_SDKCOMMAND("encap hdlc acfc", "CLIs to control address/control fields compression")
TOP_SDKCOMMAND("encap hdlc link oamegress", "Hdlc link OAM egress control CLIs")
TOP_SDKCOMMAND("encap hdlc link oamingress", "Hdlc link OAM ingress control CLIs")
TOP_SDKCOMMAND("encap hdlc link oamingress vlan", "Hdlc link OAM ingress VLAN control CLIs")
TOP_SDKCOMMAND("show hdlc link oamingress", "Hdlc link OAM ingress control CLIs")
TOP_SDKCOMMAND("debug encap", "Debug encapsulation")
TOP_SDKCOMMAND("debug encap hdlc", "Debug HDLC")
TOP_SDKCOMMAND("encap interrupt", "Module encap interrupt commands")

/* Data types */
DEF_PTYPE("EncChannelList", "regexp", ".*",
          "EncChannelIDs. Format as following:\n"
          "- VCG   : vcg.<vcgId>\n"
          "- ID.   : <id>")

DEF_PTYPE("HdlcLinkList", "regexp", ".*", "List of HDLC link IDs")
DEF_PTYPE("MtuVal", "integer", "64..9600", "MTU value")
DEF_PTYPE("PhyIntfList", "regexp", ".*",
          "Physical Interface IDs. Format as following:\n"
          "- DS1/E1 physical: de1.<de1IdList>\n"
          "- NxDS0 physical : nxds0.<de1Id>.<bitList>\n"
          "- VCG            : vcg.<vcgId>\n"
          "- PPP            : ppp.<linkId>\n"
          "- HDLC           : hdlc.<channelId>\n"
          "- FrameRelay     : framerelay.<frLinkId>\n"
          "Example: de1.1,2-5       - For DS1/E1 from 1 to 5\n"
          "         nxds0.1.2-5     - For NxDS0 bit 2-5 of DS1/E1 1\n"
          "         vcg.1-5         - For VCG from 1 to 5\n"
          "         ppp.1-5         - For PPP Link from 1 to 5\n"
          "         hdlc.1-5        - For HDLC Channel from 1 to 5\n"
          "         fr.1-5          - For Frame relay channel from 1 to 5\n"
          "         fr.1.1          - For Frame relay virtual circuit\n"
          "         mfr.1.1         - For Multi-Link Frame relay virtual circuit\n")

DEF_PTYPE("eAtEncapTypeCreate", "select", "cisco(1), ppp(2), framerelay(3), atm(4), gfp(5)","Encapsulation types")
DEF_PTYPE("eAtHdlcFrameType","select", "cisco(1), ppp(2), framerelay(3), laps(4), lapd(5)","Hdlc frame type")
DEF_PTYPE("eAtEncapType","select","hdlc(1), atm(2), gfp(3)","Channel encapsulation type")
DEF_PTYPE("eAtHdlcFcsMode", "select", "fcs16(0), fcs32(1), nofcs(3)", "HDLC FCS mode")
DEF_PTYPE("eAtHdlcFcsCalculationMode", "select", "msb(0), lsb(1)", "HDLC FCS Calculation mode")
DEF_PTYPE("eAtHdlcStuffMode", "select", "byte(0), bit(1), none(2)", "HDLC Stuffing mode")
DEF_PTYPE("eAtHdlcOamMd","select","topsn(1), tocpu(2), discard(3)","HDLC OAM link mode")
DEF_PTYPE("AtHdlcCounterList", "regexp", ".*",
          "\nHDLC Counter list, counters are separated by comma and counter types are as follow:\n"
          "- all              : all counters\n"
          "- txGoodPkt        : Encap good packet\n"
          "- txAbortPkt       : Encap Abort packet\n"
          "- rxGoodPkt        : Decap good packet\n"
          "- rxAbortPkt       : Decap Abort packet\n"
          "- rxFcsErrPkt      : Decap Fcs error packet\n"
          "- rxAddrCtrlErrPkt : Decap Address Control error packet\n"
          "- rxSapiErrPkt     : Decap Sapi error packet\n"
          "- rxErrPkt         : Decap error packet\n")
DEF_PTYPE("eAtHdlcLinkOamMode", "select", "topsn(1), tocpu(2)","OAM packet mode on HDLC Link")
DEF_PTYPE("eAtAtmNetworkInterfaceType", "select", "uni(1), nni(2)", "ATM Network interface mode")
DEF_PTYPE("CellMapMode", "select","directly(1), plcp(2)","ATM cell mapping mode")
DEF_PTYPE("HexMac", "regexp", ".*", "Mac value consists of six pairs of hexadecimal: CA.FE.CA.FE.CA.FE")
DEF_PTYPE("HexVal", "regexp", ".*", "Hex value")
DEF_PTYPE("EthFlowIds", "regexp", ".*","List of Ethernet flow ids")
DEF_PTYPE("eAtHdlcPduType","select", "ipv4(1), ipv6(2), ethernet(3), any(4)","Protocol Data Unit type")

/* Create encapsulation channel*/
DEF_SDKCOMMAND(cliEncapChannelCreate,
               "encap channel create",
               "Create encapsulation channel with specified encapsulation type\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("encapType", "eAtEncapTypeCreate", ""),
               "${channelList} ${encapType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEncapChannelEnable,
               "encap channel enable",
               "\nThis command is to enable encap channel list.\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEncapChannelDisable,
               "encap channel disable",
               "\nThis command is to disable encap channel list.\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

/* Delete encapsulation channel*/
DEF_SDKCOMMAND(cliEncapChannelDelete,
               "encap channel delete",
               "Delete encapsulation channels\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

/* Bind physical*/
DEF_SDKCOMMAND(cliEncapBindPhy,
               "encap channel bind",
               "Bind Physical interfaces for encapsulation channels\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("physicalInterfaceList", "PhyIntfList", ""),
               "${channelList} ${physicalInterfaceList}")
    {
    mAtCliCall();
    }

/* SET scramble */
DEF_SDKCOMMAND(cliEncapChannelScramble,
               "encap channel scramble",
               "Enable/disable scrambling or check if this configuration is enabled\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${channelList} ${enable}")
    {
    mAtCliCall();
    }

/* SET ISIS MAC translation */
DEF_SDKCOMMAND(cliHdlchIsisMac,
               "encap hdlcisismac",
               "SET ISIS MAC translation\n",
               DEF_SDKPARAM("mac", "HexMac", "ISIS MAC"),
               "${mac} ")
    {
    mAtCliCall();
    }

/* Show module information contains*/
DEF_SDKCOMMAND(cliEncapShow,
               "show encap",
               "Show encapsulation module information\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

/* Show encapsulation channel*/
DEF_SDKCOMMAND(cliEncapChannelShow,
               "show encap channel",
               "Show encapsulation channel configuration\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

/* SET Flag configuration */
DEF_SDKCOMMAND(cliEncapHdlcFlag,
               "encap hdlc flag",
               "SET HDLC flag\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("flagValue", "BYTE_HEX", "flag value"),
               "${channelList} ${flagValue}")
    {
    mAtCliCall();
    }

/* SET FCS mode */
DEF_SDKCOMMAND(cliEncapHdlcFcsMode,
               "encap hdlc fcs",
               "SET FCS mode\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("fcsMode", "eAtHdlcFcsMode", ""),
               "${channelList} ${fcsMode}")
    {
    mAtCliCall();
    }

/* SET stuffing mode */
DEF_SDKCOMMAND(cliEncapHdlcStuffMode,
               "encap hdlc stuffing",
               "SET stuffing mode\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("stuffingMode", "eAtHdlcStuffMode", ""),
               "${channelList} ${stuffingMode}")
    {
    mAtCliCall();
    }

/* SET MTU */
DEF_SDKCOMMAND(cliEncapHdlcMtu,
               "encap hdlc mtu",
               "SET mtu value\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("mtu", "MtuVal", ""),
               "${channelList} ${mtu}")
    {
    mAtCliCall();
    }

/* Show HDLC channel configuration */
DEF_SDKCOMMAND(cliEncapHdlcShow,
               "show encap hdlc",
               "Show HDLC channel configuration:\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

/* Read/Clear HDLC counters */
DEF_SDKCOMMAND(cliEncapHdlcCounter,
               "show encap hdlc counters",
               "Read/clear HDLC counters\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("counterReadMode", "HistoryReadingMode", "read only/read to clear mode")
			   DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${channelList} ${counterReadMode} ${silent}")
    {
    mAtCliCall();
    }

/* SET OAM mode configuration */
DEF_SDKCOMMAND(cliEncapLkOAM,
               "encap hdlc link oammode",
               "SET OAM mode\n",
               DEF_SDKPARAM("linkList", "HdlcLinkList", "")
               DEF_SDKPARAM("oamMode", "eAtHdlcOamMd", "HDLC OAM link mode"),
               "${linkList} ${oamMode}")
    {
    mAtCliCall();
    }

/* Flow binding*/
DEF_SDKCOMMAND(cliEncapHdlcLkFlowBind,
               "encap hdlc link flowbind",
               "SET Flow binding\n",
               DEF_SDKPARAM("linkId", "HdlcLinkList", "")
               DEF_SDKPARAM("flowList", "EthFlowIds", "List of Ethernet flows"),
               "${linkId} ${flowList}")
    {
    mAtCliCall();
    }

/* Traffic TX enabling*/
DEF_SDKCOMMAND(cliEncapHdlcLkTrafficTxEn,
               "encap hdlc link traffic txenable",
               "Traffic Tx enabling\n",
               DEF_SDKPARAM("linkList", "HdlcLinkList", ""),
               "${linkList}")
    {
    mAtCliCall();
    }

/* Traffic TX disabling*/
DEF_SDKCOMMAND(cliEncapHdlcLkTrafficTxDis,
               "encap hdlc link traffic txdisable",
               "Traffic Tx disabling\n",
               DEF_SDKPARAM("linkList", "HdlcLinkList", ""),
               "${linkList}")
    {
    mAtCliCall();
    }

/* Traffic RX enabling*/
DEF_SDKCOMMAND(cliEncapHdlcLkTrafficRxEn,
               "encap hdlc link traffic rxenable",
               "Traffic Rx enabling\n",
               DEF_SDKPARAM("linkList", "HdlcLinkList", ""),
               "${linkList}")
    {
    mAtCliCall();
    }

/* Traffic RX disabling*/
DEF_SDKCOMMAND(cliEncapHdlcLkTrafficRxDis,
               "encap hdlc link traffic rxdisable",
               "Traffic Rx disabling\n",
               DEF_SDKPARAM("linkList", "HdlcLinkList", ""),
               "${linkList}")
    {
    mAtCliCall();
    }

/* Show HDLC link configuration*/
DEF_SDKCOMMAND(cliEncapHdlcLkShow,
               "show encap hdlc link",
               "Show HDLC link configuration\n",
               DEF_SDKPARAM("linkList", "HdlcLinkList", ""),
               "${linkList}")
    {
    mAtCliCall();
    }

/* Configure ATM TC*/
DEF_SDKCOMMAND(cliEncapAtmTcNetwokIntf,
               "encap atmtc networkinterface",
               "Configure ATM TC Network interface type\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("networkinf", "eAtAtmNetworkInterfaceType", ""),
               "${channelList} ${networkinf}")
    {
    mAtCliCall();
    }

/* Configure ATM TC*/
DEF_SDKCOMMAND(cliEncapAtmTcCellMapMode,
               "encap atmtc cellmapmode",
               "Configure ATM TC cell mapping mode\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("cellMapMode", "CellMapMode", ""),
               "${channelList} ${cellMapMode}")
    {
    mAtCliCall();
    }

/*Show ATM TC configuration*/
DEF_SDKCOMMAND(cliEncapAtmTc,
               "show encap atmtc",
               "Show ATM TC Configuration\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

/*Show ATM TC counters*/
DEF_SDKCOMMAND(cliEncapAtmTcCounter,
              "show encap atmtc counters",
              "Show or clear ATM TC Counter\n",
              DEF_SDKPARAM("channelList", "EncChannelList", "")
              DEF_SDKPARAM("counterReadMode", "HistoryReadingMode", "read only/read to clear mode"),
              "${channelList} ${counterReadMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHdlcChannelAddressControlBypass,
              "encap hdlc addresscontrol bypass",
              "Enable/disable address/control bypassing to Ethernet side\n",
              DEF_SDKPARAM("channelList", "EncChannelList", "")
              DEF_SDKPARAM("enable", "eBool", "Enable/disable address/control bypassing to Ethernet side\n"),
              "${channelList} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHdlcChannelAddressControlErrorBypass,
              "encap hdlc addresscontrol bypasserror",
              "Enable/disable frames that address/control are mismatch be bypassed to Ethernet side\n",
              DEF_SDKPARAM("channelList", "EncChannelList", "")
              DEF_SDKPARAM("enable", "eBool", "Enable/disable frames that address/control are mismatch be bypassed to Ethernet side\n"),
              "${channelList} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHdlcChannelAddressControlInsert,
              "encap hdlc addresscontrol insert",
              "Enable/disable address/control insertion\n",
              DEF_SDKPARAM("channelList", "EncChannelList", "")
              DEF_SDKPARAM("enable", "eBool", "Enable/disable address/control insertion\n")
              DEF_SDKPARAM("address", "HexValue", "Address\n")
              DEF_SDKPARAM("control", "HexValue", "Control\n"),
              "${channelList} ${enable} ${address} ${control}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHdlcChannelAddressControlCompare,
              "encap hdlc addresscontrol compare",
              "Enable/disable address/control comparison\n",
              DEF_SDKPARAM("channelList", "EncChannelList", "")
              DEF_SDKPARAM("enable", "eBool", "Enable/disable address/control comparison\n")
              DEF_SDKPARAM("expectedAddress", "HexValue", "Expected address\n")
              DEF_SDKPARAM("expectedControl", "HexValue", "Expected control\n"),
              "${channelList} ${enable} ${address} ${control}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHdlcChannelFcsErrorBypass,
              "encap hdlc fcs bypasserror",
              "Enable/disable FCS error bypassing to Ethernet side\n",
              DEF_SDKPARAM("channelList", "EncChannelList", "")
              DEF_SDKPARAM("enable", "eBool", "Enable/disable FCS error bypassing to Ethernet side\n"),
              "${channelList} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHdlcChannelShowAddressControl,
              "show encap hdlc addresscontrol",
              "Show HDLC address/control configuration\n",
              DEF_SDKPARAM("channelList", "EncChannelList", ""),
              "${channelList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHdlcLinkOamEgDestMacSet,
               "encap hdlc link oamegress destmac",
               "Set Ethernet Egress Destination MAC Address\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("mac", "MacAddr", ""),
               "${channelList} ${mac}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHdlcLinkOamEgSrcMacSet,
               "encap hdlc link oamegress srcmac",
               "\nThis command is to set source MAC for link OAM flow\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("mac", "MacAddr", ""),
               "${channelList} ${mac}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHdlcLinkOamEgVlanSet,
               "encap hdlc link oamegress vlan",
               "Set Ethernet Egress VLAN\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("portId", "PortId", "")
               DEF_SDKPARAM("cVlan", "VlanDesc", "")
               DEF_SDKPARAM("sVlan", "VlanDesc", ""),
               "${channelList} ${portId} ${cVlan} ${sVlan}")
    {
    mAtCliCall();
    }
    
DEF_SDKCOMMAND(cliAtHdlcLinkOamEgressVlanAdd,
               "encap hdlc link oamegress vlan add",
               "Add Ethernet Egress VLAN\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("portId", "PortId", "")
               DEF_SDKPARAM("cVlan", "VlanDesc", "")
               DEF_SDKPARAM("sVlan", "VlanDesc", ""),
               "${channelList} ${portId} ${cVlan} ${sVlan}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHdlcLinkOamEgressVlanRemove,
               "encap hdlc link oamegress vlan remove",
               "Remove Ethernet Egress VLAN\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("portId", "PortId", "")
               DEF_SDKPARAM("cVlan", "VlanDesc", "")
               DEF_SDKPARAM("sVlan", "VlanDesc", ""),
               "${channelList} ${portId} ${cVlan} ${sVlan}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHdlcLinkOamIgVlanAdd,
               "encap hdlc link oamingress vlan add",
               "Set Ethernet Ingress VLAN\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("portId", "PortId", "")
               DEF_SDKPARAM("cVlan", "VlanDesc", "")
               DEF_SDKPARAM("sVlan", "VlanDesc", ""),
               "${EncChannelList} ${portId} ${cVlan} ${sVlan}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHdlcLinkOamIgVlanRemove,
               "encap hdlc link oamingress vlan remove",
               "Remove Ethernet Ingress VLAN\n",
               DEF_SDKPARAM("EncChannelList", "EncChannelList", "")
               DEF_SDKPARAM("portId", "PortId", "")
               DEF_SDKPARAM("cVlan", "VlanDesc", "")
               DEF_SDKPARAM("sVlan", "VlanDesc", ""),
               "${EncChannelList} ${portId} ${cVlan} ${sVlan}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHdlcLinkOamFlowGet,
               "show encap hdlc link oamflow",
               "Show encap HDLC link OAM flow information\n",
               DEF_SDKPARAM("EncChannelList", "EncChannelList", ""),
               "${EncChannelList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModuleEncapIdlePatternSet,
               "encap idlepattern",
               "Set Idle pattern for unused physical interfaces\n",
               DEF_SDKPARAM("pattern", "HexValue", "Idle pattern\n"),
               "${pattern}")
    {
    mAtCliCall();
    }

/* SET frame hdlc type */
DEF_SDKCOMMAND(cliEncapHdlcFrameTypeSet,
               "encap hdlc frametype",
               "Change HDLC frame type",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("frameType", "eAtHdlcFrameType", ""),
               "${channelList} ${frameType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHdlcChannelAddrCtrlTxCompressEnable,
              "encap hdlc acfc enable tx",
              "Enable address/control compression at TX direction",
              DEF_SDKPARAM("channelList", "EncChannelList", ""),
              "${channelList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHdlcChannelAddrCtrlTxCompressDisable,
              "encap hdlc acfc disable tx",
              "Disable address/control compression at TX direction",
              DEF_SDKPARAM("channelList", "EncChannelList", ""),
              "${channelList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHdlcChannelAddrCtrlRxCompressEnable,
              "encap hdlc acfc enable rx",
              "Enable address/control compression at RX direction",
              DEF_SDKPARAM("channelList", "EncChannelList", ""),
              "${channelList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHdlcChannelAddrCtrlRxCompressDisable,
              "encap hdlc acfc disable rx",
              "Disable address/control compression at RX direction",
              DEF_SDKPARAM("channelList", "EncChannelList", ""),
              "${channelList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHdlcChannelAddrCtrlCompressEnable,
              "encap hdlc acfc enable",
              "Enable address/control compression at both RX and TX directions",
              DEF_SDKPARAM("channelList", "EncChannelList", ""),
              "${channelList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHdlcChannelAddrCtrlCompressDisable,
              "encap hdlc acfc disable",
              "Disable address/control compression at both RX and TX directions",
              DEF_SDKPARAM("channelList", "EncChannelList", ""),
              "${channelList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModuleEncapDebugLoopback,
              "debug encap loopback",
              "Loopack Encap module",
              DEF_SDKPARAM("loopbackMode", "eAtLoopbackMode", ""),
              "${loopbackMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModuleEncapDebug,
              "debug module encap",
              "Show debug information of Encap module",
              DEF_NULLPARAM,
              "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHdlcChannelDebug,
               "debug encap channel",
               "To show information for debugging\n",
               DEF_SDKPARAM("channelList", "EncChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEncapChannelFlowBind,
               "encap channel flowbind",
               "Bind Encap channel to Ethernet flow\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("flowList", "EthFlowIds", "List of Ethernet flows"),
              "${channelList} ${flowList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHdlcLinkPduSet,
               "encap hdlc link pdu",
               "Set Protocol Data Unit Type for HDLC Link\n",
               DEF_SDKPARAM("linkList", "HdlcLinkList", "")
               DEF_SDKPARAM("pduType", "eAtHdlcPduType", ""),
               "${linkList} ${pduType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEncapHdlcFcsCalculationSet,
               "encap hdlc fcs calculation",
               "Config to calculate mode of FCS from MSB or LSB\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("calculationMode", "eAtHdlcFcsCalculationMode", ""),
               "${channelList} ${calculationMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHdlcLinkDebug,
               "debug encap hdlc link",
               "Show debug information for HDLC Link\n",
               DEF_SDKPARAM("linkList", "HdlcLinkList", ""),
               "${linkList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliModuleEncapInterruptEnable,
               "encap interrupt enable",
               "Enable encap channel interrupt.\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliModuleEncapInterruptDisable,
               "encap interrupt disable",
               "Disable encap channel interrupt.\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliModuleEncapShow,
               "show module encap",
               "Show module encap.\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }


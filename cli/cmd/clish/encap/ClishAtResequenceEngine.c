/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : ClishAtResequenceEngine.c
 *
 * Created Date: May 29, 2017
 *
 * Description : Cli encapsulation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("show encap", "Encapsulation show CLIs")
TOP_SDKCOMMAND("show encap resequence", "Encapsulation show re-sequence CLIs")
TOP_SDKCOMMAND("encap resequence", "Encapsulation re-sequence CLIs")
TOP_SDKCOMMAND("encap resequence engine", "Encapsulation re-sequence engine CLIs")

DEF_SDKCOMMAND(CliAtEncapResequenceEngineEnable,
               "encap resequence engine enable",
               "Enable resequence engine",
               DEF_SDKPARAM("engineIds", "UINT", "Re-sequence engine IDs:1, 2, 1-2, ..."),
               "${engineIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtEncapResequenceEngineDisable,
               "encap resequence engine disable",
               "Disable resequence engine",
               DEF_SDKPARAM("engineIds", "UINT", "Re-sequence engine IDs:1, 2, 1-2, ..."),
               "${engineIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtEncapResequenceEngineQueue,
               "encap resequence engine queue",
               "Set resequence engine",
               DEF_SDKPARAM("engineIds", "UINT", "Re-sequence engine IDs:1, 2, 1-2, ...")
               DEF_SDKPARAM("queueId", "UINT", "Re-sequence engine port queue ID"),
               "${engineIds} ${queueId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtEncapResequenceEngineQueueEnable,
               "encap resequence engine queue enable",
               "Enable requence engine queue",
               DEF_SDKPARAM("engineIds", "UINT", "Re-sequence engine IDs:1, 2, 1-2, ..."),
               "${engineIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtEncapResequenceEngineQueueDisable,
               "encap resequence engine queue disable",
               "Disable requence engine queue",
               DEF_SDKPARAM("engineIds", "UINT", "Re-sequence engine IDs:1, 2, 1-2, ..."),
               "${engineIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtEncapResequenceEngineTimeoutSet,
               "encap resequence engine timeout",
               "Set timeout resequence engine",
               DEF_SDKPARAM("engineIds", "UINT", "Re-sequence engine IDs:1, 2, 1-2, ...")
               DEF_SDKPARAM("timeoutMs", "UINT", "Time out (milisecond)"),
               "${engineIds} ${timeoutMs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtEncapResequenceEngineMsruSet,
               "encap resequence engine msru",
               "Set timeout resequence engine",
               DEF_SDKPARAM("engineIds", "UINT", "Re-sequence engine IDs:1, 2, 1-2, ...")
               DEF_SDKPARAM("msru", "UINT", "Maximum sequence resequence unit "),
               "${engineIds} ${msru}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtEncapResequenceEngineMrruSet,
               "encap resequence engine mrru",
               "Set timeout resequence engine",
               DEF_SDKPARAM("engineIds", "UINT", "Re-sequence engine IDs:1, 2, 1-2, ...")
               DEF_SDKPARAM("mrru", "UINT", ""),
               "${engineIds} ${mrru}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtShowEncapResequenceEngine,
               "show encap resequence engine",
               "Show resequence engine",
               DEF_SDKPARAM("engineIds", "UINT", "Re-sequence engine IDs:1, 2, 1-2, ..."),
               "${engineIds}")
    {
    mAtCliCall();
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ALU
 *
 * File        : ClishAtAlu.c
 *
 * Created Date: Nov 26, 2014
 *
 * Description : CLISH CLI ALU
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../AtClish.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("alu", "ALU specific commands")
TOP_SDKCOMMAND("alu xaui", "CLIs to control XAUI")
TOP_SDKCOMMAND("alu qdr", "CLIs to control QDR")
TOP_SDKCOMMAND("alu sem", "CLIs to control SEM")
TOP_SDKCOMMAND("show alu", "CLIs to show ALU specific information")
TOP_SDKCOMMAND("show alu xaui", "CLIs to show XAUI")
TOP_SDKCOMMAND("show alu pw", "CLIs to show PW")
TOP_SDKCOMMAND("show alu qdr", "CLIs to show QDR")
TOP_SDKCOMMAND("show alu ddr", "CLIs to show DDR")
TOP_SDKCOMMAND("show alu sticky", "CLIs to show sticky")
TOP_SDKCOMMAND("show alu sem", "CLIs to show SEM")
TOP_SDKCOMMAND("show alu ge", "CLIs to show GE information")
TOP_SDKCOMMAND("show alu ge fifo", "CLIs to show GE Fifo")

DEF_PTYPE("Position", "select", "nearend(0), farend(1)", "Position")

DEF_SDKCOMMAND(cliAluXauiRead,
             "alu xaui read",
             "Read XAUI register.\n",
             DEF_SDKPARAM("page", "RegisterAddress", "")
             DEF_SDKPARAM("address", "RegisterAddress", "")
             DEF_SDKPARAM_OPTION("pretty", "RegisterDisplayFormat", ""),
             "${page} ${address} ${pretty} ")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAluXauiWrite,
             "alu xaui write",
             "Write XAUI register.\n",
             DEF_SDKPARAM("page", "RegisterAddress", "")
             DEF_SDKPARAM("address", "RegisterAddress", "")
             DEF_SDKPARAM("value", "RegisterValue", ""),
             "${page} ${address} ")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAluXauiReset,
              "alu xaui reset",
              "Start reset XAUI then stop reset after 100ms.\n",
              DEF_NULLPARAM,
              "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAluXauiResetStart,
              "alu xaui reset start",
              "Start reset.\n",
              DEF_NULLPARAM,
              "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAluXauiResetStop,
              "alu xaui reset stop",
              "Stop reset.\n",
              DEF_NULLPARAM,
              "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAluXauiStatusShow,
              "show alu xaui status",
              "Show XAUI status.\n",
              DEF_SDKPARAM("readingMode", "HistoryReadingMode", ""),
              "${readingMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtAluPerformanceMonitoringShow,
              "show alu pw pm",
              "Show Performance monitoring parameters.\n",
              DEF_SDKPARAM("pwList", "PwIdList", "")
              DEF_SDKPARAM("position", "Position", ""),
              "${pwList} ${position}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtAluQdrTest,
               "alu qdr test",
               "",
               DEF_SDKPARAM("qdrList", "PwIdList", ""),
               "${qdrList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtAluQdrStatus,
               "show alu qdr status",
               "",
               DEF_SDKPARAM("qdrList", "PwIdList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", ""),
               "${qdrList} ${readingMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAluPllStickyGet,
               "show alu sticky pll",
               "",
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", ""),
               "${readingMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAluFifoStickyGet,
               "show alu sticky fifo",
               "",
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", ""),
               "$${readingMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAluGeneralStickyGet,
               "show alu sticky general",
               "",
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", ""),
               "${readingMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAluClockGet,
               "show alu clock",
               "",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAluDdrMarginsDetect,
               "show alu ddr margins",
               "",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAluSemValidate,
               "alu sem validate",
               "",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAluSemStatusGet,
               "show alu sem status",
               "",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtAluQdrMargin,
               "show alu qdr margins",
               "",
               DEF_SDKPARAM("qdrList", "PwIdList", ""),
               "${qdrList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtAluGeFifoStatusGet,
               "show alu ge fifo status",
               "",
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", ""),
              "${readingMode}")
    {
    mAtCliCall();
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : ClishAtZtePtn.c
 *
 * Created Date: Jun 28, 2013
 *
 * Description : ZTE PTN specific CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("zte", "ZTE specific commands")
TOP_SDKCOMMAND("show zte", "ZTE specific show commands")
TOP_SDKCOMMAND("zte eth", "ZTE specific commands for Eth")
TOP_SDKCOMMAND("show zte eth", "ZTE specific show commands for Ethernet")
TOP_SDKCOMMAND("show zte device", "Show device status")
TOP_SDKCOMMAND("zte eth port", "ZTE specific commands for Ethernet Port")
TOP_SDKCOMMAND("zte hal", "ZTE specific commands for HAL")

DEF_PTYPE("ZteTag1", "regexp", ".*",
          "ZTE Tag 1. Format: priority.cfi.cpuPktIndicator.encapType.pktLen\n"
          "Where:\n"
          "+ priority       : 0..7\n"
          "+ cfi            : 0/1\n"
          "+ cpuPktIndicator: 0/1\n"
          "+ encapType      : 0..31\n")

DEF_PTYPE("ZteTag2", "regexp", ".*",
          "ZTE Tag 2. Format: priority.cfi.stmPortId.isMlppp.channelId\n"
          "Where:\n"
          "+ priority : 0..7\n"
          "+ cfi      : 0/1\n"
          "+ stmPortId: 0..7\n"
          "+ isMlppp  : 0/1\n"
          "+ channelId: 0..255\n")

DEF_SDKCOMMAND(cliAtZtePtnDeviceStatusShow,
               "show zte device status",
               "Show device status - ZTE PTN specific information",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtZtePtnEthPortSwitch,
               "zte eth port switch",
               "Switch traffic which is selected from one port to another port",
               DEF_SDKPARAM("fromPortId", "UINT", "Source port")
               DEF_SDKPARAM("toPortId", "UINT", "Destination port"),
               "${fromPortId} ${toPortId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtZtePtnHalDiagMemTest,
               "zte hal memorytest",
               "",
               DEF_SDKPARAM("regAddr", "STRING", "register address")
               DEF_SDKPARAM("dataMask", "STRING", "data Mask")
               DEF_SDKPARAM("numTimes", "STRING", "num test times"),
               "${regAddr} ${dataMask} ${numTimes}")
    {
    mAtCliCall();
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : ClishAtZtePtnPpp.c
 *
 * Created Date: Oct 28, 2013
 *
 * Description : ZTE PTN specific CLIs - PPP products
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("zte eth flow", "ZTE specific commands for Eth Flow")
TOP_SDKCOMMAND("zte eth flow higig", "ZTE specific commands for Higig")
TOP_SDKCOMMAND("zte eth flow higig header", "ZTE specific commands for Higig header")
TOP_SDKCOMMAND("zte eth flow header", "Commands to control ZTE tags")
TOP_SDKCOMMAND("zte hdlc", "ZTE specific commands for Hdlc")
TOP_SDKCOMMAND("zte hdlc link", "ZTE specific commands for Hdlc Link")
TOP_SDKCOMMAND("zte hdlc link oam", "Commands to control ZTE tags Oam")
TOP_SDKCOMMAND("zte hdlc link oam higig", "Commands to control Higig ZTE tags Oam")
TOP_SDKCOMMAND("zte hdlc link oam higig header", "Commands to control Higig ZTE tags Oam header")
TOP_SDKCOMMAND("zte hdlc link oam header", "Commands to control Header ZTE tags Oam ")
TOP_SDKCOMMAND("show zte eth flow", "ZTE specific show commands for Ethernet Flow")
TOP_SDKCOMMAND("show zte hdlc", "ZTE specific show commands for Hdlc")
TOP_SDKCOMMAND("show zte hdlc link", "ZTE specific show commands for Hdlc Link")
TOP_SDKCOMMAND("show zte hdlc link oam", "ZTE specific show commands for Hdlc Link Oam")
TOP_SDKCOMMAND("zte ppptable", "CLIs to control PPP table")
TOP_SDKCOMMAND("zte mlppptable", "CLIs to control MLPPP table")
TOP_SDKCOMMAND("zte mlppptable entry", "CLIs to control MLPPP entry table")
TOP_SDKCOMMAND("zte chdlctable", "CLIs to control CHdlc table")
TOP_SDKCOMMAND("zte chdlctable entry", "CLIs to control CHdlc entry table")
TOP_SDKCOMMAND("zte frtable", "CLIs to control Frame relay table")
TOP_SDKCOMMAND("zte frtable entry", "CLIs to control Frame relay entry table")

DEF_SDKCOMMAND(cliAtZteEthFlowExpectedHeaderSet,
               "zte eth flow header expect",
               "Set expected header",
               DEF_SDKPARAM("flowIdList", "FlowIdList", "")
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("expectedTag1", "ZteTag1", "")
               DEF_SDKPARAM("expectedTag2", "ZteTag2", ""),
               "${flowIdList} ${portList} ${expectedTag1} ${expectedTag2}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtZteEthFlowTxHeaderSet,
               "zte eth flow header transmit",
               "Set ZTE Ethernet Header Information for transmitting to ETH side",
               DEF_SDKPARAM("flowIdList", "FlowIdList", "")
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("expectedTag1", "ZteTag1", "")
               DEF_SDKPARAM("expectedTag2", "ZteTag2", ""),
               "${flowIdList} ${portList} ${expectedTag1} ${expectedTag2}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHdlcLinkOamTxEthHeaderSet,
               "zte hdlc link oam header transmit",
               "Set Oam ZTE Ethernet Header Information for transmitting to ETH side",
               DEF_SDKPARAM("linkList", "PppLinklist", "")
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("expectedTag1", "ZteTag1", "")
               DEF_SDKPARAM("expectedTag2", "ZteTag2", ""),
               "${linkList} ${portList} ${expectedTag1} ${expectedTag2}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtHdlcLinkOamIgTagsSet,
               "zte hdlc link oam header expect",
               "Set expected tags for Ppp Oam lookup at RX direction of ETH",
               DEF_SDKPARAM("linkList", "PppLinklist", "")
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("expectedTag1", "ZteTag1", "")
               DEF_SDKPARAM("expectedTag2", "ZteTag2", ""),
               "${linkList} ${portList} ${expectedTag1} ${expectedTag2}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtHdlcLinkOamTagsGet,
               "show zte hdlc link oam header",
               "Show ZTE hdlc link oam header information",
               DEF_SDKPARAM("linkList", "PppLinklist", ""),
               "${linkList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtZteEthFlowTxHeaderGet,
               "show zte eth flow header",
               "Show ZTE eth flow header information",
               DEF_SDKPARAM("linkList", "PppLinklist", ""),
               "${linkList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtZtePtnPppTableShow,
               "show zte ppptable",
               "Show ZTE PPP table",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtZtePtnPppEntrySet,
               "zte ppptable entry",
               "Configure a PPP table entry",
               DEF_SDKPARAM("entries", "EntryLists", "")
               DEF_SDKPARAM("pid", "STRING", "PID")
               DEF_SDKPARAM("ethType", "STRING", "ETH TYPE")
               DEF_SDKPARAM("zteEncapType", "STRING", "ZTE Encap Type"),
               "${entries} ${pid} ${ethType} ${zteEncapType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtZtePtnPppLookupEntryEnable,
               "zte ppptable entry enable",
               "Enable a PPP table entry",
               DEF_SDKPARAM("entries", "EntryLists", ""),
               "${entries}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtZtePtnPppLookupEntryDisable,
               "zte ppptable entry disable",
               "Disable a PPP table entry",
               DEF_SDKPARAM("entries", "EntryLists", ""),
               "${entries}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtZtePtnPppLookupEntryCfiSet,
               "zte ppptable entry cfi",
               "Set CFI",
               DEF_SDKPARAM("entries", "EntryLists", "")
               DEF_SDKPARAM("cfi", "STRING", "[0, 1] CFI"),
               "${entries} {cfi}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtZtePtnPppLookupEntryPrioritySet,
               "zte ppptable entry priority",
               "Set Priority",
               DEF_SDKPARAM("entries", "EntryLists", "")
               DEF_SDKPARAM("priority", "STRING", "[0..7] Priority"),
               "${entries} {priority}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtZtePtnMlpppLookupEntryEncapTypeSet,
               "zte mlppptable entry encaptype",
               "Set MLPPP encap type",
               DEF_SDKPARAM("entries", "EntryLists", "")
               DEF_SDKPARAM("zteEncapType", "STRING", "ZTE Encap Type"),
               "${entries} {zteEncapType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtZtePtnCHdlcLookupEntryEncapTypeSet,
               "zte chdlctable encaptype",
               "Set CHDLC encap type",
               DEF_SDKPARAM("zteEncapType", "STRING", "ZTE Encap Type"),
               "{zteEncapType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtZtePtnCHdlcLookupEntryEncapTypeGet,
               "show zte chdlctable",
               "Get CHDLC table",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtZtePtnCHdlcLookupEntryPrioritySet,
               "zte chdlctable entry priority",
               "Set Priority",
               DEF_SDKPARAM("pkttype", "STRING", "")
               DEF_SDKPARAM("priority", "STRING", "[0..7] Priority"),
               "${pkttype} {priority}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtZtePtnCHdlcLookupEntryCfiSet,
               "zte chdlctable entry cfi",
               "Set CFI",
               DEF_SDKPARAM("pkttype", "STRING", "")
               DEF_SDKPARAM("cfi", "STRING", "[0, 1] CFI"),
               "${pkttype} {cfi}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtZtePtnFrLookupEntryCfiSet,
               "zte frtable entry cfi",
               "Set Priority",
               DEF_SDKPARAM("priority", "STRING", "[0..7] Priority"),
               "{prioritys}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtZtePtnFrLookupEntryPrioritySet,
               "zte frtable entry priority",
               "Set Priority",
               DEF_SDKPARAM("cfi", "STRING", "[0, 1] CFI"),
               "{cfi}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtZtePtnFrLookupEntryEncapTypeSet,
               "zte frtable encaptype",
               "Set Frame relay encap type",
               DEF_SDKPARAM("zteEncapType", "STRING", "ZTE Encap Type"),
               "{zteEncapType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtZtePtnFrLookupEntryPidTableGet,
               "show zte frtable",
               "Get pid table of Frame relay mode",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtZteEthFlowTxHigigHeaderSet,
               "zte eth flow higig header transmit",
               "Set ZTE Higig Ethernet Header Information for transmitting to ETH side",
               DEF_SDKPARAM("flowIdList", "FlowIdList", "")
               DEF_SDKPARAM("portList", "PortIdList", "Ethernet Port")
               DEF_SDKPARAM("stmPortList", "PortIdList", "STM Port")
               DEF_SDKPARAM("txTag1", "ZteTag1", "")
               DEF_SDKPARAM("txTag2", "ZteTag2", ""),
               "${flowIdList} ${portList} ${stmPortList} ${txTag1} ${txTag2}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtZteEthFlowHigigHeaderExpectedSet,
               "zte eth flow higig header expect",
               "Set Expected Higig header for PPP lookup at RX direction of ETH",
               DEF_SDKPARAM("flowIdList", "FlowIdList", "")
               DEF_SDKPARAM("portList", "PortIdList", "Ethernet Port")
               DEF_SDKPARAM("stmPortList", "PortIdList", "STM Port")
               DEF_SDKPARAM("expectedTag1", "ZteTag1", "")
               DEF_SDKPARAM("expectedTag2", "ZteTag2", ""),
               "${flowIdList} ${portList} ${stmPortList} ${expectedTag1} ${expectedTag2}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHdlcLinkOamHigigTxHeaderSet,
               "zte hdlc link oam higig header transmit",
               "Set OAM ZTE Ethernet Higig Header Information for transmitting to ETH side",
               DEF_SDKPARAM("linkList", "PppLinklist", "")
               DEF_SDKPARAM("portList", "PortIdList", "Ethernet Port")
               DEF_SDKPARAM("stmPortList", "PortIdList", "STM Port")
               DEF_SDKPARAM("txTag1", "ZteTag1", "")
               DEF_SDKPARAM("txTag2", "ZteTag2", ""),
               "${linkList} ${portList} ${stmPortList} ${txTag1} ${txTag2}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHdlcLinkOamHigigExpectedHeaderSet,
               "zte hdlc link oam higig header expect",
               "Set expected oam ppp tags for PPP lookup at RX direction of Higig ETH",
               DEF_SDKPARAM("linkList", "PppLinklist", "")
               DEF_SDKPARAM("portList", "PortIdList", "Ethernet Port")
               DEF_SDKPARAM("stmPortList", "PortIdList", "STM Port")
               DEF_SDKPARAM("expectedTag1", "ZteTag1", "")
               DEF_SDKPARAM("expectedTag2", "ZteTag2", ""),
               "${linkList} ${portList} ${stmPortList} ${expectedTag1} ${expectedTag2}")
    {
    mAtCliCall();
    }


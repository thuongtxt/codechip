/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : ClishAtZtePtnPw.c
 *
 * Created Date: Oct 28, 2013
 *
 * Description : ZTE PTN specific CLIs - PW products
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("zte pw", "ZTE specific commands for PW")
TOP_SDKCOMMAND("zte pw tags", "Commands to control ZTE tags")
TOP_SDKCOMMAND("show zte pw", "ZTE specific show commands for PW")
TOP_SDKCOMMAND("zte pw sendlbittocdr", "ZTE specific command to control sending Lbit packets to CDR module")

DEF_SDKCOMMAND(CliPwZteHeaderSet,
               "zte pw ethheader",
               "Set ZTE Ethernet Header Information for transmitting to ETH side",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("mac", "MacAddr", "DMAC Address")
               DEF_SDKPARAM("zteTag1", "ZteTag1", "")
               DEF_SDKPARAM("zteTag2", "ZteTag2", ""),
               "${pwList} ${mac} ${zteTag1} ${zteTag2}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwZteExpectedTagsSet,
               "zte pw tags expect",
               "Set expected tags for PW lookup at RX direction of ETH",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("expectedTag1", "ZteTag1", "")
               DEF_SDKPARAM("expectedTag2", "ZteTag2", ""),
               "${pwList} ${expectedTag1} ${expectedTag2}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwZteHeaderShow,
               "show zte pw header",
               "Show ZTE header information",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwZteSendLbitPacketsToCdrEnable,
               "zte pw sendlbittocdr enable",
               "Enable sending Lbit packets to CDR module\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(clPwZteSendLbitPacketsToCdrDisable,
               "zte pw sendlbittocdr disable",
               "Disable sending Lbit packets to CDR module\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(clPwZteSendLbitPacketsToCdrGet,
               "show zte pw sendlbittocdr",
               "Show option of sending Lbit packets to CDR module\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLISH
 *
 * File        : ClishZteTextUI.c
 *
 * Created Date: Feb 6, 2014
 *
 * Description : Text-UI for ZTE platform
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static AtTextUI m_clishTextUI = NULL;
static AtTextUI m_tinyTextUI  = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern int AtSkdTclEvalFile(const char *filename);
void tcl(const char *scriptName);

/*--------------------------- Implementation ---------------------------------*/
static AtTextUI TextUI(void)
    {
    /* Already created */
    if (m_clishTextUI)
        return m_clishTextUI;

    /* Somewhere else is using default text UI, cleanup by stopping */
    AtCliStop();

    /* Just create and setup a new one */
    m_tinyTextUI  = AtDefaultTinyTextUINew();
    m_clishTextUI = AtClishTextUINew(m_tinyTextUI);
    AtCliSharedTextUISet(m_clishTextUI);

    return m_clishTextUI;
    }

void tcl(const char *scriptName)
    {
    AtTextUIVerbose(TextUI(), cAtTrue);
    AtSkdTclEvalFile(scriptName);
    AtTextUIVerbose(TextUI(), cAtFalse);
    }

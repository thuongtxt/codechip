/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : ClishZte.c
 *
 * Created Date: Aug 21, 2013
 *
 * Description : For ZTE platform. Note, this file cannot be linked to AT
 *               application, it is only for ZTE platform
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsal.h"
#include "AtDevice.h"
#include "../../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
struct mod_thread
    {
    pthread_t pthrid;
    pthread_attr_t attr;

    struct sched_param schedparan;
    cpu_set_t cpuset;
    int bindcpu;
    int stacksize;

    void *(*mod_func)(void *);
    void * mode_data;
    void *priv_data;
    };

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void stm(eBool loadFpga);
void e1(eBool loadFpga);
extern void BSP_dbg_cp3banFpgaLoad(uint16 slotId);
extern void BSP_cp3banBoardInitialize(uint16 slotId);
extern void Ce1tanFpgaDownLoad(uint16 slotId);
extern void DrvAtFpgaInit(uint16 slotId);

/*--------------------------- Implementation ---------------------------------*/
void stm(eBool loadFpga)
    {
    static uint16 cSlotId = 1;

    if (loadFpga)
        BSP_dbg_cp3banFpgaLoad(cSlotId);
    BSP_cp3banBoardInitialize(cSlotId);
    }

void e1(eBool loadFpga)
    {
    static uint16 cSlotId = 1;

    if (loadFpga)
        Ce1tanFpgaDownLoad(cSlotId);
    DrvAtFpgaInit(cSlotId);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : ClishAtLoop.c
 *
 * Created Date: Mar 10, 2014
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("loop", "Loop specific commands")
TOP_SDKCOMMAND("loop serdes", "Loop specific commands")
TOP_SDKCOMMAND("loop temperature", "Loop temperature commands")
TOP_SDKCOMMAND("loop temperature redalarm", "Loop temperature RED alarm commands")
TOP_SDKCOMMAND("loop temperature redalarm reboot", "Loop temperature RED alarm reboot commands")
TOP_SDKCOMMAND("loop temperature threshold", "Loop temperature threshold commands")
TOP_SDKCOMMAND("loop device", "Loop device commands")

TOP_SDKCOMMAND("show loop", "Show LOOP information commands")
TOP_SDKCOMMAND("show loop sdh", "Show LOOP SDH information commands")
TOP_SDKCOMMAND("show loop device", "Show LOOP device LED information commands")

TOP_SDKCOMMAND("loop device led", "Loop device LED commands")
TOP_SDKCOMMAND("loop device led status", "Loop device status LED commands")
TOP_SDKCOMMAND("loop device led active", "Loop device active LED commands")

TOP_SDKCOMMAND("loop sdh", "Loop SDH commands")
TOP_SDKCOMMAND("loop sdh line", "Loop SDH line commands")
TOP_SDKCOMMAND("loop sdh line led", "Loop SDH line LED commands")

DEF_SDKCOMMAND(CliAtLoopSerdesRead,
               "loop serdes rd",
               "Serdes Read",
               DEF_SDKPARAM("address", "RegisterAddress", "")
               DEF_SDKPARAM_OPTION("portId", "UINT", "Ethernet Port Id"),
               "${address} ${portId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtLoopSerdesWrite,
             "loop serdes wr",
             "Serdes Write",
             DEF_SDKPARAM("address", "RegisterAddress", "")
             DEF_SDKPARAM("value", "RegisterValue", "")
             DEF_SDKPARAM_OPTION("portId", "UINT", "Ethernet Port Id"),
             "${address} ${value} ${portId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtLoopTemperatureRedAlarmSetThreshold,
             "loop temperature threshold setredalarm",
             "Set temperature SET threshold of RED alarm",
             DEF_SDKPARAM("celsicusValue", "UINT", ""),
             "${celsicusValue}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtLoopTemperatureRedAlarmRebootEnable,
             "loop temperature redalarm reboot enable",
             "Enable reboot system when reaching to RED alarm",
             DEF_NULLPARAM,
             "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtLoopTemperatureRedAlarmRebootDisable,
             "loop temperature redalarm reboot disable",
             "Disable reboot system when reaching to RED alarm",
             DEF_NULLPARAM,
             "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtLoopTemperatureRedAlarmClearThreshold,
             "loop temperature threshold clearredalarm",
             "Set temperature CLEAR threshold of RED alarm",
             DEF_SDKPARAM("celsicusValue", "UINT", ""),
             "${celsicusValue}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtLoopTemperatureYellowAlarmSetThreshold,
             "loop temperature threshold setyellowalarm",
             "Set temperature SET threshold of YELLOW alarm",
             DEF_SDKPARAM("celsicusValue", "UINT", ""),
             "${celsicusValue}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtLoopTemperatureYellowAlarmClearThreshold,
             "loop temperature threshold clearyellowalarm",
             "Set temperature CLEAR threshold of YELLOW alarm",
             DEF_SDKPARAM("celsicusValue", "UINT", ""),
             "${celsicusValue}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtLoopTemperatureShow,
               "show loop temperature",
               "Show information of FPGA temperature\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtLoopDeviceRedLedStateSet,
             "loop device led status red",
             "Turn ON/OFF card status red LED (PIN G22)",
             DEF_SDKPARAM("ledState", "eAtLedState", ""),
             "${ledState}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtLoopDeviceGreenLedStateSet,
             "loop device led status green",
             "Turn ON/OFF card status green LED (PIN B26)",
             DEF_SDKPARAM("ledState", "eAtLedState", ""),
             "${ledState}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtLoopDeviceRedLedActivationSet,
             "loop device led active red",
             "Turn ON/OFF card active red LED (PIN AA22)",
             DEF_SDKPARAM("ledState", "eAtLedState", ""),
             "${ledState}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtLoopDeviceGreenLedActivationSet,
             "loop device led active green",
             "Turn ON/OFF card active green LED (PIN F21)",
             DEF_SDKPARAM("ledState", "eAtLedState", ""),
             "${ledState}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtLoopSdhLineGreenLedStateSet,
               "loop sdh line led green",
               "Turn ON/OFF STM Port green LEDs:\n"
               "  - Port 1 : PIN T8  \n"
               "  - Port 2 : PIN K21 \n"
               "  - Port 3 : PIN N23 \n"
               "  - Port 4 : PIN Y8",
               DEF_SDKPARAM("lines", "SdhLineList", "")
               DEF_SDKPARAM("ledState", "eAtLedState", ""),
               "${lines} ${ledState}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtLoopSdhLineRedLedStateSet,
               "loop sdh line led red",
               "Turn ON/OFF STM Port red LEDs:\n"
               "  - Port 1 : PIN T8  \n"
               "  - Port 2 : PIN K21 \n"
               "  - Port 3 : PIN N23 \n"
               "  - Port 4 : PIN Y8",
               DEF_SDKPARAM("lines", "SdhLineList", "")
               DEF_SDKPARAM("ledState", "eAtLedState", ""),
               "${lines} ${ledState}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtLoopSdhLineStateShow,
               "show loop sdh line",
               "Show Line LEDs status and SFP status\n",
               DEF_SDKPARAM("lines", "SdhLineList", ""),
               "${lines}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtLoopDeviceLedShow,
               "show loop device led",
               "Show Loop device LED state and activation state\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

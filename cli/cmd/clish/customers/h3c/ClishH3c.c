/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : ClishH3c.c
 *
 * Created Date: Jul 16, 2013
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "sys/types.h"

#include "atclib.h"
#include "sdkcmd.h"
#include "attypes.h"
#include "AtCommon.h"
#include "AtCli.h"

/*--------------------------- Define -----------------------------------------*/
/* DS1/E1 list */
#define de1no "([1-9]|3[0-2]|1[0-9]|2[0-9])"
#define de1id de1no"|["de1no"-"de1no"]|"de1no","de1no
#define De1List de1id

/* Card type */
#define cCardTypeE1  1
#define cCardTypeStm 2

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
DEF_PTYPE("LOOP_TYPE_LIU", "select", " loopin(1) loopout(2) noloop(3) init(4) loopdigital(5)", "loop types")
DEF_PTYPE("LOOP_TYPE_GE", "select", " loopin(1) loopout(2) noloop(3) init(4) ", "loop types")
DEF_PTYPE("PLL_MODE", "select", " master(1) slaver(2) ", "pll mode (master/slaver)")
DEF_PTYPE("CARD_TYPE", "select", " e1(1) stm(2) ", "card types")
DEF_PTYPE("PORT_ID", "integer", "1..2", "port Id")
DEF_PTYPE("SLOT_ID", "integer", "1..2", "slot Id")
DEF_PTYPE("LIUHEXNUM", "regexp", "(0[xX]([0-9a-fA-F]{1,4}))", "hex num")
DEF_PTYPE("LiuDe1List", "regexp", De1List,
          "\nDS1/E1 list are as follow:\n"
          "- 1: DE1 ID list is 1\n"
          "- 1-5: DE1 ID list from 1 to 5\n"
          "- 1,5-9: DE1 ID list are 1 and 5 to 9\n")

TOP_SDKCOMMAND("h3c", "H3C platform specific CLIs")

#ifdef __ARCH_POWERPC__

static inline uint16 sync_in_be16(const volatile uint16 *addr)
    {
    uint16 ret;

    __asm__ __volatile__("lhz%U1%X1 %0,%1; twi 0,%0,0; sync; isync"
            : "=r" (ret) : "m" (*addr));
    return ret;
    }

static inline void sync_out_be16(volatile uint16 *addr, uint16 val)
    {
    __asm__ __volatile__("sth%U0%X0 %1,%0; sync;isync"
            : "=m" (*addr) : "r" (val));
    }

/* base_addr : address of slot card ( slot 1 or slot 2 )
   addr_reg   : offset of addr register on main card ( read write indirect )
   config_reg : offset of config register on main card
   addr      : offset of register on h3c card cpld
   data      : data to write at addr ( in phy device )
*/

static uint16 phy_write(uint32 base_addr,
                        uint32 addr_reg,
                        uint32 data_write_reg,
                        uint32 config_reg,
                        uint16 addr,
                        uint16 data)
    {
    sync_out_be16((uint16*) (base_addr + (addr_reg * 2)), addr);
    sync_out_be16((uint16*) (base_addr + (data_write_reg * 2)), data);
    sync_out_be16((uint16*) (base_addr + (config_reg * 2)), 0xfff5);
    AtOsalUSleep(10000);

    return 0;
    }

static uint16 phy_read(uint32 base_addr,
                       uint32 addr_reg,
                       uint32 data_read_reg,
                       uint32 config_reg,
                       uint16 addr)
    {
    uint16 data;

    sync_out_be16((uint16*) (base_addr + (addr_reg * 2)), addr);
    sync_out_be16((uint16*) (base_addr + (config_reg * 2)), 0xfff1);
    AtOsalUSleep(10000);
    data = sync_in_be16((uint16*) (base_addr + (data_read_reg * 2)));

    return data;
    }

DEF_SDKCOMMAND(cliLoopAtTranceiverSTM,
               "h3c ge",
               "Loop STM port\r\n",
               DEF_SDKPARAM("slot", "SLOT_ID", "slot id")
               DEF_SDKPARAM("port", "PORT_ID", "port id")
               DEF_SDKPARAM("lMode", "LOOP_TYPE_GE", "loop mode"),
        "${slot} ${port} ${lMode}")
    {
    const char *_slot = lub_argv__get_arg(argv, 0);
    const char *_port = lub_argv__get_arg(argv, 1);
    const char *_lMode = lub_argv__get_arg(argv, 2);
    uint32 lMode = AtStrToDw(_lMode);
    uint32 base_addr = 0, addr_reg = 0, data_write_reg = 0, data_read_reg = 0, config_reg = 0;
    uint16 addr[4];
    uint16 value[4];
    uint32 length = 0;
    uint32 slot = AtStrtol(_slot, NULL, 10);
    uint32 port = AtStrtol(_port, NULL, 10);
    uint32 i;

    AtUnused(context);

    if (slot == 1)
        base_addr = 0xd0200000;
    else if (slot == 2)
        base_addr = 0xd0400000;

    if (port == 1)
        {
        addr_reg       = 0x77;
        data_write_reg = 0x76;
        data_read_reg  = 0x75;
        config_reg     = 0x17;
        }

    else if (port == 2)
        {
        addr_reg       = 0x7a;
        data_read_reg  = 0x78;
        data_write_reg = 0x79;
        config_reg     = 0x18;
        }

    /*
     * Device Init 0x16    0x12
     *     0x14    0x4
     *
     * External Loopback   0x16    0x1
     *     0x10    0x520d
     *
     * NO External Loopback    0x16    0x1
     *     0x10    0x420d
     *
     * internal Loopback   0x16    0x0
     *     0x0 0x5140
     *
     * No internal Loopback    0x16    0x0
     *     0x0 0x1140
     */
    switch (lMode)
        {
        case 1:
            addr [0] = 0x16;
            value[0] = 0x0;
            addr [1] = 0x0;
            value[1] = 0x5140;
            length   = 2;
            break;

        case 2:
            addr [0] = 0x16;
            value[0] = 0x1;
            addr [1] = 0x10;
            value[1] = 0x520d;
            length   = 2;
            break;

        case 3:
            addr [0] = 0x16;
            value[0] = 0x1;
            addr [1] = 0x10;
            value[1] = 0x420d;
            addr [2] = 0x16;
            value[2] = 0x0;
            addr [3] = 0x0;
            value[3] = 0x1140;
            length   = 4;
            break;

        case 4:
            addr [0] = 0x16;
            value[0] = 0x12;
            addr [1] = 0x14;
            value[1] = 0x4;
            length = 2;
            break;

        default:
            break;
        }

    for (i = 0; i < length; i++)
        {
        phy_write(base_addr,
                  addr_reg,
                  data_write_reg,
                  config_reg,
                  addr[i],
                  value[i]);
        AtPrintf ("addr 0x%x write 0x%x read 0x%x\n",addr[i],value[i],
                 phy_read(base_addr,
                          addr_reg,
                          data_read_reg,
                          config_reg,
                          addr[i]));
        }

    return cAtOk;
    }

/* base_addr : address of slot card ( slot 1 or slot 2)
   addr_reg   : offset of addr register on main card( read write indirect)
   config_reg : offset of config register on main card
   addr      : offset of register on h3c card cpld
   data      : value of data to write
*/
static uint16 liu_write(uint32 base_addr,
                        uint32 addr_reg,
                        uint32 data_write_reg,
                        uint32 config_reg,
                        uint16 addr,
                        uint16 data)
    {
    sync_out_be16((uint16*) (base_addr + (addr_reg * 2)), addr);
    sync_out_be16((uint16*) (base_addr + (data_write_reg * 2)), data);
    sync_out_be16((uint16*) (base_addr + (config_reg * 2)), 0xfffb);
    AtOsalUSleep(10000);

    return 0;
    }

static uint32 SlotBaseAddress(uint8 slotId)
    {
    return (slotId == 1) ? 0xd0200000 : 0xd0400000;
    }

static uint16 liu_read(uint32 base_addr,
                       uint32 addr_reg,
                       uint32 data_read_reg,
                       uint32 config_reg,
                       uint16 addr)
    {
    uint16 data;

    sync_out_be16((uint16*) (base_addr + (addr_reg * 2)), addr);
    sync_out_be16((uint16*) (base_addr + (config_reg * 2)), 0xffff);
    AtOsalUSleep(10000);
    data = sync_in_be16((uint16*) (base_addr + (data_read_reg * 2)));

    return data;
    }

DEF_SDKCOMMAND(cliLoopAtTranceiverE1,
               "h3c liu",
               "Loop E1 LIU\r\n",
               DEF_SDKPARAM("slot", "SLOT_ID", "slot id")
               DEF_SDKPARAM("lMode", "LOOP_TYPE_LIU", "loop mode")
               DEF_SDKPARAM_OPTION("de1List", "LiuDe1List", "DS1/E1 List:\n"),
               "${slot} ${lMode} ${de1List}")
    {
    const char *_slot    = lub_argv__get_arg(argv, 0);
    const char *_lMode   = lub_argv__get_arg(argv, 1);
    const char *pDe1List = lub_argv__get_arg(argv, 2);
    uint32 slot;
    uint32 i;
    unsigned int chmsk = 0;
    uint32 lMode;
    uint32 base_addr = 0, addr_reg = 0, data_write_reg = 0, data_read_reg = 0, config_reg = 0;
    uint16 addr[] = {
            0x1f,
            0x3f,
            0xA ,
            0x2A,
            0xA ,
            0x2A,
            0xF ,
            0x2F,
            0x1f,
            0x3f,
            0x6 ,
            0x1f,
            0x3f,
            0x1 ,
            0x21,
            0x5 ,
            0x25,
            0x1f,
            0x3f,
            0x07,
            0x27,
            0x1f,
            0x3f,
            0x10,
            0x11,
            0x10,
            0x11,
            0x10,
            0x11,
            0x10,
            0x11,
            0x10,
            0x11,
            0x10,
            0x11,
            0x10,
            0x11,
            0x10,
            0x11,
            0x10,
            0x11,
            0x10,
            0x11,
            0x10,
            0x11,
            0x10,
            0x11,
            0x10,
            0x11,
            0x10,
            0x11,
            0x10,
            0x11,
            0x10,
            0x11,
            0x30,
            0x31,
            0x30,
            0x31,
            0x30,
            0x31,
            0x30,
            0x31,
            0x30,
            0x31,
            0x30,
            0x31,
            0x30,
            0x31,
            0x30,
            0x31,
            0x30,
            0x31,
            0x30,
            0x31,
            0x30,
            0x31,
            0x30,
            0x31,
            0x30,
            0x31,
            0x30,
            0x31,
            0x30,
            0x31,
            0x30,
            0x31,
            0x1f,
            0x3f,
            0x00,
            0x20,
            0x1f,
            0x3f,
            0x12,
            0x32,
            0x1f,
            0x3f,
            0x1 ,
            0x21,
            0x1f,
            0x3f,
            0x2 ,
            0x22,
            0x1f,
            0x3f,
            0xc ,
            0x2c,
    };
    uint16 value[] = {
            0x00,
            0x00,
            0xff,
            0xff,
            0x00,
            0x00,
            0x83,
            0x83,
            0x01,
            0x01,
            0x01,
            0xAA,
            0xAA,
            0x00,
            0x00,
            0xff,
            0xff,
            0x01,
            0x01,
            0xff,
            0xff,
            0x00,
            0x00,
            0x00,
            0x08,
            0x01,
            0x08,
            0x02,
            0x08,
            0x03,
            0x08,
            0x04,
            0x08,
            0x05,
            0x08,
            0x06,
            0x08,
            0x07,
            0x08,
            0x08,
            0x08,
            0x09,
            0x08,
            0x10,
            0x08,
            0x11,
            0x08,
            0x12,
            0x08,
            0x13,
            0x08,
            0x14,
            0x08,
            0x15,
            0x08,
            0x00,
            0x08,
            0x01,
            0x08,
            0x02,
            0x08,
            0x03,
            0x08,
            0x04,
            0x08,
            0x05,
            0x08,
            0x06,
            0x08,
            0x07,
            0x08,
            0x08,
            0x08,
            0x09,
            0x08,
            0x10,
            0x08,
            0x11,
            0x08,
            0x12,
            0x08,
            0x13,
            0x08,
            0x14,
            0x08,
            0x15,
            0x08,
            0xAA,
            0xAA,
            0xFF,
            0xFF,
            0x00,
            0x00,
            0xff,
            0xff,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
    };
    uint32 length;

    AtUnused(context);

    /* Parse card slot ID */
    slot = AtStrtol(_slot, NULL, 10);

    /* Get loop mode */
    lMode = AtStrToDw(_lMode);

    length = sizeof(addr) / sizeof(*addr);

    if (lub_argv__get_count(argv) >= 3)
        {
        AtIdParser parser = AtIdParserNew((const char *)pDe1List, NULL, NULL);

        while (AtIdParserHasNextNumber(parser))
            chmsk |= cBit0 << (AtIdParserNextNumber(parser) - 1);

        AtObjectDelete((AtObject)parser);
        }

    chmsk = 0;

    if (slot == 1)
        base_addr = 0xd0200000;
    else if (slot == 2)
        base_addr = 0xd0400000;

    addr_reg       = 0x19;
    data_write_reg = 0x1a;
    data_read_reg  = 0x1b;
    config_reg     = 0x18;

    switch (lMode)
        {
        case 5:
            // Digital loopback
            addr [0] = 0x1f;
            value[0] = 0x00;
            addr [1] = 0x3f;
            value[1] = 0x00;
            if (chmsk)
                {
                addr [2] = 0x0c;
                value[2] = (uint16)(liu_read(base_addr,
                                    addr_reg,
                                    data_read_reg,
                                    config_reg,
                                    addr[2]) | ((chmsk) & 0xff));
                addr [3] = 0x2c;
                value[3] = (uint16)(liu_read(base_addr,
                                    addr_reg,
                                    data_read_reg,
                                    config_reg,
                                    addr[3]) | ((chmsk >> 8) & 0xff));
                }
            else
                {
                addr [2] = 0x0c;
                value[2] = 0xff;
                addr [3] = 0x2c;
                value[3] = 0xff;
                }

            /* Un-loop analog and remote */
            addr [4] = 0x1;
			value[4] = 0x0;
			addr [5] = 0x21;
			value[5] = 0x0;

			addr [6] = 0x2;
			value[6] = 0x0;
			addr [7] = 0x22;
			value[7] = 0x0;

            length = 8;
            break;
        case 2: /* remote loop */
            addr [0] = 0x1f;
            value[0] = 0x00;
            addr [1] = 0x3f;
            value[1] = 0x00;
            if (chmsk)
                {
                addr [2] = 0x02;
                value[2] = (uint16)(liu_read(base_addr,
                                    addr_reg,
                                    data_read_reg,
                                    config_reg,
                                    addr[2]) | ((chmsk) & 0xff));
                addr [3] = 0x22;
                value[3] = (uint16)(liu_read(base_addr,
                                    addr_reg,
                                    data_read_reg,
                                    config_reg,
                                    addr[3]) | ((chmsk >> 8) & 0xff));
                }
            else
                {
                addr [2] = 0x02;
                value[2] = 0xff;
                addr [3] = 0x22;
                value[3] = 0xff;
                }

            /* Un-loop analog and digital */
		    addr [4] = 0x1;
			value[4] = 0x0;
			addr [5] = 0x21;
			value[5] = 0x0;

			addr [6] = 0xc;
			value[6] = 0x0;
			addr [7] = 0x2c;
			value[7] = 0x0;

            length = 8;
            break;
        case 3:
            addr [0] = 0x1f;
            value[0] = 0x00;
            addr [1] = 0x3f;
            value[1] = 0x00;
            if (chmsk)
                {
                addr [2] = 0x0c;
                value[2] = (uint16)(liu_read(base_addr,
                                    addr_reg,
                                    data_read_reg,
                                    config_reg,
                                    addr[2]) & ((~chmsk) & 0xff));
                addr [3] = 0x2c;
                value[3] = (uint16)(liu_read(base_addr,
                                    addr_reg,
                                    data_read_reg,
                                    config_reg,
                                    addr[3]) & ((~chmsk >> 8) & 0xff));
                }
            else
                {
                addr [2] = 0x0c;
                value[2] = 0x00;
                addr [3] = 0x2c;
                value[3] = 0x00;
                }
            addr [4] = 0x1f;
            value[4] = 0x00;
            addr [5] = 0x3f;
            value[5] = 0x00;
            if (chmsk)
                {
                addr [6] = 0x02;
                value[6] = (uint16)(liu_read(base_addr,
                                    addr_reg,
                                    data_read_reg,
                                    config_reg,
                                    addr[6]) & ((~chmsk) & 0xff));
                addr [7] = 0x22;
                value[7] = (uint16)(liu_read(base_addr,
                                    addr_reg,
                                    data_read_reg,
                                    config_reg,
                                    addr[7]) & ((~chmsk >> 8) & 0xff));
                }
            else
                {
                addr [6] = 0x02;
                value[6] = 0x00;
                addr [7] = 0x22;
                value[7] = 0x00;
                }
            addr [8] = 0x1f;
            value[8] = 0x00;
            addr [9] = 0x3f;
            value[9] = 0x00;
            if (chmsk)
                {
                addr [10] = 0x01;
                value[10] = (uint16)(liu_read(base_addr,
                                     addr_reg,
                                     data_read_reg,
                                     config_reg,
                                     addr[10]) & ((~chmsk) & 0xff));
                addr [11] = 0x21;
                value[11] = (uint16)(liu_read(base_addr,
                                     addr_reg,
                                     data_read_reg,
                                     config_reg,
                                     addr[11]) & ((~chmsk >> 8) & 0xff));
                }
            else
                {
                addr [10] = 0x01;
                value[10] = 0x00;
                addr [11] = 0x21;
                value[11] = 0x00;
                }
            length = 12;
            break;
        case 4:
            break;
        case 1:
            //Analog loopback
            addr [0] = 0x1f;
            value[0] = 0x00;
            addr [1] = 0x3f;
            value[1] = 0x00;
            if (chmsk)
                {
                addr [2] = 0x1;
                value[2] = (uint16)(liu_read(base_addr,
                                    addr_reg,
                                    data_read_reg,
                                    config_reg,
                                    addr[2]) | ((chmsk) & 0xff));
                addr [3] = 0x21;
                value[3] = (uint16)(liu_read(base_addr,
                                    addr_reg,
                                    data_read_reg,
                                    config_reg,
                                    addr[3]) | ((chmsk >> 8) & 0xff));
                }
            else
                {
                addr [2] = 0x01;
                value[2] = 0xff;
                addr [3] = 0x21;
                value[3] = 0xff;
                }

            /* Un-loop digital and remote */
			addr [4] = 0x2;
			value[4] = 0x0;
			addr [5] = 0x22;
			value[5] = 0x0;

			addr [6] = 0xc;
			value[6] = 0x0;
			addr [7] = 0x2c;
			value[7] = 0x0;

            length = 8;
            break;

        default:
            length = 0;
            break;
        }

    for (i = 0; i < length; i++)
        {
        liu_write(base_addr,
                  addr_reg,
                  data_write_reg,
                  config_reg,
                  addr[i],
                  value[i]);
        }

    return cAtOk;
    }

struct config_data {
    unsigned int addr;
    unsigned char data;
};

struct config_data pll_init0[] = {
//# # Initialize configuration
    {0x03DE ,0xF0},
    {0x03DF ,0x40},
    {0x03E0 ,0x3F},
    {0x03E1 ,0x8F},
    {0x03E2 ,0x80},
    {0x03E3 ,0x08},
    {0x03E4 ,0x98},
    {0x03E5 ,0x04},
    {0x03E5 ,0x00},
    {0x00C0 ,0x01},
    {0x00D3 ,0x03},
    {0x00D4 ,0x40},
    {0x00D6 ,0x3F},
    {0x00D7 ,0x0F},
    {0x00D8 ,0x87},
    {0x00D9 ,0x08},
    {0x00C0 ,0x02},
    {0x00D3 ,0x03},
    {0x00D4 ,0x40},
    {0x00D6 ,0x3F},
    {0x00D7 ,0x0F},
    {0x00D8 ,0x87},
    {0x00D9 ,0x08},
    {0x00C0 ,0x03},
    {0x00D3 ,0x03},
    {0x00D4 ,0x40},
    {0x00D6 ,0x3F},
    {0x00D7 ,0x0F},
    {0x00D8 ,0x87},
    {0x00D9 ,0x08},
//# Global configuration
    {0x07   ,0xA5},
    {0x108  ,0xFF},
//# IC1 configuration
    {0x60   ,0x01},
    {0x61   ,0x87},
    {0x62   ,0x43},
    {0x63   ,0x02},
    {0x64   ,0x0 },
    {0x65   ,0x0 },
    {0x66   ,0x01},
    {0x67   ,0x0 },
    {0x68   ,0x0 },
    {0x69   ,0x0 },
    {0x70   ,0x7F},
    {0x6E   ,0x7F},
    {0x6F   ,0x7F},
//# IC4 configuration
    {0x60   ,0x04},
    {0x61   ,0x81},
    {0x62   ,0x43},
    {0x63   ,0x02},
    {0x64   ,0x0 },
    {0x65   ,0x0 },
    {0x66   ,0x00},
    {0x67   ,0x0 },
    {0x68   ,0x0 },
    {0x69   ,0x0 },
    {0x70   ,0x7F},
    {0x6E   ,0x7F},
    {0x6F   ,0x7F},
//The # DPLL1 configuration
    {0x80   ,0x00},
    {0x81   ,0x01},
    {0x82   ,0x04},
    {0x83   ,0x6F},
    {0x84   ,0x6D},
    {0x85   ,0x5C},
    {0x8C   ,0x27},
    {0x8D   ,0x9F},
    {0x8E   ,0x9F},
    {0xA0   ,0x01},
    {0xA1   ,0x00},
    {0xA2   ,0x00},
    {0xA3   ,0x00},
};
struct config_data pll_init1[] = {
//# OC1 configuration mode
    {0xC0   ,0x01},
    {0xC1   ,0x10},
    {0xC2   ,0x10},
    {0xC3   ,0x80},
    {0xC4   ,0x00},
    {0xC8   ,0x00},
    {0xC9   ,0x00},
    {0xCA   ,0x00},
    {0xCB   ,0x00},
    {0xD1   ,0x1F},
    {0xD2   ,0x00},
    {0xD3   ,0x00},
    {0xD4   ,0x00},
    {0xD6   ,0x00},
    {0xD7   ,0x00},
    {0xD8   ,0x07},
    {0xD9   ,0x00},
    {0xE0   ,0x00},
    {0xE1   ,0x00},
    {0xE2   ,0x00},
    {0xE3   ,0x00},
    {0xE4   ,0x00},
    {0xE5   ,0x00},
    {0xE6   ,0x00},
    {0xE7   ,0x00},
    {0xE8   ,0x00},
    {0xE9   ,0x30},
    {0xEA   ,0x01},
    {0xEB   ,0x00},
    {0xEC   ,0x00},
    {0xED   ,0x00},
    {0xEE   ,0x00},
    {0xEF   ,0x00},
    {0xF0   ,0x00},
    {0xF1   ,0x00},
    {0xF2   ,0x2C},
    {0xF9   ,0x00},
    {0xD0   ,0x90},
//# OC2 configuration mode
    {0xC0   ,0x02},
    {0xC1   ,0x18},
    {0xC2   ,0x00},
    {0xC3   ,0x00},
    {0xC4   ,0x00},
    {0xC8   ,0x00},
    {0xC9   ,0x00},
    {0xCA   ,0x00},
    {0xCB   ,0x00},
    {0xD1   ,0x03},
    {0xD2   ,0x00},
    {0xD3   ,0x00},
    {0xD4   ,0x00},
    {0xD6   ,0x00},
    {0xD7   ,0x00},
    {0xD8   ,0x07},
    {0xD9   ,0x00},
    {0xE0   ,0x00},
    {0xE1   ,0x00},
    {0xE2   ,0x00},
    {0xE3   ,0x00},
    {0xE4   ,0x00},
    {0xE5   ,0x00},
    {0xE6   ,0x00},
    {0xE7   ,0x00},
    {0xE8   ,0x00},
    {0xE9   ,0x30},
    {0xEA   ,0x01},
    {0xEB   ,0x00},
    {0xEC   ,0x00},
    {0xED   ,0x00},
    {0xEE   ,0x00},
    {0xEF   ,0x00},
    {0xF0   ,0x00},
    {0xF1   ,0x00},
    {0xF2   ,0x00},
    {0xF9   ,0x00},
    {0xD0   ,0x90},
//# OC4 configuration mode
    {0xC0   ,0x04},
    {0xC1   ,0x00},
    {0xC2   ,0x20},
    {0xC3   ,0x00},
    {0xC4   ,0x80},
    {0xC8   ,0x00},
    {0xC9   ,0x00},
    {0xCA   ,0x00},
    {0xCB   ,0x00},
    {0xD1   ,0x00},
    {0xD2   ,0x00},
    {0xD3   ,0x00},
    {0xD4   ,0x00},
    {0xD6   ,0x00},
    {0xD7   ,0x00},
    {0xD8   ,0x00},
    {0xD9   ,0x00},
    {0xE0   ,0x00},
    {0xE1   ,0x00},
    {0xE2   ,0x00},
    {0xE3   ,0x00},
    {0xE4   ,0x00},
    {0xE5   ,0x00},
    {0xE6   ,0x00},
    {0xE7   ,0x00},
    {0xE8   ,0x00},
    {0xE9   ,0x00},
    {0xEA   ,0x00},
    {0xEB   ,0x00},
    {0xEC   ,0x00},
    {0xED   ,0x00},
    {0xEE   ,0x00},
    {0xEF   ,0x00},
    {0xF0   ,0x00},
    {0xF1   ,0x00},
    {0xF2   ,0x00},
    {0xF9   ,0x00},
    {0xD0   ,0x00},
//The # OC5 configuration mode
    {0xC0   ,0x05},
    {0xC1   ,0x00},
    {0xC2   ,0x20},
    {0xC3   ,0x00},
    {0xC4   ,0x80},
    {0xC8   ,0x00},
    {0xC9   ,0x00},
    {0xCA   ,0x00},
    {0xCB   ,0x00},
    {0xD1   ,0x00},
    {0xD2   ,0x00},
    {0xD3   ,0x00},
    {0xD4   ,0x00},
    {0xD6   ,0x00},
    {0xD7   ,0x00},
    {0xD8   ,0x00},
    {0xD9   ,0x00},
    {0xE0   ,0x00},
    {0xE1   ,0x00},
    {0xE2   ,0x00},
    {0xE3   ,0x00},
    {0xE4   ,0x00},
    {0xE5   ,0x00},
    {0xE6   ,0x00},
    {0xE7   ,0x00},
    {0xE8   ,0x00},
    {0xE9   ,0x00},
    {0xEA   ,0x00},
    {0xEB   ,0x00},
    {0xEC   ,0x00},
    {0xED   ,0x00},
    {0xEE   ,0x00},
    {0xEF   ,0x00},
    {0xF0   ,0x00},
    {0xF1   ,0x00},
    {0xF2   ,0x00},
    {0xF9   ,0x00},
    {0xD0   ,0x00},
//# OC6 configuration mode
    {0xC0   ,0x06},
    {0xC1   ,0x00},
    {0xC2   ,0x20},
    {0xC3   ,0x00},
    {0xC4   ,0x80},
    {0xC8   ,0x00},
    {0xC9   ,0x00},
    {0xCA   ,0x00},
    {0xCB   ,0x00},
    {0xD1   ,0x00},
    {0xD2   ,0x00},
    {0xD3   ,0x00},
    {0xD4   ,0x00},
    {0xD6   ,0x00},
    {0xD7   ,0x00},
    {0xD8   ,0x00},
    {0xD9   ,0x00},
    {0xE0   ,0x00},
    {0xE1   ,0x00},
    {0xE2   ,0x00},
    {0xE3   ,0x00},
    {0xE4   ,0x00},
    {0xE5   ,0x00},
    {0xE6   ,0x00},
    {0xE7   ,0x00},
    {0xE8   ,0x00},
    {0xE9   ,0x00},
    {0xEA   ,0x00},
    {0xEB   ,0x00},
    {0xEC   ,0x00},
    {0xED   ,0x00},
    {0xEE   ,0x00},
    {0xEF   ,0x00},
    {0xF0   ,0x00},
    {0xF1   ,0x00},
    {0xF2   ,0x00},
    {0xF9   ,0x00},
    {0xD0   ,0x00},
};

struct config_data pll_slave[] = {
    {0x80   ,0x01},
    {0x81   ,0x04},
    {0x82   ,0x04},
    {0x83   ,0x6F},
    {0x84   ,0x6D},
    {0x85   ,0x5C},
    {0x8C   ,0x27},
    {0x8D   ,0x9F},
    {0x8E   ,0x9F},
    {0xA0   ,0x00},
    {0xA1   ,0x10},
    {0xA2   ,0x00},
    {0xA3   ,0x00},
};

struct config_data pll_master[] = {
    {0x80   ,0x01},
    {0x81   ,0x01},
    {0x82   ,0x04},
    {0x83   ,0x6F},
    {0x84   ,0x6D},
    {0x85   ,0x5C},
    {0x8C   ,0x27},
    {0x8D   ,0x9F},
    {0x8E   ,0x9F},
    {0xA0   ,0x01},
    {0xA1   ,0x00},
    {0xA2   ,0x00},
    {0xA3   ,0x00},
};

/* base_addr : base address of card slot ( slot 1 or slot 2)
   addr_reg   : offset of addr register on main card( read write indirect)
   config_reg : offset of config register on main card
   addr      : offset of register on h3c card cpld
   data      : data to write
*/

static uint16 pll_write(uint32 base_addr,
                        uint32 addr_reg,
                        uint32 data_write_reg,
                        uint32 config_reg,
                        uint16 addr,
                        uint16 data)
    {
    sync_out_be16((uint16*) (base_addr + (addr_reg * 2)), addr);
    sync_out_be16((uint16*) (base_addr + (data_write_reg * 2)), (uint16)((data << 8) | 0xFF));
    sync_out_be16((uint16*) (base_addr + (config_reg * 2)), 0xFFFd);
    AtOsalUSleep(10000);

    return 0;
    }

static uint16 pll_read(uint32 base_addr,
                uint32 addr_reg,
                uint32 data_read_reg,
                uint32 config_reg,
                uint16 addr)
    {
    uint16 data;

    sync_out_be16((uint16*) (base_addr + (addr_reg * 2)), addr);
    sync_out_be16((uint16*) (base_addr + (config_reg * 2)), 0xFFF9);
    AtOsalUSleep(10000);
    data = sync_in_be16((uint16*) (base_addr + (data_read_reg * 2)));
    data = data & cBit7_0;

    return data;
    }

DEF_SDKCOMMAND(cliPllSet,
               "h3c pll",
               "Set pll mode\r\n",
               DEF_SDKPARAM("slot", "SLOT_ID", "slot id")
               DEF_SDKPARAM("card_type", "CARD_TYPE", "card type")
               DEF_SDKPARAM("pll_mode", "PLL_MODE", "loop mode"),
        "${slot} ${card_type} ${pll_mode}")
    {
    const char *_slot    = lub_argv__get_arg(argv, 0);
    const char *_card_type   = lub_argv__get_arg(argv, 1);
    const char *_pll_mode = lub_argv__get_arg(argv, 2);
    uint32 i,n, type;
    uint32 base_addr = 0, addr_reg = 0, data_write_reg = 0, config_reg = 0;
    uint32 slot = AtStrtol (_slot,NULL,10);
    uint32 pll_mode = AtStrtol (_pll_mode,NULL,10);

    AtUnused(context);

    if (slot == 1)
        base_addr = 0xd0200000;
    else if (slot == 2)
        base_addr = 0xd0400000;

    type = AtStrToDw(_card_type);

    if (type == cCardTypeE1)
    {
        config_reg = 0x15;
        addr_reg = 0x16;
        data_write_reg = 0x17;
    }
    else if (type == cCardTypeStm)
    {
        config_reg = 0x16;
        addr_reg = 0x7d;
        data_write_reg = 0x7b;
    }
    n = sizeof (pll_init0) / sizeof (*pll_init0);
    for (i = 0; i < n; i++)
    {
        pll_write (base_addr,addr_reg,data_write_reg,config_reg, (uint16)pll_init0[i].addr,pll_init0[i].data);
    }
#define MASTER_MODE 1
#define SLAVER_MODE 2
    if (pll_mode == MASTER_MODE)
    {
        n = sizeof (pll_master) / sizeof (*pll_master);
        for (i = 0; i < n; i++)
        {
            pll_write (base_addr, addr_reg, data_write_reg, config_reg, (uint16)pll_master[i].addr,pll_master[i].data);
        }
    }
    else if (pll_mode == SLAVER_MODE)
    {
        n = sizeof (pll_slave) / sizeof (*pll_slave);
        for (i = 0; i < n; i++)
        {
            pll_write (base_addr, addr_reg, data_write_reg, config_reg, (uint16)pll_slave[i].addr,pll_slave[i].data);
        }
    }
    n = sizeof (pll_init1) / sizeof (*pll_init1);
   for (i = 0; i < n; i++)
   {
       pll_write (base_addr,addr_reg,data_write_reg,config_reg, (uint16)pll_init1[i].addr,pll_init1[i].data);
   }
    return cAtOk;
    }

DEF_SDKCOMMAND(cliPllRegRead,
               "h3c pll rd",
               "Read pll register\r\n",
               DEF_SDKPARAM("slot", "SLOT_ID", "slot id")
               DEF_SDKPARAM("card_type", "CARD_TYPE", "card type")
               DEF_SDKPARAM("address", "RegisterAddress", "Register Address"),
               "${slot} ${card_type} ${address}")
    {
    const char *_slot        = lub_argv__get_arg(argv, 0);
    const char *_card_type   = lub_argv__get_arg(argv, 1);
    const char *_address      = lub_argv__get_arg(argv, 2);
    uint32 type;
    uint32 base_addr = 0, addr_reg = 0, data_write_reg = 0, config_reg = 0, address, regVal;
    uint32 slot = AtStrtol (_slot,NULL,10);

    AtUnused(context);

    if (slot == 1)
        base_addr = 0xd0200000;
    else if (slot == 2)
        base_addr = 0xd0400000;

    type = AtStrToDw(_card_type);

    if (type == cCardTypeE1)
    {
        config_reg = 0x15;
        addr_reg = 0x16;
        data_write_reg = 0x17;
    }
    else if (type == cCardTypeStm)
    {
        config_reg = 0x16;
        addr_reg = 0x7d;
        data_write_reg = 0x7b;
    }

    StrToDw(_address, 'h', &address);
    regVal = pll_read(base_addr,addr_reg, data_write_reg,config_reg,(uint16)address);
    AtPrintc(cSevNormal, "Read value: 0x%x\r\n", regVal);

    return cAtOk;
    }

DEF_SDKCOMMAND(cliPllRegWrite,
               "h3c pll wr",
               "Write pll register\r\n",
               DEF_SDKPARAM("slot", "SLOT_ID", "slot id")
               DEF_SDKPARAM("card_type", "CARD_TYPE", "card type")
               DEF_SDKPARAM("address", "RegisterAddress", "Register Address")
               DEF_SDKPARAM("value", "RegisterValue", "Register Value"),
               "${slot} ${card_type} ${address} ${value}")
    {
    const char *_slot        = lub_argv__get_arg(argv, 0);
    const char *_card_type   = lub_argv__get_arg(argv, 1);
    const char *_address     = lub_argv__get_arg(argv, 2);
    const char *_regVal      = lub_argv__get_arg(argv, 3);
    uint32 type;
    uint32 base_addr = 0, addr_reg = 0, data_write_reg = 0, config_reg = 0, address = 0, regVal = 0;
    uint32 slot = AtStrtol (_slot,NULL,10);

    AtUnused(context);

    if (slot == 1)
        base_addr = 0xd0200000;
    else if (slot == 2)
        base_addr = 0xd0400000;

    type = AtStrToDw(_card_type);

    if (type == cCardTypeE1)
    {
        config_reg = 0x15;
        addr_reg = 0x16;
        data_write_reg = 0x17;
    }
    else if (type == cCardTypeStm)
    {
        config_reg = 0x16;
        addr_reg = 0x7d;
        data_write_reg = 0x7b;
    }

    StrToDw(_address, 'h', &address);
    StrToDw(_regVal, 'h', &regVal);
    pll_write (base_addr,addr_reg,data_write_reg,config_reg,(uint16)address,(uint16)regVal);

    return cAtOk;
    }


static uint8 LiuRead(uint8 slotId, uint16 address)
    {
    uint32 addr_reg      = 0x19;
    uint32 data_read_reg = 0x1b;
    uint32 config_reg    = 0x18;
    return (uint8)liu_read(SlotBaseAddress(slotId), addr_reg, data_read_reg, config_reg, address);
    }

static void LiuWrite(uint8 slotId, uint16 address, uint16 data)
    {
    uint32 addr_reg       = 0x19;
    uint32 data_write_reg = 0x1a;
    uint32 config_reg     = 0x18;
    liu_write(SlotBaseAddress(slotId), addr_reg, data_write_reg, config_reg, address, data);
    }

DEF_SDKCOMMAND(cliLiuRead,
               "h3c liu rd",
               "Read LIU registers\r\n",
               DEF_SDKPARAM("slot", "SLOT_ID", "slot id")
               DEF_SDKPARAM("address", "RegisterAddress", "Register Address"),
               "${slot} ${address}")
    {
    uint32 address;
    uint8 slotId = (uint8)AtStrtol(lub_argv__get_arg(argv, 0), NULL, 10);

    AtUnused(context);

    StrToDw(lub_argv__get_arg(argv, 1), 'h', &address);
    AtPrintc(cSevNormal, "Read value: 0x%x\r\n", LiuRead(slotId, (uint16)address));

    return 0;
    }

DEF_SDKCOMMAND(cliLiuWrite,
               "h3c liu wr",
               "Write LIU registers\r\n",
               DEF_SDKPARAM("slot", "SLOT_ID", "slot id")
               DEF_SDKPARAM("address", "RegisterAddress", "Register Address")
               DEF_SDKPARAM("value", "RegisterValue", "Register Value"),
               "${slot} ${address} ${value}")
    {
    uint32 address, value;
    uint8 slotId = (uint8)AtStrtol (lub_argv__get_arg(argv, 0), NULL, 10);

    AtUnused(context);
    StrToDw(lub_argv__get_arg(argv, 1), 'h', &address);
    StrToDw(lub_argv__get_arg(argv, 2), 'h', &value);
    LiuWrite(slotId, (uint16)address, (uint16)value);

    return 0;
    }

#endif /* __ARCH_POWERPC__ */

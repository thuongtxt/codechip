/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Customer
 *
 * File        : ClishAtCienaDeviceMro.c
 *
 * Created Date: Feb 27, 2017
 *
 * Description : MRO specific CLISH CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("ciena serdes", "Ciena specific CLIs to control SERDES")
TOP_SDKCOMMAND("ciena serdes group", "Ciena specific CLIs to control SERDES group")

DEF_SDKCOMMAND(cliAtCienaMroShow,
               "show ciena device mro",
               "Show MRO device information",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCienaFaceplateSerdesGroupSet,
               "ciena serdes group faceplate",
               "Select faceplate group",
               DEF_SDKPARAM("groupId", "STRING", "[1..2] Faceplate group ID"),
               "${groupId}")
    {
    mAtCliCall();
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Customer
 *
 * File        : ClishAtCienaEthPort.c
 *
 * Created Date: Feb 27, 2017
 *
 * Description : ETH Port specific CLISH CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../AtClish.h"

/*--------------------------- Define -----------------------------------------*/
/* FOR ETH port Defect */
#define SgmiiDefectType "(da-mismatch|cvlan-mismatch|length-oversize|fcs-error|ethtype-mismatch)"
#define SgmiiDefectMask "((("SgmiiDefectType"[\\|])+"SgmiiDefectType")|"SgmiiDefectType")"


DEF_PTYPE("SgmiiDefectTypeMask", "regexp", SgmiiDefectMask,
          "\nKbyte APS Alarm masks:\n"
          "- da-mismatch      : DA/SA mismatch \n"
          "- cvlan-mismatch   : C-VLAN mismatch \n"
          "- length-oversize  : Length-Over-size \n"
          "- fcs-error        : FCS Error \n"
          "- ethtype-mismatch : ETHTYPE mismatch \n")

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("ciena eth", "Ciena ethernet module commands")
TOP_SDKCOMMAND("ciena eth port", "Ciena ethernet port commands")
TOP_SDKCOMMAND("ciena eth port flowcontrol", "Ciena ethernet port flow control commands")
TOP_SDKCOMMAND("ciena eth port flowcontrol threshold", "Ciena ethernet port flow control threshold commands")
TOP_SDKCOMMAND("ciena eth port flowcontrol watermark", "Ciena ethernet port flow control watermark commands")
TOP_SDKCOMMAND("ciena eth port bypass", "Ciena ethernet port bypass commands")
TOP_SDKCOMMAND("ciena eth port subport", "Ciena ethernet port subport commands")
TOP_SDKCOMMAND("ciena eth port subport vlan", "Ciena ethernet subport vlan encapsulation commands")
TOP_SDKCOMMAND("ciena eth subport", "Ciena ethernet subport VLAN TPID commands")
TOP_SDKCOMMAND("ciena eth subport vlan", "Ciena ethernet subport VLAN TPID commands")
TOP_SDKCOMMAND("ciena eth subport vlan tpid", "Ciena ethernet subport VLAN TPID commands")

TOP_SDKCOMMAND("show ciena eth", "Show Ciena ethernet module information commands")
TOP_SDKCOMMAND("show ciena eth subport", "Ciena specific CLIs to show subport VLAN TPID")
TOP_SDKCOMMAND("show ciena eth subport vlan", "Ciena specific CLIs to show subport VLAN TPID")
TOP_SDKCOMMAND("ciena eth port cvlan", "Ciena ethernet port commands")
TOP_SDKCOMMAND("ciena eth port dcc_kbyte", "Ciena ethernet DCC/K-Byte port commands")
TOP_SDKCOMMAND("ciena eth port dcc_kbyte alarm", "Ciena ethernet DCC/K-Byte port alarm commands")

DEF_SDKCOMMAND(CliAtCienaEthPortBypassEnable,
               "ciena eth port bypass enable",
               "Enable ethernet port bypass",
               DEF_SDKPARAM("portIdList", "STRING", "ETH Port ID"),
               "${portIdList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaEthPortBypassDisable,
               "ciena eth port bypass disable",
               "Disable ethernet port bypass",
               DEF_SDKPARAM("portIdList", "STRING", "ETH Port ID"),
               "${portIdList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaEthPortSubportVlanTransmitSet,
               "ciena eth port subport vlan transmit",
               "Configure ethernet port sub-port transmit VLAN",
               DEF_SDKPARAM("portIdList", "STRING", "ETH Port ID")
               DEF_SDKPARAM("TPID.PRI.CFI.VID", "STRING", "Vlan tag: TPID.PRI.CFI.VLAN_ID. Example: 0x8100.1.0.10"),
               "${portIdList} {TPID.PRI.CFI.VID}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaEthPortSubportVlanExpectedSet,
               "ciena eth port subport vlan expect",
               "Configure ethernet port sub-port expected VLAN",
               DEF_SDKPARAM("portIdList", "STRING", "ETH Port ID")
               DEF_SDKPARAM("TPID.PRI.CFI.VID", "STRING", "Vlan tag: TPID.PRI.CFI.VLAN_ID. Example: 0x8100.1.0.10"),
               "${portIdList} {TPID.PRI.CFI.VID}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaEthPortBypassShow,
               "show ciena eth port",
               "Show ethernet port informations. WaterMarkMax/Min is in blocks of 1kbyte unit.",
               DEF_SDKPARAM("portIdList", "STRING", "ETH Port ID"),
               "${portIdList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCienaEthFlowControlHighThresholdSet,
               "ciena eth port flowcontrol threshold high",
               "Set high threshold of ETH Flow Control",
               DEF_SDKPARAM("portIdList", "STRING", "ETH Port ID")
               DEF_SDKPARAM("value", "UINT", "Value of threshold 0->255"),
               "${portIdList} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCienaEthFlowControlLowThresholdSet,
               "ciena eth port flowcontrol threshold low",
               "Set low threshold of ETH Flow Control",
               DEF_SDKPARAM("portIdList", "STRING", "ETH Port ID")
               DEF_SDKPARAM("value", "UINT", "Value of threshold 0->255"),
               "${portIdList} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCienaEthFlowControlWaterMarkReset,
               "ciena eth port flowcontrol watermark reset",
               "reset OOB TX fifo watermark of ETH Flow Control",
               DEF_SDKPARAM("portIdList", "STRING", "ETH Port ID"),
               "${portIdList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCienaModuleEthSubPortVlanTransmitTpidSet,
               "ciena eth subport vlan tpid transmit",
               "Set global transmit VLAN TPID",
               DEF_SDKPARAM("tpidIndex", "UINT", "TPID index: 1,2")
               DEF_SDKPARAM("tpidValue", "STRING", "TPID Value: 0x0->0xFFFF"),
               "${tpidIndex} ${tpidValue}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCienaModuleEthSubPortVlanExpectedTpidSet,
               "ciena eth subport vlan tpid expect",
               "Set global expected VLAN TPID",
               DEF_SDKPARAM("tpidIndex", "UINT", "TPID index: 1,2")
               DEF_SDKPARAM("tpidValue", "STRING", "TPID Value:  0x0->0xFFFF"),
               "${tpidIndex} ${tpidValue}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCienaModuleEthSubPortVlanTpidsShow,
               "show ciena eth subport vlan tpid",
               "Show global VLAN TPID",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaEthPortExpectedCVlanSet,
               "ciena eth port cvlan expect",
               "Set SVlan for Eth Port apply on SGMII-DCC",
               DEF_SDKPARAM("portIdList", "STRING", "List of Ethernet port IDs: 2-17")
               DEF_SDKPARAM("vlanId", "NUM_HEX", "VLAN Id"),
               "${portIdList}  ${vlanId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaEthPortSgmiiCountersShow,
               "show ciena eth port dcc_kbyte counters",
               "Get counters of DCC/KByte port",
               DEF_SDKPARAM("portIdList", "STRING", "DCC/Kbyte ETH Port ID")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${portIdList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaEthPortIntrMsk,
               "ciena eth port dcc_kbyte interruptmask",
               "Set interrupt masks\n",
               DEF_SDKPARAM("portIdList", "STRING", "DCC/Kbyte ETH Port ID")
               DEF_SDKPARAM("mask", "SgmiiDefectTypeMask", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${portIdList} ${mask} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCienaSgmiiEthPortShow,
               "show ciena eth port dcc_kbyte",
               "Show port information\n",
               DEF_SDKPARAM("portIdList", "STRING", "DCC/Kbyte ETH Port ID"),
               "${portIdList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaEthPortInterruptsShow,
               "show ciena eth port dcc_kbyte interrupt",
               "Show ETH interrupts Sticky",
               DEF_SDKPARAM("portIdList", "STRING", "DCC/Kbyte ETH Port ID")
               DEF_SDKPARAM("counterReadMode", "HistoryReadingMode", "read only/read to clear mode")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${portIdList} ${counterReadMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCienaSgmiiEthPortDefectShow,
               "show ciena eth port dcc_kbyte alarm",
               "Show current alarm",
               DEF_SDKPARAM("portIdList", "STRING", "DCC/Kbyte ETH Port ID"),
               "${portIdList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCienaSgmiiEthPortAlarmCapture,
               "ciena eth port dcc_kbyte alarm capture",
               "Show current alarm",
               DEF_SDKPARAM("portIdList", "STRING", "DCC/Kbyte ETH Port ID")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${portIdList} ${enable}")
    {
    mAtCliCall();
    }

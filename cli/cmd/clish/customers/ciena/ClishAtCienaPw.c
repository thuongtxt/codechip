/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Customer
 *
 * File        : ClishAtCienaPw.c
 *
 * Created Date: Feb 27, 2017
 *
 * Description : PW specific CLISH CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("ciena module pw", "Ciena PW Module commands")
TOP_SDKCOMMAND("ciena module pw subport", "To control subport")
TOP_SDKCOMMAND("ciena module pw subport vlan", "Ciena Module PW subport VLAN commands")
TOP_SDKCOMMAND("ciena module pw payloadsizebyte", "Ciena Module PW payloadsize in byte mode commands")
TOP_SDKCOMMAND("ciena pw", "Ciena PW commands")
TOP_SDKCOMMAND("ciena pw active", "Ciena PW active commands")
TOP_SDKCOMMAND("ciena pw subport", "Ciena PW Subport VLAN commands")
TOP_SDKCOMMAND("ciena pw subport vlan", "Ciena PW Subport VLAN commands")

TOP_SDKCOMMAND("show ciena module pw", "Show Ciena pw module information commands")
TOP_SDKCOMMAND("show ciena module pw subport", "Show Ciena pw module information commands")

DEF_SDKCOMMAND(CliAtCienaPwGet,
               "show ciena pw",
               "Show application specific information",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliCienaPwActiveForce,
               "ciena pw active force",
               "Force TX actice",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliCienaPwActiveUnforce,
               "ciena pw active unforce",
               "Un-force TX actice",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaPwModuleTxSubPortVlanSet,
               "ciena module pw subport vlan transmit",
               "Set Tx subport Vlan",
               DEF_SDKPARAM("vlanIndex", "UINT", "VLAN index: 1,2")
               DEF_SDKPARAM("vlanTag", "STRING", "VLAN Tag: <tpid>.<priority>.<cfi>.<vlanId>. Example: 0x88a8.3.0.105"),
               "${vlanIndex} ${vlanTag}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaPwModuleExpectedSubPortVlanSet,
               "ciena module pw subport vlan expect",
               "Set Tx subport Vlan",
               DEF_SDKPARAM("vlanIndex", "UINT", "VLAN index: 1,2")
               DEF_SDKPARAM("vlanTag", "STRING", "VLAN Tag: <tpid>.<priority>.<cfi>.<vlanId>. Example: 0x88a8.3.0.105"),
               "${vlanIndex} ${vlanTag}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaModulePwSubportVlanGet,
               "show ciena module pw subport vlan",
               "Show subport Vlan information",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaPwTxSubportVlan,
               "ciena pw subport vlan transmit",
               "Set TX subport Vlan",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("vlanIndex", "UINT", "VLAN index: 1,2"),
               "${pwList} ${vlanIndex}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliCienaPwCounterGet,
               "show ciena pw counters",
               "Get Pseudowire counters",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${pwList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaPwModulePayloadSizeInByteEnable,
               "ciena module pw payloadsizebyte enable",
               "Enable Cesop payloadsize in byte. Payloadsize is in byte unit.",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaPwModulePayloadSizeInByteDisable,
               "ciena module pw payloadsizebyte disable",
               "Disable Cesop payloadsize in byte. Payloadsize is in frame unit.",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaPwModulePayloadSizeInByteGet,
               "show ciena module pw payloadsizebyte",
               "Show cesop payloadsize mode in byte or in frame uinit. Enable: Cesop payloadsize in byte. Disable: Cesop Payloadsize is in frame unit.",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

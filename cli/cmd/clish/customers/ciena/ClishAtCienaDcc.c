/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Customer
 *
 * File        : ClishAtCienaDcc.c
 *
 * Created Date: Feb 27, 2017
 *
 * Description : CLISH DCC CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../AtClish.h"

/*--------------------------- Define -----------------------------------------*/
/* FOR Kbyte APS Defect */
#define DccPwDefectType "(rx-pkt-discard)"
#define DccPwDefectMask "((("DccPwDefectType"[\\|])+"DccPwDefectType")|"DccPwDefectType")"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
DEF_PTYPE("DccPwIds", "regexp", ".*", "DCC PW Ids with format: <lines>.<section|line>. For example: 1-2.line, or 1-2.section")

DEF_PTYPE("DccPwAlarmTypeMask", "regexp", DccPwDefectMask,
          "\n DCCPw Alarm masks:\n"
          "- rx-pkt-discard            : Rx-Pkt-Discard \n")

TOP_SDKCOMMAND("ciena pw dcc", "Ciena PW DCC commands")
TOP_SDKCOMMAND("ciena pw dcc mac", "Ciena PW DCC MAC commands")
TOP_SDKCOMMAND("ciena pw dcc mac check", "Ciena PW DCC MAC checking commands")
TOP_SDKCOMMAND("ciena pw dcc vlan", "Ciena PW DCC VLAN commands")
TOP_SDKCOMMAND("ciena pw dcc vlan check", "Ciena PW DCC VLAN check commands")
TOP_SDKCOMMAND("ciena pw hdlc", "Ciena PW HDLC commands")
TOP_SDKCOMMAND("ciena pw dcc vlan expect", "Ciena PW Subport VLAN commands")
TOP_SDKCOMMAND("ciena pw dcc alarm", "Ciena PW Alarm commands")

DEF_SDKCOMMAND(cliCienaPwDccEthType,
               "ciena pw dcc ethtype",
               "Set ETH type field value to insert on the HDLC PW Packets",
               DEF_SDKPARAM("pw", "DccPwIds", "")
               DEF_SDKPARAM("ethtype", "STRING", "Value of Eth Type such as 0x8100"),
               "${pw} ${ethtype}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliCienaPwDccType,
               "ciena pw dcc type",
               "Set type field value to insert on the HDLC PW Packets",
               DEF_SDKPARAM("pw", "DccPwIds", "")
               DEF_SDKPARAM("type", "STRING", "Value of Eth Type such as 0x01"),
               "${pw} ${type}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliCienaPwDccVersion,
               "ciena pw dcc version",
               "Set version field value to insert on the HDLC PW Packets",
               DEF_SDKPARAM("pw", "DccPwIds", "")
               DEF_SDKPARAM("version", "STRING", "Value of version such as 0x01"),
               "${pw} ${version}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliCienaPwDccShow,
               "show ciena pw dcc",
               "Show PW DCC configuration",
               DEF_NULLPARAM,"")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliCienaPwHdlcShow,
               "show ciena pw hdlc",
               "Show PW HDLC configuration",
               DEF_SDKPARAM("pw", "DccPwIds", ""),
               "${pw}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliCienaPwDccSourceMacSet,
               "ciena pw dcc sourcemac",
               "Set sourcemac for the PW DCC",
               DEF_SDKPARAM("pw", "DccPwIds", "")
               DEF_SDKPARAM("mac", "MacAddr", "SMAC Address"),
               "${pw} ${mac}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliCienaPwDccEthHeaderSet,
               "ciena pw dcc ethheader",
               "Set Ethernet Header Information",
               DEF_SDKPARAM("pw", "DccPwIds", "")
               DEF_SDKPARAM("mac", "MacAddr", "DMAC Address")
               DEF_SDKPARAM("cVlan", "VlanDesc", "")
               DEF_SDKPARAM("sVlan", "VlanDesc", ""),
               "${pw} ${mac} ${cVlan} ${sVlan}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaPwDccCountersShow,
               "show ciena pw dcc counters",
               "Show pw DCC counters",
               DEF_SDKPARAM("pwList", "STRING", " Format Id: lines.<section|line>. Example: 1.section")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${pwList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaDccPwDestMacCheckEnable,
               "ciena pw dcc mac check enable",
               "Enable MAC checking",
               DEF_SDKPARAM("pwList", "STRING", " Format Id: lines.<section|line>. Example: 1.section"),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaDccPwDestMacCheckDisable,
               "ciena pw dcc mac check disable",
               "Disable MAC checking",
               DEF_SDKPARAM("pwList", "STRING", " Format Id: lines.<section|line>. Example: 1.section"),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaDccExpectGlobalMacSet,
               "ciena pw dcc mac global",
               "Set Global Mac to clasify Rx-DCC Packet",
               DEF_SDKPARAM("portIdList", "STRING", "List of Ethernet port IDs: 2-17 for 16 faceplate Macs")
               DEF_SDKPARAM("mac", "MacAddr", "DMAC Address"),
               "${portIdList} ${mac}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaDccPwCVLanCheckEnable,
               "ciena pw dcc vlan check enable",
               "Enable C-Vlan checking",
               DEF_SDKPARAM("pwList", "STRING", " Format Id: lines.<section|line>. Example: 1.section"),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaDccPwCVLanCheckDisable,
               "ciena pw dcc vlan check disable",
               "Disable C-Vlan checking",
               DEF_SDKPARAM("pwList", "STRING", " Format Id: lines.<section|line>. Example: 1.section"),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaDccPwExpectedLabelSet,
               "ciena pw dcc expectedlabel",
               "Set expected Label for DCC PW",
               DEF_SDKPARAM("pwList", "STRING", " Format Id: lines.<section|line>. Example: 1.section")
               DEF_SDKPARAM("label", "STRING", "Label value can be 0 to 31 or 0 to 64"),
               "${pwList} ${label}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaPwDccInterruptsShow,
               "show ciena pw dcc interrupt",
               "Show pw interrupts",
               DEF_SDKPARAM("pwList", "STRING", " Format Id: lines.<section|line>. Example: 1.section")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", "Read to clear without display table"),
               "${pwList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaPwDccAlarmShow,
               "show ciena pw dcc alarm",
               "Show pw alarm",
               DEF_SDKPARAM("pwList", "STRING", " Format Id: lines.<section|line>. Example: 1.section"),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaDccPwIntrMsk,
               "ciena pw dcc interruptmask",
               "Set interrupt masks\n",
               DEF_SDKPARAM("pwList", "STRING", " Format Id: lines.<section|line>. Example: 1.section")
               DEF_SDKPARAM("mask", "DccPwAlarmTypeMask", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${pwList} ${mask} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaDccPwAlarmCapture,
               "ciena pw dcc alarm capture",
               "Enable/disable alarm capturing\n",
               DEF_SDKPARAM("pwList", "STRING", " Format Id: lines.<section|line>. Example: 1.section")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${pwList} ${enable}")
    {
    mAtCliCall();
    }


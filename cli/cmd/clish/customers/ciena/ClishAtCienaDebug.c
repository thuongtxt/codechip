/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Customer
 *
 * File        : ClishAtCienaDebug.c
 *
 * Created Date: Oct 14, 2016
 *
 * Description : Debug CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../AtClish.h"

/*--------------------------- Define -----------------------------------------*/
#define regEthLength "0..65535"  /* value 0-65535 */

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
DEF_PTYPE("EthLength", "integer", regEthLength, "Eth Length Value")
DEF_PTYPE("LiuIdList", "regexp", ".*", "Liu IdList:\n"
                                       "+ DE3/EC1: 24\n"
                                       "+ Ds1/E1: 84")

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
DEF_PTYPE("eTha6029LineSide", "select", "faceplate(1), mate(2)", "Line side")
DEF_PTYPE("eTha60290021XcHideMode", "select", "none(1), direct(2), mate(3), faceplate(4)",
          "XC hiding scheme\n"
          "+ none: Do not hide anything, this must be default mode.\n"
          "+ direct: Hide XC but the connection between MATE/Faceplate and Terminated lines are 1:1\n"
          "+ mate: Hide XC at MATE, all faceplate ports are looped\n"
          "+ faceplate: Hide XC at Faceplate, all MATE ports are looped")
DEF_PTYPE("eTha602900xxEthPortCounterTickMode", "select", "auto(1), manual(2)",
          "Counters tick mode:\n"
          "+ auto: hardware counters logic work automatically base\n"
          "        on system clock or input 1s tick\n"
          "+ manual: tick manualy when counters are accessed")

TOP_SDKCOMMAND("debug ciena", "Ciena Debug specific commands")
TOP_SDKCOMMAND("debug ciena mro", "To handle debug on MRO")
TOP_SDKCOMMAND("debug ciena mro txshapper", "To handle debug on MRO TX RTP timestamp shapper")
TOP_SDKCOMMAND("debug ciena mro poh", "To handle POH debug on MRO")
TOP_SDKCOMMAND("debug ciena mro xc", "XC debug CLIs")
TOP_SDKCOMMAND("debug ciena pdh", "Ciena debug pdh specific commands")
TOP_SDKCOMMAND("debug ciena pdh interface", "Ciena debug pdh interface specific commands")
TOP_SDKCOMMAND("debug ciena pdh interface axi4", "Ciena specific debug pdh interface axi4 commands")
TOP_SDKCOMMAND("debug ciena pdh dim", "Ciena debug pdh dim specific commands")
TOP_SDKCOMMAND("debug ciena pdh dim liu", "Ciena specific debug pdh dim liu commands")
TOP_SDKCOMMAND("debug ciena pdh dim axi4", "Ciena specific debug pdh dim axi4 commands")
TOP_SDKCOMMAND("debug ciena cls", "Ciena debug cls specific commands")
TOP_SDKCOMMAND("debug ciena cls axi4", "Ciena debug cls axi4 specific commands")
TOP_SDKCOMMAND("debug ciena cls axi4 sequence", "Ciena debug cls axi4 sequence specific commands")
TOP_SDKCOMMAND("debug ciena cls axi4 subtypemask", "Ciena debug cls axi4 subtypemask specific commands")
TOP_SDKCOMMAND("debug ciena eth", "Ciena debug ETH specific commands")
TOP_SDKCOMMAND("debug ciena eth subport", "ETH Subport debug ETH specific commands")
TOP_SDKCOMMAND("debug ciena eth", "CLI to debug ETH module")
TOP_SDKCOMMAND("debug ciena eth port", "CLI to debug ETH port")
TOP_SDKCOMMAND("debug ciena eth port counters", "CLI to debug ETH port counters")
TOP_SDKCOMMAND("show ciena mro", "CLIs to show MRO debug information")
TOP_SDKCOMMAND("show ciena mro module", "CLIs to show MRO module debug information")
TOP_SDKCOMMAND("debug ciena mro faceplate", "CLIs to debug faceplate side")
TOP_SDKCOMMAND("debug ciena mro faceplate mac", "CLIs to debug faceplate MAC")
TOP_SDKCOMMAND("debug ciena mro backplane", "CLIs to debug backplane side")
TOP_SDKCOMMAND("debug ciena mro backplane mac", "CLIs to debug backplane MAC")
TOP_SDKCOMMAND("debug ciena pw", "CLIs to show PW debug information")
TOP_SDKCOMMAND("ciena eth dcc_max_len", "CLIs for ETH DCC max length debug information")
TOP_SDKCOMMAND("show ciena eth", "CLIs for ETH DCC max length debug information")
TOP_SDKCOMMAND("debug ciena eth counters", "CLI to debug ETH counters")

DEF_SDKCOMMAND(cliTha60290021ModuleOcnPohProcessorSideSet,
               "debug ciena mro poh side",
               "Configure ethernet port sub-port transmit VLAN",
               DEF_SDKPARAM("side", "eTha6029LineSide", "Line side for POH processor to work on"),
               "${side}")
        {
        mAtCliCall();
        }

DEF_SDKCOMMAND(cliTha60290021ModuleXcHideModeSet,
               "debug ciena mro xc hide",
               "Configure XC hiding mode",
               DEF_SDKPARAM("hideMode", "eTha60290021XcHideMode", ""),
               "${hideMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaPdhInterfaceAxi4LoopbackSetDebug,
               "debug ciena pdh interface axi4 loopback",
               "Set loopback mode for AXI4",
               DEF_SDKPARAM("loopMode", "eAtLoopbackMode", ""),
               "${loopMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaPdhInterfaceAxi4EthTypeSetDebug,
               "debug ciena pdh interface axi4 ethtype",
               "Set Type for AXI4 Port",
               DEF_SDKPARAM("type", "STRING", "Eth Type"),
               "${type}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCienaEthPortSrcMac,
               "debug ciena cls axi4 srcmac",
               "Set CLS Source Mac for AXI4 Port",
               DEF_SDKPARAM("mac", "MacAddr", ""),
               "${mac}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCienaEthPortDestMac,
               "debug ciena cls axi4 destmac",
               "Set CLS Dest Mac for AXI4 Port",
               DEF_SDKPARAM("mac", "MacAddr", ""),
               "${mac}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCienaEthPortSequenceCheckEnable,
               "debug ciena cls axi4 sequence check",
               "Enable/Disable CLS sequence check\n",
               DEF_SDKPARAM("enable", "eBool", ""),
               "${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCienaEthPortLengthCheckEnable,
               "debug ciena cls axi4 length check",
               "Enable/Disable CLS length check\n",
               DEF_SDKPARAM("enable", "eBool", ""),
               "${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCienaEthPortEthTypeCheckEnable,
               "debug ciena cls axi4 ethtype check",
               "Enable/Disable CLS eth type check\n",
               DEF_SDKPARAM("enable", "eBool", ""),
               "${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCienaEthPortSourceMacCheckEnable,
               "debug ciena cls axi4 srcmac check",
               "Enable/Disable CLS source MAC check\n",
               DEF_SDKPARAM("enable", "eBool", ""),
               "${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCienaEthPortDestMacCheckEnable,
               "debug ciena cls axi4 destmac check",
               "Enable/Disable CLS dest MAC check\n",
               DEF_SDKPARAM("enable", "eBool", ""),
               "${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCienaEthPortSubtypeMaskCheckSet,
               "debug ciena cls axi4 subtypemask check",
               "Set CLS subtypemask check\n",
               DEF_SDKPARAM("mask", "STRING", "8bits Mask: 0x0..0xFF"),
               "${mask}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCienaEthPortSubTypeSet,
               "debug ciena cls axi4 subtype",
               "Set CLS subtype\n",
               DEF_SDKPARAM("mask", "STRING", "8bits SubType: 0x0..0xFF"),
               "${mask}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCienaEthPortLengthSet,
               "debug ciena cls axi4 length",
               "Set CLS length\n",
               DEF_SDKPARAM("mask", "STRING", "Length"),
               "${mask}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCienaEthPortEthTypeSet,
               "debug ciena cls axi4 ethtype",
               "Set CLS ethtype\n",
               DEF_SDKPARAM("type", "STRING", "16bits Eth Type"),
               "${type}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaPdhDimAxi4LoopbackSetDebug,
               "debug ciena pdh dim axi4 loopback",
               "Set loopback mode for AXI4",
               DEF_SDKPARAM("loopMode", "eAtLoopbackMode", ""),
               "${loopMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaPdhDimLoopbackSetDebug,
               "debug ciena pdh dim liu loopback",
               "Set loopback mode for LIU at Arrive PDH DIM testing platform",
               DEF_SDKPARAM("idList", "LiuIdList", "")
               DEF_SDKPARAM("loopMode", "eAtLoopbackMode", ""),
               "${idList} ${loopMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaPdhDimAxi4EthTypeSetDebug,
               "debug ciena pdh dim axi4 ethtype",
               "Set Type for AXI4 Port",
               DEF_SDKPARAM("type", "STRING", "Eth Type"),
               "${type}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaSubportVlanEnableDebug,
               "debug ciena eth subport vlan",
               "Enable Subport Vlan",
               DEF_SDKPARAM("enable", "eBool", ""),
               "${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliTha6029002EthPortCounterTickModeSet,
               "debug ciena eth port counters tick",
               "Specify tick mode for eth port counters. Only MRO backplane ports "
               "have this configuration. Note, the `debug eth port <ports>` will "
               "display this information.",
               DEF_SDKPARAM("ports", "PortIdList", "")
               DEF_SDKPARAM("mode", "eTha602900xxEthPortCounterTickMode", ""),
               "${ports} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliTha60290021ModuleXcShow,
               "show ciena mro module xc",
               "Show XC information",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliTha60290022ModuleEthFaceplateMacLoopbackSet,
               "debug ciena mro faceplate mac loopback",
               "Loopback faceplate MAC",
               DEF_SDKPARAM("loopbackMode", "eAtLoopbackMode", ""),
               "${loopbackMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliTha60290022ModuleEthBackplaneMacLoopbackSet,
               "debug ciena mro backplane mac loopback",
               "Loopback backplane MAC",
               DEF_SDKPARAM("loopbackMode", "eAtLoopbackMode", ""),
               "${loopbackMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliTha6029PwKbytePwTxEthTypeSet,
               "ciena pw kbyte ethtype transmit",
               "Set TX ETH type field",
               DEF_SDKPARAM("ethtype", "STRING", "Value of Eth Type such as 0x8100"),
               "${ethtype}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliTha6029PwKbytePwExpectedEthTypeSet,
               "ciena pw kbyte ethtype expect",
               "Set Expected ETH type field",
               DEF_SDKPARAM("ethtype", "STRING", "Value of Eth Type such as 0x8100"),
               "${ethtype}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliTha6029PwKbyteTxTypeSet,
               "ciena pw kbyte type transmit",
               "Set TX Type field",
               DEF_SDKPARAM("type", "STRING", "Value of Eth Type such as 0x54"),
               "${type}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliTha6029PwKbyteExpectedTypeSet,
               "ciena pw kbyte type expect",
               "Set Expected Type field",
               DEF_SDKPARAM("type", "STRING", "Value of Eth Type such as 0x54"),
               "${type}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliTha6029PwKbyteTxVersionSet,
               "ciena pw kbyte version transmit",
               "Set TX Version field value",
               DEF_SDKPARAM("type", "STRING", "Value of Eth Type such as 0x01"),
               "${type}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliTha6029PwKbyteExpectedVersionSet,
               "ciena pw kbyte version expect",
               "Set Expected Version field value",
               DEF_SDKPARAM("type", "STRING", "Value of Eth Type such as 0x01"),
               "${type}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaDebugMroTxShapperEnable,
               "debug ciena mro txshapper enable",
               "Set Enable TX shapper before device init is started",
               DEF_NULLPARAM,
                            "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaDebugMroTxShapperDisable,
               "debug ciena mro txshapper disable",
               "Set Disable TX shapper before device init is started",
               DEF_NULLPARAM,
                          "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliThaModuleEthSgmiiEthDccOcn2EthMaxLenSet,
               "ciena eth dcc_max_len tx",
               "Set DCC ocn2eth max packet length",
               DEF_SDKPARAM("max", "UINT", "Value of max length such 1518"),
               "${max}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliThaModuleEthSgmiiEthDccEth2OcnMaxLenSet,
               "ciena eth dcc_max_len rx",
               "Set DCC eth2ocn max packet length",
               DEF_SDKPARAM("max", "UINT", "Value of max length such 1518"),
               "${max}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliThaModuleEthSgmiiEthDccEthMaxLenShow,
               "show ciena eth dcc_max_len",
               "Show DCC eth max packet length",
               DEF_NULLPARAM,
                          "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliThaModuleEthCountersTickModeSet,
               "debug ciena eth counters tick",
               "Set ETH counters tick mode",
               DEF_SDKPARAM("mode", "eTha602900xxEthPortCounterTickMode", ""),
               "${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliThaModuleEthCountersTickLatchAndClear,
               "debug ciena eth counters latch",
               "Latch and clear all Ethernet port counters",
               DEF_NULLPARAM,
                          "")
    {
    mAtCliCall();
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Customer
 *
 * File        : ClishAtCiena.c
 *
 * Created Date: Jul 22, 2016
 *
 * Description : Ciena
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../AtClish.h"

/*--------------------------- Define -----------------------------------------*/
/* FOR Kbyte APS Defect */
#define KbyteApsDefectType "(da-mismatch|ethtype-mismatch|ver-mismatch|type-mismatch|length-field-mismatch|length-count-mismatch|channel-mismatch|watchdog-expire)"
#define KbyteApsDefectMask "((("KbyteApsDefectType"[\\|])+"KbyteApsDefectType")|"KbyteApsDefectType")"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("ciena", "Ciena specific commands")
TOP_SDKCOMMAND("ciena pw", "Ciena PW commands")
TOP_SDKCOMMAND("ciena pw kbyte", "Ciena PW K-Byte commands")
TOP_SDKCOMMAND("ciena pw kbyte alarm", "Ciena PW K-Byte Alarm commands")
TOP_SDKCOMMAND("ciena pw kbyte channel", "Ciena PW K-Byte Channel commands")
TOP_SDKCOMMAND("ciena pw kbyte channel alarm", "CLIs to control K-byte channel alarm")
TOP_SDKCOMMAND("ciena pw kbyte channel override", "Ciena PW Override K-Byte Channel commands")
TOP_SDKCOMMAND("ciena pw kbyte channel validation", "Ciena PW Validation K-Byte Channel commands")
TOP_SDKCOMMAND("ciena pw kbyte label", "Ciena PW Validation K-Byte Label commands")
TOP_SDKCOMMAND("ciena sdh", "Ciena SDH commands")
TOP_SDKCOMMAND("ciena sdh line", "Ciena SDH Line commands")
TOP_SDKCOMMAND("ciena sdh line extendedkbyte", "Ciena SDH Line Extended K-Byte commands")
TOP_SDKCOMMAND("ciena sdh line kbyte", "Ciena SDH Line K-Byte commands")
TOP_SDKCOMMAND("ciena pw kbyte channel suppress", "CLIs to control supressing")
TOP_SDKCOMMAND("show ciena", "Show Ciena information commands")
TOP_SDKCOMMAND("show ciena sdh", "Show Ciena SDH commands")
TOP_SDKCOMMAND("show ciena sdh line", "Show Ciena SDH Line commands")

/* Other enumerations */
DEF_PTYPE("eAtCienaSdhLineExtendedKBytePosition", "select", "d1-sts1-4(1), d1-sts1-10(2)", "Position Extended K Byte")
DEF_PTYPE("eAtCienaSdhLineKByteSource", "select", "cpu(1), sgmii(2)", "K Byte source type")

DEF_PTYPE("KbyteApsAlarmTypeMask", "regexp", KbyteApsDefectMask,
          "\nKbyte APS Alarm masks:\n"
          "- da-mismatch          : DA/SA mismatch \n"
          "- ethtype-mismatch     : Eth Type mismatch \n"
          "- ver-mismatch         : VerSion mismatch \n"
          "- type-mismatch        : Type field mismatch\n"
          "- length-field-mismatch: Length filed mismatch \n"
          "- length-count-mismatch: Length count mismatch \n"
          "- channel-mismatch     : Channel mismatch \n"
          "- watchdog-expire      : Watchdog timer expires \n")

DEF_SDKCOMMAND(cliCienaPwKbyteEthType,
               "ciena pw kbyte ethtype",
               "Set ETH type field value to insert on the PW kbyte Pkt",
               DEF_SDKPARAM("ethtype", "STRING", "Value of Eth Type such as 0x8100"),
               "${ethtype}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliCienaPwKbyteType,
               "ciena pw kbyte type",
               "Set type field value to insert on the PW kbyte Pkt",
               DEF_SDKPARAM("type", "STRING", "Value of Eth Type such as 0x54"),
               "${type}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliCienaPwKbyteVersion,
               "ciena pw kbyte version",
               "Set version field value to insert on the PW kbyte Pkt",
               DEF_SDKPARAM("type", "STRING", "Value of Eth Type such as 0x01"),
               "${type}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliCienaPwKbyteShow,
               "show ciena pw kbyte",
               "Show PW K-byte Information",
               DEF_NULLPARAM,"")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliCienaPwKbyteChannelValidationEnable,
               "ciena pw kbyte channel validation enable",
               "Enable K-Byte validation to transmit frame immediately upon newly validated K bytes or Extended K-bytes",
               DEF_SDKPARAM("channels", "SdhLineList", ""),
               "${channels}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliCienaPwKbyteChannelValidationDisable,
               "ciena pw kbyte channel validation disable",
               "Disable K-Byte validation to transmit frame immediately upon newly validated K bytes or Extended K-bytes",
               DEF_SDKPARAM("channels", "SdhLineList", ""),
               "${channels}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliCienaPwKbyteChannelShow,
               "show ciena pw kbyte channel",
               "Show status enable of  SONET/SDH line Id on KByte Pw",
               DEF_SDKPARAM("channels", "SdhLineList", ""),
               "${channels}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliCienaPwKbyteEthHeaderSet,
               "ciena pw kbyte ethheader",
               "Set Ethernet Header Information",
               DEF_SDKPARAM("mac", "MacAddr", "DMAC Address")
               DEF_SDKPARAM("cVlan", "VlanDesc", "")
               DEF_SDKPARAM("sVlan", "VlanDesc", ""),
               "${mac} ${cVlan} ${sVlan}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliCienaPwKbyteEthCentralMacSet,
               "ciena pw kbyte centralmac",
               "Set Ethernet Header Central MAC (Expected SA)",
               DEF_SDKPARAM("centralmac", "MacAddr", "Central Address"),
               "${centralmac}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliCienaPwKbyteEthRemoteMacSet,
               "ciena pw kbyte remotemac",
               "Set Ethernet Header Remote MAC (Expected DA)",
               DEF_SDKPARAM("remotemac", "MacAddr", "Remote Address"),
               "${remotemac}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCienaSdhLineExtendedKByteEnable,
               "ciena sdh line extendedkbyte enable",
               "Enable SDH Line Extended K Byte",
               DEF_SDKPARAM("line", "SdhLineList", ""),
               "${line}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCienaSdhLineExtendedKByteDisable,
               "ciena sdh line extendedkbyte disable",
               "Disable SDH Line Extended K Byte",
               DEF_SDKPARAM("line", "SdhLineList", ""),
               "${line}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCienaSdhLineExtendedKBytePositionSet,
               "ciena sdh line extendedkbyte position",
               "Set Position of SDH Line Extended K Byte",
               DEF_SDKPARAM("line", "SdhLineList", "")
               DEF_SDKPARAM("position", "eAtCienaSdhLineExtendedKBytePosition", ""),
               "${line} ${position}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCienaSdhLineExtendedKByteShow,
               "show ciena sdh line extendedkbyte",
               "Show SDH Line Extended K Byte configuration",
               DEF_SDKPARAM("line", "SdhLineList", ""),
               "${line}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliCienaPwKbyteChannelSuppress,
               "ciena pw kbyte channel suppress enable",
               "AIS and RDI is suppressed when insert K-bytes to overhead from received packets",
               DEF_SDKPARAM("channels", "SdhLineList", ""),
               "${channels}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliCienaPwKbyteChannelUnsuppress,
               "ciena pw kbyte channel suppress disable",
               "AIS and RDI is unsuppressed when insert K-bytes to overhead from received packets",
               DEF_SDKPARAM("channels", "SdhLineList", ""),
               "${channels}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliCienaPwKbyteWatchdog,
               "ciena pw kbyte watchdog",
               "Watchdog timer if no frames received for 256us to 16ms, 0 for disable watchdog",
               DEF_SDKPARAM("timerInUs", "UINT", ""),
               "${timerInUs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliCienaPwKbyteInterval,
               "ciena pw kbyte interval",
               "Interval timer for transmit frame form 125us to 8ms, 0 for disable transmit",
               DEF_SDKPARAM("timerInUs", "UINT", ""),
               "${timerInUs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliCienaPwKbyteTrigger,
               "ciena pw kbyte trigger",
               "Trigger to transmit a Kbyte/APS ETH frame to SGMII",
               DEF_NULLPARAM,"")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliCienaPwKbyteChannelOverrideEnable,
               "ciena pw kbyte channel override enable",
               "Enable override KBytes",
               DEF_SDKPARAM("channels", "SdhLineList", ""),
               "${channels}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliCienaPwKbyteChannelOverrideDisable,
               "ciena pw kbyte channel override disable",
               "Disable override KBytes",
               DEF_SDKPARAM("channels", "SdhLineList", ""),
               "${channels}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliCienaPwKbyteChannelOverrideK1,
               "ciena pw kbyte channel override k1",
               "Set the override value of K1",
               DEF_SDKPARAM("channels", "SdhLineList", "")
               DEF_SDKPARAM("value", "STRING", "Value of transmitted K1"),
               "${channels} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliCienaPwKbyteChannelOverrideK2,
               "ciena pw kbyte channel override k2",
               "Set the override value of K2",
               DEF_SDKPARAM("channels", "SdhLineList", "")
               DEF_SDKPARAM("value", "STRING", "Value of transmitted K1"),
               "${channels} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliCienaPwKbyteChannelOverrideEK1,
               "ciena pw kbyte channel override ek1",
               "Set the override value of EK1",
               DEF_SDKPARAM("channels", "SdhLineList", "")
               DEF_SDKPARAM("value", "STRING", "Value of transmitted K1"),
               "${channels} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliCienaPwKbyteChannelOverrideEK2,
               "ciena pw kbyte channel override ek2",
               "Set the override value of EK2",
               DEF_SDKPARAM("channels", "SdhLineList", "")
               DEF_SDKPARAM("value", "STRING", "Value of transmitted K1"),
               "${channels} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaPwKbyteCountersShow,
               "show ciena pw kbyte counters",
               "Show pw kbyte counters",
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
			   DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaPwKbyteTransmit,
               "ciena pw kbyte label transmit",
               "Set value of the upper 12 bits for APS relay message",
               DEF_SDKPARAM("channels", "SdhLineList", "")
               DEF_SDKPARAM("label", "STRING", "label of APS relay"),
               "${channels} ${label}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaPwKbyteExpected,
               "ciena pw kbyte label expect",
               "Set expected value of the APS relay message",
               DEF_SDKPARAM("channels", "SdhLineList", "")
               DEF_SDKPARAM("label", "STRING", "label of APS relay"),
               "${channels} ${label}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaSdhLineKByteSourceSet,
               "ciena sdh line kbyte source",
               "Set Kbyte Source",
               DEF_SDKPARAM("line", "SdhLineList", "")
               DEF_SDKPARAM("source", "eAtCienaSdhLineKByteSource", ""),
               "${line} ${source}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaSdhLineKByteShow,
               "show ciena sdh line kbyte",
               "Show Kbyte Source of SONET/SDH line",
               DEF_SDKPARAM("line", "SdhLineList", ""),
               "${line}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaSdhLineTxEK1Set,
               "ciena sdh line ek1",
               "Set TX EK1",
               DEF_SDKPARAM("line", "SdhLineList", "")
               DEF_SDKPARAM("value", "STRING", "value EK1"),
               "${line} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaSdhLineTxEK2Set,
               "ciena sdh line ek2",
               "Set TX EK2",
               DEF_SDKPARAM("line", "SdhLineList", "")
               DEF_SDKPARAM("value", "STRING", "value EK2"),
               "${line} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaKbyteApsIntrMsk,
               "ciena pw kbyte interruptmask",
               "Set interrupt masks\n",
               DEF_SDKPARAM("mask", "KbyteApsAlarmTypeMask", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${mask} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaKbyteApsInterruptsShow,
               "show ciena pw kbyte interrupt",
               "Show interrupt",
               DEF_SDKPARAM("counterReadMode", "HistoryReadingMode", "read only/read to clear mode")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${counterReadMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaKbyteApsAlarmShow,
               "show ciena pw kbyte alarm",
               "Show current alarm",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaKbyteApsAlarmCapture,
               "ciena pw kbyte alarm capture",
               "Enable/disable Kbyte PW alarm capturing",
               DEF_SDKPARAM("enable", "eBool", ""),
               "${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCienaKbytePwDebug,
               "debug ciena pw kbyte",
               "Show debug information",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCienaPwKByteChannelsAlarmShow,
               "show ciena pw kbyte channel alarm",
               "Show KByte PW channel current alarm",
               DEF_SDKPARAM("channels", "SdhLineList", ""),
               "${channels}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCienaPwKByteChannelsInterruptShow,
               "show ciena pw kbyte channel interrupt",
               "Show KByte PW channel interrupt status",
               DEF_SDKPARAM("channels", "SdhLineList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", "Read to clear without display table"),
               "${channels} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCienaPwKByteChannelsInterruptMaskSet,
               "ciena pw kbyte channel interruptmask",
               "Enable/disable interrupt mask",
               DEF_SDKPARAM("channels", "SdhLineList", "")
               DEF_SDKPARAM("mask", "KbyteApsAlarmTypeMask", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${channels} ${mask} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCienaPwKByteChannelsAlarmCapture,
               "ciena pw kbyte channel alarm capture",
               "Enable/disable Kbyte channel alarm capturing",
               DEF_SDKPARAM("channels", "SdhLineList", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${enable}")
    {
    mAtCliCall();
    }


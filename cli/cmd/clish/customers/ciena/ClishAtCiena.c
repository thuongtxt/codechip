/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Customer
 *
 * File        : ClishAtCiena.c
 *
 * Created Date: Jul 22, 2016
 *
 * Description : Ciena
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "../../AtClish.h"

/*--------------------------- Define -----------------------------------------*/
#define EthSgmiiAlarmType "(dcc_da_mis|dcc_cvlan_mis|dcc_len_oversize|aps_da_mis|aps_type_mis|aps_ver_mis|aps_length_field_mis|aps_length_count_mis|aps_channel_mis|aps_watchdog_timer)"
#define EthSgmiiAlarmMask "((("EthSgmiiAlarmType"[\\|])+"EthSgmiiAlarmType")|"EthSgmiiAlarmType")"
DEF_PTYPE("NUM_HEX", "regexp", "(0[xX]([0-9a-fA-F]{1,4}))", "hex num")
DEF_PTYPE("EthSgmiiAlarmMaskType", "regexp", EthSgmiiAlarmMask,
          "\nalarm masks type:\n"
          "- dcc_da_mis                     : Received DA value of APS frame different from configured DA\n"
          "- dcc_cvlan_mis                  : Received 12b CVLAN ID value of DCC frame different from global provisioned CVID\n"
          "- dcc_len_oversize               : Received packet from SGMII port has FCS error\n"
          "- aps_da_mis                     : Received DA value of APS frame different from configured DA\n"
          "- aps_type_mis                   : Received ETHERNET TYPE value of APS frame different from configuration\n"
          "- aps_ver_mis                    : Received VERSION value of APS frame different from configuration\n"
          "- aps_length_field_mis           : Received LENGTH FIELD value of APS frame different from configuration\n"
          "- aps_length_count_mis           : Received PACKET BYTE COUNTER value of APS frame different from configuration\n"
          "- aps_channel_mis                : Received CHANNEL ID value of APS frame different from configuration\n"
          "- aps_watchdog_timer             : No packets received in the period defined in watchdog timer register\n"
          "Note, these masks can be ORed, example dcc_da_mis|aps_da_mis\n")

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
DEF_PTYPE("eAtCienaClockMonitorOutput", "select", "spare_serdes(0), overhead(1), xfi155_52M_0(2), xfi155_52M_1(3), xfi156_25M_0(4), xfi156_25M_1(5), eth40g_0(6), eth40g_1(7), basex2500(8), qdr(9), ddr_1(10), ddr_2(11), ddr_3(12), ddr_4(13), pcie(14), prc(15), ext(16), system(17)", "Reference clock source")

TOP_SDKCOMMAND("ciena", "Ciena specific commands")
TOP_SDKCOMMAND("ciena module", "Ciena Module commands")
TOP_SDKCOMMAND("ciena clock", "Ciena clock commands")
TOP_SDKCOMMAND("ciena clock monitor", "Ciena clock monitor commands")

TOP_SDKCOMMAND("show ciena", "Show Ciena information commands")
TOP_SDKCOMMAND("show ciena module", "Show Ciena module information commands")
TOP_SDKCOMMAND("show ciena clock", "Show Ciena clock module information commands")
TOP_SDKCOMMAND("show ciena clock monitor", "Show Ciena clock monitor commands")
TOP_SDKCOMMAND("show ciena device", "Ciena specific CLIs to show device information")

TOP_SDKCOMMAND("show ciena eth port threshold", "CLIs to show Ethernet thresholds\n")
TOP_SDKCOMMAND("ciena eth port threshold", "CLIs to handle Ethernet threshold")
TOP_SDKCOMMAND("ciena eth port threshold excessive_error_ratio", "CLIs to set Excessive Error Ratio thresholds\n")

DEF_SDKCOMMAND(CliAtCienaDeviceVersionShow,
               "show ciena device version",
               "Show device informations",
               DEF_NULLPARAM, 
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaModuleClockMonitorOutputSet,
               "ciena clock monitor output",
               "Set Ciena clock monitor output",
               DEF_SDKPARAM("outputId", "STRING", "Monitoring output ID: 1,2")
               DEF_SDKPARAM("source", "eAtCienaClockMonitorOutput", "Reference clock source"),
               "${outputId} ${source}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaModuleClockMonitorOutputGet,
               "show ciena clock monitor output",
               "Show ciena clock monitor output",
               DEF_SDKPARAM("outputId", "STRING", "Monitoring output ID: 1,2"),
               "${outputId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCienaEthPortExcessiveErrorRatioUpperThreshold,
               "ciena eth port threshold excessive_error_ratio upper",
               "Set excessive error ratio upper threshold\n",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("threshold", "UINT", "Threshold value"),
               "${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCienaEthPortExcessiveErrorRatioLowerThreshold,
               "ciena eth port threshold excessive_error_ratio lower",
               "Set excessive error ratio lower threshold\n",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("threshold", "UINT", "Threshold value"),
               "${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCienaEthPortExcessiveErrorRatioThresholdShow,
               "show ciena eth port threshold excessive_error_ratio",
               "Get excessive error ratio thresholds value\n",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    mAtCliCall();
    }

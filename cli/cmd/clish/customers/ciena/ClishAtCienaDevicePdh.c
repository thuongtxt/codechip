/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Customer
 *
 * File        : ClishAtCienaDevicePdh.c
 *
 * Created Date: Feb 27, 2017
 *
 * Description : PDH card specific CLISH CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
DEF_PTYPE("eAtCienaPdhInterfaceType", "select", "de3(0), de1(1)", "PDH interface type")

TOP_SDKCOMMAND("ciena pdh", "Ciena PDH interface")
TOP_SDKCOMMAND("ciena pdh axia4", "Ciena PDH axia4")
TOP_SDKCOMMAND("show ciena pdh", "Show the Ciena PDH interface")

DEF_SDKCOMMAND(CliAtCienaPdhInterfaceSet,
               "ciena pdh interface",
               "Set interface type for PDH",
               DEF_SDKPARAM("type", "eAtCienaPdhInterfaceType", ""),
               "${type}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaPdhTxEthEnable,
               "ciena pdh axia4 enable",
               "Enable PDH traffic to 2.5G MAC",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCienaPdhTxEthDisable,
               "ciena pdh axia4 disable",
               "Disable PDH traffic to 2.5G MAC",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCienaPdhShow,
               "show ciena device pdh",
               "Show device information of PDH card",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }



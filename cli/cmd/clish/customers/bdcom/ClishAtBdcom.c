/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : ClishAtBdCom.c
 *
 * Created Date: Oct 7, 2013
 *
 * Description : CLISH CLI for BDCOM
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("bdcom", "BDCOM specific commands")
TOP_SDKCOMMAND("bdcom eth", "BDCOM specific commands for Ethernet")
TOP_SDKCOMMAND("bdcom eth flow", "BDCOM specific commands to control BDCOM Ethernet Flows")
TOP_SDKCOMMAND("bdcom eth flow header", "BDCOM specific commands to control BDCOM Header")
TOP_SDKCOMMAND("show bdcom", "BDCOM specific show commands")
TOP_SDKCOMMAND("show bdcom eth", "BDCOM specific commands to show BDCOM Ethernet")
TOP_SDKCOMMAND("show bdcom eth flow", "Commands to get BDCOM Ethernet Flows")
TOP_SDKCOMMAND("show bdcom ppp", "Commands to get BDCOM PPP table")
TOP_SDKCOMMAND("bdcom pppheader", "BDCOM specific commands for PPP header")
TOP_SDKCOMMAND("bdcom ethtype", "BDCOM specific commands for Ethernet type")
TOP_SDKCOMMAND("bdcom hdlc", "BDCOM specific commands for Hdlc")
TOP_SDKCOMMAND("bdcom hdlc link", "BDCOM specific commands for Hdlc Link")
TOP_SDKCOMMAND("bdcom hdlc link oam", "Commands to control BDCOM tags Oam")
TOP_SDKCOMMAND("bdcom hdlc link oam header", "Commands to control Header BDCOM tags Oam ")
TOP_SDKCOMMAND("show bdcom hdlc", "BDCOM specific show commands for Hdlc")
TOP_SDKCOMMAND("show bdcom hdlc link", "BDCOM specific show commands for Hdlc Link")
TOP_SDKCOMMAND("show bdcom hdlc link oam", "BDCOM specific show commands for Hdlc Link Oam")
TOP_SDKCOMMAND("bdcom mdio", "BDCOM MDIO")

DEF_PTYPE("PacketTag", "regexp", ".*",
          "BDCOM Tag. Format: cpuPktIndicator.portNumber.encapType.slotNumber.channelNumber.pktLen\n"
          "Where:\n"
          "+ cpuPktIndicator: 0/1\n"
          "+ portNumber     : 0..15\n"
          "+ encapType      : 0..15\n"
          "+ slotNumber     : 0..15\n"
          "+ channelNumber  : 0..1023\n"
          "+ pktLen         : 0..63\n")

DEF_SDKCOMMAND(cliAtBdcomEthFlowTxHeaderSet,
               "bdcom eth flow header transmit",
               "Set BDCOM Ethernet Header Information for transmitting to ETH side",
               DEF_SDKPARAM("flowList", "FlowIdList", "")
               DEF_SDKPARAM("portId", "PortIdList", "")
               DEF_SDKPARAM("bdcomTag", "PacketTag", ""),
               "${flowList} ${portId} ${bdcomTag}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtBdcomEthFlowExpectedHeaderSet,
               "bdcom eth flow header expect",
               "Set BDCOM expected tags for PPP lookup at RX direction of ETH",
               DEF_SDKPARAM("flowList", "FlowIdList", "")
               DEF_SDKPARAM("portId", "PortIdList", "")
               DEF_SDKPARAM("bdcomTag", "PacketTag", ""),
               "${flowList} ${portId} ${bdcomTag}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtBdcomEthFlowHeaderGet,
               "show bdcom eth flow header",
               "Show ZTE eth flow header information",
               DEF_SDKPARAM("flowList", "FlowIdList", ""),
               "${flowList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtBdcomTdmToPsnPppHeaderSet,
               "bdcom pppheader tdm2psn",
               "Set PPP header for sepecified entries at TDM to PSN direction",
               DEF_SDKPARAM("linkList", "PppLinklist", "")
               DEF_SDKPARAM("entryIds", "EntryLists", "")
               DEF_SDKPARAM("pppHeader", "STRING", "PPP header value")
               DEF_SDKPARAM("headerLength", "STRING", "PPP header length"),
               "${linkList} ${entryIds} ${pppHeader} ${headerLength}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtBdcomPsnToTdmPppHeaderSet,
               "bdcom pppheader psn2tdm",
               "Set PPP header for sepecified entries at PSN to TDM direction",
               DEF_SDKPARAM("linkList", "PppLinklist", "")
               DEF_SDKPARAM("entryIds", "EntryLists", "")
               DEF_SDKPARAM("pppHeader", "STRING", "PPP header value")
               DEF_SDKPARAM("headerLength", "STRING", "PPP header length"),
               "${linkList} ${entryIds} ${pppHeader} ${headerLength}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtBdcomTdmToPsnEthTypeSet,
               "bdcom ethtype tdm2psn",
               "Set ethernet type for sepecified entries at TDM to PSN direction",
               DEF_SDKPARAM("linkList", "PppLinklist", "")
               DEF_SDKPARAM("entryIds", "EntryLists", "")
               DEF_SDKPARAM("ethType", "STRING", "Ethernet type value"),
               "${linkList} ${entryIds} ${ethType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtBdcomPsnToTdmEthTypeSet,
               "bdcom ethtype psn2tdm",
               "Set ethernet type for sepecified entries at PSN to TDM direction",
               DEF_SDKPARAM("linkList", "PppLinklist", "")
               DEF_SDKPARAM("entryIds", "EntryLists", "")
               DEF_SDKPARAM("ethType", "STRING", "Ethernet type value"),
               "${linkList} ${entryIds} ${ethType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtBdcomPppTableEntryGet,
               "show bdcom ppp table",
               "Show PPP table entry detail of PPP link.\n",
               DEF_SDKPARAM("linkList", "PppLinklist", ""),
               "${linkList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtBdcomHdlcLinkOamTxEthHeaderSet,
               "bdcom hdlc link oam header transmit",
               "Set BDCOM OAM Ethernet Header Information for transmitting to ETH side",
               DEF_SDKPARAM("linkList", "PppLinklist", "")
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("bdcomTag", "PacketTag", ""),
               "${linkList} ${portList} ${bdcomTag}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtBdcomHdlcLinkOamIgTagsSet,
               "bdcom hdlc link oam header expect",
               "Set BDCOM OAM expected tags for lookup at RX direction of ETH",
               DEF_SDKPARAM("linkList", "PppLinklist", "")
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("bdcomTag", "PacketTag", ""),
               "${linkList} ${portList} ${bdcomTag}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtBdcomHdlcLinkOamTagsGet,
               "show bdcom hdlc link oam header",
               "Show BDCOM hdlc link oam header information",
               DEF_SDKPARAM("linkList", "PppLinklist", ""),
               "${linkList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtBdcomMdioRead,
               "bdcom mdio rd",
               "MDIO Read",
               DEF_SDKPARAM("address", "RegisterAddress", ""),
               "${address}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtBdcomMdioWrite,
             "bdcom mdio wr",
             "MDIO Write",
             DEF_SDKPARAM("address", "RegisterAddress", "")
             DEF_SDKPARAM("value", "RegisterValue", ""),
             "${address} ${value}")
    {
    mAtCliCall();
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ISR
 *
 * File        : ClishAtIsrInterruptController.c
 *
 * Created Date: Sep 13, 2017
 *
 * Description : ISR interrupt controller CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("show isr", "CLI to show ISR information")
TOP_SDKCOMMAND("show isr interrupt", "CLI to show ISR interrupt Cores")
TOP_SDKCOMMAND("isr", "CLIs to handle interrupt at ISR context")
TOP_SDKCOMMAND("isr interrupt", "CLIs to handle interrupt at ISR context")
TOP_SDKCOMMAND("isr interrupt core", "CLIs to handle interrupt CORE at ISR context")

DEF_SDKCOMMAND(cliAtIsrInterruptControllerCoresShow,
              "show isr interrupt core",
              "Show interrupt COREs\n",
              DEF_NULLPARAM,
              "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtIsrInterruptControllerCoreEnable,
              "isr interrupt core enable",
              "Enable interrupt COREs\n",
              DEF_SDKPARAM("coreIds", "STRING", "Interrupt CORE IDs"),
              "${coreIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtIsrInterruptControllerCoreDisable,
              "isr interrupt core disable",
              "Disable interrupt COREs\n",
              DEF_SDKPARAM("coreIds", "STRING", "Interrupt CORE IDs"),
              "${coreIds}")
    {
    mAtCliCall();
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : ClishAtCisco.c
 *
 * Created Date: Mar 9, 2015
 *
 * Description : Cisco CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "../../AtClish.h"
#include "AtCommon.h"

/*--------------------------- Define -----------------------------------------*/
DEF_PTYPE("AtCpldLoopbackMode", "select", "release(0), local(1), remote(2) dual(3)", "CPLD Loopback mode")

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
extern eAtRet DiagnosticSetup(uint32 sdhLineId, uint32 ethPortId);
extern eAtRet DiagnosticTearDown(uint32 sdhLineId, uint32 ethPortId);

/*--------------------------- Implementation ---------------------------------*/
DEF_PTYPE("VlanControl", "regexp", ".*", "VLAN control. Its format is: PRI.CFI.VLANID (or none)")

TOP_SDKCOMMAND("cisco", "Cisco specific commands")
TOP_SDKCOMMAND("cisco sample", "Cisco sample code specific commands")
TOP_SDKCOMMAND("cisco sample diagnostic", "Cisco sample code diagnostic specific commands")
TOP_SDKCOMMAND("cisco slot", "Cisco slot specific commands")
TOP_SDKCOMMAND("cisco ip", "Cisco XFI specific commands")
TOP_SDKCOMMAND("cisco ip xfi", "Cisco XFI specific commands")
TOP_SDKCOMMAND("cisco xfi", "Cisco XFI specific commands")
TOP_SDKCOMMAND("cisco xfi diag", "Cisco XFI diag specific commands")
TOP_SDKCOMMAND("cisco cpld", "Configure CPLD commands")
TOP_SDKCOMMAND("show cisco", "Show the Cisco specific commands")
TOP_SDKCOMMAND("show cisco xfi", "Show the Cisco backplan XFI active group")
TOP_SDKCOMMAND("show cisco cpld", "Show the CPLD")
TOP_SDKCOMMAND("cisco serdes", "Configure Serdes commands")
TOP_SDKCOMMAND("cisco serdes prbs", "Configure Serdes prbs commands")
TOP_SDKCOMMAND("cisco control", "Configure Control commands")
TOP_SDKCOMMAND("cisco control flow", "Configure Control Flows commands")
TOP_SDKCOMMAND("cisco control flow egress", "Configure Control Flows Egress commands")
TOP_SDKCOMMAND("cisco control flow ingress", "Configure Control Flows Ingress commands")
TOP_SDKCOMMAND("show cisco control", "Show the Cisco Control specific commands")
TOP_SDKCOMMAND("show cisco control flow", "Show the Cisco Control specific commands")
TOP_SDKCOMMAND("cisco control flow egress control", "Configure Control Flows Egress control commands")
TOP_SDKCOMMAND("cisco control flow egress circuit", "Configure Control Flows Egress circuit commands")
TOP_SDKCOMMAND("cisco control flow egress circuit vlan", "Configure Control Flows Egress circuit vlan commands")
TOP_SDKCOMMAND("cisco control flow ingress control", "Configure Control Flows Ingress commands")
TOP_SDKCOMMAND("show cisco ip", "Show the Cisco backplan XFI active group")
TOP_SDKCOMMAND("show cisco ip xfi", "Show the Cisco backplan XFI active group")

TOP_SDKCOMMAND("cisco ge", "Configure GE commands")
TOP_SDKCOMMAND("cisco ge passthrough ptch", "Configure GE passthrough PTCH commands")
TOP_SDKCOMMAND("show cisco ge", "Show GE commands")

DEF_SDKCOMMAND(CliDiagnosticStart,
               "cisco sample diagnostic start",
               "",
               DEF_SDKPARAM("sdhLineId", "UINT", "")
               DEF_SDKPARAM("ethPortId", "UINT", ""),
               "${sdhLineId} ${ethPortId}")
    {
    eAtRet ret = DiagnosticSetup(AtStrToDw(lub_argv__get_arg(argv, 0)) - 1, AtStrToDw(lub_argv__get_arg(argv, 1)) - 1);
    AtUnused(context);
    if (ret != cAtOk)
        AtPrintc(cSevCritical, "Can not start sample diagnostic\r\n");
    return 0;
    }

DEF_SDKCOMMAND(CliDiagnosticStop,
               "cisco sample diagnostic stop",
               "",
               DEF_SDKPARAM("sdhLineId", "UINT", "")
               DEF_SDKPARAM("ethPortId", "UINT", ""),
               "${sdhLineId} ${ethPortId}")
    {
    eAtRet ret = DiagnosticTearDown(AtStrToDw(lub_argv__get_arg(argv, 0)) - 1, AtStrToDw(lub_argv__get_arg(argv, 1)) - 1);
    AtUnused(context);
    if (ret != cAtOk)
        AtPrintc(cSevCritical, "Can not stop sample diagnostic\r\n");

    return 0;
    }

DEF_SDKCOMMAND(CliAtCiscoSlotAdd,
               "cisco slot add",
               "",
               DEF_SDKPARAM("slotIdList", "STRING", "")
               DEF_SDKPARAM_OPTION("productCode", "STRING", "")
               DEF_SDKPARAM_OPTION("memMapOffset", "STRING", ""),
               "${slotIdList} ${productCode} ${memMapOffset}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCiscoSlotRemove,
               "cisco slot remove",
               "",
               DEF_SDKPARAM("slotIdList", "STRING", ""),
               "${slotIdList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCiscoXfiDiagEnable,
               "cisco xfi diag enable",
               "",
               DEF_SDKPARAM("groupIdList", "STRING", "groupId"),
               "${groupIdList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCiscoXfiDiagDisable,
               "cisco xfi diag disable",
               "",
               DEF_SDKPARAM("groupIdList", "STRING", "groupId"),
               "${groupIdList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCiscoXfiGroupSelect,
               "cisco xfi group",
               "Select XFI active group for backplan: 1: serdes 1,3,5,7; 2: serdes 2,4,6,8",
               DEF_SDKPARAM("groupIdList", "STRING", "groupId"),
               "${groupIdList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCiscoXfiGroupShow,
               "show cisco xfi group",
               "Show XFI active group for backplan: 1: serdes 1,3,5,7; 2: serdes 2,4,6,8",
               DEF_NULLPARAM,"")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCiscoCpldVersionShow,
               "show cisco cpld version",
               "Show CPLD version\n",
               DEF_NULLPARAM,"")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCiscoCpldDs1LoopbackSet,
               "cisco cpld loopback",
               "Configure loopback mode for CPLD\n",
               DEF_SDKPARAM("loopbackMode", "AtCpldLoopbackMode", ""),
               "${loopbackMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDestMacSet,
               "cisco serdes prbs destmac",
               "\nSet Serdes PRBS Destination MAC Address\n",
               DEF_SDKPARAM("serdesList", "STRING", "Serdes IDs")
               DEF_SDKPARAM("mac", "MacAddr", ""),
               "${serdesList} ${mac}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineSrcMacSet,
               "cisco serdes prbs srcmac",
               "\nSet Serdes PRBS Source MAC Address\n",
               DEF_SDKPARAM("serdesList", "STRING", "Serdes IDs")
               DEF_SDKPARAM("mac", "MacAddr", ""),
               "${serdesList} ${mac}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineEthTypeSet,
               "cisco serdes prbs ethtype",
               "\nSet Serdes PRBS Ethernet type\n",
               DEF_SDKPARAM("serdesList", "STRING", "Serdes IDs")
               DEF_SDKPARAM("ethType", "STRING", "Ethernet Type"),
               "${serdesList} ${ethType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCiscoControlFlowEgressSrcMacSet,
               "cisco control flow egress srcmac",
               "\nSet Control Flow Source MAC Address\n",
               DEF_SDKPARAM("mac", "MacAddr", ""),
               "${mac}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCiscoControlFlowEgressDestMacSet,
               "cisco control flow egress destmac",
               "\nSet Control Flow Dest MAC Address\n",
               DEF_SDKPARAM("mac", "MacAddr", ""),
               "${mac}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCiscoControlFlowEgressVlanSet,
               "cisco control flow egress control vlan",
               "\nSet Control Flow Egress vlan\n",
               DEF_SDKPARAM("tpid", "STRING", "TPID in Vlan TAG Ex.0x8100")
               DEF_SDKPARAM("vlan", "VlanDesc", ""),
               "${tpid} ${vlan}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCiscoControlFlowIngressVlanSet,
               "cisco control flow ingress control vlan",
               "\nSet Control Flow Ingress vlan\n",
               DEF_SDKPARAM("tpid", "STRING", "TPID in Vlan TAG Ex.0x8100")
               DEF_SDKPARAM_OPTION("vlan", "VlanDesc", ""),
               "${tpid} ${vlan}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCiscoControlFlowEgressEthTypeSet,
               "cisco control flow egress ethtype",
               "\nSet Control Flow Egress Ethernet Type\n",
               DEF_SDKPARAM("ethType", "STRING", "Ethernet Type"),
               "${ethType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCiscoControlFlowEgressGet,
               "show cisco control flow header",
               "\nGet control flow configure\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCiscoControlFlowEgressCircuitVlanTPIDSet,
               "cisco control flow egress circuit vlan tpid",
               "\nSet Egress circuit vlan TPID\n",
               DEF_SDKPARAM("tpid", "STRING", "TPID in Vlan TAG Ex.0x8100"),
               "${tpid}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCiscoIpXfiGroupSelect,
               "cisco ip xfi group",
               "Select CiscoIp XFI active group for backplan: 3: serdes 9,10,13,14; 4: serdes 11,12,15,16",
               DEF_SDKPARAM("groupIdList", "STRING", "groupId"),
               "${groupIdList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCiscoIpXfiGroupShow,
               "show cisco ip xfi group",
               "Show CiscoIp XFI active group for backplan: 3: serdes 9,10,13,14; 4: serdes 11,12,15,16",
               DEF_NULLPARAM,"")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCiscoUpsrAlarmMask,
             "debug aps upsr alarmmask",
             "Set APS UPSR alarm mask for SONET/SDH paths\n",
             DEF_SDKPARAM("type", "APSDefType", "")
             DEF_SDKPARAM("alarmMask", "UpsrAlarmDefMask", ""),
             "${type} ${alarmMask}")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(cliAtCiscoUpsrAlarmMaskShow,
             "show debug aps upsr alarmmask",
             "Show APS UPSR alarm mask.\n",
             DEF_NULLPARAM,
             "")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(CliAtCiscoEthPassThrough,
               "cisco ge passthrough",
               "Configure GE port pass-through",
               DEF_SDKPARAM("portId", "PortIdList", "")
               DEF_SDKPARAM("bypassPortId", "STRING", ""),
               "${portId} ${bypassPortId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCiscoEthPortPassThroughEgressPtch,
               "cisco ge passthrough ptch egress",
               "Configure GE port pass-through PTCH ID expected from ARAD",
               DEF_SDKPARAM("portId", "PortIdList", "")
               DEF_SDKPARAM("ptchId", "STRING", ""),
               "${portId} ${ptchId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCiscoEthPortPassThroughIngressPtch,
               "cisco ge passthrough ptch ingress",
               "Configure GE port pass-through PTCH ID send to ARAD",
               DEF_SDKPARAM("portId", "PortIdList", "")
               DEF_SDKPARAM("ptchId", "STRING", ""),
               "${portId} ${ptchId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCiscoEthPortPassThroughPtchEnable,
               "cisco ge passthrough ptch enable",
               "Enable PTCH header",
               DEF_SDKPARAM("portId", "PortIdList", ""),
               "${portId}}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCiscoEthPortPassThroughPtchDisable,
               "cisco ge passthrough ptch disable",
               "Diable PTCH header",
               DEF_SDKPARAM("portId", "PortIdList", ""),
               "${portId}}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCiscoEthPortPassThroughIngressPtchMode,
               "cisco ge passthrough ptch ingress mode",
               "Configure PTCH insertion mode",
               DEF_SDKPARAM("portId", "PortIdList", "")
               DEF_SDKPARAM("mode", "eAtPtchServiceIngressMode", ""),
               "${portId} ${mode}}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCiscoEthPortPassThroughShow,
               "show cisco ge passthrough",
               "Show GE passthrough",
               DEF_SDKPARAM("portId", "PortIdList", ""),
               "${portId}}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCiscoEthPortPassThroughEnable,
               "cisco ge passthrough enable",
               "Enable GE passthrough traffic",
               DEF_SDKPARAM("portId", "PortIdList", ""),
               "${portId}}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtCiscoEthPortPassThroughDisable,
               "cisco ge passthrough disable",
               "Disable GE passthrough traffic",
               DEF_SDKPARAM("portId", "PortIdList", ""),
               "${portId}}")
    {
    mAtCliCall();
    }

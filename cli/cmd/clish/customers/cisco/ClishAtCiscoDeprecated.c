/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Customer
 *
 * File        : ClishAtCiscoDeprecated.c
 *
 * Created Date: Jul 29, 2016
 *
 * Description : Deprecated CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
DEF_PTYPE("eAtDriverSyncMode", "select", "read-hw(1), none(2)", "Synchronization mode")

TOP_SDKCOMMAND("driver ha", "HA driver CLIs")
TOP_SDKCOMMAND("driver sync", "Synchronization driver CLIs")
TOP_SDKCOMMAND("debug driver", "Debug driver CLIs")
TOP_SDKCOMMAND("debug driver ha", "Debug HA driver CLIs")
TOP_SDKCOMMAND("debug driver ha simulate", "Debug HA simulate driver CLIs")

/* The following CLIs are used to adapt to old Python scripts to test HA driver */
DEF_SDKCOMMAND(CliAtDriverSyncModeSet,
               "driver sync mode",
               "Set driver synchronization mode.",
               DEF_SDKPARAM_OPTION("mode", "eAtDriverSyncMode", ""),
               "${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDriverHaRestore,
               "driver ha restore",
               "Restore driver database",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDriverHaSimulateEnable,
               "debug driver ha simulate enable",
               "Enable HA simulation\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : ClishAtCiscoTest.c
 *
 * Created Date: Aug 29, 2015
 *
 * Description : CLI for self-testing
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "../../AtClish.h"
#include "AtDevice.h"
#include "AtHalCisco.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("test cisco", "Cisco self-test CLIs")

static AtHal Hal(void)
    {
    return AtDeviceIpCoreHalGet(CliDevice(), 0);
    }

static void Write(uint32 realAddress, uint32 value)
    {
    AtHalWrite(Hal(), realAddress >> 2, value);
    }

static uint32 Read(uint32 realAddress)
    {
    return AtHalRead(Hal(), realAddress >> 2);
    }

DEF_SDKCOMMAND(CliHalSimTest,
               "test cisco hal",
               "",
               DEF_NULLPARAM,
               "")
    {
    AtHal hal = AtHalCiscoNew(0, Write, Read);
    AtUnused(context);
    AtUnused(argv);
    AtHalDiagMemTest(hal);
    AtHalDelete(hal);
    return 0;
    }

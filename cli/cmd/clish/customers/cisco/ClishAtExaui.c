/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet module
 *
 * File        : ClishAtExaui.c
 *
 * Created Date: May 7, 2016
 *
 * Description : eXAUI CLISH CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../AtClish.h"

/*--------------------------- Define -----------------------------------------*/
#define ID_VALUE_1_256       "(25[0-6]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]{0,1})" /* [1, 256] */
#define EXAUIPATH            "[1-2]."ID_VALUE_1_256
#define EXAUIPATHLISTITEM    "("EXAUIPATH"|("EXAUIPATH"[-]"EXAUIPATH"))"
#define EXAUIPATHLIST        "((("EXAUIPATHLISTITEM"[,])+"EXAUIPATHLISTITEM")|"EXAUIPATHLISTITEM")"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("eth port xaui", "eXaui commands")
TOP_SDKCOMMAND("eth port xaui ingress", "Ingress eXaui commands")
TOP_SDKCOMMAND("eth port xaui egress", "Egress eXaui commands")
TOP_SDKCOMMAND("eth port xaui egress vlan", "Egress eXaui commands")

DEF_PTYPE("eXAUIPathList", "regexp", EXAUIPATHLIST,
          "List of eXAUI path\n"
          "- <xauiId>.<chnId>\n"
          "    + xauiId[1-2]\n"
          "    + chnId[1-256]\n")

DEF_PTYPE("eXauiIdList", "regexp", ".*", "List of eXAUI IDs: [1-2]\n")

DEF_SDKCOMMAND(cliAtExauiIngressVlanSet,
               "eth port xaui ingress vlan",
               "Configure Vlan for Direction from HyPhy --> XFI.\n",
               DEF_SDKPARAM("exauiPathList", "eXAUIPathList", "")
               DEF_SDKPARAM("vlan", "STRING", "VLAN which has format <tpid.cfi.priority.vlanId>"),
               "${exauiPathList} ${vlan}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtExauiEgressVlanExpectedSet,
               "eth port xaui egress vlan expect",
               "Configure Vlan for Direction from HyPhy --> XFI.\n",
               DEF_SDKPARAM("exauiPathList", "eXAUIPathList", "")
               DEF_SDKPARAM("vlanId", "UINT", "VLAN ID"),
               "${exauiPathList} ${vlanId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtExauiShow,
               "show eth port xaui",
               "Show configuration of eXAUI.\n",
               DEF_SDKPARAM("exauiPathList", "eXAUIPathList", ""),
               "${exauiPathList}")
    {
    mAtCliCall();
    }

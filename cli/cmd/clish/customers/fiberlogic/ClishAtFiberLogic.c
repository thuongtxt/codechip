/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Customer
 *
 * File        : ClishAtFiberLogic.c
 *
 * Created Date: Jun 4, 2015
 *
 * Description : Fiber Logic
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("fiberlogic", "Fiber Logic specific commands")
TOP_SDKCOMMAND("fiberlogic liu", "Fiber Logic LIU specific commands")

DEF_PTYPE("eAtFiberLogicLiuMode", "select", "e1(0), ds1(1)", "\nLIU mode")

DEF_SDKCOMMAND(CliAtFiberLogicLiuModeSet,
               "fiberlogic liu mode",
               "Set LIU mode",
               DEF_SDKPARAM("liuMode", "eAtFiberLogicLiuMode", ""),
               "${liuMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtFiberLogicLiuLoopbackSet,
               "fiberlogic liu loopback",
               "Set loopback mode for LIU",
               DEF_SDKPARAM("liuList", "PdhDe1List", "")
               DEF_SDKPARAM("loopMode", "eAtLoopbackMode", ""),
               "${liuList} ${loopMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtFiberLogicLiuRead,
               "fiberlogic liu rd",
               "Read LIU",
               DEF_SDKPARAM("liuList", "PdhDe1List", "")
               DEF_SDKPARAM("address", "RegisterAddress", ""),
               "${liuList} ${address}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtFiberLogicLiuWrite,
               "fiberlogic liu wr",
               "Write LIU",
               DEF_SDKPARAM("liuList", "PdhDe1List", "")
               DEF_SDKPARAM("address", "RegisterAddress", "")
               DEF_SDKPARAM("value", "RegisterValue", ""),
               "${liuList} ${address}")
    {
    mAtCliCall();
    }

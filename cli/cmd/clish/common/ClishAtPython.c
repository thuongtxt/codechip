/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : ClishAtPython.c
 *
 * Created Date: Dec 6, 2015
 *
 * Description : Python CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"
#include "atclib.h"
#include "AtCliModule.h"
#include "AtPython.h"
#include "atsdkpy.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void PythonInit(void)
    {
    AtPythonInit();

#ifdef ATSDKPY
    AtSdkPyInit();
#endif
    }

DEF_SDKCOMMAND(CliAtPythonScriptRun, "python", "Usage: python <python script>",
               DEF_SDKPARAM("filename", "PATHNAME_STRING", "python script name")
               DEF_SDKPARAM_OPTION("option1", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option2", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option3", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option4", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option5", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option6", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option7", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option8", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option9", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option10", "STRING", "Option"),
               "${filename} ${option1} ${option2} ${option3} ${option4} ${option5} ${option6} ${option7} ${option8} ${option9} ${option10}")
    {
    char *filename = (char *)lub_argv__get_arg(argv, 0);
    AtTextUI textUI = AtCliSharedTextUI();
    eBool historyEnabled = AtTextUIHistoryIsEnabled(textUI);
    int ret;
    char **args = lub_argv__get_argv(argv, NULL);

    AtTextUIHistoryEnable(textUI, cAtFalse);
    AtTextUIVerbose(textUI, cAtTrue);
    PythonInit();
    ret = AtPythonScriptRun(filename, (int)lub_argv__get_count(argv), args);
    if (ret != 0)
        AtPrintc(cSevCritical, "ERROR: Python run fail, script file may not exist\r\n");
    AtTextUIVerbose(textUI, cAtFalse);
    AtTextUIHistoryEnable(textUI, historyEnabled);
    AtTextUIHistoryAdd(AtCliSharedTextUI(), context->full_command);

    lub_argv__free_argv(args);

    RET_CLI(ret);
    return 0;
    }

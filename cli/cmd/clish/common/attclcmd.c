/*
 * =====================================================================================
 *
 *       Filename:  tclcmd.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  01/04/2012 03:00:21 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Quan Cao Duc (), quancd@atvn.com.vn
 *        Company:  Arrive Technologies
 *
 * =====================================================================================
 */
#include "clish/ptype.h"
#include "../AtClish.h"
#include "extAtSdkInt.h"
#include "AtCliModule.h"

TOP_SDKCOMMAND("script", "")
TOP_SDKCOMMAND("script verbose", "")

DEF_SDKCOMMAND(TclEvalFile, "tcl", "Usage: tcl <tcl script>",
               DEF_SDKPARAM("filename", "PATHNAME_STRING", "tcl script name"),
               "${filename}")
    {
    int ret = mAtCliResult();
    AtTextUIHistoryAdd(AtCliSharedTextUI(), context->full_command);
    AtUnused(argv);
    return ret;
    }

DEF_SDKCOMMAND(TclScriptVerboseEnable, "script verbose enable", "Usage: script verbose enable",
               DEF_NULLPARAM,
               "")
    {
    AtUnused(context);
    AtUnused(argv);
    AtTextUIScriptVerbose(AtCliSharedTextUI(), cAtTrue);
    return 0;
    }

DEF_SDKCOMMAND(TclScriptVerboseDisable, "script verbose disable", "Usage: script verbose disable",
               DEF_NULLPARAM,
               "")
    {
    AtUnused(context);
    AtUnused(argv);
    AtTextUIScriptVerbose(AtCliSharedTextUI(), cAtFalse);
    return 0;
    }

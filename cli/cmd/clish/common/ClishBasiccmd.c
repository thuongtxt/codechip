/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : basiccmd.c
 *
 * Created Date: Jun 11, 2013
 *
 * Description : To have basic Tiny Text-UI CLIs in CLISH Text-UI
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("history", "CLI to control CLI history")

DEF_SDKCOMMAND(cliScriptRun,
               "run",
               "Run AT script file.",
               DEF_SDKPARAM("filename", "PATHNAME_STRING", "AT script file"),
               "${filename}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliListCmd,
               "listcmd",
               "List commands",
               DEF_SDKPARAM_OPTION("module", "STRING", "basic,all,<moduleName>")
               DEF_SDKPARAM_OPTION("level1",  "STRING", "Level 1")
               DEF_SDKPARAM_OPTION("level2",  "STRING", "Level 2")
               DEF_SDKPARAM_OPTION("level3",  "STRING", "Level 3")
               DEF_SDKPARAM_OPTION("level4",  "STRING", "Level 4")
               DEF_SDKPARAM_OPTION("level5",  "STRING", "Level 5")
               DEF_SDKPARAM_OPTION("level6",  "STRING", "Level 6")
               DEF_SDKPARAM_OPTION("level7",  "STRING", "Level 7")
               DEF_SDKPARAM_OPTION("level8",  "STRING", "Level 8")
               DEF_SDKPARAM_OPTION("level9",  "STRING", "Level 9")
               DEF_SDKPARAM_OPTION("level10", "STRING", "Level 10"),
               "${module} ${level1} ${level2} ${level3} ${level4} ${level5} ${level6} ${level7} ${level8} ${level9} ${level10}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliHistoryShow,
               "show history",
               "To show CLI history",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliHistoryClear,
               "history clear",
               "To clear CLI history",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliColorShow,
               "color",
               "Show color configuration",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliColorEnable,
               "color enable",
               "Enable color mode",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliColorDisable,
               "color disable",
               "Disable color mode",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

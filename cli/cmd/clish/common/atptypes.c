/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : atptype.c
 *
 * Created Date: Nov 8, 2012
 *
 * Description : Common CLISH declarations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "sdkcmd.h"

/*--------------------------- Define -----------------------------------------*/
/*
 * CLI modes
 */
#define CliModes "((("CliMode"[\\|])+"CliMode")|"CliMode"|none)"
#define CliMode "(autotest|tcl|silent)"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
DEF_PTYPE("eAtInitMode", "select", "cold(0), warm(1)",
          "Initialize mode. \n "
          "- cold: cold init \n "
          "- warm: warm init")
DEF_PTYPE("eBool", "select", "dis(0), en(1)", "Enable or Disable")
DEF_PTYPE("eStatus", "select", "clear(0), set(1)", "Status")
DEF_PTYPE("eAtLoopbackMode", "select", "release(0), local(1), remote(2)", "Loopback mode")
DEF_PTYPE("HistoryReadingMode", "select", "ro(0), r2c(1)", "Reading mode")
DEF_PTYPE("AtCliMode", "regexp", CliModes,
          "\nCLI modes. Supported modes:\n"
          "- autotest: enable auto test\n"
          "- tcl: enable TCL\n"
          "- silent: Silent\n"
          "- none: No option\n"
          "Note, these modes can be ORed\n")
DEF_PTYPE("HexValue", "regexp", ".*", "Hex value")
DEF_PTYPE("eAtLedState", "select", "off(0), on(1), blink(3)",
          "\nLED state:\n"
          "\n- on   : Force Led State On\n"
          "\n- off  : Force Led State Off\n"
          "\n- blink: Let hardware auto control LED. LED blink when traffic is running\n")
DEF_PTYPE("eAtBerRate", "select", "1e-2(1), 1e-3(2), 1e-4(3), 1e-5(4), 1e-6(5), 1e-7(6), 1e-8(7), 1e-9(8), 1e-10(9)", "Bit-Error-Rate(BER) value")
DEF_PTYPE("eAtDirection", "select", "tx(0), rx(1), all(3)", "\nDirection")
DEF_PTYPE("eAtTimingMode", "select", "system(0), loop(1), sdh_sys(2), sdh_line(3), prc(4), ext#1(5), ext#2(6), slave(7), acr(8), dcr(9), free_running(10), none()", "\nTiming mode")
DEF_PTYPE("eAtPdhDe1FrameType", "select", "ds1_unframed(1), ds1_sf(2), ds1_esf(3), e1_unframed(8), e1_basic(9), e1_crc(10)", "\nDS1/E1 framming types")
DEF_PTYPE("eAtCepFrameType", "select", "vc11(0), vc12(1), vc3(2), vc4(3)", "\nVC framming types:vc11, vc12, vc3, vc4")
DEF_PTYPE("eAtAuFrameType", "select", "au4(0), au3(1)", "\nAU framming types:au4, au3")
DEF_PTYPE("SdhLineList", "regexp", ".*", "List of Line IDs. For example: 1-3,6,8 gives list of 1,2,3,6,8")
DEF_PTYPE("eAtBitOrder", "select", "msb(0), lsb(1)", "\nBit ordering")
DEF_PTYPE("eAtErrorGeneratorMode", "select", "oneshot(1), continuous(2)", "error generator mode")
DEF_PTYPE("eAtPdhErrorGeneratorErrorType", "select", "fbit-include(1), fbit-exclude(2), fbit-only(3)", "PDH error generator error type")
DEF_PTYPE("eAtEthPortErrorGeneratorErrorType", "select", "fcs(1), mac-pcs(2)", "ETH port error generator error type")
DEF_PTYPE("eAtCliVerboseMode", "select", "silent",
          "Verbose mode\n"
          "- silent: when this is specified, there would be no output with \"r2c\" mode\n")

TOP_SDKCOMMAND("show", "Show")
TOP_SDKCOMMAND("debug", "Debug CLIs")
TOP_SDKCOMMAND("debug module", "Module debug CLIs")

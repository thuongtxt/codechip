/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ClishAtPwPsn.c
 *
 * Created Date: Mar 18, 2013
 *
 * Description : PW PSN CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("pw mpls", "MPLS CLIs")
TOP_SDKCOMMAND("pw mpls outerlabel", "MPLS Outer label")
TOP_SDKCOMMAND("pw mef", "MEF CLI")
TOP_SDKCOMMAND("pw mef ecid", "Configure ECID")
TOP_SDKCOMMAND("pw mef vlan", "Configure VLAN")
TOP_SDKCOMMAND("pw udp", "UDP CLIs")
TOP_SDKCOMMAND("pw ipv4", "IPv4 CLIs")
TOP_SDKCOMMAND("pw ipv6", "IPv6 CLIs")

DEF_PTYPE("MplsLabel", "regexp", ".*", "MPLS label. Format <label>.<exp>.<ttl>")
DEF_PTYPE("PsnType", "regexp", ".*",
          "PSN Type. It can be layers of:\n"
          "+ mpls: MPLS\n"
          "+ mef : MEF\n"
          "+ udp : UDP\n"
          "+ ipv4: IPv4\n"
          "+ ipv6: IPv6\n"
          "Example: mpls.ipv4: MPLS over IPv4")

DEF_SDKCOMMAND(CliPwMplsInnerLabelSet,
               "pw mpls innerlabel",
               "Set MPLS inner label",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("label", "MplsLabel", ""),
               "${pwList} ${label}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwMplsExpectedLabelSet,
               "pw mpls expectedlabel",
               "Set MPLS expected label",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("expectedLabel", "STRING", ""),
               "${pwList} ${expectedLabel}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwMplsOuterLabelAdd,
               "pw mpls outerlabel add",
               "Add MPLS outer label",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("label", "MplsLabel", ""),
               "${pwList} ${label}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwMplsOuterLabelRemove,
               "pw mpls outerlabel remove",
               "Remove MPLS outer label",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("label", "MplsLabel", ""),
               "${pwList} ${label}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwMefTxEcIdSet,
               "pw mef ecid transmit",
               "Set TX ECID",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("ECID", "STRING", "ECID"),
               "${pwList} ${ECID}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwMefExpectedEcIdSet,
               "pw mef ecid expect",
               "Set expected ECID",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("ECID", "STRING", "ECID"),
               "${pwList} ${ECID}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwMefDestMacSet,
               "pw mef dmac",
               "Set dest MAC",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("dmac", "STRING", "Dest MAC"),
               "${pwList} ${dmac}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwMefSourceMacSet,
               "pw mef smac",
               "Set source MAC",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("smac", "STRING", "Source MAC"),
               "${pwList} ${smac}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwMefExpectedMacSet,
               "pw mef expectedmac",
               "Set expected MAC",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("expectedmac", "STRING", "Expected MAC"),
               "${pwList} ${expectedmac}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwMefVlanAdd,
               "pw mef vlan add",
               "add VLAN",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("vlanType", "STRING", "VLAN type")
               DEF_SDKPARAM("vlanTag", "VlanDesc", "VLAN tag"),
               "${pwList} ${vlanType} ${vlanTag}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwMefVlanRemove,
               "pw mef vlan remove",
               "remove VLAN",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("vlanType", "STRING", "VLAN type")
               DEF_SDKPARAM("vlanTag", "VlanDesc", "VLAN tag"),
               "${pwList} ${vlanType} ${vlanTag}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwUdpSourcePortSet,
               "pw udp source",
               "Set Source Port",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("srcPort", "STRING", "Source Port or unused"),
               "${pwList} ${srcPort}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwUdpExpectedPortSet,
               "pw udp expect",
               "Set expected Port",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("expectPort", "STRING", "Expected port"),
               "${pwList} ${expectPort}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwUdpDestPortSet,
               "pw udp dest",
               "Set Dest Port UDP Header",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("destport", "STRING", "Dest Port or unused"),
               "${pwList} ${destport}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwIPv4TosSet,
               "pw ipv4 tos",
               "Set Type of Service IPv4 Header",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("tos", "STRING", "Type of Service of IPv4 Header"),
               "${pwList} ${tos}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwIPv4TtlSet,
               "pw ipv4 ttl",
               "Set Time to live",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("ttl", "STRING", "Value Time to live of IPv4 Header"),
               "${pwList} ${ttl}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtPwIpV4PsnSourceAddressSet,
               "pw ipv4 source",
               "Set source IP address",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("ipAddress", "STRING", "Source IP Address"),
               "${pwList} ${ipAddress}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtPwIpV4PsnDestAddressSet,
               "pw ipv4 dest",
               "Set destination IP address",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("ipAddress", "STRING", "Destination IP Address"),
               "${pwList} ${ipAddress}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwIPv6TrafficClassSet,
               "pw ipv6 trafficclass",
               "Set Traffic Class",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("trafficClass", "STRING", "Traffic Class of IPv6 Header"),
               "${pwList} ${trafficClass}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwIPv6FlowLabelSet,
               "pw ipv6 flowlabel",
               "Set Flow Label",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("flowlabel", "STRING", "Flow Label of IPv6 Header"),
               "${pwList} ${flowlabel}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwIPv6HopLimitSet,
               "pw ipv6 hoplimit",
               "Set Hop Limit",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("hoplimit", "STRING", "Hop Limit of IPv6 Header"),
               "${pwList} ${hoplimit}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtPwIpV6PsnSourceAddressSet,
               "pw ipv6 source",
               "Set source IP address",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("ipAddress", "STRING", "Source IP Address"),
               "${pwList} ${ipAddress}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtPwIpV6PsnDestAddressSet,
               "pw ipv6 dest",
               "Set destination IP address",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("ipAddress", "STRING", "Destination IP Address"),
               "${pwList} ${ipAddress}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishPwPsnSet,
               "pw psn",
               "Set PSN Layer",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("psnType", "PsnType", "")
               DEF_SDKPARAM_OPTION("expectedLabel", "UINT", ""),
               "${pwList} ${psnType} ${expectedLabel}")
    {
    mAtCliCall();
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLISH
 *
 * File        : ClishAtPwHdlc.c
 *
 * Created Date: May 23, 2019
 *
 * Description : PW HDLC CLISHes
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("pw hdlc", "Pseudowire HDLC")

DEF_PTYPE("eAtPwHdlcPayloadType", "select", "full(0), pdu(1)", "Pseudowire HDLC Payload Type Mode")

DEF_SDKCOMMAND(CliPwHdlcPayloadTypeSet,
               "pw hdlc payloadtype",
               "Set PW HDLC payload type",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("payloadType", "eAtPwHdlcPayloadType", "Payload type"),
               "${pwList} ${payloadType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwHdlcGet,
               "show pw hdlc",
               "Show PW HDLC configuration",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

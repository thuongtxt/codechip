/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ClishAtPwPsn.c
 *
 * Created Date: Mar 18, 2013
 *
 * Description : PW PSN CLIs
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("pw backup", "MPLS CLIs")
TOP_SDKCOMMAND("pw backup mpls", "MPLS CLIs")
TOP_SDKCOMMAND("pw backup mpls outerlabel", "MPLS Outer label")
TOP_SDKCOMMAND("pw backup mef", "MEF CLI")
TOP_SDKCOMMAND("pw backup mef ecid", "Configure ECID")
TOP_SDKCOMMAND("pw backup mef vlan", "Configure VLAN")
TOP_SDKCOMMAND("pw backup udp", "UDP CLIs")
TOP_SDKCOMMAND("pw backup ipv4", "IPv4 CLIs")
TOP_SDKCOMMAND("pw backup ipv6", "IPv6 CLIs")

TOP_SDKCOMMAND("show pw backup", "Show PW backup CLIs")

DEF_SDKCOMMAND(CliPwBackupMplsInnerLabelSet,
               "pw backup mpls innerlabel",
               "Set MPLS inner label",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("label", "MplsLabel", ""),
               "${pwList} ${label}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwBackupMplsExpectedLabelSet,
               "pw backup mpls expectedlabel",
               "Set MPLS expected label",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("expectedLabel", "STRING", ""),
               "${pwList} ${expectedLabel}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwBackupMplsOuterLabelAdd,
               "pw backup mpls outerlabel add",
               "Add MPLS outer label",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("label", "MplsLabel", ""),
               "${pwList} ${label}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwBackupMplsOuterLabelRemove,
               "pw backup mpls outerlabel remove",
               "Remove MPLS outer label",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("label", "MplsLabel", ""),
               "${pwList} ${label}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwBackupMefTxEcIdSet,
               "pw backup mef ecid transmit",
               "Set TX ECID",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("ECID", "STRING", "ECID"),
               "${pwList} ${ECID}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwBackupMefExpectedEcIdSet,
               "pw backup mef ecid expect",
               "Set expected ECID",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("ECID", "STRING", "ECID"),
               "${pwList} ${ECID}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwBackupMefDestMacSet,
               "pw backup mef dmac",
               "Set dest MAC",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("dmac", "STRING", "Dest MAC"),
               "${pwList} ${dmac}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwBackupMefSourceMacSet,
               "pw backup mef smac",
               "Set source MAC",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("smac", "STRING", "Source MAC"),
               "${pwList} ${smac}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwBackupMefExpectedMacSet,
               "pw backup mef expectedmac",
               "Set expected MAC",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("expectedmac", "STRING", "Expected MAC"),
               "${pwList} ${expectedmac}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwBackupMefVlanAdd,
               "pw backup mef vlan add",
               "add VLAN",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("vlanType", "STRING", "VLAN type")
               DEF_SDKPARAM("vlanTag", "VlanDesc", "VLAN tag"),
               "${pwList} ${vlanType} ${vlanTag}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwBackupMefVlanRemove,
               "pw backup mef vlan remove",
               "remove VLAN",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("vlanType", "STRING", "VLAN type")
               DEF_SDKPARAM("vlanTag", "VlanDesc", "VLAN tag"),
               "${pwList} ${vlanType} ${vlanTag}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwBackupUdpSourcePortSet,
               "pw backup udp source",
               "Set Source Port",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("srcPort", "STRING", "Source Port or unused"),
               "${pwList} ${srcPort}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwBackupUdpExpectedPortSet,
               "pw backup udp expect",
               "Set expected Port",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("expectPort", "STRING", "Expected port"),
               "${pwList} ${expectPort}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwBackupUdpDestPortSet,
               "pw backup udp dest",
               "Set Dest Port UDP Header",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("destport", "STRING", "Dest Port or unused"),
               "${pwList} ${destport}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwBackupIPv4TosSet,
               "pw backup ipv4 tos",
               "Set Type of Service IPv4 Header",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("tos", "STRING", "Type of Service of IPv4 Header"),
               "${pwList} ${tos}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwBackupIPv4TtlSet,
               "pw backup ipv4 ttl",
               "Set Time to live",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("ttl", "STRING", "Value Time to live of IPv4 Header"),
               "${pwList} ${ttl}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwBackupIpV4PsnSourceAddressSet,
               "pw backup ipv4 source",
               "Set source IP address",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("ipAddress", "STRING", "Source IP Address"),
               "${pwList} ${ipAddress}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwBackupIpV4PsnDestAddressSet,
               "pw backup ipv4 dest",
               "Set destination IP address",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("ipAddress", "STRING", "Destination IP Address"),
               "${pwList} ${ipAddress}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwBackupIPv6TrafficClassSet,
               "pw backup ipv6 trafficclass",
               "Set Traffic Class",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("trafficClass", "STRING", "Traffic Class of IPv6 Header"),
               "${pwList} ${trafficClass}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwBackupIPv6FlowLabelSet,
               "pw backup ipv6 flowlabel",
               "Set Flow Label",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("flowlabel", "STRING", "Flow Label of IPv6 Header"),
               "${pwList} ${flowlabel}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwBackupIPv6HopLimitSet,
               "pw backup ipv6 hoplimit",
               "Set Hop Limit",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("hoplimit", "STRING", "Hop Limit of IPv6 Header"),
               "${pwList} ${hoplimit}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwBackupIpV6PsnSourceAddressSet,
               "pw backup ipv6 source",
               "Set source IP address",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("ipAddress", "STRING", "Source IP Address"),
               "${pwList} ${ipAddress}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwBackupIpV6PsnDestAddressSet,
               "pw backup ipv6 dest",
               "Set destination IP address",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("ipAddress", "STRING", "Destination IP Address"),
               "${pwList} ${ipAddress}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwBackup,
               "pw backup psn",
               "Set PSN Layer",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("psnType", "PsnType", "")
               DEF_SDKPARAM_OPTION("expectedLabel", "UINT", ""),
               "${pwList} ${psnType} ${expectedLabel}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwBackupPsnShow,
               "show pw backup psn",
               "Show PSN header information",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwBackupPsnIpv4Show,
               "show pw backup psn ipv4",
               "Show PSN IPv4 header information",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwBackupPsnIpv6Show,
               "show pw backup psn ipv6",
               "Show PSN IPv6 header information",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwBackupPsnMplsShow,
               "show pw backup psn mpls",
               "Show PSN MPLS header information",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwBackupPsnMefShow,
               "show pw backup psn mef",
               "Show PSN MEF header information",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwBackupPsnUdpShow,
               "show pw backup psn udp",
               "Show PSN UDP header information",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwBackupEthHeaderSet,
               "pw backup ethheader",
               "Set Ethernet Header Information",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("mac", "MacAddr", "DMAC Address")
               DEF_SDKPARAM("cVlan", "VlanDesc", "")
               DEF_SDKPARAM("sVlan", "VlanDesc", ""),
               "${pwList} ${mac} ${cVlan} ${sVlan}")
    {
    mAtCliCall();
    }


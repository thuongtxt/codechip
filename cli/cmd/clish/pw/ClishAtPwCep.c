/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ClishAtPwCep.c
 *
 * Created Date: Mar 18, 2013
 *
 * Description : CEP CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("pw cep", "Pseudowire CEP")
TOP_SDKCOMMAND("pw cep autonbit", "Pseudowire CEP")
TOP_SDKCOMMAND("pw cep autopbit", "Pseudowire CEP")
TOP_SDKCOMMAND("pw cep epar", "Pseudowire CEP EPAR")
TOP_SDKCOMMAND("show pw cep", "Show Pseudowire CEP")

DEF_PTYPE("AtPwCepEquipChannel", "regexp", ".*",
          "CEP equipped channel:\n"
          "- VC3    : vc3.<lineId>.<aug1Id>.<au3IdTug3Id>\n"
          "- VC-1x  : vc1x.<lineId>.<aug1Id>.<au3Tug3Id>.<tug2Id>.<vcxId>\n")

DEF_SDKCOMMAND(CliPwCEPAutoNBitEn,
               "pw cep autonbit enable",
               "Enable auto N-Bit for CEP Pseudowire",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwCEPAutoNBitDis,
               "pw cep autonbit disable",
               "Disable auto N-Bit for CEP Pseudowire",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwCEPAutoPBitEn,
               "pw cep autopbit enable",
               "Enable auto P-Bit for CEP Pseudowire",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwCEPAutoPBitDis,
               "pw cep autopbit disable",
               "Disable auto P-Bit for CEP Pseudowire",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwCEPEparEn,
               "pw cep epar enable",
               "Enable EPAR",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwCEPEparDis,
               "pw cep epar disable",
               "Disable EPAR",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

TOP_SDKCOMMAND("pw cep ebm", "CEP EBM CLIs")

DEF_SDKCOMMAND(cliPwCepEbmEn,
               "pw cep ebm enable",
               "Enable EBM field in control word and activate on the fly mode",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwCepEbmDis,
               "pw cep ebm disable",
               "Disable EBM field in control word and de-activate on the fly mode",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwCepEquip,
               "pw cep equip",
               "Equip VC to PW CEP fractional",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("physicalIntfList", "AtPwCepEquipChannel", ""),
               "${pwList} ${physicalIntfList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwCepUnEquip,
               "pw cep unequip",
               "Un-Equip VC to PW CEP fractional",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("physicalIntfList", "AtPwCepEquipChannel", ""),
               "${pwList} ${physicalIntfList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwCepEquippedChannelsGet,
               "show pw cep equippedchannels",
               "Show all equipped channels of PW CEP fractional",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

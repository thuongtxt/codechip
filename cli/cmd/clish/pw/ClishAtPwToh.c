/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ClishAtPwToh.c
 *
 * Created Date: Mar 13, 2014
 *
 * Description : TOH PW CLISH CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
DEF_PTYPE("TohBytes", "regexp", ".*",
          "\nList of TOH bytes.\n"
          "All of OH bytes are:\n"
          "+-----+-----+-----+\n"
          "| A1  | A2  | J0  |\n"
          "+-----+-----+-----+\n"
          "| B1  | E1  | F1  |\n"
          "+-----+-----+-----+\n"
          "| D1  | D2  | D3  |\n"
          "+-----+-----+-----+\n"
          "| B2  | K1  | K2  |\n"
          "+-----+-----+-----+\n"
          "| D4  | D5  | D6  |\n"
          "+-----+-----+-----+\n"
          "| D7  | D8  | D9  |\n"
          "+-----+-----+-----+\n"
          "| D10 | D11 | D12 |\n"
          "+-----+-----+-----+\n"
          "| S1  | M1  | E2  |\n"
          "+-----+-----+-----+\n")

TOP_SDKCOMMAND("pw toh", "TOH PW CLIs")
TOP_SDKCOMMAND("pw toh byte", "CLIs to control what TOH bytes need to be carried on PW")
TOP_SDKCOMMAND("show pw tohbyte", "Show TOH byte information")

DEF_SDKCOMMAND(cliPwTohByteEnable,
               "pw toh byte enable",
               "Add bytes to be carried on Pseudowire",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("sts1", "UINT", "STS-1 index")
               DEF_SDKPARAM("ohBytes", "TohBytes", ""),
               "${pwList} ${sts1} ${ohBytes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwTohByteDisable,
               "pw toh byte disable",
               "Un-equip TOH byte from Pseudowire",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("sts1", "UINT", "STS-1 index")
               DEF_SDKPARAM("ohBytes", "TohBytes", ""),
               "${pwList} ${sts1} ${ohBytes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwTohShow,
               "show pw toh",
               "Show TOH specific information",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwTohBytesTableShow,
               "show pw tohbyte table",
               "Show TOH byte in table format",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }


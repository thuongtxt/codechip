/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ClishAtPwApsGroup.c
 *
 * Created Date: May 09, 2015
 *
 * Description : PW CLIs
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("pw group aps", "Pseudowire group aps CLIs")

DEF_SDKCOMMAND(cliPwApsGroupEnable,
               "pw group aps enable",
               "Enable PW Groups",
               DEF_SDKPARAM("pwGroupList", "PwGroupIdList", ""),
               "${pwGroupList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwApsGroupDisable,
               "pw group aps disable",
               "Disable PW Groups",
               DEF_SDKPARAM("pwGroupList", "PwGroupIdList", ""),
               "${pwGroupList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwApsGroupCreate,
               "pw group aps create",
               "Create PW Groups",
               DEF_SDKPARAM("pwGroupList", "PwGroupIdList", ""),
               "${pwGroupList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwApsGroupDelete,
               "pw group aps delete",
               "Delete PW Groups",
               DEF_SDKPARAM("pwGroupList", "PwGroupIdList", ""),
               "${pwGroupList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwApsGroupPwAdd,
               "pw group aps add",
               "Add PW to a PW Groups",
               DEF_SDKPARAM("pwGroup", "UINT", "")
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwGroup} ${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwApsGroupPwRemove,
               "pw group aps remove",
               "Remove PW of a PW Groups",
               DEF_SDKPARAM("pwGroup", "UINT", "")
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwGroup} ${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwApsGroupShow,
               "show pw group aps",
               "Show PW Groups",
               DEF_SDKPARAM("pwGroupList", "PwGroupIdList", ""),
               "${pwGroupList}")
    {
    mAtCliCall();
    }

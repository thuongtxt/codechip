/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ClishAtPwApsGroup.c
 *
 * Created Date: May 09, 2015
 *
 * Description : PW CLIs
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("pw group", "Pseudowire Group CLIs")
TOP_SDKCOMMAND("show pw group", "Pseudowire group Show CLIs")

TOP_SDKCOMMAND("pw group hs", "Pseudowire HS Group CLIs")
TOP_SDKCOMMAND("pw group hs label", "Pseudowire HS Group Lable CLIs")

DEF_PTYPE("eGroupLabelSet", "select", "primary(1), backup(2)", "Pseudowire RTP Time Stamp Mode")

DEF_SDKCOMMAND(cliPwHsGroupEnable,
               "pw group hs enable",
               "Enable PW Groups",
               DEF_SDKPARAM("pwGroupList", "PwGroupIdList", ""),
               "${pwGroupList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwHsGroupDisable,
               "pw group hs disable",
               "Disable PW Groups",
               DEF_SDKPARAM("pwGroupList", "PwGroupIdList", ""),
               "${pwGroupList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwHsGroupCreate,
               "pw group hs create",
               "Create PW Groups",
               DEF_SDKPARAM("pwGroupList", "PwGroupIdList", ""),
               "${pwGroupList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwHsGroupDelete,
               "pw group hs delete",
               "Delete PW Groups",
               DEF_SDKPARAM("pwGroupList", "PwGroupIdList", ""),
               "${pwGroupList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwHsGroupPwAdd,
               "pw group hs add",
               "Add PW to a PW Groups",
               DEF_SDKPARAM("pwGroup", "UINT", "")
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwGroup} ${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwHsGroupPwRemove,
               "pw group hs remove",
               "Remove PW of a PW Groups",
               DEF_SDKPARAM("pwGroup", "UINT", "")
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwGroup} ${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwHsGroupShow,
               "show pw group hs",
               "Show PW Groups",
               DEF_SDKPARAM("pwGroupList", "PwGroupIdList", ""),
               "${pwGroupList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwHsGroupLabelTx,
               "pw group hs label tx",
               "Set Tx Lable of a PW Groups",
               DEF_SDKPARAM("pwGroup", "PwGroupIdList", "")
               DEF_SDKPARAM("label", "eGroupLabelSet", ""),
               "${pwGroup} ${label}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwHsGroupLabelRx,
               "pw group hs label rx",
               "Set Rx Lable of a PW Groups",
               DEF_SDKPARAM("pwGroup", "PwGroupIdList", "")
               DEF_SDKPARAM("label", "eGroupLabelSet", ""),
               "${pwGroup} ${label}")
    {
    mAtCliCall();
    }

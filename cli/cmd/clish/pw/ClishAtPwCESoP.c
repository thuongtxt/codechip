/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ClishAtPwCESoP.c
 *
 * Created Date: Mar 18, 2013
 *
 * Description : CESoP CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("pw autombit", "Pseudowire Auto MBit")
TOP_SDKCOMMAND("pw cesop", "Pseudowire CESoP")
TOP_SDKCOMMAND("pw cesop autotxmbit", "Pseudowire CESoP")
TOP_SDKCOMMAND("pw cesop autorxmbit", "Pseudowire CESoP")
TOP_SDKCOMMAND("pw cesop cas", "Pseudowire CESoP CAS")
TOP_SDKCOMMAND("pw cesop cas idle", "Pseudowire CESoP CAS")
TOP_SDKCOMMAND("pw cesop cas autoidle", "Pseudowire CESoP CAS")
TOP_SDKCOMMAND("pw cesop cas rx", "Pseudowire CESoP CAS RX")
TOP_SDKCOMMAND("show pw cesop", "Pseudowire CESoP CAS")

DEF_PTYPE("eAtPwCESoPCasMode", "select", "e1-cas(1), ds1-cas(2)",
          "CESoP CAS mode.\n"
          "+ e1-cas: CESoP CAS E1 mode\n"
          "+ ds1-cas: CESoP CAS DS1 mode\n")

DEF_SDKCOMMAND(CliPwAutoMBitEn,
               "pw autombit enable",
               "Enable auto M-Bit at both directions",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwAutoMBitDis,
               "pw autombit disable",
               "Disable auto M-Bit at both directions",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwCESoPAutoTxMBitEn,
               "pw cesop autotxmbit enable",
               "Enable auto transmitting M-bit when DS1/E1 has RAI/RDI",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwCESoPAutoTxMBitDis,
               "pw cesop autotxmbit disable",
               "Disable auto transmitting M-bit when DS1/E1 has RAI/RDI",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwCESoPAutoRxMBitEn,
               "pw cesop autorxmbit enable",
               "Enable auto transmitting RDI upon receiving M-Bit",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwCESoPAutoRxMBitDis,
               "pw cesop autorxmbit disable",
               "Disable auto transmitting RDI upon receiving M-Bit",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwCESoPCasIdleCode1Set,
               "pw cesop cas idle code1",
               "Set abcd1 for CAS in DS0 trunk condition",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("idle", "STRING", "idle code in hex"),
               "${pwList} ${idle}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwCESoPCasIdleCode2Set,
               "pw cesop cas idle code2",
               "Set abcd2 for CAS in DS0 trunk condition",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("idle", "STRING", "idle code in hex"),
               "${pwList} ${idle}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwCESoPCasAutoIdleEn,
               "pw cesop cas autoidle enable",
               "Enable auto cas abcd1 and cas abcd2 in DS0 trunk condition",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwCESoPCasAutoIdleDis,
               "pw cesop cas autoidle disable",
               "Disable auto cas abcd1 and cas abcd2 in DS0 trunk condition",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwCESoPCasShow,
               "show pw cesop cas",
               "Show pw cesop cas information",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwCESoPRxCasMode,
               "pw cesop cas rx mode",
               "Set CESoP CAS reception mode",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("casMode", "eAtPwCESoPCasMode", ""),
               "${pwList} ${casMode}")
    {
    mAtCliCall();
    }


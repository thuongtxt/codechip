/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ClishAtModulePw.c
 *
 * Created Date: Nov 21, 2012
 *
 * Description : PW CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/* Top commands */
TOP_SDKCOMMAND("pw", "Pseudowire")
TOP_SDKCOMMAND("pw interrupt", "Pseudowire interrupt handle")
TOP_SDKCOMMAND("pw jittercenter", "Pseudowire jitter buffer centering")
TOP_SDKCOMMAND("pw create", "Create PW")
TOP_SDKCOMMAND("pw dcr", "CLIs to control DCR")

DEF_PTYPE("PwIdList", "regexp", ".*", "Pseudowire ID List")
DEF_PTYPE("PwList", "regexp", ".*",
          "List of Pseudowire\n"
          "- TDM PW   : Flat Ids (SAToP, CEP, CESoP)\n"
          "- HDLC PW  : hdlc.<hdlcId>\n"
          "- PPP PW   : ppp.<linkId>\n"
          "- DCC PW   : dcc.<sdhLineId>.<layer>\n"
          "- MLPPP PW : mlppp.<linkId>\n"
          "- FR VC PW : fr.<linkId>.<dlci>\n"
          "- MFR VC PW: mfr.<linkId>.<dlci>\n")
DEF_PTYPE("PwGroupIdList", "regexp", ".*", "Pseudowire Group ID List")
DEF_PTYPE("eAtAtmPwType", "select", "vcc1to1(0), vpc1to1(1), nto1(2)", "Pseudowire ATM Type")
DEF_PTYPE("eAtPwDcrClockSource", "select", "prc(1), system(2), ext1(3), ext2(4)",
          "Clock source\r\n"
          "+ prc    : PRC Reference Clock\r\n"
          "+ system : System Clock\r\n"
          "+ ext1   : External Reference Clock 1\r\n"
          "+ ext2   :External Reference Clock 2\r\n")

DEF_PTYPE("eAtPwCESoPMode", "select", "basic(1), cas(2)",
          "CESoP mode.\n"
          "+ basic: CESoP basic\n"
          "+ cas: CESoP with CAS\n")

DEF_PTYPE("eAtPwCepMode", "select", "basic(1), fractional(2)",
          "CEP mode.\n"
          "+ basic: CEP basic\n"
          "+ fractional: CEP fractional\n")

DEF_SDKCOMMAND(CliPwCepCreate,
               "pw create cep",
               "Create CEP Pseudowire",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("mode", "eAtPwCepMode", ""),
               "${pwList} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwSatopCreate,
               "pw create satop",
               "Create SAToP Pseudowire",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwCesopCreate,
               "pw create cesop",
               "Create CESoP Pseudowire",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("mode", "eAtPwCESoPMode", ""),
               "${pwList} {mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwHdlcCreate,
               "pw create hdlc",
               "Create HDLC Pseudowire",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwPppCreate,
               "pw create ppp",
               "Create PPP Pseudowire",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwMpCreate,
               "pw create mlppp",
               "Create MLPPP Pseudowire",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwAtmCreate,
               "pw create atm",
               "Create ATM Pseudowire",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("atmPwType", "eAtAtmPwType", ""),
               "${pwList} ${atmPwType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwFrVirtualCircuitCreate,
               "pw create frvc",
               "Create Link/Multilink Frame relay Virtual Circuit pseudowire",
               DEF_SDKPARAM("pwList", "PwList", "FR VC PW\r\n\n"),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwDelete,
               "pw delete",
               "Delete Pseudowire",
               DEF_SDKPARAM("pwList", "PwList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModulePwDebug,
               "debug module pw",
               "Show debug information of PW module\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModulePwDcrClockSourceSet,
               "pw dcr clocksource",
               "Set DCR clock source\n",
               DEF_SDKPARAM("clockSource", "eAtPwDcrClockSource", ""),
               "${clockSource}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModulePwDcrClockFrequencySet,
               "pw dcr clockfrequency",
               "Set DCR clock frequency\n",
               DEF_SDKPARAM("frequencyInKHz", "STRING", "Clock frequency in KHz"),
               "${frequencyInKHz}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModulePwDcrRtpTimestampFrequencySet,
               "pw dcr rtpfrequency",
               "Set RTP Time-stamp frequency\n",
               DEF_SDKPARAM("frequencyInKHz", "STRING", "RTP timestamp frequency in KHz"),
               "${frequencyInKHz}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModulePwCepDcrRtpTimestampFrequencySet,
               "pw dcr rtpfrequency cep",
               "Set RTP Time-stamp frequency for CEP\n",
               DEF_SDKPARAM("frequencyInKHz", "STRING", "RTP timestamp frequency in KHz"),
               "${frequencyInKHz}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModulePwCesDcrRtpTimestampFrequencySet,
               "pw dcr rtpfrequency ces",
               "Set RTP Time-stamp frequency for CEP\n",
               DEF_SDKPARAM("frequencyInKHz", "STRING", "RTP timestamp frequency in KHz"),
               "${frequencyInKHz}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModulePwShow,
               "show module pw",
               "Show PW module information\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModulePwLock,
               "lock module pw",
               "Lock\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModulePwUnLock,
               "unlock module pw",
               "Unlock\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModulePwIdleCodeSet,
               "pw idlecode",
               "Set Idle code\n",
               DEF_SDKPARAM_OPTION("pwList", "PwIdList", "")
               DEF_SDKPARAM_OPTION("idleCode", "STRING", "Idle code"),
               "${pwList} ${idleCode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwTohCreate,
               "pw create toh",
               "Create TOH Pseudowire",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwInterruptEnable,
               "pw interrupt enable",
               "Enable interrupt",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwInterruptDisable,
               "pw interrupt disable",
               "Disable interrupt",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwJitterCenteringEnable,
               "pw jittercenter enable",
               "Enable jitter buffer centering",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwJitterCenteringDisable,
               "pw jittercenter disable",
               "Disable jitter buffer centering",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ClishAtPw.c
 *
 * Created Date: Mar 18, 2013
 *
 * Description : PW CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define PwDefType "(lbit|rbit|mbit|lops|overrun|underrun|missing|excessive|stray|malformed|misconnection)"
#define PwDefMask "((("PwDefType"[\\|])+"PwDefType")|"PwDefType")"

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("pw reorder", "Pseudowire reorder")
TOP_SDKCOMMAND("pw rtp", "Pseudowire RTP")
TOP_SDKCOMMAND("pw controlword", "Pseudowire controlword")
TOP_SDKCOMMAND("pw circuit", "Pseudowire circuit")
TOP_SDKCOMMAND("pw autotxlbit", "Pseudowire Auto Tx LBit")
TOP_SDKCOMMAND("pw autorxlbit", "Pseudowire Auto Rx LBit")
TOP_SDKCOMMAND("pw autorbit", "Pseudowire Auto RBit")
TOP_SDKCOMMAND("pw threshold", "Pseudowire thresholds")
TOP_SDKCOMMAND("pw suppress", "Pseudowire suppression")
TOP_SDKCOMMAND("pw force", "Pseudowire force")
TOP_SDKCOMMAND("pw log", "Pseudowire logging CLIs")
TOP_SDKCOMMAND("pw alarm", "Pseudowire alarm handle")
TOP_SDKCOMMAND("pw vlan", "Pseudowire VLAN handle")
TOP_SDKCOMMAND("show pw listen", "Pseudowire listened alarm handle")
TOP_SDKCOMMAND("pw lops", "Pseudowire LOPS state handle")
TOP_SDKCOMMAND("show pw vlan", "Show Pseudowire Vlan")
TOP_SDKCOMMAND("pw jitterbuffer watermark", "Jitter buffer watermark")
TOP_SDKCOMMAND("pw cvlan", "Pseudowire cvlan")
TOP_SDKCOMMAND("pw svlan", "Pseudowire svlan")

DEF_PTYPE("eAtPwRtpTimeStampMode", "select", "absolute(0), differential(1)", "Pseudowire RTP Time Stamp Mode")
DEF_PTYPE("eAtPwCwSequenceMode", "select", "wrapzero(0), skipzero(1), dis(2)", "Pseudowire Control Word Sequence Mode")
DEF_PTYPE("eAtPwCwLengthMode", "select", "fullpkt(0), payload(1)", "Pseudowire Control-Word Length Mode")
DEF_PTYPE("eAtPwCwReplaceMode", "select", "goodpacket(0), ais(1), idlecode(2)", "Pseudowire Control-Word Replace Mode")
DEF_PTYPE("PwEthPort", "regexp", ".*", "Ethernet port: 1..N or none")
DEF_PTYPE("PwEthFlow", "regexp", ".*", "Ethernet flow: 1..N or none")
DEF_PTYPE("eAtPwAlarmType", "select", "lbit(0), rbit(1), mbit(2), lops(3), overrun(4), underrun(5), missing(6), excessive(7), stray(8), malformed(9), misconnection(10), all(11)", "Pseudowire alarm Type")

DEF_PTYPE("PwDefMask", "regexp", PwDefMask,
          "\nPW defect masks:\n"
          "- lbit           : L-bit\n"
          "- rbit           : R-bit\n"
          "- mbit           : M-bit\n"
          "- lops           : Loss of Packet Synchronization (CEP)/ Loss of Packet (CES)\n"
          "- overrun        : Jitter Buffer Overrun\n"
          "- underrun       : Jitter Buffer Underrun\n"
          "- missing        : Missing Packets (CEP)\n"
          "- excessive      : Excessive Packet Loss Rate\n"
          "- stray          : Stray packets\n"
          "- malformed      : Malformed packets\n"
          "- misconnection  : Misconnection\n"
          "- all            : All defects\n"
          "Note, these masks can be ORed, example rbit|lbit\n")


DEF_SDKCOMMAND(cliPwEnable,
               "pw enable",
               "Enable PWs",
               DEF_SDKPARAM("pwList", "PwList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwDisable,
               "pw disable",
               "Disable PWs",
               DEF_SDKPARAM("pwList", "PwList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwLopSetThresSet,
               "pw threshold lopset",
               "Set threshold to enter LOP state",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("threshold", "UINT", "Threshold to enter LOP state. Unit is packet"),
               "${pwList} ${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwLopClearThresSet,
               "pw threshold lopclear",
               "Set threshold to exit LOP state",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("threshold", "UINT", "Threshold to exit LOP state. Unit is packet"),
               "${pwList} ${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwPayloadSizeSet,
               "pw payloadsize",
               "Set payload size",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("payloadSize", "UINT", "Payload Size.\n"
                                                   "+ SAToP/CEP: Payload is in byte unit\n"
                                                   "+ CESoP    : Payload is in number of frames per packet unit"),
               "${pwList} ${payloadSize}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwPrioritySet,
               "pw priority",
               "Set priority",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("priority", "UINT", "Priority"),
               "${pwList} ${priority}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwReorderEn,
               "pw reorder enable",
               "Enable reorder",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwReorderDis,
               "pw reorder disable",
               "Disable reorder",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwJitterBufferSet,
               "pw jitterbuffer",
               "Set jitter buffer size",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("size_us", "STRING", "Buffer size in microseconds"),
               "${pwList} ${size_us}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwJitterDelaySet,
               "pw jitterdelay",
               "Set jitter delay",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("delay_us", "STRING", "Jitter Delay in microseconds"),
               "${pwList} ${delay_us}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwJitterBufferInPacketUnitSet,
               "pw jitterbuffer packet",
               "Set jitter buffer size in packet unit",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("size_pkt", "STRING", "Buffer size in packet unit"),
               "${pwList} ${size_pkt}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwJitterDelayInPktUnitSet,
               "pw jitterdelay packet",
               "Set jitter delay in packet unit",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("delay_pkt", "STRING", "Jitter Delay in packet unit"),
               "${pwList} ${delay_pkt}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwRtpEn,
               "pw rtp enable",
               "Enable RTP",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwRtpDis,
               "pw rtp disable",
               "Disable RTP",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwRtpPayloadTypeSet,
               "pw rtp payloadtype",
               "Set RTP payload type",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("payloadType", "STRING", "Payload Type value"),
               "${pwList} ${payloadType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwRtpTxPayloadTypeSet,
               "pw rtp payloadtype transmit",
               "Set TX RTP Payload Type",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("payloadType", "STRING", "Payload Type value"),
               "${pwList} ${payloadType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwRtpExpectedPayloadTypeSet,
               "pw rtp payloadtype expect",
               "Set expected RTP Payload Type",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("payloadType", "STRING", "Payload Type value"),
               "${pwList} ${payloadType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwRtpPayloadTypeCompare,
               "pw rtp payloadtype compare",
               "Enable/disable comparing RTP payload type",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("enableCompare", "eBool", ""),
               "${pwList} ${enableCompare}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwRtpSsrcSet,
               "pw rtp ssrc",
               "Set RTP SSRC",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("ssrc", "STRING", "SSRC value"),
               "${pwList} ${ssrc}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwRtpTxSsrcSet,
               "pw rtp ssrc transmit",
               "Set TX RTP SSRC",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("ssrc", "STRING", "SSRC value"),
               "${pwList} ${ssrc}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwRtpExpectedSsrcSet,
               "pw rtp ssrc expect",
               "Set expected RTP SSRC",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("ssrc", "STRING", "SSRC value"),
               "${pwList} ${ssrc}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwRtpSsrcCompare,
               "pw rtp ssrc compare",
               "Enable/disable comparing RTP SSRC",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("enableCompare", "eBool", ""),
               "${pwList} ${enableCompare}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwRtpTimeStampSet,
               "pw rtp timestamp",
               "Set RTP timestamp mode",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("timeStamp", "eAtPwRtpTimeStampMode", ""),
               "${pwList} ${timeStamp}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwSupressEn,
               "pw suppress enable",
               "Enable suppression",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwSupressDis,
               "pw suppress disable",
               "Disable suppression",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwControlWordEn,
               "pw controlword enable",
               "Enable Control Word",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwControlWordDis,
               "pw controlword disable",
               "Disable Control Word",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwAutoTxLbitEn,
               "pw autotxlbit enable",
               "Enable auto TX L-Bit",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwAutoTxLbitDis,
               "pw autotxlbit disable",
               "Disable auto TX L-Bit",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwAutoRxLbitEn,
               "pw autorxlbit enable",
               "Enable auto RX L-Bit",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwAutoRxLbitDis,
               "pw autorxlbit disable",
               "Disable auto RX L-Bit",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwAutoRBitEn,
               "pw autorbit enable",
               "Enable auto R-Bit",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwAutoRBitDis,
               "pw autorbit disable",
               "Disable auto R-Bit",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwCwSequenceSet,
               "pw controlword sequence",
               "Set Control Word sequence mode",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("seqmd", "eAtPwCwSequenceMode", ""),
               "${pwList} ${seqmd}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwCwLengthSet,
               "pw controlword length",
               "Set Control Word length mode",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("lengthmd", "eAtPwCwLengthMode", ""),
               "${pwList} ${lengthmd}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwCwReplaceModeSet,
               "pw controlword pktreplace",
               "Set Control Word packet replacing mode",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("repmd", "eAtPwCwReplaceMode", ""),
               "${pwList} ${repmd}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwLopsReplaceModeSet,
               "pw lops pktreplace",
               "Set packet replacing mode when PW is in LOPS",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("repmd", "eAtPwCwReplaceMode", ""),
               "${pwList} ${repmd}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwEthPortSet,
               "pw ethport",
               "Set Ethernet Port",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("portId", "PwEthPort", ""),
               "${pwList} ${portId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishPwEthHeaderSet,
               "pw ethheader",
               "Set Ethernet Header Information",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("mac", "MacAddr", "DMAC Address")
               DEF_SDKPARAM("cVlan", "VlanDesc", "")
               DEF_SDKPARAM("sVlan", "VlanDesc", ""),
               "${pwList} ${mac} ${cVlan} ${sVlan}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwCircuitBind,
               "pw circuit bind",
               "Bind Circuit",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("channelId", "PhyIntfList", ""),
               "${pwList} ${channelId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwCircuitUnBind,
               "pw circuit unbind",
               "Unbind Circuit",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwGet,
               "show pw",
               "Get Pseudowire configuration",
               DEF_SDKPARAM("pwList", "PwList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwPsnShow,
               "show pw psn",
               "Show PSN header information",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwRtpGet,
               "show pw rtp",
               "Get Pseudowire RTP configuration",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwControlWordGet,
               "show pw controlword",
               "Get Pseudowire control word configuration",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwCounterGet,
               "show pw counters",
               "Get Pseudowire counters",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
			   DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${pwList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwDebug,
               "debug pw",
               "Show debug information of PWs",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwAlarmShow,
               "show pw alarm",
               "Show PW alarms",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwListenedAlarmShow,
               "show pw listen alarm",
               "Show listened PW alarms",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM_OPTION("readingMode", "HistoryReadingMode", ""),
               "${pwList} ${readingMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwDefectShow,
               "show pw defect",
               "Show PW defect",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDefectInterruptShow,
               "show pw defect interrupt",
               "Show PW defect interrupt status",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
			   DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${pwList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAlarmInterruptShow,
               "show pw interrupt",
               "Show PW interrupt status",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
			   DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${pwList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwPsnIpv4Show,
               "show pw psn ipv4",
               "Show PSN IPv4 header information",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwPsnIpv6Show,
               "show pw psn ipv6",
               "Show PSN IPv6 header information",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwPsnMplsShow,
               "show pw psn mpls",
               "Show PSN MPLS header information",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwPsnMefShow,
               "show pw psn mef",
               "Show PSN MEF header information",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwPsnUdpShow,
               "show pw psn udp",
               "Show PSN UDP header information",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwJitterBufferShow,
               "show pw jitterbuffer",
               "Show PW jitter buffer information",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

/* TODO: This CLI is deprecated by "show pw jitterbuffer" but autotest module 
 * that is still using this and this declaration should be removed after 
 * autotest module is updated. */
DEF_SDKCOMMAND(cliPwJitterShow,
               "show pw jitter",
               "Show PW jitter buffer information",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwThresholdShow,
               "show pw threshold",
               "Show PW threshold information",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwConstrainGet,
               "show pw constrain",
               "Show PW constrains",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwJitterConstrainCalculate,
               "show pw constrain jitterbuffer",
               "Show PW jitter buffer constrain for a given payload size",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("payloadSize", "STRING", "Payload size in bytes (CEP, SAToP) or in frames (CESoP)"),
               "${pwList} ${payloadSize}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwPayloadSizeConstrainCalculate,
               "show pw constrain payload",
               "Show PW payload size constrain for a given jitter buffer size",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("jitterBufferSize", "STRING", "Jitter buffer size in microseconds"),
               "${pwList} ${jitterBufferSize}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwJitterDelayConstrainCalculate,
               "show pw constrain jitterdelay",
               "Show PW jitter delay constrain for a given jitter buffer size",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("jitterBufferSize", "STRING", "Jitter buffer size in microseconds"),
               "${pwList} ${jitterBufferSize}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPwJitterBufferAndPayloadSizeSet,
               "pw jitterbuffer_payload",
               "Set jitter buffer parameters in microsecond unit and payload at a time",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("jitterBufferSize_us", "STRING", "Jitter buffer size in microseconds")
               DEF_SDKPARAM("jitterDelay_us", "STRING", "Jitter delay in microseconds")
               DEF_SDKPARAM("payloadSize", "UINT", "Payload Size.\n"
                                                   "+ SAToP/CEP: Payload is in byte unit\n"
                                                   "+ CESoP    : Payload is in number of frames per packet unit"),
               "${pwList} ${jitterBufferSize_us} ${jitterDelay_us} ${payloadSize}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPwJitterBufferInPacketAndPayloadSizeSet,
               "pw jitterbuffer_payload packet",
               "Set jitter buffer parameters in packet unit and payload at a time",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("jitterBufferSize_pkt", "STRING", "Jitter buffer size in packets")
               DEF_SDKPARAM("jitterDelay_pkt", "STRING", "Jitter delay in packets")
               DEF_SDKPARAM("payloadSize", "UINT", "Payload Size.\n"
                                                   "+ SAToP/CEP: Payload is in byte unit\n"
                                                   "+ CESoP    : Payload is in number of frames per packet unit"),
               "${pwList} ${jitterBufferSize_pkt} ${jitterDelay_pkt} ${payloadSize}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPwTxAlarmForce,
               "pw force alarm",
               "Force TX alarm",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("alarmType", "eAtPwAlarmType", "")
               DEF_SDKPARAM("forceEnable", "eBool", ""),
               "${pwList} ${alarmType} {forceEnable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwForcedAlarmGet,
               "show pw forcedalarms",
               "Show forced alarms",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwMissingPacketThresSet,
               "pw threshold missingpacket",
               "Set Threshold to declare missing packet defect",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("threshold", "UINT", "Threshold to declare missing packet defect. Unit is packet"),
               "${pwList} ${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwExcessiveLossRateThresSet,
           "pw threshold excessivelossrate",
           "Set Threshold to declare excessive packet loss rate defect",
           DEF_SDKPARAM("pwList", "PwIdList", "")
           DEF_SDKPARAM("threshold", "UINT", "Threshold to excessive packet loss rate defect. Unit is packet")
           DEF_SDKPARAM("timeinsec", "UINT", "Time window in second"),
           "${pwList} ${threshold} ${timeinsec}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwStrayPacketThresSet,
               "pw threshold straypacket",
               "Set Threshold to declare stray packet defect",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("threshold", "UINT", "Threshold to declare stray packet defect. Unit is packet"),
               "${pwList} ${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwMalformedPacketThresSet,
               "pw threshold malformedpacket",
               "Set Threshold to declare malformed packet defect",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("threshold", "UINT", "Threshold to declare malformed packet defect. Unit is packet"),
               "${pwList} ${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwRemotePacketLossThresSet,
               "pw threshold remotepacketloss",
               "Set Threshold to declare remote packet loss defect",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("threshold", "UINT", "Threshold to declare remote packet loss defect. Unit is packet"),
               "${pwList} ${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwBufferUnderrunThresSet,
               "pw threshold bufferunderrun",
               "Set Threshold to declare buffer underrun defect",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("threshold", "UINT", "Threshold to declare buffer underrun defect. Unit is event"),
               "${pwList} ${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwBufferOverrunThresSet,
               "pw threshold bufferoverrun",
               "Set Threshold to declare buffer overrun defect",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("threshold", "UINT", "Threshold to declare buffer overrun defect. Unit is event"),
               "${pwList} ${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwMisConnectionThresSet,
               "pw threshold misconnection",
               "Set Threshold to declare mis-connection defect",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("threshold", "UINT", "Threshold to declare mis-connection defect. Unit is packet"),
               "${pwList} ${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwLogShow,
               "show pw log",
               "Show logged information of PWs",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwLogEnable,
               "pw log enable",
               "Enable logging for PWs",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }


DEF_SDKCOMMAND(CliPwLogDisable,
               "pw log disable",
               "Disable logging for PWs",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwAlarmCapture,
               "pw alarm capture",
               "Enable/disable alarm capturing",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("enable", "eBool", "Enable/disable alarm capturing"),
               "${pwList} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwInterruptMaskSet,
               "pw interruptmask",
               "Set PW channel interrupt mask",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("intrMask", "PwDefMask", "")
               DEF_SDKPARAM("enable", "eBool", "Set/clear mask"),
               "${pwList} ${intrMask} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwEthPortQueueSet,
               "pw ethport queue",
               "Set Ethernet Port queue",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("queueId", "UINT", "Ethernet port queue ID"),
               "${pwList} ${queueId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwAutotestAlarmCaptureGet,
               "show pw alarm capture",
               "Show captured alarms",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", "Read to clear without display table"),
               "${pwList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishPwEthSourceMacSet,
               "pw sourcemac",
               "Set Ethernet Header Source MAC",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("mac", "MacAddr", "SMAC Address"),
               "${pwList} ${mac}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtPwJitterBufferCenter,
               "pw jitterbuffer center",
               "Center jitter buffer",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtPwExpectVlanSet,
               "pw vlan expect",
               "Set expected VLAN for PW",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("cVlan", "VlanDesc", "")
               DEF_SDKPARAM("sVlan", "VlanDesc", ""),
               "${pwList} ${cVlan} ${sVlan}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtPwCvlanTpidSet,
               "pw cvlan tpid",
               "Set pw cvlan tpid",
               DEF_SDKPARAM("pwId", "PwIdList", "")
               DEF_SDKPARAM("tpId", "STRING", "Vlan Tpid"),
               "${pwId} ${tpid}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtPwSvlanTpidSet,
               "pw svlan tpid",
               "Set pw svlan tpid",
               DEF_SDKPARAM("pwId", "PwIdList", "")
               DEF_SDKPARAM("tpId", "STRING", "Vlan Tpid"),
               "${pwId} ${tpid}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtPwExpectVlanShow,
               "show pw vlan expect",
               "Show expected VLAN for PW",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtPwJitterBufferWatermarkReset,
               "pw jitterbuffer watermark reset",
               "Reset jitter buffer watermark",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPwDebugTableShow,
               "debug pw table",
               "Show debug information of PWs in table format",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwEthPortFlow,
               "pw ethflow",
               "Set Ethernet Flow",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("flowId", "PwEthFlow", ""),
               "${pwList} ${flowId}")
    {
    mAtCliCall();
    }


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive
 * Technologies. The use, copying, transfer or disclosure of such information is
 * prohibited except by express written agreement with Arrive Technologies.
 *
 * Module     : REG COMMAND
 *
 * File       : ClishRegCmd.c
 *
 * CreatedDate: Jul 03, 2015
 *
 * Description: This file contains implementation for AtCE08SwfModule.
 *
 * Notes      : TODO Notes
 *----------------------------------------------------------------------------*/
#include "../AtClish.h"

/*--------------------------- Include files ----------------------------------*/

/*--------------------------- Local variables --------------------------------*/


/*--------------------------- Define -----------------------------------------*/
DEF_PTYPE("AtRegBlock", "select", "glb(0) glbr(1) ocn(2) pdh(3) pdhdlk(4) mapho(5) map(6) cdrho(7) cdr(8) pla(9) pda(10) cla(11) pwe(12) pmc(13) eth(14) pm(16)", "RTL block select")
DEF_PTYPE("AtRegAddValue", "regexp", "(([0]{0,1})([xX]{0,1})([0-9a-fA-F]{1,8}))", "Hex number")
DEF_PTYPE("AtRegValue", "regexp", "(([0]{0,1})([xX]{0,1})([0-9a-fA-F]{1,8}))", "Value, (0x123 -> hex, 123 -> dec)")
DEF_PTYPE("AtRegValueOpt", "regexp", "(([0]{0,1})([xX]{0,1})([0-9a-fA-F]{1,8}))|([*])", "Value, (0x123 -> hex, 123 -> dec, or * -> dont care)")
DEF_PTYPE("AtRegStringValue", "regexp", "(.)|([*])", "String")
DEF_PTYPE("AtRegSetMode", "select", "field raw fill_field fill_raw", "Conf register mode")
DEF_PTYPE("AtRegGetMode", "select", "field raw", "Show register mode")
DEF_PTYPE("AtRegStrValue", "regexp", "(([*]{1,2})|([0]{0,1})([xX]{0,1})([0-9a-fA-F]{1,40}))", "String number")

DEF_SDKCOMMAND(ClishCmdAtRegRead,
				"show reg",
				"Show information of register",
				DEF_SDKPARAM("blk", "AtRegBlock", "RTL block")
				DEF_SDKPARAM_OPTION("addr_s", "AtRegAddValue", "The address start")
				DEF_SDKPARAM_OPTION("addr_e", "AtRegAddValue", "The address end (end - start <= 20)")
				DEF_SDKPARAM_OPTION("flag", "AtRegGetMode", "Choose Show Reg Mode - default: show field"),
				"${blk} ${addr_s} ${addr_e} ${flag}")
{
    mAtCliCall();
}

DEF_SDKCOMMAND(ClishCmdAtRegReadFromValue,
                "show reg from_val",
                "Show information of register",
                DEF_SDKPARAM("blk", "AtRegBlock", "RTL block")
                DEF_SDKPARAM("addr", "AtRegAddValue", "The address")
                DEF_SDKPARAM("value", "AtRegStrValue", "The value"),
                "${blk} ${addr} ${value}")
{
    mAtCliCall();
}

DEF_SDKCOMMAND(ClishCmdAtRegWrite,
				"reg",
				"Conf information of register",
				DEF_SDKPARAM("blk", "AtRegBlock", "RTL block")
				DEF_SDKPARAM("addr_s", "AtRegAddValue", "The address start")
                DEF_SDKPARAM_OPTION("value_00", "AtRegStringValue", "Value pos_00 'Field=Value' or Raw Value  String(Is address end in 'fill' mode)")
				DEF_SDKPARAM_OPTION("value_01", "AtRegStringValue", "Value pos_01 'Field=Value' or Raw Value")
				DEF_SDKPARAM_OPTION("value_02", "AtRegStringValue", "Value pos_02 'Field=Value' or Raw Value")
				DEF_SDKPARAM_OPTION("value_03", "AtRegStringValue", "Value pos_03 'Field=Value' or Raw Value")
				DEF_SDKPARAM_OPTION("value_04", "AtRegStringValue", "Value pos_04 'Field=Value' or Raw Value")
				DEF_SDKPARAM_OPTION("value_05", "AtRegStringValue", "Value pos_05 'Field=Value' or Raw Value")
				DEF_SDKPARAM_OPTION("value_06", "AtRegStringValue", "Value pos_06 'Field=Value' or Raw Value")
				DEF_SDKPARAM_OPTION("value_07", "AtRegStringValue", "Value pos_07 'Field=Value' or Raw Value")
				DEF_SDKPARAM_OPTION("value_08", "AtRegStringValue", "Value pos_08 'Field=Value' or Raw Value")
				DEF_SDKPARAM_OPTION("value_09", "AtRegStringValue", "Value pos_09 'Field=Value' or Raw Value")
				DEF_SDKPARAM_OPTION("value_10", "AtRegStringValue", "Value pos_10 'Field=Value' or Raw Value")
                DEF_SDKPARAM_OPTION("flag", "AtRegSetMode", "Choose Set Reg Mode - default: field"),
				"${blk} ${addr_s} ${value_00} ${value_01} ${value_02} ${value_03} ${value_04} ${value_05} ${value_06} ${value_07} ${value_08} ${value_09} ${value_10} ${flag}")
{
    mAtCliCall();
}

DEF_SDKCOMMAND(ClishCmdAtRegDumpRead,
				"dump reg",
				"Show information of register",
				DEF_SDKPARAM("blk", "AtRegBlock", "RTL block")
				DEF_SDKPARAM("addr_s", "AtRegAddValue", "The address start")
				DEF_SDKPARAM_OPTION("addr_e", "AtRegAddValue", "The address end (end - start <= 20)"),
				"${blk} ${addr_s} ${addr_e}")
{

    mAtCliCall();
}

DEF_SDKCOMMAND(ClishCmdAtRegChecking,
                "conf reg check",
                "Check register",
                DEF_SDKPARAM("blk", "AtRegBlock", "RTL block"),
                "${blk}")
{
    mAtCliCall();
}

DEF_SDKCOMMAND(ClishCmdAtRegBaseAddressSet,
                "reg base",
                "Set base address",
                DEF_SDKPARAM("baseAddress", "AtRegAddValue", "Address"),
                "${baseAddress}")
{
    mAtCliCall();
}


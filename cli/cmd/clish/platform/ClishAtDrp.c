/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : ClishAtDrp.c
 *
 * Created Date: Oct 27, 2015
 *
 * Description : DRP HAL CLISH
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("drp", "CLIs to control DRP")
TOP_SDKCOMMAND("drp port", "CLIs to manage DRP port")

DEF_SDKCOMMAND(CliAtDrpRead,
               "drp rd",
               "Read DRP register.\n",
               DEF_SDKPARAM("address", "STRING", "DRP address")
               DEF_SDKPARAM_OPTION("portId", "STRING", "For products that have more than one ports, if this\n"
                                                       "is input, this port will be accessed. Otherwise, the\n"
                                                       "current selected port will be accessed.")
               DEF_SDKPARAM_OPTION("pretty", "RegisterDisplayFormat", ""),
               "${address} ${portId} ${pretty} ")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDrpWrite,
             "drp wr",
             "Write DRP register\n",
             DEF_SDKPARAM("address", "STRING", "DRP address")
             DEF_SDKPARAM("value", "RegisterValue", "")
             DEF_SDKPARAM_OPTION("portId", "STRING", "For products that have more than one ports, if this\n"
                                                                    "is input, this port will be accessed. Otherwise, the\n"
                                                                    "current selected port will be accessed."),
             "${address} ${value} ${portId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDrpPortSelect,
              "drp port select",
              "Select port to use DRP operations.\n",
              DEF_SDKPARAM_OPTION("portId", "STRING", "Port ID to select. Or none if no port is selected.\n"
                                  "If portId is not input, the current selected port will"
                                  "be displayed"),
              "${portId}")
    {
    mAtCliCall();
    }

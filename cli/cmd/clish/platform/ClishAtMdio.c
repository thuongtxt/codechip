/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Platform
 *
 * File        : ClishAtXauiMdio.c
 *
 * Created Date: Jul 12, 2015
 *
 * Description : XAUI CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("mdio", "CLIs to control MDIO")
TOP_SDKCOMMAND("mdio port", "CLIs to manage MDIO SERDES")

DEF_SDKCOMMAND(CliAtMdioRead,
               "mdio rd",
               "Read MDIO register.\n",
               DEF_SDKPARAM("address", "STRING", "MDIO address with format <page>.<address>")
               DEF_SDKPARAM_OPTION("serdesId", "STRING", "For products that have more than one SERDESs, if this\n"
                                                         "is input, this SERDES will be accessed. Otherwise, the\n"
                                                         "current selected SERDES will be accessed.")
               DEF_SDKPARAM_OPTION("pretty", "RegisterDisplayFormat", ""),
               "${address} ${serdesId} ${pretty} ")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtMdioWrite,
             "mdio wr",
             "Write MDIO register\n",
             DEF_SDKPARAM("address", "STRING", "MDIO address with format <page>.<address>")
             DEF_SDKPARAM("value", "RegisterValue", "")
             DEF_SDKPARAM_OPTION("serdesId", "STRING", "For products that have more than one SERDESs, if this\n"
                                                       "is input, this SERDES will be accessed. Otherwise, the\n"
                                                       "current selected SERDES will be accessed."),
             "${address} ${value} ${serdesId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtMdioPortSelect,
              "mdio port select",
              "Select SERDES to use MDIO operations.\n",
              DEF_SDKPARAM_OPTION("serdesId", "STRING", "SERDES ID to select. Or none if no SERDES is selected.\n"
                                  "If serdesId is not input, the current selected SERDES will"
                                  "be displayed"),
              "${serdesId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtMdioDebug,
               "debug mdio",
               "Show MDIO debug information\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

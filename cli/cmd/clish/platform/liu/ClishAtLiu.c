/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Platform CLI
 *
 * File        : atcliep6c1.c
 *
 * Created Date: Oct 15, 2012
 *
 * Author      : thuynt
 *
 * Description : EP6C1 CLI
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../AtClish.h"

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Define -----------------------------------------*/
#define regDe1no "([1-9]|([1-2][0-9])|([3][0-2]))"
/*--------------------------- Macros -----------------------------------------*/
/*
 * Define defect masks regular expression
 * DefMask ::= ((DefMask,DefType)|DefType)
 * DefType ::= "(dmo|lfs|lcv-ovf|ais|rlos|qrpd)"
 */
#define DefMask "((("DefType"\\|)+"DefType")|"DefType")"
#define DefType "(dmo|lfs|lcv-ovf|ais|rlos|qrpd)"
/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("ep", "EP CLIs")
TOP_SDKCOMMAND("ep de1", "EP PDH Ds1/E1 CLIs")
TOP_SDKCOMMAND("ep de1 force", "EP PDH Ds1/E1 CLIs for forcing alarm/error")
TOP_SDKCOMMAND("ep show", "E1 CLIs for showing configuration or performance")
TOP_SDKCOMMAND("ep show de1", "EP PDH Ds1/E1 CLIs for showing configuration or performance")
TOP_SDKCOMMAND("liu", "LIU CLIs")
TOP_SDKCOMMAND("liu de1", "LIU DE1 CLIs")

/* Define regular expression for list of De1 IDs
 *
 * BNF format:
 * de1           ::= 1-32
 * de1LstElement ::= ((de1-de1)|de1)
 * de1Lst        ::= ((de1Lst,de1LstElement)|de1LstElement)
 *
 * Convert to regular expression:
 * de1Lst: (de1LstElement,)+de1LstElement|de1LstElement
 */
#define de1LstElement "(("regDe1no"-"regDe1no")|"regDe1no")"
#define de1Lst "(("de1LstElement",)+"de1LstElement"|"de1LstElement")"
#define hexValueDef "^[0][x][A-Fa-f0-9]{1,8}"


DEF_PTYPE("deviceIdList", "integer", "1,2", "Select device")
DEF_PTYPE("eEpDs1E1RailMd", "select", "dual(0), single(1)", "Select rail mode")
DEF_PTYPE("lineIdValue", "integer", "1..16", "Select line ID")
DEF_PTYPE("eEpDs1ClkOutMd", "select", "1ds1(0), 2ds1(1), 4ds1(2), 8ds1(3)", "Select Ds1 output clock mode")
DEF_PTYPE("eEpE1ClkOutMd", "select", "1e1(0), 2e1(1), 4e1(2), 8e1(3)", "Select Ds1 output clock mode")
DEF_PTYPE("eEpDs1E1ClkInMd", "select", "1e1(0), 2e1(8), 4e1(10), 8e1(12), 1ds1(1), 2ds1(9), 4ds1(11), 8ds1(13))", "Select input clock mode")
DEF_PTYPE("eEpDs1E1TrcvCodesMd", "select", "hdb3(0), b8zs(1), ami(2)","Select line code")
DEF_PTYPE("eEpDs1E1TrcvEqcMd", "select", "t1-100tp1x(8), t1-100tp2x(9), t1-100tp3x(10), t1-100tp4x(11), t1-100tp5x(12), t1-100tpar(13), e1-75coax(28), e1-120tp(29)","Select cable length")
DEF_PTYPE("eEpDs1E1TrcvTerImp", "select", "100ohm(0), 110ohm(1), 75ohm(2), 120ohm(3)","Select receive line impedance")
DEF_PTYPE("eEpDs1E1TrcvJaMd", "select", "dis(0), tx(1), rx(2)", "Select jitter attenuator mode")
DEF_PTYPE("eEpJaBandwidthMd", "select", "10hz(0), 1.5hz(1)", "Select jitter attenuator bandwidth")
DEF_PTYPE("eEpDs1E1TrcvFifoMd", "select", "32bit(1), 64bit(2)", "Select FIFO mode")
DEF_PTYPE("eEpDs1E1TrcvLoopMd", "select", "dual(0), analog(1), remote(2), digital(3), noloop(4)", "Select FIFO mode")
DEF_PTYPE("de1IdList", "regexp", de1Lst, "List of Ds1/E1 ID")
DEF_PTYPE("regAddrValue", "regexp", hexValueDef, "Register address")
DEF_PTYPE("regValue", "regexp", hexValueDef, "Value")
DEF_PTYPE("eEpDs1E1TrcvPrbsMd", "select", "prbs(0), qrss(1)", "Select PRBS mode")
DEF_PTYPE("eEpDs1E1TrcvSide", "select", "tx(0), rx(1), both(2)", "Select direction")
DEF_PTYPE("ePdhErrorType", "select", "bpv(0), lineber(1), systember(2), alltype(3)", "Select error type")
DEF_PTYPE("PdhDe1IntrMask", "regexp", DefMask,
          "\nDs1/E1 channel defect masks:\n"
          "- dmo: DMO\n"
          "- lfs: LFS\n"
          "- lcv-ovf: LCV-OVF\n"
          "- ais: AIS\n"
          "- rlos: RLOS\n"
          "- qrpd: QRPD\n"
          "Note: These masks can be ORed, for example, dmo|lfs\n")
DEF_PTYPE("PdhSystemIntrMask", "regexp", DefMask,
        "\nDs1/E1 system defect masks:\n"
        "- ais: AIS\n"
        "- los: LOS\n"
        "- qrpd: QRPD\n"
        "Note: These masks can be ORed, for example, ais|rlos\n")
DEF_PTYPE("PdhAlarmDef", "select", "normal(0), ais(1), los(2)", "Select forcing type")
DEF_PTYPE("numberOfError", "integer", "0..65535", "Input error numbers")
DEF_PTYPE("eAtEpLiuMode", "select", "e1(0), ds1(1)", "Select liu mode")

/* Read */
DEF_SDKCOMMAND(cliEpRead,
               "ep rd",
               "Read EP register\n",
               DEF_SDKPARAM("address", "regAddrValue", ""),
               "{$address}")
    {
    mAtCliCall();
    }

/* Write */
DEF_SDKCOMMAND(cliEpWrite,
               "ep wr",
               "Write value to EP register\n",
               DEF_SDKPARAM("address", "regAddrValue", "")
               DEF_SDKPARAM("value", "regValue", ""),
               "{$address} {value}")
    {
    mAtCliCall();
    }

/* Reset transceiver */
DEF_SDKCOMMAND(cliEpPdhDe1Reset,
               "ep de1 reset",
               "Reset Ds1/E1 Transceiver for PDH module\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

/* Global configuration*/
DEF_SDKCOMMAND(cliEpPdhDe1Master,
               "ep de1 master",
               "SET/GET Ds1/E1 master control for PDH module\n"
               "Usages:\n"
               "- SET: ep de1 master <deviceId> <raiMd> <autoTxAllZero> <rxClk> <txClk> <rxMute> <extendLos> <sysExtendLos> <txClkCnl>\n",
               DEF_SDKPARAM("deviceId", "deviceIdList", "")
               DEF_SDKPARAM("railMd", "eEpDs1E1RailMd", "")
               DEF_SDKPARAM("autoTxAllZero", "eBool", "Automatic Transmit All Ones if RLOS occurred")
               DEF_SDKPARAM("rxClk", "eBool", "Rx clock ")
               DEF_SDKPARAM("txClk", "eBool", "Tx clock ")
               DEF_SDKPARAM("rxMute", "eBool", "Receiver Output Mute function")
               DEF_SDKPARAM("extendLos", "eBool", "Extended Loss of Zeros")
               DEF_SDKPARAM("sysExtendLos", "eBool", "System Extended Loss of Zeros")
               DEF_SDKPARAM("txClkCnl", "eBool", "TTIP/TRING will Transmit All Ones (TAOS) or TTIP/TRING All Zeros"),
               "${deviceId} ${railMd} ${autoTxAllZero} ${rxClk} ${txClk}${rxMute} ${extendLos} ${sysExtendLos},${txClkCnl}")
    {
    mAtCliCall();
    }

/* Clock configuration */
DEF_SDKCOMMAND(cliEpPdhDe1Clock,
               "ep de1 clock",
               "SET/GET Ds1/E1 clock for PDH module\n"
               "Usages:\n"
               "- SET: ep de1 clock <deviceId> <ds1OutClockMode> <e1OutClockMode> <rxClkSynthesize> <recoverClk> <recoverLineId>\n",
               DEF_SDKPARAM("deviceId", "deviceIdList", "")
               DEF_SDKPARAM("ds1OutClockMode", "eEpDs1ClkOutMd", "")
               DEF_SDKPARAM("e1OutClockMode", "eEpE1ClkOutMd", "")
               DEF_SDKPARAM("rxClkSynthesize", "eEpDs1E1ClkInMd", "")
               DEF_SDKPARAM("recoverClk", "eBool", "Recover clock")
               DEF_SDKPARAM("recoverLineId", "lineIdValue", ""),
               "${deviceId} ${ds1OutClockMode} ${e1OutClockMode} ${rxClkSynthesize} ${recoverClk} ${recoverLineId}")
    {
    mAtCliCall();
    }

/* Configure transceiver for Ds1/E1 channel */
DEF_SDKCOMMAND(cliEpPdhDe1Chanel,
               "ep de1 chanel",
               "SET/GET Ds1/E1 chanel transceiver configuration \n"
               "Usages:\n"
               "- SET: ep de1 chanel <de1Id> <txPortEn> <rxPortEn> <lineCode> <cableLength> <rxLineImpedance>\n",
               DEF_SDKPARAM("de1Id", "de1IdList", "")
               DEF_SDKPARAM("txPortEn", "eBool", "TX port state")
               DEF_SDKPARAM("rxPortEn", "eBool", "RX port state")
               DEF_SDKPARAM("lineCode", "eEpDs1E1TrcvCodesMd", "")
               DEF_SDKPARAM("cableLength", "eEpDs1E1TrcvEqcMd", "")
               DEF_SDKPARAM("rxLineImpedance", "eEpDs1E1TrcvTerImp", ""),
               "${de1Id} ${txPortEn} ${rxPortEn} ${lineCode} ${cableLength} ${rxLineImpedance}")
    {
    mAtCliCall();
    }

/* Configure jitter attenuator for each Ds1/E1 channel*/
DEF_SDKCOMMAND(cliEpPdhDe1JitterAttenuator,
               "ep de1 jitterattenuator",
               "SET/GET Ds1/E1 jitter attenuator \n"
               "Usages:\n"
               "- SET: ep de1 jitterattenuator <de1Id> <jaMode> <jaBandwidth> <fifoDepth> \n",
               DEF_SDKPARAM("de1Id", "de1IdList", "")
               DEF_SDKPARAM("jaMode", "eEpDs1E1TrcvJaMd", "")
               DEF_SDKPARAM("jaBandwidth", "eEpJaBandwidthMd", "")
               DEF_SDKPARAM("fifoDepth", "eEpDs1E1TrcvFifoMd", ""),
               "${de1Id} ${jaMode} ${jaBandwidth} ${fifoDepth}")
    {
    mAtCliCall();
    }

/* Configure loop back Ds1/E1 channel*/
DEF_SDKCOMMAND(cliEpPdhDe1Loopback,
               "ep de1 loopback",
               "SET/GET Ds1/E1 loopback mode \n"
               "Usages:\n"
               "- SET: ep de1 loopback <de1Id> <loopbackMode>\n",
               DEF_SDKPARAM("de1Id", "de1IdList", "")
               DEF_SDKPARAM("loopbackMode", "eEpDs1E1TrcvLoopMd", ""),
               "${de1Id} ${loopbackMode}")
    {
    mAtCliCall();
    }

/* Configure global interrupt detection */
DEF_SDKCOMMAND(cliEpPdhDe1GlobalIntr,
               "ep de1 globalinterrupt",
               "SET/GET interrupt detection \n"
               "Usages:\n"
               "- SET: ep de1 globalintr <de1Id> <intrEn> \n",
               DEF_SDKPARAM("de1Id", "de1IdList", "")
               DEF_SDKPARAM("intrEn", "eBool", ""),
               "${de1Id} ${intrEn} ")
    {
    mAtCliCall();
    }

/* SET interrupt mask enable for Ds1/E1 channel */
DEF_SDKCOMMAND(cliEpPdhDe1IntrMaskSet,
               "ep de1 interrupt",
               "SET/GET interrupt masks\n"
               "Usages:\n"
               "- SET: ep de1 intr <de1Ids> <dmo> <fls> <lcv> <ais> <los> <qrpd>\n",
               DEF_SDKPARAM("de1Ids", "de1IdList", "")
               DEF_SDKPARAM("dmo", "eBool", "DMO interrupt enable")
               DEF_SDKPARAM("fls", "eBool", "FLS interrupt enable")
               DEF_SDKPARAM("lcv", "eBool", "LCV interrupt enable")
               DEF_SDKPARAM("ais", "eBool", "AIS interrupt enable")
               DEF_SDKPARAM("los", "eBool", "LOS interrupt enable")
               DEF_SDKPARAM("qrpd", "eBool", "QRPD interrupt enable"),
               "${de1Ids} ${dmo} ${fls} ${lcv} ${ais} ${los} ${qrpd}")
    {
    mAtCliCall();
    }

/* GET interrupt mask for Ds1/E1 channel */
DEF_SDKCOMMAND(cliEpPdhDe1IntrMaskGet,
               "ep show de1 interrupt",
               "SET/GET interrupt masks\n"
               "Usages:\n"
               "- GET: ep show de1 interrupt <de1Ids> <readingMode>\n",
               DEF_SDKPARAM("de1Ids", "de1IdList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", ""),
               "${de1Ids} ${readingMode}")
    {
    mAtCliCall();
    }

/* SET/GET PRBS mode */
DEF_SDKCOMMAND(cliEpPdhDe1PrbsMode,
               "ep de1 prbs",
               "SET/GET interrupt masks\n"
               "Usages:\n"
               "- SET: ep de1prbs <de1Ids> <direction> <prbsMode> <enable>\n",
               DEF_SDKPARAM("de1Ids", "de1IdList", "")
               DEF_SDKPARAM("direction", "eEpDs1E1TrcvSide", "")
               DEF_SDKPARAM("prbsMode", "eEpDs1E1TrcvPrbsMd", "")
               DEF_SDKPARAM("enable", "eBool", "PRBS generation"),
               "${de1Ids} ${prbsMode} ${enable}")
    {
    mAtCliCall();
    }


/* Alarm forcing */
DEF_SDKCOMMAND(cliEpPdhDe1ForceAlarm,
               "ep de1 force alarm",
               "Force chanel alarm or get forcing status\n"
               "Usages:\n"
               "- Force alarm at specified direction: ep de1 force alarm <de1Ids> <direction> <alarm>\n",
               DEF_SDKPARAM("de1Ids", "de1IdList", "")
               DEF_SDKPARAM("direction", "eEpDs1E1TrcvSide", "")
               DEF_SDKPARAM("alarm", "PdhAlarmDef", ""),
               "${de1Ids} ${direction} ${alarm}")
    {
    mAtCliCall();
    }


/* Insert error */
DEF_SDKCOMMAND(cliEpPdhDe1InsertError,
               "ep de1 force error",
               "Force chanel alarm or get forcing status\n"
               "Usages:\n"
               "- Insert errors : ep de1 force error <de1Ids> <alarm> <number>\n",
               DEF_SDKPARAM("de1Ids", "de1IdList", "")
               DEF_SDKPARAM("error", "ePdhErrorType", "")
               DEF_SDKPARAM("number", "numberOfError", ""),
               "${de1Ids} ${error} ${number}")
    {
    mAtCliCall();
    }

/* Insert error */
DEF_SDKCOMMAND(cliEpPdhDe1GlobalCfgGet,
               "ep show de1 global",
               "Show global configurations of Ds1/E1 transceiver\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEpPdhDe1CfgGet,
               "ep show de1 config",
               "Show global configurations of Ds1/E1 chanels\n",
               DEF_SDKPARAM("de1Ids", "de1IdList", ""),
               "${de1Ids}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEpLiuDe1LoopbackSet,
               "liu de1 loopback",
               "Set loopback for E1/DS1 channels\n",
               DEF_SDKPARAM("de1Ids", "de1IdList", "")
               DEF_SDKPARAM("loopbackMode", "eEpDs1E1TrcvLoopMd", ""),
               "${de1Id} ${loopbackMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEpLiuRead,
               "liu rd",
               "Read LIU register\n",
               DEF_SDKPARAM("address", "regAddrValue", ""),
               "${address}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEpLiuWrite,
               "liu wr",
               "Write value to LIU register\n",
               DEF_SDKPARAM("address", "regAddrValue", "")
               DEF_SDKPARAM("value", "regValue", ""),
               "${address} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEpLiuMode,
               "liu mode",
               "Set liu mode\n",
               DEF_SDKPARAM("mode", "eAtEpLiuMode", ""),
               "${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEpLiuDe1Enable,
               "liu de1 enable",
               "Enable DE1 LIU\n",
               DEF_SDKPARAM("de1Ids", "de1IdList", ""),
               "${de1Id}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEpLiuDe1Disable,
               "liu de1 disable",
               "Disable DE1 LIU\n",
               DEF_SDKPARAM("de1Ids", "de1IdList", ""),
               "${de1Id}")
    {
    mAtCliCall();
    }

#ifdef  __cplusplus
}
#endif

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2102 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : clishatplatform.c
 *
 * Created Date: Nov 9, 2012
 *
 * Description : platform
 *
 * Notes       :
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtDevice.h"
#include "AtCli.h"
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/
#define cClishAtCmdStrLen            200

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
extern void AtHalSlowDown(AtHal self, eBool slowDown);

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("measure", "measure")
TOP_SDKCOMMAND("hal", "hal")
TOP_SDKCOMMAND("hal register", "HAL register")
TOP_SDKCOMMAND("ddr", "Ddr")
TOP_SDKCOMMAND("device subordinate", "Device Ddr offload")
TOP_SDKCOMMAND("device subordinate ddr", "Device Ddr offload")

DEF_PTYPE("RegisterAddress", "regexp", ".*", "Address register")
DEF_PTYPE("RegisterBitMask", "regexp", ".*", "Bit Mask register")
DEF_PTYPE("RegisterValue", "regexp", ".*", "Register Value")
DEF_PTYPE("RegisterDisplayFormat", "select", "pretty",
          "Register display format:\n"
          "- pretty: Print register value in meaning full format with value for each bit position\n")

static AtHal Hal(void)
  {
  return AtDeviceIpCoreHalGet(CliDevice(), 0);
  }

static int ReadByCommand(const lub_argv_t * argv, const char *command)
    {
    char pCmdStr[cClishAtCmdStrLen];
    uint8  i;

    AtStrcpy(pCmdStr, command);

    /* Get bitmask, address, fpgaId */
    for (i = 0; i < lub_argv__get_count(argv); i++)
        {
        AtStrcat(pCmdStr, " ");
        AtStrcat(pCmdStr, lub_argv__get_arg(argv, i));
        }

    return AtTextUICmdProcess(AtCliSharedTextUI(), pCmdStr) ? 0: -1;
    }

static int WriteByCommand(const lub_argv_t * argv, const char *command)
    {
    char   pCmdStr[cClishAtCmdStrLen];
    uint8  i;

    AtStrcpy(pCmdStr, command);

    /* Get bitmask, address, fpgaId */
    for (i = 0; i < lub_argv__get_count(argv); i++)
        {
        AtStrcat(pCmdStr, " ");
        AtStrcat(pCmdStr, lub_argv__get_arg(argv, i));
        }

    return AtTextUICmdProcess(AtCliSharedTextUI(), pCmdStr) ? 0: -1;
    }

DEF_SDKCOMMAND(cliPlatformRead,
             "rd",
             "Read hardware register.\n",
             DEF_SDKPARAM("address", "RegisterAddress", "")
             DEF_SDKPARAM_OPTION("mask", "RegisterBitMask", "")
             DEF_SDKPARAM_OPTION("fpgaId", "UINT", "")
             DEF_SDKPARAM_OPTION("pretty", "RegisterDisplayFormat", ""),
             "${address} ${mask} ${fpgaId} ${pretty}")
    {
	AtUnused(context);
    return ReadByCommand(argv, "rd");
    }

DEF_SDKCOMMAND(cliPlatformWrite,
             "wr",
             "Write hardware register.\n",
             DEF_SDKPARAM("address", "RegisterAddress", "")
             DEF_SDKPARAM("value", "RegisterValue", "")
             DEF_SDKPARAM_OPTION("mask", "RegisterBitMask", "")
             DEF_SDKPARAM_OPTION("fpgaId", "UINT", ""),
             "${address} ${value} ${mask} ${fpgaId}")
    {
	AtUnused(context);
    return WriteByCommand(argv, "wr");
    }

static int LongReadByCommand(const lub_argv_t * argv, const char *command)
    {
    char   pCmdStr[cClishAtCmdStrLen];
    uint8  i;

    AtStrcpy(pCmdStr, command);

    /* Get bitmask, address, fpgaId */
    for (i = 0; i < lub_argv__get_count(argv); i++)
        {
        AtStrcat(pCmdStr, " ");
        AtStrcat(pCmdStr, lub_argv__get_arg(argv, i));
        }

    return AtTextUICmdProcess(AtCliSharedTextUI(), pCmdStr) ? 0: -1;
    }

DEF_SDKCOMMAND(cliPlatformLongRead,
               "lrd",
               "Read hardware register.\n",
               DEF_SDKPARAM("address", "RegisterAddress", "")
               DEF_SDKPARAM_OPTION("mask", "RegisterBitMask", "")
               DEF_SDKPARAM_OPTION("fpgaId", "UINT", "")
               DEF_SDKPARAM_OPTION("pretty", "RegisterDisplayFormat", ""),
               "${address} ${mask} ${fpgaId} ${pretty}")
    {
    AtUnused(context);
    return LongReadByCommand(argv, "lrd");
    }

DEF_SDKCOMMAND(cliPlatformLongWrite,
             "lwr",
             "Read hardware register.\n",
             DEF_SDKPARAM("address", "RegisterAddress", "")
             DEF_SDKPARAM("value", "RegisterValue", "")
             DEF_SDKPARAM_OPTION("mask", "RegisterBitMask", "")
             DEF_SDKPARAM_OPTION("fpgaId", "UINT", ""),
             "${address} ${value} ${mask} ${fpgaId}")
    {
    char   pCmdStr[cClishAtCmdStrLen];
    uint8  i;
	AtUnused(context);

    AtStrcpy(pCmdStr, "lwr");

    /* Get bitmask, address, fpgaId */
    for (i = 0; i < lub_argv__get_count(argv); i++)
        {
        AtStrcat(pCmdStr, " ");
        AtStrcat(pCmdStr, lub_argv__get_arg(argv, i));
        }

    return AtTextUICmdProcess(AtCliSharedTextUI(), pCmdStr) ? 0: -1;
    }

DEF_SDKCOMMAND(cliPlatformDump,
             "dump",
             "Dump hardware register.\n",
             DEF_SDKPARAM("fromAddress", "RegisterAddress", "")
             DEF_SDKPARAM("toAddress", "RegisterAddress", "")
             DEF_SDKPARAM_OPTION("pretty", "RegisterDisplayFormat", "")
             DEF_SDKPARAM_OPTION("mask", "RegisterBitMask", "")
             DEF_SDKPARAM_OPTION("fpgaId", "UINT", ""),
             "${fromAddress} ${toAddress} ${pretty} ${mask} ${fpgaId}")
    {
    char   pCmdStr[cClishAtCmdStrLen];
    uint8  i = 0;
	AtUnused(context);

    AtStrcpy(pCmdStr, "dump");

    /* Get bitmask, address, fpgaId */
    while (i < lub_argv__get_count(argv))
        {
        AtStrcat(pCmdStr, " ");
        AtStrcat(pCmdStr, lub_argv__get_arg(argv, i));
        i++;
        }

    return AtTextUICmdProcess(AtCliSharedTextUI(), pCmdStr) ? 0: -1;
    }

DEF_SDKCOMMAND(cliPlatformCompareDump,
             "dump compare",
             "Dump compare hardware register.\n",
             DEF_SDKPARAM("numRows", "UINT", "")
             DEF_SDKPARAM("compareInfo", "STRING", "")
             DEF_SDKPARAM_OPTION("mask", "RegisterBitMask", "")
             DEF_SDKPARAM_OPTION("fpgaId", "UINT", ""),
             "${numRows} ${compareInfo} ${mask} ${fpgaId}")
    {
    char   pCmdStr[cClishAtCmdStrLen];
    uint8  i = 0;
	AtUnused(context);

    AtStrcpy(pCmdStr, "dump compare");

    /* Get bitmask, address, fpgaId */
    while (i < lub_argv__get_count(argv))
        {
        AtStrcat(pCmdStr, " ");
        AtStrcat(pCmdStr, lub_argv__get_arg(argv, i));
        i++;
        }

    return AtTextUICmdProcess(AtCliSharedTextUI(), pCmdStr) ? 0: -1;
    }

DEF_SDKCOMMAND(cliPlatformFill,
             "fill",
             "Fill hardware register.\n",
             DEF_SDKPARAM("fromAddress", "RegisterAddress", "")
             DEF_SDKPARAM("toAddress", "RegisterAddress", "")
             DEF_SDKPARAM("value", "RegisterValue", "")
             DEF_SDKPARAM_OPTION("mask", "RegisterBitMask", "")
             DEF_SDKPARAM_OPTION("fpgaId", "UINT", ""),
             "${fromAddress} ${toAddress} ${value} ${mask} ${fpgaId}")
    {
    char   pCmdStr[cClishAtCmdStrLen];
    uint8  i = 0;
	AtUnused(context);

    AtStrcpy(pCmdStr, "fill");

    /* Get bitmask, address, value, fpgaId */
    while (i < lub_argv__get_count(argv))
        {
        AtStrcat(pCmdStr, " ");
        AtStrcat(pCmdStr, lub_argv__get_arg(argv, i));
        i++;
        }

    return AtTextUICmdProcess(AtCliSharedTextUI(), pCmdStr) ? 0: -1;
    }

DEF_SDKCOMMAND(cliAtMeasureRead,
               "measure read",
               "Measure reading operation",
               DEF_SDKPARAM("fromAddr", "RegisterAddress", "")
               DEF_SDKPARAM("toAddr", "RegisterAddress", "")
               DEF_SDKPARAM("repeatTimes", "UINT", ""),
               "${fromAddr} ${toAddr} ${repeatTimes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtMeasureWrite,
               "measure write",
               "Measure writing operation",
               DEF_SDKPARAM("fromAddr", "RegisterAddress", "")
               DEF_SDKPARAM("toAddr", "RegisterAddress", "")
               DEF_SDKPARAM("repeatTimes", "UINT", ""),
               "${fromAddr} ${toAddr} ${repeatTimes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDiagHalMemTest,
               "hal memorytest",
               "Basic memory test for HAL",
               DEF_SDKPARAM_OPTION("testTime", "UINT", ""),
               "${testTime}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDdrFlush,
               "ddr flush",
               "flush ddr",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

/* dump address */
DEF_SDKCOMMAND(cliAtLongDump,
             "ldump",
             "Dump hardware long register.\n",
             DEF_SDKPARAM("fromAddress", "RegisterAddress", "")
             DEF_SDKPARAM("toAddress", "RegisterAddress", "")
             DEF_SDKPARAM_OPTION("mask", "RegisterBitMask", "")
             DEF_SDKPARAM_OPTION("fpgaId", "UINT", ""),
             "${fromAddress} ${toAddress} ${mask} ${fpgaId}")
    {
    mAtCliCall();
    }

/* fill address */
DEF_SDKCOMMAND(cliAtLongFill,
             "lfill",
             "Fill hardware long register.\n",
             DEF_SDKPARAM("fromAddress", "RegisterAddress", "")
             DEF_SDKPARAM("toAddress", "RegisterAddress", "")
             DEF_SDKPARAM("value", "RegisterValue", "")
             DEF_SDKPARAM_OPTION("mask", "RegisterBitMask", "")
             DEF_SDKPARAM_OPTION("fpgaId", "UINT", ""),
             "${fromAddress} ${toAddress} ${value} ${mask} ${fpgaId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPlatformDirectRead,
               "drd",
               "Read hardware register directly\n",
               DEF_SDKPARAM("address", "RegisterAddress", "")
               DEF_SDKPARAM_OPTION("mask", "RegisterBitMask", "")
               DEF_SDKPARAM_OPTION("fpgaId", "UINT", ""),
               "${address} ${mask} ${fpgaId}")
    {
	AtUnused(context);
    return ReadByCommand(argv, "drd");
    }

DEF_SDKCOMMAND(cliPlatformDirectWrite,
             "dwr",
             "Write hardware register directly\n",
             DEF_SDKPARAM("address", "RegisterAddress", "")
             DEF_SDKPARAM("value", "RegisterValue", "")
             DEF_SDKPARAM_OPTION("mask", "RegisterBitMask", "")
             DEF_SDKPARAM_OPTION("fpgaId", "UINT", ""),
             "${address} ${value} ${mask} ${fpgaId}")
    {
	AtUnused(context);
    return WriteByCommand(argv, "dwr");
    }

DEF_SDKCOMMAND(cliAtHalDebug,
               "debug hal",
               "Show HAL debug information",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHalDebugEnable,
               "hal debug",
               "Enable/disable HAL debug",
               DEF_SDKPARAM_OPTION("enable", "eBool", ""),
               "${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHalReset,
               "hal reset",
               "Reset HAL, bring it to normal working state",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHalDebugFast,
               "hal debug fast",
               "Fast HAL",
               DEF_NULLPARAM,
               "")
    {
    AtUnused(context);
    AtUnused(argv);
    AtHalSlowDown(Hal(), cAtFalse);
    return 0;
    }

DEF_SDKCOMMAND(cliAtHalDebugSlow,
               "hal debug slow",
               "Slow HAL down",
               DEF_NULLPARAM,
               "")
    {
    AtUnused(context);
    AtUnused(argv);
    AtHalSlowDown(Hal(), cAtTrue);
    return 0;
    }

DEF_SDKCOMMAND(cliAtHalDebugSlowAccessTime,
               "hal debug accesstime",
               "Set access time in microseconds",
               DEF_SDKPARAM("timeUs", "UINT", "Access time in microseconds"),
               "${timeUs}")
    {
    char *timeString = lub_argv__get_arg(argv, 0);
    AtUnused(context);
    AtHalSlowAccessTimeUsSet(Hal(), AtStrToDw(timeString));
    return 0;
    }

DEF_SDKCOMMAND(cliAtHalBitFieldRegCheck,
               "bfrc",
               "Compare expected field value at a specific address. This command is use to testing purpose",
               DEF_SDKPARAM("fieldval", "RegisterAddress", "expected field value")
               DEF_SDKPARAM("fieldmask", "RegisterBitMask", "32 bit field mask, example bit0, bit1, bit31_0, ..")
               DEF_SDKPARAM("addr", "RegisterAddress", "Register address"),
               "${fieldval} ${fieldmask} ${addr}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHalBitFieldLongRegCheck,
               "bflrc",
               "Compare expected field value at a specific address. This command is use to testing purpose",
               DEF_SDKPARAM("fieldval", "RegisterAddress", "expected field value")
               DEF_SDKPARAM("fieldmask", "RegisterBitMask", "32 bit field mask, example bit0, bit1, bit31_0, ..")
               DEF_SDKPARAM("addr", "RegisterAddress", "Register address"),
               "${fieldval} ${fieldmask} ${addr}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtHalRegisterTest,
               "hal register test",
               "Test a register or a range of registers",
               DEF_SDKPARAM("regList", "STRING", "")
               DEF_SDKPARAM_OPTION("testedMask", "RegisterBitMask", "")
               DEF_SDKPARAM_OPTION("testTime", "UINT", ""),
               "${regList} ${testedMask} ${testTime}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPlatformDdrOffloadRead,
               "device subordinate ddr lrd",
               "DDR offload read hardware register.\n",
               DEF_SDKPARAM("subDeviceId", "UINT", "")
               DEF_SDKPARAM("address", "RegisterAddress", "")
               DEF_SDKPARAM_OPTION("mask", "RegisterBitMask", "")
               DEF_SDKPARAM_OPTION("fpgaId", "UINT", "")
               DEF_SDKPARAM_OPTION("pretty", "RegisterDisplayFormat", ""),
               "${subDeviceId} ${address} ${mask} ${fpgaId} ${pretty}")
    {
    AtUnused(context);
    return LongReadByCommand(argv, "device subordinate ddr lrd");
    }

DEF_SDKCOMMAND(cliPlatformDdrOffloadWrite,
             "device subordinate ddr lwr",
             "DDR offload Write hardware register.\n",
             DEF_SDKPARAM("subDeviceId", "UINT", "")
             DEF_SDKPARAM("ddraddress", "RegisterAddress", "")
             DEF_SDKPARAM("regaddress", "RegisterAddress", "")
             DEF_SDKPARAM("value", "RegisterValue", "")
             DEF_SDKPARAM_OPTION("mask", "RegisterBitMask", "")
             DEF_SDKPARAM_OPTION("fpgaId", "UINT", ""),
             "${subDeviceId} ${ddraddress} ${regaddress} ${value} ${mask} ${fpgaId}")
    {
    char   pCmdStr[cClishAtCmdStrLen];
    uint8  i;
    AtUnused(context);

    AtStrcpy(pCmdStr, "device subordinate ddr lwr");

    /* Get bitmask, address, fpgaId */
    for (i = 0; i < lub_argv__get_count(argv); i++)
        {
        AtStrcat(pCmdStr, " ");
        AtStrcat(pCmdStr, lub_argv__get_arg(argv, i));
        }

    return AtTextUICmdProcess(AtCliSharedTextUI(), pCmdStr) ? 0: -1;
    }

DEF_SDKCOMMAND(cliAtDdrOffloadDump,
             "device subordinate ddr ldump",
             "DDR offload dump hardware long register.\n",
             DEF_SDKPARAM("subDeviceId", "UINT", "")
             DEF_SDKPARAM("fromAddress", "RegisterAddress", "")
             DEF_SDKPARAM("toAddress", "RegisterAddress", "")
             DEF_SDKPARAM_OPTION("mask", "RegisterBitMask", "")
             DEF_SDKPARAM_OPTION("fpgaId", "UINT", ""),
             "${subDeviceId} ${fromAddress} ${toAddress} ${mask} ${fpgaId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDdrOffloadFill,
             "device subordinate ddr lfill",
             "DDR offload fill hardware long register.\n",
             DEF_SDKPARAM("subDeviceId", "UINT", "")
             DEF_SDKPARAM("fromDdrAddress", "RegisterAddress", "")
             DEF_SDKPARAM("toDdrAddress", "RegisterAddress", "")
             DEF_SDKPARAM("fromRegAddress", "RegisterAddress", "")
             DEF_SDKPARAM("toRegAddress", "RegisterAddress", "")
             DEF_SDKPARAM("value", "RegisterValue", "")
             DEF_SDKPARAM_OPTION("mask", "RegisterBitMask", "")
             DEF_SDKPARAM_OPTION("fpgaId", "UINT", ""),
             "${subDeviceId} ${fromDdrAddress} ${toDdrAddress} ${fromRegAddress} ${toRegAddress} ${value} ${mask} ${fpgaId}")
    {
    mAtCliCall();
    }

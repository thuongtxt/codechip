/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : ClishAtDevice.c
 *
 * Created Date: Nov 15, 2012
 *
 * Description : CLISH Device CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("register", "Register CLIs")
TOP_SDKCOMMAND("register audit", "Register audit CLIs")
TOP_SDKCOMMAND("register audit verbose", "Register audit CLIs")
TOP_SDKCOMMAND("register blacklist", "Black-List Register CLIs")
TOP_SDKCOMMAND("register whitelist", "While-List Register CLIs")
TOP_SDKCOMMAND("register blacklist short", "Black List Register CLIs")
TOP_SDKCOMMAND("register blacklist long", "Black List Register CLIs")
TOP_SDKCOMMAND("register whitelist short", "Black List Register CLIs")
TOP_SDKCOMMAND("register whitelist long", "Black List Register CLIs")
TOP_SDKCOMMAND("show register", "Show Register CLIs")
TOP_SDKCOMMAND("show register blacklist", "Show Blacklist Register CLIs")
TOP_SDKCOMMAND("show register whitelist", "Show Whitelist Register CLIs")

DEF_SDKCOMMAND(cliAtRegisterAuditStart,
               "register audit start",
               "Start register Audit",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtRegisterAuditStop,
               "register audit stop",
               "Stop register Audit",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtRegisterAuditShow,
               "show register audit",
               "Show register Audit",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtRegisterAuditPerfom,
               "register audit perform",
               "Configure parameter of Register Audit",
               DEF_SDKPARAM("duration", "STRING", "Duration in second, start from 1")
               DEF_SDKPARAM("repeat", "STRING", "Repeat number, start from 1"),
               "${duration} ${repeat} ")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtRegisterAuditVerboseOn,
               "register audit verbose on",
               "Enable Verbose for debug purpose",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtRegisterAuditVerboseOff,
               "register audit verbose off",
               "Disable Verbose for debug purpose",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtShortRegisterBlackListAdd,
               "register blacklist short add",
               "Add register to black list",
               DEF_SDKPARAM("address", "STRING", "Start Adddress with format 0xXXXXXXXX")
               DEF_SDKPARAM_OPTION("numbers", "UINT", "Start from 1")
               DEF_SDKPARAM_OPTION("step", "UINT", "Start from 1")
               DEF_SDKPARAM_OPTION("FpgaId", "UINT", "FPGA Index will start from 1"),
               "${address} ${numbers} ${step} ${FpgaId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtShortRegisterBlackListRemove,
               "register blacklist short remove",
               "Remove register to black list",
               DEF_SDKPARAM("address", "STRING", "Start Adddress with format 0xXXXXXXXX")
               DEF_SDKPARAM_OPTION("numbers", "UINT", "Start from 1")
               DEF_SDKPARAM_OPTION("step", "UINT", "Start from 1")
               DEF_SDKPARAM_OPTION("FpgaId", "UINT", "FPGA Index will start from 1"),
               "${address} ${numbers} ${step} ${FpgaId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtLongRegisterBlackListAdd,
               "register blacklist long add",
               "Add register to black list",
               DEF_SDKPARAM("address", "STRING", "Start Adddress with format 0xXXXXXXXX")
               DEF_SDKPARAM_OPTION("numbers", "UINT", "Start from 1")
               DEF_SDKPARAM_OPTION("step", "UINT", "Start from 1")
               DEF_SDKPARAM_OPTION("FpgaId", "UINT", "FPGA Index will start from 1"),
               "${address} ${numbers} ${step} ${FpgaId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtLongRegisterBlackListRemove,
               "register blacklist long remove",
               "Add register to black list",
               DEF_SDKPARAM("address", "STRING", "Start Adddress with format 0xXXXXXXXX")
               DEF_SDKPARAM_OPTION("numbers", "UINT", "Start from 1")
               DEF_SDKPARAM_OPTION("step", "UINT", "Start from 1")
               DEF_SDKPARAM_OPTION("FpgaId", "UINT", "FPGA Index will start from 1"),
               "${address} ${numbers} ${step} ${FpgaId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtShortRegisterWhitelistMask,
               "register whitelist short mask",
               "Set mask for each register for audit",
               DEF_SDKPARAM("address", "STRING", "Start Adddress with format 0xXXXXXXXX")
               DEF_SDKPARAM("mask", "STRING", "Format: bitx-y,z,t,a-b")
               DEF_SDKPARAM_OPTION("numbers", "UINT", "Start from 1")
               DEF_SDKPARAM_OPTION("step", "UINT", "Start from 1")
               DEF_SDKPARAM_OPTION("FpgaId", "UINT", "FPGA Index will start from 1"),
               "${address} ${mask} ${numbers} ${step} ${FpgaId}")
    {
    mAtCliCall();
    }



DEF_SDKCOMMAND(cliAtLongRegisterWhitelistMask,
               "register whitelist long mask",
               "Set mask for each register for audit",
               DEF_SDKPARAM("address", "STRING", "Start Adddress with format 0xXXXXXXXX")
               DEF_SDKPARAM("mask", "STRING", "Format: bitx-y,z,t,a-b")
               DEF_SDKPARAM_OPTION("numbers", "UINT", "Start from 1")
               DEF_SDKPARAM_OPTION("step", "UINT", "Start from 1")
               DEF_SDKPARAM_OPTION("FpgaId", "UINT", "FPGA Index will start from 1"),
               "${address} ${mask} ${numbers} ${step} ${FpgaId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtShortRegisterBlackListShow,
               "show register blacklist short",
               "Show the black-list of Register Audit",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtLongRegisterBlackListShow,
               "show register blacklist long",
               "Show the black-list of Register Audit",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }


DEF_SDKCOMMAND(cliAtShortRegisterWhitelistShow,
               "show register whitelist short",
               "Show the whitelist of Register Audit",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtLongRegisterWhitelistShow,
               "show register whitelist long",
               "Show the whitelist of Register Audit",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

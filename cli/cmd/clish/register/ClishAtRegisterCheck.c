/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : ClishAtRegisterCheck.c
 *
 * Created Date: Sep 17, 2015
 *
 * Description : Register checking
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("register check", "Register checking CLIs")
TOP_SDKCOMMAND("register check verbose", "Register checking CLIs")
TOP_SDKCOMMAND("register check automode", "Register checking CLIs")
TOP_SDKCOMMAND("show register", "Show register checking CLIs")

DEF_PTYPE("regMask", "regexp", ".*",
         "\nInput register mask\n"
         "+ As Hex value, example 0xffffffff\n"
         "+ fullshort: full short register [31:0]\n"
         "+ fulllong: full long register\n")

DEF_SDKCOMMAND(cliAtRegisterCheckStart,
               "register check start",
               "Start register checking",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtRegisterCheckStop,
               "register check stop",
               "Stop register checking",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtRegisterCheckShow,
               "show register check",
               "Show register checking content",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtRegisterCheckShowLogger,
               "show register check logger",
               "Show register checking logger content",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtRegisterCheckLoggerFlush,
               "register check logger flush",
               "Flush register checking logger content",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtRegisterCheckScan,
               "register check scan",
               "Scan all registers",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtRegisterCheckFlush,
               "register check flush",
               "Flush all registers",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtRegisterCheckVerboseEnable,
               "register check verbose enable",
               "Enable verbose mode",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtRegisterCheckVerboseDisable,
               "register check verbose disable",
               "Disable verbose mode",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtRegisterCheckAutoModeEnable,
               "register check automode enable",
               "Enable auto mode",
               DEF_SDKPARAM_OPTION("regType", "STRING", "Register type"),
               "${regType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtRegisterCheckAutoModeDisable,
               "register check automode disable",
               "Disable auto mode",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtRegisterCheckMemTest,
               "register check memtest",
               "Memory test all registers in checking list",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtShortRegisterCheckAdd,
               "register check add",
               "Add register to checking list",
               DEF_SDKPARAM("address", "STRING", "Start Address with format 0xXXXXXXXX")
               DEF_SDKPARAM_OPTION("mask", "regMask", "")
               DEF_SDKPARAM_OPTION("numbers", "UINT", "Number adding address")
               DEF_SDKPARAM_OPTION("step", "UINT", "Step to increase address"),
               "${address} ${mask} ${numbers} ${step}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtShortRegisterCheckRemove,
               "register check remove",
               "Remove register out of checking list",
               DEF_SDKPARAM("address", "STRING", "Start Address with format 0xXXXXXXXX")
               DEF_SDKPARAM_OPTION("numbers", "UINT", "Number adding address")
               DEF_SDKPARAM_OPTION("step", "UINT", "Step to increase address"),
               "${address} ${numbers} ${step}")
    {
    mAtCliCall();
    }

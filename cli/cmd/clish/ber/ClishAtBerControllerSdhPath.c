/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : ClishAtBerControllerSdhPath.c
 *
 * Created Date: Sep 30, 2013
 *
 * Description : CLISH CLI to control SDH path BER monitoring
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("sdh path ber measure", "BER measure CLIs")
TOP_SDKCOMMAND("sdh path ber measure inputerror", "BER measure CLIs")
TOP_SDKCOMMAND("sdh path ber measure polling", "BER measure CLIs")

DEF_PTYPE("eAtBerMeasureEngineType", "select", "soft(0), hard(1)", "Soft-Engine or Hard-Engine")
DEF_PTYPE("eAtBerMeasureingMode", "select", "manual(1), continous_none_stop(2), continous_auto_stop(3), one_shot(4)", "Measuring mode")

DEF_SDKCOMMAND(cliSdhPathBerMonitorEnable,
               "sdh path ber monitor enable",
               "Enable BER monitor\n",
               DEF_SDKPARAM("pathList", "BerSdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathBerMonitorDisable,
               "sdh path ber monitor disable",
               "Disable BER monitor\n",
               DEF_SDKPARAM("pathList", "BerSdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

/* SET BER SD threshold */
DEF_SDKCOMMAND(cliSdhPathBerSdThres,
               "sdh path ber sd",
               "Set BER-SD threshold\n",
               DEF_SDKPARAM("pathList", "BerSdhPathList", "")
               DEF_SDKPARAM("threshold", "eAtBerRate", "BER-SD threshold"),
               "${pathList} ${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathBerSfThres,
               "sdh path ber sf",
               "Set BER-SF threshold\n",
               DEF_SDKPARAM("pathList", "BerSdhPathList", "")
               DEF_SDKPARAM("threshold", "eAtBerRate", "BER-SF threshold"),
               "${pathList} ${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathBerTcaThres,
               "sdh path ber tca",
               "Set BER-TCA threshold\n",
               DEF_SDKPARAM("pathList", "BerSdhPathList", "")
               DEF_SDKPARAM("threshold", "eAtBerRate", "BER-TCA threshold"),
               "${pathList} ${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathBerGet,
               "show sdh path ber",
               "Show BER enable/disable, thresholds and current value\n",
               DEF_SDKPARAM("pathList", "BerSdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathBerDebug,
               "sdh path ber debug",
               "Enable/disable BER debug\n",
               DEF_SDKPARAM("pathList", "BerSdhPathList", "")
               DEF_SDKPARAM("enable", "eBool", "Enable/disable BER debug"),
               "${pathList} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliSdhPathBerMeasureTimeEngineCreate,
               "sdh path ber measure create",
               "Create measure engine base on soft/hard type.\n",
               DEF_SDKPARAM("pathList", "BerSdhPathList", "")
               DEF_SDKPARAM("engineType", "eAtBerMeasureEngineType", ""),
               "${pathList} {engineType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliSdhPathBerMeasureTimeEngineDelete,
               "sdh path ber measure delete",
               "Delete measure engine on this channel.\n",
               DEF_SDKPARAM("pathList", "BerSdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliSdhPathBerMeasureTimeEngineModeSet,
               "sdh path ber measure mode",
               "Set measuring mode.\n",
               DEF_SDKPARAM("pathList", "BerSdhPathList", "")
               DEF_SDKPARAM("mode", "eAtBerMeasureingMode", ""),
               "${pathList} {mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathBerMeasureTimeEnginePollingStart,
               "sdh path ber measure polling start",
               "Start polling on this BER measure engine.\n",
               DEF_SDKPARAM("pathList", "BerSdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathBerMeasureTimeEnginePollingStop,
               "sdh path ber measure polling stop",
               "Stop polling on this BER measure engine.\n",
               DEF_SDKPARAM("pathList", "BerSdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathBerMeasureTimeEngineInputErrorConnect,
               "sdh path ber measure inputerror connect",
               "Connect input error for measure detection time.\n",
               DEF_SDKPARAM("pathList", "BerSdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathBerMeasureTimeEngineInputErrorDisconnect,
               "sdh path ber measure inputerror disconnect",
               "Dis-connect input error for measure clearing time.\n",
               DEF_SDKPARAM("pathList", "BerSdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathBerMeasureTimeEngineEstimatedTimesClear,
               "sdh path ber measure clear",
               "Clear all measured time data.\n",
               DEF_SDKPARAM("pathList", "BerSdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathBerMeasureTimeEngineShow,
               "show sdh path ber measure",
               "Show all measured time data.\n",
               DEF_SDKPARAM("pathList", "BerSdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : ClishAtBerControllerPdhDe3.c
 *
 * Created Date: Aug 3, 2015
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("pdh de3 line", "De3 line configuration\n")
TOP_SDKCOMMAND("pdh de3 line ber", "De3 line Ber configuration\n")
TOP_SDKCOMMAND("pdh de3 line ber monitor", "De3 line Ber configuration\n")
TOP_SDKCOMMAND("pdh de3 path", "De3 Path configuration\n")
TOP_SDKCOMMAND("pdh de3 path ber", "De3 Path Ber configuration\n")
TOP_SDKCOMMAND("pdh de3 path ber monitor", "De3 Path Ber configuration\n")
TOP_SDKCOMMAND("show pdh de3 line", "show De3 line configuration\n")
TOP_SDKCOMMAND("show pdh de3 path", "show De3 path configuration\n")

TOP_SDKCOMMAND("pdh de3 line ber measure", "BER measure CLIs")
TOP_SDKCOMMAND("pdh de3 line ber measure inputerror", "BER measure CLIs")
TOP_SDKCOMMAND("pdh de3 line ber measure polling", "BER measure CLIs")

TOP_SDKCOMMAND("pdh de3 path ber measure", "BER measure CLIs")
TOP_SDKCOMMAND("pdh de3 path ber measure inputerror", "BER measure CLIs")
TOP_SDKCOMMAND("pdh de3 path ber measure polling", "BER measure CLIs")

DEF_SDKCOMMAND(cliPdhDe3PathBerMonitorEnable,
               "pdh de3 path ber monitor enable",
               "Enable BER monitor\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3PathBerMonitorDisable,
               "pdh de3 path ber monitor disable",
               "Disable BER monitor\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3PathBerSdThres,
               "pdh de3 path ber sd",
               "Set BER-SD threshold\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("threshold", "eAtBerRate", "BER-SD threshold"),
               "${de3s} ${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3PathBerSfThres,
               "pdh de3 path ber sf",
               "Set BER-SF threshold\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("threshold", "eAtBerRate", "BER-SF threshold"),
               "${de3s} ${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3PathBerTcaThres,
               "pdh de3 path ber tca",
               "Set BER-TCA threshold\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("threshold", "eAtBerRate", "BER-TCA threshold"),
               "${de3s} ${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3PathBerGet,
               "show pdh de3 path ber",
               "Show BER enable/disable, thresholds and current value\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3PathBerDebug,
               "pdh de3 path ber debug",
               "Enable/disable BER debug\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("enable", "eBool", "Enable/disable BER debug"),
               "${de3s} ${enable}")
    {
    mAtCliCall();
    }


DEF_SDKCOMMAND(cliPdhDe3LineBerMonitorEnable,
               "pdh de3 line ber monitor enable",
               "Enable BER monitor\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3LineBerMonitorDisable,
               "pdh de3 line ber monitor disable",
               "Disable BER monitor\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

/* SET BER SD threshold */
DEF_SDKCOMMAND(cliPdhDe3LineBerSdThres,
               "pdh de3 line ber sd",
               "Set BER-SD threshold\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("threshold", "eAtBerRate", "BER-SD threshold"),
               "${de3s} ${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3LineBerSfThres,
               "pdh de3 line ber sf",
               "Set BER-SF threshold\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("threshold", "eAtBerRate", "BER-SF threshold"),
               "${de3s} ${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3LineBerTcaThres,
               "pdh de3 line ber tca",
               "Set BER-TCA threshold\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("threshold", "eAtBerRate", "BER-TCA threshold"),
               "${de3s} ${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3LineBerGet,
               "show pdh de3 line ber",
               "Show BER enable/disable, thresholds and current value\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3LineBerDebug,
               "pdh de3 line ber debug",
               "Enable/disable BER debug\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("enable", "eBool", "Enable/disable BER debug"),
               "${de3s} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe3LineBerMeasureTimeEngineCreate,
               "pdh de3 line ber measure create",
               "Create measure engine base on soft/hard type.\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("engineType", "eAtBerMeasureEngineType", ""),
               "${de3s} {engineType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe3LineBerMeasureTimeEngineDelete,
               "pdh de3 line ber measure delete",
               "Delete measure engine on this channel.\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe3LineBerMeasureTimeEngineModeSet,
               "pdh de3 line ber measure mode",
               "Set measuring mode.\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("mode", "eAtBerMeasureingMode", ""),
               "${de3s} {mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe3LineBerMeasureTimeEnginePollingStart,
               "pdh de3 line ber measure polling start",
               "Start polling on this BER measure engine.\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe3LineBerMeasureTimeEnginePollingStop,
               "pdh de3 line ber measure polling stop",
               "Stop polling on this BER measure engine.\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe3LineBerMeasureTimeEngineInputErrorConnect,
               "pdh de3 line ber measure inputerror connect",
               "Connect input error for measure detection time.\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe3LineBerMeasureTimeEngineInputErrorDisconnect,
               "pdh de3 line ber measure inputerror disconnect",
               "Dis-connect input error for measure clearing time.\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe3LineBerMeasureTimeEngineEstimatedTimesClear,
               "pdh de3 line ber measure clear",
               "Clear all measured time data.\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe3LineBerMeasureTimeEngineShow,
               "show pdh de3 line ber measure",
               "Show all measured time data.\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe3PathBerMeasureTimeEngineCreate,
               "pdh de3 path ber measure create",
               "Create measure engine base on soft/hard type.\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("engineType", "eAtBerMeasureEngineType", ""),
               "${de3s} {engineType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe3PathBerMeasureTimeEngineDelete,
               "pdh de3 path ber measure delete",
               "Delete measure engine on this channel.\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe3PathBerMeasureTimeEngineModeSet,
               "pdh de3 path ber measure mode",
               "Set measuring mode.\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("mode", "eAtBerMeasureingMode", ""),
               "${de3s} {mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe3PathBerMeasureTimeEnginePollingStart,
               "pdh de3 path ber measure polling start",
               "Start polling on this BER measure engine.\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe3PathBerMeasureTimeEnginePollingStop,
               "pdh de3 path ber measure polling stop",
               "Stop polling on this BER measure engine.\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe3PathBerMeasureTimeEngineInputErrorConnect,
               "pdh de3 path ber measure inputerror connect",
               "Connect input error for measure detection time.\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe3PathBerMeasureTimeEngineInputErrorDisconnect,
               "pdh de3 path ber measure inputerror disconnect",
               "Dis-connect input error for measure clearing time.\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe3PathBerMeasureTimeEngineEstimatedTimesClear,
               "pdh de3 path ber measure clear",
               "Clear all measured time data.\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe3PathBerMeasureTimeEngineShow,
               "show pdh de3 path ber measure",
               "Show all measured time data.\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

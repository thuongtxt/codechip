/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : ClishAtBerControllerPdhDe1.c
 *
 * Created Date: Aug 3, 2015
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("pdh de1 line", "De1 line configuration\n")
TOP_SDKCOMMAND("pdh de1 line ber", "De1 line Ber configuration\n")
TOP_SDKCOMMAND("pdh de1 line ber monitor", "De1 line Ber configuration\n")
TOP_SDKCOMMAND("pdh de1 path", "De1 Path configuration\n")
TOP_SDKCOMMAND("pdh de1 path ber", "De1 Path Ber configuration\n")
TOP_SDKCOMMAND("pdh de1 path ber monitor", "De1 Path Ber configuration\n")
TOP_SDKCOMMAND("show pdh de1 line", "show De1 line configuration\n")
TOP_SDKCOMMAND("show pdh de1 path", "show De1 path configuration\n")

DEF_SDKCOMMAND(cliPdhDe1PathBerMonitorEnable,
               "pdh de1 path ber monitor enable",
               "Enable BER monitor\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1PathBerMonitorDisable,
               "pdh de1 path ber monitor disable",
               "Disable BER monitor\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1PathBerSdThres,
               "pdh de1 path ber sd",
               "Set BER-SD threshold\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("threshold", "eAtBerRate", "BER-SD threshold"),
               "${de1s} ${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1PathBerSfThres,
               "pdh de1 path ber sf",
               "Set BER-SF threshold\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("threshold", "eAtBerRate", "BER-SF threshold"),
               "${de1s} ${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1PathBerTcaThres,
               "pdh de1 path ber tca",
               "Set BER-TCA threshold\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("threshold", "eAtBerRate", "BER-TCA threshold"),
               "${de1s} ${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1PathBerGet,
               "show pdh de1 path ber",
               "Show BER enable/disable, thresholds and current value\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1PathBerDebug,
               "pdh de1 path ber debug",
               "Enable/disable BER debug\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("enable", "eBool", "Enable/disable BER debug"),
               "${de1s} ${enable}")
    {
    mAtCliCall();
    }


DEF_SDKCOMMAND(cliPdhDe1LineBerMonitorEnable,
               "pdh de1 line ber monitor enable",
               "Enable BER monitor\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1LineBerMonitorDisable,
               "pdh de1 line ber monitor disable",
               "Disable BER monitor\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

/* SET BER SD threshold */
DEF_SDKCOMMAND(cliPdhDe1LineBerSdThres,
               "pdh de1 line ber sd",
               "Set BER-SD threshold\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("threshold", "eAtBerRate", "BER-SD threshold"),
               "${de1s} ${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1LineBerSfThres,
               "pdh de1 line ber sf",
               "Set BER-SF threshold\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("threshold", "eAtBerRate", "BER-SF threshold"),
               "${de1s} ${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1LineBerTcaThres,
               "pdh de1 line ber tca",
               "Set BER-TCA threshold\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("threshold", "eAtBerRate", "BER-TCA threshold"),
               "${de1s} ${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1LineBerGet,
               "show pdh de1 line ber",
               "Show BER enable/disable, thresholds and current value\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1LineBerDebug,
               "pdh de1 line ber debug",
               "Enable/disable BER debug\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("enable", "eBool", "Enable/disable BER debug"),
               "${de1s} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe1LineBerMeasureTimeEngineCreate,
               "pdh de1 line ber measure create",
               "Create measure engine base on soft/hard type.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("engineType", "eAtBerMeasureEngineType", ""),
               "${de1s} {engineType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe1LineBerMeasureTimeEngineDelete,
               "pdh de1 line ber measure delete",
               "Delete measure engine on this channel.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe1LineBerMeasureTimeEngineModeSet,
               "pdh de1 line ber measure mode",
               "Set measuring mode.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("mode", "eAtBerMeasureingMode", ""),
               "${de1s} {mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe1LineBerMeasureTimeEnginePollingStart,
               "pdh de1 line ber measure polling start",
               "Start polling on this BER measure engine.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe1LineBerMeasureTimeEnginePollingStop,
               "pdh de1 line ber measure polling stop",
               "Stop polling on this BER measure engine.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe1LineBerMeasureTimeEngineInputErrorConnect,
               "pdh de1 line ber measure inputerror connect",
               "Connect input error for measure detection time.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe1LineBerMeasureTimeEngineInputErrorDisconnect,
               "pdh de1 line ber measure inputerror disconnect",
               "Dis-connect input error for measure clearing time.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe1LineBerMeasureTimeEngineEstimatedTimesClear,
               "pdh de1 line ber measure clear",
               "Clear all measured time data.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe1LineBerMeasureTimeEngineShow,
               "show pdh de1 line ber measure",
               "Show all measured time data.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe1PathBerMeasureTimeEngineCreate,
               "pdh de1 path ber measure create",
               "Create measure engine base on soft/hard type.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("engineType", "eAtBerMeasureEngineType", ""),
               "${de1s} {engineType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe1PathBerMeasureTimeEngineDelete,
               "pdh de1 path ber measure delete",
               "Delete measure engine on this channel.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe1PathBerMeasureTimeEngineModeSet,
               "pdh de1 path ber measure mode",
               "Set measuring mode.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("mode", "eAtBerMeasureingMode", ""),
               "${de1s} {mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe1PathBerMeasureTimeEnginePollingStart,
               "pdh de1 path ber measure polling start",
               "Start polling on this BER measure engine.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe1PathBerMeasureTimeEnginePollingStop,
               "pdh de1 path ber measure polling stop",
               "Stop polling on this BER measure engine.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe1PathBerMeasureTimeEngineInputErrorConnect,
               "pdh de1 path ber measure inputerror connect",
               "Connect input error for measure detection time.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe1PathBerMeasureTimeEngineInputErrorDisconnect,
               "pdh de1 path ber measure inputerror disconnect",
               "Dis-connect input error for measure clearing time.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe1PathBerMeasureTimeEngineEstimatedTimesClear,
               "pdh de1 path ber measure clear",
               "Clear all measured time data.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe1PathBerMeasureTimeEngineShow,
               "show pdh de1 path ber measure",
               "Show all measured time data.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

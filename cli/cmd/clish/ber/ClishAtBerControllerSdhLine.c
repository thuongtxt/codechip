/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : ClishAtBerControllerSdhLine.c
 *
 * Created Date: Sep 30, 2013
 *
 * Description : CLISH CLI to control BER monitoring for SDH Line
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("sdh line ms", "SDH Line MS CLIs\n")
TOP_SDKCOMMAND("sdh line rs", "SDH Line RS CLIs\n")
TOP_SDKCOMMAND("sdh line ms ber", "BER configuration for Multiplex\n")
TOP_SDKCOMMAND("sdh line rs ber", "BER configuration for Regenerator\n")
TOP_SDKCOMMAND("show sdh line rs", "CLIs to show BER information of RS\n")
TOP_SDKCOMMAND("show sdh line ms", "CLIs to show BER information of MS\n")

TOP_SDKCOMMAND("sdh line rs ber measure", "BER measure CLIs for Regenerator ")
TOP_SDKCOMMAND("sdh line rs ber measure inputerror", "BER measure CLIs for Regenerator ")
TOP_SDKCOMMAND("sdh line rs ber measure polling", "BER measure CLIs for Regenerator ")

TOP_SDKCOMMAND("sdh line ms ber measure", "BER measure CLIs for Multiplex ")
TOP_SDKCOMMAND("sdh line ms ber measure inputerror", "BER measure CLIs for Multiplex ")
TOP_SDKCOMMAND("sdh line ms ber measure polling", "BER measure CLIs for Multiplex ")


DEF_SDKCOMMAND(cliSdhLineBerRsMonitorEnable,
               "sdh line rs ber enable",
               "Enable BER monitor\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
	AtUnused(argv); mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineBerRsMonitorDisable,
               "sdh line rs ber disable",
               "Disable BER monitor\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineBerRsSdThres,
               "sdh line rs ber sd",
               "Set BER-SD threshold\n",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("threshold", "eAtBerRate", "BER-SD threshold"),
               "${lineList} ${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineBerRsSfThres,
               "sdh line rs ber sf",
               "Set BER-SF threshold\n",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("threshold", "eAtBerRate", "BER-SF threshold"),
               "${lineList} ${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineBerRsTcaThres,
               "sdh line rs ber tca",
               "Set BER-TCA threshold\n",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("threshold", "eAtBerRate", "BER-TCA threshold"),
               "${lineList} ${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineBerRsGet,
               "show sdh line rs ber",
               "Show BER enable/disable, thresholds and current value\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineBerRsDebug,
               "sdh line rs ber debug",
               "Enable/disable BER debug\n",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("enable", "eBool", "Enable/disable BER debug"),
               "${lineList} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineBerMsMonitorEnable,
               "sdh line ms ber enable",
               "Enable BER monitor\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineBerMsMonitorDisable,
               "sdh line ms ber disable",
               "Disable BER monitor\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineBerMsSdThres,
               "sdh line ms ber sd",
               "Set BER-SD threshold\n",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("threshold", "eAtBerRate", "BER-SD threshold"),
               "${lineList} ${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineBerMsSfThres,
               "sdh line ms ber sf",
               "Set BER-SF threshold\n",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("threshold", "eAtBerRate", "BER-SF threshold"),
               "${lineList} ${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineBerMsTcaThres,
               "sdh line ms ber tca",
               "Set BER-TCA threshold\n",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("threshold", "eAtBerRate", "BER-TCA threshold"),
               "${lineList} ${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineBerMsGet,
               "show sdh line ms ber",
               "Show BER enable/disable, thresholds and current value\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineBerMsDebug,
               "sdh line ms ber debug",
               "Enable/disable BER debug\n",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("enable", "eBool", "Enable/disable BER debug"),
               "${lineList} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliSdhLineRsBerMeasureTimeEngineCreate,
               "sdh line rs ber measure create",
               "Create measure engine base on soft/hard type.\n",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("engineType", "eAtBerMeasureEngineType", ""),
               "${lineList} {engineType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliSdhLineRsBerMeasureTimeEngineDelete,
               "sdh line rs ber measure delete",
               "Delete measure engine on this channel.\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliSdhLineRsBerMeasureTimeEngineModeSet,
               "sdh line rs ber measure mode",
               "Set measuring mode.\n",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("mode", "eAtBerMeasureingMode", ""),
               "${lineList} {mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliSdhLineRsBerMeasureTimeEnginePollingStart,
               "sdh line rs ber measure polling start",
               "Start polling on this BER measure engine.\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliSdhLineRsBerMeasureTimeEnginePollingStop,
               "sdh line rs ber measure polling stop",
               "Stop polling on this BER measure engine.\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliSdhLineRsBerMeasureTimeEngineInputErrorConnect,
               "sdh line rs ber measure inputerror connect",
               "Connect input error for measure detection time.\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliSdhLineRsBerMeasureTimeEngineInputErrorDisconnect,
               "sdh line rs ber measure inputerror disconnect",
               "Dis-connect input error for measure clearing time.\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliSdhLineRsBerMeasureTimeEngineEstimatedTimesClear,
               "sdh line rs ber measure clear",
               "Clear all measured time data.\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliSdhLineRsBerMeasureTimeEngineShow,
               "show sdh line rs ber measure",
               "Show all measured time data.\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliSdhLineMsBerMeasureTimeEngineCreate,
               "sdh line ms ber measure create",
               "Create measure engine base on soft/hard type.\n",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("engineType", "eAtBerMeasureEngineType", ""),
               "${lineList} {engineType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliSdhLineMsBerMeasureTimeEngineDelete,
               "sdh line ms ber measure delete",
               "Delete measure engine on this channel.\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliSdhLineMsBerMeasureTimeEngineModeSet,
               "sdh line ms ber measure mode",
               "Set measuring mode.\n",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("mode", "eAtBerMeasureingMode", ""),
               "${lineList} {mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliSdhLineMsBerMeasureTimeEnginePollingStart,
               "sdh line ms ber measure polling start",
               "Start polling on this BER measure engine.\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliSdhLineMsBerMeasureTimeEnginePollingStop,
               "sdh line ms ber measure polling stop",
               "Stop polling on this BER measure engine.\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliSdhLineMsBerMeasureTimeEngineInputErrorConnect,
               "sdh line ms ber measure inputerror connect",
               "Connect input error for measure detection time.\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliSdhLineMsBerMeasureTimeEngineInputErrorDisconnect,
               "sdh line ms ber measure inputerror disconnect",
               "Dis-connect input error for measure clearing time.\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliSdhLineMsBerMeasureTimeEngineEstimatedTimesClear,
               "sdh line ms ber measure clear",
               "Clear all measured time data.\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliSdhLineMsBerMeasureTimeEngineShow,
               "show sdh line ms ber measure",
               "Show all measured time data.\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

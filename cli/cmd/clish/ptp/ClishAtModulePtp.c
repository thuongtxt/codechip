/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PTP
 *
 * File        : ClishAtModulePtp.c
 *
 * Created Date: Jul 3, 2018
 *
 * Description : Module PTP CLISH(es)
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/
#define AlarmType "(cpuqueue)"
#define AlarmMask "((("AlarmType"[\\|])+"AlarmType")|"AlarmType")"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("ptp", "PTP Module CLIs")
TOP_SDKCOMMAND("ptp device", "PTP device CLIs")
TOP_SDKCOMMAND("ptp pps", "PTP PPS CLIs")
TOP_SDKCOMMAND("ptp correct", "PTP Correction CLIs")
TOP_SDKCOMMAND("show ptp t1t3", "PTP T1'/T3' capture CLIs")
TOP_SDKCOMMAND("show ptp t1t3 capture", "PTP T1'/T3' capture CLIs")
TOP_SDKCOMMAND("ptp t1t3", "PTP T1'/T3' capture CLIs")
TOP_SDKCOMMAND("ptp t1t3 capture", "PTP T1'/T3' capture CLIs")
TOP_SDKCOMMAND("ptp interrupt", "PTP interrupt capture CLIs")
TOP_SDKCOMMAND("ptp alarm", "PTP interrupt capture CLIs")
TOP_SDKCOMMAND("ptp alarm capture", "PTP interrupt capture CLIs")
TOP_SDKCOMMAND("ptp vlan", "PTP VLAN CLIs")
TOP_SDKCOMMAND("show ptp vlan", "PTP VLAN CLIs")
TOP_SDKCOMMAND("ptp timestamp", "PTP timestamp CLIs")
TOP_SDKCOMMAND("ptp timestamp bypass", "PTP timestamp CLIs")

DEF_PTYPE("eAtPtpDeviceType", "select", "ordinary(1), boundary(2), transparent(3)", "PTP device type")
DEF_PTYPE("eAtPtpPpsSource", "select", "internal(1), external(2)", "PTP PPS source\n")
DEF_PTYPE("eAtPtpCorrectionMode", "select", "standard(1), assistant(2)", "PTP correction mode\n")
DEF_PTYPE("eAtPtpT1T3CaptureMode", "select", "none(0), cpu(1), packet(2)", "PTP T1'/T3' capture mode\n")

DEF_PTYPE("PtpAlarm", "select", "cpuqueue(1)", "PTP module alarms")
DEF_PTYPE("PtpAlarmMask", "regexp", AlarmMask,
          "\nPTP module alarm masks:\n"
          "- cpuqueue   : CPU queue interrupt\n")

DEF_SDKCOMMAND(CliAtModulePtpShow,
               "show module ptp",
               "Show PTP Module information\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModulePtpDeviceTypeSet,
               "ptp device type",
               "Set PTP device type",
               DEF_SDKPARAM("type", "eAtPtpDeviceType", ""),
               "${type}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModulePtpDeviceDestMacSet,
               "ptp device destmac",
               "Set PTP device unicast MAC address",
               DEF_SDKPARAM("mac", "MacAddr", ""),
               "${mac}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModulePtpDeviceDestMacEnable,
               "ptp device destmac enable",
               "Enable PTP device unicast MAC address classifying",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModulePtpDeviceDestMacDisable,
               "ptp device destmac disable",
               "Disable PTP device unicast MAC address classifying",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModulePtpDebug,
               "debug module ptp",
               "Show debugging information of PTP module\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModulePtpPpsSourceSet,
               "ptp pps source",
               "Set PPS reference source",
               DEF_SDKPARAM("source", "eAtPtpPpsSource", ""),
               "${source}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModulePtpCorrectionModeSet,
               "ptp correct mode",
               "Set PTP correction mode for a PTP device running Transparent Clock",
               DEF_SDKPARAM("mode", "eAtPtpCorrectionMode", ""),
               "${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModulePtpT1T3CaptureModeSet,
               "ptp t1t3 capture mode",
               "Set PTP T1'/T3' capture mode for a PTP device running Boundary Clock",
               DEF_SDKPARAM("mode", "eAtPtpT1T3CaptureMode", ""),
               "${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModulePtpT1T3CaptureContentShow,
               "show ptp t1t3 capture content",
               "Query t1t3 capture content of the PTP module\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPtpIntrEnable,
               "ptp interrupt enable",
               "Enable PTP module interrupt\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPtpIntrDisable,
               "ptp interrupt disable",
               "Disable PTP module interrupt\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPtpIntrMsk,
               "ptp interruptmask",
               "Set interrupt masks\n",
               DEF_SDKPARAM("mask", "PtpAlarmMask", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${mask} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPtpShowAlarm,
               "show ptp alarm",
               "Show module PTP alarm\n",
               DEF_NULLPARAM,
                          "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPtpShowInterrupt,
               "show ptp interrupt",
               "Show module ptp interrupt\n",
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", "Read to clear without display table"),
               "${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPtpInterruptCaptureEnable,
               "ptp alarm capture enable",
               "Enable alarm capture",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPtpInterruptCaptureDisable,
               "ptp alarm capture disable",
               "Disable alarm capture",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtPtpExpectVlanSet,
               "ptp vlan expect",
               "Set global expected VLAN-ID for PTP",
               DEF_SDKPARAM("indexList", "UINT", "an index starts from 1")
               DEF_SDKPARAM("aVlan", "VlanDesc", ""),
               "${indexList} ${aVlan}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtPtpExpectVlanShow,
               "show ptp vlan expect",
               "Show global expected VLAN for PTP",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModulePtpDeviceToDSet,
               "ptp device tod",
               "Set PTP device ToD",
               DEF_SDKPARAM("tod", "STRING", ""),
               "${tod}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModulePtpTimestampBypassEnable,
               "ptp timestamp bypass enable",
               "Enable PTP timestamp bypass for debugging",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModulePtpTimestampBypassDisable,
               "ptp timestamp bypass disable",
               "Disable PTP timestamp bypass for normal operation",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PTP
 *
 * File        : ClishAtPtpPsnGroup.c
 *
 * Created Date: Aug 3, 2018
 *
 * Description : PTP PSN multicast group Clishes
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("ptp eth", "PTP Ethernet CLIs")
TOP_SDKCOMMAND("ptp eth multicast", "PTP Ethernet multicast (address) CLIs")
TOP_SDKCOMMAND("ptp eth multicast group", "PTP Ethernet multicast group CLIs")
TOP_SDKCOMMAND("show ptp eth", "PTP Ethernet show CLIs")
TOP_SDKCOMMAND("show ptp eth multicast", "PTP Ethernet multicast show CLIs")

TOP_SDKCOMMAND("ptp ipv4", "PTP IPv4 CLIs")
TOP_SDKCOMMAND("ptp ipv4 multicast", "PTP IPv4 multicast (address) CLIs")
TOP_SDKCOMMAND("ptp ipv4 multicast group", "PTP IPv4 multicast group CLIs")
TOP_SDKCOMMAND("show ptp ipv4", "PTP IPv4 show CLIs")
TOP_SDKCOMMAND("show ptp ipv4 multicast", "PTP IPv4 multicast show CLIs")

TOP_SDKCOMMAND("ptp ipv6", "PTP IPv6 CLIs")
TOP_SDKCOMMAND("ptp ipv6 multicast", "PTP IPv6 multicast (address) CLIs")
TOP_SDKCOMMAND("ptp ipv6 multicast group", "PTP IPv6 multicast group CLIs")
TOP_SDKCOMMAND("show ptp ipv6", "PTP IPv6 show CLIs")
TOP_SDKCOMMAND("show ptp ipv6 multicast", "PTP IPv6 multicast show CLIs")

DEF_SDKCOMMAND(cliAtPtpPsnGroupEthMacAddressSet,
               "ptp eth multicast group address",
               "Set PTP multicast MAC group address",
               DEF_SDKPARAM("groupList", "GroupIdList", "")
               DEF_SDKPARAM_OPTION("address", "MacAddr", ""),
               "${groupList} ${address}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpPsnGroupEthShow,
               "show ptp eth multicast group",
               "Show configuration of PTP multicast MAC group",
               DEF_SDKPARAM("groupList", "GroupIdList", ""),
               "${groupList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpPsnGroupIpV4AddressSet,
               "ptp ipv4 multicast group address",
               "Set PTP multicast IPv4 group address",
               DEF_SDKPARAM("groupList", "GroupIdList", "")
               DEF_SDKPARAM("address", "IPV4ADDR", ""),
               "${groupList} ${address}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpPsnGroupIpV4PortAdd,
               "ptp ipv4 multicast group add",
               "Add PTP ports into multicast IPv4 group",
               DEF_SDKPARAM("groupList", "GroupIdList", "")
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${groupList} ${portList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpPsnGroupIpV4PortRemove,
               "ptp ipv4 multicast group remove",
               "Remove PTP ports into multicast IPv4 group",
               DEF_SDKPARAM("groupList", "GroupIdList", "")
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${groupList} ${portList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpPsnGroupIpV4Show,
               "show ptp ipv4 multicast group",
               "Show configuration of PTP multicast IPv4 group",
               DEF_SDKPARAM("groupList", "GroupIdList", ""),
               "${groupList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpPsnGroupIpV6AddressSet,
               "ptp ipv6 multicast group address",
               "Set PTP multicast IPv6 group address",
               DEF_SDKPARAM("groupList", "GroupIdList", "")
               DEF_SDKPARAM("address", "IPV6ADDR", ""),
               "${groupList} ${address}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpPsnGroupIpV6PortAdd,
               "ptp ipv6 multicast group add",
               "Add PTP ports into multicast IPv6 group",
               DEF_SDKPARAM("groupList", "GroupIdList", "")
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${groupList} ${portList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpPsnGroupIpV6PortRemove,
               "ptp ipv6 multicast group remove",
               "Remove PTP ports into multicast IPv6 group",
               DEF_SDKPARAM("groupList", "GroupIdList", "")
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${groupList} ${portList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpPsnGroupIpV6Show,
               "show ptp ipv6 multicast group",
               "Show configuration of PTP multicast IPv6 group",
               DEF_SDKPARAM("groupList", "GroupIdList", ""),
               "${groupList}")
    {
    mAtCliCall();
    }

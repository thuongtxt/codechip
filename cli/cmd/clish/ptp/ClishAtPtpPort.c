/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PTP
 *
 * File        : ClishAtPtpPort.c
 *
 * Created Date: Jul 3, 2018
 *
 * Description : PTP port CLISHes
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("ptp port", "PTP port CLIs")
TOP_SDKCOMMAND("ptp port step", "PTP port step CLIs")
TOP_SDKCOMMAND("ptp port tx", "PTP port TX CLIs")
TOP_SDKCOMMAND("ptp port tx delay", "PTP port TX Delay compensation CLIs")
TOP_SDKCOMMAND("ptp port rx", "PTP port RX CLIs")
TOP_SDKCOMMAND("ptp port rx delay", "PTP port RX Delay compensation CLIs")
TOP_SDKCOMMAND("show ptp", "Show PTP CLIs")

DEF_PTYPE("eAtPtpPortState", "select", "master(1), slave(2), passive(3)", "PTP port state")
DEF_PTYPE("eAtPtpStepMode", "select", "one-step(1), two-step(2)", "PTP step mode")
DEF_PTYPE("eAtPtpTransportType", "select", "l2tp(1), ipv4(2), ipv6(3), ipv4vpn(4), ipv6vpn(5), any(6)", "PTP transport protocol type")

DEF_SDKCOMMAND(cliAtPtpPortEnable,
               "ptp port enable",
               "Enable specified PTP ports",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpPortDisable,
               "ptp port disable",
               "Disable specified PTP ports",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpPortStateSet,
               "ptp port state",
               "Set PTP port state",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("state", "eAtPtpPortState", ""),
               "${portList} ${state}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpPortStepModeSet,
               "ptp port step mode",
               "Set PTP port step mode",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("step", "eAtPtpStepMode", ""),
               "${portList} ${step}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpPortTransportTypeSet,
               "ptp port transport",
               "Set PTP Transport protocol type",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("transport", "eAtPtpTransportType", ""),
               "${portList} ${transport}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpPortShow,
               "show ptp port",
               "Show PTP port configuration",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpPortCountersShow,
               "show ptp port counters",
               "Show PTP port counters",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${portList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpPortTxDelayAdjustSet,
               "ptp port tx delay adjust",
               "Set PTP port TX delay compensation",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("nanoseconds", "UINT", ""),
               "${portList} ${nanoseconds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpPortRxDelayAdjustSet,
               "ptp port rx delay adjust",
               "Set PTP port RX delay compensation",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("nanoseconds", "UINT", ""),
               "${portList} ${nanoseconds}")
    {
    mAtCliCall();
    }

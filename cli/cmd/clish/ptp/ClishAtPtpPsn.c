/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PTP
 *
 * File        : ClishAtPtpPsn.c
 *
 * Created Date: Jul 4, 2018
 *
 * Description : PTP PSN Clishes
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/
#define IPV4SEG     "(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])"
#define IPV4ADDR    "("IPV4SEG"\\.){3,3}"IPV4SEG

/* # 1:2:3:4:5:6:7:8
 * # 1::                                 1:2:3:4:5:6:7::
 * # 1::8               1:2:3:4:5:6::8   1:2:3:4:5:6::8
 * # 1::7:8             1:2:3:4:5::7:8   1:2:3:4:5::8
 * # 1::6:7:8           1:2:3:4::6:7:8   1:2:3:4::8
 * # 1::5:6:7:8         1:2:3::5:6:7:8   1:2:3::8
 * # 1::4:5:6:7:8       1:2::4:5:6:7:8   1:2::8
 * # 1::3:4:5:6:7:8     1::3:4:5:6:7:8   1::8
 * # ::2:3:4:5:6:7:8    ::2:3:4:5:6:7:8  ::8       ::
 * # fe80::7:8%eth0     fe80::7:8%1  (link-local IPv6 addresses with zone index)
 * # ::255.255.255.255  ::ffff:255.255.255.255  ::ffff:0:255.255.255.255 (IPv4-mapped IPv6 addresses and IPv4-translated addresses)
 * # 2001:db8:3:4::192.0.2.33  64:ff9b::192.0.2.33 (IPv4-Embedded IPv6 Address)
 */
#define IPV6SEG     "[0-9a-fA-F]{1,4}"
#define IPV6ADDR    "("                                         \
                    "("IPV6SEG":){7,7}"IPV6SEG"|"               \
                    "("IPV6SEG":){1,7}:|"                       \
                    "("IPV6SEG":){1,6}:"IPV6SEG"|"              \
                    "("IPV6SEG":){1,5}(:"IPV6SEG"){1,2}|"       \
                    "("IPV6SEG":){1,4}(:"IPV6SEG"){1,3}|"       \
                    "("IPV6SEG":){1,3}(:"IPV6SEG"){1,4}|"       \
                    "("IPV6SEG":){1,2}(:"IPV6SEG"){1,5}|"       \
                    IPV6SEG":((:"IPV6SEG"){1,6})|"              \
                    ":((:"IPV6SEG"){1,7}|:)|"                   \
                    "fe80:(:"IPV6SEG"){0,4}%[0-9a-zA-Z]{1,}|"   \
                    "::(ffff(:0{1,4}){0,1}:){0,1}"IPV4ADDR"|"   \
                    "("IPV6SEG":){1,4}:"IPV4ADDR                \
                    ")"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("show ptp port vlan", "PTP VLAN CLIs")
TOP_SDKCOMMAND("ptp port vlan", "PTP VLAN CLIs")
TOP_SDKCOMMAND("ptp port eth", "PTP Ethernet CLIs")
TOP_SDKCOMMAND("ptp port eth expect", "PTP Ethernet expect (address) CLIs")
TOP_SDKCOMMAND("ptp port eth expect unicast", "PTP Ethernet expect unicast (address) CLIs")
TOP_SDKCOMMAND("ptp port eth expect multicast", "PTP Ethernet expect multicast (address) CLIs")
TOP_SDKCOMMAND("ptp port eth expect anycast", "PTP Ethernet expect anycast (address) CLIs")
TOP_SDKCOMMAND("ptp port ipv4", "PTP IPv4 PSN CLIs")
TOP_SDKCOMMAND("ptp port ipv4 expect", "PTP PSN IPv4 expect (address) CLIs")
TOP_SDKCOMMAND("ptp port ipv4 expect unicast", "PTP PSN IPv4 expect unicast (address) CLIs")
TOP_SDKCOMMAND("ptp port ipv4 expect multicast", "PTP PSN IPv4 expect multicast (address) CLIs")
TOP_SDKCOMMAND("ptp port ipv4 expect anycast", "PTP PSN IPv4 expect anycast (address) CLIs")
TOP_SDKCOMMAND("ptp port ipv6", "PTP IPv6 PSN CLIs")
TOP_SDKCOMMAND("ptp port ipv6 expect", "PTP PSN IPv6 expect (address) CLIs")
TOP_SDKCOMMAND("ptp port ipv6 expect unicast", "PTP PSN IPv6 expect unicast (address) CLIs")
TOP_SDKCOMMAND("ptp port ipv6 expect multicast", "PTP PSN IPv6 expect multicast (address) CLIs")
TOP_SDKCOMMAND("ptp port ipv6 expect anycast", "PTP PSN IPv6 expect anycast (address) CLIs")

DEF_PTYPE("IPV4ADDR", "regexp", IPV4ADDR, "IPv4 Address")
DEF_PTYPE("IPV6ADDR", "regexp", IPV6ADDR, "IPv6 Address")

DEF_SDKCOMMAND(cliAtPtpEthExpectedUnicastDestAddressSet,
               "ptp port eth expect unicast dest",
               "Set PTP expected unicast MAC address",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("enable", "eBool", "")
               DEF_SDKPARAM_OPTION("address", "MacAddr", ""),
               "${portList} ${enable} ${address}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpEthExpectedMcastDestAddressSet,
               "ptp port eth expect multicast dest",
               "Set PTP expected multicast MAC address",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("enable", "eBool", "")
               DEF_SDKPARAM_OPTION("address", "MacAddr", ""),
               "${portList} ${enable} ${address}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpEthExpectedAnycastDestAddressSet,
               "ptp port eth expect anycast dest",
               "Set PTP expected anycast dest. MAC address",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${portList} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpEthExpectedUnicastSrcAddressSet,
               "ptp port eth expect unicast source",
               "Set PTP expected unicast source MAC address",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("enable", "eBool", "")
               DEF_SDKPARAM_OPTION("address", "MacAddr", ""),
               "${portList} ${enable} ${address}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpEthExpectedAnycastSourceAddressSet,
               "ptp port eth expect anycast source",
               "Set PTP expected anycast source MAC address",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${portList} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpPortEthShow,
               "show ptp port eth",
               "Show Ethernet configuration of PTP port",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpIPv4PsnExpectedUnicastDestAddressSet,
               "ptp port ipv4 expect unicast dest",
               "Set PTP expected unicast IPv4 address",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("entry", "UINT", "Address Entry")
               DEF_SDKPARAM("enable", "eBool", "")
               DEF_SDKPARAM_OPTION("address", "IPV4ADDR", ""),
               "${portList} ${entry} ${enable} ${address}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpIPv4PsnExpectedMcastDestAddressSet,
               "ptp port ipv4 expect multicast dest",
               "Set PTP expected multicast IPv4 address",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("enable", "eBool", "")
               DEF_SDKPARAM_OPTION("address", "IPV4ADDR", ""),
               "${portList} ${enable} ${address}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpIPv4PsnExpectedAnycastDestAddressSet,
               "ptp port ipv4 expect anycast dest",
               "Set PTP expected anycast dest. IPv4 address",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${portList} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpIPv4PsnExpectedUnicastSrcAddressSet,
               "ptp port ipv4 expect unicast source",
               "Set PTP expected unicast source IPv4 address",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("entry", "UINT", "Address Entry")
               DEF_SDKPARAM("enable", "eBool", "")
               DEF_SDKPARAM_OPTION("address", "IPV4ADDR", ""),
               "${portList} ${entry} ${enable} ${address}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpIPv4PsnExpectedAnycastSourceAddressSet,
               "ptp port ipv4 expect anycast source",
               "Set PTP expected anycast source IPv4 address",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${portList} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpPortIpV4Show,
               "show ptp port ipv4",
               "Show IPv4 configuration of PTP port",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpIPv6PsnExpectedUnicastDestAddressSet,
               "ptp port ipv6 expect unicast dest",
               "Set PTP expected unicast IPv6 address",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("entry", "UINT", "Address Entry")
               DEF_SDKPARAM("enable", "eBool", "")
               DEF_SDKPARAM_OPTION("address", "IPV6ADDR", ""),
               "${portList} ${entry} ${enable} ${address}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpIPv6PsnExpectedMcastDestAddressSet,
               "ptp port ipv6 expect multicast dest",
               "Set PTP expected multicast IPv6 address",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("enable", "eBool", "")
               DEF_SDKPARAM_OPTION("address", "IPV6ADDR", ""),
               "${portList} ${enable} ${address}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpIPv6PsnExpectedAnycastDestAddressSet,
               "ptp port ipv6 expect anycast dest",
               "Set PTP expected anycast dest. IPv6 address",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${portList} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpIPv6PsnExpectedUnicastSrcAddressSet,
               "ptp port ipv6 expect unicast source",
               "Set PTP expected unicast source IPv6 address",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("entry", "UINT", "Address Entry")
               DEF_SDKPARAM("enable", "eBool", "")
               DEF_SDKPARAM_OPTION("address", "IPV6ADDR", ""),
               "${portList} ${entry} ${enable} ${address}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpIPv6PsnExpectedAnycastSourceAddressSet,
               "ptp port ipv6 expect anycast source",
               "Set PTP expected anycast source IPv6 address",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${portList} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpPortIpV6Show,
               "show ptp port ipv6",
               "Show IPv6 configuration of PTP port",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpEthExpectedDestAddressSet,
               "ptp port eth expect dest",
               "Set PTP expected MAC address",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("enable", "eBool", "")
               DEF_SDKPARAM_OPTION("address", "MacAddr", ""),
               "${portList} ${enable} ${address}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpEthExpectedSrcAddressSet,
               "ptp port eth expect source",
               "Set PTP expected source MAC address",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("enable", "eBool", "")
               DEF_SDKPARAM_OPTION("address", "MacAddr", ""),
               "${portList} ${enable} ${address}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpIPv4PsnExpectedDestAddressSet,
               "ptp port ipv4 expect dest",
               "Set PTP expected IPv4 address",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("entry", "UINT", "Address Entry")
               DEF_SDKPARAM("enable", "eBool", "")
               DEF_SDKPARAM_OPTION("address", "IPV4ADDR", ""),
               "${portList} ${entry} ${enable} ${address}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpIPv4PsnExpectedSrcAddressSet,
               "ptp port ipv4 expect source",
               "Set PTP expected source IPv4 address",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("entry", "UINT", "Address Entry")
               DEF_SDKPARAM("enable", "eBool", "")
               DEF_SDKPARAM_OPTION("address", "IPV4ADDR", ""),
               "${portList} ${entry} ${enable} ${address}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpIPv6PsnExpectedDestAddressSet,
               "ptp port ipv6 expect dest",
               "Set PTP expected IPv6 address",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("entry", "UINT", "Address Entry")
               DEF_SDKPARAM("enable", "eBool", "")
               DEF_SDKPARAM_OPTION("address", "IPV6ADDR", ""),
               "${portList} ${entry} ${enable} ${address}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpIPv6PsnExpectedSrcAddressSet,
               "ptp port ipv6 expect source",
               "Set PTP expected source IPv6 address",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("entry", "UINT", "Address Entry")
               DEF_SDKPARAM("enable", "eBool", "")
               DEF_SDKPARAM_OPTION("address", "IPV6ADDR", ""),
               "${portList} ${entry} ${enable} ${address}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpPortAnyEthShow,
               "show ptp port eth any",
               "Show eth layer configuration of PTP port which is not separate unicast and multicast addresses",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpPortAnyIpV4Show,
               "show ptp port ipv4 any",
               "Show IPv4 configuration of PTP port which is not separate unicast and multicast addresses",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtpPortAnyIpV6Show,
               "show ptp port ipv6 any",
               "Show IPv6 configuration of PTP port which is not separate unicast and multicast addresses",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtPtpPortExpectVlanSet,
               "ptp port vlan expect",
               "Set expected VLAN for PTP port",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("cVlan", "VlanDesc", "")
               DEF_SDKPARAM("sVlan", "VlanDesc", ""),
               "${portList} ${cVlan} ${sVlan}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtPtpPortExpectVlanShow,
               "show ptp port vlan expect",
               "Show expected VLAN for PTP ports",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    mAtCliCall();
    }

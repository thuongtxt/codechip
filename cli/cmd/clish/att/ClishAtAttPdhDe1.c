/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ATT
 *
 * File        : ClishAtAttPdhDe1.c
 *
 * Created Date: Dec 24, 2015
 *
 * Description : DS1-E1 Clish
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"
#include "AtCli.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/* Top commands */
TOP_SDKCOMMAND("att pdh de1", "ATT PDH DE1 command\n")
TOP_SDKCOMMAND("att pdh de1 force", "ATT PDH DE1 force command\n")
TOP_SDKCOMMAND("att pdh de1 tx", "ATT PDH DE1 Tx command\n")
TOP_SDKCOMMAND("att pdh de1 tx error", "ATT PDH DE1 Tx Error command\n")
TOP_SDKCOMMAND("att pdh de1 tx error generator", "ATT PDH DE1 Tx Error Generator command\n")
TOP_SDKCOMMAND("att pdh de1 tx alarm", "ATT PDH DE1 Tx alarm command\n")
TOP_SDKCOMMAND("att pdh de1 tx alarm generator", "ATT PDH DE1 Tx alarm Generator command\n")
TOP_SDKCOMMAND("att pdh de1 tx signaling", "ATT PDH DE1 Tx alarm command\n")
TOP_SDKCOMMAND("att pdh de1 rx", "ATT PDH DE1 Rx command\n")
TOP_SDKCOMMAND("att pdh de1 rx alarm", "ATT PDH DE1 Rx alarm command\n")
TOP_SDKCOMMAND("att pdh de1 rx alarm capture", "ATT PDH DE1 Rx alarm capture command\n")
TOP_SDKCOMMAND("att pdh de1 rx signaling", "ATT PDH DE1 Tx alarm command\n")
TOP_SDKCOMMAND("att pdh de1 wander", "ATT PDH DE1 Wander command\n")
TOP_SDKCOMMAND("att pdh de1 signaling", "ATT PDH DE1 Signaling command\n")
TOP_SDKCOMMAND("att show pdh de1", "ATT show PDH DE1 command\n")
TOP_SDKCOMMAND("att show pdh de1 force", "ATT show PDH DE1 force command\n")
TOP_SDKCOMMAND("att show pdh de1 rx", "ATT show PDH DE1 Rx command\n")
TOP_SDKCOMMAND("att show pdh de1 rx alarm", "ATT show PDH DE1 Rx Alarm command\n")
TOP_SDKCOMMAND("debug att", "ATT PDH DE1 Tx alarm command\n")
TOP_SDKCOMMAND("debug att pdh", "ATT PDH DE1 Tx alarm command\n")
TOP_SDKCOMMAND("debug att pdh de1", "ATT PDH DE1 Tx alarm command\n")
TOP_SDKCOMMAND("debug att pdh de1 tx", "ATT PDH DE1 Tx alarm command\n")
TOP_SDKCOMMAND("debug att pdh de1 rx", "ATT PDH DE1 Tx alarm command\n")
TOP_SDKCOMMAND("debug att pdh de1 tx signaling", "ATT PDH DE1 Tx alarm command\n")
TOP_SDKCOMMAND("debug att pdh de1 rx signaling", "ATT PDH DE1 Tx alarm command\n")
DEF_PTYPE("eAttPdhDe1ErrorType", "select", "fbit(1), crc(2)", "\nError type:")
DEF_PTYPE("eAttPdhDe1AlarmType", "select", "los(1), ais(2), rai(3), lof(4), sigrai(5), siglof(6)", "\nAlarm type:")
DEF_PTYPE("eAtPdhDe1FbitType", "select", "fas(0), nfas(1), crcmfas(2), mfas(3)", "\nFbit Force Mode:")
DEF_PTYPE("sigPrbsMode", "select", "sequence(0), fixed_ab(1), fixed_abcd(2), prbs(3)", "\nConfigure signaling prbs mode:")

DEF_SDKCOMMAND(CliAtAttPdhDe1ForceErrorMode,
               "att pdh de1 force error mode",
               "Set mode to generate error for DE1s\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe1ErrorType", "")
               DEF_SDKPARAM("mode", "AttForceErrorMode", "Error Forcing Mode"),
               "${de1s} ${errorType} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1TxErrorGeneratorMode,
               "att pdh de1 tx error generator mode",
               "Set mode to generate error for DE1s\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe1ErrorType", "")
               DEF_SDKPARAM("mode", "AttForceErrorMode", "Error Forcing Mode"),
               "${de1s} ${errorType} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1ForceErrorRate,
               "att pdh de1 force error rate",
               "Set Rate to force error for DE1s\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe1ErrorType", "")
               DEF_SDKPARAM("rate", "eAtBerRate", "Error Rate"),
               "${de1s} ${errorType} ${rate}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1TxErrorGeneratorRate,
               "att pdh de1 tx error generator rate",
               "Set Rate to force error for DE1s\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe1ErrorType", "")
               DEF_SDKPARAM("rate", "eAtBerRate", "Error Rate"),
               "${de1s} ${errorType} ${rate}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1ForceErrorDuration,
               "att pdh de1 force error duration",
               "Force error per second\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe1ErrorType", "DE1 Error Type")
               DEF_SDKPARAM("duration", "UINT", "Duration in ms"),
               "${de1s} ${errorType} ${duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1TxErrorGeneratorDuration,
               "att pdh de1 tx error generator duration",
               "Force error per second\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe1ErrorType", "DE1 Error Type")
               DEF_SDKPARAM("duration", "UINT", "Duration in ms"),
               "${de1s} ${errorType} ${duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1ForceErrorPerSecond,
               "att pdh de1 force error persecond",
               "Force error per second\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe1ErrorType", "DE1 Error Type")
               DEF_SDKPARAM("enable", "eBool", "Enable or Disable"),
               "${de1s} ${errorType} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1ForceError,
               "att pdh de1 force error",
               "Force error\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe1ErrorType", "DE1 Error Type")
               DEF_SDKPARAM("enable", "eBool", "Enable or Disable Trigger force Errors"),
               "${de1s} ${errorType} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1ForceErrorNumErrorsSet,
               "att pdh de1 force error numerrors",
               "Set number of errors to force for DE1s\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe1ErrorType", "")
               DEF_SDKPARAM("numErrors", "UINT", "Number of error each second"),
               "${de1s} ${errorType} ${numErrors}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1TxErrorGeneratorNumErrorsSet,
               "att pdh de1 tx error generator numerrors",
               "Set number of errors to force for DE1s\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe1ErrorType", "")
               DEF_SDKPARAM("numErrors", "UINT", "Number of error each second"),
               "${de1s} ${errorType} ${numErrors}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1ForceErrorBitPositionSet,
               "att pdh de1 force error bitposition",
               "Set bit position\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe1ErrorType", "PDH DE1 Error Type")
               DEF_SDKPARAM("bitposition", "BitPositionList", ""),
               "${de1s} ${errorType} ${bitposition}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1ForceErrorDataMaskSet,
               "att pdh de1 force error datamask",
               "Set data mask\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe1ErrorType", "PDH DE1 Error Type")
               DEF_SDKPARAM("dataMask", "BitMaskList", "Data Bit Mask List"),
               "${de1s} ${errorType} ${dataMask}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1ForceErrorCrcMaskSet,
               "att pdh de1 force error crcmask",
               "Set data mask\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe1ErrorType", "PDH DE1 Error Type")
               DEF_SDKPARAM("crcMask", "BitMaskList", "Data Bit Mask List"),
               "${de1s} ${errorType} ${crcMask}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1ForceErrorCrcMask2Set,
               "att pdh de1 force error crcmask2",
               "Set data mask\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe1ErrorType", "PDH DE1 Error Type")
               DEF_SDKPARAM("crcMask2", "BitMaskList", "Data Bit Mask List"),
               "${de1s} ${errorType} ${crcMask2}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1ForceErrorStepSet,
               "att pdh de1 force error step",
               "Set data mask\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe1ErrorType", "PDH DE1 Error Type")
               DEF_SDKPARAM("step", "UINT", "Step"),
               "${de1s} ${errorType} ${step}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1ForceErrorStatusShow,
               "att show pdh de1 force error status",
               "Show error information\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe1ErrorType", "PDH DE1 Error Type"),
               "${de1s} ${errorType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1ForceErrorShow,
               "att show pdh de1 force error",
               "Show force error information\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe1ErrorType", "PDH DE1 Error Type"),
               "${de1s} ${errorType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1ForceAlarmMode,
               "att pdh de1 force alarm mode",
               "Force number of alarm per second\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe1AlarmType", "PDH DE1 Alarm Type")
               DEF_SDKPARAM("mode", "AttForceAlarmMode", "Alarm Mode"),
               "${de1s} ${alarmType} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1TxAlarmGeneratorMode,
               "att pdh de1 tx alarm generator mode",
               "Force number of alarm per second\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe1AlarmType", "PDH DE1 Alarm Type")
               DEF_SDKPARAM("mode", "AttForceAlarmMode", "Alarm Mode"),
               "${de1s} ${alarmType} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1ForceAlarmNumEventPerSecond,
               "att pdh de1 force alarm numevent",
               "Force number of alarm per second\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe1AlarmType", "PDH DE1 Alarm Type")
               DEF_SDKPARAM("numevent", "UINT", "Number of event each second"),
               "${de1s} ${alarmType} ${numevent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1AlarmTxGeneratorNumEventPerSecond,
               "att pdh de1 tx alarm generator numevent",
               "Force number of alarm per second\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe1AlarmType", "PDH DE1 Alarm Type")
               DEF_SDKPARAM("numevent", "UINT", "Number of event per second"),
               "${de1s} ${alarmType} ${numevent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1ForceAlarmDuration,
               "att pdh de1 force alarm duration",
               "Force alarm per second\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe1AlarmType", "PDH DE1 Alarm Type")
               DEF_SDKPARAM("duration", "UINT", "Duration in seconds"),
               "${de1s} ${alarmType} ${duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1TxAlarmGeneratorDuration,
               "att pdh de1 tx alarm generator duration",
               "Force alarm per second\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe1AlarmType", "PDH DE1 Alarm Type")
               DEF_SDKPARAM("duration", "UINT", "Duration in ms"),
               "${de1s} ${alarmType} ${duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1ForceAlarmNumFrame,
               "att pdh de1 force alarm numframe",
               "Force number of frame to set alarm\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe1AlarmType", "PDH DE1 Alarm Type")
               DEF_SDKPARAM("numframe", "UINT", "Number of frame to set alarm"),
               "${de1s} ${alarmType} ${numframe}")
    {
    mAtCliCall();
    }


DEF_SDKCOMMAND(CliAtAttPdhDe1RxAlarmCaptureEnable,
               "att pdh de1 rx alarm capture enable",
               "Enable to capture RX alarm of DE1 \n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List"),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1RxAlarmCaptureDisable,
               "att pdh de1 rx alarm capture disable",
               "Disable to capture RX alarm oF DE1 \n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List"),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1RxAlarmCaptureDuration,
               "att pdh de1 rx alarm capture duration",
               "Set duration to capture alarm for DE1\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe1AlarmType", "PDH DE1 Alarm Type")
               DEF_SDKPARAM("duration", "UINT", "Duration in ms"),
               "${de1s} ${alarmType} ${duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1RxAlarmCaptureMode,
               "att pdh de1 rx alarm capture mode",
               "Set duration to capture alarm for DE1\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe1AlarmType", "PDH DE1 Alarm Type")
               DEF_SDKPARAM("mode", "AttForceAlarmMode", "Duration in ms"),
               "${de1s} ${alarmType} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1RxAlarmCaptureNumEvent,
               "att pdh de1 rx alarm capture numevent",
               "Set duration to capture alarm for DE1\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe1AlarmType", "PDH DE1 Alarm Type")
               DEF_SDKPARAM("numevent", "UINT", "Number of event each second"),
               "${de1s} ${alarmType} ${numevent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1ForceAlarmStepSet,
               "att pdh de1 force alarm step",
               "Force alarm per second\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe1AlarmType", "PDH DE1 Alarm Type")
               DEF_SDKPARAM("step", "UINT", "HW Step"),
               "${de1s} ${alarmType} ${step}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1ForceAlarm,
               "att pdh de1 force alarm",
               "Force alarm\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe1AlarmType", "PDH DE1 Alarm Type")
               DEF_SDKPARAM("enable", "eBool", "Enable or Disable Force Alarm"),
               "${de1s} ${alarmType} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1FrequencyOffsetSet,
               "att pdh de1 freqoffs",
               "Configure frequency offset for DE1\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM("freqOffs", "STRING", "ppm"),
               "${de1s} ${freqOffs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1ForceFbitTypeSet,
               "att pdh de1 force fbit",
               "Set type to force in per DE1 is forced alarm or error\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM("type", "eAtPdhDe1FbitType", "DS1 Fbit Force Type")
			   DEF_SDKPARAM("enable",   "eBool", "Enable/Disable Fbit Force Type"),
               "${de1s} ${type} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1WanderFileNameSet,
               "att pdh de1 wander filename",
               "Configure frequency offset for DE1\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM("filename", "STRING", "ppm"),
               "${de1s} ${filename}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1WanderStart,
               "att pdh de1 wander start",
               "Configure frequency offset for DE1\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM_OPTION("duration", "UINT", "Number of seconds"),
               "${de1s} ${duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1WanderStop,
               "att pdh de1 wander stop",
               "Configure frequency offset for DE1\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List"),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1ForceAlarmStatusShow,
               "att show pdh de1 force alarm status",
               "Show alarm information\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe1AlarmType", "PDH DE1 Alarm Type"),
               "${de1s} ${alarmType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1ForceAlarmShow,
               "att show pdh de1 force alarm",
               "Show force alarm information\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe1AlarmType", "PDH DE1 Alarm Type"),
               "${de1s} ${alarmType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1RxAlarmCaptureShow,
               "att show pdh de1 rx alarm capture",
               "Show force alarm information\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe1AlarmType", "PDH DE1 Alarm Type")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${de1s} ${alarmType} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe1FrequencyOffsetShow,
               "att show pdh de1 freqoffs",
               "Show frequency offset for DE1\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "PDH DE1 ID List"),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtAttPdhDe1SignalingPrbsSet,
               "att pdh de1 signaling prbs",
               "Configure pdh de1 signaling prbs\n",
               DEF_SDKPARAM("ds0List", "PhyIntfList", "")
               DEF_SDKPARAM("prbsMode", "sigPrbsMode", "")
               DEF_SDKPARAM_OPTION("threshold", "UINT", "")
               DEF_SDKPARAM_OPTION("patternVal", "STRING", ""),
               "${ds0List} {prbsMode} {threshold} {patternVal}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtAttPdhDe1TxSignalingPrbsSet,
               "att pdh de1 tx signaling prbs",
               "Configure pdh de1 signaling prbs\n",
               DEF_SDKPARAM("ds0List", "PhyIntfList", "")
               DEF_SDKPARAM("prbsMode", "sigPrbsMode", "")
               DEF_SDKPARAM_OPTION("threshold", "UINT", "")
               DEF_SDKPARAM_OPTION("patternVal", "STRING", ""),
               "${ds0List} {prbsMode} {threshold} {patternVal}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtAttPdhDe1RxSignalingPrbsSet,
               "att pdh de1 rx signaling prbs",
               "Configure pdh de1 signaling prbs\n",
               DEF_SDKPARAM("ds0List", "PhyIntfList", "")
               DEF_SDKPARAM("prbsMode", "sigPrbsMode", "")
               DEF_SDKPARAM_OPTION("threshold", "UINT", "")
               DEF_SDKPARAM_OPTION("patternVal", "STRING", ""),
               "${ds0List} {prbsMode} {threshold} {patternVal}")
    {
    mAtCliCall();
    }
DEF_SDKCOMMAND(ClishAtAttPdhDe1SignalingEnable,
               "att pdh de1 signaling enable",
               "Enable signaling\n",
               DEF_SDKPARAM("ds0List", "PhyIntfList", ""),
               "${ds0List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtAttPdhDe1TxSignalingEnable,
               "att pdh de1 tx signaling enable",
               "Enable signaling\n",
               DEF_SDKPARAM("ds0List", "PhyIntfList", ""),
               "${ds0List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtAttPdhDe1RxSignalingEnable,
               "att pdh de1 rx signaling enable",
               "Enable signaling\n",
               DEF_SDKPARAM("ds0List", "PhyIntfList", ""),
               "${ds0List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtAttPdhDe1SignalingDisable,
               "att pdh de1 signaling disable",
               "Disable signaling\n",
               DEF_SDKPARAM("ds0List", "PhyIntfList", ""),
               "${ds0List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtAttPdhDe1TxSignalingDisable,
               "att pdh de1 tx signaling disable",
               "Disable signaling\n",
               DEF_SDKPARAM("ds0List", "PhyIntfList", ""),
               "${ds0List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtAttPdhDe1RxSignalingDisable,
               "att pdh de1 rx signaling disable",
               "Disable signaling\n",
               DEF_SDKPARAM("ds0List", "PhyIntfList", ""),
               "${ds0List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtAttPdhDe1TxSignalingDump,
               "att pdh de1 tx signaling dump",
               "Disable signaling\n",
               DEF_SDKPARAM("ds0List", "PhyIntfList", ""),
               "${ds0List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtAttPdhDe1RxSignalingDump,
               "att pdh de1 rx signaling dump",
               "Disable signaling\n",
               DEF_SDKPARAM("ds0List", "PhyIntfList", ""),
               "${ds0List}")
    {
    mAtCliCall();
    }



DEF_SDKCOMMAND(ClishAtAttPdhDe1TxSignalingDumpDebug,
               "debug att pdh de1 tx signaling dump",
               "Disable signaling\n",
               DEF_SDKPARAM("ds0List", "PhyIntfList", ""),
               "${ds0List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtAttPdhDe1RxSignalingDumpDebug,
               "debug att pdh de1 rx signaling dump",
               "Disable signaling\n",
               DEF_SDKPARAM("ds0List", "PhyIntfList", ""),
               "${ds0List}")
    {
    mAtCliCall();
    }


DEF_SDKCOMMAND(CliAtAttPdhDe1SignalingShow,
               "att show pdh de1 signaling",
               "show pdh de1 signaling\n",
               DEF_SDKPARAM("ds0List", "PhyIntfList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${ds0List} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

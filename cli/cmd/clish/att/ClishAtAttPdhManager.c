/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ATT
 *
 * File        : ClishAtAttPdhDe1.c
 *
 * Created Date: Dec 24, 2015
 *
 * Description : DS1-E1 Clish
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"
#include "AtCli.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/* Top commands */
TOP_SDKCOMMAND("pdh de1 force", "PDH DE1 Force")
TOP_SDKCOMMAND("pdh de3 force", "PDH DE3 Force")
TOP_SDKCOMMAND("pdh de3 force perfrm", "PDH DE3 Force Per Frame")



DEF_SDKCOMMAND(CliAtModulePdhDe1ForcePeriodSet,
               "pdh de1 force period",
               "Set number of seconds in which all DE1s is forced alarm or error\n",
               DEF_SDKPARAM("numSeconds", "UINT", ""),
               "${numSeconds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtModulePdhDe3ForcePeriodSet,
               "pdh de3 force period",
               "Set number of seconds in which all DE3s is forced alarm or error\n",
               DEF_SDKPARAM("numSeconds", "UINT", ""),
               "${numSeconds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtModulePdhDe3ForcePerframeEnable,
               "pdh de3 force perfrm enable",
               "Enable force perframe for DE3s\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtModulePdhDe3ForcePerframeDisable,
               "pdh de3 force perfrm disable",
               "Disable force perframe for DE3s\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

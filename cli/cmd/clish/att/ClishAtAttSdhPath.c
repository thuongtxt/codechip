/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ATT
 *
 * File        : ClishAtAttSdhPath.c
 *
 * Created Date: Jul 8, 2016
 *
 * Description : SDH path ATT
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"
#include "AtCli.h"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/* Top commands */
TOP_SDKCOMMAND("att sdh path", "ATT SDH path command\n")
TOP_SDKCOMMAND("att sdh path force", "ATT SDH path force command\n")
TOP_SDKCOMMAND("att sdh path tx", "ATT SDH path Tx command\n")
TOP_SDKCOMMAND("att sdh path tx error", "ATT SDH path Tx Error command\n")
TOP_SDKCOMMAND("att sdh path tx error generator", "ATT SDH path Tx Error Generator command\n")
TOP_SDKCOMMAND("att sdh path tx alarm", "ATT SDH path Tx alarm command\n")
TOP_SDKCOMMAND("att sdh path tx alarm generator", "ATT SDH path Tx Alarm Generator command\n")
TOP_SDKCOMMAND("att sdh path rx", "ATT SDH path Rx command\n")
TOP_SDKCOMMAND("att sdh path rx alarm", "ATT SDH path Rx Alarm command\n")
TOP_SDKCOMMAND("att sdh path rx alarm capture", "ATT SDH path Rx Alarm Capture command\n")
TOP_SDKCOMMAND("att sdh path wander", "ATT SDH path wander command\n")
TOP_SDKCOMMAND("att show sdh path", "ATT show SDH path command\n")
TOP_SDKCOMMAND("att show sdh path force", "ATT show SDH path force command\n")
TOP_SDKCOMMAND("att show sdh path rx", "ATT show SDH path Rx command\n")
TOP_SDKCOMMAND("att show sdh path rx alarm", "ATT show SDH path Rx Alarm command\n")
TOP_SDKCOMMAND("att sdh path pointer_adj", "ATT SDH path pointer_adj command\n")

DEF_PTYPE("eAtSdhPathErrorType", "select", "bip(1), rei(2)", "\nError type:")
DEF_PTYPE("eAtSdhPathAlarmType", "select", "ais(1), rdi(2), lop(3), uneq(4), rfi(5), erdi-c(6), erdi-p(7), erdi-s(8), lom(9)", "\nAlarm type")
DEF_PTYPE("eAtSdhPathPointerAdjType", "select", "increase(1), decrease(2)", "\nPointer Adjust Type")
DEF_PTYPE("AttForcePointerAdjMode",   "select", "continuous(1), oneshot(2), nevent_in_t(3), g783a(4), g783b(5), g783c(6), g783d(7), g783e(8), g783f(9), g783g_part1(10), g783g_part2(11), g783g_part3(12), g783g_part4(13), g783h_parta(14), g783h_partb(15), g783h_partc(16), g783h_partd(17), g783i(18), g783j_parta(19), g783j_partb(20), g783j_partc(21), g783j_partd(22)", "\n Force Pointer Adjust Mode")

DEF_SDKCOMMAND(CliAtAttSdhPathForceErrorMode,
               "att sdh path force error mode",
               "Set mode to force error for SDH Paths\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("errorType", "eAtSdhPathErrorType", "SDH Path Error Type")
               DEF_SDKPARAM("mode", "AttForceErrorMode", "Error Rate"),
               "${pathList} ${errorType} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathTxErrorGeneratorMode,
               "att sdh path tx error generator mode",
               "Set mode to Generate error for SDH Paths\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("errorType", "eAtSdhPathErrorType", "SDH Path Error Type")
               DEF_SDKPARAM("mode", "AttForceErrorMode", "Error Rate"),
               "${pathList} ${errorType} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathForceErrorRate,
               "att sdh path force error rate",
               "Set Rate to force error for SDH Paths\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("errorType", "eAtSdhPathErrorType", "SDH Path Error Type")
               DEF_SDKPARAM("rate", "eAtBerRate", "Error Rate"),
               "${pathList} ${errorType} ${rate}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathTxErrorGeneratorRate,
               "att sdh path tx error generator rate",
               "Set rate to generate error for SDH Path\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("errorType", "eAtSdhPathErrorType", "SDH Path Error Type")
               DEF_SDKPARAM("rate", "eAtBerRate", "Error Rate"),
               "${pathList} ${errorType} ${rate}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathForceErrorDuration,
               "att sdh path force error duration",
               "Set duration to force error for SDH Path\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("errorType", "eAtSdhPathErrorType", "SDH Path Error Type")
               DEF_SDKPARAM("duration", "UINT", "Duration in ms"),
               "${pathList} ${errorType} ${duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathTxErrorGeneratorDuration,
               "att sdh path tx error generator duration",
               "Set duration to force error for SDH Path\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("errorType", "eAtSdhPathErrorType", "SDH Path Error Type")
               DEF_SDKPARAM("duration", "UINT", "Duration in ms"),
               "${pathList} ${errorType} ${duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathForceErrorPerSecond,
               "att sdh path force error persecond",
               "Force error per second\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("errorType", "eAtSdhPathErrorType", "SDH Path Error Type")
               DEF_SDKPARAM("enable", "eBool", "Enable or Disable"),
               "${pathList} ${errorType} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathForceError,
               "att sdh path force error",
               "Force error\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("errorType", "eAtSdhPathErrorType", "SDH Path Error Type")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${pathList} ${errorType} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathForceErrorNumErrorsSet,
               "att sdh path force error numerrors",
               "Set number of errors\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("errorType", "eAtSdhPathErrorType", "SDH Path Error Type")
               DEF_SDKPARAM("numErrors", "UINT", "Number of error each second"),
               "${pathList} ${errorType} ${numErrors}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathTxErrorGeneratorNumErrorsSet,
               "att sdh path tx error generator numerrors",
               "Set number of errors\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("errorType", "eAtSdhPathErrorType", "SDH Path Error Type")
               DEF_SDKPARAM("numErrors", "UINT", "Number of error each second"),
               "${pathList} ${errorType} ${numErrors}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathForceErrorBitPositionSet,
               "att sdh path force error bitposition",
               "Set bit position\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("errorType", "eAtSdhPathErrorType", "SDH Path Error Type")
               DEF_SDKPARAM("bitposition", "BitPositionList", "Position to force Error"),
               "${pathList} ${errorType} ${bitposition}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathForceErrorDataMaskSet,
               "att sdh path force error datamask",
               "Set data mask\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("errorType", "eAtSdhPathErrorType", "SDH Path Error Type")
               DEF_SDKPARAM("dataMask", "BitMaskList", "Data Bit Mask List"),
               "${pathList} ${errorType} ${dataMask}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathForceErrorStepSet,
               "att sdh path force error step",
               "Set HW Step\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("errorType", "eAtSdhPathErrorType", "SDH Path Error Type")
               DEF_SDKPARAM("step", "UINT", "HW Step"),
               "${pathList} ${errorType} ${step}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathForceErrorStatusShow,
               "att show sdh path force error status",
               "Show error information\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("errorType", "eAtSdhPathErrorType", "SDH Path Error Type"),
               "${pathList} ${errorType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathForceErrorShow,
               "att show sdh path force error",
               "Show force error information\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("errorType", "eAtSdhPathErrorType", "SDH Path Error Type"),
               "${pathList} ${errorType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathForceAlarmMode,
               "att sdh path force alarm mode",
               "Set mode for Alarm Generator\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("alarmType", "eAtSdhPathAlarmType", "SDH Path Alarm Type")
               DEF_SDKPARAM("mode", "AttForceAlarmMode", "Alarm Forcing Mode"),
               "${pathList} ${alarmType} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathTxAlarmGeneratorMode,
               "att sdh path tx alarm generator mode",
               "Set mode for Alarm Generator\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("alarmType", "eAtSdhPathAlarmType", "SDH Path Alarm Type")
               DEF_SDKPARAM("mode", "AttForceAlarmMode", "Alarm Forcing Mode"),
               "${pathList} ${alarmType} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathForceAlarmNumEventPerSecond,
               "att sdh path force alarm numevent",
               "Force alarm per second\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("alarmType", "eAtSdhPathAlarmType", "SDH Path Alarm Type")
               DEF_SDKPARAM("numevent", "UINT", "Number of event each second"),
               "${pathList} ${alarmType} ${numevent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathTxAlarmGeneratorNumEventPerSecond,
               "att sdh path tx alarm generator numevent",
               "Force alarm per second\n",
               DEF_SDKPARAM("pathList",  "SdhPathList",          "SDH Path ID List")
               DEF_SDKPARAM("alarmType", "eAtSdhPathAlarmType",  "SDH Path Alarm Type")
               DEF_SDKPARAM("numevent",  "UINT",                 "Number of event each second"),
               "${pathList} ${alarmType} ${numevent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathForceAlarmDuration,
               "att sdh path force alarm duration",
               "Force alarm per second\n",
               DEF_SDKPARAM("pathList",  "SdhPathList",         "SDH Path ID List")
               DEF_SDKPARAM("alarmType", "eAtSdhPathAlarmType", "SDH Path Alarm Type")
               DEF_SDKPARAM("duration",  "UINT",                "Duration in seconds"),
               "${pathList} ${alarmType} ${duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathTxAlarmGeneratorDuration,
               "att sdh path tx alarm generator duration",
               "Force alarm per second\n",
               DEF_SDKPARAM("pathList",  "SdhPathList",         "SDH Path ID List")
               DEF_SDKPARAM("alarmType", "eAtSdhPathAlarmType", "SDH Path Alarm Type")
               DEF_SDKPARAM("duration",  "UINT",                "Duration in ms"),
               "${pathList} ${alarmType} ${duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathForceAlarmNumFrame,
               "att sdh path force alarm numframe",
               "Number of frame to set alarm\n",
               DEF_SDKPARAM("pathList",  "SdhPathList",          "SDH Path ID List")
               DEF_SDKPARAM("alarmType", "eAtSdhPathAlarmType",  "SDH Path Alarm Type")
               DEF_SDKPARAM("numframe",  "UINT",                 "Number of frame to set alarm"),
               "${pathList} ${alarmType} ${numframe}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathRxAlarmCaptureEnable,
               "att sdh path rx alarm capture enable",
               "Set time to capture alarm for SDH path\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List"),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathRxAlarmCaptureDisable,
               "att sdh path rx alarm capture disable",
               "Set time to capture alarm for SDH path\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List"),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathRxAlarmCaptureDuration,
               "att sdh path rx alarm capture duration",
               "Set time to capture alarm for SDH path\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("alarmType", "eAtSdhPathAlarmType", "SDH Path Alarm Type")
               DEF_SDKPARAM("duration", "UINT", "Duration in ms"),
               "${pathList} ${alarmType} ${duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathRxAlarmCaptureMode,
               "att sdh path rx alarm capture mode",
               "Set time to capture alarm for SDH path\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("alarmType", "eAtSdhPathAlarmType", "SDH Path Alarm Type")
               DEF_SDKPARAM("mode", "AttForceAlarmMode", "Alarm Mode"),
               "${pathList} ${alarmType} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathRxAlarmCaptureNumEvent,
               "att sdh path rx alarm capture numevent",
               "Set time to capture alarm for SDH path\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("alarmType", "eAtSdhPathAlarmType", "SDH Path Alarm Type")
               DEF_SDKPARAM("numevent", "UINT", "Expected Number of alarm per seconds"),
               "${pathList} ${alarmType} ${numevent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathForceAlarmStepSet,
               "att sdh path force alarm step",
               "Force alarm per second\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("alarmType", "eAtSdhPathAlarmType", "SDH Path Alarm Type")
               DEF_SDKPARAM("step", "eBool", "HW step"),
               "${pathList} ${alarmType} ${step}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathForceAlarm,
               "att sdh path force alarm",
               "Force alarm\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("alarmType", "eAtSdhPathAlarmType", "SDH Path Alarm Type")
               DEF_SDKPARAM("enable", "eBool", "Enable or Disable"),
               "${pathList} ${alarmType} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathFrequencyOffsetSet,
               "att sdh path freqoffs",
               "Configure frequency offset for SDH path.\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("freqOffs", "STRING", "ppm"),
               "${pathList} ${freqOffs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathWanderFileNameSet,
               "att sdh path wander filename",
               "Configure frequency offset for SDH path.\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("filename", "STRING", "File name with .csv"),
               "${pathList} ${filename}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathWanderStart,
               "att sdh path wander start",
               "Configure frequency offset for SDH path.\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("duration", "UINT", "Duration in Number of seconds"),
               "${pathList} ${duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathWanderStop,
               "att sdh path wander stop",
               "Configure frequency offset for SDH path.\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List"),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathForceAlarmStatusShow,
               "att show sdh path force alarm status",
               "Show alarm information\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("alarmType", "eAtSdhPathAlarmType", "SDH Path Alarm Type"),
               "${pathList} ${alarmType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathForceAlarmShow,
               "att show sdh path force alarm",
               "Show force alarm information\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("alarmType", "eAtSdhPathAlarmType", "SDH Path Alarm Type"),
               "${pathList} ${alarmType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathRxAlarmCaptureShow,
               "att show sdh path rx alarm capture",
               "Show force alarm information\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("alarmType", "eAtSdhPathAlarmType", "SDH Path Alarm Type")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${pathList} ${alarmType} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathFrequencyOffsetShow,
               "att show sdh path freqoffs",
               "Show frequency offset for SDH path.\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List"),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtAttSdhPathForcePointerAdjMode,
               "att sdh path force pointer_adj mode",
               "Set mode to force Pointer Adjust for SDH Paths\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("pointerType", "eAtSdhPathPointerAdjType", "SDH Path Pointer Adjust Type")
               DEF_SDKPARAM("mode", "AttForcePointerAdjMode", "Pointer Adjust Rate"),
               "${pathList} ${pointerType} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtAttSdhPathForcePointerAdjNumEvent,
               "att sdh path force pointer_adj numevent",
               "Number of event to Force pointer\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("pointerType", "eAtSdhPathPointerAdjType", "SDH Path Pointer Adjust Type")
               DEF_SDKPARAM("numevent", "UINT", "Number of event"),
               "${pathList} ${pointerType} ${numevent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtAttSdhPathForcePointerAdjDuration,
               "att sdh path force pointer_adj duration",
               "Force alarm per second\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("pointerType", "eAtSdhPathPointerAdjType", "SDH Path Pointer Adjust Type")
               DEF_SDKPARAM("duration", "UINT", "Duration to force pointer"),
               "${pathList} ${pointerType} ${duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathForcePointerAdjBitPositionSet,
               "att sdh path force pointer_adj bitposition",
               "Set bit position\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("pointerType", "eAtSdhPathPointerAdjType", "SDH Path Pointer Adjust Type")
               DEF_SDKPARAM("bitposition", "BitPositionList", "Position to force Error"),
               "${pathList} ${errorType} ${bitposition}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathForcePointerAdjDataMaskSet,
               "att sdh path force pointer_adj datamask",
               "Set data mask\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("pointerType", "eAtSdhPathPointerAdjType", "SDH Path Pointer Adjust Type")
               DEF_SDKPARAM("dataMask", "BitMaskList", "Data Bit Mask List"),
               "${pathList} ${errorType} ${dataMask}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathForcePointerAdjStepSet,
               "att sdh path force pointer_adj step",
               "Set HW Step\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("pointerType", "eAtSdhPathPointerAdjType", "SDH Path Pointer Adjust Type")
               DEF_SDKPARAM("step", "UINT", "HW Step"),
               "${pathList} ${errorType} ${step}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathForcePointerAdj,
               "att sdh path force pointer_adj",
               "Force Pointer Adjust\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("pointerType", "eAtSdhPathPointerAdjType", "SDH Path Pointer Adjust Type")
               DEF_SDKPARAM("enable", "eBool", "Enable or Disable"),
               "${pathList} ${alarmType} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathBipCountShow,
               "att show sdh path bipcnt",
               "Show BIP counters for SDH path.\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${pathList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathPointerAdjEnable,
               "att sdh path pointer_adj enable",
               "Set state for force pointer.\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List"),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhPathPointerAdjDisable,
               "att sdh path pointer_adj disable",
               "Set state for force pointer.\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH Path ID List"),
               "${pathList}")
    {
    mAtCliCall();
    }

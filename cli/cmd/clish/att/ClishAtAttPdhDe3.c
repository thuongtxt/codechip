/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ATT
 *
 * File        : ClishAtAttPdhDe3.c
 *
 * Created Date: Dec 23, 2015
 *
 * Description : ATT Cli
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("att", "ATT command\n")
TOP_SDKCOMMAND("att pdh", "ATT PDH command\n")
TOP_SDKCOMMAND("att pdh de3", "ATT PDH DE3 command\n")
TOP_SDKCOMMAND("att pdh de3 force", "ATT PDH DE3 force command\n")
TOP_SDKCOMMAND("att show", "ATT show command\n")
TOP_SDKCOMMAND("att show pdh", "ATT show PDH command\n")
TOP_SDKCOMMAND("att show pdh de3", "ATT show PDH DE3 command\n")
TOP_SDKCOMMAND("att show pdh de3 force", "ATT show PDH DE3 force command\n")
TOP_SDKCOMMAND("att pdh de3 tx", "ATT show PDH DE3 force command\n")
TOP_SDKCOMMAND("att pdh de3 tx error", "ATT show PDH DE3 force command\n")
TOP_SDKCOMMAND("att pdh de3 tx error generator", "ATT show PDH DE3 force command\n")
TOP_SDKCOMMAND("att pdh de3 tx alarm", "ATT show PDH DE3 force command\n")
TOP_SDKCOMMAND("att pdh de3 tx alarm generator", "ATT show PDH DE3 force command\n")
TOP_SDKCOMMAND("att pdh de3 rx", "ATT PDH DE3 Rx command\n")
TOP_SDKCOMMAND("att pdh de3 rx alarm", "ATT PDH DE3 Rx alarm command\n")
TOP_SDKCOMMAND("att pdh de3 rx alarm capture", "ATT PDH DE3 Rx alarm capture command\n")
TOP_SDKCOMMAND("att pdh de3 wander", "ATT PDH DE3 Wander command\n")
TOP_SDKCOMMAND("att show pdh de3", "ATT show PDH DE3 command\n")
TOP_SDKCOMMAND("att show pdh de3 force", "ATT show PDH DE3 force command\n")
TOP_SDKCOMMAND("att show pdh de3 rx", "ATT show PDH DE3 Rx command\n")
TOP_SDKCOMMAND("att show pdh de3 rx alarm", "ATT show PDH DE3 Rx Alarm command\n")

TOP_SDKCOMMAND("debug att", "ATT show PDH DE3 force command\n")
TOP_SDKCOMMAND("debug att pdh", "ATT show PDH DE3 force command\n")
TOP_SDKCOMMAND("debug att pdh de3", "ATT show PDH DE3 force command\n")
TOP_SDKCOMMAND("debug att pdh de3 force", "ATT show PDH DE3 force command\n")


DEF_PTYPE("eAttPdhDe3ErrorType", "select", "fbit(1), cpbit(2), pbit(3), fabyte(4), bip8byte(5), reibit(6), nrbyte(7), gcbyte(8), febe(9)", "\nDE3 Error type:")
DEF_PTYPE("eAttPdhDe3AlarmType", "select", "ais(1), los(2), rai(3), lof(4)", "\nDE3 Alarm type:")
DEF_PTYPE("AttForceAlarmMode",   "select", "continuous(1), set_in_t(2), set_in_nframe(3), nevent_in_t(4)", "\nForce Alarm Mode")
DEF_PTYPE("AttForceErrorMode",   "select", "continuous(1), rate(2), oneshot(3), nerror_in_t(4), consecutive(5)", "\n Force Error Mode")

DEF_SDKCOMMAND(cliPdhDe3ErrorForce,
               "pdh de3 force error",
               "Force TX error\n",
               DEF_SDKPARAM("de3List", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe3ErrorType", "PDH DE3 Error Type")
               DEF_SDKPARAM("force", "eBool", ""),
               "${de1List} ${errorType} ${force}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3ForceErrorMode,
               "att pdh de3 force error mode",
               "Mode to force error\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe3ErrorType", "PDH DE3 Error Type")
               DEF_SDKPARAM("mode", "AttForceErrorMode", "Error Mode"),
               "${de3s} ${errorType} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3TxErrorGeneratorMode,
               "att pdh de3 tx error generator mode",
               "Mode to force error\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe3ErrorType", "PDH DE3 Error Type")
               DEF_SDKPARAM("mode", "AttForceErrorMode", "Error Mode"),
               "${de3s} ${errorType} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3ForceErrorRate,
               "att pdh de3 force error rate",
               "Force error per second\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe3ErrorType", "PDH DE3 Error Type")
               DEF_SDKPARAM("rate", "eAtBerRate", "Error Rate"),
               "${de3s} ${errorType} ${rate}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3TxErrorGeneratorRate,
               "att pdh de3 tx error generator rate",
               "Force error per second\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe3ErrorType", "PDH DE3 Error Type")
               DEF_SDKPARAM("rate", "eAtBerRate", "Error Rate"),
               "${de3s} ${errorType} ${rate}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3ForceErrorPerSecond,
               "att pdh de3 force error persecond",
               "Force error per second\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe3ErrorType", "PDH DE3 Error Type")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${de3s} ${errorType} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3ForceErrorDuration,
               "att pdh de3 force error duration",
               "Duration to force error\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe3ErrorType", "PDH DE3 Error Type")
               DEF_SDKPARAM("duration", "UINT", "Duration in ms"),
               "${de3s} ${errorType} ${duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3TxErrorGenerationDuration,
               "att pdh de3 tx error generator duration",
               "Duration to force error\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe3ErrorType", "PDH DE3 Error Type")
               DEF_SDKPARAM("duration", "UINT", "Duration"),
               "${de3s} ${errorType} ${duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3ForceErrorStepSet,
               "att pdh de3 force error step",
               "Force error per second\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe3ErrorType", "PDH DE3 Error Type")
               DEF_SDKPARAM("step", "UINT", "Duration"),
               "${de3s} ${errorType} ${step}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3ForceError,
               "att pdh de3 force error",
               "Force error\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe3ErrorType", "PDH DE3 Error Type")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${de3s} ${errorType} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3ForceErrorDebug,
               "debug att pdh de3 force error",
               "Debug\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe3ErrorType", "PDH DE3 Error Type"),
               "${de3s} ${errorType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3ForceErrorNumErrorsSet,
               "att pdh de3 force error numerrors",
               "Set number of errors\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe3ErrorType", "PDH DE3 Error Type")
               DEF_SDKPARAM("numErrors", "UINT", "Number of error each second"),
               "${de3s} ${errorType} ${numErrors}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3TxErrorGeneratorNumErrorsSet,
               "att pdh de3 tx error generator errornum",
               "Set number of errors\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe3ErrorType", "PDH DE3 Error Type")
               DEF_SDKPARAM("numErrors", "UINT", "Number of error each second"),
               "${de3s} ${errorType} ${numErrors}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3ForceErrorBitPositionSet,
               "att pdh de3 force error bitposition",
               "Set bit position\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe3ErrorType", "PDH DE3 Error Type")
               DEF_SDKPARAM("bitposition", "BitPositionList", "Error Positions"),
               "${de3s} ${errorType} ${bitposition}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3ForceErrorDataMaskSet,
               "att pdh de3 force error datamask",
               "Set data mask\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe3ErrorType", "PDH DE3 Error Type")
               DEF_SDKPARAM("dataMask", "BitMaskList", "Data Bit Mask List"),
               "${de3s} ${errorType} ${dataMask}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3ForceErrorStatusShow,
               "att show pdh de3 force error status",
               "Show error information\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe3ErrorType", "PDH DE3 Error Type"),
               "${de3s} ${errorType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3ForceAlarmMode,
               "att pdh de3 force alarm mode",
               "Force number of alarm per second\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe3AlarmType", "PDH DE3 Alarm Type")
               DEF_SDKPARAM("mode", "AttForceAlarmMode", "Alarm Force Mode"),
               "${de3s} ${alarmType} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3TxAlarmGeneratorMode,
               "att pdh de3 tx alarm generator mode",
               "Force number of alarm per second\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe3AlarmType", "PDH DE3 Alarm Type")
               DEF_SDKPARAM("mode", "AttForceAlarmMode", "Alarm Force Mode"),
               "${de3s} ${alarmType} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3ForceAlarmNumEventPerSecond,
               "att pdh de3 force alarm numevent",
               "Force number of alarm per second\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe3AlarmType", "PDH DE3 Alarm Type")
               DEF_SDKPARAM("numevent", "UINT", "Number of alarm in one second"),
               "${de3s} ${alarmType} ${numevent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3TxAlarmGeneratorNumEvent,
               "att pdh de3 tx alarm generator numevent",
               "Force number of alarm event in each second\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe3AlarmType", "PDH DE3 Alarm Type")
               DEF_SDKPARAM("numevent", "UINT", "Number of alarm in one second"),
               "${de3s} ${alarmType} ${numevent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3ForceAlarm,
               "att pdh de3 force alarm",
               "Force alarm\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe3AlarmType", "PDH DE3 Alarm Type")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${de3s} ${alarmType} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3ForceAlarmDuration,
               "att pdh de3 force alarm duration",
               "Set number of frames\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe3AlarmType", "PDH DE3 Alarm Type")
               DEF_SDKPARAM("duration", "UINT", "Duration in seconds"),
               "${de3s} ${alarmType} ${duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3TxAlarmGeneratorDuration,
               "att pdh de3 tx alarm generator duration",
               "Set Duration to force alarm\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe3AlarmType", "PDH DE3 Alarm Type")
               DEF_SDKPARAM("duration", "UINT", "Duration in ms"),
               "${de3s} ${alarmType} ${duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3ForceAlarmNumFrame,
               "att pdh de3 force alarm numframe",
               "Force number of frame to set alarm\n",
               DEF_SDKPARAM("de3s",      "PdhDe3List",          "PDH DE3 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe3AlarmType", "PDH DE3 Alarm Type")
               DEF_SDKPARAM("numframe",  "UINT",                "Number of frame to set alarm"),
               "${de3s} ${alarmType} ${numframe}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3RxAlarmCaptureEnable,
               "att pdh de3 rx alarm capture enable",
               "Enable to capture Rx alarm of DE3\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List"),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3RxAlarmCaptureDisable,
               "att pdh de3 rx alarm capture disable",
               "Disable to capture RX alarm of DE3\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List"),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3RxAlarmCaptureDuration,
               "att pdh de3 rx alarm capture duration",
               "Set Duration to force alarm\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe3AlarmType", "PDH DE3 Alarm Type")
               DEF_SDKPARAM("duration", "UINT", "Duration in ms"),
               "${de3s} ${alarmType} ${duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3RxAlarmCaptureMode,
               "att pdh de3 rx alarm capture mode",
               "Set Duration to force alarm\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe3AlarmType", "PDH DE3 Alarm Type")
               DEF_SDKPARAM("mode", "AttForceAlarmMode", "Mode in ms"),
               "${de3s} ${alarmType} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3RxAlarmCaptureNumEvent,
               "att pdh de3 rx alarm capture numevent",
               "Set Duration to force alarm\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe3AlarmType", "PDH DE3 Alarm Type")
               DEF_SDKPARAM("numevent", "UINT", "Expected Number of event each second"),
               "${de3s} ${alarmType} ${numevent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3ForceAlarmStepSet,
               "att pdh de3 force alarm step",
               "Set number of frames\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe3AlarmType", "PDH DE3 Alarm Type")
               DEF_SDKPARAM("step", "UINT", "HW Step"),
               "${de3s} ${alarmType} ${step}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3TxAlarmGenratorNumAlarmsSet,
               "att pdh de3 tx alarm generator numevent",
               "Set number of frames\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe3AlarmType", "PDH DE3 Alarm Type")
               DEF_SDKPARAM("numAlarms", "UINT", ""),
               "${de3s} ${alarmType} ${numAlarms}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3ForceAlarmBitPositionSet,
               "att pdh de3 force alarm bitposition",
               "Set bit position\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe3AlarmType", "PDH DE3 Alarm Type")
               DEF_SDKPARAM("bitposition", "BitPositionList", "Alarm Positions"),
               "${de3s} ${alarmType} ${bitposition}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3FrequencyOffsetSet,
               "att pdh de3 freqoffs",
               "Configure frequency offset for DE3\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM("freqOffs", "STRING", "ppm"),
               "${de3s} ${freqOffs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3WanderFileNameSet,
               "att pdh de3 wander filename",
               "Configure frequency offset for DE3\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM("filename", "STRING", "File Name to save wander"),
               "${de3s} ${filename}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3WanderStart,
               "att pdh de3 wander start",
               "Configure frequency offset for DE3\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM_OPTION("duration", "UINT", "Number of seconds"),
               "${de3s} ${duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3WanderStop,
               "att pdh de3 wander stop",
               "Configure frequency offset for DE3\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List"),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3ForceErrorShow,
               "att show pdh de3 force error",
               "Show force error information\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe3ErrorType", "PDH DE3 Error Type"),
               "${de3s} ${errorType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3ForceAlarmStatusShow,
               "att show pdh de3 force alarm status",
               "Show alarm information\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe3AlarmType", "PDH DE3 Alarm Type"),
               "${de3s} ${alarmType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3ForceAlarmShow,
               "att show pdh de3 force alarm",
               "Show force alarm information\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe3AlarmType", "PDH DE3 Alarm Type"),
               "${de3s} ${alarmType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3RxAlarmCaptureShow,
               "att show pdh de3 rx alarm capture",
               "Show force alarm information\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe3AlarmType", "PDH DE3 Alarm Type")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${de3s} ${alarmType} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe3FrequencyOffsetShow,
               "att show pdh de3 freqoffs",
               "Show frequency offset for DE3\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "PDH DE3 ID List"),
               "${de3s}")
    {
    mAtCliCall();
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ATT
 *
 * File        : ClishAtAttPdhDe8.c
 *
 * Created Date: May 11, 2018
 *
 * Description : DS2-E2 Clish
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"
#include "AtCli.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/* Top commands */
TOP_SDKCOMMAND("att pdh de2", "ATT PDH DE2 command\n")
TOP_SDKCOMMAND("att pdh de2 force", "ATT PDH DE2 force command\n")
TOP_SDKCOMMAND("att pdh de2 tx", "ATT PDH DE2 Tx command\n")
TOP_SDKCOMMAND("att pdh de2 tx error", "ATT PDH DE2 Tx Error command\n")
TOP_SDKCOMMAND("att pdh de2 tx error generator", "ATT PDH DE2 Tx Error Generator command\n")
TOP_SDKCOMMAND("att pdh de2 tx alarm", "ATT PDH DE2 Tx alarm command\n")
TOP_SDKCOMMAND("att pdh de2 tx alarm generator", "ATT PDH DE2 Tx alarm Generator command\n")
TOP_SDKCOMMAND("att pdh de2 rx", "ATT PDH DE2 Rx command\n")
TOP_SDKCOMMAND("att pdh de2 rx alarm", "ATT PDH DE2 Rx alarm command\n")
TOP_SDKCOMMAND("att pdh de2 rx alarm capture", "ATT PDH DE2 Rx alarm capture command\n")
TOP_SDKCOMMAND("att pdh de2 wander", "ATT PDH DE2 Wander command\n")
TOP_SDKCOMMAND("att show pdh de2", "ATT show PDH DE2 command\n")
TOP_SDKCOMMAND("att show pdh de2 force", "ATT show PDH DE2 force command\n")
TOP_SDKCOMMAND("att show pdh de2 rx", "ATT show PDH DE2 Rx command\n")
TOP_SDKCOMMAND("att show pdh de2 rx alarm", "ATT show PDH DE2 Rx Alarm command\n")

DEF_PTYPE("eAttPdhDe2ErrorType", "select", "fbit(1), parity(2)", "\nError type:")
DEF_PTYPE("eAttPdhDe2AlarmType", "select", "ais(1), rai(2), lof(3)", "\nAlarm type:")

DEF_SDKCOMMAND(CliAtAttPdhDe2ForceErrorMode,
               "att pdh de2 force error mode",
               "Set mode to generate error for DS2s/E2s\n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe2ErrorType", "")
               DEF_SDKPARAM("mode", "AttForceErrorMode", "Error Forcing Mode"),
               "${de1s} ${errorType} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2TxErrorGeneratorMode,
               "att pdh de2 tx error generator mode",
               "Set mode to generate error for DS2s/E2s\n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe2ErrorType", "")
               DEF_SDKPARAM("mode", "AttForceErrorMode", "Error Forcing Mode"),
               "${de1s} ${errorType} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2ForceErrorRate,
               "att pdh de2 force error rate",
               "Set Rate to force error for DS2s/E2s\n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe2ErrorType", "")
               DEF_SDKPARAM("rate", "eAtBerRate", "Error Rate"),
               "${de1s} ${errorType} ${rate}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2TxErrorGeneratorRate,
               "att pdh de2 tx error generator rate",
               "Set Rate to force error for DS2s/E2s\n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe2ErrorType", "")
               DEF_SDKPARAM("rate", "eAtBerRate", "Error Rate"),
               "${de1s} ${errorType} ${rate}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2ForceErrorDuration,
               "att pdh de2 force error duration",
               "Force error per second\n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe2ErrorType", "DE1 Error Type")
               DEF_SDKPARAM("duration", "UINT", "Duration in ms"),
               "${de1s} ${errorType} ${duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2TxErrorGeneratorDuration,
               "att pdh de2 tx error generator duration",
               "Force error per second\n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe2ErrorType", "DE1 Error Type")
               DEF_SDKPARAM("duration", "UINT", "Duration in ms"),
               "${de1s} ${errorType} ${duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2ForceErrorPerSecond,
               "att pdh de2 force error persecond",
               "Force error per second\n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe2ErrorType", "DE1 Error Type")
               DEF_SDKPARAM("enable", "eBool", "Enable or Disable"),
               "${de1s} ${errorType} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2ForceError,
               "att pdh de2 force error",
               "Force error\n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe2ErrorType", "DE1 Error Type")
               DEF_SDKPARAM("enable", "eBool", "Enable or Disable Trigger force Errors"),
               "${de1s} ${errorType} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2ForceErrorNumErrorsSet,
               "att pdh de2 force error numerrors",
               "Set number of errors to force for DS2s/E2s\n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe2ErrorType", "")
               DEF_SDKPARAM("numErrors", "UINT", "Number of error each second"),
               "${de1s} ${errorType} ${numErrors}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2TxErrorGeneratorNumErrorsSet,
               "att pdh de2 tx error generator numerrors",
               "Set number of errors to force for DS2s/E2s\n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe2ErrorType", "")
               DEF_SDKPARAM("numErrors", "UINT", "Number of error each second"),
               "${de1s} ${errorType} ${numErrors}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2ForceErrorBitPositionSet,
               "att pdh de2 force error bitposition",
               "Set bit position\n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe2ErrorType", "PDH DE1 Error Type")
               DEF_SDKPARAM("bitposition", "BitPositionList", ""),
               "${de1s} ${errorType} ${bitposition}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2ForceErrorDataMaskSet,
               "att pdh de2 force error datamask",
               "Set data mask\n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe2ErrorType", "PDH DE1 Error Type")
               DEF_SDKPARAM("dataMask", "BitMaskList", "Data Bit Mask List"),
               "${de1s} ${errorType} ${dataMask}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2ForceErrorStepSet,
               "att pdh de2 force error step",
               "Set data mask\n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe2ErrorType", "PDH DE1 Error Type")
               DEF_SDKPARAM("step", "UINT", "Step"),
               "${de1s} ${errorType} ${step}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2ForceErrorStatusShow,
               "att show pdh de2 force error status",
               "Show error information\n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe2ErrorType", "PDH DE1 Error Type"),
               "${de1s} ${errorType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2ForceErrorShow,
               "att show pdh de2 force error",
               "Show force error information\n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List")
               DEF_SDKPARAM("errorType", "eAttPdhDe2ErrorType", "PDH DE1 Error Type"),
               "${de1s} ${errorType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2ForceAlarmMode,
               "att pdh de2 force alarm mode",
               "Force number of alarm per second\n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe2AlarmType", "PDH DE1 Alarm Type")
               DEF_SDKPARAM("mode", "AttForceAlarmMode", "Alarm Mode"),
               "${de1s} ${alarmType} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2TxAlarmGeneratorMode,
               "att pdh de2 tx alarm generator mode",
               "Force number of alarm per second\n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe2AlarmType", "PDH DE1 Alarm Type")
               DEF_SDKPARAM("mode", "AttForceAlarmMode", "Alarm Mode"),
               "${de1s} ${alarmType} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2ForceAlarmNumEventPerSecond,
               "att pdh de2 force alarm numevent",
               "Force number of alarm per second\n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe2AlarmType", "PDH DE1 Alarm Type")
               DEF_SDKPARAM("numevent", "UINT", "Number of event each second"),
               "${de1s} ${alarmType} ${numevent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2AlarmTxGeneratorNumEventPerSecond,
               "att pdh de2 tx alarm generator numevent",
               "Force number of alarm per second\n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe2AlarmType", "PDH DE1 Alarm Type")
               DEF_SDKPARAM("numevent", "UINT", "Number of event per second"),
               "${de1s} ${alarmType} ${numevent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2ForceAlarmDuration,
               "att pdh de2 force alarm duration",
               "Force alarm per second\n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe2AlarmType", "PDH DE1 Alarm Type")
               DEF_SDKPARAM("duration", "UINT", "Duration in seconds"),
               "${de1s} ${alarmType} ${duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2TxAlarmGeneratorDuration,
               "att pdh de2 tx alarm generator duration",
               "Force alarm per second\n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe2AlarmType", "PDH DE1 Alarm Type")
               DEF_SDKPARAM("duration", "UINT", "Duration in ms"),
               "${de1s} ${alarmType} ${duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2ForceAlarmNumFrame,
               "att pdh de2 force alarm numframe",
               "Force number of frame to set alarm\n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe2AlarmType", "PDH DE1 Alarm Type")
               DEF_SDKPARAM("numframe", "UINT", "Number of frame to set alarm"),
               "${de1s} ${alarmType} ${numframe}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2RxAlarmCaptureEnable,
               "att pdh de2 rx alarm capture enable",
               "Enable to capture RX alarm of DE1 \n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List"),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2RxAlarmCaptureDisable,
               "att pdh de2 rx alarm capture disable",
               "Disable to capture RX alarm oF DE1 \n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List"),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2RxAlarmCaptureDuration,
               "att pdh de2 rx alarm capture duration",
               "Set duration to capture alarm for DE1\n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe2AlarmType", "PDH DE1 Alarm Type")
               DEF_SDKPARAM("duration", "UINT", "Duration in ms"),
               "${de1s} ${alarmType} ${duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2RxAlarmCaptureMode,
               "att pdh de2 rx alarm capture mode",
               "Set duration to capture alarm for DE1\n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe2AlarmType", "PDH DE1 Alarm Type")
               DEF_SDKPARAM("mode", "AttForceAlarmMode", "Duration in ms"),
               "${de1s} ${alarmType} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2RxAlarmCaptureNumEvent,
               "att pdh de2 rx alarm capture numevent",
               "Set duration to capture alarm for DE1\n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe2AlarmType", "PDH DE1 Alarm Type")
               DEF_SDKPARAM("numevent", "UINT", "Number of event each second"),
               "${de1s} ${alarmType} ${numevent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2ForceAlarmStepSet,
               "att pdh de2 force alarm step",
               "Force alarm per second\n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe2AlarmType", "PDH DE1 Alarm Type")
               DEF_SDKPARAM("step", "UINT", "HW Step"),
               "${de1s} ${alarmType} ${step}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2ForceAlarm,
               "att pdh de2 force alarm",
               "Force alarm\n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe2AlarmType", "PDH DE1 Alarm Type")
               DEF_SDKPARAM("enable", "eBool", "Enable or Disable Force Alarm"),
               "${de1s} ${alarmType} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2FrequencyOffsetSet,
               "att pdh de2 freqoffs",
               "Configure frequency offset for DE1\n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List")
               DEF_SDKPARAM("freqOffs", "STRING", "ppm"),
               "${de1s} ${freqOffs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2WanderFileNameSet,
               "att pdh de2 wander filename",
               "Configure frequency offset for DE1\n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List")
               DEF_SDKPARAM("filename", "STRING", "ppm"),
               "${de1s} ${filename}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2WanderStart,
               "att pdh de2 wander start",
               "Configure frequency offset for DE1\n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List")
               DEF_SDKPARAM_OPTION("duration", "UINT", "Number of seconds"),
               "${de1s} ${duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2WanderStop,
               "att pdh de2 wander stop",
               "Configure frequency offset for DE1\n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List"),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2ForceAlarmStatusShow,
               "att show pdh de2 force alarm status",
               "Show alarm information\n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe2AlarmType", "PDH DE1 Alarm Type"),
               "${de1s} ${alarmType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2ForceAlarmShow,
               "att show pdh de2 force alarm",
               "Show force alarm information\n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe2AlarmType", "PDH DE1 Alarm Type"),
               "${de1s} ${alarmType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2RxAlarmCaptureShow,
               "att show pdh de2 rx alarm capture",
               "Show force alarm information\n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List")
               DEF_SDKPARAM("alarmType", "eAttPdhDe2AlarmType", "PDH DE1 Alarm Type")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${de1s} ${alarmType} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttPdhDe2FrequencyOffsetShow,
               "att show pdh de2 freqoffs",
               "Show frequency offset for DE1\n",
               DEF_SDKPARAM("de1s", "PdhDe2List", "PDH DE1 ID List"),
               "${de1s}")
    {
    mAtCliCall();
    }

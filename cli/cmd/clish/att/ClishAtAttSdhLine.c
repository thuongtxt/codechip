/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : ClishAtAttSdhLine.c
 *
 * Created Date: Jul 8, 2016
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"
#include "AtCli.h"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/* Top commands */
TOP_SDKCOMMAND("att sdh", "ATT SDH command\n")
TOP_SDKCOMMAND("att sdh line", "ATT SDH line command\n")
TOP_SDKCOMMAND("att sdh line force", "ATT SDH line force command\n")
TOP_SDKCOMMAND("att sdh line tx", "ATT SDH line Tx command\n")
TOP_SDKCOMMAND("att sdh line tx error", "ATT SDH line Tx Error command\n")
TOP_SDKCOMMAND("att sdh line tx error generator", "ATT SDH line Tx Error Generator command\n")
TOP_SDKCOMMAND("att sdh line tx alarm", "ATT SDH line Tx command\n")
TOP_SDKCOMMAND("att sdh line tx alarm generator", "ATT SDH line Tx alarm generator command\n")
TOP_SDKCOMMAND("att sdh line rx", "ATT SDH line Rx command\n")
TOP_SDKCOMMAND("att sdh line rx alarm", "ATT SDH line Rx alarm command\n")
TOP_SDKCOMMAND("att sdh line rx alarm capture", "ATT SDH line Rx alarm capture command\n")
TOP_SDKCOMMAND("att show sdh", "ATT show SDH command\n")
TOP_SDKCOMMAND("att show sdh line", "ATT show SDH line command\n")
TOP_SDKCOMMAND("att show sdh line force", "ATT show SDH line force command\n")
TOP_SDKCOMMAND("att show sdh line rx", "ATT show SDH line Rx command\n")
TOP_SDKCOMMAND("att show sdh line rx alarm", "ATT show SDH line Rx Alarm command\n")

DEF_PTYPE("eAtSdhLineAlarmType", "select", "los(1), ais(2), lof(3), oof(4), rdi(5)", "\nAlarm type:")
DEF_PTYPE("eAtSdhLineErrorType", "select", "b1(1), b2(2), rei(3)", "\nError type:")
DEF_PTYPE("BitPositionList", "regexp", ".*", "List of Bit Possition IDs. For example: 1-3,6,15 gives list of 1,2,3,6,15")
DEF_PTYPE("BitMaskList", "regexp", ".*", "List of Bit Possition IDs. For example: 1-3,6,7 gives list of 1,2,3,6,7")

DEF_SDKCOMMAND(CliAtAttSdhLineForceErrorMode,
               "att sdh line force error mode",
               "Set error forcing mode for SDH Lines\n",
               DEF_SDKPARAM("lines", "SdhLineList", "SDH Line ID List")
               DEF_SDKPARAM("errorType", "eAtSdhLineErrorType", "SDH Line Error Type")
               DEF_SDKPARAM("mode", "AttForceErrorMode", ""),
               "${lines} ${errorType} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhLineTxErrorGeneratorMode,
               "att sdh line tx error generator mode",
               "Set error forcing mode for SDH Lines\n",
               DEF_SDKPARAM("lines", "SdhLineList", "SDH Line ID List")
               DEF_SDKPARAM("errorType", "eAtSdhLineErrorType", "SDH Line Error Type")
               DEF_SDKPARAM("mode", "AttForceErrorMode", ""),
               "${lines} ${errorType} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhLineForceErrorPerSecond,
               "att sdh line force error persecond",
               "Force error per second\n",
               DEF_SDKPARAM("lines", "SdhLineList", "SDH Line ID List")
               DEF_SDKPARAM("errorType", "eAtSdhLineErrorType", "SDH Line Error Type")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${lines} ${errorType} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhLineForceError,
               "att sdh line force error",
               "Force error\n",
               DEF_SDKPARAM("lines", "SdhLineList", "SDH Line ID List")
               DEF_SDKPARAM("errorType", "eAtSdhLineErrorType", "SDH Line Error Type")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${lines} ${errorType} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhLineForceErrorRate,
               "att sdh line force error rate",
               "Set error rate to force error for SDH Lines\n",
               DEF_SDKPARAM("lines", "SdhLineList", "SDH Line ID List")
               DEF_SDKPARAM("errorType", "eAtSdhLineErrorType", "SDH Line Error Type")
               DEF_SDKPARAM("rate", "eAtBerRate", "Error rate"),
               "${lines} ${errorType} ${rate}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhLineTxErrorGeneratorRate,
               "att sdh line tx error generator rate",
               "Set error rate to force error for SDH Lines\n",
               DEF_SDKPARAM("lines", "SdhLineList", "SDH Line ID List")
               DEF_SDKPARAM("errorType", "eAtSdhLineErrorType", "SDH Line Error Type")
               DEF_SDKPARAM("rate", "eAtBerRate", "Error rate"),
               "${lines} ${errorType} ${rate}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhLineForceErrorDuration,
               "att sdh line force error duration",
               "Set duration to force error for SDH lines\n",
               DEF_SDKPARAM("lines", "SdhLineList", "SDH Line ID List")
               DEF_SDKPARAM("errorType", "eAtSdhLineErrorType", "SDH Line Error Type")
               DEF_SDKPARAM("duration", "UINT", "Duration in ms"),
               "${lines} ${errorType} ${duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhLineTxErrorGeneratorDuration,
               "att sdh line tx error generator duration",
               "Set duration to force error for SDH lines\n",
               DEF_SDKPARAM("lines", "SdhLineList", "SDH Line ID List")
               DEF_SDKPARAM("errorType", "eAtSdhLineErrorType", "SDH Line Error Type")
               DEF_SDKPARAM("duration", "UINT", "Duration in ms"),
               "${lines} ${errorType} ${duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhLineForceErrorStepSet,
               "att sdh line force error step",
               "Force error continuous\n",
               DEF_SDKPARAM("lines", "SdhLineList", "SDH Line ID List")
               DEF_SDKPARAM("errorType", "eAtSdhLineErrorType", "SDH Line Error Type")
               DEF_SDKPARAM("step", "UINT", "Duration in ms"),
               "${lines} ${errorType} ${step}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhLineForceErrorNumErrorsSet,
               "att sdh line force error numerrors",
               "Set number of errors\n",
               DEF_SDKPARAM("lines", "SdhLineList", "SDH Line ID List")
               DEF_SDKPARAM("errorType", "eAtSdhLineErrorType", "SDH Line Error Type")
               DEF_SDKPARAM("numErrors", "UINT", "Number of error each second"),
               "${lines} ${errorType} ${numErrors}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhLineForceErrorBitPositionSet,
               "att sdh line force error bitposition",
               "Set bit position for SDH Line\n",
               DEF_SDKPARAM("lines", "SdhLineList", "SDH Line ID List")
               DEF_SDKPARAM("errorType", "eAtSdhLineErrorType", "SDH Line Error Type")
               DEF_SDKPARAM("bitposition", "BitPositionList", ""),
               "${lines} ${errorType} ${bitposition}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhLineForceErrorDataMaskSet,
               "att sdh line force error datamask",
               "Set data mask for SDH Line\n",
               DEF_SDKPARAM("lines", "SdhLineList", "SDH Line ID List")
               DEF_SDKPARAM("errorType", "eAtSdhLineErrorType", "SDH Line Error Type")
               DEF_SDKPARAM("dataMask", "BitMaskList", "Data Bit Mask List"),
               "${lines} ${errorType} ${dataMask}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhLineForceAlarmMode,
               "att sdh line force alarm mode",
               "Force alarm per second\n",
               DEF_SDKPARAM("lines", "SdhLineList", "SDH Line List")
               DEF_SDKPARAM("alarmType", "eAtSdhLineAlarmType", "SDH Line Alarm Type")
               DEF_SDKPARAM("mode", "AttForceAlarmMode", "Alarm Forcing Mode"),
               "${lines} ${alarmType} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhLineTxAlarmGeneratorMode,
               "att sdh line tx alarm generator mode",
               "Force alarm per second\n",
               DEF_SDKPARAM("lines", "SdhLineList", "SDH Line List")
               DEF_SDKPARAM("alarmType", "eAtSdhLineAlarmType", "SDH Line Alarm Type")
               DEF_SDKPARAM("mode", "AttForceAlarmMode", "Alarm Forcing Mode"),
               "${lines} ${alarmType} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhLineForceAlarmNumEventPerSecond,
               "att sdh line force alarm numevent",
               "Force alarm per second\n",
               DEF_SDKPARAM("lines", "SdhLineList", "SDH Line List")
               DEF_SDKPARAM("alarmType", "eAtSdhLineAlarmType", "SDH Line Alarm Type")
               DEF_SDKPARAM("numevent", "UINT", "Number of alarm force in one second"),
               "${lines} ${alarmType} ${numevent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhLineTxAlarmGeneratorNumEventPerSecond,
               "att sdh line tx alarm generator numevent",
               "Force alarm per second\n",
               DEF_SDKPARAM("lines", "SdhLineList", "SDH Line List")
               DEF_SDKPARAM("alarmType", "eAtSdhLineAlarmType", "SDH Line Alarm Type")
               DEF_SDKPARAM("numevent", "UINT", "Number of alarm force in one second"),
               "${lines} ${alarmType} ${numevent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhLineForceAlarmDuration,
               "att sdh line force alarm duration",
               "Force alarm per second\n",
               DEF_SDKPARAM("lines", "SdhLineList", "SDH Line List")
               DEF_SDKPARAM("alarmType", "eAtSdhLineAlarmType", "SDH Line Alarm Type")
               DEF_SDKPARAM("duration", "UINT", "Duration in seconds"),
               "${lines} ${alarmType} ${duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhLineTxAlarmGeneratorDuration,
               "att sdh line tx alarm generator duration",
               "Force alarm per second\n",
               DEF_SDKPARAM("lines", "SdhLineList", "SDH Line List")
               DEF_SDKPARAM("alarmType", "eAtSdhLineAlarmType", "SDH Line Alarm Type")
               DEF_SDKPARAM("duration", "UINT", "Duration in ms"),
               "${lines} ${alarmType} ${duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhLineForceAlarmNumFrame,
               "att sdh line force alarm numframe",
               "Number of frame to set alarm\n",
               DEF_SDKPARAM("lines", "SdhLineList", "SDH Line List")
               DEF_SDKPARAM("alarmType", "eAtSdhLineAlarmType", "SDH Line Alarm Type")
               DEF_SDKPARAM("numframe", "UINT", "Number of frame to set alarm"),
               "${lines} ${alarmType} ${numframe}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhLineRxAlarmCaptureEnable,
               "att sdh line rx alarm capture enable",
               "Enable Rx Alarm Capture\n",
               DEF_SDKPARAM("lines", "SdhLineList", "SDH Line List"),
               "${lines} ${alarmType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhLineRxAlarmCaptureDisable,
               "att sdh line rx alarm capture disable",
               "Enable Rx Alarm Capture\n",
               DEF_SDKPARAM("lines", "SdhLineList", "SDH Line List"),
               "${lines} ${alarmType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhLineRxAlarmCaptureDuration,
               "att sdh line rx alarm capture duration",
               "Force alarm per second\n",
               DEF_SDKPARAM("lines", "SdhLineList", "SDH Line List")
               DEF_SDKPARAM("alarmType", "eAtSdhLineAlarmType", "SDH Line Alarm Type")
               DEF_SDKPARAM("duration", "UINT", "Duration in ms"),
               "${lines} ${alarmType} ${duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhLineRxAlarmCaptureNumEvent,
               "att sdh line rx alarm capture numevent",
               "Force alarm per second\n",
               DEF_SDKPARAM("lines", "SdhLineList", "SDH Line List")
               DEF_SDKPARAM("alarmType", "eAtSdhLineAlarmType", "SDH Line Alarm Type")
               DEF_SDKPARAM("numevent", "UINT", "Expected Number of event each second"),
               "${lines} ${alarmType} ${numevent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhLineRxAlarmCaptureMode,
               "att sdh line rx alarm capture mode",
               "Force alarm per second\n",
               DEF_SDKPARAM("lines", "SdhLineList", "SDH Line List")
               DEF_SDKPARAM("alarmType", "eAtSdhLineAlarmType", "SDH Line Alarm Type")
               DEF_SDKPARAM("mode", "AttForceAlarmMode", "Alarm Mode"),
               "${lines} ${alarmType} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhLineForceAlarmStep,
               "att sdh line force alarm step",
               "Force alarm per second\n",
               DEF_SDKPARAM("lines", "SdhLineList", "SDH Line List")
               DEF_SDKPARAM("alarmType", "eAtSdhLineAlarmType", "SDH Line Alarm Type")
               DEF_SDKPARAM("step", "UINT", "HW Step"),
               "${lines} ${alarmType} ${step}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhLineForceAlarm,
               "att sdh line force alarm",
               "Force alarm\n",
               DEF_SDKPARAM("lines", "SdhLineList", "SDH Line List")
               DEF_SDKPARAM("alarmType", "eAtSdhLineAlarmType", "SDH Line Alarm Type")
               DEF_SDKPARAM("enable", "eBool", "Enable or Disable to trigger alarm forcing"),
               "${lines} ${alarmType} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhLineForceAlarmStatusShow,
               "att show sdh line force alarm status",
               "Show alarm information\n",
               DEF_SDKPARAM("lines", "SdhLineList", "SDH Line ID List")
               DEF_SDKPARAM("alarmType", "eAtSdhLineAlarmType", "SDH Line Alarm Type"),
               "${lines} ${alarmType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhLineForceAlarmShow,
               "att show sdh line force alarm",
               "Show force alarm information\n",
               DEF_SDKPARAM("lines", "SdhLineList", "SDH Line ID List")
               DEF_SDKPARAM("alarmType", "eAtSdhLineAlarmType", "SDH Line Alarm Type"),
               "${lines} ${alarmType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhLineRxAlarmCaptureShow,
               "att show sdh line rx alarm capture",
               "Show rx alarm capture information\n",
               DEF_SDKPARAM("lines", "SdhLineList", "SDH Line ID List")
               DEF_SDKPARAM("alarmType", "eAtSdhLineAlarmType", "SDH Line Alarm Type")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${lines} ${alarmType} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhLineForceErrorStatusShow,
               "att show sdh line force error status",
               "Show error information for SDH Line\n",
               DEF_SDKPARAM("lines", "SdhLineList", "SDH Line ID List")
               DEF_SDKPARAM("errorType", "eAtSdhLineErrorType", "SDH Line Error Type"),
               "${lines} ${errorType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhLineForceErrorShow,
               "att show sdh line force error",
               "Show force error information for SDH Line\n",
               DEF_SDKPARAM("lines", "SdhLineList", "SDH Line ID List")
               DEF_SDKPARAM("errorType", "eAtSdhLineErrorType", "SDH Line Error Type"),
               "${lines} ${errorType}")
    {
    mAtCliCall();
    }

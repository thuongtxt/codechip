/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ATT
 *
 * File        : ClishAtAttSdhLine.c
 *
 * Created Date: Feb 15, 2017
 *
 * Description : ATT CLISH CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
DEF_PTYPE("eAtSdhRefClk", "select", "free-run(0), reference(1)", "\nReference clock:")

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
DEF_SDKCOMMAND(CliAtAttSdhRefClkSet,
               "att sdh refclk",
               "set reference clock\n",
               DEF_SDKPARAM("refClk", "eAtSdhRefClk", ""),
               "${refClk}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtAttSdhRefClkShow,
               "att show sdh refclk",
               "show reference clock\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

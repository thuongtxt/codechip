/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ATT
 *
 * File        : ClishAtAttSdhLine.c
 *
 * Created Date: Feb 15, 2017
 *
 * Description : ATT CLISH CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../AtClish.h"
#include "AtCommon.h"
#include "atclib.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
TOP_SDKCOMMAND("app force_pointer", "CLIs to control Force Pointer task")
/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void AppForcePointerTaskEnable(eBool enable);
void AppForcePointerTaskPeriodInMsSet(uint32 periodMs);
/*--------------------------- Implementation ---------------------------------*/



DEF_SDKCOMMAND(cliForcePointerEnable,
               "app force_pointer enable",
               "Enable Force Pointer task\r\n",
               DEF_NULLPARAM,
               "")
    {
    AtUnused(argv);
    AtUnused(context);
    AppForcePointerTaskEnable(cAtTrue);
    return cAtOk;
    }

DEF_SDKCOMMAND(cliForcePointerDisable,
               "app force_pointer disable",
               "Disable Force Pointer task\r\n",
               DEF_NULLPARAM,
               "")
    {
    AtUnused(argv);
    AtUnused(context);
    AppForcePointerTaskEnable(cAtFalse);
    return cAtOk;
    }

DEF_SDKCOMMAND(cliPollingForcePointerTaskPeriod,
               "app force_pointer period",
               "Set Force Pointer period\r\n",
               DEF_SDKPARAM("periodMs", "STRING", "Period in milliseconds"),
               "${periodMs}")
    {
    uint32 period = AtStrToDw(lub_argv__get_arg(argv, 0));
    AtUnused(argv);
    AtUnused(context);
    AppForcePointerTaskPeriodInMsSet(period);
    return cAtOk;
    }

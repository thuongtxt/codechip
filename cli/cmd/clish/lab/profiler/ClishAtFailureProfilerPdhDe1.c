/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Profiler
 *
 * File        : ClishAtFailureProfilerPdhDe1.c
 *
 * Created Date: Aug 5, 2017
 *
 * Description : PDH DS1/E1 CLISH CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("pdh de1 profile", "CLIs to handle PDH DS1/E1 profilers")
TOP_SDKCOMMAND("pdh de1 profile failure", "CLIs to handle PDH DS1/E1 failure profiler")
TOP_SDKCOMMAND("show pdh de1 profile", "CLIs to show PDH DS1/E1 profiles")

TOP_SDKCOMMAND("debug pdh de1 profile", "CLIs to debug PDH DS1/E1 profilers")
TOP_SDKCOMMAND("debug pdh de1 profile defect", "CLIs to debug PDH DS1/E1 profilers - Defect handling")
TOP_SDKCOMMAND("debug pdh de1 profile failure", "CLIs to debug PDH DS1/E1 profilers - Failure handling")

DEF_SDKCOMMAND(cliAtFailureProfilerPdhDe1Show,
               "show pdh de1 profile failure",
               "Show failure profiling information\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${de1s} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtFailureProfilerPdhDe1Start,
               "pdh de1 profile failure start",
               "Start failure profiling\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtFailureProfilerPdhDe1Stop,
               "pdh de1 profile failure stop",
               "Stop failure profiling\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtFailureProfilerPdhDe1DefectInject,
               "debug pdh de1 profile defect inject",
               "Inject defects with specified status\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("defects", "PdhDe1Defect", "")
               DEF_SDKPARAM("status", "eStatus", ""),
               "${de1s} ${defects} ${status}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtFailureProfilerPdhDe1FailureInject,
               "debug pdh de1 profile failure inject",
               "Inject failures with specified status\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("failures", "PdhDe1Defect", "")
               DEF_SDKPARAM("status", "eStatus", ""),
               "${de1s} ${failures} ${status}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtFailureProfilerPdhDe1CurrentDefectsShow,
               "show pdh de1 profile defect",
               "Show current defect information\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

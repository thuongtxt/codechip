/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Profiler
 *
 * File        : ClishAtFailureProfilerPw.c
 *
 * Created Date: Aug 5, 2017
 *
 * Description : PW CLISH CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("pw profile", "CLIs to handle PW profilers")
TOP_SDKCOMMAND("pw profile failure", "CLIs to handle PW failure profiler")
TOP_SDKCOMMAND("show pw profile", "CLIs to show PW profiles")

TOP_SDKCOMMAND("debug pw profile", "CLIs to debug PW profilers")
TOP_SDKCOMMAND("debug pw profile defect", "CLIs to debug PW profilers - Defect handling")
TOP_SDKCOMMAND("debug pw profile failure", "CLIs to debug PW profilers - Failure handling")

DEF_SDKCOMMAND(cliAtFailureProfilerPwShow,
               "show pw profile failure",
               "Show failure profiling information\n",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${pwList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtFailureProfilerPwStart,
               "pw profile failure start",
               "Start failure profiling\n",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtFailureProfilerPwStop,
               "pw profile failure stop",
               "Stop failure profiling\n",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtFailureProfilerPwFailureInject,
               "debug pw profile failure inject",
               "Inject failures with specified status\n",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("failures", "PwDefMask", "")
               DEF_SDKPARAM("status", "eStatus", ""),
               "${pwList} ${failures} ${status}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtFailureProfilerPwDefectInject,
               "debug pw profile defect inject",
               "Inject defects with specified status\n",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("defects", "PwDefMask", "")
               DEF_SDKPARAM("status", "eStatus", ""),
               "${pwList} ${defects} ${status}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtFailureProfilerPwCurrentDefectsShow,
               "show pw profile defect",
               "Show current defect information\n",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

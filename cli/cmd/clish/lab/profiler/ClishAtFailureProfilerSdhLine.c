/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Profiler
 *
 * File        : ClishAtFailureProfilerSdhLine.c
 *
 * Created Date: Aug 5, 2017
 *
 * Description : SDH Line CLISH CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("sdh line profile", "CLIs to handle SDH Line profilers")
TOP_SDKCOMMAND("sdh line profile failure", "CLIs to handle SDH Line failure profiler")
TOP_SDKCOMMAND("show sdh line profile", "CLIs to show SDH Line profiles")

TOP_SDKCOMMAND("debug sdh line profile", "CLIs to debug SDH Line profilers")
TOP_SDKCOMMAND("debug sdh line profile defect", "CLIs to debug SDH Line profilers - Defect handling")
TOP_SDKCOMMAND("debug sdh line profile failure", "CLIs to debug SDH Line profilers - Failure handling")

DEF_SDKCOMMAND(cliAtFailureProfilerSdhLineShow,
               "show sdh line profile failure",
               "Show failure profiling information\n",
               DEF_SDKPARAM("lines", "SdhLineList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", "Read to clear without display table"),
               "${lines} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtFailureProfilerSdhLineStart,
               "sdh line profile failure start",
               "Start failure profiling\n",
               DEF_SDKPARAM("lines", "SdhLineList", ""),
               "${lines}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtFailureProfilerSdhLineStop,
               "sdh line profile failure stop",
               "Stop failure profiling\n",
               DEF_SDKPARAM("lines", "SdhLineList", ""),
               "${lines}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtFailureProfilerSdhLineDefectInject,
               "debug sdh line profile defect inject",
               "Inject defects with specified status\n",
               DEF_SDKPARAM("lines", "SdhLineList", "")
               DEF_SDKPARAM("defects", "SdhLineDefMask", "")
               DEF_SDKPARAM("status", "eStatus", ""),
               "${lines} ${defects} ${status}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtFailureProfilerSdhLineFailureInject,
               "debug sdh line profile failure inject",
               "Inject failures with specified status\n",
               DEF_SDKPARAM("lines", "SdhLineList", "")
               DEF_SDKPARAM("failures", "SdhLinePmFailureMask", "")
               DEF_SDKPARAM("status", "eStatus", ""),
               "${lines} ${failures} ${status}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtFailureProfilerSdhLineCurrentDefectsShow,
               "show sdh line profile defect",
               "Show current defect information\n",
               DEF_SDKPARAM("lines", "SdhLineList", ""),
               "${lines}")
    {
    mAtCliCall();
    }

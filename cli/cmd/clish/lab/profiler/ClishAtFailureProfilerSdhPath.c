/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Profiler
 *
 * File        : ClishAtFailureProfilerSdhPath.c
 *
 * Created Date: Aug 5, 2017
 *
 * Description : SDH Path CLISH CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("sdh path profile", "CLIs to handle SDH Path profilers")
TOP_SDKCOMMAND("sdh path profile failure", "CLIs to handle SDH Path failure profiler")
TOP_SDKCOMMAND("show sdh path profile", "CLIs to show SDH Path profiles")

TOP_SDKCOMMAND("debug sdh path profile", "CLIs to debug SDH Path profilers")
TOP_SDKCOMMAND("debug sdh path profile defect", "CLIs to debug SDH Path profilers - Defect handling")
TOP_SDKCOMMAND("debug sdh path profile failure", "CLIs to debug SDH Path profilers - Failure handling")

DEF_SDKCOMMAND(cliAtFailureProfilerSdhPathShow,
               "show sdh path profile failure",
               "Show failure profiling\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${pathList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtFailureProfileSdhPathStart,
               "sdh path profile failure start",
               "Start failure profiling\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtFailureProfileSdhPathStop,
               "sdh path profile failure stop",
               "Stop failure profiling\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtFailureProfilerSdhPathDefectInject,
               "debug sdh path profile defect inject",
               "Inject defects with specified status\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("defects", "SdhDefMask", "")
               DEF_SDKPARAM("status", "eStatus", ""),
               "${pathList} ${defects} ${status}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtFailureProfilerSdhPathFailureInject,
               "debug sdh path profile failure inject",
               "Inject failures with specified status\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("failures", "SdhDefMask", "")
               DEF_SDKPARAM("status", "eStatus", ""),
               "${pathList} ${failures} ${status}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtFailureProfilerSdhPathCurrentDefectsShow,
               "show sdh path profile defect",
               "Show current defect information\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

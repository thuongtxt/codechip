/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Profiler
 *
 * File        : ClishAtFailureProfilerPdhDe3.c
 *
 * Created Date: Aug 5, 2017
 *
 * Description : PDH DS3/E3 CLISH CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("pdh de3 profile", "CLIs to handle PDH DS3/E3 profilers")
TOP_SDKCOMMAND("pdh de3 profile failure", "CLIs to handle PDH DS3/E3 failure profiler")
TOP_SDKCOMMAND("show pdh de3 profile", "CLIs to show PDH DS3/E3 profiles")

TOP_SDKCOMMAND("debug pdh de3 profile", "CLIs to debug PDH DS3/E3 profilers")
TOP_SDKCOMMAND("debug pdh de3 profile defect", "CLIs to debug PDH DS3/E3 profilers - Defect handling")
TOP_SDKCOMMAND("debug pdh de3 profile failure", "CLIs to debug PDH DS3/E3 profilers - Failure handling")

DEF_SDKCOMMAND(cliAtFailureProfilerPdhDe3Show,
               "show pdh de3 profile failure",
               "Show failure profiling information\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${de3s} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtFailureProfilerPdhDe3Start,
               "pdh de3 profile failure start",
               "Start failure profiling\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtFailureProfilerPdhDe3Stop,
               "pdh de3 profile failure stop",
               "Stop failure profiling\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtFailureProfilerPdhDe3DefectInject,
               "debug pdh de3 profile defect inject",
               "Inject defects with specified status\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("defects", "PdhDe3Defect", "")
               DEF_SDKPARAM("status", "eStatus", ""),
               "${de3s} ${defects} ${status}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtFailureProfilerPdhDe3FailureInject,
               "debug pdh de3 profile failure inject",
               "Inject failures with specified status\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("failures", "PdhDe3Defect", "")
               DEF_SDKPARAM("status", "eStatus", ""),
               "${de3s} ${failures} ${status}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtFailureProfilerPdhDe3CurrentDefectsShow,
               "show pdh de3 profile defect",
               "Show current defect information\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

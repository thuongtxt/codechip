/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TextUI
 *
 * File        : ClishAtTextUI.c
 *
 * Created Date: Nov 19, 2015
 *
 * Description : Text UI specific CLISH command
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
DEF_PTYPE("eAtCliMode", "select", "normal(1), autotest(2), tcl(3), silent(4)", "Text UI mode")

TOP_SDKCOMMAND("textui", "CLIs to control Text UI")
TOP_SDKCOMMAND("textui remote", "CLIs to control remote Text UI")
TOP_SDKCOMMAND("textui remote clireplay", "CLIs to control remote Text UI CLI relaying")
TOP_SDKCOMMAND("textui verbose", "CLIs to control verbose")
TOP_SDKCOMMAND("textui verbose script", "CLIs to control script verbose")

DEF_SDKCOMMAND(CliAtTextUIRemoteConnect,
               "textui remote connect",
               "Connect to remote Text UI to control it remotely. When a remote\r\n"
               "Text-UI is connected, all of input CLIs will be executed at the\r\n"
               "context of the remote and all of outputs are redirected to\r\n"
               "local Text UI console.",
               DEF_SDKPARAM("connection", "STRING", "Remote CLI server connection information. It must have format <ipAddress:port>"),
               "${entries}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtTextUIRemoteDisconnect,
               "textui remote disconnect",
               "Disconnect remote Text UI to return the control to the local one\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtTextRemoteTimeoutSet,
               "textui remote timeout",
               "Set receive timeout\n",
               DEF_SDKPARAM("timeoutSeconds", "UINT", "Receive timeout in seconds"),
               "${timeoutSeconds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtTextUIRemoteCliReplayEnable,
               "textui remote clireplay enable",
               "To enable replaying CLI to remote CLI server so that every CLI\r\n"
               "executed on the local will be replayed to the remote CLI server.\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtTextUIRemoteCliReplayDisable,
               "textui remote clireplay disable",
               "To disable replaying CLI to remote CLI server\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtTextUIShow,
               "show textui",
               "Show TextUI information\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtTextVerboseEnable,
               "textui verbose enable",
               "Verbose every command\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtTextVerboseDisable,
               "textui verbose disable",
               "Do not verbose every command\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCliModeSet,
               "textui mode",
               "Set Text UI mode\n",
               DEF_SDKPARAM("mode", "eAtCliMode", "Text UI mode"),
               "${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDebugTextUI,
               "debug textui",
               "Debug TextUI information\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtTextUIScriptVerboseEnable,
               "textui verbose script enable",
               "Display all of commands while running script\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtTextUIScriptVerboseDisable,
               "textui verbose script disable",
               "Do not display commands while running script\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

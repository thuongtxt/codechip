/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TextUI
 *
 * File        : ClishAtTextUILimit.c
 *
 * Created Date: May 7, 2016
 *
 * Description : CLIs to control processing limitation to give other threads
 *               chance to execute
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("textui limit", "CLIs to control CPU processing")
TOP_SDKCOMMAND("textui limit table", "CLI to control table CPU resting")

DEF_SDKCOMMAND(cliAtCliCpuRestTimeMsSet,
               "textui limit rest",
               "When this is set to greater than 0, after processing a number of\r\n"
               "channels, this delay in milisecond will be made to give other\r\n"
               "threads chance to execute\r\n",
               DEF_SDKPARAM("restTimeInMs", "UINT", "CPU rest time in miliseconds."),
               "${restTimeInMs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCliLimitShow,
               "show textui limit",
               "Show limit information\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCliLimitNumRestPwSet,
               "textui limit pw",
               "Set number of PWs to process then give CPU a rest\r\n",
               DEF_SDKPARAM("numPws", "UINT", "Number of PWs to process then give CPU a rest"),
               "${restTimeInMs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtCliLimitNumRestTdmChannelSet,
               "textui limit tdm",
               "Set number of TDM channels to process then give CPU a rest. TDM channels include DS1/DS3/VC/...\r\n",
               DEF_SDKPARAM("numTdmChannels", "UINT", "Number of TDM channels to process then give CPU a rest"),
               "${numTdmChannels}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliTableRestTimeMsSet,
               "textui limit table rest",
               "Set Time in ms to give CPU a rest\r\n",
               DEF_SDKPARAM("restTimeMs", "UINT", "Time in ms to give CPU a rest"),
               "${restTimeMs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliTableNumRestLinesSet,
               "textui limit table line",
               "Set number of lines to give CPU a rest\r\n",
               DEF_SDKPARAM("numLinesToRest", "UINT", "Number of lines to give CPU a rest"),
               "${numLinesToRest}")
    {
    mAtCliCall();
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TextUI
 *
 * File        : ClishAtTextUIStdout.c
 *
 * Created Date: Apr 8, 2018
 *
 * Description : Capture output
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("textui stdout", "CLIs to work with stdout")
TOP_SDKCOMMAND("textui stdout capture", "CLIs to work with stdout capturing")
TOP_SDKCOMMAND("textui stdout capture autoflush", "CLIs to work with stdout capturing autoflush")
TOP_SDKCOMMAND("show textui stdout", "CLIs to show stdout capturing")

DEF_SDKCOMMAND(cliAtTextUIStdoutCaptureEnable,
               "textui stdout capture enable",
               "Enable capturing stdout",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtTextUIStdoutCaptureDisable,
               "textui stdout capture disable",
               "Disable capturing stdout",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtTextUIStdoutCaptureAutoFlushEnable,
               "textui stdout capture autoflush enable",
               "Enable stdout capturing auto flush. When this is enabled, captured "
               "buffer is flushed before a new CLI is executed.",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtTextUIStdoutCaptureAutoFlushDisable,
               "textui stdout capture autoflush disable",
               "Disable stdout capturing auto flush. When this is enabled, captured "
               "buffer is not flushed before a new CLI is executed.",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtTextUIStdoutCaptureShow,
               "show textui stdout capture",
               "Show captured output",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtTextUIStdoutCaptureFlush,
               "textui stdout capture flush",
               "Flush capture output.",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

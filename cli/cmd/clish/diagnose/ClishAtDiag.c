/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ClishThaDiag
 *
 * File        : ClishThaDiag.c
 *
 * Created Date: Jun 12, 2015
 *
 * Description : CLIs for hardware diagnose purpose
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
DEF_SDKCOMMAND(cliDiagDe1Satop,
               "device diag de1satop",
               "Diagnose DE1 satop\n",
               DEF_SDKPARAM("frame", "eAtPdhDe1FrameType", "de1 frame type")
               DEF_SDKPARAM("checknum", "UINT", "Number of times to check")
               DEF_SDKPARAM("microSleep", "UINT", "Number of micro seconds between 2 checks"),
               "${frame} ${checknum} ${microSleep}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDiagDe1Cesop,
               "device diag de1cesop",
               "Diagnose DE1 cesop\n",
               DEF_SDKPARAM("frame", "eAtPdhDe1FrameType", "de1 frame type")
               DEF_SDKPARAM("checknum", "UINT", "Number of times to check")
               DEF_SDKPARAM("microSleep", "UINT", "Number of micro seconds between 2 checks"),
               "${frame} ${checknum} ${microSleep}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDiagDe3Satop,
               "device diag de3satop",
               "Diagnose DE3 Satop\n",
               DEF_SDKPARAM("frame", "eAtPdhDe3FrameType", "de3 frame type")
               DEF_SDKPARAM("checknum", "UINT", "Number of times to check")
               DEF_SDKPARAM("microSleep", "UINT", "Number of micro seconds between 2 checks"),
               "${frame} ${checknum} ${microSleep}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDiagDe3De1Satop,
               "device diag de3de1satop",
               "Diagnose M13 DE1 Satop\n",
               DEF_SDKPARAM("de3frame", "eAtPdhDe3FrameType", "de3 frame type")
               DEF_SDKPARAM("frame", "eAtPdhDe1FrameType", "de1 frame type")
               DEF_SDKPARAM("checknum", "UINT", "Number of times to check")
               DEF_SDKPARAM("microSleep", "UINT", "Number of micro seconds between 2 checks"),
               "${de1frame} ${frame} ${checknum} ${microSleep}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDiagDe3De1Cesop,
               "device diag de3de1cesop",
               "Diagnose M13 DE1 Cesop\n",
               DEF_SDKPARAM("de3frame", "eAtPdhDe3FrameType", "de3 frame type")
               DEF_SDKPARAM("frame", "eAtPdhDe1FrameType", "de1 frame type")
               DEF_SDKPARAM("checknum", "UINT", "Number of times to check")
               DEF_SDKPARAM("microSleep", "UINT", "Number of micro seconds between 2 checks"),
               "${de1frame} ${frame} ${checknum} ${microSleep}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDiagEc1De3Satop,
               "device diag ec1de3satop",
               "Diagnose EC1-DE3 Satop\n",
               DEF_SDKPARAM("frame", "eAtPdhDe3FrameType", "de3 frame type")
               DEF_SDKPARAM("checknum", "UINT", "Number of times to check")
               DEF_SDKPARAM("microSleep", "UINT", "Number of micro seconds between 2 checks"),
               "${frame} ${checknum} ${microSleep}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDiagEc1De3De1Satop,
               "device diag ec1de3de1satop",
               "Diagnose EC1 M13 DE1 Satop\n",
               DEF_SDKPARAM("de3frame", "eAtPdhDe3FrameType", "de3 frame type")
               DEF_SDKPARAM("frame", "eAtPdhDe1FrameType", "de1 frame type")
               DEF_SDKPARAM("checknum", "UINT", "Number of times to check")
               DEF_SDKPARAM("microSleep", "UINT", "Number of micro seconds between 2 checks"),
               "${de1frame} ${frame} ${checknum} ${microSleep}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDiagEc1De3De1Cesop,
               "device diag ec1de3de1cesop",
               "Diagnose EC1 M13 DE1 Cesop\n",
               DEF_SDKPARAM("de3frame", "eAtPdhDe3FrameType", "de3 frame type")
               DEF_SDKPARAM("frame", "eAtPdhDe1FrameType", "de1 frame type")
               DEF_SDKPARAM("checknum", "UINT", "Number of times to check")
               DEF_SDKPARAM("microSleep", "UINT", "Number of micro seconds between 2 checks"),
               "${de1frame} ${frame} ${checknum} ${microSleep}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDiagEc1Vc1xDe1Satop,
               "device diag ec1vc1xde1satop",
               "Diagnose DE1 satop\n",
               DEF_SDKPARAM("frame", "eAtPdhDe1FrameType", "de1 frame type")
               DEF_SDKPARAM("checknum", "UINT", "Number of times to check")
               DEF_SDKPARAM("microSleep", "UINT", "Number of micro seconds between 2 checks"),
               "${frame} ${checknum} ${microSleep}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDiagEc1Vc1xDe1Cesop,
               "device diag ec1vc1xde1cesop",
               "Diagnose DE1 cesop\n",
               DEF_SDKPARAM("frame", "eAtPdhDe1FrameType", "de1 frame type")
               DEF_SDKPARAM("checknum", "UINT", "Number of times to check")
               DEF_SDKPARAM("microSleep", "UINT", "Number of micro seconds between 2 checks"),
               "${frame} ${checknum} ${microSleep}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDiagEc1VcCep,
               "device diag ec1vccep",
               "Diagnose VC CEP basic\n",
               DEF_SDKPARAM("frame", "eAtCepFrameType", "vc frame type: vc3, vc12, vc11")
               DEF_SDKPARAM("checknum", "UINT", "Number of times to check")
               DEF_SDKPARAM("microSleep", "UINT", "Number of micro seconds between 2 checks"),
               "${frame} ${checknum} ${microSleep}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDiagStmVcBert,
               "device diag stmvcbert",
               "Diagnose VC bert\n",
               DEF_SDKPARAM("stmlineid", "SdhLineList", "STM line ID")
               DEF_SDKPARAM("auframe", "eAtAuFrameType", "au frame type: au4, au3")
               DEF_SDKPARAM("frame", "eAtCepFrameType", "vc frame type: vc4, vc3, vc12, vc11")
               DEF_SDKPARAM("checknum", "UINT", "Number of times to check")
               DEF_SDKPARAM("microSleep", "UINT", "Number of micro seconds between 2 checks"),
               "${stmlineid} ${auframe} ${frame} ${checknum} ${microSleep}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDiagStmVcDe1Bert,
               "device diag stmvc1xde1bert",
               "Diagnose VC DE1 TDM BERT\n",
               DEF_SDKPARAM("stmlineid", "SdhLineList", "STM line ID")
               DEF_SDKPARAM("auframe", "eAtAuFrameType", "Au frame type: au4, au3")
               DEF_SDKPARAM("de1frame", "eAtPdhDe1FrameType", "DE1 frame type")
               DEF_SDKPARAM("checknum", "UINT", "Number of times to check")
               DEF_SDKPARAM("microSleep", "UINT", "Number of micro seconds between 2 checks"),
               "${stmlineid} ${auframe} ${de1frame} ${checknum} ${microSleep}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDiagStmDe3Bert,
               "device diag stmde3bert",
               "Diagnose VC DE3 TDM BERT\n",
               DEF_SDKPARAM("stmlineid", "SdhLineList", "STM line ID")
               DEF_SDKPARAM("auframe", "eAtAuFrameType", "vc frame type: au4, au3")
               DEF_SDKPARAM("de3frame", "eAtPdhDe3FrameType", "DE3 frame type")
               DEF_SDKPARAM("checknum", "UINT", "Number of times to check")
               DEF_SDKPARAM("microSleep", "UINT", "Number of micro seconds between 2 checks"),
               "${stmlineid} ${auframe} ${de3frame} ${checknum} ${microSleep}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDiagStmDe3De1Bert,
               "device diag stmde3de1bert",
               "Diagnose VC DE3 DE1 TDM BERT\n",
               DEF_SDKPARAM("stmlineid", "SdhLineList", "STM line ID")
               DEF_SDKPARAM("auframe", "eAtAuFrameType", "vc frame type: au4, au3")
               DEF_SDKPARAM("de3frame", "eAtPdhDe3FrameType", "DE3 frame type")
               DEF_SDKPARAM("de1frame", "eAtPdhDe1FrameType", "DE1 frame type")
               DEF_SDKPARAM("checknum", "UINT", "Number of times to check")
               DEF_SDKPARAM("microSleep", "UINT", "Number of micro seconds between 2 checks"),
               "${stmlineid} ${auframe} ${de3frame} ${de1frame} ${checknum} ${microSleep}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDiagStmVcDe1SatopBert,
               "device diag stmvc1xde1satopbert",
               "Diagnose VC DE1 TDM BERT\n",
               DEF_SDKPARAM("stmlineid", "SdhLineList", "STM line ID")
               DEF_SDKPARAM("auframe", "eAtAuFrameType", "vc frame type: au4, au3")
               DEF_SDKPARAM("de1frame", "eAtPdhDe1FrameType", "DE1 frame type")
               DEF_SDKPARAM("checknum", "UINT", "Number of times to check")
               DEF_SDKPARAM("microSleep", "UINT", "Number of micro seconds between 2 checks"),
               "${stmlineid} ${auframe} ${de1frame} ${checknum} ${microSleep}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDiagStmDe3SatopBert,
               "device diag stmde3satopbert",
               "Diagnose VC DE3 TDM BERT\n",
               DEF_SDKPARAM("stmlineid", "SdhLineList", "STM line ID")
               DEF_SDKPARAM("auframe", "eAtAuFrameType", "vc frame type: au4, au3")
               DEF_SDKPARAM("de3frame", "eAtPdhDe3FrameType", "DE3 frame type")
               DEF_SDKPARAM("checknum", "UINT", "Number of times to check")
               DEF_SDKPARAM("microSleep", "UINT", "Number of micro seconds between 2 checks"),
               "${stmlineid} ${auframe} ${de3frame} ${checknum} ${microSleep}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDiagStmDe3De1SatopBert,
               "device diag stmde3de1satopbert",
               "Diagnose VC DE3 DE1 TDM BERT\n",
               DEF_SDKPARAM("stmlineid", "SdhLineList", "STM line ID")
               DEF_SDKPARAM("auframe", "eAtAuFrameType", "vc frame type: au4, au3")
               DEF_SDKPARAM("de3frame", "eAtPdhDe3FrameType", "DE3 frame type")
               DEF_SDKPARAM("de1frame", "eAtPdhDe1FrameType", "DE1 frame type")
               DEF_SDKPARAM("checknum", "UINT", "Number of times to check")
               DEF_SDKPARAM("microSleep", "UINT", "Number of micro seconds between 2 checks"),
               "${stmlineid} ${auframe} ${de3frame} ${de1frame} ${checknum} ${microSleep}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDiagStmVcDe1CesopBert,
               "device diag stmvc1xde1cesopbert",
               "Diagnose VC DE1 TDM BERT\n",
               DEF_SDKPARAM("stmlineid", "SdhLineList", "STM line ID")
               DEF_SDKPARAM("auframe", "eAtAuFrameType", "vc frame type: au4, au3")
               DEF_SDKPARAM("de1frame", "eAtPdhDe1FrameType", "DE1 frame type")
               DEF_SDKPARAM("checknum", "UINT", "Number of times to check")
               DEF_SDKPARAM("microSleep", "UINT", "Number of micro seconds between 2 checks"),
               "${stmlineid} ${auframe} ${de1frame} ${checknum} ${microSleep}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDiagStmDe3De1CesopBert,
               "device diag stmde3de1cesopbert",
               "Diagnose VC DE3 DE1 TDM BERT\n",
               DEF_SDKPARAM("stmlineid", "SdhLineList", "STM line ID")
               DEF_SDKPARAM("auframe", "eAtAuFrameType", "vc frame type: au4, au3")
               DEF_SDKPARAM("de3frame", "eAtPdhDe3FrameType", "DE3 frame type")
               DEF_SDKPARAM("de1frame", "eAtPdhDe1FrameType", "DE1 frame type")
               DEF_SDKPARAM("checknum", "UINT", "Number of times to check")
               DEF_SDKPARAM("microSleep", "UINT", "Number of micro seconds between 2 checks"),
               "${stmlineid} ${auframe} ${de3frame} ${de1frame} ${checknum} ${microSleep}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDiagStmVcPwBert,
               "device diag stmvccepbert",
               "Diagnose VC PW bert\n",
               DEF_SDKPARAM("stmlineid", "SdhLineList", "STM line ID")
               DEF_SDKPARAM("auframe", "eAtAuFrameType", "au frame type: au4, au3")
               DEF_SDKPARAM("frame", "eAtCepFrameType", "vc frame type: vc4, vc3, vc12, vc11")
               DEF_SDKPARAM("checknum", "UINT", "Number of times to check")
               DEF_SDKPARAM("microSleep", "UINT", "Number of micro seconds between 2 checks"),
               "${stmlineid} ${auframe} ${frame} ${checknum} ${microSleep}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliStmVcLoop,
               "device diag stmvcloop",
               "Set global VC loopback mode for diagnose\n",
               DEF_SDKPARAM("loopbackMode", "eAtLoopbackMode", "VC loopback mode"),
               "${loopbackMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliStmPdhLoop,
               "device diag stmpdhloop",
               "Set global PDH loopback mode for diagnose\n",
               DEF_SDKPARAM("loopbackMode", "eAtPdhLoopbackMode", "PDH loopback mode"),
               "${loopbackMode}")
    {
    mAtCliCall();
    }

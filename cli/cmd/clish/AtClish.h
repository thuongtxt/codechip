/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLI
 * 
 * File        : AtClish.h
 * 
 * Created Date: Nov 8, 2012
 *
 * Description : CLISH CLI
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCLISH_H_
#define _ATCLISH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "sdkcmd.h"
#include "AtCliModule.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define mAtCliResult() AtTextUICmdProcess(AtCliSharedTextUI(), context->full_command) ? 0 : -1
#define mAtCliCall()                                                           \
    do  {                                                                      \
        AtUnused(context);                                                     \
        AtUnused(argv);                                                        \
        return mAtCliResult();                                                 \
        }while(0)

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _ATCLISH_H_ */


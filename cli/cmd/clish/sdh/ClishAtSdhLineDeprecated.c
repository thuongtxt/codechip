/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2011 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SONET/SDH
 *
 * File        : ClishAtSdhLineDeprecated.c
 *
 * Created Date: Nov 14, 2011
 *
 * Description : SONET/SDH Line CLI implementation
 *
 * Notes       :
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "../AtClish.h"
#include "../physical/ClishAtEyeScan.h"
#include "../../physical/CliAtEyeScan.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implement Functions ----------------------------*/

/*------------------- Commands Implementation --------------------------------*/
/* Top commands */
TOP_SDKCOMMAND("sdh line drp", "SDH line eyescan DRP CLIs\n")
TOP_SDKCOMMAND("sdh line eyescan stepsize", "SDH line eye scan Step size\n")
TOP_SDKCOMMAND("sdh line eyescan debug", "SDH Line eye scan debug\n")

static void EnableEyeScanTask(void)
    {
    ClishEnableEyeScanTask();
    }

DEF_SDKCOMMAND(cliAtSdhLineEyeScan,
               "sdh line eyescan",
               "Eye scan",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    AtList controllers;
    char *idStr = AtStrdup(lub_argv__get_arg(argv, 0));
    AtUnused(context);
    AtUnused(argv);

    /* Register listener */
    controllers = CliAtSdhLineSerdesEyeScanControllersFromIdString(idStr);
    CliAtEyeScanControllerDefaultListenerRegister(controllers);
    AtObjectDelete((AtObject)controllers);

    /* Enable task */
    EnableEyeScanTask();

    return 0;
    }

DEF_SDKCOMMAND(cliAtSdhLineEyeScanLaneDrpRead,
               "sdh line drp rd",
               "Read DRP register",
               DEF_SDKPARAM("line.laneId", "STRING", "Lane ID")
               DEF_SDKPARAM("drpAddress", "RegisterAddress", "")
               DEF_SDKPARAM_OPTION("pretty", "RegisterDisplayFormat", ""),
               "${line.laneId} ${drpAddress} ${pretty}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineEyeScanLaneDrpWrite,
               "sdh line drp wr",
               "Write DRP register",
               DEF_SDKPARAM("line.laneId", "STRING", "")
               DEF_SDKPARAM("drpAddress", "RegisterAddress", "")
               DEF_SDKPARAM("value", "RegisterValue", ""),
               "${line.laneId} ${laneId} ${drpAddress} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineEyeScanLaneStepSizeHorizonalSet,
               "sdh line eyescan stepsize horizonal",
               "Change horizontal step",
               DEF_SDKPARAM("lanes", "SdhLineEyeScanLanes", "")
               DEF_SDKPARAM("stepSize", "UINT", "Horizontal step size"),
               "${lanes} ${stepSize}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineEyeScanLaneStepSizeVerticalSet,
               "sdh line eyescan stepsize vertical",
               "Change vertical step",
               DEF_SDKPARAM("lanes", "SdhLineEyeScanLanes", "")
               DEF_SDKPARAM("stepSize", "UINT", "Vertical step size"),
               "${lanes} ${stepSize}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineEyeScanLaneMaxPrescaleSet,
               "sdh line eyescan maxPrescale",
               "Change max Prescale",
               DEF_SDKPARAM("lanes", "SdhLineEyeScanLanes", "")
               DEF_SDKPARAM("maxPrescale", "UINT", "Max prescale"),
               "${lanes} ${maxPrescale}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineEyeScanLaneDatawidthSet,
               "sdh line eyescan datawidth",
               "Change data width",
               DEF_SDKPARAM("lanes", "SdhLineEyeScanLanes", "")
               DEF_SDKPARAM("dataWidth", "UINT", "Data width"),
               "${lanes} ${dataWidth}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineEyeScanLaneShow,
               "show sdh line eyescan",
               "Show Eye scanning information",
               DEF_SDKPARAM("lanes", "SdhLineEyeScanLanes", ""),
               "${lanes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineEyeScanLaneEnable,
               "sdh line eyescan enable",
               "Enable Eye scanning",
               DEF_SDKPARAM("lanes", "SdhLineEyeScanLanes", ""),
               "${lanes}")
    {
    int result = mAtCliResult();
    AtUnused(argv);
    EnableEyeScanTask();
    return result;
    }

DEF_SDKCOMMAND(cliAtSdhLineEyeScanLaneDisable,
               "sdh line eyescan disable",
               "Disable Eye scanning",
               DEF_SDKPARAM("lanes", "SdhLineEyeScanLanes", ""),
               "${lanes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineEyeScanControllerEqualizerSet,
               "sdh line eyescan equalizer",
               "Change Equalizer",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("mode", "eAtEyeScanEqualizerMode", "Equalizer mode"),
               "${lineList} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineEyeScanLaneDebugEnable,
               "sdh line eyescan debug enable",
               "Enable eye-scan internal state machine debug",
               DEF_SDKPARAM("lanes", "SdhLineEyeScanLanes", ""),
               "${lanes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineEyeScanLaneDebugDisable,
               "sdh line eyescan debug disable",
               "Disable eye-scan internal state machine debug",
               DEF_SDKPARAM("lanes", "SdhLineEyeScanLanes", ""),
               "${lanes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineEyeScanLaneDebug,
               "debug sdh line eyescan lane",
               "Show lane debug information",
               DEF_SDKPARAM("lanes", "SdhLineEyeScanLanes", ""),
               "${lanes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineEyeScanControllerDebug,
               "debug sdh line eyescan",
               "Show eye-scan debug information",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }


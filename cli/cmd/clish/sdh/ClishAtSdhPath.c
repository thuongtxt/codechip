/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2011 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SONET/SDH
 *
 * File        : CliSdhPath.c
 *
 * Created Date: Nov 13, 2012
 *
 * Description : path CLIs
 *
 * Notes       :
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
/* Define High-order path ID list
 * - BNF format of High-order path ID
 * HoPathList     ::= ((HoPathList[,]HoPathListItem)|HoPathListItem)
 * HoPathListItem ::= ((Au4NcId[-]Au4NcId)|(Au3Id[-]Au3Id)|Au4NcId|Au3Id)
 * Au4ncId        ::= (lineno.aug1no)
 * Au3Id          ::= (lineno.aug1no.au3no)
 * lineno         ::= [1-8]
 * aug1no         ::= (1[0-6]|[1-9])
 * au3no          ::= [0-3]
 */
#define HoPathList "((("HoPathListItem"[,])+"HoPathListItem")|"HoPathListItem")"
#define HoPathListItem "(("Au4NcId"[-]"Au4NcId")|("Au3Id"[-]"Au3Id")|"Au4NcId"|"Au3Id")"
#define Au4NcId        "("regLineno"[.]"regAug1no")"
#define Au3Id          "("regLineno"[.]"regAug1no"[.]"regAu3no")"
/*
 * Define list of low-order path ID regular expression
 *
 * - BNF format of Low-order path ID:
 * loPathList     ::= ((loPathListItem[,]loPathListItem)|loPathListItem)
 * loPathListItem ::= (tuId|tu3Id|(tuId[-]tuId)|(tu3Id[-]tu3Id))
 * tu3Id          ::= (lineno.aug1no.tug3no)
 * tuId           ::= (lineno.aug1no.au3tug3no.tug2no.tuno)
 * loPathId       ::= (tu3Id|tuId)
 * lineno         ::= [1-8]
 * aug1no         ::= (1[0-6]|[1-9])
 * tug3no         ::= [1-3]
 * au3tug3no      ::= [1-3]
 * tug2no         ::= [1-7]
 * tuno           ::= [1-4]
 */
#define tu3Id "("regLineno"[.]"regAug1no"[.]"regTug3no")"
#define tuId "("regLineno"[.]"regAug1no"[.]"regAu3tug3no"[.]"regTug2no"[.]"regTuno")"
#define loPathId "("tuId"|"tu3Id")"
#define loPathListItem "("tuId"|"tu3Id"|("tuId"[-]"tuId")|("tu3Id"[-]"tu3Id"))"
#define loPathList "((("loPathListItem"[,])+"loPathListItem")|"loPathListItem")"

/*
 * Define pathList
 * */
#define PathList "("HoPathList"|"loPathList")"

#define PathDefType "(ais|lop|tim|uneq|p-uneq|plm|rdi|erdi-s|erdi-p|erdi-c|ber-sd|ber-sf|ber-tca|lom|rfi|clock-state-change|tti-change|psl-change)"
#define PathDefMask "((("PathDefType"[\\|])+"PathDefType")|"PathDefType")"

#define Uint32Value  "(0|[1-9][0-9]{0,7}|[1-3][0-9]{1,9}|4[0-1][0-9]{1,8}|42[0-8][0-9]{1,7}"\
                     "|429[0-3][0-9]{1,6}|4294[0-8][0-9]{1,5}|42949[0-5][0-9]{1,4}|429496[0-6][0-9]{1,3}"\
                     "|4294967[0-1][0-9]{1,2}|42949672[0-8][0-9]|429496729[0-5])" /* [0, 4294967295] */

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implement Functions ----------------------------*/

/*------------------- Commands Implementation --------------------------------*/
/* Top commands */
TOP_SDKCOMMAND("sdh", "Sdh module\n")
TOP_SDKCOMMAND("sdh path", "Order path\n")
TOP_SDKCOMMAND("sdh path erdi", "Order path\n")
TOP_SDKCOMMAND("sdh path tti", "Trail Trace Message Identifier (TTI)\n")
TOP_SDKCOMMAND("sdh path psl", "Path Signal Label (PSL)\n")
TOP_SDKCOMMAND("show sdh", "Show SDH Path\n")
TOP_SDKCOMMAND("sdh path force", "SDH Force Path Alarm CLIs")
TOP_SDKCOMMAND("sdh path ss", "CLIs to control SS bit\n")
TOP_SDKCOMMAND("sdh path overhead", "CLIs to control overhead byte\n")
TOP_SDKCOMMAND("sdh path alarm", "Path alarm\n")
TOP_SDKCOMMAND("sdh path alarm affect", "Path alarm affect enable\n")
TOP_SDKCOMMAND("sdh path ber", "Ber configuration\n")
TOP_SDKCOMMAND("sdh path ber monitor", "Ber monitor configuration\n")
TOP_SDKCOMMAND("sdh path tti monitor", "TTI monitoring CLIs")
TOP_SDKCOMMAND("sdh path tti compare", "TTI comparing CLIs")
TOP_SDKCOMMAND("sdh path autordi", "Path auto RDI configuration\n")
TOP_SDKCOMMAND("sdh path rxtraffic", "Path Rx traffic\n")
TOP_SDKCOMMAND("sdh path txtraffic", "Path Rx traffic\n")
TOP_SDKCOMMAND("sdh path plm", "PLM CLIs")
TOP_SDKCOMMAND("sdh path plm monitor", "PLM monitoring CLIs")
TOP_SDKCOMMAND("sdh path log", "SDH path logging CLIs")
TOP_SDKCOMMAND("show sdh path listen", "SDH path listener CLIs")
TOP_SDKCOMMAND("sdh path holdoff", "SDH path hold-off CLIs")
TOP_SDKCOMMAND("sdh path holdon", "SDH path hold-on CLIs")
TOP_SDKCOMMAND("sdh vc", "SDH VC specific CLIs")
TOP_SDKCOMMAND("sdh vc stuffing", "SDH VC stuffing CLIs")

/* Data types */
DEF_PTYPE("SdhPathList", "regexp", "au[3-4].*|tu[13].*|vc[13-4].*",
          "List of paths\n"
          "- AU4-16c: au4_16c.<lineId>.<aug16Id>\n"
          "- AU4-4c : au4_4c.<lineId>.<aug4Id>\n"
          "- AU4-16nc: au4_16nc.<lineId>.<aug16Id>\n"
          "- AU4-nc : au4_nc.<lineId>.<aug1Id>\n"
          "- AU4    : au4.<lineId>.<aug1Id>\n"
          "- AU3    : au3.<lineId>.<aug1Id>.<au3Id>\n"
          "- TU-3   : tu3.<lineId>.<aug1Id>.<tu3Id>\n"
          "- TU-1x  : tu1x.<lineId>.<aug1Id>.<au3Tug3Id>.<tug2Id>.<tu1xId>\n"
          "- VC4-16c: vc4_16c.<lineId>.<aug16Id>\n"
          "- VC4-4c : vc4_4c.<lineId>.<aug4Id>\n"
          "- VC4-16nc: vc4_16nc.<lineId>.<aug16Id>\n"
          "- VC4-nc : vc4_nc.<lineId>.<aug1Id>\n"
          "- VC4    : vc4.<lineId>.<aug1Id>\n"
          "- VC3    : vc3.<lineId>.<aug1Id>.<au3Tug3Id>\n"
          "- VC-1x  : vc1x.<lineId>.<aug1Id>.<au3Tug3Id>.<tug2Id>.<vcxId>\n")
DEF_PTYPE("BerSdhPathList", "regexp", "vc[13-4].*",
        "List of paths support BER\n"
        "- VC4-16c: vc4_16c.<lineId>.<aug16Id>\n"
        "- VC4-4c : vc4_4c.<lineId>.<aug4Id>\n"
        "- VC4-16nc: vc4_16nc.<lineId>.<aug16Id>\n"
        "- VC4-nc : vc4_nc.<lineId>.<aug1Id>\n"
        "- VC4    : vc4.<lineId>.<aug1Id>\n"
        "- VC3    : vc3.<lineId>.<aug1Id>.<au3Id/tug3Id>\n"
        "- VC-1x  : tu1x.<lineId>.<aug1Id>.<au3Tug3Id>.<tug2Id>.<vcxId>\n")

DEF_PTYPE("SdhDefMask", "regexp", PathDefMask,
          "\norder-path defect masks:\n"
          "- ais    : AIS\n"
          "- lop    : LOP\n"
          "- tim    : TIM\n"
          "- uneq   : UNEQ\n"
          "- p-uneq : VC-UNEQ\n"
          "- plm    : PLM\n"
          "- rdi    : RDI\n"
          "- erdi-s : ERDI-S\n"
          "- erdi-p : ERDI-P\n"
          "- erdi-c : ERDI-C\n"
          "- ber-sd : BER-SD\n"
          "- ber-sf : BER-SF\n"
          "- ber-tca: BER-TCA\n"
          "- lom    : LOM\n"
          "- rfi    : RFI\n"
          "- clock-state-change: ACR/DCR clock-state has change.\n"
          "- tti-change  : Path has received new TTI message\n"
          "- psl-change  : Path has received new PSL value\n"
          "Note, these masks can be ORed, example ais|lop|tim\n")

/* Alarm Force type for SDH path */
DEF_PTYPE("SdhPathFrcAlarmType", "select",
        "ais(0), lop(1), uneq(2), plm(3), rdi(4), erdi-s(5), erdi-p(6), erdi-c(7), ber-sd(8), ber-sf(9), ber-tca(10), lom(11), rfi(12)",
        "\nLine alarm defect masks:\n"
        "- ais    : AIS\n"
        "- lop    : LOP\n"
        "- uneq   : UNEQ\n"
        "- p-uneq : VC-UNEQ\n"
        "- plm    : PLM\n"
        "- rdi    : RDI\n"
        "- erdi-s  : ERDIS\n"
        "- erdi-p  : ERDIP\n"
        "- erdi-c  : ERDIC\n"
        "- ber-sd : BER-SD\n"
        "- ber-sf : BER-SF\n"
        "- ber-tca: BER-TCA\n"
        "- lom    : LOM\n"
        "- rfi    : RFI\n")

DEF_PTYPE("eAtSdhPathCounterType", "select", "bip(0), rei(1), none(2)", "Error counter type at path\n")

DEF_PTYPE("SdhPathVcList", "regexp", ".*",
          "List of paths\n"
          "- VC4-64c: vc4_64c.<lineId>.<aug64Id>\n"
          "- VC4-16c: vc4_16c.<lineId>.<aug16Id>\n"
          "- VC4-4c : vc4_4c.<lineId>.<aug4Id>\n"
          "- VC4-16nc: vc4_16nc.<lineId>.<aug16Id>\n"
          "- VC4-nc : vc4_nc.<lineId>.<aug1Id>\n"
          "- VC4    : vc4.<lineId>.<aug1Id>\n"
          "- VC3    : vc3.<lineId>.<aug1Id>.<au3Id/tug3Id> or vc3.<ec1-lineId>\n"
          "- VC-1x  : vc1x.<lineId>.<aug1Id>.<au3Tug3Id>.<tug2Id>.<vcxId>\n")

DEF_PTYPE("SdhPathAuTuList", "regexp", "au[3-4].*|tu[13].*",
        "List of paths\n"
        "- AU4-64c: au4_64c.<lineId>.<aug64Id>\n"
        "- AU4-16c: au4_16c.<lineId>.<aug16Id>\n"
        "- AU4-4c : au4_4c.<lineId>.<aug4Id>\n"
        "- AU4-16nc: au4_16nc.<lineId>.<aug16Id>\n"
        "- AU4-nc : au4_nc.<lineId>.<aug1Id>\n"
        "- AU4    : au4.<lineId>.<aug1Id>\n"
        "- AU3    : au3.<lineId>.<aug1Id>.<au3Id>\n"
        "- TU3    : tu3.<lineId>.<aug1Id>.<tug3Id>\n"
        "- TU1x   : tu1x.<lineId>.<aug1Id>.<au3Tug3Id>.<tug2Id>.<vcxId>\n")

DEF_PTYPE("eAtSdhPathOverheadByte", "select", "n1(0), c2(1), g1(2), f2(3), h4(4), f3(5), k3(6), v5(7), n2(8), k4(9)", "Overhead byte\n")

DEF_PTYPE("SdhPathTimer", "regexp", Uint32Value, "SDH path timer (ms).\n")

/* SET transmitted TTI */
DEF_SDKCOMMAND(cliSdhPathTxTti,
               "sdh path tti transmit",
               "SET with specified padding : sdh path tti transmit <pathList> <ttiMode> <ttiMessage> <padding>\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("ttiMode", "SdhTtiMsgMode", "")
               DEF_SDKPARAM("ttiMessage", "SdhTtiMsg", "Transmitted TTI message")
               DEF_SDKPARAM("padding", "SdhTtiMsgPaddingMode", ""),
               "${pathList} ${ttiMode} ${ttiMessage} ${padding}")
    {
    mAtCliCall();
    }

/* SET expected TTI */
DEF_SDKCOMMAND(cliSdhPathExptTti,
               "sdh path tti expect",
               "Set expected TTI message\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("ttiMode", "SdhTtiMsgMode", "")
               DEF_SDKPARAM("ttiMessage", "SdhTtiMsg", "Expected TTI message")
               DEF_SDKPARAM("padding", "SdhTtiMsgPaddingMode", ""),
               "${pathList} ${ttiMode} ${ttiMessage} ${padding}")
    {
    mAtCliCall();
    }

/* FIXME: Deprecated by "show sdh path tti", need to remove it after autotest
 * team change to use new CLI */
DEF_SDKCOMMAND(cliSdhPathRxTti,
               "show sdh path tti receive",
               "Show received TTI message\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathTtiGet,
               "show sdh path tti",
               "Show all of TTI information: transmitted, expected and received TTI\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

/* SET transmitted PSL */
DEF_SDKCOMMAND(cliSdhPathTxPsl,
               "sdh path psl transmit",
               "Transmit Path Signal Label\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("pslValue", "STRING", "Transmitted path signal label (PSL)"),
               "${pathList} ${pslValue}")
    {
    mAtCliCall();
    }

/* SET expected PSL */
DEF_SDKCOMMAND(cliSdhPathExptPsl,
               "sdh path psl expect",
               "Set expected Path Signal Label\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("pslValue", "STRING", "Expected path signal label (PSL)"),
               "${pathList} ${pslValue}")
    {
    mAtCliCall();
    }

/* SET/GET interrupt mask */
DEF_SDKCOMMAND(cliSdhPathIntrMsk,
               "sdh path interruptmask",
               "Set interrupt mask\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("mask", "SdhDefMask", "")
               DEF_SDKPARAM("intrMaskEn", "eBool", ""),
               "${pathList} ${mask} ${intrMaskEn}")
    {
    mAtCliCall();
    }

/* Read/clear counter */
DEF_SDKCOMMAND(cliSdhPathCounters,
               "show sdh path counters",
               "Show counters\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${pathList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

/* Show Path configuration */
DEF_SDKCOMMAND(cliSdhPathShowCfg,
               "show sdh path",
               "Show configuration\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

/* Show Path alarm */
DEF_SDKCOMMAND(cliSdhPathShowAlarm,
               "show sdh path alarm",
               "Show alarm\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathShowListenAlarm,
               "show sdh path listen alarm",
               "Show alarm\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM_OPTION("readingMode", "HistoryReadingMode", ""),
               "${pathList} ${readingMode}")
    {
    mAtCliCall();
    }

/* Show Path interrupt */
DEF_SDKCOMMAND(cliSdhPathShowInterrupt,
               "show sdh path interrupt",
               "Show interrupt\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${pathList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathForceAlarm,
               "sdh path force alarm",
               "Force Alarm\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("frcType", "SdhPathFrcAlarmType", "")
               DEF_SDKPARAM("direction", "eAtDirection", "")
               DEF_SDKPARAM("isFrc", "eBool", ""),
               "${pathList} ${frcType} ${direction} ${isFrc}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathTxSs,
               "sdh path ss transmit",
               "Change transmitted SS bit",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("ss", "UINT", "SS value"),
               "${pathList} ${ss}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathExpectedSs,
               "sdh path ss expect",
               "Enable/disable SS bit comparing with specific expected SS value",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("enable", "eBool", "Enable/disable SS bit comparing")
               DEF_SDKPARAM("ss", "UINT", "SS value"),
               "${pathList} ${enable} ${ss}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathAlarmEnable,
               "sdh path alarm affect enable",
               "Set path alarm affect enable\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("alarmType", "SdhDefMask", ""),
               "${pathList} ${alarmType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathAlarmDisable,
               "sdh path alarm affect disable",
               "Set path alarm affect disable\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("alarmType", "SdhDefMask", ""),
               "${pathList} ${alarmType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathAlarmGet,
               "show sdh path alarm affect",
               "Get path alarm affect\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }
    
DEF_SDKCOMMAND(cliSdhPathERdiEnable,
               "sdh path erdi enable",
               "Enable E-RDI\n",
               DEF_SDKPARAM("pathVcList", "SdhPathVcList", ""),
               "${pathVcList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathERdiDisable,
               "sdh path erdi disable",
               "Disable E-RDI\n",
               DEF_SDKPARAM("pathVcList", "SdhPathVcList", ""),
               "${pathVcList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathForceError,
               "sdh path force error",
               "Force error at path\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("errorType", "eAtSdhPathCounterType", ""),
               "${pathList} ${errorType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathTimMonitorEnable,
               "sdh path tti monitor enable",
               "Enable TIM monitoring",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathTimMonitorDisable,
               "sdh path tti monitor disable",
               "Disable TIM monitoring",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathTimingSet,
               "sdh path timing",
               "Set timing mode for SDH channels",
               DEF_SDKPARAM("pathVcList", "SdhPathVcList", "")
               DEF_SDKPARAM("timingMode", "eAtTimingMode", "")
               DEF_SDKPARAM("timeSrcId", "STRING", "Timing source"),
               "${pathVcList} ${timingMode} ${timeSrcId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathForcedAlarmGet,
               "show sdh path forcedalarms",
               "Show forced alarms\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathTxOverheadByteSet,
               "sdh path overhead transmit",
               "Set overhead value to be transmitted at TX direction",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("ohByte", "eAtSdhPathOverheadByte", "")
               DEF_SDKPARAM("ohByteValue", "STRING", ""),
               "${pathList} ${ohByte} ${ohByteValue}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathOverheadByteGet,
               "show sdh path overhead",
               "Get transmitted and received Path Overhead byte\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathLoopbackSet,
               "sdh path loopback",
               "Set Path loopback\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("loopbackMode", "eAtLoopbackMode", ""),
               "${pathList} ${loopbackMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathLoopbackGet,
               "show sdh path loopback",
               "Get Path loopback\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathDebug,
               "debug sdh path",
               "Show debug information",
               DEF_SDKPARAM("pathList", "MappableChannelList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathAutoRdiEnable,
               "sdh path autordi enable",
               "Enable RDI auto generating when critical defects happen\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM_OPTION("alarms", "SdhDefMask", ""),
               "${pathList} ${alarms}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathAutoRdiDisable,
               "sdh path autordi disable",
               "Disable RDI auto generating when critical defects happen\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM_OPTION("alarms", "SdhDefMask", ""),
               "${pathList} ${alarms}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathAlarmCapture,
               "sdh path alarm capture",
               "Enable/disable alarm capturing",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("enable", "eBool", "Enable/disable alarm capturing"),
               "${pathList} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathRxTrafficSwitch,
               "sdh path rxtraffic switch",
               "Switch the dropped traffic from one channel to other channel",
               DEF_SDKPARAM("srcPathList", "SdhPathList", "")
               DEF_SDKPARAM("destPathList", "SdhPathList", ""),
               "${srcPathList} ${destPathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathTxTrafficBridge,
               "sdh path txtraffic bridge",
               "Bridge the added traffic to other channel",
               DEF_SDKPARAM("srcPathList", "SdhPathList", "")
               DEF_SDKPARAM("destPathList", "SdhPathList", ""),
               "${srcPathList} ${destPathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathTxTrafficCut,
               "sdh path txtraffic cut",
               "Bridge the added traffic to other channel",
               DEF_SDKPARAM("srcPathList", "SdhPathList", ""),
               "${srcPathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathConcate,
               "sdh path concate",
               "Concate the master channel to all channels in slave list",
               DEF_SDKPARAM("master", "SdhPathList", "")
               DEF_SDKPARAM("slaveList", "SdhPathList", ""),
               "${master} ${slaveList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathDeconcate,
               "sdh path deconcate",
               "Deconcate the master channel",
               DEF_SDKPARAM("masters", "SdhPathList", ""),
               "${master}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathConcateGet,
               "show sdh path concate",
               "Show concate information",
               DEF_SDKPARAM("masters", "SdhPathList", ""),
               "${master}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathPlmMonitorEnable,
               "sdh path plm monitor enable",
               "Enable PLM monitoring",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathPlmMonitorDisable,
               "sdh path plm monitor disable",
               "Disable PLM monitoring",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathLogShow,
               "show sdh path log",
               "Show logged message of path",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathLogEnable,
               "sdh path log enable",
               "Enable logging for paths",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathLogDisable,
               "sdh path log disable",
               "Disable logging for paths",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }
    
DEF_SDKCOMMAND(cliAtSdhPathHoldOffTimer,
               "sdh path holdoff time",
               "Set path hold-off time\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("holdOff", "SdhPathTimer", ""),
               "${pathList} ${holdOff}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathHoldOnTimer,
               "sdh path holdon time",
               "Set path hold-on time\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("holdOn", "SdhPathTimer", ""),
               "${pathList} ${holdOn}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathCaptureShow,
               "show sdh path alarm capture",
               "Show captured alarms\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", "Read to clear without display table"),
               "${pathList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathTtiCompareEnable,
               "sdh path tti compare enable",
               "Enable TTI comparing",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathTtiCompareDisable,
               "sdh path tti compare disable",
               "Disable TTI comparing",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathSsInsertionEnable,
               "sdh path ss transmit enable",
               "Enable SS bit transmission from value configured by CPU",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathSsInsertionDisable,
               "sdh path ss transmit disable",
               "Disable SS bit transmission from value configured by CPU",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathMode,
               "sdh path mode",
               "Set path mode\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("mode", "eAtSdhChannelMode", ""),
               "${pathList} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtAtSdhPathAlarmAutoRdiGet,
               "show sdh path alarm autordi",
               "Show auto RDI alarms\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhVcStuffingEnable,
               "sdh vc stuffing enable",
               "Enable fixed stuff columns",
               DEF_SDKPARAM("pathList", "SdhPathVcList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhVcStuffingDisable,
               "sdh vc stuffing disable",
               "Disable fixed stuff columns",
               DEF_SDKPARAM("pathList", "SdhPathVcList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhVcShow,
               "show sdh vc",
               "Show VC configuration\n",
               DEF_SDKPARAM("pathList", "SdhPathVcList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }


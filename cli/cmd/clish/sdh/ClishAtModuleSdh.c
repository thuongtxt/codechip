/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : ClishAtModuleSdh.c
 *
 * Created Date: Jan 24, 2013
 *
 * Description : SDH module CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/* Top commands */
TOP_SDKCOMMAND("sdh interrupt", "SDH module interrupt")
TOP_SDKCOMMAND("sdh ber", "BER Module CLIs")
TOP_SDKCOMMAND("sdh ber debug", "BER debug CLIs")
TOP_SDKCOMMAND("debug sdh", "Set/Get loopback mode of all SDH lines")

DEF_SDKCOMMAND(cliSdhModuleOutClkSet,
               "sdh clockoutput",
               "Select what Line clock source to go to External PLL",
               DEF_SDKPARAM("refOutIds", "STRING", "List of clock IDs comes to external PLL")
               DEF_SDKPARAM("lines", "SdhLineList", ""),
               "${refOutIds} ${lines}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhModuleOutClkGet,
               "show sdh clockoutput",
               "Get clock source that is selected to go to external PLL",
               DEF_SDKPARAM("refOutIds", "STRING", "List of clock IDs comes to external PLL"),
               "${refOutIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhInterruptEnable,
               "sdh interrupt enable",
               "Enable interrupt",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhInterruptDisable,
               "sdh interrupt disable",
               "Disable interrupt",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhBerDebugEnable,
               "sdh ber debug enable",
               "Enable BER module debug mode",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhBerDebugDisable,
               "sdh ber debug disable",
               "Disable BER module debug mode",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhModuleDebugLoopback,
              "debug sdh loopback",
              "Loopack SDH module",
              DEF_SDKPARAM("loopbackMode", "eAtLoopbackMode", ""),
              "${loopbackMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliModuleSdhDebug,
               "debug module sdh",
               "Show debug information of SDH module\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliModuleSdhShow,
               "show module sdh",
               "Show SDH module information",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

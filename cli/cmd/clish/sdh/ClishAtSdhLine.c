/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2011 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SONET/SDH
 *
 * File        : ClishAtShhLine.c
 *
 * Created Date: Nov 14, 2011
 *
 * Description : SONET/SDH Line CLI implementation
 *
 * Notes       :
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "../AtClish.h"
#include "../physical/ClishAtEyeScan.h"
#include "../../physical/CliAtEyeScan.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
/* Define regular expression for list of Line IDs
 *
 * BNF format:
 * line           ::= [1-8]
 * lineLstElement ::= ((line[-]line)|line)
 * lineLst        ::= ((lineLst[,]lineLstElement)|lineLstElement)
 *
 * Convert to regular expression:
 * lineLst: (lineLstElement[,])+lineLstElement|lineLstElement
 */
#define lineLstElement "(("regLineno"[-]"regLineno")|"regLineno")"
#define lineLst "(("lineLstElement"[,])+"lineLstElement"|"lineLstElement")"

/*
 * Define defect masks regular expression
 * DefMask ::= ((DefMask[,]DefType)|DefType)
 * DefType ::= (los|oof|lof|tim|ais|rdi|ber-sd|ber-sf)
 */
#define DefType "(los|oof|lof|tim|ais|rdi|ber-sd|ber-sf|ber-tca|kbytechange|s1change|rs-sd|rs-sf|k1change|k2change)"
#define DefMask "((("DefType"[\\|])+"DefType")|"DefType")"

#define DccLayerType "(section|line|all)"
#define DccLayerMask "((("DccLayerType"[\\|])+"DccLayerType")|"DccLayerType")"

/*
 * Define counter list regular expression
 * CounterList  ::= (CounterList[,]CounterType|CounterType)
 * CounterType ::= (b1|b2|rei)
 */
#define CounterType "(b1|b2|rei)"
#define CounterList  "(("CounterType"[,])+"CounterType"|"CounterType")"

DEF_PTYPE("DccId", "regexp", ".*",
          "List of DCC\n"
          "- DCC line and section  : <lineIds>.all\n"
          "- DCC section           : <lineIds>.section\n"
          "- DCC line              : <lineIds>.line\n"
          "- DCC line|section      : <lineIds>.line|section\n"
          "- DCC line, DCC section : <lineIds>.line,<lineIds>.section\n")

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implement Functions ----------------------------*/

/*------------------- Commands Implementation --------------------------------*/
/* Top commands */
TOP_SDKCOMMAND("sdh", "SDH CLIs")
TOP_SDKCOMMAND("sdh line", "SDH Line CLIs ")
TOP_SDKCOMMAND("sdh line tti", "SDH Line TTI CLIs")
TOP_SDKCOMMAND("sdh line force", "SDH Force Line Alarm CLIs")
TOP_SDKCOMMAND("sdh line threshold", "SDH Line thresholds CLIs")
TOP_SDKCOMMAND("sdh line alarm", "SDH Line alarm CLIS")
TOP_SDKCOMMAND("sdh line alarm affect", "SDH Line alarm affect CLIS")
TOP_SDKCOMMAND("show sdh", "Show SDH information")
TOP_SDKCOMMAND("sdh line tti monitor", "TTI monitoring CLIs")
TOP_SDKCOMMAND("sdh line tti compare", "TTI comparing CLIs")
TOP_SDKCOMMAND("sdh line serdes", "SERDES configuration\n")
TOP_SDKCOMMAND("sdh line serdes equalizer", "SERDES equalizer configuration\n")
TOP_SDKCOMMAND("debug sdh", "Debug SDH")
TOP_SDKCOMMAND("sdh line scramble", "Line scramble CLIs\n")
TOP_SDKCOMMAND("sdh line autordi", "SDH Line auto RDI configuration\n")
TOP_SDKCOMMAND("sdh line serdes drp", "SDH line SERDES eyescan DRP CLIs\n")
TOP_SDKCOMMAND("sdh line serdes eyescan stepsize", "SDH line SERDES eye scan Step size\n")
TOP_SDKCOMMAND("sdh line serdes eyescan debug", "SDH Line SERDES eye scan debug\n")
TOP_SDKCOMMAND("sdh line log", "SDH Line logging CLIs\n")
TOP_SDKCOMMAND("show sdh line listen", "SDH Line Listener CLIs\n")
TOP_SDKCOMMAND("sdh line dcc", "SDH Line DCC CLIs")
TOP_SDKCOMMAND("sdh line dcc tx", "SDH Line TX DCC CLIs")
TOP_SDKCOMMAND("sdh line dcc rx", "SDH Line RX DCC CLIs")
TOP_SDKCOMMAND("sdh line dcc pw", "SDH Line DCC PW CLIs")
TOP_SDKCOMMAND("sdh line dcc hdlc", "SDH Line DCC HDLC CLIs")
TOP_SDKCOMMAND("sdh line dcc hdlc scramble", "CLIs to control scrambling")
TOP_SDKCOMMAND("sdh line dcc hdlc alarm", "SDH Line DCC HDLC alarm CLIs")
TOP_SDKCOMMAND("show sdh line dcc", "CLIs to display SDH Line DCC")
TOP_SDKCOMMAND("sdh line overhead", "CLIs to control overhead byte\n")
TOP_SDKCOMMAND("debug sdh line", "SDH Line CLIs")
TOP_SDKCOMMAND("debug sdh line dcc", "SDH Line DCC debug CLIs")
TOP_SDKCOMMAND("sdh line autorei", "SDH Line auto RDI configuration\n")

/* Other enumerations */
DEF_PTYPE("eAtSdhLineRate", "select", "stm0(1), stm1(2), stm4(3), stm16(4), stm64(5)", "Line rate")
DEF_PTYPE("eAtSdhDccLayer", "select", "section(1), line(2), all(3)", "DCC Layer Mask")
DEF_PTYPE("SdhLineDccLayerMask", "regexp", DccLayerMask,
          "\nLine DCC Layer masks:\n"
          "- section    : Section DCC on D1 to D3\n"
          "- line       : Line DCC on D4 to D12\n"
          "- all        : DCC on D1 to D12\n")

DEF_PTYPE("eAtSdhChannelMode", "select", "sdh(0), sonet(1)", "Channel mode")
DEF_PTYPE("eAtSdhLineCounterType", "select", "b1(0), b2(1), rei(2), none(3)", "Line error counter type")

DEF_PTYPE("SdhLineDef", "select", "los(1), oof(2), lof(4), tim(8), ais(16), rdi(32), ber-sd(64), ber-sf(128)", "Line defects")
DEF_PTYPE("SdhLineDefMask", "regexp", DefMask,
          "\nLine defect masks:\n"
          "- los        : LOS\n"
          "- oof        : OOF\n"
          "- lof        : LOF\n"
          "- tim        : RS-TIM\n"
          "- ais        : MS-AIS\n"
          "- rdi        : MS-RDI\n"
          "- ber-sd     : MS BER-SD\n"
          "- ber-sf     : MS BER-SF\n"
          "- ber-tca    : MS BER-TCA\n"
          "- k1change   : K1-Change\n"
          "- k2change   : K2-Change\n"
          "- s1change   : S1-Change\n"
          "- rs-sd      : RS BER-SD\n"
          "- rs-sf      : RS BER-SF\n")

/* TTI message mode */
DEF_PTYPE("SdhTtiMsgMode", "select",
          "1byte(0), 16bytes(1), 64bytes(2)",
          "\nTti message mode\n")

DEF_PTYPE("SdhTtiMsg", "regexp", ".{1,64}", "TTI message")

DEF_PTYPE("SdhTtiMsgPaddingMode", "select", "null_padding(0), space_padding(1)",
                  "\nTTI message padding modes:\n"
                  "- null_padding : padding character is NULL(0)\n"
                  "- space_padding: padding character is space\n")

/* Alarm Force type for SDH line */
DEF_PTYPE("SdhLineFrcAlarmType", "select",
          "los(0), oof(1), lof(2), tim(3), ais(4), rdi(5), ber-sd(6), ber-sf(7), ber-tca(8)",
          "\nLine alarm defect masks:\n"
          "- los        : LOS\n"
          "- oof        : OOF\n"
          "- lof        : LOF\n"
          "- tim        : RS-TIM\n"
          "- ais        : MS-AIS\n"
          "- rdi        : MS-RDI\n"
          "- ber-sd     : BER-SD\n"
          "- ber-sf     : BER-SF\n"
          "- ber-tca    : BER-TCA\n")

DEF_PTYPE("eAtSerdesParam", "regexp", ".*",
          "SERDES physical parameter:\n"
          "+ vod                  : VOD\n"
          "+ pre-tap              : Pre-emphassis pre-tap\n"
          "+ first-post-tap       : Pre-emphassis first-post-tap\n"
          "+ second-post-tap      : Pre-emphassis second-post-tap\n"
          "+ rxEqualizationDcGain : Rx Equalization DC gain\n"
          "+ rxEqualizationControl: Rx Equalization control\n"
          "+ tx-pre-cursor        : Tx Pre-emphassis cursor\n"
          "+ tx-post-cursor       : Tx Post-emphassis cursor\n"
          "+ tx-diff-ctrl         : Tx differential drive control\n"
          "+ RXLPMHFOVRDEN\n"
          "+ RXLPMLFKLOVRDEN\n"
          "+ RXOSOVRDEN\n"
          "+ RXLPMOSHOLD\n"
          "+ RXLPMOSOVRDEN\n"
          "+ RXLPM_OS_CFG1\n"
          "+ RXLPMGCHOLD\n"
          "+ RXLPMGCOVRDEN\n"
          "+ RXLPM_GC_CFG\n"
          "+ all                  : Show all parameter\n"
          "+ Other ID value\n")

DEF_PTYPE("SdhLineEyeScanLanes", "regexp", ".*", "List of eye scan lanes, format <lineId>.<laneId>")

DEF_PTYPE("eAtSdhLineOverheadByte", "select", "a1(0), a2(1), z0(2), b1(3), e1(4), f1(5), d1(6), d2(7), d3(8), h1(9), h2(10), h3(11), rs-undefined(12), "
                                              "b2(13), k1(14), k2(15), d4(16), d5(17), d6(18), d7(19), d8(20), d9(21), d10(22), d11(23), d12(24), s1(25), "
                                              "z1(26), z2(27), m0(28), m1(29), e2(30), ms-undefined(31)", "Overhead byte\n")

static void EnableEyeScanTask(void)
    {
    ClishEnableEyeScanTask();
    }

/*
 * Set line rate
 */
DEF_SDKCOMMAND(cliSdhLineRate,
               "sdh line rate",
               "Set line rate.\n"
               "Usages: sdh line rate <lines> <rate>\n",
               DEF_SDKPARAM("lines", "SdhLineList", "")
               DEF_SDKPARAM("rate", "eAtSdhLineRate", ""),
               "${lines} ${rate}")
    {
    mAtCliCall();
    }

/*
 * Set line mode
 */
DEF_SDKCOMMAND(cliSdhLineMode,
               "sdh line mode",
               "Set line mode\n",
               DEF_SDKPARAM("lines", "SdhLineList", "")
               DEF_SDKPARAM("mode", "eAtSdhChannelMode", ""),
               "${lines} ${mode}")
    {
    mAtCliCall();
    }

/*
 * Set TX TTI
 */
DEF_SDKCOMMAND(cliSdhLineRsTxTti,
               "sdh line tti transmit",
               "Set transmitted TTI message\n",
               DEF_SDKPARAM("lines", "SdhLineList", "")
               DEF_SDKPARAM("ttiMode", "SdhTtiMsgMode", "")
               DEF_SDKPARAM("ttiMessage", "SdhTtiMsg", "Transmitted TTI message")
               DEF_SDKPARAM("padding", "SdhTtiMsgPaddingMode", ""),
               "${lines} ${ttiMode} ${ttiMessage} ${padding}")
    {
    mAtCliCall();
    }

/*
 * Set expected TTI
 */
DEF_SDKCOMMAND(cliSdhLineRsExptTti,
               "sdh line tti expect",
               "Set expected TTI message\n",
               DEF_SDKPARAM("lines", "SdhLineList", "")
               DEF_SDKPARAM("ttiMode", "SdhTtiMsgMode", "")
               DEF_SDKPARAM("ttiMessage", "SdhTtiMsg", "Expected TTI message")
               DEF_SDKPARAM("padding", "SdhTtiMsgPaddingMode", ""),
               "${lines} ${ttiMode} ${ttiMessage} ${padding}")
    {
    mAtCliCall();
    }

/*
 * FIXME: Deprecated by "show sdh line tti". Need to remove it after autotest
 * team switch to use new CLI
 */
DEF_SDKCOMMAND(cliSdhLineRsRxTti,
               "show sdh line tti receive",
               "Get received TTI message\n",
               DEF_SDKPARAM("lines", "SdhLineList", ""),
               "${lines}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineTtiGet,
               "show sdh line tti",
               "Show all of TTI information: transmitted, expected and received TTI\n",
               DEF_SDKPARAM("lines", "SdhLineList", ""),
               "${lines}")
    {
    mAtCliCall();
    }

/*
 * Set transmit K1
 */
DEF_SDKCOMMAND(cliSdhLineTxK1,
               "sdh line k1",
               "Set transmitted K1\n",
               DEF_SDKPARAM("lines", "SdhLineList", "")
               DEF_SDKPARAM("value", "STRING", "Value of transmitted K1"),
               "${lines} ${value}")
    {
    mAtCliCall();
    }

/*
 * Set transmit K2
 */
DEF_SDKCOMMAND(cliSdhLineTxK2,
               "sdh line k2",
               "Set transmitted K2\n",
               DEF_SDKPARAM("lines", "SdhLineList", "")
               DEF_SDKPARAM("value", "STRING", "Value of transmitted K2"),
               "${lines} ${value}")
    {
    mAtCliCall();
    }

/*
 * Set transmit S1
 */
DEF_SDKCOMMAND(cliSdhLineTxS1,
               "sdh line s1",
               "Set transmitted S1\n",
               DEF_SDKPARAM("lines", "SdhLineList", "")
               DEF_SDKPARAM("value", "STRING", "Value of transmitted S1"),
               "${lines} ${value}")
    {
    mAtCliCall();
    }

/*
 * Set interrupt mask
 */
DEF_SDKCOMMAND(cliSdhLineIntrMsk,
               "sdh line interruptmask",
               "Set interrupt masks\n",
               DEF_SDKPARAM("lines", "SdhLineList", "")
               DEF_SDKPARAM("mask", "SdhLineDefMask", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${lines} ${mask} ${enable}")
    {
    mAtCliCall();
    }

/*
 * Set Loopback
 */
DEF_SDKCOMMAND(cliSdhLineLoopbackSet,
               "sdh line loopback",
               "Set Line loopback\n",
               DEF_SDKPARAM("lines", "SdhLineList", "")
               DEF_SDKPARAM("loopbackMode", "eAtLoopbackMode", ""),
               "${lines} ${loopbackMode}")
    {
    mAtCliCall();
    }

/*
 * Get Loopback
 */
DEF_SDKCOMMAND(cliSdhLineLoopbackGet,
               "show sdh line loopback",
               "Get Line loopback\n",
               DEF_SDKPARAM("lines", "SdhLineList", ""),
               "${lines}")
    {
    mAtCliCall();
    }

/*
 * force alarm
 */
DEF_SDKCOMMAND(cliSdhLineForceAlarm,
               "sdh line force alarm",
               "Force Alarm\n",
               DEF_SDKPARAM("lines", "SdhLineList", "")
               DEF_SDKPARAM("frcType", "SdhLineFrcAlarmType", "")
               DEF_SDKPARAM("direction", "eAtDirection", "")
               DEF_SDKPARAM("isFrc", "eBool", ""),
               "${lines} ${frcType} ${direction} ${isFrc}")
    {
    mAtCliCall();
    }

/*
 * Read/clear counter
 */
DEF_SDKCOMMAND(cliSdhLineCnt,
               "show sdh line counters",
               "Read/Clear error counters\n",
               DEF_SDKPARAM("lines", "SdhLineList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${lines} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

/*
 * Show Line configuration
 */
DEF_SDKCOMMAND(cliSdhLineShowCfg,
               "show sdh line",
               "Show Line configuration\n",
               DEF_SDKPARAM("lines", "SdhLineList", ""),
               "${lines}")
    {
    mAtCliCall();
    }

/*
 * Show Line status
 */
DEF_SDKCOMMAND(cliSdhLineShowAlarm,
               "show sdh line alarm",
               "Show Line alarm\n",
               DEF_SDKPARAM("lines", "SdhLineList", ""),
               "${lines}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineShowListenAlarm,
               "show sdh line listen alarm",
               "Show Line alarm\n",
               DEF_SDKPARAM("lines", "SdhLineList", "")
               DEF_SDKPARAM_OPTION("readingMode", "HistoryReadingMode", ""),
               "${lines} ${readingMode}")
    {
    mAtCliCall();
    }

/*
 * Show Line status
 */
DEF_SDKCOMMAND(cliSdhLineShowInterrupt,
               "show sdh line interrupt",
               "Show Line interrupt\n",
               DEF_SDKPARAM("lines", "SdhLineList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", "Read to clear without display table"),
               "${lines} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineLedStateSet,
               "sdh line ledstate",
               "Set Led State\n",
               DEF_SDKPARAM("lines", "SdhLineList", "")
               DEF_SDKPARAM("ledState", "eAtLedState", ""),
               "${lines} ${ledState}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineAlarmEnable,
               "sdh line alarm affect enable",
               "Set line alarm affect enable\n",
               DEF_SDKPARAM("lines", "SdhLineList", "")
               DEF_SDKPARAM("alarmType", "SdhLineFrcAlarmType", ""),
               "${lines} ${alarmType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineAlarmDisable,
               "sdh line alarm affect disable",
               "Set line alarm affect disable\n",
               DEF_SDKPARAM("lines", "SdhLineList", "")
               DEF_SDKPARAM("alarmType", "SdhLineFrcAlarmType", ""),
               "${lines} ${alarmType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineAlarmAffectGet,
               "show sdh line alarm affect",
               "Get line alarm affect\n",
               DEF_SDKPARAM("lines", "SdhLineList", ""),
               "${lines}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineForceError,
               "sdh line force error",
               "Force error at line\n",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("errorType", "eAtSdhLineCounterType", "Line error counter type"),
               "${lineList} ${errorType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineSerdesTimingModeSet,
               "sdh line serdes timingmode",
               "Set serdes timing mode\n",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("timingMode", "eAtSerdesTimingMode", "Serdes timing mode"),
               "${lineList} ${timingMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineSerdesTimingModeGet,
               "show sdh line serdes timingmode",
               "Show serdes timing mode\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineTimingSet,
               "sdh line timing",
               "Set timing mode for SDH lines",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("timingMode", "eAtTimingMode", ""),
               "${lineList} ${timingMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDebug,
               "debug sdh line",
               "Show all of debugging information",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineScrambleEnable,
               "sdh line scramble enable",
               "Enable scramble",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineScrambleDisable,
               "sdh line scramble disable",
               "Disable scramble",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineTimMonitorEnable,
               "sdh line tti monitor enable",
               "Enable TIM monitoring",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineTimMonitorDisable,
               "sdh line tti monitor disable",
               "Disable TIM monitoring",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineEnable,
               "sdh line enable",
               "Enable lines",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDisable,
               "sdh line disable",
               "Disable lines",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineAlarmCapture,
               "sdh line alarm capture",
               "Enable/disable alarm capturing",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("enable", "eBool", "Enable/disable alarm capturing"),
               "${lineList} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineSerdesControllerInit,
               "sdh line serdes init",
               "Initialize SERDES",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineSerdesControllerEnable,
               "sdh line serdes enable",
               "Enable SERDES",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineSerdesControllerDisable,
               "sdh line serdes disable",
               "Disable SERDES",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineSerdesPhyParamSet,
               "sdh line serdes param",
               "Change SERDES physical parameter",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("paramId", "eAtSerdesParam", "")
               DEF_SDKPARAM("value", "STRING", "Value"),
               "${lineList} ${paramId} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineSerdesShow,
               "show sdh line serdes",
               "Show SERDES information",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineSerdesAlarmShow,
               "show sdh line serdes alarm",
               "Show SERDES alarm",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineSerdesAlarmHistoryShow,
               "show sdh line serdes alarm history",
               "Show SERDES history alarm changed.",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", ""),
               "${lineList} ${readingMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineSerdesParamShow,
               "show sdh line serdes param",
               "Show value of specified SERDES parameter",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM_OPTION("paramId", "eAtSerdesParam", ""),
               "${lineList} ${paramId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineSerdesDebug,
               "debug sdh line serdes",
               "Show SERDES debug information",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineSerdesLoopback,
               "sdh line serdes loopback",
               "Loopback SERDES",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("loopbackMode", "eAtLoopbackMode", ""),
               "${lineList} ${loopbackMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineSwitch,
               "sdh line switch",
               "Switch traffic between two lines",
               DEF_SDKPARAM("fromLineId", "UINT", "")
               DEF_SDKPARAM("toLineId", "UINT", ""),
               "${fromLineId} ${toLineId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineBridge,
               "sdh line bridge",
               "Bridge traffic between two lines",
               DEF_SDKPARAM("fromLineId", "UINT", "")
               DEF_SDKPARAM("toLineId", "UINT", ""),
               "${fromLineId} ${toLineId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineSwitchRelease,
               "sdh line switch release",
               "Release switching traffic between two lines",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineBridgeRelease,
               "sdh line bridge release",
               "Release bridging traffic between two lines",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineApsGet,
               "show sdh line aps",
               "Show APS configuration for lines",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineForcedAlarmGet,
               "show sdh line forcedalarms",
               "Show forced alarms",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineTxEnable,
               "sdh line enable tx",
               "Enable TX",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineTxDisable,
               "sdh line disable tx",
               "Disable TX",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineRxEnable,
               "sdh line enable rx",
               "Enable RX",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineRxDisable,
               "sdh line disable rx",
               "Disable RX",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineAutoRdiEnable,
               "sdh line autordi enable",
               "Enable RDI auto generating when critical defects happen\n",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM_OPTION("alarms", "SdhLineDefMask", ""),
               "${lineList} ${alarms}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineAutoRdiDisable,
               "sdh line autordi disable",
               "Disable RDI auto generating when critical defects happen\n",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM_OPTION("alarms", "SdhLineDefMask", ""),
               "${lineList} ${alarms}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineSerdesEyeScan,
               "sdh line serdes eyescan",
               "Eye scan",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    AtList controllers;
    char *idStr = AtStrdup(lub_argv__get_arg(argv, 0));
    AtUnused(context);
    AtUnused(argv);

    /* Register listener */
    controllers = CliAtSdhLineSerdesEyeScanControllersFromIdString(idStr);
    CliAtEyeScanControllerDefaultListenerRegister(controllers);
    AtObjectDelete((AtObject)controllers);

    /* Enable task */
    EnableEyeScanTask();

    return 0;
    }

DEF_SDKCOMMAND(cliAtSdhLineSerdesEyeScanLaneDrpRead,
               "sdh line serdes drp rd",
               "Read DRP register",
               DEF_SDKPARAM("line.laneId", "STRING", "Lane ID")
               DEF_SDKPARAM("drpAddress", "RegisterAddress", "")
               DEF_SDKPARAM_OPTION("pretty", "RegisterDisplayFormat", ""),
               "${line.laneId} ${drpAddress} ${pretty}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineSerdesEyeScanLaneDrpWrite,
               "sdh line serdes drp wr",
               "Write DRP register",
               DEF_SDKPARAM("line.laneId", "STRING", "")
               DEF_SDKPARAM("drpAddress", "RegisterAddress", "")
               DEF_SDKPARAM("value", "RegisterValue", ""),
               "${line.laneId} ${laneId} ${drpAddress} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineSerdesEyeScanLaneStepSizeHorizonalSet,
               "sdh line serdes eyescan stepsize horizonal",
               "Change horizontal step",
               DEF_SDKPARAM("lanes", "SdhLineEyeScanLanes", "")
               DEF_SDKPARAM("stepSize", "UINT", "Horizontal step size"),
               "${lanes} ${stepSize}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineSerdesEyeScanLaneStepSizeVerticalSet,
               "sdh line serdes eyescan stepsize vertical",
               "Change vertical step",
               DEF_SDKPARAM("lanes", "SdhLineEyeScanLanes", "")
               DEF_SDKPARAM("stepSize", "UINT", "Vertical step size"),
               "${lanes} ${stepSize}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineSerdesEyeScanLaneMaxPrescaleSet,
               "sdh line serdes eyescan maxPrescale",
               "Change max Prescale",
               DEF_SDKPARAM("lanes", "SdhLineEyeScanLanes", "")
               DEF_SDKPARAM("maxPrescale", "UINT", "Max prescale"),
               "${lanes} ${maxPrescale}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineSerdesEyeScanLaneDatawidthSet,
               "sdh line serdes eyescan datawidth",
               "Change data width",
               DEF_SDKPARAM("lanes", "SdhLineEyeScanLanes", "")
               DEF_SDKPARAM("dataWidth", "UINT", "Data width"),
               "${lanes} ${dataWidth}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineSerdesEyeScanLaneShow,
               "show sdh line serdes eyescan",
               "Show Eye scanning information",
               DEF_SDKPARAM("lanes", "SdhLineEyeScanLanes", ""),
               "${lanes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineSerdesEyeScanLaneEnable,
               "sdh line serdes eyescan enable",
               "Enable Eye scanning",
               DEF_SDKPARAM("lanes", "SdhLineEyeScanLanes", ""),
               "${lanes}")
    {
    int result = mAtCliResult();
    AtUnused(argv);
    EnableEyeScanTask();
    return result;
    }

DEF_SDKCOMMAND(cliAtSdhLineSerdesEyeScanLaneDisable,
               "sdh line serdes eyescan disable",
               "Disable Eye scanning",
               DEF_SDKPARAM("lanes", "SdhLineEyeScanLanes", ""),
               "${lanes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineSerdesEyeScanControllerEqualizerSet,
               "sdh line serdes eyescan equalizer",
               "Change Equalizer",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("mode", "eAtEyeScanEqualizerMode", "Equalizer mode"),
               "${lineList} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineSerdesEyeScanLaneDebugEnable,
               "sdh line serdes eyescan debug enable",
               "Enable eye-scan internal state machine debug",
               DEF_SDKPARAM("lanes", "SdhLineEyeScanLanes", ""),
               "${lanes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineSerdesEyeScanLaneDebugDisable,
               "sdh line serdes eyescan debug disable",
               "Disable eye-scan internal state machine debug",
               DEF_SDKPARAM("lanes", "SdhLineEyeScanLanes", ""),
               "${lanes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineSerdesEyeScanLaneDebug,
               "debug sdh line serdes eyescan lane",
               "Show lane debug information",
               DEF_SDKPARAM("lanes", "SdhLineEyeScanLanes", ""),
               "${lanes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineSerdesEyeScanControllerDebug,
               "debug sdh line serdes eyescan",
               "Show eye-scan debug information",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineLogShow,
               "show sdh line log",
               "Show logged message of line",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineLogEnable,
               "sdh line log enable",
               "Enable logging for lines",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineLogDisable,
               "sdh line log disable",
               "Disable logging for lines",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineSerdesControllerReset,
               "sdh line serdes reset",
               "Reset SERDES",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineCaptureShow,
               "show sdh line alarm capture",
               "Show captured alarms\n",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", "Read to clear without display table"),
               "${lineList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtSdhLineSerdesControllerEqualizerModeSet,
               "sdh line serdes equalizer mode",
               "Set SERDES equalizer mode",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("mode", "eAtSerdesEqualizerMode", ""),
               "${lineList} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineTtiCompareEnable,
               "sdh line tti compare enable",
               "Enable TTI comparing",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineTtiCompareDisable,
               "sdh line tti compare disable",
               "Disable TTI comparing",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccHdlcFlag,
               "sdh line dcc hdlc flag",
               "Set FLag",
               DEF_SDKPARAM("dcc", "DccId", "")
               DEF_SDKPARAM("flagValue", "BYTE_HEX", "flag value")
               DEF_SDKPARAM_OPTION("numFlag", "UINT", "Number of Flag"),
               "${dcc} ${flagValue} ${numFlag}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccHdlcIdlePattern,
               "sdh line dcc hdlc idlepattern",
               "Set Idle pattern",
               DEF_SDKPARAM("dcc", "DccId", "")
               DEF_SDKPARAM("idleValue", "BYTE_HEX", "idle value in hex"),
               "${dcc} ${idleValue}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccHdlcMtu,
               "sdh line dcc hdlc mtu",
               "Set MTU",
               DEF_SDKPARAM("dcc", "DccId", "")
               DEF_SDKPARAM("mtu", "MtuVal", ""),
               "${dcc} ${mtu}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccHdlcFcs,
               "sdh line dcc hdlc fcs",
               "Set FCS mode",
               DEF_SDKPARAM("dcc", "DccId", "")
               DEF_SDKPARAM("fcsMode", "eAtHdlcFcsMode", ""),
               "${dcc} ${fcsMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineDccHdlcFcsBitOrderSet,
               "sdh line dcc hdlc fcs bitorder",
               "Set FCS bit ordering",
               DEF_SDKPARAM("dcc", "DccId", "")
               DEF_SDKPARAM("mode", "eAtHdlcFcsCalculationMode", "Bit ordering"),
               "${dcc} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccHdlcShow,
               "show sdh line dcc hdlc",
               "Show DCC HDLC Info",
               DEF_SDKPARAM("dcc", "DccId", ""),
               "${dcc}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccHdlcCountersShow,
               "show sdh line dcc hdlc counters",
               "Show DCC HDLC Counters",
               DEF_SDKPARAM("dcc", "DccId", "")
               DEF_SDKPARAM("counterReadMode", "HistoryReadingMode", "read only/read to clear mode")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${dcc} ${counterReadMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccCreate,
               "sdh line dcc create",
               "Create DCC",
               DEF_SDKPARAM("lines", "SdhLineList", "")
               DEF_SDKPARAM_OPTION("layerMask", "SdhLineDccLayerMask", " section or line or section|line"),
               "${dcc} ${layerMask}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccDelete,
               "sdh line dcc delete",
               "Delete DCC",
               DEF_SDKPARAM("dcc", "DccId", ""),
               "${dcc}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccIntrMsk,
               "sdh line dcc hdlc interruptmask",
               "Set interrupt masks\n",
               DEF_SDKPARAM("dcc", "DccId", "")
               DEF_SDKPARAM("mask", "HdlcAlarmTypeMask", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${lines} ${mask} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccHdlcInterruptsShow,
               "show sdh line dcc hdlc interrupt",
               "Show DCC HDLC interrupts Sticky",
               DEF_SDKPARAM("dcc", "DccId", "")
               DEF_SDKPARAM("counterReadMode", "HistoryReadingMode", "read only/read to clear mode")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${dcc} ${counterReadMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccHdlcAlarmShow,
               "show sdh line dcc hdlc alarm",
               "Show DCC HDLC alarms",
               DEF_SDKPARAM("dcc", "DccId", ""),
               "${dcc}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccHdlcAlarmCapture,
               "sdh line dcc hdlc alarm capture",
               "Enable/disable DCC/HDLC alarm capturing",
               DEF_SDKPARAM("dcc", "DccId", "")
               DEF_SDKPARAM("enable", "eBool", "Enable/disable alarm capturing"),
               "${dcc} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccInit,
               "sdh line dcc init",
               "Init DCC",
               DEF_SDKPARAM("dcc", "DccId", ""),
               "${dcc}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccEnable,
               "sdh line dcc enable",
               "Enable DCC",
               DEF_SDKPARAM("dcc", "DccId", ""),
               "${dcc}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccDisable,
               "sdh line dcc disable",
               "Disable DCC",
               DEF_SDKPARAM("dcc", "DccId", ""),
               "${dcc}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccTxEnable,
               "sdh line dcc tx enable",
               "Enable Tx DCC",
               DEF_SDKPARAM("dcc", "DccId", ""),
               "${dcc}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccTxDisable,
               "sdh line dcc tx disable",
               "Disable Tx DCC",
               DEF_SDKPARAM("dcc", "DccId", ""),
               "${dcc}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccRxEnable,
               "sdh line dcc rx enable",
               "Enable Rx DCC",
               DEF_SDKPARAM("dcc", "DccId", ""),
               "${dcc}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccRxDisable,
               "sdh line dcc rx disable",
               "Disable Rx DCC",
               DEF_SDKPARAM("dcc", "DccId", ""),
               "${dcc}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineDccScrambleEnable,
               "sdh line dcc hdlc scramble enable",
               "Enable scramble",
               DEF_SDKPARAM("dcc", "DccId", ""),
               "${dcc}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineDccScrambleDisable,
               "sdh line dcc hdlc scramble disable",
               "Disable scramble",
               DEF_SDKPARAM("dcc", "DccId", ""),
               "${dcc}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineDccStuffModeSet,
               "sdh line dcc hdlc stuffing",
               "Set stuffing mode",
               DEF_SDKPARAM("dcc", "DccId", "")
               DEF_SDKPARAM("stuffingMode", "eAtHdlcStuffMode", ""),
               "${dcc} ${stuffingMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineTxOverheadByteSet,
               "sdh line overhead transmit",
               "Set overhead value to be transmitted at TX direction",
               DEF_SDKPARAM("lines", "SdhLineList", "")
               DEF_SDKPARAM("ohByte", "eAtSdhLineOverheadByte", "")
               DEF_SDKPARAM("ohByteValue", "STRING", ""),
               "${lines} ${ohByte} ${ohByteValue}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineOverheadByteGet,
               "show sdh line overhead",
               "Get transmitted and received Line Overhead byte\n",
               DEF_SDKPARAM("lines", "SdhLineList", "")
               DEF_SDKPARAM("sts3Number", "UINT", "STS3 number to show with each line rate as below:\n"
                                                  "- STM-0  : Any values. \n"
                                                  "- STM-1  : STS3 number in range [1..1]. \n"
                                                  "- STM-4  : STS3 number in range [1..4]. \n"
                                                  "- STM-16 : STS3 number in range [1..16]. \n"
                                                  "- STM-64 : STS3 number in range [1..64]. \n")
               DEF_SDKPARAM_OPTION("direction", "eAtDirection", ""),
               "${lines} ${sts3Number} ${direction}")
	{
	mAtCliCall();
	}

DEF_SDKCOMMAND(cliAtSdhLineDccHdlcHierarchy,
               "show sdh line dcc hierarchy",
               "Show sdh line dcc hierarchy",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
	{
	mAtCliCall();
	}

DEF_SDKCOMMAND(cliAtSdhLineAlarmAutoRdiGet,
               "show sdh line alarm autordi",
               "Show auto RDI alarms\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
	{
	mAtCliCall();
	}

DEF_SDKCOMMAND(cliAtSdhLineDccHdlcDebugShow,
               "debug sdh line dcc hdlc",
               "Show DCC HDLC Debug Info",
               DEF_SDKPARAM("dcc", "DccId", ""),
               "${dcc}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineAutoReiEnable,
               "sdh line autorei enable",
               "Enable rei auto generating when BIP errors are detected\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineAutoReiDisable,
               "sdh line autorei disable",
               "Disable rei auto generating when BIP errors are detected\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

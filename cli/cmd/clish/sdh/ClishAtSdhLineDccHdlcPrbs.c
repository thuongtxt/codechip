/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : ClishAtSdhLineDccHdlcPrbs.c
 *
 * Created Date: Nov 6, 2016
 *
 * Description : DCC HDLC PRBS
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/* Top commands */
TOP_SDKCOMMAND("sdh line dcc hdlc prbs", "SDH Line DCC HDLC PRBS CLIs")
TOP_SDKCOMMAND("sdh line dcc hdlc prbs tx", "SDH Line DCC HDLC PRBS TX CLIs")
TOP_SDKCOMMAND("sdh line dcc hdlc prbs rx", "SDH Line DCC HDLC PRBS RX CLIs")

DEF_SDKCOMMAND(cliAtSdhLineDccHdlcPrbsEnable,
               "sdh line dcc hdlc prbs enable",
               "Enable PRBS Engine for a DCC channel",
               DEF_SDKPARAM("dcc", "DccId", ""),
               "${dcc}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccHdlcPrbsDisable,
               "sdh line dcc hdlc prbs disable",
               "Disable PRBS Engine for a DCC channel",
               DEF_SDKPARAM("dcc", "DccId", ""),
               "${dcc}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccHdlcPrbsMode,
               "sdh line dcc hdlc prbs mode",
               "Set PRBS mode for a DCC channel PRBS Engine",
               DEF_SDKPARAM("dcc", "DccId", "")
               DEF_SDKPARAM("mode", "eAtPrbsMode", ""),
               "${dcc} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccHdlcPrbsInvertEnable,
               "sdh line dcc hdlc prbs invert",
               "Invert PRBS for a DCC channel PRBS Engine",
               DEF_SDKPARAM("dcc", "DccId", ""),
               "${dcc}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccHdlcPrbsInvertDisable,
               "sdh line dcc hdlc prbs noinvert",
               "No invert PRBS for a DCC channel PRBS Engine",
               DEF_SDKPARAM("dcc", "DccId", ""),
               "${dcc}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccHdlcPrbsForce,
               "sdh line dcc hdlc prbs force",
               "Force PRBS for a DCC channel PRBS Engine",
               DEF_SDKPARAM("dcc", "DccId", ""),
               "${dcc}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccHdlcPrbsUnForce,
               "sdh line dcc hdlc prbs unforce",
               "Unforce PRBS for a DCC channel PRBS Engine",
               DEF_SDKPARAM("dcc", "DccId", ""),
               "${dcc}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccHdlcPrbsForceSingle,
               "sdh line dcc hdlc prbs force single",
               "Force single PRBS for a DCC channel PRBS Engine",
               DEF_SDKPARAM("dcc", "DccId", "")
               DEF_SDKPARAM("numForcedErrors", "STRING", "NumForcedErrors"),
               "${dcc} ${numForcedErrors}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccHdlcPrbsForceBer,
               "sdh line dcc hdlc prbs force ber",
               "Force single PRBS for a DCC channel PRBS Engine",
               DEF_SDKPARAM("dcc", "DccId", "")
               DEF_SDKPARAM("ber", "ePrbsBer", ""),
               "${dcc} ${ber}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccHdlcPrbsFixPattern,
               "sdh line dcc hdlc prbs fixpattern",
               "Force single PRBS for a DCC channel PRBS Engine",
               DEF_SDKPARAM("dcc", "DccId", "")
               DEF_SDKPARAM("tx", "STRING", "Transmitting pattern")
               DEF_SDKPARAM("expect", "STRING", "Expected pattern"),
               "${dcc} ${tx} ${expect}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccHdlcPrbsBitOrder,
               "sdh line dcc hdlc prbs bitorder",
               "Force single PRBS for a DCC channel PRBS Engine",
               DEF_SDKPARAM("dcc", "DccId", "")
               DEF_SDKPARAM("bitOrder", "eAtPrbsBitOrder", "Bit order is LSB or MSB"),
               "${dcc} ${bitOrder}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccHdlcPrbsSide,
               "sdh line dcc hdlc prbs side",
               "Set PRBS generating/monitoring side for a DCC channel PRBS Engine",
               DEF_SDKPARAM("dcc", "DccId", "")
               DEF_SDKPARAM("side", "eAtPrbsSide", "PRBS generating/monitoring side"),
               "${dcc} ${side}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccHdlcPrbsSideGenerating,
               "sdh line dcc hdlc prbs side generating",
               "Set PRBS generating/monitoring side for a DCC channel PRBS Engine",
               DEF_SDKPARAM("dcc", "DccId", "")
               DEF_SDKPARAM("side", "eAtPrbsSide", "PRBS generating/monitoring side"),
               "${dcc} ${side}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccHdlcPrbsSideMonitoring,
               "sdh line dcc hdlc prbs side monitoring",
               "Set PRBS generating/monitoring side for a DCC channel PRBS Engine",
               DEF_SDKPARAM("dcc", "DccId", "")
               DEF_SDKPARAM("side", "eAtPrbsSide", "PRBS generating/monitoring side"),
               "${dcc} ${side}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccHdlcPrbsTxEnable,
               "sdh line dcc hdlc prbs tx enable",
               "Enable TX PRBS for a DCC channel PRBS Engine",
               DEF_SDKPARAM("dcc", "DccId", ""),
               "${dcc}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccHdlcPrbsTxDisable,
               "sdh line dcc hdlc prbs tx disable",
               "Disable TX PRBS for a DCC channel PRBS Engine",
               DEF_SDKPARAM("dcc", "DccId", ""),
               "${dcc}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccHdlcPrbsRxEnable,
               "sdh line dcc hdlc prbs rx enable",
               "Enable RX PRBS for a DCC channel PRBS Engine",
               DEF_SDKPARAM("dcc", "DccId", ""),
               "${dcc}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineDccHdlcPrbsRxDisable,
               "sdh line dcc hdlc prbs rx disable",
               "Disable RX PRBS for a DCC channel PRBS Engine",
               DEF_SDKPARAM("dcc", "DccId", ""),
               "${dcc}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliShowAtSdhLineDccHdlcPrbs,
               "show sdh line dcc hdlc prbs",
               "Show PRBS for a DCC channel PRBS Engine",
               DEF_SDKPARAM("dcc", "DccId", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", "Read to clear without display table"),
               "${dcc} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliShowAtSdhLineDccHdlcPrbsBer,
               "show sdh line dcc hdlc prbs ber",
               "Show PRBS BER for a DCC channel PRBS Engine",
               DEF_SDKPARAM("dcc", "DccId", ""),
               "${dcc}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliShowAtSdhLineDccHdlcPrbsCounters,
               "show sdh line dcc hdlc prbs counters",
               "Show PRBS counters for a DCC channel PRBS Engine",
               DEF_SDKPARAM("dcc", "DccId", "")
               DEF_SDKPARAM("counterReadMode", "HistoryReadingMode", "read only/read to clear mode")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", "Read to clear without display table"),
               "${dcc} ${counterReadMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDebugAtSdhLineDccHdlcPrbs,
               "debug sdh line dcc hdlc prbs",
               "Debug PRBS for a DCC channel PRBS Engine",
               DEF_SDKPARAM("dcc", "DccId", ""),
               "${dcc}")
    {
    mAtCliCall();
    }

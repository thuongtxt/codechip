/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : ClishCliAtSdhMap.c
 *
 * Created Date: Nov 14, 2012
 *
 * Author      : dungnt
 *
 * Description : SONET/SDH Mapping CLISH(es)
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/
#define regLineno "[1-8]"
#define regAug1no "(1[0-6]|[1-9])"
#define regAug4no "[1-16]"
#define regAug16no "[1-4]"
#define regAu3no "[0-3]"
#define regTug3no regAu3no
#define regAu3tug3no regAu3no
#define regTug2no "[1-7]"
#define regTuno "[1-4]"

/*
 * Define AUG-64 ID list regular expression
 */
#define Aug64Id "("regLineno"[.]1)"
#define Aug64IdListItem "(("Aug64Id"[-]"Aug64Id")|"Aug64Id")"
#define Aug64IdList "((("Aug64IdListItem"[,])+"Aug64IdListItem")|"Aug64IdListItem")"

/*
 * Define AUG-16 ID list regular expression
 */
#define Aug16Id "("regLineno"."regAug4no")"
#define Aug16IdListItem "(("Aug16Id"[-]"Aug16Id")|"Aug16Id")"
#define Aug16IdList "((("Aug16IdListItem"[,])+"Aug16IdListItem")|"Aug16IdListItem")"

/*
 * Define AUG-4 ID list regular expression
 */
#define Aug4Id "("regLineno"."regAug4no")"
#define Aug4IdListItem "(("Aug4Id"[-]"Aug4Id")|"Aug4Id")"
#define Aug4IdList "((("Aug4IdListItem"[,])+"Aug4IdListItem")|"Aug4IdListItem")"

/*
 * Define AUG-1 ID list regular expression
 */
#define Aug1Id "("regLineno"."regAug1no")"
#define Aug1IdListItem "(("Aug1Id"[-]"Aug1Id")|"Aug1Id")"
#define Aug1IdList "((("Aug1IdListItem"[,])+"Aug1IdListItem")|"Aug1IdListItem")"

/*
 * Define AU-3/TUG-3 ID list regular expression
 */
#define Au3Tug3Id "("regLineno"[.]"regAug1no"[.]"regAu3tug3no")"
#define Au3Tug3IdListItem "(("Au3Tug3Id"[-]"Au3Tug3Id")|"Au3Tug3Id")"
#define Au3Tug3IdList "((("Au3Tug3IdListItem"[,])+"Au3Tug3IdListItem")|"Au3Tug3IdListItem")"

/*
 * Define TUG-2 ID list regular expression
 */
#define Tug2Id "("regLineno"[.]"regAug1no"[.]"regAu3tug3no"[.]"regTug2no")"
#define Tug2IdListItem "(("Tug2Id"[-]"Tug2Id")|"Tug2Id")"
#define Tug2IdList "((("Tug2IdListItem"[,])+"Tug2IdListItem")|"Tug2IdListItem")"

#define Tug2List    "tug2."Tug2IdList
#define Au3Tug3List   "(au3."Au3Tug3IdList"|tug3."Au3Tug3IdList")"
#define Vc4List     "vc4."Aug1IdList
#define Vc4_4cList  "vc4_4c."Aug1IdList
#define Aug1List   "aug1."Aug1IdList
#define Aug4List   "aug4."Aug4IdList
#define Aug16List   "aug16."Aug16IdList
#define Aug64List   "aug64."Aug64IdList

#define TypeDotIdsList          "("Aug64List"|"Aug16List"|"Aug4List"|"Aug1List"|"Vc4_4cList"|"Vc4List"|"Au3Tug3List"|"Tug2List")"

/* Define mapping types for all layers */
#define SdhMapTypeList  "(vc4_64c|c4_64c|4xaug16s|vc4_16c|c4_16c|4xaug4s|vc4_4c|4xaug1s|vc4|3xvc3s|c4_4c|c4|3xtug3s|c3|7xtug2s|de3|c1x|de1|vc3|7xtug2s|tu12|tu11|none)"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("sdh", "\nSDH mapping commands:\n")
TOP_SDKCOMMAND("show sdh", "\nShow SDH mapping commands:\n")
TOP_SDKCOMMAND("show sonet", "\nShow SONET mapping commands:\n")

DEF_PTYPE("MappableChannelList", "regexp", ".+",
          "\nChannel List:\n"
          "- AUG-64  : aug64.<lineId>.<aug64Id>\n"
          "- AUG-16  : aug16.<lineId>.<aug16Id>\n"
          "- AUG-4   : aug4.<lineId>.<aug4Id>\n"
          "- AUG-1   : aug1.<lineId>.<aug1Id>\n"
          "- VC4-64C : vc4_64c.<lineId>.<aug64Id>\n"
          "- VC4-16C : vc4_16c.<lineId>.<aug16Id>\n"
          "- VC4-4C  : vc4_4c.<lineId>.<aug1Id>\n"
          "- VC-4    : vc4.<lineId>.<aug1Id>\n"
          "- VC-3    : vc3.<lineId>.<aug1Id>.<au3Tug3Id>\n"
          "- VC-1x   : vc1x.<lineId>.<aug1Id>.<au3Tug3Id>.<tug2Id>.<tuId>\n"
          "Examples: \n"
          "- VC4 list: vc4.1.1-1.3\n"
          "- VC3 list: vc3.1.1.1-1.16.3\n")

DEF_PTYPE("SdhMapType", "regexp", SdhMapTypeList,
           "\nInput SDH mapping type:\n"
          "* AUG-64 mapping types:\n"
          "  + vc4_64c: AUG-64 maps VC4-64C\n"
          "  + 4xaug16s: AUG-64 maps 4xAUG-16s\n"
          "* AUG-16 mapping types:\n"
           "  + vc4_16c: AUG-16 maps VC4-16C\n"
           "  + 4xaug4s: AUG-16 maps 4xAUG-4s\n"
           "* AUG-4 mapping types:\n"
           "  + vc4_4c: AUG-4 maps VC4-4C\n"
           "  + 4xaug1s: AUG-4 maps 4xAUG-1s\n"
           "* AUG-1 mapping types:\n"
           "  + vc4: AUG-1 maps VC4\n"
           "  + 3xvc3s: AUG-1 maps 3xVC-3s\n"
           "* VC4-64C mapping types:\n"
           "  + c4_64c: VC4-64C maps C4-64C\n"
           "  + none: no mapping\n"
           "* VC4-16C mapping types:\n"
           "  + c4_16c: VC4-16C maps C4-16C\n"
           "  + none: no mapping\n"
           "* VC4-4C mapping types:\n"
           "  + c4_4c: VC4-4C maps C4-4C\n"
           "  + none: no mapping\n"
           "* VC-4 mapping types:\n"
           "  + c4: VC-4 maps C-4\n"
           "  + 3xtug3s: VC-4 maps 3xTUG-3\n"
           "  + none: no mapping\n"
           "* VC-3 mapping types:\n"
           "  + c3: VC-3 maps C3\n"
           "  + 7xtug2s: VC-3 maps 7xTUG2s\n"
           "  + de3: VC-3 maps DS3/E3\n"
           "  + none: no mapping\n"
           "* VC-1x mapping types:\n"
           "  + c1x: map C11/C12\n"
           "  + de1: map DS1/E1\n"
           "  + none: no mapping\n"
           "* TUG-3 mapping types:\n"
           "  + vc3: TUG-3 maps VC3\n"
           "  + 7xtug2s: TUG-3 maps 7xTUG2s\n"
           "* TUG-2 mapping types:\n"
           "  + tu12: TUG-2 maps TU12\n"
           "  + tu11: TUG-2 maps TU11\n")

DEF_PTYPE("HierarchyChannelList", "regexp", ".+",
         "\nChannel List:\n"
         "- LINE  : line.<lineId>\n"
         "- AUG-64: aug64.<lineId>.<aug64Id>\n"
         "- AUG-16: aug16.<lineId>.<aug16Id>\n"
         "- AUG-4 : aug4.<lineId>.<aug4Id>\n"
         "- AUG-1 : aug1.<lineId>.<aug1Id>\n"
         "- VC-4  : vc4.<lineId>.<aug1Id>\n"
         "- VC-3  : vc3.<lineId>.<aug1Id>.<au3Tug3Id>\n"
         "- VC-1x : vc1x.<lineId>.<aug1Id>.<au3Tug3Id>.<tug2Id>.<tuId>\n"
         "Examples: \n"
         "- VC4 list: vc4.1.1-1.3\n"
         "- VC3 list: vc3.1.1.1-1.64.3\n")

DEF_PTYPE("HierarchyDebugMode", "select", "debug_sts(0), debug_bridgeandroll(1), none(2)",
          "\nDebugging mode:\n"
          "- debug_sts           : Debug HW sts\n"
          "- debug_bridgeandroll : Debug bridge and roll\n")

/* SET SDH mapping type */
DEF_SDKCOMMAND(cliSdhMapSet,
               "sdh map",
               "Set SDH mapping type\n",
               DEF_SDKPARAM("channelList", "MappableChannelList", "")
               DEF_SDKPARAM("mapType", "SdhMapType", ""),
               "${channelList} ${mapType}")
    {
    mAtCliCall();
    }

/* Show mapping type of SDH channels */
DEF_SDKCOMMAND(cliSdhMapShow,
               "show sdh map",
               "Show mapping type of SDH channels\n",
               DEF_SDKPARAM("channelList", "MappableChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

/* Show mapping type of SDH channels */
DEF_SDKCOMMAND(cliSdhMapHierarchyShow,
               "show sdh hierarchy",
               "Show mapping hierarchy of SDH channels\n",
               DEF_SDKPARAM("channelList", "HierarchyChannelList", "")
               DEF_SDKPARAM_OPTION("debug", "HierarchyDebugMode", ""),
               "${channelList} ${debug}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSonetMapHierarchyShow,
               "show sonet hierarchy",
               "Show mapping hierarchy of SONET channels\n",
               DEF_SDKPARAM("channelList", "HierarchyChannelList", "")
               DEF_SDKPARAM_OPTION("debug", "HierarchyDebugMode", ""),
               "${channelList} ${debug}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhMapHierarchyHistoryShow,
               "show sdh hierarchy history",
               "Show mapping type of channels along with channels' history",
               DEF_SDKPARAM("channelList", "HierarchyChannelList", "")
               DEF_SDKPARAM("readMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("debug", "HierarchyDebugMode", ""),
               "${channelList} ${readMode}")
    {
    mAtCliCall();
    }

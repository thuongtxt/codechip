/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : ClishAtPppLink.c
 *
 * Created Date: Nov 23, 2012
 *
 * Description : Link CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
DEF_PTYPE("eAtPppLinkPhase", "select", "dead(0), establish(1), network(3), networkactive(4)", "Ppp Link State")
DEF_PTYPE("dataType", "regexp", ".*", "Ppp OAM data")
DEF_PTYPE("pktOamString", "regexp", ".*", "String packet Oam")

TOP_SDKCOMMAND("show ppp link", "Show PPP Link CLIs")
TOP_SDKCOMMAND("ppp link", "PPP Link CLIs")
TOP_SDKCOMMAND("debug", "Debug ppp")
TOP_SDKCOMMAND("debug ppp", "Debug Ppp link")
TOP_SDKCOMMAND("lcp", "LCP")
TOP_SDKCOMMAND("lcp send", "LCP send")
TOP_SDKCOMMAND("ipcp", "IPCP")
TOP_SDKCOMMAND("ipcp send", "IPCP send")
TOP_SDKCOMMAND("ppp packet", "PPP packet")
TOP_SDKCOMMAND("hdlc", "Hdlc Link CLIs")
TOP_SDKCOMMAND("ppp link pfc", "CLIs to control Protocol Field Compression")
TOP_SDKCOMMAND("ppp link bcp", "CLIs to control BCP")
TOP_SDKCOMMAND("ppp link bcp lanfcs", "CLIs to control BCP LAN FCS Field")
TOP_SDKCOMMAND("ppp link bcp lanfcs insert", "CLIs to control BCP LAN FCS Field insertion")

/* show counter link */
DEF_SDKCOMMAND(cliPppLinkCounterShow,
             "show ppp link counters",
             "Show link counters.\n",
             DEF_SDKPARAM("linkList", "PppLinklist", "")
             DEF_SDKPARAM("readingMode", "HistoryReadingMode","")
             DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
             "${linkList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

/* set ppp link state */
DEF_SDKCOMMAND(cliPppLinkPhaseSet,
             "ppp link phase",
             "Set PPP link phase.\n",
             DEF_SDKPARAM("linkList", "PppLinklist", "")
             DEF_SDKPARAM("phase", "eAtPppLinkPhase", ""),
             "${linkList} ${phase}")
    {
    mAtCliCall();
    }

/* Enable OAM auto capturing */
DEF_SDKCOMMAND(cliPppOamAutoCapturingEnable,
             "ppp link oamcapture",
             "Enable/disable OAM auto caturing\n",
             DEF_SDKPARAM("linkList", "PppLinklist", "")
             DEF_SDKPARAM("enable", "eBool", "Enable/disable OAM auto caturing"),
             "${linkList} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPppOamLcpMruSend,
             "lcp send mru",
             "",
             DEF_SDKPARAM("linkList", "PppLinklist", "")
             DEF_SDKPARAM("mruValue", "UINT", ""),
             "${linkList} ${mruValue}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPppOamReceive,
             "lcp receive",
             "Display received OAM message on specified links",
             DEF_SDKPARAM("linkList", "PppLinklist", ""),
             "${linkList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPppOamLcpEchoRequestSend,
             "lcp send echorequest",
             "",
             DEF_SDKPARAM("linkList", "PppLinklist", "")
             DEF_SDKPARAM("magicNumber", "UINT", "Magic number")
             DEF_SDKPARAM("data", "dataType", "ASCII data"),
             "${linkList} ${magicNumber} ${data}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPppOamIpcpIpAddressSend,
             "ipcp send ipaddress",
             "",
             DEF_SDKPARAM("linkList", "PppLinklist", "")
             DEF_SDKPARAM("ipAddr", "STRING", "Ip Address"),
             "${linkList} ${ipAddr}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPppLinkEnable,
               "ppp link enable",
               "\nThis command is to enable PPP link list.\n",
               DEF_SDKPARAM("linkList", "PppLinklist", ""),
               "${linkList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPppLinkDisable,
               "ppp link disable",
               "\nThis command is to disable PPP link list.\n",
               DEF_SDKPARAM("linkList", "PppLinklist", ""),
               "${linkList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPppOamLcpPfcSend,
             "lcp send pfc",
             "",
             DEF_SDKPARAM("linkList", "PppLinklist", ""),
             "${linkList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPppPktSend,
             "ppp packet send",
             "",
             DEF_SDKPARAM("linkList", "PppLinklist", ""),
             "${linkList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPppIntrDebug,
             "debug ppp link",
             "Show all debug information of PPP link",
             DEF_SDKPARAM("linkList", "PppLinklist", ""),
             "${linkList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPppPktOamSend,
             "hdlc send",
             "Send packet Oam",
             DEF_SDKPARAM("linkList", "PppLinklist", "")
             DEF_SDKPARAM("pktString", "pktOamString", ""),
             "${linkList} ${pktString}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPppLinkProtocolTxCompressEnable,
             "ppp link pfc enable",
             "Enable protocol compression at TX direction\n",
             DEF_SDKPARAM("linkList", "PppLinklist", ""),
             "${linkList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPppLinkProtocolTxCompressDisable,
             "ppp link pfc disable",
             "Disable protocol compression at TX direction\n",
             DEF_SDKPARAM("linkList", "PppLinklist", ""),
             "${linkList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPppLinkBcpLanFcsFieldTxEnable,
             "ppp link bcp lanfcs insert enable",
             "Enable insert LAN FCS field in BCP header at TX direction\n",
             DEF_SDKPARAM("linkList", "PppLinklist", ""),
             "${linkList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPppLinkBcpLanFcsFieldTxDisable,
             "ppp link bcp lanfcs insert disable",
             "Disable insert LAN FCS field in BCP header at TX direction\n",
             DEF_SDKPARAM("linkList", "PppLinklist", ""),
             "${linkList}")
    {
    mAtCliCall();
    }

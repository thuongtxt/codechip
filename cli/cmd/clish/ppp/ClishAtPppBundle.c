/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2102 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : atclippp.c
 *
 * Created Date: Oct 9, 2012
 *
 * Description : PPP CLIs
 *
 * Notes       :
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/
#define regPppno "1..256"
#define regBundleno "1..128"
#define regFlowno "1..1024"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
DEF_PTYPE("ClassNumber", "integer", "0..15", "Class number")
DEF_PTYPE("SequenceNumber", "integer", "0..65535", "Sequence number")

/* Top commands */
TOP_SDKCOMMAND("ppp bundle", "Bundle")
TOP_SDKCOMMAND("ppp bundle add", "Link/Flow")
TOP_SDKCOMMAND("ppp bundle remove", "Link/Flow")
TOP_SDKCOMMAND("show ppp", "Show PPP bundle/link")
TOP_SDKCOMMAND("debug", "Debug")
TOP_SDKCOMMAND("debug ppp", "Debug ppp bundle")
TOP_SDKCOMMAND("ppp bundle oamegress", "Bundle Egress OAM Flow")
TOP_SDKCOMMAND("ppp bundle oamegress vlan", "Bundle OAM Flow Egress Vlan")
TOP_SDKCOMMAND("ppp bundle oamingress", "Bundle Ingress OAM Flow")
TOP_SDKCOMMAND("ppp bundle oamingress vlan", "Bundle OAM Flow Ingress Vlan")
/*
 * Define regular expression for list of bundle/ppplink IDs
 */
/* Ppplink */
#define pppLinkLstElement "(("regPppno"[-]"regPppno")|"regPppno")"
#define pppLinkLst "(("pppLinkLstElement"[,])+"pppLinkLstElement"|"pppLinkLstElement")"

/* Bundle */
#define pppBundleLstElement "(("regBundleno"[-]"regBundleno")|"regBundleno")"
#define pppBundleLst "(("pppBundleLstElement"[,])+"pppBundleLstElement"|"pppBundleLstElement")"

/* Ethernet Flow */
#define ethFlowLstElement "(("regFlowno"[-]"regFlowno")|"regFlowno")"
#define ethFlowLst "(("ethFlowLstElement"[,])+"ethFlowLstElement"|"ethFlowLstElement")"

/* Data types */
DEF_PTYPE("MpBundlelists", "regexp", ".*", "List of bundle IDs. For example: 1-3,6,8 gives list of 1,2,3,6,8")
DEF_PTYPE("mpBundleId", "integer", ".*", "Bundle ID")
DEF_PTYPE("PppLinklist", "regexp", ".*", "List of link IDs. For example: 1-3,6,8 gives list of 1,2,3,6,8")
DEF_PTYPE("PppId", "integer", ".*", "Link ID")
DEF_PTYPE("EthFlowList", "regexp", ".*", "List of Flow IDs. For example: 1-5,7,8 gives list of 1,2,3,4,4,7,8")
DEF_PTYPE("EthFlowId", "integer", ".*", "Flow ID")

/* Other enumerations */
DEF_PTYPE("eAtFragmentSize", "select", "none(0), 64bytes(1), 128bytes(2), 256bytes(3), 512bytes(4)", "Fragment size")
DEF_PTYPE("eAtMpWorkingMode", "select", "lfi(0), mcml(1)", "Sequence Mode")
DEF_PTYPE("eAtMpSequenceMode", "select", "short(0), long(1)", "Working Mode")
DEF_PTYPE("eAtMpSchedulingMode", "select", "random(0), fair(1), strict(2), smart(3)", "Scheduling mode")

DEF_SDKCOMMAND(cliPppBundleMaxDelay,
             "ppp bundle maxdelay",
             "Set Link MaxDelay\n",
             DEF_SDKPARAM("bundlelist", "MpBundlelists", "")
             DEF_SDKPARAM("maxDelay", "UINT", "Max link delay (milisecond)"),
             "${bundlelist} ${maxDelay}")
    {
    mAtCliCall();
    }

/* Set fragmentsize bundle */
DEF_SDKCOMMAND(cliPppBundleFragmentsize,
             "ppp bundle fragmentsize",
             "Set Fragmentsize.\n",
             DEF_SDKPARAM("bundlelist", "MpBundlelists", "")
             DEF_SDKPARAM("fragmentSize", "eAtFragmentSize", ""),
             "${bundlelist} ${fragmentSize}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPppBundleSequenceMode,
             "ppp bundle sequence",
             "Set Sequence Mode applied for both TX and RX direction.\n",
             DEF_SDKPARAM("bundlelist", "MpBundlelists", "")
             DEF_SDKPARAM("sequenceMode", "eAtMpSequenceMode", ""),
             "${bundlelist} ${sequenceMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPppBundleTxSequenceMode,
             "ppp bundle txsequence",
             "Set Sequence mode direction from Ethernet to TDM.\n",
             DEF_SDKPARAM("bundlelist", "MpBundlelists", "")
             DEF_SDKPARAM("sequenceMode", "eAtMpSequenceMode", ""),
             "${bundlelist} ${sequenceMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPppBundleRxSequenceMode,
             "ppp bundle rxsequence",
             "Set Sequence mode direction from TDM to Ethernet.\n",
             DEF_SDKPARAM("bundlelist", "MpBundlelists", "")
             DEF_SDKPARAM("sequenceMode", "eAtMpSequenceMode", ""),
             "${bundlelist} ${sequenceMode}")
    {
    mAtCliCall();
    }

/* Set working mode */
DEF_SDKCOMMAND(cliPppBundleWorking,
             "ppp bundle workingmode",
             "Set working Mode.\n",
             DEF_SDKPARAM("bundleList", "MpBundlelists", "")
             DEF_SDKPARAM("workingMode", "eAtMpWorkingMode", ""),
             "${bundleList} ${workingMode}")
    {
    mAtCliCall();
    }

/* Set Mrru mode */
DEF_SDKCOMMAND(cliPppBundleMrru,
             "ppp bundle mrru",
             "Set Mrru Mode.\n",
             DEF_SDKPARAM("bundlelist", "MpBundlelists", "")
             DEF_SDKPARAM("mrru", "UINT", "MRRU"),
             "${bundlelist} ${mrru}")
    {
    mAtCliCall();
    }

/* enable/disable bundle */
DEF_SDKCOMMAND(cliPppBundleEnable,
             "ppp bundle enable",
             "Set enable bundle\n",
             DEF_SDKPARAM("bundlelist", "MpBundlelists", ""),
             "${bundlelist}")
    {
    mAtCliCall();
    }

/* enable/disable bundle */
DEF_SDKCOMMAND(cliPppBundleDisable,
             "ppp bundle disable",
             "Set Mrru Mode.\n",
             DEF_SDKPARAM("bundlelist", "MpBundlelists", ""),
             "${bundlelist}")
    {
    mAtCliCall();
    }

/* Add link to bundle */
DEF_SDKCOMMAND(cliPppBundleAddLink,
             "ppp bundle add link",
             "Add link to bundle.\n",
             DEF_SDKPARAM("bundleId", "mpBundleId", "")
             DEF_SDKPARAM("linkList", "PppLinklist", ""),
             "${bundleId} ${linkList}")
    {
    mAtCliCall();
    }

/* Remove link from bundle */
DEF_SDKCOMMAND(cliPppBundleRemoveLink,
             "ppp bundle remove link",
             "Remove link from bundle.\n",
             DEF_SDKPARAM("bundleId", "mpBundleId", "")
             DEF_SDKPARAM("linkList", "PppLinklist", ""),
             "${bundleId} ${linkList}")
    {
    mAtCliCall();
    }

/* show number link in bundle */
DEF_SDKCOMMAND(cliPppBundleShowLink,
             "show ppp bundle links",
             "show number link to bundle.\n",
             DEF_SDKPARAM("bundleList", "MpBundlelists", ""),
             "${bundleList}")
    {
    mAtCliCall();
    }

/* add flow to bundle */
DEF_SDKCOMMAND(cliPppBundleAddFlow,
             "ppp bundle add flow",
             "Add flow to bundle.\n",
             DEF_SDKPARAM("bundleId", "mpBundleId", "")
             DEF_SDKPARAM("flowId", "EthFlowId", "")
             DEF_SDKPARAM("classNumber", "ClassNumber", ""),
             "${bundleId} ${flowId} ${classNumber}")
    {
    mAtCliCall();
    }

/* Remove flow to bundle */
DEF_SDKCOMMAND(cliPppBundleRemoveFlow,
             "ppp bundle remove flow",
             "Remove flow from bundle.\n",
             DEF_SDKPARAM("bundleId", "mpBundleId", "")
             DEF_SDKPARAM("flowList", "EthFlowList", ""),
             "${bundleId} ${flowList}")
    {
    mAtCliCall();
    }

/* show flow in bundle */
DEF_SDKCOMMAND(cliPppFlowInBundleShow,
             "show ppp bundle flows",
             "Show flow in bundle.\n",
             DEF_SDKPARAM("bundleId", "MpBundlelists", ""),
             "${bundleId}")
    {
    mAtCliCall();
    }

/* show counter link */
DEF_SDKCOMMAND(cliPppBundleCounterShow,
             "show ppp bundle counters",
             "Show bundle counters.\n",
             DEF_SDKPARAM("bundleList", "MpBundlelists", "")
             DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
             DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
             "${bundleList} ${readingMode} $(silent)")
    {
    mAtCliCall();
    }

/* show configure bundle */
DEF_SDKCOMMAND(cliPppBundleConfigShow,
             "show ppp bundle",
             "show configure bundle.\n",
             DEF_SDKPARAM("bundleList", "MpBundlelists", ""),
             "${bundleList}")
    {
    mAtCliCall();
    }

/* configure sequence number bundle */
DEF_SDKCOMMAND(cliPppBundleSequenceNumberSet,
             "ppp bundle sequence number",
             "Restart sequence number with specific value\n",
             DEF_SDKPARAM("bundleId", "mpBundleId", "")
             DEF_SDKPARAM("classNumber", "ClassNumber", "")
             DEF_SDKPARAM("sequenceNumber", "SequenceNumber", ""),
             "${bundleId} ${classNumber} &{sequenceNumber}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPppBundleDebug,
               "debug ppp bundle",
               "Show all of debugging information",
               DEF_SDKPARAM("bundles", "MpBundlelists", ""),
               "${bundles}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMpBundleSchedulingModeSet,
               "ppp bundle scheduling",
               "Set scheduling mode\n",
               DEF_SDKPARAM("bundles", "MpBundlelists", "")
               DEF_SDKPARAM("schedulingMode", "eAtMpSchedulingMode", ""),
               "${bundles} ${schedulingMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMpBundleHierarchyShow,
               "show ppp bundle hierarchy",
               "Show PPP bundle hierarchy\n",
               DEF_SDKPARAM("bundles", "MpBundlelists", ""),
               "${bundles}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMpBundleOamFlowEgressVlanAdd,
               "ppp bundle oamegress vlan add",
               "Add OAM Egress VLAN\n",
               DEF_SDKPARAM("bundles", "MpBundlelists", "")
               DEF_SDKPARAM("portId", "PortId", "")
               DEF_SDKPARAM("cVlan", "VlanDesc", "")
               DEF_SDKPARAM("sVlan", "VlanDesc", ""),
               "${bundles} ${portId} ${cVlan} ${sVlan}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMpBundleOamFlowEgressVlanRemove,
               "ppp bundle oamegress vlan remove",
               "Remove OAM Egress VLAN\n",
               DEF_SDKPARAM("bundles", "MpBundlelists", "")
               DEF_SDKPARAM("portId", "PortId", "")
               DEF_SDKPARAM("cVlan", "VlanDesc", "")
               DEF_SDKPARAM("sVlan", "VlanDesc", ""),
               "${bundles} ${portId} ${cVlan} ${sVlan}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMpBundleOamFlowIngressVlanAdd,
               "ppp bundle oamingress vlan add",
               "Add OAM Ingress VLAN\n",
               DEF_SDKPARAM("bundles", "MpBundlelists", "")
               DEF_SDKPARAM("portId", "PortId", "")
               DEF_SDKPARAM("cVlan", "VlanDesc", "")
               DEF_SDKPARAM("sVlan", "VlanDesc", ""),
               "${bundles} ${portId} ${cVlan} ${sVlan}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMpBundleOamFlowIngressVlanRemove,
               "ppp bundle oamingress vlan remove",
               "Remove OAM Ingress VLAN\n",
               DEF_SDKPARAM("bundles", "MpBundlelists", "")
               DEF_SDKPARAM("portId", "PortId", "")
               DEF_SDKPARAM("cVlan", "VlanDesc", "")
               DEF_SDKPARAM("sVlan", "VlanDesc", ""),
               "${bundles} ${portId} ${cVlan} ${sVlan}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPppBundlePdu,
               "ppp bundle pdu",
               "Set PDU type for bundle\n",
               DEF_SDKPARAM("bundleList", "MpBundlelists", "")
               DEF_SDKPARAM("pduType", "eAtHdlcPduType", ""),
               "${bundleList} ${pduType}")
    {
    mAtCliCall();
    }

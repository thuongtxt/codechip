/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : ClishAtPidTable.c
 *
 * Created Date: Sep 26, 2013
 *
 * Description : PPP PID table CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("ppp table", "CLIs to control PID table")

/* Data types */
DEF_PTYPE("EntryLists", "regexp", ".*", "List of entry IDs. For example: 1-3,6,8 gives list of 1,2,3,6,8")

DEF_SDKCOMMAND(cliPppEntryTablePidSet,
               "ppp table pid",
               "Set PID value for specified entries at two directions.\n",
               DEF_SDKPARAM("entryIds", "EntryLists", "")
               DEF_SDKPARAM("pid", "STRING", "PID value"),
               "${entryIds} ${pid}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPppEntryTablePidTdmToPsnSet,
               "ppp table pid tdm2psn",
               "Set PID value for specified entries at direction from TDM to PSN.\n",
               DEF_SDKPARAM("entryIds", "EntryLists", "")
               DEF_SDKPARAM("pid", "STRING", "PID value"),
               "${entryIds} ${pid}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPppEntryTablePidPsnToTdmSet,
               "ppp table pid psn2tdm",
               "Set PID value for specified entries at direction from PSN to TDM.\n",
               DEF_SDKPARAM("entryIds", "EntryLists", "")
               DEF_SDKPARAM("pid", "STRING", "PID value"),
               "${entryIds} ${pid}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPppEntryTableEthTypeSet,
               "ppp table ethtype",
               "Set Ethernet type for specified entries at two directions.\n",
               DEF_SDKPARAM("entryIds", "EntryLists", "")
               DEF_SDKPARAM("ethType", "STRING", "Ethernet type value"),
               "${entryIds} ${ethType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPppEntryTableEthTypeTdmToPsnSet,
               "ppp table ethtype tdm2psn",
               "Set Ethernet type for specified entries at TDM to PSN directions.\n",
               DEF_SDKPARAM("entryIds", "EntryLists", "")
               DEF_SDKPARAM("ethType", "STRING", "Ethernet type value"),
               "${entryIds} ${ethType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPppEntryTableEthTypePsnToTdmSet,
               "ppp table ethtype psn2tdm",
               "Set Ethernet type for specified entries at PSN to TDM directions.\n",
               DEF_SDKPARAM("entryIds", "EntryLists", "")
               DEF_SDKPARAM("ethType", "STRING", "Ethernet type value"),
               "${entryIds} ${ethType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPppEntryTableSet,
               "ppp table",
               "Set PID and Ethernet type for specified entries at two directions.\n",
               DEF_SDKPARAM("entryIds", "EntryLists", "")
               DEF_SDKPARAM("pid", "STRING", "PID value")
               DEF_SDKPARAM("ethType", "STRING", "Ethernet type value"),
               "${entryIds} ${pid} ${ethType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPppEntryTableTdmToPsnSet,
               "ppp table tdm2psn",
               "Set PID and Ethernet type for specified entries at two directions.\n",
               DEF_SDKPARAM("entryIds", "EntryLists", "")
               DEF_SDKPARAM("pid", "STRING", "PID value")
               DEF_SDKPARAM("ethType", "STRING", "Ethernet type value"),
               "${entryIds} ${pid} ${ethType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPppEntryTablePsnToTdmSet,
               "ppp table psn2tdm",
               "Set PID and Ethernet type for specified entries at direction from PSN to TDM.\n",
               DEF_SDKPARAM("entryIds", "EntryLists", "")
               DEF_SDKPARAM("pid", "STRING", "PID value")
               DEF_SDKPARAM("ethType", "STRING", "Ethernet type value"),
               "${entryIds} ${pid} ${ethType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPppEntryTableEnableSet,
               "ppp table enable",
               "Enable two directions of specified entries.\n",
               DEF_SDKPARAM("entryIds", "EntryLists", ""),
               "${entryIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPppEntryTableEnableTdmToPsnSet,
               "ppp table enable tdm2psn",
               "Enable direction from TDM to PSN of specified entries.\n",
               DEF_SDKPARAM("entryIds", "EntryLists", ""),
               "${entryIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPppEntryTableEnablePsnToTdmSet,
               "ppp table enable psn2tdm",
               "Enable direction from PSN to TDM of specified entries.\n",
               DEF_SDKPARAM("entryIds", "EntryLists", ""),
               "${entryIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPppEntryTableDisableSet,
               "ppp table disable",
               "Disable two directions of specified entries.\n",
               DEF_SDKPARAM("entryIds", "EntryLists", ""),
               "${entryIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPppEntryTableDisableTdmToPsnSet,
               "ppp table disable tdm2psn",
               "Disable direction from Tdm to Psn of specified entries.\n",
               DEF_SDKPARAM("entryIds", "EntryLists", ""),
               "${entryIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPppEntryTableDisablePsnToTdmSet,
               "ppp table disable psn2tdm",
               "Disable direction from PSN to TDM of specified entries.\n",
               DEF_SDKPARAM("entryIds", "EntryLists", ""),
               "${entryIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliModulePppPidTableInit,
               "ppp table init",
               "Initialize PID table.\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPppEntryTableGet,
               "show ppp table",
               "Show PPP table entry detail.\n",
               DEF_SDKPARAM_OPTION("entryIds", "EntryLists", ""),
               "${entryIds}")
    {
    mAtCliCall();
    }

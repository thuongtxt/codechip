/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2102 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : atclimoduleppp.c
 *
 * Created Date: Oct 9, 2012
 *
 * Description : module PPP CLIs
 *
 * Notes       :
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/* Top commands */
TOP_SDKCOMMAND("ppp", "PPP/MLPPP module")
TOP_SDKCOMMAND("ppp bundle", "MLPPP Bundle")
TOP_SDKCOMMAND("ppp interrupt", "PPP/MLPPP module interrupt")

/* Create bundle */
DEF_SDKCOMMAND(cliPppBundleCreate,
               "ppp bundle create",
               "Create bundle.\n",
               DEF_SDKPARAM("bundleId", "MpBundlelists", ""),
               "${bundleId}")
    {
    mAtCliCall();
    }

/* Delete bundle */
DEF_SDKCOMMAND(cliPppBundleDelete,
               "ppp bundle delete",
               "Delete bundle.\n",
               DEF_SDKPARAM("bundleId", "MpBundlelists", ""),
               "${bundleId}")
    {
    mAtCliCall();
    }

/* Enable Interrupt */
DEF_SDKCOMMAND(cliPppInterruptEnable,
               "ppp interrupt enable",
               "Enable interrupt.\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

/* Disable Interrupt */
DEF_SDKCOMMAND(cliPppInterruptDisable,
               "ppp interrupt disable",
               "Disable interrupt.\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

/* Show debug information */
DEF_SDKCOMMAND(cliModulePppDebug,
               "debug module ppp",
               "Debug PPP module.\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

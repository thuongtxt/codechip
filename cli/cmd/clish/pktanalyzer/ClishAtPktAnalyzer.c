/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : ClishAtPktAnalyzer.c
 *
 * Created Date: Dec 19, 2012
 *
 * Description : Packet analyzer CLIs
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
extern void AtPacketTest(void);
extern void AtPtpPacketTest(void);

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("pktanalyzer", "Packet Analyzer")
TOP_SDKCOMMAND("pktanalyzer pattern", "Packet Analyzer Pattern")
TOP_SDKCOMMAND("pktanalyzer analyze", "Packet Analyzer Analyze")
TOP_SDKCOMMAND("pktanalyzer ppp", "Packet Analyzer Ppp")
TOP_SDKCOMMAND("pktanalyzer soh", "Packet Analyzer SOH")
TOP_SDKCOMMAND("pktanalyzer ptp", "Packet Analyzer PTP")

DEF_PTYPE("eAtPktAnalyzerCompareMode", "select", "any(0), same(1), different(2)", "Pattern compare mode")
DEF_PTYPE("eAtPktAnalyzerPacketLength", "select", "32bytes(0), 64bytes(1), 128bytes(2), 256bytes(3), 512bytes(4), 1024bytes(5), full(6)", "Packet length")
DEF_PTYPE("eAtPktAnalyzerPppPacketType", "select", "mlpprotpkt(0), pppprotpkt(1), hdlc(2), pppoam(3), mlppppkt(4), ppppkt(5)", "PPP packet mode")
DEF_PTYPE("eAtPktAnalyzerPacketDumpType", "select", "all(0), error(1), good(2)", "Packet dump type")
DEF_PTYPE("eAtPktAnalyzerSohPacketType", "select", "dcc(0), kbyte(1)", "Packet SOH type")

DEF_SDKCOMMAND(CliPktAnalyzerPatternData,
               "pktanalyzer pattern data",
               "Configure pattern data",
               DEF_SDKPARAM("pattern", "STRING", "Pattern data"),
               "${pattern}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPktAnalyzerPatternMask,
               "pktanalyzer pattern mask",
               "Configure pattern mask",
               DEF_SDKPARAM("mask", "STRING", "Pattern mask"),
               "${mask}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPktAnalyzerPatternCompareMode,
               "pktanalyzer pattern comparemode",
               "Configure pattern compare mode",
               DEF_SDKPARAM("mode", "eAtPktAnalyzerCompareMode", ""),
               "${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPktAnalyzerRecapture,
               "pktanalyzer recapture",
               "Recapture to reset dump engine",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPktAnalyzerPacketLength,
               "pktanalyzer packetlength",
               "Configure packet length",
               DEF_SDKPARAM("length", "eAtPktAnalyzerPacketLength", ""),
               "${length}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPktAnalyzerNumDisplayPackets,
               "pktanalyzer numdisplaypackets",
               "Configure number of packet displayed",
               DEF_SDKPARAM("numberOfDisplayedPacket", "UINT", "Number of displayed packet"),
               "${numberOfDisplayedPacket}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPktAnalyzerPppPacketMode,
               "pktanalyzer ppp packetmode",
               "Configure PPP packet mode",
               DEF_SDKPARAM("mode", "eAtPktAnalyzerPppPacketType", ""),
               "${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPktAnalyzerPacketDumpType,
               "pktanalyzer packetdumptype",
               "Configure packet dump type",
               DEF_SDKPARAM("dumpType", "eAtPktAnalyzerPacketDumpType", ""),
               "${dumpType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPktAnalyzerAnalyzeTx,
               "pktanalyzer analyze tx",
               "Analyze packet in Tx direction",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }
    
DEF_SDKCOMMAND(CliPktAnalyzerAnalyzeRx,
               "pktanalyzer analyze rx",
               "Analyze packet in Rx direction",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPktAnalyzerCreate,
               "pktanalyzer create",
               "",
               DEF_SDKPARAM("physicalInterface", "STRING", "Physical interface :"
                                                            "+ Ethernet port: eport.<portId>\n"
                                                            "+ Ethernet flow: flow.<flowId>\n"
                                                            "+ PPP link: ppp.<linkId>\n"
                                                            "+ Hdlc channel: hdlc.<channelId>\n"
                                                            "+ DCC channel: dcc.<lineId>.<section/line>\n"
                                                            "+ HDLC DCC channel: hdlc_dcc.<lineId>.<section/line>\n"),
               "${physicalInterfaceList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPacketAnalyzerSelfTest,
               "pktanalyzer test",
               "Self test packet analyzer to see how it work\n",
               DEF_NULLPARAM,
               "")
    {
    AtUnused(context);                                                     \
    AtUnused(argv);                                                        \
    AtPacketTest();
    return 0;
    }

DEF_SDKCOMMAND(CliPktAnalyzerSohPacketType,
               "pktanalyzer soh packettype",
               "Configure SOH packet type",
               DEF_SDKPARAM("type", "eAtPktAnalyzerSohPacketType", ""),
               "${type}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPtpPacketAnalyzerSelfTest,
               "pktanalyzer ptp test",
               "Self test PTP packet analyzer to see how it work\n",
               DEF_NULLPARAM,
               "")
    {
    AtUnused(context);                                                     \
    AtUnused(argv);                                                        \
    AtPtpPacketTest();
    return 0;
    }


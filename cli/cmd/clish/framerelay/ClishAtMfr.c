/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : ClishAtMfr.c
 *
 * Created Date: Jun 28, 2016
 *
 * Description : Multi-Frame relay.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
DEF_PTYPE("eAtMfrServiceMode", "select", "end-to-end(1), uni-nni(2)", "MFR service mode")
DEF_PTYPE("MfrIdList", "regexp", ".*", "MFR ID List. Example: 1-2,5-6")
DEF_PTYPE("MfrFragmentFormat", "select", "end_to_end(0), uni_nni(1)", "MFR fragmentation format")
DEF_PTYPE("eAtMfrSchedulingMode", "select", "random(0), fair(1), strict(2), smart(3)", "Scheduling mode")

TOP_SDKCOMMAND("mfr", "Multi-link Frame Relay CLIs")
TOP_SDKCOMMAND("mfr link", "Multi-link Frame Relay with links CLIs")
TOP_SDKCOMMAND("mfr virtualcircuit", "Multi-link Frame Relay Virtual Circuit CLIs")
TOP_SDKCOMMAND("mfr fragment", "Multi-link Frame Relay fragmentation CLIs")
TOP_SDKCOMMAND("mfr oamegress", "Multi-link Frame Relay Oam Egress CLIs")
TOP_SDKCOMMAND("mfr oamegress vlan", "Multi-link Frame Relay Oam Egress Vlan CLIs")
TOP_SDKCOMMAND("mfr oamingress", "Multi-link Frame Relay Oam Ingress CLIs")
TOP_SDKCOMMAND("mfr oamingress vlan", "Multi-link Frame Relay Oam Ingress Vlan CLIs")
TOP_SDKCOMMAND("mfr virtualcircuit", "Multi-link Frame Relay Virtual Circuit CLIs")
TOP_SDKCOMMAND("mfr virtualcircuit oamegress", "Multi-link Frame Relay Virtual Circuit Oam Egress CLIs")
TOP_SDKCOMMAND("mfr virtualcircuit oamegress vlan", "Multi-link Frame Relay Virtual Circuit Oam Egress Vlan CLIs")
TOP_SDKCOMMAND("mfr virtualcircuit oamingress", "Multi-link Frame Relay Virtual Circuit Oam Ingress CLIs")
TOP_SDKCOMMAND("mfr virtualcircuit oamingress vlan", "Multi-link Frame Relay Virtual Circuit Oam Ingress Vlan CLIs")

DEF_SDKCOMMAND(cliMfrVirtualCircuitCreate,
               "mfr virtualcircuit create",
               "Create MFR virtual circuit\n",
               DEF_SDKPARAM("mfrId", "UINT", "Mfr ID")
               DEF_SDKPARAM("dlciList", "DlciList", ""),
               "${mfrId} ${dlciList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMfrVirtualCircuitDelete,
               "mfr virtualcircuit delete",
               "Delete MFR virtual circuit\n",
               DEF_SDKPARAM("mfrId", "UINT", "Mfr ID")
               DEF_SDKPARAM("dlciList", "DlciList", ""),
               "${mfrId} ${dlciList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMfrVirtualCircuitFlowBind,
               "mfr virtualcircuit flowbind",
               "Bind flow and MFR virtual circuit\n",
               DEF_SDKPARAM("vcList", "VcIdList", "")
               DEF_SDKPARAM("flowList", "FlowIdList", ""),
               "${vcList} ${flowList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMfrBundleCreate,
               "mfr create",
               "Create MFR\n",
               DEF_SDKPARAM("mfrIdList", "MfrIdList", ""),
               "${mfrIdList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMfrBundleDelete,
               "mfr delete",
               "Delete MFR\n",
               DEF_SDKPARAM("mfrIdList", "MfrIdList", ""),
               "${mfrIdList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMfrLinkAdd,
               "mfr link add",
               "Add link to MFR\n",
               DEF_SDKPARAM("mfrId", "UINT", "")
               DEF_SDKPARAM("frList", "FrIdList", ""),
               "${mfrId} ${frList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMfrLinkRemove,
               "mfr link remove",
               "Remove link from MFR\n",
               DEF_SDKPARAM("mfrId", "UINT", "")
               DEF_SDKPARAM("frList", "FrIdList", ""),
               "${mfrId} ${frList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMfrVirtualCircuitOamEgressVlanAdd,
               "mfr virtualcircuit oamegress vlan add",
               "MFR Virtual Circuit add oam egress vlan\n",
               DEF_SDKPARAM("vcIdList", "VcIdList", "")
               DEF_SDKPARAM("portId", "PortId", "")
               DEF_SDKPARAM("cVlan", "VlanDesc", "")
               DEF_SDKPARAM("sVlan", "VlanDesc", ""),
               "${vcIdList} ${portId} ${cVlan} ${sVlan}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMfrVirtualCircuitOamEgressVlanRemove,
               "mfr virtualcircuit oamegress vlan remove",
               "MFR Virtual Circuit remove oam egress vlan\n",
               DEF_SDKPARAM("vcIdList", "VcIdList", "")
               DEF_SDKPARAM("portId", "PortId", "")
               DEF_SDKPARAM("cVlan", "VlanDesc", "")
               DEF_SDKPARAM("sVlan", "VlanDesc", ""),
               "${vcIdList} ${portId} ${cVlan} ${sVlan}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMfrVirtualCircuitOamIngressVlanAdd,
               "mfr virtualcircuit oamingress vlan add",
               "MFR Virtual Circuit add oam ingress vlan\n",
               DEF_SDKPARAM("vcIdList", "VcIdList", "")
               DEF_SDKPARAM("portId", "PortId", "")
               DEF_SDKPARAM("cVlan", "VlanDesc", "")
               DEF_SDKPARAM("sVlan", "VlanDesc", ""),
               "${vcIdList} ${portId} ${cVlan} ${sVlan}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMfrVirtualCircuitOamIngressVlanRemove,
               "mfr virtualcircuit oamingress vlan remove",
               "MFR Virtual Circuit remove oam ingress vlan\n",
               DEF_SDKPARAM("vcIdList", "VcIdList", "")
               DEF_SDKPARAM("portId", "PortId", "")
               DEF_SDKPARAM("cVlan", "VlanDesc", "")
               DEF_SDKPARAM("sVlan", "VlanDesc", ""),
               "${vcIdList} ${portId} ${cVlan} ${sVlan}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMfrVirtualCircuitOamShow,
               "show mfr virtualcircuit oamflow",
               "Show MFR Virtual Circuit oam flow\n",
               DEF_SDKPARAM("vcIdList", "VcIdList", ""),
               "${vcIdList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMfrOamEgressVlanAdd,
               "mfr oamegress vlan add",
               "MFR add oam egress vlan\n",
               DEF_SDKPARAM("mfrIdList", "MfrIdList", "")
               DEF_SDKPARAM("portId", "PortId", "")
               DEF_SDKPARAM("cVlan", "VlanDesc", "")
               DEF_SDKPARAM("sVlan", "VlanDesc", ""),
               "${mfrIdList} ${portId} ${cVlan} ${sVlan}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMfrOamEgressVlanRemove,
               "mfr oamegress vlan remove",
               "MFR remove oam egress vlan\n",
               DEF_SDKPARAM("mfrIdList", "MfrIdList", "")
               DEF_SDKPARAM("portId", "PortId", "")
               DEF_SDKPARAM("cVlan", "VlanDesc", "")
               DEF_SDKPARAM("sVlan", "VlanDesc", ""),
               "${mfrIdList} ${portId} ${cVlan} ${sVlan}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMfrOamIngressVlanAdd,
               "mfr oamingress vlan add",
               "MFR add oam ingress vlan\n",
               DEF_SDKPARAM("mfrIdList", "MfrIdList", "")
               DEF_SDKPARAM("portId", "PortId", "")
               DEF_SDKPARAM("cVlan", "VlanDesc", "")
               DEF_SDKPARAM("sVlan", "VlanDesc", ""),
               "${mfrIdList} ${portId} ${cVlan} ${sVlan}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMfrOamIngressVlanRemove,
               "mfr oamingress vlan remove",
               "MFR remove oam ingress vlan\n",
               DEF_SDKPARAM("mfrIdList", "MfrIdList", "")
               DEF_SDKPARAM("portId", "PortId", "")
               DEF_SDKPARAM("cVlan", "VlanDesc", "")
               DEF_SDKPARAM("sVlan", "VlanDesc", ""),
               "${mfrIdList} ${portId} ${cVlan} ${sVlan}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMfrOamShow,
               "show mfr oamflow",
               "Show MFR oam flow\n",
               DEF_SDKPARAM("mfrIdList", "MfrIdList", ""),
               "${mfrIdList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMfrBundleShow,
               "show mfr",
               "Show MFR\n",
               DEF_SDKPARAM("mfrIdList", "MfrIdList", ""),
               "${mfrIdList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliFfrBundleShowLink,
             "show mfr link",
             "Show links in bundle.\n",
             DEF_SDKPARAM("bundleList", "MfrIdList", ""),
             "${bundleList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliFfrBundleShowVirtualCircuit,
             "show mfr virtualcircuit",
             "Show virtualcircuit of bundle.\n",
             DEF_SDKPARAM("bundleList", "MfrIdList", ""),
             "${bundleList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMfrBundleCountersShow,
               "show mfr counters",
               "Show MFR counters\n",
               DEF_SDKPARAM("mfrIdList", "MfrIdList", "")
               DEF_SDKPARAM("counterReadMode", "HistoryReadingMode", ""),
               "${mfrIdList} ${counterReadMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMfrFragmentFormat,
               "mfr fragment format",
               "Set fragmentation format\n",
               DEF_SDKPARAM("mfrIdList", "MfrIdList", "")
               DEF_SDKPARAM("format", "MfrFragmentFormat", ""),
               "${mfrIdList} ${format}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMfrSchedulingMode,
               "mfr scheduling",
               "Set scheduling mode\n",
               DEF_SDKPARAM("mfrIdList", "MfrIdList", "")
               DEF_SDKPARAM("mode", "eAtMfrSchedulingMode", ""),
               "${mfrIdList} ${mode}")
    {
    mAtCliCall();
    }


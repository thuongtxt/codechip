/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : ClishAtFrLink.c
 *
 * Created Date: Apr 27, 2016
 *
 * Description : Frame relay link
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

TOP_SDKCOMMAND("fr", "Frame Relay CLIs")
TOP_SDKCOMMAND("fr virtualcircuit", "Frame Relay Virtual Circuit CLIs")
TOP_SDKCOMMAND("fr virtualcircuit oamingress", "Frame Relay Virtual Circuit ingress oam CLIs")
TOP_SDKCOMMAND("fr virtualcircuit oamegress", "Frame Relay Virtual Circuit egress oam CLIs")
TOP_SDKCOMMAND("fr virtualcircuit oamingress vlan", "Frame Relay Virtual Circuit ingress vlan CLIs")
TOP_SDKCOMMAND("fr virtualcircuit oamegress vlan", "Frame Relay Virtual Circuit egress vlan CLIs")
TOP_SDKCOMMAND("fr tx", "Frame Relay Transmit CLIs")

DEF_PTYPE("FrIdList", "regexp", ".*", "Fr ID List. Example: 1-2,5-6")
DEF_PTYPE("DlciList", "regexp", ".*", "DLCI ID List. Example: 1-2,5-6")
DEF_PTYPE("VcIdList", "regexp", ".*",
          "vcIds. Format as following: <Id>.<dlci>")
DEF_PTYPE("FrEncapType", "select", "snap(0), nlpid(1), cisco(2)", "FR encapsulation type")

DEF_SDKCOMMAND(cliFrVirtualCircuitCreate,
               "fr virtualcircuit create",
               "Create Frame Relay virtual circuit channel\n",
               DEF_SDKPARAM("frId", "UINT", "Fr ID")
               DEF_SDKPARAM("dlciList", "DlciList", ""),
               "${frId} ${dlciList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliFrVirtualCircuitDelete,
               "fr virtualcircuit delete",
               "Delete Frame Relay virtual circuit channel\n",
               DEF_SDKPARAM("frId", "UINT", "Fr ID")
               DEF_SDKPARAM("dlciList", "DlciList", ""),
               "${frId} ${dlciList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliFrVirtualCircuitFlowBind,
               "fr virtualcircuit flowbind",
               "Bind flow and Frame Relay virtual circuit\n",
               DEF_SDKPARAM("vcList", "VcIdList", "")
               DEF_SDKPARAM("flowList", "FlowIdList", ""),
               "${vcList} ${flowList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliFrLinkShow,
               "show fr",
               "Show Frame Relay information\n",
               DEF_SDKPARAM("frIdList", "FrIdList", ""),
               "${frIdList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliFrLinkShowVirtualCircuits,
               "show fr virtualcircuit",
               "Show Frame Relay virtual circuits information\n",
               DEF_SDKPARAM("frIdList", "FrIdList", ""),
               "${frIdList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliFrCountersShow,
               "show fr counters",
               "Show Frame Relay Counters\n",
               DEF_SDKPARAM("frIdList", "FrIdList", "")
               DEF_SDKPARAM("counterReadMode", "HistoryReadingMode", ""),
               "${frIdList} ${counterReadMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliFrVirtualcircuitCountersShow,
               "show fr virtualcircuit counters",
               "Show Frame Relay virtualcircuit counters\n",
               DEF_SDKPARAM("vcList", "VcIdList", "Virtual circuit ID list")
               DEF_SDKPARAM("counterReadMode", "HistoryReadingMode", ""),
               "${vcList} ${counterReadMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliFrVirtualCircuitOamEgressVlanAdd,
               "fr virtualcircuit oamegress vlan add",
               "Frame Relay Virtual Circuit add oam egress vlan\n",
               DEF_SDKPARAM("vcIdList", "VcIdList", "")
               DEF_SDKPARAM("portId", "PortId", "")
               DEF_SDKPARAM("cVlan", "VlanDesc", "")
               DEF_SDKPARAM("sVlan", "VlanDesc", ""),
               "${vcIdList} ${portId} ${cVlan} ${sVlan}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliFrVirtualCircuitOamEgressVlanRemove,
               "fr virtualcircuit oamegress vlan remove",
               "Frame Relay Virtual Circuit remove oam egress vlan\n",
               DEF_SDKPARAM("vcIdList", "VcIdList", "")
               DEF_SDKPARAM("portId", "PortId", "")
               DEF_SDKPARAM("cVlan", "VlanDesc", "")
               DEF_SDKPARAM("sVlan", "VlanDesc", ""),
               "${vcIdList} ${portId} ${cVlan} ${sVlan}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliFrVirtualCircuitOamIngressVlanAdd,
               "fr virtualcircuit oamingress vlan add",
               "Frame Relay Virtual Circuit add oam ingress vlan\n",
               DEF_SDKPARAM("vcIdList", "VcIdList", "")
               DEF_SDKPARAM("portId", "PortId", "")
               DEF_SDKPARAM("cVlan", "VlanDesc", "")
               DEF_SDKPARAM("sVlan", "VlanDesc", ""),
               "${vcIdList} ${portId} ${cVlan} ${sVlan}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliFrVirtualCircuitOamIngressVlanRemove,
               "fr virtualcircuit oamingress vlan remove",
               "Frame Relay Virtual Circuit remove oam ingress vlan\n",
               DEF_SDKPARAM("vcIdList", "VcIdList", "")
               DEF_SDKPARAM("portId", "PortId", "")
               DEF_SDKPARAM("cVlan", "VlanDesc", "")
               DEF_SDKPARAM("sVlan", "VlanDesc", ""),
               "${vcIdList} ${portId} ${cVlan} ${sVlan}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliFrVirtualCircuitOamShow,
               "show fr virtualcircuit oamflow",
               "Show Frame Relay Virtual Circuit oam flow\n",
               DEF_SDKPARAM("vcIdList", "VcIdList", ""),
               "${vcIdList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliFrEncapType,
               "fr encaptype",
               "Configure FR link encapsulation type\n",
               DEF_SDKPARAM("frIdList", "FrIdList", "")
               DEF_SDKPARAM("encapType", "FrEncapType", ""),
               "${frIdList} ${encapType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliFrTxCr,
               "fr tx cr",
               "Configure FR link Command/Respond Bit\n",
               DEF_SDKPARAM("frIdList", "FrIdList", "")
               DEF_SDKPARAM("cr", "UINT", "C/R Bit"),
               "${frIdList} ${cr}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliFrFECNSet,
               "fr tx fecn",
               "Set FECN bit of Q922 address at transmit directions\n",
               DEF_SDKPARAM("frIdList", "FrIdList", "")
               DEF_SDKPARAM("value", "UINT", ""),
               "${frIdList} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliFrTxDc,
               "fr tx dc",
               "Configure FR link DLCI/DL-CORE control indicator Bit\n",
               DEF_SDKPARAM("frIdList", "FrIdList", "")
               DEF_SDKPARAM("dc", "UINT", "D/C Bit"),
               "${frIdList} ${dc}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliFrBECNSet,
               "fr tx becn",
               "Set BECN bit of Q922 address at transmit directions\n",
               DEF_SDKPARAM("frIdList", "FrIdList", "")
               DEF_SDKPARAM("value", "UINT", ""),
               "${frIdList} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliFrDESet,
               "fr tx de",
               "Set DE bit of Q922 address at transmit directions\n",
               DEF_SDKPARAM("frIdList", "FrIdList", "")
               DEF_SDKPARAM("value", "UINT", ""),
               "${frIdList} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliFrDlCoreSet,
               "fr tx dlcore",
               "Set DL-Core bit of Q922 address at transmit directions\n",
               DEF_SDKPARAM("frIdList", "FrIdList", "")
               DEF_SDKPARAM("value", "UINT", ""),
               "${frIdList} ${value}")
    {
    mAtCliCall();
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLISH
 *
 * File        : ClishAtSerdesController.c
 *
 * Created Date: Jul 1, 2016
 *
 * Description : SERDES controller CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "../AtClish.h"
#include "../physical/ClishAtEyeScan.h"
#include "../../physical/CliAtEyeScan.h"
#include "../../eth/CliAtModuleEth.h"
#include "ClishAtEyeScan.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
DEF_PTYPE("SerdesIdList", "regexp", ".*", "SERDES ID")
DEF_PTYPE("eAtSerdesMode", "select", "stm0(0), stm1(1), stm4(2), stm16(3), stm64(4), 10M(5), 100M(6), 2500M(7), 1G(8), 10G(9)", "\nSERDES mode\n")
DEF_PTYPE("SerdesEyeScanLanes", "regexp", ".*", "List of eye scan lanes, format <serdesId>.<laneId>")
DEF_PTYPE("eAtSerdesPhysicalLayer", "select", "pcs(1), pma(2)", "Serdes layer")
DEF_PTYPE("eAtSerdesTimingMode", "select", "auto(0), lock2data(1), lock2ref(2)", "Serdes timing mode")
DEF_PTYPE("eAtSerdesPrbsEngineType", "select", "framed(0), raw(1)", "Serdes prbs type")

TOP_SDKCOMMAND("serdes", "CLIs to control SERDES")
TOP_SDKCOMMAND("serdes rx", "CLIs to control rx SERDES")
TOP_SDKCOMMAND("serdes tx", "CLIs to control tx SERDES")
TOP_SDKCOMMAND("serdes equalizer", "CLIs to control SERDES equalizer")
TOP_SDKCOMMAND("serdes notification", "CLIs to control SERDES notification")
TOP_SDKCOMMAND("serdes prbs", "CLIs to control SERDES PRBS")
TOP_SDKCOMMAND("serdes drp", "SERDES eye scan DRP CLIs\n")
TOP_SDKCOMMAND("serdes eyescan stepsize", "SERDES eye scan Step size\n")
TOP_SDKCOMMAND("serdes timing", "CLIs to control SERDES timing\n")
TOP_SDKCOMMAND("show serdes prbs alarm", "Show serdes prbs engine alarm\n")
TOP_SDKCOMMAND("serdes threshold", "CLIs to control SERDES thresholds")
TOP_SDKCOMMAND("serdes threshold ppm", "CLIs to control SERDES PPM thresholds")

DEF_SDKCOMMAND(cliAtSerdesControllerModeSet,
               "serdes mode",
               "",
               DEF_SDKPARAM("serdesList", "SerdesIdList", "")
               DEF_SDKPARAM("mode", "eAtSerdesMode", ""),
               "${serdesList} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerInit,
               "serdes init",
               "Initialize SERDES",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerEnable,
               "serdes enable",
               "Enable SERDES",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerDisable,
               "serdes disable",
               "Disable SERDES",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerEqualizerMode,
               "serdes equalizer mode",
               "Set SERDES equalizer mode",
               DEF_SDKPARAM("serdesList", "SerdesIdList", "")
               DEF_SDKPARAM("mode", "eAtSerdesEqualizerMode", ""),
               "${serdesList} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesIntrMsk,
               "serdes interruptmask",
               "Set Ethernet SERDES interrupt mask\n",
               DEF_SDKPARAM("serdesList", "SerdesIdList", "")
               DEF_SDKPARAM("mask", "SerdesDefMask", "")
               DEF_SDKPARAM("intrMaskEn", "eBool", ""),
               "${serdesList} ${mask} ${intrMaskEn}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesLoopback,
               "serdes loopback",
               "Loopback SERDES",
               DEF_SDKPARAM("serdesList", "SerdesIdList", "")
               DEF_SDKPARAM("loopbackMode", "eAtLoopbackMode", ""),
               "${serdesList} ${loopbackMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesNotificationEnable,
               "serdes notification enable",
               "Enable SERDES alarm change notification",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesNotificationDisable,
               "serdes notification disable",
               "Disable SERDES alarm change notification",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesPhyParamSet,
               "serdes param",
               "Change SERDES physical parameter",
               DEF_SDKPARAM("serdesList", "SerdesIdList", "")
               DEF_SDKPARAM("paramId", "eAtSerdesParam", "")
               DEF_SDKPARAM("value", "STRING", "Value"),
               "${serdesList} ${paramId} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerPowerUp,
               "serdes powerup",
               "Power up SERDES",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerPowerDown,
               "serdes powerdown",
               "Power down SERDES",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerReset,
               "serdes reset",
               "Reset SERDES",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerRxReset,
               "serdes rx reset",
               "Reset RX SERDES",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerTxReset,
               "serdes tx reset",
               "Reset TX SERDES",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesPrbsEngineSerdesEthPortModeSet,
               "serdes prbs mode",
               "Set PRBS mode",
               DEF_SDKPARAM("serdesList", "SerdesIdList", "")
               DEF_SDKPARAM("mode", "eAtPrbsMode", "")
               DEF_SDKPARAM_OPTION("direction", "eAtDirection", ""),
               "${serdesList} ${mode} ${direction}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesPrbsEngineSerdesEthPortInvertEnable,
               "serdes prbs invert",
               "PRBS invert",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesPrbsEngineSerdesEthPortInvertDisable,
               "serdes prbs noinvert",
               "PRBS no invert",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesPrbsEngineSerdesEthPortErrorForce,
               "serdes prbs force",
               "Force PRBS error",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesPrbsEngineSerdesEthPortErrorUnForce,
               "serdes prbs unforce",
               "Unforce PRBS error",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesPrbsEngineSerdesEthPortEnable,
               "serdes prbs enable",
               "Enable PRBS engine",
               DEF_SDKPARAM("serdesList", "SerdesIdList", "")
               DEF_SDKPARAM_OPTION("direction", "eAtDirection", ""),
               "${serdesList} ${direction}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesPrbsEngineSerdesEthPortDisable,
               "serdes prbs disable",
               "Disable PRBS engine",
               DEF_SDKPARAM("serdesList", "SerdesIdList", "")
               DEF_SDKPARAM_OPTION("direction", "eAtDirection", ""),
               "${serdesList} ${direction}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerShow,
               "show serdes",
               "",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerPrbsEngineShow,
               "show serdes prbs",
               "Show PRBS engines",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerPrbsEngineCountersShow,
               "show serdes prbs counters",
               "Show PRBS engines counters",
               DEF_SDKPARAM("serdesList", "SerdesIdList", "")
               DEF_SDKPARAM("counterReadMode", "HistoryReadingMode", "read only/read to clear mode")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${serdesList} ${counterReadMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerAlarmShow,
               "show serdes alarm",
               "Show SERDES alarm",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerAlarmHistoryShow,
               "show serdes alarm history",
               "Show SERDES history alarm changed.",
               DEF_SDKPARAM("serdesList", "SerdesIdList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", ""),
               "${serdesList} ${readingMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerParamShow,
               "show serdes param",
               "Show value of specified SERDES parameter",
               DEF_SDKPARAM("serdesList", "SerdesIdList", "")
               DEF_SDKPARAM_OPTION("paramId", "eAtSerdesParam", ""),
               "${serdesList} ${paramId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerEyeScanLaneDrpRead,
               "serdes drp rd",
               "Read DRP register",
               DEF_SDKPARAM("serdes.laneId", "STRING", "Lane ID")
               DEF_SDKPARAM("drpAddress", "RegisterAddress", "")
               DEF_SDKPARAM_OPTION("pretty", "RegisterDisplayFormat", ""),
               "${serdes.laneId} ${drpAddress} ${pretty}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerEyeScanLaneDrpWrite,
               "serdes drp wr",
               "Write DRP register",
               DEF_SDKPARAM("serdes.laneId", "STRING", "")
               DEF_SDKPARAM("drpAddress", "RegisterAddress", "")
               DEF_SDKPARAM("value", "RegisterValue", ""),
               "${serdes.laneId} ${drpAddress} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerEyescanEnable,
               "serdes eyescan enable",
               "Enable eyescan SERDES",
               DEF_SDKPARAM("lanes", "SerdesEyeScanLanes", ""),
               "${lanes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerEyescanDisable,
               "serdes eyescan disable",
               "Disable eyescan SERDES",
               DEF_SDKPARAM("lanes", "SerdesEyeScanLanes", ""),
               "${lanes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerEyeScanLaneStepSizeHorizonalSet,
               "serdes eyescan stepsize horizonal",
               "Change horizontal step",
               DEF_SDKPARAM("lanes", "SerdesEyeScanLanes", "")
               DEF_SDKPARAM("stepSize", "UINT", "Horizontal step size"),
               "${lanes} ${stepSize}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerEyeScanLaneStepSizeVerticalSet,
               "serdes eyescan stepsize vertical",
               "Change vertical step",
               DEF_SDKPARAM("lanes", "SerdesEyeScanLanes", "")
               DEF_SDKPARAM("stepSize", "UINT", "Vertical step size"),
               "${lanes} ${stepSize}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerEyeScanLaneMaxPrescaleSet,
               "serdes eyescan maxPrescale",
               "Change max Prescale",
               DEF_SDKPARAM("lanes", "SerdesEyeScanLanes", "")
               DEF_SDKPARAM("maxPrescale", "UINT", "Max prescale"),
               "${lanes} ${maxPrescale}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerEyeScanLaneDatawidthSet,
               "serdes eyescan datawidth",
               "Change data width",
               DEF_SDKPARAM("lanes", "SerdesEyeScanLanes", "")
               DEF_SDKPARAM("dataWidth", "UINT", "Data width"),
               "${lanes} ${dataWidth}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerEyeScanLaneDebugEnable,
               "serdes eyescan debug enable",
               "Enable eye-scan internal state machine debug",
               DEF_SDKPARAM("lanes", "SerdesEyeScanLanes", ""),
               "${lanes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerEyeScanLaneDebugDisable,
               "serdes eyescan debug disable",
               "Disable eye-scan internal state machine debug",
               DEF_SDKPARAM("lanes", "SerdesEyeScanLanes", ""),
               "${lanes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerEyeScanLaneShow,
               "show serdes eyescan",
               "Show Eye scanning information",
               DEF_SDKPARAM("lanes", "SerdesEyeScanLanes", ""),
               "${lanes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerEyeScanLaneReset,
               "serdes eyescan reset",
               "Reset Eyescan",
               DEF_SDKPARAM("lanes", "SerdesEyeScanLanes", ""),
               "${lanes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerEyescanDebug,
               "serdes eyescan debug",
               "Debug Eyescan SERDES",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerEyescan,
               "serdes eyescan",
               "Perform eye scan for a list of SERDE",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerPrbsEngineStart,
               "serdes prbs start",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerPrbsEngineStop,
               "serdes prbs stop",
               "Stop PRBS engine.\n",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerPrbsEngineDurationSet,
               "serdes prbs duration",
               "Set duration\n",
               DEF_SDKPARAM("serdesList", "SerdesIdList", "")
               DEF_SDKPARAM("duration", "UINT", " Duration in millisecond"),
               "${serdesList} {duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtShowSerdesManager,
               "show serdes manager",
               "Show list of SERDER",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerPrbsGatingShow,
               "show serdes prbs gating",
               "Show SERDES PRBS gating\n",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerTimingModeSet,
               "serdes timing mode",
               "Show SERDES PRBS gating\n",
               DEF_SDKPARAM("serdesList", "SerdesIdList", "")
               DEF_SDKPARAM("timingMode", "eAtSerdesTimingMode", "Serdes timing mode"),
               "${serdesList} ${timingMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtShowSerdesPrbsEngineAlarmHistory,
               "show serdes prbs alarm history",
               "Show SERDER prbs engine alarm history",
               DEF_SDKPARAM("serdesList", "SerdesIdList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "read only/read to clear mode")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${serdesList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerAutoResetPpmThresholdSet,
               "serdes threshold ppm autoreset",
               "Configure SERDES PPM auto reset threshold",
               DEF_SDKPARAM("serdesList", "SerdesIdList", "")
               DEF_SDKPARAM("threshold", "STRING", "PPM auto reset threshold"),
               "${serdesList} ${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerThresholdShow,
               "show serdes threshold",
               "Show SERDES threshold",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerPrbsEngineTypeSet,
               "serdes prbs type",
               "Set SERDES prbs engine type: framed or raw",
               DEF_SDKPARAM("serdesList", "SerdesIdList", "")
               DEF_SDKPARAM("type", "eAtSerdesPrbsEngineType", ""),
               "${serdesList} ${type}")
    {
    mAtCliCall();
    }

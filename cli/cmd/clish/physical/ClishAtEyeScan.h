/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLISH
 * 
 * File        : ClishAtEyeScan.h
 * 
 * Created Date: Mar 2, 2015
 *
 * Description : Eye scan
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLISHATEYESCAN_H_
#define _CLISHATEYESCAN_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void ClishEnableEyeScanTask(void);

#ifdef __cplusplus
}
#endif
#endif /* _CLISHATEYESCAN_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLISH
 *
 * File        : ClishAtEyeScan.c
 *
 * Created Date: Mar 2, 2015
 *
 * Description : Eye scan CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ClishAtEyeScan.h"
#include "../../physical/CliAtEyeScan.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
extern void AppEyeScanTaskEnable(eBool enable);

/*--------------------------- Implementation ---------------------------------*/
void ClishEnableEyeScanTask(void)
    {
    CliAtEyeScanControllerSilentScan(cAtTrue);
    AppEyeScanTaskEnable(cAtTrue);
    }

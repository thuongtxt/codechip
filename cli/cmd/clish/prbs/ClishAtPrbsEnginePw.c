/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS PW
 *
 * File        : ClishAtPrbsEnginePw.c
 *
 * Created Date: Mar 12, 2015
 *
 * Description : PRBS PW CLISH
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("pw prbs", "\nPDH PW PRBS commands\n")

DEF_SDKCOMMAND(cliAtPrbsEnginePwModeSet,
               "pw prbs mode",
               "Set PRBS mode",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("mode", "eAtPrbsMode", ""),
               "${pwList} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEnginePwEnable,
               "pw prbs enable",
               "Enable PRBS engine",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEnginePwDisable,
               "pw prbs disable",
               "Disable PRBS engine",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEnginePwShow,
               "show pw prbs",
               "Show PRBS engines",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", "Read to clear without display table"),
               "${pwList} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEnginePwSideSet,
               "pw prbs source",
               "Set PRBS source",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("side", "eAtPrbsSide", ""),
               "${pwList} ${side}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEnginePwErrorForce,
               "pw prbs force",
               "Force PRBS error",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEnginePwErrorUnForce,
               "pw prbs unforce",
               "Unforce PRBS error",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEnginePwCounterGet,
               "show pw prbs counters",
               "Show PRBS engines",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("counterReadMode", "HistoryReadingMode", "read only/read to clear mode")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${pwList} ${counterReadMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEnginePwErrorInject,
               "pw prbs force single",
               "Force single error",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM_OPTION("numForcedErrors", "STRING", "Number of forced errors"),
               "${pwList} ${numForcedErrors}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEnginePwStart,
               "pw prbs start",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEnginePwStop,
               "pw prbs stop",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEnginePwDurationSet,
               "pw prbs duration",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("duration", "UINT", " Duration in miliscond"),
               "${pwList} {duration}")
    {
    mAtCliCall();
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLISH
 *
 * File        : ClishAtPrbsEngineSerdesEthPort.c
 *
 * Created Date: Aug 24, 2013
 *
 * Description : PRBS CLISH
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("eth port serdes prbs", "\nETH Port SERDES PRBS CLIs\n")

DEF_SDKCOMMAND(cliAtPrbsEngineSerdesEthPortModeSet,
               "eth port serdes prbs mode",
               "Set PRBS mode",
               DEF_SDKPARAM("ports", "PortIdList", "")
               DEF_SDKPARAM("mode", "eAtPrbsMode", "")
               DEF_SDKPARAM_OPTION("direction", "eAtDirection", ""),
               "${ports} ${mode} ${direction}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineSerdesEthPortInvertEnable,
               "eth port serdes prbs invert",
               "PRBS invert",
               DEF_SDKPARAM("ports", "PortIdList", ""),
               "${ports}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineSerdesEthPortInvertDisable,
               "eth port serdes prbs noinvert",
               "PRBS no invert",
               DEF_SDKPARAM("ports", "PortIdList", ""),
               "${ports}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineSerdesEthPortErrorForce,
               "eth port serdes prbs force",
               "Force PRBS error",
               DEF_SDKPARAM("ports", "PortIdList", ""),
               "${ports}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineSerdesEthPortErrorUnForce,
               "eth port serdes prbs unforce",
               "Unforce PRBS error",
               DEF_SDKPARAM("ports", "PortIdList", ""),
               "${ports}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineSerdesEthPortEnable,
               "eth port serdes prbs enable",
               "Enable PRBS engine",
               DEF_SDKPARAM("ports", "PortIdList", "")
               DEF_SDKPARAM_OPTION("direction", "eAtDirection", ""),
               "${ports} ${direction}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineSerdesEthPortDisable,
               "eth port serdes prbs disable",
               "Disable PRBS engine",
               DEF_SDKPARAM("ports", "PortIdList", "")
               DEF_SDKPARAM_OPTION("direction", "eAtDirection", ""),
               "${ports} ${direction}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineSerdesEthPortShow,
               "show eth port serdes prbs",
               "Show PRBS engines",
               DEF_SDKPARAM("ports", "PortIdList", ""),
               "${ports}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineSerdesEthPortCounterShow,
               "show eth port serdes prbs counters",
               "Show PRBS engines counters",
               DEF_SDKPARAM("ports", "PortIdList", "")
               DEF_SDKPARAM("counterReadMode", "HistoryReadingMode", "read only/read to clear mode")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${ports} ${counterReadMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineSerdesEthPortDebug,
             "debug eth port serdes prbs",
             "Show all debug information of Eth Prbs engine",
             DEF_SDKPARAM("ports", "PortIdList", ""),
             "${ports}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineSerdesEthPortStart,
               "eth port serdes prbs start",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("ports", "PortIdList", ""),
               "${ports}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineSerdesEthPortStop,
               "eth port serdes prbs stop",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("ports", "PortIdList", ""),
               "${ports}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineSerdesEthPortDurationSet,
               "eth port serdes prbs duration",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("ports", "PortIdList", "")
               DEF_SDKPARAM("duration", "UINT", " Duration in miliscond"),
               "${ports} {duration}")
    {
    mAtCliCall();
    }

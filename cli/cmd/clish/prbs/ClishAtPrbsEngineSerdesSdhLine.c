/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLISH
 *
 * File        : ClishAtPrbsEngineSerdesSdhLine.c
 *
 * Created Date: Aug 24, 2013
 *
 * Description : PRBS CLISH
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("sdh line serdes prbs", "\nSDH Line SERDES PRBS CLIs\n")

DEF_SDKCOMMAND(cliAtPrbsEngineSerdesSdhLineModeSet,
               "sdh line serdes prbs mode",
               "Set PRBS mode",
               DEF_SDKPARAM("lines", "SdhLineList", "")
               DEF_SDKPARAM("mode", "eAtPrbsMode", ""),
               "${pathList} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineSerdesSdhLineInvertEnable,
               "sdh line serdes prbs invert",
               "PRBS invert",
               DEF_SDKPARAM("lines", "SdhLineList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineSerdesSdhLineInvertDisable,
               "sdh line serdes prbs noinvert",
               "PRBS no invert",
               DEF_SDKPARAM("lines", "SdhLineList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineSerdesSdhLineErrorForce,
               "sdh line serdes prbs force",
               "Force PRBS error",
               DEF_SDKPARAM("lines", "SdhLineList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineSerdesSdhLineErrorUnForce,
               "sdh line serdes prbs unforce",
               "Unforce PRBS error",
               DEF_SDKPARAM("lines", "SdhLineList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineSerdesSdhLineEnable,
               "sdh line serdes prbs enable",
               "Enable PRBS engine",
               DEF_SDKPARAM("lines", "SdhLineList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineSerdesSdhLineDisable,
               "sdh line serdes prbs disable",
               "Disable PRBS engine",
               DEF_SDKPARAM("lines", "SdhLineList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineSerdesSdhLineShow,
               "show sdh line serdes prbs",
               "Show PRBS engines",
               DEF_SDKPARAM("lines", "SdhLineList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineSerdesSdhLineStart,
               "sdh line serdes prbs start",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("lines", "SdhLineList", ""),
               "${lines}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineSerdesSdhLineStop,
               "sdh line serdes prbs stop",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("lines", "SdhLineList", ""),
               "${lines}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineSerdesSdhLineDurationSet,
               "sdh line serdes prbs duration",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("lines", "SdhLineList", "")
               DEF_SDKPARAM("duration", "UINT", " Duration in miliscond"),
               "${lines} {duration}")
    {
    mAtCliCall();
    }

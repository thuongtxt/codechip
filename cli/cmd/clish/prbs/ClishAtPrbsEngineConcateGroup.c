/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLISH
 *
 * File        : ClishAtPrbsEngineConcateGroup.c
 *
 * Created Date: Aug 10, 2016
 *
 * Description : VCG PRBS CLISH
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("vcg prbs", "\nPDH VCG PRBS commands\n")
TOP_SDKCOMMAND("vcg prbs error", "\nPDH VCG PRBS Error handle commands\n")

DEF_SDKCOMMAND(CliAtModulePrbsEngineConcateGroupCreate,
               "prbs engine create vcg",
               "Create VCG PRBS engine",
               DEF_SDKPARAM("vcgs", "VcgList", ""),
               "${vcgs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtModulePrbsEngineConcateGroupDelete,
               "prbs engine delete vcg",
               "Create VCG PRBS engine",
               DEF_SDKPARAM("vcgs", "VcgList", ""),
               "${vcgs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineConcateGroupModeSet,
               "vcg prbs mode",
               "Set PRBS mode",
               DEF_SDKPARAM("vcgs", "VcgList", "")
               DEF_SDKPARAM("mode", "eAtPrbsMode", ""),
               "${vcgs} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineConcateGroupEnable,
               "vcg prbs enable",
               "Enable PRBS engine",
               DEF_SDKPARAM("vcgs", "VcgList", ""),
               "${vcgs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineConcateGroupDisable,
               "vcg prbs disable",
               "Disable PRBS engine",
               DEF_SDKPARAM("vcgs", "VcgList", ""),
               "${vcgs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineConcateGroupShow,
               "show vcg prbs",
               "Show PRBS engines",
               DEF_SDKPARAM("vcgs", "VcgList", ""),
               "${vcgs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineConcateGroupCounterGet,
               "show vcg prbs counters",
               "Show PRBS counter",
               DEF_SDKPARAM("vcgs", "VcgList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${vcgs} ${readingMode} {silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineVcgInvertEnable,
               "vcg prbs invert",
               "PRBS invert",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineVcgInvertDisable,
               "vcg prbs noinvert",
               "PRBS no invert",
               DEF_SDKPARAM("vcgs", "VcgList", ""),
               "${vcgs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineVcgErrorForce,
               "vcg prbs error force",
               "Force PRBS error",
               DEF_SDKPARAM("vcgs", "VcgList", ""),
               "${vcgs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineVcgErrorUnForce,
               "vcg prbs error unforce",
               "Unforce PRBS error",
               DEF_SDKPARAM("vcgs", "VcgList", ""),
               "${vcgs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineVcgForceBer,
               "vcg prbs ber",
               "Force Bit-error-rate for PRBS engines",
               DEF_SDKPARAM("vcgs", "VcgList", "")
               DEF_SDKPARAM("ber", "ePrbsBer", ""),
               "${vcgs} {ber}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineVcgBerShow,
               "show vcg prbs ber",
               "Show BER of PRBS engines",
               DEF_SDKPARAM("vcgs", "VcgList", ""),
               "${vcgs}")
    {
    mAtCliCall();
    }

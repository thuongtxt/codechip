/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLISH
 *
 * File        : ClishAtPrbsEngineNxDs0.c
 *
 * Created Date: Aug 24, 2013
 *
 * Description : PRBS CLISH
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("pdh nxds0", "\nPDH NxDS0 commands\n")
TOP_SDKCOMMAND("pdh nxds0 prbs", "\nPDH NxDS0 PRBS commands\n")
TOP_SDKCOMMAND("pdh nxds0 prbs transmit", "\nPDH NxDS0 PRBS Transmit commands\n")
TOP_SDKCOMMAND("pdh nxds0 prbs expect", "\nPDH NxDS0 PRBS Expect commands\n")
TOP_SDKCOMMAND("pdh nxds0 prbs delay", "\nPDH NxDS0 PRBS Delay commands\n")
TOP_SDKCOMMAND("pdh nxds0 prbs disruption", "\nPDH NxDS0 PRBS disruption commands\n")
TOP_SDKCOMMAND("show pdh nxds0", "\nShow PDH NxDS0 information\n")

DEF_SDKCOMMAND(cliAtPrbsEngineNxDs0ModeSet,
               "pdh nxds0 prbs mode",
               "Set PRBS mode",
               DEF_SDKPARAM("de1s", "PdhNxDs0De1List", "")
               DEF_SDKPARAM("nxds0List", "PdhNxDs0List", "")
               DEF_SDKPARAM("mode", "eAtPrbsMode", ""),
               "${de1s} ${nxds0List} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineNxDs0TransmitModeSet,
               "pdh nxds0 prbs transmit mode",
               "Set PRBS mode",
               DEF_SDKPARAM("de1s", "PdhNxDs0De1List", "")
               DEF_SDKPARAM("nxds0List", "PdhNxDs0List", "")
               DEF_SDKPARAM("mode", "eAtPrbsMode", ""),
               "${de1s} ${nxds0List} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineNxDs0ExpectModeSet,
               "pdh nxds0 prbs expect mode",
               "Set PRBS mode",
               DEF_SDKPARAM("de1s", "PdhNxDs0De1List", "")
               DEF_SDKPARAM("nxds0List", "PdhNxDs0List", "")
               DEF_SDKPARAM("mode", "eAtPrbsMode", ""),
               "${de1s} ${nxds0List} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineNxDs0DataModeSet,
               "pdh nxds0 prbs datamode",
               "Set PRBS data mode",
               DEF_SDKPARAM("de1s", "PdhNxDs0De1List", "")
               DEF_SDKPARAM("nxds0List", "PdhNxDs0List", "")
               DEF_SDKPARAM("mode", "eAtPrbsDataMode", ""),
               "${de1s} ${nxds0List} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineNxDs0TransmitDataModeSet,
               "pdh nxds0 prbs transmitdata mode",
               "Set PRBS data mode",
               DEF_SDKPARAM("de1s", "PdhNxDs0De1List", "")
               DEF_SDKPARAM("nxds0List", "PdhNxDs0List", "")
               DEF_SDKPARAM("mode", "eAtPrbsDataMode", ""),
               "${de1s} ${nxds0List} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineNxDs0ExpectDataModeSet,
               "pdh nxds0 prbs expect datamode",
               "Set PRBS data mode",
               DEF_SDKPARAM("de1s", "PdhNxDs0De1List", "")
               DEF_SDKPARAM("nxds0List", "PdhNxDs0List", "")
               DEF_SDKPARAM("mode", "eAtPrbsDataMode", ""),
               "${de1s} ${nxds0List} ${mode}")
    {
    mAtCliCall();
    }


DEF_SDKCOMMAND(cliAtPrbsEngineNxDs0InvertEnable,
               "pdh nxds0 prbs invert",
               "PRBS invert",
               DEF_SDKPARAM("de1s", "PdhNxDs0De1List", "")
               DEF_SDKPARAM("nxds0List", "PdhNxDs0List", ""),
               "${de1s} ${nxds0List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineNxDs0InvertDisable,
               "pdh nxds0 prbs noinvert",
               "PRBS no invert",
               DEF_SDKPARAM("de1s", "PdhNxDs0De1List", "")
               DEF_SDKPARAM("nxds0List", "PdhNxDs0List", ""),
               "${de1s} ${nxds0List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineNxDs0ErrorForce,
               "pdh nxds0 prbs force",
               "Force PRBS error",
               DEF_SDKPARAM("de1s", "PdhNxDs0De1List", "")
               DEF_SDKPARAM("nxds0List", "PdhNxDs0List", ""),
               "${de1s} ${nxds0List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineNxDs0ErrorUnForce,
               "pdh nxds0 prbs unforce",
               "Unforce PRBS error",
               DEF_SDKPARAM("de1s", "PdhNxDs0De1List", "")
               DEF_SDKPARAM("nxds0List", "PdhNxDs0List", ""),
               "${de1s} ${nxds0List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineNxDs0Enable,
               "pdh nxds0 prbs enable",
               "Enable PRBS engine",
               DEF_SDKPARAM("de1s", "PdhNxDs0De1List", "")
               DEF_SDKPARAM("nxds0List", "PdhNxDs0List", ""),
               "${de1s} ${nxds0List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineNxDs0Disable,
               "pdh nxds0 prbs disable",
               "Disable PRBS engine",
               DEF_SDKPARAM("de1s", "PdhNxDs0De1List", "")
               DEF_SDKPARAM("nxds0List", "PdhNxDs0List", ""),
               "${de1s} ${nxds0List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineNxDs0Show,
               "show pdh nxds0 prbs",
               "Show PRBS engines",
               DEF_SDKPARAM("de1s", "PdhNxDs0De1List", "")
               DEF_SDKPARAM("nxds0List", "PdhNxDs0List", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", "Read to clear without display table"),
               "${de1s} ${nxds0List} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineNxDs0Side,
               "pdh nxds0 prbs side",
               "PRBS source",
               DEF_SDKPARAM("de1s", "PdhNxDs0De1List", "")
               DEF_SDKPARAM("nxds0List", "PdhNxDs0List", "")
               DEF_SDKPARAM("side", "eAtPrbsSide", ""),
               "${de1s} ${nxds0List} ${side}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineNxDs0GeneratingSideSet,
               "pdh nxds0 prbs side generating",
               "Set PRBS generating side",
               DEF_SDKPARAM("de1s", "PdhNxDs0De1List", "")
               DEF_SDKPARAM("nxds0s", "PdhNxDs0List", "")
               DEF_SDKPARAM("side", "eAtPrbsSide", ""),
               "${de1s} ${nxds0s} ${side}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineNxDs0MonitoringSideSet,
               "pdh nxds0 prbs side monitoring",
               "Set PRBS monitoring side",
               DEF_SDKPARAM("de1s", "PdhNxDs0De1List", "")
               DEF_SDKPARAM("nxds0s", "PdhNxDs0List", "")
               DEF_SDKPARAM("side", "eAtPrbsSide", ""),
               "${de1s} ${nxds0s} ${side}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineNxDs0CounterGet,
               "show pdh nxds0 prbs counters",
               "Show PRBS counter",
               DEF_SDKPARAM("de1s", "PdhNxDs0De1List", "")
               DEF_SDKPARAM("nxds0s", "PdhNxDs0List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${de1s} ${nxds0s} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineNxDs0ErrorInject,
               "pdh nxds0 prbs force single",
               "Force single error",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("nxds0List", "PdhNxDs0List", "")
               DEF_SDKPARAM_OPTION("numForcedErrors", "STRING", "Number of forced errors"),
               "${de1s} ${nxds0List} ${numForcedErrors}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineNxDs0BerShow,
               "show pdh nxds0 prbs ber",
               "Show BER of PRBS engines",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("nxds0List", "PdhNxDs0List", ""),
               "${de1s} ${nxds0List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineNxDs0ForceBer,
               "pdh nxds0 prbs force ber",
               "Force Bit-error-rate for PRBS engines",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("nxds0List", "PdhNxDs0List", "")
               DEF_SDKPARAM("ber", "ePrbsBer", ""),
               "${de1s} ${nxds0List} {ber}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineNxDs0FixPatternSet,
               "pdh nxds0 prbs fixpattern",
               "Set Transmitting and Expected fix-pattern for PRBS engines",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("nxds0List", "PdhNxDs0List", "")
               DEF_SDKPARAM("pattern", "DWORD_HEX", " Transmitting pattern"),
               "${de1s} ${nxds0List} {pattern}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineNxDs0TransmitFixPatternSet,
               "pdh nxds0 prbs transmit fixpattern",
               "Set Transmitting fix-pattern for PRBS engines",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("nxds0List", "PdhNxDs0List", "")
               DEF_SDKPARAM("trans", "DWORD_HEX", " Transmitting pattern"),
               "${de1s} ${nxds0List} {trans}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineNxDs0ExpectFixPatternSet,
               "pdh nxds0 prbs expect fixpattern",
               "Set Expected fix-pattern for PRBS engines",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("nxds0List", "PdhNxDs0List", "")
               DEF_SDKPARAM("expect", "DWORD_HEX", " Expected pattern"),
               "${de1s} ${nxds0List} {expect}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineNxDs0BitOrderSet,
               "pdh nxds0 prbs bitorder",
               "Set PRBS bit order",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("nxds0List", "PdhNxDs0List", "")
               DEF_SDKPARAM("bitOrder", "eAtPrbsBitOrder", ""),
               "${de1s} ${nxds0List} ${bitOrder}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineNxDs0Start,
               "pdh nxds0 prbs start",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("nxds0List", "PdhNxDs0List", ""),
               "${de1s} ${nxds0List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineNxDs0Stop,
               "pdh nxds0 prbs stop",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("nxds0List", "PdhNxDs0List", ""),
               "${de1s} ${nxds0List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineNxDs0DurationSet,
               "pdh nxds0 prbs duration",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("nxds0List", "PdhNxDs0List", "")
               DEF_SDKPARAM("duration", "UINT", " Duration in miliscond"),
               "${de1s} ${nxds0List} ${duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineNxDs0DelayEnable,
               "pdh nxds0 prbs delay enable",
               "Enable PDH NxDS0 Delay.\n",
			   DEF_SDKPARAM("de1s", "PdhDe1List", "")
			   DEF_SDKPARAM("nxds0List", "PdhNxDs0List", ""),
               "${de1s} ${nxds0List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineNxDs0DelayDisable,
               "pdh nxds0 prbs delay disable",
               "Disable PDH NxDS0 Delay.\n",
			   DEF_SDKPARAM("de1s", "PdhDe1List", "")
			   DEF_SDKPARAM("nxds0List", "PdhNxDs0List", ""),
               "${de1s} ${nxds0List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineNxDs0DelayGet,
               "show pdh nxds0 prbs delay",
               "Show PDH NxDS0 Delay.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
			   DEF_SDKPARAM("nxds0List", "PdhNxDs0List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "Mode read to clear or read only")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${de1s} ${nxds0List} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineNxDs0DisruptionMaxExpectationTimeSet,
               "pdh nxds0 prbs disruption max_expect_time",
               "Set PRBS disruption max expected time in ms.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM_OPTION("nxds0List", "PdhNxDs0List", "")
               DEF_SDKPARAM_OPTION("max_expect_time", "UINT", " Duration in miliscond"),
               "${de1s} ${nxds0List} ${max_expect_time}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineNxDs0DisruptionGet,
               "show pdh nxds0 prbs disruption",
               "Show PDH NxDS0 disruption.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("nxds0List", "PdhNxDs0List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "Mode read to clear or read only")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${de1s} ${nxds0List} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

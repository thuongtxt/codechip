/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLISH
 *
 * File        : ClishAtPrbsEngineVc.c
 *
 * Created Date: Aug 24, 2013
 *
 * Description : PRBS CLISH
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("sdh path prbs", "\nPDH DS1/E1 PRBS commands\n")
TOP_SDKCOMMAND("sdh path prbs transmit", "\nPDH DS1/E1 PRBS Transmit commands\n")
TOP_SDKCOMMAND("sdh path prbs expect", "\nPDH DS1/E1 PRBS Expect commands\n")
TOP_SDKCOMMAND("sdh path prbs delay", "\nPDH DS1/E1 PRBS Delay commands\n")
TOP_SDKCOMMAND("sdh path prbs disruption", "\nPDH DS1/E1 PRBS disruption commands\n")


DEF_SDKCOMMAND(cliAtPrbsEngineVcModeSet,
               "sdh path prbs mode",
               "Set PRBS mode",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("mode", "eAtPrbsMode", ""),
               "${pathList} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineVcTransmitModeSet,
               "sdh path prbs transmit mode",
               "Set PRBS mode",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("mode", "eAtPrbsMode", ""),
               "${pathList} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineVcExpectModeSet,
               "sdh path prbs expect mode",
               "Set PRBS mode",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("mode", "eAtPrbsMode", ""),
               "${pathList} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineVcDataModeSet,
               "sdh path prbs datamode",
               "Set PRBS data mode",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("mode", "eAtPrbsDataMode", ""),
               "${pathList} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineVcTransmitDataModeSet,
               "sdh path prbs transmit datamode",
               "Set PRBS data mode",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("mode", "eAtPrbsDataMode", ""),
               "${pathList} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineVcExpectDataModeSet,
               "sdh path prbs expect datamode",
               "Set PRBS data mode",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("mode", "eAtPrbsDataMode", ""),
               "${pathList} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineVcInvertEnable,
               "sdh path prbs invert",
               "PRBS invert",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineVcInvertDisable,
               "sdh path prbs noinvert",
               "PRBS no invert",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineVcErrorForce,
               "sdh path prbs force",
               "Force PRBS error",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineVcErrorUnForce,
               "sdh path prbs unforce",
               "Unforce PRBS error",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineVcEnable,
               "sdh path prbs enable",
               "Enable PRBS engine",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineVcDisable,
               "sdh path prbs disable",
               "Disable PRBS engine",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineVcShow,
               "show sdh path prbs",
               "Show PRBS engines",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", "Read to clear without display table"),
               "${pathList} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineVcSideSet,
               "sdh path prbs side",
               "Set PRBS side",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("side", "eAtPrbsSide", ""),
               "${vcs} ${side}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineVcGeneratingSideSet,
               "sdh path prbs side generating",
               "Set PRBS generating side",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("side", "eAtPrbsSide", ""),
               "${vcs} ${side}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineVcMonitoringSideSet,
               "sdh path prbs side monitoring",
               "Set PRBS monitoring side",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("side", "eAtPrbsSide", ""),
               "${vcs} ${side}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineVcCounterGet,
               "show sdh path prbs counters",
               "Show PRBS counter",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${pathList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineVcErrorInject,
               "sdh path prbs force single",
               "Force single error",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM_OPTION("numForcedErrors", "STRING", "Number of forced errors"),
               "${pathList} ${numForcedErrors}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineVcFixPatternSet,
               "sdh path prbs fixpattern",
               "Set Transmitting and Expected fix-pattern for PRBS engines",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("pattern", "DWORD_HEX", " Transmitting pattern"),
               "${pathList} {pattern}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineVcTransmitFixPatternSet,
               "sdh path prbs transmit fixpattern",
               "Set Transmitting fix-pattern for PRBS engines",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("trans", "DWORD_HEX", " Transmitting pattern"),
               "${pathList} {trans}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineVcExpectFixPatternSet,
               "sdh path prbs expect fixpattern",
               "Set Expected fix-pattern for PRBS engines",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("expect", "DWORD_HEX", " Expected pattern"),
               "${pathList} {expect}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineVcBerShow,
               "show sdh path prbs ber",
               "Show BER of PRBS engines",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineVcForceBer,
               "sdh path prbs force ber",
               "Force Bit-error-rate for PRBS engines",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("ber", "ePrbsBer", ""),
               "${pathList} {ber}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineVcBitOrderSet,
               "sdh path prbs bitorder",
               "Set PRBS bit order",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("bitOrder", "eAtPrbsBitOrder", ""),
               "${pathList} ${bitOrder}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineVcStart,
               "sdh path prbs start",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineVcStop,
               "sdh path prbs stop",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineVcDurationSet,
               "sdh path prbs duration",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("duration", "UINT", " Duration in miliscond"),
               "${pathList} ${duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtPrbsEngineVcDelayEnable,
               "sdh path prbs delay enable",
               "Enable PRBS delay.\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtPrbsEngineVcDelayDisable,
               "sdh path prbs delay disable",
               "Disable PRBS delay.\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtPrbsEngineVcDelayGet,
               "show sdh path prbs delay",
               "Get PRBS delay.\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${pathList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineVcDisruptionMaxExpectationTimeSet,
               "sdh path prbs disruption max_expect_time",
               "Set PRBS disruption max expected time in ms.\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("max_expect_time", "UINT", " Duration in miliscond"),
               "${pathList} ${max_expect_time}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtPrbsEngineVcDisruptionGet,
               "show sdh path prbs disruption",
               "Get PRBS disruption.\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${pathList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

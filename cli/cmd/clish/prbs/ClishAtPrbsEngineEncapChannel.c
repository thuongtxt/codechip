/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLISH
 *
 * File        : ClishAtPrbsEngineDe1.c
 *
 * Created Date: Aug 24, 2013
 *
 * Description : PRBS CLISH
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("encap channel prbs", "\nENCAP channel PRBS commands\n")

DEF_SDKCOMMAND(cliAtPrbsEngineEncapChannelModeSet,
               "encap channel prbs mode",
               "Set PRBS mode",
               DEF_SDKPARAM("vcgs", "EncChannelList", "")
               DEF_SDKPARAM("mode", "eAtPrbsMode", ""),
               "${vcgs} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineEncapChannelInvertEnable,
               "encap channel prbs invert",
               "PRBS invert",
               DEF_SDKPARAM("vcgs", "EncChannelList", ""),
               "${vcgs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineEncapChannelInvertDisable,
               "encap channel prbs noinvert",
               "PRBS no invert",
               DEF_SDKPARAM("vcgs", "EncChannelList", ""),
               "${vcgs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineEncapChannelErrorForce,
               "encap channel prbs force",
               "Force PRBS error",
               DEF_SDKPARAM("vcgs", "EncChannelList", ""),
               "${vcgs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineEncapChannelErrorUnForce,
               "encap channel prbs unforce",
               "Unforce PRBS error",
               DEF_SDKPARAM("vcgs", "EncChannelList", ""),
               "${vcgs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineEncapChannelEnable,
               "encap channel prbs enable",
               "Enable PRBS engine",
               DEF_SDKPARAM("vcgs", "EncChannelList", ""),
               "${vcgs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineEncapChannelDisable,
               "encap channel prbs disable",
               "Disable PRBS engine",
               DEF_SDKPARAM("vcgs", "EncChannelList", ""),
               "${vcgs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineEncapChannelShow,
               "show encap channel prbs",
               "Show PRBS engines",
               DEF_SDKPARAM("vcgs", "EncChannelList", ""),
               "${vcgs}")
    {
    mAtCliCall();
    }

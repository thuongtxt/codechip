/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLISH
 *
 * File        : ClishAtPrbsEngineSerdesEthPort.c
 *
 * Created Date: Oct 02, 2018
 *
 * Description : PRBS CLISH
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("eth port prbs", "\nETH Port PRBS CLIs\n")
TOP_SDKCOMMAND("eth port prbs bandwidth", "\nETH Port PRBS CLIs\n")
TOP_SDKCOMMAND("eth port prbs bandwidth length", "\nETH Port PRBS CLIs\n")

DEF_SDKCOMMAND(cliAtPrbsEngineEthPortModeSet,
               "eth port prbs mode",
               "Set PRBS mode",
               DEF_SDKPARAM("ports", "PortIdList", "")
               DEF_SDKPARAM("mode", "eAtPrbsMode", "")
               DEF_SDKPARAM_OPTION("direction", "eAtDirection", ""),
               "${ports} ${mode} ${direction}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineEthPortInvertEnable,
               "eth port prbs invert",
               "PRBS invert",
               DEF_SDKPARAM("ports", "PortIdList", ""),
               "${ports}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineEthPortInvertDisable,
               "eth port prbs noinvert",
               "PRBS no invert",
               DEF_SDKPARAM("ports", "PortIdList", ""),
               "${ports}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineEthPortErrorForce,
               "eth port prbs force",
               "Force PRBS error",
               DEF_SDKPARAM("ports", "PortIdList", ""),
               "${ports}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineEthPortErrorUnForce,
               "eth port prbs unforce",
               "Unforce PRBS error",
               DEF_SDKPARAM("ports", "PortIdList", ""),
               "${ports}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineEthPortEnable,
               "eth port prbs enable",
               "Enable PRBS engine",
               DEF_SDKPARAM("ports", "PortIdList", "")
               DEF_SDKPARAM_OPTION("direction", "eAtDirection", ""),
               "${ports} ${direction}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineEthPortDisable,
               "eth port prbs disable",
               "Disable PRBS engine",
               DEF_SDKPARAM("ports", "PortIdList", "")
               DEF_SDKPARAM_OPTION("direction", "eAtDirection", ""),
               "${ports} ${direction}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineEthPortShow,
               "show eth port prbs",
               "Show PRBS engines",
               DEF_SDKPARAM("ports", "PortIdList", ""),
               "${ports}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineEthPortCounterShow,
               "show eth port prbs counters",
               "Show PRBS engines counters",
               DEF_SDKPARAM("ports", "PortIdList", "")
               DEF_SDKPARAM("counterReadMode", "HistoryReadingMode", "read only/read to clear mode")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${ports} ${counterReadMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineEthPortDebug,
             "debug eth port prbs",
             "Show all debug information of Eth Prbs engine",
             DEF_SDKPARAM("ports", "PortIdList", ""),
             "${ports}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineEthPortStart,
               "eth port prbs start",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("ports", "PortIdList", ""),
               "${ports}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineEthPortStop,
               "eth port prbs stop",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("ports", "PortIdList", ""),
               "${ports}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineEthPortDurationSet,
               "eth port prbs duration",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("ports", "PortIdList", "")
               DEF_SDKPARAM("duration", "UINT", " Duration in miliscond"),
               "${ports} {duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineEthPortBandwidthGet,
               "show eth port prbs bandwidth",
               "Show PRBS engine bandwidth control",
               DEF_SDKPARAM("ports", "PortIdList", ""),
               "${ports}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineEthPortPayloadLengthModeSet,
               "eth port prbs bandwidth length mode",
               "Set PRBS payload length mode",
               DEF_SDKPARAM("ports", "PortIdList", "")
               DEF_SDKPARAM("mode", "eAtPrbsPayloadLengthMode", "PRBS length mode"),
               "${ports} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineEthPortLengthMinSet,
               "eth port prbs bandwidth length min",
               "Set min length for payload",
               DEF_SDKPARAM("ports", "PortIdList", "")
               DEF_SDKPARAM("length", "STRING",  "min length"),
               "${ports} {length}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineEthPortLengthMaxSet,
               "eth port prbs bandwidth length max",
               "Set max length for payload",
               DEF_SDKPARAM("ports", "PortIdList", "")
               DEF_SDKPARAM("length", "STRING", "max length"),
               "${ports} {length}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineEthPortLengthBurstSet,
               "eth port prbs bandwidth length burst",
               "Set burst length for payload",
               DEF_SDKPARAM("ports", "PortIdList", "")
               DEF_SDKPARAM("length", "STRING", "burst length"),
               "${ports} {length}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineEthPortBandwidthPercentageSet,
               "eth port prbs bandwidth percentage",
               "Set percenatage of generating bandwidth against 100% bandwidth of full rate transmission",
               DEF_SDKPARAM("ports", "PortIdList", "")
               DEF_SDKPARAM("percentage", "STRING", "percentage (0-100)"),
               "${ports} {length}")
    {
    mAtCliCall();
    }

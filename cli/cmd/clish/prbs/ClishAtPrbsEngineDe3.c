/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLISH
 *
 * File        : ClishAtPrbsEngineDe3.c
 *
 * Created Date: April 07, 2015
 *
 * Description : PRBS CLISH
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("pdh de3 prbs", "\nPDH DS3/E3 PRBS commands\n")
TOP_SDKCOMMAND("pdh de3 prbs transmit", "\nPDH DS3/E3 PRBS Transmit commands\n")
TOP_SDKCOMMAND("pdh de3 prbs expect", "\nPDH DS3/E3 PRBS Expect commands\n")
TOP_SDKCOMMAND("pdh de3 prbs delay", "\nPDH DS3/E3 PRBS Delay commands\n")
TOP_SDKCOMMAND("pdh de3 prbs disruption", "\nPDH DS3/E3 PRBS disruption commands\n")
TOP_SDKCOMMAND("pdh de3 line", "\nPDH DS1/E1 Line PRBS commands\n")
TOP_SDKCOMMAND("pdh de3 line prbs", "\nPDH DS3/E3 Line PRBS commands\n")
TOP_SDKCOMMAND("show pdh de3", "\nPDH DS3/E3 Line PRBS commands\n")
TOP_SDKCOMMAND("show pdh de3 line", "\nPDH DS3/E3 Line PRBS commands\n")


DEF_SDKCOMMAND(cliAtPrbsEngineDe3ModeSet,
               "pdh de3 prbs mode",
               "Set PRBS mode",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("mode", "eAtPrbsMode", ""),
               "${de3s} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtPrbsEngineDe3TransmitModeSet,
               "pdh de3 prbs transmit mode",
               "Set PRBS mode",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("mode", "eAtPrbsMode", ""),
               "${de3s} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtPrbsEngineDe3ExpectModeSet,
               "pdh de3 prbs expect mode",
               "Set PRBS mode",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("mode", "eAtPrbsMode", ""),
               "${de3s} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe3DataModeSet,
               "pdh de3 prbs datamode",
               "Set PRBS data mode",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("mode", "eAtPrbsDataMode", ""),
               "${de3s} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtPrbsEngineDe3TransmitDataModeSet,
               "pdh de3 prbs transmit datamode",
               "Set PRBS data mode",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("mode", "eAtPrbsDataMode", ""),
               "${de3s} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtPrbsEngineDe3ExpectDataModeSet,
               "pdh de3 prbs expect datamode",
               "Set PRBS data mode",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("mode", "eAtPrbsDataMode", ""),
               "${de3s} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe3InvertEnable,
               "pdh de3 prbs invert",
               "PRBS invert",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe3InvertDisable,
               "pdh de3 prbs noinvert",
               "PRBS no invert",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe3ErrorForce,
               "pdh de3 prbs force",
               "Force PRBS error",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe3ErrorUnForce,
               "pdh de3 prbs unforce",
               "Unforce PRBS error",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe3Enable,
               "pdh de3 prbs enable",
               "Enable PRBS engine",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe3Disable,
               "pdh de3 prbs disable",
               "Disable PRBS engine",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe3Show,
               "show pdh de3 prbs",
               "Show PRBS engines",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", "Read to clear without display table"),
               "${de3s} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe3ForceBer,
               "pdh de3 prbs force ber",
               "Force Bit-error-rate for PRBS engines",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("ber", "ePrbsBer", ""),
               "${de3s} {ber}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe3FixPatternSet,
               "pdh de3 prbs fixpattern",
               "Set Transmitting and Expected fix-pattern for PRBS engines",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("pattern", "DWORD_HEX", " Transmitting pattern"),
               "${de3s} {pattern}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe3TransmitFixPatternSet,
               "pdh de3 prbs transmit fixpattern",
               "Set Transmitting fix-pattern for PRBS engines",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("trans", "DWORD_HEX", " Transmitting pattern"),
               "${de3s} {trans}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe3ExpectFixPatternSet,
               "pdh de3 prbs transmit fixpattern",
               "Set Expected fix-pattern for PRBS engines",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("expect", "DWORD_HEX", " Expected pattern"),
               "${de3s} {expect}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe3BerShow,
               "show pdh de3 prbs ber",
               "Show BER of PRBS engines",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe3Side,
               "pdh de3 prbs side",
               "Set PRBS engine generating/monitoring side",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("side", "eAtPrbsSide", ""),
               "${de3s} ${side}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe3GeneratingSideSet,
               "pdh de3 prbs side generating",
               "Set PRBS generating side",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("side", "eAtPrbsSide", ""),
               "${de3s} ${side}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe3MonitoringSideSet,
               "pdh de3 prbs side monitoring",
               "Set PRBS monitoring side",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("side", "eAtPrbsSide", ""),
               "${de3s} ${side}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe3CounterGet,
               "show pdh de3 prbs counters",
               "Show PRBS counter",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${de3s} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe3ErrorInject,
               "pdh de3 prbs force single",
               "Force single error",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM_OPTION("numForcedErrors", "STRING", "Number of forced errors"),
               "${de3s} ${numForcedErrors}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe3BitOrderSet,
               "pdh de3 prbs bitorder",
               "Set PRBS bit order",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("bitOrder", "eAtPrbsBitOrder", ""),
               "${de3s} ${bitOrder}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineLineDe3ErrorForce,
               "pdh de3 line prbs force",
               "Force Line PRBS error",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineLineDe3ErrorUnForce,
               "pdh de3 line prbs unforce",
               "Unforce Line PRBS error",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineLineDe3Enable,
               "pdh de3 line prbs enable",
               "Enable Line PRBS engine",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineLineDe3Disable,
               "pdh de3 line prbs disable",
               "Disable Line PRBS engine",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineLineDe3Show,
               "show pdh de3 line prbs",
               "Show Line PRBS engines",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe3Start,
               "pdh de3 prbs start",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe3Stop,
               "pdh de3 prbs stop",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe3DurationSet,
               "pdh de3 prbs duration",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("duration", "UINT", " Duration in miliscond"),
               "${de3s} {duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineLineDe3Start,
               "pdh de3 line prbs start",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineLineDe3Stop,
               "pdh de3 line prbs stop",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineLineDe3DurationSet,
               "pdh de3 line prbs duration",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("duration", "UINT", " Duration in miliscond"),
               "${de3s} ${duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtPrbsEngineDe3DelayEnable,
               "pdh de3 prbs delay enable",
               "Enable PDH DE3 delay.\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtPrbsEngineDe3DelayDisable,
               "pdh de3 prbs delay disable",
               "Disable PDH DE3 delay.\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtPrbsEngineDe3DelayGet,
               "show pdh de3 prbs delay",
               "Show PDH DE3 delay.\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "Mode read to clear or read only")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${de3s} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineLineDe3DisruptionMaxExpectationTimeSet,
               "pdh de3 prbs disruption max_expect_time",
               "Set PRBS disruption max expected time in ms.\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("max_expect_time", "UINT", " Duration in miliscond"),
               "${de3s} ${max_expect_time}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtPrbsEngineDe3DisruptionGet,
               "show pdh de3 prbs disruption",
               "Show PDH DE3 disruption.\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "Mode read to clear or read only")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${de3s} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

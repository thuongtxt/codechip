/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLISH
 *
 * File        : ClishAtPrbsEngineDe1.c
 *
 * Created Date: Aug 24, 2013
 *
 * Description : PRBS CLISH
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("pdh de1 prbs", "\nPDH DS1/E1 PRBS commands\n")
TOP_SDKCOMMAND("pdh de1 prbs transmit", "\nPDH DS1/E1 PRBS Transmit commands\n")
TOP_SDKCOMMAND("pdh de1 prbs expect", "\nPDH DS1/E1 PRBS Expect commands\n")
TOP_SDKCOMMAND("pdh de1 prbs delay", "\nPDH DS1/E1 PRBS Delay commands\n")
TOP_SDKCOMMAND("pdh de1 prbs disruption", "\nPDH DS1/E1 PRBS disruption commands\n")
TOP_SDKCOMMAND("pdh de1 line", "\nPDH DS1/E1 Line PRBS commands\n")
TOP_SDKCOMMAND("pdh de1 line prbs", "\nPDH DS1/E1 Line PRBS commands\n")
TOP_SDKCOMMAND("show pdh de1 line", "\nPDH DS1/E1 Line PRBS commands\n")

DEF_SDKCOMMAND(cliAtPrbsEngineDe1ModeSet,
               "pdh de1 prbs mode",
               "Set PRBS mode",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("mode", "eAtPrbsMode", ""),
               "${de1s} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtPrbsEngineDe1TransmitModeSet,
               "pdh de1 prbs transmit mode",
               "Set PRBS mode",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("mode", "eAtPrbsMode", ""),
               "${de1s} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtPrbsEngineDe1ExpectModeSet,
               "pdh de1 prbs expect mode",
               "Set PRBS mode",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("mode", "eAtPrbsMode", ""),
               "${de1s} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe1DataModeSet,
               "pdh de1 prbs datamode",
               "Set PRBS data mode",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("mode", "eAtPrbsDataMode", ""),
               "${de1s} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtPrbsEngineDe1TransmitDataModeSet,
               "pdh de1 prbs transmit datamode",
               "Set PRBS datamode",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("mode", "eAtPrbsDataMode", ""),
               "${de1s} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtPrbsEngineDe1ExpectDataModeSet,
               "pdh de1 prbs expect datamode",
               "Set PRBS data mode",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("mode", "eAtPrbsDataMode", ""),
               "${de1s} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe1InvertEnable,
               "pdh de1 prbs invert",
               "PRBS invert",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe1InvertDisable,
               "pdh de1 prbs noinvert",
               "PRBS no invert",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe1ErrorForce,
               "pdh de1 prbs force",
               "Force PRBS error",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe1ErrorUnForce,
               "pdh de1 prbs unforce",
               "Unforce PRBS error",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe1Enable,
               "pdh de1 prbs enable",
               "Enable PRBS engine",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe1Disable,
               "pdh de1 prbs disable",
               "Disable PRBS engine",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe1Show,
               "show pdh de1 prbs",
               "Show PRBS engines",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", "Read to clear without display table"),
               "${de1s} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe1ForceBer,
               "pdh de1 prbs force ber",
               "Force Bit-error-rate for PRBS engines",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("ber", "ePrbsBer", ""),
               "${de1s} {ber}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe1FixPatternSet,
               "pdh de1 prbs fixpattern",
               "Set fix-pattern for PRBS engines",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("pattern", "DWORD_HEX", " Transmitting pattern"),
               "${de1s} {pattern}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe1TransmitFixPatternSet,
               "pdh de1 prbs transmit fixpattern",
               "Set Transmit fix-pattern for PRBS engines",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("trans", "DWORD_HEX", " Transmitting pattern"),
               "${de1s} {trans}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe1ExpectFixPatternSet,
               "pdh de1 prbs expect fixpattern",
               "Set Expect fix-pattern for PRBS engines",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("expect", "DWORD_HEX", " Expected pattern"),
               "${de1s} {expect}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe1BerShow,
               "show pdh de1 prbs ber",
               "Show BER of PRBS engines",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe1Side,
               "pdh de1 prbs side",
               "Set PRBS engine generating/monitoring side",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("side", "eAtPrbsSide", ""),
               "${de1s} ${side}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe1GeneratingSideSet,
               "pdh de1 prbs side generating",
               "Set PRBS generating side",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("side", "eAtPrbsSide", ""),
               "${de1s} ${side}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe1MonitoringSideSet,
               "pdh de1 prbs side monitoring",
               "Set PRBS monitoring side",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("side", "eAtPrbsSide", ""),
               "${de1s} ${side}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe1CounterGet,
               "show pdh de1 prbs counters",
               "Show PRBS counter",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${de1s} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe1ErrorInject,
               "pdh de1 prbs force single",
               "Force single error",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM_OPTION("numForcedErrors", "STRING", "Number of forced errors"),
               "${de1s} ${numForcedErrors}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe1BitOrderSet,
               "pdh de1 prbs bitorder",
               "Set PRBS bit order",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("bitOrder", "eAtPrbsBitOrder", ""),
               "${de1s} ${bitOrder}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineLineDe1ErrorForce,
               "pdh de1 line prbs force",
               "Force Line PRBS error",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineLineDe1ErrorUnForce,
               "pdh de1 line prbs unforce",
               "Unforce Line PRBS error",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineLineDe1Enable,
               "pdh de1 line prbs enable",
               "Enable Line PRBS engine",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineLineDe1Disable,
               "pdh de1 line prbs disable",
               "Disable Line PRBS engine",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineLineDe1Show,
               "show pdh de1 line prbs",
               "Show Line PRBS engines",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe1Start,
               "pdh de1 prbs start",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe1Stop,
               "pdh de1 prbs stop",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDe1DurationSet,
               "pdh de1 prbs duration",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("duration", "UINT", " Duration in miliscond"),
               "${de1s} {duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineLineDe1Start,
               "pdh de1 line prbs start",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineLineDe1Stop,
               "pdh de1 line prbs stop",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineLineDe1DurationSet,
               "pdh de1 line prbs duration",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("duration", "UINT", " Duration in miliscond"),
               "${de1s} {duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtPrbsEngineDe1DelayEnable,
               "pdh de1 prbs delay enable",
               "Enable PDH DE1 delay.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtPrbsEngineDe1DelayDisable,
               "pdh de1 prbs delay disable",
               "Disable PDH DE1 delay.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtPrbsEngineDe1DelayGet,
               "show pdh de1 prbs delay",
               "Show PDH DE1 delay.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "Mode read to clear or read only")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${de1s} {readingMode} {silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineLineDe1DisruptionMaxExpectationTimeSet,
               "pdh de1 prbs disruption max_expect_time",
               "Set PRBS disruption max expected time in ms.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("max_expect_time", "UINT", " Duration in miliscond"),
               "${de1s} {max_expect_time}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtPrbsEngineDe1DisruptionGet,
               "show pdh de1 prbs disruption",
               "Show PDH DE1 delay.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "Mode read to clear or read only")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${de1s} {readingMode} {silent}")
    {
    mAtCliCall();
    }

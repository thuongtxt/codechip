/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLISH
 *
 * File        : ClishAtPrbsEngineSdhLine.c
 *
 * Created Date: Nov 05, 2015
 *
 * Description : PRBS CLISH
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("sdh line", "\nPDH DS1/E1 Line PRBS commands\n")
TOP_SDKCOMMAND("sdh line prbs", "\nPDH DS1/E1 Line PRBS commands\n")
TOP_SDKCOMMAND("show sdh line", "\nPDH DS1/E1 Line PRBS commands\n")

DEF_SDKCOMMAND(cliAtPrbsEngineLineSdhErrorForce,
               "sdh line prbs force",
               "Force Line PRBS error",
               DEF_SDKPARAM("lines", "SdhLineList", ""),
               "${lines}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineLineSdhErrorUnForce,
               "sdh line prbs unforce",
               "Unforce Line PRBS error",
               DEF_SDKPARAM("lines", "SdhLineList", ""),
               "${lines}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineLineSdhEnable,
               "sdh line prbs enable",
               "Enable Line PRBS engine",
               DEF_SDKPARAM("lines", "SdhLineList", ""),
               "${lines}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineLineSdhDisable,
               "sdh line prbs disable",
               "Disable Line PRBS engine",
               DEF_SDKPARAM("lines", "SdhLineList", ""),
               "${lines}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineLineSdhShow,
               "show sdh line prbs",
               "Show Line PRBS engines",
               DEF_SDKPARAM("lines", "SdhLineList", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", "Read to clear without display table"),
               "${lines} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineLineSdhStart,
               "sdh line prbs start",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("lines", "SdhLineList", ""),
               "${lines}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineLineSdhStop,
               "sdh line prbs stop",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("lines", "SdhLineList", ""),
               "${lines}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineLineSdhDurationSet,
               "sdh line prbs duration",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("lines", "SdhLineList", "")
               DEF_SDKPARAM("duration", "UINT", " Duration in miliscond"),
               "${lines} {duration}")
    {
    mAtCliCall();
    }

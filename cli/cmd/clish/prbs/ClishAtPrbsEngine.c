/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : ClishAtPrbsEngine.c
 *
 * Created Date: Aug 24, 2013
 *
 * Description : PRBS CLISH
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
DEF_PTYPE("eAtPrbsMode", "select", "prbs7(1), prbs9(2), prbs15(3), prbs23(4), prbs31(5), prbs11(6), prbs20(7), prbs20qrss(8), prbs20r(9), sequence(10), fixedpattern1byte(11), fixedpattern2bytes(12), fixedpattern3bytes(13), fixedpattern4bytes(14)", "\nPRBS mode\n")
DEF_PTYPE("eAtPrbsDataMode", "select", "64kb(1), 56kb(2)", "\nPRBS data mode\n")
DEF_PTYPE("ePrbsBer", "select", "off(0), 1e-2(1), 1e-3(2), 1e-4(3), 1e-5(4), 1e-6(5), 1e-7(6), 1e-8(7), 1e-9(8), 1e-10(9)", "\nPRBS Bit-error-rate\n")
DEF_PTYPE("eAtPrbsSide", "select", "tdm(1), psn(2)", "\nPRBS side\n")
DEF_PTYPE("eAtPrbsBitOrder", "select", "msb(1), lsb(2)", "\nPRBS bit order\n")
DEF_PTYPE("eAtPrbsPayloadLengthMode", "select", "fix(0), increase(1), decrease(2), random(3)", "\nPRBS payload length mode\n")

TOP_SDKCOMMAND("prbs", "\nPRBS commands\n")
TOP_SDKCOMMAND("prbs engine", "\nPRBS engine commands\n")
TOP_SDKCOMMAND("prbs engine create", "\nPRBS engine create commands\n")
TOP_SDKCOMMAND("show prbs", "\nPRBS show commands\n")
TOP_SDKCOMMAND("prbs engine tx", "\nPRBS commands to control transmit side\n")
TOP_SDKCOMMAND("prbs engine rx", "\nPRBS commands to control received side\n")
TOP_SDKCOMMAND("debug prbs engine monitor", "\nMonitor bert engines\n")
TOP_SDKCOMMAND("prbs engine bandwidth", "\nPRBS commands to control bandwidth\n")
TOP_SDKCOMMAND("prbs engine bandwidth length", "\nPRBS commands to control bandwidth length\n")

DEF_SDKCOMMAND(CliAtModulePrbsEnginePdhDe1Create,
               "prbs engine create de1",
               "Create DE1 PRBS engine",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ...")
               DEF_SDKPARAM("de1Ids", "PdhDe1List", "DE1 IDs"),
               "${engineIds} ${de1Ids}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtModulePrbsEnginePdhDe3Create,
               "prbs engine create de3",
               "Create DE3 PRBS engine",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ...")
               DEF_SDKPARAM("de3Ids", "PdhDe3List", "DE3 IDs"),
               "${engineIds} ${de3Ids}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtModulePrbsEnginePdhNxDs0Create,
               "prbs engine create nxds0",
               "Create NxDs0 PRBS engine",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ...")
               DEF_SDKPARAM("de1s", "PdhNxDs0De1List", "DE1 IDs")
               DEF_SDKPARAM("nxds0List", "PdhNxDs0List", "DS0s"),
               "${engineIds} ${de1s} ${nxds0List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtModulePrbsEngineSdhPathCreate,
               "prbs engine create vc",
               "Create SDH path PRBS engine",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ...")
               DEF_SDKPARAM("pathList", "SdhPathList", "SDH path IDs"),
               "${engineIds} ${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtModulePrbsEngineEthPortCreate,
               "prbs engine create eth",
               "Create Ethernet port PRBS engine",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ...")
               DEF_SDKPARAM("portId", "PortIdList", "Ethernet Port IDs"),
               "${engineIds} ${portId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineGet,
               "show prbs engine",
               "Show PRBS engine",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ..."),
               "${engineIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtModulePrbsEngineDelete,
               "prbs engine delete",
               "Delete PRBS engine",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ..."),
               "${engineIds}")
    {
    mAtCliCall();
    }


DEF_SDKCOMMAND(cliAtPrbsEngineModeSet,
               "prbs engine mode",
               "Set PRBS mode",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ...")
               DEF_SDKPARAM("mode", "eAtPrbsMode", "PRBS mode"),
               "${engineIds} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineTxModeSet,
               "prbs engine mode tx",
               "Set TX PRBS mode",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ...")
               DEF_SDKPARAM("mode", "eAtPrbsMode", "PRBS mode"),
               "${engineIds} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineRxModeSet,
               "prbs engine mode rx",
               "Set RX PRBS mode",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ...")
               DEF_SDKPARAM("mode", "eAtPrbsMode", "PRBS mode"),
               "${engineIds} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineInvertEnable,
               "prbs engine invert",
               "PRBS invert",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ..."),
               "${engineIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineInvertDisable,
               "prbs engine noinvert",
               "PRBS no invert",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ..."),
               "${engineIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineErrorForce,
               "prbs engine force",
               "Force PRBS error",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ..."),
               "${engineIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineErrorUnForce,
               "prbs engine unforce",
               "Unforce PRBS error",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ..."),
               "${engineIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineEnable,
               "prbs engine enable",
               "Enable PRBS engine",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ..."),
               "${engineIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDisable,
               "prbs engine disable",
               "Disable PRBS engine",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ..."),
               "${engineIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineForceBer,
               "prbs engine force ber",
               "Force Bit-error-rate for PRBS engines",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ...")
               DEF_SDKPARAM("ber", "ePrbsBer", "BER level"),
               "${engineIds} {ber}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineFixPatternSet,
               "prbs engine fixpattern",
               "Set fix-pattern for PRBS engines",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ...")
               DEF_SDKPARAM("trans", "STRING", " Transmitting pattern")
               DEF_SDKPARAM("expect", "STRING", " Expected pattern"),
               "${engineIds} {trans} {expect}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineBerShow,
               "show prbs engine ber",
               "Show BER of PRBS engines",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ..."),
               "${engineIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineSide,
               "prbs engine side",
               "Set PRBS engine generating/monitoring side",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ...")
               DEF_SDKPARAM("side", "eAtPrbsSide", "bert side"),
               "${engineIds} ${side}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineGeneratingSideSet,
               "prbs engine side generating",
               "Set PRBS generating side",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ...")
               DEF_SDKPARAM("side", "eAtPrbsSide", "bert side"),
               "${engineIds} ${side}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineMonitoringSideSet,
               "prbs engine side monitoring",
               "Set PRBS monitoring side",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ...")
               DEF_SDKPARAM("side", "eAtPrbsSide", "bert side"),
               "${engineIds} ${side}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineCounterGet,
               "show prbs engine counters",
               "Show PRBS engines",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ...")
               DEF_SDKPARAM("counterReadMode", "HistoryReadingMode", "read only/read to clear mode")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${engineIds} ${counterReadMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineErrorInject,
               "prbs engine force single",
               "Force single error",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ...")
               DEF_SDKPARAM_OPTION("numForcedErrors", "STRING", "number of errors"),
               "${engineIds} ${numForcedErrors}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineBitOrderSet,
               "prbs engine bitorder",
               "Set PRBS bit order",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ...")
               DEF_SDKPARAM("bitOrder", "eAtPrbsBitOrder", "bit order"),
               "${engineIds} ${bitOrder}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineTxEnable,
               "prbs engine tx enable",
               "Enable PRBS engine at transmit direction",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ..."),
               "${engineIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineTxDisable,
               "prbs engine tx disable",
               "Disable PRBS engine at transmit direction",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ..."),
               "${engineIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineRxEnable,
               "prbs engine rx enable",
               "Enable PRBS engine at received direction",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ..."),
               "${engineIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineRxDisable,
               "prbs engine rx disable",
               "Disable PRBS engine at received direction",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ..."),
               "${engineIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDebug,
               "debug prbs engine",
               "Debug PRBS engine.\n",
               DEF_SDKPARAM("engineIds", "STRING", ""),
               "${engineIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineMonitorStart,
               "debug prbs engine monitor start",
               "Start debugging PRBS engine.\n",
               DEF_SDKPARAM("engineIds", "STRING", ""),
               "${engineIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineStart,
               "prbs engine start",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("engineIds", "STRING", ""),
               "${engineIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineMonitorStop,
               "debug prbs engine monitor stop",
               "Stop debugging PRBS engine monitoring.\n",
               DEF_NULLPARAM,"")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineStop,
               "prbs engine stop",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("engineIds", "STRING", ""),
               "${engineIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineDurationSet,
               "prbs engine duration",
               "Start PRBS engine.\n",
               DEF_SDKPARAM("engineIds", "STRING", "")
               DEF_SDKPARAM("duration", "UINT", " Duration in miliscond"),
               "${engineIds} {duration}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineTxInvertEnable,
               "prbs engine invert tx",
               "PRBS invert",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ..."),
               "${engineIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineTxInvertDisable,
               "prbs engine noinvert tx",
               "PRBS no invert",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ..."),
               "${engineIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineRxInvertEnable,
               "prbs engine invert rx",
               "PRBS invert",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ..."),
               "${engineIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineRxInvertDisable,
               "prbs engine noinvert rx",
               "PRBS no invert",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ..."),
               "${engineIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineTxFixPatternSet,
               "prbs engine fixpattern tx",
               "Set fix-pattern for PRBS engines at TX side",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ...")
               DEF_SDKPARAM("trans", "STRING", " Transmitting pattern"),
               "${engineIds} {trans}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineRxFixPatternSet,
               "prbs engine fixpattern rx",
               "Set expected fix-pattern for PRBS engines at RX side",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ...")
               DEF_SDKPARAM("expect", "STRING", " Expected pattern"),
               "${engineIds} {expect}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineBandwidthGet,
               "show prbs engine bandwidth",
               "Show PRBS engine bandwidth control",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ..."),
               "${engineIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEnginePayloadLengthModeSet,
               "prbs engine bandwidth length mode",
               "Set PRBS payload length mode",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ...")
               DEF_SDKPARAM("mode", "eAtPrbsPayloadLengthMode", "PRBS length mode"),
               "${engineIds} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineLengthMinSet,
               "prbs engine bandwidth length min",
               "Set min length for payload",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ...")
               DEF_SDKPARAM("length", "STRING",  "min length"),
               "${engineIds} {length}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineLengthMaxSet,
               "prbs engine bandwidth length max",
               "Set max length for payload",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ...")
               DEF_SDKPARAM("length", "STRING", "max length"),
               "${engineIds} {length}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineLengthBurstSet,
               "prbs engine bandwidth length burst",
               "Set burst length for payload",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ...")
               DEF_SDKPARAM("length", "STRING", "burst length"),
               "${engineIds} {length}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPrbsEngineBandwidthPercentageSet,
               "prbs engine bandwidth percentage",
               "Set percenatage of generating bandwidth against 100% bandwidth of full rate transmission",
               DEF_SDKPARAM("engineIds", "STRING", "PRBS engine IDs:1, 2, 1-2, ...")
               DEF_SDKPARAM("percentage", "STRING", "percentage (0-100)"),
               "${engineIds} {length}")
    {
    mAtCliCall();
    }

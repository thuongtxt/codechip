/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Clock
 *
 * File        : ClishAtModuleClock.c
 *
 * Created Date: Nov 22, 2013
 *
 * Description : Clock module CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("clock interrupt", "CLIs to control module clock interrupt")

DEF_SDKCOMMAND(cliAtModuleClockDebug,
               "debug module clock",
               "Show debug information of Clock module\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModuleClockCheck,
               "clock check",
               "Check all clocks\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliModuleClockInterruptEnable,
               "clock interrupt enable",
               "Enable interrupt",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliModuleClockInterruptDisable,
               "clock interrupt disable",
               "Disable interrupt",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliModuleClockShow,
               "show module clock",
               "Show Clock module information",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

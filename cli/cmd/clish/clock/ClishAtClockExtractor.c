/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Clock
 *
 * File        : ClishAtClockExtractor.c
 *
 * Created Date: Sep 3, 2013
 *
 * Description : CLISH of clock extractor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/
/*
 * Define defect mask regular expression
 * DefType ::= (los|lof|oof|ais|rai|cas_lom)
 * DefMsk  ::= (DefMsk[\|]DefType|DefType)
 */
#define DefType "(los|lof|ais|eth-lf|eth-rf|eth-lossync|eth-eer|eth-linkdown|none)"
#define DefMsk  DefType"|"DefType

DEF_PTYPE("ClockSquelchingOptions", "regexp", DefMsk,
        "\nClock squelching option mask, it is ORed of:\n"
        "- los: LOS\n"
        "- lof: LOF\n"
        "- ais: AIS\n"
        "- eth-lf: Ethernet XGE local fault\n"
        "- eth-rf: Ethernet XGE remote fault\n"
        "- eth-lossync: XGE Lossync\n"
        "- eth-eer: Ethernet XGE/GE Excessive error rate\n"
        "- eth-linkdown: Eth link down\n"
        "- none: no squelching\n")

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("clock", "Clock CLIs")
TOP_SDKCOMMAND("clock extract", "Clock extract CLIs")
TOP_SDKCOMMAND("show clock", "CLIs to show clock")

DEF_SDKCOMMAND(cliAtClockExtractorSystemClockExtract,
               "clock extract system",
               "Extract system clock",
               DEF_SDKPARAM("extractors", "STRING", "List of clock extractor IDs"),
               "${extractors}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtClockExtractorSdhSystemClockExtract,
               "clock extract sdh_sys",
               "Extract system clock provided for SDH Module",
               DEF_SDKPARAM("extractors", "STRING", "List of clock extractor IDs"),
               "${extractors}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtClockExtractorSdhLineClockExtract,
               "clock extract sdh_line",
               "Extract clock from specified SDH Line",
               DEF_SDKPARAM("extractors", "STRING", "List of clock extractor IDs")
               DEF_SDKPARAM("lines", "SdhLineList", ""),
               "${extractors} ${lines}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtClockExtractorPdhDe1ClockExtract,
               "clock extract de1",
               "Extract clock from DS1/E1",
               DEF_SDKPARAM("extractors", "STRING", "List of clock extractor IDs")
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${extractors} ${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtClockExtractorPdhDe1LiuClockExtract,
               "clock extract de1 liu",
               "Extract clock from DS1/E1 RX LIU. This CLI is only applicable for DS1s/E1s that connect with LIUs",
               DEF_SDKPARAM("extractors", "STRING", "List of clock extractor IDs")
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${extractors} ${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtClockExtractorExternalClockExtract,
               "clock extract ext",
               "Extract clock from external clock source",
               DEF_SDKPARAM("extractors", "STRING", "List of clock extractor IDs")
               DEF_SDKPARAM("externalIds", "UINT", "External clock source IDs"),
               "${extractors} ${externalIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtClockExtractorEnable,
               "clock extract enable",
               "Enable clock output",
               DEF_SDKPARAM("extractors", "STRING", "List of clock extractor IDs"),
               "${extractors}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtClockExtractorDisable,
               "clock extract disable",
               "Disable clock output",
               DEF_SDKPARAM("extractors", "STRING", "List of clock extractor IDs"),
               "${extractors}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtClockExtractorShow,
               "show clock extract",
               "Show clock extractors",
               DEF_SDKPARAM("extractors", "STRING", "List of clock extractor IDs"),
               "${extractors}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtClockExtractorPdhDe3ClockExtract,
               "clock extract de3",
               "Extract clock from DS3/E3",
               DEF_SDKPARAM("extractors", "STRING", "List of clock extractor IDs")
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${extractors} ${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtClockExtractorPdhDe3LiuClockExtract,
               "clock extract de3 liu",
               "Extract clock from DS3/E3 RX LIU. This CLI is only applicable for DS3s/E3s that connect with LIUs",
               DEF_SDKPARAM("extractors", "STRING", "List of clock extractor IDs")
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${extractors} ${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtClockExtractorSerdesClockExtract,
               "clock extract serdes",
               "Extract clock from RX Serdes.",
               DEF_SDKPARAM("extractors", "STRING", "List of clock extractor IDs")
               DEF_SDKPARAM("serdes", "SerdesIdList", ""),
               "${extractors} ${serdes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtClockExtractorSquelchingOptionSet,
               "clock extract squelching",
               "Set options to mask output clock to zero when defection condition matchs to the options (LOS/LOF/AIS)",
               DEF_SDKPARAM("extractors", "STRING", "List of clock extractor IDs")
               DEF_SDKPARAM("options", "ClockSquelchingOptions", ""),
               "${extractors} ${options}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtClockExtractorDebug,
               "clock extract debug",
               "Show debug information of clock extractors",
               DEF_SDKPARAM("extractors", "STRING", "List of clock extractor IDs"),
               "${extractors}")
    {
    mAtCliCall();
    }

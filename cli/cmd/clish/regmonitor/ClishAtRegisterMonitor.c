/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : ClishAtDevice.c
 *
 * Created Date: Nov 15, 2012
 *
 * Description : CLISH Device CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("register", "Register CLIs")
TOP_SDKCOMMAND("register mon", "Register Monitoring CLIs")
TOP_SDKCOMMAND("register mon short", "Short Register Monitoring CLIs")
TOP_SDKCOMMAND("show register", "Show Register CLIs")
TOP_SDKCOMMAND("show register mon", "Show Register Monitoring CLIs")

DEF_SDKCOMMAND(cliAtRegisterMonStart,
               "register mon start",
               "Start register mon",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtRegisterMonStop,
               "register mon stop",
               "Stop register mon",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtRegisterMonSyncHw,
               "register mon synchw",
               "Sync all value with hardware",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtRegisterMonInfoShow,
               "show register mon info",
               "Show more info of register monitoring",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtRegisterMonResultShow,
               "show register mon result",
               "Show sumary result of monitoring",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtRegisterMonFailedShow,
               "show register mon fail",
               "Show these failed register when monitoring",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtRegistermonPerfom,
               "register mon perform",
               "Configure parameter of Register mon",
               DEF_SDKPARAM("duration", "STRING", "Duration in second, start from 1")
               DEF_SDKPARAM("repeat", "STRING", "Repeat number, start from 1"),
               "${duration} ${repeat} ")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtShortRegisterMonAdd,
               "register mon short add",
               "Add register for monitoring",
               DEF_SDKPARAM("address", "STRING", "Start Adddress with format 0xXXXXXXXX")
               DEF_SDKPARAM("value", "STRING", "Any value")
               DEF_SDKPARAM_OPTION("numbers", "UINT", "Start from 1")
               DEF_SDKPARAM_OPTION("step", "UINT", "Start from 1")
               DEF_SDKPARAM_OPTION("FpgaId", "UINT", "FPGA Index will start from 1"),
               "${address} ${value} ${numbers} ${step} ${FpgaId} ")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtShortRegisterMonRmv,
               "register mon short remove",
			   "Remove register, that will not monitor any more",
               DEF_SDKPARAM("address", "STRING", "Start Adddress with format 0xXXXXXXXX")
               DEF_SDKPARAM_OPTION("numbers", "UINT", "Start from 1")
               DEF_SDKPARAM_OPTION("step", "UINT", "Start from 1")
               DEF_SDKPARAM_OPTION("FpgaId", "UINT", "FPGA Index will start from 1"),
               "${address} ${numbers} ${step} ${FpgaId} ")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtShortRegisterMonMask,
               "register mon short mask",
               "Add register for monitoring",
               DEF_SDKPARAM("address", "STRING", "Start Adddress with format 0xXXXXXXXX")
               DEF_SDKPARAM("mask", "STRING",  "Any value")
               DEF_SDKPARAM_OPTION("numbers", "UINT", "Start from 1")
               DEF_SDKPARAM_OPTION("step", "UINT", "Start from 1")
               DEF_SDKPARAM_OPTION("FpgaId", "UINT", "FPGA Index will start from 1"),
               "${address} ${mask} ${numbers} ${step} ${FpgaId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtShortRegisterMonValue,
               "register mon short value",
               "Add register for monitoring",
               DEF_SDKPARAM("address", "STRING", "Start Adddress with format 0xXXXXXXXX")
               DEF_SDKPARAM("value", "STRING",  "Any value")
               DEF_SDKPARAM_OPTION("numbers", "UINT", "Start from 1")
               DEF_SDKPARAM_OPTION("step", "UINT", "Start from 1")
               DEF_SDKPARAM_OPTION("FpgaId", "UINT", "FPGA Index will start from 1"),
               "${address} ${value} ${numbers} ${step} ${FpgaId}")
    {
    mAtCliCall();
    }

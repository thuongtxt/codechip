/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : ClishAtPtchService.c
 *
 * Created Date: May 7, 2016
 *
 * Description : PTCH service CLISH CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCli.h"
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("eth ptchservice", "eXaui commands")
TOP_SDKCOMMAND("eth ptchservice ingress", "ingress eXaui commands")
TOP_SDKCOMMAND("eth ptchservice egress", "egress eXaui commands")

DEF_PTYPE("eAtPtchServiceType", "select", "cem(1), imsg_eop(2), exaui0(3), exaui1(4)", "PTCH service type")
DEF_PTYPE("eAtPtchServiceIngressMode", "select", "1-byte(1), 2-bytes(2)", "PTCH insertion modes: 1-byte or 2-bytes")

DEF_SDKCOMMAND(cliAtPtchServiceIngressOffsetSet,
               "eth ptchservice ingress offset",
               "Congifure offset value for Direction from HyPhy --> XFI.\n",
               DEF_SDKPARAM("ptchservice", "eAtPtchServiceType", "")
               DEF_SDKPARAM("offset", "STRING", "Offset value"),
               "${ptchservice} ${offset}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtchServiceEgressOffsetSet,
               "eth ptchservice egress offset",
               "Congifure offset value for Direction from XFI --> HyPhy.\n",
               DEF_SDKPARAM("ptchservice", "eAtPtchServiceType", "")
               DEF_SDKPARAM("offset", "STRING", "Offset value"),
               "${ptchservice} ${offset}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtchServiceIngressPtchSet,
               "eth ptchservice ingress ptch",
               "Congifure PTCH ID for Direction from HyPhy --> XFI.\n",
               DEF_SDKPARAM("ptchservice", "eAtPtchServiceType", "")
               DEF_SDKPARAM("ptch", "STRING", "PTCH ID"),
               "${ptchservice} ${ptch}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtchServiceEgressPtchSet,
               "eth ptchservice egress ptch",
               "Congifure PTCH ID for Direction from XFI --> HyPhy.\n",
               DEF_SDKPARAM("ptchservice", "eAtPtchServiceType", "")
               DEF_SDKPARAM("ptch", "STRING", "PTCH ID"),
               "${ptchservice} ${ptch}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtchServiceShow,
               "show eth ptchservice",
               "Show configuration of PTCH services.\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtchServiceCounterShow,
               "show eth ptchservice counters",
               "Show counters of PTCH services.\n",
               DEF_SDKPARAM("readmode", "HistoryReadingMode", ""),
               "${readmode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtchServiceEnable,
               "eth ptchservice enable",
               "Enable PTCH service\n",
               DEF_SDKPARAM("ptchservice", "eAtPtchServiceType", ""),
               "${ptchservice}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtchServiceDisable,
               "eth ptchservice disable",
               "Disable PTCH service\n",
               DEF_SDKPARAM("ptchservice", "eAtPtchServiceType", ""),
               "${ptchservice}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPtchServiceIngressModeSet,
               "eth ptchservice ingress mode",
               "Configure PTCH insertion mode for per services. \n",
               DEF_SDKPARAM("ptchservice", "eAtPtchServiceType", "")
               DEF_SDKPARAM("mode", "eAtPtchServiceIngressMode", ""),
               "${ptchservice} ${mode}")
    {
    mAtCliCall();
    }

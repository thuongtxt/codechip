/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : ClishAtEthPortFlowControl.c
 *
 * Created Date: Feb 18, 2014
 *
 * Description : ETH Port flow control
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("show eth port", "Show Ethernet Port Information")
TOP_SDKCOMMAND("eth port flowcontrol pauseframe", "Flow Control for Ethernet flow")

DEF_SDKCOMMAND(cliEthPortFlowControlGet,
               "show eth port flowcontrol",
               "Show configuration of Ethernet flow control",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortFlowControlCountersGet,
               "show eth port flowcontrol counters",
               "Show eth port flow control counters",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${portList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortFlowControlPauseFrameDestMacSet,
               "eth port flowcontrol pauseframe destmac",
               "Set DMAC Address for port flow control pause frame",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("mac", "MacAddr", "Mac Address"),
               "${portList} ${mac}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortFlowControlPauseFrameSrcMacSet,
               "eth port flowcontrol pauseframe srcmac",
               "Set SMAC Address for port flow control pause frame",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("mac", "MacAddr", "Mac Address"),
               "${portList} ${mac}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortFlowControlPauseFrameEthTypeSet,
               "eth port flowcontrol pauseframe ethtype",
               "Set Ethernet type for port flow control pause frame",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("ethType", "STRING", "Ethernet type"),
               "${portList} ${ethType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortFlowControlHighThresholdSet,
               "eth port flowcontrol highthreshold",
               "Set high threshold for Ethernet port flow control",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("threshold", "STRING", "High threshold"),
               "${portList} ${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortFlowControlLowThresholdSet,
               "eth port flowcontrol lowthreshold",
               "Set low threshold for Ethernet port flow control",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("threshold", "STRING", "Low threshold"),
               "${portList} ${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortFlowControlPauseFramePeriodSet,
               "eth port flowcontrol pauseframe period",
               "Set period for port flow control pause frame",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("timeInterval", "STRING", "PAUSE frame period in ms"),
               "${portList} ${timeInterval}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortFlowControlPauseFrameQuantaSet,
               "eth port flowcontrol pauseframe quanta",
               "Set number of quantas for port flow control pause frame",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("quanta", "STRING", "Number PAUSE frame quantas in quanta unit"),
               "${portList} ${quanta}")
    {
    mAtCliCall();
    }


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : atcliethport.c
 *
 * Created Date: Oct 10, 2012
 *
 * Description : ETH Port CLISH CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "../AtClish.h"
#include "../physical/ClishAtEyeScan.h"
#include "../../physical/CliAtEyeScan.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/* Top commands */
TOP_SDKCOMMAND("eth port drp", "Ethernet port eye scan DRP CLIs\n")
TOP_SDKCOMMAND("eth port eyescan stepsize", "Ethernet port eye scan Step size\n")
TOP_SDKCOMMAND("eth port eyescan debug", "Ethernet port eye scan debug\n")


DEF_SDKCOMMAND(cliAtEthPortEyeScan,
               "eth port eyescan",
               "Start eye scanning",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    AtList controllers;
    char *idStr = AtStrdup(lub_argv__get_arg(argv, 0));
    AtUnused(context);

    /* Register listener */
    controllers = CliAtEthPortSerdesEyeScanControllerFromIdString(idStr);
    CliAtEyeScanControllerDefaultListenerRegister(controllers);
    AtObjectDelete((AtObject)controllers);
    AtOsalMemFree(idStr);

    /* Enable task */
    ClishEnableEyeScanTask();

    return 0;
    }

DEF_SDKCOMMAND(cliAtEthPortEyeScanLaneDrpRead,
               "eth port drp rd",
               "Read DRP register",
               DEF_SDKPARAM("port.laneId", "STRING", "Lane ID")
               DEF_SDKPARAM("drpAddress", "RegisterAddress", "")
               DEF_SDKPARAM_OPTION("pretty", "RegisterDisplayFormat", ""),
               "${port.laneId} ${drpAddress} ${pretty}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortEyeScanLaneDrpWrite,
               "eth port drp wr",
               "Write DRP register",
               DEF_SDKPARAM("port.laneId", "STRING", "")
               DEF_SDKPARAM("drpAddress", "RegisterAddress", "")
               DEF_SDKPARAM("value", "RegisterValue", ""),
               "${port.laneId} ${drpAddress} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortEyeScanLaneStepSizeHorizonalSet,
               "eth port eyescan stepsize horizonal",
               "Change horizontal step",
               DEF_SDKPARAM("lanes", "EthPortEyeScanLanes", "")
               DEF_SDKPARAM("stepSize", "UINT", "Horizontal step size"),
               "${lanes} ${stepSize}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortEyeScanLaneStepSizeVerticalSet,
               "eth port eyescan stepsize vertical",
               "Change vertical step",
               DEF_SDKPARAM("lanes", "EthPortEyeScanLanes", "")
               DEF_SDKPARAM("stepSize", "UINT", "Vertical step size"),
               "${lanes} ${stepSize}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortEyeScanLaneMaxPrescaleSet,
               "eth port eyescan maxPrescale",
               "Change max Prescale",
               DEF_SDKPARAM("lanes", "EthPortEyeScanLanes", "")
               DEF_SDKPARAM("maxPrescale", "UINT", "Max prescale"),
               "${lanes} ${maxPrescale}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortEyeScanLaneDatawidthSet,
               "eth port eyescan datawidth",
               "Change data width",
               DEF_SDKPARAM("lanes", "EthPortEyeScanLanes", "")
               DEF_SDKPARAM("dataWidth", "UINT", "Data width"),
               "${lanes} ${dataWidth}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortEyeScanLaneShow,
               "show eth port eyescan",
               "Show Eye scanning information",
               DEF_SDKPARAM("lanes", "EthPortEyeScanLanes", ""),
               "${lanes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortEyeScanLaneEnable,
               "eth port eyescan enable",
               "Enable Eye scanning",
               DEF_SDKPARAM("lanes", "EthPortEyeScanLanes", ""),
               "${lanes}")
    {
    int result = mAtCliResult();
    AtUnused(context);
    AtUnused(argv);
    ClishEnableEyeScanTask();
    return result;
    }

DEF_SDKCOMMAND(cliAtEthPortEyeScanLaneDisable,
               "eth port eyescan disable",
               "Disable Eye scanning",
               DEF_SDKPARAM("lanes", "EthPortEyeScanLanes", ""),
               "${lanes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortEyeScanControllerEqualizerSet,
               "eth port eyescan equalizer",
               "Change Equalizer",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("mode", "eAtEyeScanEqualizerMode", "Equalizer mode"),
               "${portList} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortEyeScanLaneDebugEnable,
               "eth port eyescan debug enable",
               "Enable eye-scan internal state machine debug",
               DEF_SDKPARAM("lanes", "EthPortEyeScanLanes", ""),
               "${lanes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortEyeScanLaneDebugDisable,
               "eth port eyescan debug disable",
               "Disable eye-scan internal state machine debug",
               DEF_SDKPARAM("lanes", "EthPortEyeScanLanes", ""),
               "${lanes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortEyeScanLaneDebug,
               "debug eth port eyescan lane",
               "Show lane debug information",
               DEF_SDKPARAM("lanes", "EthPortEyeScanLanes", ""),
               "${lanes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortEyeScanControllerDebug,
               "debug eth port eyescan",
               "Show eye-scan debug information",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    mAtCliCall();
    }

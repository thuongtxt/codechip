/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : ClishAtEthFlowFlowControl.c
 *
 * Created Date: Dec 6, 2013
 *
 * Description : Flow control CLIs for ETH Flow
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("eth flow flowcontrol", "Flow Control for Ethernet flow")

DEF_SDKCOMMAND(cliEthFlowFlowControlDestMacSet,
               "eth flow flowcontrol destmac",
               "Set flow control DMAC Address",
               DEF_SDKPARAM("flowList", "FlowIdList", "")
               DEF_SDKPARAM("mac", "MacAddr", "Mac Address"),
               "${flowList} ${mac}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthFlowFlowControlSrcMacSet,
               "eth flow flowcontrol srcmac",
               "Set flow control SMAC Address",
               DEF_SDKPARAM("flowList", "FlowIdList", "")
               DEF_SDKPARAM("mac", "MacAddr", "Mac Address"),
               "${flowList} ${mac}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthFlowFlowControlEthTypeSet,
               "eth flow flowcontrol ethtype",
               "Set flow control Ethernet type",
               DEF_SDKPARAM("flowList", "FlowIdList", "")
               DEF_SDKPARAM("ethType", "STRING", "Ethernet type"),
               "${flowList} ${ethType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthFlowFlowControlHighThresholdSet,
               "eth flow flowcontrol highthreshold",
               "Set high threshold for Ethernet flow control",
               DEF_SDKPARAM("flowList", "FlowIdList", "")
               DEF_SDKPARAM("threshold", "STRING", "High threshold"),
               "${flowList} ${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthFlowFlowControlLowThresholdSet,
               "eth flow flowcontrol lowthreshold",
               "Set low threshold for Ethernet flow control",
               DEF_SDKPARAM("flowList", "FlowIdList", "")
               DEF_SDKPARAM("threshold", "STRING", "Low threshold"),
               "${flowList} ${threshold}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthFlowFlowControlPauseFramePeriodSet,
               "eth flow flowcontrol period",
               "Set PAUSE frame period",
               DEF_SDKPARAM("flowList", "FlowIdList", "")
               DEF_SDKPARAM("timeInterval", "STRING", "PAUSE frame period in ms"),
               "${flowList} ${timeInterval}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthFlowFlowControlGet,
               "show eth flow flowcontrol",
               "Show configuration of Ethernet flow control",
               DEF_SDKPARAM("flowList", "FlowIdList", ""),
               "${flowList}")
    {
    mAtCliCall();
    }

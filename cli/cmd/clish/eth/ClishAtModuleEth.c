/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : ETH
 *
 * File        : ClishAtModuleEth.c
 *
 * Created Date: Sep 21, 2015
 *
 * Description : ETH module CLISH command lines implement.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("eth interrupt", "ETH module interrupt")
TOP_SDKCOMMAND("eth default", "CLI to control ETH Module default settings")

DEF_SDKCOMMAND(cliEthInterruptEnable,
               "eth interrupt enable",
               "Enable interrupt",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtModuleEthShow,
               "show module eth",
               "Show ETH Module information\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthInterruptDisable,
               "eth interrupt disable",
               "Disable interrupt",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtModuleEthDefaultCVlanTpIdSet,
               "eth default cvlantpid",
               "Change default C-VLAN TPID\n",
               DEF_SDKPARAM_OPTION("cVlanTpid", "STRING", "C-VLAN TPID"),
               "${cVlanTpid}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtModuleEthDefaultSVlanTpIdSet,
               "eth default svlantpid",
               "Change default S-VLAN TPID\n",
               DEF_SDKPARAM_OPTION("sVlanTpid", "STRING", "S-VLAN TPID"),
               "${sVlanTpid}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliModuleEthShow,
               "show module eth",
               "Show ETH module information",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : atcliethflow.c
 *
 * Created Date: Oct 10, 2012
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/
#define vlanStringMin "0.0.0"
#define vlanStringMax "7.1.4095"
#define regFlowno "1..2048"
#define regVlanPriono "0..7" /* value 0-7 */
#define regVlanCfino "0..1" /* value 0-1 */
#define regVlanIdno "0..4095" /* value 0-4095 */
#define regPortno "1..2"
#define flowLstElement "(("regFlowno"[-]"regFlowno")|"regFlowno")"
#define flowLst "(("flowLstElement"[,])+"flowLstElement"|"flowLstElement")"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("eth flow", "Flow")
TOP_SDKCOMMAND("eth flow ingress", "Flow Ingress commands")
TOP_SDKCOMMAND("eth flow ingress vlan", "Flow Ingress VLAN commands")
TOP_SDKCOMMAND("eth flow egress", "Flow Egress commands")
TOP_SDKCOMMAND("eth flow egress svlan", "SVLAN Type")
TOP_SDKCOMMAND("eth flow egress svlan edit", "SVLAN type edition")
TOP_SDKCOMMAND("eth flow egress cvlan", "CVLAN type edition")
TOP_SDKCOMMAND("eth flow egress cvlan edit", "CVLAN Type")
TOP_SDKCOMMAND("debug eth", "Debug Ethernet flow")
TOP_SDKCOMMAND("show eth", "Show Ethernet Flow Information")
TOP_SDKCOMMAND("show eth flow vlan", "Show Ethernet Flow VLAN Information")

DEF_PTYPE("FlowIdList", "regexp", ".*", "Flow ID")
DEF_PTYPE("PortId", "integer", regPortno, "Port ID")
DEF_PTYPE("VlanDesc", "regexp", ".*", "VLAN descriptor. Its format is: PRI.CFI.VLANID (or none)")
DEF_PTYPE("eAtEthFlowType", "select", "nop(0), eop(1), eth(2)", "Flow Type")
DEF_PTYPE("MacAddr", "regexp", ".*", "MAC Address")
DEF_PTYPE("VlanType", "regexp", ".*", "VLAN Type")

DEF_SDKCOMMAND(cliEthFlowCreate,
               "eth flow create",
               "Create Ethernet flow.\n",
               DEF_SDKPARAM("flowList", "FlowIdList", ""),
               "${flowList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthFlowEnable,
               "eth flow enable",
               "\nThis command is to enable flow list.\n",
               DEF_SDKPARAM("flowList", "FlowIdList", ""),
               "${flowList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthFlowDisable,
               "eth flow disable",
               "\nThis command is to disable flow list.\n",
               DEF_SDKPARAM("flowList", "FlowIdList", ""),
               "${flowList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthFlowDelete,
               "eth flow delete",
               "\nThis command is to delete flow list.\n",
               DEF_SDKPARAM("flowList", "FlowIdList", ""),
               "${flowList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthFlowEgDestMacSet,
               "eth flow egress destmac",
               "",
               DEF_SDKPARAM("flowList", "FlowIdList", "")
               DEF_SDKPARAM("mac", "MacAddr", ""),
               "${flowList} ${mac}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthFlowEgSrcMacSet,
               "eth flow egress srcmac",
               "\nThis command is to set source MAC for each flow\n",
               DEF_SDKPARAM("flowList", "FlowIdList", "")
               DEF_SDKPARAM("mac", "MacAddr", ""),
               "${flowList} ${mac}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthFlowEgVlanSet,
               "eth flow egress vlan",
               "",
               DEF_SDKPARAM("flowList", "FlowIdList", "")
               DEF_SDKPARAM("portId", "PortId", "")
               DEF_SDKPARAM("cVlan", "VlanDesc", "")
               DEF_SDKPARAM("sVlan", "VlanDesc", ""),
               "${flowList} ${portId} ${cVlan} ${sVlan}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthFlowGet,
               "show eth flow",
               "",
               DEF_SDKPARAM("flowList", "FlowIdList", ""),
               "${flowList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthFlowIgVlanAdd,
               "eth flow ingress vlan add",
               "",
               DEF_SDKPARAM("flowList", "FlowIdList", "")
               DEF_SDKPARAM("portId", "PortId", "")
               DEF_SDKPARAM("cVlan", "VlanDesc", "")
               DEF_SDKPARAM("sVlan", "VlanDesc", ""),
               "${flowList} ${portId} ${cVlan} ${sVlan}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthFlowIgVlanRemove,
               "eth flow ingress vlan remove",
               "",
               DEF_SDKPARAM("flowList", "FlowIdList", "")
               DEF_SDKPARAM("portId", "PortId", "")
               DEF_SDKPARAM("cVlan", "VlanDesc", "")
               DEF_SDKPARAM("sVlan", "VlanDesc", ""),
               "${flowList} ${portId} ${cVlan} ${sVlan}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthFlowEgVlanAdd,
               "eth flow egress vlan add",
               "",
               DEF_SDKPARAM("flowList", "FlowIdList", "")
               DEF_SDKPARAM("portId", "PortId", "")
               DEF_SDKPARAM("cVlan", "VlanDesc", "")
               DEF_SDKPARAM("sVlan", "VlanDesc", ""),
               "${flowList} ${portId} ${cVlan} ${sVlan}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthFlowEgVlanRemove,
               "eth flow egress vlan remove",
               "",
               DEF_SDKPARAM("flowList", "FlowIdList", "")
               DEF_SDKPARAM("portId", "PortId", "")
               DEF_SDKPARAM("cVlan", "VlanDesc", "")
               DEF_SDKPARAM("sVlan", "VlanDesc", ""),
               "${flowList} ${portId} ${cVlan} ${sVlan}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthFlowCounterGet,
               "show eth flow counters",
               "",
               DEF_SDKPARAM("flowList", "FlowIdList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
			   DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
			   "${flowList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthFlowDebug,
               "debug eth flow",
               "Show all of debugging information",
               DEF_SDKPARAM("flowList", "FlowIdList", ""),
               "${flowList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthFlowEgressCVlanType,
               "eth flow egress cvlan tpid",
               "Set CVLAN type",
               DEF_SDKPARAM("flowList", "FlowIdList", "")
               DEF_SDKPARAM("cVlanType", "VlanType", ""),
               "${flowList} ${cVlanType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthFlowEgressSVlanType,
               "eth flow egress svlan tpid",
               "Set SVLAN type",
               DEF_SDKPARAM("flowList", "FlowIdList", "")
               DEF_SDKPARAM("sVlanType", "VlanType", ""),
               "${flowList} ${sVlanType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthFlowVlanTypeGet,
               "show eth flow vlan tpid",
               "Get VLAN type",
               DEF_SDKPARAM("flowList", "FlowIdList", ""),
               "${flowList}")
    {
    mAtCliCall();
    }

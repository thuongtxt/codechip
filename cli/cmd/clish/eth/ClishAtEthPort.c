/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : atcliethport.c
 *
 * Created Date: Oct 10, 2012
 *
 * Description : ETH Port CLISH CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "../AtClish.h"
#include "../physical/ClishAtEyeScan.h"
#include "../../physical/CliAtEyeScan.h"

/*--------------------------- Define -----------------------------------------*/
#define regPortno "1..2"
#define regTxIpgno "0..31"  /* value 0-31 */
#define regRxIpgno "4..15"  /* value 0-15 */
#define regPauFrmIntvlno "0..32768"  /* value 0-32768 */
#define regPauFrmExpTimeno "0..32768"  /* value 0-32768 */
#define portLstElement "(("regPortno"[-]"regPortno")|"regPortno")"
#define portLst "(("portLstElement"[,])+"portLstElement"|"portLstElement")"
#define portDefType "(los|txfault|linkdown)"
#define portDefMask "((("portDefType"[\\|])+"portDefType")|"portDefType")"
#define EthPortDefType "(up|down|los|sync|realign|autoneg_state_change)"
#define EthPortDefMask "((("EthPortDefType"[\\|])+"EthPortDefType")|"EthPortDefType")"
#define EthPortDropPktType "(pcs-error|fcs-error|undersized|oversized|pause-frame|loop-da)"
#define EthPortDropPktMask "((("EthPortDropPktType"[\\|])+"EthPortDropPktType")|"EthPortDropPktType")"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/* Top commands */
TOP_SDKCOMMAND("eth", "Port")
TOP_SDKCOMMAND("eth port", "Port")
TOP_SDKCOMMAND("test eth port", "Test Port")
TOP_SDKCOMMAND("show eth", "Show Ethernet Port Information")
TOP_SDKCOMMAND("eth port pauseframe", "Ethernet Port Pause Frame Time")
TOP_SDKCOMMAND("eth isis", "Etherner ISIS")
TOP_SDKCOMMAND("show eth isis", "Show Etherner ISIS")
TOP_SDKCOMMAND("eth port serdes rx", "SERDES configuration\n")
TOP_SDKCOMMAND("eth port serdes tx", "SERDES configuration\n")
TOP_SDKCOMMAND("eth port serdes", "SERDES configuration\n")
TOP_SDKCOMMAND("eth port serdes equalizer", "SERDES equalizer configuration\n")
TOP_SDKCOMMAND("eth port higig", "Ethernet port Higig mode CLIs\n")
TOP_SDKCOMMAND("eth port serdes drp", "Ethernet port SERDES eye scan DRP CLIs\n")
TOP_SDKCOMMAND("eth port serdes eyescan stepsize", "Ethernet port SERDES eye scan Step size\n")
TOP_SDKCOMMAND("eth port serdes eyescan debug", "Ethernet port SERDES eye scan debug\n")
TOP_SDKCOMMAND("eth port log", "Ethernet port logging CLIs\n")
TOP_SDKCOMMAND("eth port serdes notification", "Ethernet port SEDES notification CLIs\n")
TOP_SDKCOMMAND("eth port serdes force", "Ethernet port SEDES Force CLIs\n")
TOP_SDKCOMMAND("eth port autoneg", "CLIs to control Auto Negotiation")
TOP_SDKCOMMAND("eth port alarm", "Ethernet port capture reported alarms")
TOP_SDKCOMMAND("eth port autoneg restart", "CLI to control autoneg restarting")
TOP_SDKCOMMAND("diag", "Diagnostic CLIs\n")
TOP_SDKCOMMAND("diag eth", "Diagnostic Ethernet CLIs\n")
TOP_SDKCOMMAND("diag eth port", "Diagnostic Ethernet Port CLIs\n")

TOP_SDKCOMMAND("eth port tx", "\nEthernet port error generator CLIs\n")
TOP_SDKCOMMAND("eth port tx error", "\nEthernet port error generator CLIs\n")
TOP_SDKCOMMAND("eth port tx error generator", "\nEthernet port error generator CLIs\n")
TOP_SDKCOMMAND("show eth port tx", "\nShow configuration of TX Ethernet error generator\n")
TOP_SDKCOMMAND("show eth port tx error", "\nShow configuration of TX Ethernet error generator\n")
TOP_SDKCOMMAND("eth port rx", "\nEthernet port error generator CLIs\n")
TOP_SDKCOMMAND("eth port rx error", "\nEthernet port error generator CLIs\n")
TOP_SDKCOMMAND("eth port rx error generator", "\nEthernet port error generator CLIs\n")
TOP_SDKCOMMAND("show eth port rx", "\nShow configuration of RX Ethernet error generator\n")
TOP_SDKCOMMAND("show eth port rx error", "\nShow configuration of RX Ethernet error generator\n")

TOP_SDKCOMMAND("eth port packet", "\nEthernet port packet CLIs\n")
TOP_SDKCOMMAND("eth port packet drop", "\nEthernet port drop packet CLIs\n")
TOP_SDKCOMMAND("show eth port packet", "\nShow configuration of packet ETH\n")
TOP_SDKCOMMAND("show eth port packet drop", "\nShow configuration of drop packet ETH\n")
TOP_SDKCOMMAND("eth port fec", "CLIs to handle FEC")
TOP_SDKCOMMAND("eth port force", "CLIs to handle alarm forcing")
TOP_SDKCOMMAND("eth port unforce", "CLIs to handle alarm unforcing")
TOP_SDKCOMMAND("eth port vlan", "\nEthernet port VLAN CLIs\n")

TOP_SDKCOMMAND("eth port clock", "\nEthernet port clock CLIs\n")
TOP_SDKCOMMAND("eth port clock monitor", "\nEthernet port clock monitor CLIs\n")
TOP_SDKCOMMAND("show eth port clock", "\nShow configuration clock monitor\n")

TOP_SDKCOMMAND("eth port link", "\nEthernet port link CLIs\n")
TOP_SDKCOMMAND("eth port link training", "\nEthernet port link training CLIs\n")

DEF_PTYPE("PortIdList", "regexp", ".*", "Port ID or lane ID")
DEF_PTYPE("eAtEthPortSpeed", "select", "10m(1), 100m(2), 1000m(3), 2500m(4), 10g(5)", "Ethernet Port speed")
DEF_PTYPE("eAtEthPortDuplex", "select", "halfduplex(0), fullduplex(1), autodetect(2)", "Ethernet Port Duplex Mode")
DEF_PTYPE("eAtEthPortInterface", "select", "1000basex(1), 2500basex(2), sgmii(3), qsgmii(4), rgmii(5), gmii(6), mii(7), xgmii(8), xaui(9), base-r(10), base-kr(11), base-fx(12)", "Ethernet Port Interface Mode")

DEF_PTYPE("eAtQueueMode", "select", "dwrr(1), strictprio(2)", "Ethernet Port Queue Mode")
DEF_PTYPE("eAtSerdesEqualizerMode", "select", "dfe(1), lpm(2)", "Ethernet Port serdes equalizer mode")
DEF_PTYPE("TxIpgVal", "integer", regTxIpgno, "Tx IPG Value")
DEF_PTYPE("RxIpgVal", "integer", regRxIpgno, "Rx IPG Value")
DEF_PTYPE("PauFrmIntvlVal", "integer", regPauFrmIntvlno, "Pause Frame Interval Value")
DEF_PTYPE("PauFrmExpTimeVal", "integer", regPauFrmExpTimeno, "Pause Frame Expire Value")
DEF_PTYPE("Ipv4Addr", "regexp", ".*", "IPv4 Address")
DEF_PTYPE("Ipv6Addr", "regexp", ".*", "IPv6 Address")
DEF_PTYPE("eAtEyeScanEqualizerMode", "select", "dfe(0), lpm(1)", "Eye Scan Equalizer Mode")
DEF_PTYPE("eAtEyeScanRate", "select", "full(0), half(1), quarter(2), octal(3), hex(4)", "Eye Scan rate")
DEF_PTYPE("EthPortEyeScanLanes", "regexp", ".*", "List of eye scan lanes, format <portId>.<laneId>")
DEF_PTYPE("SerdesDefMask", "regexp", portDefMask,
          "\nSERDES alarm masks:\n"
          "- los    : Loss of Sigal\n"
          "- txfault: Transmission fault\n"
          "- linkdown: Link Down\n"
          "Note, these masks can be ORed, example los|linkdown\n")
DEF_PTYPE("eAtSerdesAlarmType", "select", "los(0), txfault(1), linkdown(2)", "\nSERDES alarms\n")

DEF_PTYPE("EthPortAlarmType", "regexp", ".*",
          "Port interrupt mask:\n"
          "+ up\n"
          "+ down\n"
          "+ los\n"
          "+ tx-fault\n"
          "+ tx-underflow-error\n"
          "+ local-fault\n"
          "+ remote-fault\n"
          "+ rx-internal-fault\n"
          "+ rx-received-local-fault\n"
          "+ rx-aligned-error\n"
          "+ rx-mis-aligned\n"
          "+ rx-truncated\n"
          "+ rx-hi-ber\n"
          "+ excessive-error-ratio\n"
          "+ loss-of-clock\n"
          "+ frequency-out-of-range\n"
          "+ loss-of-data-sync\n"
          "+ loss-of-frame\n"
          "+ auto-neg-parallel-detection-fault\n"
          "+ auto-neg-state-change\n"
          "\n"
          "Subport interrupt mask:\n"
          "+ rx-framing-error\n"
          "+ rx-synced-error\n"
          "+ rx-mf-len-error\n"
          "+ rx-mf-repeat-error\n"
          "+ rx-mf-error\n"
          "+ rx-bip-error\n"
          "+ auto-neg-fec-inc-cant-correct\n"
          "+ auto-neg-fec-inc-correct\n"
          "+ auto-neg-fec-lock-error\n"
          "Note, these masks can be ORed, example up|down\n")

DEF_PTYPE("EthPortDropPktType", "regexp", EthPortDropPktMask,
          "\nalarm masks type:\n"
          "- pcs-error                : PCS Error\n"
          "- fcs-error                : FCS Error\n"
          "- undersized               : Undersized\n"
          "- oversized                : Oversized\n"
          "- pause-frame              : Pause frame\n"
          "- loop-da                  : Loop DA\n"
          "Note, these masks can be ORed, example pcs_error|fcs_error\n")

DEF_PTYPE("eAtClockMonitorThreshold", "regexp", ".*",
          "\nClock threshold type:\n"
          "- loss-of-clock: threshold to declare loss of clock\n"
          "- frequency-out-of-range: threshold to declare frequency out of range\n"
          "Threshold types are separated by \',\'")

DEF_SDKCOMMAND(cliEthPortSpeedSet,
               "eth port speed",
               "",
               DEF_SDKPARAM("portId", "PortIdList", "")
               DEF_SDKPARAM("speed", "eAtEthPortSpeed", ""),
               "${portId} ${speed}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortDuplexSet,
               "eth port duplex",
               "",
               DEF_SDKPARAM("portId", "PortIdList", "")
               DEF_SDKPARAM("duplex", "eAtEthPortDuplex", ""),
               "${portId} ${duplex}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortTxIpgSet,
               "eth port txipg",
               "",
               DEF_SDKPARAM("portId", "PortIdList", "")
               DEF_SDKPARAM("txipg", "TxIpgVal", ""),
               "${portId} ${txipg}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortRxIpgSet,
               "eth port rxipg",
               "",
               DEF_SDKPARAM("portId", "PortIdList", "")
               DEF_SDKPARAM("rxipg", "RxIpgVal", ""),
               "${portId} ${rxipg}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortPauFrmIntvlSet,
               "eth port pauseframe interval",
               "",
               DEF_SDKPARAM("portId", "PortIdList", "")
               DEF_SDKPARAM("pauFrmIntvl", "PauFrmIntvlVal", ""),
               "${portId} ${pauFrmIntvl}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortPauFrmExpTimeSet,
               "eth port pauseframe expire",
               "",
               DEF_SDKPARAM("portId", "PortIdList", "")
               DEF_SDKPARAM("pauFrmExpTime", "PauFrmExpTimeVal", ""),
               "${portId} ${pauFrmExpTime}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortFlowCtrl,
               "eth port flowcontrol",
               "",
               DEF_SDKPARAM("portId", "PortIdList", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${portId} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortAutoNegEnable,
               "eth port autoneg enable",
               "Enable Auto Negotiation",
               DEF_SDKPARAM("portId", "PortIdList", ""),
               "${portId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortAutoNegDisable,
               "eth port autoneg disable",
               "Disable Auto Negotiation",
               DEF_SDKPARAM("portId", "PortIdList", ""),
               "${portId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortCfgGet,
               "show eth port",
               "",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortCounterGet,
               "show eth port counters",
               "",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${portList}, ${readingMode}, ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortAlarmGet,
               "show eth port alarm",
               "",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortIntrGet,
               "show eth port interrupt",
               "",
               DEF_SDKPARAM("portId", "PortIdList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
			   DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${portId} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortSrcMac,
               "eth port srcmac",
               "",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("mac", "MacAddr", ""),
               "${portId} ${mac}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortDestMac,
               "eth port destmac",
               "Set Destination MAC Address",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("mac", "MacAddr", ""),
               "${portId} ${mac}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortInterfaceSet,
               "eth port interface",
               "",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("interface", "eAtEthPortInterface", ""),
               "${portList} ${interface}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortMacCheck,
               "eth port maccheck",
               "",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${portList} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortLedState,
               "eth port ledstate",
               "",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("ledstate", "eAtLedState", ""),
               "${portList} ${ledstate}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortPacketDump,
               "eth port packetdump",
               "",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("direct", "eAtDirection", "")
               DEF_SDKPARAM("numBytes", "UINT", ""),
               "${portList} ${ipv6addr} ${numNytes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliEthPortLoopbackSet,
               "eth port loopback",
               "",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("loopMode", "eAtLoopbackMode", ""),
               "${portList} ${loopMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortEnable,
               "eth port enable",
               "Enable specified ports",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortDisable,
               "eth port disable",
               "Disable specified ports",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliModuleEthISISMacSet,
               "eth isis mac",
               "\nThis command is used to set MAC for ISIS packet.\n",
               DEF_SDKPARAM("macAddress", "MacAddr", ""),
               "${macAddress}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliModuleEthISISMacGet,
               "show eth isis mac",
               "\nThis command is used to show MAC for ISIS packet.\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesControllerInit,
               "eth port serdes init",
               "Initialize SERDES",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesControllerEnable,
               "eth port serdes enable",
               "Enable SERDES",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesControllerDisable,
               "eth port serdes disable",
               "Disable SERDES",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesPhyParamSet,
               "eth port serdes param",
               "Change SERDES physical parameter",
               DEF_SDKPARAM("serdesList", "SerdesIdList", "")
               DEF_SDKPARAM("paramId", "eAtSerdesParam", "")
               DEF_SDKPARAM("value", "STRING", "Value"),
               "${serdesList} ${paramId} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesShow,
               "show eth port serdes",
               "Show SERDES information",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesAlarmShow,
               "show eth port serdes alarm",
               "Show SERDES alarm",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesAlarmHistoryShow,
               "show eth port serdes alarm history",
               "Show SERDES history alarm changed.",
               DEF_SDKPARAM("serdesList", "SerdesIdList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${serdesList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesParamShow,
               "show eth port serdes param",
               "Show value of specified SERDES parameter",
               DEF_SDKPARAM("serdesList", "SerdesIdList", "")
               DEF_SDKPARAM_OPTION("paramId", "eAtSerdesParam", ""),
               "${serdesList} ${paramId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesDebug,
               "debug eth port serdes",
               "Show SERDES debug information",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesLoopback,
               "eth port serdes loopback",
               "Loopback SERDES",
               DEF_SDKPARAM("serdesList", "SerdesIdList", "")
               DEF_SDKPARAM("loopbackMode", "eAtLoopbackMode", ""),
               "${serdesList} ${loopbackMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortMaxPacketSize,
               "eth port maxpacketsize",
               "Set maximum packet size",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("sizeInBytes", "UINT", "Packet size in bytes\n"),
               "${portList} ${sizeInBytes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortMinPacketSize,
               "eth port minpacketsize",
               "Set minimum packet size",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("sizeInBytes", "UINT", "Packet size in bytes\n"),
               "${portList} ${sizeInBytes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortDebug,
               "debug eth port",
               "Show all of debugging information",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSwitch,
               "eth port switch",
               "Switch traffic which is selected from one port to another port",
               DEF_SDKPARAM("fromPortId", "UINT", "Source port")
               DEF_SDKPARAM("toPortId", "UINT", "Destination port"),
               "${fromPortId} ${toPortId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortBridge,
               "eth port bridge",
               "Bridge traffic which is currently transmitted by one port to another port",
               DEF_SDKPARAM("fromPortId", "UINT", "Source port")
               DEF_SDKPARAM("toPortId", "UINT", "Destination port"),
               "${fromPortId} ${toPortId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSwitchRelease,
               "eth port switch release",
               "Switch traffic back to the input ports",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortBridgeRelease,
               "eth port bridge release",
               "Release bridging traffic between two ports",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortApsGet,
               "show eth port aps",
               "Show APS configuration for ports",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortHigigEnable,
               "eth port higig enable",
               "Enable Higig mode",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortHigigDisable,
               "eth port higig disable",
               "Disable Higig mode",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortIpv4Set,
           "eth port ipv4",
           "Set IPv4 Address",
           DEF_SDKPARAM("portList", "PortIdList", "")
           DEF_SDKPARAM("ipv4addr", "Ipv4Addr", ""),
           "${portList} ${ipv4addr}")
    {
    mAtCliCall();
    }


DEF_SDKCOMMAND(cliEthPortIpv6Set,
               "eth port ipv6",
               "Set IPv6 Address",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("ipv6addr", "Ipv6Addr", ""),
               "${portList} ${ipv6addr}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesEyeScan,
               "eth port serdes eyescan",
               "Start SERDES eye scanning",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    AtList controllers;
    char *idStr = AtStrdup(lub_argv__get_arg(argv, 0));
    AtUnused(context);

    /* Register listener */
    controllers = CliAtEthPortSerdesEyeScanControllerFromIdString(idStr);
    CliAtEyeScanControllerDefaultListenerRegister(controllers);
    AtObjectDelete((AtObject)controllers);
    AtOsalMemFree(idStr);

    /* Enable task */
    ClishEnableEyeScanTask();

    return 0;
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesEyeScanLaneDrpRead,
               "eth port serdes drp rd",
               "Read DRP register",
               DEF_SDKPARAM("port.laneId", "STRING", "Lane ID")
               DEF_SDKPARAM("drpAddress", "RegisterAddress", "")
               DEF_SDKPARAM_OPTION("pretty", "RegisterDisplayFormat", ""),
               "${port.laneId} ${drpAddress} ${pretty}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesEyeScanLaneDrpWrite,
               "eth port serdes drp wr",
               "Write DRP register",
               DEF_SDKPARAM("port.laneId", "STRING", "")
               DEF_SDKPARAM("drpAddress", "RegisterAddress", "")
               DEF_SDKPARAM("value", "RegisterValue", ""),
               "${port.laneId} ${drpAddress} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesEyeScanLaneStepSizeHorizonalSet,
               "eth port serdes eyescan stepsize horizonal",
               "Change horizontal step",
               DEF_SDKPARAM("lanes", "EthPortEyeScanLanes", "")
               DEF_SDKPARAM("stepSize", "UINT", "Horizontal step size"),
               "${lanes} ${stepSize}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesEyeScanLaneStepSizeVerticalSet,
               "eth port serdes eyescan stepsize vertical",
               "Change vertical step",
               DEF_SDKPARAM("lanes", "EthPortEyeScanLanes", "")
               DEF_SDKPARAM("stepSize", "UINT", "Vertical step size"),
               "${lanes} ${stepSize}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesEyeScanLaneMaxPrescaleSet,
               "eth port serdes eyescan maxPrescale",
               "Change max Prescale",
               DEF_SDKPARAM("lanes", "EthPortEyeScanLanes", "")
               DEF_SDKPARAM("maxPrescale", "UINT", "Max prescale"),
               "${lanes} ${maxPrescale}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesEyeScanLaneDatawidthSet,
               "eth port serdes eyescan datawidth",
               "Change data width",
               DEF_SDKPARAM("lanes", "EthPortEyeScanLanes", "")
               DEF_SDKPARAM("dataWidth", "UINT", "Data width"),
               "${lanes} ${dataWidth}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesEyeScanLaneShow,
               "show eth port serdes eyescan",
               "Show Eye scanning information",
               DEF_SDKPARAM("lanes", "EthPortEyeScanLanes", ""),
               "${lanes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesEyeScanLaneEnable,
               "eth port serdes eyescan enable",
               "Enable Eye scanning",
               DEF_SDKPARAM("lanes", "EthPortEyeScanLanes", ""),
               "${lanes}")
    {
    int result = mAtCliResult();
    AtUnused(context);
    AtUnused(argv);
    ClishEnableEyeScanTask();
    return result;
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesEyeScanLaneDisable,
               "eth port serdes eyescan disable",
               "Disable Eye scanning",
               DEF_SDKPARAM("lanes", "EthPortEyeScanLanes", ""),
               "${lanes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesEyeScanControllerEqualizerSet,
               "eth port serdes eyescan equalizer",
               "Change Equalizer",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("mode", "eAtEyeScanEqualizerMode", "Equalizer mode"),
               "${portList} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesEyeScanLaneDebugEnable,
               "eth port serdes eyescan debug enable",
               "Enable eye-scan internal state machine debug",
               DEF_SDKPARAM("lanes", "EthPortEyeScanLanes", ""),
               "${lanes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesEyeScanLaneDebugDisable,
               "eth port serdes eyescan debug disable",
               "Disable eye-scan internal state machine debug",
               DEF_SDKPARAM("lanes", "EthPortEyeScanLanes", ""),
               "${lanes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesEyeScanLaneDebug,
               "debug eth port serdes eyescan lane",
               "Show lane debug information",
               DEF_SDKPARAM("lanes", "EthPortEyeScanLanes", ""),
               "${lanes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesEyeScanControllerDebug,
               "debug eth port serdes eyescan",
               "Show eye-scan debug information",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModuleEthDebug,
               "debug module eth",
               "Show debug information of Eth module\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortLogShow,
               "show eth port log",
               "Show logged information of Eth port\n",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortLogEnable,
               "eth port log enable",
               "Enable logging for Eth port\n",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortLogDisable,
               "eth port log disable",
               "Disable logging for Eth port\n",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    mAtCliCall();
    }
              
DEF_SDKCOMMAND(cliAtEthPortSerdesBridge,
               "eth port serdes bridge",
               "Bridge (duplicate) traffic to specified SERDES ",
               DEF_SDKPARAM("portId", "UINT", "Port Id")
               DEF_SDKPARAM("serdesId", "STRING", "SERDES to bridge traffic. If \"none\" is input, the existing bridge will be released.\r\n"),
               "${portId} ${serdesId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesSelect,
               "eth port serdes select",
               "Select SERDES that the ETH Port will receive traffic.",
               DEF_SDKPARAM("portId", "UINT", "Port Id")
               DEF_SDKPARAM("serdesId", "STRING", "SERDES that ETH Port will receive traffic."),
               "${portId} ${serdesId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortTxSerdesSet,
               "eth port serdes transmit",
               "Select SERDES that the ETH Port will transmit traffic.",
               DEF_SDKPARAM("portId", "UINT", "Port Id")
               DEF_SDKPARAM("serdesId", "UINT", "SERDES that ETH Port will transmit traffic."),
               "${portId} ${serdesId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesApsGet,
               "show eth port serdes aps",
               "Show SERDES APS configuration including bridge/switch status",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesNotificationEnable,
               "eth port serdes notification enable",
               "Enable SERDES alarm change notification",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesNotificationDisable,
               "eth port serdes notification disable",
               "Disable SERDES alarm change notification",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesIntrMsk,
               "eth port serdes interruptmask",
               "Set Ethernet SERDES interrupt mask\n",
               DEF_SDKPARAM("serdesList", "SerdesIdList", "")
               DEF_SDKPARAM("mask", "SerdesDefMask", "")
               DEF_SDKPARAM("intrMaskEn", "eBool", ""),
               "${serdesList} ${mask} ${intrMaskEn}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesForceAlarm,
               "eth port serdes force alarm",
               "Force/unforce Ethernet SERDES alarms\n",
               DEF_SDKPARAM("serdesList", "SerdesIdList", "")
               DEF_SDKPARAM("direction", "eAtDirection", "")
               DEF_SDKPARAM("alarm", "eAtSerdesAlarmType", "")
               DEF_SDKPARAM("force", "eBool", ""),
               "${serdesList} ${direction} ${alarm} ${force}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesForcedAlarmShow,
               "show eth port serdes forcedalarm",
               "Show SERDES being-forced alarms",
               DEF_SDKPARAM("serdesList", "SerdesIdList", "")
               DEF_SDKPARAM("direction", "eAtDirection", ""),
               "${serdesList} ${direction}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesForcableAlarmShow,
               "show eth port serdes forcablealarm",
               "Show SERDES force-able alarms",
               DEF_SDKPARAM("serdesList", "SerdesIdList", "")
               DEF_SDKPARAM("direction", "eAtDirection", ""),
               "${serdesList} ${direction}")
    {
    mAtCliCall();
    }


DEF_SDKCOMMAND(cliAtEthPortSerdesDiagnostic,
               "diag eth port serdes",
               "Diagnostic Ethernet port SERDES\n",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesControllerReset,
               "eth port serdes reset",
               "Reset SERDES",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesControllerRxReset,
               "eth port serdes rx reset",
               "Reset rx SERDES",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesControllerTxReset,
               "eth port serdes tx reset",
               "Reset tx SERDES",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtEthPortSerdesControllerEqualizerModeSet,
               "eth port serdes equalizer mode",
               "Set SERDES equalizer mode",
               DEF_SDKPARAM("serdesList", "SerdesIdList", "")
               DEF_SDKPARAM("mode", "eAtSerdesEqualizerMode", ""),
               "${serdesList} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesControllerPowerUp,
               "eth port serdes powerup",
               "Power up SERDES",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortSerdesControllerPowerDown,
               "eth port serdes powerdown",
               "Power down SERDES",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortTxErrorGeneratorErrorTypeSet,
               "eth port tx error generator type",
               "Set error generator type on transmit direction\n",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("type", "eAtEthPortErrorGeneratorErrorType", ""),
               "${portList} ${type}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortTxErrorGeneratorErrorNumSet,
               "eth port tx error generator errornum",
               "Set number of error in one-shot mode for error generator on transmit direction\n",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("num", "UINT", ""),
               "${portList} ${num}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortTxErrorGeneratorStart,
               "eth port tx error generator start",
               "Start the error generator",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortTxErrorGeneratorStop,
               "eth port tx error generator stop",
               "Stop the error generator",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortTxErrorGeneratorShow,
               "show eth port tx error generator",
               "Show configuration of error generator",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortRxErrorGeneratorErrorTypeSet,
               "eth port rx error generator type",
               "Set error generator type on RX direction\n",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("type", "eAtEthPortErrorGeneratorErrorType", ""),
               "${portList} ${type}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortRxErrorGeneratorErrorNumSet,
               "eth port rx error generator errornum",
               "Set number of error in one-shot mode for error generator on transmit direction\n",
               DEF_SDKPARAM("portList", "PortIdList", "")
               DEF_SDKPARAM("num", "UINT", ""),
               "${portList} ${num}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortRxErrorGeneratorStart,
               "eth port rx error generator start",
               "Start the error generator",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortRxErrorGeneratorStop,
               "eth port rx error generator stop",
               "Stop the error generator",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortRxErrorGeneratorShow,
               "show eth port rx error generator",
               "Show configuration of error generator",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModuleEthInit,
               "eth init",
               "Initialize ETH Module",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthAutoNegShow,
               "show eth port autoneg",
               "Show autoneg information",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
        {
        mAtCliCall();
        }

DEF_SDKCOMMAND(cliAtEthPortAutoNegRestartOn,
               "eth port autoneg restart on",
               "Restart autoneg",
               DEF_SDKPARAM("portId", "PortIdList", ""),
               "${portId}")
        {
        mAtCliCall();
        }

DEF_SDKCOMMAND(cliAtEthPortAutoNegRestartOff,
               "eth port autoneg restart off",
               "Stop restarting autoneg",
               DEF_SDKPARAM("portId", "PortIdList", ""),
               "${portId}")
        {
        mAtCliCall();
        }

DEF_SDKCOMMAND(cliAtEthPortAutoNegRestart,
               "eth port autoneg restart",
               "Restart autoneg",
               DEF_SDKPARAM("portId", "PortIdList", ""),
               "${portId}")
        {
        mAtCliCall();
        }

DEF_SDKCOMMAND(cliAtEthPortIntrMsk,
               "eth port interruptmask",
               "Set Ethernet interrupt mask\n",
               DEF_SDKPARAM("portId", "PortIdList", "")
               DEF_SDKPARAM("alarm", "EthPortAlarmType", "")
               DEF_SDKPARAM("intrMaskEn", "eBool", ""),
               "${portId} ${alarm} ${intrMaskEn}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthportAlarmCapture,
               "eth port alarm capture",
               "Enable/disable alarm capturing\n",
               DEF_SDKPARAM("portId", "PortIdList", "")
               DEF_SDKPARAM("enable", "eBool", "Enable/disable alarm capturing"),
               "${portId} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthportAlarmCaptureGet,
               "show eth port alarm capture",
               "Show captured alarms\n",
               DEF_SDKPARAM("portId", "PortIdList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", "Read to clear without display table"),
               "${portId} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortDropPacketConditionMsk,
               "eth port packet drop condition",
               "Set conditions that will drop packet\n",
               DEF_SDKPARAM("portId", "PortIdList", "")
               DEF_SDKPARAM("conditionsMask", "EthPortDropPktType", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${portId} ${conditionsMask} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortDropPacketConditionShow,
               "show eth port packet drop condition",
               "Show Condition that cause drop packet",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortInterruptMaskGet,
               "show eth port interruptmask",
               "Show interrupt mask",
               DEF_SDKPARAM("portId", "PortIdList", ""),
               "${portId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortDisparityForce,
               "eth port force disparity",
               "Force disparity",
               DEF_SDKPARAM("portId", "PortIdList", ""),
               "${portId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortDisparityUnForce,
               "eth port unforce disparity",
               "Unforce disparity",
               DEF_SDKPARAM("portId", "PortIdList", ""),
               "${portId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortFecEnable,
               "eth port fec enable",
               "Enable FEC",
               DEF_SDKPARAM("portId", "PortIdList", ""),
               "${portId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortFecDisable,
               "eth port fec disable",
               "Disable FEC",
               DEF_SDKPARAM("portId", "PortIdList", ""),
               "${portId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortRxFecEnable,
               "eth port fec enable rx",
               "Enable FEC at RX direction",
               DEF_SDKPARAM("portId", "PortIdList", ""),
               "${portId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortRxFecDisable,
               "eth port fec disable rx",
               "Disable FEC at RX direction",
               DEF_SDKPARAM("portId", "PortIdList", ""),
               "${portId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortTxFecEnable,
               "eth port fec enable tx",
               "Enable FEC at TX direction",
               DEF_SDKPARAM("portId", "PortIdList", ""),
               "${portId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortTxFecDisable,
               "eth port fec disable tx",
               "Disable FEC at TX direction",
               DEF_SDKPARAM("portId", "PortIdList", ""),
               "${portId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortAlarmForce,
               "eth port force alarm",
               "Force alarm\n",
               DEF_SDKPARAM("portId", "PortIdList", "")
               DEF_SDKPARAM("alarm", "EthPortAlarmType", "")
               DEF_SDKPARAM("direction", "eAtDirection", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${portId} ${alarm} ${direction} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortForcedAlarmShow,
               "show eth port forcedalarms",
               "Show forced alarms",
               DEF_SDKPARAM("portId", "PortIdList", ""),
               "${portId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortK30_7Force,
               "eth port force k30_7",
               "Force K30.7",
               DEF_SDKPARAM("portId", "PortIdList", ""),
               "${portId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortK30_7UnForce,
               "eth port unforce k30_7",
               "Unforce K30.7",
               DEF_SDKPARAM("portId", "PortIdList", ""),
               "${portId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortClockMonitorThresholdTypeSet,
               "eth port clock monitor threshold",
               "Set clock monitor threshold values\n",
               DEF_SDKPARAM("portId", "PortIdList", "")
               DEF_SDKPARAM("thresholdTypes", "eAtClockMonitorThreshold", "")
               DEF_SDKPARAM("values", "STRING", "Values"),
               "${portId} ${thresholdTypes} ${values}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortClockMonitorGet,
               "show eth port clock monitor",
               "Show clock monitor information\n",
               DEF_SDKPARAM("portId", "PortIdList", ""),
               "${portId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtEthPortVlanTransmitSet,
               "eth port vlan transmit",
               "Configure ethernet port transmit VLAN",
               DEF_SDKPARAM("portList", "STRING", "ETH Port ID")
               DEF_SDKPARAM("TPID.PRI.CFI.VID", "STRING", "Vlan tag: TPID.PRI.CFI.VLAN_ID. Example: 0x8100.1.0.10"),
               "${portList} {TPID.PRI.CFI.VID}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtEthPortVlanExpectedSet,
               "eth port vlan expect",
               "Configure ethernet port expected VLAN",
               DEF_SDKPARAM("portList", "STRING", "ETH Port ID")
               DEF_SDKPARAM("TPID.PRI.CFI.VID", "STRING", "Vlan tag: TPID.PRI.CFI.VLAN_ID. Example: 0x8100.1.0.10"),
               "${portList} {TPID.PRI.CFI.VID}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortVlansGet,
               "show eth port vlan",
               "",
               DEF_SDKPARAM("portList", "PortIdList", ""),
               "${portList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortLinkTrainingEnable,
               "eth port link training enable",
               "Enable Link Training",
               DEF_SDKPARAM("portId", "PortIdList", ""),
               "${portId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortLinkTrainingDisable,
               "eth port link training disable",
               "Disable Link Training",
               DEF_SDKPARAM("portId", "PortIdList", ""),
               "${portId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEthPortLinkTrainingRestart,
               "eth port link training restart",
               "Restart Link Training",
               DEF_SDKPARAM("portId", "PortIdList", ""),
               "${portId}")
    {
    mAtCliCall();
    }


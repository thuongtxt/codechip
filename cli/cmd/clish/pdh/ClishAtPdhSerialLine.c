/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : ClishAtPdhDe3.c
 *
 * Created Date: Feb 4, 2013
 *
 * Description : DE3 CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
/* Serial line list */
#define serLineno "([1-9]|3[0-2]|1[0-9]|2[0-9])"
#define serLineid serLineno"|["serLineno"-"serLineno"]|"serLineno","serLineno
#define serLineList serLineid
/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/* Data types */
DEF_PTYPE("PdhSerLineList", "regexp", serLineList,
          "\nSerial line list are as follow:\n"
          "- 1: Serial line ID list is 1\n"
          "- 1-5: Serial line ID list from 1 to 5\n"
          "- 1,5-9: Serial line  ID list are 1 and 5 to 9\n")

/* Top commands */
TOP_SDKCOMMAND("pdh serline", "\nPDH DS3/E3 Serial line CLIs\n")
DEF_PTYPE("eAtPdhSerialLineMode", "select", "ec1(1), ds3(2), e3(3) ", "\nDS3/E3 serial mode:")

DEF_SDKCOMMAND(CliPdhSerialLineModeSet,
               "pdh serline mode",
               "Set DS3/E3 serial line mode\n",
               DEF_SDKPARAM("serline", "PdhSerLineList", "")
               DEF_SDKPARAM("mode", "eAtPdhSerialLineMode", ""),
               "${serline} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhSerialLineModeShow,
               "show pdh serline",
               "Show DS3/E3 serial line mode\n",
               DEF_SDKPARAM("serline", "PdhSerLineList", ""),
               "${serline}")
    {
    mAtCliCall();
    }


DEF_SDKCOMMAND(cliPdhSerLineLoopbackSet,
               "pdh serline loopback",
               "Set serial line loopback\n",
               DEF_SDKPARAM("serline", "PdhSerLineList", "")
               DEF_SDKPARAM("loopbackMode", "eAtLoopbackMode", ""),
               "${serline} ${loopbackMode}")
    {
    mAtCliCall();
    }

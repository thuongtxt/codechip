/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2011 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : atclipdh_de3.c
 *
 * Created Date: Dec 1, 2011
 *
 * Description : DS3/E3 CLIs
 *
 * Notes       :
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
DEF_PTYPE("eAtPdhLoopbackMode", "select", "release(0), payloadloopin(1), payloadloopout(2), lineloopin(3), lineloopout(4)", "\nLoop back mode:")

/* Top commands */
TOP_SDKCOMMAND("pdh", "PDH service")
TOP_SDKCOMMAND("pdh interrupt", "PDH Interrupt")
TOP_SDKCOMMAND("pdh de1", "PDH DE1")
/*TOP_SDKCOMMAND("pdh de1 force", "PDH DE1 Force")
TOP_SDKCOMMAND("pdh de3", "PDH DE3")
TOP_SDKCOMMAND("pdh de3 force", "PDH DE3 Force")
TOP_SDKCOMMAND("pdh de3 force perfrm", "PDH DE3 Force Per Frame")*/
TOP_SDKCOMMAND("show pdh", "\nShow configuration of PDH channels\n")

/* Initialize PDH module */
DEF_SDKCOMMAND(cliPdhInit,
               "pdh init",
               "Initialize PDH module",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

/* select liu clock */
DEF_SDKCOMMAND(cliPdhOutClk,
               "pdh clockoutput",
               "Select what DS1/E1 clock source to go to External PLL",
               DEF_SDKPARAM("refOutIds", "STRING", "List of clock IDs comes to external PLL")
               DEF_SDKPARAM("de1List", "STRING", "PDH DS1/E1 list or none"),
               "${refOutIds} ${de1List}")
    {
    mAtCliCall();
    }

/* select liu clock */
DEF_SDKCOMMAND(cliPdhOutClkGet,
               "show pdh clockoutput",
               "Get clock source that is selected to go to external PLL",
               DEF_SDKPARAM("refOutIds", "STRING", "List of clock IDs comes to external PLL"),
               "${refOutIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhInterruptEnable,
               "pdh interrupt enable",
               "Enable interrupt",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhInterruptDisable,
               "pdh interrupt disable",
               "Disable interrupt",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModulePdhPrmCrCompareEnable,
               "pdh de1 prm cr compare enable",
               "Enable comparing prm cr bit",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModulePdhPrmCrCompareDisable,
               "pdh de1 prm cr compare disable",
               "Disable comparing prm cr bit",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

/*DEF_SDKCOMMAND(CliAtModulePdhDe1ForcePeriodSet,
               "pdh de1 force period",
               "Set number of seconds in which all DE1s is forced alarm or error\n",
               DEF_SDKPARAM("numSeconds", "UINT", ""),
               "${numSeconds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtModulePdhDe3ForcePeriodSet,
               "pdh de3 force period",
               "Set number of seconds in which all DE3s is forced alarm or error\n",
               DEF_SDKPARAM("numSeconds", "UINT", ""),
               "${numSeconds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtModulePdhDe3ForcePerframeEnable,
               "pdh de3 force perfrm enable",
               "Enable force perframe for DE3s\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtModulePdhDe3ForcePerframeDisable,
               "pdh de3 force perfrm disable",
               "Disable force perframe for DE3s\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }*/

DEF_SDKCOMMAND(cliAtModulePdhGet,
               "show module pdh",
               "Show PDH module information",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliModulePdhDebug,
               "debug module pdh",
               "Show debug information of PDH module\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

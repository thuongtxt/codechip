/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2011 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : atclipdh_de1.c
 *
 * Created Date: Dec 1, 2011
 *
 * Description : DS1/E1 CLIs
 *
 * Notes       :
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"
#include "AtCli.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
/* DS1/E1 list */
#define de1no "([1-9]|3[0-2]|1[0-9]|2[0-9])"
#define de1id de1no"|["de1no"-"de1no"]|"de1no","de1no
#define De1List de1id

/* Signaling DS0 list */
#define ds0no "([0-9]|3[0-1]|1[0-9]|2[0-9])"
#define ds0list ds0no"|["ds0no"-"ds0no"]|"ds0no","ds0no
#define ds0list1 ds0list"|,"ds0list
#define sigBitmapList ds0list1

/* Define timing source ID */
#define noneRefChannel "none"
#define TimeSrcIdList de1no"|"noneRefChannel

/*
 * Define defect mask regular expression
 * DefType ::= (los|lof|oof|ais|rai|cas_lom)
 * DefMsk  ::= (DefMsk[\|]DefType|DefType)
 */
#define DefType "(los|lof|ais|rai|lomf|ber-sf|ber-sd|siglof|sigrai|ebit|bom-change|ssm-change|inband-loopcode-change|prm-lbbit-change|ber-tca|clock-state-change)"
#define DefMsk  DefType"|"DefType

/*
 * Define regular expression of counter list
 * CounterType ::= (crc|fbit|rei)
 * CounterList  ::= (CounterList[,]CounterType|CounterType)
 */
#define CounterType "(bpvexz|crc|fbit|rei)"
#define CounterList "(("CounterType"[,])+"CounterType"|"CounterType")"

#define SaMaskType "(sa-4|sa-5|sa-6|sa-7|sa-8)"
#define SaMsk  SaMaskType"|"SaMaskType

DEF_PTYPE("saMask", "regexp", SaMsk,
        "\n SaMask for SSM, it is ORed of:\n"
        "- sa-4: use Sa-4 bit for transport SSM\n"
        "- sa-5: use Sa-4 bit for transport SSM\n"
        "- sa-6: use Sa-4 bit for transport SSM\n"
        "- sa-7: use Sa-4 bit for transport SSM\n"
        "- sa-8: use Sa-4 bit for transport SSM\n")

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/* Top commands */
TOP_SDKCOMMAND("pdh de1", "\nPDH DS1/E1 commands\n")
TOP_SDKCOMMAND("pdh de1 force", "\nForce PDH DS1/E1 channel commands\n")
TOP_SDKCOMMAND("show pdh", "\nShow configuration of PDH channels\n")
TOP_SDKCOMMAND("show de1", "\nShow DE1 channels\n")
TOP_SDKCOMMAND("debug pdh", "\nDebug PDH\n")
TOP_SDKCOMMAND("pdh de1 alarm", "\nPDH DS1/E1 configuration alarm\n")
TOP_SDKCOMMAND("pdh de1 alarm affect", "\nDS1/E1 alarm affect handle\n")
TOP_SDKCOMMAND("pdh de1 alarm detection", "\nPDH DS1/E1 alarm detection configuration\n")
TOP_SDKCOMMAND("pdh de1 alarm detection los", "\nPDH DS1/E1 LOS configuration enable/disable\n")
TOP_SDKCOMMAND("pdh de1 autorai", "\nPDH DS1/E1 auto RAI configuration\n")
TOP_SDKCOMMAND("pdh de1 autoais", "\nPDH DS1/E1 auto AIS configuration\n")
TOP_SDKCOMMAND("pdh de1 bom", "\nBOM PDH DS1/E1 channel commands\n")
TOP_SDKCOMMAND("pdh de1 loopcode", "\nPDH DS1/E1 channel Inband loopcode commands\n")
TOP_SDKCOMMAND("pdh de1 prm", "\nPRM PDH DS1/E1 channel commands\n")
TOP_SDKCOMMAND("pdh de1 slipbuffer", "\nCLIs to control slip buffer\n")
TOP_SDKCOMMAND("pdh de1 slipbuffer rx", "\nCLIs to control slip buffer\n")
TOP_SDKCOMMAND("pdh de1 prm cr", "\nPRM CR PDH DS1/E1 channel commands\n")
TOP_SDKCOMMAND("pdh de1 prm cr compare", "\nPRM CR Comparing PDH DS1/E1 channel commands\n")
TOP_SDKCOMMAND("pdh de1 ssm", "\nSSM PDH DS1/E1 channel commands\n")
TOP_SDKCOMMAND("pdh de1 interrupt", "\nCLIs to handle interrupt\n")
TOP_SDKCOMMAND("pdh de1 idle", "\nCLIs to control PDH DS1/E1 IDLE code\n")

TOP_SDKCOMMAND("pdh de1 datalink", "\nDatalink PDH DS1/E1 channel commands\n")
TOP_SDKCOMMAND("pdh de1 datalink message", "\n Send Datalink message for PDH DS1/E1 channel commands\n")
TOP_SDKCOMMAND("pdh de1 datalink notification", "\nData link notification PDH DS1/E1 channel commands\n")
TOP_SDKCOMMAND("pdh de1 datalink fcs", "\nPDH DS1/E1 channel datalink FCS commands\n")
TOP_SDKCOMMAND("pdh de1 log", "\nLog CLIs of DS1/E1\n")

TOP_SDKCOMMAND("show pdh de1 listen", "\nShow PDH DS1/E1 channel listener commands\n")

TOP_SDKCOMMAND("pdh de1 tx", "\nPDH DS1/E1 TX control CLIs\n")
TOP_SDKCOMMAND("pdh de1 tx error", "\nPDH DS1/E1 TX error generator CLIs\n")
TOP_SDKCOMMAND("pdh de1 tx error generator", "\nPDH DS1/E1 Tx error generator CLIs\n")
TOP_SDKCOMMAND("pdh de1 rx", "\nPDH DS1/E1 RX control CLIs\n")
TOP_SDKCOMMAND("pdh de1 rx error", "\nPDH DS1/E1 RX error generator CLIs\n")
TOP_SDKCOMMAND("pdh de1 rx error generator", "\nPDH DS1/E1 RX error generator CLIs\n")
TOP_SDKCOMMAND("show pdh de1 tx", "\nShow TX configuration of DS1/E1\n")
TOP_SDKCOMMAND("show pdh de1 tx error", "\nShow configuration of DS1/E1 TX error generator\n")
TOP_SDKCOMMAND("show pdh de1 rx", "\nShow RX configuration of DS1/E1\n")
TOP_SDKCOMMAND("show pdh de1 rx error", "\nShow configuration of DS1/E1 TX error generator\n")

/* Data types */
DEF_PTYPE("PdhDe1List", "regexp", De1List,
          "\nDS1/E1 list are as follow:\n"
          "- 1: DE1 ID list is 1\n"
          "- 1-5: DE1 ID list from 1 to 5\n"
          "- 1,5-9: DE1 ID list are 1 and 5 to 9\n")

DEF_PTYPE("sigDs0Bitmap", "regexp", sigBitmapList,
          "\nSignaling DS0 list of DS1/E1 are as follow (DS0 #0 and #16 are not used):\n"
          "- 1: Signaling DS0 list is 1\n"
          "- 1-15: Signaling DS0 list from 1 to 15\n"
          "- 1-15,17-31: Signaling DS0 list are 1 to 15 and 17 to 31\n")

DEF_PTYPE("TimingSourceId", "regexp", ".*",
          "\nInput an ID in ID DE1 channel range: [1-32]\n"
          "(This ID is referential source clock that is used in slave timing mode)\n")

DEF_PTYPE("PdhDe1Defect", "regexp", DefMsk,
        "\nDS1/E1 defect mask, it is ORed of:\n"
        "- los: LOS\n"
        "- lof: LOF\n"
        "- ais: AIS\n"
        "- rai: RAI\n"
        "- lomf: LOMF\n"
        "- siglof: SIG LOF\n"
        "- sigrai: SIG RAI\n"
        "- ebit: Ebit\n"
        "- ber-sd: BER-SD\n"
        "- ber-sf: BER-SF\n"
        "- ber-tca: BER-TCA\n"
        "- bom-change: DS1 ESF BOM detection event\n"
        "- inband-loopcode-change: DS1 inband loopcode detection event\n"
        "- ssm-change: E1 CRC SSM detection event\n"
        "- prm-lbbit-change: DS1 ESF PRM LB bit got change.\n"
        "- clock-state-change: DS1/E1 ACR/CDR clock state change.")

DEF_PTYPE("PdhDe1AlarmForce", "regexp", DefMsk,
        "\nDS1/E1 defect mask, it is ORed of:\n"
        "- los: LOS (at Rx)\n"
        "- lof: LOF (at Rx)\n"
        "- ais: AIS (at Tx)\n"
        "- rai: RAI (at Tx)\n")

DEF_PTYPE("PdhDe1CounterList", "regexp", CounterList,
        "\nList of DS1/E1 counters, counter types are as follow:\n"
        "- bpvexz  : Frame CRC error accumulation of a RX DS1-ESF/E1-CRC framer\n"
        "- crc  : Frame CRC error accumulation of a RX DS1-ESF/E1-CRC framer\n"
        "- fbit : Fbit error accumulation of a RX DS1-SF/DS1-ESF/E1-BSC/E1-CRC framer\n"
        "- rei  : REI accumulation of a RX E1 CRC framer.\n")

DEF_PTYPE("eAtPdhDe1CounterType", "select", "bpv(0), crc(1), fe(2), rei(3)", "\nError type")

DEF_PTYPE("eAtPdhDe1Loopcode", "select", "line-active(1), line-deactive(2), \
           facility1-active(3), facility1-deactive(4), facility2-active(5), \
           facility2-deactive(6), inband-disable(7)","DS1 Inband Loop code")

DEF_PTYPE("eAtPdhDe1PrmStandard", "select", "ansi(1), at&t(2)", "\nPRM standard")

DEF_PTYPE("eAtPdhDe1TxBomSentMode", "select", "invalid(0), oneshot(1), continuous(2)", "\nBOM sent mode")

/* Enable channel */
DEF_SDKCOMMAND(cliPdhDe1Enable,
               "pdh de1 enable",
               "\nThis command is to enable DS1/E1 channel list.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

/* Disable channel */
DEF_SDKCOMMAND(cliPdhDe1Disable,
               "pdh de1 disable",
               "\nThis command is to disable DS1/E1 channel list.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

/* SET DS1/E1 framing mode */
DEF_SDKCOMMAND(cliPdhDe1FrmMode,
               "pdh de1 framing",
               "Set DS1/E1 framing mode\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("framingMode", "eAtPdhDe1FrameType", ""),
               "${de1s} ${framingMode}")
    {
    mAtCliCall();
    }

/* Enable/Disable and get signaling */
DEF_SDKCOMMAND(ClishPdhDe1Signaling,
               "pdh de1 signaling",
               "Enable/Disable signaling state\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("sigBitmap", "sigDs0Bitmap", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${de1s} ${sigBitmap} ${enable} ")
    {
    mAtCliCall();
    }


DEF_SDKCOMMAND(ClishPdhDe1TxSignaling,
               "pdh de1 tx signaling",
               "Enable/Disable signaling state\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("sigBitmap", "sigDs0Bitmap", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${de1s} ${sigBitmap} ${enable} ")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishPdhDe1RxSignaling,
               "pdh de1 rx signaling",
               "Enable/Disable signaling state\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("sigBitmap", "sigDs0Bitmap", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${de1s} ${sigBitmap} ${enable} ")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1SignalingPatternSet,
               "pdh de1 signaling pattern",
               "Set signaling pattern\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("sigBitmap", "sigDs0Bitmap", "")
               DEF_SDKPARAM("pattern", "STRING", ""),
               "${de1s} ${sigBitmap} ${pattern} ")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1SignalingShow,
               "show pdh de1 signaling",
               "Show signaling information\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("sigBitmap", "sigDs0Bitmap", ""),
               "${de1s} ${sigBitmap}")
    {
    mAtCliCall();
    }

/* SET interrupt mask */
DEF_SDKCOMMAND(cliPdhDe1InterruptMask,
               "pdh de1 interruptmask",
               "SET DS1/E1 interrupt mask\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("intrMask", "PdhDe1Defect", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${de1s} ${intrMask} ${enable}")
    {
    mAtCliCall();
    }

/* SET loopback mode */
DEF_SDKCOMMAND(cliPdhDe1Loopback,
               "pdh de1 loopback",
               "Set loopback mode of specified level:\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("loopbackMode", "eAtPdhLoopbackMode", ""),
               "${de1s} ${loopbackMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1Timing,
               "pdh de1 timing",
               "Configure timing mode for DE1 channels\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("timingMode", "eAtTimingMode", "")
               DEF_SDKPARAM("timeSrcId", "STRING", "Timing source"),
               "${de1s} ${timingMode} ${timeSrcId}")
    {
    mAtCliCall();
    }

/* Force alarm or get alarm forcing status */
DEF_SDKCOMMAND(cliPdhDe1AlarmForce,
               "pdh de1 force alarm",
               "Force/un-force alarms\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("direction", "eAtDirection", "")
               DEF_SDKPARAM("alarm", "PdhDe1AlarmForce", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${de1s} ${direction} ${alarm} ${enable}")
    {
    mAtCliCall();
    }

/* Show counters in read to clear mode or read only mode */
DEF_SDKCOMMAND(cliPdhDe1Counter,
               "show pdh de1 counters",
               "Show PDH DS1/E1 error counters\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
			   DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${de1s} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

/* Show alarm states of DS1/E1 */
DEF_SDKCOMMAND(cliPdhDe1ShowAlarm,
               "show pdh de1 alarm",
               "Show alarm states of DS1/E1 channels",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

/* Show listened alarm states of DS1/E1 */
DEF_SDKCOMMAND(cliPdhDe1ShowListenedAlarm,
               "show pdh de1 listen alarm",
               "Show listened alarm states of DS1/E1 channels",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM_OPTION("readingMode", "HistoryReadingMode", ""),
               "${de1s} ${readingMode}")
    {
    mAtCliCall();
    }

/* Show alarm interrupt states of DS1/E1 */
DEF_SDKCOMMAND(cliPdhDe1ShowInterrupt,
               "show pdh de1 interrupt",
               "Show interrupt states of DS1/E1 channels",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
			   DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${de1s} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

/* Show configuration of DS1/E1 */
DEF_SDKCOMMAND(cliPdhDe1ShowConfig,
               "show pdh de1",
               "Show configuration of DS1/E1 channels\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1LedStateSet,
               "pdh de1 ledstate",
               "Set Led State for DE1 channels\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("ledState", "eAtLedState", ""),
               "${de1s} ${ledState}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDe1MapHierarchyShow,
               "show pdh de1 hierarchy",
               "Show mapping hierarchy of De1 channels\n",
               DEF_SDKPARAM("de1List", "PdhDe1List", ""),
               "${de1List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDe1Debug,
               "debug pdh de1",
               "Show debug information of DS1/E1\n",
               DEF_SDKPARAM("de1List", "PdhDe1List", ""),
               "${de1List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1ErrorForce,
               "pdh de1 force error",
               "Force TX error\n",
               DEF_SDKPARAM("de1List", "PdhDe1List", "")
               DEF_SDKPARAM("errorType", "eAtPdhDe1CounterType", "")
               DEF_SDKPARAM("force", "eBool", ""),
               "${de1List} ${errorType} ${force}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1CaptureEnable,
               "pdh de1 alarm capture",
               "Enable/disable alarm caturing\n",
               DEF_SDKPARAM("de1List", "PdhDe1List", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${de1List} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1AutoRaiEnable,
               "pdh de1 autorai enable",
               "Enable backward RAI alarm when critical alarms happen\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1AutoRaiDisable,
               "pdh de1 autorai disable",
               "Disable backward RAI alarm when critical alarms happen\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1DataLinkEnable,
               "pdh de1 datalink enable",
               "Enable Data-link\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1DataLinkDisable,
               "pdh de1 datalink disable",
               "Disable Data-link\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1LoopcodeEnable,
               "pdh de1 loopcode enable",
               "Enable DS1 Inband loop-code\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1LoopcodeDisable,
               "pdh de1 loopcode disable",
               "Disable DS1 Inband loop-code\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1DataLinkNotificationEnable,
               "pdh de1 datalink notification enable",
               "Enable notification for Data-Link\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1DataLinkNotificationDisable,
               "pdh de1 datalink notification disable",
               "Disable notification for Data-Link\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1DataLinkSaBitsMaskSet,
               "pdh de1 datalink sabit",
               "Set Sabit Mask for Datalink on the E1 channels\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("sabits", "UINT", ""),
               "${de1s} {sabits}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1DataLinkMsgSend,
               "pdh de1 datalink message send",
               "send a message Datalink on the E1 channels\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("msg", "STRING", ""),
               "${de1s} {msg}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1TxLoopcode,
               "pdh de1 loopcode transmit",
               "Transmit an inband loop code on the DS1 channels\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("code", "eAtPdhDe1Loopcode", ""),
               "${de1s} {code}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1DataLinkCountersShow,
               "show pdh de1 datalink counters",
               "Show PDH DE1 data link counters\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
			   DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode",""),
               "${de1s} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1DataLinkShow,
               "show pdh de1 datalink",
               "Show PDH DE1 data link\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s} ")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1LoopCodeShow,
               "show pdh de1 loopcode",
               "Show PDH DS1 inband Loop code\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s} ")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1TxBom,
               "pdh de1 bom transmit",
               "send a BOM message such as loop code/ssm on the DS1/E1 channels. This command automatically send continous 10 BOM codewords. \n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("value", "STRING", "Value of transmitted BOM")
               DEF_SDKPARAM_OPTION("sentMode", "eAtPdhDe1TxBomSentMode", "mode for sending BOM"),
               "${de1s} {value} {sentMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1TxBomNtimes,
               "pdh de1 bom ntransmit",
               "send a BOM message such as loop code/ssm on the DS1/E1 channels. This command automatically send continous N-times (input) BOM codewords. \n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("value", "STRING", "Value of transmitted BOM")
               DEF_SDKPARAM("ntimes", "UINT", "N-times of continuously transmitted BOM codeword"),
               "${de1s} {value} {ntimes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1BomShow,
               "show pdh de1 bom",
               "Show PDH DE1 BOM\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s} ")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1LogShow,
               "show pdh de1 log",
               "Show logged message of DS1/E1\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1LogEnable,
               "pdh de1 log enable",
               "Enable logging for DS1/E1\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1LogDisable,
               "pdh de1 log disable",
               "Disable logging for DS1/E1\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1PrmStandard,
               "pdh de1 prm standard",
               "Set PRM standard of the DS1/E1 channels\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("standard", "eAtPdhDe1PrmStandard", ""),
               "${de1s} {standard}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1PrmShow,
               "show pdh de1 prm",
               "Show PRM of DS1/E1\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }


DEF_SDKCOMMAND(cliPdhDe1TxSlipBufferEnable,
               "pdh de1 slipbuffer enable",
               "Enable TX slip buffer\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1TxSlipBufferDisable,
               "pdh de1 slipbuffer disable",
               "Disable TX slip buffer\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1TxSlipBufferShow,
               "show pdh de1 slipbuffer",
               "Show slip buffer information\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1SsmSa,
               "pdh de1 ssm sa",
               "Set Sa for SSM of E1s\n",
               DEF_SDKPARAM("de1List", "PdhDe1List", "")
               DEF_SDKPARAM("saMask", "saMask", ""),
               "${de1s} ${saMask}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1SsmEnable,
               "pdh de1 ssm enable",
               "Enable SSM of E1s\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1SsmDisable,
               "pdh de1 ssm disable",
               "Disable SSM of E1s\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1TxSsm,
               "pdh de1 ssm transmit",
               "Set SSM code for overhead DS1s/E1s\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("message", "STRING", ""),
               "${de1s} ${message}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1SsmShow,
               "show pdh de1 ssm",
               "Show SSM of DS1s/E1s\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }
DEF_SDKCOMMAND(cliPdhDe1TxPrmStandard,
               "pdh de1 prm standard transmit",
               "Set PRM standard at transmit direction of the DS1/E1 channels\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("standard", "eAtPdhDe1PrmStandard", ""),
               "${de1s} {standard}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1RxPrmStandard,
               "pdh de1 prm standard receive",
               "Set PRM standard at received direction of the DS1/E1 channels\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("standard", "eAtPdhDe1PrmStandard", ""),
               "${de1s} {standard}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPdhDe1PrmDisable,
               "pdh de1 prm disable",
               "Disable PRM on the DS1/E1 channels\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPdhDe1PrmEnable,
               "pdh de1 prm enable",
               "Enable PRM on the DS1/E1 channels\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPdhDe1PrmTxCRBitSet,
               "pdh de1 prm cr transmit",
               "Set TX C/R bit\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("cr", "UINT", ""),
               "${de1s} {cr}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1TxLBBit,
               "pdh de1 prm lb",
               "Insert LB-bit into PRM payload framed\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("lbBit", "UINT", ""),
               "${de1s} {lbBit}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPdhDe1PrmCRBitExpectSet,
               "pdh de1 prm cr expect",
               "Configure expect C/R bit\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("cr", "UINT", ""),
               "${de1s} {cr}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1CaptureShow,
               "show pdh de1 alarm capture",
               "Show captured alarms\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", ""),
               "${de1s} ${readingMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1LosDetectionEnable,
               "pdh de1 alarm detection los enable",
               "Enable DS1/E1 to detect LOS\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1LosDetectionDisable,
               "pdh de1 alarm detection los disable",
               "Disable DS1/E1 detect LOS\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1TxErrorGeneratorErrorTypeSet,
               "pdh de1 tx error generator type",
               "Set error type for DS1/E1 TX error generator\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("type", "eAtPdhErrorGeneratorErrorType", ""),
               "${de1s} ${type}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1TxErrorGeneratorModeSet,
               "pdh de1 tx error generator mode",
               "Set generation mode for DS1/E1 TX error generator\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("mode", "eAtErrorGeneratorMode", ""),
               "${de1s} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1TxErrorGeneratorErrorNumSet,
               "pdh de1 tx error generator errornum",
               "Set number of error in one-shot mode for DS1/E1 TX error generator\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("num", "UINT", ""),
               "${de1s} ${num}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1TxErrorGeneratorErrorRateSet,
               "pdh de1 tx error generator rate",
               "Set error rate in continuous mode for DS1/E1 TX error generator\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("rate", "eAtBerRate", ""),
               "${de1s} ${rate}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1TxErrorGeneratorStart,
               "pdh de1 tx error generator start",
               "Start the error generator",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1TxErrorGeneratorStop,
               "pdh de1 tx error generator stop",
               "Stop the error generator",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1TxErrorGeneratorShow,
               "show pdh de1 tx error generator",
               "Show configuration of DS1/E1 TX error generator",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1RxErrorGeneratorErrorTypeSet,
               "pdh de1 rx error generator type",
               "Set error type for DS1/E1 RX error generator\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("type", "eAtPdhErrorGeneratorErrorType", ""),
               "${de1s} ${type}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1RxErrorGeneratorModeSet,
               "pdh de1 rx error generator mode",
               "Set generation mode for DS1/E1 RX error generator\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("mode", "eAtErrorGeneratorMode", ""),
               "${de1s} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1RxErrorGeneratorErrorNumSet,
               "pdh de1 rx error generator errornum",
               "Set number of error in one-shot mode for DS1/E1 RX error generator\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("num", "UINT", ""),
               "${de1s} ${num}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1RxErrorGeneratorErrorRateSet,
               "pdh de1 rx error generator rate",
               "Set error rate in continuous mode for DS1/E1 RX error generator\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("rate", "eAtBerRate", ""),
               "${de1s} ${rate}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1RxErrorGeneratorStart,
               "pdh de1 rx error generator start",
               "Start the error generator",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1RxErrorGeneratorStop,
               "pdh de1 rx error generator stop",
               "Stop the error generator",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1RxErrorGeneratorShow,
               "show pdh de1 rx error generator",
               "Show configuration of DS1/E1 RX error generator",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1InterruptClear,
               "pdh de1 interrupt clear",
               "Clear interrupt on specified defects\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("defectsToClear", "PdhDe1Defect", ""),
               "${de1s} ${defectsToClear}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe1DataLinkFcsBitOrderSet,
               "pdh de1 datalink fcs bitorder",
               "Configure datalink FCS bit ordering for DS1/E1",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("bitOrder", "eAtBitOrder", ""),
               "${de1s} ${bitOrder}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1AutoTxAisEnable,
               "pdh de1 autoais enable",
               "Enable auto AIS when PWs receive L-Bit. In case of NxDs0, when "
               "any CESoP of NxDS0s belonging to this DE1 receive L-Bit packets, "
               "AIS will be playout onto DE1 layer.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1AutoTxAisDisable,
               "pdh de1 autoais disable",
               "Disable auto AIS when PWs receive L-Bit. In case of NxDs0, all-one "
               "will be playout onto NxDs0s that have there own PWs receive L-Bit.\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1AlarmEnable,
               "pdh de1 alarm affect enable",
               "Enable DS1/E1 alarm affect\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("alarmType", "PdhDe1Defect", ""),
               "${de1s} ${alarmType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1AlarmDisable,
               "pdh de1 alarm affect disable",
               "Disable DS1/E1 alarm affect\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("alarmType", "PdhDe1Defect", ""),
               "${de1s} ${alarmType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1AlarmGet,
               "show pdh de1 alarm affect",
               "Get DS1/E1 alarm affect\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1IdleEnable,
               "pdh de1 idle enable",
               "Enable DS1/E1 IDLE code transmission\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1IdleDisable,
               "pdh de1 idle disable",
               "Disable DS1/E1 IDLE code transmission\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1IdleCodeSet,
               "pdh de1 idle code",
               "Set DS1/E1 IDLE code\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("code", "STRING", "Transmitted IDLE code"),
               "${de1s} ${code}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1RxSlipBufferEnable,
               "pdh de1 slipbuffer rx enable",
               "Enable RX slip buffer\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1RxSlipBufferDisable,
               "pdh de1 slipbuffer rx disable",
               "Disable RX slip buffer\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

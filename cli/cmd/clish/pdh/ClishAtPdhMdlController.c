/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : ClishAtPdhMdlController.c
 *
 * Created Date: Jnue 15, 2015
 *
 * Description : MDL CLIs
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/* Top commands */
TOP_SDKCOMMAND("pdh de3 mdl", "\nSet MDL for DS3/E3 channel\n")
TOP_SDKCOMMAND("pdh de3 mdl path", "\nSet MDL for DS3 channel\n")
TOP_SDKCOMMAND("pdh de3 mdl testsignal", "\nSet MDL for DS3 channel\n")
TOP_SDKCOMMAND("pdh de3 mdl idlesignal", "\nSet MDL for DS3 channel\n")
TOP_SDKCOMMAND("show pdh de3 mdl", "\nShow configuration of MDL on DS3 channel\n")
TOP_SDKCOMMAND("debug pdh de3 mdl", "\n Show debug of MDL on DS3 channel\n")

TOP_SDKCOMMAND("show pdh de3 mdl path", "\nShow information of Path Message on DS3 channel\n")
TOP_SDKCOMMAND("show pdh de3 mdl testsignal", "\nShow information of Test Signal Message on DS3 channel\n")
TOP_SDKCOMMAND("show pdh de3 mdl idlesignal", "\nShow information of Idle Signal Message on DS3 channel\n")

DEF_SDKCOMMAND(cliPdhDe3MdlpathEnable,
               "pdh de3 mdl path enable",
               "Configure enable MDL on DS3s\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3MdlpathDisable,
               "pdh de3 mdl path disable",
               "Configure disable MDL on DS3s\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3MdlPathTxShow,
               "show pdh de3 mdl path tx",
               "Show Tx MDL configuration on DS3s\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM_OPTION("options", "STRING", "raw"),
               "${de3s} ${options}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3MdlPathRxShow,
               "show pdh de3 mdl path rx",
               "Show Rx MDL configuration on DS3s\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM_OPTION("options", "STRING", "raw"),
               "${de3s} ${options}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3MdlRxShow,
               "show pdh de3 mdl rx",
               "Show Rx MDL configuration and received message on DS3s\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM_OPTION("options", "STRING", "raw"),
               "${de3s} ${options}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3MdlPathDebug,
               "debug pdh de3 mdl path",
               "Show debug information  of MDL on DS3s\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3MdlPathTransmit,
               "pdh de3 mdl path transmit",
               "Send a testsignal message on DS3s\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3MdlPathRxScan,
               "pdh de3 mdl path receive",
               "Check Rx message is detected on DS3s\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3MdlPathCountersShow,
               "show pdh de3 mdl path counters",
               "Show MDL controller counters\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", ""),
               "${de3s} ${readingMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3MdlpathEicSet,
               "pdh de3 mdl path eic",
               "Set Equipment identification code string of MDL on DS3s\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("contents", "STRING", " String contents "),
               "${de3s} ${contents} ")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3MdlpathLicSet,
               "pdh de3 mdl path lic",
               "Set Location identification code string of MDL on DS3s\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("contents", "STRING", " String contents "),
               "${de3s} ${contents} ")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3MdlpathFicSet,
                "pdh de3 mdl path fic",
                "Set Frame identification code string of MDL on DS3s\n",
                DEF_SDKPARAM("de3s", "PdhDe3List", "")
                DEF_SDKPARAM("contents", "STRING", " String contents "),
                "${de3s} ${contents} ")
     {
     mAtCliCall();
     }

DEF_SDKCOMMAND(cliPdhDe3MdlpathUnitSet,
                "pdh de3 mdl path unit",
                "Set Unit identification code string of MDL on DS3s\n",
                DEF_SDKPARAM("de3s", "PdhDe3List", "")
                DEF_SDKPARAM("contents", "STRING", " String contents "),
                "${de3s} ${contents} ")
     {
     mAtCliCall();
     }

DEF_SDKCOMMAND(cliPdhDe3MdlpathPfiSet,
                "pdh de3 mdl path pfi",
                "Set Facility identification code string of MDL on DS3s\n",
                DEF_SDKPARAM("de3s", "PdhDe3List", "")
                DEF_SDKPARAM("contents", "STRING", " String contents "),
                "${de3s} ${contents} ")
     {
     mAtCliCall();
     }

DEF_SDKCOMMAND(cliPdhDe3MdlTestSignalEnable,
               "pdh de3 mdl testsignal enable",
               "Configure enable MDL on DS3s\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3MdlTestSignalDisable,
               "pdh de3 mdl testsignal disable",
               "Configure disable MDL on DS3s\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3MdlTestSignalTxShow,
               "show pdh de3 mdl testsignal tx",
               "Show Tx MDL configuration on DS3s\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM_OPTION("options", "STRING", "raw"),
               "${de3s} ${options}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3MdlTestSignalRxShow,
               "show pdh de3 mdl testsignal rx",
               "Show Rx MDL configuration on DS3s\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM_OPTION("options", "STRING", "raw"),
               "${de3s} ${options}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3MdlTestSignalDebug,
               "debug pdh de3 mdl testsignal",
               "Show debug information  of MDL on DS3s\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3MdlTestSignalTransmit,
               "pdh de3 mdl testsignal transmit",
               "Send a testsignal message on DS3s\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3MdlTestSignalRxScan,
               "pdh de3 mdl testsignal receive",
               "Check Rx message is detected on DS3s\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3MdlTestSignalCountersShow,
               "show pdh de3 mdl testsignal counters",
               "Show MDL controller counters\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", ""),
               "${de3s} ${readingMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3MdlTestSignalEicSet,
               "pdh de3 mdl testsignal eic",
               "Set Equipment identification code string of MDL on DS3s\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("contents", "STRING", " String contents "),
               "${de3s} ${contents} ")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3MdlTestSignalLicSet,
               "pdh de3 mdl testsignal lic",
               "Set Location identification code string of MDL on DS3s\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("contents", "STRING", " String contents "),
               "${de3s} ${contents} ")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3MdlTestSignalFicSet,
                "pdh de3 mdl testsignal fic",
                "Set Frame identification code string of MDL on DS3s\n",
                DEF_SDKPARAM("de3s", "PdhDe3List", "")
                DEF_SDKPARAM("contents", "STRING", " String contents "),
                "${de3s} ${contents} ")
     {
     mAtCliCall();
     }

DEF_SDKCOMMAND(cliPdhDe3MdlTestSignalUnitSet,
                "pdh de3 mdl testsignal unit",
                "Set Unit identification code string of MDL on DS3s\n",
                DEF_SDKPARAM("de3s", "PdhDe3List", "")
                DEF_SDKPARAM("contents", "STRING", " String contents "),
                "${de3s} ${contents} ")
     {
     mAtCliCall();
     }

DEF_SDKCOMMAND(cliPdhDe3MdlTestSignalGeneratorNumberSet,
                "pdh de3 mdl testsignal generator",
                "Set Generator number string of MDL on DS3s\n",
                DEF_SDKPARAM("de3s", "PdhDe3List", "")
                DEF_SDKPARAM("contents", "STRING", " String contents "),
                "${de3s} ${contents} ")
     {
     mAtCliCall();
     }

DEF_SDKCOMMAND(cliPdhDe3MdlIdleSignalEnable,
               "pdh de3 mdl idlesignal enable",
               "Configure enable MDL on DS3s\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3MdlIdleSignalDisable,
               "pdh de3 mdl idlesignal disable",
               "Configure disable MDL on DS3s\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3MdlIdleSignalTxShow,
               "show pdh de3 mdl idlesignal tx",
               "Show Tx MDL configuration on DS3s\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM_OPTION("options", "STRING", "raw"),
               "${de3s} ${options}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3MdlIdleSignalRxShow,
               "show pdh de3 mdl idlesignal rx",
               "Show Rx MDL configuration on DS3s\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM_OPTION("options", "STRING", "raw"),
               "${de3s} ${options}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3MdlIdleSignalDebug,
               "debug pdh de3 mdl idlesignal",
               "Show debug information  of MDL on DS3s\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3MdlIdleSignalTransmit,
               "pdh de3 mdl idlesignal transmit",
               "Send a testsignal message on DS3s\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3MdlIdleSignalRxScan,
               "pdh de3 mdl idlesignal receive",
               "Check Rx message is detected on DS3s\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3MdlIdleSignalCountersShow,
               "show pdh de3 mdl idlesignal counters",
               "Show MDL controller counters\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", ""),
               "${de3s} ${readingMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3MdlIdleSignalEicSet,
               "pdh de3 mdl idlesignal eic",
               "Set Equipment identification code string of MDL on DS3s\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("contents", "STRING", " String contents "),
               "${de3s} ${contents} ")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3MdlIdleSignalLicSet,
               "pdh de3 mdl idlesignal lic",
               "Set Location identification code string of MDL on DS3s\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("contents", "STRING", " String contents "),
               "${de3s} ${contents} ")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3MdlIdleSignalFicSet,
                "pdh de3 mdl idlesignal fic",
                "Set Frame identification code string of MDL on DS3s\n",
                DEF_SDKPARAM("de3s", "PdhDe3List", "")
                DEF_SDKPARAM("contents", "STRING", " String contents "),
                "${de3s} ${contents} ")
     {
     mAtCliCall();
     }

DEF_SDKCOMMAND(cliPdhDe3MdlIdleSignalUnitSet,
                "pdh de3 mdl idlesignal unit",
                "Set Unit identification code string of MDL on DS3s\n",
                DEF_SDKPARAM("de3s", "PdhDe3List", "")
                DEF_SDKPARAM("contents", "STRING", " String contents "),
                "${de3s} ${contents} ")
     {
     mAtCliCall();
     }

DEF_SDKCOMMAND(cliPdhDe3MdlIdleSignalPortSet,
                "pdh de3 mdl idlesignal port",
                "Set Equipment port string of MDL on DS3s\n",
                DEF_SDKPARAM("de3s", "PdhDe3List", "")
                DEF_SDKPARAM("contents", "STRING", " String contents "),
                "${de3s} ${contents} ")
     {
     mAtCliCall();
     }


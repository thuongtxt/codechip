/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : ClishAtPdhDe3.c
 *
 * Created Date: Feb 4, 2013
 *
 * Description : DE3 CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/* Top commands */
TOP_SDKCOMMAND("pdh de2", "\nPDH DS2/E2\n")
TOP_SDKCOMMAND("pdh de2 force", "\nForce PDH DS3/E3 channel\n")
TOP_SDKCOMMAND("show pdh", "\nShow configuration of PDH channels\n")
TOP_SDKCOMMAND("pdh de2 log", "\nLogging CLIs of PDH channels\n")

DEF_PTYPE("PdhDe2List", "regexp", ".*", "PDH DE2 channel list")
DEF_PTYPE("PdhDe2AlarmForce", "select", "ais(1), rai(2), lof(3)", "\nPDH DE2 Alarm type:")
DEF_PTYPE("PdhDe2Defect", "regexp", ".*", "PDH DE2 Mask Defect")

DEF_SDKCOMMAND(cliPdhDe2InterruptMask,
               "pdh de2 interruptmask",
               "SET DS2/E2 interrupt mask\n",
               DEF_SDKPARAM("de2s", "PdhDe2List", "1.1.1.1, 1.1.1.1-1.1.1.7,...")
               DEF_SDKPARAM("intrMask", "PdhDe2Defect", "lof")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${de2s} ${intrMask} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe2ForceAlarm,
               "pdh de2 force alarm",
               "Force/un-force alarms\n",
               DEF_SDKPARAM("de2s", "PdhDe2List", "")
               DEF_SDKPARAM("direction", "eAtDirection", "rx")
               DEF_SDKPARAM("alarm", "PdhDe2AlarmForce", "PDH DE2 Alarm type")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${de2s} ${direction} ${alarm} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe2AlarmShow,
               "show pdh de2 alarm",
               "Show alarm states of DS2/E2 channels",
               DEF_SDKPARAM("de2s", "PdhDe2List", ""),
               "${de2s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe2InterruptShow,
               "show pdh de2 interrupt",
               "Show interrupt states of DS2/E2 channels",
               DEF_SDKPARAM("de2s", "PdhDe2List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", ""),
               "${de2s} ${readingMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe2Show,
               "show pdh de2",
               "Show configuration of DS2/E2 channels\n",
               DEF_SDKPARAM("de2s", "PdhDe2List", ""),
               "${de2s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe2CountersShow,
               "show pdh de2 counters",
               "Show counters of DS2/E2 channels\n",
               DEF_SDKPARAM("de2s", "PdhDe2List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${de2s} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe2LogShow,
               "show pdh de2 log",
               "Show logged message of DS2/E2 channels\n",
               DEF_SDKPARAM("de2s", "PdhDe2List", ""),
               "${de2s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe2LogEnable,
               "pdh de2 log enable",
               "Enable logging for DS2/E2 channels\n",
               DEF_SDKPARAM("de2s", "PdhDe2List", ""),
               "${de2s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe2LogDisable,
               "pdh de2 log disable",
               "Disable logging for DS2/E2 channels\n",
               DEF_SDKPARAM("de2s", "PdhDe2List", ""),
               "${de2s}")
    {
    mAtCliCall();
    }



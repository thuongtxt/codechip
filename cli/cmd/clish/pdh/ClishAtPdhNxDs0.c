/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : ClishAtPdhNxDs0.c
 *
 * Created Date: Nov 29, 2012
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"
#include "AtCli.h"

/*--------------------------- Define -----------------------------------------*/
/* DS1/E1 list */
#define de1no "([1-9]|3[0-2]|1[0-9]|2[0-9])"
#define de1id de1no"|["de1no"-"de1no"]|"de1no","de1no
#define De1List de1id

/* DS0 list */
#define ds0no "([0-9]|3[0-1]|1[0-9]|2[0-9])"
#define ds0list ds0no"|["ds0no"-"ds0no"]|"ds0no","ds0no
#define ds0List ds0list"|,"ds0list

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("pdh de1 nxds0", "\nPDH NxDS0\n")
TOP_SDKCOMMAND("pdh de1 nxds0 cas", "\nPDH NxDS0 CAS\n")
TOP_SDKCOMMAND("pdh de1 nxds0 cas idle", "\nPDH NxDS0 CAS IDLE\n")

DEF_PTYPE("PdhNxDs0De1List", "regexp", De1List,
          "\nDS1/E1 list are as follow:\n"
          "- 1: DE1 ID list is 1\n"
          "- 1-5: DE1 ID list from 1 to 5\n"
          "- 1,5-9: DE1 ID list are 1 and 5 to 9\n")

DEF_PTYPE("PdhNxDs0List", "regexp", ds0List,
          "\nDS0 list of DS1/E1 are as follow:\n"
          "- 1: DS0 list of DS1/E1 #1 is 1\n"
          "- 0-31: DS0 list of DS1/E1 #2 from 0 to 31\n"
          "- 1-15,17-31: DS0 list of DS1/E1 #2 are 1 to 15 and 17 to 31\n")

/* Create nxDS0 list */
DEF_SDKCOMMAND(cliPdhNxDs0Create,
               "pdh de1 nxds0create",
               "Create a NxDs0 of a DS1/E1 channel\n",
               DEF_SDKPARAM("de1s", "PdhNxDs0De1List", "")
               DEF_SDKPARAM("nxds0List", "PdhNxDs0List", ""),
               "${de1s} ${nxds0List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhNxDs0Delete,
               "pdh de1 nxds0delete",
               "Delete NxDs0 list from DE1 channel\n",
               DEF_SDKPARAM("de1s", "PdhNxDs0De1List", "")
               DEF_SDKPARAM("nxds0List", "PdhNxDs0List", ""),
               "${de1s} ${nxds0List}")
    {
    mAtCliCall();
    }

/* show nxDS0 list */
DEF_SDKCOMMAND(cliPdhNxDs0Show,
               "show pdh de1 nxds0",
               "Show a NxDs0 of a DS1/E1 channel\n",
               DEF_SDKPARAM("de1s", "PdhNxDs0De1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhNxDs0CasIdleCodeSet,
               "pdh de1 nxds0 cas idle code",
               "Set NxDs0 CAS idle code for a nxds0 channel\n",
               DEF_SDKPARAM("de1s", "PdhNxDs0De1List", "")
               DEF_SDKPARAM("nxds0List", "PdhNxDs0List", "")
               DEF_SDKPARAM("idle", "STRING", ""),
               "${de1s} ${nxds0List} ${idle}")
    {
    mAtCliCall();
    }

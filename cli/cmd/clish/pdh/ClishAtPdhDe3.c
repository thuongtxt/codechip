/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : ClishAtPdhDe3.c
 *
 * Created Date: Feb 4, 2013
 *
 * Description : DE3 CLIs
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
/*
 * Define defect mask regular expression
 * DefType ::= (los|lof|oof|ais|rai)
 * DefMsk  ::= (DefMsk[\|]DefType|DefType)
 */
#define DefType "(los|lof|ais|rai|idle|ber-sd|ber-sf|ber-tca|aic-change|feac-change|pldtype-change|tim|tm-change|ssm-change|mdl-change|clock-state-change)"
#define DefMsk  DefType"|"DefType


/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/* Top commands */
TOP_SDKCOMMAND("pdh de3", "\nPDH DS3/E3\n")
TOP_SDKCOMMAND("pdh de3 force", "\nForce PDH DS3/E3 channel\n")
TOP_SDKCOMMAND("pdh de3 tm", "\nEnable TM for DS3/E3 channel\n")
TOP_SDKCOMMAND("pdh de3 idle", "\nEnable Idle code for DS3/E3 channel\n")
TOP_SDKCOMMAND("pdh de3 feac", "\nSet Transmit Feac code for DS3/E3 channel\n")
TOP_SDKCOMMAND("pdh de3 feac notification", "\nSet notification Feac code for DS3/E3 channel\n")
TOP_SDKCOMMAND("show pdh", "\nShow configuration of PDH channels\n")
TOP_SDKCOMMAND("pdh de3 log", "\nLog CLIs of PDH channels\n")
TOP_SDKCOMMAND("pdh de3 alarm", "\nDS3/E3 alarm handle\n")
TOP_SDKCOMMAND("pdh de3 alarm affect", "\nDS3/E3 alarm affect handle\n")
TOP_SDKCOMMAND("pdh de3 alarm detection", "\nDS3/E3 alarm detection configuration\n")
TOP_SDKCOMMAND("pdh de3 alarm detection los", "\nDS3/E3 alarm LOS detection enable/disable\n")
TOP_SDKCOMMAND("pdh de3 ssm", "\nSSM DS3/E3 CLIs\n")
TOP_SDKCOMMAND("pdh de3 datalink", "\nDatalink PDH DS3/E3 channel commands\n")
TOP_SDKCOMMAND("pdh de3 datalink fcs", "\nPDH DS3/E3 channel datalink FCS commands\n")
TOP_SDKCOMMAND("pdh de3 slipbuffer", "\nCLIs to control slip buffer\n")
TOP_SDKCOMMAND("debug pdh", "\nDebug PDH\n")
TOP_SDKCOMMAND("pdh de3 autorai", "\nPDH DS3/E3 auto RAI configuration\n")
TOP_SDKCOMMAND("pdh de3 udl", "\nPDH DS3/E3 UDL configuration\n")
TOP_SDKCOMMAND("show pdh de3 listen", "\nShow PDH DS3/E3 channel listener commands\n")
TOP_SDKCOMMAND("pdh de3 interrupt", "\nCLIs to handle interrupt\n")

TOP_SDKCOMMAND("pdh de3 tx", "\nPDH DS3/E3 TX control CLIs\n")
TOP_SDKCOMMAND("pdh de3 tx error", "\nPDH DS3/E3 TX error generator CLI\n")
TOP_SDKCOMMAND("pdh de3 tx error generator", "\nPDH DS3/E3 TX error generator CLI\n")
TOP_SDKCOMMAND("pdh de3 rx", "\nPDH DS3/E3 RX control CLIs\n")
TOP_SDKCOMMAND("pdh de3 rx error", "\nPDH DS3/E3 RX error generator CLI\n")
TOP_SDKCOMMAND("pdh de3 rx error generator", "\nPDH DS3/E3 RX error generator CLI\n")
TOP_SDKCOMMAND("show pdh de3 tx", "\nShow TX configuration of DS3/E3\n")
TOP_SDKCOMMAND("show pdh de3 tx error", "\nShow configuration of DS3/E3 TX error generator\n")
TOP_SDKCOMMAND("show pdh de3 rx", "\nShow RX configuration of DS3/E3\n")
TOP_SDKCOMMAND("show pdh de3 rx error", "\nShow configuration of DS3/E3 RX error generator\n")

DEF_PTYPE("PdhDe3List", "regexp", ".*", "PDH DE3 channel list")
DEF_PTYPE("TimingSourceDe3Id", "regexp", ".*", "Timing source of PDH DE3 channel")
DEF_PTYPE("PdhDe3AlarmForce", "regexp", ".*", "PDH DE3 Alarm Force")
DEF_PTYPE("eAtPdhDe3FrameType", "select", "ds3_unframed(1), ds3_cbit_unchannelize(2), ds3_cbit_28ds1(3), ds3_cbit_21e1(4), ds3_m13_28ds1(5), ds3_m13_21e1(6), e3_unframed(7), e3_g832(8), e3_g751(9), e3_g751_16e1s(10)", "\nDS3/E3 framming types:")
DEF_PTYPE("eAtPdhDe3FeacType", "select", "unknown(0), alarm(1), loop_active(2), loop_deactive(3)", "\nDS3 Cbit FEAC types:")
DEF_PTYPE("eAtPdhMdlStandard", "select", "ansi(1), at&t(2)", "\nMDL standard")
DEF_PTYPE("eAtPdhDe3DataLinkOption", "select", "dl(1), udl(2)", "\nData-Link Options")
DEF_PTYPE("eAtPdhDe3StuffMode", "select", "fine_stuff(1), full_stuff(2)", "\nDS3/E3 stuff mode:")
DEF_PTYPE("eAtPdhDe3TxFeacSentMode", "select", "invalid(0), oneshot(1), continuous(2)", "\nFEAC sent mode")

DEF_PTYPE("PdhDe3Defect", "regexp", DefMsk,
        "\nDS3/E3 Defect and Event Mask, it is ORed of:\n"
        "- los: LOS\n"
        "- lof: LOF\n"
        "- ais: AIS\n"
        "- rai: RAI\n"
        "- ber-sd: BER-SD\n"
        "- ber-sf: BER-SF\n"
        "- ber-tca: BER-TCA\n"
        "- idle: DS3 Cbit/M13 IDLE\n"
        "- aic-change: DS3 Cbit AIC change.\n"
        "- feac-change: DS3 Cbit FEAC change.\n"
        "- tim: E3 G.832 TIM\n"
        "- pldtype-change: E3 G.832 Payload Type change.\n"
        "- tm-change: E3 G.832 TM change.\n"
        "- ssm-change: DS3 C-bit BOM-like SSM on UDL or E3 G.832 SSM change.\n"
        "- mdl-change: DS3 C-bit MDL change.\n"
        "- clock-state-change: DS3/E3 ACR/CDR clock state change.\n")

DEF_SDKCOMMAND(cliPdhDe3FrmMode,
               "pdh de3 framing",
               "Set DS3/E3 framing mode\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("framingMode", "eAtPdhDe3FrameType", ""),
               "${de3s} ${framingMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3InterruptMask,
               "pdh de3 interruptmask",
               "SET DS3/E3 interrupt mask\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("intrMask", "PdhDe3Defect", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${de3s} ${intrMask} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3Loopback,
               "pdh de3 loopback",
               "Set loopback mode of specified level:\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("loopbackMode", "eAtPdhLoopbackMode", ""),
               "${de3s} ${loopbackMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3Timing,
               "pdh de3 timing",
               "Configure timing mode for DE3 channels\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("timingMode", "eAtTimingMode", "")
               DEF_SDKPARAM("timeSrcId", "TimingSourceDe3Id", ""),
               "${de3s} ${timingMode} ${timeSrcId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3ForceAlarm,
               "pdh de3 force alarm",
               "Force/un-force alarms\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("direction", "eAtDirection", "")
               DEF_SDKPARAM("alarm", "PdhDe3AlarmForce", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${de3s} ${direction} ${alarm} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3CounterShow,
               "show pdh de3 counters",
               "Show PDH DS3/E3 error counters\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
			   DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${de3s} ${readingMode} $(silent)")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3AlarmShow,
               "show pdh de3 alarm",
               "Show alarm states of DS3/E3 channels",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3ListenedAlarmShow,
               "show pdh de3 listen alarm",
               "Show listened alarm states of DS3/E3 channels",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM_OPTION("readingMode", "HistoryReadingMode", ""),
               "${de3s} ${readingMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3InterruptShow,
               "show pdh de3 interrupt",
               "Show interrupt states of DS3/E3 channels",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
			   DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${de3s} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3Show,
               "show pdh de3",
               "Show configuration of DS3/E3 channels\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3PayloadType,
               "pdh de3 pldtype",
               "Configure Payload Type for DE3 channels\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("pldtype", "UINT", ""),
               "${de3s} ${pldtype}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3TmEnable,
               "pdh de3 tm enable",
               "Enable TM for DE3 channels\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3TmDisable,
               "pdh de3 tm disable",
               "Disable TM for DE3 channels\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3TmCode,
               "pdh de3 tm code",
               "Set TM code for DE3 channels\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("tmCode", "UINT", ""),
               "${de3s} {tmCode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3Enable,
               "pdh de3 enable",
               "Enable for DE3 channel\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3Disable,
               "pdh de3 disable",
               "Disable for DE3 channels\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3NbitTxSet,
               "pdh de3 nbit",
               "Configure National bit for per DS3/E3 channels\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("patern", "UINT", ""),
               "${de3s} ${patern}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3OverHeadShow,
               "show pdh de3 overhead",
               "Show Overhead information of DS3/E3 channels\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3IdleCodeEnable,
               "pdh de3 idle enable",
               "Configure enable Idle overhead DS3s/E3s\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3IdleCodeDisable,
               "pdh de3 idle disable",
               "Configure disable Idle overhead DS3s/E3s\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3TxFeacCodeSet,
               "pdh de3 feac transmit",
               "Configure Transmit FEAC code in DS3s/E3s\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("control", "eAtPdhDe3FeacType", "FEAC type")
               DEF_SDKPARAM("code", "STRING", "6x (in decimal) of 0xxxxxx011111111 FEAC pattern")
               DEF_SDKPARAM_OPTION("sentMode", "eAtPdhDe3TxFeacSentMode", "FEAC sending mode"),
               "${de3s} ${control} ${code} {sentMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3FeacCodeGet,
               "show pdh de3 feac",
               "Show Transmit & RX FEAC code in DS3s cbit\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "DS3 list"),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3FeacNotificationEnable,
               "pdh de3 feac notification enable",
               "Enable notification for FEAC\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3FeacNotificationDisable,
               "pdh de3 feac notification disable",
               "Disable notification for FEAC\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3LogShow,
               "show pdh de3 log",
               "Show logged information of DS3/E3\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3LogEnable,
               "pdh de3 log enable",
               "Enable logging for DS3/E3\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3LogDisable,
               "pdh de3 log disable",
               "Disable logging for DS3/E3\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3CaptureEnable,
               "pdh de3 alarm capture",
               "Enable/disable alarm caturing\n",
               DEF_SDKPARAM("de3List", "PdhDe3List", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${de3List} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3MdlStandard,
               "pdh de3 mdl standard",
               "Set PRM standard of the DS3/E3 channels\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("standard", "eAtPdhMdlStandard", ""),
               "${de3s} {standard}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3MdlShow,
               "show pdh de3 mdl",
               "Show PRM of DS3/E3\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDe3MapHierarchyShow,
               "show pdh de3 hierarchy",
               "Show mapping hierarchy of De3 channels\n",
               DEF_SDKPARAM("de3List", "PdhDe3List", ""),
               "${de3List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3TxSlipBufferEnable,
               "pdh de3 slipbuffer enable",
               "Enable TX slip buffer\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3TxSlipBufferDisable,
               "pdh de3 slipbuffer disable",
               "Disable TX slip buffer\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3TxSlipBufferShow,
               "show pdh de3 slipbuffer",
               "Show slip buffer information\n",
               DEF_SDKPARAM("de3s", "PdhDe1List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3SsmEnable,
               "pdh de3 ssm enable",
               "Enable SSM of DS3/E3\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3SsmDisable,
               "pdh de3 ssm disable",
               "Disable SSM of DS3/E3\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3SsmSend,
               "pdh de3 ssm transmit",
               "Set SSM code for overhead DS1s/E1s\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("message", "STRING", ""),
               "${de3s} ${message}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3SsmShow,
               "show pdh de3 ssm",
               "Show SSM information of DS3/E3 channels\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3DataLinkEnable,
               "pdh de3 datalink enable",
               "Enable Data-link\n",
               DEF_SDKPARAM("de1s", "PdhDe3List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3DataLinkDisable,
               "pdh de3 datalink disable",
               "Disable Data-link\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3DataLinkOption,
               "pdh de3 datalink option",
               "Set Data-link option of the DS3/E3 channels\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("option", "eAtPdhDe3DataLinkOption", ""),
               "${de3s} {option}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3DataLinkShow,
               "show pdh de3 datalink",
               "Show Data-Link of DS3/E3\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3CaptureShow,
               "show pdh de3 alarm capture",
               "Show captured alarms\n",
               DEF_SDKPARAM("de3List", "PdhDe3List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", "Read to clear without display table"),
               "${de3List} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDe3Debug,
               "debug pdh de3",
               "Show debug information of DS3/E3\n",
               DEF_SDKPARAM("de3List", "PdhDe3List", ""),
               "${de3List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3AutoRaiEnable,
               "pdh de3 autorai enable",
               "Enable backward RAI alarm when critical alarms happen\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3AutoRaiDisable,
               "pdh de3 autorai disable",
               "Disable backward RAI alarm when critical alarms happen\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3UdlBitOrderSet,
               "pdh de3 udl bitorder",
               "Configure bit ordering for both transmit and receive sides\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("bitOrder", "eAtBitOrder", ""),
               "${de3s} ${bitOrder}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3TxUdlBitOrderSet,
               "pdh de3 udl bitorder tx",
               "Configure transmit bit ordering\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("bitOrder", "eAtBitOrder", ""),
               "${de3s} ${bitOrder}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3RxUdlBitOrderSet,
               "pdh de3 udl bitorder rx",
               "Configure receive bit ordering\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("bitOrder", "eAtBitOrder", ""),
               "${de3s} ${bitOrder}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3LosDetectionEnable,
               "pdh de3 alarm detection los enable",
               "Enable DS3/E3 to detect LOS\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3LosDetectionDisable,
               "pdh de3 alarm detection los disable",
               "Disable DS3/E3 detect LOS\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3TxErrorGeneratorErrorTypeSet,
               "pdh de3 tx error generator type",
               "Set error type for DS3/E3 TX error generator\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("type", "eAtPdhErrorGeneratorErrorType", ""),
               "${de3s} ${type}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3TxErrorGeneratorErrorNumSet,
               "pdh de3 tx error generator errornum",
               "Set number of error in one-shot mode for DS3/E3 TX error generator\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("num", "UINT", ""),
               "${de3s} ${num}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3TxErrorGeneratorStart,
               "pdh de3 tx error generator start",
               "Start the error generator",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3TxErrorGeneratorStop,
               "pdh de3 tx error generator stop",
               "Stop the error generator",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3TxErrorGeneratorShow,
               "show pdh de3 tx error generator",
               "Show configuration of DS3/E3 TX error generator",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3RxErrorGeneratorErrorTypeSet,
               "pdh de3 rx error generator type",
               "Set error type for DS3/E3 RX generator type\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("type", "eAtPdhErrorGeneratorErrorType", ""),
               "${de3s} ${type}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3RxErrorGeneratorErrorNumSet,
               "pdh de3 rx error generator errornum",
               "Set number of error in one-shot mode for DS3/E3 RX error generator\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("num", "UINT", ""),
               "${de3s} ${num}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3RxErrorGeneratorStart,
               "pdh de3 rx error generator start",
               "Start the error generator",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3RxErrorGeneratorStop,
               "pdh de3 rx error generator stop",
               "Stop the error generator",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3RxErrorGeneratorShow,
               "show pdh de3 rx error generator",
               "Show configuration for DS3/E3 RX error generator",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe3DataLinkFcsBitOrderSet,
               "pdh de3 datalink fcs bitorder",
               "Configure datalink FCS bit ordering for DS3/E3",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("bitOrder", "eAtBitOrder", ""),
               "${de3s} ${bitOrder}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3InterruptClear,
               "pdh de3 interrupt clear",
               "Clear interrupt on specified defects\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("defectsToClear", "PdhDe3Defect", ""),
               "${de3s} ${defectsToClear}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3AlarmEnable,
               "pdh de3 alarm affect enable",
               "Enable DS3/E3 alarm affect\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("alarmType", "PdhDe3Defect", ""),
               "${de3s} ${alarmType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3AlarmDisable,
               "pdh de3 alarm affect disable",
               "Disable DS3/E3 alarm affect\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("alarmType", "PdhDe3Defect", ""),
               "${de3s} ${alarmType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishPdhDe3StuffMode,
               "pdh de3 stuff_mode",
               "Set DS3/E3 fstuff mode\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", "")
               DEF_SDKPARAM("stuffMode", "eAtPdhDe3StuffMode", ""),
               "${de3s} ${stuffMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3AlarmGet,
               "show pdh de3 alarm affect",
               "Get DS3/E3 alarm affect\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

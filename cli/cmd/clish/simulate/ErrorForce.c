/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : App
 *
 * File        : error_force.c
 *
 * Created Date: Oct 18, 2013
 *
 * Description : To simulate error force
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"
#include "atclib.h"
#include "AtDevice.h"
#include "AtSdhLine.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static AtSdhLine m_line = NULL;
static uint32    m_numErrorForOneTime = 0;
static uint32    m_totalError = 0;

/*--------------------------- Forward declarations ---------------------------*/
extern AtDevice CliDevice(void);

/*--------------------------- Implementation ---------------------------------*/
static uint32 B2ErrorCounterAddress(AtSdhLine line)
    {
    return 0x0074908 + AtChannelIdGet((AtChannel)line);
    }

static void ForceB2Error(AtSdhLine line, uint32 numError)
    {
    AtDevice device = CliDevice();
    AtHal hal = AtDeviceIpCoreHalGet(device, 0);
    if (line == NULL)
        return;
    AtHalWrite(hal, B2ErrorCounterAddress(line), numError);
    }

DEF_SDKCOMMAND(cliLineForceError_,
               "sdh line force b2",
               "Force B2 error at line\n",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("numErrorsPerSecond", "UINT", "Number of error per second"),
               "${lineList} ${numErrorsPerSecond}")
    {
    uint32 lineId   = AtStrToDw(lub_argv__get_arg(argv, 0)) - 1;
    uint32 numError = AtStrToDw(lub_argv__get_arg(argv, 1));
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(CliDevice(), cAtModuleSdh);
    uint32 taskPeriodInMs = 100;
    AtUnused(context);

    m_totalError         = 0;
    m_numErrorForOneTime = (numError * taskPeriodInMs) / 1000;
    m_line               = AtModuleSdhLineGet(sdhModule, (uint8)lineId);
    if (m_numErrorForOneTime == 0)
        ForceB2Error(m_line, 0);

    return 0;
    }

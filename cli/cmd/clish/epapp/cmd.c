/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APP
 *
 * File        : cmd.c
 *
 * Created Date: Nov 1, 2012
 *
 * Description : Application command
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "sys/types.h"

#include "atclib.h"
#include "sdkcmd.h"
#include "attypes.h"
#include "apputil.h"
#include "tasks.h"
#include "AtCommon.h"
#include "AtHalTcpServer.h"
#include "AtCli.h"
#include "AtCliService.h"
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
extern void CliClientExit(void);
extern void AppPollingTaskEnable(eBool enable);
extern void AppInterruptTaskEnable(eBool enable);
extern void AppPollingTaskPeriodInMsSet(uint32 periodMs);
extern void AppFmPmTaskEnable(eBool enable);
extern void AppForcePointerTaskEnable(eBool enable);
extern void AppForcePointerTaskPeriodInMsSet(uint32 periodMs);

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("close", "Close ...")
TOP_SDKCOMMAND("app", "CLIs to control application")
TOP_SDKCOMMAND("app polling", "CLIs to control polling task")
TOP_SDKCOMMAND("app interrupt", "CLIs to control interrupt task")
TOP_SDKCOMMAND("app fmpm", "CLIs to control FM/PM task")
TOP_SDKCOMMAND("app interrupt flooding", "CLIs to control interrupt flood watching")
TOP_SDKCOMMAND("app interrupt flooding threshold", "CLIs to control interrupt flood watching")
TOP_SDKCOMMAND("app interrupt flooding watch", "CLIs to control interrupt flood watching")
TOP_SDKCOMMAND("app force_pointer", "CLIs to control Force Pointer task")
TOP_SDKCOMMAND("show app", "CLIs to show application information")
TOP_SDKCOMMAND("show app interrupt", "CLI to show interrupt flooding catching tool")

static void AppConnectionShow(void)
    {
    AtConnection connection;
    AtHal tcpHalServer = AppHalServer();
    tTab *tabPtr;
    const char *pHeading[] = {"Attribute", "Value"};

    tabPtr = TableAlloc(2, mCount(pHeading), pHeading);
    if (tabPtr == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Cannot create table\r\n");
        return;
        }

    connection = AtHalTcpServerAccessConnection(tcpHalServer);
    StrToCell(tabPtr, 0, 0, "HAL server listening port");
    if (connection)
        StrToCell(tabPtr, 0, 1, CliNumber2String(AtConnectionServerListeningPort(connection), "%d"));
    else
        StrToCell(tabPtr, 0, 1, "Not started");

    connection = AtCliServerConnection();
    StrToCell(tabPtr, 1, 0, "CLI server listening port");
    if (connection)
        StrToCell(tabPtr, 1, 1, CliNumber2String(AtConnectionServerListeningPort(connection), "%d"));
    else
        StrToCell(tabPtr, 1, 1, "Not started");

    TablePrint(tabPtr);
    TableFree(tabPtr);
    }

DEF_SDKCOMMAND(CmdCliClientExit,
               "close shell",
               "Close CLI shell\r\n",
               DEF_NULLPARAM,
               "")
    {
    AtUnused(argv);
    AtUnused(context);
    CliClientExit();

    return cAtOk;
    }

DEF_SDKCOMMAND(cliPollingEnable,
               "app polling enable",
               "Enable polling task\r\n",
               DEF_NULLPARAM,
               "")
    {
    AtUnused(argv);
    AtUnused(context);
    AppPollingTaskEnable(cAtTrue);
    return cAtOk;
    }

DEF_SDKCOMMAND(cliPollingDisable,
               "app polling disable",
               "Disable polling task\r\n",
               DEF_NULLPARAM,
               "")
    {
    AtUnused(argv);
    AtUnused(context);
    AppPollingTaskEnable(cAtFalse);
    return cAtOk;
    }

DEF_SDKCOMMAND(cliPollingTaskPeriod,
               "app polling period",
               "Set polling period\r\n",
               DEF_SDKPARAM("periodMs", "STRING", "Period in milliseconds"),
               "${periodMs}")
    {
    uint32 period = AtStrToDw(lub_argv__get_arg(argv, 0));
    AtUnused(argv);
    AtUnused(context);
    AppPollingTaskPeriodInMsSet(period);
    return cAtOk;
    }

DEF_SDKCOMMAND(cliAutoTestEnable,
               "autotest enable",
               "Enable auto test\r\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAutoTestDisable,
               "autotest disable",
               "Disable auto test\r\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAutoTestGet,
               "autotest",
               "Show auto test is enabled or disabled\r\n",
               DEF_NULLPARAM,
               "")
    {
    AtUnused(argv);
    AtUnused(context);
    AtPrintc(cSevNormal, "%s\n", AutotestIsEnabled() ? "Enable" : "Disable");
    return cAtOk;
    }

DEF_SDKCOMMAND(cliInterruptEnable,
               "app interrupt enable",
               "Enable interrupt task\r\n",
               DEF_NULLPARAM,
               "")
    {
    AtUnused(argv);
    AtUnused(context);
    AppInterruptTaskEnable(cAtTrue);
    return cAtOk;
    }

DEF_SDKCOMMAND(cliInterruptDisable,
               "app interrupt disable",
               "Disable interrupt task\r\n",
               DEF_NULLPARAM,
               "")
    {
    AtUnused(argv);
    AtUnused(context);
    AppInterruptTaskEnable(cAtFalse);
    return cAtOk;
    }

DEF_SDKCOMMAND(cliFmPmTaskEnabel,
               "app fmpm enable",
               "Enable FM/PM task\r\n",
               DEF_NULLPARAM,
               "")
    {
    AtUnused(argv);
    AtUnused(context);
    AppFmPmTaskEnable(cAtTrue);
    return cAtOk;
    }

DEF_SDKCOMMAND(cliFmPmTaskDisable,
               "app fmpm disable",
               "Disable FM/PM task\r\n",
               DEF_NULLPARAM,
               "")
    {
    AtUnused(argv);
    AtUnused(context);
    AppFmPmTaskEnable(cAtFalse);
    return cAtOk;
    }

DEF_SDKCOMMAND(CliAppConnectionShow,
               "show app connection",
               "Show all of network connection\r\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAppInterruptFloodingWatchStart,
               "app interrupt flooding watch start",
               "Start watching for interrupt flooding\r\n",
               DEF_NULLPARAM,
               "")
    {
    AtUnused(argv);
    AtUnused(context);
    AppInterruptFloodingWatch(cAtTrue);
    return cAtOk;
    }

DEF_SDKCOMMAND(CliAppInterruptFloodingWatchStop,
               "app interrupt flooding watch stop",
               "Stop watching for interrupt flooding\r\n",
               DEF_NULLPARAM,
               "")
    {
    AtUnused(argv);
    AtUnused(context);
    AppInterruptFloodingWatch(cAtFalse);
    return cAtOk;
    }

DEF_SDKCOMMAND(cliAppInterruptFloodingContinuousThresholdSet,
               "app interrupt flooding threshold continuous",
               "Set number of interrupts that allow to happen continuously to declare flooding\r\n",
               DEF_SDKPARAM("threshold", "STRING", "Number of interrupts that allow to happen continuously to declare flooding"),
               "${threshold}")
    {
    uint32 threshold = AtStrToDw(lub_argv__get_arg(argv, 0));
    AtUnused(argv);
    AtUnused(context);
    AppInterruptFloodingContinuousThresholdSet(threshold);
    return cAtOk;
    }

DEF_SDKCOMMAND(cliAppInterruptFloodingTotalThresholdSet,
               "app interrupt flooding threshold total",
               "Set number of interrupts that allow to happen within a second\r\n",
               DEF_SDKPARAM("threshold", "STRING", "Number of interrupts that allow to happen within a second"),
               "${threshold}")
    {
    uint32 threshold = AtStrToDw(lub_argv__get_arg(argv, 0));
    AtUnused(argv);
    AtUnused(context);
    AppInterruptFloodingTotalThresholdSet(threshold);
    return cAtOk;
    }

DEF_SDKCOMMAND(cliAppInterruptFloodingShow,
               "show app interrupt flooding",
               "Show interrupt flooding catching tool information\r\n",
               DEF_NULLPARAM,
               "")
    {
    AtUnused(argv);
    AtUnused(context);
    AppInterruptFloodingShow();
    return cAtOk;
    }

eBool CmdAppConnectionShow(char argc, char **argv);
eBool CmdAppConnectionShow(char argc, char **argv)
    {
    AtUnused(argc);
    AtUnused(argv);
    AppConnectionShow();
    return cAtTrue;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ATT
 *
 * File        : ClishAtAttManagerDebug.c
 *
 * Created Date: Aug 24, 2017
 *
 * Description : Att Manager Clish
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"
#include "AtCli.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
DEF_PTYPE("AttLogicForcing", "select", "att(0), dut(1)", "Att Logic Forcing")
DEF_PTYPE("AttForceAlarmDurationUnit", "select", "ms(0), second(1)", "Att Alarm Duration Forcing Unit")
/* Top commands */
TOP_SDKCOMMAND("debug att", "Debug Att")
TOP_SDKCOMMAND("debug att pdh", "Debug Att PDH")
TOP_SDKCOMMAND("debug att pdh force", "Debug Att PDH Force")
TOP_SDKCOMMAND("att pdh force", "Debug Att SDH Force")
TOP_SDKCOMMAND("att pdh force alarm", "Debug Att PDH Force")
TOP_SDKCOMMAND("att pdh force alarm duration", "Debug Att PDH Force")
TOP_SDKCOMMAND("debug att sdh", "Debug Att SDH")
TOP_SDKCOMMAND("debug att sdh ho", "Debug Att SDH Force")
TOP_SDKCOMMAND("debug att sdh force", "Debug Att SDH Force")
TOP_SDKCOMMAND("debug att sdh soh",     "Debug Att SDH SOH")
TOP_SDKCOMMAND("debug att sdh ho pointer", "Debug Att SDH Force")
TOP_SDKCOMMAND("debug att sdh ho pointer looptime", "Debug Att SDH Force")
TOP_SDKCOMMAND("debug att sdh lo", "Debug Att SDH Force")
TOP_SDKCOMMAND("debug att sdh lo pointer", "Debug Att SDH Force")
TOP_SDKCOMMAND("debug att sdh lo pointer looptime", "Debug Att SDH Force")
TOP_SDKCOMMAND("att sdh force", "Debug Att SDH Force")
TOP_SDKCOMMAND("att sdh force alarm", "Debug Att SDH Force")
TOP_SDKCOMMAND("att sdh force alarm duration", "Debug Att SDH Force")
TOP_SDKCOMMAND("att show sdh force", "Debug Att SDH Force")
TOP_SDKCOMMAND("att show map", "Debug Att SDH Force")
/*TOP_SDKCOMMAND("att sdh force pointer_adj", "Att sdh force pointer_adj")*/
TOP_SDKCOMMAND("att sdh force pointer_adj source", "Att sdh force pointer_adj source")
TOP_SDKCOMMAND("att sdh force pointer_adj source multiplexing", "Att sdh force pointer_adj source multiplexing")


DEF_SDKCOMMAND(CliAttPdhDebugForceLogicSet,
               "debug att pdh force logic",
               "Configure logic forcing.\n",
               DEF_SDKPARAM("logic", "AttLogicForcing", ""),
               "${logic}")
    {
    mAtCliCall();
    }
DEF_SDKCOMMAND(CliAttPdhForceAlarmDurationUnitSet,
               "att pdh force alarm duration unit",
               "Configure logic forcing.\n",
               DEF_SDKPARAM("unit", "AttForceAlarmDurationUnit", "Alarm Duration Unit is ms or second"),
               "${unit}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAttSdhDebugForceLogicSet,
               "debug att sdh force logic",
               "Configure logic forcing.\n",
               DEF_SDKPARAM("logic", "AttLogicForcing", ""),
               "${logic}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAttSdhForceAlarmDurationUnitSet,
               "att sdh force alarm duration unit",
               "Configure logic forcing.\n",
               DEF_SDKPARAM("unit", "AttForceAlarmDurationUnit", "Alarm Duration Unit is ms or second"),
               "${unit}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAttSdhDebugHoPointerLooptimeEnable,
               "debug att sdh ho pointer looptime enable",
               "Configure logic forcing.\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }


DEF_SDKCOMMAND(CliAttSdhDebugHoPointerLooptimeDisable,
               "debug att sdh ho pointer looptime disable",
               "Configure logic forcing.\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAttSdhDebugLoPointerLooptimeEnable,
               "debug att sdh lo pointer looptime enable",
               "Configure logic forcing.\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAttSdhDebugLoPointerLooptimeDisable,
               "debug att sdh lo pointer looptime disable",
               "Configure logic forcing.\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAttSdhDebugForceRateShowSet,
               "debug att sdh force rateshow",
               "Configure logic forcing.\n",
               DEF_SDKPARAM("enable", "eBool", "Enable or Disable"),
               "${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAttSdhDebugForceRateCheckSet,
               "debug att sdh force ratecheck",
               "Configure logic forcing.\n",
               DEF_SDKPARAM("enable", "eBool", "Enable or Disable"),
               "${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAttSdhDebugSohColRemoveSet,
               "debug att sdh soh colrmv",
               "Configure logic forcing.\n",
               DEF_SDKPARAM("numCols", "UINT", ""),
               "${numCols}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAttPdhDebugForceRateShowSet,
               "debug att pdh force rateshow",
               "Configure to control show info when force Rate.\n",
               DEF_SDKPARAM("enable", "eBool", "Enable or Disable"),
               "${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAttSdhForcePointerEnable,
               "att sdh force pointer_adj",
               "Enable or Disable Forcing Pointer Adjustment.\n",
               DEF_SDKPARAM("enable", "eBool", "Enable or Disable"),
               "${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAttSdhG783DebugSet,
               "att sdh force pointer_adj g783_debug",
               "Configure to control show info when force pointer mode G783.\n",
               DEF_SDKPARAM("enable", "eBool", "Enable or Disable"),
               "${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAttSdhG783T0Set,
               "att sdh force pointer_adj g783_t0",
               "Configure G783 T0.\n",
               DEF_SDKPARAM("value", "UINT", "in ms"),
               "${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAttSdhG783T1Set,
               "att sdh force pointer_adj g783_t1",
               "Configure G783 T1.\n",
               DEF_SDKPARAM("value", "UINT", "in ms"),
               "${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAttSdhG783T2Set,
               "att sdh force pointer_adj g783_t2",
               "Configure G783 T2.\n",
               DEF_SDKPARAM("value", "UINT", "in ms"),
               "${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAttSdhG783T3Set,
               "att sdh force pointer_adj g783_t3",
               "Configure G783 T3.\n",
               DEF_SDKPARAM("value", "UINT", "in ms"),
               "${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAttSdhG783T3Note2Set,
               "att sdh force pointer_adj g783_t3_note2",
               "Configure G783 T3Note2.\n",
               DEF_SDKPARAM("value", "UINT", "in ms"),
               "${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAttSdhG783T4Set,
               "att sdh force pointer_adj g783_t4",
               "Configure G783 T4.\n",
               DEF_SDKPARAM("value", "UINT", "in ms"),
               "${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAttSdhG783T4Note5Set,
               "att sdh force pointer_adj g783_t4_note5",
               "Configure G783 T4Note5.\n",
               DEF_SDKPARAM("value", "UINT", "in ms"),
               "${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAttSdhG783T5Set,
               "att sdh force pointer_adj g783_t5",
               "Configure G783 T5.\n",
               DEF_SDKPARAM("value", "UINT", "in ms"),
               "${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAttSdhG783T5Note6Set,
               "att sdh force pointer_adj g783_t5_note6",
               "Configure G783 T5Note6.\n",
               DEF_SDKPARAM("value", "UINT", "in ms"),
               "${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAttMapIdMatch,
               "att show map id_match",
               "Configure G783 T5Note6.\n",
               DEF_SDKPARAM("id", "UINT", "ID To check")
               DEF_SDKPARAM("startAddr", "RegisterAddress", "Start Address")
               DEF_SDKPARAM("endAddr",   "RegisterAddress",    "End Address"),
               "${id} ${startAddr} ${endAddr}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAttSdhForcePointerShow,
               "att show sdh force pointer_adj",
               "Configure G783 T5Note6.\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAttSdhForcePointerSourceMultiplexingResync,
               "att sdh force pointer_adj source multiplexing resync",
               "resync source multiplexing\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

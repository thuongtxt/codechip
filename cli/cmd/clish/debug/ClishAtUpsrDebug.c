/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : UPSR
 *
 * File        : ClishAtUpsrDebug.c
 *
 * Created Date: Sep 7, 2015
 *
 * Description : CLISH UPSR command lines.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

#define APSDefType "(aps-sd|aps-sf)"
#define APSDefMask "((("APSDefType"[\\|])+"APSDefType")|"APSDefType")"

#define UpsrAlarmDefType "(ais|lop|plm|uneq|tim|ber-sd|ber-sf)"
#define UpsrAlarmDefMask "((("UpsrAlarmDefType"[\\|])+"UpsrAlarmDefType")|"UpsrAlarmDefType")|none"

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/

TOP_SDKCOMMAND("debug aps", "APS Debugging CLIs")
TOP_SDKCOMMAND("debug aps upsr", "APS UPSR debugging CLIs")
TOP_SDKCOMMAND("debug aps upsr interrupt", "APS UPSR interrupt CLIs")
TOP_SDKCOMMAND("debug aps upsr notification", "APS UPSR notification CLIs")
TOP_SDKCOMMAND("show debug", "Show debugging CLIs")
TOP_SDKCOMMAND("show debug aps", "Show HS Debugging CLIs")

TOP_SDKCOMMAND("debug pw group", "HS Debugging CLIs")
TOP_SDKCOMMAND("debug pw group aps", "APS Group Debugging CLIs")
TOP_SDKCOMMAND("debug pw group hs", "HS Group Debugging CLIs")
TOP_SDKCOMMAND("debug pw group hs label", "HS Group Label Debugging CLIs")
TOP_SDKCOMMAND("show debug pw", "Show HS Debugging CLIs")
TOP_SDKCOMMAND("show debug pw group", "Show HS UPSR Debugging CLIs")

DEF_PTYPE("APSDefMask", "regexp", APSDefMask,
          "\nAPS defect masks:\n"
          "- aps-sf : APS SF\n"
          "- aps-sd : APS SD\n"
          "Note, these masks can be ORed, example aps-sd|aps-sf\n")

DEF_PTYPE("APSDefType", "select", "aps-sf(0), aps-sd(1)", "APS defect types\n")

DEF_PTYPE("UpsrAlarmDefMask", "regexp", UpsrAlarmDefMask,
          "\nPath alarm APS defect masks:\n"
          "- ais    : AIS\n"
          "- lop    : LOP\n"
          "- plm    : PLM\n"
          "- uneq   : UNEQ\n"
          "- tim    : TIM\n"
          "- ber-sd : BER-SD\n"
          "- ber-sf : BER-SF\n"
          "Note, these masks can be ORed, example ais|lop|plm\n")

DEF_PTYPE("GroupIdList", "regexp", ".*", "Group ID List")
DEF_PTYPE("eUpsrGroupLabelSet", "select", "primary(1), backup(2)", "Group label set")

DEF_SDKCOMMAND(cliDebugUpsrIntrMsk,
             "debug aps upsr interruptmask",
             "Set APS UPSR interrupt mask\n",
             DEF_SDKPARAM("pathList", "SdhPathList", "")
             DEF_SDKPARAM("mask", "APSDefMask", "")
             DEF_SDKPARAM("intrMaskEn", "eBool", ""),
             "${pathList} ${mask} ${intrMaskEn}")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(cliDebugUpsrShow,
             "show debug aps upsr",
             "Show APS UPSR configuration and status.\n",
             DEF_SDKPARAM("pathList", "SdhPathList", ""),
             "${pathList}")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(cliDebugUpsrInterruptEnable,
             "debug aps upsr interrupt enable",
             "Enable APS UPSR interrupt.\n",
             DEF_NULLPARAM,
             "")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(cliDebugUpsrInterruptDisable,
             "debug aps upsr interrupt disable",
             "Disable APS UPSR interrupt.\n",
             DEF_NULLPARAM,
             "")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(cliDebugUpsrNotificationEnable,
             "debug aps upsr notification enable",
             "Enable APS UPSR notification.\n",
             DEF_NULLPARAM,
             "")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(cliDebugUpsrNotificationDisable,
             "debug aps upsr notification disable",
             "Disable APS UPSR notification.\n",
             DEF_NULLPARAM,
             "")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(cliDebugUpsrInterruptRestore,
             "debug aps upsr interrupt restore",
             "Restore APS UPSR interrupt.\n",
             DEF_NULLPARAM,
             "")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(cliDebugUpsrNotificationGet,
             "show debug aps upsr notification",
             "Show APS UPSR notification.\n",
             DEF_SDKPARAM("pathList", "SdhPathList", "")
             DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
             DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", "Read to clear without display table"),
             "${pathList} ${readingMode} ${silent}")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(cliDebugApsUpsrGroupEnable,
             "debug pw group aps enable",
             "Enable PW APS group.\n",
             DEF_SDKPARAM("groupIds", "GroupIdList", ""),
             "${GroupIdList}")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(cliDebugApsUpsrGroupDisable,
             "debug pw group aps disable",
             "Disable PW APS group.\n",
             DEF_SDKPARAM("groupIds", "GroupIdList", ""),
             "${GroupIdList}")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(cliDebugHsUpsrGroupTxlabelSelect,
             "debug pw group hs label tx",
             "Select TX label is backup or primary label.\n",
             DEF_SDKPARAM("groupIds", "GroupIdList", "")
             DEF_SDKPARAM("label", "eUpsrGroupLabelSet", ""),
             "${GroupIdList} ${label}")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(cliDebugHsUpsrGroupRxlabelSelect,
             "debug pw group hs label rx",
             "Select RX label is backup or primary label.\n",
             DEF_SDKPARAM("groupIds", "GroupIdList", "")
             DEF_SDKPARAM("label", "eUpsrGroupLabelSet", ""),
             "${GroupIdList} ${label}")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(cliDebugHsUpsrGroupGet,
             "show debug pw group hs",
             "Show HS groups configuration.\n",
             DEF_SDKPARAM("groupIds", "GroupIdList", ""),
             "${GroupIdList}")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(cliDebugApsUpsrGroupGet,
             "show debug pw group aps",
             "Show APS groups configuration.\n",
             DEF_SDKPARAM("groupIds", "GroupIdList", ""),
             "${GroupIdList}")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(cliDebugGetUpsrInfor,
               "debug upsr",
               "Show debug UPSR information.\n",
               DEF_NULLPARAM,
               "")
  {
  mAtCliCall();
  }

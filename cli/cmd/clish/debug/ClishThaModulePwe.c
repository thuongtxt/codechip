/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PWE
 *
 * File        : ClishThaModulePwe.c
 *
 * Created Date: Apr 17, 2017
 *
 * Description : PWE CLISH debug CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("debug show module", "CLIs to show module information")
TOP_SDKCOMMAND("debug show module pwe", "CLIs to show PWE module information")

DEF_SDKCOMMAND(cliThaModulePweCacheUsageShow,
               "debug show module pwe cache",
               "Show PWE cache usage",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliThaModulePweReactivate,
               "debug module pwe reactivate",
               "Reactivate PWE module",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPweModuleDebug,
               "debug module pwe",
               "Show debug information\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliThaModulePweDebuggerShow,
               "debug module pwe table",
               "Show debug information in table format\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

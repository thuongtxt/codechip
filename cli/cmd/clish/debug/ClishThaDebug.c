/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Debug
 *
 * File        : ClishThaDebug.c
 *
 * Created Date: Feb 6, 2013
 *
 * Description : CLIs for hardware debugging purpose
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
#define PhysicalModuleList  "(ocn|poh|xc|cdr|map|pdh|pwe|pda|cla|eth|concate|encap|mpeg|mpig)"

DEF_PTYPE("PdhMuxChannelList", "regexp", ".*", "PDH MUX channel ID(0..127)")
DEF_PTYPE("PdhChannelList", "regexp", ".+",
          "\nChannel List:\n"
          "+ DE1 : de1.<lineId>.<aug1Id>.<au3Tug3Id>.<tug2Id>.<vcxId> or de1.<liu>\n"
          "+ DE3 : de3.<line>.<aug1>.<tug3/au3> or de3.<liu>\n"
          "+ DE2 : de2.<line>.<aug1>.<tug3/au3>.<de2> or de2.<liu>.<de2>\n")

DEF_PTYPE("PhysicalModule", "regexp", PhysicalModuleList,
           "\nInput physical module:\n"
           "  + ocn: Module OCN\n"
           "  + poh: Module POH\n"
           "  + xc : Module XC\n"
           "  + cdr: Module CDR\n"
           "  + map: Module MAP/DEMAP\n"
           "  + pdh: Module PDH\n"
           "  + pwe: Module PWE\n"
           "  + pda: Module PDA\n"
           "  + cla: Module CLA\n"
           "  + eth: Module ETH\n"
           "  + concate: Module CONCATE\n"
           "  + encap: Module ENCAP\n"
           "  + mpeg: Module MPEG\n"
           "  + mpig: Module MPIG\n")

DEF_PTYPE("eAtModule", "select", "sdh(0), pdh(0), ppp(0), fr(0), encap(0), eth(0), pw(0), prbs(0), ima(0), atm(0), ram(0), ber(0), pktanalyzer(0), clock(0), xc(0), aps(0), sur(0), pmc(0)", "Module name")
DEF_PTYPE("eQueuePriority", "select", "highest(1), medium(2), lowest(3)", "Priority mode")
DEF_PTYPE("Vcglists", "regexp", ".*", "List of Vcg IDs. For example: 1-3,6,8 gives list of 1,2,3,6,8")
DEF_PTYPE("QueueIdlists", "regexp", ".*", "List of Queue IDs. For example: 1-3,6,8 gives list of 1,2,3,6,8")
DEF_PTYPE("ChannelIdlists", "regexp", ".*", "List of Channel IDs. For example: 1-3,6,8 gives list of 1,2,3,6,8")
DEF_PTYPE("SourceMode", "regexp", ".*", "Select source:\n"
                                       "  + exaui: eXAUI side\n"
                                       "  + xfi  : XFI side\n")
DEF_PTYPE("PrbsLenMode", "select", "fix(0), increase(1)", "Prbs length mode: fix or increase\n")
DEF_PTYPE("PrbsBandwidth", "regexp", ".*", "Prbs bandwidth (1.2, 2.4, 3.6, 4.8, 6, 7.2, 8.4, 9.6)")
DEF_PTYPE("PtchInsertMode", "select", "none(0), 1-byte(1), 2-bytes(2)", "PTCH insertion modes: 1-byte or 2-bytes\n")
DEF_PTYPE("eConcateDataDumpMode", "select", "full(0), frame(1)", "Data dump mode")
DEF_PTYPE("eConcateDataDumpBus", "select", "pdhfifoin(0), pdhfifoout(1), sinkpdh(2), encap(3)", "Data dump Bus")
DEF_PTYPE("eConcateDdrBus", "select", "vcat2pla(0), pla2vcat(1)", "DDR dump Bus option")
DEF_PTYPE("PmcTickMode", "select", "auto(0), manual(1)", "PMC tick mode")

TOP_SDKCOMMAND("ddr bmt", "CLIs for BMT DDR")
TOP_SDKCOMMAND("pw debug", "CLIs for pw debug prbs")
TOP_SDKCOMMAND("pw debug prbs", "CLIs for pw debug prbs")
TOP_SDKCOMMAND("show pw", "CLIs for pw debug prbs")
TOP_SDKCOMMAND("show pw debug", "CLIs for pw debug prbs")
TOP_SDKCOMMAND("debug pdh de1", "CLI for debug PDH DE1")
TOP_SDKCOMMAND("debug pdh de3", "CLI for debug PDH DE3")
TOP_SDKCOMMAND("debug pdh channel", "CLI for debug PDH DE3")
TOP_SDKCOMMAND("debug pdh de1 prm monitor", "CLI for debug PDH DE1 PRM Monitor")
TOP_SDKCOMMAND("debug module pdh counters", "CLIs to debug PDH module counters")

TOP_SDKCOMMAND("debug module pw counters", "CLIs to debug PW counters")
TOP_SDKCOMMAND("debug module pw alarm", "CLIs to debug PW alarm")
TOP_SDKCOMMAND("debug pw resourcelimit", "CLIs to enable/disable resource limitation")
TOP_SDKCOMMAND("debug prbs", "CLI for debug PRBS")
TOP_SDKCOMMAND("debug concate", "CLI for debug Concate ")
TOP_SDKCOMMAND("debug module sdh counters", "CLIs to debug SONET/SDH counters")
TOP_SDKCOMMAND("debug encap", "CLI for debug ")
TOP_SDKCOMMAND("debug encap channel", "CLI for debug channel")
TOP_SDKCOMMAND("debug ppp bundle", "CLI for debug bundle")

TOP_SDKCOMMAND("show mpig", "CLIs for showing MPIG")
TOP_SDKCOMMAND("show mpig queue", "CLIs for showing MPIG queue")
TOP_SDKCOMMAND("mpig", "CLI for debug MPIG")
TOP_SDKCOMMAND("mpig queue", "CLI for debug MPIG queue")
TOP_SDKCOMMAND("mpig queue hybrid", "CLI for debug MPIG hybrid queueing")
TOP_SDKCOMMAND("mpig queue hybrid", "CLI for debug MPIG hybrid queueing")
TOP_SDKCOMMAND("mpig debug", "CLI for debug MPIG")
TOP_SDKCOMMAND("mpig transmit", "CLI for debug transmit MPIG")
TOP_SDKCOMMAND("mpig transmit oneshot", "CLI for debug transmit oneshot MPIG")
TOP_SDKCOMMAND("mpig channel", "CLI for channel MPIG")

TOP_SDKCOMMAND("mpeg", "CLIs for MPEG Controls")
TOP_SDKCOMMAND("mpeg bundle", "CLIs for mpeg bundle Controls")
TOP_SDKCOMMAND("mpeg bundle queue", "CLIs for mpeg bundle queue Controls")
TOP_SDKCOMMAND("mpeg vcg", "CLIs for MPEG vcg Controls")
TOP_SDKCOMMAND("mpeg vcg queue", "CLIs for MPEG vcg Controls")
TOP_SDKCOMMAND("mpeg link", "CLIs for MPEG Link Controls")
TOP_SDKCOMMAND("mpeg link queue", "CLIs for MPEG Link Queue Controls")
TOP_SDKCOMMAND("mpeg bypass", "CLIs for MPEG Bypass Controls")
TOP_SDKCOMMAND("mpeg bypass queue", "CLIs for MPEG Bypass Queue Controls")
TOP_SDKCOMMAND("show mpeg", "CLIs for Show MPEG Queue Controls")
TOP_SDKCOMMAND("show mpeg bundle", "CLIs for Show MPEG Bundle Queue Controls")
TOP_SDKCOMMAND("show mpeg vcg", "CLIs for Show MPEG VCG Queue Controls")
TOP_SDKCOMMAND("show mpeg link", "CLIs for Show MPEG Link Queue Controls")
TOP_SDKCOMMAND("show mpeg queue", "CLIs for showing MPEG queue")
TOP_SDKCOMMAND("show mpeg bypass", "CLIs for Show MPEG Bypass Queue Controls")

TOP_SDKCOMMAND("debug module eth", "CLIs to select ETH counters module")
TOP_SDKCOMMAND("debug module eth counters", "CLIs to select ETH counters module")
TOP_SDKCOMMAND("debug module eth ptch", "debug ptch of Eth module commands")
TOP_SDKCOMMAND("debug module eth ptch lookup", "debug ptch lookup of Eth module commands")
TOP_SDKCOMMAND("debug pw ptch insert", "debug pw ptch insertion commands")
TOP_SDKCOMMAND("show eth xaui", "CLIs for Show eXAUI counters")
TOP_SDKCOMMAND("debug eth ptchservice", "debug Eth PTCH service commands")
TOP_SDKCOMMAND("debug eth ptchservice ingress", "debug Eth PTCH service ingress commands")
TOP_SDKCOMMAND("debug module eth fsm tx", "CLIs to debug ETH TX FSM")
TOP_SDKCOMMAND("debug module eth fsm rx", "CLIs to debug ETH RX FSM")

TOP_SDKCOMMAND("debug eth xaui", "Debug Eth XAUI commands")
TOP_SDKCOMMAND("debug eth xaui prbs generate", "Debug Eth XAUI Generate commands")
TOP_SDKCOMMAND("debug eth xaui prbs side", "Debug Eth XAUI Side commands")
TOP_SDKCOMMAND("show eth debug", "Show Debug commands")
TOP_SDKCOMMAND("show eth debug xaui", "Show Debug Eth XAUI commands")
TOP_SDKCOMMAND("debug eth flow ptch insert", "debug eth flow ptch insertion commands")
TOP_SDKCOMMAND("debug module encap counters", "CLIs to debug Encap counters")
TOP_SDKCOMMAND("debug module ppp counters", "CLIs to debug PPP module counters")
TOP_SDKCOMMAND("debug module pmc", "CLIs for PCM module")

TOP_SDKCOMMAND("debug show pmc", "CLI for PMC module")
TOP_SDKCOMMAND("debug show pmc cla", "CLI PMC module")
TOP_SDKCOMMAND("debug show pmc eth", "CLI PMC module")
TOP_SDKCOMMAND("debug show pmc pdhmux", "CLI PMC module")
TOP_SDKCOMMAND("debug eth sgmii", "Debug Eth SGMII commands")
TOP_SDKCOMMAND("debug eth sgmii prbs", "Debug Eth SGMII PRBS commands")
TOP_SDKCOMMAND("debug eth qsgmii", "Debug Eth QSGMII commands")
TOP_SDKCOMMAND("debug eth qsgmii prbs", "Debug Eth QSGMII PRBS commands")
TOP_SDKCOMMAND("debug eth service", "Debug Eth Service commands")
TOP_SDKCOMMAND("debug pmc", "Debug PMC commands")

DEF_SDKCOMMAND(cliBmtDdrRead,
               "ddr bmt rd",
               "Read BMT DDR\n",
               DEF_SDKPARAM("address", "RegisterAddress", ""),
               "${address}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliBmtDdrWrite,
               "ddr bmt wr",
               "Read BMT DDR\n",
               DEF_SDKPARAM("address", "RegisterAddress", "")
               DEF_SDKPARAM("value", "RegisterValue", ""),
               "${address} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwPrbsCheck,
               "pw debug prbs check",
               "pw debug prbs check\n",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwPrbsShow,
               "show pw debug prbs",
               "show pw debug prbs\n",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwPrbsEnable,
               "pw debug prbs enable",
               "pw debug rbs enable\n",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwPrbsDisable,
               "pw debug prbs disable",
               "pw debug prbs disable\n",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhChannelRegsDebug,
               "debug sdh path regs",
               "Show all registers related to an SDH channel\n",
               DEF_SDKPARAM("channelList", "MappableChannelList", "")
               DEF_SDKPARAM("phyModule", "PhysicalModule", ""),
               "${channelList} ${phyModule}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhChannelRegsDebug,
               "debug pdh channel regs",
               "Show all registers related to a PDH channel\n",
               DEF_SDKPARAM("channelList", "PdhChannelList", "")
               DEF_SDKPARAM("phyModule", "PhysicalModule", ""),
               "${channelList} ${phyModule}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1RegsDebug,
               "debug pdh de1 regs",
               "Show all registers related to a PDH channel\n",
               DEF_SDKPARAM("de1List", "PdhDe1List", "")
               DEF_SDKPARAM("phyModule", "PhysicalModule", ""),
               "${de1List} ${phyModule}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3RegsDebug,
               "debug pdh de3 regs",
               "Show all registers related to a PDH channel\n",
               DEF_SDKPARAM("de3List", "PdhDe3List", "")
               DEF_SDKPARAM("phyModule", "PhysicalModule", ""),
               "${de3List} ${phyModule}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwRegsDebug,
               "debug pw regs",
               "Show all registers related to a PW\n",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("phyModule", "PhysicalModule", ""),
               "${pwList} ${phyModule}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthPortRegsDebug,
               "debug eth port regs",
               "Show all registers related to an Ethernet port\n",
               DEF_SDKPARAM("portId", "PortIdList", "")
               DEF_SDKPARAM("phyModule", "PhysicalModule", ""),
               "${portId} ${phyModule}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliOcnModuleDebug,
               "debug module ocn",
               "Debug module OCN\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMapModuleDebug,
               "debug module map",
               "Debug module MAP\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliClaModuleDebug,
               "debug module cla",
               "Debug module CLA\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdaModuleDebug,
               "debug module pda",
               "Debug module PDA\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhChannelCdrDebug,
               "debug sdh path cdr",
               "Show CDR debug information of SDH path\n",
               DEF_SDKPARAM("channelList", "MappableChannelList", ""),
               "${channelList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwHeaderCdrDebug,
               "debug pw cdr",
               "Show debug PW header information at CDR module\n",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwPrbsDebug,
               "debug pw prbs",
               "Debug PW PRBS at several check points PLA,PWE,CLA and PDA\n",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwSpeedDebug,
               "debug pw speed",
               "Show PW speed information\n",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1CdrDebug,
               "debug pdh de1 cdr",
               "Show CDR debug information of PDH DE1\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3CdrDebug,
               "debug pdh de3 cdr",
               "Show CDR debug information of PDH DE3\n",
               DEF_SDKPARAM("de3s", "PdhDe3List", ""),
               "${de3s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliThaModulePwDebugCounterModule,
               "debug module pw counters select",
               "Specify module to retrieve PW counters.\n",
               DEF_SDKPARAM_OPTION("moduleName", "eAtModule", ""),
               "${moduleName}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliThaModulePwDebugAlarmModule,
               "debug module pw alarm select",
               "Specify module to retrieve PW alarm.\n",
               DEF_SDKPARAM_OPTION("moduleName", "eAtModule", ""),
               "${moduleName}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwDebugHeader,
               "debug pw header",
               "Debug PW header.\n",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwResourcesLimitDisable,
               "debug pw resourcelimit disable",
               "Disable resource limitation.\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwResourcesLimitEnable,
               "debug pw resourcelimit enable",
               "Enable resource limitation.\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliBundleQueueMinThreshold,
               "mpeg bundle queue minthreshold",
               "Configure minimum threshold for bundle queue.\n",
               DEF_SDKPARAM("bundleList", "MpBundlelists", "")
               DEF_SDKPARAM("internalFifo", "UINT", "Internal FIFO queue threshold in packet unit")
               DEF_SDKPARAM("externalFifo", "UINT", "External FIFO queue threshold in unit of 128 packets"),
               "${bundleList} ${internalFifo} ${externalFifo}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliBundleQueueMaxThreshold,
               "mpeg bundle queue maxthreshold",
               "Configure maximum threshold for bundle queue.\n",
               DEF_SDKPARAM("bundleList", "MpBundlelists", "")
               DEF_SDKPARAM("internalFifo", "UINT", "Internal FIFO queue threshold in packet unit")
               DEF_SDKPARAM("externalFifo", "UINT", "External FIFO queue threshold in unit of 128 packets"),
               "${bundleList} ${internalFifo} ${externalFifo}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliBundleQueuePriority,
               "mpeg bundle queue priority",
               "Configure priority for bundle queue.\n",
               DEF_SDKPARAM("bundleList", "MpBundlelists", "")
               DEF_SDKPARAM("priority", "eQueuePriority", ""),
               "${bundleList} ${priority}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliBundleQueueQuantum,
               "mpeg bundle queue quantum",
               "Configure quantum value for DWRR sheduling mode.\n",
               DEF_SDKPARAM("bundleList", "MpBundlelists", "")
               DEF_SDKPARAM("quantum", "UINT", "Quantum in bytes for DWRR scheduling mode"),
               "${bundleList} ${quantum}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliBundleQueueGet,
               "show mpeg bundle queue",
               "Get bundle queue configuration.\n",
               DEF_SDKPARAM("bundleList", "MpBundlelists", ""),
               "${bundleList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgQueueMaxThreshold,
               "mpeg vcg queue maxthreshold",
               "Configure maximum threshold for vcg queue.\n",
               DEF_SDKPARAM("vcgList", "Vcglists", "")
               DEF_SDKPARAM("internalFifo", "UINT", "Internal FIFO queue threshold in packet unit")
               DEF_SDKPARAM("externalFifo", "UINT", "External FIFO queue threshold in unit of 128 packets"),
               "${vcgList} ${internalFifo} ${externalFifo}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgQueueMinThreshold,
               "mpeg vcg queue minthreshold",
               "Configure minimum threshold for vcg queue.\n",
               DEF_SDKPARAM("vcgList", "Vcglists", "")
               DEF_SDKPARAM("internalFifo", "UINT", "Internal FIFO queue threshold in packet unit")
               DEF_SDKPARAM("externalFifo", "UINT", "External FIFO queue threshold in unit of 128 packets"),
               "${vcgList} ${internalFifo} ${externalFifo}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgQueuePriority,
               "mpeg vcg queue priority",
               "Configure priority for Vcg queue.\n",
               DEF_SDKPARAM("vcgList", "Vcglists", "")
               DEF_SDKPARAM("priority", "eQueuePriority", ""),
               "${vcgList} ${priority}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgQueueQuantum,
               "mpeg vcg queue quantum",
               "Configure quantum value for DWRR sheduling mode.\n",
               DEF_SDKPARAM("vcgList", "Vcglists", "")
               DEF_SDKPARAM("quantum", "UINT", "Quantum in byte for DWRR scheduling mode"),
               "${vcgList} ${quantum}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgQueueGet,
               "show mpeg vcg queue",
               "Get vcg queue configuration.\n",
               DEF_SDKPARAM("vcgList", "Vcglists", ""),
               "${vcgList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliLinkQueueMaxThreshold,
               "mpeg link queue maxthreshold",
               "Configure maximum threshold for vcg queue.\n",
               DEF_SDKPARAM("pppLinkList", "PppLinklist", "")
               DEF_SDKPARAM("queueList", "QueueIdlists", "")
               DEF_SDKPARAM("internalFifo", "UINT", "Internal FIFO queue threshold in packet unit")
               DEF_SDKPARAM("externalFifo", "UINT", "External FIFO queue threshold in unit of 128 packets"),
               "${pppLinkList} ${queueList} ${internalFifo} ${externalFifo}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliLinkQueueMinThreshold,
               "mpeg link queue minthreshold",
               "Configure minimum threshold for vcg queue.\n",
               DEF_SDKPARAM("pppLinkList", "PppLinklist", "")
               DEF_SDKPARAM("queueList", "QueueIdlists", "")
               DEF_SDKPARAM("internalFifo", "UINT", "Internal FIFO queue threshold in packet unit")
               DEF_SDKPARAM("externalFifo", "UINT", "External FIFO queue threshold in unit of 128 packets"),
               "${pppLinkList} ${queueList} ${internalFifo} ${externalFifo}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliLinkQueuePriority,
               "mpeg link queue priority",
               "Configure prority for link queue.\n",
               DEF_SDKPARAM("pppLinkList", "PppLinklist", "")
               DEF_SDKPARAM("queueList", "QueueIdlists", "")
               DEF_SDKPARAM("priority", "eQueuePriority", ""),
               "${pppLinkList} ${queueList} ${priority}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliLinkQueueQuantum,
               "mpeg link queue quantum",
               "Configure quantum for link queue.\n",
               DEF_SDKPARAM("pppLinkList", "PppLinklist", "")
               DEF_SDKPARAM("queueList", "QueueIdlists", "")
               DEF_SDKPARAM("quantum", "UINT", "Number bytes in DWRR scheduling mode"),
               "${pppLinkList} ${queueList} ${quantum}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliLinkQueueGet,
               "show mpeg link queue",
               "Get configuration of link queue.\n",
               DEF_SDKPARAM("pppLinkList", "PppLinklist", "")
               DEF_SDKPARAM("queueList", "QueueIdlists", ""),
               "${pppLinkList} ${queueList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMpigQueueThresholdSet,
               "mpig queue threshold",
               "Set threshold for mpig queue.\n",
               DEF_SDKPARAM("queue", "QueueIdlists", "")
               DEF_SDKPARAM("internalThres", "UINT", "Internal FIFO queue threshold in packet unit")
               DEF_SDKPARAM("externalThres", "UINT", "External FIFO queue threshold in unit of 128 packets"),
               "${queue} ${internalThres} ${externalThres}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMpigQueueEnable,
               "mpig queue enable",
               "Enable queue.\n",
               DEF_SDKPARAM("queue", "QueueIdlists", ""),
               "${queue}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMpigQueueDisable,
               "mpig queue disable",
               "Disable queue.\n",
               DEF_SDKPARAM("queue", "QueueIdlists", ""),
               "${queue}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMpigHybridQueueingEnable,
               "mpig queue hybrid enable",
               "Enable queue.\n",
               DEF_SDKPARAM("queue", "QueueIdlists", ""),
               "${queue}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMpigHybridQueueingDisable,
               "mpig queue hybrid disable",
               "Disable queue.\n",
               DEF_SDKPARAM("queue", "QueueIdlists", ""),
               "${queue}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMpigQueueQuantumSet,
               "mpig queue quantum",
               "Set quantum in byte for queue.\n",
               DEF_SDKPARAM("queue", "QueueIdlists", "")
               DEF_SDKPARAM("quantum", "UINT", "Quantum in byte"),
               "${queue} ${quantum}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMpigQueuePrioritySet,
               "mpig queue priority",
               "Set priority for queue.\n",
               DEF_SDKPARAM("queue", "QueueIdlists", "")
               DEF_SDKPARAM("priority", "eQueuePriority", ""),
               "${queue} ${priority}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMpigQueueConfigAllShow,
               "show mpig queues",
               "Show configuration of all MPIG queues.\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMpigQueueCounterShow,
               "show mpig queue counters",
               "Show counters of queue.\n",
               DEF_SDKPARAM("queue", "QueueIdlists", "")
               DEF_SDKPARAM("readMode", "HistoryReadingMode", ""),
               "${queue} ${readMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMpigGlobalCounterShow,
               "show mpig counters",
               "Show global counters of MPIG.\n",
               DEF_SDKPARAM("readMode", "HistoryReadingMode", ""),
               "${readMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMpigDebug,
               "debug module mpig",
               "Debug sticky of MPIG\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMpigDumpInforDebugEnable,
               "mpig debug dumpinfo enable",
               "Enable dump debug infrmation \n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMpigDumpInforDebugDisable,
               "mpig debug dumpinfo disable",
               "Disable dump debug infrmation \n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMpigDumpInforDebug,
               "mpig debug dumpinfo",
               "Dump debug infrmation \n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMpigTransmitOneshotEnable,
               "mpig transmit oneshot enable",
               "Enable oneshot mode  \n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMpigTransmitOneshotDisable,
               "mpig transmit oneshot disable",
               "Disable oneshot mode \n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMpigTransmitOneshotNumPktSet,
               "mpig transmit oneshot numpkt",
               "Set number of transmiting packets in mode oneshot .\n",
               DEF_SDKPARAM("numpkt", "UINT", "Number of packets"),
               "${numpkt}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMpigTransmitOneshotNumPktStoreBeforeTransmitSet,
               "mpig transmit oneshot numpktstorebefore",
               "Set number of packets stored before transmitting in mode oneshot.\n",
               DEF_SDKPARAM("numpkt", "UINT", "Number of packets"),
               "${numpkt}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMpigMonitorPacketSet,
               "mpig channel pktlenmonitor",
               "Set expected packet length for channels.\n",
               DEF_SDKPARAM("channel", "ChannelIdlists", "")
               DEF_SDKPARAM("pktlen", "UINT", "packket length"),
               "${channel} ${pktlen}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliBundleQueueEnable,
               "mpeg bundle queue enable",
               "Enable queue for bundle.\n",
               DEF_SDKPARAM("bundleList", "MpBundlelists", ""),
               "${bundleList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliBundleQueueDisable,
               "mpeg bundle queue disable",
               "Disable queue for bundle.\n",
               DEF_SDKPARAM("bundleList", "MpBundlelists", ""),
               "${bundleList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgQueueEnable,
               "mpeg vcg queue enable",
               "Enable queue for VCG.\n",
               DEF_SDKPARAM("vcgList", "Vcglists", ""),
               "${bundleList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgQueueDisable,
               "mpeg vcg queue disable",
               "Disable queue for VCG.\n",
               DEF_SDKPARAM("vcgList", "Vcglists", ""),
               "${bundleList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliLinkQueueEnable,
               "mpeg link queue enable",
               "Enable queue for PPP Link.\n",
               DEF_SDKPARAM("linkList", "PppLinklist", "")
               DEF_SDKPARAM("queueId", "QueueIdlists", ""),
               "${linkList} ${queueId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliLinkQueueDisable,
               "mpeg link queue disable",
               "Disable queue for PPP Link.\n",
               DEF_SDKPARAM("linkList", "PppLinklist", "")
               DEF_SDKPARAM("queueId", "QueueIdlists", ""),
               "${linkList} ${queueId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDebugCounterGet,
               "show mpeg queue counters",
               "Get debug counters mpeg.\n",
               DEF_SDKPARAM("readMode", "HistoryReadingMode", ""),
               "${readMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliMpegModuleDebug,
               "debug module mpeg",
               "Debug module MPEG\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthBypassQueueMaxThreshold,
               "mpeg bypass queue maxthreshold",
               "Configure maximum threshold for Ethernet bypass queue.\n",
               DEF_SDKPARAM("portList", "PortIdList", "Gigabit Ethernet ports 1-4, eXAUI port 5-6.")
               DEF_SDKPARAM("queueList", "QueueIdlists", "")
               DEF_SDKPARAM("internalFifo", "UINT", "Internal FIFO queue threshold in packet unit")
               DEF_SDKPARAM("externalFifo", "UINT", "External FIFO queue threshold in unit of 128 packets"),
               "${portList} ${queueList} ${internalFifo} ${externalFifo}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthBypassQueueMinThreshold,
               "mpeg bypass queue minthreshold",
               "Configure minimum threshold for Ethernet bypass queue.\n",
               DEF_SDKPARAM("portList", "PortIdList", "Gigabit Ethernet ports 1-4, eXAUI port 5-6.")
               DEF_SDKPARAM("queueList", "QueueIdlists", "")
               DEF_SDKPARAM("internalFifo", "UINT", "Internal FIFO queue threshold in packet unit")
               DEF_SDKPARAM("externalFifo", "UINT", "External FIFO queue threshold in unit of 128 packets"),
               "${portList} ${queueList} ${internalFifo} ${externalFifo}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthBypassQueuePriority,
               "mpeg bypass queue priority",
               "Configure prority for link queue.\n",
               DEF_SDKPARAM("portList", "PortIdList", "Gigabit Ethernet ports 1-4, eXAUI port 5-6.")
               DEF_SDKPARAM("queueList", "QueueIdlists", "")
               DEF_SDKPARAM("priority", "eQueuePriority", ""),
               "${portList} ${queueList} ${priority}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthBypassQueueQuantum,
               "mpeg bypass queue quantum",
               "Configure quantum for Ethernet bypass queue.\n",
               DEF_SDKPARAM("portList", "PortIdList", "Gigabit Ethernet ports 1-4, eXAUI port 5-6.")
               DEF_SDKPARAM("queueList", "QueueIdlists", "")
               DEF_SDKPARAM("quantum", "UINT", "Number bytes in DWRR scheduling mode"),
               "${portList} ${queueList} ${quantum}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthBypassQueueGet,
               "show mpeg bypass queue",
               "Get configuration of Ethernet bypass queue.\n",
               DEF_SDKPARAM("portList", "PortIdList", "Gigabit Ethernet ports 1-4, eXAUI port 5-6.")
               DEF_SDKPARAM("queueList", "QueueIdlists", ""),
               "${portList} ${queueList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthBypassQueueEnable,
               "mpeg bypass queue enable",
               "Enable queue for Ethernet bypass.\n",
               DEF_SDKPARAM("portList", "PortIdList", "Gigabit Ethernet ports 1-4, eXAUI port 5-6.")
               DEF_SDKPARAM("queueId", "QueueIdlists", ""),
               "${portList} ${queueId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthBypassQueueDisable,
               "mpeg bypass queue disable",
               "Disable queue for Ethernet bypass.\n",
               DEF_SDKPARAM("portList", "PortIdList", "Gigabit Ethernet ports 1-4, eXAUI port 5-6.")
               DEF_SDKPARAM("queueId", "QueueIdlists", ""),
               "${portList} ${queueId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPtchLookupEnable,
               "debug module eth ptch lookup enable",
               "Enable PTCH lookup for CEM/iMSG/EoP",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPtchLookupDisable,
               "debug module eth ptch lookup disable",
               "Disable PTCH lookup for CEM/iMSG/EoP.\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtPwPtchInsertionMode,
               "debug pw ptch insert mode",
               "Enable PTCh insertion mode for PW channel",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("mode", "PtchInsertMode", ""),
               "${pwList} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtPwPtchInsertionShow,
               "debug pw ptch",
               "Show PTCh insertion for PW channel",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtEthEXauiPrbsSideMonitoring,
               "debug eth xaui prbs side monitor",
               "Select source to monitor PRBS engine",
               DEF_SDKPARAM("portId", "PortIdList", "")
               DEF_SDKPARAM("source", "SourceMode", ""),
               "${portId} ${source}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtExauiCounterShow,
               "show eth xaui counters",
               "show eth xaui counters.\n",
               DEF_SDKPARAM("readMode", "HistoryReadingMode", ""),
               "${readMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtEthEXauiPrbsSingleGenerate,
               "debug eth xaui prbs generate single",
               "Set number packet generate",
               DEF_SDKPARAM("portId", "PortIdList", "")
               DEF_SDKPARAM("numPkt", "UINT", ""),
               "${portId} ${numPkt}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtEthEXauiPrbsContinuousGenerate,
               "debug eth xaui prbs generate continuous",
               "Set Continuous generate packet",
               DEF_SDKPARAM("portId", "PortIdList", ""),
               "${portId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtEthEXauiPrbsEnable,
               "debug eth xaui prbs enable",
               "Enable generate prbs",
               DEF_SDKPARAM("portId", "PortIdList", ""),
               "${portId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtEthEXauiPrbsDisable,
               "debug eth xaui prbs disable",
               "Disable generate prbs",
               DEF_SDKPARAM("portId", "PortIdList", ""),
               "${portId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtEthEXauiPrbsForceError,
               "debug eth xaui prbs force",
               "Enable force error",
               DEF_SDKPARAM("portId", "PortIdList", ""),
               "${portId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtEthEXauiPrbsUnForceError,
               "debug eth xaui prbs unforce",
               "Disable force error",
               DEF_SDKPARAM("portId", "PortIdList", ""),
               "${portId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtPrbsEngineEXauiLenModeSet,
               "debug eth xaui prbs lenmode",
               "Configure PRBS length mode",
               DEF_SDKPARAM("portId", "PortIdList", "")
               DEF_SDKPARAM("lenmode", "PrbsLenMode", ""),
               "${portId} {lenmode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtPrbsEngineEXauiMaxLenSet,
               "debug eth xaui prbs maxlen",
               "Configure max length for PRBS packet",
               DEF_SDKPARAM("portId", "PortIdList", "")
               DEF_SDKPARAM("pktlen", "UINT", "Packet length"),
               "${portId} ${pktlen}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtPrbsEngineEXauiMinLenSet,
               "debug eth xaui prbs minlen",
               "Configure min length for PRBS packet",
               DEF_SDKPARAM("portId", "PortIdList", "")
               DEF_SDKPARAM("pktlen", "UINT", "Packet length"),
               "${portId} ${pktlen}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtPrbsEngineEXauiBandwidthSet,
               "debug eth xaui prbs bandwidth",
               "Configure bandwidth for PRBS packet",
               DEF_SDKPARAM("portId", "PortIdList", "")
               DEF_SDKPARAM("bandwidth", "PrbsBandwidth", ""),
               "${portId} ${bandwidth}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtEthEXauiPrbsConfigGet,
               "debug eth xaui prbs",
               "Get configure prbs",
               DEF_SDKPARAM("portId", "PortIdList", ""),
               "${portId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliModuleEthIngressChannelIdSet,
               "debug eth ptchservice ingress sop",
               "Congifure Channel ID PTCH service for Direction from XFI --> HyPhy.\n",
               DEF_SDKPARAM("ptchservice", "eAtPtchServiceType", "")
               DEF_SDKPARAM("channel", "HexValue", "Offset value"),
               "${ptchservice} ${channel}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtEthEXauiPrbsCounterGet,
               "debug eth xaui prbs counters",
               "Get Counter prbs",
               DEF_SDKPARAM("portId", "PortIdList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", ""),
               "${portId} ${readingMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtConcateDumpData,
               "debug concate dumpdata",
               "Dump data for concate module",
               DEF_SDKPARAM("mode", "eConcateDataDumpMode", "")
               DEF_SDKPARAM("bus", "eConcateDataDumpBus", ""),
               "${mode} ${bus}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDebugDeviceCounter,
               "debug device counters",
               "Read/clear Device debug counters\n",
               DEF_SDKPARAM("counterReadMode", "HistoryReadingMode", "read only/read to clear mode"),
               "${counterReadMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtEthFlowPtchInsertionMode,
               "debug eth flow ptch insert mode",
               "Enable PTCh insertion mode for Flows",
               DEF_SDKPARAM("flowList", "FlowIdList", "")
               DEF_SDKPARAM("mode", "PtchInsertMode", ""),
               "${flowList} ${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtEthFlowPtchInsertionShow,
               "debug eth flow ptch",
               "Show PTCh insertion for Flows",
               DEF_SDKPARAM("flowList", "FlowIdList", ""),
               "${flowList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliConcateModuleDebug,
               "debug module concate",
               "Debug module Concate\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliEthFlowHeaderShow,
               "debug eth flow header",
               "Debug Ethernet Flow header.\n",
               DEF_SDKPARAM("flowList", "FlowIdList", ""),
               "${flowList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishAtConcateDumpDdr,
               "debug concate dumpddr",
               "Dump DDR for concate module",
               DEF_SDKPARAM("line", "UINT", "")
               DEF_SDKPARAM("ddrBus", "eConcateDdrBus", ""),
               "${mode} ${ddrBus}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliConcateVcgBertSet,
               "debug vcg bert",
               "Set BERT for debug VCG\n",
               DEF_SDKPARAM("vcg", "UINT", "")
               DEF_SDKPARAM("enable", "eBool", " enable/disable ")
               DEF_SDKPARAM("prbsMode", "eAtPrbsMode", " PRBS mode"),
               "${vcg} ${enable} ${prbsMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliConcateVcgBertCounters,
               "debug vcg bert counters",
               "Show BERT counters\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliThaModuleEncapDebugCounterModule,
               "debug module encap counters select",
               "Specify module to retrieve Encap counters.\n",
               DEF_SDKPARAM_OPTION("moduleName", "eAtModule", ""),
               "${moduleName}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliThaModulePppDebugCounterModule,
               "debug module ppp counters select",
               "Specify module to retrieve PPP module counters.\n",
               DEF_SDKPARAM_OPTION("moduleName", "eAtModule", ""),
               "${moduleName}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDebugPwPrbsEnable,
               "debug pw prbs enable",
               "Enable gen prbs at PDA\n",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDebugPwPrbsDisable,
               "debug pw prbs disable",
               "Disable gen prbs at PDA\n",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDebugPwPrbsGet,
               "debug show pw prbs",
               "Monitor Pw PRBS engine\n",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishDebugSgmiiPrbsEnable,
               "debug eth sgmii prbs enable",
               "Enable generate/monitor prbs",
               DEF_SDKPARAM("portId", "PortIdList", ""),
               "${portId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishDebugSgmiiPrbsDisable,
               "debug eth sgmii prbs disable",
               "Disable generate/monitor prbs",
               DEF_SDKPARAM("portId", "PortIdList", ""),
               "${portId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishDebugQsgmiiPrbsEnable,
               "debug eth qsgmii prbs enable",
               "Enable generate/monitor prbs",
               DEF_SDKPARAM("portId", "PortIdList", "")
               DEF_SDKPARAM("laneId", "PortIdList", ""),
               "${portId} ${laneId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishDebugQsgmiiPrbsDisable,
               "debug eth qsgmii prbs disable",
               "Disable generate/monitor prbs",
               DEF_SDKPARAM("portId", "PortIdList", "")
               DEF_SDKPARAM("laneId", "PortIdList", ""),
               "${portId} ${laneId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliThaModuleSdhDebugCountersModule,
               "debug module sdh counters select",
               "Specify module to retrieve SONET/SDH module counters.\n",
               DEF_SDKPARAM_OPTION("moduleName", "eAtModule", ""),
               "${moduleName}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliThaModulePdhDebugCountersModule,
               "debug module pdh counters select",
               "Specify module to retrieve PDH module counters.\n",
               DEF_SDKPARAM_OPTION("moduleName", "eAtModule", ""),
               "${moduleName}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliThaModuleEthDebugCountersModule,
               "debug module eth counters select",
               "Specify module to retrieve ETH module counters.\n",
               DEF_SDKPARAM_OPTION("moduleName", "eAtModule", ""),
               "${moduleName}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDebugModulePmcClaCounter,
               "debug show pmc cla counters",
               "Show CLASSIFY counter from PMC module",
               DEF_SDKPARAM("counterReadMode", "HistoryReadingMode", "read only/read to clear mode"),
               "${counterReadMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDebugModulePmcEthPdhMuxCounter,
               "debug show pmc eth counters",
               "Show ETH counter for PDH MUX from PMC module",
               DEF_SDKPARAM("counterReadMode", "HistoryReadingMode", "read only/read to clear mode"),
               "${counterReadMode}")
        {
        mAtCliCall();
        }

DEF_SDKCOMMAND(cliDebugModulePmcPdhMuxCounter,
               "debug show pmc pdhmux counters",
               "Show DE1/DE3 counter for PDH MUX from PMC module",
               DEF_SDKPARAM("channelId", "PdhMuxChannelList", "Channel ID list")
               DEF_SDKPARAM("counterReadMode", "HistoryReadingMode", "read only/read to clear mode"),
               "${channelId} ${counterReadMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliThaModulePmcDebugTickModeSet,
               "debug module pmc tick mode",
               "Set PMC tick mode for PMC module",
               DEF_SDKPARAM("mode", "PmcTickMode", ""),
               "${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliThaModulePmcDebugTickSet,
               "debug module pmc tick",
               "Set PMC tick for PMC module\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliThaModuleEthDebugTxFsmEnable,
               "debug module eth fsm tx enable",
               "Enable TX FSM.\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliThaModuleEthDebugTxFsmDisable,
               "debug module eth fsm tx disable",
               "Disable TX FSM.\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliThaModuleEthDebugRxFsmEnable,
               "debug module eth fsm rx enable",
               "Enable RX FSM.\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliThaModuleEthDebugRxFsmDisable,
               "debug module eth fsm rx disable",
               "Disable RX FSM.\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliThaModuleEthDebugFsmShow,
               "debug module eth fsm",
               "Show Eth TX/RX FSM configuration and status.\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliDebugEthernetCounterPerServiceGet,
               "debug eth service counters",
               "Get counter per service",
               DEF_SDKPARAM("portId", "PortIdList", "")
               DEF_SDKPARAM("counterReadMode", "HistoryReadingMode", "read only/read to clear mode"),
               "${portId} ${counterReadMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDebugEncapChannelRegs,
               "debug encap channel regs",
               "Debug encap channel\n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("phyModule", "PhysicalModule", ""),
               "${channelList} ${phyModule}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDebugPppBundleRegs,
               "debug ppp bundle regs",
               "Debug ppp bundle \n",
               DEF_SDKPARAM("channelList", "EncChannelList", "")
               DEF_SDKPARAM("phyModule", "PhysicalModule", ""),
               "${channelList} ${phyModule}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDebugEthFlowRegs,
               "debug eth flow regs",
               "Debug eth flow \n",
               DEF_SDKPARAM("flowList", "FlowIdList", "")
               DEF_SDKPARAM("phyModule", "PhysicalModule", ""),
               "${flowList} ${phyModule}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliThaModulePdaDebuggerShow,
               "debug module pda table",
               "Show debug information in table format\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliThaModulePmcShow,
               "show module pmc",
               "Show PMC module information",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliThaModuleEthDebugDccEnable,
               "debug module eth dcc enable",
               "Enable DCC.\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliThaModuleEthDebugDccDisable,
               "debug module eth dcc disable",
               "Disable DCC\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliThaModuleEthDebugDccShow,
               "debug module eth dcc",
               "Show Eth Dcc configuration\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDebugPmcCounterGet,
           "debug pmc counters",
           "Get debug pmc global counter.\n",
           DEF_SDKPARAM("readMode", "HistoryReadingMode", ""),
           "${readMode}")
    {
    mAtCliCall();
    }

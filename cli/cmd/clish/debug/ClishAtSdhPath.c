/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : ClishAtSdhPath.c
 *
 * Created Date: Mar 27, 2016
 *
 * Description : CLISH debug CLI for path
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("sdh path poh", "CLIs to control POH\n")
TOP_SDKCOMMAND("sdh path poh monitor", "CLIs to control POH monitoring\n")
TOP_SDKCOMMAND("sdh path poh insertion", "CLIs to control POH inesrtion\n")

DEF_SDKCOMMAND(cliAtSdhPathPohMonitorEnable,
               "sdh path poh monitor enable",
               "Enable POH monitoring\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathPohMonitorDisable,
               "sdh path poh monitor disable",
               "Disable POH monitoring\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathPohInsertionEnable,
               "sdh path poh insertion enable",
               "Enable POH insertion\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathPohInsertionDisable,
               "sdh path poh insertion disable",
               "Disable POH insertion\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathPohShow,
               "show sdh path poh",
               "Show POH information\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathCountersVirtualFifo,
               "show sdh path counters virtualfifo",
               "Get counter N/P at after virtual fifo module\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", ""),
               "${pathList} ${readingMode}")
    {
    mAtCliCall();
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : ClishAtModuleRam.c
 *
 * Created Date: Dec 16, 2016
 *
 * Description : CLISH debug CLIs for RAM
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("debug module ram ddr", "Module debug CLIs on DDR")
TOP_SDKCOMMAND("debug module ram ddr error", "Module debug CLIs on DDR error")
TOP_SDKCOMMAND("debug module ram ddr error latching", "Module debug CLIs on DDR error latching")
TOP_SDKCOMMAND("debug module ram qdr", "Module debug CLIs on QDR")
TOP_SDKCOMMAND("debug module ram qdr error", "Module debug CLIs on QDR error")
TOP_SDKCOMMAND("debug module ram qdr error latching", "Module debug CLIs on QDR error latching")
TOP_SDKCOMMAND("debug ddr hw_assist", "Command to debug HW assist tool")
TOP_SDKCOMMAND("debug qdr hw_assist", "Command to debug HW assist tool")

DEF_SDKCOMMAND(cliAtModuleRamDdrErrorLatchingEnable,
               "debug module ram ddr error latching enable",
               "Force enable DDR error latching logic\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModuleRamDdrErrorLatchingDisable,
               "debug module ram ddr error latching disable",
               "Disable DDR error latching logic\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModuleRamQdrErrorLatchingEnable,
               "debug module ram qdr error latching enable",
               "Force enable QDR error latching logic\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModuleRamQdrErrorLatchingDisable,
               "debug module ram qdr error latching disable",
               "Disable QDR error latching logic\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDdrHwAssistAcceptanceTest,
               "debug ddr hw_assist acceptance_test",
               "To unittest the HW assist tool to see if it basically work\n",
               DEF_SDKPARAM("ddrList", "DdrIdList", ""),
               "${ddrList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtQdrHwAssistAcceptanceTest,
               "debug qdr hw_assist acceptance_test",
               "To unittest the HW assist tool to see if it basically work\n",
               DEF_SDKPARAM("qdrList", "QdrIdList", ""),
               "${qdrList}")
    {
    mAtCliCall();
    }

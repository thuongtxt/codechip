/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Debug
 *
 * File        : clishAtDevice.c
 *
 * Created Date: Sep 29, 2016
 *
 * Description : Device debug CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("debug device testbench", "CLIs to work with testbench")
TOP_SDKCOMMAND("debug device ddroffload", "Device ddr offload CLIs")

DEF_SDKCOMMAND(cliAtDevicePlatformSet,
               "debug device platform",
               "Specify the platform by using product code.",
               DEF_SDKPARAM("productCode", "STRING", "Platform type identified by product code"),
               "${productCode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceTestbenchEnable,
               "debug device testbench enable",
               "Enforce internal logic to work with testbench",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceTestbenchDisable,
               "debug device testbench disable",
               "Disable testbench logic",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceDebugTableShow,
               "debug device table",
               "Display debug information in table.",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceDdrOffloadAuditEnable,
               "debug device ddroffload audit enable",
               "Enable the real device to audit ddr offload function",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceDdrOffloadAuditDisable,
               "debug device ddroffload audit disable",
               "Disable the real device to audit ddr offload function",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceDdrOffloadAudit,
               "debug device ddroffload audit",
               "Audit ddr offload configuration for a sub device and output result to file ",
               DEF_SDKPARAM("subdeviceId", "STRING", "subdevice ID")
               DEF_SDKPARAM("outputfilePath", "STRING", "output file path")
               DEF_SDKPARAM_OPTION("auditMode", "STRING", "Auditmode: none or compare"),
               "${subdeviceId} ${outputfilePath} ${auditMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceDdrOffloadAuditInit,
               "debug device ddroffload audit init",
               "Clear all addresses which was audit of a subdevice ",
               DEF_SDKPARAM("subdeviceId", "STRING", "subdevice ID"),
               "${subdeviceId}")
    {
    mAtCliCall();
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Debug
 *
 * File        : ClishThaModulePw.c
 *
 * Created Date: Dec 15, 2016
 *
 * Description : Debug CLI for ThaModulePw
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("debug module pw subport", "CLIs to debug sub port")
TOP_SDKCOMMAND("debug module pw subport vlan", "CLIs to debug sub port vlan")
TOP_SDKCOMMAND("debug module pw subport vlan insertion", "CLIs to handle sub port VLAN insertion")
TOP_SDKCOMMAND("debug module pw subport vlan checking", "CLIs to handle sub port VLAN checking")
TOP_SDKCOMMAND("debug module pw header", "CLIs to debug PW header")
TOP_SDKCOMMAND("debug module pw header hitless", "CLIs to debug PW header in hitless mode")

TOP_SDKCOMMAND("debug module pw header", "CLIs to debug PW header")
TOP_SDKCOMMAND("debug module pw header hitless", "CLIs to debug PW header in hitless mode")

DEF_SDKCOMMAND(cliThaModulePwSubPortVlanInsertionEnable,
               "debug module pw subport vlan insertion enable",
               "Enable sub port VLAN insertion\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliThaModulePwSubPortVlanInsertionDisable,
               "debug module pw subport vlan insertion disable",
               "Disable sub port VLAN insertion\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliThaModulePwSubPortVlanCheckingEnable,
               "debug module pw subport vlan checking enable",
               "Enable sub port VLAN checking\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliThaModulePwSubPortVlanCheckingDisable,
               "debug module pw subport vlan checking disable",
               "Disable sub port VLAN checking\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliThaModulePwHitlessHeaderChangeEnable,
               "debug module pw header hitless enable",
               "Enable PW header changing logic. This feature is normally disabled by default\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliThaModulePwHitlessHeaderChangeDisable,
               "debug module pw header hitless disable",
               "Disable PW header changing logic.\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

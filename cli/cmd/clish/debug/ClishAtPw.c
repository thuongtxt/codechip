/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ClishAtPw.c
 *
 * Created Date: Dec 31, 2016
 *
 * Description : PW CLISH debug CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("debug pw force", "CLIs to debug force PW error")

DEF_SDKCOMMAND(cliAtPwHwHeaderShow,
               "debug show pw header",
               "Show hardware header",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPwHwHeaderSet,
               "debug pw header raw",
               "Set raw PW header",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("headers", "STRING", "List of headers separated by ','"),
               "${pwList} ${headers}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPwBufferOverrunForce,
               "debug pw force overrun",
               "Force/unforce jitter buffer overrun",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("isFrc", "eBool", ""),
               "${pwList} ${isFrc}")
    {
    mAtCliCall();
    }

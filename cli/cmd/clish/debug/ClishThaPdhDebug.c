/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : DEBUG
 *
 * File        : ClishThaPdhDebug.c
 *
 * Created Date: Nov 11, 2013
 *
 * Description : CLI define for PDH debugging
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
/* DS1/E1 list */
#define de1no "([1-9]|3[0-2]|1[0-9]|2[0-9])"
#define de1id de1no"|["de1no"-"de1no"]|"de1no","de1no
#define De1List de1id


/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("show pdh de1", "\nShow configuration of PDH channels\n")

TOP_SDKCOMMAND("show debug", "Show debug information of PW module\n")
TOP_SDKCOMMAND("show debug module", "Show debug information of PW module\n")
TOP_SDKCOMMAND("show debug module pdh", "Show debug information of PW module\n")
TOP_SDKCOMMAND("show debug module pdh liu", "Show debug information of Liu\n")
TOP_SDKCOMMAND("show debug module pdh liu rxclock", "Show debug information of Liu\n")
TOP_SDKCOMMAND("debug module pdh liu", "Debug PDH Liu\n")
TOP_SDKCOMMAND("debug module pdh liu frequency", "Debug PDH Liu\n")

/* Show auto AIS/RAI */
DEF_SDKCOMMAND(cliPdhDe1AutoAisRdiGet,
               "show pdh de1 autoalarm",
               "Show auto AIS/RAI of DS1/E1 channels",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModulePdhLiuClockMonitorDe1Debug,
               "debug module pdh liu frequency de1",
               "Liu clock monitor set",
               DEF_SDKPARAM("de1List", "STRING", "PDH DS1/E1 Id"),
               "${de1List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModulePdhLiuClockMonitorDe3Debug,
               "debug module pdh liu frequency de3",
               "Liu clock monitor set",
               DEF_SDKPARAM("de3List", "STRING", "PDH DS3/E3 Id"),
               "${de3List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModulePdhLiuClockMonitorDebug,
               "show debug module pdh liu frequency",
               "Show liu tx/rx clock monitor",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModulePdhLiuRxLosOfClockDe1Debug,
               "show debug module pdh liu rxclock de1",
               "Show Los Of Clock of rx for de1 channel",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModulePdhLiuRxLosOfClockDe3Debug,
               "show debug module pdh liu rxclock de3",
               "Show Los Of Clock of rx for de3 channel",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Debug
 *
 * File        : ClishAtDebug.c
 *
 * Created Date: Feb 6, 2013
 *
 * Description : CLIs for hardware debugging purpose
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("ddr counters", "CLIs to control DDR counter engine")
TOP_SDKCOMMAND("debug link", "Debug Link")
TOP_SDKCOMMAND("task", "CLI control task")
TOP_SDKCOMMAND("eth port dic", "Debug EthPort")
TOP_SDKCOMMAND("osal", "OSAL CLIs")
TOP_SDKCOMMAND("device diag check", "Diagnostic checking command")
TOP_SDKCOMMAND("debug time", "Debug time CLI.")
TOP_SDKCOMMAND("debug time logging", "Debug logging time CLI.")

DEF_SDKCOMMAND(cliDdrCounterFlush,
               "ddr counters flush",
               "Flush DDR counter engine to bring it back to normal state. This "
               "CLI is for debugging purpose to confirm if counter engine halt\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDebugLinkAttach,
               "debug link attach",
               "Attach link to bundle\n",
               DEF_SDKPARAM("link", "PppId", "")
               DEF_SDKPARAM("bundle", "mpBundleId", ""),
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDebugLinkDetach,
               "debug link detach",
               "Detach link to bundle\n",
               DEF_SDKPARAM("link", "PppId", "")
               DEF_SDKPARAM("bundle", "mpBundleId", ""),
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliTaskSuspend,
               "task suspend",
               "Suspend task\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDebugEthPortDIcEnable,
               "eth port dic enable",
               "Enable DIC mode\n",
               DEF_SDKPARAM("ethport", "PortIdList", ""),
               "${portId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDebugEthPortDicDisable,
               "eth port dic disable",
               "Disable DIC mode\n",
               DEF_SDKPARAM("ethport", "PortIdList", ""),
               "${portId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliOsalSleep,
               "osal sleep",
               "Sleep in seconds ",
               DEF_SDKPARAM("timeToSleep", "UINT", ""),
               "${timeToSleep}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliOsalUSleep,
               "osal usleep",
               "Sleep in micro seconds ",
               DEF_SDKPARAM("timeToSleep", "UINT", ""),
               "${timeToSleep}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDebugTimeLoggingEnable,
               "debug time logging enable",
               "Enable logging time\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDebugTimeLoggingDisable,
               "debug time logging disable",
               "Disable logging time\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

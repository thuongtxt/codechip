/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Debug
 *
 * File        : ClishAtPdhDe1Debug.c
 *
 * Created Date: Sep 21, 2015
 *
 * Description : Debug CLI
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
#define PrmForceModeList  "(8bit|10bit|dis)"
#define PrmForceList  "(G1|G2|G3|G4|G5|G6|SE|FE|LV|SL|LB)"
#define PrmForceMask "((("PrmForceList"[\\|])+"PrmForceList")|"PrmForceList")"

DEF_PTYPE("PrmForceMode", "regexp", PrmForceModeList,
         "\nInput Prm Forced Mode:\n"
         "  + 8bit: Full bit paload, except bit Nm/Ni\n"
         "  + 10bit: Full bit paload\n"
         "  + dis : Disable monitor\n")

 DEF_PTYPE("PrmForce", "regexp", PrmForceMask,
        "\nInput PRM Payload Bit Forced:\n"
        "  + G1: G1 bit\n"
        "  + G2: G2 bit\n"
        "  + G3: G3 bit\n"
        "  + G4: G4 bit\n"
        "  + G5: G5 bit\n"
        "  + G6: G6 bit\n"
        "  + SE: SE bit\n"
        "  + FE: FE bit\n"
        "  + LV: LV bit\n"
        "  + SL: SL bit\n"
        "  + LB: LB bit\n")

TOP_SDKCOMMAND("show pdh de1 debug", "CLIs for PDH DE1 Debug")
TOP_SDKCOMMAND("debug pdh de1 liu", "Debug LIU CLIs")
TOP_SDKCOMMAND("debug pdh de1 fastsearch", "Debug DE1 fastsearch CLIs")
TOP_SDKCOMMAND("debug show pdh", "Debug PDH CLIs")
TOP_SDKCOMMAND("debug show pdh de1", "Debug DE1 CLIs")
TOP_SDKCOMMAND("debug show pdh de1 liu", "Debug LIU CLIs")

DEF_SDKCOMMAND(cliPdhDe1PrmDebugChannelSelect,
               "debug pdh de1 prm",
               "Monitor PRM on channel that selected\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1PrmDebugMonitorEnable,
               "debug pdh de1 prm monitor enable",
               "Enable monitor PRM \n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1PrmDebugMonitorDisable,
               "debug pdh de1 prm monitor disable",
               "Disable monitor PRM \n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1PrmDebugForceMode,
               "debug pdh de1 prm force mode",
               "Enable/Disable force full or part of PRM payload\n",
               DEF_SDKPARAM("mode", "PrmForceMode", ""),
               "${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1PrmDebugForce,
               "debug pdh de1 prm force",
               "Forces value for PRM payload\n",
               DEF_SDKPARAM("prmPayloadBit", "PrmForce", ""),
               "${prmPayloadBit}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1PrmDebugUnForce,
               "debug pdh de1 prm unforce",
               "Un-Forces value for PRM payload\n",
               DEF_SDKPARAM("prmPayloadBit", "PrmForce", ""),
               "${prmPayloadBit}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1PrmDebugForceNmNiBit,
               "debug pdh de1 prm force nmni",
               "Forced value for PRM NMNI payload bit\n",
               DEF_SDKPARAM("values", "UINT", ""),
               "${values}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1PrmDebugGet,
               "show pdh de1 debug prm",
               "Get prm de1 debug",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPdhDe1PrmCounterGet,
               "show pdh de1 debug prm counters",
               "Get counters PRM on E1 Channels\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${de1s} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPdhDe1PrmInterruptGet,
               "show pdh de1 debug prm interrupt",
               "Get history alarm PRM on E1 Channels\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${de1s} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1LiuLoopbackSet,
               "debug pdh de1 liu loopback",
               "Loopback DE1 at LIU interface\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("loopbackMode", "eAtLoopbackMode", ""),
               "${de1s} ${eAtLoopbackMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1LiuLoopbackGet,
               "debug show pdh de1 liu loopback",
               "Show loopback configuration of DE1 at LIU interface\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPdhDe1PrmControllerCaptureRxMessages,
               "pdh de1 prm capture",
               "Capture PRM messages\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPdhDe1PrmControllerCaptureStart,
               "pdh de1 prm capture start",
               "Start capturing PRM messages\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPdhDe1PrmControllerCaptureStop,
               "pdh de1 prm capture stop",
               "Stop capturing PRM messages\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtPdhDe1FastSearchEnable,
               "debug pdh de1 fastsearch enable",
               "Enable fast search engine\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtPdhDe1FastSearchDisable,
               "debug pdh de1 fastsearch disable",
               "Disable fast search engine\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", ""),
               "${de1s}")
    {
    mAtCliCall();
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : ClishAtIpCore.c
 *
 * Created Date: Jul 29, 2016
 *
 * Description : IP core debug CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("show device ipcore", "Show device IP core information")

DEF_SDKCOMMAND(CliAtIpCoreInterruptShow,
               "show device ipcore interrupt",
               "Show IP core interrupt configuration and status",
               DEF_SDKPARAM("coreIds", "STRING", "List of IP Core IDs to display debug information")
               DEF_SDKPARAM_OPTION("readingMode", "HistoryReadingMode", ""),
               "${readingMode}")
    {
    mAtCliCall();
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Debug
 *
 * File        : ClishAtModule.c
 *
 * Created Date: Jul 20, 2018
 *
 * Description : Module debug CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

DEF_SDKCOMMAND(cliAtModuleActivate,
               "debug module activate",
               "Activate a module",
               DEF_SDKPARAM("module", "STRING", ""),
               "${module}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModuleDeactivate,
               "debug module deactivate",
               "Deactivate a module",
               DEF_SDKPARAM("module", "STRING", ""),
               "${module}")
    {
    mAtCliCall();
    }

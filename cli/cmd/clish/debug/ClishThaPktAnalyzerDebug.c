/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : ClishTha60210012PktAnalyzer.c
 *
 * Created Date: May 17, 2016
 *
 * Description : Packet analyzer CLIs for debug
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/
#define regAddressOffset "0..2047"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
DEF_PTYPE("AddressOffset", "regexp", ".*", "Memory address offset (0..2047)")

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
DEF_SDKCOMMAND(CliPktAnalyzerPacketCla2Pda,
               "pktanalyzer analyze rx cla2pda",
               "Analyze packets on direction from CLA to PDA",
               DEF_SDKPARAM("serviceId", "UINT", "Service ID"),
               "${serviceId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPktAnalyzerPacketPda2Map,
               "pktanalyzer analyze rx pda2map",
               "Analyze packets on direction from PDA to MAP",
               DEF_SDKPARAM("serviceId", "UINT", "Service ID"),
               "${serviceId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPktAnalyzerPacketTxFromPla,
               "pktanalyzer analyze tx txfrompla",
               "Analyze packets sending out from PLA",
               DEF_SDKPARAM_OPTION("addressOffset", "AddressOffset", "Memory address offset"),
               "${addressOffset}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPktAnalyzerPacketRxAtPwe,
               "pktanalyzer analyze tx rxatpwe",
               "Analyze packets received at PWE",
               DEF_SDKPARAM_OPTION("addressOffset", "AddressOffset", "Memory address offset"),
               "${addressOffset}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPktAnalyzerPacketTxPlaHeader,
               "pktanalyzer analyze tx plaheader",
               "Analyze packets header at PLA",
               DEF_SDKPARAM_OPTION("addressOffset", "AddressOffset", "Memory address offset"),
               "${addressOffset}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPktAnalyzerPacketTxPweHeader,
               "pktanalyzer analyze tx pweheader",
               "Analyze packets header at PWE",
               DEF_SDKPARAM_OPTION("addressOffset", "AddressOffset", "Memory address offset"),
               "${addressOffset}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPktAnalyzerPacketCla2Cdr,
               "pktanalyzer analyze rx cla2cdr",
               "Analyze packets on direction from CLA to CDR",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM_OPTION("numPkt", "UINT", ""),
               "${serviceId} ${numPkt}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPktAnalyzerPacketRxCls,
               "pktanalyzer analyze rx cls",
               "Analyze packets at rx input 2.5G port cls",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPktAnalyzerPacketTxClsDs1,
               "pktanalyzer analyze rx cls de1",
               "Analyze DS1/E1 packets at output 2.5G cls to the PDH MUX",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPktAnalyzerPacketTxClsDs3,
               "pktanalyzer analyze rx cls de3",
               "Analyze DS3/E3/EC1 packets at output 2.5G cls to the PDH MUX",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPktAnalyzerPacketTxClsControl,
               "pktanalyzer analyze rx cls control",
               "Analyze control packets at the 2.5G cls to the DIM packet control port",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : ClishAtPmSesThresholdProfile.c
 *
 * Created Date: Sep 5, 2017
 *
 * Description : PM SES Threshold Debug CLISHs.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

TOP_SDKCOMMAND("pdh de3 pm ses", "CLIs for PDH DS3/E3 PM SES")
TOP_SDKCOMMAND("pdh de3 pm ses threshold", "CLIs for PDH DS3/E3 PM SES Threshold")
TOP_SDKCOMMAND("pdh de1 pm ses", "CLIs for PDH DS1/E1 PM SES")
TOP_SDKCOMMAND("pdh de1 pm ses threshold", "CLIs for PDH DS1/E1 PM SES Threshold")
TOP_SDKCOMMAND("sdh line pm ses", "CLIs for SDH Line PM SES")
TOP_SDKCOMMAND("sdh line pm ses threshold", "CLIs for SDH Line PM SES Threshold")
TOP_SDKCOMMAND("sdh path pm ses", "CLIs for SDH Path PM SES")
TOP_SDKCOMMAND("sdh path pm ses threshold", "CLIs for SDH Path PM SES Threshold")
TOP_SDKCOMMAND("pw pm ses", "CLIs for PW PM SES")
TOP_SDKCOMMAND("pw pm ses threshold", "CLIs for PW PM SES Threshold")

TOP_SDKCOMMAND("show pdh de3 pm ses", "CLIs to show PDH DS3/E3 PM SES")
TOP_SDKCOMMAND("show pdh de3 pm ses threshold", "CLIs to show PDH DS3/E3 PM SES Threshold")
TOP_SDKCOMMAND("show pdh de1 pm ses", "CLIs to show PDH DS1/E1 PM SES")
TOP_SDKCOMMAND("show pdh de1 pm ses threshold", "CLIs to show PDH DS1/E1 PM SES Threshold")
TOP_SDKCOMMAND("show sdh line pm ses", "CLIs to show SDH Line PM SES")
TOP_SDKCOMMAND("show sdh line pm ses threshold", "CLIs to show SDH Line PM SES Threshold")
TOP_SDKCOMMAND("show sdh path pm ses", "CLIs to show SDH Path PM SES")
TOP_SDKCOMMAND("show sdh path pm ses threshold", "CLIs to show SDH Path PM SES Threshold")
TOP_SDKCOMMAND("show pw pm ses", "CLIs to show PW PM SES")
TOP_SDKCOMMAND("show pw pm ses threshold", "CLIs to show PW PM SES Threshold")

DEF_SDKCOMMAND(clishPdhDe3PmSesThresholdProfile,
             "pdh de3 pm ses threshold profile",
             "\nSet SES threshold profile for PDH DS3/E3 channel\n",
             DEF_SDKPARAM("de3List", "PdhDe3List", "")
             DEF_SDKPARAM("profileId", "STRING", ""),
             "${de3List} ${profileId}")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(clishPdhDe1PmSesThresholdProfile,
             "pdh de1 pm ses threshold profile",
             "\nSet SES threshold profile for PDH DS1/E1 channel\n",
             DEF_SDKPARAM("de1List", "PdhDe1List", "")
             DEF_SDKPARAM("profileId", "STRING", ""),
             "${de1List} ${profileId}")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(clishSdhLinePmSesThresholdProfile,
             "sdh line pm ses threshold profile",
             "\nSet SES threshold profile for SDH Lines\n",
             DEF_SDKPARAM("lineList", "SdhLineList", "")
             DEF_SDKPARAM("profileId", "STRING", ""),
             "${lineList} ${profileId}")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(clishSdhPathPmSesThresholdProfile,
             "sdh path pm ses threshold profile",
             "\nSet SES threshold profile for SDH Paths\n",
             DEF_SDKPARAM("pathList", "SdhPathList", "")
             DEF_SDKPARAM("profileId", "STRING", ""),
             "${pathList} ${profileId}")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(clishPwPmSesThresholdProfile,
             "pw pm ses threshold profile",
             "\nSet SES threshold profile for PW\n",
             DEF_SDKPARAM("pwList", "PwIdList", "")
             DEF_SDKPARAM("profileId", "STRING", ""),
             "${pwList} ${profileId}")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(clishPdhDe3PmSesThresholdProfileShow,
             "show pdh de3 pm ses threshold profile",
             "\nShow SES threshold profile for PDH DS3/E3 channel\n",
             DEF_SDKPARAM("de3List", "PdhDe3List", ""),
             "${de3List}")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(clishPdhDe1PmSesThresholdProfileShow,
             "show pdh de1 pm ses threshold profile",
             "\nShow SES threshold profile for PDH DS1/E1 channel\n",
             DEF_SDKPARAM("de1List", "PdhDe1List", ""),
             "${de1List}")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(clishSdhLinePmSesThresholdProfileShow,
             "show sdh line pm ses threshold profile",
             "\nShow SES threshold profile for SDH Lines\n",
             DEF_SDKPARAM("lineList", "SdhLineList", ""),
             "${lineList}")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(clishSdhPathPmSesThresholdProfileShow,
             "show sdh path pm ses threshold profile",
             "\nShow SES threshold profile for SDH Paths\n",
             DEF_SDKPARAM("pathList", "SdhPathList", ""),
             "${pathList}")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(clishPwPmSesThresholdProfileShow,
             "show pw pm ses threshold profile",
             "\nShow SES threshold profile for PW\n",
             DEF_SDKPARAM("pwList", "PwIdList", ""),
             "${pwList}")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(clishSurThresholdProfileDe3SesLSet,
             "sur threshold profile de3 sesl",
             "\nSet DS3/E3 SES-L threshold value\n",
             DEF_SDKPARAM("profileId", "STRING", "")
             DEF_SDKPARAM("threshold", "STRING", ""),
             "${profileId} ${threshold}")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(clishSurThresholdProfileDe3SesPSet,
             "sur threshold profile de3 sesp",
             "\nSet DS3/E3 SES-P threshold value\n",
             DEF_SDKPARAM("profileId", "STRING", "")
             DEF_SDKPARAM("threshold", "STRING", ""),
             "${profileId} ${threshold}")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(clishSurThresholdProfileDe1SesLSet,
             "sur threshold profile de1 sesl",
             "\nSet DS1/E1 SES-L threshold value\n",
             DEF_SDKPARAM("profileId", "STRING", "")
             DEF_SDKPARAM("threshold", "STRING", ""),
             "${profileId} ${threshold}")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(clishSurThresholdProfileDe1SesPSet,
             "sur threshold profile de1 sesp",
             "\nSet DS1/E1 SES-P threshold value\n",
             DEF_SDKPARAM("profileId", "STRING", "")
             DEF_SDKPARAM("threshold", "STRING", ""),
             "${profileId} ${threshold}")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(clishSurThresholdProfileLineSesSSet,
             "sur threshold profile line sess",
             "\nSet SONET/SDH Line SES-S threshold value\n",
             DEF_SDKPARAM("profileId", "STRING", "")
             DEF_SDKPARAM("threshold", "STRING", ""),
             "${profileId} ${threshold}")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(clishSurThresholdProfileLineSesLSet,
             "sur threshold profile line sesl",
             "\nSet SONET/SDH Line SES-L threshold value\n",
             DEF_SDKPARAM("profileId", "STRING", "")
             DEF_SDKPARAM("threshold", "STRING", ""),
             "${profileId} ${threshold}")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(clishSurThresholdProfilePathSesPSet,
             "sur threshold profile hopath ses",
             "\nSet SONET/SDH Ho-path SES-P threshold value\n",
             DEF_SDKPARAM("profileId", "STRING", "")
             DEF_SDKPARAM("threshold", "STRING", ""),
             "${profileId} ${threshold}")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(clishSurThresholdProfilePathSesVSet,
             "sur threshold profile lopath ses",
             "\nSet SONET/SDH Lo-path SES-V threshold value\n",
             DEF_SDKPARAM("profileId", "STRING", "")
             DEF_SDKPARAM("threshold", "STRING", ""),
             "${profileId} ${threshold}")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(clishSurThresholdProfilePwSesSet,
             "sur threshold profile pw ses",
             "\nSet PW SES threshold value\n",
             DEF_SDKPARAM("profileId", "STRING", "")
             DEF_SDKPARAM("threshold", "STRING", ""),
             "${profileId} ${threshold}")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(clishSurThresholdProfileSesShow,
             "show sur threshold profile ses",
             "\nShow Surveillance SES threshold profile\n",
             DEF_SDKPARAM("profileId", "STRING", ""),
             "${profileId}")
  {
  mAtCliCall();
  }


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : ClishAtEyeScan.c
 *
 * Created Date: Jul 29, 2017
 *
 * Description : Eye-scan debug CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("debug serdes eyescan default_setting", "CLIs to control default settings")
TOP_SDKCOMMAND("debug serdes eyescan lane", "CLIs to debug eyescan lanes")
TOP_SDKCOMMAND("debug serdes eyescan lane pixel", "CLIs to debug eyescan lane pixels")

DEF_SDKCOMMAND(cliAtEyeScanControllerDefaultSettingEnable,
               "debug serdes eyescan default_setting enable",
               "Apply default setting before scaning",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEyeScanControllerDefaultSettingDisable,
               "debug serdes eyescan default_setting disable",
               "Donot apply default setting before scaning",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEyeScanLaneEyescanEnable,
               "debug serdes eyescan lane enable",
               "Enable eyescan in hardware",
               DEF_SDKPARAM("lanes", "SerdesEyeScanLanes", ""),
               "${lanes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEyeScanLaneEyescanDisable,
               "debug serdes eyescan lane disable",
               "Disable eyescan in hardware",
               DEF_SDKPARAM("lanes", "SerdesEyeScanLanes", ""),
               "${lanes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEyeScanLaneInit,
               "debug serdes eyescan lane init",
               "Initialize eyescan lanes",
               DEF_SDKPARAM("lanes", "SerdesEyeScanLanes", ""),
               "${lanes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEyeScanLaneSetup,
               "debug serdes eyescan lane setup",
               "Setup eyescan lanes",
               DEF_SDKPARAM("lanes", "SerdesEyeScanLanes", ""),
               "${lanes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtEyeScanLanePixelScan,
               "debug serdes eyescan lane pixel scan",
               "Scan specified pixels",
               DEF_SDKPARAM("lanes", "SerdesEyeScanLanes", "")
               DEF_SDKPARAM("pixels", "STRING", "Pixels to scan. Example: 1.1,-2.-3"),
               "${lanes} ${pixels}")
    {
    mAtCliCall();
    }

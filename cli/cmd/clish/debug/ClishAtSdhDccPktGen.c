/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : ClishAtSdhDccPktGen.c
 *
 * Created Date: Apr 6, 2017
 *
 * Description : CLISH debug CLI for DCC
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/
#define PktErrorType "(length|fcs|payload)"
#define PktErrorMask "((("PktErrorType"[\\|])+"PktErrorType")|"PktErrorType")"

#define PktLoopbackType "(none|dcc-sgmii|dcc-hdlc|kbyte-sgmii|kbyte-sdh|dcc-buffer)"
#define PktLoopbackMask "((("PktLoopbackType"[\\|])+"PktLoopbackType")|"PktLoopbackType")"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
DEF_PTYPE("EncapPktErrorMask", "regexp", PktErrorMask,
          "\n Encap Packet Error Mask:\n"
          "- fcs          : PKT errored at FCS\n"
          "- length       : PKT errored at length field\n"
          "- payload      : PKT errored at payload field\n")

  DEF_PTYPE("PktDebugLoopbackMask", "regexp", PktLoopbackMask,
           "\n Loopback Mask:\n"
           "- none            : Release all loopback\n"
           "- dcc-sgmii       : DCC-SGMII loopback\n"
           "- dcc-hdlc        : DCC-Loopback at encap to decap\n"
           "- kbyte-sgmii     : KByte SGMII loopback\n"
           "- kbyte-sdh       : KByte SDH loopback\n"
           "- dcc-buffer      : DCC Buffer loopback\n")

DEF_PTYPE("eAtPktGenMode", "select", "none(0), oneshot(1), continuous(2)", "Packet Generator mode")
DEF_PTYPE("eAtPktErrorType", "select", "none(0), length(1), fcs(2), payload(3)", "Packet Generator Error Type")
DEF_PTYPE("eAtPktLengthMode", "select", "fix(1), increase(2), random(3)", "Packet Length mode")
DEF_PTYPE("DWORD_HEX", "regexp", "(0[xX]([0-9a-fA-F]{1,8}))", "A Dword in Hex format such as 0xCAFECAFE")

TOP_SDKCOMMAND("debug pkt", "CLIs to debug Packet\n")
TOP_SDKCOMMAND("debug dcc", "CLIs to debug DCC\n")
TOP_SDKCOMMAND("debug dcc monitor", "CLIs to debug monitor DCC\n")
TOP_SDKCOMMAND("debug dcc generator", "CLIs to debug generator DCC\n")
TOP_SDKCOMMAND("debug dcc generator error", "CLIs to debug generator error DCC\n")
TOP_SDKCOMMAND("debug dcc generator channel", "CLIs to debug generator channel DCC\n")
TOP_SDKCOMMAND("debug dcc generator channel header", "CLIs to debug generator channel header DCC\n")
TOP_SDKCOMMAND("debug dcc generator channel length", "CLIs to debug generator channel length DCC\n")
TOP_SDKCOMMAND("debug dcc generator channel payload", "CLIs to debug generator channel payload DCC\n")
TOP_SDKCOMMAND("show debug dcc generator", "CLIs to show debug DCC\n")
TOP_SDKCOMMAND("debug dcc generator length", "CLIs to control generator length\n")

DEF_SDKCOMMAND(cliAtDebugDccInit,
               "debug dcc init",
               "Init DCC",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDebugDccMonEnable,
               "debug dcc monitor enable",
               "Enable monitoring",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDebugDccMonDisable,
               "debug dcc monitor disable",
               "Disable monitoring",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDebugDccGenEnable,
               "debug dcc generator enable",
               "Enable generator",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDebugDccGenDisable,
               "debug dcc generator disable",
               "Disable generator",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDebugDccMonIntervalSet,
               "debug dcc monitor interval",
               "Set interval for DCC monitor\n",
               DEF_SDKPARAM("interval", "DWORD_HEX", ""),
               "${interval}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDebugDccGenIntervalSet,
               "debug dcc generator interval",
               "Set interval for DCC generator\n",
               DEF_SDKPARAM("interval", "DWORD_HEX", ""),
               "${interval}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDebugDccGenMinLengthSet,
               "debug dcc generator length min",
               "Set min length for DCC generator\n",
               DEF_SDKPARAM("length", "UINT", ""),
               "${length}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDebugDccGenMaxLengthSet,
               "debug dcc generator length max",
               "Set max length for DCC generator\n",
               DEF_SDKPARAM("length", "UINT", ""),
               "${length}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDebugDccGenModeSet,
               "debug dcc generator mode",
               "Set mode for DCC generator\n",
               DEF_SDKPARAM("mode", "eAtPktGenMode", "Packet Generator mode"),
               "${mode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDebugDccGenErrorForce,
               "debug dcc generator error force",
               "Force Error on DCC generator\n",
               DEF_SDKPARAM("errorType", "eAtPktErrorType", "Encap Packet Error Type"),
               "${errorType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDebugDccGenErrorUnForce,
               "debug dcc generator error unforce",
               "unforce Error on DCC generator\n",
               DEF_SDKPARAM("errorType", "eAtPktErrorType", "Encap Packet Error Type"),
               "${errorType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDebugDccShow,
               "show debug dcc",
               "Disable generator",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDebugDccGenChannelEnable,
                "debug dcc generator channel enable",
                "enable channel on DCC generator\n",
                DEF_SDKPARAM("dcc", "DccId", ""),
                "${dcc}")

    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDebugDccGenChannelDisable,
           "debug dcc generator channel disable",
           "disable channel on DCC generator\n",
           DEF_SDKPARAM("dcc", "DccId", ""),
           "${dcc}")

    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDebugDccGenChannelHeaderVlanSet,
           "debug dcc generator channel header vlanid",
           "disable channel on DCC generator\n",
           DEF_SDKPARAM("dcc", "DccId", "")
           DEF_SDKPARAM("vlanId", "DWORD_HEX", " Vlan in format HEXA exampe 0x1AF"),
           "${dcc} ${vlanId}")

    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDebugDccGenChannelHeaderDestMacLsbSet,
           "debug dcc generator channel header maclsb",
           "Set MAC LSB for channel on DCC generator\n",
           DEF_SDKPARAM("dcc", "DccId", "")
           DEF_SDKPARAM("maclsb", "BYTE_HEX", ""),
           "${dcc} ${maclsb}")

    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDebugDccGenChannelLengthModeSet,
           "debug dcc generator channel length mode",
           "Set Length mode for channel on DCC generator\n",
           DEF_SDKPARAM("dcc", "DccId", "")
           DEF_SDKPARAM("mode", "eAtPktLengthMode", "Packet Length mode"),
           "${dcc} ${mode}")

    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDebugDccGenChannelPayloadModeSet,
           "debug dcc generator channel payload mode",
           "Set payload mode for channel on DCC generator\n",
           DEF_SDKPARAM("dcc", "DccId", "")
           DEF_SDKPARAM("mode", "eAtPrbsMode", "Packet Payload mode"),
           "${dcc} ${mode}")

    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDebugDccGenChannelPayloadPatternSet,
           "debug dcc generator channel payload pattern",
           "Set payload mode for channel on DCC generator\n",
           DEF_SDKPARAM("dcc", "DccId", "")
           DEF_SDKPARAM("pattern", "DWORD_HEX", " Fixed pattern with hexa format"),
           "${dcc} ${pattern}")

    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDebugDccGenChannelNumPktsSet,
           "debug dcc generator channel numpkts",
           "Set number of packet for OneShort\n",
           DEF_SDKPARAM("dcc", "DccId", "")
           DEF_SDKPARAM("numpkts", "UINT", "number of packet from 1"),
           "${dcc} ${numpkts}")

    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDebugDccGenChannelShow,
           "show debug dcc generator channel",
           "Set payload mode for channel on DCC generator\n",
           DEF_SDKPARAM("dcc", "DccId", ""),
           "${dcc}")

    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDebugDccGenChannelCountersShow,
           "show debug dcc generator channel counters",
           "Set payload mode for channel on DCC generator\n",
           DEF_SDKPARAM("dcc", "DccId", "")
           DEF_SDKPARAM("counterReadMode", "HistoryReadingMode", "read only/read to clear mode")
           DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
           "${dcc} ${counterReadMode} ${silent}")

    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDebugPktLoopbackSet,
               "debug pkt loopback",
               "Debug Packet loopback\n",
               DEF_SDKPARAM("mask", "PktDebugLoopbackMask", " Packet Loopback Mode")
               DEF_SDKPARAM("en", "eBool", ""),
               " ${mask} ${en}")
    {
    mAtCliCall();
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Debug
 *
 * File        : ClishAtModulePw.c
 *
 * Created Date: Oct 23, 2015
 *
 * Description : PW debug CLI
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/
#define regMemoryPoolId "0..31"
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
DEF_PTYPE("MemoryPoolId", "regexp", ".*", "Memory address offset (0..31)")
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("debug pw timeout", "CLIs to control timeout")
TOP_SDKCOMMAND("debug show pw", "CLIs to getting internal PW counters")
TOP_SDKCOMMAND("debug pw internalcounter", "CLIs to setting internal PW counters memory pool")

DEF_SDKCOMMAND(cliThaModulePwJitterEmptyTimeoutSet,
               "debug pw timeout jitter_buffer_empty",
               "Set timeout to wait for jitter buffer be empty. Note, set to 0 to use default timeout.\n",
               DEF_SDKPARAM("timeoutMs", "STRING", "Timeout to wait for jitter buffer be empty"),
               "${timeoutMs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliThaDebugPwInternalCounterGet,
               "debug show pw internalcounters",
               "Get Pseudowire internal counters",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", ""),
               "${pwList} ${readingMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDebugPwInternalMemoryPoolGet,
               "debug show pw counterpools",
               "Show memory pool for PW internal counters",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDebugPwInternalCounterPoolSet,
               "debug pw counterpool set",
               "Set memory pool for PW internal counters",
               DEF_SDKPARAM("pwList", "PwIdList", "PW ID list")
               DEF_SDKPARAM_OPTION("poolList", "MemoryPoolId", "Memory pool ID list"),
               "${pwList} ${poolList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDebugPwInternalCounterPoolClear,
               "debug pw counterpool clear",
               "Clear memory pool of PW internal counters",
               DEF_SDKPARAM("pwList", "PwIdList", "PW ID list"),
               "${pwList}")
    {
    mAtCliCall();
    }


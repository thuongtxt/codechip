/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : SUR
 *
 * File        : ClishThaSurDebug.c
 *
 * Created Date: Sep 16, 2015
 *
 * Description : SUR Debug CLISHs.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/
#define PageNo "([0-1])"
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/
DEF_PTYPE("PageNumber", "regexp", PageNo, "Page number 0 or 1.\n")
DEF_PTYPE("SurDdrCounterType", "select", "sts(0), vt(1), de3(2), de1(3), pw(4), pw-counters(5)", "Surveillance counter type.\n")

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/

TOP_SDKCOMMAND("debug sur", "CLIs for Surveillance Debug")
TOP_SDKCOMMAND("debug sur ddr", "CLIs for Surveillance DDR Debug")
TOP_SDKCOMMAND("debug sur page", "CLIs for Surveillance page Debug")

DEF_SDKCOMMAND(clishSurDdrRead,
             "debug sur ddr read",
             "\nRead Surveillance DDR\n",
             DEF_SDKPARAM("page", "PageNumber", "")
             DEF_SDKPARAM("counterType", "SurDdrCounterType", "")
             DEF_SDKPARAM("address", "RegisterAddress", "")
             DEF_SDKPARAM("historyMode", "HistoryReadingMode", ""),
             "${page} ${counterType} ${address} ${historyMode}")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(clishSurActivate,
             "debug sur activate",
             "\nActivate Surveillance module\n",
             DEF_NULLPARAM,
             "")
  {
  mAtCliCall();
  }

DEF_SDKCOMMAND(clishSurDeActivate,
             "debug sur deactivate",
             "\nDeactivate Surveillance module\n",
             DEF_NULLPARAM,
             "")
  {
  mAtCliCall();
  }

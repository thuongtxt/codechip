/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OCN
 *
 * File        : ClishThaModuleOcn.c
 *
 * Created Date: Oct 20, 2016
 *
 * Description : OCN module debug CLISH CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
DEF_SDKCOMMAND(cliThaModuleOcnDebug,
               "debug module ocn",
               "Show debug information of OCN module\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

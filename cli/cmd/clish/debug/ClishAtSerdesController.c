/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Serdes
 *
 * File        : ClishAtSerdesController.c
 *
 * Created Date: Aug 30, 2016
 *
 * Description : Debug serdes
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
DEF_PTYPE("eAtSerdesControllerEyescanLogic", "select", "auto_select(0), v2(0), stm(0), stm_v2(0), xaui(0), gty(0)", "Logic for eyescan.")

TOP_SDKCOMMAND("debug serdes eyescan", "CLIs to handle eyescan debug")

DEF_SDKCOMMAND(cliAtSerdesControllerLoopbackDebug,
               "debug serdes loopback",
               "Debug Loopback SERDES",
               DEF_SDKPARAM("serdesList", "SerdesIdList", "")
               DEF_SDKPARAM("loopbackMode", "eAtLoopbackMode", "")
               DEF_SDKPARAM_OPTION("layer", "eAtSerdesPhysicalLayer", ""),
               "${serdesList} ${loopbackMode} ${layer}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerDebug,
               "debug serdes",
               "Show SERDES debug information",
               DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
               "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerPrbsEngineDebug,
             "debug serdes prbs",
             "Show all debug information of Serdes Prbs engine",
             DEF_SDKPARAM("serdesList", "SerdesIdList", ""),
             "${serdesList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerForcedAlarmShow,
               "show serdes forcedalarm",
               "Show SERDES being-forced alarms",
               DEF_SDKPARAM("serdesList", "SerdesIdList", "")
               DEF_SDKPARAM("direction", "eAtDirection", ""),
               "${serdesList} ${direction}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerForcableAlarmShow,
               "show serdes forcablealarm",
               "Show SERDES force-able alarms",
               DEF_SDKPARAM("serdesList", "SerdesIdList", "")
               DEF_SDKPARAM("direction", "eAtDirection", ""),
               "${serdesList} ${direction}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSerdesControllerEyescanLogicSet,
               "debug serdes eyescan logic",
               "Show SERDES force-able alarms",
               DEF_SDKPARAM("serdesList", "SerdesIdList", "")
               DEF_SDKPARAM("logic", "eAtSerdesControllerEyescanLogic", ""),
               "${serdesList} ${logic}")
    {
    mAtCliCall();
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Debug
 *
 * File        : ClishAtHalDebug.c
 *
 * Created Date: Sep 18, 2015
 *
 * Description : HAL debug CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("hal readwrite", "CLIs to control read/write access")
TOP_SDKCOMMAND("hal log", "CLIs to control logging")
TOP_SDKCOMMAND("hal caching", "CLIs to control caching")

DEF_SDKCOMMAND(cliAtHalReadWriteAccessEnable,
               "hal readwrite enable",
               "Enable read/write access",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHalReadWriteAccessDisable,
               "hal readwrite disable",
               "Disable read/write access",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHalLogFileSet,
               "hal log file",
               "Set HAL log file for read/write operations",
               DEF_SDKPARAM("logFile", "STRING", "path to log file to read/write log messages"),
               "${logFile}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHalLogEnable,
               "hal log enable",
               "Enable logging all of read/write operations to file set by `hal log file <logFile>`",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHalLogDisable,
               "hal log disable",
               "Disable logging all of read/write operations",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHalCachingEnable,
               "hal caching enable",
               "Enable caching to speedup read/write",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtHalCachingDisable,
               "hal caching disable",
               "Disable caching to always read/write from hardware.",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : ClishAtSdhChannel.c
 *
 * Created Date: May 1, 2017
 *
 * Description : SDH channel CLISH debug CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("debug show sdh", "CLIs to display SDH debug information")
TOP_SDKCOMMAND("debug show sdh channel", "CLIs to display SDH Channel debug information")
TOP_SDKCOMMAND("debug show sdh channel hardware", "CLIs to display SDH Channel hardware debug information")
TOP_SDKCOMMAND("debug show sdh channel software", "CLIs to display SDH Channel software debug information")

TOP_SDKCOMMAND("debug show pdh", "CLIs to display PDH debug information")
TOP_SDKCOMMAND("debug show pdh channel", "CLIs to display PDH Channel debug information")
TOP_SDKCOMMAND("debug show pdh channel hardware", "CLIs to display PDH Channel hardware debug information")

DEF_SDKCOMMAND(cliAtSdhChannelHwIdShow,
               "debug show sdh channel hardware id",
               "Show hardware ID\n",
               DEF_SDKPARAM("channelList", "MappableChannelList", "")
               DEF_SDKPARAM_OPTION("moduleNames", "STRING", "Module names separated by ,"),
               "${channelList} ${moduleNames}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhChannelSwIdLookup,
               "debug show sdh channel software id",
               "Show software ID from HW ID\n",
               DEF_SDKPARAM("hwIdList", "STRING", "List of hardware ID with format: <au|tu>.<slice>.<sts>.<vtg>.<vt>")
               DEF_SDKPARAM_OPTION("moduleName", "STRING", "Module name to lookup"),
               "${hwIdList} ${moduleName}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPdhChannelHwIdShow,
               "debug show pdh channel hardware id",
               "Show hardware ID\n",
               DEF_SDKPARAM("channelList", "PdhChannelList", "")
               DEF_SDKPARAM_OPTION("moduleNames", "STRING", "Module names separated by ,"),
               "${channelList} ${moduleNames}")
    {
    mAtCliCall();
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Debug
 *
 * File        : ClishThaEncapProfile.c
 *
 * Created Date: Jun 28, 2016
 *
 * Description : Encap profiles debug
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("debug show", "Show debug information")
TOP_SDKCOMMAND("debug show profile", "Show debug profile information")
TOP_SDKCOMMAND("debug profile", "Debug Profile")
TOP_SDKCOMMAND("debug profile oam", "Debug OAM Profile")
TOP_SDKCOMMAND("debug profile network", "Debug Network Profile")
TOP_SDKCOMMAND("debug profile error", "Debug Error Profile")
TOP_SDKCOMMAND("debug profile bcp", "Debug Bcp Profile")
TOP_SDKCOMMAND("debug profile fcn", "Debug Fcn Profile")

DEF_PTYPE("ProfileList", "regexp", ".*", "List of Profile IDs. For example: 1-3,6,8 gives list of 1,2,3,6,8")
DEF_PTYPE("NetworkProfileTypes", "select", "ipv4(1), ipv6(2), uni_mpls(3), mul_mpls(4), osi_network(5), appletalk(6), novel_ipx(7), xerox_nsidp(8),"
          "cisco_system(9), cisco_discovery_protocol(10), net_bios_framing(11), ipv6_hdr_compress(12), 802.1D_packet(13), ibm_bdpu(14),"
          "dec_lan_brigde(15), bcp(16), all(17)",
          "\r\nList of network profile types:\n"
          "- ipv4                     : IPv4\n"
          "- ipv6                     : IPv6\n"
          "- uni_mpls                 : Unicast MPLS\n"
          "- mul_mpls                 : Multicast MPLS\n"
          "- osi_network              : OSI Network Layer Control\n"
          "- appletalk                : Appletalk Control\n"
          "- novel_ipx                : Novell IPX Control\n"
          "- xerox_nsidp              : Xerox NS IDP Control\n"
          "- cisco_system             : Cisco Systems Control \n"
          "- cisco_discovery_protocol : Cisco Discovery Control\n"
          "- net_bios_framing         : NETBIOS Framing\n"
          "- ipv6_hdr_compress        : Ipv6 Header compressing\n"
          "- 802.1D_packet            : 802.1D Packet\n"
          "- ibm_bdpu                 : IBM Source Routing BPDU \n"
          "- dec_lan_brigde           : DEC LANBridge100 Spanning Tree\n"
          "- bcp                      : BCP\n"
          "- all                      : All types\n")

DEF_PTYPE("OamProfileTypes", "select", "lcp(1), ncp(2), q933(3), cisco_lmi(4), clnp(5), esis(6), ISIS(7), xerox_nsidp(8), lipcm(9), lipcm(10), all(11)",
          "\r\nList of control profile types:\n"
          "- lcp         : Link-layer Control Protocols\n"
          "- ncp         : Network Control Protocols\n"
          "- q933        : Q933\n"
          "- cisco_lmi   : Cisco LMI\n"
          "- clnp        : CLNP\n"
          "- esis        : ESIS\n"
          "- ISIS        : ISIS\n"
          "- xerox_nsidp : Xerox NS IDP Control\n"
          "- lipcm       : LIPCM \n"
          "- all         : All types\n")

DEF_PTYPE("ErrorProfileTypes", "select", "fcs_err(1), decap_abort(2), address_err(3), control_err(4), protocol_err(5), mac_err(6), lookup_err(7), all(8)",
          "\r\nList of Error profile types:\n"
          "- fcs_err       : Decap FCS Error\n"
          "- decap_abort   : Decap abort\n"
          "- address_err   : Address Error\n"
          "- control_err   : Control Error\n"
          "- protocol_err  : Protocol Error\n"
          "- multilink_err : Multilink Error\n"
          "- mac_err       : MAC Error\n"
          "- lookup_err    : Lookup Error\n"
          "- all           : All types\n")

DEF_PTYPE("BcpProfileTypes", "select", "fbit(1), zbit(2), bbit(3), pbit(4), pads_field(5), all(6)",
          "\r\nList of BCP profile types:\n"
          "- fbit       : F-Bit\n"
          "- zbit       : Z-Bit\n"
          "- bbit       : B-Bit\n"
          "- pbit       : P-Bit\n"
          "- pads_field : PADs Field\n"
          "- all        : All types\n")

DEF_PTYPE("FcnProfileTypes", "select", "fecn(1), becn(2), de(3), cbit(4), all(5)",
          "\r\nList of FCN profile types:\n"
          "- fecn : FECN-Bit\n"
          "- becn : BECN-Bit\n"
          "- de   : DE-Bit\n"
          "- cbit : C-Bit\n"
          "- all  : All types\n")

DEF_PTYPE("ProfileRules", "select", "drop(1), oam(2), data(3), corrupt(4)",
          "\r\nList of FCN profile types:\n"
          "- drop    : Drop\n"
          "- oam     : Forward to Control Queue\n"
          "- data    : Forward to Data Queue\n"
          "- corrupt : Forward to Corrupt Queue\n")

DEF_SDKCOMMAND(ClishThaNetworkProfileDebugGet,
               "debug show profile network",
               "Get debug information network profile",
               DEF_SDKPARAM_OPTION("profileId", "ProfileList", ""),
               "${profileId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishThaNetworkProfileDebugSet,
               "debug profile network set",
               "Configure debug network profile",
               DEF_SDKPARAM("profileId", "ProfileList", "")
               DEF_SDKPARAM("type", "NetworkProfileTypes", "")
               DEF_SDKPARAM("rule", "ProfileRules", ""),
               "${profileId} ${type} ${rule}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishThaOamProfileDebugGet,
               "debug show profile oam",
               "Get debug information oam profile",
               DEF_SDKPARAM_OPTION("profileId", "ProfileList", ""),
               "${profileId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishThaOamProfileDebugSet,
               "debug profile oam set",
               "Configure debug oam profile",
               DEF_SDKPARAM("profileId", "ProfileList", "")
               DEF_SDKPARAM("type", "OamProfileTypes", "")
               DEF_SDKPARAM("rule", "ProfileRules", ""),
               "${profileId} ${type} ${rule}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishThaErrorProfileDebugGet,
               "debug show profile error",
               "Get debug information error profile",
               DEF_SDKPARAM_OPTION("profileId", "ProfileList", ""),
               "${profileId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishThaErrorProfileDebugSet,
               "debug profile error set",
               "Configure debug error profile",
               DEF_SDKPARAM("profileId", "ProfileList", "")
               DEF_SDKPARAM("type", "ErrorProfileTypes", "")
               DEF_SDKPARAM("rule", "ProfileRules", ""),
               "${profileId} ${type} ${rule}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishThaBcpProfileDebugGet,
               "debug show profile bcp",
               "Get debug information bcp profile",
               DEF_SDKPARAM_OPTION("profileId", "ProfileList", ""),
               "${profileId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishThaBcpProfileDebugSet,
               "debug profile bcp set",
               "Configure debug bcp profile",
               DEF_SDKPARAM("profileId", "ProfileList", "")
               DEF_SDKPARAM("type", "BcpProfileTypes", "")
               DEF_SDKPARAM("rule", "ProfileRules", ""),
               "${profileId} ${type} ${rule}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishThaFcnProfileDebugGet,
               "debug show profile fcn",
               "Get debug information fcn profile",
               DEF_SDKPARAM_OPTION("profileId", "ProfileList", ""),
               "${profileId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClishThaFcnProfileDebugSet,
               "debug profile fcn",
               "Configure debug bcp profile",
               DEF_SDKPARAM("profileId", "ProfileList", "")
               DEF_SDKPARAM("type", "FcnProfileTypes", "")
               DEF_SDKPARAM("rule", "ProfileRules", ""),
               "${profileId} ${type} ${rule}")
    {
    mAtCliCall();
    }

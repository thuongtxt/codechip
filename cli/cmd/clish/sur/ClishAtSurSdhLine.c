/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : ClishAtSurSdhLine.c
 *
 * Created Date: Aug 25, 2015
 *
 * Description : SDH Line Surveillance CLISH CLIs
 *
 * Notes       :
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define LineFailureType "(los|lof|tim|ais|rfi|ber-sd|ber-sf)"
#define LineFailureMask "((("LineFailureType"[\\|])+"LineFailureType")|"LineFailureType")"

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implement Functions ----------------------------*/

/*------------------- Commands Implementation --------------------------------*/
TOP_SDKCOMMAND("sdh line surveillance", "CLIs to control surveillance engine")
TOP_SDKCOMMAND("sdh line surveillance pm", "CLIs to control Performance Monitoring of surveillance engine")
TOP_SDKCOMMAND("sdh line pm", "CLIs to control Performance Monitoring")
TOP_SDKCOMMAND("sdh line pm threshold", "CLIs to control thresholds of Performance Monitoring")
TOP_SDKCOMMAND("sdh line pm current", "CLIs to control current registers")
TOP_SDKCOMMAND("sdh line pm current period", "CLIs to control current period registers")
TOP_SDKCOMMAND("sdh line pm current day", "CLIs to control current day registers")
TOP_SDKCOMMAND("sdh line pm previous", "CLIs to control previous registers")
TOP_SDKCOMMAND("sdh line pm previous period", "CLIs to control previous period registers")
TOP_SDKCOMMAND("sdh line pm previous day", "CLIs to control previous day registers")
TOP_SDKCOMMAND("sdh line surveillance notification", "CLIs to handle notifications")
TOP_SDKCOMMAND("sdh line surveillance threshold", "CLIs to set threshold")
TOP_SDKCOMMAND("sdh line pm recent", "CLIs to control recent registers")
TOP_SDKCOMMAND("sdh line pm recent period", "CLIs to control recent periods")
TOP_SDKCOMMAND("sdh line pm threshold k", "CLIs to handle K thresholds")
TOP_SDKCOMMAND("sdh line failure", "CLIs to handle failures")
TOP_SDKCOMMAND("show sdh line pm", "CLIs to show PM specific information")
TOP_SDKCOMMAND("show sdh line pm threshold", "CLIs to show PM thresholds")
TOP_SDKCOMMAND("show sdh line pm current", "CLIs to show current registers")
TOP_SDKCOMMAND("show sdh line pm previous", "CLIs to show previous registers")
TOP_SDKCOMMAND("show sdh line pm recent", "CLIs to show recent registers")
TOP_SDKCOMMAND("show sdh line surveillance threshold", "CLIs to show threshold")

DEF_PTYPE("eAtSurEngineSdhLinePmParam", "regexp", ".*",
          "List of PM parameters:\n"
          "- sefss : Section Severely Error Framing Seconds\n"
          "- cvs   : Section Coding Violation\n"
          "- ess   : Section Error Seconds\n"
          "- sess  : Section Severely Error Seconds\n"
          "- cvl   : Near-end Line Code Violation\n"
          "- esl   : Near-end Line Error Seconds\n"
          "- sesl  : Near-end Line Severely Error Seconds\n"
          "- uasl  : Near-end Line Unavailable Seconds\n"
          "- fcl   : Near-end Line Failure Counts\n"
          "- cvlfe : Far-end Line Code Violation\n"
          "- eslfe : Far-end Line Error Seconds\n"
          "- seslfe: Far-end Line Severely Error Seconds\n"
          "- uaslfe: Far-end Line Unavailable Seconds\n"
          "- fclfe : Far-end Line Failure Counts\n")

DEF_PTYPE("SdhLinePmFailureMask", "regexp", LineFailureMask,
          "Failure mask\n"
          "- los   : LOS\n"
          "- lof   : LOF\n"
          "- tim   : TIM\n"
          "- ais   : AIS\n"
          "- rfi   : RFI\n"
          "- ber-sd: BER-SD\n"
          "- ber-sf: BER-SF\n"
          "Note, these masks can be ORed, example los|lof|tim\n")

DEF_SDKCOMMAND(cliSdhLineSurInit,
               "sdh line surveillance init",
               "Initialize surveillance engine\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineSurEnable,
               "sdh line surveillance enable",
               "Enable surveillance engine\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineSurDisable,
               "sdh line surveillance disable",
               "Disable surveillance engine\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineSurPmEnable,
               "sdh line surveillance pm enable",
               "Enable Performance Monitoring of surveillance engine\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineSurPmDisable,
               "sdh line surveillance pm disable",
               "Disable Performance Monitoring of surveillance engine\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineNotificationEnable,
               "sdh line surveillance notification enable",
               "Listen on Failures and TCA\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineNotificationDisable,
               "sdh line surveillance notification disable",
               "Stop listening on Failures and TCA \n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineKThresholdEntrySet,
               "sdh line pm threshold k poolentry",
               "Specify K threshold pool entry index",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("entryIndex", "UINT", ""),
               "${lineList} ${entryIndex}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineFailureNotificationMaskSet,
               "sdh line failure notificationmask",
               "Set failure notification mask\n",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("failureMask", "SdhLinePmFailureMask", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${lineList} ${failureMask} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineSurEngineShow,
               "show sdh line surveillance",
               "Show configuration information of surveillance engine\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineFailureShow,
               "show sdh line failure",
               "Show current failure status\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineFailureNotificationMaskShow,
               "show sdh line failure notificationmask",
               "Show failure notification mask\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLinePmKThresholdShow,
               "show sdh line pm threshold k",
               "Show K threshold\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineFailureHistoryShow,
               "show sdh line failure history",
               "Show failure history\n",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${lineList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhLineFailureCaptureShow,
               "show sdh line failure capture",
               "Show captured failures\n",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", "Read to clear without display table"),
               "${lineList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLinePmParamCurrentPeriodRegisterShow,
               "show sdh line pm current period",
               "Show current period\n",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${lineList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLinePmParamCurrentDayRegisterShow,
               "show sdh line pm current day",
               "Show current day\n",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${lineList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLinePmParamPreviousPeriodRegisterShow,
               "show sdh line pm previous period",
               "Show previous period\n",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${lineList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLinePmParamPreviousDayRegisterShow,
               "show sdh line pm previous day",
               "Show previous day\n",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${lineList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLinePmParamRecentPeriodRegisterShow,
               "show sdh line pm recent period",
               "Show recent periods\n",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("recentPeriod", "UINT", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${lineList} ${recentPeriod} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineSurThresholdProfileSet,
               "sdh line surveillance threshold profile",
               "Set Threshold Profile\n",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("profileId", "UINT", " profile ID"),
               "${lineList} ${profileId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineSurThresholdProfileShow,
               "show sdh line surveillance threshold profile",
               "Show Threshold Profile\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineTcaNotificationMaskSet,
               "sdh line pm notificationmask",
               "Set pm notification mask\n",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("pmMask", "eAtSurEngineSdhLinePmParam", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${lineList} ${pmMask} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineTcaNotificationMaskShow,
               "show sdh line pm notificationmask",
               "Show pm notification mask\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }


DEF_SDKCOMMAND(cliSdhLineTcaHistoryShow,
               "show sdh line pm history",
               "Show pm history\n",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", "Read to clear without display table"),
               "${lineList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLineTcaCaptureShow,
               "show sdh line pm capture",
               "Show pm capture\n",
               DEF_SDKPARAM("lineList", "SdhLineList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", "Read to clear without display table"),
               "${lineList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLinePmParamPeriodThresholdShow,
               "show sdh line pm threshold period",
               "Show period threshold\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhLinePmParamDayThresholdShow,
               "show sdh line pm threshold day",
               "Show day threshold\n",
               DEF_SDKPARAM("lineList", "SdhLineList", ""),
               "${lineList}")
    {
    mAtCliCall();
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : ClishAtModuleSur.c
 *
 * Created Date: Sep 12, 2015
 *
 * Description : SUR CLISH CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
DEF_PTYPE("SurRegisterType", "select", "period(1), day(2)", "Register type")
DEF_PTYPE("eAtSurPeriodExpireMethod", "select", "manual(1), auto(2)",
          "Period expire method\n"
          "+ manual: Application will manually trigger period expire\n"
          "+ auto: Period expire is triggered automatically by internal implementation\n")
DEF_PTYPE("eAtSurTickSource", "select", "internal(1), external(2)", "Tick source\n")

TOP_SDKCOMMAND("sur", "SUR CLIs")
TOP_SDKCOMMAND("sur interrupt", "SUR module interrupt")
TOP_SDKCOMMAND("sur interrupt capture", "CLIs to control SUR interrupt capture")
TOP_SDKCOMMAND("show sur", "CLIs to show SUR information")
TOP_SDKCOMMAND("show sur threshold", "CLIs to show SUR threshold information")
TOP_SDKCOMMAND("show sur threshold pool", "CLIs to show SUR threshold pool information")
TOP_SDKCOMMAND("debug sur", "Surveillance's CLIs Debug")
TOP_SDKCOMMAND("sur period", "CLIs to control period")
TOP_SDKCOMMAND("sur period expire notification", "CLIs to control period expire notification")
TOP_SDKCOMMAND("sur threshold", "CLIs to control thresholds")
TOP_SDKCOMMAND("sur threshold profile", "CLIs to control threshold profiles")
TOP_SDKCOMMAND("show sur threshold profile", "CLIs to show SUR threshold profile information")
TOP_SDKCOMMAND("sur failure", "CLIs to control failure globally")
TOP_SDKCOMMAND("sur failure holdoff", "CLIs to control failure hold-off")
TOP_SDKCOMMAND("sur failure holdon", "CLIs to control failure hold-on")
TOP_SDKCOMMAND("sur tick", "CLIs to control PM ticks")

DEF_SDKCOMMAND(cliSurDebugEnable,
               "debug sur enable",
               "Enable debugging\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSurDebugDisable,
               "debug sur disable",
               "Disable debugging\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSurDebug,
               "debug module sur",
               "Show debugging information of SUR module\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModuleSurKThresholdPoolsShow,
               "show sur threshold pool k",
               "Show all of K threshold pools\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModuleSurLineRsKThresholdPoolEntrySet,
               "sur threshold pool k line rs",
               "Specify value for input entries of SDH Line RS K threshold pool\n",
               DEF_SDKPARAM("entries", "STRING", "List of entries")
               DEF_SDKPARAM("value", "UINT", "Value"),
               "${entries} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModuleSurLineMsKThresholdPoolEntrySet,
               "sur threshold pool k line ms",
               "Specify value for input entries of SDH Line MS K threshold pool\n",
               DEF_SDKPARAM("entries", "STRING", "List of entries")
               DEF_SDKPARAM("value", "UINT", "Value"),
               "${entries} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModuleSurHoPathKThresholdPoolEntrySet,
               "sur threshold pool k ho path",
               "Specify value for input entries of SDH HO Path K threshold pool\n",
               DEF_SDKPARAM("entries", "STRING", "List of entries")
               DEF_SDKPARAM("value", "UINT", "Value"),
               "${entries} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModuleSurLoPathKThresholdPoolEntrySet,
               "sur threshold pool k lo path",
               "Specify value for input entries of SDH LO Path K threshold pool\n",
               DEF_SDKPARAM("entries", "STRING", "List of entries")
               DEF_SDKPARAM("value", "UINT", "Value"),
               "${entries} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModuleSurDe3LineKThresholdPoolEntrySet,
               "sur threshold pool k de3 line",
               "Specify value for input entries of DS3/E3 Line K threshold pool\n",
               DEF_SDKPARAM("entries", "STRING", "List of entries")
               DEF_SDKPARAM("value", "UINT", "Value"),
               "${entries} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModuleSurDe3PathKThresholdPoolEntrySet,
               "sur threshold pool k de3 path",
               "Specify value for input entries of DS3/E3 Path K threshold pool\n",
               DEF_SDKPARAM("entries", "STRING", "List of entries")
               DEF_SDKPARAM("value", "UINT", "Value"),
               "${entries} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModuleSurDe1LineKThresholdPoolEntrySet,
               "sur threshold pool k de1 line",
               "Specify value for input entries of DS1/E1 Line K threshold pool\n",
               DEF_SDKPARAM("entries", "STRING", "List of entries")
               DEF_SDKPARAM("value", "UINT", "Value"),
               "${entries} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModuleSurDe1PathKThresholdPoolEntrySet,
               "sur threshold pool k de1 path",
               "Specify value for input entries of DS1/E1 Path K threshold pool\n",
               DEF_SDKPARAM("entries", "STRING", "List of entries")
               DEF_SDKPARAM("value", "UINT", "Value"),
               "${entries} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModuleSurPwKThresholdPoolEntrySet,
               "sur threshold pool k pw",
               "Specify value for input entries of PW K threshold pool\n",
               DEF_SDKPARAM("entries", "STRING", "List of entries")
               DEF_SDKPARAM("value", "UINT", "Value"),
               "${entries} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSurInterruptEnable,
               "sur interrupt enable",
               "Enable interrupt",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSurInterruptDisable,
               "sur interrupt disable",
               "Disable interrupt",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSurPeriodExpire,
               "sur period expire",
               "Notify SUR module that 15-minute period has just expired",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSurPeriodAsyncExpire,
               "sur period expire async",
               "Notify SUR module that 15-minute period has just expired and this calling is none-blocking.",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSurModuleShow,
               "show module sur",
               "Show Surveillance module configuration and status",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSurThresholProfileLine,
               "sur threshold profile line",
               "Set threshold profile\n",
               DEF_SDKPARAM("profiles", "STRING", "List of profiles")
               DEF_SDKPARAM("paramType", "eAtSurEngineSdhLinePmParam", "SDH/SONET Line Param Type")
               DEF_SDKPARAM("value", "UINT", "Value"),
               "${profiles} ${paramType} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSurThresholProfileHoPath,
               "sur threshold profile hopath",
               "Set threshold profile\n",
               DEF_SDKPARAM("profiles", "STRING", "List of profiles")
               DEF_SDKPARAM("paramType", "eAtSurEngineSdhPathPmParam", "SDH/SONET Path Param Type")
               DEF_SDKPARAM("value", "UINT", "Value"),
               "${profiles} ${paramType} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSurThresholProfileLoPath,
               "sur threshold profile lopath",
               "Set threshold profile\n",
               DEF_SDKPARAM("profiles", "STRING", "List of profiles")
               DEF_SDKPARAM("paramType", "eAtSurEngineSdhPathPmParam", "SDH/SONET Path Param Type")
               DEF_SDKPARAM("value", "UINT", "Value"),
               "${profiles} ${paramType} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSurThresholProfileDe1,
               "sur threshold profile de1",
               "Set threshold profile\n",
               DEF_SDKPARAM("profiles", "STRING", "List of profiles")
               DEF_SDKPARAM("paramType", "eAtSurEnginePdhDe1PmParam", "PDH DS1/E1 Param Type")
               DEF_SDKPARAM("value", "UINT", "Value"),
               "${profiles} ${paramType} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSurThresholProfileDe3,
               "sur threshold profile de3",
               "Set threshold profile\n",
               DEF_SDKPARAM("profiles", "STRING", "List of profiles")
               DEF_SDKPARAM("paramType", "eAtSurEnginePdhDe3PmParam", "PDH DS3/E3 Param Type")
               DEF_SDKPARAM("value", "UINT", "Value"),
               "${profiles} ${paramType} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSurThresholProfilePw,
               "sur threshold profile pw",
               "Set threshold profile\n",
               DEF_SDKPARAM("profiles", "STRING", "List of profiles")
               DEF_SDKPARAM("paramType", "eAtSurEnginePwPmParam", "PW Param Type")
               DEF_SDKPARAM("value", "UINT", "Value"),
               "${profiles} ${paramType} ${value}")
    {
    mAtCliCall();
    }


DEF_SDKCOMMAND(cliAtSurThresholProfileLineShow,
               "show sur threshold profile line",
               "Show threshold profile\n",
               DEF_SDKPARAM("profiles", "STRING", "List of profiles"),
               "${profiles}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSurThresholProfileHoPathShow,
               "show sur threshold profile hopath",
               "Show threshold profile\n",
               DEF_SDKPARAM("profiles", "STRING", "List of profiles"),
               "${profiles}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSurThresholProfileLoPathShow,
               "show sur threshold profile lopath",
               "Show threshold profile\n",
               DEF_SDKPARAM("profiles", "STRING", "List of profiles"),
               "${profiles} ")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSurThresholProfileDe1Show,
               "show sur threshold profile de1",
               "Show threshold profile\n",
               DEF_SDKPARAM("profiles", "STRING", "List of profiles"),
               "${profiles}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSurThresholProfileDe3Show,
               "show sur threshold profile de3",
               "Show threshold profile\n",
               DEF_SDKPARAM("profiles", "STRING", "List of profiles"),
               "${profiles}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSurThresholProfilePwShow,
               "show sur threshold profile pw",
               "Show threshold profile\n",
               DEF_SDKPARAM("profiles", "STRING", "List of profiles"),
               "${profiles}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModuleSurFailureHoldOffTimerSet,
               "sur failure holdoff time",
               "Set failure hold-off timer.",
               DEF_SDKPARAM("timerInMs", "UINT", "Failure hold-off timer. When defect "
                            "persists for timerInMs +/- 500ms then corresponding "
                            "failure will raise"),
               "${timerInMs}")
        {
        mAtCliCall();
        }

DEF_SDKCOMMAND(cliAtModuleSurFailureHoldOnTimerSet,
               "sur failure holdon time",
               "Set failure hold-on timer.",
               DEF_SDKPARAM("timerInMs", "UINT", "Failure hold-on timer. When "
                            "defect clears for timerInMs +/- 500ms, then "
                            "corresponding failure will clear"),
               "${timerInMs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModuleSurExpireMethodSet,
               "sur period expire method",
               "Set period expire method (manual or auto)",
               DEF_SDKPARAM("method", "eAtSurPeriodExpireMethod", ""),
               "${method}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModuleSurTickSourceSet,
               "sur tick source",
               "Set period expire method (manual or auto)",
               DEF_SDKPARAM("source", "eAtSurTickSource", ""),
               "${source}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSurExpireNotificationEnable,
               "sur period expire notification enable",
               "Enable expiration notification",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSurExpireNotificationDisable,
               "sur period expire notification disable",
               "Disable expiration notification",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSurInterruptCaptureEnable,
               "sur interrupt capture enable",
               "Enable interrupt capture",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSurInterruptCaptureDisable,
               "sur interrupt capture disable",
               "Disable interrupt capture",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }


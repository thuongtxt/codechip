/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : ClishAtSurSdhPath.c
 *
 * Created Date: Aug 25, 2015
 *
 * Description : SDH Path Surveillance CLISH CLIs
 *
 * Notes       :
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define PathFailureType "(ais|lop|tim|uneq|plm|ber-sd|ber-sf|lom|rfi|rfi-s|rfi-c|rfi-p)"
#define PathFailureMask "((("PathFailureType"[\\|])+"PathFailureType")|"PathFailureType")"

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implement Functions ----------------------------*/

/*------------------- Commands Implementation --------------------------------*/
TOP_SDKCOMMAND("sdh path surveillance", "CLIs to control surveillance engine")
TOP_SDKCOMMAND("sdh path surveillance pm", "CLIs to control Performance Monitoring of surveillance engine")
TOP_SDKCOMMAND("sdh path pm", "CLIs to control Performance Monitoring")
TOP_SDKCOMMAND("sdh path pm threshold", "CLIs to control thresholds of Performance Monitoring")
TOP_SDKCOMMAND("sdh path pm current", "CLIs to control current registers")
TOP_SDKCOMMAND("sdh path pm current period", "CLIs to control current period registers")
TOP_SDKCOMMAND("sdh path pm current day", "CLIs to control current day registers")
TOP_SDKCOMMAND("sdh path pm previous", "CLIs to control previous registers")
TOP_SDKCOMMAND("sdh path pm previous period", "CLIs to control previous period registers")
TOP_SDKCOMMAND("sdh path pm previous day", "CLIs to control previous day registers")
TOP_SDKCOMMAND("sdh path surveillance notification", "CLIs to handle notifications")
TOP_SDKCOMMAND("sdh path pm recent", "CLIs to control recent registers")
TOP_SDKCOMMAND("sdh path pm recent period", "CLIs to control recent periods")
TOP_SDKCOMMAND("sdh path pm threshold k", "CLIs to handle K thresholds")
TOP_SDKCOMMAND("sdh path failure", "CLIs to handle failures")
TOP_SDKCOMMAND("show sdh path pm", "CLIs to show PM specific information")
TOP_SDKCOMMAND("show sdh path pm threshold", "CLIs to show PM thresholds")
TOP_SDKCOMMAND("show sdh path pm current", "CLIs to show current registers")
TOP_SDKCOMMAND("show sdh path pm previous", "CLIs to show previous registers")
TOP_SDKCOMMAND("show sdh path pm recent", "CLIs to show recent registers")
TOP_SDKCOMMAND("show sdh path surveillance threshold", "CLIs to show threshold")
TOP_SDKCOMMAND("sdh path surveillance threshold", "CLIs to Set threshold")

DEF_PTYPE("eAtSurEngineSdhPathPmParam", "regexp", ".*",
          "List of PM parameters:\n"
          "- ppjcpdet: Positive Pointer Justification Count - STS/VT Path Detected\n"
          "- npjcpdet: Negative Pointer Justification Count - STS/VT Path Detected\n"
          "- ppjcpgen: Positive Pointer Justification Count - STS/VT Path Generated\n"
          "- npjcpgen: Negative Pointer Justification Count - STS/VT Path Generated\n"
          "- pjcdiff : Pointer Justification Count Difference\n"
          "- pjcspdet: Pointer Justification Count Seconds - STS/VT Path Detected\n"
          "- pjcspgen: Pointer Justification Count Seconds - STS/VT Path Generated\n"
          "- cvp     : Near-end STS/VT Path Code Violation\n"
          "- esp     : Near-end STS/VT Path Error Seconds\n"
          "- sesp    : Near-end STS/VT Path Severely Error Seconds\n"
          "- uasp    : Near-end STS/VT Path Unavailable Seconds\n"
          "- fcp     : Near End STS/VT Path Failure Counts\n"
          "- cvpfe   : Far-end STS/VT Path Coding Violations. Also Far End Block Error (FEBE)\n"
          "- espfe   : Far-end STS/VT Path Error Seconds\n"
          "- sespfe  : Far-end STS/VT Path Severely Error Seconds\n"
          "- uaspfe  : Far-end STS/VT Path Unavailable Seconds\n"
          "- fcpfe   : Far-end STS/VT Path Failure Counts\n"
          "- efsp    : Error-Free Second count. Derived from ES-P.\n"
          "- efspfe  : Far End STS path Error-Free Second count. Derived from ES-PFE.\n"
          "- asp     : Path Available Second count. Derived from UAS-P.\n"
          "- aspfe   : Far End Path Available Second count. Derived from UAS-PFE.\n"
          "- ebp     : Path Errored Block Count.\n"
          "- bbep    : Path Background Block Error Count.\n"
          "- esrp    : Path Errored Second Ratio.\n"
          "- sesrp   : Path Severely Errored Second Ratio.\n"
          "- bberp   : Path Background Block Error Ratio.\n")

DEF_PTYPE("SdhPathPmFailureMask", "regexp", PathFailureMask,
          "Failure mask\n"
          "- ais\n"
          "- lop\n"
          "- tim\n"
          "- uneq\n"
          "- plm\n"
          "- ber-sd\n"
          "- ber-sf\n"
          "- lom\n"
          "- rfi\n"
          "- rfi-s\n"
          "- rfi-c\n"
          "- rfi-p\n"
          "Note, these masks can be ORed, example ais|lop|tim\n")

DEF_SDKCOMMAND(cliSdhPathSurInit,
               "sdh path surveillance init",
               "Initialize surveillance engine\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathSurEnable,
               "sdh path surveillance enable",
               "Enable surveillance engine\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathSurDisable,
               "sdh path surveillance disable",
               "Disable surveillance engine\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathSurPmEnable,
               "sdh path surveillance pm enable",
               "Enable Performance Monitoring of surveillance engine\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathSurPmDisable,
               "sdh path surveillance pm disable",
               "Disable Performance Monitoring of surveillance engine\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathNotificationEnable,
               "sdh path surveillance notification enable",
               "Listen on Failures and TCA\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathNotificationDisable,
               "sdh path surveillance notification disable",
               "Stop listening on Failures and TCA \n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathKThresholdEntrySet,
               "sdh path pm threshold k poolentry",
               "Specify K threshold pool entry index",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("entryIndex", "UINT", ""),
               "${pathList} ${entryIndex}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathFailureNotificationMaskSet,
               "sdh path failure notificationmask",
               "Set failure notification mask\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("failureMask", "SdhPathPmFailureMask", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${pathList} ${failureMask} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathSurEngineShow,
               "show sdh path surveillance",
               "Show configuration information of surveillance engine\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathFailureShow,
               "show sdh path failure",
               "Show current failure status\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathFailureNotificationMaskShow,
               "show sdh path failure notificationmask",
               "Show failure notification mask\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathPmKThresholdShow,
               "show sdh path pm threshold k",
               "Show K threshold\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathFailureHistoryShow,
               "show sdh path failure history",
               "Show failure history\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${pathList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathFailureCaptureShow,
               "show sdh path failure capture",
               "Show captured failures\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", "Read to clear without display table"),
               "${pathList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathPmParamCurrentPeriodRegisterShow,
               "show sdh path pm current period",
               "Show current period\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${pathList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathPmParamCurrentDayRegisterShow,
               "show sdh path pm current day",
               "Show current day\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${pathList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathPmParamPreviousPeriodRegisterShow,
               "show sdh path pm previous period",
               "Show previous period\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${pathList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathPmParamPreviousDayRegisterShow,
               "show sdh path pm previous day",
               "Show previous day\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${pathList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathPmParamRecentPeriodRegisterShow,
               "show sdh path pm recent period",
               "Show recent periods\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("recentPeriod", "UINT", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${pathList} ${recentPeriod} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathSurThresholdProfileSet,
               "sdh path surveillance threshold profile",
               "Set Threshold Profile\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("profileId", "UINT", " profile ID"),
               "${pathList} ${profileId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathSurThresholdProfileShow,
               "show sdh path surveillance threshold profile",
               "Show Threshold Profile\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathTcaNotificationMaskSet,
               "sdh path pm notificationmask",
               "Set pm notification mask\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("pmMask", "eAtSurEngineSdhPathPmParam", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${pathList} ${pmMask} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathTcaNotificationMaskShow,
               "show sdh path pm notificationmask",
               "Show pm notification mask\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathTcaHistoryShow,
               "show sdh path pm history",
               "Show pm history\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", "Read to clear without display table"),
               "${pathList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathTcaCaptureShow,
               "show sdh path pm capture",
               "Show pm capture\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", "Read to clear without display table"),
               "${pathList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathPmParamPeriodThresholdShow,
               "show sdh path pm threshold period",
               "Show period threshold\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliSdhPathPmParamDayThresholdShow,
               "show sdh path pm threshold day",
               "Show day threshold\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

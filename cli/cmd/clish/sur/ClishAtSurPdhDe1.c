/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH DE1 SUR
 *
 * File        : ClishAtSurPdhDe1.c
 *
 * Created Date: Aug 25, 2015
 *
 * Description : PDH DS1s/E1s CLI implementation
 *
 * Notes       :
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define PdhDe1FailureType "(los|lof|ais|rai|ais-ci|rai-ci)"
#define PdhDe1FailureMask "((("PdhDe1FailureType"[\\|])+"PdhDe1FailureType")|"PdhDe1FailureType")"

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implement Functions ----------------------------*/

/*------------------- Commands Implementation --------------------------------*/
TOP_SDKCOMMAND("pdh de1 surveillance", "CLIs to control surveillance engine")
TOP_SDKCOMMAND("pdh de1 surveillance pm", "CLIs to control Performance Monitoring of surveillance engine")
TOP_SDKCOMMAND("pdh de1 pm", "CLIs to control Performance Monitoring")
TOP_SDKCOMMAND("pdh de1 pm threshold", "CLIs to control thresholds of Performance Monitoring")
TOP_SDKCOMMAND("pdh de1 pm current", "CLIs to control current registers")
TOP_SDKCOMMAND("pdh de1 pm current period", "CLIs to control current period registers")
TOP_SDKCOMMAND("pdh de1 pm current day", "CLIs to control current day registers")
TOP_SDKCOMMAND("pdh de1 pm previous", "CLIs to control previous registers")
TOP_SDKCOMMAND("pdh de1 pm previous period", "CLIs to control previous period registers")
TOP_SDKCOMMAND("pdh de1 pm previous day", "CLIs to control previous day registers")
TOP_SDKCOMMAND("pdh de1 surveillance notification", "CLIs to handle notifications")
TOP_SDKCOMMAND("pdh de1 surveillance threshold", "CLIs to set threshold")

TOP_SDKCOMMAND("pdh de1 pm recent", "CLIs to control recent registers")
TOP_SDKCOMMAND("pdh de1 pm recent period", "CLIs to control recent periods")
TOP_SDKCOMMAND("pdh de1 pm threshold k", "CLIs to handle K thresholds")
TOP_SDKCOMMAND("pdh de1 failure", "CLIs to handle failures")
TOP_SDKCOMMAND("show pdh de1 pm", "CLIs to show PM specific information")
TOP_SDKCOMMAND("show pdh de1 pm threshold", "CLIs to show PM thresholds")
TOP_SDKCOMMAND("show pdh de1 pm current", "CLIs to show current registers")
TOP_SDKCOMMAND("show pdh de1 pm previous", "CLIs to show previous registers")
TOP_SDKCOMMAND("show pdh de1 pm recent", "CLIs to show recent registers")
TOP_SDKCOMMAND("show pdh de1 surveillance threshold", "CLIs to show thresholds")

DEF_PTYPE("eAtSurEnginePdhDe1PmParam", "regexp", ".*",
          "List of PM parameters:\n"
          "- cvl     : Near-end Line Coding Violation\n"
          "- esl     : Near-end Line Error Seconds\n"
          "- sesl    : Near-end Line Severely Error Seconds\n"
          "- lossl   : Near-end Line LOS Seconds\n"
          "- eslfe   : Far-end Line Error Seconds\n"
          "- cvp     : Near-end Path Coding Violation\n"
          "- esp     : Near-end Path Error Seconds\n"
          "- sesp    : Near-end Path Severely Error Seconds\n"
          "- aissp   : Near-end Path AIS Seconds\n"
          "- sasp    : Near-end Path Service Affecting Seconds\n"
          "- cssp    : Near-end Path Controlled Slips Second\n"
          "- uasp    : Near-end Path Unavailable Seconds\n"
          "- sefspfe : Far-end Path Error Frame Seconds\n"
          "- espfe   : Far-end Path Error Seconds\n"
          "- sespfe  : Far-end Path Severely Error Seconds\n"
          "- csspfe  : Far-end Path Controlled Slips Seconds\n"
          "- fcpfe   : Far-end Failure Counts\n"
          "- uaspfe  : Far-end Path Unavailable Seconds\n"
          "- fcp     : Near-end Failure Count-Path\n")

DEF_PTYPE("PdhDe1PmFailureMask", "regexp", PdhDe1FailureMask,
          "Failure mask\n"
          "- los: LOS\n"
          "- lof: LOF\n"
          "- ais: AIS\n"
          "- rai: RAI\n"
          "- ais-ci: AIS-CI\n"
          "- rai-ci: RAI-CI\n"
          "Note, these masks can be ORed, example los|lof|ais\n")

DEF_SDKCOMMAND(cliPdhDe1SurInit,
               "pdh de1 surveillance init",
               "Initialize surveillance engine\n",
               DEF_SDKPARAM("de1List", "PdhDe1List", ""),
               "${de1List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1SurEnable,
               "pdh de1 surveillance enable",
               "Enable surveillance engine\n",
               DEF_SDKPARAM("de1List", "PdhDe1List", ""),
               "${de1List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1SurDisable,
               "pdh de1 surveillance disable",
               "Disable surveillance engine\n",
               DEF_SDKPARAM("de1List", "PdhDe1List", ""),
               "${de1List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1SurPmEnable,
               "pdh de1 surveillance pm enable",
               "Enable Performance Monitoring of surveillance engine\n",
               DEF_SDKPARAM("de1List", "PdhDe1List", ""),
               "${de1List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1SurPmDisable,
               "pdh de1 surveillance pm disable",
               "Disable Performance Monitoring of surveillance engine\n",
               DEF_SDKPARAM("de1List", "PdhDe1List", ""),
               "${de1List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe1NotificationEnable,
               "pdh de1 surveillance notification enable",
               "Listen on Failures and TCA\n",
               DEF_SDKPARAM("de1List", "PdhDe1List", ""),
               "${de1List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1NotificationDisable,
               "pdh de1 surveillance notification disable",
               "Stop listening on Failures and TCA \n",
               DEF_SDKPARAM("de1List", "PdhDe1List", ""),
               "${de1List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1KThresholdEntrySet,
               "pdh de1 pm threshold k poolentry",
               "Specify K threshold pool entry index",
               DEF_SDKPARAM("de1List", "PdhDe1List", "")
               DEF_SDKPARAM("entryIndex", "UINT", ""),
               "${de1List} ${entryIndex}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1FailureNotificationMaskSet,
               "pdh de1 failure notificationmask",
               "Set failure notification mask\n",
               DEF_SDKPARAM("de1List", "PdhDe1List", "")
               DEF_SDKPARAM("failureMask", "PdhDe1PmFailureMask", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${de1List} ${failureMask} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1SurEngineShow,
               "show pdh de1 surveillance",
               "Show configuration information of surveillance engine\n",
               DEF_SDKPARAM("de1List", "PdhDe1List", ""),
               "${de1List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1FailureShow,
               "show pdh de1 failure",
               "Show current failure status\n",
               DEF_SDKPARAM("de1List", "PdhDe1List", ""),
               "${de1List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1FailureCaptureShow,
               "show pdh de1 failure capture",
               "Show captured failures\n",
               DEF_SDKPARAM("de1s", "PdhDe1List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", "Read to clear without display table"),
               "${de1s} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1FailureNotificationMaskShow,
               "show pdh de1 failure notificationmask",
               "Show failure notification mask\n",
               DEF_SDKPARAM("de1List", "PdhDe1List", ""),
               "${de1List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPdhDe1PmKThresholdShow,
               "show pdh de1 pm threshold k",
               "Show K threshold\n",
               DEF_SDKPARAM("de1List", "PdhDe1List", ""),
               "${de1List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1FailureHistoryShow,
               "show pdh de1 failure history",
               "Show failure history\n",
               DEF_SDKPARAM("de1List", "PdhDe1List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${de1List} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1PmParamCurrentPeriodRegisterShow,
               "show pdh de1 pm current period",
               "Show current period\n",
               DEF_SDKPARAM("de1List", "PdhDe1List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${de1List} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1PmParamCurrentDayRegisterShow,
               "show pdh de1 pm current day",
               "Show current day\n",
               DEF_SDKPARAM("de1List", "PdhDe1List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${de1List} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1PmParamPreviousPeriodRegisterShow,
               "show pdh de1 pm previous period",
               "Show previous period\n",
               DEF_SDKPARAM("de1List", "PdhDe1List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${de1List} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1PmParamPreviousDayRegisterShow,
               "show pdh de1 pm previous day",
               "Show previous day\n",
               DEF_SDKPARAM("de1List", "PdhDe1List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${de1List} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1PmParamRecentPeriodRegisterShow,
               "show pdh de1 pm recent period",
               "Show recent periods\n",
               DEF_SDKPARAM("de1List", "PdhDe1List", "")
			   DEF_SDKPARAM("recentPeriod", "UINT", "")
			   DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
			   DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${de1List} ${recentPeriod} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1SurThresholdProfileSet,
               "pdh de1 surveillance threshold profile",
               "Set Threshold Profile\n",
               DEF_SDKPARAM("de1List", "PdhDe1List", "")
               DEF_SDKPARAM("profileId", "UINT", " profile ID"),
               "${de1List} ${profileId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1SurThresholdProfileShow,
               "show pdh de1 surveillance threshold profile",
               "Show Threshold Profile\n",
               DEF_SDKPARAM("de1List", "PdhDe1List", ""),
               "${de1List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1TcaNotificationMaskSet,
               "pdh de1 pm notificationmask",
               "Set pm notification mask\n",
               DEF_SDKPARAM("de1List", "PdhDe1List", "")
               DEF_SDKPARAM("pmMask", "eAtSurEnginePdhDe1PmParam", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${de1List} ${pmMask} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1TcaNotificationMaskShow,
               "show pdh de1 pm notificationmask",
               "Show pm notification mask\n",
               DEF_SDKPARAM("de1List", "PdhDe1List", ""),
               "${de1List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1TcaHistoryShow,
               "show pdh de1 pm history",
               "Show pm history\n",
               DEF_SDKPARAM("de1List", "PdhDe1List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", "Read to clear without display table"),
               "${de1List} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1TcaCaptureShow,
               "show pdh de1 pm capture",
               "Show pm capture\n",
               DEF_SDKPARAM("de1List", "PdhDe1List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", "Read to clear without display table"),
               "${de1List} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1PmParamPeriodThresholdShow,
               "show pdh de1 pm threshold period",
               "Show period threshold\n",
               DEF_SDKPARAM("de1List", "PdhDe1List", ""),
               "${de1List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe1PmParamDayThresholdShow,
               "show pdh de1 pm threshold day",
               "Show day threshold\n",
               DEF_SDKPARAM("de1List", "PdhDe1List", ""),
               "${de1List}")
    {
    mAtCliCall();
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : ClishAtSurPw.c
 *
 * Created Date: Aug 25, 2015
 *
 * Description : PW Surveillance CLISH CLIs
 *
 * Notes       :
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define PwFailureType "(lops|underrun|overrun|rbit|lbit|stray|malformed|missing|mbit)"
#define PwFailureMask "((("PwFailureType"[\\|])+"PwFailureType")|"PwFailureType")"

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implement Functions ----------------------------*/

/*------------------- Commands Implementation --------------------------------*/
TOP_SDKCOMMAND("pw surveillance", "CLIs to control surveillance engine")
TOP_SDKCOMMAND("pw surveillance pm", "CLIs to control Performance Monitoring of surveillance engine")
TOP_SDKCOMMAND("pw pm", "CLIs to control Performance Monitoring")
TOP_SDKCOMMAND("pw pm threshold", "CLIs to control thresholds of Performance Monitoring")
TOP_SDKCOMMAND("pw pm current", "CLIs to control current registers")
TOP_SDKCOMMAND("pw pm current period", "CLIs to control current period registers")
TOP_SDKCOMMAND("pw pm current day", "CLIs to control current day registers")
TOP_SDKCOMMAND("pw pm previous", "CLIs to control previous registers")
TOP_SDKCOMMAND("pw pm previous period", "CLIs to control previous period registers")
TOP_SDKCOMMAND("pw pm previous day", "CLIs to control previous day registers")
TOP_SDKCOMMAND("pw surveillance notification", "CLIs to handle notifications")
TOP_SDKCOMMAND("pw surveillance threshold", "CLIs to set threshold")
TOP_SDKCOMMAND("pw pm recent", "CLIs to control recent registers")
TOP_SDKCOMMAND("pw pm recent period", "CLIs to control recent periods")
TOP_SDKCOMMAND("pw pm threshold k", "CLIs to handle K thresholds")
TOP_SDKCOMMAND("pw failure", "CLIs to handle failures")
TOP_SDKCOMMAND("show pw pm", "CLIs to show PM specific information")
TOP_SDKCOMMAND("show pw pm threshold", "CLIs to show PM thresholds")
TOP_SDKCOMMAND("show pw pm current", "CLIs to show current registers")
TOP_SDKCOMMAND("show pw pm previous", "CLIs to show previous registers")
TOP_SDKCOMMAND("show pw pm recent", "CLIs to show recent registers")
TOP_SDKCOMMAND("show pw surveillance threshold", "CLIs to show threshold")

DEF_PTYPE("eAtSurEnginePwPmParam", "regexp", ".*",
          "List of PM parameters:\n"
          "- es    : Near-end Error Second\n"
          "- ses   : Near-end Severely Error Seconds\n"
          "- uas   : Near-end Unavailable Seconds\n"
          "- fe-es : Far-end Error Second\n"
          "- fe-ses: Far-end Severely Error Seconds\n"
          "- fe-uas: Far-end Unavailable Seconds\n")

DEF_PTYPE("PwPmFailureMask", "regexp", PwFailureMask,
          "Failure mask\n"
          "- lops          : Loss of packet synchronization\n"
          "- overrun       : Jitter overrun\n"
          "- underrun      : Jitter underrun\n"
          "- rbit          : R Bit\n"
          "- lbit          : L Bit\n"
          "- stray         : Stray packets\n"
          "- malformed     : Malformed packets\n"
          "- missing       : Missing consecutive packets greater than pre-defined configurable threshold\n"
          "Note, these masks can be ORed, example los|lof|ais\n")

DEF_SDKCOMMAND(cliPwSurInit,
               "pw surveillance init",
               "Initialize surveillance engine\n",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwSurEnable,
               "pw surveillance enable",
               "Enable surveillance engine\n",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwSurDisable,
               "pw surveillance disable",
               "Disable surveillance engine\n",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwSurPmEnable,
               "pw surveillance pm enable",
               "Enable Performance Monitoring of surveillance engine\n",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwSurPmDisable,
               "pw surveillance pm disable",
               "Disable Performance Monitoring of surveillance engine\n",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwNotificationEnable,
               "pw surveillance notification enable",
               "Listen on Failures and TCA\n",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwNotificationDisable,
               "pw surveillance notification disable",
               "Stop listening on Failures and TCA \n",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwKThresholdEntrySet,
               "pw pm threshold k poolentry",
               "Specify K threshold pool entry index",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("entryIndex", "UINT", ""),
               "${pwList} ${entryIndex}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwFailureNotificationMaskSet,
               "pw failure notificationmask",
               "Set failure notification mask\n",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("failureMask", "PwPmFailureMask", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${pwList} ${failureMask} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwSurEngineShow,
               "show pw surveillance",
               "Show configuration information of surveillance engine\n",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwFailureShow,
               "show pw failure",
               "Show current failure status\n",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwFailureNotificationMaskShow,
               "show pw failure notificationmask",
               "Show failure notification mask\n",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPwPmKThresholdShow,
               "show pw pm threshold k",
               "Show K threshold\n",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwFailureHistoryShow,
               "show pw failure history",
               "Show failure history\n",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${pwList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPwFailureCaptureGet,
               "show pw failure capture",
               "Show captured failures",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", "Read to clear without display table"),
               "${pwList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwPmParamCurrentPeriodRegisterShow,
               "show pw pm current period",
               "Show current period\n",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${pwList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwPmParamCurrentDayRegisterShow,
               "show pw pm current day",
               "Show current day\n",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${pwList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwPmParamPreviousPeriodRegisterShow,
               "show pw pm previous period",
               "Show previous period\n",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${pwList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwPmParamPreviousDayRegisterShow,
               "show pw pm previous day",
               "Show previous day\n",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${pwList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwPmParamRecentPeriodRegisterShow,
               "show pw pm recent period",
               "Show recent periods\n",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("recentPeriod", "UINT", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${pwList} ${recentPeriod} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwSurThresholdProfileSet,
               "pw surveillance threshold profile",
               "Set Threshold Profile\n",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("threshold", "UINT", " profile ID"),
               "${pwList} ${profileId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwSurThresholdProfileShow,
               "show pw surveillance threshold profile",
               "Show Threshold Profile\n",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwTcaNotificationMaskSet,
               "pw pm notificationmask",
               "Set pm notification mask\n",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("pmMask", "eAtSurEnginePwPmParam", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${pwList} ${pmMask} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwTcaNotificationMaskShow,
               "show pw pm notificationmask",
               "Show pm notification mask\n",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwTcaHistoryShow,
               "show pw pm history",
               "Show pm history\n",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", "Read to clear without display table"),
               "${pwList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwTcaCaptureShow,
               "show pw pm capture",
               "Show pm capture\n",
               DEF_SDKPARAM("pwList", "PwIdList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", "Read to clear without display table"),
               "${pwList} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwPmParamPeriodThresholdShow,
               "show pw pm threshold period",
               "Show period threshold\n",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPwPmParamDayThresholdShow,
               "show pw pm threshold day",
               "Show day threshold\n",
               DEF_SDKPARAM("pwList", "PwIdList", ""),
               "${pwList}")
    {
    mAtCliCall();
    }

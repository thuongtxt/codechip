/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH DE3 SUR
 *
 * File        : ClishAtSurPdhDe3.c
 *
 * Created Date: Aug 25, 2015
 *
 * Description : PDH DS1s/E1s CLI implementation
 *
 * Notes       :
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define PdhDe3FailureType "(los|lof|ais|rai)"
#define PdhDe3FailureMask "((("PdhDe3FailureType"[\\|])+"PdhDe3FailureType")|"PdhDe3FailureType")"

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implement Functions ----------------------------*/

/*------------------- Commands Implementation --------------------------------*/
TOP_SDKCOMMAND("pdh de3 surveillance", "CLIs to control surveillance engine")
TOP_SDKCOMMAND("pdh de3 surveillance pm", "CLIs to control Performance Monitoring of surveillance engine")
TOP_SDKCOMMAND("pdh de3 pm", "CLIs to control Performance Monitoring")
TOP_SDKCOMMAND("pdh de3 pm threshold", "CLIs to control thresholds of Performance Monitoring")
TOP_SDKCOMMAND("pdh de3 pm current", "CLIs to control current registers")
TOP_SDKCOMMAND("pdh de3 pm current period", "CLIs to control current period registers")
TOP_SDKCOMMAND("pdh de3 pm current day", "CLIs to control current day registers")
TOP_SDKCOMMAND("pdh de3 pm previous", "CLIs to control previous registers")
TOP_SDKCOMMAND("pdh de3 pm previous period", "CLIs to control previous period registers")
TOP_SDKCOMMAND("pdh de3 pm previous day", "CLIs to control previous day registers")
TOP_SDKCOMMAND("pdh de3 surveillance notification", "CLIs to handle notifications")
TOP_SDKCOMMAND("pdh de3 surveillance threshold", "CLIs to set threshold")
TOP_SDKCOMMAND("pdh de3 pm recent", "CLIs to control recent registers")
TOP_SDKCOMMAND("pdh de3 pm recent period", "CLIs to control recent periods")
TOP_SDKCOMMAND("pdh de3 pm threshold k", "CLIs to handle K thresholds")
TOP_SDKCOMMAND("pdh de3 failure", "CLIs to handle failures")
TOP_SDKCOMMAND("show pdh de3 pm", "CLIs to show PM specific information")
TOP_SDKCOMMAND("show pdh de3 pm threshold", "CLIs to show PM thresholds")
TOP_SDKCOMMAND("show pdh de3 pm current", "CLIs to show current registers")
TOP_SDKCOMMAND("show pdh de3 pm previous", "CLIs to show previous registers")
TOP_SDKCOMMAND("show pdh de3 pm recent", "CLIs to show recent registers")
TOP_SDKCOMMAND("show pdh de3 surveillance threshold", "CLIs to show threshold")

DEF_PTYPE("eAtSurEnginePdhDe3PmParam", "regexp", ".*",
          "List of PM parameters:\n"
          "- cvl     : Near-end Line Coding Violation\n"
          "- esl     : Near-end Line Error Seconds\n"
          "- sesl    : Near-end Line Severely Error Seconds\n"
          "- lossl   : Near-end Line LOS Seconds\n"
          "- cvpp    : Near-end Path Code Violation\n"
          "- cvcpp   : Near-end Path C-bit Code Violation\n"
          "- espp    : Near-end Path Error Seconds\n"
          "- escpp   : Near-end C-bit Error Seconds\n"
          "- sespp   : Near-end Severely Error Seconds\n"
          "- sescpp  : Near-end C-bit Severely Error Seconds\n"
          "- sasp    : Near-end Path Service Affecting Seconds\n"
          "- aissp   : Near-end Path AIS Seconds\n"
          "- uaspp   : Near-end Path Unavailable Seconds\n"
          "- uascpp  : Near-end C-bit Unavailable Seconds\n"
          "- cvcppfe : Far-end C-bit Coding Violation\n"
          "- escppfe : Far-end C-bit Error Seconds\n"
          "- esbcppfe: Far-end C-bit Error Seconds Type B,Error Second type B\n"
          "- sescppfe: Far-end C-bit Severely Error Seconds\n"
          "- sascppfe: Far-end C-bit Service Affecting Seconds\n"
          "- uascppfe: Far-end C-bit Unavailable Seconds\n"
          "- esal    : Errored Second-Line type A\n"
          "- esbl    : Errored Second-Line type B\n"
          "- esapp   : Errored Second type A, P-bit parity\n"
          "- esacpp  : Errored Second type A, CP-bit parity\n"
          "- esbpp   : Errored Second type B, P-bit parity\n"
          "- esbcpp  : Errored Second type B, CP-bit parity\n"
          "- fcp     : Near-end Failure Count-Path\n"
          "- esacppfe: Error Second type A\n"
          "- fccppfe : Far-end Failure Count-Path\n")

DEF_PTYPE("PdhDe3PmFailureMask", "regexp", PdhDe3FailureMask,
          "Failure mask\n"
          "- los: LOS\n"
          "- lof: LOF\n"
          "- ais: AIS\n"
          "- rai: RAI\n"
          "Note, these masks can be ORed, example los|lof|ais\n")

DEF_SDKCOMMAND(cliPdhDe3SurInit,
               "pdh de3 surveillance init",
               "Initialize surveillance engine\n",
               DEF_SDKPARAM("de3List", "PdhDe3List", ""),
               "${de3List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3SurEnable,
               "pdh de3 surveillance enable",
               "Enable surveillance engine\n",
               DEF_SDKPARAM("de3List", "PdhDe3List", ""),
               "${de3List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3SurDisable,
               "pdh de3 surveillance disable",
               "Disable surveillance engine\n",
               DEF_SDKPARAM("de3List", "PdhDe3List", ""),
               "${de3List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3SurPmEnable,
               "pdh de3 surveillance pm enable",
               "Enable Performance Monitoring of surveillance engine\n",
               DEF_SDKPARAM("de3List", "PdhDe3List", ""),
               "${de3List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3SurPmDisable,
               "pdh de3 surveillance pm disable",
               "Disable Performance Monitoring of surveillance engine\n",
               DEF_SDKPARAM("de3List", "PdhDe3List", ""),
               "${de3List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliPdhDe3NotificationEnable,
               "pdh de3 surveillance notification enable",
               "Listen on Failures and TCA\n",
               DEF_SDKPARAM("de3List", "PdhDe3List", ""),
               "${de3List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3NotificationDisable,
               "pdh de3 surveillance notification disable",
               "Stop listening on Failures and TCA \n",
               DEF_SDKPARAM("de3List", "PdhDe3List", ""),
               "${de3List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3KThresholdEntrySet,
               "pdh de3 pm threshold k poolentry",
               "Specify K threshold pool entry index",
               DEF_SDKPARAM("de3List", "PdhDe3List", "")
               DEF_SDKPARAM("entryIndex", "UINT", ""),
               "${de3List} ${entryIndex}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3FailureNotificationMaskSet,
               "pdh de3 failure notificationmask",
               "Set failure notification mask\n",
               DEF_SDKPARAM("de3List", "PdhDe3List", "")
               DEF_SDKPARAM("failureMask", "PdhDe3PmFailureMask", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${de3List} ${failureMask} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3SurEngineShow,
               "show pdh de3 surveillance",
               "Show configuration information of surveillance engine\n",
               DEF_SDKPARAM("de3List", "PdhDe3List", ""),
               "${de3List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3FailureShow,
               "show pdh de3 failure",
               "Show current failure status\n",
               DEF_SDKPARAM("de3List", "PdhDe3List", ""),
               "${de3List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3FailureCaptureShow,
               "show pdh de3 failure capture",
               "Show captured failures\n",
               DEF_SDKPARAM("de3List", "PdhDe3List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", "Read to clear without display table"),
               "${de3List} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3FailureNotificationMaskShow,
               "show pdh de3 failure notificationmask",
               "Show failure notification mask\n",
               DEF_SDKPARAM("de3List", "PdhDe3List", ""),
               "${de3List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPdhDe3PmKThresholdShow,
               "show pdh de3 pm threshold k",
               "Show K threshold\n",
               DEF_SDKPARAM("de3List", "PdhDe3List", ""),
               "${de3List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3FailureHistoryShow,
               "show pdh de3 failure history",
               "Show failure history\n",
               DEF_SDKPARAM("de3List", "PdhDe3List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${de3List} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3PmParamCurrentPeriodRegisterShow,
               "show pdh de3 pm current period",
               "Show current period\n",
               DEF_SDKPARAM("de3List", "PdhDe3List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${de3List} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3PmParamCurrentDayRegisterShow,
               "show pdh de3 pm current day",
               "Show current day\n",
               DEF_SDKPARAM("de3List", "PdhDe3List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${de3List} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3PmParamPreviousPeriodRegisterShow,
               "show pdh de3 pm previous period",
               "Show previous period\n",
               DEF_SDKPARAM("de3List", "PdhDe3List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${de3List} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3PmParamPreviousDayRegisterShow,
               "show pdh de3 pm previous day",
               "Show previous day\n",
               DEF_SDKPARAM("de3List", "PdhDe3List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${de3List} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3PmParamRecentPeriodRegisterShow,
               "show pdh de3 pm recent period",
               "Show recent periods\n",
               DEF_SDKPARAM("de3List", "PdhDe3List", "")
               DEF_SDKPARAM("recentPeriod", "UINT", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${de3List} ${recentPeriod} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3SurThresholdProfileSet,
               "pdh de3 surveillance threshold profile",
               "Set Threshold Profile\n",
               DEF_SDKPARAM("de3List", "PdhDe3List", "")
               DEF_SDKPARAM("profileId", "UINT", " profile ID"),
               "${de3List} ${profileId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3SurThresholdProfileShow,
               "show pdh de3 surveillance threshold profile",
               "Show Threshold Profile\n",
               DEF_SDKPARAM("de3List", "PdhDe3List", ""),
               "${de3List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3TcaNotificationMaskSet,
               "pdh de3 pm notificationmask",
               "Set pm notification mask\n",
               DEF_SDKPARAM("de3List", "PdhDe3List", "")
               DEF_SDKPARAM("pmMask", "eAtSurEnginePdhDe3PmParam", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${de3List} ${pmMask} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3TcaNotificationMaskShow,
               "show pdh de3 pm notificationmask",
               "Show pm notification mask\n",
               DEF_SDKPARAM("de3List", "PdhDe3List", ""),
               "${de3List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3TcaHistoryShow,
               "show pdh de3 pm history",
               "Show pm history\n",
               DEF_SDKPARAM("de3List", "PdhDe3List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", "Read to clear without display table"),
               "${de3List} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3TcaCaptureShow,
               "show pdh de3 pm capture",
               "Show pm capture\n",
               DEF_SDKPARAM("de3List", "PdhDe3List", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", "Read to clear without display table"),
               "${de3List} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3PmParamPeriodThresholdShow,
               "show pdh de3 pm threshold period",
               "Show period threshold\n",
               DEF_SDKPARAM("de3List", "PdhDe3List", ""),
               "${de3List}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliPdhDe3PmParamDayThresholdShow,
               "show pdh de3 pm threshold day",
               "Show day threshold\n",
               DEF_SDKPARAM("de3List", "PdhDe3List", ""),
               "${de3List}")
    {
    mAtCliCall();
    }

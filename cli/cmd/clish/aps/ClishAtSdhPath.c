/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : ClishAtSdhPath.c
 *
 * Created Date: Oct 24, 2016
 *
 * Description : CLISH CLIs to handle APS associations on Path
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("sdh path aps", "CLIs to set APS information of SONET/SDH path\n")
TOP_SDKCOMMAND("sdh path aps force", "CLIs to force APS information of SONET/SDH path\n")
TOP_SDKCOMMAND("show sdh path aps", "CLIs to show APS information\n")

DEF_SDKCOMMAND(cliAtSdhPathApsEngineShow,
               "show sdh path aps engine",
               "Show APS Engine infromation\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "${pathList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathApsSelectorsShow,
               "show sdh path aps selectors",
               "Show APS Selectors infromation\n",
               DEF_SDKPARAM("path", "SdhPathList", ""),
               "${path}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathApsSwitchingConditionSet,
               "sdh path aps condition",
               "Set path APS switching condition\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("defectMask", "UpsrPathDefectMask", "")
               DEF_SDKPARAM("switchCond", "eAtApsSwitchCondition", ""),
               "$(pathList) ${defectMask} ${switchCond}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathApsForcedSwitchingConditionSet,
               "sdh path aps force condition",
               "Force path APS switching condition\n",
               DEF_SDKPARAM("pathList", "SdhPathList", "")
               DEF_SDKPARAM("switchCond", "eAtApsSwitchCondition", ""),
               "$(pathList) ${switchCond}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtSdhPathApsSwitchingConditionShow,
               "show sdh path aps condition",
               "Show path APS switching condition\n",
               DEF_SDKPARAM("pathList", "SdhPathList", ""),
               "$(pathList)")
    {
    mAtCliCall();
    }


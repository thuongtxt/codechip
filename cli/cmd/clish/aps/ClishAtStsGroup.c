/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS Module
 *
 * File        : ClishAtStsGroup.c
 *
 * Created Date: Dec 2, 2016
 *
 * Description : Sts1s Group
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("aps sts", "STS Pass-through Groups CLIs")
TOP_SDKCOMMAND("aps sts group", "STS Pass-through Groups CLIs")
TOP_SDKCOMMAND("aps sts group passthrough", "STS Pass-through Groups CLIs")
TOP_SDKCOMMAND("show aps sts", "Show STS Pass-through Group CLIs ")

DEF_PTYPE("Sts1IdList", "regexp", "sts1.*",
          "List of Sts1 Group\n"
          "- STS1 : sts1.<lineId>.<sts1Id>\n")

DEF_SDKCOMMAND(cliAtStsGroupCreate,
               "aps sts group create",
               "Create STS1 pass-through Group\n",
               DEF_SDKPARAM("groupIds", "GroupIdList", ""),
               "${groupIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtStsGroupDelete,
               "aps sts group delete",
               "Delete STS1 pass-through Group\n",
               DEF_SDKPARAM("groupIds", "GroupIdList", ""),
               "${groupIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtStsGroupConnect,
               "aps sts group connect",
               "Connect source STS pass-through groups to destination STS pass-through groups\n",
               DEF_SDKPARAM("srcGroupIds", "GroupIdList", "")
               DEF_SDKPARAM("destGroupIds", "GroupIdList", ""),
               "${srcGroupIds} ${destGroupIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtStsGroupDisconnect,
               "aps sts group disconnect",
               "Connect destination STS pass-through groups from source STS pass-through groups\n",
               DEF_SDKPARAM("srcGroupIds", "GroupIdList", ""),
               "${srcGroupIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtStsGroupAdd,
               "aps sts group add",
               "Add STS-1s into an STS pass-through group\n",
               DEF_SDKPARAM("groupIds", "GroupIdList", "")
               DEF_SDKPARAM("sts1Ids", "Sts1IdList", ""),
               "${groupIds} ${sts1Ids}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtStsGroupRemove,
               "aps sts group remove",
               "Remove STS-1s from an STS pass-through group\n",
               DEF_SDKPARAM("groupIds", "GroupIdList", "")
               DEF_SDKPARAM("sts1Ids", "Sts1IdList", ""),
               "${groupIds} ${sts1Ids}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtStsGroupPassthroughEanble,
               "aps sts group passthrough enable",
               "Enable unidirectional pass-through from STS group to its destination STS group.\n",
               DEF_SDKPARAM("groupIds", "GroupIdList", ""),
               "${groupIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtStsGroupPassthroughDisable,
               "aps sts group passthrough disable",
               "Disable unidirectional pass-through from STS group to its destination STS group\n",
               DEF_SDKPARAM("groupIds", "GroupIdList", ""),
               "${groupIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtStsGroupShow,
               "show aps sts group",
               "Show STS pass-through groups\n",
               DEF_SDKPARAM("groupIds", "GroupIdList", ""),
               "${groupIds}")
    {
    mAtCliCall();
    }

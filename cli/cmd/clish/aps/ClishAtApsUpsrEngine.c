/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : ClishAtApsUpsrEngine.c
 *
 * Created Date: Apr 17, 2017
 *
 * Description : APS UPSR engine CLISH
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/
#define UpsrDefectType "(ais|lop|uneq|plm|tim|ber-sd|ber-sf)"
#define UpsrDefectMask "((("UpsrDefectType"[\\|])+"UpsrDefectType")|"UpsrDefectType")"

#define UpsrEvents "(switch|state-change)"
#define UpsrEventsMask  "((("UpsrEvents"[\\|])+"UpsrEvents")|"UpsrEvents")"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("aps upsr", "APS UPSR CLIs")
TOP_SDKCOMMAND("aps upsr engine", "APS UPSR Engine CLIs")
TOP_SDKCOMMAND("aps upsr engine alarm", "APS UPSR Engine alarm CLIs")
TOP_SDKCOMMAND("show aps upsr", "Show APS UPSR CLIs ")
TOP_SDKCOMMAND("debug aps upsr", "Show APS UPSR information")

DEF_PTYPE("UpsrPathDefectMask", "regexp", UpsrDefectMask,
          "\nUPSR path defect masks:\n"
          "- ais    : AIS\n"
          "- lop    : LOP\n"
          "- tim    : TIM\n"
          "- uneq   : UNEQ\n"
          "- plm    : PLM\n"
          "- ber-sd : BER-SD\n"
          "- ber-sf : BER-SF\n"
          "Note, these masks can be ORed, example ais|lop|tim\n")
DEF_PTYPE("eAtApsUpsrEvent", "regexp", UpsrEventsMask,
          "\nUPSR interrupt masks:\n"
          "- switch       : A switching has been taken\n"
          "- state-change : USPR engine get change of its state\n"
          "Note, these masks can be ORed, example switch|state-change\n")
DEF_PTYPE("eAtApsUpsrExtCmd", "select", "clear(0), fs2wrk(1), fs2prt(2), ms2wrk(3), ms2prt(4), lp(5)", "APS UPSR External commands")

DEF_SDKCOMMAND(cliAtModuleApsUpsrEngineCreate,
               "aps upsr engine create",
               "Create APS UPSR engine\n",
               DEF_SDKPARAM("engineId", "UINT", "Engine ID")
               DEF_SDKPARAM("workingPath", "SdhPathList", "")
               DEF_SDKPARAM("protectionPath", "SdhPathList", ""),
               "${engineId} ${workingPath} ${protectionPath}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModuleApsUpsrEngineDelete,
               "aps upsr engine delete",
               "Delete APS UPSR engine\n",
               DEF_SDKPARAM("engineIds", "EngineIdList", ""),
               "${engineIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtApsUpsrEnginesStart,
               "aps upsr engine start",
               "Start APS UPSR engine\n",
               DEF_SDKPARAM("engineIds", "EngineIdList", ""),
               "$(engineIds)")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtApsUpsrEnginesStop,
               "aps upsr engine stop",
               "Stop APS UPSR engine\n",
               DEF_SDKPARAM("engineIds", "EngineIdList", ""),
               "$(engineIds)")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtApsUpsrEngineDebug,
               "debug aps upsr engine",
               "Show APS UPSR engine debug information\n",
               DEF_SDKPARAM("engineIds", "EngineIdList", ""),
               "$(engineIds)")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtApsUpsrEnginesSwitchConditionShow,
               "show aps upsr engine switch_condition",
               "Show switching condition\n",
               DEF_SDKPARAM("engineIds", "EngineIdList", ""),
               "$(engineIds)")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModuleApsUpsrEnginesGet,
               "show aps upsr engine",
               "Show Upsr APS engines\n",
               DEF_SDKPARAM("engineIds", "EngineIdList", ""),
               "$(engineIds)")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtApsUpsrEnginesExternalCommand,
               "aps upsr engine extcmd",
               "Issue external command\n",
               DEF_SDKPARAM("engineIds", "EngineIdList", "")
               DEF_SDKPARAM("extCmd", "eAtApsUpsrExtCmd", ""),
               "$(engineIds) ${extCmd}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtApsUpsrEnginesSwitchingType,
               "aps upsr engine switchtype",
               "Change switching type\n",
               DEF_SDKPARAM("engineIds", "EngineIdList", "")
               DEF_SDKPARAM("switchingType", "eAtApsSwitchType", ""),
               "$(engineIds) ${switchingType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtApsUpsrEnginesWtrSet,
               "aps upsr engine wtr",
               "Set WTR timer\n",
               DEF_SDKPARAM("engineIds", "EngineIdList", "")
               DEF_SDKPARAM("wtr", "UINT", "WTRTimer"),
               "$(engineIds) ${wtr}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtApsUpsrEnginesSwitchingConditionSet,
               "aps upsr engine condition",
               "Set switching condition\n",
               DEF_SDKPARAM("engineIds", "EngineIdList", "")
               DEF_SDKPARAM("defectMask", "UpsrPathDefectMask", "")
               DEF_SDKPARAM("switchCond", "eAtApsSwitchCondition", ""),
               "$(engineIds) ${defectMask} ${switchCond}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtApsUpsrEnginesInterruptMaskSet,
               "aps upsr engine interruptmask",
               "Enable UPSR Alarm Interrupt Status\n",
               DEF_SDKPARAM("engineIds", "EngineIdList", "")
               DEF_SDKPARAM("intrMask", "eAtApsUpsrEvent", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "$(engineIds) ${intrMask} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtApsUpsrEngineCaptureEnable,
               "aps upsr engine alarm capture",
               "Enable/disable alarm caturing\n",
               DEF_SDKPARAM("engineIds", "EngineIdList", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${engineIds} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModuleApsUpsrEnginesInterruptGet,
               "show aps upsr engine interrupt",
               "Show UPSR APS engine interrupt\n",
               DEF_SDKPARAM("engineIds", "EngineIdList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${engineIds} ${readingMode} ${silent}")
    {
    mAtCliCall();
    }

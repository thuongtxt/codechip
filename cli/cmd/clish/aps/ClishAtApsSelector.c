/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : ClistAtApsSelector.c
 *
 * Created Date: Aug 15, 2016
 *
 * Description : APS Selecter CLISH
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
DEF_SDKCOMMAND(cliAtModuleApsSelectorCreate,
               "aps selector create",
               "Create APS Selector\n",
               DEF_SDKPARAM("selectorIds", "SelectorIdList", "")
               DEF_SDKPARAM("working", "SdhPathList", "")
               DEF_SDKPARAM("protection", "SdhPathList", "")
               DEF_SDKPARAM("outgoing", "SdhPathList", ""),
               "${selectorIds} ${working} ${protection} ${outgoing}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModuleApsSelectorDelete,
               "aps selector delete",
               "Delete APS Selector\n",
               DEF_SDKPARAM("selectorIds", "SelectorIdList", ""),
               "${selectorIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModuleApsSelectorShow,
               "show aps selector",
               "Show APS selector\n",
               DEF_SDKPARAM("selectorIds", "SelectorIdList", ""),
               "${selectorIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtApsSelectorEnable,
               "aps selector enable",
               "Enable APS Selector\n",
               DEF_SDKPARAM("selectorIds", "SelectorIdList", ""),
               "${selectorIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtApsSelectorDisable,
               "aps selector disable",
               "Disable APS Selector\n",
               DEF_SDKPARAM("selectorIds", "SelectorIdList", ""),
               "${selectorIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtApsSelectorChannel,
               "aps selector channel",
               "Selecte channel of APS Selector\n",
               DEF_SDKPARAM("selectorIds", "SelectorIdList", "")
               DEF_SDKPARAM("channel", "SdhPathList", ""),
               "${selectorIds} ${channel}")
    {
    mAtCliCall();
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : ClistAtModuleAps.c
 *
 * Created Date: Jun 10, 2013
 *
 * Description : APS CLISH
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/* Top commands */
TOP_SDKCOMMAND("aps", "APS CLIs")
TOP_SDKCOMMAND("aps linear", "APS Linear CLIs")
TOP_SDKCOMMAND("aps group", "APS Group CLIs")
TOP_SDKCOMMAND("aps group selector", "APS Group Selector CLIs")
TOP_SDKCOMMAND("aps selector", "APS Selector CLIs")
TOP_SDKCOMMAND("aps linear engine", "APS Linear engine CLIs")
TOP_SDKCOMMAND("aps interrupt", "APS Interrupt")
TOP_SDKCOMMAND("show aps", "Show APS CLIs ")
TOP_SDKCOMMAND("show aps linear", "Show APS Linear CLIs ")
TOP_SDKCOMMAND("debug aps", "Show APS debug information")
TOP_SDKCOMMAND("debug aps linear", "Show APS Linear information")

DEF_PTYPE("eAtApsLinearArch", "select", "1+1(0), 1:n(1)", "APS architecture")
DEF_PTYPE("eAtApsLinearOpMode", "select", "uni(0), bid(1)", "APS operation mode")
DEF_PTYPE("eAtApsSwitchType", "select", "revertive(0), non-revertive(1)", "APS switching type")
DEF_PTYPE("eAtApsLinearExtCmd", "select", "clear(0), exer(1), ms(2), fs(3), lp(4)", "APS Linear External commands")
DEF_PTYPE("eAtApsGroupState", "select", "working protection", "APS Group State")
DEF_PTYPE("WtrTime", "integer", "0..7680", "Wait-To-Restore value in seconds")
DEF_PTYPE("EngineIdList", "regexp", ".*", "List of engine IDs. For example: 1-3,6,8 gives list of 1,2,3,6,8")
DEF_PTYPE("ApsGroupIdList", "regexp", ".*", "List of Group IDs. For example: 1-3,6,8 gives list of 1,2,3,6,8")
DEF_PTYPE("SelectorIdList", "regexp", ".*", "List of Selector IDs. For example: 1-3,6,8 gives list of 1,2,3,6,8")
DEF_PTYPE("eAtApsSwitchCondition", "select", "none(0), sd(1), sf(2)", "APS switch condition")

DEF_SDKCOMMAND(cliAtModuleApsLinearEngineCreate,
               "aps linear engine create",
               "Create APS Linear engine\n",
               DEF_SDKPARAM("engineId", "UINT", "Engine ID")
               DEF_SDKPARAM("workingLines", "SdhLineList", "")
               DEF_SDKPARAM("protectLine", "SdhLineList", "")
               DEF_SDKPARAM("architecture", "eAtApsLinearArch", ""),
               "${engineId} ${protectLine} ${workingLines} ${architecture}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModuleApsLinearEngineDelete,
               "aps linear engine delete",
               "Delete APS Linear engine\n",
               DEF_SDKPARAM("engineIds", "EngineIdList", ""),
               "${engineIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModuleApsEnginesGet,
               "show aps linear engine",
               "Show Linear APS engines\n",
               DEF_SDKPARAM("engineIds", "EngineIdList", ""),
               "$(engineIds)")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtApsEnginesStart,
               "aps linear engine start",
               "Start APS engine\n",
               DEF_SDKPARAM("engineIds", "EngineIdList", ""),
               "$(engineIds)")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtApsEnginesStop,
               "aps linear engine stop",
               "Stop APS engine\n",
               DEF_SDKPARAM("engineIds", "EngineIdList", ""),
               "$(engineIds)")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtApsEngineDebug,
               "debug aps linear engine",
               "Show APS engine debug information\n",
               DEF_SDKPARAM("engineIds", "EngineIdList", ""),
               "$(engineIds)")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtApsEnginesSwitchingType,
               "aps linear engine switchtype",
               "Change switching type\n",
               DEF_SDKPARAM("engineIds", "EngineIdList", "")
               DEF_SDKPARAM("switchingType", "eAtApsSwitchType", ""),
               "$(engineIds) ${switchingType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtApsEnginesOperationMode,
               "aps linear engine operation",
               "Change operation mode\n",
               DEF_SDKPARAM("engineIds", "EngineIdList", "")
               DEF_SDKPARAM("operationMode", "eAtApsLinearOpMode", ""),
               "$(engineIds) ${operationMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtApsEnginesArchitectureMode,
               "aps linear engine architect",
               "Change architecture\n",
               DEF_SDKPARAM("engineIds", "EngineIdList", "")
               DEF_SDKPARAM("architect", "eAtApsLinearArch", ""),
               "$(engineIds) ${architect}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtApsEnginesExternalCommand,
               "aps linear engine extcmd",
               "Issue external command\n",
               DEF_SDKPARAM("engineIds", "EngineIdList", "")
               DEF_SDKPARAM("lineAffected", "SdhLineList", "")
               DEF_SDKPARAM("extCmd", "eAtApsLinearExtCmd", ""),
               "$(engineIds) ${lineAffected} ${extCmd}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModuleApsDebug,
               "debug module aps",
               "Show debug information of APS module\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliApsInterruptEnable,
               "aps interrupt enable",
               "Enable interrupt",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliApsInterruptDisable,
               "aps interrupt disable",
               "Disable interrupt",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModuleApsGet,
               "show module aps",
               "Show APS module information",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

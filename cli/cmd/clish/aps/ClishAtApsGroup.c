/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : ClishAtApsGroup.c
 *
 * Created Date: Aug 15, 2016
 *
 * Description : APS Group CLISH
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
DEF_SDKCOMMAND(cliAtModuleApsGroupCreate,
               "aps group create",
               "Create APS Group\n",
               DEF_SDKPARAM("groupIds", "GroupIdList", ""),
               "${groupIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModuleApsGroupDelete,
               "aps group delete",
               "Delete APS Group\n",
               DEF_SDKPARAM("groupIds", "GroupIdList", ""),
               "${groupIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtApsGroupSelectorAdd,
               "aps group selector add",
               "Add seletor to APS Group\n",
               DEF_SDKPARAM("groupId", "UINT", " Group ID")
               DEF_SDKPARAM("selectorIds", "SelectorIdList", ""),
               "${groupId} ${selectorIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtApsGroupSelectorRemove,
               "aps group selector remove",
               "Remove seletor from APS Group\n",
               DEF_SDKPARAM("groupId", "UINT", " Group ID")
               DEF_SDKPARAM("selectorIds", "SelectorIdList", ""),
               "${groupId} ${selectorIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtApsGroupStateSet,
               "aps group state",
               "Set state for APS Group\n",
               DEF_SDKPARAM("groupIds", "GroupIdList", "")
               DEF_SDKPARAM("state", "eAtApsGroupState", ""),
               "${groupIds} ${state}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtModuleApsGroupShow,
               "show aps group",
               "Show APS Group\n",
               DEF_SDKPARAM("groupIds", "GroupIdList", ""),
               "${groupIds}")
    {
    mAtCliCall();
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP
 *
 * File        : ClishAtModuleMap.c
 *
 * Created Date: Jun 6, 2014
 *
 * Description : MAP CLISH
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("vcg", "VCG commands")
TOP_SDKCOMMAND("vcg deskew", "VCG Deskew commands")
TOP_SDKCOMMAND("concate", "Module VCAT/LCAS commands")
TOP_SDKCOMMAND("concate interrupt", "Module VCAT/LCAS interrupt commands")

DEF_PTYPE("eAtConcateMemberType", "select",
          "vc4_64c(1), vc4_16c(2), vc4_4c(3), vc4_nc(4), vc4(5), vc3(6), vc12(7), vc11(8), ds1(9), e1(10), ds3(11), e3(12)",
          "Member type")

DEF_PTYPE("AtVcgMemberChannel", "regexp", ".*",
          "List of channels that can be provisioned to VCG\n"
          "- VC4-64c: vc4_64c.<lineId>.<aug64Id>\n"
          "- VC4-16c: vc4_16c.<lineId>.<aug16Id>\n"
          "- VC4-4c : vc4_4c.<lineId>.<aug4Id>\n"
          "- VC4-nc : vc4_nc.<lineId>.<aug4Id>\n"
          "- VC4    : vc4.<lineId>.<aug1Id>\n"
          "- VC3    : vc3.<lineId>.<aug1Id>.<au3Id/tug3Id>\n"
          "- VC-1x  : tu1x.<lineId>.<aug1Id>.<au3Tug3Id>.<tug2Id>.<vcxId>\n"
          "- DS3/E3 : de3.<grpId/coreId/busId>.<au3vc3Id>\n"
          "- DS1/E1 : de1.<grpId/coreId/busId>.<au3Tug3Id>.<tug2Id>.<vcxId>\n")
DEF_PTYPE("VcgList", "regexp", ".*", "List of VCG IDs. For example: 1-3,6,8 gives list of 1,2,3,6,8")
DEF_PTYPE("eAtConcateGroupType", "select", "ccat(0), vcat(1), nonvcat(2), nonvcat_g804(3), nonvcat_g8040(4)", "\n Concate types:")

DEF_SDKCOMMAND(cliVcgCreate,
               "vcg create",
               "Create new VCG.\n",
               DEF_SDKPARAM("vcgList", "VcgList", "")
               DEF_SDKPARAM("concatetype", "eAtConcateGroupType", "")
               DEF_SDKPARAM_OPTION("memberType", "eAtConcateMemberType", ""),
               "${vcgList} ${concatetype} ${memberType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgDelete,
               "vcg delete",
               "Delete VCGs.\n",
               DEF_SDKPARAM("vcgList", "VcgList", ""),
               "${vcgList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgDeskewThresholdSet,
               "vcg deskew threshold",
               "Set deskew threshold.\n",
               DEF_SDKPARAM_OPTION("memberType", "eAtConcateMemberType", "")
               DEF_SDKPARAM_OPTION("thresholdInRes", "UINT", " Value start from 1"),
               "${memberType} ${thresholdInRes} ")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgDeskewThresholdShow,
               "show vcg deskew",
               "Show all deskew thresholds.\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliModuleConcateInterruptEnable,
               "concate interrupt enable",
               "Enable VCAT/LCAS interrupt enable.\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliModuleConcateInterruptDisable,
               "concate interrupt disable",
               "Enable VCAT/LCAS interrupt disable.\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliModuleConcateShow,
               "show module concate",
               "Show module VCAT/LCAS.\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP
 *
 * File        : ClishAtVcgMember.c
 *
 * Created Date: Jun 6, 2014
 *
 * Description : VCG member CLISH
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/
#define MemberDefType "(crc|lom|gidm|sqm|mnd|sqnc|loa|oom|sink-state-change|source-state-change)"
#define MemberDefMask "((("MemberDefType"[\\|])+"MemberDefType")|"MemberDefType")"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("show vcg source", "CLIs to manage members at source")
TOP_SDKCOMMAND("show vcg sink", "CLIs to manage members at sink")
TOP_SDKCOMMAND("show vcg member", "CLIs to manage members")
TOP_SDKCOMMAND("vcg member alarm", "CLIs to control VCG member alarms")

DEF_PTYPE("MemberDefMask", "regexp", MemberDefMask,
          "\nVCAT/LCAS member defect masks:\n"
          "- crc    : CRC Error\n"
          "- lom    : Loss of Multiframe\n"
          "- gidm   : GID Mismatch\n"
          "- sqm    : Sequence Mismatch\n"
          "- mnd    : Member Not Deskewable\n"
          "- sqnc   : Sequence Non Consistent\n"
          "- loa    : Loss of Alignment\n"
          "- oom    : Out of Multiframe\n"
          "- sink-state-change : LCAS sink state change\n"
          "- source-state-change : LCAS source state change\n"
          "Note, these masks can be ORed, example crc|lom|sqm\n")

DEF_SDKCOMMAND(cliVcgSourceMemberShow,
               "show vcg source member",
               "Show member status at source\n",
               DEF_SDKPARAM("vcgList", "VcgList", "")
               DEF_SDKPARAM_OPTION("members", "AtVcgMemberChannel", ""),
               "${vcgList} $(members)")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgSinkMemberShow,
               "show vcg sink member",
               "Show member status at sink\n",
               DEF_SDKPARAM("vcgList", "VcgList", "")
               DEF_SDKPARAM_OPTION("members", "AtVcgMemberChannel", ""),
               "${vcgList} $(members)")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgMemberAlarmShow,
               "show vcg member alarm",
               "Show member alarms\n",
               DEF_SDKPARAM("vcgList", "VcgList", "")
               DEF_SDKPARAM_OPTION("members", "AtVcgMemberChannel", ""),
               "${vcgList} $(members)")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgMemberInterruptShow,
               "show vcg member interrupt",
               "Show member interrupt\n",
               DEF_SDKPARAM("vcgList", "VcgList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("members", "AtVcgMemberChannel", ""),
               "${vcgList} ${readingMode} $(members)")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgMemberInterruptMask,
               "vcg member interruptmask",
               "Set member interrupt mask\n",
               DEF_SDKPARAM("vcgList", "VcgList", "")
               DEF_SDKPARAM("intrMask", "MemberDefMask", "")
               DEF_SDKPARAM("intrMaskEn", "eBool", "")
               DEF_SDKPARAM_OPTION("members", "AtVcgMemberChannel", ""),
               "${vcgList} ${intrMask} ${intrMaskEn} $(members)")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgMemberAlarmCapture,
               "vcg member alarm capture",
               "Enable/disable member alarm capture\n",
               DEF_SDKPARAM("vcgList", "VcgList", "")
               DEF_SDKPARAM("captureEn", "eBool", "")
               DEF_SDKPARAM_OPTION("members", "AtVcgMemberChannel", ""),
               "${vcgList} ${captureEn} $(members)")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgMemberAlarmCaptureShow,
               "show vcg member alarm capture",
               "Show member captured alarms\n",
               DEF_SDKPARAM("vcgList", "VcgList", "")
               DEF_SDKPARAM_OPTION("members", "AtVcgMemberChannel", "")
               DEF_SDKPARAM_OPTION("silent", "STRING", ""),
               "${vcgList} $(members) $(silent)")
    {
    mAtCliCall();
    }



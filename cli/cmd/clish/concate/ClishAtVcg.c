/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP
 *
 * File        : ClishAtVcg.c
 *
 * Created Date: Jun 6, 2014
 *
 * Description : VCG CLISH
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("vcg vcat", "CLIs to control VCAT VCG")
TOP_SDKCOMMAND("vcg vcat source", "CLIs to manage VCAT CLIs at Source side")
TOP_SDKCOMMAND("vcg vcat sink", "CLIs to manage VCAT CLIs at Source side")
TOP_SDKCOMMAND("vcg vcat source member", "CLIs to manage VCAT members at Source side")
TOP_SDKCOMMAND("vcg vcat sink member", "CLIs to manage VCAT members at Sink side")

TOP_SDKCOMMAND("vcg lcas", "CLIs to control LCAS VCG")
TOP_SDKCOMMAND("vcg lcas timer", "CLIs to control LCAS timer")
TOP_SDKCOMMAND("vcg lcas source", "CLIs to manage VCAT CLIs at Source side")
TOP_SDKCOMMAND("vcg lcas sink", "CLIs to manage VCAT CLIs at Source side")
TOP_SDKCOMMAND("vcg lcas source member", "CLIs to manage VCAT members at Source side")
TOP_SDKCOMMAND("vcg lcas sink member", "CLIs to manage VCAT members at Sink side")

TOP_SDKCOMMAND("vcg member", "CLIs to control VCG members")
TOP_SDKCOMMAND("vcg source", "CLIs to control LCAS Source side")
TOP_SDKCOMMAND("vcg source member", "CLIs to manage LCAS members at Source side")
TOP_SDKCOMMAND("vcg sink", "CLIs to control LCAS Sink side")
TOP_SDKCOMMAND("vcg sink member", "CLIs to manage LCAS members at Sink side")

DEF_SDKCOMMAND(cliVcgVcatLcasEnable,
               "vcg lcas enable",
               "Enable LCAS\n",
               DEF_SDKPARAM("vcgList", "VcgList", ""),
               "${vcgList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgVcatLcasDisable,
               "vcg lcas disable",
               "Disable LCAS\n",
               DEF_SDKPARAM("vcgList", "VcgList", ""),
               "${vcgList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgVcatLcasHoldOffTimerSet,
               "vcg lcas timer holdoff",
               "Set LCAS hold off timer\n",
               DEF_SDKPARAM("vcgList", "VcgList", "")
               DEF_SDKPARAM("timer", "STRING", "Hold-off timer"),
               "${vcgList} ${timer}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgLcasWtrSet,
               "vcg lcas timer wtr",
               "Set WTR timer of LCAS for the VCG\n",
               DEF_SDKPARAM("vcgList", "VcgList", "")
               DEF_SDKPARAM("timer", "STRING", "WTR timer"),
               "${vcgList} ${timer}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgLcasRmvSet,
               "vcg lcas timer rmv",
               "Set RMV timer of LCAS for the VCG\n",
               DEF_SDKPARAM("vcgList", "VcgList", "")
               DEF_SDKPARAM("timer", "STRING", "RMV timer"),
               "${vcgList} ${timer}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgLcasMemberSourceAdd,
               "vcg lcas source member add",
               "Add a list members to LCAS VCG at Source side\n",
               DEF_SDKPARAM("vcgList", "VcgList", "")
               DEF_SDKPARAM("members", "AtVcgMemberChannel", ""),
               "${vcgList} ${members}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgLcasMemberSinkAdd,
               "vcg lcas sink member add",
               "Add a list members to LCAS VCG at Sink side\n",
               DEF_SDKPARAM("vcgList", "VcgList", "")
               DEF_SDKPARAM("members", "AtVcgMemberChannel", ""),
               "${vcgList} ${members}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgLcasSourceMemberRemove,
               "vcg lcas source member remove",
               "Remove a list members of  a VCG at Source side\n",
               DEF_SDKPARAM("vcgList", "VcgList", "")
               DEF_SDKPARAM("members", "AtVcgMemberChannel", ""),
               "${vcgList} ${members}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgLcasSinkMemberRemove,
               "vcg lcas sink member remove",
               "Remove a list members of  a VCG at Sink side\n",
               DEF_SDKPARAM("vcgList", "VcgList", "")
               DEF_SDKPARAM("members", "AtVcgMemberChannel", ""),
               "${vcgList} ${members}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgShow,
               "show vcg",
               "Show VCG information\n",
               DEF_SDKPARAM("vcgList", "VcgList", ""),
               "${vcgList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgAlarmShow,
               "show vcg alarm",
               "Show VCG alarms\n",
               DEF_SDKPARAM("vcgList", "VcgList", ""),
               "${vcgList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgInterruptShow,
               "show vcg interrupt",
               "Show VCG interrupt\n",
               DEF_SDKPARAM("vcgList", "VcgList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", ""),
               "${vcgList} ${readingMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgSourceMemberProvision,
               "vcg source member provision",
               "Provision source members to VCG\n",
               DEF_SDKPARAM("vcgList", "VcgList", "")
               DEF_SDKPARAM("members", "AtVcgMemberChannel", ""),
               "${vcgList} ${members}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgSinkMemberProvision,
               "vcg sink member provision",
               "Provision sink members to VCG\n",
               DEF_SDKPARAM("vcgList", "VcgList", "")
               DEF_SDKPARAM("members", "AtVcgMemberChannel", ""),
               "${vcgList} ${members}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgSourceMemberDeProvision,
               "vcg source member deprovision",
               "Deprovision source members out of VCG\n",
               DEF_SDKPARAM("vcgList", "VcgList", "")
               DEF_SDKPARAM("members", "AtVcgMemberChannel", ""),
               "${vcgList} ${members}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgSinkMemberDeProvision,
               "vcg sink member deprovision",
               "Deprovision sink members out of VCG\n",
               DEF_SDKPARAM("vcgList", "VcgList", "")
               DEF_SDKPARAM("members", "AtVcgMemberChannel", ""),
               "${vcgList} ${members}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgVcatMemberSourceSequenceSet,
               "vcg vcat source member sequence",
               "Set sequence value for members of a VCG at Source side\n",
               DEF_SDKPARAM("vcgList", "VcgList", "")
               DEF_SDKPARAM("members", "AtVcgMemberChannel", "")
               DEF_SDKPARAM("sequence", "STRING", ""),
               "${vcgList} ${members} $(sequence)")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgVcatMemberSinkSequenceSet,
               "vcg vcat sink member expsequence",
               "Set sequence value for members of a VCG at Sink side\n",
               DEF_SDKPARAM("vcgList", "VcgList", "")
               DEF_SDKPARAM("members", "AtVcgMemberChannel", "")
               DEF_SDKPARAM("sequence", "STRING", ""),
               "${vcgList} ${members} $(sequence)")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgMemberProvision,
               "vcg member provision",
               "Provision members to VCG\n",
               DEF_SDKPARAM("vcgList", "VcgList", "")
               DEF_SDKPARAM("members", "AtVcgMemberChannel", ""),
               "${vcgList} ${members}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgMemberDeProvision,
               "vcg member deprovision",
               "Deprovision members out of VCG\n",
               DEF_SDKPARAM("vcgList", "VcgList", "")
               DEF_SDKPARAM("members", "AtVcgMemberChannel", ""),
               "${vcgList} ${members}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliVcgEncapTypeSet,
               "vcg encap",
               "Set encapsulation type\n",
               DEF_SDKPARAM("vcgList", "VcgList", "")
               DEF_SDKPARAM("encapType", "eAtEncapType", ""),
               "${vcgList} ${encapType}")
    {
    mAtCliCall();
    }

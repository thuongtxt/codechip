/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : ClishAtInterruptPin.c
 *
 * Created Date: Oct 1, 2016
 *
 * Description : Interrupt PIN CLISH CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
DEF_PTYPE("eAtTriggerMode", "select", "edge(0), level-high(1), level-low(2)", "Trigger mode")
DEF_PTYPE("AtInterruptPin", "regexp", ".*", "List of PIN IDs. Example: 1,2")

TOP_SDKCOMMAND("device interrupt pin", "CLIS to handle interrupt PINs")
TOP_SDKCOMMAND("device interrupt pin trigger", "CLIS to handle interrupt PINs triggering")

DEF_SDKCOMMAND(cliAtInterruptPinTriggerModeSet,
               "device interrupt pin trigger mode",
               "Set trigger mode",
               DEF_SDKPARAM("pinIds", "AtInterruptPin", "")
               DEF_SDKPARAM("triggerMode", "eAtTriggerMode", ""),
               "${pinIds} ${triggerMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtInterruptPinTriggerStart,
               "device interrupt pin trigger start",
               "Start triggering interrupt PINs",
               DEF_SDKPARAM("pinIds", "AtInterruptPin", ""),
               "${pinIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtInterruptPinTriggerStop,
               "device interrupt pin trigger stop",
               "Stop triggering interrupt PINs",
               DEF_SDKPARAM("pinIds", "AtInterruptPin", ""),
               "${pinIds}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtInterruptPinShow,
               "show device interrupt pin",
               "Show interrupt PIN information",
               DEF_SDKPARAM("pinIds", "AtInterruptPin", ""),
               "${pinIds}")
    {
    mAtCliCall();
    }

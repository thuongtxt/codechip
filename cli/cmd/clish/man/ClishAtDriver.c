/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : ClishAtDriver.c
 *
 * Created Date: Dec 27, 2012
 *
 * Description : Driver CLI
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"
#include "AtDriver.h"
#include "AtHalTcp.h"
#include "AtTokenizer.h"
#include "AtIdParser.h"
#include "AtList.h"
#include "AtCli.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/
extern const char* const SDK_BUILD_DATE;
extern char* const SDK_BUILD_NAME;
extern char* const SDK_SRC_COMMIT;

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
DEF_PTYPE("eAtDriverRole", "select", "active(0), standby(1)", "Driver role")
DEF_PTYPE("NetworkAddress", "regexp", ".*", "Network address with format <ipAddress:port>")
DEF_PTYPE("AtLogLevel", "select", "critical(0), warning(1), info(2), normal(3)", "Log level")
DEF_PTYPE("eAtLoggerFlushOption", "select", "silence(0)", "Flush option")

#define AtLogLevelType "(critical|warning|info|normal|debug)"
#define AtLogLevelMask "((("AtLogLevelType"[\\|])+"AtLogLevelType")|"AtLogLevelType")"

DEF_PTYPE("eAtLogLevel", "regexp", AtLogLevelMask,
          "Log level:\n"
          "- critical : Critical level\n"
          "- warning  : Warning level\n"
          "- info     : Information level\n"
          "- normal   : Normal level\n"
          "- debug    : Debug level\n"
          "Note, these masks can be ORed, example critical|warning|info|normal|debug\n")

TOP_SDKCOMMAND("driver", "Driver CLIs")
TOP_SDKCOMMAND("logger", "Logger CLIs")
TOP_SDKCOMMAND("logger level", "Logger CLIs")
TOP_SDKCOMMAND("logger verbose", "Enable/disable verbose mode")
TOP_SDKCOMMAND("show module", "CLIs to show module information")
TOP_SDKCOMMAND("driver debug", "CLIs to control driver debugging")
TOP_SDKCOMMAND("logger listen", "CLIs to handle driver logger events")
TOP_SDKCOMMAND("logger api_log", "CLIs to control API Logging")
TOP_SDKCOMMAND("logger api_log_all", "CLIs to control API Logging")
TOP_SDKCOMMAND("logger api_log_multidev_prefix", "CLIs to control mdev_x_/sdev_x.y_ prefix in AtObjectToString and API Logging")

DEF_SDKCOMMAND(cliAtDriverShow,
               "show driver",
               "Show driver information",
               DEF_NULLPARAM,
               "")
    {
    AtPrintc(cSevInfo, "SDK Built On    : %s\r\n", SDK_BUILD_DATE);
    AtPrintc(cSevInfo, "                : %s\r\n", SDK_BUILD_NAME);
    AtPrintc(cSevInfo, "Source REV      : %s\r\n", SDK_SRC_COMMIT);
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDriverLoggerVerboseEnable,
               "logger verbose enable",
               "Every log action will print to console",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDriverLoggerVerboseDisable,
               "logger verbose disable",
               "Every log action will not print to console",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDriverLoggerShow,
               "show logger",
               "Show all of logged messages",
               DEF_SDKPARAM_OPTION("level", "AtLogLevel", ""),
               "${level}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDriverLoggerFlush,
               "logger flush",
               "Flush all of logged messages",
               DEF_SDKPARAM_OPTION("option", "eAtLoggerFlushOption", "Flush option"),
               "${option}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDriverLoggerTest,
               "logger test",
               "test logger",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDriverLoggerEnable,
               "logger enable",
               "Enable logging",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDriverLoggerDisable,
               "logger disable",
               "Disable logging",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDriverDeviceCreate,
               "device create",
               "Create a remote device",
               DEF_SDKPARAM("ipAddress", "STRING", "Remote device: <ipAddress>[:port]")
               DEF_SDKPARAM_OPTION("productCode", "STRING", "Product code in Hex format"),
               "${ipAddress} ${productCode}")
    {
    char *addressString = AtStrdup(lub_argv__get_arg(argv, 0));
    AtTokenizer tokenizer = AtTokenizerSharedTokenizer(addressString, ":");
    char ipAddress[32];
    uint16 port = cAtHalTcpDefaultPort;
    uint32 productCode = 0;
    AtHal hal;
    AtDevice newDevice = NULL;
    uint8 numAddedDevices = 0;
    AtUnused(context);

    /* Parse remote device address */
    if (AtTokenizerNumStrings(tokenizer) == 0)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid address, expect: <ip>[:<port>]\r\n");
        return -1;
        }

    /* Have the IP address */
    AtSprintf(ipAddress, "%s", AtTokenizerNextString(tokenizer));

    /* And user also specify the port */
    if (AtTokenizerHasNextString(tokenizer))
        port = (uint8)AtStrToDw(AtTokenizerNextString(tokenizer));

    /* User also specify the product code */
    if (lub_argv__get_count(argv) > 1)
        StrToDw(lub_argv__get_arg(argv, 1), 'h', &productCode);

    /* Create HAL */
    hal = AtHalTcpNew(ipAddress, port);
    if (hal == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Create HAL fail\r\n");
        AtOsalMemFree(addressString);
        return -1;
        }

    /* Read product code */
    if (productCode == 0)
        productCode = AtProductCodeGetByHal(hal);
    if (!AtDriverProductCodeIsValid(AtDriverSharedDriverGet(), productCode))
        {
        AtPrintc(cSevCritical, "ERROR: Product code 0x%08x is invalid\r\n", productCode);
        AtOsalMemFree(addressString);
        return -1;
        }

    /* Check if there is still enough space */
    AtDriverAddedDevicesGet(AtDriverSharedDriverGet(), &numAddedDevices);
    if (numAddedDevices == AtDriverMaxNumberOfDevicesGet(AtDriverSharedDriverGet()))
        {
        AtPrintc(cSevCritical,
                 "ERROR: just can create %d devices\r\n",
                 AtDriverMaxNumberOfDevicesGet(AtDriverSharedDriverGet()));
        AtOsalMemFree(addressString);
        return -1;
        }

    /* Create device */
    newDevice = AtDriverDeviceCreate(AtDriverSharedDriverGet(), productCode);
    if (newDevice == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Create device fail\r\n");
        AtOsalMemFree(addressString);
        return -1;
        }

    /* Setup */
    AtDeviceIpCoreHalSet(newDevice, 0, hal);
    AtOsalMemFree(addressString);

    return 0;
    }

DEF_SDKCOMMAND(cliAtDriverDeviceDelete,
               "device delete",
               "Delete device",
               DEF_SDKPARAM("deviceIndexes", "STRING", "List of device indexes"),
               "${deviceIndexes}")
    {
    char *idString = AtStrdup(lub_argv__get_arg(argv, 0));
    AtIdParser parser = AtIdParserNew(idString, NULL, NULL);
    eBool success = cAtTrue;
    AtList devices;
    AtUnused(context);

    if (parser == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Invalid ID format. Expect a list of IDs, for example: 1-3,4,6-9\r\n");
        AtOsalMemFree(idString);
        return cAtFalse;
        }

    devices = AtListCreate(0);
    while (AtIdParserHasNextNumber(parser))
        {
        uint32 deviceIndex = AtIdParserNextNumber(parser);
        AtDevice device = AtDriverDeviceGet(AtDriverSharedDriverGet(), (uint8)(deviceIndex - 1));

        if (device == NULL)
            {
            AtPrintc(cSevWarning, "WARNING: No such device at index %d\r\n", deviceIndex);
            continue;
            }

        if (device == CliDevice())
            {
            AtPrintc(cSevWarning, "Device at index %d is being selected, it is ignored\r\n", deviceIndex);
            continue;
            }

        AtListObjectAdd(devices, (AtObject)device);
        }

    while (AtListLengthGet(devices))
        {
        AtDevice device = (AtDevice)AtListObjectRemoveAtIndex(devices, 0);
        AtHal hal = AtDeviceIpCoreHalGet(device, 0);
        AtDriverDeviceDelete(AtDriverSharedDriverGet(), device);
        AtHalDelete(hal);
        }

    AtObjectDelete((AtObject)parser);
    AtObjectDelete((AtObject)devices);
    AtOsalMemFree(idString);

    return success ? 0 : -1;
    }

DEF_SDKCOMMAND(cliAtDriverSerialize,
               "serialize",
               "Serialize database model",
               DEF_SDKPARAM_OPTION("outputFile", "STRING", "Optional output file. If this is not specified, the stdout will be used to output serialized data"),
               "${outputFile}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDriverRoleSet,
               "driver role",
               "Set driver role",
               DEF_SDKPARAM("role", "eAtDriverRole", ""),
               "${role}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDriverSynchronize,
               "driver synchronize",
               "Synchronize the local driver with the remote one",
               DEF_SDKPARAM_OPTION("remoteAddress", "NetworkAddress", ""),
               "${remoteAddress}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDriverLoggerLevelEnable,
               "logger level enable",
               "Enable logging level",
               DEF_SDKPARAM("level", "eAtLogLevel", ""),
               "${level}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDriverLoggerLevelDisable,
               "logger level disable",
               "Disable logging level",
               DEF_SDKPARAM("level", "eAtLogLevel", ""),
               "${level}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDriverDebug,
               "debug driver",
               "Show driver debug information",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDriverDebugEnable,
               "driver debug enable",
               "Enable debugging",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDriverDebugDisable,
               "driver debug disable",
               "Disable debugging",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDriverRestore,
               "driver restore",
               "Restore driver database",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDriverLoggerExport,
               "logger export",
               "Export all of messages to file",
               DEF_SDKPARAM("file", "STRING", "File to export"),
               "${file}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDriverLoggerListenStart,
               "logger listen start",
               "Start listening on logger events",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDriverLoggerListenStop,
               "logger listen stop",
               "Stop listening on logger events",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDriverApiLogEnable,
               "logger api_log enable",
               "Enable API logging.",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDriverApiLogDisable,
               "logger api_log disable",
               "Disable API logging.",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDriverAllApiLogEnable,
               "logger api_log_all enable",
               "Enable all API logging.",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDriverAllApiLogDisable,
               "logger api_log_all disable",
               "Disable logger for the most frequently API calls.",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDriverMultiDevPrefixApiLogEnable,
               "logger api_log_multidev_prefix enable",
               "Enable mdev_x_/sdev_x.y_ prefix in AtObjectToString/API logging for multi-device/sub-device driver.",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDriverMultiDevPrefixApiLogDisable,
               "logger api_log_multidev_prefix disable",
               "Disable mdev_x_/sdev_x.y_ prefix in AtObjectToString/API logging for multi-device/sub-device driver.",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : ClishAtDevice.c
 *
 * Created Date: Nov 15, 2012
 *
 * Description : CLISH Device CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define ThermalAlmType "(overheat)"
#define ThermalAlmMask "((("ThermalAlmType"[\\|])+"ThermalAlmType")|"ThermalAlmType")"

#define SemAlmType "(active|idle|initialization|correction|injection|classification|observation|fatal-error|essential|correctable|uncorrectable|heartbeat|heartbeat1|heartbeat2|activeslr1|activeslr2)"
#define SemAlmMask "((("SemAlmType"[\\|])+"SemAlmType")|"SemAlmType")"

#define DevAlmType "(protection)"
#define DevAlmMask "((("DevAlmType"[\\|])+"DevAlmType")|"DevAlmType")"
/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
extern void AppInterruptTaskEnable(eBool enable);
extern eBool AtDeviceIsSimulated(AtDevice self);
extern AtDevice CliDevice(void);

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("device", "Device CLIs")
TOP_SDKCOMMAND("device sem", "Device SEM CLIs")
TOP_SDKCOMMAND("device sem interrupt", "Device SEM interrupt CLIs")
TOP_SDKCOMMAND("device sem notification", "Device SEM notification CLIs")
TOP_SDKCOMMAND("device sem uart force", "Device SEM force error CLIs")
TOP_SDKCOMMAND("device sem uart async", "Device SEM async force error CLIs")
TOP_SDKCOMMAND("device sem uart async force", "Device SEM force error CLIs")
TOP_SDKCOMMAND("device sysmon", "Device sensor/sysmon(thermal+voltage) interrupt CLIs")
TOP_SDKCOMMAND("device sysmon interrupt", "Device sensor/sysmon(thermal+voltage) interrupt CLIs")
TOP_SDKCOMMAND("device sensor", "Device sensor CLIs")
TOP_SDKCOMMAND("device sensor thermal", "Device sensor thermal CLIs")
TOP_SDKCOMMAND("device sensor thermal notification", "Device sensor thermal interrupt notification CLIs")
TOP_SDKCOMMAND("device sensor thermal threshold", "Device sensor thermal threshold CLIs")
TOP_SDKCOMMAND("device sensor power_supply", "Device sensor power supply  CLIs")
TOP_SDKCOMMAND("device sensor power_supply threshold", "Device sensor power supply threshold CLIs")
TOP_SDKCOMMAND("device show", "CLIs to show device information")
TOP_SDKCOMMAND("device services", "CLIs to handle device services")
TOP_SDKCOMMAND("device interrupt", "CLIs to handle device interrupt")
TOP_SDKCOMMAND("device interrupt log", "CLIs to handle device interrupt logging")
TOP_SDKCOMMAND("debug", "Show debug information")
TOP_SDKCOMMAND("debug device sem", "Show debug device SEM information")
TOP_SDKCOMMAND("show channel", "Show channel information")
TOP_SDKCOMMAND("show device", "Show device information")
TOP_SDKCOMMAND("show device sensor", "Show device sensor CLIs")
TOP_SDKCOMMAND("device clear", "CLIs to clear status and other information of device")
TOP_SDKCOMMAND("device warm", "CLIs to control warm restore")
TOP_SDKCOMMAND("device sskey", "CLIs to control SSKey")
TOP_SDKCOMMAND("lock", "Lock")
TOP_SDKCOMMAND("lock module", "Lock module")
TOP_SDKCOMMAND("unlock", "Unlock")
TOP_SDKCOMMAND("unlock module", "Unlock module")
TOP_SDKCOMMAND("device log", "CLIs to control device logging")
TOP_SDKCOMMAND("device listenedalarm", "CLIs to control listened alarm")
TOP_SDKCOMMAND("device diag", "CLIs for diagnose")
TOP_SDKCOMMAND("device query", "CLIs to query device information")
TOP_SDKCOMMAND("device diag uart", "CLIs to control UART")
TOP_SDKCOMMAND("device alarm", "CLIs for alarm capture")

DEF_PTYPE("ThermalAlmMask", "regexp", ThermalAlmMask,
          "\nThermal sensor alarm masks:\n"
          "- overheat  : The current temperature is greater than the set threshold.\n"
          "Note, these masks can be ORed\n")

DEF_PTYPE("SemAlmMask", "regexp", SemAlmMask,
          "\nSEM alarm masks:\n"
          "- active\n"
          "- idle\n"
          "- initialization\n"
          "- observation\n"
          "- injection\n"
          "- classification\n"
          "- correction\n"
          "- fatal-error\n"
          "Note, these masks can be ORed\n")

DEF_PTYPE("eAtDeviceAsyncInitMode", "select", "block(1)", "Device async init blocking")
DEF_PTYPE("eAtDeviceRole", "select", "active(0), standby(1), auto(2)", "Device role")
DEF_PTYPE("eAtPowerSupplyVoltage", "select", "int(0), aux(1), bram(2), psintlp(3), psintfp(4), psaux(5), user0(6), user1(7), user2(8), user3(9)", "Voltage types")

DEF_PTYPE("DevAlmMask", "regexp", DevAlmMask,
          "\nDevice alarm masks:\n"
          "- protection  : Active profile(sub-device) in the protection device switch done.\n"
          "Note, these masks can be ORed\n")

static void InterruptTaskEnable(eBool enable)
    {
    /* Enable task on real platform only.
     * Simulation mode should use "device interrupt process" manually. */
    if (!AtDeviceIsSimulated(CliDevice()))
        AppInterruptTaskEnable(enable);
    }

/* Initialize device */
DEF_SDKCOMMAND(cliAtDeviceInit,
               "device init",
               "Initialize device",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceAsyncInit,
               "device init async",
               "Initialize device in async way. This has to be called many times until complete/failure indication is returned",
               DEF_SDKPARAM_OPTION("block", "eAtDeviceAsyncInitMode", "If \"block\" is not input, this CLI just run once. If it is input, it will retry till done within 5 minutes"),
               "${block}")
    {
    mAtCliCall();
    }

/* Control showing read write register access */
DEF_SDKCOMMAND(cliAtDeviceShowReadWrite,
               "device show readwrite",
               "Enable/disable displaying read/write detail",
               DEF_SDKPARAM("showRead", "eBool", "Show reading detail")
               DEF_SDKPARAM("showWrite", "eBool", "Show writing detail"),
               "${showRead} ${showWrite}")
    {
    mAtCliCall();
    }

/* Select device */
DEF_SDKCOMMAND(cliAtDeviceSelected,
               "device select",
               "Select device that CLI module works on",
               DEF_SDKPARAM("deviceId", "STRING", "Device ID, starts from 1. \"none\" if no device is selected"),
               "${deviceId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceAllServicesDestroy,
               "device services destroy",
               "Destroy all of resources:\n"
               "  - PPP bundles\n"
               "  - All links\n"
               "  - All flows\n"
               "  - All PWs"
               "  - All PRBS engines"
               "  - Clear all forced alarms",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceInterruptProcess,
               "device interrupt process",
               "Manual interrupt process",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceInterruptEnable,
               "device interrupt enable",
               "Enable interrupt",
               DEF_NULLPARAM,
               "")
    {
    int ret = mAtCliResult();
    AtUnused(context);
    AtUnused(argv);
    InterruptTaskEnable(cAtTrue);
    return ret;
    }

DEF_SDKCOMMAND(cliAtDeviceInterruptDisable,
               "device interrupt disable",
               "Disable interrupt",
               DEF_NULLPARAM,
               "")
    {
    int ret = mAtCliResult();
    AtUnused(context);
    AtUnused(argv);
    InterruptTaskEnable(cAtFalse);
    return ret;
    }

DEF_SDKCOMMAND(cliAtDeviceSemInterruptEnable,
               "device sem interrupt enable",
               "Enable sem interrupt",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceSemInterruptDisable,
               "device sem interrupt disable",
               "Disable sem interrupt",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceSysmonInterruptEnable,
               "device sysmon interrupt enable",
               "Enable sysmon(thermal+voltage) interrupt",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceSysmonInterruptDisable,
               "device sysmon interrupt disable",
               "Disable sysmon(thermal+voltage) interrupt",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceMemoryTest,
               "device memorytest",
               "Test memory of device\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceShowModules,
               "show device modules",
               "Show supported modules\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceDebug,
               "debug device",
               "Show device information\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceStatusClear,
               "device clear status",
               "Clear status of all modules and channels\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceWarmRestoreStart,
               "device warm start",
               "Start warm restore process\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceWarmRestoreStop,
               "device warm stop",
               "Stop warm restore process\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceWarmRestore,
               "device warm restore",
               "Warm restore device's configuration\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(clisAtDeviceSSKeyCheck,
               "device sskey check",
               "To check if SSKey is working\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceDiagnosticModeEnable,
               "device diag enable",
               "Force the device work in diagnostic mode (full FPGA core is not available)\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceDiagnosticModeDisable,
               "device diag disable",
               "Make device work in normal mode with full FPGA core.\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceDiagnosticShow,
               "show device diag",
               "Show device diagnostic information\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceDiagnosticCheckEnable,
               "device diag check enable",
               "Enable diagnostic checking when initializing device.\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceDiagnosticCheckDisabled,
               "device diag check disable",
               "Disable diagnostic checking when initializing device\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceInterruptRestore,
               "device interrupt restore",
               "Restore interrupt signal from current channel status.",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceReadWriteLog,
               "device log readwrite",
               "Log read/write operations",
               DEF_SDKPARAM_OPTION("readLog", "eBool", "Enable/disable read log")
               DEF_SDKPARAM_OPTION("writeLog", "eBool", "Enable/disable write log")
               DEF_SDKPARAM_OPTION("logFile", "STRING", "Logged file. This is optional, if it is not input, logged messages will be displayed directly on the console"),
               "${readLog} ${writeLog} ${logFile}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceReadWriteLogIgnoreRange,
               "device log readwrite ignore",
               "Ignore read/write log on registers of specified range.",
               DEF_SDKPARAM("range", "STRING", "Range of registers to be ignored. Start and stop are separated by '-'. Example: 200000-2BFFFF"),
               "${range}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceReset,
               "device reset",
               "Reset device",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceAsyncReset,
               "device reset async",
               "Reset device in async way. This has to be called many times until complete/failure indication is returned",
               DEF_SDKPARAM_OPTION("block", "eAtDeviceAsyncInitMode", "If \"block\" is not input, this CLI just run once. If it is input, it will retry till done within 5 minutes"),
               "${block}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDeviceSemValidate,
               "device sem validate",
               "Validate Soft Error Mitigation function",
               DEF_SDKPARAM("semId", "STRING", "SEM controller ID"),
               "${semId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDeviceSemShow,
               "show device sem",
               "Show configuration of Soft Error Mitigation",
               DEF_SDKPARAM("semId", "STRING", "SEM controller ID"),
               "${semId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDeviceSemInfoShow,
               "show device sem info",
               "Show fpga device name, frame size, max frame, number of SLR and EBD file paths of Soft Error Mitigation",
               DEF_SDKPARAM("semId", "STRING", "SEM controller ID"),
               "${semId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDeviceSemAlarmShow,
               "show device sem alarm",
               "Show alarm of Soft Error Mitigation",
               DEF_SDKPARAM("semId", "STRING", "SEM controller ID"),
               "${semId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDeviceSemInterruptShow,
               "show device sem interrupt",
               "Show interrupt of Soft Error Mitigation",
               DEF_SDKPARAM("semId", "STRING", "SEM controller ID")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", ""),
               "${semId} ${readingMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDeviceSemCountersShow,
               "show device sem counters",
               "Show counters of Soft Error Mitigation",
               DEF_SDKPARAM("semId", "STRING", "SEM controller ID")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", ""),
               "${semId} ${readingMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDeviceSemInterruptMask,
               "device sem interruptmask",
               "Set SEM interrupt mask.",
               DEF_SDKPARAM("semId", "STRING", "SEM controller ID")
               DEF_SDKPARAM("mask", "SemAlmMask", "")
               DEF_SDKPARAM("intrMaskEn", "eBool", ""),
               "${semId} ${mask} ${intrMaskEn}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDeviceSemNotificationEnable,
               "device sem notification enable",
               "Enable SEM interrupt notification",
               DEF_SDKPARAM("semId", "STRING", "SEM controller ID"),
               "${semId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDeviceSemNotificationDisable,
               "device sem notification disable",
               "Disable SEM interrupt notification",
               DEF_SDKPARAM("semId", "STRING", "SEM controller ID"),
               "${semId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliCmdAtDeviceSemUartCommand,
               "device sem uart",
               "Send UART command to SEM controller and receive data if any",
               DEF_SDKPARAM("semId", "STRING", "SEM controller ID")
               DEF_SDKPARAM("commandString", "STRING", "UART command")
               DEF_SDKPARAM_OPTION("option1", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option2", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option3", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option4", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option5", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option6", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option7", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option8", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option9", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option10", "STRING", "Option"),
               "${semId} ${commandString} ${option1} ${option2} ${option3} ${option4} ${option5} ${option6} ${option7} ${option8} ${option9} ${option10}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDeviceSemUartCommandSend,
               "device sem uart send",
               "Send UART command to SEM controller ",
               DEF_SDKPARAM("semId", "STRING", "SEM controller ID")
               DEF_SDKPARAM("commandString", "STRING", "UART command")
               DEF_SDKPARAM_OPTION("option1", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option2", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option3", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option4", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option5", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option6", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option7", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option8", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option9", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option10", "STRING", "Option"),
               "${semId} ${commandString} ${option1} ${option2} ${option3} ${option4} ${option5} ${option6} ${option7} ${option8} ${option9} ${option10}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDeviceSemUartCommandReceive,
               "device sem uart receive",
               "Receive data as UART interface for SEM controller  ",
               DEF_SDKPARAM("semId", "STRING", "SEM controller ID"),
               "${semId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClilAtDeviceSemUartValidate,
               "device sem uart validate",
               "Validate Soft Error Mitigation function via UART interface",
               DEF_SDKPARAM("semId", "STRING", "SEM controller ID"),
               "${semId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClilAtDeviceSemUartForceError,
               "device sem uart force error",
               "Force correctable/uncorrectable Soft Error Mitigation function via UART interface",
               DEF_SDKPARAM("semId", "STRING", "SEM controller ID")
               DEF_SDKPARAM("errorType", "STRING", "Error type: correctable or uncorrectable"),
               "${semId} ${errorType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClilAtDeviceSemUartAsyncForceError,
               "device sem uart async force error",
               "Async Force correctable/uncorrectable Soft Error Mitigation function via UART interface",
               DEF_SDKPARAM("semId", "STRING", "SEM controller ID")
               DEF_SDKPARAM("errorType", "STRING", "Error type: correctable or uncorrectable"),
               "${semId} ${errorType}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(ClilAtDeviceSemUartEbdFilePathSet,
               "device sem uart ebdfile",
               "Set path to the EBD file for lookup essential error",
               DEF_SDKPARAM("semid", "UINT", "SEM ID")
               DEF_SDKPARAM("filepath", "STRING", "path to the ebd file")
               DEF_SDKPARAM_OPTION("option1", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option2", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option3", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option4", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option5", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option6", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option7", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option8", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option9", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option10", "STRING", "Option"),
               "${semid} ${filepath} ${option1} ${option2} ${option3} ${option4} ${option5} ${option6} ${option7} ${option8} ${option9} ${option10}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDeviceSemUartDebug,
               "debug device sem uart",
               "Debug UART interface",
               DEF_SDKPARAM("semId", "STRING", "SEM controller ID"),
               "${semId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDeviceThermalSensorAlarmSetThresholdSet,
               "device sensor thermal threshold alarmset",
               "Set SET alarm threshold",
               DEF_SDKPARAM("celsius", "STRING", "SET alarm threshold value"),
               "${celsius}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDeviceThermalSensorAlarmClearThresholdSet,
               "device sensor thermal threshold alarmclear",
               "Set CLEAR alarm threshold",
               DEF_SDKPARAM("celsius", "STRING", "CLEAR alarm threshold value"),
               "${celsius}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDeviceThermalSensorShow,
               "show device sensor thermal",
               "Show status and configuration of thermal sensor",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDeviceThermalSensorAlarmShow,
               "show device sensor thermal alarm",
               "Show current alarm of thermal sensor",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDeviceThermalSensorHistoryShow,
               "show device sensor thermal history",
               "Show history alarm of thermal sensor",
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", ""),
               "${readingMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDeviceSensorThermalInterruptMask,
               "device sensor thermal interruptmask",
               "Set thermal sensor interrupt mask.",
               DEF_SDKPARAM("mask", "ThermalAlmMask", "")
               DEF_SDKPARAM("intrMaskEn", "eBool", ""),
               "${mask} ${intrMaskEn}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDeviceSensorThermalNotificationEnable,
               "device sensor thermal notification enable",
               "Enable thermal sensor interrupt notification",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDeviceThermalSensorConstrainShow,
               "show device sensor thermal constrain",
               "Show thermal threshold constrains",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDeviceSensorThermalNotificationDisable,
               "device sensor thermal notification disable",
               "Disable thermal sensor interrupt notification",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDeviceInterruptShow,
               "show device interrupt",
               "Show device interrupt configuration and status",
               DEF_SDKPARAM_OPTION("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${readingmode} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceInterruptLogEnable,
               "device interrupt log enable",
               "Enable device interrupt logging",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceInterruptLogDisable,
               "device interrupt log disable",
               "Disable device interrupt logging",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceAllChannelsListenDefectClear,
               "device listenedalarm clear",
               "Clear all channels' alarm cached by listener callback",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceDiagUartReceive,
               "device diag uart receive",
               "Receive message from UART",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceDiagUartTransmit,
               "device diag uart transmit",
               "Send UART message",
               DEF_SDKPARAM("commandString", "STRING", "UART message")
               DEF_SDKPARAM_OPTION("option1", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option2", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option3", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option4", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option5", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option6", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option7", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option8", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option9", "STRING", "Option")
               DEF_SDKPARAM_OPTION("option10", "STRING", "Option"),
               "${commandString} ${option1} ${option2} ${option3} ${option4} ${option5} ${option6} ${option7} ${option8} ${option9} ${option10}")
    {
    mAtCliCall();
    }
 
DEF_SDKCOMMAND(CliAtDeviceRoleSet,
               "device role",
               "Set device role",
               DEF_SDKPARAM("role", "eAtDeviceRole", ""),
               "${role}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtPowerSupplySensorAlarmUpperThresholdSet,
               "device sensor power_supply threshold upper",
               "Set SET alarm threshold",
               DEF_SDKPARAM("voltageType", "eAtPowerSupplyVoltage", "Power supply voltage types")
               DEF_SDKPARAM("voltageValue", "UINT", "Upper threshold value"),
               "${voltageValue} ${voltageValue}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtPowerSupplySensorAlarmLowerThresholdSet,
               "device sensor power_supply threshold lower",
               "Set CLEAR alarm threshold",
               DEF_SDKPARAM("voltageType", "eAtPowerSupplyVoltage", "Power supply voltage types")
               DEF_SDKPARAM("voltageValue", "UINT", "Lower threshold value"),
               "${voltageValue} ${voltageValue}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDevicePowerSupplySensorShow,
               "show device sensor power_supply",
               "Show status and configuration of power_supply sensor",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDevicePowerSupplySensorHistoryShow,
               "show device sensor power_supply history",
               "Show history alarm of power_supply sensor",
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", ""),
               "${readingMode}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDevicePowerSupplySensorAlarmShow,
               "show device sensor power_supply alarm",
               "Show current alarm of power_supply sensor",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceThermalSensorInit,
               "device sensor thermal init",
               "Initialize thermal sensor",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtPowerSupplySensorInit,
               "device sensor power_supply init",
               "Initialize power supply sensor",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceProblemsQuery,
               "device query problem",
               "To query all of problems in the device. This will scan all of modules and all of managed objects.\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDeviceShowSubordinate,
               "show device subordinate",
               "Show sub devices\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

/* Select sub device */
DEF_SDKCOMMAND(cliAtSubDeviceSelect,
               "device select subordinate",
               "Select sub device that CLI module works on",
               DEF_SDKPARAM("subdeviceId", "STRING", "Device ID, starts from 1. \"none\" if no device is selected"),
               "${subdeviceId}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDeviceInterruptMask,
               "device interruptmask",
               "Set device interrupt mask.",
               DEF_SDKPARAM("mask", "DevAlmMask", "")
               DEF_SDKPARAM("intrMaskEn", "eBool", ""),
               "${mask} ${intrMaskEn}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDeviceAlarmCapture,
               "device alarm capture",
               "Enable/disable alarm capturing",
               DEF_SDKPARAM("enable", "eBool", "Enable/disable alarm capturing"),
               "${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliDeviceAlarmShow,
               "show device alarm",
               "Show device alarms",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : XC module
 *
 * File        : ClishAtCrossConnect.c
 *
 * Created Date: Oct 23, 2014
 *
 * Description : CLISH CLI of XC module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/* Top commands */
TOP_SDKCOMMAND("xc", "\r\nCross connect\r\n")
TOP_SDKCOMMAND("xc nxds0", "\r\nCross connect for NxDS0 layer\r\n")
TOP_SDKCOMMAND("xc de1", "\r\nCross connect for DE1 layer\r\n")
TOP_SDKCOMMAND("xc de3", "\r\nCross connect for DE3 layer\r\n")
TOP_SDKCOMMAND("xc vc", "\r\nCross connect for SDH-VC layer\r\n")
TOP_SDKCOMMAND("show xc", "\r\nShow cross connect information\r\n")
TOP_SDKCOMMAND("show xc nxds0", "\r\nShow cross connect source for NxDS0 layer destination\r\n")
TOP_SDKCOMMAND("show xc de1", "\r\nShow cross connect source for DE1 layer destination\r\n")
TOP_SDKCOMMAND("show xc de3", "\r\nShow cross connect source for DE3 layer destination\r\n")
TOP_SDKCOMMAND("show xc vc", "\r\nShow cross connect source for  SDH-VC layer\r\n")

DEF_PTYPE("ConnectOptions", "select", "two-way(0)", "\n Two-way configuration options")
DEF_PTYPE("ShowConnectOptions", "select", "full(0)", "\n Show all of connections")

DEF_PTYPE("SdhVcList", "regexp", ".*",
        "List of VCs support XC\n"
        "- VC4-16c: vc4_16c.<lineId>.<aug16Id>\n"
        "- VC4-4c : vc4_4c.<lineId>.<aug4Id>\n"
        "- VC4    : vc4.<lineId>.<aug1Id>\n"
        "- VC3    : vc3.<lineId>.<aug1Id>.<au3Id/tug3Id>\n"
        "- VC-1x  : vc1x.<lineId>.<aug1Id>.<au3Tug3Id>.<tug2Id>.<vcxId>\n")

DEF_SDKCOMMAND(CliXcNxDs0Connect,
               "xc nxds0 connect",
               "Cross connect for NxDS0 layer\r\n",
               DEF_SDKPARAM("source", "PdhNxDs0List", "")
               DEF_SDKPARAM("destList", "PdhNxDs0List", "")
               DEF_SDKPARAM_OPTION("options", "ConnectOptions", ""),
               "${source} ${destList} ${options}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliXcNxDs0Disconnect,
               "xc nxds0 disconnect",
               "Cross disconnect for NxDS0 layer\r\n",
               DEF_SDKPARAM("source", "PdhNxDs0List", "")
               DEF_SDKPARAM_OPTION("destList", "PdhNxDs0List", "")
               DEF_SDKPARAM_OPTION("options", "ConnectOptions", ""),
               "${source} ${destList} ${options}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliXcNxDs0SourceShow,
               "show xc nxds0 source",
               "Show cross connect source for NxDS0 layer destination\r\n",
               DEF_SDKPARAM("destList", "PdhNxDs0List", ""),
               "${destList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliXcNxDs0DestShow,
               "show xc nxds0 dest",
               "Show cross connect destination for NxDS0 layer source\r\n",
               DEF_SDKPARAM("sourceList", "PdhNxDs0List", ""),
               "${sourceList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliXcDe1Connect,
               "xc de1 connect",
               "Cross connect for DE1 layer\r\n",
               DEF_SDKPARAM("source", "PdhDe1List", "")
               DEF_SDKPARAM("destList", "PdhDe1List", "")
               DEF_SDKPARAM_OPTION("options", "ConnectOptions", ""),
               "${source} ${destList} ${options}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliXcDe1Disconnect,
               "xc de1 disconnect",
               "Cross disconnect for DE1 layer\r\n",
               DEF_SDKPARAM("source", "PdhDe1List", "")
               DEF_SDKPARAM_OPTION("destList", "PdhDe1List", "")
               DEF_SDKPARAM_OPTION("options", "ConnectOptions", ""),
               "${source} ${destList} ${options}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliXcDe1SourceShow,
               "show xc de1 source",
               "Show cross connect source for DE1 layer destination\r\n",
               DEF_SDKPARAM("destList", "PdhDe1List", ""),
               "${destList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliXcDe1DestShow,
               "show xc de1 dest",
               "Show cross connect destination for DE1 layer source\r\n",
               DEF_SDKPARAM("sourceList", "PdhDe1List", ""),
               "${sourceList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliXcDe3Connect,
               "xc de3 connect",
               "Cross connect for DE3 layer\r\n",
               DEF_SDKPARAM("source", "PdhDe3List", "")
               DEF_SDKPARAM("destList", "PdhDe3List", "")
               DEF_SDKPARAM_OPTION("options", "ConnectOptions", ""),
               "${source} ${destList} ${options}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliXcDe3Disconnect,
               "xc de3 disconnect",
               "Cross disconnect for DE3 layer\r\n",
               DEF_SDKPARAM("source", "PdhDe3List", "")
               DEF_SDKPARAM_OPTION("destList", "PdhDe3List", "")
               DEF_SDKPARAM_OPTION("options", "ConnectOptions", ""),
               "${source} ${destList} ${options}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliXcDe3SourceShow,
               "show xc de3 source",
               "Show cross connect source for DE3 layer destination\r\n",
               DEF_SDKPARAM("destList", "PdhDe3List", ""),
               "${destList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliXcDe3DestShow,
               "show xc de3 dest",
               "Show cross connect destination for DE3 layer source\r\n",
               DEF_SDKPARAM("sourceList", "PdhDe3List", ""),
               "${sourceList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliXcVcConnect,
               "xc vc connect",
               "Cross connect for VC layer\r\n",
               DEF_SDKPARAM("source", "SdhVcList", "")
               DEF_SDKPARAM("destList", "SdhVcList", "")
               DEF_SDKPARAM_OPTION("options", "ConnectOptions", ""),
               "${source} ${destList} ${options}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliXcVcDisconnect,
               "xc vc disconnect",
               "Cross disconnect for VC layer\r\n",
               DEF_SDKPARAM("source", "SdhVcList", "")
               DEF_SDKPARAM_OPTION("destList", "SdhVcList", "")
               DEF_SDKPARAM_OPTION("options", "ConnectOptions", ""),
               "${source} ${destList} ${options}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliXcVcSourceShow,
               "show xc vc source",
               "Show cross connect source for VC layer destination\r\n",
               DEF_SDKPARAM("destList", "SdhVcList", ""),
               "${destList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliXcVcDestShow,
               "show xc vc dest",
               "Show cross connect destination for VC layer source\r\n",
               DEF_SDKPARAM("sourceList", "SdhVcList", "")
               DEF_SDKPARAM_OPTION("options", "ShowConnectOptions", ""),
               "${sourceList} ${options}")
    {
    mAtCliCall();
    }

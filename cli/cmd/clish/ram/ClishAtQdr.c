/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : QDR
 *
 * File        : ClishAtQdr.c
 *
 * Created Date: Jun 30, 2015
 *
 * Description : QDR CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
DEF_PTYPE("QdrIdList", "regexp", ".*", "List of QDRs\n")

TOP_SDKCOMMAND("qdr", "QDR CLIs\n")
TOP_SDKCOMMAND("qdr test", "CLIs to control QDR accessing")
TOP_SDKCOMMAND("qdr test hwassist", "CLIs to control hardware assist")
TOP_SDKCOMMAND("qdr access", "CLIs to control QDR accessing")
TOP_SDKCOMMAND("qdr calib", "CLIs to control QDR calibration")
TOP_SDKCOMMAND("debug qdr bussize", "CLIs to change bus size")
TOP_SDKCOMMAND("qdr error", "CLIs Error generator")
TOP_SDKCOMMAND("qdr error generator", "CLIs Error generator")
TOP_SDKCOMMAND("show qdr error", "CLIs to show Error generator")

DEF_SDKCOMMAND(cliAtQdrAddressBusTest,
               "qdr test addressbus",
               "Test address bus\n",
               DEF_SDKPARAM("qdrList", "QdrIdList", "")
               DEF_SDKPARAM_OPTION("durationMs", "STRING", "Testing duration in milliseconds."),
               "${qdrList} ${durationMs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtQdrDataBusTest,
               "qdr test databus",
               "Test data bus\n",
               DEF_SDKPARAM("qdrList", "QdrIdList", "")
               DEF_SDKPARAM_OPTION("durationMs", "STRING", "Testing duration in milliseconds."),
               "${qdrList} ${durationMs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtQdrMemoryTest,
               "qdr test memory",
               "Test memory\n",
               DEF_SDKPARAM("qdrList", "QdrIdList", "")
               DEF_SDKPARAM_OPTION("durationMs", "STRING", "Testing duration in milliseconds."),
               "${qdrList} ${durationMs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtQdrAccessEnable,
               "qdr access enable",
               "Enable SW read/write QDR memory\n",
               DEF_SDKPARAM("qdrList", "QdrIdList", ""),
               "${qdrList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtQdrAccessDisable,
               "qdr access disable",
               "Disable SW read/write QDR memory\n",
               DEF_SDKPARAM("qdrList", "QdrIdList", ""),
               "${qdrList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtQdrCountersGet,
               "show qdr counters",
               "Get QDR counters\n",
               DEF_SDKPARAM("qdrList", "QdrIdList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${qdrList} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtQdrHwTestStart,
               "qdr test start",
               "Start hardware base testing.\n",
               DEF_SDKPARAM("qdrList", "QdrIdList", ""),
               "${qdrList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtQdrHwTestStop,
               "qdr test stop",
               "Stop hardware base testing.\n",
               DEF_SDKPARAM("qdrList", "QdrIdList", ""),
               "${qdrList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtQdrHwTestStatusShow,
               "show qdr test",
               "Show QDR hardware test status\n",
               DEF_SDKPARAM("qdrList", "QdrIdList", ""),
               "${qdrList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtQdrDebugAddressBusSizeSet,
               "debug qdr bussize address",
               "Configure address bus size for testing and debugging purpose\n",
               DEF_SDKPARAM("qdrList", "QdrIdList", "")
               DEF_SDKPARAM("busSize", "STRING", "Address bus size. Set this to 0 to revert back to default bus size."),
               "${qdrList} ${busSize}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtQdrDebugDataBusSizeSet,
               "debug qdr bussize data",
               "Configure data bus size for testing and debugging purpose\n",
               DEF_SDKPARAM("qdrList", "QdrIdList", "")
               DEF_SDKPARAM("busSize", "STRING", "data bus size. Set this to 0 to revert back to default bus size."),
               "${qdrList} ${busSize}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtQdrDebug,
               "debug qdr",
               "Show debug information of QDR\n",
               DEF_SDKPARAM("qdrList", "QdrIdList", ""),
               "${qdrList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtQdrHwTestErrorForce,
               "qdr test force",
               "Force error during hardware base testing is running\n",
               DEF_SDKPARAM("qdrList", "QdrIdList", ""),
               "${qdrList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtQdrHwTestErrorUnforce,
               "qdr test unforce",
               "Unforce error during hardware base testing is running\n",
               DEF_SDKPARAM("qdrList", "QdrIdList", ""),
               "${qdrList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtQdrRead,
               "qdr rd",
               "Read QDR\n",
               DEF_SDKPARAM("qdrId", "UINT", "QDR ID")
               DEF_SDKPARAM("localAddress", "RegisterAddress", ""),
               "${qdrId} ${localAddress}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtQdrWrite,
               "qdr wr",
               "Write QDR\n",
               DEF_SDKPARAM("qdrId", "UINT", "QDR ID")
               DEF_SDKPARAM("localAddress", "RegisterAddress", "")
               DEF_SDKPARAM("value", "RegisterValue", ""),
               "${qdrId} ${localAddress} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtQdrTestBurstSet,
               "qdr burst",
               "Configure number of addresses hardware can do burst read/write\n",
               DEF_SDKPARAM("qdrList", "QdrIdList", "")
               DEF_SDKPARAM("burst", "STRING", "Number of addresses hardware can do burst read/write"),
               "${qdrList} ${burst}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtQdrTestHwAssistEnable,
               "qdr test hwassist enable",
               "Enable hardware assist mode to speedup memory fill and compare operations. Note, not all of products can support this.\n",
               DEF_SDKPARAM("qdrList", "QdrIdList", ""),
               "${qdrList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtQdrTestHwAssistDisable,
               "qdr test hwassist disable",
               "Disable hardware assist mode. Memory fill and compare operations are done by software. It would take long time to complete testing.\n",
               DEF_SDKPARAM("qdrList", "QdrIdList", ""),
               "${qdrList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtQdrShow,
               "show qdr",
               "Show QDR information\n",
               DEF_SDKPARAM("qdrList", "QdrIdList", ""),
               "${qdrList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtQdrCalibMeasure,
               "qdr calib measure",
               "Measure calibration time\n",
               DEF_SDKPARAM("qdrList", "QdrIdList", "")
               DEF_SDKPARAM_OPTION("repeatTimes", "UINT", "Repeat times"),
               "${qdrList} ${repeatTimes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliQdrErrorGeneratorErrorTypeSet,
               "qdr error generator type",
               "Set error generator type for the Qdr list\n",
               DEF_SDKPARAM("qdrList", "QdrIdList", "")
               DEF_SDKPARAM("type", "eAtRamErrorGeneratorErrorType", ""),
               "${ramList} ${type}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliQdrErrorGeneratorErrorNumSet,
               "qdr error generator errornum",
               "Set number of error for the error generator of the Qdr list in case oneshot mode\n",
               DEF_SDKPARAM("qdrList", "QdrIdList", "")
               DEF_SDKPARAM("num", "UINT", ""),
               "${ramList} ${num}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliQdrErrorGeneratorErrorStart,
               "qdr error generator start",
               "Start the error generator",
               DEF_SDKPARAM("qdrList", "QdrIdList", ""),
               "${ramList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliQdrErrorGeneratorErrorStop,
               "qdr error generator stop",
               "Stop the error generator",
               DEF_SDKPARAM("qdrList", "QdrIdList", ""),
               "${ramList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliQdrErrorGeneratorErrorShow,
               "show qdr error generator",
               "Show the qdr error generator",
               DEF_SDKPARAM("qdrList", "QdrIdList", ""),
               "${ramList}")
    {
    mAtCliCall();
    }


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : ClishAtModuleRam.c
 *
 * Created Date: Sep 14, 2015
 *
 * Description : RAM CLISH CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
TOP_SDKCOMMAND("show ram", "CLIs to show RAM information")
TOP_SDKCOMMAND("ram", "CLIs to control any RAM")
TOP_SDKCOMMAND("ram interrupt", "CLIs to control RAM interrupt")
TOP_SDKCOMMAND("ram interrupt capture", "CLIs to control RAM interrupt capture")

DEF_SDKCOMMAND(cliAtModuleRamShow,
               "show module ram",
               "Show RAM module information\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliRamInterruptEnable,
               "ram interrupt enable",
               "Enable interrupt",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliRamInterruptDisable,
               "ram interrupt disable",
               "Disable interrupt",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliRamInterruptCaptureEnable,
               "ram interrupt capture enable",
               "Enable interrupt capture",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliRamInterruptCaptureDisable,
               "ram interrupt capture disable",
               "Disable interrupt capture",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliModuleSdhRam,
               "debug module ram",
               "Show debug information of RAM module\n",
               DEF_NULLPARAM,
               "")
    {
    mAtCliCall();
    }

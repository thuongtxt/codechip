/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ZBT
 *
 * File        : ClishAtZbt.c
 *
 * Created Date: Feb 1, 2013
 *
 * Description : ZBT CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
DEF_PTYPE("ZbtIdList", "regexp", ".*", "List of ZBTs\n")

TOP_SDKCOMMAND("zbt", "ZBT CLIs\n")
TOP_SDKCOMMAND("zbt access", "CLIs to control ZTE accessing\n")
TOP_SDKCOMMAND("zbt test hwassist", "CLIs to control hardware assist")
TOP_SDKCOMMAND("debug zbt bussize", "CLIs to change bus size")

DEF_SDKCOMMAND(cliAtZbtAddressBusTest,
               "zbt test addressbus",
               "Test address bus\n",
               DEF_SDKPARAM("zbtList", "ZbtIdList", "")
               DEF_SDKPARAM_OPTION("durationMs", "STRING", "Testing duration in milliseconds."),
               "${zbtList} ${durationMs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtZbtDataBusTest,
               "zbt test databus",
               "Test data bus\n",
               DEF_SDKPARAM("zbtList", "ZbtIdList", "")
               DEF_SDKPARAM_OPTION("durationMs", "STRING", "Testing duration in milliseconds."),
               "${zbtList} ${durationMs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtZbtMemoryTest,
               "zbt test memory",
               "Test memory\n",
               DEF_SDKPARAM("zbtList", "ZbtIdList", "")
               DEF_SDKPARAM_OPTION("durationMs", "STRING", "Testing duration in milliseconds."),
               "${zbtList} ${durationMs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtZbtRead,
               "zbt rd",
               "Read ZBT\n",
               DEF_SDKPARAM("zbtId", "UINT", "ZBT ID")
               DEF_SDKPARAM("localAzbtess", "RegisterAddress", ""),
               "${zbtId} ${localAzbtess}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtZbtWrite,
               "zbt wr",
               "Write ZBT\n",
               DEF_SDKPARAM("zbtId", "UINT", "ZBT ID")
               DEF_SDKPARAM("localAzbtess", "RegisterAddress", "")
               DEF_SDKPARAM("value", "RegisterValue", ""),
               "${zbtId} ${localAzbtess} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtZbtDebug,
               "debug zbt",
               "Show debug information of ZBT\n",
               DEF_SDKPARAM("zbtList", "ZbtIdList", ""),
               "${zbtList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtZbtAccessEnable,
               "zbt access enable",
               "Enable SW read/write ZBT memory\n",
               DEF_SDKPARAM("zbtList", "ZbtIdList", ""),
               "${zbtList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtZbtAccessDisable,
               "zbt access disable",
               "Disable SW read/write ZBT memory\n",
               DEF_SDKPARAM("zbtList", "ZbtIdList", ""),
               "${zbtList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtZbtTest,
               "zbt test",
               "Full RAM testing\n",
               DEF_SDKPARAM("zbtList", "ZbtIdList", "")
               DEF_SDKPARAM_OPTION("durationMs", "UINT", "Testing duration in milliseconds."),
               "${zbtList} ${durationMs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtZbtHwTestStart,
               "zbt test start",
               "Start hardware base testing.\n",
               DEF_SDKPARAM("zbtList", "ZbtIdList", ""),
               "${zbtList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtZbtHwTestStop,
               "zbt test stop",
               "Stop hardware base testing.\n",
               DEF_SDKPARAM("zbtList", "ZbtIdList", ""),
               "${zbtList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtZbtHwTestStatusShow,
               "show zbt test",
               "Show ZBT hardware test status\n",
               DEF_SDKPARAM("zbtList", "ZbtIdList", ""),
               "${zbtList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtZbtDebugAddressBusSizeSet,
               "debug zbt bussize address",
               "Configure address bus size for testing and debugging purpose\n",
               DEF_SDKPARAM("zbtList", "ZbtIdList", "")
               DEF_SDKPARAM("busSize", "STRING", "Address bus size. Set this to 0 to revert back to default bus size."),
               "${zbtList} ${busSize}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtZbtDebugDataBusSizeSet,
               "debug zbt bussize data",
               "Configure data bus size for testing and debugging purpose\n",
               DEF_SDKPARAM("zbtList", "ZbtIdList", "")
               DEF_SDKPARAM("busSize", "STRING", "data bus size. Set this to 0 to revert back to default bus size."),
               "${zbtList} ${busSize}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtZbtTestBurstSet,
               "zbt burst",
               "Configure number of addresses hardware can do burst read/write\n",
               DEF_SDKPARAM("zbtList", "ZbtIdList", "")
               DEF_SDKPARAM("burst", "STRING", "Number of addresses hardware can do burst read/write"),
               "${zbtList} ${burst}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtZbtTestHwAssistEnable,
               "zbt test hwassist enable",
               "Enable hardware assist mode to speedup memory fill and compare operations. Note, not all of products can support this.\n",
               DEF_SDKPARAM("zbtList", "ZbtIdList", ""),
               "${zbtList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtZbtTestHwAssistDisable,
               "zbt test hwassist disable",
               "Disable hardware assist mode. Memory fill and compare operations are done by software. It would take long time to complete testing.\n",
               DEF_SDKPARAM("zbtList", "ZbtIdList", ""),
               "${zbtList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtZbtShow,
               "show zbt",
               "Show ZBT information\n",
               DEF_SDKPARAM("zbtList", "ZbtIdList", ""),
               "${zbtList}")
    {
    mAtCliCall();
    }

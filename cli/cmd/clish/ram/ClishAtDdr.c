/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : DDR
 *
 * File        : ClishAtDdr.c
 *
 * Created Date: Feb 1, 2013
 *
 * Description : DDR CLIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
DEF_PTYPE("DdrIdList", "regexp", ".*", "List of DDRs\n")

TOP_SDKCOMMAND("ddr", "DDR CLIs\n")
TOP_SDKCOMMAND("ddr access", "CLIs to control DDR accessing")
TOP_SDKCOMMAND("ddr calib", "CLIs to control DDR calibration")
TOP_SDKCOMMAND("ddr test hwassist", "CLIs to control hardware assist")
TOP_SDKCOMMAND("debug ddr bussize", "CLIs to change bus size")

DEF_SDKCOMMAND(cliAtDdrAddressBusTest,
               "ddr test addressbus",
               "Test address bus\n",
               DEF_SDKPARAM("ddrList", "DdrIdList", "")
               DEF_SDKPARAM_OPTION("durationMs", "STRING", "Testing duration in milliseconds."),
               "${ddrList} ${durationMs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDdrDataBusTest,
               "ddr test databus",
               "Test data bus\n",
               DEF_SDKPARAM("ddrList", "DdrIdList", "")
               DEF_SDKPARAM_OPTION("durationMs", "STRING", "Testing duration in milliseconds."),
               "${ddrList} ${durationMs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDdrMemoryTest,
               "ddr test memory",
               "Test memory\n",
               DEF_SDKPARAM("ddrList", "DdrIdList", "")
               DEF_SDKPARAM_OPTION("durationMs", "STRING", "Testing duration in milliseconds."),
               "${ddrList} ${durationMs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDdrRead,
               "ddr rd",
               "Read DDR\n",
               DEF_SDKPARAM("ddrId", "UINT", "DDR ID")
               DEF_SDKPARAM("localAddress", "RegisterAddress", ""),
               "${ddrId} ${localAddress}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDdrWrite,
               "ddr wr",
               "Write DDR\n",
               DEF_SDKPARAM("ddrId", "UINT", "DDR ID")
               DEF_SDKPARAM("localAddress", "RegisterAddress", "")
               DEF_SDKPARAM("value", "RegisterValue", ""),
               "${ddrId} ${localAddress} ${value}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDdrDebug,
               "debug ddr",
               "Show debug information of DDR\n",
               DEF_SDKPARAM("ddrList", "DdrIdList", ""),
               "${ddrList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDdrAccessEnable,
               "ddr access enable",
               "Enable SW read/write DDR memory\n",
               DEF_SDKPARAM("ddrList", "DdrIdList", ""),
               "${ddrList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDdrAccessDisable,
               "ddr access disable",
               "Disable SW read/write DDR memory\n",
               DEF_SDKPARAM("ddrList", "DdrIdList", ""),
               "${ddrList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDdrTest,
               "ddr test",
               "Full RAM testing\n",
               DEF_SDKPARAM("ddrList", "DdrIdList", "")
               DEF_SDKPARAM_OPTION("durationMs", "UINT", "Testing duration in milliseconds."),
               "${ddrList} ${durationMs}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliAtDdrCountersGet,
               "show ddr counters",
               "Get DDR counters\n",
               DEF_SDKPARAM("ddrList", "DdrIdList", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${ddrList} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDdrHwTestStart,
               "ddr test start",
               "Start hardware base testing.\n",
               DEF_SDKPARAM("ddrList", "DdrIdList", ""),
               "${ddrList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDdrHwTestStop,
               "ddr test stop",
               "Stop hardware base testing.\n",
               DEF_SDKPARAM("ddrList", "DdrIdList", ""),
               "${ddrList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDdrHwTestStatusShow,
               "show ddr test",
               "Show DDR hardware test status\n",
               DEF_SDKPARAM("ddrList", "DdrIdList", ""),
               "${ddrList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDdrDebugAddressBusSizeSet,
               "debug ddr bussize address",
               "Configure address bus size for testing and debugging purpose\n",
               DEF_SDKPARAM("ddrList", "DdrIdList", "")
               DEF_SDKPARAM("busSize", "STRING", "Address bus size. Set this to 0 to revert back to default bus size."),
               "${ddrList} ${busSize}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDdrDebugDataBusSizeSet,
               "debug ddr bussize data",
               "Configure data bus size for testing and debugging purpose\n",
               DEF_SDKPARAM("ddrList", "DdrIdList", "")
               DEF_SDKPARAM("busSize", "STRING", "data bus size. Set this to 0 to revert back to default bus size."),
               "${ddrList} ${busSize}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDdrTestBurstSet,
               "ddr burst",
               "Configure number of addresses hardware can do burst read/write\n",
               DEF_SDKPARAM("ddrList", "DdrIdList", "")
               DEF_SDKPARAM("burst", "STRING", "Number of addresses hardware can do burst read/write"),
               "${ddrList} ${burst}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDdrTestHwAssistEnable,
               "ddr test hwassist enable",
               "Enable hardware assist mode to speedup memory fill and compare operations. Note, not all of products can support this.\n",
               DEF_SDKPARAM("ddrList", "DdrIdList", ""),
               "${ddrList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDdrTestHwAssistDisable,
               "ddr test hwassist disable",
               "Disable hardware assist mode. Memory fill and compare operations are done by software. It would take long time to complete testing.\n",
               DEF_SDKPARAM("ddrList", "DdrIdList", ""),
               "${ddrList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDdrShow,
               "show ddr",
               "Show DDR information\n",
               DEF_SDKPARAM("ddrList", "DdrIdList", ""),
               "${ddrList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDdrCalibMeasure,
               "ddr calib measure",
               "Measure calibration time\n",
               DEF_SDKPARAM("ddrList", "DdrIdList", "")
               DEF_SDKPARAM_OPTION("repeatTimes", "UINT", "Repeat times"),
               "${ddrList} ${repeatTimes}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDdrHwTestErrorForce,
               "ddr test force",
               "Force error during hardware base testing is running\n",
               DEF_SDKPARAM("ddrList", "DdrIdList", ""),
               "${ddrList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliAtDdrHwTestErrorUnforce,
               "ddr test unforce",
               "Unforce error during hardware base testing is running\n",
               DEF_SDKPARAM("ddrList", "DdrIdList", ""),
               "${ddrList}")
    {
    mAtCliCall();
    }

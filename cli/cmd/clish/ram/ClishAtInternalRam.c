/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : ClishAtInternalRam.c
 *
 * Created Date: Dec 7, 2015
 *
 * Description : Internal RAM CLISHs.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtClish.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define RamErrorType "(parity|crc|ecc-correctable|ecc-uncorrectable)"
#define RamErrorMask "((("RamErrorType"[\\|])+"RamErrorType")|"RamErrorType")"


/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

TOP_SDKCOMMAND("ram internal", "CLIs to control internal RAM")
TOP_SDKCOMMAND("ram internal force", "CLIs to force internal RAM")
TOP_SDKCOMMAND("ram internal parity", "CLIs to control internal RAM parity")
TOP_SDKCOMMAND("ram internal ecc", "CLIs to control internal RAM ECC")
TOP_SDKCOMMAND("ram internal crc", "CLIs to control internal RAM CRC")
TOP_SDKCOMMAND("ram internal error", "CLIs to control internal RAM Error")
TOP_SDKCOMMAND("ram internal error generator", "CLIs to control internal RAM Error Generator")
TOP_SDKCOMMAND("show ram internal error", "Show RAM Error Generator")

DEF_PTYPE("eAtRamErrorGeneratorErrorType", "select", "ecc-correctable(1), ecc-uncorrectable(2), crc(3)", "RAM error generator error type")

DEF_PTYPE("RamIdList", "regexp", ".*", "Internal RAM ID List")
DEF_PTYPE("RamIdListOrModule", "regexp", ".*",
          "Internal RAM from number ID List: 1, 2-10, ... OR\n"
          "Internal RAM from RTL module:\n"
          " - ocn: OCN module\n"
          " - poh: POH module\n"
          " - cdr: CDR module\n"
          " - sur: PM/FM module\n"
          " - pdh: PDH module\n"
          " - map: MAP/DEMAP module\n"
          " - pwe: PWE module\n"
          " - cla: CLA module\n"
          " - pda: PDA module\n")

DEF_PTYPE("RamErrorMask", "regexp", RamErrorMask,
          "\nRAM error masks:\n"
          "- parity : Parity error\n"
          "- crc : CRC error\n"
          "- ecc-correctable : ECC correctable error\n"
          "- ecc-uncorrectable : ECC uncorrectable error\n"
          "Note, these masks can be ORed, example parity\n")

DEF_SDKCOMMAND(cliRamInternalRamShow,
               "show ram internal",
               "Show internal ram information\n",
               DEF_SDKPARAM("ramList", "RamIdListOrModule", ""),
               "${ramList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliRamInternalRamInterruptShow,
               "show ram internal history",
               "Show internal ram history\n",
               DEF_SDKPARAM("ramList", "RamIdListOrModule", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${readingMode} ${ramList} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliRamInternalRamErrorForce,
               "ram internal force error",
               "Force error on internal RAMs\n",
               DEF_SDKPARAM("ramList", "RamIdList", "List of internal RAMs: 1, 1-3, ...")
               DEF_SDKPARAM("errorList", "RamErrorMask", "")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${ramList} ${errorList} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliRamInternalRamParityMonitor,
               "ram internal parity monitor",
               "Enable parity monitoring\n",
               DEF_SDKPARAM("ramList", "RamIdList", "List of internal RAMs: 1, 1-3, ...")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${ramList} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliRamInternalRamEccMonitor,
               "ram internal ecc monitor",
               "Enable ECC monitoring\n",
               DEF_SDKPARAM("ramList", "RamIdList", "List of internal RAMs: 1, 1-3, ...")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${ramList} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliRamInternalRamCrcMonitor,
               "ram internal crc monitor",
               "Enable CRC monitoring\n",
               DEF_SDKPARAM("ramList", "RamIdList", "List of internal RAMs: 1, 1-3, ...")
               DEF_SDKPARAM("enable", "eBool", ""),
               "${ramList} ${enable}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliRamInternalRamCountersShow,
               "show ram internal counters",
               "Show internal ram counters\n",
               DEF_SDKPARAM("ramList", "RamIdListOrModule", "")
               DEF_SDKPARAM("readingMode", "HistoryReadingMode", "")
               DEF_SDKPARAM_OPTION("silent", "eAtCliVerboseMode", ""),
               "${readingMode} ${ramList} ${silent}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliRamInternalRamErrorGeneratorErrorTypeSet,
               "ram internal error generator type",
               "Set error generator type for a Ram Internal list\n",
               DEF_SDKPARAM("ramList", "RamIdList", "List of internal RAMs: 1, 1-3, ...")
               DEF_SDKPARAM("type", "eAtRamErrorGeneratorErrorType", ""),
               "${ramList} ${type}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliRamInternalRamErrorGeneratorErrorNumSet,
               "ram internal error generator errornum",
               "Set number of error for the error generator of a Ram Internal list in case oneshot mode\n",
               DEF_SDKPARAM("ramList", "RamIdList", "List of internal RAMs: 1, 1-3, ...")
               DEF_SDKPARAM("num", "UINT", ""),
               "${ramList} ${num}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliRamInternalRamErrorGeneratorErrorStart,
               "ram internal error generator start",
               "Start the error generator",
               DEF_SDKPARAM("ramList", "RamIdList", "List of internal RAMs: 1, 1-3, ..."),
               "${ramList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliRamInternalRamErrorGeneratorErrorStop,
               "ram internal error generator stop",
               "Stop the error generator",
               DEF_SDKPARAM("ramList", "RamIdList", "List of internal RAMs: 1, 1-3, ..."),
               "${ramList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(cliRamInternalRamErrorGeneratorErrorShow,
               "show ram internal error generator",
               "Show the ram internal error generator",
               DEF_SDKPARAM("ramList", "RamIdList", "List of internal RAMs: 1, 1-3, ..."),
               "${ramList}")
    {
    mAtCliCall();
    }

DEF_SDKCOMMAND(CliRamInternalRamErrorGeneratorModeSet,
               "ram internal error generator mode",
               "Set generator mode at TX direction\n",
               DEF_SDKPARAM("ramList", "RamIdList", "List of internal RAMs: 1, 1-3, ...")
               DEF_SDKPARAM("mode", "eAtErrorGeneratorMode", ""),
               "${ramList} ${mode}")
    {
    mAtCliCall();
    }

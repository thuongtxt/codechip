/*
 * =====================================================================================
 *
 *       Filename:  sdkcmd.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  11/18/2011 05:25:51 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Quan Cao Duc (), quancd@atvn.com.vn
 *        Company:  Arrive Technologies
 *
 * =====================================================================================
 */
#ifndef __SDKCMD_H__
#define __SDKCMD_H__

#include "attypes.h"
#include "clish_sdkcommon.h"
#include "clish/shell.h"
#include "AtCliModule.h"

#if defined(__MAKE_COMMAND_LIST__)
/*
 * NOTE: these macros are used by auto-generated tool and XML checking tool
 */

#define DEF_NULLPARAM NULL,

#define DEF_SDKPARAM(_name, _ptype, _helpstr) \
	(clish_param_t[1]){{.name=(char *)(size_t)_name, .ptype_name=(char *)(size_t)_ptype, .paramv=&PARAMV_NULL, .text=(char *)(size_t)_helpstr}},

#define DEF_SDKPARAM_OPTION(_name, _ptype, _helpstr) \
	(clish_param_t[1]){{.name=(char *)(size_t)_name, .ptype_name=(char *)(size_t)_ptype, .paramv=&PARAMV_NULL, .text=(char *)(size_t)_helpstr, .optional=BOOL_TRUE}},


/*
 * This will generate a list commands from C source file
 */
#define DEF_SDKCOMMAND_PROTOTYPE(funcname) \
	extern int funcname(clish_context_t *context,const lub_argv_t * argv)

#define DEF_PTYPE_COMMON(_name, _method, _pattern, _help) \
{\
	.name=(char *)(size_t)_name,\
	.text=(char *)(size_t)_help,\
	.pattern=(char *)(size_t)_pattern,\
	.method=_method, \
	.preprocess=CLISH_PTYPE_NONE,\
	.alloc = BOOL_FALSE, \
}

#define DEF_PTYPE_REGEXP(_name, _pattern, _help) \
{\
	.name=(char *)(size_t)_name,\
	.text=(char *)(size_t)_help,\
	.pattern=(char *)(size_t)"^" _pattern "$",\
	.method=CLISH_PTYPE_REGEXP, \
	.preprocess=CLISH_PTYPE_NONE,\
	.alloc = BOOL_FALSE, \
}

#define MAKE_DEF_SDKCOMMAND(_index, _funcname, _cmdname, _help, _actionparam,...) \
{\
	.name = (char *)(size_t)_cmdname,\
	.text = (char *)(size_t)_help,\
	.paramv = (clish_paramv_t[1]){{ \
		.paramv = (clish_param_t*[]){ \
			__VA_ARGS__ \
			NULL \
		}, \
       .paramc = 1 \
	}}, \
	.action = (clish_action_t[1]){{ \
		.builtin = (char *)(size_t)"sdkclish_"#_funcname, \
		.script = (char *)(size_t)_actionparam, \
		.index = _index \
	}}, \
	.config = (clish_config_t[1]){{ \
		.op = CLISH_CONFIG_NONE, \
		.splitter = BOOL_TRUE, \
		.unique = BOOL_TRUE, \
	}}, \
	.alloc = BOOL_FALSE, \
}, \

#define TOP_SDKCOMMAND(_cmdname, _help) \
    {\
        .name = (char *)(size_t)_cmdname,\
        .text = (char *)(size_t)_help,\
        .paramv = (clish_paramv_t[1]){{ \
            .paramv = NULL,\
			.paramc = 0 \
		}}, \
        .action = (clish_action_t[1]){{ \
            .builtin = NULL, \
            .script = NULL, \
            .index = -1 \
        }}, \
        .config = (clish_config_t[1]){{ \
            .op = CLISH_CONFIG_NONE, \
            .splitter = BOOL_TRUE, \
            .unique = BOOL_TRUE, \
        }}, \
		.alloc = BOOL_FALSE, \
    }, \

#define DEF_SDKCOMMAND_CLISHBUILTIN(funcname) \
    { \
		.name = "sdkclish_" #funcname, \
		.callback = (clish_shell_builtin_fn_t*)funcname \
    }

#elif defined(__PERL_EXTRACT_LIST__)

#define DEF_SDKCOMMAND(funcname, cmdname, help, paramlist, action) \
	MAKE_DEF_SDKCOMMAND(funcname, cmdname, help, action, paramlist);

#define ALIAS_SDKCOMMAND(funcname, cmdname, help, paramlist, actionparam) \
	MAKE_DEF_SDKCOMMAND(funcname, cmdname, help, actionparam, paramlist);

#else 

#define DEF_SDKCOMMAND(funcname, cmdname, help, paramlist, action) \
	int funcname(clish_context_t *context,const lub_argv_t * argv); \
	int funcname(clish_context_t *context,const lub_argv_t * argv)

#define TOP_SDKCOMMAND(cmdname, help) 

#define ALIAS_SDKCOMMAND(funcname, cmdname, help, paramlist, actionparam) 

#define DEF_PTYPE(name, method, pattern, help)

int shell_printf(const char *fmt, ...);

#define RET_CLI(retcode)                                                       \
{                                                                              \
    if(AUTOTEST_NOSILENT_CHK) {                                                \
        if (0 == retcode)                                                      \
            shell_printf("<Cli_Result retcode=\"OK\" />n");                    \
        else                                                                   \
            shell_printf("<Cli_Result retcode=\"FAIL\" />n");                  \
    }                                                                          \
    return(retcode);                                                           \
}

#define CALL_API(okval, func, retcode, retstr)                                 \
    do {                                                                       \
        retcode = func;                                                        \
        if (okval != retcode)                                                  \
        {                                                                      \
            if (AUTOTEST_NOSILENT_CHK) {                                       \
                shell_printf("<Cli_Result retcode="FAIL" n");                  \
                shell_printf("detail="CALL_API %s return code = %s, FILE = %s, LINE = %d"n />n", #func, retstr, __FILE__, __LINE__);  \
            }                                                                  \
            return(retcode);                                                   \
        }                                                                      \
    } while(0)

#endif /*  __MAKE_COMMAND_LIST__ */

extern uint32 glbIdList[];

#endif /* __SDKCMD_H__ */


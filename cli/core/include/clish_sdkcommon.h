/*
 * =====================================================================================
 *
 *       Filename:  clish_sdkcommon.h
 *
 *    Description:  Basic type for clish, startup node, root view
 *
 *        Version:  1.0
 *        Created:  11/21/2011 05:44:45 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Quan Cao Duc (), quancd@atvn.com.vn
 *        Company:  Arrive Technologies
 *
 * =====================================================================================
 */
#ifndef __CLISH_SDKCOMMON_H__
#define __CLISH_SDKCOMMON_H__

#define SDKCLISH_BASIC_PTYPE \
       "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" \
       "<CLISH_MODULE xmlns=\"http://clish.sourceforge.net/XMLSchema\" \n" \
       "          xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" \n" \
       " xsi:schemaLocation=\"http://clish.sourceforge.net/XMLSchema\n" \
       "                     http://clish.sourceforge.net/XMLSchema/clish.xsd\">\n" \
       "    <!--=======================================================-->\n" \
       "	<PTYPE name=\"VLAN_ID\" \n" \
       "        pattern=\"(409[0-5]|40[0-8][0-9]|[1-3][0-9]{3}|[1-9][0-9]{2}|[1-9][0-9]|[1-9])\"\n" \
       "           help=\"Number in the range 1-4095\"/>\n" \
       "	<!--=======================================================-->\n" \
       "	<PTYPE name=\"IP_ADDR\"\n" \
       "	    pattern=\"(((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))\"\n" \
       "	       help=\"IP address AAA.BBB.CCC.DDD where each part is in the range 0-255\"/>\n" \
       "	<!--=======================================================-->\n" \
       "	<PTYPE name=\"UINT\"\n" \
       "	    pattern=\"[0-9]+\"\n" \
       "	       help=\"Unsigned integer\"/>\n" \
       "	<!--=======================================================-->\n" \
       "	<PTYPE name=\"STRING\"\n" \
       "	    pattern=\"[^\\*]+\"\n" \
       "	       help=\"String\"/>\n" \
       "	<!--=======================================================-->\n" \
       "	<PTYPE name=\"BOOL\"\n" \
       "        method = select" \
       "	    pattern= \"true(1), false(0)\" \n" \
       "	       help=\"Boolean choice\"/>\n" \
       "    <!--=======================================================-->\n" \
       "    <PTYPE name=\"BYTE_HEX\"\n" \
       "        pattern= \"(0[xX]([0-9a-fA-F]{1,2}))\" \n" \
       "           help=\"A Byte in Hex format\"/>\n" \
       "    <!--=======================================================-->\n" \
       "    <PTYPE name=\"PATHNAME_STRING\"\n" \
       "        method = \"pathname\"" \
       "	    pattern=\".+\"\n" \
       "           help=\"Path name\"/>\n" \
       "    <!--=======================================================-->\n" \
       "</CLISH_MODULE>"


#define SDKCLISH_ROOT_VIEW \
       "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" \
       "<CLISH_MODULE xmlns=\"http://clish.sourceforge.net/XMLSchema\" \n" \
       "          xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" \n" \
       " xsi:schemaLocation=\"http://clish.sourceforge.net/XMLSchema\n" \
       "                     http://clish.sourceforge.net/XMLSchema/clish.xsd\">\n" \
       " 	<!--=======================================================-->\n" \
       "    <VIEW name=\"root-view\"\n" \
       "        prompt=\"AF6048 > \">\n" \
       "        <!--===================================================-->\n" \
       "        <COMMAND name=\"exit\"\n" \
       "                 help=\"Exit this CLI session\">\n" \
       "            <ACTION builtin=\"clish_close\"/>\n" \
       "        </COMMAND>\n" \
       "        <COMMAND name=\"help\"\n" \
       "                 help=\"Help message\">\n" \
       "            <ACTION builtin=\"clish_overview\"/>\n" \
       "        </COMMAND>\n" \
       "        <COMMAND name=\"ls\"\n" \
       "                 help=\"List the files in the current directory\">\n" \
       "            <ACTION>ls -l -a</ACTION> \n" \
       "        </COMMAND>\n" \
       "        <COMMAND name=\"ps\"\n" \
       "                 help=\"Give details of current processes\">\n" \
       "            <ACTION>ps</ACTION> \n" \
       "        </COMMAND>\n" \
       "        <COMMAND name=\"pwd\"\n" \
       "                 help=\"Show current working foler\">\n" \
       "            <ACTION>pwd</ACTION> \n" \
       "        </COMMAND>\n" \
       "        <!--===================================================-->\n" \
       "    </VIEW>\n" \
       " 	<!--=======================================================-->    \n" \
       "</CLISH_MODULE>"


#define SDKCLISH_START_UP  \
       "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" \
       "<CLISH_MODULE xmlns=\"http://clish.sourceforge.net/XMLSchema\" \n" \
       "          xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" \n" \
       " xsi:schemaLocation=\"http://clish.sourceforge.net/XMLSchema\n" \
       "                     http://clish.sourceforge.net/XMLSchema/clish.xsd\">\n" \
       " 	<!--=======================================================-->\n" \
       "	<OVERVIEW>\n" \
       "CONTEXT SENSITIVE HELP\n" \
       "[?] - Display context sensitive help. This is either a list of possible\n" \
       "      command completions with summaries, or the full syntax of the \n" \
       "      current command. A subsequent repeat of this key, when a command\n" \
       "      has been resolved, will display a detailed reference.\n" \
       "\n" \
       "AUTO-COMPLETION\n" \
       "The following keys both perform auto-completion for the current command line.\n" \
       "If the command prefix is not unique then the bell will ring and a subsequent\n" \
       "repeat of the key will display possible completions.\n" \
       "\n" \
       "[enter] - Auto-completes, syntax-checks then executes a command. If there is \n" \
       "          a syntax error then offending part of the command line will be\n" \
       "          highlighted and explained.\n" \
       "\n" \
       "[space] - Auto-completes, or if the command is already resolved inserts a space.\n" \
       "\n"\
       "MOVEMENT KEYS\n" \
       "[CTRL-A] - Move to the start of the line\n" \
       "[CTRL-E] - Move to the end of the line.\n" \
       "[up]     - Move to the previous command line held in history.\n" \
       "[down]   - Move to the next command line held in history.\n" \
       "[left]   - Move the insertion point left one character.\n" \
       "[right]  - Move the insertion point right one character.\n" \
       "\n" \
       "DELETION KEYS\n" \
       "[CTRL-C]    - Delete and abort the current line\n" \
       "[CTRL-D]    - Delete the character to the right on the insertion point.\n" \
       "[CTRL-K]    - Delete all the characters to the right of the insertion point.\n" \
       "[CTRL-U]    - Delete the whole line.\n" \
       "[backspace] - Delete the character to the left of the insertion point.\n" \
       "\n" \
       "SEARCHING KEYS\n" \
       "[CTRL-R]    - Backward/reverse search the history from current position\n" \
       "[CTRL-S]    - Forware search the history from current position.\n" \
       "\n" \
       "ESCAPE SEQUENCES\n" \
       "!!  - Subsitute the the last command line.\n" \
       "!N  - Substitute the Nth command line (absolute as per 'history' command)\n" \
       "!-N - Substitute the command line entered N lines before (relative)\n" \
       "    </OVERVIEW>\n" \
       " 	<!--=======================================================-->\n" \
       "	<STARTUP view=\"root-view\">\n" \
       "		<DETAIL>\n" \
       "********************************************\n" \
       "*         Arrive Technologies              *\n" \
       "*         Command Line SHELL               *\n" \
       "*             AF6048 SDK                   *\n" \
       "*                                          *\n" \
       "*      WARNING: Authorized Access Only     *\n" \
       "********************************************\n" \
       "        </DETAIL>\n" \
       "	</STARTUP>\n" \
       " 	<!--=======================================================-->\n" \
       "</CLISH_MODULE>"

#endif /*  __CLISH_SDKCOMMON_H__ */

/*
 * =====================================================================================
 *
 *       Filename:  tcl_sdkembed.h
 *
 *    Description:  Emded TCL to SDK
 *
 *        Version:  1.0
 *        Created:  12/02/2011 10:21:04 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Quan Cao Duc (), quancd@atvn.com.vn
 *        Company:  Arrive Technologies
 *
 * =====================================================================================
 */

#ifndef __EXT_ATSDK_INT_H__
#define __EXT_ATSDK_INT_H__
#include "clish/shell.h"

#include "at_listener.h"
#define cAtSdkTclEventCmdBegin    0x00000001U /*  Begin executing a SDK TCL command */
#define cAtSdkTclEventCmdEnd      0x00000002U /*  End executing a SDK TCL command */
#define cAtSdkTclEventScriptBegin 0x00000004U /*  Begin executing a SDK TCL script */
#define cAtSdkTclEventScriptEnd   0x00000008U /*  End executing a SDK TCL script */

#ifdef ENABLE_TCL_INTERP 
#include "tcl.h"

#define ATSDK_TCL_NAMESPACE_LEN (sizeof(ATSDK_TCL_NAMESPACE))
extern const char const ATSDK_TCL_NAMESPACE[];
#define TCL_MAX_COMMAND_LEN 256

typedef Tcl_ObjCmdProc Tcl_ObjCmdProc_t;
typedef struct Tcl_Subcmd_s {
    const char* usage;
    Tcl_ObjCmdProc_t *proc;
	int maxArgc;
	int minArgc;
} Tcl_Subcmd_t; 

#define DEF_TCL_SUBCMD(_usage,_proc,_min,_max) \
{\
	.usage=_usage,\
	.proc = _proc,\
	.maxArgc = _max,\
	.minArgc = _min, \
}

/*
 * Public events 
 */
#include "at_listener.h"
#define cAtSdkTclEventCmdBegin    0x00000001U /*  Begin executing a SDK TCL command */
#define cAtSdkTclEventCmdEnd      0x00000002U /*  End executing a SDK TCL command */
#define cAtSdkTclEventScriptBegin 0x00000004U /*  Begin executing a SDK TCL script */
#define cAtSdkTclEventScriptEnd   0x00000008U /*  End executing a SDK TCL script */
typedef struct AtSdkTclEvent_t {
	const char *command;
	const char *errMsg;
	int errCode;
}tAtSdkTclEvent;
extern int AtSdkTclEventRegister(AtEventListenerCallback_f event_cb, unsigned long evtMask);
/*  Unregister */
extern int AtSdkTclEventUnregister(AtEventListenerCallback_f event_cb, unsigned long evtMask);
void AtSdkCreateTclCommand(const char *cmd_name, Tcl_ObjCmdProc *proc);
void AtSdkDeleteTclCommand(const char *cmd_name);
void AtSdkTclSetIntegerResult(int value);

#endif /* ENABLE_TCL_INTERP */
extern int AtSdkTclInit(clish_shell_t *shell);
extern int AtSdkTclShutdown(void);
extern int AtSkdTclEvalFile(const char *filename);
extern int AtSkdTclEval(const char *cmd);
void AtSdkTclLibPathRem(const char *libPath);
void AtSdkTclLibPathAdd(const char *libPath);
int AtSdkTclEventRegister(AtEventListenerCallback_f event_cb, unsigned long evtMask);
void AtSdkTclSetIntegerResult(int value);

#define ARRAY_SIZE(_array) \
	sizeof(_array)/sizeof((_array)[0])

int AtClishSetup(void);
int AtClishStart(void);
int AtClishStop(void);

#endif /* __EXT_ATSDK_INT_H__ */


/*
 * =====================================================================================
 *
 *       Filename:  extShowtableInt.h
 *
 *    Description:  TCL Showtable extension
 *
 *        Version:  1.0
 *        Created:  12/14/2011 04:13:29 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Quan Cao Duc (), quancd@atvn.com.vn
 *        Company:  Arrive Technologies
 *
 * =====================================================================================
 */
#ifndef __EXT_SHOWTABLE_INT_H__
#define __EXT_SHOWTABLE_INT_H__

#ifdef ENABLE_TCL_INTERP
#include "tcl.h"
/*
 * Showtable extension
 */
extern int ExtShowtable_Init(Tcl_Interp *intrp, Tcl_Namespace *nsp);
extern int ExtShowtable_Shutdown(Tcl_Interp *intrp);
#endif /* ENABLE_TCL_INTERP */

#endif /* __EXT_SHOWTABLE_INT_H__ */


/*
 * =====================================================================================
 *
 *       Filename:  embedtcl.c
 *
 *    Description:  Embedding TCL languages
 *
 *        Version:  1.0
 *        Created:  12/02/2011 10:17:11 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Quan Cao Duc (), quancd@atvn.com.vn
 *        Company:  Arrive Technologies
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "clish/shell.h"
#include "extAtSdkInt.h"
#include "AtOsal.h"
#ifdef ENABLE_TCL_INTERP
#include <tcl.h>
#include "sdkcmd.h"
#include "extShowtableInt.h"
#include "commacro.h"

struct tcl_library_path_t {
    char **path; //Store list of tcl library path. Each path separated by :
    int size;
    int cache_ptr;
#define LIBRARY_PATH_ALLOC_STEP 10
    int alloc_size;
} G_TCL_LIBRARY_PATH;

extern Tcl_Obj*  Tcl_GetDirname(Tcl_Interp *interp, const char *fileName);
extern int Tcl_CheckDir(const char *fileName);
extern const char *TCL_COMMAND_LIST[];
int Tcl_CheckFileExist(const char *filename);
extern const char *TCL_COMMAND_LIST[];

/*
 * Global Interpreter
 */
const char const ATSDK_TCL_PACKAGE[] = "atsdk";
const char const ATSDK_TCL_NAMESPACE[] = "atsdk::";
const char PKG_CONFIG_NAME[] = "pkgIndex.tcl";

static Tcl_Interp *G_TCL_INTERP = NULL;
static Tcl_Namespace *G_TCL_NAMESP = NULL;
static clish_shell_t *G_CLISH_SHELL = NULL;

static int extAtSdk_cmd(ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[]); 
static int extSilent_cmd(ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[]);
static tAtEventListenerList *G_EVT_LIST;

/**
 * @brief Remove a path from this list
 *
 * @param libPath
 */
void AtSdkTclLibPathRem(const char *libPath)
{
    int i;

    for (i = 0; i < G_TCL_LIBRARY_PATH.size; i++) {
        if (!strcmp(G_TCL_LIBRARY_PATH.path[i], libPath)) {
            int j;

            //Move down
            free(G_TCL_LIBRARY_PATH.path[i]);
            G_TCL_LIBRARY_PATH.size--;
            for (j = i; j < G_TCL_LIBRARY_PATH.size; j++) {
                G_TCL_LIBRARY_PATH.path[j] = G_TCL_LIBRARY_PATH.path[j+1];
            }
        }
    }
}

/**
 * @brief Adding an entry to library path if this is not added.
 *
 * @param libPath
 */
#define MAX_SCRIPT_NAME 800
char g_scriptName[MAX_SCRIPT_NAME+1];
void AtSdkTclLibPathAdd(const char *libPath)
{
    int i;
    int len;
    int result;

    for (i = 0; i < G_TCL_LIBRARY_PATH.size; i++) {
        if (!strcmp(G_TCL_LIBRARY_PATH.path[i], libPath)) {
            return;
        }
    }
    if (G_TCL_LIBRARY_PATH.size >= G_TCL_LIBRARY_PATH.alloc_size) {
        char **newPathPtr;

        newPathPtr = realloc(G_TCL_LIBRARY_PATH.path, sizeof(*newPathPtr)*((size_t)G_TCL_LIBRARY_PATH.alloc_size + LIBRARY_PATH_ALLOC_STEP));
        if (newPathPtr) {
            G_TCL_LIBRARY_PATH.path = newPathPtr;
            G_TCL_LIBRARY_PATH.alloc_size += LIBRARY_PATH_ALLOC_STEP;
        } else {
            AtOsalTrace(cAtOsalTraceInfo, "Could not resize Tcl Library Path. Out of memory. \n");
            return;
        }
    }
    //pkgIndex.tcl on this folder.
    if (TCL_OK == Tcl_CheckDir(libPath)) {
        len = snprintf(g_scriptName, MAX_SCRIPT_NAME, "set dir \"%s\" \n source %s/%s", libPath, libPath, PKG_CONFIG_NAME);
        G_TCL_LIBRARY_PATH.path[G_TCL_LIBRARY_PATH.size++] = strdup(libPath);
    } else {
        Tcl_Obj *dirname = Tcl_GetDirname(G_TCL_INTERP, libPath);
        G_TCL_LIBRARY_PATH.path[G_TCL_LIBRARY_PATH.size++] = strdup(Tcl_GetString(dirname));
        len = snprintf(g_scriptName, MAX_SCRIPT_NAME, "set dir \"%s\" \n source %s/%s", Tcl_GetString(dirname), Tcl_GetString(dirname), PKG_CONFIG_NAME);
        Tcl_DecrRefCount(dirname);
    }
    if (len >= MAX_SCRIPT_NAME)
        len = MAX_SCRIPT_NAME;
    g_scriptName[len] = '\0';
    result = Tcl_Eval(G_TCL_INTERP, g_scriptName);
    if (result != TCL_OK) {
        AtOsalTrace(cAtOsalTraceInfo, "Auto source tcl file %s FAIL \n", g_scriptName);
    }
}



void AtSdkDeleteTclCommand(const char *cmd_name)
{
    char atsdk_cmd[TCL_MAX_COMMAND_LEN];

    snprintf(atsdk_cmd, TCL_MAX_COMMAND_LEN, "%s::%s", ATSDK_TCL_PACKAGE, cmd_name);
    Tcl_DeleteCommand(G_TCL_INTERP, atsdk_cmd);
}

void AtSdkCreateTclCommand(const char *cmd_name, Tcl_ObjCmdProc *proc)
{
    char atsdk_cmd[TCL_MAX_COMMAND_LEN];

    snprintf(atsdk_cmd, TCL_MAX_COMMAND_LEN, "%s::%s", ATSDK_TCL_PACKAGE, cmd_name);
    Tcl_CreateObjCommand(G_TCL_INTERP, atsdk_cmd, proc, (ClientData) NULL, (Tcl_CmdDeleteProc *) NULL);
    //Export this command to namespace
    Tcl_Export(G_TCL_INTERP, G_TCL_NAMESP, cmd_name, 0);
}

//#define DEBUG_INIT_TCL
#ifdef DEBUG_INIT_TCL

void DumpTclLibPath(void)
{
    int i;

    printf("List of TCL_LIBRARY: \n");
    for (i = 0; i < G_TCL_LIBRARY_PATH.size;i++) {
        printf("%s \n", G_TCL_LIBRARY_PATH.path[i]);
    }
    printf("End of TCL_LIBRARY\n");
}

void tclish_show_result(Tcl_Interp * interp)
{
    Tcl_Obj *obj = Tcl_GetObjResult(interp);
    int length;
    if (NULL != obj) {
        char *string = Tcl_GetStringFromObj(obj, &length);
        if (NULL != string) {
            printf("%s", string);
        }
    }
}
#endif /*  DEBUG_INIT_TCL */

static void internalSdkTclInit(Tcl_Interp *interp)
{
    /*
     * In order to find init.tcl during initialization, the following script
     * is invoked by Tcl_Init(). It looks in several different directories:
     *
     *  $tcl_library        - can specify a primary location, if set, no
     *                other locations will be checked. This is the
     *                recommended way for a program that embeds
     *                Tcl to specifically tell Tcl where to find
     *                an init.tcl file.
     *
     *  $env(TCL_LIBRARY)   - highest priority so user can always override
     *                the search path unless the application has
     *                specified an exact directory above
     *
     *  $tclDefaultLibrary  - INTERNAL: This variable is set by Tcl on
     *                those platforms where it can determine at
     *                runtime the directory where it expects the
     *                init.tcl file to be. After [tclInit] reads
     *                and uses this value, it [unset]s it.
     *                External users of Tcl should not make use of
     *                the variable to customize [tclInit].
     *
     *  $tcl_libPath        - OBSOLETE: This variable is no longer set by
     *                Tcl itself, but [tclInit] examines it in
     *                case some program that embeds Tcl is
     *                customizing [tclInit] by setting this
     *                variable to a list of directories in which
     *                to search.
     *
     *  [tcl::pkgconfig get scriptdir,runtime]
     *              - the directory determined by configure to be
     *                the place where Tcl's script library is to
     *                be installed.
     *
     * The first directory on this path that contains a valid init.tcl script
     * will be set as the value of tcl_library.
     *
     * Note that this entire search mechanism can be bypassed by defining an
     * alternate tclInit command before calling Tcl_Init().
     */
    Tcl_Eval(interp,
            "if {[namespace which -command tclInit] eq \"\"} {\n"
            "  proc tclInit {} {\n"
            "    global tcl_libPath tcl_library env tclDefaultLibrary\n"
            "    rename tclInit {}\n"
            "    if {[info exists tcl_library]} {\n"
            "   set scripts {{set tcl_library}}\n"
            "    } else {\n"
            "   set scripts {}\n"
            "   if {[info exists env(TCL_LIBRARY)] && ($env(TCL_LIBRARY) ne {})} {\n"
            "       lappend scripts {set env(TCL_LIBRARY)}\n"
            "       lappend scripts {\n"
            "if {[regexp ^tcl(.*)$ [file tail $env(TCL_LIBRARY)] -> tail] == 0} continue\n"
            "if {$tail eq [info tclversion]} continue\n"
            "file join [file dirname $env(TCL_LIBRARY)] tcl[info tclversion]}\n"
            "   }\n"
            "   if {[info exists tclDefaultLibrary]} {\n"
            "       lappend scripts {set tclDefaultLibrary}\n"
            "   } else {\n"
            "       lappend scripts {::tcl::pkgconfig get scriptdir,runtime}\n"
            "   }\n"
            "   lappend scripts {\n"
            "set parentDir [file dirname [file dirname [info nameofexecutable]]]\n"
            "set grandParentDir [file dirname $parentDir]\n"
            "file join $parentDir lib tcl[info tclversion]} \\\n"
            "   {file join $grandParentDir lib tcl[info tclversion]} \\\n"
            "   {file join $parentDir library} \\\n"
            "   {file join $grandParentDir library} \\\n"
            "   {file join $grandParentDir tcl[info patchlevel] library} \\\n"
            "   {\n"
            "file join [file dirname $grandParentDir] tcl[info patchlevel] library}\n"
            "   if {[info exists tcl_libPath]\n"
            "       && [catch {llength $tcl_libPath} len] == 0} {\n"
            "       for {set i 0} {$i < $len} {incr i} {\n"
            "       lappend scripts [list lindex \\$tcl_libPath $i]\n"
            "       }\n"
            "   }\n"
            "    }\n"
            "    set dirs {}\n"
            "    set errors {}\n"
            "    foreach script $scripts {\n"
            "   lappend dirs [eval $script]\n"
            "   set tcl_library [lindex $dirs end]\n"
            "   set tclfile [file join $tcl_library init.tcl]\n"
            "       return\n"
            "    }\n"
            "  }\n"
            "}\n"
            "tclInit");
    {
        //Get environment variable TCL_LIBRARY
        char *tcl_libray =  getenv("TCL_LIBRARY");
        char *s,*d;

        if (!tcl_libray) {
            return;
        }
        tcl_libray = strdup(tcl_libray);
        if (!tcl_libray)
            return;
        //The TCL_LIBRARY has many path separated by :
        d = s = tcl_libray;
        while(*s) {
            while(*d && *d != ':') {
                d++;
            }
            if (':' == *d) {
                *d = '\0';
                AtSdkTclLibPathAdd(s);
                d++;
                s = d;
            } else {
                AtSdkTclLibPathAdd(s);
                break;
            }
        }
        free(tcl_libray);
    }

#ifdef DEBUG_INIT_TCL
    tclish_show_result(interp);
#endif /* DEBUG_INIT_TCL */
}

/**
 * @brief 
 *
 * @param shell
 *
 * @return 0: on success, !0: on fail
 */
int AtSdkTclInit(clish_shell_t *shell)
{
    int cmd_idx;

    /*
     * Init TCL interpreter
     */
    G_CLISH_SHELL = shell;
    //Init event list
    G_EVT_LIST = AtEventListenerListCreate();
    AtAssert(G_EVT_LIST);
    G_TCL_INTERP = Tcl_CreateInterp();
    AtAssert(G_TCL_INTERP);
    /* initialise the memory for intepreter */
    Tcl_InitMemory(G_TCL_INTERP);
    internalSdkTclInit(G_TCL_INTERP);
    Tcl_Preserve(G_TCL_INTERP);

    //Create atsdk package
    AtAssert(Tcl_PkgProvideEx(G_TCL_INTERP, ATSDK_TCL_PACKAGE, TCL_PATCH_LEVEL, (ClientData)NULL) == TCL_OK);
    /*
     * Register our commands to extend TCL functions
     */
    cmd_idx = 0;
    //Create namespace atsdk
    G_TCL_NAMESP = Tcl_CreateNamespace(G_TCL_INTERP, ATSDK_TCL_NAMESPACE, NULL, (Tcl_NamespaceDeleteProc*)NULL);
    AtAssert(G_TCL_NAMESP);
    while(TCL_COMMAND_LIST[cmd_idx]){
        AtSdkCreateTclCommand(TCL_COMMAND_LIST[cmd_idx], extAtSdk_cmd);
        cmd_idx++;
    }
    //Add silent command
    AtSdkCreateTclCommand("silent", extSilent_cmd);
    //Add showtable command
    ExtShowtable_Init(G_TCL_INTERP, G_TCL_NAMESP);
    AtOsalTrace(cAtOsalTraceInfo, "Init TCL Interpreter DONE \n");
    return(0); 
}

static void initEvent(tAtSdkTclEvent *event)
{
    event->command = NULL;
    event->errCode = 0;
    event->errMsg = NULL;
}

int AtSkdTclEvalFile(const char *filename)
{
    int r;
    const char *errmsg = NULL;
    tAtSdkTclEvent event;

    //Verify that file is exist
    if (TCL_OK != Tcl_CheckFileExist(filename)) {
        if (AtCliModeGet() & cAtCliModeAutotest) {
            shell_printf("<Tcl><Error>\n"
                    "File %s does not exist. \n"
                    "</Error></Tcl>\n", filename);
        } else {
            shell_printf("File %s does not exist. \n", filename);
        }
        return(-1);
    }
    //Adding this path to library list
    AtSdkTclLibPathAdd(filename);
    initEvent(&event);
    AtCliModeSet(cAtCliModeTcl);
    event.command = filename;
    //Notify begin a script
    AtEventListenerNotify(G_EVT_LIST, cAtSdkTclEventScriptBegin, &event); 
    r = Tcl_EvalFile(G_TCL_INTERP, filename);
    if (TCL_OK != r){
        if (AtCliModeGet() & cAtCliModeAutotest) {
            shell_printf("<Tcl><Error>\n");
        }
        errmsg = Tcl_GetVar(G_TCL_INTERP, "errorInfo", (TCL_GLOBAL_ONLY | TCL_LEAVE_ERR_MSG));
        if (errmsg) {
            shell_printf("%s \n", errmsg);
        }
        else {
            shell_printf("Execute file %s FAIL \n", filename);
        }
        if (AtCliModeGet() & cAtCliModeAutotest) {
            shell_printf("</Error></Tcl>\n");
        }
    }
    event.errCode = r;
    event.errMsg = errmsg;
    //Notify end a script
    AtEventListenerNotify(G_EVT_LIST, cAtSdkTclEventScriptEnd, &event); 
    AtCliModeClr(cAtCliModeTcl);
    return(TCL_OK == r ? 0 : -1);
}

int AtSkdTclEval(const char *cmd)
{
    int r;

    AtCliModeSet(cAtCliModeTcl);
    r = Tcl_Eval(G_TCL_INTERP, cmd);
    AtCliModeClr(cAtCliModeTcl);
    return(TCL_OK == r ? 0 : -1);
}

int AtSdkTclShutdown(void)
{
    int cmd_idx;

    ExtShowtable_Shutdown(G_TCL_INTERP);
    /*
     * Delete all our commands to extend TCL functions
     */
    cmd_idx = 0;
    while(TCL_COMMAND_LIST[cmd_idx]){
        AtSdkDeleteTclCommand(TCL_COMMAND_LIST[cmd_idx]);
        cmd_idx++;
    }
    //Add silent command
    AtSdkDeleteTclCommand("silent");
    if (G_TCL_NAMESP) {
        Tcl_DeleteNamespace(G_TCL_NAMESP);
        G_TCL_NAMESP = NULL;
    }
    /* cleanup the TCL interpreter */
    if (G_TCL_INTERP) {
        (void)Tcl_Release(G_TCL_INTERP);
        (void)Tcl_DeleteInterp(G_TCL_INTERP);
        G_TCL_INTERP = NULL;
    }
    Tcl_Finalize();
    AtEventListenerListDestroy(G_EVT_LIST); 
    {
        //Free TCL Library
        int i;

        for (i = 0; i < G_TCL_LIBRARY_PATH.size; i++)
            free(G_TCL_LIBRARY_PATH.path[i]);
        free(G_TCL_LIBRARY_PATH.path);
    }
    AtOsalTrace(cAtOsalTraceInfo, "Destroy TCL Interpreter DONE \n");
    return(0);
}

//atsdk::silent [on|off]
int extSilent_cmd(ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[]) 
{
    Tcl_Obj * tcl_result;
	AtUnused(clientData);

    tcl_result = Tcl_GetObjResult(interp);
    switch(objc){
        case 1:
            {
                const char* silentMode = (AtCliModeGet() & cAtCliModeSilent) ? "on" : "off";

                Tcl_SetStringObj(tcl_result, silentMode, -1);
                shell_printf("%s\n", silentMode);
            }
            break;
        case 2:
            {
                if (!strcmp("on", Tcl_GetStringFromObj(objv[1], NULL))){
                    AtCliModeSet(cAtCliModeSilent);
                }
                else {
                    AtCliModeClr(cAtCliModeSilent);
                }
            }
            break;
        default:
            return(TCL_ERROR);
    }
    return(TCL_OK);
}

#define CLISH_EXEC_ERRCODE_GET(retcode) ((retcode) >> 16)
#define CLISH_CLI_ERRCODE_GET(retcode) ((retcode) & 0xFFFF)
static const char CLISH_EXEC_ERRMSG_PARTIAL[] = "CLISH execute ERROR: Partial command";
static const char CLISH_EXEC_ERRMSG_BAD_CMD[] = "CLISH execute ERROR: Bad command";
static const char CLISH_EXEC_ERRMSG_BAD_PARAM[] = "CLISH execute ERROR: Bad parameter";
static const char CLISH_EXEC_ERRMSG_BAD_HIS[] = "CLISH execute ERROR: Bad history";
int extAtSdk_cmd(ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[]) 
{
#define MAX_COMMAND_LINE_SIZE (1024 * 1024) /* 4KBs */
    Tcl_Obj * tcl_result;
    static char clish_cmd[MAX_COMMAND_LINE_SIZE];
    char *s;
    int cliret = 0;
    eAtCliMode cliMode = 0;
    int result = TCL_OK;
    int opId = 1; //Normaly, start at 1
    tAtSdkTclEvent event;
	AtUnused(clientData);

    //TODO: store clish context to ClientData so that we could call them here
    AtAssert(G_CLISH_SHELL);
    //AtOsalTrace(cAtOsalTraceInfo, "Entering function %s \n", __func__);
    tcl_result = Tcl_GetObjResult(interp);
    //TODO: we could improve this for other option when executing a command
    //Hook silent mode here
    cliMode = 0x0;
    if (objc >= 2 && !strcmp("--silent", Tcl_GetStringFromObj(objv[1], NULL))){
        if (!(cAtCliModeSilent & AtCliModeGet())){
            cliMode |= cAtCliModeSilent;
        }
        opId = 2;
    }
    AtCliModeSet(cliMode);
    //Build a command here
    clish_cmd[0] = '\0';
    s = clish_cmd;
    //Got the top command first
    s += sprintf(s, "%s", Tcl_GetStringFromObj(objv[0], NULL));
    for (; opId < objc; opId++){
        /* Check if buffer is enough */
        char *objectString = Tcl_GetStringFromObj(objv[opId], NULL);
        if ((strlen(clish_cmd) + strlen(objectString)) >= MAX_COMMAND_LINE_SIZE)
            {
            result = TCL_ERROR;
            goto out;
            }

        /* Buffer is enough, put this string */
        s += sprintf(s, " %s", objectString);
    }
    *s = '\0';
    //Strip off the namespace len
    if (strncmp(clish_cmd, ATSDK_TCL_NAMESPACE, ATSDK_TCL_NAMESPACE_LEN - 1)) {
        //Has been imported atsdk namespace
        s = clish_cmd;
    } else {
        //Not import atsdk namespace
        s = &clish_cmd[ATSDK_TCL_NAMESPACE_LEN - 1];
    }
    //AtOsalTrace(cAtOsalTraceInfo, "Build clish comamnd: %s \n", s);
    //Notify a new command is executed
    AtOsalMemInit(&event, 0, sizeof(event));
    event.command = clish_cmd;
    AtEventListenerNotify(G_EVT_LIST, cAtSdkTclEventCmdBegin, &event); 
    cliret = clish_shell_execute_tcline(G_CLISH_SHELL, s);
    if (cliret){
        if (CLISH_EXEC_ERRCODE_GET(cliret)){
            switch(CLISH_EXEC_ERRCODE_GET(cliret)){
                case CLISH_LINE_PARTIAL:
                    Tcl_AddObjErrorInfo(interp, CLISH_EXEC_ERRMSG_PARTIAL, sizeof(CLISH_EXEC_ERRMSG_PARTIAL)-1);
                    break;
                case CLISH_BAD_CMD:
                    Tcl_AddObjErrorInfo(interp, CLISH_EXEC_ERRMSG_BAD_CMD, sizeof(CLISH_EXEC_ERRMSG_BAD_CMD)-1);
                    break;
                case CLISH_BAD_PARAM:
                    Tcl_AddObjErrorInfo(interp, CLISH_EXEC_ERRMSG_BAD_PARAM, sizeof(CLISH_EXEC_ERRMSG_BAD_PARAM)-1);
                    break;
                case CLISH_BAD_HISTORY:
                    Tcl_AddObjErrorInfo(interp, CLISH_EXEC_ERRMSG_BAD_HIS, sizeof(CLISH_EXEC_ERRMSG_BAD_HIS)-1);
                    break;
                default:
                    break;
            }
            result = TCL_ERROR;
            goto out;
        }
        else {
            //TODO: we could get the detail result from command to here
#define MAX_ERRMSG_LEN 256
            char errmsg[MAX_ERRMSG_LEN];
            int len;

            len = snprintf(errmsg, MAX_ERRMSG_LEN, "CLI execute error, retcode=%d ", CLISH_CLI_ERRCODE_GET(cliret));
            Tcl_AddObjErrorInfo(interp, errmsg, len > MAX_ERRMSG_LEN ? MAX_ERRMSG_LEN : len);
        }
    }
    Tcl_SetIntObj(tcl_result, cliret ? TCL_ERROR : TCL_OK);
out:
    //Notify end command
    AtOsalMemInit(&event, 0, sizeof(event));
    event.command = clish_cmd;
    if (cliret == -1)
        event.errMsg = Tcl_GetVar(G_TCL_INTERP, "errorInfo", (TCL_GLOBAL_ONLY | TCL_LEAVE_ERR_MSG));
    event.errCode = cliret;
    AtEventListenerNotify(G_EVT_LIST, cAtSdkTclEventCmdEnd, &event); 
    AtCliModeClr(cliMode);
    return result;
}

int AtSdkTclEventRegister(AtEventListenerCallback_f event_cb, unsigned long evtMask)
{
    return(AtEventListenerRegister(G_EVT_LIST, event_cb, evtMask));
}

int AtSdkTclEventUnregister(AtEventListenerCallback_f event_cb, unsigned long evtMask)
{
    return(AtEventListenerUnregister(G_EVT_LIST, event_cb, evtMask));
}


void AtSdkTclSetIntegerResult(int value)
    {
    Tcl_Obj * tcl_result = Tcl_GetObjResult(G_TCL_INTERP);
    Tcl_SetIntObj(tcl_result, value);
    }

#else

void AtSdkTclSetIntegerResult(int value)
    {
    AtUnused(value);
    }

int AtSdkTclEventRegister(AtEventListenerCallback_f event_cb, unsigned long evtMask)
    {
    AtUnused(event_cb);
    AtUnused(evtMask);
    return (0);
    }

int AtSdkTclInit(clish_shell_t *shell)
{
    AtUnused(shell);
    return(0);
}

int AtSdkTclShutdown(void)
{
    return(0);
}
int AtSkdTclEvalFile(const char *filename)
{
    AtUnused(filename);
    return(0);
}
int AtSkdTclEval(const char *cmd)
{
    AtUnused(cmd);
    return(0);
}

#endif


/*
 * =====================================================================================
 *
 *       Filename:  extShowtable.c
 *
 *    Description:  An extension of TCL for showtable module
 *
 *        Version:  1.0
 *        Created:  12/14/2011 03:51:20 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Quan Cao Duc (), quancd@atvn.com.vn
 *        Company:  Arrive Technologies
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "clish/shell.h"
#include "AtOsal.h"
#include "commacro.h"
#include "showtable.h"
extern tTab* GlobalTableGet(void);
extern tTab* TableClone(tTab *pTab);

#ifdef ENABLE_TCL_INTERP
#include <tcl.h>
#include "sdkcmd.h"
#include "extAtSdkInt.h"
#include "extShowtableInt.h"

#define DEFAULT_TABLE_LIST_SIZE 10
#define MAX_CELL_SIZE 80

typedef struct ShowtableList_s {
    size_t size;  /*  Real size of allocated array */
    size_t count; /*  Number of valid table. < size */
    tTab **tabList; /* Point to an array of table */
} ShowtableList_t;
static const tTab *CURRENT_PRINT_TABLE;

/*
 * Supported command
 */
static ShowtableList_t G_TABLE_LIST;

static void removeTable(tTab *pTab)
{
    //A table is destroy, just remove it from the list
    size_t i;

    //TODO: adding unittest for this extension
    //printf("Remove table %lu \n", (unsigned long)pTab);
    //Ignore the global table
    for (i = 1; i < G_TABLE_LIST.count; i++){
        if (pTab == G_TABLE_LIST.tabList[i]){
            G_TABLE_LIST.tabList[i] = G_TABLE_LIST.tabList[G_TABLE_LIST.count - 1];
            G_TABLE_LIST.count--;
            G_TABLE_LIST.tabList[G_TABLE_LIST.count] = NULL;
            TableFree(pTab);
            return;
        }
    }
}

//An hook for begin and finish execution a command from TCL sdk
static void tclCmdScriptHook(unsigned long evtMask, tAtSdkTclEvent *cmdEvt)
{
	AtUnused(cmdEvt);
    if (evtMask & cAtSdkTclEventCmdBegin) {
        CURRENT_PRINT_TABLE = NULL;
    }
    if (evtMask & cAtSdkTclEventScriptEnd){
        while(G_TABLE_LIST.count > 1){
            removeTable(G_TABLE_LIST.tabList[1]);
        }
    }
}

static void showtablePrintEvtHook(unsigned long evtMask, const tTab *pTab)
{
    //We got notify that a new table is printed. User could call atsdk:showtable clone to got this new table
    if (!(evtMask & cShowtableEventPrint))
        return;
    CURRENT_PRINT_TABLE = pTab;
}

/**
 * @brief Find a table from known id
 *
 * @param tableId
 *
 * @return 
 */
static tTab* findTable(unsigned long tableId)
{
    size_t i;

    //If tableId is zero, we use the GLOBAL 
    //TODO: adding unit-test for this extension
    //printf("findTable %lu \n", tableId);
    if (!tableId)
        return(G_TABLE_LIST.tabList[0]);
    for (i = 1; i < G_TABLE_LIST.count; i++){
        if (tableId == (unsigned long)G_TABLE_LIST.tabList[i])
            return(G_TABLE_LIST.tabList[i]);
    }
    return(NULL);
}

static tTab* helperGetTable(Tcl_Interp *interp, Tcl_Obj *tcl_obj)
{
    unsigned long tableId = 0;
    tTab *result;
    int ret;

    ret = Tcl_GetLongFromObj(interp, tcl_obj, (long*)&tableId);
    if (TCL_OK != ret){
        Tcl_AddObjErrorInfo(interp, "Expect table id is a number", -1);
        return(NULL);
    }
    result = findTable(tableId);
    if (!result){
        char error[80];

        snprintf(error, 80, " Table id 0x%lx is not valid ", tableId);
        Tcl_AddObjErrorInfo(interp, error, -1);
        return(NULL);
    }
    return(result);
}

//TODO: adding support to automatically destroy table when a variable come out of its scope
static int helperDestroyTable(Tcl_Interp *interp, Tcl_Obj *tcl_obj)
{
    unsigned long tableId = 0;
    tTab *result;
    int ret;

    ret = Tcl_GetLongFromObj(interp, tcl_obj, (long*)&tableId);
    if (TCL_OK != ret){
        Tcl_AddObjErrorInfo(interp, "Expect table id is a number", -1);
        return(-1);
    }
    result = findTable(tableId);
    if (!result){
        char error[80];

        snprintf(error, 80, " Table id 0x%lx is not valid ", tableId);
        Tcl_AddObjErrorInfo(interp, error, -1);
        return(-1);
    }
    removeTable(result);
    return(0);
}

static tTab* helperCloneTable(Tcl_Interp *interp, tTab *pTab)
{
    tTab *cloneTab;

    //Ensure enough space for new entry
    if (G_TABLE_LIST.count >= G_TABLE_LIST.size){
        //Time to extend this table
        tTab **newPtr;
        size_t newSize = G_TABLE_LIST.size + DEFAULT_TABLE_LIST_SIZE;
        size_t oldSize = G_TABLE_LIST.size;

        newPtr = realloc(G_TABLE_LIST.tabList, sizeof(*G_TABLE_LIST.tabList)*newSize);
        if (!newPtr)
            return(NULL);
        G_TABLE_LIST.tabList = newPtr;
        G_TABLE_LIST.size = newSize;
        AtOsalMemInit(newPtr + sizeof(*G_TABLE_LIST.tabList)*oldSize, 0, sizeof(*G_TABLE_LIST.tabList)*DEFAULT_TABLE_LIST_SIZE);
    }
    cloneTab = TableClone(pTab);
    if (!cloneTab){
        Tcl_AddObjErrorInfo(interp, " Cloning table fail. Out of memory ", -1);
        return(NULL);
    }
    G_TABLE_LIST.tabList[G_TABLE_LIST.count] = cloneTab;
    G_TABLE_LIST.count++;
    return(cloneTab);
}

//atsdk::table row <tableid> ## Get number of row
//atsdk::table col <tableid>  ## Get number of column
static int extSize_cmd(ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[]) 
{
    Tcl_Obj * tcl_result;
    tTab *pTab;
    uint32 rowCount;
    uint32 colCount;
    int size;
	AtUnused(clientData);

    pTab = helperGetTable(interp, objv[2]);
    if (!pTab){
        return(TCL_ERROR);
    }
    if(!TableSizeGet(pTab, &rowCount, &colCount))
    {
        return(TCL_ERROR);
    }
    if (!strcmp("row", Tcl_GetStringFromObj(objv[1], NULL))){
        size = (int)rowCount;
    }
    else {
        size = (int)colCount;
    }
    tcl_result = Tcl_GetObjResult(interp);
    //Hard-code here
    if(4 == objc ){
        Tcl_Obj *size_val;
        //Set this to variable
        size_val = Tcl_NewIntObj(size);
        Tcl_ObjSetVar2(interp, objv[3], NULL, size_val, 0);
    } else {
        printf("%u\n", size);
    }
    Tcl_SetIntObj(tcl_result, size);
    return(TCL_OK);
}

static int helperGetCellIndex(Tcl_Interp *interp, tTab *pTab, uint32 rowCount, uint32 colCount, Tcl_Obj * CONST rowObj, Tcl_Obj * CONST colObj, int *_row, int *_col)
{
    int row = 0;
    int col = 0;
    int result;

    if (rowObj) {
        result = Tcl_GetIntFromObj(interp, rowObj, &row);
        if (TCL_OK != result){
            Tcl_AddObjErrorInfo(interp, "Expect row id is a number", -1);
            return(TCL_ERROR);
        }
    }
    if (colObj) {
        col = TableColumnIdGet(pTab, Tcl_GetStringFromObj(colObj, NULL));
    }
    if (row >= (int)rowCount || row < 0 || col < 0){
        char error[80];

        if (col < 0) {
            snprintf(error, 80, "Column \'%s\' is not a valid column name \n", Tcl_GetStringFromObj(colObj, NULL));
        }
        else {
            snprintf(error, 80, "Table is out of range size=(%u,%u) while require cell[%d,%d]", rowCount, colCount, row, col);
        }
        Tcl_AddObjErrorInfo(interp, error, -1);
        return(TCL_ERROR);
    }
    if (_row)
        *_row = row;
    if (_col)
        *_col = col;
    return(TCL_OK);
}

//atsdk::table value <tableid> <row id> <col id> ## Return value at cell id <row> <col>
static int extValue_cmd(ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[])
{
    Tcl_Obj * tcl_result;
    int row;
    int col;
    tTab *pTab;
    uint32 colCount;
    uint32 rowCount;
    Tcl_Obj *tcl_value;
	AtUnused(clientData);

    pTab = helperGetTable(interp, objv[2]);
    if (!pTab){
        return(TCL_ERROR);
    }
    if(!TableSizeGet(pTab, &rowCount, &colCount))
    {
        return(TCL_ERROR);
    }
    tcl_result = Tcl_GetObjResult(interp);
    if (TCL_OK != helperGetCellIndex(interp, pTab, rowCount, colCount, objv[3], objv[4], &row, &col)){
        return(TCL_ERROR);
    }
    tcl_value = Tcl_NewStringObj(TableCellStringGet(pTab, (uint32)row, (uint32)col), -1);
    if(6 == objc ){
        //Set this to variable
        Tcl_ObjSetVar2(interp, objv[5], NULL, tcl_value, 0);
    } else {
        printf("%s\n", TableCellStringGet(pTab, (uint32)row, (uint32)col));
    }
    Tcl_SetStringObj(tcl_result, TableCellStringGet(pTab, (uint32)row, (uint32)col), -1);
    return(TCL_OK);

}

//atsdk::table foreach <tableid> ?var_row ?var_col ?var_value script ## Go for each row,col and do the script
static int extForeach_cmd(ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[])
{
    int row, col;
    Tcl_Obj *rowObj;
    Tcl_Obj *colObj;
    Tcl_Obj *valObj;
    const char *script;
    int result;
    tTab *pTab;
    uint32 colCount;
    uint32 rowCount;
	AtUnused(objc);
	AtUnused(clientData);

    pTab = helperGetTable(interp, objv[2]);
    if (!pTab){
        return(TCL_ERROR);
    }
    if(!TableSizeGet(pTab, &rowCount, &colCount))
    {
        return(TCL_ERROR);
    }
    rowObj = Tcl_NewIntObj(0);
    colObj = Tcl_NewIntObj(0);
    valObj = Tcl_NewStringObj("", -1);
    AtAssert(rowObj != NULL && colObj != NULL && valObj != NULL);
    script = Tcl_GetStringFromObj(objv[6], NULL);
    for (row = 0; row < (int)rowCount; row++){
        for (col = 0; col < (int)colCount; col++){
            //NOTE: we could not call Tcl_SetIntObj to share object
            Tcl_SetIntObj(rowObj, row);
            Tcl_SetStringObj(colObj, TableColumnNameGet(pTab, col), -1);
            Tcl_SetStringObj(valObj, TableCellStringGet(pTab, (uint32)row, (uint32)col)  , -1);
            Tcl_ObjSetVar2(interp, objv[3], NULL, rowObj, 0);
            Tcl_ObjSetVar2(interp, objv[4], NULL, colObj, 0);
            Tcl_ObjSetVar2(interp, objv[5], NULL, valObj, 0);
            result = Tcl_Eval(interp, script);
            switch(result) {
                case TCL_OK:
                case TCL_CONTINUE:
                    result = TCL_OK;
                    break;
                case TCL_BREAK:
                    return(TCL_OK);
                case TCL_RETURN:
                    return(TCL_RETURN);
                default:
                    Tcl_AppendObjToErrorInfo(interp, Tcl_ObjPrintf(
                                "\n    (\"foreach\" body line %d)",
                                interp->errorLine));
                    return(TCL_ERROR);
            }
        }
    }
    return(TCL_OK);
}

//atsdk::table destroy <table_id>
static int extDestroy_cmd(ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[])
{
	AtUnused(clientData);
	AtUnused(objc);
    if (helperDestroyTable(interp, objv[2]))
        return(TCL_ERROR);
    return(TCL_OK);
}

//atsdk::table getrow ?<tableid>? ?rowid? ?listname?
static int extGetSizeList_cmd(ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[])
{
    tTab *pTab;
    uint32 colCount;
    uint32 rowCount;
    uint32 colMin = 0;
    uint32 rowMin = 0;
    int row, col;
    Tcl_Obj *cellObj;
    Tcl_Obj *listObj = NULL;
	AtUnused(clientData);
	AtUnused(objc);
	AtUnused(clientData);

    pTab = helperGetTable(interp, objv[2]);
    if (!pTab){
        return(TCL_ERROR);
    }
    if(!TableSizeGet(pTab, &rowCount, &colCount))
    {
        return(TCL_ERROR);
    }
    if (!strcmp("getrow", Tcl_GetStringFromObj(objv[1], NULL))){
        //Get one row
        if (TCL_ERROR == helperGetCellIndex(interp, pTab, rowCount, colCount, objv[3], NULL, &row, NULL)){
            return(TCL_ERROR);
        }
        rowMin = (uint32)row;
        rowCount = (uint32)(row + 1);
    } else {
        //Get one col
        if (TCL_ERROR == helperGetCellIndex(interp, pTab, rowCount, colCount, NULL, objv[3], NULL, &col)){
            return(TCL_ERROR);
        }
        colMin = (uint32)col;
        colCount = (uint32)(col + 1);
    }
    if ((5 == objc)) {
        //Unset the variable first
        Tcl_UnsetVar(interp, Tcl_GetStringFromObj(objv[4], NULL), 0);
    } 
    //Create a new list object
    listObj = Tcl_NewListObj(0, NULL); 
    AtAssert(listObj);
    for (row = (int)rowMin; row < (int)rowCount; row++){
        for (col = (int)colMin; col < (int)colCount; col++){
            cellObj = Tcl_NewStringObj(TableCellStringGet(pTab, (uint32)row, (uint32)col), -1);
            AtAssert(cellObj);
            Tcl_ListObjAppendElement(interp, listObj, cellObj);
        }
    }
    if (5 == objc) {
        Tcl_ObjSetVar2(interp, objv[4], NULL, listObj, 0);
    }
    Tcl_SetObjResult(interp, listObj);
    return(TCL_OK);
}

//atsdk::table destroy <table_id>
static int extConvert_cmd(ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[])
{
    tTab *pTab;
    uint32 colCount;
    uint32 rowCount;
    int row, col;
    Tcl_Obj *cellObjs[2];
    Tcl_Obj *listObj = NULL;
    char cellVal[MAX_CELL_SIZE];
    int len;
	AtUnused(clientData);

    pTab = helperGetTable(interp, objv[2]);
    if (!pTab){
        return(TCL_ERROR);
    }
    if(!TableSizeGet(pTab, &rowCount, &colCount))
    {
        return(TCL_ERROR);
    }
    if ((4 == objc)) {
        //Unset the variable first
        Tcl_UnsetVar(interp, Tcl_GetStringFromObj(objv[3], NULL), 0);
    } 
    //Create a new list object
    listObj = Tcl_NewListObj(0, NULL); 
    AtAssert(listObj);
    for (row = 0; row < (int)rowCount; row++){
        for (col = 0; col < (int)colCount; col++){
            len = snprintf(cellVal, MAX_CELL_SIZE, "%d.%s", row, TableColumnNameGet(pTab, col));
            if (len >= MAX_CELL_SIZE)
                len = MAX_CELL_SIZE - 1;
            cellObjs[0] = Tcl_NewStringObj(cellVal, len);
            cellObjs[1] = Tcl_NewStringObj(TableCellStringGet(pTab, (uint32)row, (uint32)col), -1);
            AtAssert(cellObjs[0] != NULL && cellObjs[1] != NULL);
            Tcl_ListObjAppendElement(interp, listObj, cellObjs[0]);
            Tcl_ListObjAppendElement(interp, listObj, cellObjs[1]);
        }
    }
    if (4 == objc) {
        Tcl_ObjSetVar2(interp, objv[3], NULL, listObj, 0);
    }
    Tcl_SetObjResult(interp, listObj);
    return(TCL_OK);
}

const char CLONE_ERRMSG[] = " No table available ";
//atsdk::table cloneglobal ?table_id_var
/**
 * @brief Return a table id cloning from global table
 *
 * @param clientData
 * @param interp
 * @param objc
 * @param objv[]
 *
 * @return 
 */
static int extCloneGlobal_cmd(ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[])
{
    tTab *pTab;
    Tcl_Obj *tcl_result;
    Tcl_Obj *cloneTab;
	AtUnused(objc);
	AtUnused(clientData);

    tcl_result = Tcl_GetObjResult(interp);
    //Check if previous command has created a new table
    if (!CURRENT_PRINT_TABLE){
        Tcl_SetStringObj(tcl_result, CLONE_ERRMSG, sizeof(CLONE_ERRMSG) - 1);
        return(TCL_ERROR);
    } 
    pTab = helperCloneTable(interp, findTable(0)); 
    if (!pTab)
        return(TCL_ERROR);
    cloneTab = Tcl_NewLongObj((long)pTab);
    Tcl_ObjSetVar2(interp, objv[2], NULL, cloneTab, 0);
    return(TCL_OK);
}

static const char *subCommands[] = {"rowSize", "columnSize", "cellContent", "foreach", "cloneGlobalTable", "destroyTable", "convert", "row", "column", NULL};
static Tcl_Subcmd_t SHOWTABLE_SUBCMD_LIST[] = {
    DEF_TCL_SUBCMD("rowSize ?<tableid>? ?var_row_size?", extSize_cmd, 3, 4),
    DEF_TCL_SUBCMD("columnSize ?<tableid>? ?var_col_size?", extSize_cmd, 3, 4),
    DEF_TCL_SUBCMD("cellContent ?<tableid>? ?var_row? ?var_col? ?var_value?", extValue_cmd, 5, 6),
    DEF_TCL_SUBCMD("foreach ?<tableid>? ?var_row? ?var_col? ?var_value? ?script? ", extForeach_cmd, 7, 7), 
    DEF_TCL_SUBCMD("cloneGlobalTable ?var_table_id?", extCloneGlobal_cmd, 3, 3), //Clone global table to new table
    DEF_TCL_SUBCMD("destroyTable ?<tableid>?", extDestroy_cmd, 3, 3), //Destroy a table
    DEF_TCL_SUBCMD("convert ?<tableid>? ?listname?", extConvert_cmd, 3, 4), //Convert a table to a list with pattern {0.colname0 cell_val 0.colname1 ... 1.colname0 cell_val 1.colnam1 cell_val ...}
    DEF_TCL_SUBCMD("row ?<tableid>? ?rowid? ?listname?", extGetSizeList_cmd, 4, 5), //Get all value from a row list to a list
    DEF_TCL_SUBCMD("column ?<tableid>? ?col_name? ?listname?", extGetSizeList_cmd, 4, 5), //Get all value from a row list to a list
};

//Wrapper command
static int extShowtable_cmd(ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[])
{
    int cmdnum;
    int result;

    if (objc < 3){
        Tcl_WrongNumArgs(interp, 1, objv, "subcommand <tableid> ?options?");
        return(TCL_ERROR);
    }
    result = Tcl_GetIndexFromObj(interp, objv[1], subCommands, "subcommand", TCL_EXACT, &cmdnum);
    /*
     * If the result is not TCL_OK, then the error message is
     * already in the Tcl Interpreter, this code can
     * immediately return.
     */
    if (result != TCL_OK) {
        return TCL_ERROR;
    }
    if (SHOWTABLE_SUBCMD_LIST[cmdnum].maxArgc < objc || SHOWTABLE_SUBCMD_LIST[cmdnum].minArgc > objc){
        Tcl_WrongNumArgs(interp, 1, objv, SHOWTABLE_SUBCMD_LIST[cmdnum].usage);
        return TCL_ERROR;
    }
    return(SHOWTABLE_SUBCMD_LIST[cmdnum].proc(clientData, interp, objc, objv));
}

int ExtShowtable_Init(Tcl_Interp *intrp, Tcl_Namespace *nsp)
{
	AtUnused(nsp);
    /*
     * Allocate table list
     */
    G_TABLE_LIST.tabList = calloc(DEFAULT_TABLE_LIST_SIZE, sizeof(*G_TABLE_LIST.tabList));
    AtAssert(G_TABLE_LIST.tabList);
    G_TABLE_LIST.size = DEFAULT_TABLE_LIST_SIZE;
    G_TABLE_LIST.tabList[0] = GlobalTableGet(); 
    AtAssert(G_TABLE_LIST.tabList[0]);
    G_TABLE_LIST.count = 1;
    AtAssert(intrp);
    AtSdkCreateTclCommand("table", extShowtable_cmd);

    //Hooking to receive new, delete event of table
    AtAssert(!ShowtableEventRegister((AtEventListenerCallback_f)showtablePrintEvtHook, cShowtableEventPrint));
    //Hooking to receive beginning executing a command
    AtAssert(!AtSdkTclEventRegister((AtEventListenerCallback_f)tclCmdScriptHook, cAtSdkTclEventCmdBegin|cAtSdkTclEventScriptEnd));
    return(0);
}

int ExtShowtable_Shutdown(Tcl_Interp *intrp)
{
    size_t i;
    AtUnused(intrp);

    AtAssert(!AtSdkTclEventUnregister((AtEventListenerCallback_f)tclCmdScriptHook, cAtSdkTclEventCmdBegin|cAtSdkTclEventScriptEnd));
    AtAssert(!ShowtableEventUnregister((AtEventListenerCallback_f)showtablePrintEvtHook, cShowtableEventPrint));
    for (i = 0; i < G_TABLE_LIST.size; i++){
        if (G_TABLE_LIST.tabList[i]){
            TableFree(G_TABLE_LIST.tabList[i]);
        }
    }
    free(G_TABLE_LIST.tabList);
    G_TABLE_LIST.count = 0;
    G_TABLE_LIST.size = 0;
    //Delete command
    AtSdkDeleteTclCommand("table");

    return(0);
}
#endif /* ENABLE_TCL_INTERP */


/*
 * =====================================================================================
 *
 *       Filename:  shell_helper.c
 *
 *    Description:  Helper function to setup pre-build clish
 *
 *        Version:  1.0
 *        Created:  12/12/2011 02:54:23 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Quan Cao Duc (), quancd@atvn.com.vn
 *        Company:  Arrive Technologies
 *
 * =====================================================================================
 */

#include "clish/shell.h"
#include "sdkcmd.h"
#include "showtable.h"
#include "extAtSdkInt.h"

#include "AtOsal.h"

/*
 * There are external data structures which are auto-generated
 */
extern clish_shell_t *ClishCreate(void);
extern int ClishStart(void);
extern int ClishStop(void);

int tinyrl_vprintf(const tinyrl_t * this, const char *fmt, va_list args);

/*
 * All private state for cli should be defined here
 */
static clish_shell_t* m_shell = NULL;

int shell_printf(const char *format, ...)
{
    va_list vl;
    int ret;

    va_start(vl, format);
    ret = tinyrl_vprintf(clish_shell__get_tinyrl(m_shell), format, vl);
    va_end(vl);
    return(ret);
}

int AtClishSetup(void)
    {
    int ret = 0;

    m_shell = ClishCreate();

    /* More than one Text-UI can use show table lib, it needs to be checked
     * before initializing */
    if (!ShowTableLibIsInitialized())
        ShowtableLibInit();

    ret |= AtSdkTclInit(m_shell);

    return ret;
    }

int AtClishStart(void)
    {
    return ClishStart();
    }

int AtClishStop(void)
    {
    AtSdkTclShutdown();
    ClishStop();
    ShowtableLibShutdown();

    return 0;
    }



include ${ATSDK_HOME}/Rules.Make

# ==============================================================================
# Public
# ==============================================================================
AT_CLI_LIB = ${BUILD_DIR}/atcli.o

.PHONY: all atcli atclitable cleanatcli cleancmdgenfiles atclimanual

# ==============================================================================
# Private
# ==============================================================================
CLI_LIB_DIR = ${CLI_DIR}/libs
CLI_CFLAGS  = 
CLI_ALL_SRC =
GEN_CMD_SRC_FILE  = no

# ==============================================================================
# Standard Text-UI
# ==============================================================================
TEXTUI_DIR = ${CLI_LIB_DIR}/text-ui

# Get objects
CLI_ALL_CMD_SRC       = ${CLI_DIR}/atsdkcli.c
CLI_ALL_CMD_HEADER    = ${CLI_DIR}/atsdkcli.h
TEXTUI_ALL_SRC_FILES  = ${TEXTUI_DIR}/src/AtTextUI.c
TEXTUI_ALL_SRC_FILES += ${TEXTUI_DIR}/src/AtTinyTextUI.c
TEXTUI_ALL_SRC_FILES += ${TEXTUI_DIR}/src/AtDefaultTinyTextUI.c
TEXTUI_ALL_SRC_FILES += $(shell find ${TEXTUI_DIR}/util -iname "*.c")
CLI_ALL_SRC 	     += ${TEXTUI_ALL_SRC_FILES}
CLI_ALL_SRC 	     += ${CLI_ALL_CMD_SRC}

# Additional include
CLI_CFLAGS += -I${TEXTUI_DIR}/include -I${CLI_LIB_DIR}/include -I${CLI_DIR}/include -I${CLI_LIB_DIR}/attcl/include

# ==============================================================================
# Configuration file generator
# ==============================================================================
ifeq (${GEN_CMD_SRC_FILE},yes)
CMD_CONF_GEN      = ${BUILD_DIR}/cmdconfgen
CMD_MANUAL_GEN    = ${ATSDK_HOME}/tools/converter/cli2md.py
CMD_CONF_GEN_SRC  = ${TEXTUI_DIR}/confgen/cmdconfgen.c ${TEXTUI_DIR}/util/cmdutil.c
CMD_CONF_GEN_SRC += $(shell find ${PLATFORM_DIR}/osal -iname "*.c")
CMD_CONF_GEN_SRC += $(shell find ${PLATFORM_DIR}/stuff/osal -iname "*.c")
CMD_CONF_GEN_SRC += $(shell find ${UTIL_DIR} -iname "*.c")
CMD_CONF_GEN_SRC += $(shell find ${CLI_LIB_DIR}/idparser -iname "*.c")
CMD_CONF_GEN_SRC += $(shell find ${DRIVER_DIR}/src/util -iname "*.c")
CMD_CONF_GEN_SRC += ${DRIVER_DIR}/src/generic/common/AtObject.c
endif

# ==============================================================================
# Get AT CLI object files
# ==============================================================================
# TODO: Add CLI configuration files here
AT_CMD_ALL_SRC_FILES  = $(shell find ${CLI_DIR}/cmd/common -iname "*.c")
AT_CMD_ALL_SRC_FILES += $(shell find ${CLI_DIR}/cmd/ber -iname "*.c")
AT_CMD_ALL_SRC_FILES += $(shell find ${CLI_DIR}/cmd/customers -iname "*.c")
AT_CMD_ALL_SRC_FILES += $(shell find ${CLI_DIR}/cmd/eth -iname "*.c")
AT_CMD_ALL_SRC_FILES += $(shell find ${CLI_DIR}/cmd/pdh -iname "*.c")
AT_CMD_ALL_SRC_FILES += $(shell find ${CLI_DIR}/cmd/ppp -iname "*.c")
AT_CMD_ALL_SRC_FILES += $(shell find ${CLI_DIR}/cmd/man -iname "*.c")
AT_CMD_ALL_SRC_FILES += $(shell find ${CLI_DIR}/cmd/encap -iname "*.c")
AT_CMD_ALL_SRC_FILES += $(shell find ${CLI_DIR}/cmd/pw -iname "*.c")
AT_CMD_ALL_SRC_FILES += $(shell find ${CLI_DIR}/cmd/ram -iname "*.c")
AT_CMD_ALL_SRC_FILES += $(shell find ${CLI_DIR}/cmd/platform -iname "*.c")
AT_CMD_ALL_SRC_FILES += $(shell find ${CLI_DIR}/cmd/sdh -iname "*.c")
AT_CMD_ALL_SRC_FILES += $(shell find ${CLI_DIR}/cmd/debug -iname "*.c")
AT_CMD_ALL_SRC_FILES += $(shell find ${CLI_DIR}/cmd/reg -iname "*.c")
AT_CMD_ALL_SRC_FILES += $(shell find ${CLI_DIR}/cmd/diag -iname "*.c")
AT_CMD_ALL_SRC_FILES += $(shell find ${CLI_DIR}/cmd/pktanalyzer -iname "*.c")
AT_CMD_ALL_SRC_FILES += $(shell find ${CLI_DIR}/cmd/physical -iname "*.c")
AT_CMD_ALL_SRC_FILES += $(shell find ${CLI_DIR}/cmd/clock -iname "*.c")
AT_CMD_ALL_SRC_FILES += $(shell find ${CLI_DIR}/cmd/prbs -iname "*.c")
AT_CMD_ALL_SRC_FILES += $(shell find ${CLI_DIR}/cmd/hierarchy -iname "*.c")
AT_CMD_ALL_SRC_FILES += $(shell find ${CLI_DIR}/cmd/concate -iname "*.c")
AT_CMD_ALL_SRC_FILES += $(shell find ${CLI_DIR}/cmd/xc -iname "*.c")
AT_CMD_ALL_SRC_FILES += $(shell find ${CLI_DIR}/cmd/aps -iname "*.c")
AT_CMD_ALL_SRC_FILES += $(shell find ${CLI_DIR}/cmd/register -iname "*.c")
AT_CMD_ALL_SRC_FILES += $(shell find ${CLI_DIR}/cmd/sur -iname "*.c")

# Depend libraries
AT_CMD_ALL_SRC_FILES += $(shell find ${CLI_LIB_DIR}/showtable -iname "*.c")
AT_CMD_ALL_SRC_FILES += $(shell find ${CLI_LIB_DIR}/stdtable -iname "*.c")
AT_CMD_ALL_SRC_FILES += $(shell find ${CLI_LIB_DIR}/listener -iname "*.c")
AT_CMD_ALL_SRC_FILES += $(shell find ${CLI_LIB_DIR}/idparser -iname "*.c")
AT_CMD_ALL_SRC_FILES += $(shell find ${CLI_LIB_DIR}/attcl -iname "*.c")

# Get configuration files
AT_CLI_ALL_CONF_FILES  = $(shell find ${CLI_DIR}/cmd/common -iname "*.conf")
AT_CLI_ALL_CONF_FILES += $(shell find ${CLI_DIR}/cmd/ber -iname "*.conf")
AT_CLI_ALL_CONF_FILES += $(shell find ${CLI_DIR}/cmd/customers -iname "*.conf")
AT_CLI_ALL_CONF_FILES += $(shell find ${CLI_DIR}/cmd/eth -iname "*.conf")
AT_CLI_ALL_CONF_FILES += $(shell find ${CLI_DIR}/cmd/pdh -iname "*.conf")
AT_CLI_ALL_CONF_FILES += $(shell find ${CLI_DIR}/cmd/ppp -iname "*.conf")
AT_CLI_ALL_CONF_FILES += $(shell find ${CLI_DIR}/cmd/man -iname "*.conf")
AT_CLI_ALL_CONF_FILES += $(shell find ${CLI_DIR}/cmd/encap -iname "*.conf")
AT_CLI_ALL_CONF_FILES += $(shell find ${CLI_DIR}/cmd/sdh -iname "*.conf")
AT_CLI_ALL_CONF_FILES += $(shell find ${CLI_DIR}/cmd/pw -iname "*.conf")
AT_CLI_ALL_CONF_FILES += $(shell find ${CLI_DIR}/cmd/ram -iname "*.conf")
AT_CLI_ALL_CONF_FILES += $(shell find ${CLI_DIR}/cmd/debug -iname "*.conf")
AT_CLI_ALL_CONF_FILES += $(shell find ${CLI_DIR}/cmd/reg -iname "*.conf")
AT_CLI_ALL_CONF_FILES += $(shell find ${CLI_DIR}/cmd/diag -iname "*.conf")
AT_CLI_ALL_CONF_FILES += $(shell find ${CLI_DIR}/cmd/pktanalyzer -iname "*.conf")
AT_CLI_ALL_CONF_FILES += $(shell find ${CLI_DIR}/cmd/clock -iname "*.conf")
AT_CLI_ALL_CONF_FILES += $(shell find ${CLI_DIR}/cmd/prbs -iname "*.conf")
AT_CLI_ALL_CONF_FILES += $(shell find ${CLI_DIR}/cmd/physical -iname "*.conf")
AT_CLI_ALL_CONF_FILES += $(shell find ${CLI_DIR}/cmd/platform -iname "*.conf")
AT_CLI_ALL_CONF_FILES += $(shell find ${CLI_DIR}/cmd/concate -iname "*.conf")
AT_CLI_ALL_CONF_FILES += $(shell find ${CLI_DIR}/cmd/xc -iname "*.conf")
AT_CLI_ALL_CONF_FILES += $(shell find ${CLI_DIR}/cmd/aps -iname "*.conf")
AT_CLI_ALL_CONF_FILES += $(shell find ${CLI_DIR}/cmd/register -iname "*.conf")
AT_CLI_ALL_CONF_FILES += $(shell find ${CLI_DIR}/cmd/sur -iname "*.conf")

# Application CLIs
AT_CLI_ALL_CONF_FILES += $(shell find ${APPS_DIR} -iname "*.conf")

AT_ALL_CMD_CONF_FILE = ${ATSDK_HOME}/allcmd.conf

CLI_ALL_SRC += ${AT_CMD_ALL_SRC_FILES}
CLI_CFLAGS  += -I${CLI_DIR}/cmd/include -DPRINT_COLOR
CLI_CFLAGS  += -I${ATSDK_HOME}/debugutil/include
AT_CFLAGS   += $(CLI_CFLAGS)

# ==============================================================================
# Other
# ==============================================================================
CLI_ALL_SRC += $(shell find ${CLI_DIR}/start -iname "*.c")

# ==============================================================================
# Compile
# ==============================================================================
all: atcli atclitable atclimanual

CLI_ALL_OBJS 	  = $(call GetObjectsFromSource, ${CLI_ALL_SRC})
CMD_CONF_GEN_OBJS = $(call GetObjectsFromSource, ${CMD_CONF_GEN_SRC})

atcli: atclitable ${AT_CLI_LIB}
$(AT_CLI_LIB): $(CLI_ALL_OBJS)
	$(AT_LD) $(LDFLAGS) $@ $^

ifeq (${GEN_CMD_SRC_FILE},yes)

$(CMD_CONF_GEN): $(CMD_CONF_GEN_OBJS)
	$(AT_CC) $(CLI_CFLAGS) -o $@ $^ -lpthread

atclitable: AT_CFLAGS := $(AT_CFLAGS) -DARRIVE_TYPES
atclitable: $(CMD_CONF_GEN)
atclitable: CONF_FILE=$(basename ${CLI_ALL_CMD_SRC}).conf
atclitable: ${AT_CLI_ALL_CONF_FILES} atclimanual 
	@rm -f $(CONF_FILE)
	@touch $(CONF_FILE)
	@for i in ${AT_CLI_ALL_CONF_FILES}; do echo "include $$i" >> $(CONF_FILE) && echo >> $(CONF_FILE); done
	chmod 777 $(CMD_CONF_GEN)
	$(CMD_CONF_GEN) --conffile=$(CONF_FILE) --fullconffile=$(AT_ALL_CMD_CONF_FILE) --cmdlibfile=${CLI_ALL_CMD_SRC} --cmdheaderfile=$(CLI_ALL_CMD_HEADER)

GENERATE_CLI_MANUAL=yes
ifeq (${GENERATE_CLI_MANUAL},yes)
atclimanual:
	chmod 777 $(CMD_MANUAL_GEN)
	$(CMD_MANUAL_GEN) -d ${CLI_DIR}/cmd/ -m ${DRIVER_DIR}/docs/cli_manual/atsdk_cli_manual.md 
else
atclimanual:
endif

else
atclitable:
endif

cleancmdgenfiles:
ifeq (${GEN_CMD_SRC_FILE},yes)
	rm -f $(AT_ALL_CMD_CONF_FILE)
endif

cleanatcli: cleancmdgenfiles
	rm -f $(CLI_ALL_OBJS) $(AT_CLI_LIB) $(CMD_CONF_GEN) $(CMD_CONF_GEN_OBJS) $(UTIL_SRC_OBJ) $(basename $(CLI_ALL_CMD_SRC)).conf
	rm -f $(patsubst %.o, %.d, $(CLI_ALL_OBJS) $(AT_CLI_LIB) $(CMD_CONF_GEN) $(CMD_CONF_GEN_OBJS) $(UTIL_SRC_OBJ))

clean: cleanatcli

clidebug:
	echo ${CMD_CONF_GEN_OBJS}

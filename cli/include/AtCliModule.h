/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLI
 * 
 * File        : AtCli.h
 * 
 * Created Date: Nov 5, 2012
 *
 * Description : CLI module public interfaces
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCLIMODULE_H_
#define _ATCLIMODULE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtTextUI.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define cAtCliModeAutoSilent (cAtCliModeAutotest | cAtCliModeSilent)
#define AUTOTEST_NOSILENT_CHK (cAtCliModeAutotest == (AtCliModeGet() & cAtCliModeAutoSilent))

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Global Text-UI management */
int AtCliStart(void);
int AtCliStop(void);
int AtCliExecute(const char *cmdString);
int AtCliExecuteFormat(const char *format, ...) AtAttributePrintf(1, 2);
int AtCliStartWithTextUI(AtTextUI textUI);
AtTextUI AtCliSharedTextUI(void);
void AtCliSharedTextUISet(AtTextUI textUI);

/* Misc functions */
void AtCliModeSet(eAtCliMode climode);
void AtCliModeClr(eAtCliMode climode);
eAtCliMode AtCliModeGet(void);

/* To give another tasks chance to execute */
uint32 AtCliNextIdWithSleep(uint32 currentId, uint32 numIdToRest);
void AtCliCpuRestTimeMsSet(uint32 cpuRestTimeMs);
uint32 AtCliCpuRestTimeMsGet(void);

/* For autotest */
void AutotestEnable(eBool enable);
eBool AutotestIsEnabled(void);

#ifdef __cplusplus
}
#endif
#endif /* _ATCLIMODULE_H_ */


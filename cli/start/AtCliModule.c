/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLI
 *
 * File        : AtCli.c
 *
 * Created Date: Nov 5, 2012
 *
 * Description : CLI module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCliModule.h"
#include "AtOsal.h"
#include "../libs/text-ui/src/AtTextUIInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static AtTextUI m_sharedTextUI  = NULL; /* To cache the text UI created and input by the outside */
static AtTextUI m_defaultTextUI = NULL; /* The default text UI created by this module */

/* When many channels are input, CLI may take full CPU for a long time and OS
 * will kill the process. So, it would be better to give CPU a rest after
 * processing a number of channels (which is determined from each CLI context) */
static uint32 m_cpuRestTimeMs = 0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/**
 * @brief Set cli mode
 *
 * @param climode: cli mode want to set
 */
void AtCliModeSet(eAtCliMode climode)
    {
    AtTextUICliModeSet(AtCliSharedTextUI(), climode);
    }

/**
 * @brief Clear cli mode
 *
 * @param climode
 */
void AtCliModeClr(eAtCliMode climode)
    {
    AtTextUICliModeClear(AtCliSharedTextUI(), climode);
    }

/**
 * @brief Get current cli mode
 *
 * @return
 */
eAtCliMode AtCliModeGet(void)
    {
    return AtTextUICliModeGet(AtCliSharedTextUI());
    }

/**
 * Start CLI module with default Text-UI (Tiny Text-UI)
 */
int AtCliStart(void)
    {
    AtTextUIStart(AtCliSharedTextUI());
    return 0;
    }

/**
 * Start CLI module with specified Text-UI
 * @param textUI
 * @return
 */
int AtCliStartWithTextUI(AtTextUI textUI)
    {
    AtCliSharedTextUISet(textUI);
    return AtCliStart();
    }

/**
 * Get the global text-UI
 *
 * @return The global text-UI
 */
AtTextUI AtCliSharedTextUI(void)
    {
    /* Use the one that installed by the outside */
    if (m_sharedTextUI)
        return m_sharedTextUI;

    /* The outside may want to use the default. Just create one */
    m_defaultTextUI = AtTinyTextUINew();
    m_sharedTextUI  = m_defaultTextUI;

    return m_defaultTextUI;
    }

/**
 * Change the global text-UI
 * @param textUI Global text-UI
 */
void AtCliSharedTextUISet(AtTextUI textUI)
    {
    if (textUI == m_defaultTextUI)
        return;

    /* Delete the default one if it exist */
    AtTextUIDelete(m_defaultTextUI);
    m_defaultTextUI = NULL;

    /* Use this new one */
    m_sharedTextUI = textUI;
    }

/**
 * Stop CLI module
 */
int AtCliStop(void)
    {
    /* Just do nothing if no Text-UI */
    if (m_sharedTextUI == NULL)
        return 0;

    /* Stop it */
    AtTextUIStop(m_sharedTextUI);
    AtCliSharedTextUISet(NULL);

    /* Delete the text UI created by context of this module */
    AtTextUIDelete(m_defaultTextUI);
    m_defaultTextUI = NULL;

    return 0;
    }

/**
 * Execute a command string
 *
 * @param cmdString
 *
 * @retval  0: success
 * @retval -1: fail
 */
int AtCliExecute(const char *cmdString)
    {
    /* For the first time, if no text UI is created, just start this module to
     * have one */
    if (m_sharedTextUI == NULL)
        AtCliStart();

    /* Process command */
    return AtTextUICmdProcess(m_sharedTextUI, cmdString) ? 0 : -1;
    }

/**
 * Execute a format command string
 *
 * @param cmdString
 *
 * @retval  0: success
 * @retval -1: fail
 */
int AtCliExecuteFormat(const char *format, ...)
    {
    eBool success;
    va_list args;

    /* For the first time, if no text UI is created, just start this module to
     * have one */
    if (m_sharedTextUI == NULL)
        AtCliStart();

    va_start(args, format);
    success = AtTextUIFormatCmdProcess(m_sharedTextUI, format, args);
    va_end(args);

    return success ? 0 : -1;
    }

uint32 AtCliNextIdWithSleep(uint32 currentId, uint32 numIdToRest)
    {
    uint32 nextId = currentId + 1;

    if ((currentId == 0) || (numIdToRest == 0) || (m_cpuRestTimeMs == 0))
        return nextId;

    if ((currentId % numIdToRest) == 0)
        AtOsalUSleep(m_cpuRestTimeMs * 1000UL);

    return nextId;
    }

void AtCliCpuRestTimeMsSet(uint32 cpuRestTimeMs)
    {
    m_cpuRestTimeMs = cpuRestTimeMs;
    }

uint32 AtCliCpuRestTimeMsGet(void)
    {
    return m_cpuRestTimeMs;
    }

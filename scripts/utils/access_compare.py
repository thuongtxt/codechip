import re
import sys
import os

from python.arrive.AtAppManager.AtColor import AtColor
from python.jenkin.AtOs import AtOs


def hex2Int(numberOrString):
    if type(numberOrString) == str:
        return int(numberOrString, 16)
    return numberOrString

class RegisterProvider(object):
    def __init__(self, ignoredAddresses_ = None, ignoreAddressesInFile = None):
        self._loadIgnoreAddresses(ignoredAddresses_, ignoreAddressesInFile)
    
    def _loadIgnoreAddresses(self, ignoredAddresses_ = None, ignoreAddressesInFile = None):
        self.ignoreAddresses = []
        self._putIgnoredAddresses(ignoredAddresses_)
        self._putIgnoredAddressesFromFile(ignoreAddressesInFile)
    
    def _putIgnoredAddresses(self, ignoredAddresses_):
        if ignoredAddresses_ is None:
            return
        
        if type(ignoredAddresses_) == list:
            for address in ignoredAddresses_:
                self.ignoreAddresses.append(hex2Int(address))
            return
        
        if type(ignoredAddresses_) == str:
            addressStrings = ignoredAddresses_.split()
            for addressString in addressStrings:
                self.ignoreAddresses.append(hex2Int(addressString))
            return
        
        if type(ignoredAddresses_) == int:
            self.ignoreAddresses.append(hex2Int(ignoredAddresses_))
            return
        
        raise Exception("Unable to handle type %s" % type(ignoredAddresses_))
        
    def _putIgnoredAddressesFromFile(self, ignoreAddressesInFile):
        if ignoreAddressesInFile is None:
            return
        
        addressesFile = open(ignoreAddressesInFile)
        for line in addressesFile:
            self.ignoreAddresses.append(hex2Int(line))
    
    def shouldCompleteIgnoreAddress(self, address):
        return address in self.ignoreAddresses
    
    def shouldIgnoreAddress(self, address, value1 = None , value2 = None):
        return self.shouldCompleteIgnoreAddress(address)
    
class Compare(object):
    def __init__(self, file1_, file2_, ignoredAddresses_ = None, ignoreAddressesInFile = None):
        self.file1 = file1_
        self.file2 = file2_
        self.dict1 = None
        self.dict2 = None
        self.differents = []
        self.registerProvider = RegisterProvider(ignoredAddresses_, ignoreAddressesInFile)
        
    def _logProblem(self, string):
        self.differents.append(string)
    
    def _regValues(self, fileName):
        expression = "Write (.+), value (.+) \(.+\)"
        pattern = re.compile(expression)
        accessFile = open(fileName)
        dicts = {}
        for line in accessFile:
            match = pattern.match(line)
            if match is None:
                continue

            address = hex2Int(match.group(1))
            if self.registerProvider.shouldCompleteIgnoreAddress(address):
                AtColor.printColor(AtColor.YELLOW, "Completly ignore address: 0x%08x" % address)
                continue
            
            value = hex2Int(match.group(2))
            dicts[address] = value
        
        return dicts
    
    def _detectChanges(self, dict1, dict2):
        same = True
        
        if len(dict1) != len(dict2):
            self._logProblem("Number of write operations is different (%d, %d)" % (len(dict1), len(dict2)))
            same = False
        
        for address, value1 in dict1.iteritems():
            try:
                value2 = dict2[address]
                if value2 != value1:
                    if self.registerProvider.shouldIgnoreAddress(address, value1, value2):
                        AtColor.printColor(AtColor.YELLOW, "Ignore address 0x%08x, value1 = 0x%08x, value2 = 0x%08x" % (address, value1, value2))
                        continue
                    
                    self._logProblem("File 1 (wr 0x%08x 0x%08x), file 2 (wr 0x%08x 0x%08x)" % (address, value1, address, value2))
                    same = False
                    
            except:
                self._logProblem("File 1 (wr 0x%08x 0x%08x), file 2 (         None           )" % (address, value1))
                same = False
                
        for address, value2 in dict2.iteritems():
            try:
                _ = dict1[address]
            except:
                self._logProblem("File 1 (         None           ), file 2 (wr 0x%08x 0x%08x)" % (address, value2))
                same = False
        
        return same
    
    def _canCompare(self):
        canCompare = True
            
        if not os.path.isfile(self.file1):
            self._logProblem("File 1 does not exist")
            canCompare = False
        if not os.path.isfile(self.file2):
            self._logProblem("File 2 does not exist")
            canCompare = False
            
        return canCompare
    
    def compare(self):
        """
        @return True if same
        """
        
        if not self._canCompare():
            return False
        
        def parseFile1():
            print "Parsing %s..." % self.file1
            self.dict1 = self._regValues(self.file1)
        thread1 = AtOs.createThreadWithFunction(parseFile1)
        
        def parseFile2():
            print "Parsing %s..." % self.file2
            self.dict2 = self._regValues(self.file2)
        thread2 = AtOs.createThreadWithFunction(parseFile2)
        
        thread1.start()
        thread2.start()
        thread1.join()
        thread2.join()
        
        print "Detect changes..."
        return self._detectChanges(self.dict1, self.dict2)
    
    def showDifferents(self):
        for different in self.differents:
            print different
    
if __name__ == '__main__':
    file1 = None
    file2 = None
    ignoredAddresses = None
    ignoredFile = None
    
    def printUsage():
        print "Usage: %s --file1=<file1> --file2=<file2> [--ignored-addresses=<address1,address2,..>] [--ignored-file=<ignored file>] " % sys.argv[0]
    
    def parseOptions():
        global file1
        global file2
        global ignoredAddresses
        global ignoredFile
        
        import getopt
        expectedOptions = ["file1=", "file2=", "ignored-addresses=", "ignored-file="]
        
        try:
            opts, _ = getopt.getopt(sys.argv[1:], "", expectedOptions)
        except getopt.GetoptError:
            printUsage()
            sys.exit(2)
            
        for opt, arg in opts:
            if opt == "--file1":
                file1 = arg
            if opt == "--file2":
                file2 = arg
            if opt == "--ignored-addresses":
                ignoredAddresses = arg
            if opt == "--ignored-file":
                ignoredFile = arg
                
        if (file1 is None) or (file2 is None):
            printUsage()
            sys.exit(1)
    
    parseOptions()
    compare = Compare(file1, file2, ignoredAddresses_=ignoredAddresses, ignoreAddressesInFile=ignoredFile)
    if compare.compare():
        print "Same"
        exit(0)
    else:
        print "Different"
        compare.showDifferents()
        exit(1)

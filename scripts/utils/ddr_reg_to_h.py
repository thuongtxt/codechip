#!/usr/bin/env python

import click
import re
import coloredlogs, logging
import sys
from collections import namedtuple
import python.arrive.atsdk.AtRegister as AtRegister
import enum

coloredlogs.install()

ddr_reg_file_path = "/home/scratch/chitt/git/atsdk.old/driver/docs/anna/RTL/PDH_CAS_1_N_RD/ddr_reg/PDH CEM Memory Configuration Map.csv"
ddr_h_file_path = "/home/scratch/chitt/git/atsdk.old/driver/docs/anna/RTL/PDH_CAS_1_N_RD/ddr_reg/ddr_reg.h"
reg_pattern = ","
module_patter =",,"
generated_code_start_definition = "typedef struct tTha60290061DdrAddressEntry\r\n" \
                                  "{\r\n" \
                                  "uint8 regName[128];\r\n" \
                                  "uint32 ddrAddress;\r\n" \
                                  "uint32 regBase;\r\n" \
                                  "uint32 regDepth;\r\n" \
                                  "uint32 regWidth;\r\n" \
                                  "} tTha60290061DdrAddressEntry;\r\n" \
                                  "\r\n"

def module_declare_code_gen(module_parts, dest):
    module_name = module_parts[0].strip(" ").replace(" ", "").replace(".", "_")
    var_name = "static const uint32 " + module_name.lower() + "_BaseAddress = "
    #print "module_declare_code_gen Debug: module_parts = "
    #print module_parts
    address = "" + module_parts[2].replace("_", "").strip(" ")
    var_name = var_name + address + ";\r\n"
    dest.writelines(var_name)

    var_name = "static tTha60290061DdrAddressEntry "
    var_name = var_name + module_name.lower() + "_DdrLookupArray[] = {\r\n"
    dest.writelines(var_name)
    return address

def reg_declare_code_gen(reg_parts, dest, module_base_address):
    reg_name = "{\"" + reg_parts[1].replace("\"", "_") + "\", "
    ddrAddress = "0x" + reg_parts[2].strip(" ") + ", "
    regBase = "" + module_base_address + " + " + reg_parts[4].replace("_", "").strip(" ") + ", "
    regDepth = "" + reg_parts[5].strip(" ") + ", "
    regWidth = "" + reg_parts[6].strip(" ") + "}"
    var_value = reg_name + ddrAddress + regBase + regDepth + regWidth
    dest.writelines(var_value)

def reg_declare_code_separator(isMidle):
    separator = " };\r\n\r\n"
    if isMidle :
        separator = ",\r\n"
    return separator

def reg_to_h(source, dest):
    title_line = "Module,Registers Name,DDR Address (HEX),MAX,Base Address,Depth,Width,Total,Note\r\n"
    #dest.writelines(generated_code_start_definition)

    reg_found = False
    module_found = False
    module_base_address = ""
    for line in source:
        if line == title_line:
            continue

        module_parts = line.split(",,")
        reg_parts = line.split(",")
        if len(module_parts) > 3:
            module_name = module_parts[0].strip(" ")
            if module_name == "":
                continue

            if module_found:
                dest.write(reg_declare_code_separator(False))

            module_base_address = module_declare_code_gen(module_parts, dest)
            reg_found = False
            module_found = True

        elif len(reg_parts) > 5:
            reg_name = reg_parts[1].strip(" ")
            if reg_name == "":
                continue

            if reg_found:
                dest.write(reg_declare_code_separator(True))
            reg_declare_code_gen(reg_parts, dest, module_base_address)
            reg_found = True

        next_line = next(source)

        next_module_parts = next_line.split(",,")
        next_reg_parts = next_line.split(",")

        print "reg_to_h Debug: next_module_parts = "
        print next_module_parts
        print "reg_to_h Debug: next_reg_parts = "
        print next_reg_parts

        if len(next_module_parts) > 3:
            if module_found:
                dest.write(reg_declare_code_separator(False))

            module_base_address = module_declare_code_gen(next_module_parts, dest)
            reg_found = False
            module_found = True

        elif len(next_reg_parts) > 5:
            if reg_found:
                dest.write(reg_declare_code_separator(True))
            reg_declare_code_gen(next_reg_parts, dest, module_base_address)
            reg_found = True

        else:
            dest.write(reg_declare_code_separator(False))

    if module_found :
        dest.write(reg_declare_code_separator(False))

def Main():
    source_file = open(ddr_reg_file_path,'r')
    dest_file = open(ddr_h_file_path, 'w')
    reg_to_h(source_file, dest_file)



if __name__ == '__main__':
    Main()

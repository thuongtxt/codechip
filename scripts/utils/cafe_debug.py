"""
Created on Feb 21, 2017

@author: namnn
"""
import re
import sys
import getopt

from python.arrive.AtAppManager.AtAppManager import AtAppManager
from python.arrive.AtAppManager.AtColor import AtColor
from python.arrive.atsdk.platform import AtHalListener


class RegisterProvider(object):
    def shouldIgnoreAddress(self, address):
        return False
    
class Anna20GMroRegisterProvider(RegisterProvider):
    def makeRange(self, baseAddress, start, stop):
        absoluteStartAddress = baseAddress + start
        absoluteStopAddress  = baseAddress + stop
        return absoluteStartAddress, absoluteStopAddress
            
    
    @staticmethod
    def knownInvalidAddresses():
        return [ 
               ]

    @staticmethod
    def pmBaseAddress():
        return 0x00700000
    
    @staticmethod
    def cdrBaseAddress(sliceId):
        sliceBaseAddresses = [0x0C00000,              
                              0x0C40000,              
                              0x0C80000,              
                              0x0CC0000,              
                              0x0D00000,              
                              0x0D40000,              
                              0x0D80000,              
                              0x0DC0000]
        return sliceBaseAddresses[sliceId]  
    
    def numSlices(self):
        return 8
    
    def makeKnownAllSlicesInvalidRanges(self, baseAddressCalculate, start, stop):
        def makeRange(sliceId, start, stop):
            return self.makeRange(baseAddressCalculate(sliceId), start, stop)
        
        def makeRangesForAllSlices(start, stop):
            ranges = list()
            for sliceId in range(self.numSlices()):
                aRange = makeRange(sliceId, start, stop)
                ranges.append(aRange)
                
            return ranges
        
        return makeRangesForAllSlices(start, stop)
    
    def knownCdrInvalidRanges(self):
        def baseAddressCalculate(sliceId):
            return self.cdrBaseAddress(sliceId)
        
        allRanges = list()
        allRanges.extend(self.makeKnownAllSlicesInvalidRanges(baseAddressCalculate, 0x0020800, 0x0020FFF)) # cdr_acr_eng_timing_ctrl
        
        return allRanges
    
    @staticmethod
    def pdhBaseAddress(sliceId):
        sliceBaseAddresses = [0x1000000,              
                              0x1100000,              
                              0x1200000,              
                              0x1300000,              
                              0x1400000,              
                              0x1500000,              
                              0x1600000,              
                              0x1700000]
        return sliceBaseAddresses[sliceId]  
    
    def knownPdhInvalidRanges(self):
        def baseAddressCalculate(sliceId):
            return self.pdhBaseAddress(sliceId)
        
        allRanges = list()
        allRanges.extend(self.makeKnownAllSlicesInvalidRanges(baseAddressCalculate, 0x00040000, 0x0004003F)) # rxm23e23_ctrl
        
        return allRanges
    
    def shouldIgnoreAddressWithKnownRanges(self, address, knownInvalidRanges):
        for start, stop in knownInvalidRanges:
            if start <= address <= stop:
                return True
        
        return False
    
    def isCdrAddress(self, address):
        if (0x0C00000 <= address <= 0x0C3FFFF or 
            0x0C40000 <= address <= 0x0C7FFFF or 
            0x0C80000 <= address <= 0x0CBFFFF or 
            0x0CC0000 <= address <= 0x0CFFFFF or 
            0x0D00000 <= address <= 0x0D3FFFF or 
            0x0D40000 <= address <= 0x0D7FFFF or 
            0x0D80000 <= address <= 0x0DBFFFF or 
            0x0DC0000 <= address <= 0x0DFFFFF):
            return True
        return False
    
    def isPdhAddress(self, address):
        if (0x1000000 <= address <= 0x10FFFFF or       
            0x1100000 <= address <= 0x11FFFFF or       
            0x1200000 <= address <= 0x12FFFFF or       
            0x1300000 <= address <= 0x13FFFFF or       
            0x1400000 <= address <= 0x14FFFFF or       
            0x1500000 <= address <= 0x15FFFFF or       
            0x1600000 <= address <= 0x16FFFFF or       
            0x1700000 <= address <= 0x17FFFFF):
            return True
        return False      

    def isOcnDiagAddress(self, address):
        if (0xFAA000 <= address <= 0xFAA7FF or
            0xFAA800 <= address <= 0xFAAFFF or
            0xFAB000 <= address <= 0xFAB7FF or
            0xFAB800 <= address <= 0xFABFFF):
            return True
        return False

    def shouldIgnoreAddress(self, address):
        ignoreAddresses = [0x01ed0001]

        if address in ignoreAddresses:
            return True

        if self.isOcnDiagAddress(address):
            return True

        return False

class CafeDebug(object):
    def __init__(self, script, logContent, commands = None):
        object.__init__(self)
        self.script = script
        self.logContent = logContent
        self.invalidRegisters = None
        self.hasInvalidAccess = False
        self.registerProvider = None
        self.commands = commands
    
    def shouldIgnoreAddress(self, address):
        if self.registerProvider is None:
            return False
        return self.registerProvider.shouldIgnoreAddress(address)
    
    def _parseInvalidRegisters(self):
        self.invalidRegisters = []
        
        def matchLine(line):
            for pattern in [".*\(.+\) Read (0x.+?),.*",
                             ".*Read\s+(0x.+?),.*"]:
                match = re.match(pattern, line)
                if match is not None:
                    return match
                
            return None
        
        for line in self.logContent.splitlines():
            line = line.strip()
            if len(line) == 0:
                continue
            
            match = matchLine(line)
            if match is None:
                AtColor.printColor(AtColor.YELLOW, "Ignore: %s" % line)
                
            address = int(match.group(1), 16)
            if self.shouldIgnoreAddress(address):
                continue
            
            if address not in self.invalidRegisters:
                self.invalidRegisters.append(address)
        
    def _runScript(self):
        if self.script:
            AtAppManager.localApp().runScript(self.script)
        if self.commands:
            AtAppManager.localApp().runCli(self.commands)
    
    def run(self):
        class _HalListener(AtHalListener):
            def __init__(self, watchedRegisters):
                AtHalListener.__init__(self)
                self.watchedRegisters = watchedRegisters
                self.accessDicts = {}
            
            def _invalidAccess(self, address):
                if address in self.watchedRegisters:
                    return True
                return False
                
            def _addressDict(self, address):
                try:
                    return self.accessDicts[address]
                except:
                    self.accessDicts[address] = {"address": address, "readCount": 0, "writeCount": 0}
                    return self.accessDicts[address]
                
            def didRead(self, address, value):
                if type(address) is str:
                    address = int(address, 16)
                    
                if not self._invalidAccess(address):
                    return
                
                addressDict = self._addressDict(address)
                addressDict["readCount"] = addressDict["readCount"] + 1
            
            def didWrite(self, address, value):
                if type(address) is str:
                    address = int(address, 16)
                    
                if not self._invalidAccess(address):
                    return
                
                addressDict = self._addressDict(address)
                addressDict["writeCount"] = addressDict["writeCount"] + 1
        
        self._parseInvalidRegisters()
        listener = _HalListener(self.invalidRegisters)
        AtHalListener.addListener(listener)
        self._runScript()
        AtHalListener.removeListener(listener)
        
        if len(listener.accessDicts) == 0:
            return
            
        self.hasInvalidAccess = True
        for _, accessDict in listener.accessDicts.iteritems():
            print "Address: 0x%08x, readCount = %d, writeCount = %d" % (accessDict["address"], accessDict["readCount"], accessDict["writeCount"])

def Main():
    def printUsage():
        print "Usage: %s [--help] [--log-file=<logFile>] [--script-file=<scriptFile>]" % sys.argv[0]
    
    try:
        opts, _ = getopt.getopt(sys.argv[1:],"",["help", "log-file=", "script-file="])
    except getopt.GetoptError:
        printUsage()
        return 1
    
    logFile = None
    scriptFile = None
    
    # TODO: Modify this to match  your need if do not want to use scripts
    commands = """
    device init
    serdes mode 1,9 stm64
    sdh line rate 1,9 stm64
    serdes powerup 1,9
    serdes enable 1,9
    sdh line mode 1,9,25-32 sonet
    sdh map aug1.1.1-1.64,9.1-9.64,25.1-32.16 3xvc3s
    xc vc connect vc3.1.1.1-1.64.3,9.1.1-9.64.3 vc3.25.1.1-32.16.3 two-way
    sdh map vc3.25.1.1-32.16.3 7xtug2s
    sdh map tug2.25.1.1.1-32.16.3.7 tu11
    sdh map vc1x.25.1.1.1.1-32.16.3.7.4 de1
    pdh de1 framing 25.1.1.1.1-32.16.3.7.4 ds1_unframed
    pdh de1 timing 25.1.1.1.1-32.16.3.7.4 system 1
    sdh path psl expect vc3.1.1.1-1.64.3,9.1.1-9.64.3 0x2
    sdh path psl transmit vc3.1.1.1-1.64.3,9.1.1-9.64.3 0x2
    pw create satop 1-10752
    pw circuit bind 1-10752 de1.25.1.1.1.1-32.16.3.7.4
    pw jitterbuffer 1-10752 8000
    pw jitterdelay 1-10752  4000
    pw psn 1-10752 mpls
    eth port enable 1
    eth port maccheck 1 dis
    pw ethport 1-10752 1
    pw enable 1-10752
    serdes loopback 25,26 local
    """
    AtAppManager.localApp().runCli(commands)

    commands = "device clear status"
    
    for opt, arg in opts:
        if opt == "--help":
            printUsage()
            return 0
        
        if opt == "--log-file":
            logFile = arg
            
        if opt == "--script-file":
            scriptFile = arg
    
    logContent = """
    """
    
    if logFile:
        openFile = open(logFile)
        logContent = openFile.read()
        openFile.close()
    
    debug = CafeDebug(scriptFile, logContent, commands)
    debug.registerProvider = Anna20GMroRegisterProvider()
    debug.run()
    if not debug.hasInvalidAccess:
        AtColor.printColor(AtColor.GREEN, "No invalid accesses are found")

if __name__ == '__main__':
    Main()

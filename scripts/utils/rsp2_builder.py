
import sys
import os
from multiprocessing import Process
import shutil
import time
import decimal

from python.arrive.AtAppManager.AtColor import AtColor
from python.jenkin.AtOs import AtOs
from AtColor import AtColor
from builder import *

class Rsp2Builder(Builder):
    def __init__(self, sdkDir, rsp2Dir):
        super(Rsp2Builder, self).__init__(sdkDir, "cisco")
        self.rsp2Dir = rsp2Dir
        self.fpga = None
        self.rsp2netOnly = False

    def _build_IOS_lib(self):
        print AtColor.GREEN + ("Building IOS lib .so") + AtColor.CLEAR
        self._runOsCommand("sh ios_build.sh")

    def _build_rsp2net(self):
        print AtColor.GREEN + ("Building rsp2net") + AtColor.CLEAR
        cwd = os.getcwd()
        os.chdir(self.rsp2Dir)
        self._runOsCommand("make update ATSDK_PATH=%s" %self.sdkDir)
        
        if self.fpga is not None:
            firmwareDir = "firmwares"
            self._runOsCommand("rm -rf %s/*.bin" % firmwareDir)
            shutil.copy(self.fpga, firmwareDir)
            self._runOsCommand("make -C %s" % firmwareDir)
            self._runOsCommand("make clean")
            
        self._runOsCommand("make all")
        shutil.copy("rsp2net.tar.gz", "rsp2net_fpga.%s.tar.gz" % (self._fpga_version()))
        os.chdir(cwd)

    def existing_fpga(self):
        for file in os.listdir(self.rsp2Dir + "/firmwares"):
            if file.endswith(".bin"):
                return file
        
        return None
    
    def _fpga_version(self):
        fpga = self.fpga
        if fpga is None:
            fpga = self.existing_fpga()
            
        fpgaName = os.path.splitext(os.path.basename(fpga))[0]
        list = fpgaName.split("_")
        if len(list) >= 2:
            return list[-2] + "_" + list[-1]
        return fpgaName

    def _build_release_done(self):
        print AtColor.GREEN + ("Building release package") + AtColor.CLEAR
        self._runOsCommand("rm -rf release")
        self._runOsCommand("sh release.sh")
        releasePackage = os.path.join(self.sdkDir, "release/atsdk")
        
        if not os.path.exists(releasePackage):
            return False

        return True

    def _buildAll(self):
        def build_IOS_Lib():
            if self._shouldBuild("ios"):
                self._build_IOS_lib()

        if self._shouldBuild("rsp2net"):                
            if not self._build_release_done():
                print AtColor.RED + "Generate release package failed, stop building" + AtColor.CLEAR
                return False

        def build_rsp2net():
            if self._shouldBuild("rsp2net"):
                self._build_rsp2net()
                
        buildIOSLibProcess = self._asyncBuild(build_IOS_Lib)
        buildRsp2netProcess = self._asyncBuild(build_rsp2net)

        buildIOSLibProcess.join()
        buildRsp2netProcess.join()
        return True

    def build(self):
        if not self._canBuild():
            return False

        if not self._buildAll():
            return False

        self._makeRelease()
        return True

    def _allBuildNames(self):
        return ["ios", "rsp2net"]

    def _stripBinary(self, buildName, binary, output=None):
        return

    def _executableBinary(self, buildName):
        if buildName == "ios":
            return os.path.join(self.sdkDir, ".build/libarrive.so.tar.gz")

        if buildName == "rsp2net":
            return os.path.join(self.rsp2Dir, "rsp2net_fpga.%s.tar.gz" % self._fpga_version())

    def _shouldBuild(self, buildName):
        if buildName == "rsp2net" and self.rsp2Dir:
            return True

        if buildName == "ios" and (not self.rsp2netOnly):
            return True

        return False

    def _canBuild(self):
        return True

    def _copyToTarget(self, targetDirectory):
        if os.path.exists(targetDirectory):
            if self._shouldBuild("ios"):
                iosDir = os.path.join(self._releaseDirPath(), self._releaseNameForBuild("ios"))
                iosTarget = os.path.join(targetDirectory, self._releaseNameForBuild("ios"))
                shutil.copytree(iosDir, iosTarget)

            if self._shouldBuild("rsp2net"):
                rsp2netDir = os.path.join(self._releaseDirPath(), self._releaseNameForBuild("rsp2net"))
                rsp2netTarget = os.path.join(targetDirectory, self._releaseNameForBuild("rsp2net"))
                shutil.copytree(rsp2netDir, rsp2netTarget)
        else:
            super(Rsp2Builder, self)._copyToTarget(targetDirectory)

if __name__ == '__main__':
    import getopt
    def printUsage(exitCode=None):
        print "Usage: %s --sdk-dir=<sdkDir> --version=<version> [--rsp2-dir=<rsp2Dir> --fpga=<FPGA.bin> [--rsp2net-only]] " \
              "[--help] [--copy-to=<directory>]" % sys.argv[0]
        if exitCode:
            sys.exit(exitCode)
    try:
        opts, _ = getopt.getopt(sys.argv[1:], "", ["help", "sdk-dir=", "rsp2-dir=", "fpga=", "version=", "help", "no-cleanup", "copy-to=", "rsp2net-only"])
    except getopt.GetoptError:
        printUsage(exitCode=1)

    sdkDir = None
    version = None
    cleanupAfterBuild = True
    targetDirectory = None
    rsp2Dir = None
    fpga = None
    buildRsp2netOnly = False

    for opt, arg in opts:
        if opt == "--help":
            printUsage(exitCode=0)
        if opt == "--sdk-dir":
            sdkDir = arg
        if opt == "--rsp2-dir":
            rsp2Dir = arg
        if opt == "--version":
            version = arg
        if opt == "--no-cleanup":
            cleanupAfterBuild = False
        if opt == "--copy-to":
            targetDirectory = arg
        if opt == "--fpga":
            fpga = arg
        if opt == "--rsp2net-only":
            buildRsp2netOnly = True

    if sdkDir is None or version is None:
        print AtColor.RED + "SDK dir and verion are always required" + AtColor.CLEAR
        printUsage(exitCode=1)

    if rsp2Dir is not None:
        if fpga is not None and not os.path.exists(fpga):
            print AtColor.RED + "FPGA .bin file does not exist to compile rsp2net" + AtColor.CLEAR
            printUsage(exitCode=1)

    builder = Rsp2Builder(sdkDir, rsp2Dir)
    if builder is None:
        print "%s is not supported. Supported customers: %s" % (customerName, Builder.supportedCustomers())
        printUsage(1)

    if rsp2Dir is not None:
        if fpga is None:
            if builder.existing_fpga() is None:
                print AtColor.RED + "FPGA .bin file does not exist to compile rsp2net" + AtColor.CLEAR
                printUsage(exitCode=1)
                
        elif not os.path.exists(fpga):
            print AtColor.RED + "FPGA .bin file does not exist to compile rsp2net" + AtColor.CLEAR
            printUsage(exitCode=1)
            
    builder.version = version
    builder.cleanupAfterBuild = cleanupAfterBuild
    builder.targetDirectory = targetDirectory
    builder.fpga = fpga
    builder.rsp2netOnly = buildRsp2netOnly

    startTime = time.time()
    builder.build()
    seconds = (time.time() - startTime)
    minutes = decimal.Decimal(seconds / 60).to_integral_value()
    print AtColor.GREEN + ("Took %s seconds (%s minutes)" % (seconds, minutes)) + AtColor.CLEAR

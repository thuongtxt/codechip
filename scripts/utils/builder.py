"""
Created on Mar 2, 2017

@author: namnng
"""
import sys
import os
from distutils import dir_util
from multiprocessing import Process
import shutil
import time
import decimal
from subprocess import call

from python.arrive.AtAppManager.AtColor import AtColor
from python.jenkin.AtOs import AtOs


class CrossBuilderInfo(object):
    @classmethod
    def _buildInfoClass(cls, buildName):
        classes = {Builder.KINTEX:KintexBuilderInfo,
                   Builder.VU190 : Vu190BuilderInfo,
                   Builder.VU9P  : Vu9PBuilderInfo}
        return classes[buildName]
    
    @classmethod
    def buildInfo(cls, buildName):
        className = cls._buildInfoClass(buildName)
        return className()
    
    def crossCompile(self):
        return None
    
    def sysroot(self):
        return None
    
    def host(self):
        return None
    
class KintexBuilderInfo(CrossBuilderInfo):
    def crossCompile(self):
        return "/opt/tools/eldk5.3/powerpc-e500v2/sysroots/i686-eldk-linux/usr/bin/ppce500v2-linux-gnuspe/powerpc-linux-gnuspe"
    
    def host(self):
        return "powerpc-linux"

    def sysroot(self):
        return "/opt/tools/eldk5.3/powerpc-e500v2/sysroots/ppce500v2-linux-gnuspe"

class Vu9PBuilderInfo(KintexBuilderInfo):
    pass

class Vu190BuilderInfo(CrossBuilderInfo):
    def crossCompile(self):
        return "/opt/tools/freescale/QorIQ-SDK-V1.5/x86_64/sysroots/x86_64-fsl_networking_sdk-linux/usr/bin/ppce5500-fsl_networking-linux/powerpc-fsl_networking-linux"
    
    def sysroot(self):
        return "/opt/tools/freescale/QorIQ-SDK-V1.5/x86_64/sysroots/ppce5500-fsl_networking-linux/"

    def host(self):
        return "powerpc-fsl_networking-linux"

class Builder(object):
    X86 = "x86"
    KINTEX = "kintex"
    VU190 = "vu190"
    VU9P = "vu9p"
    
    def __init__(self, sdkDir, customer):
        self.sdkDir = sdkDir
        self.customer = customer
        self.needBootstrap = True
        self.version = None
        self.cleanupAfterBuild = True
        self.targetDirectory = None
        self.needOverwrite = False
        self.needBuild = True
    
    def _makeBuildDirWithName(self, dirName):
        return os.path.join(self._allBuildDir(), ".%s" % dirName)
    
    def _buildDir(self, buildName):
        return self._makeBuildDirWithName(buildName)
    
    def _configureCommandWithFlag(self, flags, buildDir):
        configureScript = os.path.join(self.sdkDir, "configure")
        command  = "pushd .; cd %s " % buildDir
        command += "; sh %s --srcdir=%s %s" % (configureScript, self.sdkDir, flags)
        command += "; popd"
        return command
    
    @staticmethod
    def _makeDir(dirPath):
        os.makedirs(dirPath)
    
    def _runOsCommand(self, command):
        AtOs.run(command)
    
    def _compile(self, buildDir):
        self._runOsCommand("make -j 10 -C %s" % buildDir)
    
    def _buildWithConfigureCommand(self, configureCommand, buildDir):
        self._makeDir(buildDir)
        self._runOsCommand(configureCommand)
        self._compile(buildDir)
    
    def _build_x86(self):
        buildDir = self._buildDir(self.X86)
        configureFlags = "CFLAGS=\"-g3 -O0 -m32\""
        configureCommand = self._configureCommandWithFlag(configureFlags, buildDir)
        self._buildWithConfigureCommand(configureCommand, buildDir)
    
    @staticmethod
    def crossBuildInfo(buildName):
        return CrossBuilderInfo.buildInfo(buildName)
        
    def _cflags(self, buildName):
        flags = "-g3 -O0 -DARRIVE_TYPES -Wno-cast-align"
        sysRoot = self.crossBuildInfo(buildName).sysroot()
        if sysRoot:
            flags = flags + " --sysroot=" + sysRoot
        return "\"%s\"" % flags
    
    def _crossBuild(self, buildName):
        buildDir = self._buildDir(buildName)
        crossInfo = self.crossBuildInfo(buildName)
        configureCommand  = "export CROSS_COMPILE=%s;" % crossInfo.crossCompile()
        toolchainBinaries = "CC=$CROSS_COMPILE-gcc LD=$CROSS_COMPILE-ld AR=$CROSS_COMPILE-ar RANLIB=$CROSS_COMPILE-ranlib AS=$CROSS_COMPILE-as OBJCOPY=$CROSS_COMPILE-objcopy STRIP=$CROSS_COMPILE-strip"
        allFlags = "--build=i686-linux --host=%s CFLAGS=%s %s" % (
            crossInfo.host(), 
            self._cflags(buildName), 
            toolchainBinaries)
        flags = self._configureCommandWithFlag(allFlags, buildDir)
        configureCommand += flags
        self._buildWithConfigureCommand(configureCommand, buildDir)
    
    def _generateCli(self):
        self._runOsCommand("make atclitable -j 10 -C %s" % self.sdkDir)
    
    def _bootstrap(self):
        if self.needBootstrap:
            self._runOsCommand("cd %s;sh bootstrap.sh" % self.sdkDir)
    
    def _asyncBuild(self, buildFunction):
        class NewProcess(Process):
            def __init__(self, runFunction, group=None, target=None, name=None, args=(), kwargs=list()):
                Process.__init__(self, group=group, target=target, name=name, args=args, kwargs=kwargs)
                self.runFunction = runFunction
                
            def run(self):
                self.runFunction()
                
        def runFunction():
            buildFunction()
        
        process = NewProcess(runFunction)
        process.start()
        return process
    
    def allSupportedBuildNames(self):
        return [self.X86, self.KINTEX, self.VU190, self.VU9P]
    
    def _allBuildNames(self):
        return []
    
    def _shouldBuild(self, buildName):
        if buildName in self._allBuildNames():
            return True
        return False
    
    def _buildAll(self):
        def buildX86():
            if self._shouldBuild(self.X86):
                self._build_x86()
            
        def buildKintex():
            if self._shouldBuild(self.KINTEX):
                self._crossBuild(self.KINTEX)
        
        def buildVu190():
            if self._shouldBuild(self.VU190):
                self._crossBuild(self.VU190)
        
        def buildVu9P():
            if self._shouldBuild(self.VU9P):
                self._crossBuild(self.VU9P)
        
        buildX86Process    = self._asyncBuild(buildX86)
        buildKintexProcess = self._asyncBuild(buildKintex)
        buildVu190Process  = self._asyncBuild(buildVu190)
        buildVu9pProcess   = self._asyncBuild(buildVu9P)
        
        buildX86Process.join()
        buildKintexProcess.join()
        buildVu190Process.join()
        buildVu9pProcess.join()
        
        def summary(exitCode, buildName):
            if not self._shouldBuild(buildName):
                return
            
            message = "Build %s: " % buildName
            if exitCode != 0:
                print message + AtColor.RED   + "Fail." + AtColor.CLEAR
            else:
                binary = self._executableBinary(buildName)
                if os.path.exists(binary):
                    print message + AtColor.GREEN + "Success. " + AtColor.CLEAR + \
                          "Binary is at: " + binary
                else:
                    print message + AtColor.RED   + "Fail. " + AtColor.CLEAR + \
                          "Binary %s not found" % binary 
        
        # Summary
        summary(buildX86Process.exitcode, self.X86)
        summary(buildKintexProcess.exitcode, self.KINTEX)
        summary(buildVu190Process.exitcode, self.VU190)
        summary(buildVu9pProcess.exitcode, self.VU9P)
    
    def _allBuildDir(self):
        return os.path.join(self.sdkDir, ".buildall")
    
    @staticmethod
    def _removeDir(dirPath):
        shutil.rmtree(dirPath, ignore_errors = True)
    
    def _makeAndCleanUpBuildDir(self):
        buildDir = self._allBuildDir()
        self._removeDir(buildDir)
        self._makeDir(buildDir)
    
    def _releaseDirName(self):
        version = self.version
        if version is None:
            version = "release"
        return version
    
    def _releaseDirPath(self):
        return os.path.join(self._allBuildDir(), self._releaseDirName())
    
    def _releaseNameForBuild(self, buildName):
        return buildName
    
    def _executableBinary(self, buildName):
        buildDir = self._buildDir(buildName)
        return os.path.join(buildDir, "apps/ep_app/epapp")
    
    def _executableCliShellBinary(self, buildName):
        buildDir = self._buildDir(buildName)
        return os.path.join(buildDir, "apps/ep_app/clishell")

    @staticmethod
    def _compilerStrip(buildName):
        if buildName == Builder.X86:
            return "strip"
        
        crossInfo = CrossBuilderInfo.buildInfo(buildName)
        return crossInfo.crossCompile() + "-strip"
    
    def _stripBinary(self, buildName, binary, output = None):
        compilerStrip = self._compilerStrip(buildName)
        command = "%s %s" % (compilerStrip, binary)
        if output:
            command += " -o %s" % output
        self._runOsCommand(command)

    def _releaseNoteFileName(self):
        return "release_note.txt"
     
    def _openVimForEditingRlsNote(self, release_note):
        EDITOR = os.environ.get('EDITOR','vim') #that easy!
        call([EDITOR, release_note])
        
    def _editReleaseNote(self, targetDirectory):
        if self.version is None:
            return
        
        versionAndBuild = self.version.split(".b")
        build_number = versionAndBuild[1]
        if int(build_number) > 0:
            older_build = versionAndBuild[0] + ".b" + "%03d" % (int(build_number) - 1)
            version_dir = os.path.abspath(os.path.join(targetDirectory, os.pardir))
            older_releaseNote = os.path.join(version_dir, older_build, self._releaseNoteFileName())
            
            if not os.path.exists(older_releaseNote):
                print AtColor.YELLOW + "Release note %s does not exist" % (older_releaseNote)
                return
            
            shutil.copy(older_releaseNote, targetDirectory)
            
        new_releaseNote = os.path.join(targetDirectory, self._releaseNoteFileName())
        print AtColor.GREEN + "Update release note at %s" % new_releaseNote
        self._openVimForEditingRlsNote(new_releaseNote)
    
    def _copyToTarget(self, targetDirectory):
        dir_util.copy_tree(self._releaseDirPath(), targetDirectory)
        self._runOsCommand("chmod 777 -Rf %s" % targetDirectory)

    def _makeRelease(self):
        if not self.needOverwrite:
            self._removeDir(self._releaseDirPath())
        
        for buildName in self._allBuildNames():
            if not self._shouldBuild(buildName):
                continue

            binary = self._executableBinary(buildName)
            if os.path.exists(binary):
                self._stripBinary(buildName, binary)
                releaseDir = os.path.join(self._releaseDirPath(), self._releaseNameForBuild(buildName))
                self._makeDir(releaseDir)
                shutil.copy(binary, releaseDir)
                if buildName == "annaauto":
                    shutil.copy(self._executableCliShellBinary(buildName), releaseDir)
                if self.cleanupAfterBuild:
                    self._removeDir(self._buildDir(buildName))

            else:
                print AtColor.RED + ("Binary %s does not exist, build may be fail" % binary) + AtColor.CLEAR
        
        # Copy to release folder
        if self.targetDirectory:
            targetDirectory = os.path.join(self.targetDirectory, self._releaseDirName())
            self._copyToTarget(targetDirectory)
            self._editReleaseNote(targetDirectory)
        else:
            targetDirectory = self._releaseDirPath()
                
        print AtColor.GREEN + "All binaries are available at: " + AtColor.CLEAR + targetDirectory
    
    def _canBuild(self):
        if not self.needOverwrite and self.targetDirectory is not None:
            targetDirectory = os.path.join(self.targetDirectory, self._releaseDirName())
            if os.path.exists(targetDirectory):
                print AtColor.RED + ("Cannot overwrite existing release: %s. Please specify other directory or remove the existing one" % targetDirectory) + AtColor.CLEAR
                return False
            
        return True
    
    def build(self):
        if not self._canBuild():
            return False

        if self.needBuild:
            self._makeAndCleanUpBuildDir()
            if self.needBootstrap:
                self._bootstrap()
            self._generateCli()
            self._buildAll()

        self._makeRelease()
        
        return True

    def __sizeof__(self):
        return super(Builder, self).__sizeof__()

    @classmethod
    def _builderClasses(cls):
        return {"anna": AnnaBuilder, "annaauto":AnnaAutotestBuilder,"cisco" : CiscoBuilder, "dummy" : DummyBuilder}
    
    @classmethod
    def _builderClass(cls, customerName):
        builders = cls._builderClasses()
        return builders[customerName]
    
    @classmethod
    def supportedCustomers(cls):
        return ", ".join(cls._builderClasses().keys())
    
    @classmethod
    def builder(cls, sdkDir, customerName):
        try:
            return cls._builderClass(customerName)(sdkDir, customerName)
        except:
            return None

class AnnaBuilder(Builder):
    def _allBuildNames(self):
        return [self.X86, self.KINTEX, self.VU190, self.VU9P]
    
    def _releaseNameForBuild(self, buildName):
        if buildName == self.VU9P:
            return "anna"
        if buildName == self.VU190:
            return "cisco"
        
        return Builder._releaseNameForBuild(self, buildName)

class AnnaAutotestBuilder(Builder):
    def _allBuildNames(self):
        return [self.KINTEX]
    
    def _releaseNameForBuild(self, buildName):
        return Builder._releaseNameForBuild(self, buildName)
          
class CiscoBuilder(Builder):
    def _allBuildNames(self):
        return [self.VU190]
    
    def _releaseNameForBuild(self, buildName):
        if buildName == self.VU190:
            return "cisco"
        if buildName == self.X86:
            return "xilinx"

        return Builder._releaseNameForBuild(self, buildName)
     
class DummyBuilder(Builder):
    def _runOsCommand(self, command):
        print command
    
    def _allBuildNames(self):
        return self.allSupportedBuildNames()
    
    def _createDummyBinary(self, buildName):
        binaryPath = self._executableBinary(buildName)
        os.makedirs(os.path.dirname(binaryPath))
        binary = open(binaryPath, "w")
        binary.write(buildName)
        binary.close()
    
    def _build_x86(self):
        Builder._build_x86(self)
        self._createDummyBinary(self.X86)
    
    def _crossBuild(self, buildName):
        Builder._crossBuild(self, buildName)
        self._createDummyBinary(buildName)

    def build(self):
        return super(DummyBuilder, self).build()

    def _releaseNameForBuild(self, buildName):
        if buildName == self.VU9P:
            return "anna"
        if buildName == self.VU190:
            return "cisco"
        
        return Builder._releaseNameForBuild(self, buildName)
    
if __name__ == '__main__':
    import getopt
    
    def printUsage(exitCode = None):
        print "Usage: %s --customer=<anna|annaauto|cisco|dummy> --sdk-dir=<sdkDir> " \
              "--version=<version> [--help] [--no-bootstrap] [--no-cleanup] [--no-build] [--copy-to=<directory>] [--overwrite]" % sys.argv[0]
        if exitCode is not None:
            sys.exit(exitCode)

    opts = list()
    try:
        opts, _ = getopt.getopt(sys.argv[1:],"",["help", "customer=", "sdk-dir=", "version=", "help", "no-bootstrap",
                                                 "no-cleanup", "copy-to=", "overwrite", "no-build"])
    except getopt.GetoptError:
        printUsage(exitCode=1)

    customerName = None
    sdkDir = None
    bootstrap = True
    version = None
    cleanupAfterBuild = True
    targetDirectory = None
    needOverwrite = False
    needBuild = True

    for opt, arg in opts:
        if opt == "--help":
            printUsage(exitCode = 0)
        if opt == "--customer":
            customerName = arg
        if opt == "--sdk-dir":
            sdkDir = arg
        if opt == "--no-bootstrap":
            bootstrap = False
        if opt == "--version":
            version = arg
        if opt == "--no-cleanup":
            cleanupAfterBuild = False
        if opt == "--copy-to":
            targetDirectory = arg
        if opt == "--overwrite":
            needOverwrite = True
        if opt == "--no-build":
            needBuild = False
        
    if customerName is None or sdkDir is None:
        printUsage(exitCode=1)
    
    if version is None:
        printUsage(exitCode=1)
    
    builder = Builder.builder(sdkDir, customerName)
    if builder is None:
        print "%s is not supported. Supported customers: %s" % (customerName, Builder.supportedCustomers())
        printUsage(1)
    
    builder.needBootstrap = bootstrap
    builder.version = version
    builder.cleanupAfterBuild = cleanupAfterBuild
    builder.targetDirectory = targetDirectory
    builder.needOverwrite = needOverwrite
    builder.needBuild = needBuild

    startTime = time.time()
    builder.build()
    seconds = (time.time() - startTime)
    minutes = decimal.Decimal(seconds / 60).to_integral_value()
    print AtColor.GREEN + ("Took %s seconds (%s minutes)" % (seconds, minutes)) + AtColor.CLEAR

"""
There is an issue that running the same script with:
- Different in software version
- Different in hw version
And this produces different result. So there should be a tool to compare register values of working case with the fail
case

Usage: [--help] [--script=<script>] [--save-to=<outputFile>] [--audit-from=<inputFile>]
--script: specify script to run
--save-to: save register values to this file
--audit-from: file containing register values

Example:
# Capture register values of working case
./epapp_working <options>
python check_configure.py --script=<pathToScript> --save-to=<workings.reg>
exit

# Now check that with the fail case
./epapp_fail <options>
python check_configure.py --script=<pathToScript> --audit-from=<workings.reg>
exit
"""

import getopt
import sys

from python.arrive.AtAppManager.AtColor import AtColor

from utils.audit import AbstractEntity, RegisterCollector, Expector


class ConfigurationComparision(AbstractEntity):
    def __init__(self, scriptFile = None, fileToSave=None, fileToAudit=None):
        super(ConfigurationComparision, self).__init__()
        self.scriptFile = scriptFile
        self.fileToSave = fileToSave
        self.fileToAudit = fileToAudit

    def compare(self):
        if self.fileToAudit is None:
            return

        registerCollector = RegisterCollector.collectorFromFile(self.fileToAudit)
        return registerCollector.audit()

    def applyScript(self):
        # Create collector to collect all of register values
        collector = None
        if self.fileToSave:
            collector = RegisterCollector()
            collector.start()

        if self.scriptFile:
            self.app().runScript(self.scriptFile)

        # Stop capturing and save
        if collector:
            collector.stop()
            collector.save(self.fileToSave)

    def run(self):
        self.applyScript()
        return self.compare()

class AnnaExpector(Expector):
    pass

class AnnaConfigurationComparision(ConfigurationComparision):
    def createExpector(self):
        return AnnaExpector()

def comparisionClass(customer):
    if customer == "anna":
        return AnnaConfigurationComparision
    return ConfigurationComparision

def Main():
    def printUsage():
        print "Usage: %s [--help] [--script=<script>] [--save-to=<outputFile>] [--audit-from=<inputFile>] [--customer=<anna>]" % sys.argv[0]

    try:
        opts, _ = getopt.getopt(sys.argv[1:], "", ["help", "script=", "save-to=", "audit-from=", "customer="])
    except getopt.GetoptError:
        printUsage()
        return 1

    script = None
    fileToSave = None
    fileToAudit = None
    customer = None

    for opt, arg in opts:
        if opt == "--help":
            printUsage()
            return 0

        if opt == "--script":
            script = arg

        if opt == "--save-to":
            fileToSave = arg

        if opt == "--audit-from":
            fileToAudit = arg

        if opt == "--customer":
            customer = arg

    comparision = comparisionClass(customer)(script, fileToSave, fileToAudit)
    noIssue = comparision.run()
    if noIssue:
        AtColor.printColor(AtColor.GREEN, "No different is found")

    return 0 if noIssue else 1

if __name__ == '__main__':
    Main()
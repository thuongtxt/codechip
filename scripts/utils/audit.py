import pickle
import sys
import getopt

from python.arrive.AtAppManager.AtColor import AtColor
import python.arrive.atsdk.AtRegister as AtRegister
from python.arrive.atsdk.device import AtDeviceListener

try:
    import atsdk
except:
    pass

from python.arrive.AtAppManager.AtAppManager import AtAppManager


class AbstractEntity(object):
    def createExpector(self):
        return Expector()

    def __init__(self):
        self.expector = self.createExpector()

    @staticmethod
    def app():
        """
        :rtype: python.arrive.AtAppManager.AtAppManager.AtApp
        """
        return AtAppManager.localApp()

    @staticmethod
    def rd(address, core=0):
        return atsdk.rd(address, core) & 0xFFFFFFFF

    @staticmethod
    def lrd(address, core=0):
        values = atsdk.lrd(address, core)
        return [value & 0xFFFFFFFF for value in values]

class RegisterCollector(AbstractEntity, AtDeviceListener):
    def __init__(self):
        super(RegisterCollector, self).__init__()
        self._registers = dict()

    def didLongWrite(self, address, value, coreId):
        self._registers[address] = value

    def didWrite(self, address, value, coreId):
        self._registers[address] = value

    def start(self):
        AtDeviceListener.addListener(self)

    def stop(self):
        AtDeviceListener.removeListener(self)

    def registerDict(self):
        """
        :rtype: dict
        """
        return self._registers

    def save(self, filePath):
        assert self._registers
        with open(filePath, "w") as aFile:
            pickle.dump(self._registers, aFile)

    def load(self, filePath):
        with open(filePath, "r") as aFile:
            self._registers = pickle.load(aFile)

    @classmethod
    def collectorFromFile(cls, filePath):
        collector = RegisterCollector()
        collector.load(filePath)
        return collector

    def audit(self):
        same = True

        for address, value in self.registerDict().iteritems():
            if type(value) == list:  # long register
                currentValue = self.lrd(address)
            else:
                currentValue = self.rd(address)
            if not self.expector.expect(address, value, currentValue):
                same = False

        return same

class Expector(object):
    @staticmethod
    def allMask():
        return AtRegister.cBit31_0

    def shortCompareMask(self, address):
        return self.allMask()

    @staticmethod
    def longValueString(longValues):
        reversedValues = list(longValues)
        reversedValues.reverse()
        return "0x%s" % (".".join(["%08x" % value for value in reversedValues]))

    def longExpect(self, address, expectValue, actualValue):
        if actualValue == expectValue:
            return True

        AtColor.printColor(AtColor.RED,
                           "Register 0x%08x, expected %s, actual %s" % (
                           address, self.longValueString(expectValue), self.longValueString(actualValue)))
        return False

    def shortExpect(self, address, expectValue, actualValue):
        if (expectValue & self.shortCompareMask(address)) == (actualValue & self.shortCompareMask(address)):
            return True

        AtColor.printColor(AtColor.RED, "Register 0x%08x, expected 0x%08x, actual 0x%08x" % (address, expectValue, actualValue))
        return False

    def shouldCheckAddress(self, address):
        return True

    def expect(self, address, expectValue, actualValue):
        if not self.shouldCheckAddress(address):
            return True

        if type(expectValue) == list:
            if type(actualValue) != list:
                AtColor.printColor(AtColor.RED, "Address 0x%x previously written as long register with value %s, but now it "
                                                "is accessed as short register with value 0x%08x" % (address, self.longValueString(expectValue), actualValue))
                return False

            return self.longExpect(address, expectValue, actualValue)

        return self.shortExpect(address, expectValue, actualValue)

class Anna20GDebugExpector(Expector):
    pass

class Auditor(AbstractEntity):
    def __init__(self, workScript, failScript):
        super(Auditor, self).__init__()
        self.workScript = workScript
        self.failScript = failScript

    def captureAccess(self, scriptFile):
        """
        :rtype: RegisterCollector
        """
        collector = RegisterCollector()
        collector.expector = self.expector
        collector.start()
        self.applyScript(scriptFile)
        collector.stop()

        return collector

    def applyScript(self, scriptFile):
        self.app().runScript(scriptFile)

    def audit(self):
        registerCollector = self.captureAccess(self.workScript)
        self.applyScript(self.failScript)
        return registerCollector.audit()

def Main():
    def printUsage():
        print "Usage: %s [--help] [--work-script=<script>] [--fail-script=<script>] [--expector=anna]" % sys.argv[0]

    try:
        opts, _ = getopt.getopt(sys.argv[1:], "", ["help", "work-script=", "fail-script=", "expector="])
    except getopt.GetoptError:
        printUsage()
        return 1
    
    #import pydevd
    #pydevd.settrace("localhost")
    
    workScript = None
    failScript = None
    expector = Expector()

    for opt, arg in opts:
        if opt == "--help":
            printUsage()
            return 0

        if opt == "--work-script":
            workScript = arg

        if opt == "--fail-script":
            failScript = arg

        if opt == "--expector":
            if arg == "anna":
                expector = Anna20GDebugExpector()

    auditor = Auditor(workScript, failScript)
    auditor.expector = expector
    noIssue = auditor.audit()
    if noIssue:
        AtColor.printColor(AtColor.GREEN, "No issues found")
        return 0
    else:
        AtColor.printColor(AtColor.RED, "CONFIGURATION MISMATCH FOUND")

    return 0 if noIssue else 1

if __name__ == '__main__':
    Main()

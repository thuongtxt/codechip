'''
Created on Mar 28, 2017

@author: namnn
'''

from subprocess import Popen, PIPE
import sys
import getopt

from AtColor import AtColor
import AtRegister
from AtTestCase import AtTestCase, AtTestCaseRunner

class AtCienaPcp(object):
    def __init__(self):
        self.verbose = True
        self.simulate = False
    
    @staticmethod
    def _driverRestart():
        command = "/etc/init.d/drivers/rc.pcp_prusik"
        try:
            process = Popen([command, "stop"], stdout=PIPE)
            (_, _) = process.communicate()
            exit_code = process.wait()
            assert(exit_code == 0)
        except:
            pass
        
        process = Popen([command, "start"], stdout=PIPE)
        process.communicate()
        exit_code = process.wait()
        assert(exit_code == 0)

    @staticmethod
    def _write(address, value):
        process = Popen(["pcp", "write", "%x" % address, "%x" % value], stdout=PIPE)
        (output, _) = process.communicate()
        process.wait()
        assert("failed" not in output)
    
    @staticmethod
    def _read(address, numRegs):
        process = Popen(["pcp", "read", "%x" % address, "%d" % numRegs], stdout=PIPE)
        (output, _) = process.communicate()
        process.wait()
        assert("failed" not in output)
        
        lines =  output.strip().split("\n")
        assert(len(lines) == (numRegs + 1))
        
        values = []
        for line in lines[1:]:
            valueString = line.split(":")[1]
            values.append(int(valueString, 16))
        
        if numRegs == 1:
            return values[0]
        
        return values
    
    def write(self, address, value):
        def access():
            if self.verbose:
                message = "pcp write %x %04x" % (address, value)
                print message
            
            if not self.simulate:
                self._write(address, value)
            
        try:
            access()
        except:
            print AtColor.YELLOW + "Writting fail, driver may have not been started yet, just start it" + AtColor.CLEAR
            self._driverRestart()
            access()
    
    def read(self, address, numRegs = 1):
        def access():
            if self.verbose:
                message = "pcp read %x %d" % (address, numRegs)
                print message
            
            if not self.simulate:
                return self._read(address, numRegs)
            
            return 0
            
        try:
            return access()
        except:
            print AtColor.YELLOW + "Read fail, driver may have not been started yet, just start it" + AtColor.CLEAR
            self._driverRestart()
            return access()

class AtCienaI2c(object):
    def __init__(self, pcp, page = 0xAC):
        self.pcp = pcp
        self.page = page
        self.simulate = False
        self._simulateSelectedPort = None
    
    def setup(self):
        self.pcp.write(0x1101010, 0x1400)
         
        self.pcp.write(0x1101004, 0x0170)
        self.pcp.write(0x1101008, 0x0001)
        self.pcp.write(0x1101040, 0x0000)
        self.pcp.write(0x1101000, 0x1)
         
        self.pcp.write(0x1101004, 0x0171)
        self.pcp.write(0x1101008, 0x0001)
        self.pcp.write(0x1101040, 0x0000)
        self.pcp.write(0x1101000, 0x1)
         
        self.pcp.write(0x1101004, 0x0172)
        self.pcp.write(0x1101008, 0x0001)
        self.pcp.write(0x1101040, 0x0000)
        self.pcp.write(0x1101000, 0x1)
    
    def initPort(self, portId):
        self.selectedPort = portId
        self.write(0x1b, 0x8004)
        self.write(0x4,  0x01e1)
        self.write(0x9,  0x0e00)
        self.write(0x0,  0x9140)
        
    @staticmethod
    def maxNumPorts():
        return 8
    
    def portIsSupported(self, portId):
        return portId < self.maxNumPorts()
    
    def _selectedPort(): #@NoSelf
        def fget(self):
            self.pcp.write(0x1101004, 0x0070)
            self.pcp.write(0x1101008, 0x0100)
            self.pcp.write(0x1101040, 0x0000)
            self.pcp.write(0x1101000, 0x1)
            
            value = self.pcp.read(0x1101080)
            
            if self.simulate:
                return self._simulateSelectedPort
            
            for portId in range(32):
                mask = AtRegister.cBit0 << portId
                if value & mask:
                    return portId
            
            return None
    
        def fset(self, portId):
            assert(self.portIsSupported(portId))
            
            value = AtRegister.cBit0 << portId
            self.pcp.write(0x1101004, 0x0170)
            self.pcp.write(0x1101008, 0x0001)
            self.pcp.write(0x1101040, value)
            self.pcp.write(0x1101000, 0x1)
            
            if self.simulate:
                self._simulateSelectedPort = portId
            
        return locals()
    
    selectedPort = property(**_selectedPort())
            
    @staticmethod
    def _numBytes():
        return 2
    
    @staticmethod
    def _readRequestType():
        return 2
    @staticmethod
    def _writeRequestType():
        return 1

    def read(self, address):
        stringValue = "%02x%x" % (self._readRequestType(), self.page >> 1)
        self.pcp.write(0x1101004, int(stringValue, 16))
        
        numBytesToWrite = 1 # The address offset need to written
        stringValue = "%02x%02x" % (2, numBytesToWrite)
        self.pcp.write(0x1101008, int(stringValue, 16))
        
        stringValue = "00%02x" % address
        self.pcp.write(0x1101040, int(stringValue, 16))
        self.pcp.write(0x1101000, 1) # Trigger
        
        value = self.pcp.read(0x1101080)
        #Swap MSB, LSB
        value_7_0  = value & 0xFF
        value_15_8 = (value & 0xFF00) >> 8
        value = (value_7_0 << 8) + value_15_8
        
        return value
        
    def write(self, address, value):
        value_7_0  = value & 0xFF
        value_15_8 = (value & 0xFF00) >> 8
        
        # Request type and page
        stringValue = "%02x%x" % (self._writeRequestType(), self.page >> 1)
        self.pcp.write(0x1101004, int(stringValue, 16))
        
        # Number of bytes to write
        self.pcp.write(0x1101008, int("0003", 16))
        
        stringValue = "%02x%02x" % (value_15_8, address)
        self.pcp.write(0x1101040, int(stringValue, 16))
        stringValue = "00%02x" % (value_7_0)
        self.pcp.write(0x1101044, int(stringValue, 16))
            
        self.pcp.write(0x1101000, 1) # Trigger

class SfpRegister(AtRegister.AtRegister):
    def __init__(self, address, i2c):
        AtRegister.AtRegister.__init__(self, value=None)
        self.address = address
        self.i2c = i2c
        self.value = self.read()
    
    def read(self):
        self.value = self.i2c.read(self.address)
        return self.value 
        
    def write(self, value):
        self.i2c.write(self.address, value)
    
    def setBit(self, bitPosition, bitValue, applyHw=False):
        AtRegister.AtRegister.setBit(self, bitPosition, bitValue, applyHw=False)
        if applyHw:
            self.write(self.value)
    
    def apply(self):
        self.write(self.value)
    
    def display(self):
        print "Address 0x%04x, value 0x%04x" % (self.address, self.value)
    
class SfpRegisterFactory(object):
    def __init__(self, i2c, *args, **kwargs):
        object.__init__(self, *args, **kwargs)
        self.i2c = i2c
    
    @staticmethod
    def registerClasses(address):
        classes = {0: SfpRegister_Control,
                   1: SfpRegister_Status,
                   4: SfpRegister_AutoNegAdvertisement,
                   5: SfpRegister_AutoNegLinkPartnerAbility,
                   6: SfpRegister_AutoNegExpansion,
                   7: SfpRegister_AutoNegNextPageTransmit,
                   8: SfpRegister_AutoNegLinkPartnerReceivedNextPage,
                   9: SfpRegister_MasterSlaveControl,
                   10: SfpRegister_MasterSlaveStatus,
                   16: SfpRegister_ExtendedControl1,
                   17: SfpRegister_ExtendedStatus1,
                   20: SfpRegister_ExtendedControl2,
                   21: SfpRegister_ReceivErrorCounter,
                   22: SfpRegister_CableDiag1,
                   26: SfpRegister_ExtendedControl3,
                   27: SfpRegister_ExtendedStatus3,
                   28: SfpRegister_CableDiag2}
        
        try:
            return classes[address]
        except:
            return SfpRegister
    
    def createRegisterWithAddress(self, address):
        return self.registerClasses(address)(address, self.i2c)

class SfpRegister_Control(SfpRegister):
    def display(self):
        self.read()
        print "Address                 : 0x%04x, value: 0x%04x" % (self.address, self.value)
        print "Reset                   : %s" % ("yes" if self.getBit(15) else "no")
        print "Loopback                : %s" % ("enabled" if self.getBit(14) else "disabled")
        print "Speed Selection (LSB)   : %s" % ("1000 Mb/s" if self.getBit(13) == 0 else "unknown")
        print "Auto-Negotiation        : %s" % ("enabled" if self.getBit(12) else "disabled")
        print "Power Down              : %s" % ("Power Down" if self.getBit(11) else "Normal Operation")
        print "Isolate                 : %s" % ("Isolate" if self.getBit(10) else "Normal Operation")
        print "Restart Auto-Negotiation: %s" % ("Restart" if self.getBit(9) else "Normal Operation")
        print "Duplex Mode             : %s" % ("Full Duplex" if self.getBit(8) else "Half Duplex")
        print "Collision Test          : %s" % ("enabled" if self.getBit(7) else "disabled")
        print "Speed Selection (MSB)   : %s" % ("1000 Mb/s" if self.getBit(13) else "unknown")

class SfpRegister_Status(SfpRegister):
    def display(self):
        self.read()
        print "Address                  : 0x%04x, value: 0x%04x" % (self.address, self.value)
        print "Extended Status          : %s" % ("Extended status information in register 15" if self.getBit(8) else "unknown")
        print "MF Preamble Suppression  : %s" % ("PHY will accept management frames with preamble suppressed" if self.getBit(6) else "unknown")
        print "Auto-Negotiation Complete: %s" % ("Completed" if self.getBit(5) else "Not completed")
        print "Remote Fault             : %s" % ("Remote fault condition detected" if self.getBit(4) else "No remote fault condition detected")
        print "Auto-Negotiation Ability : %s" % ("Able to perform AutoNeg" if self.getBit(3) else "Unable to perform AutoNeg")
        print "Link Status              : %s" % ("up" if self.getBit(2) else "down")
        print "Jabber Detect            : %s" % ("Jabber condition detected" if self.getBit(1) else "No jabber condition detected")
        print "Extended Capability      : %s" % ("Extended register capabilities" if self.getBit(0) else "unknown")

class SfpRegister_AutoNegAdvertisement(SfpRegister):
    def display(self):
        self.read()
        print "Address                  : 0x%04x, value: 0x%04x" % (self.address, self.value)
        print "Remote Fault             : %s" % ("set" if self.getBit(13) else "none")
        pauseEncode = self.getField(AtRegister.cBit11_10, 10)
        if  pauseEncode == 3:
            pauseEncodeString = "Both Asymmetric PAUSE and Symmetric PAUSE toward local device"
        elif  pauseEncode == 2:
            pauseEncodeString = "Asymmetric PAUSE toward link partner"
        elif  pauseEncode == 1:
            pauseEncodeString = "Symmetric PAUSE"
        else:
            pauseEncodeString = "No PAUSE"
        print "PAUSE Encoding           : %s" % (pauseEncodeString)
        print "100BASE-TX Full Duplex   : %s" % ("Full duplex" if self.getBit(8) else "Not full duplex")
        print "100BASE-TX Half Duplex   : %s" % ("Half duplex" if self.getBit(7) else "Not half duplex")
        print "10BASE-T Full Duplex     : %s" % ("Full duplex" if self.getBit(6) else "Not full duplex")
        print "10BASE-T Half Duplex     : %s" % ("Half duplex" if self.getBit(5) else "Not half duplex")
        print "IEEE 802.3 Selector Field: %d" % (self.getField(AtRegister.cBit4_0, 0))

class SfpRegister_AutoNegLinkPartnerAbility(SfpRegister):
    def display(self):
        self.read()
        print "Address                  : 0x%04x, value: 0x%04x" % (self.address, self.value)
        print "Next Page                : %s" % ("Next page ability" if self.getBit(15) else "Not advertise next page ability")
        print "Acknowledge              : %s" % ("Link partner ACK" if self.getBit(14) else "Link partner does not ACK")
        print "Remote Fault             : %s" % ("Remote fault" if self.getBit(13) else "Not remote fault")
        pauseEncode = self.getField(AtRegister.cBit11_10, 10)
        if  pauseEncode == 3:
            pauseEncodeString = "Both Asymmetric PAUSE and Symmetric PAUSE toward local device"
        elif  pauseEncode == 2:
            pauseEncodeString = "Asymmetric PAUSE toward link partner"
        elif  pauseEncode == 1:
            pauseEncodeString = "Symmetric PAUSE"
        else:
            pauseEncodeString = "No PAUSE"
        print "PAUSE Encoding           : %s" % (pauseEncodeString)
        print "IEEE 802.3 Selector Field: %d" % (self.getField(AtRegister.cBit4_0, 0))

class SfpRegister_AutoNegExpansion(SfpRegister):
    def display(self):
        self.read()
        print "Address                          : 0x%04x, value: 0x%04x" % (self.address, self.value)
        print "Parallel Detection Fault         : %s" % ("fault" if self.getBit(4) else "none")
        print "Link Partner Next Page Able      : %s" % ("able" if self.getBit(3) else "unable")
        print "Next Page Able                   : %s" % ("able" if self.getBit(2) else "unable")
        print "Page Received                    : %s" % ("received" if self.getBit(1) else "not received")
        print "Link Partner AutoNegotiation Able: %s" % ("enabled" if self.getBit(0) else "disabled")

class SfpRegister_AutoNegNextPageTransmit(SfpRegister):
    def display(self):
        self.read()
        print "Address                       : 0x%04x, value: 0x%04x" % (self.address, self.value)
        print "Next Page                     : %s" % ("Additional next pages" if self.getBit(15) else "Last page")
        print "Message Page                  : %s" % ("Message page" if self.getBit(13) else "Unformatted page")
        print "Acknowledge 2                 : %s" % ("set" if self.getBit(12) else "none")
        print "Toggle                        : %s" % ("Previous value of the toggle bit was 0" if self.getBit(11) else "Previous value of the toggle bit was 1")
        print "Message/Unformatted Code Field: %d" % (self.getField(AtRegister.cBit10_0, 0))

class SfpRegister_AutoNegLinkPartnerReceivedNextPage(SfpRegister):
    def display(self):
        self.read()
        print "Address                       : 0x%04x, value: 0x%04x" % (self.address, self.value)
        print "Next Page                     : %s" % ("Additional next pages" if self.getBit(15) else "Last page")
        print "Message Page                  : %s" % ("Message page" if self.getBit(13) else "Unformatted page")
        print "Acknowledge 2                 : %s" % ("set" if self.getBit(12) else "none")
        print "Toggle                        : %s" % ("Previous value of the toggle bit was 0" if self.getBit(11) else "Previous value of the toggle bit was 1")
        print "Message/Unformatted Code Field: %d" % (self.getField(AtRegister.cBit10_0, 0))

class SfpRegister_MasterSlaveControl(SfpRegister):
    def display(self):
        self.read()
        print "Address                           : 0x%04x, value: 0x%04x" % (self.address, self.value)
        transmitterTestMode = self.getField(AtRegister.cBit15_13, 13)
        if  transmitterTestMode == 0:
            transmitterTestModeString = "Normal Operation"
        elif  transmitterTestMode == 1:
            transmitterTestModeString = "Transmit Waveform Test"
        elif  transmitterTestMode == 2:
            transmitterTestModeString = "Transmit Jitter Test in MASTER Mode"
        else:
            transmitterTestModeString = "Transmit Jitter Test in SLAVE Mode"
        print "Transmitter Test Mode             : %s" % (transmitterTestModeString)
        print "MASTER-SLAVE Manual Config Enable : %s" % ("enabled" if self.getBit(12) else "disabled")
        print "MASTER-SLAVE Config Value         : %s" % ("Master" if self.getBit(11) else "Slave")
        print "Port Type                         : %s" % ("Multiport" if self.getBit(10) else "Single port")
        print "1000BASE-T Full Duplex            : %s" % ("Full duplex" if self.getBit(9) else "Not full duplex")
        print "1000BASE-T Half Duplex            : %s" % ("Half duplex" if self.getBit(8) else "Not half duplex")

class SfpRegister_MasterSlaveStatus(SfpRegister):
    def display(self):
        self.read()
        print "Address                               : 0x%04x, value: 0x%04x" % (self.address, self.value)
        print "MASTER-SLAVE Configuration Fault      : %s" % ("fault" if self.getBit(15) else "not fault")
        print "MASTER-SLAVE Configuration Resolution : %s" % ("Master" if self.getBit(14) else "Slave")
        print "Local Receiver Status                 : %s" % ("OK" if self.getBit(13) else "fault")
        print "Remote Receiver Status                : %s" % ("OK" if self.getBit(12) else "fault")
        print "Link Partner Full Duplex              : %s" % ("Full duplex" if self.getBit(11) else "Not full duplex")
        print "Link Partner Half Duplex              : %s" % ("Half duplex" if self.getBit(10) else "Not half duplex")
        print "Idle Error Count                      : %d" % (self.getField(AtRegister.cBit7_0, 0))

class SfpRegister_ExtendedControl1(SfpRegister):
    def display(self):
        self.read()
        print "Address                  : 0x%04x, value: 0x%04x" % (self.address, self.value)
        mdiCrossoverMode = self.getField(AtRegister.cBit6_5, 5)
        if  mdiCrossoverMode == 0:
            mdiCrossoverModeString = "Manual MDI configuration"
        elif  mdiCrossoverMode == 1:
            mdiCrossoverModeString = "Manual MDIX configuration"
        elif  mdiCrossoverMode == 2:
            mdiCrossoverModeString = "N/A to SFP Module"
        else:
            mdiCrossoverModeString = "Enable automatic crossover"
        print "MDI Crossover Mode       : %s" % (mdiCrossoverModeString)

class SfpRegister_ExtendedStatus1(SfpRegister):
    def display(self):
        self.read()
        print "Address                   : 0x%04x, value: 0x%04x" % (self.address, self.value)
        speed = self.getField(AtRegister.cBit15_14, 14)
        if  speed == 0:
            speedString = "10Mbps"
        elif  speed == 1:
            speedString = "100Mbps"
        elif  speed == 10:
            speedString = "1000Mbps"
        else:
            speedString = "unknonw"
        print "Speed                     : %s" % (speedString)
        print "Duplex                    : %s" % ("Full duplex" if self.getBit(13) else "Half duplex")
        print "Page Received             : %s" % ("Recived" if self.getBit(12) else "Not received")
        print "Speed and Duplex Resolved : %s" % ("Resolved" if self.getBit(11) else "Speed not resolved")
        print "Link                      : %s" % ("up" if self.getBit(10) else "down")
        cableLengh = self.getField(AtRegister.cBit9_7, 7)
        if  cableLengh == 0:
            cableLenghString = "< 50 m"
        elif  cableLengh == 1:
            cableLenghString = "50 - 80 m"
        elif  cableLengh == 2:
            cableLenghString = "80 - 110 m"
        elif  cableLengh == 3:
            cableLenghString = "110 - 140 m"
        elif  cableLengh == 4:
            cableLenghString = "> 140 m"
        else:
            cableLenghString = "unknonw"
        print "Cable Length              : %s" % (cableLenghString)
        print "MDI Crossover Status      : %s" % ("Crossover" if self.getBit(6) else "No crossover")
        print "MAC Transmit Pause Enabled: %s" % ("enable" if self.getBit(3) else "disable")
        print "MAC Receive Pause Enabled : %s" % ("enable" if self.getBit(2) else "disable")
        print "Polarity                  : %s" % ("Reversed" if self.getBit(1) else "Not reversed")
        print "Jabber Detect             : %s" % ("Jabber detected" if self.getBit(0) else "No jabber detected")

class SfpRegister_ExtendedControl2(SfpRegister):
    def display(self):
        self.read()
        print "Address                  : 0x%04x, value: 0x%04x" % (self.address, self.value)
        print "Link down on no idles    : %s" % ("Link lock lost" if self.getBit(15) else "Link lock intact")

class SfpRegister_ReceivErrorCounter(SfpRegister):
    def display(self):
        self.read()
        print "Address                  : 0x%04x, value: 0x%04x" % (self.address, self.value)
        print "Receive errors           : %d" % (self.getField(AtRegister.cBit15_0, 0))

class SfpRegister_CableDiag1(SfpRegister):
    def display(self):
        self.read()
        print "Address                  : 0x%04x, value: 0x%04x" % (self.address, self.value)
        mdiPairSelect = self.getField(AtRegister.cBit1_0, 0)
        if  mdiPairSelect == 0:
            mdiPairSelectString = "Pins 1 & 2 (Channel A)"
        elif  mdiPairSelect == 1:
            mdiPairSelectString = "Pins 3 & 6 (Channel B)"
        elif  mdiPairSelect == 2:
            mdiPairSelectString = "Pins 4 & 5 (Channel C)"
        else:
            mdiPairSelectString = "Pins 7 & 8 (Channel D)"
        print "MDI Pair Select          : %s" % (mdiPairSelectString)

class SfpRegister_ExtendedControl3(SfpRegister):
    def display(self):
        self.read()
        print "Address                  : 0x%04x, value: 0x%04x" % (self.address, self.value)
        outputAmplitude = self.getField(AtRegister.cBit2_0, 0)
        if  outputAmplitude == 0:
            outputAmplitudeString = "0.50V"
        elif  outputAmplitude == 1:
            outputAmplitudeString = "0.60V"
        elif  outputAmplitude == 2:
            outputAmplitudeString = "0.70V"
        elif  outputAmplitude == 3:
            outputAmplitudeString = "0.80V"
        elif  outputAmplitude == 4:
            outputAmplitudeString = "0.90V"
        elif  outputAmplitude == 5:
            outputAmplitudeString = "1.00V"
        elif  outputAmplitude == 6:
            outputAmplitudeString = "1.10V"
        else:
            outputAmplitudeString = "1.20V"
        print "RD+/- Output Amplitude   : %s" % (outputAmplitudeString)

class SfpRegister_ExtendedStatus3(SfpRegister):
    def display(self):
        self.read()
        print "Address                  : 0x%04x, value: 0x%04x" % (self.address, self.value)
        print "1000BASE-X AutoNeg Bypass Enable: %s" % ("enable" if self.getBit(12) else "disable")
        print "1000BASE-X AutoNeg Bypass Status: %s" % ("BASE-X AutoNeg failed and BASE-X link came up becase bypass mode timer expired"
        if self.getBit(11) else "BASE-X link came up because regular BASE-X auto-negotiation was completed")

class SfpRegister_CableDiag2(SfpRegister):
    def display(self):
        self.read()
        print "Address                  : 0x%04x, value: 0x%04x" % (self.address, self.value)
        print "Enable Cable Diag Test   : %s" % ("enable" if self.getBit(15) else "disable")
        status = self.getField(AtRegister.cBit14_13, 13)
        if  status == 0:
            statusString = "No short or open detected in twisted pair"
        elif  status == 1:
            statusString = "Short detected in twisted pair"
        elif  status == 2:
            statusString = "Open detected in twisted pair"
        else:
            statusString = "Test failed"
        print "Status                   : %s" % (statusString)

        status = self.getField(AtRegister.cBit12_8, 8)
        if  status == 0:
            statusString = "-1V"
        elif  status == 16:
            statusString = "0V"
        elif  status == 31:
            statusString = "1V"
        else:
            statusString = "unknown"
        print "Reflected Magnitude      : %s" % (statusString)
        print "Distance short or open   : %d" % (self.getField(AtRegister.cBit7_0, 0))
    
class AtCienaSfp(object):
    def __init__(self, portId, i2c, *args, **kwargs):
        object.__init__(self, *args, **kwargs)
        self.portId = portId
        self.i2c = i2c
        self.select()
        self.factory = SfpRegisterFactory()
        
    def select(self):
        self.i2c.selectedPort = self.portId
        
    def displayRegister(self, address):
        reg = self.factory.createRegisterWithAddress(address) 
        reg.display()
    
class AtCienaI2cTest(AtTestCase):
    simulated = False
    i2c = None
    
    @classmethod
    def setUpClass(cls):
        cls.i2c.setup()
        
    def testSelectPort(self):
        ports = range(self.i2c.maxNumPorts())
        
        for portId in ports:
            self.i2c.selectedPort = portId
            self.assertEqual(self.i2c.selectedPort, portId, None)

class CommandProcessor(object):
    def __init__(self, commands, i2c, *args, **kwargs):
        object.__init__(self, *args, **kwargs)
        self.commands = commands.split(",")
        self.i2c = i2c
    
    @staticmethod
    def _valueFromString(aString):
        value = None
        try:
            if aString[:2] == "0x":
                value = int(aString, 16)
            else:
                value = int(aString)
        except:
            print "%s is not a valid number" % aString
            return None
        
        return value
    
    def _valueWithMaskBit(self, tokens, regValue, value):
        stopBit = tokens[3].split("bit")
        if (len(stopBit) < 2):
            print "=== Format bit mask is wrong, it must be bitx_y, EX: bit1_0 ==="
            assert(len(stopBit) >= 2)
        stopBit = self._valueFromString(stopBit[1])
            
        startBit = stopBit
        if len(tokens) == 5:
            startBit = self._valueFromString(tokens[4])
            
        if startBit > stopBit:
            print "=== startBit > stopBit: startBit = %d, startBit = %d ===" % (startBit, stopBit)
        assert(startBit <= stopBit)
        
        mask = (AtRegister.cBit31_0 << startBit) & (AtRegister.cBit31_0 >> (31 - stopBit))
        
        register = AtRegister.AtRegister()
        register.__init__(regValue)
        return register.setField(mask, startBit, value)
    
    def processCommand(self, command):
        # Ignore empty line or comment
        line = command.strip()
        if len(line) == 0:
            return
        if line[0] == '#':
            return
        
        if "_" in line:
            tokens = line.split("_")
        else:
            tokens = line.split()
        if len(tokens) == 2:
            if tokens[0] != "rd":
                raise Exception("Expected command \"rd\" but got %s " % tokens[0])
            
            address = self._valueFromString(tokens[1])
            assert(address is not None)
            print "Read  0x%04x, value 0x%04x" % (address, self.i2c.read(address))
        
        if len(tokens) == 3:
            if tokens[0] != "wr":
                raise Exception("Expected command \"wr\" but got %s " % tokens[0])
            
            address = self._valueFromString(tokens[1])
            assert(address is not None)
            value = self._valueFromString(tokens[2])
            assert(value is not None)
            self.i2c.write(address, value)
            print "Write 0x%04x, value 0x%04x" % (address, value)
            
        # Have bit mask
        if len(tokens) > 3:
            if tokens[0] != "wr":
                raise Exception("Expected command \"wr\" but got %s " % tokens[0])
            address = self._valueFromString(tokens[1])
            assert(address is not None)
            
            #Read
            regValue = self.i2c.read(address)
            value = self._valueFromString(tokens[2])
            assert(value is not None)
            regValue = self._valueWithMaskBit(tokens, regValue, value)
            self.i2c.write(address, value)
            
            print "Write 0x%04x, value 0x%04x" % (address, regValue)
    
    def process(self):
        for command in self.commands:
            self.processCommand(command)
            
class ScriptProcessor(CommandProcessor):
    def __init__(self, scriptFilePath, i2c, *args, **kwargs):
        CommandProcessor.__init__(self, "", i2c, *args, **kwargs)
        self.scriptFilePath = scriptFilePath
    
    def process(self):
        scriptFile = open(self.scriptFilePath)
        for line in scriptFile:
            self.processCommand(line)
        scriptFile.close()

class RegisterDisplayer(object):
    def __init__(self, addressString, i2c, *args, **kwargs):
        object.__init__(self, *args, **kwargs)
        self.addresses = [int(string.strip(), 16) for string in addressString.split(",")]
        self.i2c = i2c
    
    def display(self):
        factory = SfpRegisterFactory(self.i2c)
        for address in self.addresses:
            reg = factory.createRegisterWithAddress(address)
            print "============================================================"
            print "= Address: 0x%04x" % address 
            print "============================================================"
            reg.display()

def Main():
    def printUsage():
        print "Usage: %s [--help] [--init] [--port=portId>] [--page=<page in range [0, 1], default: page 0>] [--setup] [--simulate] [--unittest] [--commands=<commands: rd_0,wr_0_1,wr_0_1_bit3>] [--script=<scriptFile>] [--pcp-verbose] [--show-registers=<address1, address2, ...>]" % sys.argv[0]
    
    try:
        opts, _ = getopt.getopt(sys.argv[1:],"",["help", "init", "setup", "simulate", "unittest", "commands=", "script=", "pcp-verbose", "show-registers=", "port=", "page="])
    except getopt.GetoptError:
        printUsage()
        return 1
    
    init = False
    setup = False
    simulate = False
    unittest = False
    commands = None
    scriptFile = None
    pcpVerbose = False
    displayedRegisters = None
    portId = None
    page = 0
    hasPortInput = False
    
    for opt, arg in opts:
        if opt == "--help":
            printUsage()
            return 0
        
        if opt == "--init":
            init = True
            hasPortInput = True
        
        if opt == "--setup":
            setup = True
        
        if opt == "--simulate":
            simulate = True
            
        if opt == "--unittest":
            unittest = True
            
        if opt == "--commands":
            commands = arg
            hasPortInput = True
            
        if opt == "--script":
            scriptFile = arg
            hasPortInput = True
        
        if opt == "--pcp-verbose":
            pcpVerbose = True
            
        if opt == "--show-registers":
            displayedRegisters = arg
            hasPortInput = True
            
        if opt == "--port":
            portId = int(arg) - 1
            
        if opt == "--page":
            page = int(arg, 16)

    pcp = AtCienaPcp()
    pcp.verbose = pcpVerbose
    pcp.simulate = simulate
    i2c = AtCienaI2c(pcp, page)
        
    if setup:
        i2c.setup()
    
    if unittest:
        AtCienaI2cTest.i2c = i2c
        runner = AtTestCaseRunner.runner()
        runner.run(AtCienaI2cTest)
        runner.summary()
    
    if hasPortInput:
        if portId is None:
            print "Port ID is not specified with --port option, ignoring commands related to port"
            printUsage()
            return 0
    else:
        return 0
    
    if init:
        i2c.initPort(portId)
        
    i2c.selectedPort = portId
    i2c.simulate = simulate

    if commands is not None:
        commandProcessor = CommandProcessor(commands, i2c)
        commandProcessor.process()
    
    if scriptFile is not None:
        scriptProcessor = ScriptProcessor(scriptFile, i2c)
        scriptProcessor.process()
    
    if displayedRegisters is not None:
        displayer = RegisterDisplayer(displayedRegisters, i2c)
        displayer.display()
        
    return 0
        
if __name__ == '__main__':
    Main()

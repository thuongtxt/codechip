atsdk::device init

set tfi5LineId 1
set hoLineId   2
set numPws     16

# Configure SDH
atsdk::sdh line rate $tfi5LineId stm16
atsdk::sdh line rate $hoLineId   stm16

# Mapping for TFI-5
atsdk::sdh map "aug1.$tfi5LineId.1-$tfi5LineId.16 vc4"

# Mapping for LoLine
atsdk::sdh map "aug1.$hoLineId.1-$hoLineId.16 vc4"
atsdk::sdh map "vc4.$hoLineId.1-$hoLineId.16 c4"

# Cross-connect
atsdk::xc vc connect "vc4.$tfi5LineId.1-$tfi5LineId.16 vc4.$hoLineId.1-$hoLineId.16 two-way"

# Configure PW
set pws "1-$numPws"
atsdk::pw create cep $pws basic
atsdk::pw circuit bind "$pws vc4.$hoLineId.1-$hoLineId.16"
atsdk::pw ethport $pws 1

# Ethernet header
atsdk::eth port srcmac 1 C0.CA.C0.CA.C0.CA

for {set pw_i 1} {$pw_i <= $numPws} {incr pw_i} {
	atsdk::pw ethheader $pw_i C0.CA.C0.CA.C0.CA "0.0.$pw_i" none
	
	# MPLS PSN
	atsdk::pw psn $pw_i mpls
	atsdk::pw mpls innerlabel $pw_i "$pw_i.1.1"
	atsdk::pw mpls outerlabel add  $pw_i "$pw_i.1.1"
	atsdk::pw mpls expectedlabel $pw_i "[expr $pw_i]"
	
	# Enable PW
	atsdk::pw enable $pw_i
}

# Loopback for unit-testing
atsdk::eth port loopback 1 local

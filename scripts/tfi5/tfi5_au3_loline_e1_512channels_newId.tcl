atsdk::hal debug dis
atsdk::fill 530000 53003f 0
atsdk::device init

set tfi5LineId 1
set loLineId   1

# Configure SDH
atsdk::sdh line rate $tfi5LineId stm16
atsdk::sdh line mode $tfi5LineId sdh

atsdk::rd 530000 bit4_1

# Mapping for TFI-5
atsdk::sdh map "aug1.$tfi5LineId.1-$tfi5LineId.16 3xvc3s"
atsdk::rd 530000 bit4_1
# Mapping for LoLine
# Mapping for LoLine
atsdk::sdh map "vc3.$loLineId.1.1-$loLineId.16.3 7xtug2s"
atsdk::sdh map "tug2.$loLineId.1.1.1-$loLineId.16.3.7 tu12"
atsdk::sdh map "vc1x.$loLineId.1.1.1.1-$loLineId.16.3.7.3 de1"
atsdk::pdh de1 framing "$loLineId.1.1.1.1-$loLineId.16.3.7.3" e1_unframed
atsdk::pdh de1 timing "$loLineId.1.1.1.1-$loLineId.16.3.7.3" loop none
atsdk::rd 530000 bit4_1
# Configure PW
set pws "1-504"
set numPws 504
atsdk::pw create satop $pws
atsdk::pw circuit bind "$pws de1.$loLineId.1.1.1.1-$loLineId.8.3.7.3"
atsdk::rd 530000 bit4_1
#atsdk::pw ethport 1 1
atsdk::rd 530000 bit4_1
atsdk::pw ethport $pws 1
atsdk::rd 530000 bit4_1
atsdk::pw jitterbuffer $pws 20000
atsdk::rd 530000 bit4_1
atsdk::pw jitterdelay $pws 10000
atsdk::rd 530000 bit4_1
# Ethernet header
atsdk::eth port srcmac 1 C0.CA.C0.CA.C0.CA

for {set pw_i 1} {$pw_i <= $numPws} {incr pw_i} {
	atsdk::pw ethheader $pw_i C0.CA.C0.CA.C0.CA "0.0.$pw_i" none
	
	# MPLS PSN
	atsdk::pw psn $pw_i mpls
	atsdk::pw mpls innerlabel $pw_i "$pw_i.1.1"
	atsdk::pw mpls outerlabel add  $pw_i "$pw_i.1.1"
	atsdk::pw mpls expectedlabel $pw_i "[expr $pw_i]"
	atsdk::rd 530000 bit4_1
	# Enable PW
	atsdk::pw enable $pw_i
	atsdk::rd 530000 bit4_1
}

# Loopback for unit-testing
atsdk::eth port loopback 1 local
atsdk::eth port enable 1
atsdk::rd 530000 bit4_1

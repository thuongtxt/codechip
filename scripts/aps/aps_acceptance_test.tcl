namespace import -force atsdk::*

#  Correct addresses of the two boards
set device1 172.33.45.11
set device2 172.33.45.11

set productCode "60031021"

# All related lines
set externalLines [list 1 2]
set internalLines [list 3 4]

proc DeviceIndex {deviceIpAddress} {
	show driver
	set globalTable 0
	table foreach $globalTable row col cellValue {
		puts "row = $row, col = $col, cell = $cellValue"
		if {$col eq "HAL" && [string match "$deviceIpAddress*" $cellValue]} {
			return [expr $row + 1]
		}
	}
	
	return -1
}

proc DeviceAdded {deviceIpAddress} {
	if {[DeviceIndex $deviceIpAddress] == -1} {
		return False
	}
	return True
}

proc AddDevice {deviceIpAddress} {
	global productCode

	if {[DeviceAdded $deviceIpAddress]} { return; }
	
	if {[info exist productCode] == 1} {
		device create $deviceIpAddress $productCode
	} else {
		device create $deviceIpAddress
	}
	
	puts [DeviceIndex $deviceIpAddress]
	#[device select [DeviceIndex $deviceIpAddress]]
	#device init
}

proc setup {} {
	global device1 device1
	global device2 device2
	
	AddDevice $device1
	AddDevice $device2
}

proc main {} {
	setup
}

main

device init
sdh map vc3.1.1.1-1.1.3 de3

vcg create 1 vcat ds3
vcg member provision 1 de3.1.1.1
vcg member provision 1 de3.1.1.2
vcg member provision 1 de3.1.1.3

pdh de3 framing 1-4 ds3_cbit_unchannelize 
vcg create 2 vcat ds3
vcg member provision 2 de3.1
vcg member provision 2 de3.2
vcg member provision 2 de3.3
vcg member provision 2 de3.4

pdh de1 framing 1-12 ds1_esf 
vcg create 3 vcat ds1
vcg member provision 3 de1.1
vcg member provision 3 de1.2
vcg member provision 3 de1.3
vcg member provision 3 de1.4
vcg member provision 3 de1.12

vcg encap 1-3 gfp
eth flow create 1-3
eth flow egress destmac 1-3 C0.CA.C0.CA.C0.CA
eth flow egress srcmac 1-3 C0.FE.C0.FE.C0.FE
eth flow egress vlan 1 1 1.1.1 1.1.1
eth flow egress vlan 2 1 1.1.2 1.1.2
eth flow egress vlan 3 1 1.1.3 1.1.3
eth flow ingress vlan add 1 1 1.1.1 1.1.1
eth flow ingress vlan add 2 1 1.1.2 1.1.2
eth flow ingress vlan add 3 1 1.1.3 1.1.3
encap channel flowbind vcg.1 1
encap channel flowbind vcg.2 2
encap channel flowbind vcg.3 3
eth flow enable 1-3

encap channel flowbind vcg.1 none
encap channel flowbind vcg.2 none
encap channel flowbind vcg.3 none
vcg encap 1-3 hdlc
pdh de3 line ber monitor enable 1-3
pdh de3 path ber monitor enable 1-3
pdh de1 line ber monitor enable 1-4
pdh de1 path ber monitor enable 1-4
pdh de3 line ber monitor enable 1.1.1-1.1.3
pdh de3 path ber monitor enable 1.1.1-1.1.3


import pydevd
pydevd.settrace('172.33.47.2') # Which is the IP of the PC

import atsdk
import time
import sys

cBit0     = 0x00000001
cBit15_4  = 0x0000FFF0
cBit10    = 0x00000400
cBit15_10 = 0x0000FC00
cBit4_0   = 0x0000001F
cBit8     = 0x00000100
cBit9     = 0x00000200
cBit8_2   = 0x000001FC
cBit1_0   = 0x00000003
cBit10_0  = 0x000007FF
cBit3_0   = 0x0000000F

DRP_BASE = 0xF41000

def drpRead(drpAddress):
    regAddr = DRP_BASE + drpAddress
    return atsdk.rd(regAddr, 0)

def drpWrite(drpAddress, value):
    address = drpAddress + DRP_BASE
    atsdk.runCli("wr %x %x" % (address, value))

def regFieldGet(value, mask, shift):
    return (value & mask) >> shift

def regFieldSet(value, mask, shift, fieldValue):
    return ((value & (~mask)) | ((fieldValue << shift) & mask))

def bitGet(value, bitPosition):
    mask = cBit0 << bitPosition
    return regFieldGet(value, mask, bitPosition)  

def bitSet(value, bitPosition, bitValue):
    mask = cBit0 << bitPosition
    return regFieldSet(value, mask, bitPosition, value)

def scan():
    atsdk.runCli("device show readwrite dis dis")
    
    '''
    Step 1: The GT should be up and running. Here for example, you configure the 
    transmitter and receiver equalizers, run the reset sequences, wait for reset 
    done, and check that buffers are not overflowing etc.. In this example the 
    LPM mode is used.
    
    Hope the following command will internally reset SERDES
    '''
    atsdk.runCli("serdes mode 1 1G")
    
    '''
    Select the first faceplate SERDES
    ''' 
    atsdk.runCli("wr 0x00f40043 0x1")
    
    '''
    Step 2: Set ES_HORZ_OFFSET[11] and USE_PCS_CLK_PHASE_SEL in accordance with 
    Table 1 and Table 2
    '''
    ES_HORZ_OFFSET_ADDR  = 0x4F
    ES_HORZ_OFFSET_MASK  = cBit15_4
    ES_HORZ_OFFSET_SHIFT = 4
    value = drpRead(ES_HORZ_OFFSET_ADDR)
    horzOffset = regFieldGet(value, ES_HORZ_OFFSET_MASK, ES_HORZ_OFFSET_SHIFT)
    horzOffset = bitSet(horzOffset, 11, bitGet(horzOffset, 10))
    value = regFieldSet(value, ES_HORZ_OFFSET_MASK, ES_HORZ_OFFSET_SHIFT, horzOffset)
    drpWrite(ES_HORZ_OFFSET_ADDR, value)
    
    # USE_PCS_CLK_PHASE_SEL = 0
    USE_PCS_CLK_PHASE_SEL_ADDR  = 0x94
    USE_PCS_CLK_PHASE_SEL_MASK  = cBit10
    USE_PCS_CLK_PHASE_SEL_SHIFT = 10
    value = drpRead(USE_PCS_CLK_PHASE_SEL_ADDR)
    value = regFieldSet(value, USE_PCS_CLK_PHASE_SEL_MASK, USE_PCS_CLK_PHASE_SEL_SHIFT, 0)
    drpWrite(USE_PCS_CLK_PHASE_SEL_ADDR, value)
    
    '''
    Step 3: Be ready for the scan: 
    ES_CONTROL [5:0] = 6b000000
    ES_EYE_SCAN_EN = 1b1 enables the Eye Scan
    ES_ERRDET_EN = 1b1 enables the error detection: each bit of the Sdata bus is 
    1 if and only if the corresponding offset data sample does not agree with the 
    recovered data sample.
    ES_PRESCALE according to the BER target (we can set it to be very small initially: i.e. 5b00100)
    '''
    ES_CONTROL_ADDR      = 0x3C
    ES_CONTROL_MASK      = cBit15_10
    ES_CONTROL_SHIFT     = 10
    ES_PRESCALE_MASK     = cBit4_0
    ES_PRESCALE_SHIFT    = 0
    ES_EYE_SCAN_EN_MASK  = cBit8
    ES_EYE_SCAN_EN_SHIFT = 8
    ES_ERRDET_EN_MASK    = cBit9
    ES_ERRDET_EN_SHIFT   = 9
    value = drpRead(ES_CONTROL_ADDR)
    value = regFieldSet(value, ES_CONTROL_MASK, ES_CONTROL_SHIFT, 0)
    value = regFieldSet(value, ES_EYE_SCAN_EN_MASK, ES_EYE_SCAN_EN_SHIFT, 1)
    value = regFieldSet(value, ES_ERRDET_EN_MASK, ES_ERRDET_EN_SHIFT, 1)
    value = regFieldSet(value, ES_PRESCALE_MASK, ES_PRESCALE_SHIFT, 0x4)
    drpWrite(ES_CONTROL_ADDR, value)
    
    '''
    Step 4: Before running the error counting, we need to tell the Eye Scan engine 
    what to measure. This is well described in the User Guide, in the Eye Scan 
    Architecture chapter.
    Commonly, a statistical eye view uses the following:
    ES_SDATA_MASK = {80{1b1}, 80{1b0}} for 80-bit data
    ES_QUAL_MASK = {160{1b1}} 
    '''
    ES_SDATA_MASK0_ADDR = 0x049
    ES_SDATA_MASK1_ADDR = 0x04A
    ES_SDATA_MASK2_ADDR = 0x04B
    ES_SDATA_MASK3_ADDR = 0x04C
    ES_SDATA_MASK4_ADDR = 0x04D
    drpWrite(ES_SDATA_MASK4_ADDR, 0xFFFFFFFF)
    drpWrite(ES_SDATA_MASK3_ADDR, 0xFFFFFFFF)
    drpWrite(ES_SDATA_MASK2_ADDR, 0xFFFF0000)
    drpWrite(ES_SDATA_MASK1_ADDR, 0x00000000)
    drpWrite(ES_SDATA_MASK0_ADDR, 0x00000000)
    
    ES_QUAL_MASK0_ADDR = 0x044
    ES_QUAL_MASK1_ADDR = 0x045
    ES_QUAL_MASK2_ADDR = 0x046
    ES_QUAL_MASK3_ADDR = 0x047
    ES_QUAL_MASK4_ADDR = 0x048
    drpWrite(ES_QUAL_MASK0_ADDR, 0xFFFFFFFF)
    drpWrite(ES_QUAL_MASK1_ADDR, 0xFFFFFFFF)
    drpWrite(ES_QUAL_MASK2_ADDR, 0xFFFFFFFF)
    drpWrite(ES_QUAL_MASK3_ADDR, 0xFFFFFFFF)
    drpWrite(ES_QUAL_MASK4_ADDR, 0xFFFFFFFF)
             
    '''
    Step 5: set the vertical offset. For example:
    RX_EYESCAN_VS_NEG_DIR = 1b0 (Equivalent to ES_VERT_OFFSET[7] in 7 series devices)
    RX_EYESCAN_VS_UT_SIGN = 1b0 (Equivalent to ES_VERT_OFFSET[8] in 7 series devices)
    RX_EYESCAN_VS_CODE = 7b0000000 (Equivalent to ES_VERT_OFFSET[6:0] in 7 series devices)
    RX_EYESCAN_VS_RANGE = 2b00 (This sets scale factor for eye scan)
    '''
    drpAddress = 0x97
    RX_EYESCAN_VS_NEG_DIR_MASK  = cBit10
    RX_EYESCAN_VS_NEG_DIR_SHIFT = 10
    RX_EYESCAN_VS_UT_SIGN_MASK  = cBit9
    RX_EYESCAN_VS_UT_SIGN_SHIFT = 9
    RX_EYESCAN_VS_CODE_MASK     = cBit8_2
    RX_EYESCAN_VS_CODE_SHIFT    = 2
    RX_EYESCAN_VS_RANGE_MASK    = cBit1_0
    RX_EYESCAN_VS_RANGE_SHIFT   = 00
    value = drpRead(drpAddress)
    value = regFieldSet(value, RX_EYESCAN_VS_NEG_DIR_MASK, RX_EYESCAN_VS_NEG_DIR_SHIFT, 0)
    value = regFieldSet(value, RX_EYESCAN_VS_UT_SIGN_MASK, RX_EYESCAN_VS_UT_SIGN_SHIFT, 0)
    value = regFieldSet(value, RX_EYESCAN_VS_CODE_MASK, RX_EYESCAN_VS_CODE_SHIFT, 0)
    value = regFieldSet(value, RX_EYESCAN_VS_RANGE_MASK, RX_EYESCAN_VS_RANGE_SHIFT, 0)
    drpWrite(drpAddress, value)

    '''
    Step 6: set the horizontal offset position. For example, 4 taps to the right with respect to the data sampler:
    ES_HORZ_OFFSET[10:0] = 11'b00000000100 
    '''
    value = drpRead(ES_HORZ_OFFSET_ADDR)
    horzOffset = regFieldGet(value, ES_HORZ_OFFSET_MASK, ES_HORZ_OFFSET_SHIFT)
    horzOffset = regFieldSet(horzOffset, cBit10_0, 0, 0x4)
    value = regFieldSet(value, ES_HORZ_OFFSET_MASK, ES_HORZ_OFFSET_SHIFT, horzOffset)
    drpWrite(ES_HORZ_OFFSET_ADDR, value)
    
    '''
    Step 7: run the Scan by bringing the FSM to RESET and decide on the measurement loop:
    Set the ES_CONTROL = 6b000001
    '''
    value = drpRead(ES_CONTROL_ADDR)
    value = regFieldSet(value, ES_CONTROL_MASK, ES_CONTROL_SHIFT, 1)
    drpWrite(drpAddress, value)
    
    '''
    Step 8: check that the FSM is in END status. 
    Read ES_CONTROL_STATUS should be equal to 3b010
    '''
    drpAddress = 0x253
    ES_CONTROL_STATUS_MASK  = cBit3_0
    ES_CONTROL_STATUS_SHIFT = 0
    done = False
    for i in range(0, 5):
        value = drpRead(drpAddress)
        if regFieldGet(value, ES_CONTROL_STATUS_MASK, ES_CONTROL_STATUS_SHIFT) == 0x2:
            done = True
            break;
        
        time.sleep(1)
        
    if not done:
        print "Eye-scan timeout"
        return
        
    '''
    Step 9: when the FSM is in END status we can finally read errors, samples, and calculate the BER.
    '''
    ES_ERROR_COUNT_ADDR  = 0x251
    ES_SAMPLE_COUNT_ADDR = 0x252
    print "Error count: %d" % drpRead(ES_ERROR_COUNT_ADDR)
    print "Sample count: %d" % drpRead(ES_SAMPLE_COUNT_ADDR)

if __name__ == '__main__':
    scan()

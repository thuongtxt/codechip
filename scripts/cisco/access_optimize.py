'''
Created on Mar 8, 2017

@author: namnng
'''
from AtHalListener import AtHalListener
from AtAppManager import AtAppManager
from AtColor import AtColor

import filecmp
import collections

class _HalListener(AtHalListener):
    def __init__(self, *args, **kwargs):
        AtHalListener.__init__(self, *args, **kwargs)
        self.addressesUseHwDefault = {}
        self.addressesDonotUseHwDefault = {}
        self.numReads = 0
        self.numWrites = 0
    
    def addressDonotUseHwDefault(self, address):
        try:
            return self.addressesDonotUseHwDefault[address]
        except:
            pass
    
    def _createAccessInfo(self):
        return {"rd" : None, "wr" : None}

    def didRead(self, address, value):
        self.numReads = self.numReads + 1
        accessDict = None
        
        try:
            accessDict = self.addressesDonotUseHwDefault[address]
        except:
            accessDict = self._createAccessInfo()
            self.addressesUseHwDefault[address] = accessDict
        
        accessDict["rd"] = value
        
    def didWrite(self, address, value):
        self.numWrites = self.numWrites + 1
        accessDict = None
        
        try:
            accessDict = self.addressesDonotUseHwDefault[address]
        except:
            try:
                accessDict = self.addressesUseHwDefault[address]
            except:
                accessDict = self._createAccessInfo()
                self.addressesDonotUseHwDefault[address] = accessDict
        
        accessDict["wr"] = value 

class DefaultValueCacher(object):
    def _writeLogToFile(self, accessDicts, filePath):
        sortedDicts = collections.OrderedDict(sorted(accessDicts.items()))
        
        openFile = open(filePath, "w")
        
        prevAddress = None
        
        for address, accessDict in sortedDicts.iteritems():
            if prevAddress is None:
                prevAddress = address
                
            if address < prevAddress:
                print AtColor.YELLOW + "Address has not been sorted" + AtColor.CLEAR
                
            prevAddress = address
                
            if accessDict["wr"] is not None:
                openFile.write("wr 0x%x 0x%x\n" % (address, accessDict["wr"]))
                
            openFile.flush()
                
        openFile.close()
                
    def catch(self):
        listener = _HalListener()
        AtHalListener.addListener(listener)
        AtAppManager.localApp().runCli("device init")
        AtHalListener.removeListener(listener)
        
        print "* Number of addresses that use hardware default: " + \
              AtColor.YELLOW + \
              ("%d registers" % len(listener.addressesUseHwDefault)) + \
              AtColor.CLEAR
              
        print "* Number of addresses that DONOT use hardware default: " + \
              AtColor.GREEN + \
              ("%d registers" % len(listener.addressesDonotUseHwDefault)) + \
              AtColor.CLEAR
        
        estimateTimeUs = ((listener.numReads * 5) + (listener.numWrites * 10))
        print "NumReads = %d, NumWrites = %d, Total = %d, estimate = %.3f (s)" % (listener.numReads, 
                                                             listener.numWrites,
                                                             listener.numReads + listener.numWrites,
                                                             estimateTimeUs / 1000000.0) 
        
        print "Writting log to: .use_hw_default.log"
        self._writeLogToFile(listener.addressesUseHwDefault, ".use_hw_default.log")
        
        print "Writting log to: .not_use_hw_default.log"
        self._writeLogToFile(listener.addressesDonotUseHwDefault, ".not_use_hw_default.log")
        
        if not filecmp.cmp('.use_hw_default.log', '.use_hw_default_original.log'):
            print "Compare .use_hw_default.log with .use_hw_default_original.log: " + AtColor.RED + "FAIL" + AtColor.CLEAR 
            return
        
        if not filecmp.cmp('.not_use_hw_default.log', '.not_use_hw_default_original.log'):
            print "Compare .not_use_hw_default.log with .not_use_hw_default_original.log: " + AtColor.RED + "FAIL" + AtColor.CLEAR 
            return
        
        print AtColor.GREEN + "No different found" + AtColor.CLEAR
        
if __name__ == '__main__':
    catcher = DefaultValueCacher()
    catcher.catch()

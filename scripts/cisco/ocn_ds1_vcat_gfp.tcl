atsdk::device init
atsdk::sdh line rate 1 stm16
atsdk::sdh line mode 1 sonet
atsdk::sdh line loopback 1 release
atsdk::sdh map aug16.1.1 4xaug4s
atsdk::sdh map aug4.1.1-1.4 4xaug1s
atsdk::sdh map aug1.1.1-1.16 3xvc3s
atsdk::sdh map vc3.1.1.1-1.16.3 7xtug2s
atsdk::sdh map tug2.1.1.1.1-1.16.3.7 tu11
atsdk::sdh map vc1x.1.1.1.1.1-1.16.3.7.4 de1
atsdk::sdh path tti transmit vc3.1.1.1-1.16.3 16bytes a null_padding
atsdk::sdh path tti expect vc3.1.1.1-1.16.3 16bytes a null_padding
atsdk::sdh path psl expect vc3.1.1.1-1.16.3 0x2
atsdk::sdh path psl transmit vc3.1.1.1-1.16.3 0x2
atsdk::sdh path tti transmit vc1x.1.1.1.1.1-1.16.3.7.4 16bytes a null_padding
atsdk::sdh path tti expect vc1x.1.1.1.1.1-1.16.3.7.4 16bytes a null_padding
atsdk::sdh path psl expect vc1x.1.1.1.1.1-1.16.3.7.4 0x2
atsdk::sdh path psl transmit vc1x.1.1.1.1.1-1.16.3.7.4 0x2
atsdk::pdh de1 framing 1.1.1.1.1-1.16.3.7.4 ds1_sf
atsdk::pdh de1 timing 1.1.1.1.1-1.16.3.7.4 system none
atsdk::vcg create 1 vcat ds1
atsdk::vcg member provision 1 de1.1.1.1.1.1-1.1.1.4.4
atsdk::vcg encap 1 gfp
atsdk::eth flow create 1
atsdk::eth flow egress destmac 1 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 1 C0.FE.C0.FE.C0.FE
atsdk::eth flow egress vlan 1 1 1.1.1 1.1.1
atsdk::eth flow ingress vlan add 1 1 1.1.1 1.1.1
atsdk::encap channel flowbind vcg.1 1
atsdk::eth flow enable 1


import re
from subprocess import *
import sys

def parseString(string):
    patern = re.compile("arrive:\S+?[\+](\S+)")
    return re.findall(patern, string)

def addressToLine(address):
    argvs = ["powerpc-fsl_networking-linux-addr2line", "-e", "atsdkall.so", address] 
    process = Popen(argvs, stdout=PIPE, stderr=PIPE)
    stdout, stderr = process.communicate()
    return stdout

def stackToLines(string):
    addresses = parseString(string)
    lineInfo = {}
    for address in addresses:
        print "%s: %s" % (address, addressToLine(address))

def test():
    testString = """\    
*Mar 24 15:27:35.986: %TRANSCEIVER-6-INSERTED: SIP0: iomd:  transceiver module inserted in SPA-1T8S-10CS_4/0
*Mar 24 15:27:35.994: %TRANSCEIVER-6-INSERTED: SIP0: iomd:  transceiver module inserted in SPA-1T8S-10CS_4/1
*Mar 24 15:27:35.995: %TRANSCEIVER-6-INSERTED: SIP0: iomd:  transceiver module inserted in SPA-1T8S-10CS_4/2
*Mar 24 15:27:35.997: %TRANSCEIVER-6-INSERTED: SIP0: iomd:  transceiver module inserted in SPA-1T8S-10CS_4/3
*Mar 24 15:27:50.756: %CONTROLLER-5-UPDOWN: Controller SONET 0/4/0, changed state to up
*Mar 24 15:28:11.373: %IOSXE-4-PLATFORM: R0/0: kernel: Process : iomd (1899) encountered fatal signal 6
*Mar 24 15:28:11.373: %IOSXE-4-PLATFORM: R0/0: kernel: Process : iomd stack :  c:089de000+368fc c:089de000+38520 binos:0ce42000+95e0 btrace:0ce7e000+54ac btrace:0ce7e000+549c btrace:0ce7e000+549c btrace:0ce7e000+76a0 btrace:0ce7e000+7744 binos:0ce42000+f30c :00100000+344 :00100000+fff0001a iomd_infra:0ffd0000+a14c iomd_infra:0ffd0000+aa84 iomd_infra:0ffd0000+951c lotr_tdl_common:0de71000+edb4 tdm_msg_lib:0fd0a000+38dc ct3ds0:0b1f5000+c7540 if_combined:0a5d3000+ecbe4 if_combined:0a5d3000+ecdd4 iomd_infra:0ffd0000+bdc0 :10000000+17
*Mar 24 15:28:11.373: %IOSXE-4-PLATFORM: R0/0: kernel:
*Mar 24 15:28:11.373: %IOSXE-4-PLATFORM: R0/0: kernel: Process : iomd (2150) encountered fatal signal 9
*Mar 24 15:28:11.373: %IOSXE-4-PLATFORM: R0/0: kernel: Process : iomd (6545) encountered fatal signal 9
*Mar 24 15:28:11.373: %IOSXE-4-PLATFORM: R0/0: kernel: Process : iomd stack : Process : iomd stack :  c:089de000+393c0Process : iomd (2151) encountered fatal signal 9
*Mar 24 15:28:11.373: %IOSXE-4-PLATFORM: R0/0: kernel:  c:089de000+9c080Process : iomd stack :  c:089de000+9c2d4 pthread:08b62000+fd70 c:089de000+9a5f4 pthread:08b62000+ff24 c:089de000+9a494 :10000000+320b8 arrive:0e228000+4045b0 prelib:08f68000+3214 arrive:0e228000+400b8c pthread:08b62000+5a4c arrive:0e228000+153158 c:089de000+e231c
*Mar 24 15:28:11.373: %IOSXE-4-PLATFORM: R0/0: kernel:  arrive:0e228000+153644
*Mar 24 15:28:11.373: %IOSXE-4-PLATFORM: R0/0: kernel:  arrive:0e228000+153a14 arrive:0e228000+18a924 arrive:0e228000+1970f8 arrive:0e228000+197888 arrive:0e228000+197d24 lotr_cem_fpga:0deaa000+61b4 uea_eowyn_im:0df7e000+6b8c uea_eowyn_im:0df7e000+71f8 :10000000+32be4 prelib:08f68000+3214 pthread:08b62000+5a4c c:089de000+e231c arrive:0e228000+40e3f8
*Mar 24 15:28:11.373: %IOSXE-4-PLATFORM: R0/0: kernel:
*Mar 24 15:28:21.524: %PMAN-3-PROCFAIL: SIP0: pman.sh:  The process iomd has failed (rc 134)
*Mar 24 15:28:51.858: %IOSXE_RP_SPA-4-IFCFG_CMD_TIMEOUT: Interface configuration command (0x12) to slot 0/4 timed out
-Traceback= 1#1e9855ebdfc259a2264680e400079e5e  :10000000+EBF584 :10000000+EBF934 :10000000+3655AD4 :10000000+3658914 :10000000+3E4F090 :10000000+3E030D0 :10000000+3E0B984 :10000000+3E0C184 :10000000+3F62008 :10000000+3F64600 :10000000+3EF3E04 :10000000+3158770 :10000000+3EF1B44 :10000000+3F115A0 :10000000+3667CDC :10000000+366F2C0
*Mar 24 15:28:51.932: %SPA_OIR-6-ONLINECARD: SPA (NCS4200-1T8S-10CS) online in subslot 0/4
*Mar 24 15:28:51.934: %IOSXE_OIR-6-REMSPA: SPA removed from subslot 0/4, interfaces disabled
*Mar 24 15:28:51.946: %SPA_OIR-6-OFFLINECARD: SPA (NCS4200-1T8S-10CS) offline in subslot 0/4
*Mar 24 15:28:51.947: %IOSXE_OIR-6-INSSPA: SPA inserted in subslot 0/4
*Mar 24 15:28:52.640: %LINEPROTO-5-UPDOWN: Line protocol on Interface CEM0/4/0, changed state to up
*Mar 24 15:28:53.857: %LINK-3-UPDOWN: Interface SONET 0/4/0, changed state to down
*Mar 24 15:33:12.753: %TRANSCEIVER-6-INSERTED: SIP0: iomd:  transceiver module inserted in SPA-1T8S-10CS_4/0
*Mar 24 15:33:12.761: %TRANSCEIVER-6-INSERTED: SIP0: iomd:  transceiver module inserted in SPA-1T8S-10CS_4/1
*Mar 24 15:33:12.762: %TRANSCEIVER-6-INSERTED: SIP0: iomd:  transceiver module inserted in SPA-1T8S-10CS_4/2
*Mar 24 15:33:12.763: %TRANSCEIVER-6-INSERTED: SIP0: iomd:  transceiver module inserted in SPA-1T8S-10CS_4/3
*Mar 24 15:33:27.775: %CONTROLLER-5-UPDOWN: Controller SONET 0/4/0, changed state to up
*Mar 24 15:33:31.787: %CONTROLLER-5-UPDOWN: Controller SONET 0/4/3, changed state to up
*Mar 24 15:33:35.786: %SONET-4-ALARM:  SONET 0/4/5: SLOS asserted
*Mar 24 15:33:35.786: %SONET-4-ALARM:  SONET 0/4/5: SLOS asserted
*Mar 24 15:33:36.766: %SONET-4-ALARM:  SONET 0/4/7: SLOS asserted
*Mar 24 15:33:36.766: %SONET-4-ALARM:  SONET 0/4/7: SLOS asserted
*Mar 24 15:33:46.666: %SPA_OIR-6-ONLINECARD: SPA (NCS4200-1T8S-10CS) online in subslot 0/4
[Router_RP_0:/bootflash]$

"""
    print stackToLines(testString)

if __name__ == '__main__':
    fileName = None
    if len(sys.argv) > 1:
        fileName = sys.argv[1]
    
    if fileName is None:
        print "Input file name"
        exit(1)
    
    file = open(fileName)
    print stackToLines(file.read())

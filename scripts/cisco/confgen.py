# Build DS1 SAToP between two ports

commands = "ios_config"

def rate():
    return 48

def cemGroupCalculate(stsId, vtgId, vtId):
    return (stsId * 28) + (vtgId * 4) + vtId

def cemConnectionName(portId1, stsId1, vtgId1, vtId1,
                      portId2, stsId2, vtgId2, vtId2):
    connection1 = (portId1 * 1344) + (stsId1 * 28) + (vtgId1 * 4) + vtId1;
    connection2 = (portId2 * 1344) + (stsId2 * 28) + (vtgId2 * 4) + vtId2;
    return "%d_%d" % (connection1, connection2)

def startConfigure():
    tcl("configure terminal")
def endConfigure(save=False):
    tcl("end")
    if save:
        tcl("write")

def tcl(command):
    global commands
    #print "ios_config \"%s\"" % command
    print command
    #commands += " \"%s\"" % command

def ds1GenMapping(bayId, portId):
    interface = "0/%d/%d" % (bayId, portId)

    tcl("controller mediaType %s" % interface)
    tcl("mode sonet")
    tcl("controller sonet %s" % interface)
    tcl("rate oc%d" % rate())
    
    for sts_i in range(0, rate()):
        stsId = sts_i + 1
        tcl("sts-1 %d" % stsId)
        tcl("mode vt-15")
        
        for vtg_i in range(0, 7):
            vtgId = vtg_i + 1
            for vt_i in range(0, 4):
                vtId = vt_i + 1
                tcl("vtg %d t1 %d framing unframed" % (vtgId, vtId))
                tcl("no vtg %d t1 %d shutdown" % (vtgId, vtId))
                tcl("vtg %d t1 %d cem-group %d unframed" % (vtgId, vtId, cemGroupCalculate(sts_i, vtg_i, vt_i)))

def ds1GenDeprovision(bayId, portId):
    interface = "0/%d/%d" % (bayId, portId)
    tcl("controller sonet %s" % interface)

    for sts_i in range(0, rate()):
        stsId = sts_i + 1
        tcl("sts-1 %d" % stsId)
        for vtg_i in range(0, 7):
            vtgId = vtg_i + 1
            for vt_i in range(0, 4):
                vtId = vt_i + 1
                tcl("no vtg %d t1 %d cem-group %d unframed" % (vtgId, vtId, cemGroupCalculate(sts_i, vtg_i, vt_i)))
                tcl("no vtg %d t1 %d framing unframed" % (vtgId, vtId))
        
        tcl("no mode vt-15")
        
    tcl("controller mediaType %s" % interface)
    tcl("no mode sonet")
        
def ds1CrossConnect(bay1, port1, bay2, port2, connect = True):
    for sts_i in range(0, rate()):
        stsId = sts_i + 1
        for vtg_i in range(0, 7):
            vtgId = vtg_i + 1
            for vt_i in range(0, 4):
                vtId = vt_i + 1
                
                vt1CemGroupId = cemGroupCalculate(sts_i, vtg_i, vt_i)
                vt2CemGroupId = cemGroupCalculate(sts_i, vtg_i, vt_i)
                
                # Connect them
                endPoint1 = "cem 0/%d/%d %d" % (bay1, port1, vt1CemGroupId)
                endPoint2 = "cem 0/%d/%d %d" % (bay2, port2, vt2CemGroupId)
                connectionName = cemConnectionName(port1, sts_i, vtg_i, vt_i, 
                                                   port2, sts_i, vtg_i, vt_i)
                "%d_%d" % (vt1CemGroupId, vt2CemGroupId)
                if connect:
                    tcl("connect %s %s %s" % (connectionName, endPoint1, endPoint2))
                else:
                    tcl("no connect %s" % (connectionName))

def vtGenMapping(bayId, portId):
    interface = "0/%d/%d" % (bayId, portId)
    
    tcl("controller mediaType %s" % interface)
    tcl("mode sonet")
    tcl("controller sonet %s" % interface)
    tcl("rate oc%d" % rate())
    
    for sts_i in range(0, rate()):
        stsId = sts_i + 1
        tcl("sts-1 %d" % stsId)
        tcl("mode vt-15")
        
        for vtg_i in range(0, 7):
            vtgId = vtg_i + 1
            for vt_i in range(0, 4):
                vtId = vt_i + 1
                tcl("no vtg %d vt %d shutdown" % (vtgId, vtId))
                tcl("vtg %d vt %d cem-group %d cep" % (vtgId, vtId, cemGroupCalculate(sts_i, vtg_i, vt_i)))

def vtCrossConnect(bay1, port1, bay2, port2, connect = True):
    ds1CrossConnect(bay1, port1, bay2, port2, connect)

def vc3GenMapping(bayId, portId):
    interface = "0/%d/%d" % (bayId, portId)
    
    tcl("controller mediaType %s" % interface)
    tcl("mode sonet")
    tcl("controller sonet %s" % interface)
    tcl("rate oc%d" % rate())
    
    for sts_i in range(0, rate()):
        stsId = sts_i + 1
        tcl("sts-1 %d" % stsId)
        tcl("mode unframed")
        
        tcl("no shutdown")
        tcl("cem-group %d cep" % cemGroupCalculate(sts_i, 0, 0))

def vc3CrossConnect(bay1, port1, bay2, port2, connect = True):
    for sts_i in range(0, rate()):
        vc3CemGroupId1 = cemGroupCalculate(sts_i, 0, 0)
        vc3CemGroupId2 = cemGroupCalculate(sts_i, 0, 0)
    
    # Connect them
    endPoint1 = "cem 0/%d/%d %d" % (bay1, port1, vc3CemGroupId1)
    endPoint2 = "cem 0/%d/%d %d" % (bay2, port2, vc3CemGroupId2)
    
    connectionName = cemConnectionName(port1, sts_i, 0, 0, port2, sts_i, 0, 0)
    if connect:
        tcl("connect %s %s %s" % (connectionName, endPoint1, endPoint2))
    else:
        tcl("no connect %s %s %s" % (connectionName, endPoint1, endPoint2))
                
if __name__ == '__main__':
    startConfigure()
    ds1GenMapping(1, 0)
    ds1GenMapping(1, 2)
    ds1GenMapping(1, 4)
    ds1GenMapping(1, 6)
    ds1CrossConnect(1, 0, 1, 2, connect=True)
    ds1CrossConnect(1, 4, 1, 4, connect=True)
    endConfigure(save = True)

#These CLI to add define list register to monitor value of them.
#Test result will pass if their value is not changing anytimes in perforamce,
#Test resull declear fail if any changing value when performace in normal 
# condition or temperature testing

#Sample to test with Mask set with Hexa-Format
register mon start
register mon short add  0x0044C000  0xAAAAAAAA 10 2 1
register mon short mask 0x0044C000  0xCAFECAFE 10 2 1

#Sync all Value form register by Read Operations
register mon synchw


#Show Only failed Register
#Task require for Polling and Check value of registers
app polling enable

#CLI to make time performance and test duration
register mon perform 10 10

#Some example to force error during test perform
wr 0x0044C000 1
wr 0x0044C000 2
wr 0x0044C000 aaaaaaa

#Show all register Information
show register mon info

#Show Only failed Registers
show register mon fail

#Sample to test with Mask set with Bit-Numbers
register mon start
register mon short add  0x0044C000  0xAAAAAAAA 10 2 1
register mon short mask 0x0044C000  0xCAFECAFE 10 2 1

#Sync all Value form register by Read Operations
register mon synchw


#Show Only failed Register
#Task require for Polling and Check value of registers
app polling enable

#CLI to make time performance and test duration
register mon perform 10 10

#Some example to force error during test perform
wr 0x0044C000 1
wr 0x0044C000 2
wr 0x0044C000 aaaaaaa

#Show all register Information
show register mon info

#Show Only failed Registers
show register mon fail
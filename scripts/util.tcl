proc pdhLiuLoopInMaskForPort { portId } {
	set hwPortId [expr $portId-1]
	return "bit$hwPortId"
}

proc pdhLiuLoopIn { portId } {
	set bitMask [pdhLiuLoopInMaskForPort $portId]
	atsdk::wr 0x700305 1 $bitMask
}

proc pdhLiuReleaseLoopIn { portId } {
	set bitMask [pdhLiuLoopInMaskForPort $portId]
	atsdk::wr 0x700305 0 $bitMask
}

proc delay { second } {
	after [expr $second*1000]
}

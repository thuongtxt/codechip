source e1_mlppp.tcl 

proc showStatusOfAllPorts { } {
	# Show PDH status
	atsdk::show pdh de1 alarm [de1Ports]
	atsdk::show pdh de1 counters [de1Ports] r2c
	atsdk::show pdh de1 counters [de1Ports] r2c
	
	# Show Ethernet status
	atsdk::show eth port counters [ethPort] r2c
	
	# Show PPP counters
	atsdk::show ppp link counters [links] r2c
	atsdk::show encap hdlc counters [links] r2c
	
	# Show flow counters
	atsdk::show eth flow counters [links] r2c
	
	# Show bundle counters
	global numBundles
	if {$numBundles == 1} {
		set bundles 1
	} else {
		set bundles 1-$numBundles
	}
	atsdk::show ppp bundle counters $bundles r2c
}

showStatusOfAllPorts
after 1
showStatusOfAllPorts
after 1
showStatusOfAllPorts

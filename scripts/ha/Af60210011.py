import sys
import Af6021HaTest


class Af60210011HaTest(Af6021HaTest.Af6021HaTest):
    def testDs1Satop(self):
        # Configure the active
        self.activeRun("driver role active")
        self.activeRun("device init")
        self.activeRun("sdh map aug1.1.1-6.16 vc4")
        self.activeRun("sdh map vc4.1.1-6.16 3xtug3s")
        self.activeRun("sdh map tug3.1.1.1-6.16.3 7xtug2s")
        self.activeRun("sdh map tug2.1.1.1.1-6.16.3.7 tu12")
        self.activeRun("sdh map vc1x.1.1.1.1.1-6.16.3.7.3 de1")
        self.activeRun("pw create satop 1-6048")
        self.activeRun("pw circuit bind 1-6048 de1.1.1.1.1.1-6.16.3.7.3")
        self.activeRun("pw ethport 1-3024    1")
        self.activeRun("pw ethport 3025-6048 2")
        self.activeRun("pw psn 1-6048 mpls")
        self.activeRun("pw enable 1-6048")
        self.activeRun("serialize .active.json")
        
        # Configure the standby
        self.standbyRun("driver role standby")
        self.standbyRun("device init")
        self.standbyRun("sdh map aug1.4.1-6.16,1.1-3.16 vc4")
        self.standbyRun("sdh map vc4.4.1-6.16,1.1-3.16 3xtug3s")
        self.standbyRun("sdh map tug3.4.1.1-6.16.3,1.1.1-3.16.3 7xtug2s")
        self.standbyRun("sdh map tug2.4.1.1.1-6.16.3.7,1.1.1.1-3.16.3.7 tu12")
        self.standbyRun("sdh map vc1x.4.1.1.1.1-6.16.3.7.3,1.1.1.1.1-3.16.3.7.3 de1")
        self.standbyRun("pw create satop 3025-6048,1-3024")
        self.standbyRun("pw circuit bind 3025-6048,1-3024 de1.4.1.1.1.1-6.16.3.7.3,1.1.1.1.1-3.16.3.7.3")
        self.standbyRun("pw ethport 1513-3024,1-1512 1")
        self.standbyRun("pw ethport 4537-6048,3025-4537 2")
        self.standbyRun("pw psn 3025-6048,1-3024 mpls")
        self.standbyRun("pw enable 3025-6048,1-3024")
        self.standbyRun("serialize .standby.json")
        
        self.serializeAndCompare()

    def testLoCep(self):
        # Configure the active
        self.activeRun("driver role active")
        self.activeRun("device init")
        self.activeRun("sdh map aug1.1.1-6.16 vc4")
        self.activeRun("sdh map vc4.1.1-6.16 3xtug3s")
        self.activeRun("sdh map tug3.1.1.1-6.16.3 7xtug2s")
        self.activeRun("sdh map tug2.1.1.1.1-6.16.3.7 tu12")
        self.activeRun("sdh map vc1x.1.1.1.1.1-6.16.3.7.3 c1x")
        self.activeRun("pw create cep 1-6048 basic")
        self.activeRun("pw circuit bind 1-6048 vc1x.1.1.1.1.1-6.16.3.7.3")
        self.activeRun("pw ethport 1-3024    1")
        self.activeRun("pw ethport 3025-6048 2")
        self.activeRun("pw psn 1-6048 mpls")
        self.activeRun("pw enable 1-6048")
        
        # Configure the standby
        self.standbyRun("driver role standby")
        self.standbyRun("device init")
        self.standbyRun("sdh map aug1.4.1-6.16,1.1-3.16 vc4")
        self.standbyRun("sdh map vc4.4.1-6.16,1.1-3.16 3xtug3s")
        self.standbyRun("sdh map tug3.4.1.1-6.16.3,1.1.1-3.16.3 7xtug2s")
        self.standbyRun("sdh map tug2.4.1.1.1-6.16.3.7,1.1.1.1-3.16.3.7 tu12")
        self.standbyRun("sdh map vc1x.4.1.1.1.1-6.16.3.7.3,1.1.1.1.1-3.16.3.7.3 c1x")
        self.standbyRun("pw create cep 3025-6048,1-3024 basic")
        self.standbyRun("pw circuit bind 3025-6048,1-3024 vc1x.4.1.1.1.1-6.16.3.7.3,1.1.1.1.1-3.16.3.7.3")
        self.standbyRun("pw ethport 1513-3024,1-1512 1")
        self.standbyRun("pw ethport 4537-6048,3025-4537 2")
        self.standbyRun("pw psn 3025-6048,1-3024 mpls")
        self.standbyRun("pw enable 3025-6048,1-3024")
        
        self.serializeAndCompare()

    def testHoCep(self):
        self.activeRun("driver role active")
        self.activeRun("device init")
        self.activeRun("sdh map aug1.1.1-8.16 3xvc3s")
        self.activeRun("sdh map vc3.1.1.1-8.16.3 c3")
        self.activeRun("pw create cep 1-384 basic")
        self.activeRun("pw circuit bind 1-384 vc3.1.1.1-8.16.3")
        self.activeRun("pw ethport 1-192 1")
        self.activeRun("pw ethport 193-384 2")
        self.activeRun("pw psn 1-384 mpls")
        self.activeRun("pw enable 1-384")
        
        self.standbyRun("driver role standby")
        self.standbyRun("device init")
        self.standbyRun("sdh map aug1.4.1-8.16,1.1-3.16 3xvc3s")
        self.standbyRun("sdh map vc3.4.1.1-8.16.3,1.1.1-3.16.3 c3")
        self.standbyRun("pw create cep 193-384,1-192 basic")
        self.standbyRun("pw circuit bind 193-384,1-192 vc3.5.1.1-8.16.3,1.1.1-4.16.3")
        self.standbyRun("pw ethport 193-384 2")
        self.standbyRun("pw ethport 1-192 1")
        self.standbyRun("pw psn 193-384,1-192 mpls")
        self.standbyRun("pw enable 193-384,1-192")
        
        self.serializeAndCompare()

if __name__ == "__main__":
    Af6021HaTest.run(Af60210011HaTest, sys.argv)

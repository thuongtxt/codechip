from Af60210051Randomizer import Af60210051Randomizer
import random

class Af60210031Randomizer(Af60210051Randomizer):
    def random(self):
        aList = []
        self.pwIdList = []
        self.labelIdList = []
        serlineMode = ['ec1', 'e3', 'ds3']

        for lineId in range(1, self.dbNumLines + 1):
            lineCliList = []
            subChannelsList = []
            
            lineDict = self.dictCreate('line', lineId, lineCliList, subChannelsList)
            aList.append(lineDict)
            serlineRandomMode = random.choice(serlineMode)
            lineCliList.append("pdh serline mode " + str(lineId) + " " + serlineRandomMode)
            
            if serlineRandomMode == 'ec1':
                self.vc3Random(lineId, str(lineId), subChannelsList)
            else:
                self.de3Random(lineId, str(lineId), subChannelsList)
                
        return aList
    
    def makeDefault(self):
        self.numLinesSet(48)
        self.numPwsSet(1344)
        self.numEthPortsSet(1)
        self.psnLabelStartIdSet(45000)


def main():
    pass

if __name__ == "__main__":
    main()
    

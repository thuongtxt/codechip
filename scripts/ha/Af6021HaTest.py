import unittest
import sys
import filecmp
import getopt
import os
from python.arrive.AtAppManager.AtAppManager import AtAppManager

sys.path.append("/opt/tools/python2.7/lib/python2.7/site-packages/unittest_xml_reporting-1.4.3-py2.7.egg/")

class Af6021HaTest(unittest.TestCase):
    activeApp     = None
    standbyApp    = None
    remoteCliPort = None
    activeSerializedFile = ".active.json"
    standbySerializedFile = ".standby.json"
    simulated = False
    verbose = False
    logFile = None
    
    def tearDown(self):
        self.logStop()
    
    def logStart(self, fileName=None):
        if fileName is None:
            fileName = self.__class__.__name__
        
        if self.logFile is not None:
            self.logStop()
            
        self.logFile = open(fileName, 'w')
    
    def logString(self, aString):
        if self.logFile is not None:
            self.logFile.write(aString)
    
    def logStop(self):
        if self.logFile is not None:
            self.logFile.close()
    
    def serializeAndCompare(self):
        self.standbyRun("driver role active")
        self.standbyRun("driver restore")
        
        activeFile  = self.__class__.activeSerializedFile
        standbyFile = self.__class__.standbySerializedFile
        self.activeRun("serialize " + activeFile, 60)
        self.standbyRun("serialize " + standbyFile, 60)
        sameDb = filecmp.cmp(activeFile, standbyFile)
        self.assertTrue(sameDb, "Database of two apps must be the same")
        if sameDb:
            os.remove(activeFile)
            os.remove(standbyFile)
    
    def standbyApplication(self):
        if self.__class__.standbyApp is None:
            self.__class__.standbyApp = AtAppManager.remoteApp("127.0.0.1", self.__class__.remoteCliPort)
        return self.__class__.standbyApp
    
    def activeApplication(self):
        if self.__class__.activeApp is not None:
            return self.__class__.activeApp
        
        self.__class__.activeApp = AtAppManager.localApp()
        self.__class__.activeApp.runCli("textui verbose disable")
            
        return self.__class__.activeApp
    
    def isVerbose(self):
        return self.__class__.verbose
    
    def activeRun(self, cli, timeout=60):
        if self.isVerbose():
            print "Active : " + cli
        cliResult = self.activeApplication().runCli(cli, timeout)
        self.assertTrue(cliResult.success())
        self.logString("self.activeRun(\"%s\")\n" % cli)
        return cliResult
        
    def standbyRun(self, cli, timeout=10):
        if self.isVerbose():
            print "Standby: " + cli
        cliResult = self.standbyApplication().runCli(cli, timeout)
        self.logString("self.standbyRun(\"%s\")\n" % cli)
        return cliResult

def run(testSuite, argv):
    remotePort = None
    verbose = False
    simulate = False
    
    try:
        opts, _ = getopt.getopt(argv[1:], "", ["remote=", "verbose", "simulate"])
    except:
        print "Usage: " + argv[0] + " --remote=<port> [--verbose] [--simulate]"
        return
    
    for opt, arg in opts:
        if opt == "--remote":
            remotePort = int(arg)
        if opt == "--verbose":
            verbose = True
        if opt == "--simulate":
            simulate = True

    testSuite.activeSerializedFile = ".active.json"
    testSuite.standbySerializedFile = ".standby.json"
    testSuite.remoteCliPort = remotePort
    testSuite.verbose = verbose
    testSuite.simulated = simulate
    
    suite = unittest.TestLoader().loadTestsFromTestCase(testSuite)
    try:
        import xmlrunner
        xmlrunner.XMLTestRunner().run(suite)
    except:
        return unittest.TextTestRunner(verbosity=2).run(suite)

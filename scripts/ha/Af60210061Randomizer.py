from Af60210012Randomizer import *

class Af60210061Randomizer(Af60210012Randomizer):       
    def hashHwLabel(self, hwLabel):
        swHbcePatern21_12 = (hwLabel >> 12) & 0x3ff
        swHbcePatern11_0  = hwLabel & 0xfff

        return swHbcePatern11_0 ^ swHbcePatern21_12
    
    def randomConflictLabelGenerate(self, startId, endId, idList, ethPortId):
        hwLabel  = self.hwLabelBuild(0, self.cMplsType, ethPortId)
        bit13_12 = (self.currentHash ^ hwLabel) & 0x3
        bit11_10 = ((self.currentHash >> 10) ^ 0x0) & 0x3
        
        for bit9_2 in range(0xff):
            bit21_14 = ((self.currentHash >> 2) ^ bit9_2) & 0xff
            fullHwLabel = hwLabel | (bit9_2 << 2) | (bit11_10 << 10) | (bit13_12 << 12) |  (bit21_14 << 14)
            newValue = (fullHwLabel >> 2) & 0xfffff
            
            if newValue not in idList and startId <= newValue <= endId:
                return newValue
            
        return self.cInvalidLabel
    
def main():
    pass

if __name__ == "__main__":
    main()
    
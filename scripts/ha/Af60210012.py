import random
import sys

import Af6021HaTest
from Af60210012Randomizer import Af60210012Randomizer

class Af60210012HaTest(Af6021HaTest.Af6021HaTest):
    @staticmethod
    def defaultTimeout():
        return 120

    def testHspwBasic(self):
        self.activeRun("driver role active")
        self.activeRun("device init")
        self.activeRun("sdh map aug1.1.1 vc4")
        self.activeRun("sdh map vc4.1.1 3xtug3s")
        self.activeRun("sdh map tug3.1.1.1 7xtug2s")
        self.activeRun("sdh map tug2.1.1.1.1 tu11")
        self.activeRun("sdh map vc1x.1.1.1.1.1 de1")
        self.activeRun("pw create satop 1")
        self.activeRun("pw circuit bind 1 de1.1.1.1.1.1")
        self.activeRun("pw ethport 1 1")
        self.activeRun("pw psn 1 mpls")
        self.activeRun("pw backup psn 1 mpls 2")
        self.activeRun("pw enable 1")
        self.activeRun("pw group hs create 1")
        self.activeRun("pw group hs add 1 1")
        
        self.standbyRun("driver role standby")
        self.standbyRun("debug driver ha simulate enable")
        self.standbyRun("device init")
        self.standbyRun("sdh map aug1.1.1 vc4")
        self.standbyRun("sdh map vc4.1.1 3xtug3s")
        self.standbyRun("sdh map tug3.1.1.1 7xtug2s")
        self.standbyRun("sdh map tug2.1.1.1.1 tu11")
        self.standbyRun("sdh map vc1x.1.1.1.1.1 de1")
        self.standbyRun("pw create satop 1")
        self.standbyRun("pw circuit bind 1 de1.1.1.1.1.1")
        self.standbyRun("pw ethport 1 1")
        self.standbyRun("pw psn 1 mpls")
        self.standbyRun("pw backup psn 1 mpls 2")
        self.standbyRun("pw enable 1")
        self.standbyRun("pw group hs create 1")
        self.standbyRun("pw group hs add 1 1")
        
        self.serializeAndCompare()

    def testDs1Satop(self):
        # Configure the active
        self.activeRun("driver role active")
        self.activeRun("device init")
        self.activeRun("sdh map aug1.1.1-2.16 vc4")
        self.activeRun("sdh map vc4.1.1-2.16 3xtug3s")
        self.activeRun("sdh map tug3.1.1.1-2.16.3 7xtug2s")
        self.activeRun("sdh map tug2.1.1.1.1-2.16.3.7 tu11")
        self.activeRun("sdh map vc1x.1.1.1.1.1-2.16.3.7.4 de1")
        self.activeRun("pw create satop 1-2688")
        self.activeRun("pw circuit bind 1-2688 de1.1.1.1.1.1-2.16.3.7.4")
        self.activeRun("pw ethport 1-2688    1")
        self.activeRun("pw psn 1-2688 mpls")
        self.activeRun("pw enable 1-2688")
        
        # Configure the standby
        self.standbyRun("driver role standby")
        self.standbyRun("device init")
        self.standbyRun("sdh map aug1.2.1-2.16,1.1-1.16 vc4")
        self.standbyRun("sdh map vc4.2.1-2.16,1.1-1.16 3xtug3s")
        self.standbyRun("sdh map tug3.2.1.1-2.16.3,1.1.1-1.16.3 7xtug2s")
        self.standbyRun("sdh map tug2.2.1.1.1-2.16.3.7,1.1.1.1-1.16.3.7 tu11", self.defaultTimeout())
        self.standbyRun("sdh map vc1x.2.1.1.1.1-2.16.3.7.4,1.1.1.1.1-1.16.3.7.4 de1")
        self.standbyRun("pw create satop 1345-2688,1-1344")
        self.standbyRun("pw circuit bind 1345-2688,1-1344 de1.2.1.1.1.1-2.16.3.7.4,1.1.1.1.1-1.16.3.7.4")
        self.standbyRun("pw ethport 1345-2688,1-1344 1", self.defaultTimeout())
        self.standbyRun("pw psn 1345-2688,1-1344 mpls", self.defaultTimeout())
        self.standbyRun("pw enable 1345-2688,1-1344", self.defaultTimeout())
        
        self.serializeAndCompare()
    
    def testLoCep(self):
        # Configure the active
        self.activeRun("driver role active")
        self.activeRun("device init")
        self.activeRun("sdh map aug1.1.1-2.16 vc4")
        self.activeRun("sdh map vc4.1.1-2.16 3xtug3s")
        self.activeRun("sdh map tug3.1.1.1-2.16.3 7xtug2s")
        self.activeRun("sdh map tug2.1.1.1.1-2.16.3.7 tu11")
        self.activeRun("sdh map vc1x.1.1.1.1.1-2.16.3.7.4 c1x")
        self.activeRun("pw create cep 1-2688 basic")
        self.activeRun("pw circuit bind 1-2688 vc1x.1.1.1.1.1-2.16.3.7.4")
        self.activeRun("pw ethport 1-2688 1")
        self.activeRun("pw psn 1-2688 mpls")
        self.activeRun("pw enable 1-2688")
        
        # Configure the standby
        self.standbyRun("driver role standby")
        self.standbyRun("device init")
        self.standbyRun("sdh map aug1.2.1-2.16,1.1-1.16 vc4")
        self.standbyRun("sdh map vc4.2.1-2.16,1.1-1.16 3xtug3s")
        self.standbyRun("sdh map tug3.2.1.1-2.16.3,1.1.1-1.16.3 7xtug2s")
        self.standbyRun("sdh map tug2.2.1.1.1-2.16.3.7,1.1.1.1-1.16.3.7 tu11", self.defaultTimeout())
        self.standbyRun("sdh map vc1x.2.1.1.1.1-2.16.3.7.4,1.1.1.1.1-1.16.3.7.4 c1x", self.defaultTimeout())
        self.standbyRun("pw create cep 1345-2688,1-1344 basic")
        self.standbyRun("pw circuit bind 1345-2688,1-1344 vc1x.2.1.1.1.1-2.16.3.7.4,1.1.1.1.1-1.16.3.7.4", self.defaultTimeout())
        self.standbyRun("pw ethport 1345-2688,1-1344 1", self.defaultTimeout())
        self.standbyRun("pw psn 1345-2688,1-1344 mpls", self.defaultTimeout())
        self.standbyRun("pw enable 1345-2688,1-1344", self.defaultTimeout())
        
        self.serializeAndCompare()

    def testVc3Cep(self):
        self.activeRun("driver role active")
        self.activeRun("device init")
        self.activeRun("sdh map aug1.1.1-2.16 3xvc3s")
        self.activeRun("sdh map vc3.1.1.1-2.16.3 c3")
        self.activeRun("pw create cep 1-96 basic")
        self.activeRun("pw circuit bind 1-96 vc3.1.1.1-2.16.3")
        self.activeRun("pw ethport 1-96 1")
        self.activeRun("pw psn 1-96 mpls")
        self.activeRun("pw enable 1-96")
        
        self.standbyRun("driver role standby")
        self.standbyRun("device init")
        self.standbyRun("sdh map aug1.2.1-2.16,1.1-1.16 3xvc3s")
        self.standbyRun("sdh map vc3.2.1.1-2.16.3,1.1.1-1.16.3 c3")
        self.standbyRun("pw create cep 49-96,1-48 basic")
        self.standbyRun("pw circuit bind 49-96,1-48 vc3.2.1.1-2.16.3,1.1.1-1.16.3")
        self.standbyRun("pw ethport 1-96 1")
        self.standbyRun("pw psn 49-96,1-48 mpls")
        self.standbyRun("pw enable 49-96,1-48")
        
        self.serializeAndCompare()

    def testVc4Cep(self):
        self.activeRun("driver role active")
        self.activeRun("device init")
        self.activeRun("sdh map aug1.1.1-2.16 vc4")
        self.activeRun("sdh map vc4.1.1-2.16 c4")
        self.activeRun("pw create cep 1-32 basic")
        self.activeRun("pw circuit bind 1-32 vc4.1.1-2.16")
        self.activeRun("pw ethport 1-32 1")
        self.activeRun("pw psn 1-32 mpls")
        self.activeRun("pw enable 1-32")
        
        self.standbyRun("driver role standby")
        self.standbyRun("device init")
        self.standbyRun("sdh map aug1.2.1-2.16,1.1-1.16 vc4")
        self.standbyRun("sdh map vc4.2.1-2.16,1.1-1.16 c4")
        self.standbyRun("pw create cep 17-32,1-16 basic")
        self.standbyRun("pw circuit bind 17-32,1-16 vc4.2.1-2.16,1.1-1.16")
        self.standbyRun("pw ethport 1-32 1")
        self.standbyRun("pw psn 17-32,1-16 mpls")
        self.standbyRun("pw enable 17-32,1-16")
        
        self.serializeAndCompare()
    
    def testVc4_4cCep(self):
        self.activeRun("driver role active")
        self.activeRun("device init")
        self.activeRun("sdh map aug4.1.1-2.4 vc4_4c")
        self.activeRun("sdh map vc4_4c.1.1-2.4 c4_4c")
        self.activeRun("pw create cep 1-8 basic")
        self.activeRun("pw circuit bind 1-8 vc4_4c.1.1-2.4")
        self.activeRun("pw ethport 1-8 1")
        self.activeRun("pw psn 1-8 mpls")
        self.activeRun("pw enable 1-8")
        
        self.standbyRun("driver role standby")
        self.standbyRun("device init")
        self.standbyRun("sdh map aug4.2.1-2.4,1.1-1.4 vc4_4c")
        self.standbyRun("sdh map vc4_4c.2.1-2.4,1.1-1.4 c4_4c")
        self.standbyRun("pw create cep 5-8,1-4 basic")
        self.standbyRun("pw circuit bind 5-8,1-4 vc4_4c.2.1-2.4,1.1-1.4")
        self.standbyRun("pw ethport 1-8 1")
        self.standbyRun("pw psn 5-8,1-4 mpls")
        self.standbyRun("pw enable 5-8,1-4")
        
        self.serializeAndCompare()
    
    def testVc4_16cCep(self):
        self.activeRun("driver role active")
        self.activeRun("device init")
        self.activeRun("sdh map aug16.1.1-2.1 vc4_16c")
        self.activeRun("sdh map vc4_16c.1.1-2.1 c4_16c")
        self.activeRun("pw create cep 1-2 basic")
        self.activeRun("pw circuit bind 1-2 vc4_16c.1.1-2.1")
        self.activeRun("pw ethport 1-2 1")
        self.activeRun("pw psn 1-2 mpls")
        self.activeRun("pw enable 1-2")
        
        self.standbyRun("driver role standby")
        self.standbyRun("device init")
        self.standbyRun("sdh map aug16.2.1,1.1 vc4_16c")
        self.standbyRun("sdh map vc4_16c.2.1,1.1 c4_16c")
        self.standbyRun("pw create cep 2,1 basic")
        self.standbyRun("pw circuit bind 2,1 vc4_16c.2.1,1.1")
        self.standbyRun("pw ethport 1-2 1")
        self.standbyRun("pw psn 2,1 mpls")
        self.standbyRun("pw enable 2,1")
        
        self.serializeAndCompare()
    
    def testDs1Cesop(self):
        # Configure the active
        self.activeRun("driver role active")
        self.activeRun("device init")
        self.activeRun("sdh map aug1.1.1-2.16 vc4")
        self.activeRun("sdh map vc4.1.1-2.16 3xtug3s")
        self.activeRun("sdh map tug3.1.1.1-2.16.3 7xtug2s")
        self.activeRun("sdh map tug2.1.1.1.1-2.16.3.7 tu11")
        self.activeRun("sdh map vc1x.1.1.1.1.1-2.16.3.7.4 de1")
        self.activeRun("pdh de1 framing 1.1.1.1.1-2.16.3.7.4 ds1_sf")
        self.activeRun("pdh de1 nxds0create 1.1.1.1.1-1.16.3.7.4 1-15")
        self.activeRun("pdh de1 nxds0create 2.1.1.1.1-2.16.3.7.4 1-15")       
        self.activeRun("pw create cesop 1-2688 basic")
        self.activeRun("pw circuit bind 1-1344 nxds0.1.1.1.1.1-1.16.3.7.4.1-15")
        self.activeRun("pw circuit bind 1345-2688 nxds0.2.1.1.1.1-2.16.3.7.4.1-15")
        self.activeRun("pw ethport 1-2688    1")
        self.activeRun("pw psn 1-2688 mpls")
        self.activeRun("pw enable 1-2688")
        
        # Configure the standby
        self.standbyRun("driver role standby")
        self.standbyRun("device init")
        self.standbyRun("sdh map aug1.2.1-2.16,1.1-1.16 vc4")
        self.standbyRun("sdh map vc4.2.1-2.16,1.1-1.16 3xtug3s")
        self.standbyRun("sdh map tug3.2.1.1-2.16.3,1.1.1-1.16.3 7xtug2s")
        self.standbyRun("sdh map tug2.2.1.1.1-2.16.3.7,1.1.1.1-1.16.3.7 tu11", self.defaultTimeout())
        self.standbyRun("sdh map vc1x.2.1.1.1.1-2.16.3.7.4,1.1.1.1.1-1.16.3.7.4 de1")
        self.standbyRun("pdh de1 framing 2.1.1.1.1-2.16.3.7.4,1.1.1.1.1-1.16.3.7.4 ds1_sf")
        self.standbyRun("pdh de1 nxds0create 2.1.1.1.1-2.16.3.7.4 1-15")
        self.standbyRun("pdh de1 nxds0create 1.1.1.1.1-1.16.3.7.4 1-15")
        self.standbyRun("pw create cesop 1345-2688 basic")
        self.standbyRun("pw circuit bind 1345-2688 nxds0.2.1.1.1.1-2.16.3.7.4.1-15")
        self.standbyRun("pw create cesop 1-1344 basic")
        self.standbyRun("pw circuit bind 1-1344 nxds0.1.1.1.1.1-1.16.3.7.4.1-15")

        self.standbyRun("pw ethport 1513-2688,1-1512 1", self.defaultTimeout())
        self.standbyRun("pw psn 1345-2688,1-1344 mpls", self.defaultTimeout())
        self.standbyRun("pw enable 1345-2688,1-1344", self.defaultTimeout())
        
        self.serializeAndCompare()

    def random(self):
        self.randomizer = Af60210012Randomizer()
        return self.randomizer.defaultRandom()
    
    def testIssuRandom(self):
        def _activeRun(cli):
            self.activeRun(cli)
        def _standbyRun(cli):
            self.standbyRun(cli)
        
        def _applyDict(aDict, cliRunFunc):
            for cli in aDict["cli"]:
                cliRunFunc(cli)
            
            subChannels = aDict["subChannels"]
            random.shuffle(subChannels)
            for subChannel in subChannels:
                _applyDict(subChannel, cliRunFunc)
            
        for _ in range(0, 10):
            self.activeRun("driver role active")
            self.standbyRun("driver role standby")
            linesConfiguration = self.random()
            
            # Randomly apply this configuration on active
            self.activeRun("device init")
            random.shuffle(linesConfiguration)
            for line in linesConfiguration:
                _applyDict(line, _activeRun)
                
            # Randomly apply this configuration on standby
            self.standbyRun("device init")
            random.shuffle(linesConfiguration)
            for line in linesConfiguration:
                _applyDict(line, _standbyRun)
            
            self.serializeAndCompare()
        
    def testRedundancyRandom(self):
        def _applyDict(aDict):
            for cli in aDict["cli"]:
                self.activeRun(cli)
                self.standbyRun(cli)
            
            subChannels = aDict["subChannels"]
            random.shuffle(subChannels)
            for subChannel in subChannels:
                _applyDict(subChannel)
        
        for _ in range(0, 10):
            self.activeRun("driver role active")
            self.standbyRun("driver role standby")
        
            self.activeRun("device init")
            self.standbyRun("device init")
            
            linesConfiguration = self.random()
            random.shuffle(linesConfiguration)
            for line in linesConfiguration:
                _applyDict(line)
                
            self.serializeAndCompare()
    
    @staticmethod
    def randomConflictLabels():
        randomizer = Af60210012Randomizer()
        randomizer.maxNumConflictLabelSet(10)
        randomizer.maxNumConflictGroupSet(5)
        return randomizer.defaultRandom()
    
    def testRandomConflictLabel(self):
        def _applyDict(aDict):
            for cli in aDict["cli"]:
                self.activeRun(cli)
                self.standbyRun(cli)
            
            subChannels = aDict["subChannels"]
            random.shuffle(subChannels)
            for subChannel in subChannels:
                _applyDict(subChannel)
        
        for _ in range(0, 10):
            self.activeRun("driver role active")
            self.standbyRun("driver role standby")
        
            self.activeRun("device init")
            self.standbyRun("device init")
            
            linesConfiguration = self.randomConflictLabels()
            random.shuffle(linesConfiguration)
            for line in linesConfiguration:
                _applyDict(line)
                
            self.serializeAndCompare()
                    
if __name__ == "__main__":
    Af6021HaTest.run(Af60210012HaTest, sys.argv)

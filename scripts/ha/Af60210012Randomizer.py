from Af60210051Randomizer import Af60210051Randomizer

class Af60210012Randomizer(Af60210051Randomizer):    
    def hwLabelBuild(self, label, psnType, portId):
        return psnType | label << 2
    
    def hashHwLabel(self, hwLabel):
        swHbcePatern21_13 = (hwLabel >> 13) & 0x1ff
        swHbcePatern12_0  = hwLabel & 0x1fff

        return swHbcePatern12_0 ^ swHbcePatern21_13
    
    def randomConflictLabelGenerate(self, startId, endId, idList, ethPortId):
        hwLabel  = self.hwLabelBuild(0, self.cMplsType, ethPortId)
        bit14_13 = (self.currentHash ^ hwLabel) & 0x3
        bit12_9 = ((self.currentHash >> 9) ^ 0x0) & 0xf
        
        for bit8_2 in range(0x7f):
            bit21_15 = ((self.currentHash >> 2) ^ bit8_2) & 0x7f
            fullHwLabel = hwLabel | (bit8_2 << 2) | (bit12_9 << 9) | (bit14_13 << 13) |  (bit21_15 << 15)
            newValue = (fullHwLabel >> 2) & 0xfffff
            
            if newValue not in idList and startId <= newValue <= endId:
                return newValue
            
        return self.cInvalidLabel
    
    def makeDefault(self):
        self.numLinesSet(2)
        self.numPwsSet(1344 * 2)
        self.numEthPortsSet(1)
        self.psnLabelStartIdSet(45000)
    
def main():
    pass

if __name__ == "__main__":
    main()
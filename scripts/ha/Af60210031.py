import Af6021HaTest
from Af60210031Randomizer import Af60210031Randomizer
import random
import sys

class Af60210031HaTest(Af6021HaTest.Af6021HaTest):
    def testHspwBasic(self):        
        self.activeRun("driver role active")
        self.activeRun("device init")
        self.activeRun("pdh de3 framing 1-48 ds3_cbit_28ds1")
        self.activeRun("pw create satop 1")
        self.activeRun("pw circuit bind 1 de1.1.1.1")
        self.activeRun("pw ethport 1 1")
        self.activeRun("pw psn 1 mpls")
        self.activeRun("pw backup psn 1 mpls 2")
        self.activeRun("pw enable 1")
        self.activeRun("pw group hs create 1")
        self.activeRun("pw group hs add 1 1")
        
        self.standbyRun("driver role standby")
        self.standbyRun("device init")
        self.standbyRun("pdh de3 framing 25-48,1-24 ds3_cbit_28ds1")
        self.standbyRun("pw create satop 1")
        self.standbyRun("pw circuit bind 1 de1.1.1.1")
        self.standbyRun("pw ethport 1 1")
        self.standbyRun("pw psn 1 mpls")
        self.standbyRun("pw backup psn 1 mpls 2")
        self.standbyRun("pw enable 1")
        self.standbyRun("pw group hs create 1")
        self.standbyRun("pw group hs add 1 1")
        self.standbyRun("driver role active")
        self.standbyRun("driver restore")
        
        self.serializeAndCompare()

    def testDs3Satop(self):
        self.activeRun("driver role active")
        self.activeRun("device init")
        self.activeRun("pw create satop 1-48") 
        self.activeRun("pw circuit bind 1-48 de3.1-48")
        self.activeRun("pw ethport 1-48 1")
        self.activeRun("pw psn 1-48 mpls")
        self.activeRun("pw enable 1-48")
        
        self.standbyRun("driver role standby")
        self.standbyRun("device init")
        self.standbyRun("pw create satop 25-48,1-24") 
        self.standbyRun("pw circuit bind 25-48,1-24 de3.25-48,1-24")
        self.standbyRun("pw ethport 25-48,1-24 1")
        self.standbyRun("pw psn 25-48,1-24 mpls")
        self.standbyRun("pw enable 25-48,1-24")
        
        self.serializeAndCompare()
    
    def testDs3Satop_1(self):
        self.activeRun("device init")
        self.activeRun("pdh de3 framing 1 ds3_unframed")
        self.activeRun("pdh de3 timing 1 system none")
        self.activeRun("pdh de3 enable 1")
        self.activeRun("pw create satop 1")
        self.activeRun("pw circuit bind 1 de3.1")
        self.activeRun("pw psn 1 mpls")
        self.activeRun("pw mpls innerlabel 1 1.1.255")
        self.activeRun("pw mpls expectedlabel 1 1")
        self.activeRun("pw payloadsize 1 700")
        self.activeRun("pw enable 1")
        self.activeRun("pw rtp disable 1")
        
        self.standbyRun("driver role standby")
        self.standbyRun("device init")
        self.standbyRun("pdh de3 framing 1 ds3_unframed")
        self.standbyRun("pdh de3 timing 1 system none")
        self.standbyRun("pdh de3 enable 1")
        self.standbyRun("pw create satop 1")
        self.standbyRun("pw circuit bind 1 de3.1")
        self.standbyRun("pw psn 1 mpls")
        self.standbyRun("pw mpls innerlabel 1 1.1.255")
        self.standbyRun("pw mpls expectedlabel 1 1")
        self.standbyRun("pw payloadsize 1 700")
        self.standbyRun("pw enable 1")
        self.standbyRun("pw rtp disable 1")
        
        self.serializeAndCompare()
    
    def testDs1Satop(self):
        self.activeRun("driver role active")
        self.activeRun("device init")
        self.activeRun("pdh de3 framing 1-48 ds3_cbit_28ds1")
        self.activeRun("pw create satop 1-1344")
        self.activeRun("pw circuit bind 1-1344 de1.1.1.1-48.7.4")
        self.activeRun("pw ethport 1-1344 1")
        self.activeRun("pw psn 1-1344 mpls")
        self.activeRun("pw enable 1-1344")
        
        self.standbyRun("driver role standby")
        self.standbyRun("device init")
        self.standbyRun("pdh de3 framing 25-48,1-24 ds3_cbit_28ds1")
        self.standbyRun("pw create satop 673-1344,1-672")
        self.standbyRun("pw circuit bind 673-1344,1-672 de1.25.1.1-48.7.4,1.1.1-24.7.4")
        self.standbyRun("pw ethport 673-1344,1-672 1")
        self.standbyRun("pw psn 673-1344,1-672 mpls")
        self.standbyRun("pw enable 673-1344,1-672")
        
        self.serializeAndCompare()

    def testDs1Cesop(self):
        self.activeRun("driver role active")
        self.activeRun("device init")
        self.activeRun("pdh de3 framing 1-48 ds3_cbit_28ds1")
        self.activeRun("pdh de1 framing 1.1.1-48.7.4 ds1_sf")
        self.activeRun("pdh de1 nxds0create 1.1.1-48.7.4 1-15")
        self.activeRun("pdh de1 nxds0create 1.1.1-48.7.4 16-23")
        self.activeRun("pw create cesop 1-1344 basic")
        self.activeRun("pw circuit bind 1-672 nxds0.1.1.1-24.7.4.1-15")
        self.activeRun("pw circuit bind 673-1344 nxds0.1.1.1-24.7.4.16-23")
        self.activeRun("pw ethport 1-1344 1")
        self.activeRun("pw psn 1-1344 mpls")
        self.activeRun("pw enable 1-1344")
        
        # Do not configure channels timing on standby driver because channel timing may be changed in interrupt context
        # that never happen on standby driver
        self.activeRun("pdh de1 timing 1.1.1-12.7.4 acr 1-336")
        self.activeRun("pdh de1 timing 13.1.1-24.7.4 dcr 1009-1344")
        
        self.standbyRun("driver role standby")
        self.standbyRun("device init")
        self.standbyRun("pdh de3 framing 25-48,1-24 ds3_cbit_28ds1")
        self.standbyRun("pdh de1 framing 1.1.1-48.7.4 ds1_sf")
        self.standbyRun("pdh de1 nxds0create 1.1.1-48.7.4 1-15")
        self.standbyRun("pdh de1 nxds0create 1.1.1-48.7.4 16-23")
        self.standbyRun("pw create cesop 1-1344 basic")
        self.standbyRun("pw circuit bind 673-1344 nxds0.1.1.1-24.7.4.16-23")
        self.standbyRun("pw circuit bind 1-672 nxds0.1.1.1-24.7.4.1-15")
        self.standbyRun("pw ethport 673-1344,1-672 1")
        self.standbyRun("pw psn 673-1344,1-672 mpls")
        self.standbyRun("pw enable 673-1344,1-672")       
        self.serializeAndCompare()
        
    def _random(self):
        self.randomizer = Af60210031Randomizer()
        return self.randomizer.defaultRandom()
    
    def TestIssuRandomWithDeprovision(self, deprovision=False):
        def _activeRun(cli):
            self.activeRun(cli)
        def _standbyRun(cli):
            self.standbyRun(cli)
        
        def _applyDict(aDict, cliRunFunc):
            for cli in aDict["cli"]:
                cliRunFunc(cli)
            
            subChannels = aDict["subChannels"]
            random.shuffle(subChannels)
            for subChannel in subChannels:
                _applyDict(subChannel, cliRunFunc)
            
        for _ in range(0, 10):
            self.activeRun("driver role active")
            self.standbyRun("driver role standby")
        
            linesConfiguration = self._random()
            
            # Randomly apply this configuration on active
            self.activeRun("device init")
            random.shuffle(linesConfiguration)
            for line in linesConfiguration:
                _applyDict(line, _activeRun)
                
            # Randomly apply this configuration on standby
            self.standbyRun("device init")
            random.shuffle(linesConfiguration)
            for line in linesConfiguration:
                _applyDict(line, _standbyRun)
            
            if deprovision:
                def _deprovision(deprovisionInfo, cliRunFunc):
                    for pwCliList in deprovisionInfo:
                        for cli in pwCliList:
                            cliRunFunc(cli)
                            
                # Have deprovision random sequence
                deprovisions = self.randomizer.randomDeprovisonPw()
                            
                # Apply on active
                random.shuffle(deprovisions)
                _deprovision(deprovisions, _activeRun)
                
                # Apply on standby
                random.shuffle(deprovisions)
                _deprovision(deprovisions, _standbyRun)
            
            self.serializeAndCompare()
    
    def testIssuRandom(self):
        self.TestIssuRandomWithDeprovision(False)
        
    def testIssuRandomWithDeprovision(self):
        self.TestIssuRandomWithDeprovision(True)
            
    def testRedundancyRandom(self):
        def _applyDict(aDict):
            for cli in aDict["cli"]:
                self.activeRun(cli)
                self.standbyRun(cli)
            
            subChannels = aDict["subChannels"]
            random.shuffle(subChannels)
            for subChannel in subChannels:
                _applyDict(subChannel)
        
        for _ in range(0, 10):
            self.activeRun("driver role active")
            self.standbyRun("driver role standby")
        
            self.activeRun("device init")
            self.standbyRun("device init")
            
            linesConfiguration = self._random()
            random.shuffle(linesConfiguration)
            for line in linesConfiguration:
                _applyDict(line)
                
            self.serializeAndCompare()
            
if __name__ == "__main__":
    Af6021HaTest.run(Af60210031HaTest, sys.argv)

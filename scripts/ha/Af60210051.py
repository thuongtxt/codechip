import random
import sys

import Af6021HaTest
from Af60210051Randomizer import Af60210051Randomizer

class Af60210051HaTest(Af6021HaTest.Af6021HaTest):
    _xfiGroups = [1, 2]
        
    @staticmethod
    def defaultTimeout():
        return 120
    
    @staticmethod
    def anyXfiGroup():
        return random.choice(Af60210051HaTest._xfiGroups)
    
    def setActiveXfiGroup(self, xfiGroup):
        self.activeRun("cisco xfi group %d" % xfiGroup)
            
    def setStandbyXfiGroup(self, xfiGroup):
        self.standbyRun("cisco xfi group %d" % xfiGroup)
            
    def testHspwBasic(self):
        xfiGroup = self.anyXfiGroup()
        
        self.activeRun("driver role active")
        self.activeRun("device init")
        self.setActiveXfiGroup(xfiGroup)
        self.activeRun("sdh map aug1.1.1 vc4")
        self.activeRun("sdh map vc4.1.1 3xtug3s")
        self.activeRun("sdh map tug3.1.1.1 7xtug2s")
        self.activeRun("sdh map tug2.1.1.1.1 tu11")
        self.activeRun("sdh map vc1x.1.1.1.1.1 de1")
        self.activeRun("pw create satop 1")
        self.activeRun("pw circuit bind 1 de1.1.1.1.1.1")
        self.activeRun("pw ethport 1 1")
        self.activeRun("pw psn 1 mpls")
        self.activeRun("pw backup psn 1 mpls 2")
        self.activeRun("pw enable 1")
        self.activeRun("pw group hs create 1")
        self.activeRun("pw group hs add 1 1")
        
        self.standbyRun("driver role standby")
        self.standbyRun("device init")
        self.setStandbyXfiGroup(xfiGroup)
        self.standbyRun("sdh map aug1.1.1 vc4")
        self.standbyRun("sdh map vc4.1.1 3xtug3s")
        self.standbyRun("sdh map tug3.1.1.1 7xtug2s")
        self.standbyRun("sdh map tug2.1.1.1.1 tu11")
        self.standbyRun("sdh map vc1x.1.1.1.1.1 de1")
        self.standbyRun("pw create satop 1")
        self.standbyRun("pw circuit bind 1 de1.1.1.1.1.1")
        self.standbyRun("pw ethport 1 1")
        self.standbyRun("pw psn 1 mpls")
        self.standbyRun("pw backup psn 1 mpls 2")
        self.standbyRun("pw enable 1")
        self.standbyRun("pw group hs create 1")
        self.standbyRun("pw group hs add 1 1")
        self.standbyRun("driver role active")
        self.standbyRun("driver restore")
        
        self.serializeAndCompare()

    def testDs1Satop(self):
        xfiGroup = self.anyXfiGroup()
        
        # Configure the active
        self.activeRun("driver role active")
        self.activeRun("device init")
        self.setActiveXfiGroup(xfiGroup)
        self.activeRun("sdh map aug1.1.1-4.16 vc4")
        self.activeRun("sdh map vc4.1.1-4.16 3xtug3s")
        self.activeRun("sdh map tug3.1.1.1-4.16.3 7xtug2s")
        self.activeRun("sdh map tug2.1.1.1.1-4.16.3.7 tu11")
        self.activeRun("sdh map vc1x.1.1.1.1.1-4.16.3.7.4 de1")
        self.activeRun("pw create satop 1-5376")
        self.activeRun("pw circuit bind 1-5376 de1.1.1.1.1.1-4.16.3.7.4")
        self.activeRun("pw ethport 1-2688    1")
        self.activeRun("pw ethport 2689-5376 2")
        self.activeRun("pw psn 1-5376 mpls")
        self.activeRun("pw enable 1-5376")
        
        # Configure the standby
        self.standbyRun("driver role standby")
        self.standbyRun("device init")
        self.setStandbyXfiGroup(xfiGroup)
        self.standbyRun("sdh map aug1.3.1-4.16,1.1-2.16 vc4")
        self.standbyRun("sdh map vc4.3.1-4.16,1.1-2.16 3xtug3s")
        self.standbyRun("sdh map tug3.3.1.1-4.16.3,1.1.1-2.16.3 7xtug2s")
        self.standbyRun("sdh map tug2.3.1.1.1-4.16.3.7,1.1.1.1-2.16.3.7 tu11", self.defaultTimeout())
        self.standbyRun("sdh map vc1x.3.1.1.1.1-4.16.3.7.4,1.1.1.1.1-2.16.3.7.4 de1")
        self.standbyRun("pw create satop 2689-5376,1-2688")
        self.standbyRun("pw circuit bind 2689-5376,1-2688 de1.3.1.1.1.1-4.16.3.7.4,1.1.1.1.1-2.16.3.7.4")
        self.standbyRun("pw ethport 1513-2688,1-1512 1", self.defaultTimeout())
        self.standbyRun("pw ethport 4537-5376,2689-4537 2", self.defaultTimeout())
        self.standbyRun("pw psn 2689-5376,1-2688 mpls", self.defaultTimeout())
        self.standbyRun("pw enable 2689-5376,1-2688", self.defaultTimeout())
        
        self.serializeAndCompare()

    def testLoCep(self):
        xfiGroup = self.anyXfiGroup()
        
        # Configure the active
        self.activeRun("driver role active")
        self.activeRun("device init")
        self.setActiveXfiGroup(xfiGroup)
        self.activeRun("sdh map aug1.1.1-4.16 vc4")
        self.activeRun("sdh map vc4.1.1-4.16 3xtug3s")
        self.activeRun("sdh map tug3.1.1.1-4.16.3 7xtug2s")
        self.activeRun("sdh map tug2.1.1.1.1-4.16.3.7 tu11")
        self.activeRun("sdh map vc1x.1.1.1.1.1-4.16.3.7.4 c1x")
        self.activeRun("pw create cep 1-5376 basic")
        self.activeRun("pw circuit bind 1-5376 vc1x.1.1.1.1.1-4.16.3.7.4")
        self.activeRun("pw ethport 1-2688    1")
        self.activeRun("pw ethport 2689-5376 2")
        self.activeRun("pw psn 1-5376 mpls")
        self.activeRun("pw enable 1-5376")
        
        # Configure the standby
        self.standbyRun("driver role standby")
        self.standbyRun("device init")
        self.setStandbyXfiGroup(xfiGroup)
        self.standbyRun("sdh map aug1.3.1-4.16,1.1-2.16 vc4")
        self.standbyRun("sdh map vc4.3.1-4.16,1.1-2.16 3xtug3s")
        self.standbyRun("sdh map tug3.3.1.1-4.16.3,1.1.1-2.16.3 7xtug2s")
        self.standbyRun("sdh map tug2.3.1.1.1-4.16.3.7,1.1.1.1-2.16.3.7 tu11", self.defaultTimeout())
        self.standbyRun("sdh map vc1x.3.1.1.1.1-4.16.3.7.4,1.1.1.1.1-2.16.3.7.4 c1x", self.defaultTimeout())
        self.standbyRun("pw create cep 2689-5376,1-2688 basic")
        self.standbyRun("pw circuit bind 2689-5376,1-2688 vc1x.3.1.1.1.1-4.16.3.7.4,1.1.1.1.1-2.16.3.7.4", self.defaultTimeout())
        self.standbyRun("pw ethport 1513-2688,1-1512 1", self.defaultTimeout())
        self.standbyRun("pw ethport 4537-5376,2689-4537 2", self.defaultTimeout())
        self.standbyRun("pw psn 2689-5376,1-2688 mpls", self.defaultTimeout())
        self.standbyRun("pw enable 2689-5376,1-2688", self.defaultTimeout())
        
        self.serializeAndCompare()

    def testVc3Cep(self):
        xfiGroup = self.anyXfiGroup()
        
        self.activeRun("driver role active")
        self.activeRun("device init")
        self.setActiveXfiGroup(xfiGroup)
        self.activeRun("sdh map aug1.1.1-4.16 3xvc3s")
        self.activeRun("sdh map vc3.1.1.1-4.16.3 c3")
        self.activeRun("pw create cep 1-192 basic")
        self.activeRun("pw circuit bind 1-192 vc3.1.1.1-4.16.3")
        self.activeRun("pw ethport 1-96 1")
        self.activeRun("pw ethport 97-192 2")
        self.activeRun("pw psn 1-192 mpls")
        self.activeRun("pw enable 1-192")
        
        self.standbyRun("driver role standby")
        self.standbyRun("device init")
        self.setStandbyXfiGroup(xfiGroup)
        self.standbyRun("sdh map aug1.3.1-4.16,1.1-2.16 3xvc3s")
        self.standbyRun("sdh map vc3.3.1.1-4.16.3,1.1.1-2.16.3 c3")
        self.standbyRun("pw create cep 97-192,1-96 basic")
        self.standbyRun("pw circuit bind 97-192,1-96 vc3.3.1.1-4.16.3,1.1.1-2.16.3")
        self.standbyRun("pw ethport 97-192 2")
        self.standbyRun("pw ethport 1-96 1")
        self.standbyRun("pw psn 97-192,1-96 mpls")
        self.standbyRun("pw enable 97-192,1-96")
        
        self.serializeAndCompare()

    def testVc4Cep(self):
        xfiGroup = self.anyXfiGroup()
        
        self.activeRun("driver role active")
        self.activeRun("device init")
        self.setActiveXfiGroup(xfiGroup)
        self.activeRun("sdh map aug1.1.1-4.16 vc4")
        self.activeRun("sdh map vc4.1.1-4.16 c4")
        self.activeRun("pw create cep 1-64 basic")
        self.activeRun("pw circuit bind 1-64 vc4.1.1-4.16")
        self.activeRun("pw ethport 1-32 1")
        self.activeRun("pw ethport 33-64 2")
        self.activeRun("pw psn 1-64 mpls")
        self.activeRun("pw enable 1-64")
        
        self.standbyRun("driver role standby")
        self.standbyRun("device init")
        self.setStandbyXfiGroup(xfiGroup)
        self.standbyRun("sdh map aug1.3.1-4.16,1.1-2.16 vc4")
        self.standbyRun("sdh map vc4.3.1-4.16,1.1-2.16 c4")
        self.standbyRun("pw create cep 33-64,1-32 basic")
        self.standbyRun("pw circuit bind 33-64,1-32 vc4.3.1-4.16,1.1-2.16")
        self.standbyRun("pw ethport 33-64 2")
        self.standbyRun("pw ethport 1-32 1")
        self.standbyRun("pw psn 33-64,1-32 mpls")
        self.standbyRun("pw enable 33-64,1-32")
        
        self.serializeAndCompare()
    
    def testVc4_4cCep(self):
        xfiGroup = self.anyXfiGroup()
        
        self.activeRun("driver role active")
        self.activeRun("device init")
        self.setActiveXfiGroup(xfiGroup)
        self.activeRun("sdh map aug4.1.1-4.4 vc4_4c")
        self.activeRun("sdh map vc4_4c.1.1-4.4 c4_4c")
        self.activeRun("pw create cep 1-16 basic")
        self.activeRun("pw circuit bind 1-16 vc4_4c.1.1-4.4")
        self.activeRun("pw ethport 1-8 1")
        self.activeRun("pw ethport 9-16 2")
        self.activeRun("pw psn 1-16 mpls")
        self.activeRun("pw enable 1-16")
        
        self.standbyRun("driver role standby")
        self.standbyRun("device init")
        self.setStandbyXfiGroup(xfiGroup)
        self.standbyRun("sdh map aug4.3.1-4.4,1.1-2.4 vc4_4c")
        self.standbyRun("sdh map vc4_4c.3.1-4.4,1.1-2.4 c4_4c")
        self.standbyRun("pw create cep 9-16,1-8 basic")
        self.standbyRun("pw circuit bind 9-16,1-8 vc4_4c.3.1-4.4,1.1-2.4")
        self.standbyRun("pw ethport 9-16 2")
        self.standbyRun("pw ethport 1-8 1")
        self.standbyRun("pw psn 9-16,1-8 mpls")
        self.standbyRun("pw enable 9-16,1-8")
        
        self.serializeAndCompare()
    
    def testVc4_16cCep(self):
        xfiGroup = self.anyXfiGroup()
        
        self.activeRun("driver role active")
        self.activeRun("device init")
        self.setActiveXfiGroup(xfiGroup)
        self.activeRun("sdh map aug16.1.1-4.1 vc4_16c")
        self.activeRun("sdh map vc4_16c.1.1-4.1 c4_16c")
        self.activeRun("pw create cep 1-4 basic")
        self.activeRun("pw circuit bind 1-4 vc4_16c.1.1-4.1")
        self.activeRun("pw ethport 1-2 1")
        self.activeRun("pw ethport 3-4 2")
        self.activeRun("pw psn 1-4 mpls")
        self.activeRun("pw enable 1-4")
        
        self.standbyRun("driver role standby")
        self.standbyRun("device init")
        self.setStandbyXfiGroup(xfiGroup)
        self.standbyRun("sdh map aug16.3.1-4.1,1.1-2.1 vc4_16c")
        self.standbyRun("sdh map vc4_16c.3.1-4.1,1.1-2.1 c4_16c")
        self.standbyRun("pw create cep 3-4,1-2 basic")
        self.standbyRun("pw circuit bind 3-4,1-2 vc4_16c.3.1-4.1,1.1-2.1")
        self.standbyRun("pw ethport 3-4 2")
        self.standbyRun("pw ethport 1-2 1")
        self.standbyRun("pw psn 3-4,1-2 mpls")
        self.standbyRun("pw enable 3-4,1-2")
        
        self.serializeAndCompare()
    
    def testDs1Cesop(self):
        xfiGroup = self.anyXfiGroup()
        
        # Configure the active
        self.activeRun("driver role active")
        self.activeRun("device init")
        self.setActiveXfiGroup(xfiGroup)
        self.activeRun("sdh map aug1.1.1-4.16 vc4")
        self.activeRun("sdh map vc4.1.1-4.16 3xtug3s")
        self.activeRun("sdh map tug3.1.1.1-4.16.3 7xtug2s")
        self.activeRun("sdh map tug2.1.1.1.1-4.16.3.7 tu11")
        self.activeRun("sdh map vc1x.1.1.1.1.1-4.16.3.7.4 de1")
        self.activeRun("pdh de1 framing 1.1.1.1.1-4.16.3.7.4 ds1_sf")
        self.activeRun("pdh de1 nxds0create 1.1.1.1.1-1.16.3.7.4 1-15")
        self.activeRun("pdh de1 nxds0create 2.1.1.1.1-2.16.3.7.4 1-15")
        self.activeRun("pdh de1 nxds0create 3.1.1.1.1-3.16.3.7.4 1-15")
        self.activeRun("pdh de1 nxds0create 4.1.1.1.1-4.16.3.7.4 1-15")
        self.activeRun("pw create cesop 1-5376 basic")
        self.activeRun("pw circuit bind 1-1344 nxds0.1.1.1.1.1-1.16.3.7.4.1-15")
        self.activeRun("pw circuit bind 1345-2688 nxds0.2.1.1.1.1-2.16.3.7.4.1-15")
        self.activeRun("pw circuit bind 2689-4032 nxds0.3.1.1.1.1-3.16.3.7.4.1-15")
        self.activeRun("pw circuit bind 4033-5376 nxds0.4.1.1.1.1-4.16.3.7.4.1-15")
        self.activeRun("pw ethport 1-2688    1")
        self.activeRun("pw ethport 2689-5376 2")
        self.activeRun("pw psn 1-5376 mpls")
        self.activeRun("pw enable 1-5376")
        
        # Configure the standby
        self.standbyRun("driver role standby")
        self.standbyRun("device init")
        self.setStandbyXfiGroup(xfiGroup)
        self.standbyRun("sdh map aug1.3.1-4.16,1.1-2.16 vc4")
        self.standbyRun("sdh map vc4.3.1-4.16,1.1-2.16 3xtug3s")
        self.standbyRun("sdh map tug3.3.1.1-4.16.3,1.1.1-2.16.3 7xtug2s")
        self.standbyRun("sdh map tug2.3.1.1.1-4.16.3.7,1.1.1.1-2.16.3.7 tu11", self.defaultTimeout())
        self.standbyRun("sdh map vc1x.3.1.1.1.1-4.16.3.7.4,1.1.1.1.1-2.16.3.7.4 de1")
        self.standbyRun("pdh de1 framing 3.1.1.1.1-4.16.3.7.4,1.1.1.1.1-2.16.3.7.4 ds1_sf")
        self.standbyRun("pdh de1 nxds0create 3.1.1.1.1-3.16.3.7.4 1-15")
        self.standbyRun("pdh de1 nxds0create 2.1.1.1.1-2.16.3.7.4 1-15")
        self.standbyRun("pdh de1 nxds0create 4.1.1.1.1-4.16.3.7.4 1-15")
        self.standbyRun("pdh de1 nxds0create 1.1.1.1.1-1.16.3.7.4 1-15")
        self.standbyRun("pw create cesop 2689-4032 basic")
        self.standbyRun("pw circuit bind 2689-4032 nxds0.3.1.1.1.1-3.16.3.7.4.1-15")
        self.standbyRun("pw create cesop 4033-5376 basic")
        self.standbyRun("pw circuit bind 4033-5376 nxds0.4.1.1.1.1-4.16.3.7.4.1-15")
        self.standbyRun("pw create cesop 1-1344 basic")
        self.standbyRun("pw circuit bind 1-1344 nxds0.1.1.1.1.1-1.16.3.7.4.1-15")
        self.standbyRun("pw create cesop 1345-2688 basic")
        self.standbyRun("pw circuit bind 1345-2688 nxds0.2.1.1.1.1-2.16.3.7.4.1-15")
        self.standbyRun("pw ethport 1513-2688,1-1512 1", self.defaultTimeout())
        self.standbyRun("pw ethport 4537-5376,2689-4537 2", self.defaultTimeout())
        self.standbyRun("pw psn 2689-5376,1-2688 mpls", self.defaultTimeout())
        self.standbyRun("pw enable 2689-5376,1-2688", self.defaultTimeout())
        
        self.serializeAndCompare()
    
    def testDs1CesopAcrDcr(self):
        xfiGroup = self.anyXfiGroup()
        
        # Configure the active
        self.activeRun("driver role active")
        self.activeRun("device init")
        self.setActiveXfiGroup(xfiGroup)
        self.activeRun("sdh map aug1.1.1-4.16 vc4")
        self.activeRun("sdh map vc4.1.1-4.16 3xtug3s")
        self.activeRun("sdh map tug3.1.1.1-4.16.3 7xtug2s")
        self.activeRun("sdh map tug2.1.1.1.1-4.16.3.7 tu11")
        self.activeRun("sdh map vc1x.1.1.1.1.1-4.16.3.7.4 de1")
        self.activeRun("pdh de1 framing 1.1.1.1.1-4.16.3.7.4 ds1_sf")
        self.activeRun("pdh de1 nxds0create 1.1.1.1.1-1.16.3.7.4 1-15")
        self.activeRun("pdh de1 nxds0create 1.1.1.1.1-1.16.3.7.4 16-23")
        self.activeRun("pdh de1 nxds0create 2.1.1.1.1-2.16.3.7.4 1-15")
        self.activeRun("pdh de1 nxds0create 2.1.1.1.1-2.16.3.7.4 16-23")
        self.activeRun("pw create cesop 1-2016 basic")
        self.activeRun("pw circuit bind 1-1344 nxds0.1.1.1.1.1-1.16.3.7.4.1-15")
        self.activeRun("pw circuit bind 1345-2016 nxds0.1.1.1.1.1-1.8.3.7.4.16-23")
        self.activeRun("pw create cesop 2689-4704 basic")
        self.activeRun("pw circuit bind 2689-4032 nxds0.2.1.1.1.1-2.16.3.7.4.1-15")
        self.activeRun("pw circuit bind 4033-4704 nxds0.2.1.1.1.1-2.8.3.7.4.16-23")
        self.activeRun("pw ethport 1-2016    1")
        self.activeRun("pw ethport 2689-4704 2")
        self.activeRun("pw psn 1-2016,2689-4704 mpls")
        self.activeRun("pw enable 1-2016,2689-4704")
        
        # Do not configure channels timing on standby driver because channel timing may be changed in interrupt context
        # that never happen on standby driver
        self.activeRun("pdh de1 timing 1.1.1.1.1-1.8.3.7.4 acr 1345-2016")
        self.activeRun("pdh de1 timing 1.9.1.1.1-1.16.3.7.4 acr 673-1344")
        
        self.activeRun("pdh de1 timing 2.1.1.1.1-2.8.3.7.4 dcr 4033-4704")
        self.activeRun("pdh de1 timing 2.9.1.1.1-2.16.3.7.4 dcr 3361-4032")
        
        # Configure the standby
        self.standbyRun("driver role standby")
        self.standbyRun("device init")
        self.setStandbyXfiGroup(xfiGroup)
        self.standbyRun("sdh map aug1.3.1-4.16,1.1-2.16 vc4")
        self.standbyRun("sdh map vc4.3.1-4.16,1.1-2.16 3xtug3s")
        self.standbyRun("sdh map tug3.3.1.1-4.16.3,1.1.1-2.16.3 7xtug2s")
        self.standbyRun("sdh map tug2.3.1.1.1-4.16.3.7,1.1.1.1-2.16.3.7 tu11", self.defaultTimeout())
        self.standbyRun("sdh map vc1x.3.1.1.1.1-4.16.3.7.4,1.1.1.1.1-2.16.3.7.4 de1")
        self.standbyRun("pdh de1 framing 3.1.1.1.1-4.16.3.7.4,1.1.1.1.1-2.16.3.7.4 ds1_sf")
        self.standbyRun("pdh de1 nxds0create 1.1.1.1.1-1.16.3.7.4 1-15")
        self.standbyRun("pdh de1 nxds0create 1.1.1.1.1-1.16.3.7.4 16-23")
        self.standbyRun("pdh de1 nxds0create 2.1.1.1.1-2.16.3.7.4 1-15")
        self.standbyRun("pdh de1 nxds0create 2.1.1.1.1-2.16.3.7.4 16-23")
        self.standbyRun("pw create cesop 2689-4032 basic")
        self.standbyRun("pw circuit bind 2689-4032 nxds0.2.1.1.1.1-2.16.3.7.4.1-15")
        self.standbyRun("pw create cesop 4033-4704 basic")
        self.standbyRun("pw circuit bind 4033-4704 nxds0.2.1.1.1.1-2.8.3.7.4.16-23")
        self.standbyRun("pw create cesop 1-1344 basic")
        self.standbyRun("pw circuit bind 1-1344 nxds0.1.1.1.1.1-1.16.3.7.4.1-15")
        self.standbyRun("pw create cesop 1345-2016 basic")
        self.standbyRun("pw circuit bind 1345-2016 nxds0.1.1.1.1.1-1.8.3.7.4.16-23")
        self.standbyRun("pw ethport 1513-2016,1-1512 1", self.defaultTimeout())
        self.standbyRun("pw ethport 4537-4704,2689-4537 2", self.defaultTimeout())
        self.standbyRun("pw psn 2689-4704,1-2016 mpls", self.defaultTimeout())
        self.standbyRun("pw enable 2689-4704,1-2016", self.defaultTimeout())
        self.serializeAndCompare()
        
    def notestPrbsBasic(self):
        # Configure the active
        self.activeRun("driver role active")
        self.activeRun("device init")
        self.activeRun("sdh map aug1.1.1 vc4")
        self.activeRun("sdh map vc4.1.1 3xtug3s")
        self.activeRun("sdh map tug3.1.1.1 7xtug2s")
        self.activeRun("sdh map tug2.1.1.1.1 tu11")
        self.activeRun("sdh map vc1x.1.1.1.1.1-1.1.1.1.3 de1")
        self.activeRun("sdh map aug1.1.2-1.16 vc4")
        self.activeRun("sdh map vc4.1.2-1.16 c4")
        self.activeRun("prbs engine create vc 1 vc4.1.2")
        self.activeRun("prbs engine create de1 2 1.1.1.1.1")
        self.activeRun("prbs engine create vc 3 vc4.1.3")
        self.activeRun("prbs engine create de1 4 1.1.1.1.2")
        
        # Configure the standby
        self.standbyRun("driver role standby")
        self.standbyRun("device init")
        self.standbyRun("sdh map aug1.1.1 vc4")
        self.standbyRun("sdh map vc4.1.1 3xtug3s")
        self.standbyRun("sdh map tug3.1.1.1 7xtug2s")
        self.standbyRun("sdh map tug2.1.1.1.1 tu11")
        self.standbyRun("sdh map vc1x.1.1.1.1.1-1.1.1.1.3 de1")
        self.standbyRun("sdh map aug1.1.2-1.16 vc4")
        self.standbyRun("sdh map vc4.1.2-1.16 c4")
        self.standbyRun("prbs engine create vc 3 vc4.1.3")
        self.standbyRun("prbs engine create vc 1 vc4.1.2")
        self.standbyRun("prbs engine create de1 4 1.1.1.1.2")
        self.standbyRun("prbs engine create de1 2 1.1.1.1.1")
        
        self.serializeAndCompare()
    
    @staticmethod
    def randomTimes():
        return 10
      
    def random(self):
        self.randomizer = Af60210051Randomizer()
        return self.randomizer.defaultRandom()
    
    def TestIssuRandomWithDeprovision(self, deprovision=False):
        def _activeRun(cli):
            self.activeRun(cli)
        def _standbyRun(cli):
            self.standbyRun(cli)
        
        def _applyDict(aDict, cliRunFunc):
            for cli in aDict["cli"]:
                cliRunFunc(cli)
            
            subChannels = aDict["subChannels"]
            random.shuffle(subChannels)
            for subChannel in subChannels:
                _applyDict(subChannel, cliRunFunc)
            
        for _ in range(0, self.randomTimes()):
            self.activeRun("driver role active")
            self.standbyRun("driver role standby")
        
            linesConfiguration = self.random()
            xfiGroup = self.anyXfiGroup()
            
            # Randomly apply this configuration on active
            self.activeRun("device init")
            self.setActiveXfiGroup(xfiGroup)
            random.shuffle(linesConfiguration)
            for line in linesConfiguration:
                _applyDict(line, _activeRun)
                
            # Randomly apply this configuration on standby
            self.standbyRun("device init")
            self.setStandbyXfiGroup(xfiGroup)
            random.shuffle(linesConfiguration)
            for line in linesConfiguration:
                _applyDict(line, _standbyRun)
            
            if deprovision:
                def _deprovision(deprovisions, cliRunFunc):
                    for pwCliList in deprovisions:
                        for cli in pwCliList:
                            cliRunFunc(cli)
                            
                # Have deprovision random sequence
                deprovisions = self.randomizer.randomDeprovisonPw()
                            
                # Apply on active
                random.shuffle(deprovisions)
                _deprovision(deprovisions, _activeRun)
                
                # Apply on standby
                random.shuffle(deprovisions)
                _deprovision(deprovisions, _standbyRun)
            
            self.serializeAndCompare()
    
    def testIssuRandom(self):
        self.TestIssuRandomWithDeprovision(False)
            
    def testIssuRandomWithDeprovision(self):
        self.TestIssuRandomWithDeprovision(True)
        
    def testRedundancyRandom(self):
        def _applyDict(aDict):
            for cli in aDict["cli"]:
                self.activeRun(cli)
                self.standbyRun(cli)
            
            subChannels = aDict["subChannels"]
            random.shuffle(subChannels)
            for subChannel in subChannels:
                _applyDict(subChannel)
        
        for _ in range(0, self.randomTimes()):
            xfiGroup = self.anyXfiGroup()
            self.activeRun("driver role active")
            self.standbyRun("driver role standby")
        
            self.activeRun("device init")
            self.standbyRun("device init")
            
            self.setActiveXfiGroup(xfiGroup)
            self.setStandbyXfiGroup(xfiGroup)
            
            linesConfiguration = self.random()
            random.shuffle(linesConfiguration)
            for line in linesConfiguration:
                _applyDict(line)
                
            self.serializeAndCompare()
  
    @staticmethod
    def randomConflictLabels():
        randomizer = Af60210051Randomizer()
        randomizer.makeDefault()
        randomizer.maxNumConflictLabelSet(5)
        randomizer.maxNumConflictGroupSet(2)
        return randomizer.random()
    
    def testRandomConflictLabel(self):
        def _applyDict(aDict):
            for cli in aDict["cli"]:
                self.activeRun(cli)
                self.standbyRun(cli)
            
            subChannels = aDict["subChannels"]
            random.shuffle(subChannels)
            for subChannel in subChannels:
                _applyDict(subChannel)
        
        for _ in range(0, self.randomTimes()):
            xfiGroup = self.anyXfiGroup()
            
            self.activeRun("driver role active")
            self.standbyRun("driver role standby")
        
            self.activeRun("device init")
            self.standbyRun("device init")
            
            self.setActiveXfiGroup(xfiGroup)
            self.setStandbyXfiGroup(xfiGroup)
            
            linesConfiguration = self.randomConflictLabels()
            random.shuffle(linesConfiguration)
            for line in linesConfiguration:
                _applyDict(line)
                
            self.serializeAndCompare()

if __name__ == "__main__":
    Af6021HaTest.run(Af60210051HaTest, sys.argv)

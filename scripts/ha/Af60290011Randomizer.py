import random

from ha.Af60210021Randomizer import Af60210021Randomizer
from ha.Af60210031Randomizer import Af60210031Randomizer
from ha.Af6ConfigurationRandomizer import Af6ConfigurationRandomizer

class Af60290011Util(object):
    @classmethod
    def makeDefault(cls, randomizer):
        randomizer.numLinesSet(24)
        randomizer.numPwsSet(672)
        randomizer.numEthPortsSet(1)
        randomizer.psnLabelStartIdSet(45000)

class Af60290011De3Randomizer(Af60210031Randomizer):
    def makeDefault(self):
        Af60290011Util.makeDefault(self)

class Af60290011De1Randomizer(Af60210021Randomizer):
    def makeDefault(self):
        Af60290011Util.makeDefault(self)

class Af60290011Randomizer(Af6ConfigurationRandomizer):
    INTERFACE_MODE_DE1 = "de1"
    INTERFACE_MODE_DE3 = "de3"

    @staticmethod
    def setDefaultRandom(randomizer):
        randomizer.numLinesSet(24)
        randomizer.numPwsSet(672)
        randomizer.numEthPortsSet(1)
        randomizer.psnLabelStartIdSet(45000)

    def createDe3Randomizer(self):
        randomizer = Af60290011De3Randomizer()
        self.setDefaultRandom(randomizer)
        return randomizer

    def createDe1Randomizer(self):
        randomizer = Af60290011De1Randomizer()
        self.setDefaultRandom(randomizer)
        return randomizer

    def __init__(self):
        super(Af60290011Randomizer, self).__init__()

        self.de3Randomizer = self.createDe3Randomizer()
        self.de1Randomizer = self.createDe1Randomizer()

    def activeRandomizer(self, interface):
        if interface == self.INTERFACE_MODE_DE1:
            return self.de1Randomizer

        if interface == self.INTERFACE_MODE_DE3:
            return self.de3Randomizer

        return None

    def random(self):
        commands = list()
        interfaceMode = random.choice([self.INTERFACE_MODE_DE1, self.INTERFACE_MODE_DE3])

        clis = list()
        clis.append("ciena pdh interface %s" % interfaceMode)
        confDict = self.dictCreate("", "", clis, list())
        commands.append(confDict)
        randomizer = self.activeRandomizer(interfaceMode)
        commands.extend(randomizer.random())

        return commands

    def makeDefault(self):
        return self.random()

if __name__ == '__main__':
    ran = Af60290011Randomizer()
    ran.show()

#! /usr/bin/python

import getopt
import sys
import os
import time
from functools import wraps

from python.AtConnection import AtConnection
from python.EpApp import EpApp
from python.arrive.AtAppManager.AtColor import AtColor

sys.path.append(os.path.dirname(__file__) + "/../python")
sys.path.append(os.path.dirname(__file__) + "/../python/arrive/AtAppManager")
sys.path.append(os.path.dirname(__file__))

PLATFORM_SIMULATE = "simulate"
PLATFORM_KINTEX   = "kintex"
PLATFORM_CISCO    = "cisco"
FPGA_PATH = "" 
PROF_DATA = {}

failResult = {}

# Network connection
halPort = None
cliPort = None
def nextNetworkPorts():
    global halPort
    global cliPort
    
    if halPort is None or cliPort is None:
        halPort = 2222
        cliPort = 3333
        return halPort, cliPort
    
    halPort = halPort + 1
    cliPort = cliPort + 1
    return halPort, cliPort

def getHalPort():
    global halPort
    return halPort

def getCliPort():
    global cliPort
    return cliPort

# Options
epappCommand = None
verbose = False
platform = PLATFORM_SIMULATE

_simulateVersion = None
_defaultSimulationVersion = "\"18/12/31 F.F.FFFF\""

def profile(fn):
    @wraps(fn)
    def with_profiling(*args, **kwargs):
        start_time = time.time()

        ret = fn(*args, **kwargs)

        elapsed_time = time.time() - start_time

        if fn.__name__ not in PROF_DATA:
            PROF_DATA[fn.__name__] = [0, []]
        PROF_DATA[fn.__name__][0] += 1
        PROF_DATA[fn.__name__][1].append(elapsed_time)

        return ret

    return with_profiling

def print_prof_data():
    for fname, data in PROF_DATA.items():
        max_time = max(data[1])
        avg_time = sum(data[1]) / len(data[1])
        print "Function %s called %d times. " % (fname, data[0]),
        print 'Execution time max: %.3f(s), average: %.3f(s)' % (max_time, avg_time)

def clear_prof_data():
    global PROF_DATA
    PROF_DATA = {}
    
def simulateVersion():
    if _simulateVersion is None:
        return _defaultSimulationVersion
    return _simulateVersion

def setSimulateVersion(version):
    global _simulateVersion
    _simulateVersion = version

def activeAppCommand(product):
    if platform == PLATFORM_SIMULATE:
        return epappCommand + " -s " + product + (" -L %d -w " % getHalPort()) + simulateVersion()
    if platform == PLATFORM_KINTEX:
        return epappCommand + " -p ep5"
    
    return None


def standbyAppCommand(product):
    if platform == PLATFORM_SIMULATE:
        return epappCommand + " -P " + "F" + product[1:] + (" -r 127.0.0.1:%d -u %d  -w " % (getHalPort(), getCliPort())) + simulateVersion()
    if platform == PLATFORM_KINTEX:
        return epappCommand + " -p ep5"
    return None


def createConnection(boardIp):
    return AtConnection.telnetConnection(boardIp)


def createActiveApp(product, targetIp=None):
    if platform == PLATFORM_SIMULATE:
        return EpApp(activeAppCommand(product))
    
    if platform == PLATFORM_KINTEX:
        return EpApp(activeAppCommand(product), createConnection(targetIp))


def createStandbyApp(product, targetIp=None):
    if platform == PLATFORM_SIMULATE:
        return EpApp(standbyAppCommand(product))
    if platform == PLATFORM_KINTEX:
        return EpApp(standbyAppCommand(product), createConnection(targetIp))


def loadFpga(boardIp, fpgaPath):
    if platform == PLATFORM_SIMULATE:
        return
    
    print AtColor.GREEN + "Loading FPGA: " + AtColor.CLEAR + fpgaPath
    connection = createConnection(boardIp)
    connection.connect()
    connection.execute("fpgaload " + fpgaPath)
    connection.disconnect()


def scriptPath():
    return os.path.dirname(os.path.realpath(sys.argv[0]))


def testSuccess(result):
    lines = result.split("\r\n")
    resultLine = lines[len(lines) - 4]
    if resultLine == "OK":
        return True
    return False


def runTestScript(scriptFileName, product, boardIp, verbosed = False):
    activeApp = createActiveApp(product, boardIp)
    standbyApp = createStandbyApp(product, boardIp)
    
    activeApp.start()
    standbyApp.start()
    
    command = "python " + scriptPath() + "/" + scriptFileName + (" --remote=%d --simulate" % getCliPort())
    if verbosed:
        command = command + " --verbose"
        
    cliResult = activeApp.runCli(command, 30 * 60)
    
    if testSuccess(cliResult.raw()):
        print "Run " + scriptFileName + ": " + AtColor.GREEN + "OK" + AtColor.CLEAR
    else:
        print "Run " + scriptFileName + ": " + AtColor.RED + "FAIL" + AtColor.CLEAR
        failResult[scriptFileName] = cliResult.raw()
    
    standbyApp.exit()
    activeApp.exit()


@profile
def testProduct60210011(boardIp=None, verbose = False):
    product = "60210011"
    runTestScript("Af60210011.py", product, None, verbose)


@profile 
def testProduct60210051(boardIp=None, verbose = False):
    product = "60210051"
    runTestScript("Af60210051.py", product, None, verbose)
    

@profile    
def testProduct60210021(boardIp=None, verbose = False):
    product = "60210021"
    fpgaPath = "/home/ATVN_Eng/af6xxx/af6cci0021/fpga/ep6cci0021top_15092832_3.2.0.0.bit"
    loadFpga(boardIp, fpgaPath)
    runTestScript("Af60210021.py", product, boardIp, verbose)


@profile 
def testProduct60210031(boardIp=None, verbose = False):
    product = "60210031"
    fpgaPath = "/home/ATVN_Eng/af6xxx/af6cci0031/fpga/ep6cci0031top_15092832_3.2.0.0.bit"
    loadFpga(boardIp, fpgaPath)
    runTestScript("Af60210031.py", product, boardIp, verbose)


@profile 
def testProduct60210012(boardIp=None, verbose = False):
    product = "60210012"
    fpgaPath = None
    loadFpga(boardIp, fpgaPath)
    runTestScript("Af60210012.py", product, boardIp, verbose)


@profile 
def testProduct60210061(boardIp=None, verbose = False):
    product = "60210061"
    fpgaPath = None
    loadFpga(boardIp, fpgaPath)
    runTestScript("Af60210061.py", product, boardIp, verbose)

    
def boardIpAddress():
    KINTEX_BOARD = "172.33.44.4"
    return KINTEX_BOARD


def printUsage(appName):
    print ("Usage: %s --epapp=<epappCommand> [--platform=simulate|kintex|cisco] [--verbose]" % appName)


def parseOptions(argv):
    global verbose 
    global epappCommand 
    global platform
    
    try:
        opts, args = getopt.getopt(argv[1:], "",["verbose", "epapp=", "platform="])
    except getopt.GetoptError:
        printUsage(argv[0])
        sys.exit(2)
        
    for opt, arg in opts:
        if opt == "--verbose":
            verbose = True
        if opt == "--epapp":
            epappCommand = arg
        if opt == "--platform":
            platform = arg
    
    if epappCommand is None:
        print "Missing epapp path"
        printUsage(argv[0])
        sys.exit(1)
    

def printFailDetail():
    if len(failResult) == 0:
        return
    
    for key, value in failResult.iteritems():
        print AtColor.RED + "==================================" + AtColor.CLEAR
        print AtColor.RED + "= "  + key + AtColor.CLEAR
        print AtColor.RED + "==================================" + AtColor.CLEAR
        print value


def main(argv):
    parseOptions(argv)
    
    def testRun(aFunction):
        nextNetworkPorts()
        aFunction(boardIp = boardIpAddress(), verbose = verbose)
    
    testRun(testProduct60210051)
    testRun(testProduct60210021)
    testRun(testProduct60210031)
    testRun(testProduct60210012)
    testRun(testProduct60210061)

    printFailDetail()
    
if __name__ == "__main__":
   main(sys.argv)

from Af60210051Randomizer import Af60210051Randomizer

class Af60210021Randomizer(Af60210051Randomizer):
    def random(self):
        aList = []
        self.pwIdList = []
        self.labelIdList = []

        for lineId in range(1, self.dbNumLines + 1):
            lineCliList = []
            subChannelsList = []
            
            lineDict = self.dictCreate('line', lineId, lineCliList, subChannelsList)
            aList.append(lineDict)
            self.pwConfigureClisAppend(lineCliList, " de1." + str(lineId))
                
        return aList
    
    def makeDefault(self):
        self.numLinesSet(48)
        self.numPwsSet(192)
        self.numEthPortsSet(1)
        self.psnLabelStartIdSet(45000)


def main():
    pass

if __name__ == "__main__":
    main()
    

class Af6ConfigurationRandomizer(object):
    def random(self):
        return list()
    
    def makeDefault(self):
        pass
    
    def defaultRandom(self):
        self.makeDefault()
        return self.random()
    
    @staticmethod
    def dictCreate(atype, _id, cli, subChannels):
        newDict = dict()
        newDict['type'] = atype
        newDict['id'] = _id
        newDict['cli'] = cli
        newDict['subChannels'] = subChannels
        return newDict

    @staticmethod
    def traverse(randomDicts, handler):
        def _applyDict(aDict):
            for cli in aDict["cli"]:
                handler(cli)

            subChannels = aDict["subChannels"]
            for subChannel in subChannels:
                _applyDict(subChannel)

        for configureDict in randomDicts:
            _applyDict(configureDict)

    def show(self):
        def handler(cli):
            print cli

        ranDict = self.defaultRandom()
        self.traverse(ranDict, handler)

    @classmethod
    def createRandomizerForProductCode(cls, productCode):
        def _create(aProductCode):
            if type(aProductCode) == str:
                aProductCode = int(aProductCode, 16)
            
            if aProductCode == 0x60210051:
                from Af60210051Randomizer import Af60210051Randomizer
                return Af60210051Randomizer()
            
            if aProductCode == 0x60210021:
                from Af60210021Randomizer import Af60210021Randomizer
                return Af60210021Randomizer()
            
            if aProductCode == 0x60210031:
                from Af60210031Randomizer import Af60210031Randomizer
                return Af60210031Randomizer()
            
            if aProductCode == 0x60210061:
                from Af60210012Randomizer import Af60210012Randomizer
                return Af60210012Randomizer()
            
            if aProductCode in [0x60290021, 0x60290022]:
                from test.anna.randomizer import AnnaRandomizer
                return AnnaRandomizer.randomizer(productCode)
            
        randomizer = _create(productCode)
        randomizer.makeDefault()
        
        return randomizer
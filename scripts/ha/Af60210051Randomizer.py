import random
from Af6ConfigurationRandomizer import Af6ConfigurationRandomizer

class Af60210051Randomizer(Af6ConfigurationRandomizer):
    dbNumLines = 0
    dbNumPws = 0
    dbNumPorts = 0
    dbStartLabel = 0
    dbStopLabel = 0xFFFFF
    pwIdList = []
    labelIdList = []
    cMplsType = 2
    cInvalidLabel = 0xffffffff
        
    def __init__(self):
        self.maxNumConflictLabel = 0
        self.numConflictLabel = 0
        self.currentHash = 0
        self.maxNumConflictGroup = 1
        self.numConflictGroup = 0
        self.fullConflictHashList = []
    
    def makeDefault(self):
        self.numLinesSet(4)
        self.numPwsSet(1344 * 4)
        self.numEthPortsSet(2)
        self.psnLabelStartIdSet(45000)
        return self.random()
    
    def numLinesSet(self, numLines):
        self.dbNumLines = numLines
        
    def numPwsSet(self, numPws):
        self.dbNumPws = numPws
        
    def numEthPortsSet(self, numPorts):
        self.dbNumPorts = numPorts
        
    def psnLabelStartIdSet(self, value):    
        self.dbStartLabel = value
        
    def psnLabelStopIdSet(self, value):    
        self.dbStopLabel = value
                
    def maxNumConflictLabelSet(self, maxNumConflictLabel):
        self.maxNumConflictLabel = maxNumConflictLabel
    
    def maxNumConflictGroupSet(self, maxNumConflictGroup):
        self.maxNumConflictGroup = maxNumConflictGroup
            
    @staticmethod
    def __generateRandomUniqueId(startId, endId, idList):
        while 1:
            newValue = random.randint(startId, endId)
            if newValue not in idList:
                return newValue
    
    def randomPwId(self):
        pwId = self.__generateRandomUniqueId(1, self.dbNumPws, self.pwIdList)
        self.pwIdList.append(pwId)
        return pwId
    
    def randomLabelId(self):
        labelId = self.__generateRandomUniqueId(self.dbStartLabel, self.dbStopLabel, self.labelIdList)
        self.labelIdList.append(labelId)
        return labelId
    
    def __ethPortFromChannelString(self, channelString):
        portId = 1
        if self.dbNumPorts > 1:
            channelStringTokens = channelString.split(".")
            lineId = channelStringTokens[1][0]
            if int(lineId) < 3:
                portId = 1
            else:
                portId = 2
        return portId
    
    def pwConfigureClisAppend(self, cliList, channelString):
        pwId = self.randomPwId()
        psnTypeList = ['mpls']
        portId = self.__ethPortFromChannelString(channelString)
        labelId = self.randomConflictLabels(pwId, portId)
        shuffleList = []
        psnList = []
        flatShuffleList = []
            
        if "de" in channelString:
            pwType = "satop"
            cliList.append("pw create satop " + str(pwId))
        elif "nxds0" in channelString:
            pwType = "cesop"
            cliList.append("pw create cesop " + str(pwId) + " basic")
        else:
            pwType = "cep"
            cliList.append("pw create cep " + str(pwId) + " basic")
        
        psnType = random.choice(psnTypeList)
        shuffleList.append("pw circuit bind " + str(pwId) + channelString)

        psnList.append("pw psn " + str(pwId) + " " + psnType + " " + str(labelId))        
        if psnType == 'mpls':
            psnList.append("pw mpls innerlabel " + str(pwId) + " " + str(labelId) + ".0.0")
            
        if psnType == 'mef':
            psnList.append("pw mef ecid transmit " + str(pwId) + " " + str(labelId))
            
        shuffleList.append(psnList)
        psnListLength = len(psnList)

        shuffleList.append("pw ethport " + str(pwId) + " " + str(portId))
        shuffleList.append("pw enable " + str(pwId))
        random.shuffle(shuffleList)
        
        for i in shuffleList:
            if len(i) == psnListLength:
                flatShuffleList.extend(i)
            else:
                flatShuffleList.append(i)
                
        cliList.extend(flatShuffleList)
        
    @classmethod
    def listCliRun(cls, aList, localApp):
        for i in aList:
            localApp.runCli(i)
            
    @classmethod
    def dictRun(cls, aDict, localApp):
        Af60210051Randomizer.listCliRun(aDict["cli"], localApp)
        for i in aDict["subChannels"]:
            Af60210051Randomizer.dictRun(i, localApp)
    
    @classmethod
    def listDictRun(cls, alist, localApp):
        for i in alist:
            Af60210051Randomizer.dictRun(i, localApp)
            
    def listUsedPw(self):
        return self.pwIdList
            
    def de3Random(self, de3Id, de3FlatIdString, parentSubChannelList):
        de3CliList = []
        de3SubChannelsList = []
        de3FrameModeList = ['ds3_unframed', 'ds3_cbit_unchannelize', 'ds3_cbit_28ds1',
                            'ds3_cbit_21e1', 'ds3_m13_28ds1', 'ds3_m13_21e1', 'e3_unframed',
                            'e3_g832', 'e3_g751', 'e3_g751_16e1s']
        de3Dict = self.dictCreate('de3', de3Id, de3CliList, de3SubChannelsList)
        parentSubChannelList.append(de3Dict)
        de3RandomFrameType = random.choice(de3FrameModeList)
        de3CliList.append("pdh de3 framing " + de3FlatIdString + " " + de3RandomFrameType)
        numDe1InDe2 = 0
        numDe2 = 0
        
        if (de3RandomFrameType == 'ds3_unframed' or de3RandomFrameType == 'e3_unframed' or
            de3RandomFrameType == 'e3_g832'      or de3RandomFrameType == 'e3_g751'     or
            de3RandomFrameType == 'ds3_cbit_unchannelize'):
            self.pwConfigureClisAppend(de3CliList, " de3." + de3FlatIdString)
            
        if de3RandomFrameType == 'ds3_cbit_28ds1' or de3RandomFrameType == 'ds3_m13_28ds1':
            numDe2 = 7
            numDe1InDe2 = 4
        if de3RandomFrameType == 'ds3_cbit_21e1' or de3RandomFrameType == 'ds3_m13_21e1':
            numDe2 = 7
            numDe1InDe2 = 3
        if de3RandomFrameType == 'e3_g751_16e1s':
            numDe2 = 4
            numDe1InDe2 = 4
            
        for de2Id in range(1, numDe2 + 1):
            for de1Id in range(1, numDe1InDe2 + 1):
                if de3RandomFrameType == 'ds3_cbit_28ds1' or de3RandomFrameType == 'ds3_m13_28ds1':
                    self.ds1Random(de3CliList, de3FlatIdString + "." + str(de2Id) + "." + str(de1Id), de3SubChannelsList)
                else:
                    self.pwConfigureClisAppend(de3CliList, " de1." + de3FlatIdString + "." + str(de2Id) + "." + str(de1Id))
                
    def ds1Random(self, de1Id, de1IdString, parentSubChannelList):
        cliList = []
        subChannelsList = []
        frameModes = ['ds1_unframed', 'ds1_sf', 'ds1_esf']
        de1Dict = self.dictCreate('de1', de1Id, cliList, subChannelsList)
        parentSubChannelList.append(de1Dict)
        frameType = random.choice(frameModes)
        cliList.append("pdh de1 framing %s %s" % (de1IdString, frameType))
        nxDs0List = []
        
        if frameType == 'ds1_unframed':
            self.pwConfigureClisAppend(cliList, " de1." + de1IdString)
    
        if frameType == 'ds1_sf':
            nxDs0List.append("1-23")
        if frameType == 'ds1_esf':
            nxDs0List.append("1-10")
            nxDs0List.append("12-20")
            
        for nxDs0 in nxDs0List:
            cliList.append("pdh de1 nxds0create " + de1IdString + " " + nxDs0)
            self.pwConfigureClisAppend(cliList, " nxds0." + de1IdString + "." + nxDs0)
    
    def vc3Random(self, vc3Id, vc3CliIdString, parentSubChannelList):
        cliList = []
        subChannelsList = []
        vc3Dict = self.dictCreate('vc3', vc3Id, cliList, subChannelsList)
        parentSubChannelList.append(vc3Dict)
            
        mapType = random.choice(self.vc3MappingList())
        cliList.append("sdh map vc3.%s %s" % (vc3CliIdString, mapType))

        if mapType == 'c3':
            self.pwConfigureClisAppend(cliList, " vc3." + vc3CliIdString)
            
        if mapType == '7xtug2s':
            for tug2Id in range(1, 8):
                self.tug2Random(vc3CliIdString, tug2Id, subChannelsList)
                        
        if mapType == 'de3':
            self.de3Random(vc3Id, vc3CliIdString, subChannelsList)
    
    def makeLineRateClis(self, lineId, lineRate):
        scripts = list()

        scripts.append("sdh line rate " + str(lineId) + " " + lineRate)
        scripts.append("sdh line mode " + str(lineId) + " " + random.choice(["sonet", "sdh"]))

        return scripts
    
    def lineRateRandom(self, lineId, subChannelsList, lineRate):
        lineCliList = []
        lineDict = self.dictCreate('line', lineId, lineCliList, subChannelsList)
        
        for cli in self.makeLineRateClis(lineId, lineRate):
            lineCliList.append(cli)
        return lineDict
                
    def lineRateList(self, lineIdList, lineRateList):
        for i in range(0, self.dbNumLines):
            lineIdList.append(i + 1)
            
        for i in range(0, self.dbNumLines):
            lineRateList.append("stm16")
        
    def tu1xRandom(self, tug2IdString, tu1xId, parentSubChannelsList):
        cliIdString = tug2IdString + "." + str(tu1xId)
        cliList = []
        subChannelsList = []
        
        tu1xDict = self.dictCreate('vc11', tu1xId, cliList, subChannelsList)
        parentSubChannelsList.append(tu1xDict)
        vc1xRandomMapping = random.choice(self.vc1xMappingList())
        cliList.append("sdh map vc1x." + cliIdString + " " + vc1xRandomMapping)
        
        if vc1xRandomMapping == 'c1x':
            self.pwConfigureClisAppend(cliList, " vc1x." + cliIdString)
            
        if vc1xRandomMapping == 'de1':
            self.ds1Random(tu1xId, cliIdString, subChannelsList)
        
    def tug2Random(self, vc3Tug3IdString, tug2Id, parentSubChannelsList):
        cliIdString = vc3Tug3IdString + "." + str(tug2Id)
        cliList = []
        subChannelsList = []
        
        tug2Dict = self.dictCreate('tug-2', tug2Id, cliList, subChannelsList)
        parentSubChannelsList.append(tug2Dict)
        mapType = "tu11" # TODO: how about TU12?
        cliList.append("sdh map tug2.%s %s" % (cliIdString, mapType))
        
        for tu1xId in range(1, 5):
            self.tu1xRandom(cliIdString, tu1xId, subChannelsList)
        
    def tug3Random(self, aug1IdString, tug3Id, subChannelsList):
        tug3IdString = aug1IdString + "." + str(tug3Id)
        tug3CliList = []
        tug3SubChannelsList = []
        
        tug3Dict = self.dictCreate('tug-3', tug3Id, tug3CliList, tug3SubChannelsList)
        subChannelsList.append(tug3Dict)
        tug3RandomMapping = random.choice(self.tug3MappingList())
        tug3CliList.append("sdh map tug3." + tug3IdString + " " + tug3RandomMapping)

        if tug3RandomMapping == 'vc3':
            tug3CliList.append("sdh map vc3." + tug3IdString + " " + "c3")
            self.pwConfigureClisAppend(tug3CliList, " vc3." + tug3IdString)
             
        if tug3RandomMapping == '7xtug2s':
            for tug2Id in range(1, 8):
                self.tug2Random(tug3IdString, tug2Id, tug3SubChannelsList)
    
    def vc4Random(self, augCliIdString, aug1Id, parentSubChannelsList):
        cliList = []
        subChannelsList = []
        
        vc4Dict = self.dictCreate('vc4', aug1Id, cliList, subChannelsList)
        parentSubChannelsList.append(vc4Dict)
        vc4RandomMapping = random.choice(self.vc4MappingList())
        cliList.append("sdh map vc4." + augCliIdString + " " + vc4RandomMapping)
        
        if vc4RandomMapping == 'c4':
            self.pwConfigureClisAppend(cliList, " vc4." + augCliIdString)
            
        if vc4RandomMapping == '3xtug3s':
            for tug3Id in range(1, 4):
                self.tug3Random(augCliIdString, tug3Id, subChannelsList)
    
    def aug1Random(self, lineId, aug16Id, aug4Id, aug1Id, parentSubChannelsList):
        cliList = []
        subChannelsList = []
        
        augDict = self.dictCreate('aug-1', aug1Id, cliList, subChannelsList)
        aug1FlatId = ((aug16Id - 1) * 16) + (4 * (aug4Id - 1)) + aug1Id
        augIdString = "%d.%d" % (lineId, aug1FlatId)
        
        parentSubChannelsList.append(augDict)
        mapType = random.choice(self.aug1MappingList())
        cliList.append("sdh map aug1.%s %s" % (augIdString, mapType))
        
        if mapType == 'vc4':
            self.vc4Random(augIdString, aug1Id, subChannelsList)
            
        if mapType == '3xvc3s':
            for vc3Id in range(1,4):
                vc3CliIdString = augIdString + "." + str(vc3Id)
                self.vc3Random(vc3Id, vc3CliIdString, subChannelsList)
    
    def aug4Random(self, lineId, aug16Id, aug4Id, parentSubChannelsList):
        cliList = []
        subChannelsList = []
        
        augDict = self.dictCreate('aug-4', aug4Id, cliList, subChannelsList)
        parentSubChannelsList.append(augDict)
        mapType = random.choice(self.aug4MappingList())
        
        aug4FlatId = ((aug16Id - 1) * 4) + aug4Id
        cliId = "%d.%d" % (lineId, aug4FlatId)
        cliList.append("sdh map aug4.%s %s" % (cliId, mapType))

        if mapType == 'vc4_4c':
            self.pwConfigureClisAppend(cliList, " vc4_4c.%s" % cliId)

        if mapType == '4xaug1s':
            for aug1Id in range(1, 5):
                self.aug1Random(lineId, aug16Id, aug4Id, aug1Id, subChannelsList)
    
    def aug16Random(self, lineId, aug16Id, parentSubChannelsList):
        cliList = []
        subChannelsList = []
        
        augDict = self.dictCreate('aug-16', aug16Id, cliList, subChannelsList)
        parentSubChannelsList.append(augDict)
        mapType = random.choice(self.aug16MappingList())
        augCliId = "%d.%d" % (lineId, aug16Id)
        cliList.append("sdh map aug16.%s %s" % (augCliId, mapType))

        if mapType == 'vc4_16c':
            self.pwConfigureClisAppend(cliList, " vc4_16c.%s" % augCliId)
            
        if mapType == '4xaug4s': 
            for aug4Id in range(1, 5):
                self.aug4Random(lineId, aug16Id, aug4Id, subChannelsList)
    
    def aug64MapTypeIsSupported(self, mapType):
        return True
    
    def aug64Random(self, lineId, aug64Id, parentSubChannelsList):
        cliList = []
        subChannelsList = []
        
        augDict = self.dictCreate('aug-64', aug64Id, cliList, subChannelsList)
        parentSubChannelsList.append(augDict)
        
        mapType = None
        mapTypes = self.aug64MappingList()
        while len(mapTypes) > 0:
            randomMapType = random.choice(mapTypes)
            mapTypes.remove(randomMapType)
            if self.aug64MapTypeIsSupported(randomMapType):
                mapType = randomMapType
                break
        
        assert(mapType is not None)
        
        augCliId = "%d.%d" % (lineId, aug64Id)
        cliList.append("sdh map aug64.%s %s" % (augCliId, mapType))

        if mapType == 'vc4_64c':
            self.pwConfigureClisAppend(cliList, " vc4_64c.%s" % augCliId)
            
        if mapType == '4xaug16s': 
            for aug16Id in range(1, 5):
                self.aug16Random(lineId, aug16Id, subChannelsList)
    
    @staticmethod
    def aug64MappingList():
        return ['vc4_64c', '4xaug16s']
    
    @staticmethod
    def aug16MappingList():
        return ['vc4_16c', '4xaug4s']
    
    @staticmethod
    def aug4MappingList():
        return ['vc4_4c', '4xaug1s']
    
    @staticmethod
    def aug1MappingList():
        return ['vc4', '3xvc3s']
        
    @staticmethod
    def vc4MappingList():
        return ['c4', '3xtug3s']
    
    @staticmethod
    def tug3MappingList():
        return ['vc3', '7xtug2s']
    
    @staticmethod
    def vc1xMappingList():
        return ['c1x', 'de1']
    
    @staticmethod
    def vc3MappingList():
        return ['c3', '7xtug2s', 'de3'] 
    
    def random(self):
        cfgList = []
        self.pwIdList = []
        self.labelIdList = []
        lineIdList = []
        lineRateList = []
        lineIndex = 0
        
        self.lineRateList(lineIdList, lineRateList)

        for lineId in lineIdList:
            subChannelsList = []
            rate = lineRateList[lineIndex]
            cfgList.append(self.lineRateRandom(lineId, subChannelsList, rate))
            
            for augId in [1]:
                if rate == "stm64":
                    self.aug64Random(lineId, augId, subChannelsList)
                    
                if rate == "stm16":
                    self.aug16Random(lineId, augId, subChannelsList)
                
                if rate == "stm4":
                    self.aug4Random(lineId, 1, augId, subChannelsList)
                    
                if rate == "stm1":
                    self.aug1Random(lineId, 1, 1, augId, subChannelsList)
                
            lineIndex = lineIndex + 1
                                        
        return cfgList
    
    def hwLabelBuild(self, label, psnType, portId):
        return (portId - 1) | psnType << 2 | label << 4
    
    def hashHwLabel(self, hwLabel):
        swHbcePatern23_14 = (hwLabel >> 14) & 0x3ff
        swHbcePatern13_0  = hwLabel & 0x3fff
        
        return swHbcePatern13_0 ^ swHbcePatern23_14
    
    def hash(self, label, psnType, portId):
        hwLabel = self.hwLabelBuild(label, psnType, portId)
        return self.hashHwLabel(hwLabel)
        
    # noinspection PyUnusedLocal
    def __randomFirstLabel(self, startId, endId, idList, pwId, ethPortId):
        while 1:
            newValue = random.randint(startId, endId)
            hashValue = self.hash(newValue, self.cMplsType, ethPortId)
            if newValue in idList or hashValue in self.fullConflictHashList:
                continue
            
            # Don't need to generate conflict label
            if  self.maxNumConflictLabel < 2 or self.numConflictGroup >= self.maxNumConflictGroup:
                return newValue
            
            self.currentHash = hashValue
            self.numConflictLabel = 1
            return newValue
    
    def randomConflictLabelGenerate(self, startId, endId, idList, ethPortId):
        """
        Idea to generate a label which will conflict with a current label:

        - Pattern before going through hash function in this product is concated from label, type and ethernet port ID:
            [label 23:4][psnType 3:2][portId 1:0]
        - Hash function apply on pattern is [Patern 23:14] XOR [Pattern 13:0]
        - Fixed values: PSN type for MPLS is 2, ethernet port, and a desired hash value

                         [Patern 23:14]                     [23 .. 18][17 .. 14]
                   XOR                         XOR
                         [Pattern 13:0]           [13 .. 10][9  ..  4][3  ..  0]
                   -------------------    =  -----------------------------------
                     desired hash value                        desired hash value

        Bits 3:0 is [psnType 3:2][portId 1:0]
        Bits 9:4 is random picked (*)
        Bits 13:10 = 0 ^ desired hash value[13:10]
        Bits 17:14 is bits 3:0 ^ desired hash value[3:0]
        Bits 23:8 is bits 9:4 ^ desired hash value[9:4]

        OR all above bits, we have a new pattern which have same hash value with desired hash value
        And new label is Bits 23:4 (20 bits) of this new pattern

        Conclusion (*): only Bits 9:4 need to be scanned to find a label which will conflict with a existing label with a desired hash value
        """
        
        hwLabel  = self.hwLabelBuild(0, self.cMplsType, ethPortId)
        bit17_14 = (self.currentHash ^ hwLabel) & 0xf
        bit13_10 = ((self.currentHash >> 10) ^ 0x0) & 0xf
        
        for bit9_4 in range(0x3f):
            bit23_18 = ((self.currentHash >> 4) ^ bit9_4) & 0x3f
            fullHwLabel = hwLabel | (bit9_4 << 4) | (bit13_10 << 10) | (bit17_14 << 14) |  (bit23_18 << 18)
            newValue = (fullHwLabel >> 4) & 0xfffff
            
            if newValue not in idList and startId <= newValue <= endId:
                return newValue
            
        return self.cInvalidLabel
    
    def __generateRandomUniqueConflictLabel(self, startId, endId, idList, pwId, ethPortId):
        if self.numConflictLabel == 0:
            return self.__randomFirstLabel(startId, endId, idList, pwId, ethPortId)
        
        newValue = self.randomConflictLabelGenerate(startId, endId, idList, ethPortId)
        
        # Could not find out any label, restart
        if newValue == self.cInvalidLabel:
            return self.__randomFirstLabel(startId, endId, idList, pwId, ethPortId)    
           
        self.numConflictLabel = self.numConflictLabel + 1
        
        # Enough labels in group, restart for next group
        if self.numConflictLabel >= self.maxNumConflictLabel:
            self.numConflictLabel = 0
            self.numConflictGroup = self.numConflictGroup + 1
            self.fullConflictHashList.append(self.currentHash)
            
        return newValue  
            
    def randomConflictLabels(self, pwId, ethPortId):
        labelId = self.__generateRandomUniqueConflictLabel(self.dbStartLabel, self.dbStopLabel, self.labelIdList, pwId, ethPortId)
        self.labelIdList.append(labelId)
        return labelId
    
    def randomDeprovisonPw(self):
        pwDeprovisionList = []
        clonePwIdList = list(self.pwIdList)
        numPwNeedBeRemoved = random.randint(1, len(self.pwIdList))
        deprovisionModeList = ["disable", "delete"]
        deactivateTriggerModeList = ["circuit", "ethport"]
        
        random.shuffle(clonePwIdList)
        
        for _ in range(0, numPwNeedBeRemoved):
            cliListPerPw = []
            deprovisionMode = random.choice(deprovisionModeList)
            
            pwId = clonePwIdList.pop()
            if deprovisionMode == "disable":
                cliListPerPw.append("pw disable " + str(pwId))
            
            if deprovisionMode == "delete":
                deactivateTriggerMode = random.choice(deactivateTriggerModeList)
                if deactivateTriggerMode == "circuit":
                    cliListPerPw.append("pw circuit unbind " + str(pwId))
                    cliListPerPw.append("pw ethport " + str(pwId) + " none")
                    
                if deactivateTriggerMode == "ethport":
                    cliListPerPw.append("pw ethport " + str(pwId) + " none")
                    cliListPerPw.append("pw circuit unbind " + str(pwId))
                    
                cliListPerPw.append("pw delete " + str(pwId))
                
            pwDeprovisionList.append(cliListPerPw)
                
        return pwDeprovisionList

import Af6021HaTest 
from Af60210021Randomizer import Af60210021Randomizer
import random
import sys

class Af60210021HaTest(Af6021HaTest.Af6021HaTest):
    @staticmethod
    def numDe1():
        return 48

    def testHspwBasic(self):
        self.activeRun("driver role active")
        self.activeRun("device init")
        self.activeRun("pw create satop 1")
        self.activeRun("pw circuit bind 1 de1.1")
        self.activeRun("pw ethport 1 1")
        self.activeRun("pw psn 1 mpls")
        self.activeRun("pw backup psn 1 mpls 2")
        self.activeRun("pw enable 1")
        self.activeRun("pw group hs create 1")
        self.activeRun("pw group hs add 1 1")
        
        self.standbyRun("driver role standby")
        self.standbyRun("device init")
        self.standbyRun("pw create satop 1")
        self.standbyRun("pw circuit bind 1 de1.1")
        self.standbyRun("pw ethport 1 1")
        self.standbyRun("pw psn 1 mpls")
        self.standbyRun("pw backup psn 1 mpls 2")
        self.standbyRun("pw enable 1")
        self.standbyRun("pw group hs create 1")
        self.standbyRun("pw group hs add 1 1")
        self.standbyRun("driver role active")
        self.standbyRun("driver restore")
        
        self.serializeAndCompare()
      
    def testIssuDs1Satop(self):
        self.activeRun("driver role active")
        self.activeRun("device init")
        self.activeRun("pw create satop 1-48") 
        self.activeRun("pw circuit bind 1-48 de1.1-48")
        self.activeRun("pw ethport 1-48 1")
        self.activeRun("pw psn 1-48 mpls")
        self.activeRun("pw enable 1-48")
        
        self.standbyRun("driver role standby")
        self.standbyRun("device init")
        self.standbyRun("pw create satop 25-48,1-24") 
        self.standbyRun("pw circuit bind 25-48,1-24 de1.25-48,1-24")
        self.standbyRun("pw ethport 25-48,1-24 1")
        self.standbyRun("pw psn 25-48,1-24 mpls")
        self.standbyRun("pw enable 25-48,1-24")
        
        self.serializeAndCompare()
    
    def testIssuDs1Satop1(self):
        self.activeRun("device init")
        self.activeRun("pw create satop 1")
        self.activeRun("pw circuit bind 1 de1.1")
        self.activeRun("pw payloadsize 1 256")
        self.activeRun("pw jitterbuffer 1 32000")
        self.activeRun("pw jitterdelay 1 16000")
        self.activeRun("pw reorder enable 1")
        self.activeRun("pw ethport 1 1")
        self.activeRun("pw ethheader 1 C0.CA.C0.CA.C0.CA none none")
        self.activeRun("pw psn 1 mpls")
        self.activeRun("pw mpls innerlabel 1 1.1.1")
        self.activeRun("pw mpls expectedlabel 1 1")
        self.activeRun("pw debug prbs enable 1")
        self.activeRun("pw enable 1")
        
        self.standbyRun("driver role standby")
        self.standbyRun("device init")
        self.standbyRun("pw create satop 1")
        self.standbyRun("pw circuit bind 1 de1.1")
        self.standbyRun("pw payloadsize 1 256")
        self.standbyRun("pw jitterbuffer 1 32000")
        self.standbyRun("pw jitterdelay 1 16000")
        self.standbyRun("pw enable 1")
        self.standbyRun("pw reorder enable 1")
        self.standbyRun("pw ethport 1 1")
        self.standbyRun("pw ethheader 1 C0.CA.C0.CA.C0.CA none none")
        self.standbyRun("pw psn 1 mpls")
        self.standbyRun("pw mpls innerlabel 1 1.1.1")
        self.standbyRun("pw mpls expectedlabel 1 1")
        self.standbyRun("pw debug prbs enable 1")
        
        self.serializeAndCompare()

    def testIssuDs1Satop_2(self):
        self.activeRun("device init")
        self.activeRun("pdh de1 framing 1-48 e1_unframed")
        self.activeRun("pw create satop 1")
        self.activeRun("pw circuit bind 1 de1.1")
        self.activeRun("pw payloadsize 1 256")
        self.activeRun("pw jitterbuffer 1 32000")
        self.activeRun("pw jitterdelay 1 16000")
        self.activeRun("pw reorder enable 1")
        self.activeRun("pw ethport 1 1")
        self.activeRun("pw ethheader 1 C0.CA.C0.CA.C0.CA none none")
        self.activeRun("pw psn 1 mpls")
        self.activeRun("pw mpls innerlabel 1 1.1.1")
        self.activeRun("pw mpls expectedlabel 1 1")
        self.activeRun("pw debug prbs enable 1")
        self.activeRun("pw enable 1")
        
        self.standbyRun("driver role standby")
        self.standbyRun("device init")
        self.standbyRun("pdh de1 framing 1-48 e1_unframed")
        self.standbyRun("pw create satop 1")
        self.standbyRun("pw circuit bind 1 de1.1")
        self.standbyRun("pw jitterbuffer 1 32000")
        self.standbyRun("pw jitterdelay 1 16000")
        self.standbyRun("pw payloadsize 1 256")
        self.standbyRun("pw enable 1")
        self.standbyRun("pw psn 1 mpls")
        self.standbyRun("pw mpls innerlabel 1 1.1.1")
        self.standbyRun("pw mpls expectedlabel 1 1")
        self.standbyRun("pw reorder enable 1")
        self.standbyRun("pw ethport 1 1")
        self.standbyRun("pw ethheader 1 C0.CA.C0.CA.C0.CA none none")
        self.standbyRun("pw debug prbs enable 1")
        
        self.serializeAndCompare()
        
    def testIssuDs1CESoP(self):
        self.activeRun("device init")
        self.activeRun("eth port srcmac 1 C0.CA.C0.CA.C0.CA")
        self.activeRun("eth port ipv4 1 85.85.85.85")
        self.activeRun("eth port ipv6 1 0.0.0.0.0.0.0.0.0.0.0.0.85.85.85.85")
        self.activeRun("eth port maccheck 1 en")
        self.activeRun("eth port interface 1 xgmii")
        self.activeRun("eth port autoneg 1 dis")
        self.activeRun("eth port txipg 1 12")
        self.activeRun("eth port rxipg 1 4")
        self.activeRun("eth port pauseframe interval 1 1003")
        self.activeRun("eth port pauseframe expire 1 2007")
        self.activeRun("eth port loopback 1 release")
        self.activeRun("pdh de1 framing 1-48 ds1_esf")
        self.activeRun("pdh de1 nxds0create 1 0-23")
        self.activeRun("pw create cesop 1 basic")
        self.activeRun("pw circuit bind 1 nxds0.1.0-23")
        
        self.standbyRun("driver role standby")
        self.standbyRun("device init")
        self.standbyRun("eth port srcmac 1 C0.CA.C0.CA.C0.CA")
        self.standbyRun("eth port ipv4 1 85.85.85.85")
        self.standbyRun("eth port ipv6 1 0.0.0.0.0.0.0.0.0.0.0.0.85.85.85.85")
        self.standbyRun("eth port maccheck 1 en")
        self.standbyRun("eth port interface 1 xgmii")
        self.standbyRun("eth port autoneg 1 dis")
        self.standbyRun("eth port txipg 1 12")
        self.standbyRun("eth port rxipg 1 4")
        self.standbyRun("eth port pauseframe interval 1 1003")
        self.standbyRun("eth port pauseframe expire 1 2007")
        self.standbyRun("eth port loopback 1 release")
        self.standbyRun("pdh de1 framing 1-48 ds1_esf")
        self.standbyRun("pdh de1 nxds0create 1 0-23")
        self.standbyRun("pw create cesop 1 basic")
        self.standbyRun("pw circuit bind 1 nxds0.1.0-23")
        
        self.serializeAndCompare()
        
    def testIssuDs1CESoPAcrDcr(self):
        self.activeRun("device init")
        self.activeRun("pdh de1 framing 1-48 ds1_esf")
        self.activeRun("pdh de1 nxds0create 1-48 0-15")
        self.activeRun("pdh de1 nxds0create 1-48 16-23")
        self.activeRun("pw create cesop 1-96 basic")
        self.activeRun("pw circuit bind 1-48 nxds0.1-48.0-15")
        self.activeRun("pw circuit bind 49-96 nxds0.1-48.16-23")
        self.activeRun("pw enable 1-96")
        self.activeRun("pw psn 1-96 mpls")
        # Do not configure channels timing on standby driver because channel timing may be changed in interrupt context
        # that never happen on standby driver
        self.activeRun("pdh de1 timing 1-24 acr 1-24")
        self.activeRun("pdh de1 timing 25-48 dcr 73-96")
        
        self.standbyRun("driver role standby")
        self.standbyRun("device init")
        self.standbyRun("pdh de1 framing 1-48 ds1_esf")
        self.standbyRun("pdh de1 nxds0create 1-48 0-15")
        self.standbyRun("pdh de1 nxds0create 1-48 16-23")
        self.standbyRun("pw create cesop 1-96 basic")
        self.standbyRun("pw circuit bind 49-96 nxds0.1-48.16-23")
        self.standbyRun("pw circuit bind 1-48 nxds0.1-48.0-15")
        self.standbyRun("pw enable 1-96")
        self.standbyRun("pw psn 1-96 mpls")
        
        self.serializeAndCompare()
        
    def testRedundantDs1Satop(self):
        self.standbyRun("driver role standby")
        
        self.activeRun("device init")
        self.activeRun("eth port interface 1 xgmii")
        self.standbyRun("device init")
        self.standbyRun("eth port interface 1 xgmii")
        
        for i in range(0, self.numDe1()):
            de1Id = `i + 1`
            pwId  = de1Id
            
            self.activeRun("pdh de1 framing " + de1Id + " ds1_unframed")
            self.activeRun("pw create satop " + pwId)
            self.activeRun("pw circuit bind " + pwId + " de1." + de1Id)
            
            self.standbyRun("pdh de1 framing " + de1Id + " ds1_unframed")
            self.standbyRun("pw create satop " + pwId)
            self.standbyRun("pw circuit bind " + pwId + " de1." + de1Id)
        
        for i in range(0, self.numDe1()):
            pwId = `i + 1`
            
            self.activeRun("pw ethheader " + pwId + " C0.CA.C0.CA.C0.CA 0.0.4095 none")
            self.activeRun("eth port maccheck 1 en")
            self.activeRun("pw psn " + pwId + " mpls " + `49200 + i`)
            self.activeRun("pw ethport " + pwId + " 1")
            
            self.standbyRun("pw ethheader " + pwId + " C0.CA.C0.CA.C0.CA 0.0.4095 none")
            self.standbyRun("eth port maccheck 1 en")
            self.standbyRun("pw psn " + pwId + " mpls " + `49200 + i`)
            self.standbyRun("pw ethport " + pwId + " 1")

            self.activeRun("pw enable " + pwId)
            self.standbyRun("pw enable " + pwId)
        
        self.serializeAndCompare()

    def _random(self):
        self.randomizer = Af60210021Randomizer()
        return self.randomizer.defaultRandom()
    
    def TestIssuRandomWithDeprovision(self, deprovision=False):
        def _activeRun(cli):
            self.activeRun(cli)
        def _standbyRun(cli):
            self.standbyRun(cli)
        
        def _applyDict(aDict, cliRunFunc):
            for cli in aDict["cli"]:
                cliRunFunc(cli)
            
            subChannels = aDict["subChannels"]
            random.shuffle(subChannels)
            for subChannel in subChannels:
                _applyDict(subChannel, cliRunFunc)
            
        for _ in range(0, 10):
            self.activeRun("driver role active")
            self.standbyRun("driver role standby")
            
            linesConfiguration = self._random()
            
            # Randomly apply this configuration on active
            self.activeRun("device init")
            random.shuffle(linesConfiguration)
            for line in linesConfiguration:
                _applyDict(line, _activeRun)
                
            # Randomly apply this configuration on standby
            self.standbyRun("device init")
            random.shuffle(linesConfiguration)
            for line in linesConfiguration:
                _applyDict(line, _standbyRun)
            
            if deprovision:
                def _deprovision(deprovisions, cliRunFunc):
                    for pwCliList in deprovisions:
                        for cli in pwCliList:
                            cliRunFunc(cli)
                            
                # Have deprovision random sequence
                deprovisions = self.randomizer.randomDeprovisonPw()
                            
                # Apply on active
                random.shuffle(deprovisions)
                _deprovision(deprovisions, _activeRun)
                
                # Apply on standby
                random.shuffle(deprovisions)
                _deprovision(deprovisions, _standbyRun)
            
            self.serializeAndCompare()
            
    def testIssuRandom(self):
        self.TestIssuRandomWithDeprovision(False)
            
    def testIssuRandomWithDeprovision(self):
        self.TestIssuRandomWithDeprovision(True)
            
    def testRedundancyRandom(self):
        def _applyDict(aDict):
            for cli in aDict["cli"]:
                self.activeRun(cli)
                self.standbyRun(cli)
            
            subChannels = aDict["subChannels"]
            random.shuffle(subChannels)
            for subChannel in subChannels:
                _applyDict(subChannel)
        
        for _ in range(0, 10):
            self.activeRun("driver role active")
            self.standbyRun("driver role standby")
        
            self.activeRun("device init")
            self.standbyRun("device init")
            
            linesConfiguration = self._random()
            random.shuffle(linesConfiguration)
            for line in linesConfiguration:
                _applyDict(line)
                
            self.serializeAndCompare()
                
if __name__ == "__main__":
    Af6021HaTest.run(Af60210021HaTest, sys.argv)

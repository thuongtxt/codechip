from AtOs import *
from AtColor import * 

if __name__ == '__main__':
    sdkHome = "/home/scratch/namnn/atsdk"
    targetDir = "/home/ATVN/namnn/libs/python/"
    
    def parseOptions(argv):
        global sdkHome
        global targetDir
        
        try:
            opts, args = getopt.getopt(argv[1:], "",["sdk-home=", "target-dir="])
        except getopt.GetoptError:
            printUsage(argv[0])
            sys.exit(2)
            
        for opt, arg in opts:
            if opt == "--sdk-home":
                sdkHome = arg
            if opt == "--target-dir":
                targetDir = arg
        
        if (sdkHome is None) or (targetDir is None):
            print "Usage: %s --sdk-home=<SDK home> --target-dir=<target directory to copy>" % argv[0]
            sys.exit(1)
    
    AtOs.run("cp -fr %s/scripts/python/jenkin %s" % (sdkHome, targetDir))
    AtOs.run("cp -fr %s/scripts/python/build %s" % (sdkHome, targetDir))
    print AtColor.printColor(AtColor.GREEN, "Update done!")

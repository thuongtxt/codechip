import python.arrive.atsdk.AtRegister as AtRegister

class RegisterProviderFactory(AtRegister.AtRegisterProviderFactory):
    def _allRegisterProviders(self):
        allProviders = {}

        from _AF6CCI0021_RD_ReTiming import _AF6CCI0021_RD_ReTiming
        allProviders["_AF6CCI0021_RD_ReTiming"] = _AF6CCI0021_RD_ReTiming()

        from _AF6CCI0031_RD_BERT_GEN import _AF6CCI0031_RD_BERT_GEN
        allProviders["_AF6CCI0031_RD_BERT_GEN"] = _AF6CCI0031_RD_BERT_GEN()

        from _AF6CCI0031_RD_BERT_MON import _AF6CCI0031_RD_BERT_MON
        allProviders["_AF6CCI0031_RD_BERT_MON"] = _AF6CCI0031_RD_BERT_MON()

        from _AF6CCI0031_RD_BERT_MON_PW import _AF6CCI0031_RD_BERT_MON_PW
        allProviders["_AF6CCI0031_RD_BERT_MON_PW"] = _AF6CCI0031_RD_BERT_MON_PW()

        from _AF6CCI0031_RD_CDR import _AF6CCI0031_RD_CDR
        allProviders["_AF6CCI0031_RD_CDR"] = _AF6CCI0031_RD_CDR()

        from _AF6CCI0031_RD_CLA import _AF6CCI0031_RD_CLA
        allProviders["_AF6CCI0031_RD_CLA"] = _AF6CCI0031_RD_CLA()

        from _AF6CCI0031_RD_ETH import _AF6CCI0031_RD_ETH
        allProviders["_AF6CCI0031_RD_ETH"] = _AF6CCI0031_RD_ETH()

        from _AF6CCI0031_RD_GLB import _AF6CCI0031_RD_GLB
        allProviders["_AF6CCI0031_RD_GLB"] = _AF6CCI0031_RD_GLB()

        from _AF6CCI0031_RD_MAP import _AF6CCI0031_RD_MAP
        allProviders["_AF6CCI0031_RD_MAP"] = _AF6CCI0031_RD_MAP()

        from _AF6CCI0031_RD_OCN import _AF6CCI0031_RD_OCN
        allProviders["_AF6CCI0031_RD_OCN"] = _AF6CCI0031_RD_OCN()

        from _AF6CCI0031_RD_PDA import _AF6CCI0031_RD_PDA
        allProviders["_AF6CCI0031_RD_PDA"] = _AF6CCI0031_RD_PDA()

        from _AF6CCI0031_RD_PDH import _AF6CCI0031_RD_PDH
        allProviders["_AF6CCI0031_RD_PDH"] = _AF6CCI0031_RD_PDH()

        from _AF6CCI0031_RD_PDH_MDLPRM import _AF6CCI0031_RD_PDH_MDLPRM
        allProviders["_AF6CCI0031_RD_PDH_MDLPRM"] = _AF6CCI0031_RD_PDH_MDLPRM()

        from _AF6CCI0031_RD_PLA import _AF6CCI0031_RD_PLA
        allProviders["_AF6CCI0031_RD_PLA"] = _AF6CCI0031_RD_PLA()

        from _AF6CCI0031_RD_PM import _AF6CCI0031_RD_PM
        allProviders["_AF6CCI0031_RD_PM"] = _AF6CCI0031_RD_PM()

        from _AF6CCI0031_RD_PMC import _AF6CCI0031_RD_PMC
        allProviders["_AF6CCI0031_RD_PMC"] = _AF6CCI0031_RD_PMC()

        from _AF6CCI0031_RD_POH_BER import _AF6CCI0031_RD_POH_BER
        allProviders["_AF6CCI0031_RD_POH_BER"] = _AF6CCI0031_RD_POH_BER()

        from _AF6CCI0031_RD_PWE import _AF6CCI0031_RD_PWE
        allProviders["_AF6CCI0031_RD_PWE"] = _AF6CCI0031_RD_PWE()

        from _AF6CCI0031_RD_TOP import _AF6CCI0031_RD_TOP
        allProviders["_AF6CCI0031_RD_TOP"] = _AF6CCI0031_RD_TOP()

        from _AF6CCI0031_RD_XGE import _AF6CCI0031_RD_XGE
        allProviders["_AF6CCI0031_RD_XGE"] = _AF6CCI0031_RD_XGE()


        return allProviders

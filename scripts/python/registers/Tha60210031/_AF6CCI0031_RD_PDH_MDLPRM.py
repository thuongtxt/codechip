import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0031_RD_PDH_MDLPRM(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["upen_cfg_debug"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_cfg_debug()
        allRegisters["upen_cfgid_mon"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_cfgid_mon()
        allRegisters["upen_prm_txcfgcr"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_prm_txcfgcr()
        allRegisters["upen_prm_txcfglb"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_prm_txcfglb()
        allRegisters["upen_prm_txsta"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_prm_txsta()
        allRegisters["upen_mdl_txsta_stuff"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_txsta_stuff()
        allRegisters["upen_cfg_ctrl0"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_cfg_ctrl0()
        allRegisters["upen_txprm_cnt_byte"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txprm_cnt_byte()
        allRegisters["upen_txprm_cnt_pkt"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txprm_cnt_pkt()
        allRegisters["upen_txprm_cnt_info"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txprm_cnt_info()
        allRegisters["upen_mdl_idle1"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_idle1()
        allRegisters["upen_mdl_idle2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_idle2()
        allRegisters["upen_mdl_tp1"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_tp1()
        allRegisters["upen_mdl_tp2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_tp2()
        allRegisters["upen_mdl_txsta"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_txsta()
        allRegisters["upen_sta_idle_alren"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_sta_idle_alren()
        allRegisters["upen_sta_tp_alren"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_sta_tp_alren()
        allRegisters["upen_cfg_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_cfg_mdl()
        allRegisters["upen_txmdl_cntr2c_byteidle1"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntr2c_byteidle1()
        allRegisters["upen_txmdl_cntro_byteidle1"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntro_byteidle1()
        allRegisters["upen_txmdl_cntr2c_bytepath"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntr2c_bytepath()
        allRegisters["upen_txmdl_cntro_bytepath"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntro_bytepath()
        allRegisters["upen_txmdl_cntr2c_bytetest"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntr2c_bytetest()
        allRegisters["upen_txmdl_cntro_bytetest"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntro_bytetest()
        allRegisters["upen_txmdl_cntr2c_valididle"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntr2c_valididle()
        allRegisters["upen_txmdl_cntro_valididle"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntro_valididle()
        allRegisters["upen_txmdl_cntr2c_validpath"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntr2c_validpath()
        allRegisters["upen_txmdl_cntro_validpath"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntro_validpath()
        allRegisters["upen_txmdl_cntr2c_validtest"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntr2c_validtest()
        allRegisters["upen_txmdl_cntro_validtest"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntro_validtest()
        allRegisters["upen_mdl_idle1_2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_idle1_2()
        allRegisters["upen_mdl_idle2_2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_idle2_2()
        allRegisters["upen_mdl_tp1_2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_tp1_2()
        allRegisters["upen_mdl_tp2_2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_tp2_2()
        allRegisters["upen_mdl_txsta_2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_txsta_2()
        allRegisters["upen_sta_idle_alren_2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_sta_idle_alren_2()
        allRegisters["upen_sta_tp_alren_2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_sta_tp_alren_2()
        allRegisters["upen_cfg_mdl_2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_cfg_mdl_2()
        allRegisters["upen_txmdl_cntr2c_byteidle2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntr2c_byteidle2()
        allRegisters["upen_txmdl_cntro_byteidle2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntro_byteidle2()
        allRegisters["upen_txmdl_cntr2c_bytepath2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntr2c_bytepath2()
        allRegisters["upen_txmdl_cntro_bytepath2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntro_bytepath2()
        allRegisters["upen_txmdl_cntr2c_bytetest2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntr2c_bytetest2()
        allRegisters["upen_txmdl_cntro_bytetest2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntro_bytetest2()
        allRegisters["upen_txmdl_cntr2c_valididle2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntr2c_valididle2()
        allRegisters["upen_txmdl_cntro_valididle2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntro_valididle2()
        allRegisters["upen_txmdl_cntr2c_validpath2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntr2c_validpath2()
        allRegisters["upen_txmdl_cntro_validpath2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntro_validpath2()
        allRegisters["upen_txmdl_cntr2c_validtest2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntr2c_validtest2()
        allRegisters["upen_txmdl_cntro_validtest2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntro_validtest2()
        allRegisters["upen_rxprm_cfgstd"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxprm_cfgstd()
        allRegisters["upen_rxmdl_typebuff1"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_typebuff1()
        allRegisters["upen_rxmdl_cfgtype"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cfgtype()
        allRegisters["upen_destuff_ctrl0"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_destuff_ctrl0()
        allRegisters["upen_mdl_stk_cfg1"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_stk_cfg1()
        allRegisters["upen_mdl_stk_cfg2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_stk_cfg2()
        allRegisters["upen_rxprm_gmess"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxprm_gmess()
        allRegisters["upen_rxprm_drmess"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxprm_drmess()
        allRegisters["upen_rxprm_mmess"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxprm_mmess()
        allRegisters["upen_fcs_stk1"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_fcs_stk1()
        allRegisters["upen_fcs_stk2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_fcs_stk2()
        allRegisters["upen_rxprm_cnt_byte"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxprm_cnt_byte()
        allRegisters["upen_rxmdl_cntr2c_byteidle"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntr2c_byteidle()
        allRegisters["upen_rxmdl_cntro_byteidle"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntro_byteidle()
        allRegisters["upen_rxmdl_cntr2c_bytepath"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntr2c_bytepath()
        allRegisters["upen_rxmdl_cntro_bytepath"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntro_bytepath()
        allRegisters["upen_rxmdl_cntr2c_bytetest"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntr2c_bytetest()
        allRegisters["upen_rxmdl_cntro_bytetest"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntro_bytetest()
        allRegisters["upen_rxmdl_cntr2c_goodidle"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntr2c_goodidle()
        allRegisters["upen_rxmdl_cntro_goodidle"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntro_goodidle()
        allRegisters["upen_rxmdl_cntr2c_goodpath"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntr2c_goodpath()
        allRegisters["upen_rxmdl_cntro_goodpath"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntro_goodpath()
        allRegisters["upen_rxmdl_cntr2c_goodtest"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntr2c_goodtest()
        allRegisters["upen_rxmdl_cntro_goodtest"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntro_goodtest()
        allRegisters["upen_rxmdl_cntr2c_dropidle"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntr2c_dropidle()
        allRegisters["upen_rxmdl_cntro_dropidle"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntro_dropidle()
        allRegisters["upen_rxmdl_cntr2c_droppath"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntr2c_droppath()
        allRegisters["upen_rxmdl_cntro_droppath"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntro_droppath()
        allRegisters["upen_rxmdl_cntr2c_droptest"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntr2c_droptest()
        allRegisters["upen_rxmdl_cntro_droptest"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntro_droptest()
        allRegisters["prm_cfg_lben_int"] = _AF6CCI0031_RD_PDH_MDLPRM._prm_cfg_lben_int()
        allRegisters["prm_lb_int_sta"] = _AF6CCI0031_RD_PDH_MDLPRM._prm_lb_int_sta()
        allRegisters["prm_lb_int_crrsta"] = _AF6CCI0031_RD_PDH_MDLPRM._prm_lb_int_crrsta()
        allRegisters["prm_lb_intsta"] = _AF6CCI0031_RD_PDH_MDLPRM._prm_lb_intsta()
        allRegisters["prm_lb_sta_int"] = _AF6CCI0031_RD_PDH_MDLPRM._prm_lb_sta_int()
        allRegisters["prm_lb_en_int"] = _AF6CCI0031_RD_PDH_MDLPRM._prm_lb_en_int()
        allRegisters["mdl_cfgen_int"] = _AF6CCI0031_RD_PDH_MDLPRM._mdl_cfgen_int()
        allRegisters["mdl_int_sta"] = _AF6CCI0031_RD_PDH_MDLPRM._mdl_int_sta()
        allRegisters["mdl_int_crrsta"] = _AF6CCI0031_RD_PDH_MDLPRM._mdl_int_crrsta()
        allRegisters["mdl_intsta"] = _AF6CCI0031_RD_PDH_MDLPRM._mdl_intsta()
        allRegisters["mdl_sta_int"] = _AF6CCI0031_RD_PDH_MDLPRM._mdl_sta_int()
        allRegisters["mdl_en_int"] = _AF6CCI0031_RD_PDH_MDLPRM._mdl_en_int()
        return allRegisters

    class _upen_cfg_debug(AtRegister.AtRegister):
        def name(self):
            return "CONFIG ENABLE MONITOR FOR DEBUG BOARD"
    
        def description(self):
            return "config enable monitor data before stuff"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00014001
            
        def endAddress(self):
            return 0xffffffff

        class _out_cfg_bar(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 19
        
            def name(self):
                return "out_cfg_bar"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _out_cfg_selinfo1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "out_cfg_selinfo1"
            
            def description(self):
                return "select just 8 bit INFO from PRM FORCE but except 2 bit NmNi, (1) is enable, (0) is disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_loopaback_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "cfg_loopaback_mdl"
            
            def description(self):
                return "loop back MDL DS3, (1) is enable, (0) is disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_loopaback_prm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "cfg_loopaback_prm"
            
            def description(self):
                return "loop back PRM DS1, (1) is enable, (0) is disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _out_info_force(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "out_info_force"
            
            def description(self):
                return "10 bits info force [15:13] : 3'd1 : to set bit G1 3'd2 :  to set bit G2 3'd3 :  to set bit G3 3'd4 : to set bit G4 3'd5 : to set bit G5 3'd6 :  to set bit G6 [12] : to set bit SE [11] : to set bit FE [10] : to set bit LV [9] : to set bit SL [8] : to set bit LB"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 1
        
            def name(self):
                return "reserve"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _out_cfg_dis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "out_cfg_dis"
            
            def description(self):
                return "disable monitor, reset address buffer monitor, (1) is disable, (0) is enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["out_cfg_bar"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_cfg_debug._out_cfg_bar()
            allFields["out_cfg_selinfo1"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_cfg_debug._out_cfg_selinfo1()
            allFields["cfg_loopaback_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_cfg_debug._cfg_loopaback_mdl()
            allFields["cfg_loopaback_prm"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_cfg_debug._cfg_loopaback_prm()
            allFields["out_info_force"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_cfg_debug._out_info_force()
            allFields["reserve"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_cfg_debug._reserve()
            allFields["out_cfg_dis"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_cfg_debug._out_cfg_dis()
            return allFields

    class _upen_cfgid_mon(AtRegister.AtRegister):
        def name(self):
            return "CONFIG ID TO MONITOR FOR DEBUG BOARD"
    
        def description(self):
            return "config ID to monitor tx message"
            
        def width(self):
            return 10
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00014002
            
        def endAddress(self):
            return 0xffffffff

        class _out_cfgid_mon(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "out_cfgid_mon"
            
            def description(self):
                return "ID which is configured to monitor"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["out_cfgid_mon"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_cfgid_mon._out_cfgid_mon()
            return allFields

    class _upen_prm_txcfgcr(AtRegister.AtRegister):
        def name(self):
            return "CONFIG PRM BIT C/R & STANDARD Tx"
    
        def description(self):
            return "Config PRM bit C/R & Standard"
            
        def width(self):
            return 4
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1000+$STSID[4:3]*7 + $VTGID*64 + $SLICEID*32 + $STSID[2:0]*4 + $VTID"
            
        def startAddress(self):
            return 0x00001000
            
        def endAddress(self):
            return 0x0000153f

        class _cfg_prmen_tx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "cfg_prmen_tx"
            
            def description(self):
                return "config enable Tx, (0) is disable (1) is enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_prm_txenstuff(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "cfg_prm_txenstuff"
            
            def description(self):
                return "config enable stuff message, (1) is disable, (0) is enbale"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_prmstd_tx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfg_prmstd_tx"
            
            def description(self):
                return "config standard Tx, (0) is ANSI, (1) is AT&T"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_prm_cr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_prm_cr"
            
            def description(self):
                return "config bit command/respond PRM message"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_prmen_tx"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_prm_txcfgcr._cfg_prmen_tx()
            allFields["cfg_prm_txenstuff"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_prm_txcfgcr._cfg_prm_txenstuff()
            allFields["cfg_prmstd_tx"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_prm_txcfgcr._cfg_prmstd_tx()
            allFields["cfg_prm_cr"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_prm_txcfgcr._cfg_prm_cr()
            return allFields

    class _upen_prm_txcfglb(AtRegister.AtRegister):
        def name(self):
            return "CONFIG PRM BIT L/B Tx"
    
        def description(self):
            return "CONFIG PRM BIT L/B Tx"
            
        def width(self):
            return 2
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x1800+$SLICEID*1024 + $STSID*32 + $VTGID*4 + $VTID"
            
        def startAddress(self):
            return 0x00001800
            
        def endAddress(self):
            return 0x00001d3f

        class _cfg_prm_enlb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfg_prm_enlb"
            
            def description(self):
                return "config enable CPU config LB bit, (0) is disable, (1) is enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_prm_lb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_prm_lb"
            
            def description(self):
                return "config bit Loopback PRM message"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_prm_enlb"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_prm_txcfglb._cfg_prm_enlb()
            allFields["cfg_prm_lb"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_prm_txcfglb._cfg_prm_lb()
            return allFields

    class _upen_prm_txsta(AtRegister.AtRegister):
        def name(self):
            return "Status process PRM status stuff"
    
        def description(self):
            return "Status process PRM data"
            
        def width(self):
            return 5
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x2000+$STSID[4:3]*7 + $VTGID*64 + $SLICEID*32 + $STSID[2:0]*4 + $VTID"
            
        def startAddress(self):
            return 0x00002000
            
        def endAddress(self):
            return 0x0000253f

        class _sta_arm_mess(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "sta_arm_mess"
            
            def description(self):
                return "use for detect new message"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cnt_prm_lenmess(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_prm_lenmess"
            
            def description(self):
                return "count number of byte in message"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["sta_arm_mess"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_prm_txsta._sta_arm_mess()
            allFields["cnt_prm_lenmess"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_prm_txsta._cnt_prm_lenmess()
            return allFields

    class _upen_mdl_txsta_stuff(AtRegister.AtRegister):
        def name(self):
            return "Status process MDL STUFF"
    
        def description(self):
            return "Status process MDL data"
            
        def width(self):
            return 36
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x2800+$MDLID"
            
        def startAddress(self):
            return 0x00002800
            
        def endAddress(self):
            return 0x0000282f

        class _sta1_de3_enstuff(AtRegister.AtRegisterField):
            def stopBit(self):
                return 35
                
            def startBit(self):
                return 20
        
            def name(self):
                return "sta1_de3_enstuff"
            
            def description(self):
                return "status 1 for enstuff byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _sta0_de3_enstuff(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "sta0_de3_enstuff"
            
            def description(self):
                return "status 0 for enstuff byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["sta1_de3_enstuff"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_txsta_stuff._sta1_de3_enstuff()
            allFields["sta0_de3_enstuff"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_txsta_stuff._sta0_de3_enstuff()
            return allFields

    class _upen_cfg_ctrl0(AtRegister.AtRegister):
        def name(self):
            return "CONFIG CONTROL STUFF 0"
    
        def description(self):
            return "config control Stuff global"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00003000
            
        def endAddress(self):
            return 0xffffffff

        class _out_txcfg_ctrl0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 7
        
            def name(self):
                return "out_txcfg_ctrl0"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _reserve1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 2
        
            def name(self):
                return "reserve1"
            
            def description(self):
                return "reserve1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fcsinscfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "fcsinscfg"
            
            def description(self):
                return "(1) enable insert FCS, (0) disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "reserve"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["out_txcfg_ctrl0"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_cfg_ctrl0._out_txcfg_ctrl0()
            allFields["reserve1"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_cfg_ctrl0._reserve1()
            allFields["fcsinscfg"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_cfg_ctrl0._fcsinscfg()
            allFields["reserve"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_cfg_ctrl0._reserve()
            return allFields

    class _upen_txprm_cnt_byte(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE TX PRM"
    
        def description(self):
            return "counter byte message PRM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x4000+$STSID[4:3]*7 + $VTGID*64 + $SLICEID*32 + $STSID[2:0]*4 + $VTID + $UPRO * 2048 "
            
        def startAddress(self):
            return 0x00004000
            
        def endAddress(self):
            return 0x00004d3f

        class _cnt_byte_prm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_byte_prm"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_byte_prm"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txprm_cnt_byte._cnt_byte_prm()
            return allFields

    class _upen_txprm_cnt_pkt(AtRegister.AtRegister):
        def name(self):
            return "COUNTER PACKET MESSAGE TX PRM"
    
        def description(self):
            return "counter packet message PRM"
            
        def width(self):
            return 15
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x5000+$STSID[4:3]*7 + $VTGID*64 + $SLICEID*32 + $STSID[2:0]*4 + $VTID + $UPRO * 2048 "
            
        def startAddress(self):
            return 0x00005000
            
        def endAddress(self):
            return 0x00005d3f

        class _cnt_pkt_prm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_pkt_prm"
            
            def description(self):
                return "value counter packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_pkt_prm"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txprm_cnt_pkt._cnt_pkt_prm()
            return allFields

    class _upen_txprm_cnt_info(AtRegister.AtRegister):
        def name(self):
            return "COUNTER VALID INFO TX PRM"
    
        def description(self):
            return "counter valid info PRM"
            
        def width(self):
            return 15
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x6000+$STSID[4:3]*7 + $VTGID*64 + $SLICEID*32 + $STSID[2:0]*4 + $VTID + $UPRO * 2048 "
            
        def startAddress(self):
            return 0x00006000
            
        def endAddress(self):
            return 0x00006d3f

        class _cnt_vld_prm_info(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_vld_prm_info"
            
            def description(self):
                return "value counter valid info"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_vld_prm_info"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txprm_cnt_info._cnt_vld_prm_info()
            return allFields

    class _upen_mdl_idle1(AtRegister.AtRegister):
        def name(self):
            return "Buff Message MDL IDLE1"
    
        def description(self):
            return "config message MDL IDLE Channel ID 0-15"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0000+ $DWORDID*16 + $DE3ID1"
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0x0000012f

        class _idle_byte13(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "idle_byte13"
            
            def description(self):
                return "MS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _ilde_byte12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ilde_byte12"
            
            def description(self):
                return "BYTE 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _idle_byte11(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "idle_byte11"
            
            def description(self):
                return "BYTE 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _idle_byte10(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "idle_byte10"
            
            def description(self):
                return "LS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["idle_byte13"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_idle1._idle_byte13()
            allFields["ilde_byte12"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_idle1._ilde_byte12()
            allFields["idle_byte11"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_idle1._idle_byte11()
            allFields["idle_byte10"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_idle1._idle_byte10()
            return allFields

    class _upen_mdl_idle2(AtRegister.AtRegister):
        def name(self):
            return "Buff Message MDL IDLE2"
    
        def description(self):
            return "config message MDL IDLE Channel ID 16-23"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0130+ $DWORDID*8 + $DE3ID2"
            
        def startAddress(self):
            return 0x00000130
            
        def endAddress(self):
            return 0x000001c7

        class _idle_byte23(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "idle_byte23"
            
            def description(self):
                return "MS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _ilde_byte22(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ilde_byte22"
            
            def description(self):
                return "BYTE 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _idle_byte21(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "idle_byte21"
            
            def description(self):
                return "BYTE 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _idle_byte20(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "idle_byte20"
            
            def description(self):
                return "LS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["idle_byte23"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_idle2._idle_byte23()
            allFields["ilde_byte22"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_idle2._ilde_byte22()
            allFields["idle_byte21"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_idle2._idle_byte21()
            allFields["idle_byte20"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_idle2._idle_byte20()
            return allFields

    class _upen_mdl_tp1(AtRegister.AtRegister):
        def name(self):
            return "Config Buff Message MDL TESTPATH1"
    
        def description(self):
            return "config message MDL TESTPATH Channel ID 0-15"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0200+$DE3ID1 + $DWORDID*16"
            
        def startAddress(self):
            return 0x00000200
            
        def endAddress(self):
            return 0x0000032f

        class _tp_byte13(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "tp_byte13"
            
            def description(self):
                return "MS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tp_byte12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "tp_byte12"
            
            def description(self):
                return "BYTE 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tp_byte11(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "tp_byte11"
            
            def description(self):
                return "BYTE 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tp_byte10(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tp_byte10"
            
            def description(self):
                return "LS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tp_byte13"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_tp1._tp_byte13()
            allFields["tp_byte12"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_tp1._tp_byte12()
            allFields["tp_byte11"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_tp1._tp_byte11()
            allFields["tp_byte10"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_tp1._tp_byte10()
            return allFields

    class _upen_mdl_tp2(AtRegister.AtRegister):
        def name(self):
            return "Config Buff Message MDL TESTPATH2"
    
        def description(self):
            return "config message MDL TESTPATH Channel ID 16-23"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0330+ $DWORDID*8+ $DE3ID2"
            
        def startAddress(self):
            return 0x00000330
            
        def endAddress(self):
            return 0x000003c7

        class _tp_byte23(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "tp_byte23"
            
            def description(self):
                return "MS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tp_byte22(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "tp_byte22"
            
            def description(self):
                return "BYTE 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tp_byte21(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "tp_byte21"
            
            def description(self):
                return "BYTE 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tp_byte20(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tp_byte20"
            
            def description(self):
                return "LS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tp_byte23"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_tp2._tp_byte23()
            allFields["tp_byte22"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_tp2._tp_byte22()
            allFields["tp_byte21"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_tp2._tp_byte21()
            allFields["tp_byte20"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_tp2._tp_byte20()
            return allFields

    class _upen_mdl_txsta(AtRegister.AtRegister):
        def name(self):
            return "Status process MDL data"
    
        def description(self):
            return "Status process MDL data"
            
        def width(self):
            return 11
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0400+$DE3ID"
            
        def startAddress(self):
            return 0x00000400
            
        def endAddress(self):
            return 0x00000417

        class _vld_page(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "vld_page"
            
            def description(self):
                return "valid page"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _sta_cnt_mess1s(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "sta_cnt_mess1s"
            
            def description(self):
                return "use for count MDL message to sent in 1s"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _sta_alrm_1s(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "sta_alrm_1s"
            
            def description(self):
                return "use for detecting new sending message"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cnt_mdl_lenmess(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_mdl_lenmess"
            
            def description(self):
                return "count number of byte in message"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["vld_page"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_txsta._vld_page()
            allFields["sta_cnt_mess1s"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_txsta._sta_cnt_mess1s()
            allFields["sta_alrm_1s"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_txsta._sta_alrm_1s()
            allFields["cnt_mdl_lenmess"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_txsta._cnt_mdl_lenmess()
            return allFields

    class _upen_sta_idle_alren(AtRegister.AtRegister):
        def name(self):
            return "SET TO HAVE PACKET MDL BUFFER 1"
    
        def description(self):
            return "SET/CLEAR to ALRM STATUS MESSAGE in BUFFER 1"
            
        def width(self):
            return 1
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0420+$DE3ID"
            
        def startAddress(self):
            return 0x00000420
            
        def endAddress(self):
            return 0x00000437

        class _idle_cfgen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "idle_cfgen"
            
            def description(self):
                return "(0) : engine clear for indication to have sent, (1) CPU set for indication to have new message which must send for buffer 0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["idle_cfgen"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_sta_idle_alren._idle_cfgen()
            return allFields

    class _upen_sta_tp_alren(AtRegister.AtRegister):
        def name(self):
            return "SET TO HAVE PACKET MDL BUFFER 2"
    
        def description(self):
            return "SET/CLEAR to ALRM STATUS MESSAGE in BUFFER 2"
            
        def width(self):
            return 1
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0440+$DE3ID"
            
        def startAddress(self):
            return 0x00000440
            
        def endAddress(self):
            return 0x00000457

        class _tp_cfgen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tp_cfgen"
            
            def description(self):
                return "(0) : engine clear for indication to have sent, (1) CPU set for indication to have new message which must send for buffer 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tp_cfgen"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_sta_tp_alren._tp_cfgen()
            return allFields

    class _upen_cfg_mdl(AtRegister.AtRegister):
        def name(self):
            return "CONFIG MDL Tx"
    
        def description(self):
            return "Config Tx MDL"
            
        def width(self):
            return 6
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0460+$DE3ID"
            
        def startAddress(self):
            return 0x00000460
            
        def endAddress(self):
            return 0x00000477

        class _reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "reserve"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_seq_tx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "cfg_seq_tx"
            
            def description(self):
                return "config enable Tx continous, (0) is disable, (1) is enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_entx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "cfg_entx"
            
            def description(self):
                return "config enable Tx, (0) is disable, (1) is enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_fcs_tx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "cfg_fcs_tx"
            
            def description(self):
                return "config mode transmit FCS, (1) is T.403-MSB, (0) is T.107-LSB"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_mdlstd_tx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfg_mdlstd_tx"
            
            def description(self):
                return "config standard Tx, (0) is ANSI, (1) is AT&T"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_mdl_cr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_mdl_cr"
            
            def description(self):
                return "config bit command/respond MDL message, bit 0 is channel 0 of DS3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["reserve"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_cfg_mdl._reserve()
            allFields["cfg_seq_tx"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_cfg_mdl._cfg_seq_tx()
            allFields["cfg_entx"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_cfg_mdl._cfg_entx()
            allFields["cfg_fcs_tx"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_cfg_mdl._cfg_fcs_tx()
            allFields["cfg_mdlstd_tx"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_cfg_mdl._cfg_mdlstd_tx()
            allFields["cfg_mdl_cr"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_cfg_mdl._cfg_mdl_cr()
            return allFields

    class _upen_txmdl_cntr2c_byteidle1(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE TX MDL IDLE R2C 1"
    
        def description(self):
            return "counter byte read to clear IDLE message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0700+ $DE3ID"
            
        def startAddress(self):
            return 0x00000700
            
        def endAddress(self):
            return 0x00000717

        class _cntr2c_byte_idle_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_byte_idle_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_byte_idle_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntr2c_byteidle1._cntr2c_byte_idle_mdl()
            return allFields

    class _upen_txmdl_cntro_byteidle1(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE TX MDL IDLE RO1"
    
        def description(self):
            return "counter byte read only IDLE message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0780+ $DE3ID"
            
        def startAddress(self):
            return 0x00000780
            
        def endAddress(self):
            return 0x00000797

        class _cntro_byte_idle_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntro_byte_idle_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntro_byte_idle_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntro_byteidle1._cntro_byte_idle_mdl()
            return allFields

    class _upen_txmdl_cntr2c_bytepath(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE TX MDL PATH R2C"
    
        def description(self):
            return "counter byte read to clear PATH message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0720+ $DE3ID"
            
        def startAddress(self):
            return 0x00000720
            
        def endAddress(self):
            return 0x00000737

        class _cntr2c_byte_path_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_byte_path_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_byte_path_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntr2c_bytepath._cntr2c_byte_path_mdl()
            return allFields

    class _upen_txmdl_cntro_bytepath(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE TX MDL PATH RO"
    
        def description(self):
            return "counter byte read only PATH message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x07A0+ $DE3ID"
            
        def startAddress(self):
            return 0x000007a0
            
        def endAddress(self):
            return 0x000007b7

        class _cntro_byte_path_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntro_byte_path_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntro_byte_path_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntro_bytepath._cntro_byte_path_mdl()
            return allFields

    class _upen_txmdl_cntr2c_bytetest(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE TX MDL TEST R2C"
    
        def description(self):
            return "counter byte read to clear TEST message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0740+ $DE3ID"
            
        def startAddress(self):
            return 0x00000740
            
        def endAddress(self):
            return 0x00000757

        class _cntr2c_byte_test_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_byte_test_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_byte_test_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntr2c_bytetest._cntr2c_byte_test_mdl()
            return allFields

    class _upen_txmdl_cntro_bytetest(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE TX MDL TEST RO"
    
        def description(self):
            return "counter byte read only TEST message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x07C0+ $DE3ID"
            
        def startAddress(self):
            return 0x000007c0
            
        def endAddress(self):
            return 0x000007d7

        class _cntro_byte_test_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntro_byte_test_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntro_byte_test_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntro_bytetest._cntro_byte_test_mdl()
            return allFields

    class _upen_txmdl_cntr2c_valididle(AtRegister.AtRegister):
        def name(self):
            return "COUNTER VALID MESSAGE TX MDL IDLE R2C"
    
        def description(self):
            return "counter valid read to clear IDLE message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0600+ $DE3ID"
            
        def startAddress(self):
            return 0x00000600
            
        def endAddress(self):
            return 0x00000617

        class _cntr2c_valid_idle_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_valid_idle_mdl"
            
            def description(self):
                return "value counter valid"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_valid_idle_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntr2c_valididle._cntr2c_valid_idle_mdl()
            return allFields

    class _upen_txmdl_cntro_valididle(AtRegister.AtRegister):
        def name(self):
            return "COUNTER VALID MESSAGE TX MDL IDLE RO"
    
        def description(self):
            return "counter valid read only IDLE message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0680+ $DE3ID"
            
        def startAddress(self):
            return 0x00000680
            
        def endAddress(self):
            return 0x00000697

        class _cntro_valid_idle_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntro_valid_idle_mdl"
            
            def description(self):
                return "value counter valid"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntro_valid_idle_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntro_valididle._cntro_valid_idle_mdl()
            return allFields

    class _upen_txmdl_cntr2c_validpath(AtRegister.AtRegister):
        def name(self):
            return "COUNTER VALID MESSAGE TX MDL PATH R2C"
    
        def description(self):
            return "counter valid read to clear PATH message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0620+ $DE3ID"
            
        def startAddress(self):
            return 0x00000620
            
        def endAddress(self):
            return 0x00000637

        class _cntr2c_valid_path_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_valid_path_mdl"
            
            def description(self):
                return "value counter valid"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_valid_path_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntr2c_validpath._cntr2c_valid_path_mdl()
            return allFields

    class _upen_txmdl_cntro_validpath(AtRegister.AtRegister):
        def name(self):
            return "COUNTER VALID MESSAGE TX MDL PATH RO"
    
        def description(self):
            return "counter valid read only PATH message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x06A0+ $DE3ID"
            
        def startAddress(self):
            return 0x000006a0
            
        def endAddress(self):
            return 0x000006b7

        class _cntro_valid_path_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntro_valid_path_mdl"
            
            def description(self):
                return "value counter valid"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntro_valid_path_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntro_validpath._cntro_valid_path_mdl()
            return allFields

    class _upen_txmdl_cntr2c_validtest(AtRegister.AtRegister):
        def name(self):
            return "COUNTER VALID MESSAGE TX MDL TEST R2C"
    
        def description(self):
            return "counter valid read to clear TEST message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0640+ $DE3ID"
            
        def startAddress(self):
            return 0x00000640
            
        def endAddress(self):
            return 0x00000657

        class _cntr2c_valid_test_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_valid_test_mdl"
            
            def description(self):
                return "value counter valid"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_valid_test_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntr2c_validtest._cntr2c_valid_test_mdl()
            return allFields

    class _upen_txmdl_cntro_validtest(AtRegister.AtRegister):
        def name(self):
            return "COUNTER VALID MESSAGE TX MDL TEST RO"
    
        def description(self):
            return "counter valid read only TEST message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x06C0+ $DE3ID"
            
        def startAddress(self):
            return 0x000006c0
            
        def endAddress(self):
            return 0x000006d7

        class _cntro_valid_test_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntro_valid_test_mdl"
            
            def description(self):
                return "value counter valid"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntro_valid_test_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntro_validtest._cntro_valid_test_mdl()
            return allFields

    class _upen_mdl_idle1_2(AtRegister.AtRegister):
        def name(self):
            return "Buff Message MDL IDLE1_2"
    
        def description(self):
            return "config message MDL IDLE Channel ID 0-15"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0800+ $DWORDID*16 + $DE3ID1"
            
        def startAddress(self):
            return 0x00000800
            
        def endAddress(self):
            return 0x0000092f

        class _idle_byte13_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "idle_byte13_2"
            
            def description(self):
                return "MS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _ilde_byte12_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ilde_byte12_2"
            
            def description(self):
                return "BYTE 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _idle_byte11_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "idle_byte11_2"
            
            def description(self):
                return "BYTE 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _idle_byte10_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "idle_byte10_2"
            
            def description(self):
                return "LS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["idle_byte13_2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_idle1_2._idle_byte13_2()
            allFields["ilde_byte12_2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_idle1_2._ilde_byte12_2()
            allFields["idle_byte11_2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_idle1_2._idle_byte11_2()
            allFields["idle_byte10_2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_idle1_2._idle_byte10_2()
            return allFields

    class _upen_mdl_idle2_2(AtRegister.AtRegister):
        def name(self):
            return "Buff Message MDL IDLE2_2"
    
        def description(self):
            return "config message MDL IDLE Channel ID 16-23"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0930+ $DWORDID*8 + $DE3ID2"
            
        def startAddress(self):
            return 0x00000930
            
        def endAddress(self):
            return 0x000009c7

        class _idle_byte23_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "idle_byte23_2"
            
            def description(self):
                return "MS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _ilde_byte22_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ilde_byte22_2"
            
            def description(self):
                return "BYTE 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _idle_byte21_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "idle_byte21_2"
            
            def description(self):
                return "BYTE 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _idle_byte20_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "idle_byte20_2"
            
            def description(self):
                return "LS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["idle_byte23_2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_idle2_2._idle_byte23_2()
            allFields["ilde_byte22_2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_idle2_2._ilde_byte22_2()
            allFields["idle_byte21_2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_idle2_2._idle_byte21_2()
            allFields["idle_byte20_2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_idle2_2._idle_byte20_2()
            return allFields

    class _upen_mdl_tp1_2(AtRegister.AtRegister):
        def name(self):
            return "Config Buff Message MDL TESTPATH1_2"
    
        def description(self):
            return "config message MDL TESTPATH Channel ID 0-15"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0A00+$DE3ID1 + $DWORDID*16"
            
        def startAddress(self):
            return 0x00000a00
            
        def endAddress(self):
            return 0x00000b2f

        class _tp_byte13_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "tp_byte13_2"
            
            def description(self):
                return "MS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tp_byte12_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "tp_byte12_2"
            
            def description(self):
                return "BYTE 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tp_byte11_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "tp_byte11_2"
            
            def description(self):
                return "BYTE 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tp_byte10_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tp_byte10_2"
            
            def description(self):
                return "LS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tp_byte13_2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_tp1_2._tp_byte13_2()
            allFields["tp_byte12_2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_tp1_2._tp_byte12_2()
            allFields["tp_byte11_2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_tp1_2._tp_byte11_2()
            allFields["tp_byte10_2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_tp1_2._tp_byte10_2()
            return allFields

    class _upen_mdl_tp2_2(AtRegister.AtRegister):
        def name(self):
            return "Config Buff Message MDL TESTPATH2_2"
    
        def description(self):
            return "config message MDL TESTPATH Channel ID 16-23"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0B30+ $DWORDID*8+ $DE3ID2"
            
        def startAddress(self):
            return 0x00000b30
            
        def endAddress(self):
            return 0x00000bc7

        class _tp_byte23_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "tp_byte23_2"
            
            def description(self):
                return "MS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tp_byte22_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "tp_byte22_2"
            
            def description(self):
                return "BYTE 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tp_byte21_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "tp_byte21_2"
            
            def description(self):
                return "BYTE 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tp_byte20_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tp_byte20_2"
            
            def description(self):
                return "LS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tp_byte23_2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_tp2_2._tp_byte23_2()
            allFields["tp_byte22_2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_tp2_2._tp_byte22_2()
            allFields["tp_byte21_2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_tp2_2._tp_byte21_2()
            allFields["tp_byte20_2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_tp2_2._tp_byte20_2()
            return allFields

    class _upen_mdl_txsta_2(AtRegister.AtRegister):
        def name(self):
            return "Status process MDL data_2"
    
        def description(self):
            return "Status process MDL data"
            
        def width(self):
            return 11
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0C00+$DE3ID"
            
        def startAddress(self):
            return 0x00000c00
            
        def endAddress(self):
            return 0x00000c17

        class _vld_page_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "vld_page_2"
            
            def description(self):
                return "valid page"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _sta_cnt_mess1s_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "sta_cnt_mess1s_2"
            
            def description(self):
                return "use for count MDL message to sent in 1s"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _sta_alrm_1s_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "sta_alrm_1s_2"
            
            def description(self):
                return "use for detecting new sending message"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cnt_mdl_lenmess_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_mdl_lenmess_2"
            
            def description(self):
                return "count number of byte in message"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["vld_page_2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_txsta_2._vld_page_2()
            allFields["sta_cnt_mess1s_2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_txsta_2._sta_cnt_mess1s_2()
            allFields["sta_alrm_1s_2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_txsta_2._sta_alrm_1s_2()
            allFields["cnt_mdl_lenmess_2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_txsta_2._cnt_mdl_lenmess_2()
            return allFields

    class _upen_sta_idle_alren_2(AtRegister.AtRegister):
        def name(self):
            return "SET TO HAVE PACKET MDL BUFFER 1_2"
    
        def description(self):
            return "SET/CLEAR to ALRM STATUS MESSAGE in BUFFER 1"
            
        def width(self):
            return 1
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0C20+$DE3ID"
            
        def startAddress(self):
            return 0x00000c20
            
        def endAddress(self):
            return 0x00000c37

        class _idle_cfgen_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "idle_cfgen_2"
            
            def description(self):
                return "(0) : engine clear for indication to have sent, (1) CPU set for indication to have new message which must send for buffer 0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["idle_cfgen_2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_sta_idle_alren_2._idle_cfgen_2()
            return allFields

    class _upen_sta_tp_alren_2(AtRegister.AtRegister):
        def name(self):
            return "SET TO HAVE PACKET MDL BUFFER 2_2"
    
        def description(self):
            return "SET/CLEAR to ALRM STATUS MESSAGE in BUFFER 2"
            
        def width(self):
            return 1
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0C40+$DE3ID"
            
        def startAddress(self):
            return 0x00000c40
            
        def endAddress(self):
            return 0x00000c57

        class _tp_cfgen_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tp_cfgen_2"
            
            def description(self):
                return "(0) : engine clear for indication to have sent, (1) CPU set for indication to have new message which must send for buffer 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tp_cfgen_2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_sta_tp_alren_2._tp_cfgen_2()
            return allFields

    class _upen_cfg_mdl_2(AtRegister.AtRegister):
        def name(self):
            return "CONFIG MDL Tx_2"
    
        def description(self):
            return "Config Tx MDL"
            
        def width(self):
            return 6
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0C60+$DE3ID"
            
        def startAddress(self):
            return 0x00000c60
            
        def endAddress(self):
            return 0x00000c77

        class _reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "reserve"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_seq_txen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "cfg_seq_txen"
            
            def description(self):
                return "config enable Tx continous, (0) is disable, (1) is enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_entx_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "cfg_entx_2"
            
            def description(self):
                return "config enable Tx, (0) is disable, (1) is enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_fcs_tx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "cfg_fcs_tx"
            
            def description(self):
                return "config mode transmit FCS, (1) is T.403-MSB, (0) is T.107-LSB"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_mdlstd_tx_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfg_mdlstd_tx_2"
            
            def description(self):
                return "config standard Tx, (0) is ANSI, (1) is AT&T"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_mdl_cr_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_mdl_cr_2"
            
            def description(self):
                return "config bit command/respond MDL message, bit 0 is channel 0 of DS3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["reserve"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_cfg_mdl_2._reserve()
            allFields["cfg_seq_txen"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_cfg_mdl_2._cfg_seq_txen()
            allFields["cfg_entx_2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_cfg_mdl_2._cfg_entx_2()
            allFields["cfg_fcs_tx"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_cfg_mdl_2._cfg_fcs_tx()
            allFields["cfg_mdlstd_tx_2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_cfg_mdl_2._cfg_mdlstd_tx_2()
            allFields["cfg_mdl_cr_2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_cfg_mdl_2._cfg_mdl_cr_2()
            return allFields

    class _upen_txmdl_cntr2c_byteidle2(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE TX MDL IDLE R2C2"
    
        def description(self):
            return "counter byte read to clear IDLE message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0F00+ $DE3ID"
            
        def startAddress(self):
            return 0x00000f00
            
        def endAddress(self):
            return 0x00000f17

        class _cntr2c_byte_idle_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_byte_idle_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_byte_idle_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntr2c_byteidle2._cntr2c_byte_idle_mdl()
            return allFields

    class _upen_txmdl_cntro_byteidle2(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE TX MDL IDLE RO2"
    
        def description(self):
            return "counter byte read only IDLE message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0F80+ $DE3ID"
            
        def startAddress(self):
            return 0x00000f80
            
        def endAddress(self):
            return 0x00000f97

        class _cntro_byte_idle_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntro_byte_idle_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntro_byte_idle_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntro_byteidle2._cntro_byte_idle_mdl()
            return allFields

    class _upen_txmdl_cntr2c_bytepath2(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE TX MDL PATH R2C2"
    
        def description(self):
            return "counter byte read to clear PATH message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0F20+ $DE3ID"
            
        def startAddress(self):
            return 0x00000f20
            
        def endAddress(self):
            return 0x00000f37

        class _cntr2c_byte_path_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_byte_path_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_byte_path_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntr2c_bytepath2._cntr2c_byte_path_mdl()
            return allFields

    class _upen_txmdl_cntro_bytepath2(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE TX MDL PATH RO2"
    
        def description(self):
            return "counter byte read only PATH message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0FA0+ $DE3ID"
            
        def startAddress(self):
            return 0x00000fa0
            
        def endAddress(self):
            return 0x00000fb7

        class _cntro_byte_path_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntro_byte_path_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntro_byte_path_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntro_bytepath2._cntro_byte_path_mdl()
            return allFields

    class _upen_txmdl_cntr2c_bytetest2(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE TX MDL TEST R2C2"
    
        def description(self):
            return "counter byte read to clear TEST message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0F40+ $DE3ID"
            
        def startAddress(self):
            return 0x00000f40
            
        def endAddress(self):
            return 0x00000f57

        class _cntr2c_byte_test_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_byte_test_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_byte_test_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntr2c_bytetest2._cntr2c_byte_test_mdl()
            return allFields

    class _upen_txmdl_cntro_bytetest2(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE TX MDL TEST RO2"
    
        def description(self):
            return "counter byte read only TEST message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0FC0+ $DE3ID"
            
        def startAddress(self):
            return 0x00000fc0
            
        def endAddress(self):
            return 0x00000fd7

        class _cntro_byte_test_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntro_byte_test_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntro_byte_test_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntro_bytetest2._cntro_byte_test_mdl()
            return allFields

    class _upen_txmdl_cntr2c_valididle2(AtRegister.AtRegister):
        def name(self):
            return "COUNTER VALID MESSAGE TX MDL IDLE R2C2"
    
        def description(self):
            return "counter valid read to clear IDLE message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0E00+ $DE3ID"
            
        def startAddress(self):
            return 0x00000e00
            
        def endAddress(self):
            return 0x00000e17

        class _cntr2c_valid_idle_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_valid_idle_mdl"
            
            def description(self):
                return "value counter valid"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_valid_idle_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntr2c_valididle2._cntr2c_valid_idle_mdl()
            return allFields

    class _upen_txmdl_cntro_valididle2(AtRegister.AtRegister):
        def name(self):
            return "COUNTER VALID MESSAGE TX MDL IDLE RO2"
    
        def description(self):
            return "counter valid read only IDLE message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0E80+ $DE3ID"
            
        def startAddress(self):
            return 0x00000e80
            
        def endAddress(self):
            return 0x00000e97

        class _cntro_valid_idle_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntro_valid_idle_mdl"
            
            def description(self):
                return "value counter valid"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntro_valid_idle_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntro_valididle2._cntro_valid_idle_mdl()
            return allFields

    class _upen_txmdl_cntr2c_validpath2(AtRegister.AtRegister):
        def name(self):
            return "COUNTER VALID MESSAGE TX MDL PATH R2C2"
    
        def description(self):
            return "counter valid read to clear PATH message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0E20+ $DE3ID"
            
        def startAddress(self):
            return 0x00000e20
            
        def endAddress(self):
            return 0x00000e37

        class _cntr2c_valid_path_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_valid_path_mdl"
            
            def description(self):
                return "value counter valid"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_valid_path_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntr2c_validpath2._cntr2c_valid_path_mdl()
            return allFields

    class _upen_txmdl_cntro_validpath2(AtRegister.AtRegister):
        def name(self):
            return "COUNTER VALID MESSAGE TX MDL PATH RO2"
    
        def description(self):
            return "counter valid read only PATH message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0EA0+ $DE3ID"
            
        def startAddress(self):
            return 0x00000ea0
            
        def endAddress(self):
            return 0x00000eb7

        class _cntro_valid_path_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntro_valid_path_mdl"
            
            def description(self):
                return "value counter valid"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntro_valid_path_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntro_validpath2._cntro_valid_path_mdl()
            return allFields

    class _upen_txmdl_cntr2c_validtest2(AtRegister.AtRegister):
        def name(self):
            return "COUNTER VALID MESSAGE TX MDL TEST R2C2"
    
        def description(self):
            return "counter valid read to clear TEST message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0E40+ $DE3ID"
            
        def startAddress(self):
            return 0x00000e40
            
        def endAddress(self):
            return 0x00000e57

        class _cntr2c_valid_test_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_valid_test_mdl"
            
            def description(self):
                return "value counter valid"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_valid_test_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntr2c_validtest2._cntr2c_valid_test_mdl()
            return allFields

    class _upen_txmdl_cntro_validtest2(AtRegister.AtRegister):
        def name(self):
            return "COUNTER VALID MESSAGE TX MDL TEST RO2"
    
        def description(self):
            return "counter valid read only TEST message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0EC0+ $DE3ID"
            
        def startAddress(self):
            return 0x00000ec0
            
        def endAddress(self):
            return 0x00000ed7

        class _cntro_valid_test_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntro_valid_test_mdl"
            
            def description(self):
                return "value counter valid"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntro_valid_test_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_txmdl_cntro_validtest2._cntro_valid_test_mdl()
            return allFields

    class _upen_rxprm_cfgstd(AtRegister.AtRegister):
        def name(self):
            return "Config Buff Message PRM STANDARD"
    
        def description(self):
            return "config standard message PRM"
            
        def width(self):
            return 3
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x08000+$STSID[4:3]*7 + $VTGID*64 + $SLICEID*32 + $STSID[2:0]*4 + $VTID"
            
        def startAddress(self):
            return 0x00008000
            
        def endAddress(self):
            return 0x0000853f

        class _cfg_prm_cr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "cfg_prm_cr"
            
            def description(self):
                return "config C/R bit expected"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_prm_rxenstuff(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfg_prm_rxenstuff"
            
            def description(self):
                return "enable destuff   (0) is disable, (1) is enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_std_prm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_std_prm"
            
            def description(self):
                return "config standard   (0) is ANSI, (1) is AT&T"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_prm_cr"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxprm_cfgstd._cfg_prm_cr()
            allFields["cfg_prm_rxenstuff"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxprm_cfgstd._cfg_prm_rxenstuff()
            allFields["cfg_std_prm"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxprm_cfgstd._cfg_std_prm()
            return allFields

    class _upen_rxmdl_typebuff1(AtRegister.AtRegister):
        def name(self):
            return "Buff Message MDL with configuration type1"
    
        def description(self):
            return "buffer message MDL which is configured type"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9800 + $STSID* 64 + $SLICEID*32 + $RXDWORDID"
            
        def startAddress(self):
            return 0x00009800
            
        def endAddress(self):
            return 0x00009bff

        class _mdl_tbyte3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "mdl_tbyte3"
            
            def description(self):
                return "MS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _mdl_tbyte2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "mdl_tbyte2"
            
            def description(self):
                return "BYTE 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _mdl_tbyte1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "mdl_tbyte1"
            
            def description(self):
                return "BYTE 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _mdl_tbyte0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdl_tbyte0"
            
            def description(self):
                return "LS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdl_tbyte3"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_typebuff1._mdl_tbyte3()
            allFields["mdl_tbyte2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_typebuff1._mdl_tbyte2()
            allFields["mdl_tbyte1"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_typebuff1._mdl_tbyte1()
            allFields["mdl_tbyte0"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_typebuff1._mdl_tbyte0()
            return allFields

    class _upen_rxmdl_cfgtype(AtRegister.AtRegister):
        def name(self):
            return "Config Buff Message MDL TYPE"
    
        def description(self):
            return "config type message MDL"
            
        def width(self):
            return 6
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0A000+$MDLID"
            
        def startAddress(self):
            return 0x0000a000
            
        def endAddress(self):
            return 0x0000a02f

        class _cfg_fcs_rx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "cfg_fcs_rx"
            
            def description(self):
                return "config mode receive FCS, (1) is T.403-MSB, (0) is T.107-LSB"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_mdl_cr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "cfg_mdl_cr"
            
            def description(self):
                return "config C/R bit expected"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_mdl_std(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "cfg_mdl_std"
            
            def description(self):
                return "(0) is ANSI, (1) is AT&T"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_mdl_mask_test(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "cfg_mdl_mask_test"
            
            def description(self):
                return "config enable mask to moitor test massage (1): enable, (0): disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_mdl_mask_path(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfg_mdl_mask_path"
            
            def description(self):
                return "config enable mask to moitor path massage (1): enable, (0): disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_mdl_mask_idle(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_mdl_mask_idle"
            
            def description(self):
                return "config enable mask to moitor idle massage (1): enable, (0): disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_fcs_rx"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cfgtype._cfg_fcs_rx()
            allFields["cfg_mdl_cr"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cfgtype._cfg_mdl_cr()
            allFields["cfg_mdl_std"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cfgtype._cfg_mdl_std()
            allFields["cfg_mdl_mask_test"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cfgtype._cfg_mdl_mask_test()
            allFields["cfg_mdl_mask_path"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cfgtype._cfg_mdl_mask_path()
            allFields["cfg_mdl_mask_idle"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cfgtype._cfg_mdl_mask_idle()
            return allFields

    class _upen_destuff_ctrl0(AtRegister.AtRegister):
        def name(self):
            return "CONFIG CONTROL RX DESTUFF 0"
    
        def description(self):
            return "config control DeStuff global"
            
        def width(self):
            return 6
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000a041
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_crmon(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "cfg_crmon"
            
            def description(self):
                return "(0) : disable, (1) : enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _reserve1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "reserve1"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fcsmon(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "fcsmon"
            
            def description(self):
                return "(0) : disable monitor, (1) : enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 1
        
            def name(self):
                return "reserve"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _headermon_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "headermon_en"
            
            def description(self):
                return "(1) : enable monitor, (0) disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_crmon"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_destuff_ctrl0._cfg_crmon()
            allFields["reserve1"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_destuff_ctrl0._reserve1()
            allFields["fcsmon"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_destuff_ctrl0._fcsmon()
            allFields["reserve"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_destuff_ctrl0._reserve()
            allFields["headermon_en"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_destuff_ctrl0._headermon_en()
            return allFields

    class _upen_mdl_stk_cfg1(AtRegister.AtRegister):
        def name(self):
            return "STICKY TO RECEIVE PACKET MDL BUFF CONFIG1"
    
        def description(self):
            return "sticky row for buffer of type message is configured 0-31"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000a045
            
        def endAddress(self):
            return 0xffffffff

        class _stk_cfg_alr1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "stk_cfg_alr1"
            
            def description(self):
                return "sticky for buffer of type message is configured 0-31"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["stk_cfg_alr1"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_stk_cfg1._stk_cfg_alr1()
            return allFields

    class _upen_mdl_stk_cfg2(AtRegister.AtRegister):
        def name(self):
            return "STICKY TO RECEIVE PACKET MDL BUFF CONFIG 2"
    
        def description(self):
            return ""
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000a046
            
        def endAddress(self):
            return 0xffffffff

        class _stk_cfg_alr2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "stk_cfg_alr2"
            
            def description(self):
                return "sticky for buffer of type message is configured 32-47"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["stk_cfg_alr2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_mdl_stk_cfg2._stk_cfg_alr2()
            return allFields

    class _upen_rxprm_gmess(AtRegister.AtRegister):
        def name(self):
            return "COUNTER GOOD MESSAGE RX PRM"
    
        def description(self):
            return "counter good message PRM"
            
        def width(self):
            return 15
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xC000+$STSID[4:3]*7 + $VTGID*64 + $SLICEID*32 + $STSID[2:0]*4 + $VTID + $UPRO * 2048 "
            
        def startAddress(self):
            return 0x0000c000
            
        def endAddress(self):
            return 0x0000cd3f

        class _cnt_gmess_prm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_gmess_prm"
            
            def description(self):
                return "value counter packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_gmess_prm"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxprm_gmess._cnt_gmess_prm()
            return allFields

    class _upen_rxprm_drmess(AtRegister.AtRegister):
        def name(self):
            return "COUNTER DROP MESSAGE RX PRM"
    
        def description(self):
            return "counter drop message PRM"
            
        def width(self):
            return 15
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xD000+$STSID[4:3]*7 + $VTGID*64 + $SLICEID*32 + $STSID[2:0]*4 + $VTID + $UPRO * 2048 "
            
        def startAddress(self):
            return 0x0000d000
            
        def endAddress(self):
            return 0x0000dd3f

        class _cnt_drmess_prm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_drmess_prm"
            
            def description(self):
                return "value counter packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_drmess_prm"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxprm_drmess._cnt_drmess_prm()
            return allFields

    class _upen_rxprm_mmess(AtRegister.AtRegister):
        def name(self):
            return "COUNTER MISS MESSAGE RX PRM"
    
        def description(self):
            return "counter miss message PRM"
            
        def width(self):
            return 15
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xE000+$STSID[4:3]*7 + $VTGID*64 + $SLICEID*32 + $STSID[2:0]*4 + $VTID + $UPRO * 2048 "
            
        def startAddress(self):
            return 0x0000e000
            
        def endAddress(self):
            return 0x0000ed3f

        class _cnt_mmess_prm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_mmess_prm"
            
            def description(self):
                return "value counter packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_mmess_prm"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxprm_mmess._cnt_mmess_prm()
            return allFields

    class _upen_fcs_stk1(AtRegister.AtRegister):
        def name(self):
            return "STICKY TO RECEIVE FCS MESSAGE MDL 1"
    
        def description(self):
            return "STICKY TO RECEIVE FCS PACKET MDL 0-31"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000a049
            
        def endAddress(self):
            return 0xffffffff

        class _stk_mdl_fcs1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "stk_mdl_fcs1"
            
            def description(self):
                return "sticky FCS message channel 0-31"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["stk_mdl_fcs1"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_fcs_stk1._stk_mdl_fcs1()
            return allFields

    class _upen_fcs_stk2(AtRegister.AtRegister):
        def name(self):
            return "STICKY TO RECEIVE FCS MESSAGE MDL 2"
    
        def description(self):
            return "STICKY TO RECEIVE FCS PACKET MDL 32-47"
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000a04a
            
        def endAddress(self):
            return 0xffffffff

        class _stk_mdl_fcs2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "stk_mdl_fcs2"
            
            def description(self):
                return "sticky FCS message channel 32-47"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["stk_mdl_fcs2"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_fcs_stk2._stk_mdl_fcs2()
            return allFields

    class _upen_rxprm_cnt_byte(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE RX PRM"
    
        def description(self):
            return "counter byte message PRM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xF000+$STSID[4:3]*7 + $VTGID*64 + $SLICEID*32 + $STSID[2:0]*4 + $VTID + $UPRO * 2048 "
            
        def startAddress(self):
            return 0x0000f000
            
        def endAddress(self):
            return 0x0000fd3f

        class _cnt_byte_rxprm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_byte_rxprm"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_byte_rxprm"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxprm_cnt_byte._cnt_byte_rxprm()
            return allFields

    class _upen_rxmdl_cntr2c_byteidle(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE RX MDL IDLE R2C"
    
        def description(self):
            return "counter byte read to clear IDLE message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xB400+ $MDLID"
            
        def startAddress(self):
            return 0x0000b400
            
        def endAddress(self):
            return 0x0000b42f

        class _cntr2c_byte_idle_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_byte_idle_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_byte_idle_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntr2c_byteidle._cntr2c_byte_idle_mdl()
            return allFields

    class _upen_rxmdl_cntro_byteidle(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE RX MDL IDLE RO"
    
        def description(self):
            return "counter byte read only IDLE message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xB500+ $MDLID"
            
        def startAddress(self):
            return 0x0000b500
            
        def endAddress(self):
            return 0x0000b52f

        class _cntro_byte_idle_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntro_byte_idle_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntro_byte_idle_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntro_byteidle._cntro_byte_idle_mdl()
            return allFields

    class _upen_rxmdl_cntr2c_bytepath(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE RX MDL PATH R2C"
    
        def description(self):
            return "counter byte read to clear PATH message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xB440+ $MDLID"
            
        def startAddress(self):
            return 0x0000b440
            
        def endAddress(self):
            return 0x0000b46f

        class _cntr2c_byte_path_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_byte_path_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_byte_path_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntr2c_bytepath._cntr2c_byte_path_mdl()
            return allFields

    class _upen_rxmdl_cntro_bytepath(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE RX MDL PATH RO"
    
        def description(self):
            return "counter byte read only PATH message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xB540+ $MDLID"
            
        def startAddress(self):
            return 0x0000b540
            
        def endAddress(self):
            return 0x0000b56f

        class _cntro_byte_path_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntro_byte_path_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntro_byte_path_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntro_bytepath._cntro_byte_path_mdl()
            return allFields

    class _upen_rxmdl_cntr2c_bytetest(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE RX MDL TEST R2C"
    
        def description(self):
            return "counter byte read to clear TEST message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xB480+ $MDLID"
            
        def startAddress(self):
            return 0x0000b480
            
        def endAddress(self):
            return 0x0000b4af

        class _cntr2c_byte_test_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_byte_test_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_byte_test_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntr2c_bytetest._cntr2c_byte_test_mdl()
            return allFields

    class _upen_rxmdl_cntro_bytetest(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE RX MDL TEST RO"
    
        def description(self):
            return "counter byte read only TEST message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xB580+ $MDLID"
            
        def startAddress(self):
            return 0x0000b580
            
        def endAddress(self):
            return 0x0000b5af

        class _cntro_byte_test_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntro_byte_test_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntro_byte_test_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntro_bytetest._cntro_byte_test_mdl()
            return allFields

    class _upen_rxmdl_cntr2c_goodidle(AtRegister.AtRegister):
        def name(self):
            return "COUNTER GOOD MESSAGE RX MDL IDLE R2C"
    
        def description(self):
            return "counter good read to clear IDLE message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xB000+ $MDLID"
            
        def startAddress(self):
            return 0x0000b000
            
        def endAddress(self):
            return 0x0000b02f

        class _cntr2c_good_idle_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_good_idle_mdl"
            
            def description(self):
                return "value counter good"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_good_idle_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntr2c_goodidle._cntr2c_good_idle_mdl()
            return allFields

    class _upen_rxmdl_cntro_goodidle(AtRegister.AtRegister):
        def name(self):
            return "COUNTER GOOD MESSAGE RX MDL IDLE RO"
    
        def description(self):
            return "counter good read only IDLE message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xB200+ $MDLID"
            
        def startAddress(self):
            return 0x0000b200
            
        def endAddress(self):
            return 0x0000b22f

        class _cntro_good_idle_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntro_good_idle_mdl"
            
            def description(self):
                return "value counter good"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntro_good_idle_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntro_goodidle._cntro_good_idle_mdl()
            return allFields

    class _upen_rxmdl_cntr2c_goodpath(AtRegister.AtRegister):
        def name(self):
            return "COUNTER GOOD MESSAGE RX MDL PATH R2C"
    
        def description(self):
            return "counter good read to clear PATH message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xB040+ $MDLID"
            
        def startAddress(self):
            return 0x0000b040
            
        def endAddress(self):
            return 0x0000b06f

        class _cntr2c_good_path_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_good_path_mdl"
            
            def description(self):
                return "value counter good"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_good_path_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntr2c_goodpath._cntr2c_good_path_mdl()
            return allFields

    class _upen_rxmdl_cntro_goodpath(AtRegister.AtRegister):
        def name(self):
            return "COUNTER GOOD MESSAGE RX MDL PATH RO"
    
        def description(self):
            return "counter good read only PATH message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xB240+ $MDLID"
            
        def startAddress(self):
            return 0x0000b240
            
        def endAddress(self):
            return 0x0000b26f

        class _cntro_good_path_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntro_good_path_mdl"
            
            def description(self):
                return "value counter good"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntro_good_path_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntro_goodpath._cntro_good_path_mdl()
            return allFields

    class _upen_rxmdl_cntr2c_goodtest(AtRegister.AtRegister):
        def name(self):
            return "COUNTER GOOD MESSAGE RX MDL TEST R2C"
    
        def description(self):
            return "counter good read to clear TEST message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xB080+ $MDLID"
            
        def startAddress(self):
            return 0x0000b080
            
        def endAddress(self):
            return 0x0000b0af

        class _cntr2c_good_test_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_good_test_mdl"
            
            def description(self):
                return "value counter good"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_good_test_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntr2c_goodtest._cntr2c_good_test_mdl()
            return allFields

    class _upen_rxmdl_cntro_goodtest(AtRegister.AtRegister):
        def name(self):
            return "COUNTER GOOD MESSAGE RX MDL TEST RO"
    
        def description(self):
            return "counter good read only TEST message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xB280+ $MDLID"
            
        def startAddress(self):
            return 0x0000b280
            
        def endAddress(self):
            return 0x0000b2af

        class _cntro_good_test_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntro_good_test_mdl"
            
            def description(self):
                return "value counter good"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntro_good_test_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntro_goodtest._cntro_good_test_mdl()
            return allFields

    class _upen_rxmdl_cntr2c_dropidle(AtRegister.AtRegister):
        def name(self):
            return "COUNTER DROP MESSAGE RX MDL IDLE R2C"
    
        def description(self):
            return "counter drop read to clear IDLE message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xB0C0+ $MDLID"
            
        def startAddress(self):
            return 0x0000b0c0
            
        def endAddress(self):
            return 0x0000b0ef

        class _cntr2c_drop_idle_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_drop_idle_mdl"
            
            def description(self):
                return "value counter drop"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_drop_idle_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntr2c_dropidle._cntr2c_drop_idle_mdl()
            return allFields

    class _upen_rxmdl_cntro_dropidle(AtRegister.AtRegister):
        def name(self):
            return "COUNTER DROP MESSAGE RX MDL IDLE RO"
    
        def description(self):
            return "counter drop read only IDLE message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xB2C0+ $MDLID"
            
        def startAddress(self):
            return 0x0000b2c0
            
        def endAddress(self):
            return 0x0000b2ef

        class _cntro_drop_idle_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntro_drop_idle_mdl"
            
            def description(self):
                return "value counter drop"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntro_drop_idle_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntro_dropidle._cntro_drop_idle_mdl()
            return allFields

    class _upen_rxmdl_cntr2c_droppath(AtRegister.AtRegister):
        def name(self):
            return "COUNTER DROP MESSAGE RX MDL PATH R2C"
    
        def description(self):
            return "counter drop read to clear PATH message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xB100+ $MDLID"
            
        def startAddress(self):
            return 0x0000b100
            
        def endAddress(self):
            return 0x0000b12f

        class _cntr2c_drop_path_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_drop_path_mdl"
            
            def description(self):
                return "value counter drop"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_drop_path_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntr2c_droppath._cntr2c_drop_path_mdl()
            return allFields

    class _upen_rxmdl_cntro_droppath(AtRegister.AtRegister):
        def name(self):
            return "COUNTER DROP MESSAGE RX MDL PATH RO"
    
        def description(self):
            return "counter drop read only PATH message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xB300+ $MDLID"
            
        def startAddress(self):
            return 0x0000b300
            
        def endAddress(self):
            return 0x0000b32f

        class _cntro_drop_path_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntro_drop_path_mdl"
            
            def description(self):
                return "value counter drop"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntro_drop_path_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntro_droppath._cntro_drop_path_mdl()
            return allFields

    class _upen_rxmdl_cntr2c_droptest(AtRegister.AtRegister):
        def name(self):
            return "COUNTER DROP MESSAGE RX MDL TEST R2C"
    
        def description(self):
            return "counter drop read to clear TEST message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xB140+ $MDLID"
            
        def startAddress(self):
            return 0x0000b140
            
        def endAddress(self):
            return 0x0000b16f

        class _cntr2c_drop_test_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_drop_test_mdl"
            
            def description(self):
                return "value counter drop"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_drop_test_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntr2c_droptest._cntr2c_drop_test_mdl()
            return allFields

    class _upen_rxmdl_cntro_droptest(AtRegister.AtRegister):
        def name(self):
            return "COUNTER DROP MESSAGE RX MDL TEST RO"
    
        def description(self):
            return "counter drop read only TEST message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xB340+ $MDLID"
            
        def startAddress(self):
            return 0x0000b340
            
        def endAddress(self):
            return 0x0000b36f

        class _cntro_drop_test_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntro_drop_test_mdl"
            
            def description(self):
                return "value counter drop"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntro_drop_test_mdl"] = _AF6CCI0031_RD_PDH_MDLPRM._upen_rxmdl_cntro_droptest._cntro_drop_test_mdl()
            return allFields

    class _prm_cfg_lben_int(AtRegister.AtRegister):
        def name(self):
            return "PRM LB per Channel Interrupt Enable Control"
    
        def description(self):
            return "This is the per Channel interrupt enable"
            
        def width(self):
            return 2
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1A000+ $STSID*32 + $VTGID*4 + $VTID"
            
        def startAddress(self):
            return 0x0001a000
            
        def endAddress(self):
            return 0x0001a3ff

        class _prm_cfglben_int1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "prm_cfglben_int1"
            
            def description(self):
                return "Set 1 to enable change event to generate an interrupt slice 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _prm_cfglben_int0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "prm_cfglben_int0"
            
            def description(self):
                return "Set 1 to enable change event to generate an interrupt slice 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prm_cfglben_int1"] = _AF6CCI0031_RD_PDH_MDLPRM._prm_cfg_lben_int._prm_cfglben_int1()
            allFields["prm_cfglben_int0"] = _AF6CCI0031_RD_PDH_MDLPRM._prm_cfg_lben_int._prm_cfglben_int0()
            return allFields

    class _prm_lb_int_sta(AtRegister.AtRegister):
        def name(self):
            return "PRM LB Interrupt per Channel Interrupt Status"
    
        def description(self):
            return "This is the per Channel interrupt status."
            
        def width(self):
            return 2
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x1A400+ $STSID*32 + $VTGID*4 + $VTID"
            
        def startAddress(self):
            return 0x0001a400
            
        def endAddress(self):
            return 0x0001a7ff

        class _prm_lbint_sta1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "prm_lbint_sta1"
            
            def description(self):
                return "Set 1 if there is a change event slice 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _prm_lbint_sta0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "prm_lbint_sta0"
            
            def description(self):
                return "Set 1 if there is a change event slice 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prm_lbint_sta1"] = _AF6CCI0031_RD_PDH_MDLPRM._prm_lb_int_sta._prm_lbint_sta1()
            allFields["prm_lbint_sta0"] = _AF6CCI0031_RD_PDH_MDLPRM._prm_lb_int_sta._prm_lbint_sta0()
            return allFields

    class _prm_lb_int_crrsta(AtRegister.AtRegister):
        def name(self):
            return "PRM LB Interrupt per Channel Current Status"
    
        def description(self):
            return "This is the per Channel Current status."
            
        def width(self):
            return 2
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x1A800+ $STSID*32 + $VTGID*4 + $VTID"
            
        def startAddress(self):
            return 0x0001a800
            
        def endAddress(self):
            return 0x0001abff

        class _prm_lbint_crrsta1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "prm_lbint_crrsta1"
            
            def description(self):
                return "Current status of event slice 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _prm_lbint_crrsta0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "prm_lbint_crrsta0"
            
            def description(self):
                return "Current status of event slice 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prm_lbint_crrsta1"] = _AF6CCI0031_RD_PDH_MDLPRM._prm_lb_int_crrsta._prm_lbint_crrsta1()
            allFields["prm_lbint_crrsta0"] = _AF6CCI0031_RD_PDH_MDLPRM._prm_lb_int_crrsta._prm_lbint_crrsta0()
            return allFields

    class _prm_lb_intsta(AtRegister.AtRegister):
        def name(self):
            return "PRM LB Interrupt per Channel Interrupt OR Status"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return "0x1AC00 +  $GID"
            
        def startAddress(self):
            return 0x0001ac00
            
        def endAddress(self):
            return 0x0001ac20

        class _prm_lbintsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "prm_lbintsta"
            
            def description(self):
                return "Set to 1 if any interrupt status bit of corresponding channel is set and its interrupt is enabled."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prm_lbintsta"] = _AF6CCI0031_RD_PDH_MDLPRM._prm_lb_intsta._prm_lbintsta()
            return allFields

    class _prm_lb_sta_int(AtRegister.AtRegister):
        def name(self):
            return "PRM LB Interrupt OR Status"
    
        def description(self):
            return "The register consists of 2 bits. Each bit is used to store Interrupt OR status."
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0001afff
            
        def endAddress(self):
            return 0xffffffff

        class _prm_lbsta_int(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "prm_lbsta_int"
            
            def description(self):
                return "Set to 1 if any interrupt status bit is set and its interrupt is enabled"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prm_lbsta_int"] = _AF6CCI0031_RD_PDH_MDLPRM._prm_lb_sta_int._prm_lbsta_int()
            return allFields

    class _prm_lb_en_int(AtRegister.AtRegister):
        def name(self):
            return "PRM LB Interrupt Enable Control"
    
        def description(self):
            return "The register consists of 2 interrupt enable bits ."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0001affe
            
        def endAddress(self):
            return 0xffffffff

        class _prm_lben_int(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "prm_lben_int"
            
            def description(self):
                return "Set to 1 to enable to generate interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prm_lben_int"] = _AF6CCI0031_RD_PDH_MDLPRM._prm_lb_en_int._prm_lben_int()
            return allFields

    class _mdl_cfgen_int(AtRegister.AtRegister):
        def name(self):
            return "MDL per Channel Interrupt Enable Control"
    
        def description(self):
            return "This is the per Channel interrupt enable"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xA100 +  $MDLDE3ID + $LINEID * 32"
            
        def startAddress(self):
            return 0x0000a100
            
        def endAddress(self):
            return 0x0000a13f

        class _mdl_cfgen_int(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdl_cfgen_int"
            
            def description(self):
                return "Set 1 to enable change event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdl_cfgen_int"] = _AF6CCI0031_RD_PDH_MDLPRM._mdl_cfgen_int._mdl_cfgen_int()
            return allFields

    class _mdl_int_sta(AtRegister.AtRegister):
        def name(self):
            return "MDL Interrupt per Channel Interrupt Status"
    
        def description(self):
            return "This is the per Channel interrupt status."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xA140 +  $MDLDE3ID + $LINEID * 32"
            
        def startAddress(self):
            return 0x0000a140
            
        def endAddress(self):
            return 0x0000a17f

        class _mdlint_sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdlint_sta"
            
            def description(self):
                return "Set 1 if there is a change event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdlint_sta"] = _AF6CCI0031_RD_PDH_MDLPRM._mdl_int_sta._mdlint_sta()
            return allFields

    class _mdl_int_crrsta(AtRegister.AtRegister):
        def name(self):
            return "MDL Interrupt per Channel Current Status"
    
        def description(self):
            return "This is the per Channel Current status."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xA180 +  $MDLDE3ID + $LINEID * 32"
            
        def startAddress(self):
            return 0x0000a180
            
        def endAddress(self):
            return 0x0000a1af

        class _mdlint_crrsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdlint_crrsta"
            
            def description(self):
                return "Current status of event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdlint_crrsta"] = _AF6CCI0031_RD_PDH_MDLPRM._mdl_int_crrsta._mdlint_crrsta()
            return allFields

    class _mdl_intsta(AtRegister.AtRegister):
        def name(self):
            return "MDL Interrupt per Channel Interrupt OR Status"
    
        def description(self):
            return "The register consists of 32 bits for 32 VT/TUs of the related STS/VC in the Rx DS1/E1/J1 Framer. Each bit is used to store Interrupt OR tus of the related DS1/E1/J1."
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return "0xA1C0 +  $GID"
            
        def startAddress(self):
            return 0x0000a1c0
            
        def endAddress(self):
            return 0x0000a1c1

        class _mdlintsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdlintsta"
            
            def description(self):
                return "Set to 1 if any interrupt status bit of corresponding channel is set and its interrupt is enabled."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdlintsta"] = _AF6CCI0031_RD_PDH_MDLPRM._mdl_intsta._mdlintsta()
            return allFields

    class _mdl_sta_int(AtRegister.AtRegister):
        def name(self):
            return "MDL Interrupt OR Status"
    
        def description(self):
            return "The register consists of 2 bits. Each bit is used to store Interrupt OR status."
            
        def width(self):
            return 2
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000a1ff
            
        def endAddress(self):
            return 0xffffffff

        class _mdlsta_int(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdlsta_int"
            
            def description(self):
                return "Set to 1 if any interrupt status bit is set and its interrupt is enabled"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdlsta_int"] = _AF6CCI0031_RD_PDH_MDLPRM._mdl_sta_int._mdlsta_int()
            return allFields

    class _mdl_en_int(AtRegister.AtRegister):
        def name(self):
            return "MDL Interrupt Enable Control"
    
        def description(self):
            return "The register consists of 2 interrupt enable bits ."
            
        def width(self):
            return 2
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000a1fe
            
        def endAddress(self):
            return 0xffffffff

        class _mdlen_int(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdlen_int"
            
            def description(self):
                return "Set to 1 to enable to generate interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdlen_int"] = _AF6CCI0031_RD_PDH_MDLPRM._mdl_en_int._mdlen_int()
            return allFields

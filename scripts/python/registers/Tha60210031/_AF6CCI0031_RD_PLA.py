import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0031_RD_PLA(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["sig_conv_sta"] = _AF6CCI0031_RD_PLA._sig_conv_sta()
        allRegisters["add_pro_2_prevent_burst"] = _AF6CCI0031_RD_PLA._add_pro_2_prevent_burst()
        allRegisters["pyld_size_ctrl"] = _AF6CCI0031_RD_PLA._pyld_size_ctrl()
        allRegisters["up_add_conv_ctrl"] = _AF6CCI0031_RD_PLA._up_add_conv_ctrl()
        allRegisters["txeth_enable_ctrl"] = _AF6CCI0031_RD_PLA._txeth_enable_ctrl()
        return allRegisters

    class _sig_conv_sta(AtRegister.AtRegister):
        def name(self):
            return "Thalassa PDHPW Payload Assemble Signaling Convert 1 to 4 Status"
    
        def description(self):
            return "These registers are used to convert"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00008000  + 1024*$stsid + 128*$vtg + 32*$vtn + $ds0id"
            
        def startAddress(self):
            return 0x00008000
            
        def endAddress(self):
            return 0x0000af77

        class _PDHPWPlaAsmSigConv1to4Sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDHPWPlaAsmSigConv1to4Sta"
            
            def description(self):
                return "convert 1 to 4 signaling bits of timeslots of DS1 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDHPWPlaAsmSigConv1to4Sta"] = _AF6CCI0031_RD_PLA._sig_conv_sta._PDHPWPlaAsmSigConv1to4Sta()
            return allFields

    class _add_pro_2_prevent_burst(AtRegister.AtRegister):
        def name(self):
            return "Thalassa PDHPW Adding Protocol To Prevent Burst"
    
        def description(self):
            return "This register is to add a pseudo-wire that does not cause bottle-neck at transmit PW cause ACR performance bad at receive side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _AddPwId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 4
        
            def name(self):
                return "AddPwId"
            
            def description(self):
                return "PWID to be added"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _AddPwState(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "AddPwState"
            
            def description(self):
                return "State machine to add a PDH PW Step1: Write pseudo-wire ID to be added to AddPwId and set AddPwState to 0x1 to prepare add PW Step2: Do all configuration to add a pseudo-wire such as MAP,DEMAP,PLA,PDA,CLA,PSN.... Step3: Write AddPwState value 0x2 to start add PW protocol Step4: Poll AddPwState until return value of AddPwState equal to 0x0 the finish"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["AddPwId"] = _AF6CCI0031_RD_PLA._add_pro_2_prevent_burst._AddPwId()
            allFields["AddPwState"] = _AF6CCI0031_RD_PLA._add_pro_2_prevent_burst._AddPwState()
            return allFields

    class _pyld_size_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Thalassa PDHPW Payload Assemble Payload Size Control"
    
        def description(self):
            return "These registers are used to configure payload size in each PW channel"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00002000 +  $PWID"
            
        def startAddress(self):
            return 0x00002000
            
        def endAddress(self):
            return 0x0000253f

        class _RTPTsShapperEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "RTPTsShapperEnb"
            
            def description(self):
                return "RTP TS Shapper Enable. Should be enable in DS1/E1 SAToP. In CESoPSN only enable for PW which is used as Source for DCR"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmPldTypeCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 15
        
            def name(self):
                return "PDHPWPlaAsmPldTypeCtrl"
            
            def description(self):
                return "Payload type in the PW channel 0: SAToP 4: CES with CAS 5: CES without CAS 6: CEP"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmPldSizeCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDHPWPlaAsmPldSizeCtrl"
            
            def description(self):
                return "Payload size in the PW channel SAToP mode: [11:0] payload size in a packet CES mode [5:0] number of DS0 [14:6] number of NxDS0 basic CEP mode: [11:0] payload size in a packetizer fractional CEP mode: [3:0] Because value of payload size may be 3/9, 4/9, 5/9, 6/9, 7/9, 8/9, or 9/9 of VC-3 or VC-4 SPE, depended on fractional VC-3 or fractional VC-4 CEP mode. And a VC-3 or VC-4 //SPE has 9 row. So configured value will be number of rows of a SPE. Example, if value of payload size is 3/9, it means packetizer will assemble payload data of 3 rows of a SPE,  so value of the field //will be 3."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RTPTsShapperEnb"] = _AF6CCI0031_RD_PLA._pyld_size_ctrl._RTPTsShapperEnb()
            allFields["PDHPWPlaAsmPldTypeCtrl"] = _AF6CCI0031_RD_PLA._pyld_size_ctrl._PDHPWPlaAsmPldTypeCtrl()
            allFields["PDHPWPlaAsmPldSizeCtrl"] = _AF6CCI0031_RD_PLA._pyld_size_ctrl._PDHPWPlaAsmPldSizeCtrl()
            return allFields

    class _up_add_conv_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Thalassa PDHPW Payload Assemble uP Address Convert Control"
    
        def description(self):
            return "These registers are used to enable converting mode for uP address as accessing into some RAMs which request to convert identification."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _PDHPWPlaAsmCPUAdrConvEnCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PDHPWPlaAsmCPUAdrConvEnCtrl"
            
            def description(self):
                return "Enable uP address converting 1: enable 0: disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDHPWPlaAsmCPUAdrConvEnCtrl"] = _AF6CCI0031_RD_PLA._up_add_conv_ctrl._PDHPWPlaAsmCPUAdrConvEnCtrl()
            return allFields

    class _txeth_enable_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Thalassa PDHPW Payload Assemble Engine Status"
    
        def description(self):
            return "These registers are used to save status in the engine."
            
        def width(self):
            return 96
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00028000 +  $PWID"
            
        def startAddress(self):
            return 0x00028000
            
        def endAddress(self):
            return 0xffffffff

        class _PDHPWPlaAsmAAL1PtrSetSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 78
                
            def startBit(self):
                return 78
        
            def name(self):
                return "PDHPWPlaAsmAAL1PtrSetSta"
            
            def description(self):
                return "indicate that the pointer is already set in that AAL1 cycle 1: enable 0: disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSigNumStartSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 77
                
            def startBit(self):
                return 76
        
            def name(self):
                return "PDHPWPlaAsmSigNumStartSta"
            
            def description(self):
                return "indicate number of signaling bytes to append into the last payload bytes to become a 4-bytes data valid."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmGenSigCntSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 75
                
            def startBit(self):
                return 71
        
            def name(self):
                return "PDHPWPlaAsmGenSigCntSta"
            
            def description(self):
                return "indicate number of the remaining signaling bytes which need to be append into PW packet."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmPDUNumCntSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 70
                
            def startBit(self):
                return 65
        
            def name(self):
                return "PDHPWPlaAsmPDUNumCntSta"
            
            def description(self):
                return "indicate the current number of the PDU counter in AAL1 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmPDUCylCntSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 64
                
            def startBit(self):
                return 62
        
            def name(self):
                return "PDHPWPlaAsmPDUCylCntSta"
            
            def description(self):
                return "indicate the current number of the cycle PDU counter in AAL1 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmPDUByteCntSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 61
                
            def startBit(self):
                return 58
        
            def name(self):
                return "PDHPWPlaAsmPDUByteCntSta"
            
            def description(self):
                return "indicate the current number of bytes of a PDU in AAL1 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSatopCESByteCntSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 57
                
            def startBit(self):
                return 43
        
            def name(self):
                return "PDHPWPlaAsmSatopCESByteCntSta"
            
            def description(self):
                return "indicate number of bytes in Satop and CES mode With Satop mode number of bytes of the payload in PW packet With CES mode [57:49]  indicate the current number of NxDS0 counter in CES mode [48:43]  indicate the current number of DS0 byte counter in CES mode With basic CEP mode: number of bytes of the payload in PW packet With fractional CEP mode [57:49] indicate the current number of bytes of actual payload in PW packet [48:43] indicate the current row of payload in PW packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmDataNumSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 42
                
            def startBit(self):
                return 40
        
            def name(self):
                return "PDHPWPlaAsmDataNumSta"
            
            def description(self):
                return "indicate the current number of data bytes in the data buffer"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmDataBufSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 39
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDHPWPlaAsmDataBufSta"
            
            def description(self):
                return "as data buffer to save data with the maximum 5 bytes"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDHPWPlaAsmAAL1PtrSetSta"] = _AF6CCI0031_RD_PLA._txeth_enable_ctrl._PDHPWPlaAsmAAL1PtrSetSta()
            allFields["PDHPWPlaAsmSigNumStartSta"] = _AF6CCI0031_RD_PLA._txeth_enable_ctrl._PDHPWPlaAsmSigNumStartSta()
            allFields["PDHPWPlaAsmGenSigCntSta"] = _AF6CCI0031_RD_PLA._txeth_enable_ctrl._PDHPWPlaAsmGenSigCntSta()
            allFields["PDHPWPlaAsmPDUNumCntSta"] = _AF6CCI0031_RD_PLA._txeth_enable_ctrl._PDHPWPlaAsmPDUNumCntSta()
            allFields["PDHPWPlaAsmPDUCylCntSta"] = _AF6CCI0031_RD_PLA._txeth_enable_ctrl._PDHPWPlaAsmPDUCylCntSta()
            allFields["PDHPWPlaAsmPDUByteCntSta"] = _AF6CCI0031_RD_PLA._txeth_enable_ctrl._PDHPWPlaAsmPDUByteCntSta()
            allFields["PDHPWPlaAsmSatopCESByteCntSta"] = _AF6CCI0031_RD_PLA._txeth_enable_ctrl._PDHPWPlaAsmSatopCESByteCntSta()
            allFields["PDHPWPlaAsmDataNumSta"] = _AF6CCI0031_RD_PLA._txeth_enable_ctrl._PDHPWPlaAsmDataNumSta()
            allFields["PDHPWPlaAsmDataBufSta"] = _AF6CCI0031_RD_PLA._txeth_enable_ctrl._PDHPWPlaAsmDataBufSta()
            return allFields

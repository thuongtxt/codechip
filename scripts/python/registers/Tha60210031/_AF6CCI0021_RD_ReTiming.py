import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0021_RD_ReTiming(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["retisrc_pen"] = _AF6CCI0021_RD_ReTiming._retisrc_pen()
        allRegisters["retibypass_pen"] = _AF6CCI0021_RD_ReTiming._retibypass_pen()
        allRegisters["txctrl_pen"] = _AF6CCI0021_RD_ReTiming._txctrl_pen()
        allRegisters["rxctrl_pen"] = _AF6CCI0021_RD_ReTiming._rxctrl_pen()
        allRegisters["status_pen"] = _AF6CCI0021_RD_ReTiming._status_pen()
        allRegisters["stk0_upen"] = _AF6CCI0021_RD_ReTiming._stk0_upen()
        allRegisters["cnt0_pen"] = _AF6CCI0021_RD_ReTiming._cnt0_pen()
        return allRegisters

    class _retisrc_pen(AtRegister.AtRegister):
        def name(self):
            return "Tx  Re-Timing Loopback"
    
        def description(self):
            return "These registers are used to select source Re-Timing"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000315
            
        def endAddress(self):
            return 0xffffffff

        class _Src1FrTXPdh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Src1FrTXPdh"
            
            def description(self):
                return "Source 1 timing from TX PDH 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Src1FrRXLiuenb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Src1FrRXLiuenb"
            
            def description(self):
                return "Source 1 Data from RX LIU enable 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Src0FrRXLiuenb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Src0FrRXLiuenb"
            
            def description(self):
                return "Source 0 Data from RX LIU enable 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Src0FrRXLiumod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Src0FrRXLiumod"
            
            def description(self):
                return "Source 0 Data from RX LIU mode 2:DS3 3:E3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Src1FrTXPdh"] = _AF6CCI0021_RD_ReTiming._retisrc_pen._Src1FrTXPdh()
            allFields["Src1FrRXLiuenb"] = _AF6CCI0021_RD_ReTiming._retisrc_pen._Src1FrRXLiuenb()
            allFields["Src0FrRXLiuenb"] = _AF6CCI0021_RD_ReTiming._retisrc_pen._Src0FrRXLiuenb()
            allFields["Src0FrRXLiumod"] = _AF6CCI0021_RD_ReTiming._retisrc_pen._Src0FrRXLiumod()
            return allFields

    class _retibypass_pen(AtRegister.AtRegister):
        def name(self):
            return "Re-Timing Engine Enable"
    
        def description(self):
            return "These registers are used to enable Re-Timing engine"
            
        def width(self):
            return 64
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000f4000
            
        def endAddress(self):
            return 0xffffffff

        class _retiEngEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "retiEngEn"
            
            def description(self):
                return "Re-Timing engine Enable - 1: Enable - 0: Bypass"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["retiEngEn"] = _AF6CCI0021_RD_ReTiming._retibypass_pen._retiEngEn()
            return allFields

    class _txctrl_pen(AtRegister.AtRegister):
        def name(self):
            return "Tx  Re-Timing Framer Control"
    
        def description(self):
            return "This is used to configure the SSM transmitter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000F0000 + LiuID"
            
        def startAddress(self):
            return 0x000f0000
            
        def endAddress(self):
            return 0x000f0017

        class _SSMNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "SSMNum"
            
            def description(self):
                return "Number SSM message to transmit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SSMInv(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "SSMInv"
            
            def description(self):
                return "Set 1 to invert SSM - 1:invert mode , SSM format 11111111_11_0xxxxxx0 - 1:non_invert mode , SSM format 0xxxxxx0_11_11111111"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SSMMess(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 5
        
            def name(self):
                return "SSMMess"
            
            def description(self):
                return "SSM Message - [3:0]: G832 SSM Message - [5:0]: G832 SSM Message"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _G832SSMEna(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "G832SSMEna"
            
            def description(self):
                return "G832 SSM enable/C-Bit parity SSM continously 1: Enable SSM G832/Enable send SSM continously for C-Bit parity SS 0: Disable SSM G832"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE3Md(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDE3Md"
            
            def description(self):
                return "Transmit DS3/E3 framing mode - 0000: G832 - 0001: G751 - 0010: DS3 (M13,C-Bit) - Other: Unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SSMNum"] = _AF6CCI0021_RD_ReTiming._txctrl_pen._SSMNum()
            allFields["SSMInv"] = _AF6CCI0021_RD_ReTiming._txctrl_pen._SSMInv()
            allFields["SSMMess"] = _AF6CCI0021_RD_ReTiming._txctrl_pen._SSMMess()
            allFields["G832SSMEna"] = _AF6CCI0021_RD_ReTiming._txctrl_pen._G832SSMEna()
            allFields["TxDE3Md"] = _AF6CCI0021_RD_ReTiming._txctrl_pen._TxDE3Md()
            return allFields

    class _rxctrl_pen(AtRegister.AtRegister):
        def name(self):
            return "Rx Re-Timing Framer Control"
    
        def description(self):
            return "This is used to configure the SSM transmitter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000FC000 + LiuID"
            
        def startAddress(self):
            return 0x000fc000
            
        def endAddress(self):
            return 0x000fc017

        class _RxDE3Enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "RxDE3Enb"
            
            def description(self):
                return "Set 1 to Enable Framer"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE3LofThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 5
        
            def name(self):
                return "RxDE3LofThres"
            
            def description(self):
                return "Threshold for declare LOF"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE3FrcLof(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RxDE3FrcLof"
            
            def description(self):
                return "Set 1 to force Re-frame (default 0)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE3Md(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE3Md"
            
            def description(self):
                return "Receive DS3/E3 framing mode - 0000: G832 - 0001: G751 - 0010: DS3 (M13,C-Bit) - Other: Unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE3Enb"] = _AF6CCI0021_RD_ReTiming._rxctrl_pen._RxDE3Enb()
            allFields["RxDE3LofThres"] = _AF6CCI0021_RD_ReTiming._rxctrl_pen._RxDE3LofThres()
            allFields["RxDE3FrcLof"] = _AF6CCI0021_RD_ReTiming._rxctrl_pen._RxDE3FrcLof()
            allFields["RxDE3Md"] = _AF6CCI0021_RD_ReTiming._rxctrl_pen._RxDE3Md()
            return allFields

    class _status_pen(AtRegister.AtRegister):
        def name(self):
            return "Rx SSM Framer HW Status"
    
        def description(self):
            return "These registers are used for Hardware status only"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000FC400 + liuid"
            
        def startAddress(self):
            return 0x000fc400
            
        def endAddress(self):
            return 0x000fc017

        class _RxDE3Sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE3Sta"
            
            def description(self):
                return "RX Framer Status - 3: In Frame - 0: LOF - Other: Searching State"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE3Sta"] = _AF6CCI0021_RD_ReTiming._status_pen._RxDE3Sta()
            return allFields

    class _stk0_upen(AtRegister.AtRegister):
        def name(self):
            return "Re-Timing Slip Buffer Data Sticky"
    
        def description(self):
            return "These registers are used to"
            
        def width(self):
            return 64
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000f4001
            
        def endAddress(self):
            return 0xffffffff

        class _RetiSlipDat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RetiSlipDat"
            
            def description(self):
                return "Re-Timing Slip Buffer - 1: Slip Enable - 0: Slip Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RetiSlipDat"] = _AF6CCI0021_RD_ReTiming._stk0_upen._RetiSlipDat()
            return allFields

    class _cnt0_pen(AtRegister.AtRegister):
        def name(self):
            return "Re-Timing Slip Buffer Frame Sticky"
    
        def description(self):
            return "This is the per channel CRC error counter for DS1/E1/J1 receive framer"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000F4100 + 64*Rd2Clr + LiuID"
            
        def startAddress(self):
            return 0x000f4100
            
        def endAddress(self):
            return 0x000f412f

        class _SlipCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SlipCnt"
            
            def description(self):
                return "Slip Buffer Counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SlipCnt"] = _AF6CCI0021_RD_ReTiming._cnt0_pen._SlipCnt()
            return allFields

import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0031_RD_TOP(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["top_o_control0"] = _AF6CCI0031_RD_TOP._top_o_control0()
        allRegisters["top_o_control1"] = _AF6CCI0031_RD_TOP._top_o_control1()
        allRegisters["top_o_control2"] = _AF6CCI0031_RD_TOP._top_o_control2()
        return allRegisters

    class _top_o_control0(AtRegister.AtRegister):
        def name(self):
            return "Top ETH-Port Control 0"
    
        def description(self):
            return "This register indicates the active ports."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000040
            
        def endAddress(self):
            return 0xffffffff

        class _E1G_Diag(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "E1G_Diag"
            
            def description(self):
                return "E1G Diag Enable  1: Diag  0: Normal"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _E10G_IdSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "E10G_IdSel"
            
            def description(self):
                return "E10G XFI Port Select"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _E1G_PSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "E1G_PSel"
            
            def description(self):
                return "E1G SGMII Protection Group Select 1: Protection 0: Working"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _E1G_IdDiag(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 4
        
            def name(self):
                return "E1G_IdDiag"
            
            def description(self):
                return "Diag ID select"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["E1G_Diag"] = _AF6CCI0031_RD_TOP._top_o_control0._E1G_Diag()
            allFields["E10G_IdSel"] = _AF6CCI0031_RD_TOP._top_o_control0._E10G_IdSel()
            allFields["E1G_PSel"] = _AF6CCI0031_RD_TOP._top_o_control0._E1G_PSel()
            allFields["E1G_IdDiag"] = _AF6CCI0031_RD_TOP._top_o_control0._E1G_IdDiag()
            return allFields

    class _top_o_control1(AtRegister.AtRegister):
        def name(self):
            return "Top ETH-Port Control 1"
    
        def description(self):
            return "This register indicates the active ports."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000041
            
        def endAddress(self):
            return 0xffffffff

        class _E10G_TxEng(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "E10G_TxEng"
            
            def description(self):
                return "Tx XFI Enable bit_00: XFI_Port#0 bit_01: XFI_Port#1 bit_02: XFI_Port#2 bit_03: XFI_Port#3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _E1G_TxEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "E1G_TxEn"
            
            def description(self):
                return "Tx SGMII Enable bit_00: SGMII_Port#0 of QSGMII#0, MAC_Queue#0 when select working bit_01: SGMII_Port#1 of QSGMII#0, MAC_Queue#1 when select working bit_02: SGMII_Port#2 of QSGMII#0, MAC_Queue#2 when select working bit_03: SGMII_Port#3 of QSGMII#0, MAC_Queue#3 when select working bit_04: SGMII_Port#0 of QSGMII#2, MAC_Queue#4 when select working bit_05: SGMII_Port#1 of QSGMII#2, Unused bit_06: SGMII_Port#2 of QSGMII#2, Unused bit_07: SGMII_Port#3 of QSGMII#2, Unused bit_08: SGMII_Port#0 of QSGMII#1, MAC_Queue#0 when select protection bit_09: SGMII_Port#1 of QSGMII#1, MAC_Queue#0 when select protection bit_10: SGMII_Port#2 of QSGMII#1, MAC_Queue#0 when select protection bit_11: SGMII_Port#3 of QSGMII#1, MAC_Queue#0 when select protection bit_12: SGMII_Port#0 of QSGMII#3, MAC_Queue#0 when select protection bit_13: SGMII_Port#1 of QSGMII#3, Unused bit_14: SGMII_Port#2 of QSGMII#3, Unused bit_15: SGMII_Port#3 of QSGMII#3, Unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["E10G_TxEng"] = _AF6CCI0031_RD_TOP._top_o_control1._E10G_TxEng()
            allFields["E1G_TxEn"] = _AF6CCI0031_RD_TOP._top_o_control1._E1G_TxEn()
            return allFields

    class _top_o_control2(AtRegister.AtRegister):
        def name(self):
            return "Top ETH-Port Control 2"
    
        def description(self):
            return "This register indicates the active ports."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000042
            
        def endAddress(self):
            return 0xffffffff

        class _E1G_LoopEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "E1G_LoopEn"
            
            def description(self):
                return "GMII local loopback Enable bit_00: LOCAL LOOPBACK SGMII_Port#0 of QSGMII#0 (Current loopback for QSGMII#0) bit_01: LOCAL LOOPBACK SGMII_Port#1 of QSGMII#0 (for future) bit_02: LOCAL LOOPBACK SGMII_Port#2 of QSGMII#0 (for future) bit_03: LOCAL LOOPBACK SGMII_Port#3 of QSGMII#0 (for future) bit_04: LOCAL LOOPBACK SGMII_Port#0 of QSGMII#1 (Current loopback for QSGMII#1) bit_05: LOCAL LOOPBACK SGMII_Port#1 of QSGMII#1 (for future) bit_06: LOCAL LOOPBACK SGMII_Port#2 of QSGMII#1 (for future) bit_07: LOCAL LOOPBACK SGMII_Port#3 of QSGMII#1 (for future) bit_08: LOCAL LOOPBACK SGMII_Port#0 of QSGMII#2 (Current loopback for QSGMII#2) bit_09: LOCAL LOOPBACK SGMII_Port#1 of QSGMII#2 (for future) bit_10: LOCAL LOOPBACK SGMII_Port#2 of QSGMII#2 (for future) bit_11: LOCAL LOOPBACK SGMII_Port#3 of QSGMII#2 (for future) bit_12: LOCAL LOOPBACK SGMII_Port#0 of QSGMII#3 (Current loopback for QSGMII#3) bit_13: LOCAL LOOPBACK SGMII_Port#1 of QSGMII#3 (for future) bit_14: LOCAL LOOPBACK SGMII_Port#2 of QSGMII#3 (for future) bit_15: LOCAL LOOPBACK SGMII_Port#3 of QSGMII#3 (for future)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["E1G_LoopEn"] = _AF6CCI0031_RD_TOP._top_o_control2._E1G_LoopEn()
            return allFields

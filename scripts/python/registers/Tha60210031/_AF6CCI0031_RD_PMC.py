import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0031_RD_PMC(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["eth_rx_fcs_err_cnt_ro"] = _AF6CCI0031_RD_PMC._eth_rx_fcs_err_cnt_ro()
        allRegisters["eth_rx_fcs_err_cnt_r2c"] = _AF6CCI0031_RD_PMC._eth_rx_fcs_err_cnt_r2c()
        allRegisters["eth_rx_header_err_cnt_ro"] = _AF6CCI0031_RD_PMC._eth_rx_header_err_cnt_ro()
        allRegisters["eth_rx_header_err_cnt_r2c"] = _AF6CCI0031_RD_PMC._eth_rx_header_err_cnt_r2c()
        allRegisters["eth_rx_psnheader_err_cnt_ro"] = _AF6CCI0031_RD_PMC._eth_rx_psnheader_err_cnt_ro()
        allRegisters["eth_rx_psnheader_err_cnt_r2c"] = _AF6CCI0031_RD_PMC._eth_rx_psnheader_err_cnt_r2c()
        allRegisters["eth_rx_udpport_err_cnt_ro"] = _AF6CCI0031_RD_PMC._eth_rx_udpport_err_cnt_ro()
        allRegisters["eth_rx_udpport_err_cnt_r2c"] = _AF6CCI0031_RD_PMC._eth_rx_udpport_err_cnt_r2c()
        allRegisters["eth_rx_oam_pkt_cnt_ro"] = _AF6CCI0031_RD_PMC._eth_rx_oam_pkt_cnt_ro()
        allRegisters["eth_rx_oam_pkt_cnt_r2c"] = _AF6CCI0031_RD_PMC._eth_rx_oam_pkt_cnt_r2c()
        allRegisters["eth_tx_oam_pkt_cnt_ro"] = _AF6CCI0031_RD_PMC._eth_tx_oam_pkt_cnt_ro()
        allRegisters["eth_tx_oam_pkt_cnt_r2c"] = _AF6CCI0031_RD_PMC._eth_tx_oam_pkt_cnt_r2c()
        allRegisters["eth_rx_total_byte_cnt_ro"] = _AF6CCI0031_RD_PMC._eth_rx_total_byte_cnt_ro()
        allRegisters["eth_rx_total_byte_cnt_r2c"] = _AF6CCI0031_RD_PMC._eth_rx_total_byte_cnt_r2c()
        allRegisters["eth_rx_total_pkt_cnt_ro"] = _AF6CCI0031_RD_PMC._eth_rx_total_pkt_cnt_ro()
        allRegisters["eth_rx_total_pkt_cnt_r2c"] = _AF6CCI0031_RD_PMC._eth_rx_total_pkt_cnt_r2c()
        allRegisters["eth_rx_cla_pkt_cnt_ro"] = _AF6CCI0031_RD_PMC._eth_rx_cla_pkt_cnt_ro()
        allRegisters["eth_rx_cla_pkt_cnt_r2c"] = _AF6CCI0031_RD_PMC._eth_rx_cla_pkt_cnt_r2c()
        allRegisters["eth_tx_byte_cnt_ro"] = _AF6CCI0031_RD_PMC._eth_tx_byte_cnt_ro()
        allRegisters["eth_tx_byte_cnt_r2c"] = _AF6CCI0031_RD_PMC._eth_tx_byte_cnt_r2c()
        allRegisters["eth_tx_pkt_cnt_ro"] = _AF6CCI0031_RD_PMC._eth_tx_pkt_cnt_ro()
        allRegisters["eth_tx_pkt_cnt_r2c"] = _AF6CCI0031_RD_PMC._eth_tx_pkt_cnt_r2c()
        allRegisters["eth_rx_1to64byte_pkt_cnt_ro"] = _AF6CCI0031_RD_PMC._eth_rx_1to64byte_pkt_cnt_ro()
        allRegisters["eth_rx_1to64byte_pkt_cnt_r2c"] = _AF6CCI0031_RD_PMC._eth_rx_1to64byte_pkt_cnt_r2c()
        allRegisters["eth_tx_1to64byte_pkt_cnt_ro"] = _AF6CCI0031_RD_PMC._eth_tx_1to64byte_pkt_cnt_ro()
        allRegisters["eth_tx_1to64byte_pkt_cnt_r2c"] = _AF6CCI0031_RD_PMC._eth_tx_1to64byte_pkt_cnt_r2c()
        allRegisters["eth_rx_65to128byte_pkt_cnt_ro"] = _AF6CCI0031_RD_PMC._eth_rx_65to128byte_pkt_cnt_ro()
        allRegisters["eth_rx_65to128byte_pkt_cnt_r2c"] = _AF6CCI0031_RD_PMC._eth_rx_65to128byte_pkt_cnt_r2c()
        allRegisters["eth_tx_65to128byte_pkt_cnt_ro"] = _AF6CCI0031_RD_PMC._eth_tx_65to128byte_pkt_cnt_ro()
        allRegisters["eth_tx_65to128byte_pkt_cnt_r2c"] = _AF6CCI0031_RD_PMC._eth_tx_65to128byte_pkt_cnt_r2c()
        allRegisters["eth_rx_129to256byte_pkt_cnt_ro"] = _AF6CCI0031_RD_PMC._eth_rx_129to256byte_pkt_cnt_ro()
        allRegisters["eth_rx_129to256byte_pkt_cnt_r2c"] = _AF6CCI0031_RD_PMC._eth_rx_129to256byte_pkt_cnt_r2c()
        allRegisters["eth_tx_129to256byte_pkt_cnt_ro"] = _AF6CCI0031_RD_PMC._eth_tx_129to256byte_pkt_cnt_ro()
        allRegisters["eth_tx_129to256byte_pkt_cnt_r2c"] = _AF6CCI0031_RD_PMC._eth_tx_129to256byte_pkt_cnt_r2c()
        allRegisters["eth_rx_257to512byte_pkt_cnt_ro"] = _AF6CCI0031_RD_PMC._eth_rx_257to512byte_pkt_cnt_ro()
        allRegisters["eth_rx_257to512byte_pkt_cnt_r2c"] = _AF6CCI0031_RD_PMC._eth_rx_257to512byte_pkt_cnt_r2c()
        allRegisters["eth_tx_257to512byte_pkt_cnt_ro"] = _AF6CCI0031_RD_PMC._eth_tx_257to512byte_pkt_cnt_ro()
        allRegisters["eth_tx_257to512byte_pkt_cnt_r2c"] = _AF6CCI0031_RD_PMC._eth_tx_257to512byte_pkt_cnt_r2c()
        allRegisters["eth_rx_513to1024byte_pkt_cnt_ro"] = _AF6CCI0031_RD_PMC._eth_rx_513to1024byte_pkt_cnt_ro()
        allRegisters["eth_rx_513to1024byte_pkt_cnt_r2c"] = _AF6CCI0031_RD_PMC._eth_rx_513to1024byte_pkt_cnt_r2c()
        allRegisters["eth_tx_513to1024byte_pkt_cnt_ro"] = _AF6CCI0031_RD_PMC._eth_tx_513to1024byte_pkt_cnt_ro()
        allRegisters["eth_tx_513to1024byte_pkt_cnt_r2c"] = _AF6CCI0031_RD_PMC._eth_tx_513to1024byte_pkt_cnt_r2c()
        allRegisters["eth_rx_1025to1528byte_pkt_cnt_ro"] = _AF6CCI0031_RD_PMC._eth_rx_1025to1528byte_pkt_cnt_ro()
        allRegisters["eth_rx_1025to1528byte_pkt_cnt_r2c"] = _AF6CCI0031_RD_PMC._eth_rx_1025to1528byte_pkt_cnt_r2c()
        allRegisters["eth_tx_1025to1528byte_pkt_cnt_ro"] = _AF6CCI0031_RD_PMC._eth_tx_1025to1528byte_pkt_cnt_ro()
        allRegisters["eth_tx_1025to1528byte_pkt_cnt_r2c"] = _AF6CCI0031_RD_PMC._eth_tx_1025to1528byte_pkt_cnt_r2c()
        allRegisters["eth_rx_jumbobyte_pkt_cnt_ro"] = _AF6CCI0031_RD_PMC._eth_rx_jumbobyte_pkt_cnt_ro()
        allRegisters["eth_rx_jumbobyte_pkt_cnt_r2c"] = _AF6CCI0031_RD_PMC._eth_rx_jumbobyte_pkt_cnt_r2c()
        allRegisters["eth_tx_jumbobyte_pkt_cnt_ro"] = _AF6CCI0031_RD_PMC._eth_tx_jumbobyte_pkt_cnt_ro()
        allRegisters["eth_tx_jumbobyte_pkt_cnt_r2c"] = _AF6CCI0031_RD_PMC._eth_tx_jumbobyte_pkt_cnt_r2c()
        allRegisters["Pseudowire_Transmit_Good_Packet_Counter_ro"] = _AF6CCI0031_RD_PMC._Pseudowire_Transmit_Good_Packet_Counter_ro()
        allRegisters["Pseudowire_Transmit_Good_Packet_Counter_rc"] = _AF6CCI0031_RD_PMC._Pseudowire_Transmit_Good_Packet_Counter_rc()
        allRegisters["Pseudowire_Receive_Payload_Octet_Counter_ro"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Payload_Octet_Counter_ro()
        allRegisters["Pseudowire_Receive_Payload_Octet_Counter_rc"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Payload_Octet_Counter_rc()
        allRegisters["Pseudowire_Transmit_LBit_Packet_Counter_ro"] = _AF6CCI0031_RD_PMC._Pseudowire_Transmit_LBit_Packet_Counter_ro()
        allRegisters["Pseudowire_Transmit_LBit_Packet_Counter_rc"] = _AF6CCI0031_RD_PMC._Pseudowire_Transmit_LBit_Packet_Counter_rc()
        allRegisters["Pseudowire_Transmit_RBit_Packet_Counter_ro"] = _AF6CCI0031_RD_PMC._Pseudowire_Transmit_RBit_Packet_Counter_ro()
        allRegisters["Pseudowire_Transmit_RBit_Packet_Counter_rc"] = _AF6CCI0031_RD_PMC._Pseudowire_Transmit_RBit_Packet_Counter_rc()
        allRegisters["Pseudowire_Transmit_MBit_NBit_Packet_Counter_ro"] = _AF6CCI0031_RD_PMC._Pseudowire_Transmit_MBit_NBit_Packet_Counter_ro()
        allRegisters["Pseudowire_Transmit_MBit_NBit_Packet_Counter_rc"] = _AF6CCI0031_RD_PMC._Pseudowire_Transmit_MBit_NBit_Packet_Counter_rc()
        allRegisters["Pseudowire_Transmit_PBit_Packet_Counter_ro"] = _AF6CCI0031_RD_PMC._Pseudowire_Transmit_PBit_Packet_Counter_ro()
        allRegisters["Pseudowire_Transmit_PBit_Packet_Counter_rc"] = _AF6CCI0031_RD_PMC._Pseudowire_Transmit_PBit_Packet_Counter_rc()
        allRegisters["Pseudowire_Receive_Good_Packet_Counter_ro"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Good_Packet_Counter_ro()
        allRegisters["Pseudowire_Receive_Good_Packet_Counter_rc"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Good_Packet_Counter_rc()
        allRegisters["Pseudowire_Receive_Good_Byte_Counter_ro"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Good_Byte_Counter_ro()
        allRegisters["Pseudowire_Receive_Good_Byte_Counter_rc"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Good_Byte_Counter_rc()
        allRegisters["Pseudowire_Receive_LOFS_Counter_ro"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_LOFS_Counter_ro()
        allRegisters["Pseudowire_Receive_LOFS_Counter_rc"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_LOFS_Counter_rc()
        allRegisters["Pseudowire_Receive_LBit_Packet_Counter_ro"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_LBit_Packet_Counter_ro()
        allRegisters["Pseudowire_Receive_LBit_Packet_Counter_rc"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_LBit_Packet_Counter_rc()
        allRegisters["Pseudowire_Receive_M_NBit_Packet_Counter_ro"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_M_NBit_Packet_Counter_ro()
        allRegisters["Pseudowire_Receive_M_NBit_Packet_Counter_rc"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_M_NBit_Packet_Counter_rc()
        allRegisters["Pseudowire_Receive_PBit_Packet_Counter_ro"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_PBit_Packet_Counter_ro()
        allRegisters["Pseudowire_Receive_PBit_Packet_Counter_rc"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_PBit_Packet_Counter_rc()
        allRegisters["Pseudowire_Receive_RBit_Packet_Counter_ro"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_RBit_Packet_Counter_ro()
        allRegisters["Pseudowire_Receive_RBit_Packet_Counter_rc"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_RBit_Packet_Counter_rc()
        allRegisters["Pseudowire_Receive_Reorder_Drop_Packet_Counter_ro"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Reorder_Drop_Packet_Counter_ro()
        allRegisters["Pseudowire_Receive_Reorder_Drop_Packet_Counter_rc"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Reorder_Drop_Packet_Counter_rc()
        allRegisters["Pseudowire_Receive_Reorder_Out_Of_Sequence_Packet_Counter_ro"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Reorder_Out_Of_Sequence_Packet_Counter_ro()
        allRegisters["Pseudowire_Receive_Reorder_Out_Of_Sequence_Packet_Counter_rc"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Reorder_Out_Of_Sequence_Packet_Counter_rc()
        allRegisters["Pseudowire_Receive_Reorder_Lost_Packet_Counter_ro"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Reorder_Lost_Packet_Counter_ro()
        allRegisters["Pseudowire_Receive_Reorder_Lost_Packet_Counter_rc"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Reorder_Lost_Packet_Counter_rc()
        allRegisters["Pseudowire_Receive_Jitter_Buffer_OverRun_Counter_ro"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Jitter_Buffer_OverRun_Counter_ro()
        allRegisters["Pseudowire_Receive_Jitter_Buffer_OverRun_Counter_rc"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Jitter_Buffer_OverRun_Counter_rc()
        allRegisters["Pseudowire_Receive_Jitter_Buffer_UnderRun_Counter_ro"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Jitter_Buffer_UnderRun_Counter_ro()
        allRegisters["Pseudowire_Receive_Jitter_Buffer_UnderRun_Counter_rc"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Jitter_Buffer_UnderRun_Counter_rc()
        allRegisters["Pseudowire_Receive_Malform_packet_Counter_ro"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Malform_packet_Counter_ro()
        allRegisters["Pseudowire_Receive_Malform_packet_Counter_rc"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Malform_packet_Counter_rc()
        allRegisters["Pseudowire_Receive_Stray_packet_Counter_ro"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Stray_packet_Counter_ro()
        allRegisters["Pseudowire_Receive_Stray_packet_Counter_rc"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Stray_packet_Counter_rc()
        allRegisters["Pseudowire_Receive_Duplicate_packet_Counter_ro"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Duplicate_packet_Counter_ro()
        allRegisters["Pseudowire_Receive_Duplicate_packet_Counter_rc"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Duplicate_packet_Counter_rc()
        allRegisters["Counter_Per_Alarm_Interrupt_Enable_Control"] = _AF6CCI0031_RD_PMC._Counter_Per_Alarm_Interrupt_Enable_Control()
        allRegisters["Counter_Per_Alarm_Interrupt_Status"] = _AF6CCI0031_RD_PMC._Counter_Per_Alarm_Interrupt_Status()
        allRegisters["Counter_Per_Alarm_Current_Status"] = _AF6CCI0031_RD_PMC._Counter_Per_Alarm_Current_Status()
        allRegisters["Counter_Interrupt_OR_Status"] = _AF6CCI0031_RD_PMC._Counter_Interrupt_OR_Status()
        allRegisters["counter_per_group_intr_or_stat"] = _AF6CCI0031_RD_PMC._counter_per_group_intr_or_stat()
        allRegisters["counter_per_group_intr_en_ctrl"] = _AF6CCI0031_RD_PMC._counter_per_group_intr_en_ctrl()
        allRegisters["counter_per_slice_intr_or_stat"] = _AF6CCI0031_RD_PMC._counter_per_slice_intr_or_stat()
        return allRegisters

    class _eth_rx_fcs_err_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive FCS Error Packet Counter"
    
        def description(self):
            return "TCount number of FCS error packets detected"
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _RxFcsErrorPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxFcsErrorPk"
            
            def description(self):
                return "This counter count the number of FCS error Ethernet PHY packets received."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxFcsErrorPk"] = _AF6CCI0031_RD_PMC._eth_rx_fcs_err_cnt_ro._RxFcsErrorPk()
            return allFields

    class _eth_rx_fcs_err_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive FCS Error Packet Counter"
    
        def description(self):
            return "TCount number of FCS error packets detected"
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000001
            
        def endAddress(self):
            return 0xffffffff

        class _RxFcsErrorPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxFcsErrorPk"
            
            def description(self):
                return "This counter count the number of FCS error Ethernet PHY packets received."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxFcsErrorPk"] = _AF6CCI0031_RD_PMC._eth_rx_fcs_err_cnt_r2c._RxFcsErrorPk()
            return allFields

    class _eth_rx_header_err_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive Header Error Packet Counter"
    
        def description(self):
            return "Count number of Ethernet MAC header error packets detected"
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0xffffffff

        class _RxEthErrorPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthErrorPk"
            
            def description(self):
                return "This counter count the number of packets with MAC Header Error."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthErrorPk"] = _AF6CCI0031_RD_PMC._eth_rx_header_err_cnt_ro._RxEthErrorPk()
            return allFields

    class _eth_rx_header_err_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive Header Error Packet Counter"
    
        def description(self):
            return "Count number of Ethernet MAC header error packets detected"
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _RxEthErrorPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthErrorPk"
            
            def description(self):
                return "This counter count the number of packets with MAC Header Error."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthErrorPk"] = _AF6CCI0031_RD_PMC._eth_rx_header_err_cnt_r2c._RxEthErrorPk()
            return allFields

    class _eth_rx_psnheader_err_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive PSN Header Error Packet Counter"
    
        def description(self):
            return "Count number of PSN header error packets detected"
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000004
            
        def endAddress(self):
            return 0xffffffff

        class _RxPsnErrorPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPsnErrorPk"
            
            def description(self):
                return "This counter count the number of packets with PSN Header error."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPsnErrorPk"] = _AF6CCI0031_RD_PMC._eth_rx_psnheader_err_cnt_ro._RxPsnErrorPk()
            return allFields

    class _eth_rx_psnheader_err_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive PSN Header Error Packet Counter"
    
        def description(self):
            return "Count number of PSN header error packets detected"
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000005
            
        def endAddress(self):
            return 0xffffffff

        class _RxPsnErrorPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPsnErrorPk"
            
            def description(self):
                return "This counter count the number of packets with PSN Header error."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPsnErrorPk"] = _AF6CCI0031_RD_PMC._eth_rx_psnheader_err_cnt_r2c._RxPsnErrorPk()
            return allFields

    class _eth_rx_udpport_err_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive UDP Port Error Packet Counter"
    
        def description(self):
            return "Count number of UDP port error packets detected"
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000006
            
        def endAddress(self):
            return 0xffffffff

        class _RxUdpErrorPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxUdpErrorPk"
            
            def description(self):
                return "This counter count the number of packets with UDP port error."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxUdpErrorPk"] = _AF6CCI0031_RD_PMC._eth_rx_udpport_err_cnt_ro._RxUdpErrorPk()
            return allFields

    class _eth_rx_udpport_err_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive UDP Port Error Packet Counter"
    
        def description(self):
            return "Count number of UDP port error packets detected"
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000007
            
        def endAddress(self):
            return 0xffffffff

        class _RxUdpErrorPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxUdpErrorPk"
            
            def description(self):
                return "This counter count the number of packets with UDP port error."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxUdpErrorPk"] = _AF6CCI0031_RD_PMC._eth_rx_udpport_err_cnt_r2c._RxUdpErrorPk()
            return allFields

    class _eth_rx_oam_pkt_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "4.1.5. Ethernet Receive OAM Packet Counter"
    
        def description(self):
            return "Count number of classified OAM packets (ARP, ICMP,...) received from Ethernet port"
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000e
            
        def endAddress(self):
            return 0xffffffff

        class _RxOamPkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxOamPkt"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxOamPkt"] = _AF6CCI0031_RD_PMC._eth_rx_oam_pkt_cnt_ro._RxOamPkt()
            return allFields

    class _eth_rx_oam_pkt_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "4.1.5. Ethernet Receive OAM Packet Counter"
    
        def description(self):
            return "Count number of classified OAM packets (ARP, ICMP,...) received from Ethernet port"
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000f
            
        def endAddress(self):
            return 0xffffffff

        class _RxOamPkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxOamPkt"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxOamPkt"] = _AF6CCI0031_RD_PMC._eth_rx_oam_pkt_cnt_r2c._RxOamPkt()
            return allFields

    class _eth_tx_oam_pkt_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit OAM Packet Counter"
    
        def description(self):
            return "Count number of transmitted OAM packets (ARP, ICMP,...) to Ethernet port"
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000010
            
        def endAddress(self):
            return 0xffffffff

        class _TxOamPkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxOamPkt"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxOamPkt"] = _AF6CCI0031_RD_PMC._eth_tx_oam_pkt_cnt_ro._TxOamPkt()
            return allFields

    class _eth_tx_oam_pkt_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit OAM Packet Counter"
    
        def description(self):
            return "Count number of transmitted OAM packets (ARP, ICMP,...) to Ethernet port"
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000011
            
        def endAddress(self):
            return 0xffffffff

        class _TxOamPkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxOamPkt"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxOamPkt"] = _AF6CCI0031_RD_PMC._eth_tx_oam_pkt_cnt_r2c._TxOamPkt()
            return allFields

    class _eth_rx_total_byte_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "4.1.7. Ethernet Receive Total Byte Counter"
    
        def description(self):
            return "Count total number of bytes received from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000012
            
        def endAddress(self):
            return 0xffffffff

        class _RxByte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxByte"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxByte"] = _AF6CCI0031_RD_PMC._eth_rx_total_byte_cnt_ro._RxByte()
            return allFields

    class _eth_rx_total_byte_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "4.1.7. Ethernet Receive Total Byte Counter"
    
        def description(self):
            return "Count total number of bytes received from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000013
            
        def endAddress(self):
            return 0xffffffff

        class _RxByte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxByte"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxByte"] = _AF6CCI0031_RD_PMC._eth_rx_total_byte_cnt_r2c._RxByte()
            return allFields

    class _eth_rx_total_pkt_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "4.1.7. Ethernet Receive Total Packt Counter"
    
        def description(self):
            return "Count total number of packets received from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000014
            
        def endAddress(self):
            return 0xffffffff

        class _RxPacket(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPacket"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPacket"] = _AF6CCI0031_RD_PMC._eth_rx_total_pkt_cnt_ro._RxPacket()
            return allFields

    class _eth_rx_total_pkt_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "4.1.7. Ethernet Receive Total Packt Counter"
    
        def description(self):
            return "Count total number of packets received from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000015
            
        def endAddress(self):
            return 0xffffffff

        class _RxPacket(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPacket"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPacket"] = _AF6CCI0031_RD_PMC._eth_rx_total_pkt_cnt_r2c._RxPacket()
            return allFields

    class _eth_rx_cla_pkt_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive Classified Packet Counter"
    
        def description(self):
            return "Count number of classified packets (PW packets plus OAM packets) from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000016
            
        def endAddress(self):
            return 0xffffffff

        class _RxClassifyPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxClassifyPk"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxClassifyPk"] = _AF6CCI0031_RD_PMC._eth_rx_cla_pkt_cnt_ro._RxClassifyPk()
            return allFields

    class _eth_rx_cla_pkt_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive Classified Packet Counter"
    
        def description(self):
            return "Count number of classified packets (PW packets plus OAM packets) from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000017
            
        def endAddress(self):
            return 0xffffffff

        class _RxClassifyPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxClassifyPk"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxClassifyPk"] = _AF6CCI0031_RD_PMC._eth_rx_cla_pkt_cnt_r2c._RxClassifyPk()
            return allFields

    class _eth_tx_byte_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit Byte Counter"
    
        def description(self):
            return "Count total number of  bytes transmitted to Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000018
            
        def endAddress(self):
            return 0xffffffff

        class _TxByte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxByte"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxByte"] = _AF6CCI0031_RD_PMC._eth_tx_byte_cnt_ro._TxByte()
            return allFields

    class _eth_tx_byte_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit Byte Counter"
    
        def description(self):
            return "Count total number of  bytes transmitted to Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000019
            
        def endAddress(self):
            return 0xffffffff

        class _TxByte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxByte"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxByte"] = _AF6CCI0031_RD_PMC._eth_tx_byte_cnt_r2c._TxByte()
            return allFields

    class _eth_tx_pkt_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit Packet Counter"
    
        def description(self):
            return "Count total number of  packets transmitted to Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000001a
            
        def endAddress(self):
            return 0xffffffff

        class _TxPacket(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxPacket"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxPacket"] = _AF6CCI0031_RD_PMC._eth_tx_pkt_cnt_ro._TxPacket()
            return allFields

    class _eth_tx_pkt_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit Packet Counter"
    
        def description(self):
            return "Count total number of  packets transmitted to Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000001b
            
        def endAddress(self):
            return 0xffffffff

        class _TxPacket(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxPacket"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxPacket"] = _AF6CCI0031_RD_PMC._eth_tx_pkt_cnt_r2c._TxPacket()
            return allFields

    class _eth_rx_1to64byte_pkt_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive 1to64Byte Packet Counter"
    
        def description(self):
            return "Count total number of  received packets length from 1 to 64 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000020
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0031_RD_PMC._eth_rx_1to64byte_pkt_cnt_ro._PacketNum()
            return allFields

    class _eth_rx_1to64byte_pkt_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive 1to64Byte Packet Counter"
    
        def description(self):
            return "Count total number of  received packets length from 1 to 64 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000021
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0031_RD_PMC._eth_rx_1to64byte_pkt_cnt_r2c._PacketNum()
            return allFields

    class _eth_tx_1to64byte_pkt_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit 1to64Byte Packet Counter"
    
        def description(self):
            return "Count total number of  transmitted packets length from 1 to 64 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000030
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0031_RD_PMC._eth_tx_1to64byte_pkt_cnt_ro._PacketNum()
            return allFields

    class _eth_tx_1to64byte_pkt_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit 1to64Byte Packet Counter"
    
        def description(self):
            return "Count total number of  transmitted packets length from 1 to 64 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000031
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0031_RD_PMC._eth_tx_1to64byte_pkt_cnt_r2c._PacketNum()
            return allFields

    class _eth_rx_65to128byte_pkt_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive 65to128Byte Packet Counter"
    
        def description(self):
            return "Count total number of  received packets length from 65 to 128 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000022
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0031_RD_PMC._eth_rx_65to128byte_pkt_cnt_ro._PacketNum()
            return allFields

    class _eth_rx_65to128byte_pkt_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive 65to128Byte Packet Counter"
    
        def description(self):
            return "Count total number of  received packets length from 65 to 128 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000023
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0031_RD_PMC._eth_rx_65to128byte_pkt_cnt_r2c._PacketNum()
            return allFields

    class _eth_tx_65to128byte_pkt_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit 65to128Byte Packet Counter"
    
        def description(self):
            return "Count total number of  transmitted packets length from 65 to 128 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000032
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0031_RD_PMC._eth_tx_65to128byte_pkt_cnt_ro._PacketNum()
            return allFields

    class _eth_tx_65to128byte_pkt_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit 65to128Byte Packet Counter"
    
        def description(self):
            return "Count total number of  transmitted packets length from 65 to 128 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000033
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0031_RD_PMC._eth_tx_65to128byte_pkt_cnt_r2c._PacketNum()
            return allFields

    class _eth_rx_129to256byte_pkt_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive 129to256Byte Packet Counter"
    
        def description(self):
            return "Count total number of  received packets length from 129  to 256 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000024
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0031_RD_PMC._eth_rx_129to256byte_pkt_cnt_ro._PacketNum()
            return allFields

    class _eth_rx_129to256byte_pkt_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive 129to256Byte Packet Counter"
    
        def description(self):
            return "Count total number of  received packets length from 129  to 256 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000025
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0031_RD_PMC._eth_rx_129to256byte_pkt_cnt_r2c._PacketNum()
            return allFields

    class _eth_tx_129to256byte_pkt_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit 129to256Byte Packet Counter"
    
        def description(self):
            return "Count total number of  transmitted packets length from 129  to 256 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000034
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0031_RD_PMC._eth_tx_129to256byte_pkt_cnt_ro._PacketNum()
            return allFields

    class _eth_tx_129to256byte_pkt_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit 129to256Byte Packet Counter"
    
        def description(self):
            return "Count total number of  transmitted packets length from 129  to 256 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000035
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0031_RD_PMC._eth_tx_129to256byte_pkt_cnt_r2c._PacketNum()
            return allFields

    class _eth_rx_257to512byte_pkt_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive 257to512Byte Packet Counter"
    
        def description(self):
            return "Count total number of  received packets length from 257  to  512 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000026
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0031_RD_PMC._eth_rx_257to512byte_pkt_cnt_ro._PacketNum()
            return allFields

    class _eth_rx_257to512byte_pkt_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive 257to512Byte Packet Counter"
    
        def description(self):
            return "Count total number of  received packets length from 257  to  512 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000027
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0031_RD_PMC._eth_rx_257to512byte_pkt_cnt_r2c._PacketNum()
            return allFields

    class _eth_tx_257to512byte_pkt_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit 257to512Byte Packet Counter"
    
        def description(self):
            return "Count total number of  transmitted packets length from 257  to  512 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000036
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0031_RD_PMC._eth_tx_257to512byte_pkt_cnt_ro._PacketNum()
            return allFields

    class _eth_tx_257to512byte_pkt_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit 257to512Byte Packet Counter"
    
        def description(self):
            return "Count total number of  transmitted packets length from 257  to  512 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000037
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0031_RD_PMC._eth_tx_257to512byte_pkt_cnt_r2c._PacketNum()
            return allFields

    class _eth_rx_513to1024byte_pkt_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive 513to1024Byte Packet Counter"
    
        def description(self):
            return "Count total number of  received packets length from 513 to 1024 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000028
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0031_RD_PMC._eth_rx_513to1024byte_pkt_cnt_ro._PacketNum()
            return allFields

    class _eth_rx_513to1024byte_pkt_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive 513to1024Byte Packet Counter"
    
        def description(self):
            return "Count total number of  received packets length from 513 to 1024 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000029
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0031_RD_PMC._eth_rx_513to1024byte_pkt_cnt_r2c._PacketNum()
            return allFields

    class _eth_tx_513to1024byte_pkt_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit 513to1024Byte Packet Counter"
    
        def description(self):
            return "Count total number of  transmitted packets length from 513 to 1024 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000038
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0031_RD_PMC._eth_tx_513to1024byte_pkt_cnt_ro._PacketNum()
            return allFields

    class _eth_tx_513to1024byte_pkt_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit 513to1024Byte Packet Counter"
    
        def description(self):
            return "Count total number of  transmitted packets length from 513 to 1024 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000039
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0031_RD_PMC._eth_tx_513to1024byte_pkt_cnt_r2c._PacketNum()
            return allFields

    class _eth_rx_1025to1528byte_pkt_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive 1025to1528Byte Packet Counter"
    
        def description(self):
            return "Count total number of  received packets length from 1025 to 1528 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000002a
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0031_RD_PMC._eth_rx_1025to1528byte_pkt_cnt_ro._PacketNum()
            return allFields

    class _eth_rx_1025to1528byte_pkt_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive 1025to1528Byte Packet Counter"
    
        def description(self):
            return "Count total number of  received packets length from 1025 to 1528 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000002b
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0031_RD_PMC._eth_rx_1025to1528byte_pkt_cnt_r2c._PacketNum()
            return allFields

    class _eth_tx_1025to1528byte_pkt_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit 1025to1528Byte Packet Counter"
    
        def description(self):
            return "Count total number of  transmitted packets length from 1025 to 1528 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000003a
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0031_RD_PMC._eth_tx_1025to1528byte_pkt_cnt_ro._PacketNum()
            return allFields

    class _eth_tx_1025to1528byte_pkt_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit 1025to1528Byte Packet Counter"
    
        def description(self):
            return "Count total number of  transmitted packets length from 1025 to 1528 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000003b
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0031_RD_PMC._eth_tx_1025to1528byte_pkt_cnt_r2c._PacketNum()
            return allFields

    class _eth_rx_jumbobyte_pkt_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive jumboByte Packet Counter"
    
        def description(self):
            return "Count total number of  received packets length from jumbo bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000002c
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0031_RD_PMC._eth_rx_jumbobyte_pkt_cnt_ro._PacketNum()
            return allFields

    class _eth_rx_jumbobyte_pkt_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive jumboByte Packet Counter"
    
        def description(self):
            return "Count total number of  received packets length from jumbo bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000002d
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0031_RD_PMC._eth_rx_jumbobyte_pkt_cnt_r2c._PacketNum()
            return allFields

    class _eth_tx_jumbobyte_pkt_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit jumboByte Packet Counter"
    
        def description(self):
            return "Count total number of  transmitted packets length from jumbo bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000003c
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0031_RD_PMC._eth_tx_jumbobyte_pkt_cnt_ro._PacketNum()
            return allFields

    class _eth_tx_jumbobyte_pkt_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit jumboByte Packet Counter"
    
        def description(self):
            return "Count total number of  transmitted packets length from jumbo bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000003d
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0031_RD_PMC._eth_tx_jumbobyte_pkt_cnt_r2c._PacketNum()
            return allFields

    class _Pseudowire_Transmit_Good_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit Good Packet Counter"
    
        def description(self):
            return "Count number of CESoETH packets transmitted to Ethernet side."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x010800 + PwId"
            
        def startAddress(self):
            return 0x00010800
            
        def endAddress(self):
            return 0x00010fff

        class _TxPwGoodPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxPwGoodPk"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxPwGoodPk"] = _AF6CCI0031_RD_PMC._Pseudowire_Transmit_Good_Packet_Counter_ro._TxPwGoodPk()
            return allFields

    class _Pseudowire_Transmit_Good_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit Good Packet Counter"
    
        def description(self):
            return "Count number of CESoETH packets transmitted to Ethernet side."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x010000 + PwId"
            
        def startAddress(self):
            return 0x00010000
            
        def endAddress(self):
            return 0x000107ff

        class _TxPwGoodPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxPwGoodPk"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxPwGoodPk"] = _AF6CCI0031_RD_PMC._Pseudowire_Transmit_Good_Packet_Counter_rc._TxPwGoodPk()
            return allFields

    class _Pseudowire_Receive_Payload_Octet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Payload Octet Counter"
    
        def description(self):
            return "Count number of pseudowire payload octet received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x011800 + PwId"
            
        def startAddress(self):
            return 0x00011800
            
        def endAddress(self):
            return 0x00011fff

        class _RxPwPayOct(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwPayOct"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwPayOct"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Payload_Octet_Counter_ro._RxPwPayOct()
            return allFields

    class _Pseudowire_Receive_Payload_Octet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Payload Octet Counter"
    
        def description(self):
            return "Count number of pseudowire payload octet received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x011000 + PwId"
            
        def startAddress(self):
            return 0x00011000
            
        def endAddress(self):
            return 0x000117ff

        class _RxPwPayOct(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwPayOct"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwPayOct"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Payload_Octet_Counter_rc._RxPwPayOct()
            return allFields

    class _Pseudowire_Transmit_LBit_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit LBit Packet Counter"
    
        def description(self):
            return "Count number of CESoETH packets transmitted to Ethernet side."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x012800 + PwId"
            
        def startAddress(self):
            return 0x00012800
            
        def endAddress(self):
            return 0x00012fff

        class _TxPwLbitPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxPwLbitPk"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxPwLbitPk"] = _AF6CCI0031_RD_PMC._Pseudowire_Transmit_LBit_Packet_Counter_ro._TxPwLbitPk()
            return allFields

    class _Pseudowire_Transmit_LBit_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit LBit Packet Counter"
    
        def description(self):
            return "Count number of CESoETH packets transmitted to Ethernet side."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x012000 + PwId"
            
        def startAddress(self):
            return 0x00012000
            
        def endAddress(self):
            return 0x000127ff

        class _TxPwLbitPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxPwLbitPk"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxPwLbitPk"] = _AF6CCI0031_RD_PMC._Pseudowire_Transmit_LBit_Packet_Counter_rc._TxPwLbitPk()
            return allFields

    class _Pseudowire_Transmit_RBit_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit RBit Packet Counter"
    
        def description(self):
            return "Count number of CESoETH packets transmitted to Ethernet side."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x013800 + PwId"
            
        def startAddress(self):
            return 0x00013800
            
        def endAddress(self):
            return 0x00013fff

        class _TxPwRbitPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxPwRbitPk"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxPwRbitPk"] = _AF6CCI0031_RD_PMC._Pseudowire_Transmit_RBit_Packet_Counter_ro._TxPwRbitPk()
            return allFields

    class _Pseudowire_Transmit_RBit_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit RBit Packet Counter"
    
        def description(self):
            return "Count number of CESoETH packets transmitted to Ethernet side."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x013000 + PwId"
            
        def startAddress(self):
            return 0x00013000
            
        def endAddress(self):
            return 0x000137ff

        class _TxPwRbitPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxPwRbitPk"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxPwRbitPk"] = _AF6CCI0031_RD_PMC._Pseudowire_Transmit_RBit_Packet_Counter_rc._TxPwRbitPk()
            return allFields

    class _Pseudowire_Transmit_MBit_NBit_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit MBit_NBit Packet Counter"
    
        def description(self):
            return "Count number of CESoETH packets transmitted to Ethernet side."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x014800 + PwId"
            
        def startAddress(self):
            return 0x00014800
            
        def endAddress(self):
            return 0x00014fff

        class _TxPwMbit_NBitPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxPwMbit_NBitPk"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxPwMbit_NBitPk"] = _AF6CCI0031_RD_PMC._Pseudowire_Transmit_MBit_NBit_Packet_Counter_ro._TxPwMbit_NBitPk()
            return allFields

    class _Pseudowire_Transmit_MBit_NBit_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit MBit_NBit Packet Counter"
    
        def description(self):
            return "Count number of CESoETH packets transmitted to Ethernet side."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x014000 + PwId"
            
        def startAddress(self):
            return 0x00014000
            
        def endAddress(self):
            return 0x000147ff

        class _TxPwMbit_NBitPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxPwMbit_NBitPk"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxPwMbit_NBitPk"] = _AF6CCI0031_RD_PMC._Pseudowire_Transmit_MBit_NBit_Packet_Counter_rc._TxPwMbit_NBitPk()
            return allFields

    class _Pseudowire_Transmit_PBit_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit PBit Packet Counter"
    
        def description(self):
            return "Count number of CESoETH packets transmitted to Ethernet side."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x015800 + PwId"
            
        def startAddress(self):
            return 0x00015800
            
        def endAddress(self):
            return 0x00015fff

        class _TxPwPbitPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxPwPbitPk"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxPwPbitPk"] = _AF6CCI0031_RD_PMC._Pseudowire_Transmit_PBit_Packet_Counter_ro._TxPwPbitPk()
            return allFields

    class _Pseudowire_Transmit_PBit_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit PBit Packet Counter"
    
        def description(self):
            return "Count number of CESoETH packets transmitted to Ethernet side."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x015000 + PwId"
            
        def startAddress(self):
            return 0x00015000
            
        def endAddress(self):
            return 0x000157ff

        class _TxPwPbitPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxPwPbitPk"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxPwPbitPk"] = _AF6CCI0031_RD_PMC._Pseudowire_Transmit_PBit_Packet_Counter_rc._TxPwPbitPk()
            return allFields

    class _Pseudowire_Receive_Good_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Good Packet Counter"
    
        def description(self):
            return "Count number of CESoETH packets received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x020800 + PwId"
            
        def startAddress(self):
            return 0x00020800
            
        def endAddress(self):
            return 0x00020fff

        class _RxPwGoodPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwGoodPk"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwGoodPk"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Good_Packet_Counter_ro._RxPwGoodPk()
            return allFields

    class _Pseudowire_Receive_Good_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Good Packet Counter"
    
        def description(self):
            return "Count number of CESoETH packets received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x020000 + PwId"
            
        def startAddress(self):
            return 0x00020000
            
        def endAddress(self):
            return 0x000207ff

        class _RxPwGoodPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwGoodPk"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwGoodPk"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Good_Packet_Counter_rc._RxPwGoodPk()
            return allFields

    class _Pseudowire_Receive_Good_Byte_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Good Byte Counter"
    
        def description(self):
            return "Count number of CESoETH packets received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x021800 + PwId"
            
        def startAddress(self):
            return 0x00021800
            
        def endAddress(self):
            return 0x00021fff

        class _RxPwBytePk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwBytePk"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwBytePk"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Good_Byte_Counter_ro._RxPwBytePk()
            return allFields

    class _Pseudowire_Receive_Good_Byte_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Good Byte Counter"
    
        def description(self):
            return "Count number of CESoETH packets received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x021000 + PwId"
            
        def startAddress(self):
            return 0x00021000
            
        def endAddress(self):
            return 0x000217ff

        class _RxPwBytePk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwBytePk"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwBytePk"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Good_Byte_Counter_rc._RxPwBytePk()
            return allFields

    class _Pseudowire_Receive_LOFS_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive LOFS Counter"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x022800 + PwId"
            
        def startAddress(self):
            return 0x00022800
            
        def endAddress(self):
            return 0x00022fff

        class _RxPwLofsEvent(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwLofsEvent"
            
            def description(self):
                return "This counter count the number of transitions from normal sate to loss of frame state"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwLofsEvent"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_LOFS_Counter_ro._RxPwLofsEvent()
            return allFields

    class _Pseudowire_Receive_LOFS_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive LOFS Counter"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x022000 + PwId"
            
        def startAddress(self):
            return 0x00022000
            
        def endAddress(self):
            return 0x000227ff

        class _RxPwLofsEvent(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwLofsEvent"
            
            def description(self):
                return "This counter count the number of transitions from normal sate to loss of frame state"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwLofsEvent"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_LOFS_Counter_rc._RxPwLofsEvent()
            return allFields

    class _Pseudowire_Receive_LBit_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive LBit Packet Counter"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x023800 + PwId"
            
        def startAddress(self):
            return 0x00023800
            
        def endAddress(self):
            return 0x00023fff

        class _RxPwLbitEvent(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwLbitEvent"
            
            def description(self):
                return "This counter count the number of Receive LBit Packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwLbitEvent"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_LBit_Packet_Counter_ro._RxPwLbitEvent()
            return allFields

    class _Pseudowire_Receive_LBit_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive LBit Packet Counter"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x023000 + PwId"
            
        def startAddress(self):
            return 0x00023000
            
        def endAddress(self):
            return 0x000237ff

        class _RxPwLbitEvent(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwLbitEvent"
            
            def description(self):
                return "This counter count the number of Receive LBit Packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwLbitEvent"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_LBit_Packet_Counter_rc._RxPwLbitEvent()
            return allFields

    class _Pseudowire_Receive_M_NBit_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive M_NBit Packet Counter"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x024800 + PwId"
            
        def startAddress(self):
            return 0x00024800
            
        def endAddress(self):
            return 0x00024fff

        class _RxPwM_NbitEvent(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwM_NbitEvent"
            
            def description(self):
                return "This counter count the number of Receive M_NBit Packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwM_NbitEvent"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_M_NBit_Packet_Counter_ro._RxPwM_NbitEvent()
            return allFields

    class _Pseudowire_Receive_M_NBit_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive M_NBit Packet Counter"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x024000 + PwId"
            
        def startAddress(self):
            return 0x00024000
            
        def endAddress(self):
            return 0x000247ff

        class _RxPwM_NbitEvent(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwM_NbitEvent"
            
            def description(self):
                return "This counter count the number of Receive M_NBit Packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwM_NbitEvent"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_M_NBit_Packet_Counter_rc._RxPwM_NbitEvent()
            return allFields

    class _Pseudowire_Receive_PBit_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive PBit Packet Counter"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x025800 + PwId"
            
        def startAddress(self):
            return 0x00025800
            
        def endAddress(self):
            return 0x00025fff

        class _RxPwPbitEvent(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwPbitEvent"
            
            def description(self):
                return "This counter count the number of Receive PBit Packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwPbitEvent"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_PBit_Packet_Counter_ro._RxPwPbitEvent()
            return allFields

    class _Pseudowire_Receive_PBit_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive PBit Packet Counter"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x025000 + PwId"
            
        def startAddress(self):
            return 0x00025000
            
        def endAddress(self):
            return 0x000257ff

        class _RxPwPbitEvent(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwPbitEvent"
            
            def description(self):
                return "This counter count the number of Receive PBit Packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwPbitEvent"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_PBit_Packet_Counter_rc._RxPwPbitEvent()
            return allFields

    class _Pseudowire_Receive_RBit_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive RBit Packet Counter"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x026800 + PwId"
            
        def startAddress(self):
            return 0x00026800
            
        def endAddress(self):
            return 0x00026fff

        class _RxPwRbitEvent(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwRbitEvent"
            
            def description(self):
                return "This counter count the number of Receive RBit Packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwRbitEvent"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_RBit_Packet_Counter_ro._RxPwRbitEvent()
            return allFields

    class _Pseudowire_Receive_RBit_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive RBit Packet Counter"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x026000 + PwId"
            
        def startAddress(self):
            return 0x00026000
            
        def endAddress(self):
            return 0x000267ff

        class _RxPwRbitEvent(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwRbitEvent"
            
            def description(self):
                return "This counter count the number of Receive RBit Packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwRbitEvent"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_RBit_Packet_Counter_rc._RxPwRbitEvent()
            return allFields

    class _Pseudowire_Receive_Reorder_Drop_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Reorder Drop Packet Counter"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x027800 + PwId"
            
        def startAddress(self):
            return 0x00027800
            
        def endAddress(self):
            return 0x00027fff

        class _RxPwReorDropEvent(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwReorDropEvent"
            
            def description(self):
                return "This counter count the number of Receive Reorder Drop Packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwReorDropEvent"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Reorder_Drop_Packet_Counter_ro._RxPwReorDropEvent()
            return allFields

    class _Pseudowire_Receive_Reorder_Drop_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Reorder Drop Packet Counter"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x027000 + PwId"
            
        def startAddress(self):
            return 0x00027000
            
        def endAddress(self):
            return 0x000277ff

        class _RxPwReorDropEvent(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwReorDropEvent"
            
            def description(self):
                return "This counter count the number of Receive Reorder Drop Packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwReorDropEvent"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Reorder_Drop_Packet_Counter_rc._RxPwReorDropEvent()
            return allFields

    class _Pseudowire_Receive_Reorder_Out_Of_Sequence_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Reorder Out Of Sequence Packet Counter"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x028800 + PwId"
            
        def startAddress(self):
            return 0x00028800
            
        def endAddress(self):
            return 0x00028fff

        class _RxPwReorOSeqEvent(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwReorOSeqEvent"
            
            def description(self):
                return "This counter count the number of Receive Reorder Out Of Sequence Packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwReorOSeqEvent"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Reorder_Out_Of_Sequence_Packet_Counter_ro._RxPwReorOSeqEvent()
            return allFields

    class _Pseudowire_Receive_Reorder_Out_Of_Sequence_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Reorder Out Of Sequence Packet Counter"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x028000 + PwId"
            
        def startAddress(self):
            return 0x00028000
            
        def endAddress(self):
            return 0x000287ff

        class _RxPwReorOSeqEvent(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwReorOSeqEvent"
            
            def description(self):
                return "This counter count the number of Receive Reorder Out Of Sequence Packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwReorOSeqEvent"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Reorder_Out_Of_Sequence_Packet_Counter_rc._RxPwReorOSeqEvent()
            return allFields

    class _Pseudowire_Receive_Reorder_Lost_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Reorder Lost Packet Counter"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x029800 + PwId"
            
        def startAddress(self):
            return 0x00029800
            
        def endAddress(self):
            return 0x00029fff

        class _RxPwReorLostEvent(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwReorLostEvent"
            
            def description(self):
                return "This counter count the number of Receive Reorder Lost Packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwReorLostEvent"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Reorder_Lost_Packet_Counter_ro._RxPwReorLostEvent()
            return allFields

    class _Pseudowire_Receive_Reorder_Lost_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Reorder Lost Packet Counter"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x029000 + PwId"
            
        def startAddress(self):
            return 0x00029000
            
        def endAddress(self):
            return 0x000297ff

        class _RxPwReorLostEvent(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwReorLostEvent"
            
            def description(self):
                return "This counter count the number of Receive Reorder Lost Packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwReorLostEvent"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Reorder_Lost_Packet_Counter_rc._RxPwReorLostEvent()
            return allFields

    class _Pseudowire_Receive_Jitter_Buffer_OverRun_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Jitter Buffer OverRun Counter"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x02A800 + PwId"
            
        def startAddress(self):
            return 0x0002a800
            
        def endAddress(self):
            return 0x0002afff

        class _RxPwOverRunEvent(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwOverRunEvent"
            
            def description(self):
                return "This counter count the number of Receive Jitter Buffer Overrun"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwOverRunEvent"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Jitter_Buffer_OverRun_Counter_ro._RxPwOverRunEvent()
            return allFields

    class _Pseudowire_Receive_Jitter_Buffer_OverRun_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Jitter Buffer OverRun Counter"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x02A000 + PwId"
            
        def startAddress(self):
            return 0x0002a000
            
        def endAddress(self):
            return 0x0002a7ff

        class _RxPwOverRunEvent(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwOverRunEvent"
            
            def description(self):
                return "This counter count the number of Receive Jitter Buffer Overrun"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwOverRunEvent"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Jitter_Buffer_OverRun_Counter_rc._RxPwOverRunEvent()
            return allFields

    class _Pseudowire_Receive_Jitter_Buffer_UnderRun_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Jitter Buffer UnderRun Counter"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x02B800 + PwId"
            
        def startAddress(self):
            return 0x0002b800
            
        def endAddress(self):
            return 0x0002bfff

        class _RxPwUnderRunEvent(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwUnderRunEvent"
            
            def description(self):
                return "This counter count the number of Receive Jitter Buffer UnderRun"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwUnderRunEvent"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Jitter_Buffer_UnderRun_Counter_ro._RxPwUnderRunEvent()
            return allFields

    class _Pseudowire_Receive_Jitter_Buffer_UnderRun_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Jitter Buffer UnderRun Counter"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x02B000 + PwId"
            
        def startAddress(self):
            return 0x0002b000
            
        def endAddress(self):
            return 0x0002b7ff

        class _RxPwUnderRunEvent(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwUnderRunEvent"
            
            def description(self):
                return "This counter count the number of Receive Jitter Buffer UnderRun"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwUnderRunEvent"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Jitter_Buffer_UnderRun_Counter_rc._RxPwUnderRunEvent()
            return allFields

    class _Pseudowire_Receive_Malform_packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Malform packet Counter"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x02C800 + PwId"
            
        def startAddress(self):
            return 0x0002c800
            
        def endAddress(self):
            return 0x0002cfff

        class _RxPwMalformEvent(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwMalformEvent"
            
            def description(self):
                return "This counter count the number of Receive Malform packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwMalformEvent"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Malform_packet_Counter_ro._RxPwMalformEvent()
            return allFields

    class _Pseudowire_Receive_Malform_packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Malform packet Counter"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x02C000 + PwId"
            
        def startAddress(self):
            return 0x0002c000
            
        def endAddress(self):
            return 0x0002c7ff

        class _RxPwMalformEvent(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwMalformEvent"
            
            def description(self):
                return "This counter count the number of Receive Malform packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwMalformEvent"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Malform_packet_Counter_rc._RxPwMalformEvent()
            return allFields

    class _Pseudowire_Receive_Stray_packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Stray packet Counter"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x02D800 + PwId"
            
        def startAddress(self):
            return 0x0002d800
            
        def endAddress(self):
            return 0x0002dfff

        class _RxPwStrayEvent(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwStrayEvent"
            
            def description(self):
                return "This counter count the number of Receive Stray packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwStrayEvent"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Stray_packet_Counter_ro._RxPwStrayEvent()
            return allFields

    class _Pseudowire_Receive_Stray_packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Stray packet Counter"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x02D000 + PwId"
            
        def startAddress(self):
            return 0x0002d000
            
        def endAddress(self):
            return 0x0002d7ff

        class _RxPwStrayEvent(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwStrayEvent"
            
            def description(self):
                return "This counter count the number of Receive Stray packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwStrayEvent"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Stray_packet_Counter_rc._RxPwStrayEvent()
            return allFields

    class _Pseudowire_Receive_Duplicate_packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Duplicate packet Counter"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x02E800 + PwId"
            
        def startAddress(self):
            return 0x0002e800
            
        def endAddress(self):
            return 0x0002efff

        class _RxPwDupliateEvent(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwDupliateEvent"
            
            def description(self):
                return "This counter count the number of Receive Duplicate packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwDupliateEvent"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Duplicate_packet_Counter_ro._RxPwDupliateEvent()
            return allFields

    class _Pseudowire_Receive_Duplicate_packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Duplicate packet Counter"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x02E000 + PwId"
            
        def startAddress(self):
            return 0x0002e000
            
        def endAddress(self):
            return 0x0002e7ff

        class _RxPwDupliateEvent(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwDupliateEvent"
            
            def description(self):
                return "This counter count the number of Receive Duplicate packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwDupliateEvent"] = _AF6CCI0031_RD_PMC._Pseudowire_Receive_Duplicate_packet_Counter_rc._RxPwDupliateEvent()
            return allFields

    class _Counter_Per_Alarm_Interrupt_Enable_Control(AtRegister.AtRegister):
        def name(self):
            return "Counter Per Alarm Interrupt Enable Control"
    
        def description(self):
            return "This is per Alarm interrupt enable of pseudowires. Each register is used to store 4 bits to enable interrupts when the related alarms in pseudowires happen."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x040000 +  GrpID*32 + BitID"
            
        def startAddress(self):
            return 0x00040000
            
        def endAddress(self):
            return 0xffffffff

        class _StrayStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "StrayStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable Stray packet event to generate an interrupt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MalformStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "MalformStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable Malform packet event to generate an interrupt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MbitStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "MbitStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable Mbit packet event to generate an interrupt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RbitStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "RbitStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable Rbit packet event to generate an interrupt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LofsSyncStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "LofsSyncStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable Lost of frame Sync event to generate an interrupt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _UnderrunStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "UnderrunStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable change jitter buffer state event from normal to underrun and vice versa in the related pseudowire to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OverrunStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "OverrunStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable change jitter buffer state event from normal to overrun and vice versa in the related pseudowire to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LofsStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "LofsStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable change lost of frame state(LOFS) event from normal to LOFS and vice versa in the related pseudowire to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LbitStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LbitStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable Lbit packet event to generate an interrupt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["StrayStateChgIntrEn"] = _AF6CCI0031_RD_PMC._Counter_Per_Alarm_Interrupt_Enable_Control._StrayStateChgIntrEn()
            allFields["MalformStateChgIntrEn"] = _AF6CCI0031_RD_PMC._Counter_Per_Alarm_Interrupt_Enable_Control._MalformStateChgIntrEn()
            allFields["MbitStateChgIntrEn"] = _AF6CCI0031_RD_PMC._Counter_Per_Alarm_Interrupt_Enable_Control._MbitStateChgIntrEn()
            allFields["RbitStateChgIntrEn"] = _AF6CCI0031_RD_PMC._Counter_Per_Alarm_Interrupt_Enable_Control._RbitStateChgIntrEn()
            allFields["LofsSyncStateChgIntrEn"] = _AF6CCI0031_RD_PMC._Counter_Per_Alarm_Interrupt_Enable_Control._LofsSyncStateChgIntrEn()
            allFields["UnderrunStateChgIntrEn"] = _AF6CCI0031_RD_PMC._Counter_Per_Alarm_Interrupt_Enable_Control._UnderrunStateChgIntrEn()
            allFields["OverrunStateChgIntrEn"] = _AF6CCI0031_RD_PMC._Counter_Per_Alarm_Interrupt_Enable_Control._OverrunStateChgIntrEn()
            allFields["LofsStateChgIntrEn"] = _AF6CCI0031_RD_PMC._Counter_Per_Alarm_Interrupt_Enable_Control._LofsStateChgIntrEn()
            allFields["LbitStateChgIntrEn"] = _AF6CCI0031_RD_PMC._Counter_Per_Alarm_Interrupt_Enable_Control._LbitStateChgIntrEn()
            return allFields

    class _Counter_Per_Alarm_Interrupt_Status(AtRegister.AtRegister):
        def name(self):
            return "Counter Per Alarm Interrupt Status"
    
        def description(self):
            return "This is per Alarm interrupt enable of pseudowires. Each register is used to store 4 bits to enable interrupts when the related alarms in pseudowires happen."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x040800 +  GrpID*32 + BitID"
            
        def startAddress(self):
            return 0x00040800
            
        def endAddress(self):
            return 0xffffffff

        class _StrayStateChgIntrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "StrayStateChgIntrSta"
            
            def description(self):
                return "Set 1 when Stray packet event is detected in the pseudowire"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MalformStateChgIntrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "MalformStateChgIntrSta"
            
            def description(self):
                return "Set 1 when Malform packet event is detected in the pseudowire"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MbitStateChgIntrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "MbitStateChgIntrSta"
            
            def description(self):
                return "Set 1 when a Mbit packet event is detected in the pseudowire"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RbitStateChgIntrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "RbitStateChgIntrSta"
            
            def description(self):
                return "Set 1 when a Rbit packet event is detected in the pseudowire"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LofsSyncStateChgIntrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "LofsSyncStateChgIntrSta"
            
            def description(self):
                return "Set 1 when a lost of frame Sync event is detected in the pseudowire"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _UnderrunStateChgIntrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "UnderrunStateChgIntrSta"
            
            def description(self):
                return "Set 1 when there is a change in jitter buffer underrun state  in the related pseudowire"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OverrunStateChgIntrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "OverrunStateChgIntrSta"
            
            def description(self):
                return "Set 1 when there is a change in jitter buffer overrun state in the related pseudowire"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LofsStateChgIntrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "LofsStateChgIntrSta"
            
            def description(self):
                return "Set 1 when there is a change in lost of frame state(LOFS) in the related pseudowire"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LbitStateChgIntrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LbitStateChgIntrSta"
            
            def description(self):
                return "Set 1 when a Lbit packet event is detected in the pseudowire"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["StrayStateChgIntrSta"] = _AF6CCI0031_RD_PMC._Counter_Per_Alarm_Interrupt_Status._StrayStateChgIntrSta()
            allFields["MalformStateChgIntrSta"] = _AF6CCI0031_RD_PMC._Counter_Per_Alarm_Interrupt_Status._MalformStateChgIntrSta()
            allFields["MbitStateChgIntrSta"] = _AF6CCI0031_RD_PMC._Counter_Per_Alarm_Interrupt_Status._MbitStateChgIntrSta()
            allFields["RbitStateChgIntrSta"] = _AF6CCI0031_RD_PMC._Counter_Per_Alarm_Interrupt_Status._RbitStateChgIntrSta()
            allFields["LofsSyncStateChgIntrSta"] = _AF6CCI0031_RD_PMC._Counter_Per_Alarm_Interrupt_Status._LofsSyncStateChgIntrSta()
            allFields["UnderrunStateChgIntrSta"] = _AF6CCI0031_RD_PMC._Counter_Per_Alarm_Interrupt_Status._UnderrunStateChgIntrSta()
            allFields["OverrunStateChgIntrSta"] = _AF6CCI0031_RD_PMC._Counter_Per_Alarm_Interrupt_Status._OverrunStateChgIntrSta()
            allFields["LofsStateChgIntrSta"] = _AF6CCI0031_RD_PMC._Counter_Per_Alarm_Interrupt_Status._LofsStateChgIntrSta()
            allFields["LbitStateChgIntrSta"] = _AF6CCI0031_RD_PMC._Counter_Per_Alarm_Interrupt_Status._LbitStateChgIntrSta()
            return allFields

    class _Counter_Per_Alarm_Current_Status(AtRegister.AtRegister):
        def name(self):
            return "Counter Per Alarm Current Status"
    
        def description(self):
            return "This is per Alarm interrupt enable of pseudowires. Each register is used to store 4 bits to enable interrupts when the related alarms in pseudowires happen."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x041000 +  GrpID*32 + BitID"
            
        def startAddress(self):
            return 0x00041000
            
        def endAddress(self):
            return 0xffffffff

        class _StrayCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "StrayCurStatus"
            
            def description(self):
                return "Stray packet state current status in the related pseudowire."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MalformCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "MalformCurStatus"
            
            def description(self):
                return "Malform packet state current status in the related pseudowire."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MbitCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "MbitCurStatus"
            
            def description(self):
                return "Mbit packet state current status in the related pseudowire."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RbitCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "RbitCurStatus"
            
            def description(self):
                return "Rbit packet state current status in the related pseudowire."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LofsSyncCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "LofsSyncCurStatus"
            
            def description(self):
                return "Lost of frame Sync state current status in the related pseudowire."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _UnderrunCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "UnderrunCurStatus"
            
            def description(self):
                return "Jitter buffer underrun current status in the related pseudowire."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OverrunCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "OverrunCurStatus"
            
            def description(self):
                return "Jitter buffer overrun current status in the related pseudowire."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LofsStateCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "LofsStateCurStatus"
            
            def description(self):
                return "Lost of frame state current status in the related pseudowire."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LbitStateCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LbitStateCurStatus"
            
            def description(self):
                return "a Lbit packet state current status in the related pseudowire"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["StrayCurStatus"] = _AF6CCI0031_RD_PMC._Counter_Per_Alarm_Current_Status._StrayCurStatus()
            allFields["MalformCurStatus"] = _AF6CCI0031_RD_PMC._Counter_Per_Alarm_Current_Status._MalformCurStatus()
            allFields["MbitCurStatus"] = _AF6CCI0031_RD_PMC._Counter_Per_Alarm_Current_Status._MbitCurStatus()
            allFields["RbitCurStatus"] = _AF6CCI0031_RD_PMC._Counter_Per_Alarm_Current_Status._RbitCurStatus()
            allFields["LofsSyncCurStatus"] = _AF6CCI0031_RD_PMC._Counter_Per_Alarm_Current_Status._LofsSyncCurStatus()
            allFields["UnderrunCurStatus"] = _AF6CCI0031_RD_PMC._Counter_Per_Alarm_Current_Status._UnderrunCurStatus()
            allFields["OverrunCurStatus"] = _AF6CCI0031_RD_PMC._Counter_Per_Alarm_Current_Status._OverrunCurStatus()
            allFields["LofsStateCurStatus"] = _AF6CCI0031_RD_PMC._Counter_Per_Alarm_Current_Status._LofsStateCurStatus()
            allFields["LbitStateCurStatus"] = _AF6CCI0031_RD_PMC._Counter_Per_Alarm_Current_Status._LbitStateCurStatus()
            return allFields

    class _Counter_Interrupt_OR_Status(AtRegister.AtRegister):
        def name(self):
            return "Counter Interrupt OR Status"
    
        def description(self):
            return "This is per Alarm interrupt enable of pseudowires. Each register is used to store 4 bits to enable interrupts when the related alarms in pseudowires happen."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x041800 +  Slice*1024 + GrpID"
            
        def startAddress(self):
            return 0x00041800
            
        def endAddress(self):
            return 0xffffffff

        class _IntrORStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IntrORStatus"
            
            def description(self):
                return "Set to 1 to indicate that there is any interrupt status bit in the Counter per Alarm Interrupt Status register of the related pseudowires to be set and they are enabled to raise interrupt. Bit 0 of GrpID#0 for pseudowire 0, bit 31 of GrpID#0 for pseudowire 31, bit 0 of GrpID#1 for pseudowire 32, respectively."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IntrORStatus"] = _AF6CCI0031_RD_PMC._Counter_Interrupt_OR_Status._IntrORStatus()
            return allFields

    class _counter_per_group_intr_or_stat(AtRegister.AtRegister):
        def name(self):
            return "Counter per Group Interrupt OR Status"
    
        def description(self):
            return "The register consists of 8 bits for 8 Group of the PW Counter. Each bit is used to store Interrupt OR status of the related Group."
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return "0x041BFF +  Slice*1024"
            
        def startAddress(self):
            return 0x00041bff
            
        def endAddress(self):
            return 0xffffffff

        class _GroupIntrOrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "GroupIntrOrSta"
            
            def description(self):
                return "Set to 1 if any interrupt bit of corresponding Group is set and its interrupt is enabled"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["GroupIntrOrSta"] = _AF6CCI0031_RD_PMC._counter_per_group_intr_or_stat._GroupIntrOrSta()
            return allFields

    class _counter_per_group_intr_en_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Counter per Group Interrupt Enable Control"
    
        def description(self):
            return "The register consists of 8 interrupt enable bits for 8 group in the PW counter."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x041BFE +  Slice*1024"
            
        def startAddress(self):
            return 0x00041bfe
            
        def endAddress(self):
            return 0xffffffff

        class _GroupIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "GroupIntrEn"
            
            def description(self):
                return "Set to 1 to enable the related Group to generate interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["GroupIntrEn"] = _AF6CCI0031_RD_PMC._counter_per_group_intr_en_ctrl._GroupIntrEn()
            return allFields

    class _counter_per_slice_intr_or_stat(AtRegister.AtRegister):
        def name(self):
            return "Counter per Slice Interrupt OR Status"
    
        def description(self):
            return "The register consists of 2 bits for 2 Slice of the PW Counter. Each bit is used to store Interrupt OR status of the related Group."
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00042000
            
        def endAddress(self):
            return 0xffffffff

        class _GroupIntrOrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "GroupIntrOrSta"
            
            def description(self):
                return "Set to 1 if any interrupt bit of corresponding Slice is set and its interrupt is enabled"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["GroupIntrOrSta"] = _AF6CCI0031_RD_PMC._counter_per_slice_intr_or_stat._GroupIntrOrSta()
            return allFields

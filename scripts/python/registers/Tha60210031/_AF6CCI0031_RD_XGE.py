import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0031_RD_XGE(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["XFI_FCS_errins_en_cfg"] = _AF6CCI0031_RD_XGE._XFI_FCS_errins_en_cfg()
        return allRegisters

    class _XFI_FCS_errins_en_cfg(AtRegister.AtRegister):
        def name(self):
            return "XFI FCS Force Error Control"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00001007
            
        def endAddress(self):
            return 0xffffffff

        class _ETHFCSErrMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 29
        
            def name(self):
                return "ETHFCSErrMode"
            
            def description(self):
                return "0: FCS (MAC); 1:Tx interface MAC2PCS; 2: Rx interface PCS2MAC"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ETHFCSErrRate(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "ETHFCSErrRate"
            
            def description(self):
                return "0: One-shot with number of errors; 1:Line rate"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ETHFCSErrThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ETHFCSErrThr"
            
            def description(self):
                return "Error Threshold in bit unit or the number of error in one-shot mode. Valid value from 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ETHFCSErrMode"] = _AF6CCI0031_RD_XGE._XFI_FCS_errins_en_cfg._ETHFCSErrMode()
            allFields["ETHFCSErrRate"] = _AF6CCI0031_RD_XGE._XFI_FCS_errins_en_cfg._ETHFCSErrRate()
            allFields["ETHFCSErrThr"] = _AF6CCI0031_RD_XGE._XFI_FCS_errins_en_cfg._ETHFCSErrThr()
            return allFields

import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0031_RD_POH_BER(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["pcfg_trshglbctr"] = _AF6CCI0031_RD_POH_BER._pcfg_trshglbctr()
        allRegisters["pohstspohgrb"] = _AF6CCI0031_RD_POH_BER._pohstspohgrb()
        allRegisters["pohvtpohgrb"] = _AF6CCI0031_RD_POH_BER._pohvtpohgrb()
        allRegisters["pohcpestsctr"] = _AF6CCI0031_RD_POH_BER._pohcpestsctr()
        allRegisters["pohcpevtctr"] = _AF6CCI0031_RD_POH_BER._pohcpevtctr()
        allRegisters["pohcpestssta"] = _AF6CCI0031_RD_POH_BER._pohcpestssta()
        allRegisters["pohcpevtsta"] = _AF6CCI0031_RD_POH_BER._pohcpevtsta()
        allRegisters["pohmsgstsexp"] = _AF6CCI0031_RD_POH_BER._pohmsgstsexp()
        allRegisters["pohmsgstscur"] = _AF6CCI0031_RD_POH_BER._pohmsgstscur()
        allRegisters["pohmsgvtexp"] = _AF6CCI0031_RD_POH_BER._pohmsgvtexp()
        allRegisters["pohmsgvtcur"] = _AF6CCI0031_RD_POH_BER._pohmsgvtcur()
        allRegisters["pohmsgstsins"] = _AF6CCI0031_RD_POH_BER._pohmsgstsins()
        allRegisters["pohmsgvtins"] = _AF6CCI0031_RD_POH_BER._pohmsgvtins()
        allRegisters["ter_ctrlhi"] = _AF6CCI0031_RD_POH_BER._ter_ctrlhi()
        allRegisters["ter_ctrllo"] = _AF6CCI0031_RD_POH_BER._ter_ctrllo()
        allRegisters["rtlpohccterbufhi"] = _AF6CCI0031_RD_POH_BER._rtlpohccterbufhi()
        allRegisters["rtlpohccterbuflo"] = _AF6CCI0031_RD_POH_BER._rtlpohccterbuflo()
        allRegisters["pcfg_glbenb"] = _AF6CCI0031_RD_POH_BER._pcfg_glbenb()
        allRegisters["pcfg_errsel0"] = _AF6CCI0031_RD_POH_BER._pcfg_errsel0()
        allRegisters["pcfg_errsel1"] = _AF6CCI0031_RD_POH_BER._pcfg_errsel1()
        allRegisters["stkalarm"] = _AF6CCI0031_RD_POH_BER._stkalarm()
        allRegisters["imemrwptrsh1"] = _AF6CCI0031_RD_POH_BER._imemrwptrsh1()
        allRegisters["imemrwptrsh2"] = _AF6CCI0031_RD_POH_BER._imemrwptrsh2()
        allRegisters["imemrwpctrl1"] = _AF6CCI0031_RD_POH_BER._imemrwpctrl1()
        allRegisters["imemrwpctrl2"] = _AF6CCI0031_RD_POH_BER._imemrwpctrl2()
        allRegisters["ramberratevtds"] = _AF6CCI0031_RD_POH_BER._ramberratevtds()
        allRegisters["ramberrateststu3"] = _AF6CCI0031_RD_POH_BER._ramberrateststu3()
        allRegisters["ipm_cnthi"] = _AF6CCI0031_RD_POH_BER._ipm_cnthi()
        allRegisters["ipm_cntlo"] = _AF6CCI0031_RD_POH_BER._ipm_cntlo()
        allRegisters["alm_mskhi"] = _AF6CCI0031_RD_POH_BER._alm_mskhi()
        allRegisters["alm_stahi"] = _AF6CCI0031_RD_POH_BER._alm_stahi()
        allRegisters["alm_chghi"] = _AF6CCI0031_RD_POH_BER._alm_chghi()
        allRegisters["alm_glbchghi"] = _AF6CCI0031_RD_POH_BER._alm_glbchghi()
        allRegisters["alm_glbmskhi"] = _AF6CCI0031_RD_POH_BER._alm_glbmskhi()
        allRegisters["alm_msklo"] = _AF6CCI0031_RD_POH_BER._alm_msklo()
        allRegisters["alm_stalo"] = _AF6CCI0031_RD_POH_BER._alm_stalo()
        allRegisters["alm_chglo"] = _AF6CCI0031_RD_POH_BER._alm_chglo()
        allRegisters["alm_orstalo"] = _AF6CCI0031_RD_POH_BER._alm_orstalo()
        allRegisters["alm_glbchglo"] = _AF6CCI0031_RD_POH_BER._alm_glbchglo()
        allRegisters["alm_glbmsklo"] = _AF6CCI0031_RD_POH_BER._alm_glbmsklo()
        allRegisters["alm_glbmsk"] = _AF6CCI0031_RD_POH_BER._alm_glbmsk()
        allRegisters["alm_glbchg"] = _AF6CCI0031_RD_POH_BER._alm_glbchg()
        allRegisters["alm_glbchgo"] = _AF6CCI0031_RD_POH_BER._alm_glbchgo()
        return allRegisters

    class _pcfg_trshglbctr(AtRegister.AtRegister):
        def name(self):
            return "POH Threshold Global Control"
    
        def description(self):
            return "This register is used to set Threshold for stable detection."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _jnmsgdebound(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 28
        
            def name(self):
                return "jnmsgdebound"
            
            def description(self):
                return "Debound Threshold for Jn 16/64byte"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _V5RFIStbTrsh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 24
        
            def name(self):
                return "V5RFIStbTrsh"
            
            def description(self):
                return "V5 RDI Stable Thershold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _V5RDIStbTrsh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 20
        
            def name(self):
                return "V5RDIStbTrsh"
            
            def description(self):
                return "V5 RDI Stable Thershold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _V5SlbStbTrsh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "V5SlbStbTrsh"
            
            def description(self):
                return "V5 Signal Lable Stable Thershold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _G1RDIStbTrsh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "G1RDIStbTrsh"
            
            def description(self):
                return "G1 RDI Path Stable Thershold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _C2PlmStbTrsh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "C2PlmStbTrsh"
            
            def description(self):
                return "C2 Path Signal Lable Stable Thershold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _JnStbTrsh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "JnStbTrsh"
            
            def description(self):
                return "J1/J2 Message Stable Threshold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _debound(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "debound"
            
            def description(self):
                return "Debound Threshold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["jnmsgdebound"] = _AF6CCI0031_RD_POH_BER._pcfg_trshglbctr._jnmsgdebound()
            allFields["V5RFIStbTrsh"] = _AF6CCI0031_RD_POH_BER._pcfg_trshglbctr._V5RFIStbTrsh()
            allFields["V5RDIStbTrsh"] = _AF6CCI0031_RD_POH_BER._pcfg_trshglbctr._V5RDIStbTrsh()
            allFields["V5SlbStbTrsh"] = _AF6CCI0031_RD_POH_BER._pcfg_trshglbctr._V5SlbStbTrsh()
            allFields["G1RDIStbTrsh"] = _AF6CCI0031_RD_POH_BER._pcfg_trshglbctr._G1RDIStbTrsh()
            allFields["C2PlmStbTrsh"] = _AF6CCI0031_RD_POH_BER._pcfg_trshglbctr._C2PlmStbTrsh()
            allFields["JnStbTrsh"] = _AF6CCI0031_RD_POH_BER._pcfg_trshglbctr._JnStbTrsh()
            allFields["debound"] = _AF6CCI0031_RD_POH_BER._pcfg_trshglbctr._debound()
            return allFields

    class _pohstspohgrb(AtRegister.AtRegister):
        def name(self):
            return "POH Hi-order Path Over Head Grabber"
    
        def description(self):
            return "This register is used to grabber Hi-Order Path Overhead"
            
        def width(self):
            return 68
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x02_4000 + $stsid * 8 + $sliceid"
            
        def startAddress(self):
            return 0x00024000
            
        def endAddress(self):
            return 0xffffffff

        class _hlais(AtRegister.AtRegisterField):
            def stopBit(self):
                return 67
                
            def startBit(self):
                return 67
        
            def name(self):
                return "hlais"
            
            def description(self):
                return "High-level AIS from OCN"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _lom(AtRegister.AtRegisterField):
            def stopBit(self):
                return 66
                
            def startBit(self):
                return 66
        
            def name(self):
                return "lom"
            
            def description(self):
                return "LOM  from OCN"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _lop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 65
                
            def startBit(self):
                return 65
        
            def name(self):
                return "lop"
            
            def description(self):
                return "LOP from OCN"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _ais(AtRegister.AtRegisterField):
            def stopBit(self):
                return 64
                
            def startBit(self):
                return 64
        
            def name(self):
                return "ais"
            
            def description(self):
                return "AIS from OCN"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _K3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 56
        
            def name(self):
                return "K3"
            
            def description(self):
                return "K3 byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _F3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 55
                
            def startBit(self):
                return 48
        
            def name(self):
                return "F3"
            
            def description(self):
                return "F3 byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _H4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 40
        
            def name(self):
                return "H4"
            
            def description(self):
                return "H4 byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _F2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 39
                
            def startBit(self):
                return 32
        
            def name(self):
                return "F2"
            
            def description(self):
                return "F2 byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _G1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "G1"
            
            def description(self):
                return "G1 byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _C2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "C2"
            
            def description(self):
                return "C2 byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _N1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "N1"
            
            def description(self):
                return "N1 byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _J1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "J1"
            
            def description(self):
                return "J1 byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["hlais"] = _AF6CCI0031_RD_POH_BER._pohstspohgrb._hlais()
            allFields["lom"] = _AF6CCI0031_RD_POH_BER._pohstspohgrb._lom()
            allFields["lop"] = _AF6CCI0031_RD_POH_BER._pohstspohgrb._lop()
            allFields["ais"] = _AF6CCI0031_RD_POH_BER._pohstspohgrb._ais()
            allFields["K3"] = _AF6CCI0031_RD_POH_BER._pohstspohgrb._K3()
            allFields["F3"] = _AF6CCI0031_RD_POH_BER._pohstspohgrb._F3()
            allFields["H4"] = _AF6CCI0031_RD_POH_BER._pohstspohgrb._H4()
            allFields["F2"] = _AF6CCI0031_RD_POH_BER._pohstspohgrb._F2()
            allFields["G1"] = _AF6CCI0031_RD_POH_BER._pohstspohgrb._G1()
            allFields["C2"] = _AF6CCI0031_RD_POH_BER._pohstspohgrb._C2()
            allFields["N1"] = _AF6CCI0031_RD_POH_BER._pohstspohgrb._N1()
            allFields["J1"] = _AF6CCI0031_RD_POH_BER._pohstspohgrb._J1()
            return allFields

    class _pohvtpohgrb(AtRegister.AtRegister):
        def name(self):
            return "POH Lo-order VT/TU3 Over Head Grabber"
    
        def description(self):
            return "This register is used to grabber Lo-Order Path Overhead. Incase the TU3 mode, the $vtid = 0, using for Tu3 POH grabber. Incase VT mode, the $vtid = 0-27, using for VT POH grabber."
            
        def width(self):
            return 36
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x02_6000 + $sliceid*672 + $stsid*28 + $vtid"
            
        def startAddress(self):
            return 0x00026000
            
        def endAddress(self):
            return 0xffffffff

        class _hlais(AtRegister.AtRegisterField):
            def stopBit(self):
                return 35
                
            def startBit(self):
                return 35
        
            def name(self):
                return "hlais"
            
            def description(self):
                return "High-level AIS from OCN"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _lop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 33
                
            def startBit(self):
                return 33
        
            def name(self):
                return "lop"
            
            def description(self):
                return "LOP from OCN"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _ais(AtRegister.AtRegisterField):
            def stopBit(self):
                return 32
                
            def startBit(self):
                return 32
        
            def name(self):
                return "ais"
            
            def description(self):
                return "AIS from OCN"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Byte3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "Byte3"
            
            def description(self):
                return "G1 byte or K3 byte or K4 byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Byte2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Byte2"
            
            def description(self):
                return "C2 byte or F3 byte or N2 byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Byte1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Byte1"
            
            def description(self):
                return "N1 byte or H4 byte or J2 byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Byte0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Byte0"
            
            def description(self):
                return "J1 byte or F2 byte or V5 byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["hlais"] = _AF6CCI0031_RD_POH_BER._pohvtpohgrb._hlais()
            allFields["lop"] = _AF6CCI0031_RD_POH_BER._pohvtpohgrb._lop()
            allFields["ais"] = _AF6CCI0031_RD_POH_BER._pohvtpohgrb._ais()
            allFields["Byte3"] = _AF6CCI0031_RD_POH_BER._pohvtpohgrb._Byte3()
            allFields["Byte2"] = _AF6CCI0031_RD_POH_BER._pohvtpohgrb._Byte2()
            allFields["Byte1"] = _AF6CCI0031_RD_POH_BER._pohvtpohgrb._Byte1()
            allFields["Byte0"] = _AF6CCI0031_RD_POH_BER._pohvtpohgrb._Byte0()
            return allFields

    class _pohcpestsctr(AtRegister.AtRegister):
        def name(self):
            return "POH CPE STS/TU3 Control Register"
    
        def description(self):
            return "This register is used to configure the POH Hi-order Path Monitoring."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x02_A000 + $sliceid*48 + $stsid*2 + $tu3en"
            
        def startAddress(self):
            return 0x0002a000
            
        def endAddress(self):
            return 0xffffffff

        class _PlmEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "PlmEnb"
            
            def description(self):
                return "PLM enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VcaisDstren(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "VcaisDstren"
            
            def description(self):
                return "VcaisDstren"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlmDstren(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "PlmDstren"
            
            def description(self):
                return "PlmDstren"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _UneqDstren(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "UneqDstren"
            
            def description(self):
                return "UneqDstren"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TimDstren(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "TimDstren"
            
            def description(self):
                return "TimDstren. For slice id 2,3, this will enable/disable send TIM alarm to OCN block for generating TIM alarm."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Sdhmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "Sdhmode"
            
            def description(self):
                return "SDH mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Blkmden(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "Blkmden"
            
            def description(self):
                return "Block mode BIP"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ERDIenb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "ERDIenb"
            
            def description(self):
                return "Enable E-RDI"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PslExp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PslExp"
            
            def description(self):
                return "C2 Expected Path Signal Lable Value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TimEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "TimEnb"
            
            def description(self):
                return "Enable Monitor TIM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Reiblkmden(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Reiblkmden"
            
            def description(self):
                return "Block mode REI"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _J1mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "J1mode"
            
            def description(self):
                return "0: 1Byte 1:16Byte 2:64byte 3:Floating"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlmEnb"] = _AF6CCI0031_RD_POH_BER._pohcpestsctr._PlmEnb()
            allFields["VcaisDstren"] = _AF6CCI0031_RD_POH_BER._pohcpestsctr._VcaisDstren()
            allFields["PlmDstren"] = _AF6CCI0031_RD_POH_BER._pohcpestsctr._PlmDstren()
            allFields["UneqDstren"] = _AF6CCI0031_RD_POH_BER._pohcpestsctr._UneqDstren()
            allFields["TimDstren"] = _AF6CCI0031_RD_POH_BER._pohcpestsctr._TimDstren()
            allFields["Sdhmode"] = _AF6CCI0031_RD_POH_BER._pohcpestsctr._Sdhmode()
            allFields["Blkmden"] = _AF6CCI0031_RD_POH_BER._pohcpestsctr._Blkmden()
            allFields["ERDIenb"] = _AF6CCI0031_RD_POH_BER._pohcpestsctr._ERDIenb()
            allFields["PslExp"] = _AF6CCI0031_RD_POH_BER._pohcpestsctr._PslExp()
            allFields["TimEnb"] = _AF6CCI0031_RD_POH_BER._pohcpestsctr._TimEnb()
            allFields["Reiblkmden"] = _AF6CCI0031_RD_POH_BER._pohcpestsctr._Reiblkmden()
            allFields["J1mode"] = _AF6CCI0031_RD_POH_BER._pohcpestsctr._J1mode()
            return allFields

    class _pohcpevtctr(AtRegister.AtRegister):
        def name(self):
            return "POH CPE VT Control Register"
    
        def description(self):
            return "This register is used to configure the POH Lo-order Path Monitoring."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x02_8000 + $sliceid*672 + $stsid*28 +$vtid"
            
        def startAddress(self):
            return 0x00028000
            
        def endAddress(self):
            return 0xffffffff

        class _PlmEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "PlmEnb"
            
            def description(self):
                return "VcaisDstren"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VcaisDstren(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "VcaisDstren"
            
            def description(self):
                return "VcaisDstren"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlmDstren(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PlmDstren"
            
            def description(self):
                return "PlmDstren"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _UneqDstren(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "UneqDstren"
            
            def description(self):
                return "UneqDstren"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TimDstren(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "TimDstren"
            
            def description(self):
                return "TimDstren"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VSdhmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "VSdhmode"
            
            def description(self):
                return "SDH mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VBlkmden(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "VBlkmden"
            
            def description(self):
                return "Block mode BIP"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ERDIenb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "ERDIenb"
            
            def description(self):
                return "Enable E-RDI"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VslExp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 4
        
            def name(self):
                return "VslExp"
            
            def description(self):
                return "V5 Expected Path Signal Lable Value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TimEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "TimEnb"
            
            def description(self):
                return "Enable Monitor TIM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Reiblkmden(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Reiblkmden"
            
            def description(self):
                return "Block mode REI"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _J2mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "J2mode"
            
            def description(self):
                return "0: 1Byte 1:16Byte 2:64byte 3:Floating"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlmEnb"] = _AF6CCI0031_RD_POH_BER._pohcpevtctr._PlmEnb()
            allFields["VcaisDstren"] = _AF6CCI0031_RD_POH_BER._pohcpevtctr._VcaisDstren()
            allFields["PlmDstren"] = _AF6CCI0031_RD_POH_BER._pohcpevtctr._PlmDstren()
            allFields["UneqDstren"] = _AF6CCI0031_RD_POH_BER._pohcpevtctr._UneqDstren()
            allFields["TimDstren"] = _AF6CCI0031_RD_POH_BER._pohcpevtctr._TimDstren()
            allFields["VSdhmode"] = _AF6CCI0031_RD_POH_BER._pohcpevtctr._VSdhmode()
            allFields["VBlkmden"] = _AF6CCI0031_RD_POH_BER._pohcpevtctr._VBlkmden()
            allFields["ERDIenb"] = _AF6CCI0031_RD_POH_BER._pohcpevtctr._ERDIenb()
            allFields["VslExp"] = _AF6CCI0031_RD_POH_BER._pohcpevtctr._VslExp()
            allFields["TimEnb"] = _AF6CCI0031_RD_POH_BER._pohcpevtctr._TimEnb()
            allFields["Reiblkmden"] = _AF6CCI0031_RD_POH_BER._pohcpevtctr._Reiblkmden()
            allFields["J2mode"] = _AF6CCI0031_RD_POH_BER._pohcpevtctr._J2mode()
            return allFields

    class _pohcpestssta(AtRegister.AtRegister):
        def name(self):
            return "POH CPE STS Status Register"
    
        def description(self):
            return "This register is used to get POH Hi-order status of monitoring."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x02_C580 + $sliceid + $stsid*4"
            
        def startAddress(self):
            return 0x0002c580
            
        def endAddress(self):
            return 0xffffffff

        class _C2stbsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "C2stbsta"
            
            def description(self):
                return "C2 stable status"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _C2stbcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "C2stbcnt"
            
            def description(self):
                return "C2 stable counter"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _C2acpt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "C2acpt"
            
            def description(self):
                return "C2 accept byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["C2stbsta"] = _AF6CCI0031_RD_POH_BER._pohcpestssta._C2stbsta()
            allFields["C2stbcnt"] = _AF6CCI0031_RD_POH_BER._pohcpestssta._C2stbcnt()
            allFields["C2acpt"] = _AF6CCI0031_RD_POH_BER._pohcpestssta._C2acpt()
            return allFields

    class _pohcpevtsta(AtRegister.AtRegister):
        def name(self):
            return "POH CPE VT/TU3 Status Register"
    
        def description(self):
            return "This register is used to get POH Lo-order status of monitoring."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x02_C000 + $sliceid*672 + $stsid*28 +$vtid"
            
        def startAddress(self):
            return 0x0002c000
            
        def endAddress(self):
            return 0xffffffff

        class _Vslstbsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "Vslstbsta"
            
            def description(self):
                return "VSL stable status"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Vslstbcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 9
        
            def name(self):
                return "Vslstbcnt"
            
            def description(self):
                return "VSL stable counter"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Vslacpt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Vslacpt"
            
            def description(self):
                return "VSL accept byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _RFIstatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RFIstatus"
            
            def description(self):
                return "RFI status"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Vslstbsta"] = _AF6CCI0031_RD_POH_BER._pohcpevtsta._Vslstbsta()
            allFields["Vslstbcnt"] = _AF6CCI0031_RD_POH_BER._pohcpevtsta._Vslstbcnt()
            allFields["Vslacpt"] = _AF6CCI0031_RD_POH_BER._pohcpevtsta._Vslacpt()
            allFields["RFIstatus"] = _AF6CCI0031_RD_POH_BER._pohcpevtsta._RFIstatus()
            return allFields

    class _pohmsgstsexp(AtRegister.AtRegister):
        def name(self):
            return "POH CPE J1 STS Expected Message buffer"
    
        def description(self):
            return "The J1 Expected Message Buffer."
            
        def width(self):
            return 64
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0B_0000 + $sliceid*8 + $stsid*32 + $msgid"
            
        def startAddress(self):
            return 0x000b0000
            
        def endAddress(self):
            return 0xffffffff

        class _J1ExpMsg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 0
        
            def name(self):
                return "J1ExpMsg"
            
            def description(self):
                return "J1 Expected Message"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["J1ExpMsg"] = _AF6CCI0031_RD_POH_BER._pohmsgstsexp._J1ExpMsg()
            return allFields

    class _pohmsgstscur(AtRegister.AtRegister):
        def name(self):
            return "POH CPE J1 STS Current Message buffer"
    
        def description(self):
            return "The J1 Current Message Buffer."
            
        def width(self):
            return 64
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0B_1000 + $sliceid*8 + $stsid*32 + $msgid"
            
        def startAddress(self):
            return 0x000b1000
            
        def endAddress(self):
            return 0xffffffff

        class _J1CurMsg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 0
        
            def name(self):
                return "J1CurMsg"
            
            def description(self):
                return "J1 Current Message"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["J1CurMsg"] = _AF6CCI0031_RD_POH_BER._pohmsgstscur._J1CurMsg()
            return allFields

    class _pohmsgvtexp(AtRegister.AtRegister):
        def name(self):
            return "POH CPE J2 Expected Message buffer"
    
        def description(self):
            return "The J2 Expected Message Buffer."
            
        def width(self):
            return 64
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x08_0000 + $sliceid*5376 + $stsid*224 + $vtid*8 + $msgid"
            
        def startAddress(self):
            return 0x00080000
            
        def endAddress(self):
            return 0xffffffff

        class _J2ExpMsg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 0
        
            def name(self):
                return "J2ExpMsg"
            
            def description(self):
                return "J2 Expected Message"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["J2ExpMsg"] = _AF6CCI0031_RD_POH_BER._pohmsgvtexp._J2ExpMsg()
            return allFields

    class _pohmsgvtcur(AtRegister.AtRegister):
        def name(self):
            return "POH CPE J2 Current Message buffer"
    
        def description(self):
            return "The J2 Current Message Buffer."
            
        def width(self):
            return 64
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x09_0000 + $sliceid*5376 + $stsid*224 + $vtid*8 + $msgid"
            
        def startAddress(self):
            return 0x00090000
            
        def endAddress(self):
            return 0xffffffff

        class _J2CurMsg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 0
        
            def name(self):
                return "J2CurMsg"
            
            def description(self):
                return "J2 Current Message"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["J2CurMsg"] = _AF6CCI0031_RD_POH_BER._pohmsgvtcur._J2CurMsg()
            return allFields

    class _pohmsgstsins(AtRegister.AtRegister):
        def name(self):
            return "POH CPE J1 Insert Message buffer"
    
        def description(self):
            return "The J1 Current Message Buffer."
            
        def width(self):
            return 64
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0B_2000 + $sliceid*8 + $stsid*32 + $msgid"
            
        def startAddress(self):
            return 0x000b2000
            
        def endAddress(self):
            return 0xffffffff

        class _J1InsMsg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 0
        
            def name(self):
                return "J1InsMsg"
            
            def description(self):
                return "J1 Insert Message"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["J1InsMsg"] = _AF6CCI0031_RD_POH_BER._pohmsgstsins._J1InsMsg()
            return allFields

    class _pohmsgvtins(AtRegister.AtRegister):
        def name(self):
            return "POH CPE J2 Insert Message buffer"
    
        def description(self):
            return "The J2 Insert Message Buffer."
            
        def width(self):
            return 64
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0A_0000 + $sliceid*5376 + $stsid*224 + $vtid*8 + $msgid"
            
        def startAddress(self):
            return 0x000a0000
            
        def endAddress(self):
            return 0xffffffff

        class _J2InsMsg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 0
        
            def name(self):
                return "J2InsMsg"
            
            def description(self):
                return "J2 Insert Message"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["J2InsMsg"] = _AF6CCI0031_RD_POH_BER._pohmsgvtins._J2InsMsg()
            return allFields

    class _ter_ctrlhi(AtRegister.AtRegister):
        def name(self):
            return "POH Termintate Insert Control STS"
    
        def description(self):
            return "This register is used to control STS POH insert ."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x04_0400 + $STS*2 + $OCID"
            
        def startAddress(self):
            return 0x00040400
            
        def endAddress(self):
            return 0xffffffff

        class _g1spare(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "g1spare"
            
            def description(self):
                return "G1 spare value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _plm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "plm"
            
            def description(self):
                return "0 : Enable, 1: Disable send ERDI if PLM detected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _uneq(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "uneq"
            
            def description(self):
                return "0 : Enable, 1: Disable send ERDI if UNEQ detected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _timmsk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "timmsk"
            
            def description(self):
                return "0 : Enable, 1: Disable send ERDI if TIM detected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _aislopmsk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "aislopmsk"
            
            def description(self):
                return "0 : Enable, 1: Disable send ERDI if AIS,LOP detected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _jnfrmd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "jnfrmd"
            
            def description(self):
                return "0:1 byte, 1: 16/64byte"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["g1spare"] = _AF6CCI0031_RD_POH_BER._ter_ctrlhi._g1spare()
            allFields["plm"] = _AF6CCI0031_RD_POH_BER._ter_ctrlhi._plm()
            allFields["uneq"] = _AF6CCI0031_RD_POH_BER._ter_ctrlhi._uneq()
            allFields["timmsk"] = _AF6CCI0031_RD_POH_BER._ter_ctrlhi._timmsk()
            allFields["aislopmsk"] = _AF6CCI0031_RD_POH_BER._ter_ctrlhi._aislopmsk()
            allFields["jnfrmd"] = _AF6CCI0031_RD_POH_BER._ter_ctrlhi._jnfrmd()
            return allFields

    class _ter_ctrllo(AtRegister.AtRegister):
        def name(self):
            return "POH Termintate Insert Control VT/TU3"
    
        def description(self):
            return "This register is used to control STS POH insert. TU3 is at VT ID = 0. Fields must be the same as ter_ctrlhi"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x04_4000 + $STS*56 + $OCID*28 + $VT"
            
        def startAddress(self):
            return 0x00044000
            
        def endAddress(self):
            return 0xffffffff

        class _k4b0b1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 13
        
            def name(self):
                return "k4b0b1"
            
            def description(self):
                return "K4b0b1 value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _k4aps(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 11
        
            def name(self):
                return "k4aps"
            
            def description(self):
                return "K4aps value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _k4spare(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "k4spare"
            
            def description(self):
                return "K4spare value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _rfival(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "rfival"
            
            def description(self):
                return "RFI value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _vslval(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 6
        
            def name(self):
                return "vslval"
            
            def description(self):
                return "VT signal label value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _plm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "plm"
            
            def description(self):
                return "0 : Enable, 1: Disable send ERDI if PLM detected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _uneq(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "uneq"
            
            def description(self):
                return "0 : Enable, 1: Disable send ERDI if UNEQ detected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _timmsk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "timmsk"
            
            def description(self):
                return "0 : Enable, 1: Disable send ERDI if TIM detected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _aislopmsk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "aislopmsk"
            
            def description(self):
                return "0 : Enable, 1: Disable send ERDI if AIS,LOP detected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _jnfrmd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "jnfrmd"
            
            def description(self):
                return "0:1 byte, 1: 16/64byte"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["k4b0b1"] = _AF6CCI0031_RD_POH_BER._ter_ctrllo._k4b0b1()
            allFields["k4aps"] = _AF6CCI0031_RD_POH_BER._ter_ctrllo._k4aps()
            allFields["k4spare"] = _AF6CCI0031_RD_POH_BER._ter_ctrllo._k4spare()
            allFields["rfival"] = _AF6CCI0031_RD_POH_BER._ter_ctrllo._rfival()
            allFields["vslval"] = _AF6CCI0031_RD_POH_BER._ter_ctrllo._vslval()
            allFields["plm"] = _AF6CCI0031_RD_POH_BER._ter_ctrllo._plm()
            allFields["uneq"] = _AF6CCI0031_RD_POH_BER._ter_ctrllo._uneq()
            allFields["timmsk"] = _AF6CCI0031_RD_POH_BER._ter_ctrllo._timmsk()
            allFields["aislopmsk"] = _AF6CCI0031_RD_POH_BER._ter_ctrllo._aislopmsk()
            allFields["jnfrmd"] = _AF6CCI0031_RD_POH_BER._ter_ctrllo._jnfrmd()
            return allFields

    class _rtlpohccterbufhi(AtRegister.AtRegister):
        def name(self):
            return "POH Termintate Insert Buffer STS"
    
        def description(self):
            return "This register is used for storing POH BYTEs inserted to Sonet/SDH. %% BGRP = 0 : G1,J1  %% BGRP = 1 : N1,C2  %% BGRP = 2 : H4,F2  %% BGRP = 3 : K3,F3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x01_0800 + $OCID*256 + $STS*4 + $BGRP"
            
        def startAddress(self):
            return 0x00010800
            
        def endAddress(self):
            return 0xffffffff

        class _byte1msk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "byte1msk"
            
            def description(self):
                return "Enable/Disable (1/0)write to buffer"
            
            def type(self):
                return "WO"
            
            def resetValue(self):
                return 0xffffffff

        class _byte1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 9
        
            def name(self):
                return "byte1"
            
            def description(self):
                return "Byte1 (G1/N1/H4/K3)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _byte0msk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "byte0msk"
            
            def description(self):
                return "Enable/Disable (1/0) write to buffer"
            
            def type(self):
                return "WO"
            
            def resetValue(self):
                return 0xffffffff

        class _byte0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "byte0"
            
            def description(self):
                return "Byte0 (J1/C2/F2/F3)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["byte1msk"] = _AF6CCI0031_RD_POH_BER._rtlpohccterbufhi._byte1msk()
            allFields["byte1"] = _AF6CCI0031_RD_POH_BER._rtlpohccterbufhi._byte1()
            allFields["byte0msk"] = _AF6CCI0031_RD_POH_BER._rtlpohccterbufhi._byte0msk()
            allFields["byte0"] = _AF6CCI0031_RD_POH_BER._rtlpohccterbufhi._byte0()
            return allFields

    class _rtlpohccterbuflo(AtRegister.AtRegister):
        def name(self):
            return "POH Termintate Insert Buffer TU3/VT"
    
        def description(self):
            return "This register is used for storing POH BYTEs inserted to Sonet/SDH. TU3 is at VT ID = 0,1 %% For VT %% BGRP = 0 : J2,V5 %% BGRP = 1 : K4,N2 %% For TU3 %% VT = 0, BGRP = 0 : G1,J1 %% VT = 0, BGRP = 1 : N1,C2 %% VT = 1, BGRP = 0 : H4,F2 %% VT = 1, BGRP = 1 : K3,F3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x01_8000 + $OCID*4096 + $STS*64 + $VT*2 + $BGRP"
            
        def startAddress(self):
            return 0x00018000
            
        def endAddress(self):
            return 0xffffffff

        class _byte1msk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "byte1msk"
            
            def description(self):
                return "Enable/Disable (1/0)write to buffer"
            
            def type(self):
                return "WO"
            
            def resetValue(self):
                return 0xffffffff

        class _byte1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 9
        
            def name(self):
                return "byte1"
            
            def description(self):
                return "Byte1 (J2/K4)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _byte0msk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "byte0msk"
            
            def description(self):
                return "Enable/Disable (1/0) write to buffer"
            
            def type(self):
                return "WO"
            
            def resetValue(self):
                return 0xffffffff

        class _byte0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "byte0"
            
            def description(self):
                return "Byte0 (V5/N2)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["byte1msk"] = _AF6CCI0031_RD_POH_BER._rtlpohccterbuflo._byte1msk()
            allFields["byte1"] = _AF6CCI0031_RD_POH_BER._rtlpohccterbuflo._byte1()
            allFields["byte0msk"] = _AF6CCI0031_RD_POH_BER._rtlpohccterbuflo._byte0msk()
            allFields["byte0"] = _AF6CCI0031_RD_POH_BER._rtlpohccterbuflo._byte0()
            return allFields

    class _pcfg_glbenb(AtRegister.AtRegister):
        def name(self):
            return "POH BER Global Control"
    
        def description(self):
            return "This register is used to enable STS,VT,DSN globally."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00060000
            
        def endAddress(self):
            return 0xffffffff

        class _timerenb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "timerenb"
            
            def description(self):
                return "Enable timer"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _stsenb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "stsenb"
            
            def description(self):
                return "Enable STS/TU3 channel"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _vtenb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "vtenb"
            
            def description(self):
                return "Enable STS/TU3 channel"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _dsnsenb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dsnsenb"
            
            def description(self):
                return "Enable STS/TU3 channel"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["timerenb"] = _AF6CCI0031_RD_POH_BER._pcfg_glbenb._timerenb()
            allFields["stsenb"] = _AF6CCI0031_RD_POH_BER._pcfg_glbenb._stsenb()
            allFields["vtenb"] = _AF6CCI0031_RD_POH_BER._pcfg_glbenb._vtenb()
            allFields["dsnsenb"] = _AF6CCI0031_RD_POH_BER._pcfg_glbenb._dsnsenb()
            return allFields

    class _pcfg_errsel0(AtRegister.AtRegister):
        def name(self):
            return "POH BER Error Code Select Control 0"
    
        def description(self):
            return "This register is used to select error to monitor ."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00060004
            
        def endAddress(self):
            return 0xffffffff

        class _linecodeen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "linecodeen"
            
            def description(self):
                return "1: Line code violation , 0: Parity code. For"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["linecodeen"] = _AF6CCI0031_RD_POH_BER._pcfg_errsel0._linecodeen()
            return allFields

    class _pcfg_errsel1(AtRegister.AtRegister):
        def name(self):
            return "POH BER Error Code Select Control 0"
    
        def description(self):
            return "This register is used to select error to monitor ."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00060005
            
        def endAddress(self):
            return 0xffffffff

        class _linecodeen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "linecodeen"
            
            def description(self):
                return "1: Line code violation , 0: Parity code. For"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["linecodeen"] = _AF6CCI0031_RD_POH_BER._pcfg_errsel1._linecodeen()
            return allFields

    class _stkalarm(AtRegister.AtRegister):
        def name(self):
            return "POH BER Error Sticky"
    
        def description(self):
            return "This register is used to check error in BER engine."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00060001
            
        def endAddress(self):
            return 0xffffffff

        class _stserr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "stserr"
            
            def description(self):
                return "STS error"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _vterr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "vterr"
            
            def description(self):
                return "VT error"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["stserr"] = _AF6CCI0031_RD_POH_BER._stkalarm._stserr()
            allFields["vterr"] = _AF6CCI0031_RD_POH_BER._stkalarm._vterr()
            return allFields

    class _imemrwptrsh1(AtRegister.AtRegister):
        def name(self):
            return "POH BER Threshold 1"
    
        def description(self):
            return "This register is used to configure threshold of BER level 3."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x06_2300 + $Rate"
            
        def startAddress(self):
            return 0x00062300
            
        def endAddress(self):
            return 0xffffffff

        class _setthres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 9
        
            def name(self):
                return "setthres"
            
            def description(self):
                return "SetThreshold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _winthres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 0
        
            def name(self):
                return "winthres"
            
            def description(self):
                return "WindowThreshold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["setthres"] = _AF6CCI0031_RD_POH_BER._imemrwptrsh1._setthres()
            allFields["winthres"] = _AF6CCI0031_RD_POH_BER._imemrwptrsh1._winthres()
            return allFields

    class _imemrwptrsh2(AtRegister.AtRegister):
        def name(self):
            return "POH BER Threshold 2"
    
        def description(self):
            return "This register is used to configure threshold of BER level 4 to level 8."
            
        def width(self):
            return 34
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x06_0400 + $Rate*8 + $Thresloc"
            
        def startAddress(self):
            return 0x00060400
            
        def endAddress(self):
            return 0xffffffff

        class _scwthres1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 33
                
            def startBit(self):
                return 17
        
            def name(self):
                return "scwthres1"
            
            def description(self):
                return "Set/Clear/Window Threshold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _scwthres2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 0
        
            def name(self):
                return "scwthres2"
            
            def description(self):
                return "Set/Clear/Window Threshold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["scwthres1"] = _AF6CCI0031_RD_POH_BER._imemrwptrsh2._scwthres1()
            allFields["scwthres2"] = _AF6CCI0031_RD_POH_BER._imemrwptrsh2._scwthres2()
            return allFields

    class _imemrwpctrl1(AtRegister.AtRegister):
        def name(self):
            return "POH BER Control VT/DSN"
    
        def description(self):
            return "This register is used to enable and set threshold SD SF ."
            
        def width(self):
            return 48
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x06_2000 + $STS*32 + $OCID*8+ $VTG"
            
        def startAddress(self):
            return 0x00062000
            
        def endAddress(self):
            return 0xffffffff

        class _tcatrsh4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 45
                
            def startBit(self):
                return 43
        
            def name(self):
                return "tcatrsh4"
            
            def description(self):
                return "TCA threshold raise channel 4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _tcatrsh3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 42
                
            def startBit(self):
                return 40
        
            def name(self):
                return "tcatrsh3"
            
            def description(self):
                return "TCA threshold raise channel 3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _tcatrsh2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 39
                
            def startBit(self):
                return 37
        
            def name(self):
                return "tcatrsh2"
            
            def description(self):
                return "TCA threshold raise channel 2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _tcatrsh1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 36
                
            def startBit(self):
                return 34
        
            def name(self):
                return "tcatrsh1"
            
            def description(self):
                return "TCA threshold raise channel 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _etype4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "etype4"
            
            def description(self):
                return "0: DS1/VT1.5 1: E1/VT2 channel 3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _sftrsh4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 28
        
            def name(self):
                return "sftrsh4"
            
            def description(self):
                return "SF threshold raise channel 3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _sdtrsh4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 25
        
            def name(self):
                return "sdtrsh4"
            
            def description(self):
                return "SD threshold raise channel 3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ena4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "ena4"
            
            def description(self):
                return "Enable channel 3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _etype3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "etype3"
            
            def description(self):
                return "0: DS1/VT1.5 1: E1/VT2 channel 2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _sftrsh3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 20
        
            def name(self):
                return "sftrsh3"
            
            def description(self):
                return "SF threshold raise channel 2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _sdtrsh3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 17
        
            def name(self):
                return "sdtrsh3"
            
            def description(self):
                return "SD threshold raise channel 2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ena3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ena3"
            
            def description(self):
                return "Enable channel 2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _etype2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "etype2"
            
            def description(self):
                return "0: DS1/VT1.5 1: E1/VT2 channel 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _sftrsh2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 12
        
            def name(self):
                return "sftrsh2"
            
            def description(self):
                return "SF threshold raise channel 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _sdtrsh2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 9
        
            def name(self):
                return "sdtrsh2"
            
            def description(self):
                return "SD threshold raise channel 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ena2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "ena2"
            
            def description(self):
                return "Enable channel 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _etype1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "etype1"
            
            def description(self):
                return "0: DS1/VT1.5 1: E1/VT2 channel 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _sftrsh1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 4
        
            def name(self):
                return "sftrsh1"
            
            def description(self):
                return "SF threshold raise channel 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _sdtrsh1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 1
        
            def name(self):
                return "sdtrsh1"
            
            def description(self):
                return "SD threshold raise channel 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ena1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ena1"
            
            def description(self):
                return "Enable channel 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tcatrsh4"] = _AF6CCI0031_RD_POH_BER._imemrwpctrl1._tcatrsh4()
            allFields["tcatrsh3"] = _AF6CCI0031_RD_POH_BER._imemrwpctrl1._tcatrsh3()
            allFields["tcatrsh2"] = _AF6CCI0031_RD_POH_BER._imemrwpctrl1._tcatrsh2()
            allFields["tcatrsh1"] = _AF6CCI0031_RD_POH_BER._imemrwpctrl1._tcatrsh1()
            allFields["etype4"] = _AF6CCI0031_RD_POH_BER._imemrwpctrl1._etype4()
            allFields["sftrsh4"] = _AF6CCI0031_RD_POH_BER._imemrwpctrl1._sftrsh4()
            allFields["sdtrsh4"] = _AF6CCI0031_RD_POH_BER._imemrwpctrl1._sdtrsh4()
            allFields["ena4"] = _AF6CCI0031_RD_POH_BER._imemrwpctrl1._ena4()
            allFields["etype3"] = _AF6CCI0031_RD_POH_BER._imemrwpctrl1._etype3()
            allFields["sftrsh3"] = _AF6CCI0031_RD_POH_BER._imemrwpctrl1._sftrsh3()
            allFields["sdtrsh3"] = _AF6CCI0031_RD_POH_BER._imemrwpctrl1._sdtrsh3()
            allFields["ena3"] = _AF6CCI0031_RD_POH_BER._imemrwpctrl1._ena3()
            allFields["etype2"] = _AF6CCI0031_RD_POH_BER._imemrwpctrl1._etype2()
            allFields["sftrsh2"] = _AF6CCI0031_RD_POH_BER._imemrwpctrl1._sftrsh2()
            allFields["sdtrsh2"] = _AF6CCI0031_RD_POH_BER._imemrwpctrl1._sdtrsh2()
            allFields["ena2"] = _AF6CCI0031_RD_POH_BER._imemrwpctrl1._ena2()
            allFields["etype1"] = _AF6CCI0031_RD_POH_BER._imemrwpctrl1._etype1()
            allFields["sftrsh1"] = _AF6CCI0031_RD_POH_BER._imemrwpctrl1._sftrsh1()
            allFields["sdtrsh1"] = _AF6CCI0031_RD_POH_BER._imemrwpctrl1._sdtrsh1()
            allFields["ena1"] = _AF6CCI0031_RD_POH_BER._imemrwpctrl1._ena1()
            return allFields

    class _imemrwpctrl2(AtRegister.AtRegister):
        def name(self):
            return "POH BER Control STS/TU3"
    
        def description(self):
            return "This register is used to enable and set threshold SD SF."
            
        def width(self):
            return 48
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x06_2007 + $STS*32 + $OCID*8"
            
        def startAddress(self):
            return 0x00062007
            
        def endAddress(self):
            return 0xffffffff

        class _tcatrsh2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 39
                
            def startBit(self):
                return 37
        
            def name(self):
                return "tcatrsh2"
            
            def description(self):
                return "TCA threshold raise channel 2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _tcatrsh1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 36
                
            def startBit(self):
                return 34
        
            def name(self):
                return "tcatrsh1"
            
            def description(self):
                return "TCA threshold raise channel 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _rate2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 21
        
            def name(self):
                return "rate2"
            
            def description(self):
                return "STS Rate 0-63 type"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _sftrsh2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 18
        
            def name(self):
                return "sftrsh2"
            
            def description(self):
                return "SF threshold raise channel 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _sdtrsh2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 15
        
            def name(self):
                return "sdtrsh2"
            
            def description(self):
                return "SD threshold raise channel 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ena2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "ena2"
            
            def description(self):
                return "Enable channel 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _rate1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 7
        
            def name(self):
                return "rate1"
            
            def description(self):
                return "STS Rate 0-63 type"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _sftrsh1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 4
        
            def name(self):
                return "sftrsh1"
            
            def description(self):
                return "SF threshold raise channel 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _sdtrsh1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 1
        
            def name(self):
                return "sdtrsh1"
            
            def description(self):
                return "SD threshold raise channel 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ena1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ena1"
            
            def description(self):
                return "Enable channel 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tcatrsh2"] = _AF6CCI0031_RD_POH_BER._imemrwpctrl2._tcatrsh2()
            allFields["tcatrsh1"] = _AF6CCI0031_RD_POH_BER._imemrwpctrl2._tcatrsh1()
            allFields["rate2"] = _AF6CCI0031_RD_POH_BER._imemrwpctrl2._rate2()
            allFields["sftrsh2"] = _AF6CCI0031_RD_POH_BER._imemrwpctrl2._sftrsh2()
            allFields["sdtrsh2"] = _AF6CCI0031_RD_POH_BER._imemrwpctrl2._sdtrsh2()
            allFields["ena2"] = _AF6CCI0031_RD_POH_BER._imemrwpctrl2._ena2()
            allFields["rate1"] = _AF6CCI0031_RD_POH_BER._imemrwpctrl2._rate1()
            allFields["sftrsh1"] = _AF6CCI0031_RD_POH_BER._imemrwpctrl2._sftrsh1()
            allFields["sdtrsh1"] = _AF6CCI0031_RD_POH_BER._imemrwpctrl2._sdtrsh1()
            allFields["ena1"] = _AF6CCI0031_RD_POH_BER._imemrwpctrl2._ena1()
            return allFields

    class _ramberratevtds(AtRegister.AtRegister):
        def name(self):
            return "POH BER Report VT/DSN"
    
        def description(self):
            return "This register is used to get current BER rate ."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x06_8000 + $STS*112 + $OCID*28 + $VT"
            
        def startAddress(self):
            return 0x00068000
            
        def endAddress(self):
            return 0xffffffff

        class _hwsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "hwsta"
            
            def description(self):
                return "Hardware status"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _rate(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rate"
            
            def description(self):
                return "BER rate"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["hwsta"] = _AF6CCI0031_RD_POH_BER._ramberratevtds._hwsta()
            allFields["rate"] = _AF6CCI0031_RD_POH_BER._ramberratevtds._rate()
            return allFields

    class _ramberrateststu3(AtRegister.AtRegister):
        def name(self):
            return "POH BER Report STS/TU3"
    
        def description(self):
            return "This register is used to get current BER rate . BER DE3 used with OCID 2-3, TU3TYPE = 1. BER EC1 used with OCID 2-3, TU3TYPE = 0."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x06_8B00 + $STS*8 + $OCID*2 + $TU3TYPE"
            
        def startAddress(self):
            return 0x00068b00
            
        def endAddress(self):
            return 0xffffffff

        class _hwsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "hwsta"
            
            def description(self):
                return "Hardware status"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _rate(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rate"
            
            def description(self):
                return "BER rate"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["hwsta"] = _AF6CCI0031_RD_POH_BER._ramberrateststu3._hwsta()
            allFields["rate"] = _AF6CCI0031_RD_POH_BER._ramberrateststu3._rate()
            return allFields

    class _ipm_cnthi(AtRegister.AtRegister):
        def name(self):
            return "POH Counter Report STS"
    
        def description(self):
            return "This register is used to get POH Counter. OCID 2,3 is used for B2 counter with 24 B2 each OCID."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0C_8580 + $STS*4 + $OCID"
            
        def startAddress(self):
            return 0x000c8580
            
        def endAddress(self):
            return 0xffffffff

        class _reicnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "reicnt"
            
            def description(self):
                return "REI counter"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        class _bipcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "bipcnt"
            
            def description(self):
                return "BIP counter"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["reicnt"] = _AF6CCI0031_RD_POH_BER._ipm_cnthi._reicnt()
            allFields["bipcnt"] = _AF6CCI0031_RD_POH_BER._ipm_cnthi._bipcnt()
            return allFields

    class _ipm_cntlo(AtRegister.AtRegister):
        def name(self):
            return "POH Counter Report TU3/VT"
    
        def description(self):
            return "This register is used to get POH Counter, Rx SDH pointer increase, decrease counter."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0C_8000 + $STS*56 + $OCID*28 + $VT"
            
        def startAddress(self):
            return 0x000c8000
            
        def endAddress(self):
            return 0xffffffff

        class _reicnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "reicnt"
            
            def description(self):
                return "REI counter"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        class _bipcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "bipcnt"
            
            def description(self):
                return "BIP counter"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["reicnt"] = _AF6CCI0031_RD_POH_BER._ipm_cntlo._reicnt()
            allFields["bipcnt"] = _AF6CCI0031_RD_POH_BER._ipm_cntlo._bipcnt()
            return allFields

    class _alm_mskhi(AtRegister.AtRegister):
        def name(self):
            return "POH Alarm Status Mask Report STS"
    
        def description(self):
            return "This register is used to get POH alarm mask report."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0D_0000 + $STS + $OCID*128"
            
        def startAddress(self):
            return 0x000d0000
            
        def endAddress(self):
            return 0xffffffff

        class _bersdmsk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "bersdmsk"
            
            def description(self):
                return "bersd mask"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _bersfmsk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "bersfmsk"
            
            def description(self):
                return "bersf  mask"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _erdimsk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "erdimsk"
            
            def description(self):
                return "erdi mask"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _bertcamsk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "bertcamsk"
            
            def description(self):
                return "bertca mask"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _jnstbmsk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "jnstbmsk"
            
            def description(self):
                return "jn stable mask"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _pslstbmsk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "pslstbmsk"
            
            def description(self):
                return "psl stable mask"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _rfimsk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "rfimsk"
            
            def description(self):
                return "rfi/lom mask"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _timmsk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "timmsk"
            
            def description(self):
                return "tim mask"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _uneqmsk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "uneqmsk"
            
            def description(self):
                return "uneq mask"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _plmmsk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "plmmsk"
            
            def description(self):
                return "plm mask"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _aismsk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "aismsk"
            
            def description(self):
                return "ais mask"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _lopmsk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lopmsk"
            
            def description(self):
                return "lop mask"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["bersdmsk"] = _AF6CCI0031_RD_POH_BER._alm_mskhi._bersdmsk()
            allFields["bersfmsk"] = _AF6CCI0031_RD_POH_BER._alm_mskhi._bersfmsk()
            allFields["erdimsk"] = _AF6CCI0031_RD_POH_BER._alm_mskhi._erdimsk()
            allFields["bertcamsk"] = _AF6CCI0031_RD_POH_BER._alm_mskhi._bertcamsk()
            allFields["jnstbmsk"] = _AF6CCI0031_RD_POH_BER._alm_mskhi._jnstbmsk()
            allFields["pslstbmsk"] = _AF6CCI0031_RD_POH_BER._alm_mskhi._pslstbmsk()
            allFields["rfimsk"] = _AF6CCI0031_RD_POH_BER._alm_mskhi._rfimsk()
            allFields["timmsk"] = _AF6CCI0031_RD_POH_BER._alm_mskhi._timmsk()
            allFields["uneqmsk"] = _AF6CCI0031_RD_POH_BER._alm_mskhi._uneqmsk()
            allFields["plmmsk"] = _AF6CCI0031_RD_POH_BER._alm_mskhi._plmmsk()
            allFields["aismsk"] = _AF6CCI0031_RD_POH_BER._alm_mskhi._aismsk()
            allFields["lopmsk"] = _AF6CCI0031_RD_POH_BER._alm_mskhi._lopmsk()
            return allFields

    class _alm_stahi(AtRegister.AtRegister):
        def name(self):
            return "POH Alarm Status Report STS"
    
        def description(self):
            return "This register is used to get POH alarm status report."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0D_0040 + $STS + $OCID*128"
            
        def startAddress(self):
            return 0x000d0040
            
        def endAddress(self):
            return 0xffffffff

        class _bersdsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "bersdsta"
            
            def description(self):
                return "bersd  status"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _bersfsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "bersfsta"
            
            def description(self):
                return "bersf  status"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _erdista(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "erdista"
            
            def description(self):
                return "erdi status status"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _bertcasta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "bertcasta"
            
            def description(self):
                return "bertca status"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _jnstbsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "jnstbsta"
            
            def description(self):
                return "jn stable status"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _pslstbsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "pslstbsta"
            
            def description(self):
                return "psl stable status"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _rfista(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "rfista"
            
            def description(self):
                return "rfi/lom status"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _timsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "timsta"
            
            def description(self):
                return "tim status"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _uneqsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "uneqsta"
            
            def description(self):
                return "uneq status"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _plmsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "plmsta"
            
            def description(self):
                return "plm status"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _aissta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "aissta"
            
            def description(self):
                return "ais status"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _lopsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lopsta"
            
            def description(self):
                return "lop status"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["bersdsta"] = _AF6CCI0031_RD_POH_BER._alm_stahi._bersdsta()
            allFields["bersfsta"] = _AF6CCI0031_RD_POH_BER._alm_stahi._bersfsta()
            allFields["erdista"] = _AF6CCI0031_RD_POH_BER._alm_stahi._erdista()
            allFields["bertcasta"] = _AF6CCI0031_RD_POH_BER._alm_stahi._bertcasta()
            allFields["jnstbsta"] = _AF6CCI0031_RD_POH_BER._alm_stahi._jnstbsta()
            allFields["pslstbsta"] = _AF6CCI0031_RD_POH_BER._alm_stahi._pslstbsta()
            allFields["rfista"] = _AF6CCI0031_RD_POH_BER._alm_stahi._rfista()
            allFields["timsta"] = _AF6CCI0031_RD_POH_BER._alm_stahi._timsta()
            allFields["uneqsta"] = _AF6CCI0031_RD_POH_BER._alm_stahi._uneqsta()
            allFields["plmsta"] = _AF6CCI0031_RD_POH_BER._alm_stahi._plmsta()
            allFields["aissta"] = _AF6CCI0031_RD_POH_BER._alm_stahi._aissta()
            allFields["lopsta"] = _AF6CCI0031_RD_POH_BER._alm_stahi._lopsta()
            return allFields

    class _alm_chghi(AtRegister.AtRegister):
        def name(self):
            return "POH Interrupt Status Report STS"
    
        def description(self):
            return "This register is used to get POH alarm change status report."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0D_0020 + $STS + $OCID*128"
            
        def startAddress(self):
            return 0x000d0020
            
        def endAddress(self):
            return 0xffffffff

        class _bersdstachg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "bersdstachg"
            
            def description(self):
                return "bersd stable status change"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _bersfstachg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "bersfstachg"
            
            def description(self):
                return "bersf stable status change"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _erdistachg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "erdistachg"
            
            def description(self):
                return "erdi status change"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _bertcastachg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "bertcastachg"
            
            def description(self):
                return "bertca status change"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _jnstbstachg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "jnstbstachg"
            
            def description(self):
                return "jn stable status change"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _pslstbstachg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "pslstbstachg"
            
            def description(self):
                return "psl stable status change"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _rfistachg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "rfistachg"
            
            def description(self):
                return "rfi/lom status change"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _timstachg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "timstachg"
            
            def description(self):
                return "tim status change"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _uneqstachg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "uneqstachg"
            
            def description(self):
                return "uneq status change"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _plmstachg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "plmstachg"
            
            def description(self):
                return "plm status change"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _aisstachg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "aisstachg"
            
            def description(self):
                return "ais status change"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _lopstachg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lopstachg"
            
            def description(self):
                return "lop status change"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["bersdstachg"] = _AF6CCI0031_RD_POH_BER._alm_chghi._bersdstachg()
            allFields["bersfstachg"] = _AF6CCI0031_RD_POH_BER._alm_chghi._bersfstachg()
            allFields["erdistachg"] = _AF6CCI0031_RD_POH_BER._alm_chghi._erdistachg()
            allFields["bertcastachg"] = _AF6CCI0031_RD_POH_BER._alm_chghi._bertcastachg()
            allFields["jnstbstachg"] = _AF6CCI0031_RD_POH_BER._alm_chghi._jnstbstachg()
            allFields["pslstbstachg"] = _AF6CCI0031_RD_POH_BER._alm_chghi._pslstbstachg()
            allFields["rfistachg"] = _AF6CCI0031_RD_POH_BER._alm_chghi._rfistachg()
            allFields["timstachg"] = _AF6CCI0031_RD_POH_BER._alm_chghi._timstachg()
            allFields["uneqstachg"] = _AF6CCI0031_RD_POH_BER._alm_chghi._uneqstachg()
            allFields["plmstachg"] = _AF6CCI0031_RD_POH_BER._alm_chghi._plmstachg()
            allFields["aisstachg"] = _AF6CCI0031_RD_POH_BER._alm_chghi._aisstachg()
            allFields["lopstachg"] = _AF6CCI0031_RD_POH_BER._alm_chghi._lopstachg()
            return allFields

    class _alm_glbchghi(AtRegister.AtRegister):
        def name(self):
            return "POH Interrupt Global Status Report STS"
    
        def description(self):
            return "This register is used to get POH alarm global change status report."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0D_007F + $OCID*128"
            
        def startAddress(self):
            return 0x000d007f
            
        def endAddress(self):
            return 0xffffffff

        class _glbstachg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "glbstachg"
            
            def description(self):
                return "global status change bit"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["glbstachg"] = _AF6CCI0031_RD_POH_BER._alm_glbchghi._glbstachg()
            return allFields

    class _alm_glbmskhi(AtRegister.AtRegister):
        def name(self):
            return "POH Interrupt Global Mask Report STS"
    
        def description(self):
            return "This register is used to get POH alarm global mask report."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0D_007E + $OCID*128"
            
        def startAddress(self):
            return 0x000d007e
            
        def endAddress(self):
            return 0xffffffff

        class _glbmsk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "glbmsk"
            
            def description(self):
                return "global mask"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["glbmsk"] = _AF6CCI0031_RD_POH_BER._alm_glbmskhi._glbmsk()
            return allFields

    class _alm_msklo(AtRegister.AtRegister):
        def name(self):
            return "POH Alarm Status Mask Report VT/TU3"
    
        def description(self):
            return "This register is used to get POH alarm mask report."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0E_0000 + $STS*32 + $OCID*4096 + $VTID"
            
        def startAddress(self):
            return 0x000e0000
            
        def endAddress(self):
            return 0xffffffff

        class _bersdmsk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "bersdmsk"
            
            def description(self):
                return "bersd mask"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _bersfmsk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "bersfmsk"
            
            def description(self):
                return "bersf  mask"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _erdimsk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "erdimsk"
            
            def description(self):
                return "erdi mask"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _bertcamsk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "bertcamsk"
            
            def description(self):
                return "bertca mask"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _jnstbmsk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "jnstbmsk"
            
            def description(self):
                return "jn stable mask"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _pslstbmsk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "pslstbmsk"
            
            def description(self):
                return "psl stable mask"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _rfimsk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "rfimsk"
            
            def description(self):
                return "rfi mask"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _timmsk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "timmsk"
            
            def description(self):
                return "tim mask"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _uneqmsk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "uneqmsk"
            
            def description(self):
                return "uneq mask"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _plmmsk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "plmmsk"
            
            def description(self):
                return "plm mask"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _aismsk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "aismsk"
            
            def description(self):
                return "ais mask"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _lopmsk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lopmsk"
            
            def description(self):
                return "lop mask"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["bersdmsk"] = _AF6CCI0031_RD_POH_BER._alm_msklo._bersdmsk()
            allFields["bersfmsk"] = _AF6CCI0031_RD_POH_BER._alm_msklo._bersfmsk()
            allFields["erdimsk"] = _AF6CCI0031_RD_POH_BER._alm_msklo._erdimsk()
            allFields["bertcamsk"] = _AF6CCI0031_RD_POH_BER._alm_msklo._bertcamsk()
            allFields["jnstbmsk"] = _AF6CCI0031_RD_POH_BER._alm_msklo._jnstbmsk()
            allFields["pslstbmsk"] = _AF6CCI0031_RD_POH_BER._alm_msklo._pslstbmsk()
            allFields["rfimsk"] = _AF6CCI0031_RD_POH_BER._alm_msklo._rfimsk()
            allFields["timmsk"] = _AF6CCI0031_RD_POH_BER._alm_msklo._timmsk()
            allFields["uneqmsk"] = _AF6CCI0031_RD_POH_BER._alm_msklo._uneqmsk()
            allFields["plmmsk"] = _AF6CCI0031_RD_POH_BER._alm_msklo._plmmsk()
            allFields["aismsk"] = _AF6CCI0031_RD_POH_BER._alm_msklo._aismsk()
            allFields["lopmsk"] = _AF6CCI0031_RD_POH_BER._alm_msklo._lopmsk()
            return allFields

    class _alm_stalo(AtRegister.AtRegister):
        def name(self):
            return "POH Alarm Status Report VT/TU3"
    
        def description(self):
            return "This register is used to get POH alarm status."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0E_0800 + $STS*32 + $OCID*4096 + $VTID"
            
        def startAddress(self):
            return 0x000e0800
            
        def endAddress(self):
            return 0xffffffff

        class _bersdsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "bersdsta"
            
            def description(self):
                return "bersd status"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _bersfsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "bersfsta"
            
            def description(self):
                return "bersf  status"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _erdista(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "erdista"
            
            def description(self):
                return "erdi status status"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _bertcasta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "bertcasta"
            
            def description(self):
                return "bertca status"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _jnstbsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "jnstbsta"
            
            def description(self):
                return "jn stable status"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _pslstbsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "pslstbsta"
            
            def description(self):
                return "psl stable status"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _rfista(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "rfista"
            
            def description(self):
                return "rfi status"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _timsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "timsta"
            
            def description(self):
                return "tim status"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _uneqsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "uneqsta"
            
            def description(self):
                return "uneq status"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _plmsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "plmsta"
            
            def description(self):
                return "plm status"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _aissta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "aissta"
            
            def description(self):
                return "ais status"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _lopsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lopsta"
            
            def description(self):
                return "lop status"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["bersdsta"] = _AF6CCI0031_RD_POH_BER._alm_stalo._bersdsta()
            allFields["bersfsta"] = _AF6CCI0031_RD_POH_BER._alm_stalo._bersfsta()
            allFields["erdista"] = _AF6CCI0031_RD_POH_BER._alm_stalo._erdista()
            allFields["bertcasta"] = _AF6CCI0031_RD_POH_BER._alm_stalo._bertcasta()
            allFields["jnstbsta"] = _AF6CCI0031_RD_POH_BER._alm_stalo._jnstbsta()
            allFields["pslstbsta"] = _AF6CCI0031_RD_POH_BER._alm_stalo._pslstbsta()
            allFields["rfista"] = _AF6CCI0031_RD_POH_BER._alm_stalo._rfista()
            allFields["timsta"] = _AF6CCI0031_RD_POH_BER._alm_stalo._timsta()
            allFields["uneqsta"] = _AF6CCI0031_RD_POH_BER._alm_stalo._uneqsta()
            allFields["plmsta"] = _AF6CCI0031_RD_POH_BER._alm_stalo._plmsta()
            allFields["aissta"] = _AF6CCI0031_RD_POH_BER._alm_stalo._aissta()
            allFields["lopsta"] = _AF6CCI0031_RD_POH_BER._alm_stalo._lopsta()
            return allFields

    class _alm_chglo(AtRegister.AtRegister):
        def name(self):
            return "POH Interrupt Status Report STS"
    
        def description(self):
            return "This register is used to get POH alarm change status report."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0E_0400 + $STS*32 + $OCID*4096 + $VTID"
            
        def startAddress(self):
            return 0x000e0400
            
        def endAddress(self):
            return 0xffffffff

        class _bersdstachg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "bersdstachg"
            
            def description(self):
                return "bersd stable status change"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _bersfstachg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "bersfstachg"
            
            def description(self):
                return "bersf stable status change"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _erdistachg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "erdistachg"
            
            def description(self):
                return "erdi status change"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _bertcastachg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "bertcastachg"
            
            def description(self):
                return "bertca status change"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _jnstbstachg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "jnstbstachg"
            
            def description(self):
                return "jn stable status change"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _pslstbstachg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "pslstbstachg"
            
            def description(self):
                return "psl stable status change"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _rfistachg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "rfistachg"
            
            def description(self):
                return "rfi status change"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _timstachg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "timstachg"
            
            def description(self):
                return "tim status change"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _uneqstachg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "uneqstachg"
            
            def description(self):
                return "uneq status change"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _plmstachg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "plmstachg"
            
            def description(self):
                return "plm status change"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _aisstachg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "aisstachg"
            
            def description(self):
                return "ais status change"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _lopstachg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lopstachg"
            
            def description(self):
                return "lop status change"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["bersdstachg"] = _AF6CCI0031_RD_POH_BER._alm_chglo._bersdstachg()
            allFields["bersfstachg"] = _AF6CCI0031_RD_POH_BER._alm_chglo._bersfstachg()
            allFields["erdistachg"] = _AF6CCI0031_RD_POH_BER._alm_chglo._erdistachg()
            allFields["bertcastachg"] = _AF6CCI0031_RD_POH_BER._alm_chglo._bertcastachg()
            allFields["jnstbstachg"] = _AF6CCI0031_RD_POH_BER._alm_chglo._jnstbstachg()
            allFields["pslstbstachg"] = _AF6CCI0031_RD_POH_BER._alm_chglo._pslstbstachg()
            allFields["rfistachg"] = _AF6CCI0031_RD_POH_BER._alm_chglo._rfistachg()
            allFields["timstachg"] = _AF6CCI0031_RD_POH_BER._alm_chglo._timstachg()
            allFields["uneqstachg"] = _AF6CCI0031_RD_POH_BER._alm_chglo._uneqstachg()
            allFields["plmstachg"] = _AF6CCI0031_RD_POH_BER._alm_chglo._plmstachg()
            allFields["aisstachg"] = _AF6CCI0031_RD_POH_BER._alm_chglo._aisstachg()
            allFields["lopstachg"] = _AF6CCI0031_RD_POH_BER._alm_chglo._lopstachg()
            return allFields

    class _alm_orstalo(AtRegister.AtRegister):
        def name(self):
            return "POH Interrupt Or Status Report VT/TU3"
    
        def description(self):
            return "This register is used to get POH alarm or status change status report."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0E_0C00 + $STS + $OCID*4096"
            
        def startAddress(self):
            return 0x000e0c00
            
        def endAddress(self):
            return 0xffffffff

        class _orstachg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "orstachg"
            
            def description(self):
                return "or status change bit"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["orstachg"] = _AF6CCI0031_RD_POH_BER._alm_orstalo._orstachg()
            return allFields

    class _alm_glbchglo(AtRegister.AtRegister):
        def name(self):
            return "POH Interrupt Global Status Report VT/TU3"
    
        def description(self):
            return "This register is used to get POH alarm global change status report."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0E_0FFF + $OCID*4096"
            
        def startAddress(self):
            return 0x000e0fff
            
        def endAddress(self):
            return 0xffffffff

        class _glbstachg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "glbstachg"
            
            def description(self):
                return "global status change bit"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["glbstachg"] = _AF6CCI0031_RD_POH_BER._alm_glbchglo._glbstachg()
            return allFields

    class _alm_glbmsklo(AtRegister.AtRegister):
        def name(self):
            return "POH Interrupt Global Mask Report VT/TU3"
    
        def description(self):
            return "This register is used to get POH alarm global mask report."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0E_0FFE + $OCID*4096"
            
        def startAddress(self):
            return 0x000e0ffe
            
        def endAddress(self):
            return 0xffffffff

        class _glbmsk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "glbmsk"
            
            def description(self):
                return "global status change bit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["glbmsk"] = _AF6CCI0031_RD_POH_BER._alm_glbmsklo._glbmsk()
            return allFields

    class _alm_glbmsk(AtRegister.AtRegister):
        def name(self):
            return "POH Interrupt Global Mask Report"
    
        def description(self):
            return "This register is used to get POH alarm global mask report for high,low order."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000004
            
        def endAddress(self):
            return 0xffffffff

        class _glbmsklo(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 16
        
            def name(self):
                return "glbmsklo"
            
            def description(self):
                return "global mask bit for low order slice - ocid1,ocid0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _glbmskhi(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "glbmskhi"
            
            def description(self):
                return "global mask change bit for high order slice - ocid3,ocid1,ocid2,ocid0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["glbmsklo"] = _AF6CCI0031_RD_POH_BER._alm_glbmsk._glbmsklo()
            allFields["glbmskhi"] = _AF6CCI0031_RD_POH_BER._alm_glbmsk._glbmskhi()
            return allFields

    class _alm_glbchg(AtRegister.AtRegister):
        def name(self):
            return "POH Interrupt Global Status Report"
    
        def description(self):
            return "This register is used to get POH alarm global change status report for high,low order."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000005
            
        def endAddress(self):
            return 0xffffffff

        class _glbstachglo(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 16
        
            def name(self):
                return "glbstachglo"
            
            def description(self):
                return "global status change bit for low order slice - ocid1,ocid0"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _glbstachghi(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "glbstachghi"
            
            def description(self):
                return "global status change bit for high order slice - ocid3,ocid1,ocid2,ocid0"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["glbstachglo"] = _AF6CCI0031_RD_POH_BER._alm_glbchg._glbstachglo()
            allFields["glbstachghi"] = _AF6CCI0031_RD_POH_BER._alm_glbchg._glbstachghi()
            return allFields

    class _alm_glbchgo(AtRegister.AtRegister):
        def name(self):
            return "POH Interrupt  Global Status Out Report"
    
        def description(self):
            return "This register is used to get POH alarm global change status report for high,low order after ANDED with mask."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000006
            
        def endAddress(self):
            return 0xffffffff

        class _glbstachglo(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 16
        
            def name(self):
                return "glbstachglo"
            
            def description(self):
                return "global status change bit for low order slice - ocid1,ocid0"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _glbstachghi(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "glbstachghi"
            
            def description(self):
                return "global status change bit for high order slice - ocid3,ocid1,ocid2,ocid0"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["glbstachglo"] = _AF6CCI0031_RD_POH_BER._alm_glbchgo._glbstachglo()
            allFields["glbstachghi"] = _AF6CCI0031_RD_POH_BER._alm_glbchgo._glbstachghi()
            return allFields

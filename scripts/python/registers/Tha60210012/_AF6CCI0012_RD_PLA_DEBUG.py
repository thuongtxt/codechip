import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0012_RD_PLA_DEBUG(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["pla_lo_clk155_stk"] = _AF6CCI0012_RD_PLA_DEBUG._pla_lo_clk155_stk()
        allRegisters["pla_ho_clk311_stk"] = _AF6CCI0012_RD_PLA_DEBUG._pla_ho_clk311_stk()
        allRegisters["pla_interface1_stk"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface1_stk()
        allRegisters["pla_interface2_stk"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface2_stk()
        allRegisters["pla_debug1_stk"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug1_stk()
        allRegisters["pla_debug2_stk"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug2_stk()
        allRegisters["pla_debug_sta"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug_sta()
        return allRegisters

    class _pla_lo_clk155_stk(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Low-Order clk155 Sticky"
    
        def description(self):
            return "This register is used to used to sticky some alarms for debug per OC-24 Lo-order @clk155.52 domain"
            
        def width(self):
            return 9
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0_0001 + $LoOc48Slice*32768 + $LoOc24Slice*8192"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _PlaLo155OC24ConvErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PlaLo155OC24ConvErr"
            
            def description(self):
                return "OC24 convert clock Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo155OC24InPwVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "PlaLo155OC24InPwVld"
            
            def description(self):
                return "OC24 input of expected pwid"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo155OC24InVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PlaLo155OC24InVld"
            
            def description(self):
                return "OC24 input data from demap"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo155OC24InFst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PlaLo155OC24InFst"
            
            def description(self):
                return "OC24 input first-timeslot"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo155OC24InAIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PlaLo155OC24InAIS"
            
            def description(self):
                return "OC24 input AIS"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo155OC24InRDI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "PlaLo155OC24InRDI"
            
            def description(self):
                return "OC24 input RDI"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo155OC24InPos(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PlaLo155OC24InPos"
            
            def description(self):
                return "OC24 input CEP Pos Pointer"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo155OC24InNeg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PlaLo155OC24InNeg"
            
            def description(self):
                return "OC24 input CEP Neg Pointer"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo155OC24InJ1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaLo155OC24InJ1"
            
            def description(self):
                return "OC24 input CEP J1"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaLo155OC24ConvErr"] = _AF6CCI0012_RD_PLA_DEBUG._pla_lo_clk155_stk._PlaLo155OC24ConvErr()
            allFields["PlaLo155OC24InPwVld"] = _AF6CCI0012_RD_PLA_DEBUG._pla_lo_clk155_stk._PlaLo155OC24InPwVld()
            allFields["PlaLo155OC24InVld"] = _AF6CCI0012_RD_PLA_DEBUG._pla_lo_clk155_stk._PlaLo155OC24InVld()
            allFields["PlaLo155OC24InFst"] = _AF6CCI0012_RD_PLA_DEBUG._pla_lo_clk155_stk._PlaLo155OC24InFst()
            allFields["PlaLo155OC24InAIS"] = _AF6CCI0012_RD_PLA_DEBUG._pla_lo_clk155_stk._PlaLo155OC24InAIS()
            allFields["PlaLo155OC24InRDI"] = _AF6CCI0012_RD_PLA_DEBUG._pla_lo_clk155_stk._PlaLo155OC24InRDI()
            allFields["PlaLo155OC24InPos"] = _AF6CCI0012_RD_PLA_DEBUG._pla_lo_clk155_stk._PlaLo155OC24InPos()
            allFields["PlaLo155OC24InNeg"] = _AF6CCI0012_RD_PLA_DEBUG._pla_lo_clk155_stk._PlaLo155OC24InNeg()
            allFields["PlaLo155OC24InJ1"] = _AF6CCI0012_RD_PLA_DEBUG._pla_lo_clk155_stk._PlaLo155OC24InJ1()
            return allFields

    class _pla_ho_clk311_stk(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler High-Order clk311 Sticky"
    
        def description(self):
            return "This register is used to used to sticky some alarms for debug per OC-48 Ho-order @clk311.04 domain"
            
        def width(self):
            return 9
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1_0001 + $HoOc48Slice*256"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _PlaLo311OC24ConvErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PlaLo311OC24ConvErr"
            
            def description(self):
                return "OC48 convert clock Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo311OC24InPwVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "PlaLo311OC24InPwVld"
            
            def description(self):
                return "OC48 input of expected pwid"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo311OC24InVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PlaLo311OC24InVld"
            
            def description(self):
                return "OC4824 input data from demap"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo311OC24InAIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PlaLo311OC24InAIS"
            
            def description(self):
                return "OC24 input AIS"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo311OC24InPos(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PlaLo311OC24InPos"
            
            def description(self):
                return "OC48 input CEP Pos Pointer"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo311OC24InNeg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PlaLo311OC24InNeg"
            
            def description(self):
                return "OC48 input CEP Neg Pointer"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo311OC24InJ1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaLo311OC24InJ1"
            
            def description(self):
                return "OC48 input CEP J1"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaLo311OC24ConvErr"] = _AF6CCI0012_RD_PLA_DEBUG._pla_ho_clk311_stk._PlaLo311OC24ConvErr()
            allFields["PlaLo311OC24InPwVld"] = _AF6CCI0012_RD_PLA_DEBUG._pla_ho_clk311_stk._PlaLo311OC24InPwVld()
            allFields["PlaLo311OC24InVld"] = _AF6CCI0012_RD_PLA_DEBUG._pla_ho_clk311_stk._PlaLo311OC24InVld()
            allFields["PlaLo311OC24InAIS"] = _AF6CCI0012_RD_PLA_DEBUG._pla_ho_clk311_stk._PlaLo311OC24InAIS()
            allFields["PlaLo311OC24InPos"] = _AF6CCI0012_RD_PLA_DEBUG._pla_ho_clk311_stk._PlaLo311OC24InPos()
            allFields["PlaLo311OC24InNeg"] = _AF6CCI0012_RD_PLA_DEBUG._pla_ho_clk311_stk._PlaLo311OC24InNeg()
            allFields["PlaLo311OC24InJ1"] = _AF6CCI0012_RD_PLA_DEBUG._pla_ho_clk311_stk._PlaLo311OC24InJ1()
            return allFields

    class _pla_interface1_stk(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Interface#1 Sticky"
    
        def description(self):
            return "This register is used to used to sticky some intefrace of PLA"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00021010
            
        def endAddress(self):
            return 0xffffffff

        class _PlaOutPWEFlowVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "PlaOutPWEFlowVld"
            
            def description(self):
                return "Output PWE expected Flow"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutPWEReq(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "PlaOutPWEReq"
            
            def description(self):
                return "Output Request to PWE"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaInPWEGet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "PlaInPWEGet"
            
            def description(self):
                return "Input PWE get data"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutPWEVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "PlaOutPWEVld"
            
            def description(self):
                return "Output Valid to PWE"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaInVldFromRSQ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "PlaInVldFromRSQ"
            
            def description(self):
                return "Input Info from RSQ"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutEn2RSQ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "PlaOutEn2RSQ"
            
            def description(self):
                return "Output Enable to RSQ"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutVld2RSQ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "PlaOutVld2RSQ"
            
            def description(self):
                return "Output Info to RSQ"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaInMSGErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "PlaInMSGErr"
            
            def description(self):
                return "Input MSG Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaInMSGSoP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "PlaInMSGSoP"
            
            def description(self):
                return "Input MSG Start Packet"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaInMSGEoP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "PlaInMSGEoP"
            
            def description(self):
                return "Input MSG End Packet"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaInMSGLidVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "PlaInMSGLidVld"
            
            def description(self):
                return "Input MSG expected Lid"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaInMSGVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "PlaInMSGVld"
            
            def description(self):
                return "Input MSG Valid"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaInVCGSoP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "PlaInVCGSoP"
            
            def description(self):
                return "Input VCG Start Packet"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaInVCGEoP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "PlaInVCGEoP"
            
            def description(self):
                return "Input VCG End Packet"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaInVCGLidVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "PlaInVCGLidVld"
            
            def description(self):
                return "Input VCG expected Lid"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaInVCGVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PlaInVCGVld"
            
            def description(self):
                return "Input VCG Valid"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaInVCGErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "PlaInVCGErr"
            
            def description(self):
                return "Input VCG Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutVcatPkLidVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "PlaOutVcatPkLidVld"
            
            def description(self):
                return "Output VCAT Pkt expected Lid"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutVcatPkVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PlaOutVcatPkVld"
            
            def description(self):
                return "Output VCAT Pkt Vld"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutVcatPkEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "PlaOutVcatPkEn"
            
            def description(self):
                return "Output VCAT Pkt Enable"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaInVcatPkLidVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "PlaInVcatPkLidVld"
            
            def description(self):
                return "Input VCAT Pkt expected Lid"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaInVcatPkVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PlaInVcatPkVld"
            
            def description(self):
                return "Input VCAT Pkt Valid"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutVCATTDMLidVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PlaOutVCATTDMLidVld"
            
            def description(self):
                return "Output VCAT TDM expected Lid"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutVcatTDMVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PlaOutVcatTDMVld"
            
            def description(self):
                return "Output VCAT TDM Valid"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaInVcatTDMSoP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "PlaInVcatTDMSoP"
            
            def description(self):
                return "Input VCAT TDM Start Packet"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaInVcatTDMEoP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PlaInVcatTDMEoP"
            
            def description(self):
                return "Input VCAT TDM End Packet"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaInVcatTDMLidVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PlaInVcatTDMLidVld"
            
            def description(self):
                return "Input VCAT TDM expected Lid"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaInVcatTDMVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaInVcatTDMVld"
            
            def description(self):
                return "Input VCAT TDM Valid"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaOutPWEFlowVld"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface1_stk._PlaOutPWEFlowVld()
            allFields["PlaOutPWEReq"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface1_stk._PlaOutPWEReq()
            allFields["PlaInPWEGet"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface1_stk._PlaInPWEGet()
            allFields["PlaOutPWEVld"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface1_stk._PlaOutPWEVld()
            allFields["PlaInVldFromRSQ"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface1_stk._PlaInVldFromRSQ()
            allFields["PlaOutEn2RSQ"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface1_stk._PlaOutEn2RSQ()
            allFields["PlaOutVld2RSQ"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface1_stk._PlaOutVld2RSQ()
            allFields["PlaInMSGErr"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface1_stk._PlaInMSGErr()
            allFields["PlaInMSGSoP"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface1_stk._PlaInMSGSoP()
            allFields["PlaInMSGEoP"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface1_stk._PlaInMSGEoP()
            allFields["PlaInMSGLidVld"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface1_stk._PlaInMSGLidVld()
            allFields["PlaInMSGVld"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface1_stk._PlaInMSGVld()
            allFields["PlaInVCGSoP"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface1_stk._PlaInVCGSoP()
            allFields["PlaInVCGEoP"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface1_stk._PlaInVCGEoP()
            allFields["PlaInVCGLidVld"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface1_stk._PlaInVCGLidVld()
            allFields["PlaInVCGVld"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface1_stk._PlaInVCGVld()
            allFields["PlaInVCGErr"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface1_stk._PlaInVCGErr()
            allFields["PlaOutVcatPkLidVld"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface1_stk._PlaOutVcatPkLidVld()
            allFields["PlaOutVcatPkVld"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface1_stk._PlaOutVcatPkVld()
            allFields["PlaOutVcatPkEn"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface1_stk._PlaOutVcatPkEn()
            allFields["PlaInVcatPkLidVld"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface1_stk._PlaInVcatPkLidVld()
            allFields["PlaInVcatPkVld"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface1_stk._PlaInVcatPkVld()
            allFields["PlaOutVCATTDMLidVld"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface1_stk._PlaOutVCATTDMLidVld()
            allFields["PlaOutVcatTDMVld"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface1_stk._PlaOutVcatTDMVld()
            allFields["PlaInVcatTDMSoP"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface1_stk._PlaInVcatTDMSoP()
            allFields["PlaInVcatTDMEoP"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface1_stk._PlaInVcatTDMEoP()
            allFields["PlaInVcatTDMLidVld"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface1_stk._PlaInVcatTDMLidVld()
            allFields["PlaInVcatTDMVld"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface1_stk._PlaInVcatTDMVld()
            return allFields

    class _pla_interface2_stk(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Interface#2 Sticky"
    
        def description(self):
            return "This register is used to used to sticky some intefrace of PLA"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00021011
            
        def endAddress(self):
            return 0xffffffff

        class _PlaRdDDRVldFFErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "PlaRdDDRVldFFErr"
            
            def description(self):
                return "ReadDDR VLD fifo err"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaRdDDRAckFFErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "PlaRdDDRAckFFErr"
            
            def description(self):
                return "ReadDDR ACK fifo err"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaRdDDRDatReqFFErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "PlaRdDDRDatReqFFErr"
            
            def description(self):
                return "ReadDDR Data Req fifo err"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaRdDDRVCATReqFFErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "PlaRdDDRVCATReqFFErr"
            
            def description(self):
                return "ReadDDR VCAT Req fifo err"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaRdPortPWPktVldFFErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "PlaRdPortPWPktVldFFErr"
            
            def description(self):
                return "ReadPort PW pkt VLD fifo err"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaRdPortPktVldFFErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "PlaRdPortPktVldFFErr"
            
            def description(self):
                return "ReadPort pkt VLD fifo err"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaRdPortDatFFErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "PlaRdPortDatFFErr"
            
            def description(self):
                return "ReadPort pkt data fifo err"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaRdPortInfFFErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "PlaRdPortInfFFErr"
            
            def description(self):
                return "ReadPort pkt info fifo err"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaRdBufPkFFErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "PlaRdBufPkFFErr"
            
            def description(self):
                return "ReadBuf pkt VLD fifo err"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaRdBufPkInfFFErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "PlaRdBufPkInfFFErr"
            
            def description(self):
                return "ReadBuf pkt info fifo err"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaRdBufPSNAckFFErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "PlaRdBufPSNAckFFErr"
            
            def description(self):
                return "ReadBuf PSN ACK fifo err"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaRdBufPSNVldFFErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PlaRdBufPSNVldFFErr"
            
            def description(self):
                return "ReadBuf PSN VLD fifo err"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPWCoreSoP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "PlaPWCoreSoP"
            
            def description(self):
                return "Output PW Core Start Packet"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPWCoreEoP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "PlaPWCoreEoP"
            
            def description(self):
                return "Output PW Core End Packet"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPWCoreVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PlaPWCoreVld"
            
            def description(self):
                return "Output PW Core Valid"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaInPSNVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "PlaInPSNVld"
            
            def description(self):
                return "Input PSN Vld"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaInPSNAck(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "PlaInPSNAck"
            
            def description(self):
                return "Input PSN Ack"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutPSNReq(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PlaOutPSNReq"
            
            def description(self):
                return "Output PSN Request"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaInDDRRdVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PlaInDDRRdVld"
            
            def description(self):
                return "Input Read DDR Vld"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaInDDRRdAck(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PlaInDDRRdAck"
            
            def description(self):
                return "Input Read DDR Ack"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutDDRRdReq(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PlaOutDDRRdReq"
            
            def description(self):
                return "Output Read DDR Request"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaInDDRWrVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PlaInDDRWrVld"
            
            def description(self):
                return "Input Write DDR Vld"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaInDDRWrAck(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PlaInDDRWrAck"
            
            def description(self):
                return "Input Write DDR Ack"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutDDRWrReq(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaOutDDRWrReq"
            
            def description(self):
                return "Output Write DDR Request"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaRdDDRVldFFErr"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface2_stk._PlaRdDDRVldFFErr()
            allFields["PlaRdDDRAckFFErr"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface2_stk._PlaRdDDRAckFFErr()
            allFields["PlaRdDDRDatReqFFErr"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface2_stk._PlaRdDDRDatReqFFErr()
            allFields["PlaRdDDRVCATReqFFErr"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface2_stk._PlaRdDDRVCATReqFFErr()
            allFields["PlaRdPortPWPktVldFFErr"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface2_stk._PlaRdPortPWPktVldFFErr()
            allFields["PlaRdPortPktVldFFErr"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface2_stk._PlaRdPortPktVldFFErr()
            allFields["PlaRdPortDatFFErr"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface2_stk._PlaRdPortDatFFErr()
            allFields["PlaRdPortInfFFErr"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface2_stk._PlaRdPortInfFFErr()
            allFields["PlaRdBufPkFFErr"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface2_stk._PlaRdBufPkFFErr()
            allFields["PlaRdBufPkInfFFErr"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface2_stk._PlaRdBufPkInfFFErr()
            allFields["PlaRdBufPSNAckFFErr"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface2_stk._PlaRdBufPSNAckFFErr()
            allFields["PlaRdBufPSNVldFFErr"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface2_stk._PlaRdBufPSNVldFFErr()
            allFields["PlaPWCoreSoP"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface2_stk._PlaPWCoreSoP()
            allFields["PlaPWCoreEoP"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface2_stk._PlaPWCoreEoP()
            allFields["PlaPWCoreVld"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface2_stk._PlaPWCoreVld()
            allFields["PlaInPSNVld"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface2_stk._PlaInPSNVld()
            allFields["PlaInPSNAck"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface2_stk._PlaInPSNAck()
            allFields["PlaOutPSNReq"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface2_stk._PlaOutPSNReq()
            allFields["PlaInDDRRdVld"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface2_stk._PlaInDDRRdVld()
            allFields["PlaInDDRRdAck"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface2_stk._PlaInDDRRdAck()
            allFields["PlaOutDDRRdReq"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface2_stk._PlaOutDDRRdReq()
            allFields["PlaInDDRWrVld"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface2_stk._PlaInDDRWrVld()
            allFields["PlaInDDRWrAck"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface2_stk._PlaInDDRWrAck()
            allFields["PlaOutDDRWrReq"] = _AF6CCI0012_RD_PLA_DEBUG._pla_interface2_stk._PlaOutDDRWrReq()
            return allFields

    class _pla_debug1_stk(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler debug Sticky"
    
        def description(self):
            return "This register is used to used to sticky some debug info of PLA"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00021012
            
        def endAddress(self):
            return 0xffffffff

        class _PlaCRCCheckErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "PlaCRCCheckErr"
            
            def description(self):
                return "CRC check error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaWrCacheSameErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "PlaWrCacheSameErr"
            
            def description(self):
                return "Write Cache same err"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaWrCacheEmpErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "PlaWrCacheEmpErr"
            
            def description(self):
                return "Write Cache empty err"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaWrCacheErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "PlaWrCacheErr"
            
            def description(self):
                return "Write Cache err"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaEnqVCGBlkEmpErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "PlaEnqVCGBlkEmpErr"
            
            def description(self):
                return "Enque VCG Block empty err"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaEnqVCGBlkSameErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "PlaEnqVCGBlkSameErr"
            
            def description(self):
                return "Enque VCG Block same err"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaWrDDRACKFFErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "PlaWrDDRACKFFErr"
            
            def description(self):
                return "WriteDDR ACK fifo err"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaEnqBlkCfgErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "PlaEnqBlkCfgErr"
            
            def description(self):
                return "Enque Block Cfg err"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaWrDDRVldRdyFFErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "PlaWrDDRVldRdyFFErr"
            
            def description(self):
                return "WriteDDR VLD ready fifo err"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaWrDDRVldFFErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "PlaWrDDRVldFFErr"
            
            def description(self):
                return "WriteDDR VLD fifo err"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaMuxPWCoreHOErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "PlaMuxPWCoreHOErr"
            
            def description(self):
                return "Mux PW Core HO err"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaMuxPWCoreLOErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PlaMuxPWCoreLOErr"
            
            def description(self):
                return "Mux PW Core LO err"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaMuxPWLOOC48_2Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "PlaMuxPWLOOC48_2Err"
            
            def description(self):
                return "Mux PW LO OC48_2 err"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaMuxPWLOOC48_1Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PlaMuxPWLOOC48_1Err"
            
            def description(self):
                return "Mux PW LO OC48_1 err"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaMuxSrcIn3Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PlaMuxSrcIn3Err"
            
            def description(self):
                return "Mux VCG input2 MSG err"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaMuxSrcIn2Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PlaMuxSrcIn2Err"
            
            def description(self):
                return "Mux VCG input2 VCAT err"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaMuxSrcIn1Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PlaMuxSrcIn1Err"
            
            def description(self):
                return "Mux VCG input1 PW err"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaMuxVCGIn2Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PlaMuxVCGIn2Err"
            
            def description(self):
                return "Mux VCG input2 VCG err"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaMuxVCGIn1Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaMuxVCGIn1Err"
            
            def description(self):
                return "Mux VCG input1 PW err"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaCRCCheckErr"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug1_stk._PlaCRCCheckErr()
            allFields["PlaWrCacheSameErr"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug1_stk._PlaWrCacheSameErr()
            allFields["PlaWrCacheEmpErr"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug1_stk._PlaWrCacheEmpErr()
            allFields["PlaWrCacheErr"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug1_stk._PlaWrCacheErr()
            allFields["PlaEnqVCGBlkEmpErr"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug1_stk._PlaEnqVCGBlkEmpErr()
            allFields["PlaEnqVCGBlkSameErr"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug1_stk._PlaEnqVCGBlkSameErr()
            allFields["PlaWrDDRACKFFErr"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug1_stk._PlaWrDDRACKFFErr()
            allFields["PlaEnqBlkCfgErr"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug1_stk._PlaEnqBlkCfgErr()
            allFields["PlaWrDDRVldRdyFFErr"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug1_stk._PlaWrDDRVldRdyFFErr()
            allFields["PlaWrDDRVldFFErr"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug1_stk._PlaWrDDRVldFFErr()
            allFields["PlaMuxPWCoreHOErr"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug1_stk._PlaMuxPWCoreHOErr()
            allFields["PlaMuxPWCoreLOErr"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug1_stk._PlaMuxPWCoreLOErr()
            allFields["PlaMuxPWLOOC48_2Err"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug1_stk._PlaMuxPWLOOC48_2Err()
            allFields["PlaMuxPWLOOC48_1Err"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug1_stk._PlaMuxPWLOOC48_1Err()
            allFields["PlaMuxSrcIn3Err"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug1_stk._PlaMuxSrcIn3Err()
            allFields["PlaMuxSrcIn2Err"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug1_stk._PlaMuxSrcIn2Err()
            allFields["PlaMuxSrcIn1Err"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug1_stk._PlaMuxSrcIn1Err()
            allFields["PlaMuxVCGIn2Err"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug1_stk._PlaMuxVCGIn2Err()
            allFields["PlaMuxVCGIn1Err"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug1_stk._PlaMuxVCGIn1Err()
            return allFields

    class _pla_debug2_stk(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler debug Sticky#2"
    
        def description(self):
            return "This register is used to used to sticky some debug info of PLA"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00021013
            
        def endAddress(self):
            return 0xffffffff

        class _DiscardUPSR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "DiscardUPSR"
            
            def description(self):
                return "discard due to upsr disable"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _SeqBufErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "SeqBufErr"
            
            def description(self):
                return "Sequence buffer error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _eXAUI1DatBufErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "eXAUI1DatBufErr"
            
            def description(self):
                return "eXAUI1 data buffer error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _eXAUI0DatBufErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "eXAUI0DatBufErr"
            
            def description(self):
                return "eXAUI0 data buffer error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _eXAUI1SeqTimeout(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "eXAUI1SeqTimeout"
            
            def description(self):
                return "eXAUI1 Sequence Timeout"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _eXAUI1SeqBad(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "eXAUI1SeqBad"
            
            def description(self):
                return "eXAUI1 Sequence Bad"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _eXAUI1SeqGood(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "eXAUI1SeqGood"
            
            def description(self):
                return "eXAUI1 Sequence Good"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _eXAUI0SeqTimeout(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "eXAUI0SeqTimeout"
            
            def description(self):
                return "eXAUI0 Sequence Timeout"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _eXAUI0SeqBad(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "eXAUI0SeqBad"
            
            def description(self):
                return "eXAUI0 Sequence Bad"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _eXAUI0SeqGood(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "eXAUI0SeqGood"
            
            def description(self):
                return "eXAUI0 Sequence Good"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PWSeqTimeout(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "PWSeqTimeout"
            
            def description(self):
                return "PW Sequence Timeout"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PWSeqBad(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "PWSeqBad"
            
            def description(self):
                return "PW Sequence Bad"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PWSeqGood(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PWSeqGood"
            
            def description(self):
                return "PW Sequence Good"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _eXAUI1DatEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "eXAUI1DatEn"
            
            def description(self):
                return "eXAUI1 buffer data enable"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _eXAUI1DatVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "eXAUI1DatVld"
            
            def description(self):
                return "data valid from eXAUI1"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _eXAUI1PktEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "eXAUI1PktEn"
            
            def description(self):
                return "eXAUI1 buffer packet enable"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _eXAUI1PktVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "eXAUI1PktVld"
            
            def description(self):
                return "packet info valid to eXAUI0"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _eXAUI0DatEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "eXAUI0DatEn"
            
            def description(self):
                return "eXAUI0 buffer data enable"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _eXAUI0DatVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "eXAUI0DatVld"
            
            def description(self):
                return "data valid from eXAUI0"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _eXAUI0PktEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "eXAUI0PktEn"
            
            def description(self):
                return "eXAUI0 buffer packet enable"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _eXAUI0PktVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "eXAUI0PktVld"
            
            def description(self):
                return "packet info valid to eXAUI0"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DiscardUPSR"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug2_stk._DiscardUPSR()
            allFields["SeqBufErr"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug2_stk._SeqBufErr()
            allFields["eXAUI1DatBufErr"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug2_stk._eXAUI1DatBufErr()
            allFields["eXAUI0DatBufErr"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug2_stk._eXAUI0DatBufErr()
            allFields["eXAUI1SeqTimeout"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug2_stk._eXAUI1SeqTimeout()
            allFields["eXAUI1SeqBad"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug2_stk._eXAUI1SeqBad()
            allFields["eXAUI1SeqGood"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug2_stk._eXAUI1SeqGood()
            allFields["eXAUI0SeqTimeout"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug2_stk._eXAUI0SeqTimeout()
            allFields["eXAUI0SeqBad"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug2_stk._eXAUI0SeqBad()
            allFields["eXAUI0SeqGood"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug2_stk._eXAUI0SeqGood()
            allFields["PWSeqTimeout"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug2_stk._PWSeqTimeout()
            allFields["PWSeqBad"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug2_stk._PWSeqBad()
            allFields["PWSeqGood"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug2_stk._PWSeqGood()
            allFields["eXAUI1DatEn"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug2_stk._eXAUI1DatEn()
            allFields["eXAUI1DatVld"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug2_stk._eXAUI1DatVld()
            allFields["eXAUI1PktEn"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug2_stk._eXAUI1PktEn()
            allFields["eXAUI1PktVld"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug2_stk._eXAUI1PktVld()
            allFields["eXAUI0DatEn"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug2_stk._eXAUI0DatEn()
            allFields["eXAUI0DatVld"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug2_stk._eXAUI0DatVld()
            allFields["eXAUI0PktEn"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug2_stk._eXAUI0PktEn()
            allFields["eXAUI0PktVld"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug2_stk._eXAUI0PktVld()
            return allFields

    class _pla_debug_sta(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler debug Status"
    
        def description(self):
            return "This register is used to used to show some debug info of PLA"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00021028
            
        def endAddress(self):
            return 0xffffffff

        class _VCG_free_block_number(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "VCG_free_block_number"
            
            def description(self):
                return "current VCG free block number"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _write_cache_number(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "write_cache_number"
            
            def description(self):
                return "current write cache number"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["VCG_free_block_number"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug_sta._VCG_free_block_number()
            allFields["write_cache_number"] = _AF6CCI0012_RD_PLA_DEBUG._pla_debug_sta._write_cache_number()
            return allFields

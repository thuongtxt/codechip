import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0012_RD_INTPMC(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["pmccpuhold0_pen"] = _AF6CCI0012_RD_INTPMC._pmccpuhold0_pen()
        allRegisters["pmc_glbcnt_pen"] = _AF6CCI0012_RD_INTPMC._pmc_glbcnt_pen()
        allRegisters["tdmhocntidlk_pen"] = _AF6CCI0012_RD_INTPMC._tdmhocntidlk_pen()
        allRegisters["tdmlocntidlk_pen"] = _AF6CCI0012_RD_INTPMC._tdmlocntidlk_pen()
        allRegisters["bndcntidlk_pen"] = _AF6CCI0012_RD_INTPMC._bndcntidlk_pen()
        allRegisters["ethcntidlk_pen"] = _AF6CCI0012_RD_INTPMC._ethcntidlk_pen()
        allRegisters["encpktcnt_pen"] = _AF6CCI0012_RD_INTPMC._encpktcnt_pen()
        allRegisters["encbytecnt_pen"] = _AF6CCI0012_RD_INTPMC._encbytecnt_pen()
        allRegisters["encpkttypcnt_pen"] = _AF6CCI0012_RD_INTPMC._encpkttypcnt_pen()
        allRegisters["encidlebytecnt_pen"] = _AF6CCI0012_RD_INTPMC._encidlebytecnt_pen()
        allRegisters["decpktcnt_pen"] = _AF6CCI0012_RD_INTPMC._decpktcnt_pen()
        allRegisters["decbytecnt_pen"] = _AF6CCI0012_RD_INTPMC._decbytecnt_pen()
        allRegisters["decpkttypcnt_pen"] = _AF6CCI0012_RD_INTPMC._decpkttypcnt_pen()
        allRegisters["decidlebytecnt_pen"] = _AF6CCI0012_RD_INTPMC._decidlebytecnt_pen()
        allRegisters["hoencpktcnt_pen"] = _AF6CCI0012_RD_INTPMC._hoencpktcnt_pen()
        allRegisters["hoencbytecnt_pen"] = _AF6CCI0012_RD_INTPMC._hoencbytecnt_pen()
        allRegisters["hoencpkttypcnt_pen"] = _AF6CCI0012_RD_INTPMC._hoencpkttypcnt_pen()
        allRegisters["hoencidlebytecnt_pen"] = _AF6CCI0012_RD_INTPMC._hoencidlebytecnt_pen()
        allRegisters["loencpktcnt_pen"] = _AF6CCI0012_RD_INTPMC._loencpktcnt_pen()
        allRegisters["loencbytecnt_pen"] = _AF6CCI0012_RD_INTPMC._loencbytecnt_pen()
        allRegisters["loencpkttypcnt_pen"] = _AF6CCI0012_RD_INTPMC._loencpkttypcnt_pen()
        allRegisters["loencidlebytecnt_pen"] = _AF6CCI0012_RD_INTPMC._loencidlebytecnt_pen()
        allRegisters["hodecpktcnt_pen"] = _AF6CCI0012_RD_INTPMC._hodecpktcnt_pen()
        allRegisters["hodecbytecnt_pen"] = _AF6CCI0012_RD_INTPMC._hodecbytecnt_pen()
        allRegisters["hodecpkttypcnt_pen"] = _AF6CCI0012_RD_INTPMC._hodecpkttypcnt_pen()
        allRegisters["hodecidlebytecnt_pen"] = _AF6CCI0012_RD_INTPMC._hodecidlebytecnt_pen()
        allRegisters["lodecpktcnt_pen"] = _AF6CCI0012_RD_INTPMC._lodecpktcnt_pen()
        allRegisters["lodecbytecnt_pen"] = _AF6CCI0012_RD_INTPMC._lodecbytecnt_pen()
        allRegisters["lodecpkttypcnt_pen"] = _AF6CCI0012_RD_INTPMC._lodecpkttypcnt_pen()
        allRegisters["lodecidlebytecnt_pen"] = _AF6CCI0012_RD_INTPMC._lodecidlebytecnt_pen()
        allRegisters["txbndpktcnt_pen"] = _AF6CCI0012_RD_INTPMC._txbndpktcnt_pen()
        allRegisters["txbndbytecnt_pen"] = _AF6CCI0012_RD_INTPMC._txbndbytecnt_pen()
        allRegisters["rxbndpktcnt_pen"] = _AF6CCI0012_RD_INTPMC._rxbndpktcnt_pen()
        allRegisters["rxbndbytecnt_pen"] = _AF6CCI0012_RD_INTPMC._rxbndbytecnt_pen()
        allRegisters["igethpkttypcnt_pen"] = _AF6CCI0012_RD_INTPMC._igethpkttypcnt_pen()
        allRegisters["egethpkttypcnt_pen"] = _AF6CCI0012_RD_INTPMC._egethpkttypcnt_pen()
        allRegisters["ethfulltypcnt_pen"] = _AF6CCI0012_RD_INTPMC._ethfulltypcnt_pen()
        return allRegisters

    class _pmccpuhold0_pen(AtRegister.AtRegister):
        def name(self):
            return "CPU Access Data Holding 0"
    
        def description(self):
            return "Used to CPU access with data is greater than 32-bit wide."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0xffffffff

        class _Dat_Hold0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Dat_Hold0"
            
            def description(self):
                return ""
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Dat_Hold0"] = _AF6CCI0012_RD_INTPMC._pmccpuhold0_pen._Dat_Hold0()
            return allFields

    class _pmc_glbcnt_pen(AtRegister.AtRegister):
        def name(self):
            return "PMC Global Counters"
    
        def description(self):
            return "Support the following Counters ID: - glbcntid   = 4'd0 : Input CLA Counter - glbcntid   = 4'd1 : Output CLA Counter - glbcntid   = 4'd2 : Input Framming Counter - glbcntid   = 4'd3 : Output Framming Counter - glbcntid   = 4'd4 : Input PDA Counter - glbcntid   = 4'd5 : Output PDA to Lo-ENC#0 Counter - glbcntid   = 4'd6 : Output PDA to Lo-ENC#1 Counter - glbcntid   = 4'd7 : Output PDA to Lo-ENC#2 Counter - glbcntid   = 4'd8 : Output PDA to Lo-ENC#3 Counter - glbcntid   = 4'd9 : Output PWE Counter - glbcntid   = 4'd10: Output MUX to OOBFC Counter - glbcntid   = 4'd11: Output MUX to PWE Counter - glbcntid   = 4'd12: Output MUX to XFI Counter - glbcntid(13-15)   : Reserved Each Counter ID Support following Counter Types: - glbcnttyp = 3'd0 : Packet Valid Counter - glbcnttyp = 3'd1 : Packet Sop Counter - glbcnttyp = 3'd2 : Packet Eop Counter - glbcnttyp = 3'd3 : Packet Err Counter - glbcnttyp = 3'd4 : Packet Nob Counter - glbcnttyp(5-7)  : Reserved"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00100 + $glbcntid*16 + $r2c*8 + $glbcnttyp"
            
        def startAddress(self):
            return 0x00000100
            
        def endAddress(self):
            return 0x000001ff

        class _GLB_CNT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "GLB_CNT"
            
            def description(self):
                return "Global Counter"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["GLB_CNT"] = _AF6CCI0012_RD_INTPMC._pmc_glbcnt_pen._GLB_CNT()
            return allFields

    class _tdmhocntidlk_pen(AtRegister.AtRegister):
        def name(self):
            return "Link HO Counter ID Lookup Control"
    
        def description(self):
            return "This register is used to config to select Pool ID for Link counters"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x10000 + $side*4096 + $slcid*64 + $lid"
            
        def startAddress(self):
            return 0x00010000
            
        def endAddress(self):
            return 0x0001107f

        class _Link_Ho_PCNT_EN(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Link_Ho_PCNT_EN"
            
            def description(self):
                return "Link Pool Count Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Link_Ho_PCNT_ID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Link_Ho_PCNT_ID"
            
            def description(self):
                return "Link Pool ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Link_Ho_PCNT_EN"] = _AF6CCI0012_RD_INTPMC._tdmhocntidlk_pen._Link_Ho_PCNT_EN()
            allFields["Link_Ho_PCNT_ID"] = _AF6CCI0012_RD_INTPMC._tdmhocntidlk_pen._Link_Ho_PCNT_ID()
            return allFields

    class _tdmlocntidlk_pen(AtRegister.AtRegister):
        def name(self):
            return "Link LO Counter ID Lookup Control"
    
        def description(self):
            return "This register is used to config to select Pool ID for Link counters"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x12000 + $side*4096 + $slcid*1024 + $lid"
            
        def startAddress(self):
            return 0x00012000
            
        def endAddress(self):
            return 0x00013fff

        class _Link_Lo_PCNT_EN(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Link_Lo_PCNT_EN"
            
            def description(self):
                return "Link Pool Count Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Link_Lo_PCNT_ID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Link_Lo_PCNT_ID"
            
            def description(self):
                return "Link Pool ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Link_Lo_PCNT_EN"] = _AF6CCI0012_RD_INTPMC._tdmlocntidlk_pen._Link_Lo_PCNT_EN()
            allFields["Link_Lo_PCNT_ID"] = _AF6CCI0012_RD_INTPMC._tdmlocntidlk_pen._Link_Lo_PCNT_ID()
            return allFields

    class _bndcntidlk_pen(AtRegister.AtRegister):
        def name(self):
            return "Bundle Counter ID Lookup Control"
    
        def description(self):
            return "This register is used to config to select Pool ID for Bundle Counter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x16000 + $side*1024 + $bundid"
            
        def startAddress(self):
            return 0x00016000
            
        def endAddress(self):
            return 0x000165ff

        class _BND_PCNT_EN(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "BND_PCNT_EN"
            
            def description(self):
                return "Bundle Pool Count Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _BND_PCNT_ID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "BND_PCNT_ID"
            
            def description(self):
                return "Bundle Pool ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["BND_PCNT_EN"] = _AF6CCI0012_RD_INTPMC._bndcntidlk_pen._BND_PCNT_EN()
            allFields["BND_PCNT_ID"] = _AF6CCI0012_RD_INTPMC._bndcntidlk_pen._BND_PCNT_ID()
            return allFields

    class _ethcntidlk_pen(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Counter ID Lookup Control"
    
        def description(self):
            return "This register is used to config to select Pool ID for Tx Ethernet Counter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x14000 + $side*4096 + $efid"
            
        def startAddress(self):
            return 0x00014000
            
        def endAddress(self):
            return 0x00015fff

        class _ETH_PCNT_EN(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "ETH_PCNT_EN"
            
            def description(self):
                return "Ethernet Pool Count Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _ETH_PCNT_ID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ETH_PCNT_ID"
            
            def description(self):
                return "Ethernet Pool ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ETH_PCNT_EN"] = _AF6CCI0012_RD_INTPMC._ethcntidlk_pen._ETH_PCNT_EN()
            allFields["ETH_PCNT_ID"] = _AF6CCI0012_RD_INTPMC._ethcntidlk_pen._ETH_PCNT_ID()
            return allFields

    class _encpktcnt_pen(AtRegister.AtRegister):
        def name(self):
            return "TDM Pool Encap Packet Counters"
    
        def description(self):
            return "Support the following Enc Packet counter: - cnt_type = 0: ENC Packet Good - cnt_type = 1: ENC Packet Error"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x01000+ $r2c*64+$cnt_type*32+$cnt_id"
            
        def startAddress(self):
            return 0x00001000
            
        def endAddress(self):
            return 0x0000107f

        class _ENC_PKT_CNT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ENC_PKT_CNT"
            
            def description(self):
                return "Encap Packet Count"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ENC_PKT_CNT"] = _AF6CCI0012_RD_INTPMC._encpktcnt_pen._ENC_PKT_CNT()
            return allFields

    class _encbytecnt_pen(AtRegister.AtRegister):
        def name(self):
            return "TDM Pool Encap Byte Counters"
    
        def description(self):
            return "Support the following Enc Packet counter: - cnt_type = 0: ENC Byte Good - cnt_type = 1: ENC Byte Error"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x01100+ $r2c*64+$cnt_type*32+$cnt_id"
            
        def startAddress(self):
            return 0x00001100
            
        def endAddress(self):
            return 0x0000113f

        class _ENC_Byte_CNT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ENC_Byte_CNT"
            
            def description(self):
                return "Encap Byte Count"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ENC_Byte_CNT"] = _AF6CCI0012_RD_INTPMC._encbytecnt_pen._ENC_Byte_CNT()
            return allFields

    class _encpkttypcnt_pen(AtRegister.AtRegister):
        def name(self):
            return "TDM Pool Encap Packet Type Counters"
    
        def description(self):
            return "Support the following Encap Packet Type Counters: - cnt_type = 3'd0: Enc Packet fragments - cnt_type = 3'd1: Enc Packet Plain - cnt_type = 3'd2: Enc Packet OAM - cnt_type = 3'd3: Enc Packet Abort - cnt_type(5-7)  : Reserved"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x01200 +$cnt_type*256+$r2c*32+$cnt_id"
            
        def startAddress(self):
            return 0x00001200
            
        def endAddress(self):
            return 0x0000163f

        class _ENC_PktTyp_CNT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ENC_PktTyp_CNT"
            
            def description(self):
                return "Encap Packet Type Count"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ENC_PktTyp_CNT"] = _AF6CCI0012_RD_INTPMC._encpkttypcnt_pen._ENC_PktTyp_CNT()
            return allFields

    class _encidlebytecnt_pen(AtRegister.AtRegister):
        def name(self):
            return "TDM Pool Encap Idlie Byte Counter"
    
        def description(self):
            return "Encap Idle Byte Counter:"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x01A00 + $r2c*32+$cnt_id"
            
        def startAddress(self):
            return 0x00001a00
            
        def endAddress(self):
            return 0x00001a3f

        class _ENC_IDLE_CNT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ENC_IDLE_CNT"
            
            def description(self):
                return "Encap Idle Byte Count"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ENC_IDLE_CNT"] = _AF6CCI0012_RD_INTPMC._encidlebytecnt_pen._ENC_IDLE_CNT()
            return allFields

    class _decpktcnt_pen(AtRegister.AtRegister):
        def name(self):
            return "TDM Decap Packet Counters"
    
        def description(self):
            return "Support the following Enc Packet counter: - cnt_type = 0: DEC Packet Good - cnt_type = 1: DEC Packet Error"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x02000+ $r2c*64+$cnt_type*32+$cnt_id"
            
        def startAddress(self):
            return 0x00002000
            
        def endAddress(self):
            return 0x0000207f

        class _DEC_PKT_CNT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DEC_PKT_CNT"
            
            def description(self):
                return "Decap Packet Count"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DEC_PKT_CNT"] = _AF6CCI0012_RD_INTPMC._decpktcnt_pen._DEC_PKT_CNT()
            return allFields

    class _decbytecnt_pen(AtRegister.AtRegister):
        def name(self):
            return "TDM Decap Byte Counters"
    
        def description(self):
            return "Support the following Decap Byte counters: - cnt_type = 0: DEC Byte Good - cnt_type = 1: DEC Byte Error"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x02100+ $r2c*64+$cnt_type*32+$cnt_id"
            
        def startAddress(self):
            return 0x00002100
            
        def endAddress(self):
            return 0x0000217f

        class _DEC_Byte_CNT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DEC_Byte_CNT"
            
            def description(self):
                return "Decap Byte Count"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DEC_Byte_CNT"] = _AF6CCI0012_RD_INTPMC._decbytecnt_pen._DEC_Byte_CNT()
            return allFields

    class _decpkttypcnt_pen(AtRegister.AtRegister):
        def name(self):
            return "TDM Decap Packet Type Counters"
    
        def description(self):
            return "Support the following Decap Packet Type counters: - cnt_type = 3'd0: Dec Packet fragments - cnt_type = 3'd1: Dec Packet Plain - cnt_type = 3'd2: Dec Packet OAM - cnt_type = 3'd3: Dec Packet Abort - cnt_type = 3'd4: Dec FCS Error - cnt_type(5-7)  : Reserved"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x02200 + $cnt_type*256+$r2c*32+$cnt_id"
            
        def startAddress(self):
            return 0x00002200
            
        def endAddress(self):
            return 0x0000273f

        class _DEC_PktTyp_CNT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DEC_PktTyp_CNT"
            
            def description(self):
                return "Decap Packet Type Counter"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DEC_PktTyp_CNT"] = _AF6CCI0012_RD_INTPMC._decpkttypcnt_pen._DEC_PktTyp_CNT()
            return allFields

    class _decidlebytecnt_pen(AtRegister.AtRegister):
        def name(self):
            return "TDM Decap Idlie Byte Counter"
    
        def description(self):
            return "Decap Idle Byte Counter:"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x02A00 + $r2c*32+$cnt_id"
            
        def startAddress(self):
            return 0x00002a00
            
        def endAddress(self):
            return 0x00002a3f

        class _DEC_IDLE_CNT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DEC_IDLE_CNT"
            
            def description(self):
                return "Decap Idle Byte Count"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DEC_IDLE_CNT"] = _AF6CCI0012_RD_INTPMC._decidlebytecnt_pen._DEC_IDLE_CNT()
            return allFields

    class _hoencpktcnt_pen(AtRegister.AtRegister):
        def name(self):
            return "TDM HO Encap Packet Counters"
    
        def description(self):
            return "Support the following Enc Packet counter: - cnt_type = 0: HO ENC Packet Good - cnt_type = 1: HO ENC Packet Error"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x09000+ $r2c*256+$cnt_type*128+$hoslc_id*64+$hoenc_id"
            
        def startAddress(self):
            return 0x00009000
            
        def endAddress(self):
            return 0x000091ff

        class _HO_ENC_PKT_CNT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "HO_ENC_PKT_CNT"
            
            def description(self):
                return "Encap Packet Count"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["HO_ENC_PKT_CNT"] = _AF6CCI0012_RD_INTPMC._hoencpktcnt_pen._HO_ENC_PKT_CNT()
            return allFields

    class _hoencbytecnt_pen(AtRegister.AtRegister):
        def name(self):
            return "TDM HO Encap Byte Counters"
    
        def description(self):
            return "Support the following Enc Packet counter: - cnt_type = 0: HO ENC Byte Good - cnt_type = 1: HO ENC Byte Error"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x09200+ $r2c*256+$cnt_type*128+$hoslc_id*64+$hoenc_id"
            
        def startAddress(self):
            return 0x00009200
            
        def endAddress(self):
            return 0x000093ff

        class _HO_ENC_Byte_CNT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "HO_ENC_Byte_CNT"
            
            def description(self):
                return "Encap Byte Count"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["HO_ENC_Byte_CNT"] = _AF6CCI0012_RD_INTPMC._hoencbytecnt_pen._HO_ENC_Byte_CNT()
            return allFields

    class _hoencpkttypcnt_pen(AtRegister.AtRegister):
        def name(self):
            return "TDM HO Encap Packet Type Counters"
    
        def description(self):
            return "Support the following Encap Packet Type Counters: - cnt_type = 3'd0: ENC Packet fragments - cnt_type = 3'd1: ENC Packet Plain - cnt_type = 3'd2: ENC Packet OAM - cnt_type = 3'd3: ENC Packet Abort"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x09800 +$cnt_type*256+$r2c*128+$hoslc_id*64+$hoenc_id"
            
        def startAddress(self):
            return 0x00009800
            
        def endAddress(self):
            return 0x00009bff

        class _HO_ENC_PktTyp_CNT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "HO_ENC_PktTyp_CNT"
            
            def description(self):
                return "Encap Packet Type Count"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["HO_ENC_PktTyp_CNT"] = _AF6CCI0012_RD_INTPMC._hoencpkttypcnt_pen._HO_ENC_PktTyp_CNT()
            return allFields

    class _hoencidlebytecnt_pen(AtRegister.AtRegister):
        def name(self):
            return "TDM Encap Idlie Byte Counter"
    
        def description(self):
            return "Encap Idle Byte Counter:"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x09C00 + $r2c*128+$hoslc_id*64+$hoenc_id"
            
        def startAddress(self):
            return 0x00009c00
            
        def endAddress(self):
            return 0x00009cff

        class _HO_ENC_IDLE_CNT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "HO_ENC_IDLE_CNT"
            
            def description(self):
                return "Encap Idle Byte Count"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["HO_ENC_IDLE_CNT"] = _AF6CCI0012_RD_INTPMC._hoencidlebytecnt_pen._HO_ENC_IDLE_CNT()
            return allFields

    class _loencpktcnt_pen(AtRegister.AtRegister):
        def name(self):
            return "TDM LO Encap Packet Counters"
    
        def description(self):
            return "Support the following Enc Packet counter: - cnt_type = 0: ENC Packet Good - cnt_type = 1: ENC Packet Error"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x40000+ $r2c*8192+$cnt_type*4096+$loslc_id*1024+$loenc_id"
            
        def startAddress(self):
            return 0x00040000
            
        def endAddress(self):
            return 0x00043fff

        class _LO_ENC_PKT_CNT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LO_ENC_PKT_CNT"
            
            def description(self):
                return "Encap Packet Count"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["LO_ENC_PKT_CNT"] = _AF6CCI0012_RD_INTPMC._loencpktcnt_pen._LO_ENC_PKT_CNT()
            return allFields

    class _loencbytecnt_pen(AtRegister.AtRegister):
        def name(self):
            return "TDM LO Encap Byte Counters"
    
        def description(self):
            return "Support the following Enc Packet counter: - cnt_type = 0: ENC Byte Good - cnt_type = 1: ENC Byte Error"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x44000+ $r2c*8192+$cnt_type*4096+$loslc_id*1024+$loenc_id"
            
        def startAddress(self):
            return 0x00044000
            
        def endAddress(self):
            return 0x00047fff

        class _LO_ENC_Byte_CNT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LO_ENC_Byte_CNT"
            
            def description(self):
                return "Encap Byte Count"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["LO_ENC_Byte_CNT"] = _AF6CCI0012_RD_INTPMC._loencbytecnt_pen._LO_ENC_Byte_CNT()
            return allFields

    class _loencpkttypcnt_pen(AtRegister.AtRegister):
        def name(self):
            return "TDM LO Encap Packet Type Counters"
    
        def description(self):
            return "Support the following Encap Packet Type Counters: - cnt_type = 3'd0: ENC Packet fragments - cnt_type = 3'd1: ENC Packet Plain - cnt_type = 3'd2: ENC Packet OAM - cnt_type = 3'd3: ENC Packet Abort"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x50000 +$cnt_type*8192+$r2c*4096+$loslc_id*1024+$loenc_id"
            
        def startAddress(self):
            return 0x00050000
            
        def endAddress(self):
            return 0x00057fff

        class _LO_ENC_PktTyp_CNT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LO_ENC_PktTyp_CNT"
            
            def description(self):
                return "Encap Packet Type Count"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["LO_ENC_PktTyp_CNT"] = _AF6CCI0012_RD_INTPMC._loencpkttypcnt_pen._LO_ENC_PktTyp_CNT()
            return allFields

    class _loencidlebytecnt_pen(AtRegister.AtRegister):
        def name(self):
            return "TDM LO Encap Idlie Byte Counter"
    
        def description(self):
            return "Encap Idle Byte Counter:"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x58000 + $r2c*4096+$loslc_id*1024+$loenc_id"
            
        def startAddress(self):
            return 0x00058000
            
        def endAddress(self):
            return 0x00059fff

        class _LO_ENC_IDLE_CNT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LO_ENC_IDLE_CNT"
            
            def description(self):
                return "Encap Idle Byte Count"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["LO_ENC_IDLE_CNT"] = _AF6CCI0012_RD_INTPMC._loencidlebytecnt_pen._LO_ENC_IDLE_CNT()
            return allFields

    class _hodecpktcnt_pen(AtRegister.AtRegister):
        def name(self):
            return "TDM HO Decap Packet Counters"
    
        def description(self):
            return "Support the following Enc Packet counter: - cnt_type = 0: HO DEC Packet Good - cnt_type = 1: HO DEC Packet Error"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0A000+ $r2c*256+$cnt_type*128+$hoslc_id*64+$hodec_id"
            
        def startAddress(self):
            return 0x0000a000
            
        def endAddress(self):
            return 0x0000a1ff

        class _HO_DEC_PKT_CNT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "HO_DEC_PKT_CNT"
            
            def description(self):
                return "Decap Packet Count"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["HO_DEC_PKT_CNT"] = _AF6CCI0012_RD_INTPMC._hodecpktcnt_pen._HO_DEC_PKT_CNT()
            return allFields

    class _hodecbytecnt_pen(AtRegister.AtRegister):
        def name(self):
            return "TDM HO Decap Byte Counters"
    
        def description(self):
            return "Support the following Enc Packet counter: - cnt_type = 0: HO DEC Byte Good - cnt_type = 1: HO DEC Byte Error"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0A200+ $r2c*256+$cnt_type*128+$hoslc_id*64+$hodec_id"
            
        def startAddress(self):
            return 0x0000a200
            
        def endAddress(self):
            return 0x0000a3ff

        class _HO_DEC_Byte_CNT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "HO_DEC_Byte_CNT"
            
            def description(self):
                return "Decap Byte Count"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["HO_DEC_Byte_CNT"] = _AF6CCI0012_RD_INTPMC._hodecbytecnt_pen._HO_DEC_Byte_CNT()
            return allFields

    class _hodecpkttypcnt_pen(AtRegister.AtRegister):
        def name(self):
            return "TDM HO Decap Packet Type Counters"
    
        def description(self):
            return "Support the following Decap Packet Type Counters: - cnt_type = 3'd0: DEC Packet fragments - cnt_type = 3'd1: DEC Packet Plain - cnt_type = 3'd2: DEC Packet OAM - cnt_type = 3'd3: DEC Packet Abort - cnt_type = 3'd4: DEC FCS Error - cnt_type = 3'd5: DEC Discarded Packet - cnt_type = 3'd6: DEC MRU Packet"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0A800 +$cnt_type*256+$r2c*128+$hoslc_id*64+$hodec_id"
            
        def startAddress(self):
            return 0x0000a800
            
        def endAddress(self):
            return 0x0000aeff

        class _HO_DEC_PktTyp_CNT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "HO_DEC_PktTyp_CNT"
            
            def description(self):
                return "Decap Packet Type Count"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["HO_DEC_PktTyp_CNT"] = _AF6CCI0012_RD_INTPMC._hodecpkttypcnt_pen._HO_DEC_PktTyp_CNT()
            return allFields

    class _hodecidlebytecnt_pen(AtRegister.AtRegister):
        def name(self):
            return "TDM Decap Idlie Byte Counter"
    
        def description(self):
            return "Decap Idle Byte Counter:"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0AF00 + $r2c*128+$hoslc_id*64+$hodec_id"
            
        def startAddress(self):
            return 0x0000af00
            
        def endAddress(self):
            return 0x0000afff

        class _HO_DEC_IDLE_CNT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "HO_DEC_IDLE_CNT"
            
            def description(self):
                return "Decap Idle Byte Count"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["HO_DEC_IDLE_CNT"] = _AF6CCI0012_RD_INTPMC._hodecidlebytecnt_pen._HO_DEC_IDLE_CNT()
            return allFields

    class _lodecpktcnt_pen(AtRegister.AtRegister):
        def name(self):
            return "TDM LO Decap Packet Counters"
    
        def description(self):
            return "Support the following Enc Packet counter: - cnt_type = 0: DEC Packet Good - cnt_type = 1: DEC Packet Error"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x60000+ $r2c*8192+$cnt_type*4096+$loslc_id*1024+$lodec_id"
            
        def startAddress(self):
            return 0x00060000
            
        def endAddress(self):
            return 0x00063fff

        class _LO_DEC_PKT_CNT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LO_DEC_PKT_CNT"
            
            def description(self):
                return "Decap Packet Count"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["LO_DEC_PKT_CNT"] = _AF6CCI0012_RD_INTPMC._lodecpktcnt_pen._LO_DEC_PKT_CNT()
            return allFields

    class _lodecbytecnt_pen(AtRegister.AtRegister):
        def name(self):
            return "TDM LO Decap Byte Counters"
    
        def description(self):
            return "Support the following Decap Byte counters: - cnt_type = 0: DEC Byte Good - cnt_type = 1: DEC Byte Error"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x64000+ $r2c*8192+$cnt_type*4096+$loslc_id*1024+$lodec_id"
            
        def startAddress(self):
            return 0x00064000
            
        def endAddress(self):
            return 0x00067fff

        class _LO_DEC_Byte_CNT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LO_DEC_Byte_CNT"
            
            def description(self):
                return "Decap Byte Count"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["LO_DEC_Byte_CNT"] = _AF6CCI0012_RD_INTPMC._lodecbytecnt_pen._LO_DEC_Byte_CNT()
            return allFields

    class _lodecpkttypcnt_pen(AtRegister.AtRegister):
        def name(self):
            return "TDM LO Decap Packet Type Counters"
    
        def description(self):
            return "Support the following Decap Packet Type counters: - cnt_type = 3'd0: DEC Packet fragments - cnt_type = 3'd1: DEC Packet Plain - cnt_type = 3'd2: DEC Packet OAM - cnt_type = 3'd3: DEC Packet Abort - cnt_type = 3'd4: DEC FCS Error - cnt_type = 3'd5: DEC Discarded Packet - cnt_type = 3'd6: DEC MRU Packet"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x70000 + $cnt_type*8192+$r2c*4096+$loslc_id*1024+$lodec_id"
            
        def startAddress(self):
            return 0x00070000
            
        def endAddress(self):
            return 0x0007dfff

        class _LO_DEC_PktTyp_CNT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LO_DEC_PktTyp_CNT"
            
            def description(self):
                return "Decap Packet Type Counter"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["LO_DEC_PktTyp_CNT"] = _AF6CCI0012_RD_INTPMC._lodecpkttypcnt_pen._LO_DEC_PktTyp_CNT()
            return allFields

    class _lodecidlebytecnt_pen(AtRegister.AtRegister):
        def name(self):
            return "TDM Decap Idlie Byte Counter"
    
        def description(self):
            return "Decap Idle Byte Counter:"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x7E000 + $r2c*4096+$loslc_id*1024+$lodec_id"
            
        def startAddress(self):
            return 0x0007e000
            
        def endAddress(self):
            return 0x0007ffff

        class _LO_DEC_IDLE_CNT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LO_DEC_IDLE_CNT"
            
            def description(self):
                return "Decap Idle Byte Count"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["LO_DEC_IDLE_CNT"] = _AF6CCI0012_RD_INTPMC._lodecidlebytecnt_pen._LO_DEC_IDLE_CNT()
            return allFields

    class _txbndpktcnt_pen(AtRegister.AtRegister):
        def name(self):
            return "Tx Bundle Packet Counter"
    
        def description(self):
            return "Support the following Tx Bundle Packet counter: - cnt_type = 0: Tx Bundle Packet Good - cnt_type = 1: Tx Bundle Packet Discard"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x03000+ $r2c*64+$cnt_type*32+$cnt_id"
            
        def startAddress(self):
            return 0x00003000
            
        def endAddress(self):
            return 0x0000307f

        class _TxBND_PKT_CNT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxBND_PKT_CNT"
            
            def description(self):
                return "Tx Bundle Packet Count"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxBND_PKT_CNT"] = _AF6CCI0012_RD_INTPMC._txbndpktcnt_pen._TxBND_PKT_CNT()
            return allFields

    class _txbndbytecnt_pen(AtRegister.AtRegister):
        def name(self):
            return "Tx Bundle Byte Counters"
    
        def description(self):
            return "Support the following Tx Bundle Byte counter: - cnt_type = 0: Tx Bundle Byte Good - cnt_type = 1: Tx Bundle Byte Discard"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x03100+ $r2c*64+$cnt_type*32+$cnt_id"
            
        def startAddress(self):
            return 0x00003100
            
        def endAddress(self):
            return 0x0000317f

        class _TxBND_Byte_CNT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxBND_Byte_CNT"
            
            def description(self):
                return "Tx Bundle Byte Count"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxBND_Byte_CNT"] = _AF6CCI0012_RD_INTPMC._txbndbytecnt_pen._TxBND_Byte_CNT()
            return allFields

    class _rxbndpktcnt_pen(AtRegister.AtRegister):
        def name(self):
            return "Rx Bundle Packet Counter"
    
        def description(self):
            return "Support the following Rx Bundle Packet counter: - cnt_type = 0: Rx Bundle Packet Good - cnt_type = 1: Rx Bundle Packet Discard"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x04000+ $r2c*64+$cnt_type*32+$cnt_id"
            
        def startAddress(self):
            return 0x00004000
            
        def endAddress(self):
            return 0x0000407f

        class _RxBND_PKT_CNT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxBND_PKT_CNT"
            
            def description(self):
                return "Rx Bundle Packet Count"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxBND_PKT_CNT"] = _AF6CCI0012_RD_INTPMC._rxbndpktcnt_pen._RxBND_PKT_CNT()
            return allFields

    class _rxbndbytecnt_pen(AtRegister.AtRegister):
        def name(self):
            return "Rx Bundle Byte Counter"
    
        def description(self):
            return "Support the following Rx Bundle Byte counter: - cnt_type = 0: Rx Bundle Byte Good - cnt_type = 1: Rx Bundle Byte Discard"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x04100+ $r2c*64+$cnt_type*32+$cnt_id"
            
        def startAddress(self):
            return 0x00004100
            
        def endAddress(self):
            return 0x0000417f

        class _RxBND_Byte_CNT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxBND_Byte_CNT"
            
            def description(self):
                return "Rx Bundle Byte Count"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxBND_Byte_CNT"] = _AF6CCI0012_RD_INTPMC._rxbndbytecnt_pen._RxBND_Byte_CNT()
            return allFields

    class _igethpkttypcnt_pen(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Ingress Packet Type Counters"
    
        def description(self):
            return "Support the following Ingress type counters: With ethmod = {0} - cnt_type = 4'd0 : PWE Transmit Packet Counter - cnt_type = 4'd1 : PWE Transmit Byte Counter - cnt_type = 4'd2 : PWE Transmit Packet Fragment - cnt_type = 4'd3 : PWE Transmit Lbit Packet - cnt_type = 4'd4 : PWE Transmit Rbit Packet - cnt_type = 4'd5 : PWE Transmit Mbit Packet - cnt_type = 4'd6 : PWE Transmit Pbit Packet - cnt_type = 4'd7-4'd15: Reserved With ethmod = {1} - cnt_type = 4'd0 : Ethernet Ingress Packet Counter - cnt_type = 4'd1 : Ethernet Ingress Discarded Packet Counter - cnt_type = 4'd2 : Ethernet Ingress Byte Counter - cnt_type = 4'd3 : Ethernet Ingress Discarded Byte Counter - cnt_type = 4'd4 : Ethernet Ingress BECN Packet Counter - cnt_type = 4'd5 : Ethernet Ingress FECN Packet Counter - cnt_type = 4'd6 : Ethernet Ingress DE Packet Counter - cnt_type = 4'd7 : Ethernet Ingress MRU Packet Counter - cnt_type = 4'd8 : Ethernet Ingress Fragment Counter - cnt_type = 4'd9-4'd15: Reserved"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x08000+$ethmod*1024+$r2c*512+$cnt_type*32+$cnt_id"
            
        def startAddress(self):
            return 0x00008000
            
        def endAddress(self):
            return 0x00008fff

        class _IGETH_PktTyp_CNT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IGETH_PktTyp_CNT"
            
            def description(self):
                return "Ingress Packet Type Counter"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IGETH_PktTyp_CNT"] = _AF6CCI0012_RD_INTPMC._igethpkttypcnt_pen._IGETH_PktTyp_CNT()
            return allFields

    class _egethpkttypcnt_pen(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Egress Packet Type Counters"
    
        def description(self):
            return "Support the following Egress Ethernet Type counters: With ethmod = {0} - cnt_type = 4'd0 : PWE Receive Packet Counter - cnt_type = 4'd1 : PWE Receive Byte Counter - cnt_type = 4'd2 : PWE Receive Lofs - cnt_type = 4'd3 : PWE Receive Lbit Packet - cnt_type = 4'd4 : PWE Receive Nbit Packet - cnt_type = 4'd5 : PWE Receive Pbit Packet - cnt_type = 4'd6 : PWE Receive Rbit Packet - cnt_type = 4'd7 : PWE Receive Late Packet - cnt_type = 4'd8 : PWE Receive Early Packet - cnt_type = 4'd9 : PWE Receive Lost Packet - cnt_type = 4'd10: PWE Receive Overrun - cnt_type = 4'd11: PWE Receive Underrun - cnt_type = 4'd12: PWE Receive Malform Packet - cnt_type = 4'd13: PWE Receive Stray Packet - cnt_type = 4'd14: Reserved - cnt_type = 4'd15: Reserved With ethmod = {1} - cnt_type = 4'd0 : Ethernet Egress Packet Counter - cnt_type = 4'd1 : Ethernet Egress Discarded Packet Counter - cnt_type = 4'd2 : Ethernet Egress Byte Counter - cnt_type = 4'd3 : Ethernet Egress Discarded Byte Counter - cnt_type = 4'd4 : Ethernet Egress BECN Packet Counter - cnt_type = 4'd5 : Ethernet Egress FECN Packet Counter - cnt_type = 4'd6 : Ethernet Egress DE Packet Counter - cnt_type = 4'd7 : Ethernet Egress MTU Packet Counter - cnt_type = 4'd8 : Ethernet Egress Fragment Counter - cnt_type = 4'd9-4'd15: Reserved"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x06000+$ethmod*1024+$cnt_type*64+$r2c*32+$cnt_id"
            
        def startAddress(self):
            return 0x00006000
            
        def endAddress(self):
            return 0x000067ff

        class _EGETH_CNT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "EGETH_CNT"
            
            def description(self):
                return "Egress Ethernet Counters"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["EGETH_CNT"] = _AF6CCI0012_RD_INTPMC._egethpkttypcnt_pen._EGETH_CNT()
            return allFields

    class _ethfulltypcnt_pen(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Full Counters"
    
        def description(self):
            return "Full 4096 Ethernet Flow Counters,support the following: - cnt_type = 4'd0 : Ethernet Egress Packet Counter - cnt_type = 4'd1 : Ethernet Egress Byte Counter - cnt_type = 4'd2 : Ethernet Egress Fragment Counter - cnt_type = 4'd3 : Ethernet Egress Discarded Packet Counter - cnt_type = 4'd4 : Ethernet Egress Discarded Byte Counter - cnt_type = 4'd5 : Ethernet Egress BECN Packet Counter - cnt_type = 4'd6 : Ethernet Egress FECN Packet Counter - cnt_type = 4'd7 : Ethernet Egress DE Packet Counter - cnt_type = 4'd8 : Ethernet Igress Packet Counter - cnt_type = 4'd9 : Ethernet Igress Byte Counter - cnt_type = 4'd10: Ethernet Igress Fragment Counter - cnt_type = 4'd11: Ethernet Igress Discarded Packet Counter - cnt_type = 4'd12: Ethernet Igress Discarded Byte Counter - cnt_type = 4'd13: Ethernet Igress BECN Packet Counter - cnt_type = 4'd14: Ethernet Igress FECN Packet Counter - cnt_type = 4'd15: Ethernet Igress DE Packet Counter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x20000+$cnt_type*8192+$r2c*4096+$cnt_id"
            
        def startAddress(self):
            return 0x00020000
            
        def endAddress(self):
            return 0x0003ffff

        class _ETH_CNT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ETH_CNT"
            
            def description(self):
                return "Ethernet Counters"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ETH_CNT"] = _AF6CCI0012_RD_INTPMC._ethfulltypcnt_pen._ETH_CNT()
            return allFields

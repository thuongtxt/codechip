import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0012_RD_PDA(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["ramjitbufcfg"] = _AF6CCI0012_RD_PDA._ramjitbufcfg()
        allRegisters["ramjitbufsta"] = _AF6CCI0012_RD_PDA._ramjitbufsta()
        allRegisters["ramreorcfg"] = _AF6CCI0012_RD_PDA._ramreorcfg()
        allRegisters["ramlotdmmodecfg"] = _AF6CCI0012_RD_PDA._ramlotdmmodecfg()
        allRegisters["ramlotdmlkupcfg"] = _AF6CCI0012_RD_PDA._ramlotdmlkupcfg()
        allRegisters["ramvcatlkupcfg"] = _AF6CCI0012_RD_PDA._ramvcatlkupcfg()
        allRegisters["ramhotdmlkupcfg"] = _AF6CCI0012_RD_PDA._ramhotdmlkupcfg()
        allRegisters["ramgelkupcfg"] = _AF6CCI0012_RD_PDA._ramgelkupcfg()
        allRegisters["ramhotdmmodecfg"] = _AF6CCI0012_RD_PDA._ramhotdmmodecfg()
        allRegisters["pda_hold_status"] = _AF6CCI0012_RD_PDA._pda_hold_status()
        allRegisters["ramlotdmprbsmon"] = _AF6CCI0012_RD_PDA._ramlotdmprbsmon()
        allRegisters["ramlotdmprbsgen"] = _AF6CCI0012_RD_PDA._ramlotdmprbsgen()
        allRegisters["pda_config_ecc_crc_parity_control"] = _AF6CCI0012_RD_PDA._pda_config_ecc_crc_parity_control()
        allRegisters["pda_config_ecc_crc_parity_disable_control"] = _AF6CCI0012_RD_PDA._pda_config_ecc_crc_parity_disable_control()
        allRegisters["pda_config_ecc_crc_parity_sticky"] = _AF6CCI0012_RD_PDA._pda_config_ecc_crc_parity_sticky()
        allRegisters["rdha3_0_control"] = _AF6CCI0012_RD_PDA._rdha3_0_control()
        allRegisters["rdha7_4_control"] = _AF6CCI0012_RD_PDA._rdha7_4_control()
        allRegisters["rdha11_8_control"] = _AF6CCI0012_RD_PDA._rdha11_8_control()
        allRegisters["rdha15_12_control"] = _AF6CCI0012_RD_PDA._rdha15_12_control()
        allRegisters["rdha19_16_control"] = _AF6CCI0012_RD_PDA._rdha19_16_control()
        allRegisters["rdha23_20_control"] = _AF6CCI0012_RD_PDA._rdha23_20_control()
        allRegisters["rdha24data_control"] = _AF6CCI0012_RD_PDA._rdha24data_control()
        allRegisters["rdha_hold63_32"] = _AF6CCI0012_RD_PDA._rdha_hold63_32()
        allRegisters["rdindr_hold95_64"] = _AF6CCI0012_RD_PDA._rdindr_hold95_64()
        allRegisters["rdindr_hold127_96"] = _AF6CCI0012_RD_PDA._rdindr_hold127_96()
        allRegisters["rtldumprxpwid"] = _AF6CCI0012_RD_PDA._rtldumprxpwid()
        allRegisters["rtldumprxdata"] = _AF6CCI0012_RD_PDA._rtldumprxdata()
        allRegisters["rtldumptxpwid"] = _AF6CCI0012_RD_PDA._rtldumptxpwid()
        allRegisters["rtldumptxdata"] = _AF6CCI0012_RD_PDA._rtldumptxdata()
        allRegisters["ramlotdmsmallds0control"] = _AF6CCI0012_RD_PDA._ramlotdmsmallds0control()
        return allRegisters

    class _ramjitbufcfg(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA Jitter Buffer Control"
    
        def description(self):
            return "This register configures jitter buffer parameters per pseudo-wire HDL_PATH: rtljitbuf.ramjitbufcfg.ram.ram[$PWID]"
            
        def width(self):
            return 57
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00010000 +  PWID"
            
        def startAddress(self):
            return 0x00010000
            
        def endAddress(self):
            return 0xffffffff

        class _PwLowDs0Mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 56
                
            def startBit(self):
                return 56
        
            def name(self):
                return "PwLowDs0Mode"
            
            def description(self):
                return "Pseudo-wire CES Low DS0 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwCEPMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 55
                
            def startBit(self):
                return 55
        
            def name(self):
                return "PwCEPMode"
            
            def description(self):
                return "Pseudo-wire CEP mode indication"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwHoLoOc48Id(AtRegister.AtRegisterField):
            def stopBit(self):
                return 54
                
            def startBit(self):
                return 53
        
            def name(self):
                return "PwHoLoOc48Id"
            
            def description(self):
                return "Indicate 2x Hi order OC48 or 2x Low order OC48 slice"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwLoSlice24Sel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 52
                
            def startBit(self):
                return 52
        
            def name(self):
                return "PwLoSlice24Sel"
            
            def description(self):
                return "Pseudo-wire Low order OC24 slice selection, only valid in Lo Pseudo-wire 1: This PW belong to Slice24 that trasnport STS 1,3,5,...,47 within a Lo OC48 0: This PW belong to Slice24 that trasnport STS 0,2,4,...,46 within a Lo OC48"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwTdmLineId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 51
                
            def startBit(self):
                return 42
        
            def name(self):
                return "PwTdmLineId"
            
            def description(self):
                return "Pseudo-wire(PW) corresponding TDM line ID. If the PW belong to Low order path, this is the OC24 TDM line ID that is using in Lo CDR,PDH and MAP. If the PW belong to Hi order CEP path, this is the OC48 master STS ID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwEparEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 41
                
            def startBit(self):
                return 41
        
            def name(self):
                return "PwEparEn"
            
            def description(self):
                return "Pseudo-wire EPAR timing mode enable 1: Enable EPAR timing mode 0: Disable EPAR timing mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwHiLoPathInd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 40
                
            def startBit(self):
                return 40
        
            def name(self):
                return "PwHiLoPathInd"
            
            def description(self):
                return "Pseudo-wire belong to Hi-order or Lo-order path 1: Pseudo-wire belong to Hi-order path 0: Pseudo-wire belong to Lo-order path"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwPayloadLen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 39
                
            def startBit(self):
                return 26
        
            def name(self):
                return "PwPayloadLen"
            
            def description(self):
                return "TDM Payload of pseudo-wire in byte unit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PdvSizeInPkUnit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 13
        
            def name(self):
                return "PdvSizeInPkUnit"
            
            def description(self):
                return "Pdv size in packet unit. This parameter is to prevent packet delay variation(PDV) from PSN. PdvSizePk is packet delay variation measured in packet unit. The formula is as below: PdvSizeInPkUnit = (PwSpeedinKbps * PdvInUsUnit)/(8000*PwPayloadLen) + ((PwSpeedInKbps * PdvInUsus)%(8000*PwPayloadLen) !=0)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _JitBufSizeInPkUnit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 0
        
            def name(self):
                return "JitBufSizeInPkUnit"
            
            def description(self):
                return "Jitter buffer size in packet unit. This parameter is mostly double of PdvSizeInPkUnit. The formula is as below: JitBufSizeInPkUnit = (PwSpeedinKbps * JitBufInUsUnit)/(8000*PwPayloadLen) + ((PwSpeedInKbps * JitBufInUsus)%(8000*PwPayloadLen) !=0)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PwLowDs0Mode"] = _AF6CCI0012_RD_PDA._ramjitbufcfg._PwLowDs0Mode()
            allFields["PwCEPMode"] = _AF6CCI0012_RD_PDA._ramjitbufcfg._PwCEPMode()
            allFields["PwHoLoOc48Id"] = _AF6CCI0012_RD_PDA._ramjitbufcfg._PwHoLoOc48Id()
            allFields["PwLoSlice24Sel"] = _AF6CCI0012_RD_PDA._ramjitbufcfg._PwLoSlice24Sel()
            allFields["PwTdmLineId"] = _AF6CCI0012_RD_PDA._ramjitbufcfg._PwTdmLineId()
            allFields["PwEparEn"] = _AF6CCI0012_RD_PDA._ramjitbufcfg._PwEparEn()
            allFields["PwHiLoPathInd"] = _AF6CCI0012_RD_PDA._ramjitbufcfg._PwHiLoPathInd()
            allFields["PwPayloadLen"] = _AF6CCI0012_RD_PDA._ramjitbufcfg._PwPayloadLen()
            allFields["PdvSizeInPkUnit"] = _AF6CCI0012_RD_PDA._ramjitbufcfg._PdvSizeInPkUnit()
            allFields["JitBufSizeInPkUnit"] = _AF6CCI0012_RD_PDA._ramjitbufcfg._JitBufSizeInPkUnit()
            return allFields

    class _ramjitbufsta(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA Jitter Buffer Status"
    
        def description(self):
            return "This register shows jitter buffer status per pseudo-wire"
            
        def width(self):
            return 55
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00014000 +  PWID"
            
        def startAddress(self):
            return 0x00014000
            
        def endAddress(self):
            return 0xffffffff

        class _JitBufHwComSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 54
                
            def startBit(self):
                return 20
        
            def name(self):
                return "JitBufHwComSta"
            
            def description(self):
                return "Harware debug only status"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _JitBufNumPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 4
        
            def name(self):
                return "JitBufNumPk"
            
            def description(self):
                return "Current number of packet in jitter buffer"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _JitBufFull(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "JitBufFull"
            
            def description(self):
                return "Jitter Buffer full status"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _JitBufState(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "JitBufState"
            
            def description(self):
                return "Jitter buffer state machine status 0: START 1: FILL 2: READY 3: READ 4: LOST 5: NEAR_EMPTY Others: NOT VALID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["JitBufHwComSta"] = _AF6CCI0012_RD_PDA._ramjitbufsta._JitBufHwComSta()
            allFields["JitBufNumPk"] = _AF6CCI0012_RD_PDA._ramjitbufsta._JitBufNumPk()
            allFields["JitBufFull"] = _AF6CCI0012_RD_PDA._ramjitbufsta._JitBufFull()
            allFields["JitBufState"] = _AF6CCI0012_RD_PDA._ramjitbufsta._JitBufState()
            return allFields

    class _ramreorcfg(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA Reorder Control"
    
        def description(self):
            return "This register configures reorder parameters per pseudo-wire"
            
        def width(self):
            return 26
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00020000 +  PWID"
            
        def startAddress(self):
            return 0x00020000
            
        def endAddress(self):
            return 0xffffffff

        class _PwSetLofsInPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 17
        
            def name(self):
                return "PwSetLofsInPk"
            
            def description(self):
                return "Number of consecutive lost packet to declare lost of packet state"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwSetLopsInMsec(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 9
        
            def name(self):
                return "PwSetLopsInMsec"
            
            def description(self):
                return "Number of empty time to declare lost of packet synchronization"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwReorEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PwReorEn"
            
            def description(self):
                return "Set 1 to enable reorder"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwReorTimeout(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PwReorTimeout"
            
            def description(self):
                return "Reorder timeout in 512us unit to detect lost packets. The formula is as below: ReorTimeout = ((Min(31,PdvSizeInPk) * PwPayloadLen * 16)/PwSpeedInKbps"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PwSetLofsInPk"] = _AF6CCI0012_RD_PDA._ramreorcfg._PwSetLofsInPk()
            allFields["PwSetLopsInMsec"] = _AF6CCI0012_RD_PDA._ramreorcfg._PwSetLopsInMsec()
            allFields["PwReorEn"] = _AF6CCI0012_RD_PDA._ramreorcfg._PwReorEn()
            allFields["PwReorTimeout"] = _AF6CCI0012_RD_PDA._ramreorcfg._PwReorTimeout()
            return allFields

    class _ramlotdmmodecfg(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA Low Order TDM mode Control"
    
        def description(self):
            return "This register configure TDM mode for interworking between Pseudowire and Lo TDM"
            
        def width(self):
            return 28
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00030000 + $LoOc48ID*0x1000 + $LoOrderoc24Slice*0x400 + $TdmOc24Pwid"
            
        def startAddress(self):
            return 0x00030000
            
        def endAddress(self):
            return 0xffffffff

        class _PDANxDS0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 22
        
            def name(self):
                return "PDANxDS0"
            
            def description(self):
                return "Number of DS0 allocated for CESoP PW"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAIdleCode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 14
        
            def name(self):
                return "PDAIdleCode"
            
            def description(self):
                return "Idle pattern to replace data in case of Lost/Lbit packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAAisRdiOff_BundEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "PDAAisRdiOff_BundEn"
            
            def description(self):
                return "Set 1 to indicate the link is joined in MLPPP bundle or disable sending AIS/Unequip/RDI from CES PW to TDM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDARepMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 11
        
            def name(self):
                return "PDARepMode"
            
            def description(self):
                return "Mode to replace lost and Lbit packet 0: Replace by replaying previous good packet (often for voice or video application) 1: Replace by AIS (default) 2: Replace by a configuration idle code Replace by a configuration idle code"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAStsId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PDAStsId"
            
            def description(self):
                return "Used in DS3/E3 SAToP  and TU3/VC3 CEP basic or ENCAP STS/DS3/E3 level, corresponding Slice OC24 STS ID of PW circuit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PDAMode"
            
            def description(self):
                return "TDM Payload De-Assembler modes 0: DS1/E1 SAToP or MLPPP/PPP over DS1/E1 2: CESoP without CAS 8: VC3/TU3 CEP basic 12: VC11/VC12 CEP basic 15: DS3/E3 SAToP or MLPPP/PPP over DS1/E1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAFracEbmEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDAFracEbmEn"
            
            def description(self):
                return "EBM enable in CEP fractional mode 0: EBM disable 1: EBM enable"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDANxDS0"] = _AF6CCI0012_RD_PDA._ramlotdmmodecfg._PDANxDS0()
            allFields["PDAIdleCode"] = _AF6CCI0012_RD_PDA._ramlotdmmodecfg._PDAIdleCode()
            allFields["PDAAisRdiOff_BundEn"] = _AF6CCI0012_RD_PDA._ramlotdmmodecfg._PDAAisRdiOff_BundEn()
            allFields["PDARepMode"] = _AF6CCI0012_RD_PDA._ramlotdmmodecfg._PDARepMode()
            allFields["Reserved"] = _AF6CCI0012_RD_PDA._ramlotdmmodecfg._Reserved()
            allFields["PDAStsId"] = _AF6CCI0012_RD_PDA._ramlotdmmodecfg._PDAStsId()
            allFields["PDAMode"] = _AF6CCI0012_RD_PDA._ramlotdmmodecfg._PDAMode()
            allFields["PDAFracEbmEn"] = _AF6CCI0012_RD_PDA._ramlotdmmodecfg._PDAFracEbmEn()
            return allFields

    class _ramlotdmlkupcfg(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA Low Order TDM Look Up Control"
    
        def description(self):
            return "This register configure lookup from TDM PWID to global 3072 PWID"
            
        def width(self):
            return 24
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00040000 + LoOc48ID*0x800 + LoOc24Slice*0x400 + TdmOc24PwID"
            
        def startAddress(self):
            return 0x00040000
            
        def endAddress(self):
            return 0xffffffff

        class _MlpppBundId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 15
        
            def name(self):
                return "MlpppBundId"
            
            def description(self):
                return "MLPPP bundle ID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MlpppEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "MlpppEn"
            
            def description(self):
                return "MLPPP enable 0: Link run PPP mode 1: Link join in a bundle in MLPPP mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwCesEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "PwCesEnable"
            
            def description(self):
                return "CES PW Enable 0: MLPPP/PPP mode 1: CES PW mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwLkEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PwLkEnable"
            
            def description(self):
                return "Enable Lookup"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PwID"
            
            def description(self):
                return "Flat 3072 CES PWID or other service ID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MlpppBundId"] = _AF6CCI0012_RD_PDA._ramlotdmlkupcfg._MlpppBundId()
            allFields["MlpppEn"] = _AF6CCI0012_RD_PDA._ramlotdmlkupcfg._MlpppEn()
            allFields["PwCesEnable"] = _AF6CCI0012_RD_PDA._ramlotdmlkupcfg._PwCesEnable()
            allFields["PwLkEnable"] = _AF6CCI0012_RD_PDA._ramlotdmlkupcfg._PwLkEnable()
            allFields["PwID"] = _AF6CCI0012_RD_PDA._ramlotdmlkupcfg._PwID()
            return allFields

    class _ramvcatlkupcfg(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA VCAT Look Up Control"
    
        def description(self):
            return "This register configure lookup enable of VCAT VC group"
            
        def width(self):
            return 18
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00041000 + VcgID"
            
        def startAddress(self):
            return 0x00041000
            
        def endAddress(self):
            return 0xffffffff

        class _PwReserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 13
        
            def name(self):
                return "PwReserve"
            
            def description(self):
                return "Must set to zero"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VcgLkEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "VcgLkEnable"
            
            def description(self):
                return "VC group lookup enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Reserve"
            
            def description(self):
                return "Set to zero"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PwReserve"] = _AF6CCI0012_RD_PDA._ramvcatlkupcfg._PwReserve()
            allFields["VcgLkEnable"] = _AF6CCI0012_RD_PDA._ramvcatlkupcfg._VcgLkEnable()
            allFields["Reserve"] = _AF6CCI0012_RD_PDA._ramvcatlkupcfg._Reserve()
            return allFields

    class _ramhotdmlkupcfg(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA High Order TDM Look Up Control"
    
        def description(self):
            return "This register configure lookup from HO TDM master STSIS to global 6144 PWID"
            
        def width(self):
            return 17
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00041100 + HoOc48ID*0x40 + HoMasSTS"
            
        def startAddress(self):
            return 0x00041100
            
        def endAddress(self):
            return 0xffffffff

        class _PwReserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 14
        
            def name(self):
                return "PwReserve"
            
            def description(self):
                return "Must set to zero"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwCesEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "PwCesEnable"
            
            def description(self):
                return "CES PW Enable 0: MLPPP/PPP mode 1: CES PW mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwLkEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PwLkEnable"
            
            def description(self):
                return "Enable Lookup"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PwID"
            
            def description(self):
                return "Flat 3072 PWID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PwReserve"] = _AF6CCI0012_RD_PDA._ramhotdmlkupcfg._PwReserve()
            allFields["PwCesEnable"] = _AF6CCI0012_RD_PDA._ramhotdmlkupcfg._PwCesEnable()
            allFields["PwLkEnable"] = _AF6CCI0012_RD_PDA._ramhotdmlkupcfg._PwLkEnable()
            allFields["PwID"] = _AF6CCI0012_RD_PDA._ramhotdmlkupcfg._PwID()
            return allFields

    class _ramgelkupcfg(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA GE Bypass Look Up Control"
    
        def description(self):
            return "This register configure lookup enable of GE bypass, GeId = 4,5 is used for eXAUI"
            
        def width(self):
            return 17
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00041180 + GeId"
            
        def startAddress(self):
            return 0x00041180
            
        def endAddress(self):
            return 0xffffffff

        class _Reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 13
        
            def name(self):
                return "Reserve"
            
            def description(self):
                return "Must set to zero"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _GeLkEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "GeLkEnable"
            
            def description(self):
                return "Enable Lookup"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _GeServiceID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "GeServiceID"
            
            def description(self):
                return "Service ID configured in CLA"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Reserve"] = _AF6CCI0012_RD_PDA._ramgelkupcfg._Reserve()
            allFields["GeLkEnable"] = _AF6CCI0012_RD_PDA._ramgelkupcfg._GeLkEnable()
            allFields["GeServiceID"] = _AF6CCI0012_RD_PDA._ramgelkupcfg._GeServiceID()
            return allFields

    class _ramhotdmmodecfg(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA High Order TDM mode Control"
    
        def description(self):
            return "This register configure TDM mode for interworking between Pseudowire and Ho TDM"
            
        def width(self):
            return 13
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00050000 + HoOc48ID*0x100 + HoMasSTS"
            
        def startAddress(self):
            return 0x00050000
            
        def endAddress(self):
            return 0xffffffff

        class _PDAHoIdleCode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PDAHoIdleCode"
            
            def description(self):
                return "Idle pattern to replace data in case of Lost/Lbit packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAHoAisUneqOff(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PDAHoAisUneqOff"
            
            def description(self):
                return "Set 1 to disable sending AIS/Unequip from PW to Ho TDM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAHoRepMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PDAHoRepMode"
            
            def description(self):
                return "HO Mode to replace lost and Lbit packet 0: Replace by replaying previous good packet (often for voice or video application) 1: Replace by AIS (default) 2: Replace by a configuration idle code Replace by a configuration idle code"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAHoMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDAHoMode"
            
            def description(self):
                return "TDM HO TDM mode 0: STS1 CEP basic 1: STS3/STS6 CEP basic 2: STS9/STS12/STS15 CEP basic 3: STS18 to STS48 CEP basic"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDAHoIdleCode"] = _AF6CCI0012_RD_PDA._ramhotdmmodecfg._PDAHoIdleCode()
            allFields["PDAHoAisUneqOff"] = _AF6CCI0012_RD_PDA._ramhotdmmodecfg._PDAHoAisUneqOff()
            allFields["PDAHoRepMode"] = _AF6CCI0012_RD_PDA._ramhotdmmodecfg._PDAHoRepMode()
            allFields["PDAHoMode"] = _AF6CCI0012_RD_PDA._ramhotdmmodecfg._PDAHoMode()
            return allFields

    class _pda_hold_status(AtRegister.AtRegister):
        def name(self):
            return "PDA Hold Register Status"
    
        def description(self):
            return "This register using for hold remain that more than 128bits"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000000 +  HID"
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0x00000002

        class _PdaHoldStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PdaHoldStatus"
            
            def description(self):
                return "Hold 32bits"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PdaHoldStatus"] = _AF6CCI0012_RD_PDA._pda_hold_status._PdaHoldStatus()
            return allFields

    class _ramlotdmprbsmon(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA per OC48 Low Order TDM PRBS J1 V5 Monitor Status"
    
        def description(self):
            return "This register show the PRBS monitor status after PDA"
            
        def width(self):
            return 18
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00080800 + LoOc48ID*0x2000 + LoOc24Slice*0x1000 + TdmOc24PwID"
            
        def startAddress(self):
            return 0x00080800
            
        def endAddress(self):
            return 0x0008ffff

        class _PDAPrbsB3V5Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "PDAPrbsB3V5Err"
            
            def description(self):
                return "PDA PRBS or B3 or V5 monitor error"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAPrbsSyncSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PDAPrbsSyncSta"
            
            def description(self):
                return "PDA PRBS sync status"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAPrbsValue(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDAPrbsValue"
            
            def description(self):
                return "PDA PRBS monitor value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDAPrbsB3V5Err"] = _AF6CCI0012_RD_PDA._ramlotdmprbsmon._PDAPrbsB3V5Err()
            allFields["PDAPrbsSyncSta"] = _AF6CCI0012_RD_PDA._ramlotdmprbsmon._PDAPrbsSyncSta()
            allFields["PDAPrbsValue"] = _AF6CCI0012_RD_PDA._ramlotdmprbsmon._PDAPrbsValue()
            return allFields

    class _ramlotdmprbsgen(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA per OC48 Low Order TDM PRBS Generator Control"
    
        def description(self):
            return "This register show the PRBS monitor status after PDA"
            
        def width(self):
            return 2
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00080400 + LoOc48ID*0x2000 + LoOc24Slice*0x1000 + TdmOc24PwID"
            
        def startAddress(self):
            return 0x00080400
            
        def endAddress(self):
            return 0x0008ffff

        class _PDASeqmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PDASeqmode"
            
            def description(self):
                return "PDA Sequential data mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAPrbsGenEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDAPrbsGenEn"
            
            def description(self):
                return "PDA PRBS Generation enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDASeqmode"] = _AF6CCI0012_RD_PDA._ramlotdmprbsgen._PDASeqmode()
            allFields["PDAPrbsGenEn"] = _AF6CCI0012_RD_PDA._ramlotdmprbsgen._PDAPrbsGenEn()
            return allFields

    class _pda_config_ecc_crc_parity_control(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA ECC CRC Parity Control"
    
        def description(self):
            return "This register configures PDA ECC CRC and Parity."
            
        def width(self):
            return 7
        
        def type(self):
            return "Config|Enable"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000008
            
        def endAddress(self):
            return 0xffffffff

        class _PDAForceEccCor(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PDAForceEccCor"
            
            def description(self):
                return "PDA force link list Ecc error correctable   1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAForceEccErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PDAForceEccErr"
            
            def description(self):
                return "PDA force link list Ecc error noncorrectable   1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAForceCrcErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PDAForceCrcErr"
            
            def description(self):
                return "PDA force data CRC error  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAForceLoTdmParErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "PDAForceLoTdmParErr"
            
            def description(self):
                return "Pseudowire PDA Low Order TDM mode Control force parity error  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAForceTdmLkParErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PDAForceTdmLkParErr"
            
            def description(self):
                return "Pseudowire PDA Lo and Ho TDM Look Up Control force parity error  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAForceJitBufParErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PDAForceJitBufParErr"
            
            def description(self):
                return "Pseudowire PDA Jitter Buffer Control force parity error  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAForceReorderParErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDAForceReorderParErr"
            
            def description(self):
                return "Pseudowire PDA Reorder Control force parity error  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDAForceEccCor"] = _AF6CCI0012_RD_PDA._pda_config_ecc_crc_parity_control._PDAForceEccCor()
            allFields["PDAForceEccErr"] = _AF6CCI0012_RD_PDA._pda_config_ecc_crc_parity_control._PDAForceEccErr()
            allFields["PDAForceCrcErr"] = _AF6CCI0012_RD_PDA._pda_config_ecc_crc_parity_control._PDAForceCrcErr()
            allFields["PDAForceLoTdmParErr"] = _AF6CCI0012_RD_PDA._pda_config_ecc_crc_parity_control._PDAForceLoTdmParErr()
            allFields["PDAForceTdmLkParErr"] = _AF6CCI0012_RD_PDA._pda_config_ecc_crc_parity_control._PDAForceTdmLkParErr()
            allFields["PDAForceJitBufParErr"] = _AF6CCI0012_RD_PDA._pda_config_ecc_crc_parity_control._PDAForceJitBufParErr()
            allFields["PDAForceReorderParErr"] = _AF6CCI0012_RD_PDA._pda_config_ecc_crc_parity_control._PDAForceReorderParErr()
            return allFields

    class _pda_config_ecc_crc_parity_disable_control(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA ECC CRC Parity Disable Control"
    
        def description(self):
            return "This register configures PDA ECC CRC and Parity Disable."
            
        def width(self):
            return 7
        
        def type(self):
            return "Config|Enable"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000009
            
        def endAddress(self):
            return 0xffffffff

        class _PDADisableEccCor(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PDADisableEccCor"
            
            def description(self):
                return "PDA disable link list Ecc error correctable   1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDADisableEccErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PDADisableEccErr"
            
            def description(self):
                return "PDA disable link list Ecc error noncorrectable   1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDADisableCrcErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PDADisableCrcErr"
            
            def description(self):
                return "PDA disable data CRC error  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDADisableLoTdmParErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "PDADisableLoTdmParErr"
            
            def description(self):
                return "Pseudowire PDA Low Order TDM mode Control disable parity error  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDADisableTdmLkParErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PDADisableTdmLkParErr"
            
            def description(self):
                return "Pseudowire PDA Lo and Ho TDM Look Up Control disable parity error  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDADisableJitBufParErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PDADisableJitBufParErr"
            
            def description(self):
                return "Pseudowire PDA Jitter Buffer Control disable parity error  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDADisableReorderParErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDADisableReorderParErr"
            
            def description(self):
                return "Pseudowire PDA Reorder Control disable parity error  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDADisableEccCor"] = _AF6CCI0012_RD_PDA._pda_config_ecc_crc_parity_disable_control._PDADisableEccCor()
            allFields["PDADisableEccErr"] = _AF6CCI0012_RD_PDA._pda_config_ecc_crc_parity_disable_control._PDADisableEccErr()
            allFields["PDADisableCrcErr"] = _AF6CCI0012_RD_PDA._pda_config_ecc_crc_parity_disable_control._PDADisableCrcErr()
            allFields["PDADisableLoTdmParErr"] = _AF6CCI0012_RD_PDA._pda_config_ecc_crc_parity_disable_control._PDADisableLoTdmParErr()
            allFields["PDADisableTdmLkParErr"] = _AF6CCI0012_RD_PDA._pda_config_ecc_crc_parity_disable_control._PDADisableTdmLkParErr()
            allFields["PDADisableJitBufParErr"] = _AF6CCI0012_RD_PDA._pda_config_ecc_crc_parity_disable_control._PDADisableJitBufParErr()
            allFields["PDADisableReorderParErr"] = _AF6CCI0012_RD_PDA._pda_config_ecc_crc_parity_disable_control._PDADisableReorderParErr()
            return allFields

    class _pda_config_ecc_crc_parity_sticky(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA ECC CRC Parity Sticky"
    
        def description(self):
            return "This register configures PDA ECC CRC and Parity."
            
        def width(self):
            return 7
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000a
            
        def endAddress(self):
            return 0xffffffff

        class _PDAStickyEccCor(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PDAStickyEccCor"
            
            def description(self):
                return "PDA sticky link list Ecc error correctable   1: Set  0: Clear"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAStickyEccErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PDAStickyEccErr"
            
            def description(self):
                return "PDA sticky link list Ecc error noncorrectable   1: Set  0: Clear"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAStickyCrcErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PDAStickyCrcErr"
            
            def description(self):
                return "PDA sticky data CRC error  1: Set  0: Clear"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAStickyLoTdmParErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "PDAStickyLoTdmParErr"
            
            def description(self):
                return "Pseudowire PDA Low Order TDM mode Control sticky parity error  1: Set  0: Clear"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAStickyTdmLkParErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PDAStickyTdmLkParErr"
            
            def description(self):
                return "Pseudowire PDA Lo and Ho TDM Look Up Control sticky parity error  1: Set  0: Clear"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAStickyJitBufParErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PDAStickyJitBufParErr"
            
            def description(self):
                return "Pseudowire PDA Jitter Buffer Control sticky parity error  1: Set  0: Clear"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAStickyReorderParErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDAStickyReorderParErr"
            
            def description(self):
                return "Pseudowire PDA Reorder Control sticky parity error  1: Set  0: Clear"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDAStickyEccCor"] = _AF6CCI0012_RD_PDA._pda_config_ecc_crc_parity_sticky._PDAStickyEccCor()
            allFields["PDAStickyEccErr"] = _AF6CCI0012_RD_PDA._pda_config_ecc_crc_parity_sticky._PDAStickyEccErr()
            allFields["PDAStickyCrcErr"] = _AF6CCI0012_RD_PDA._pda_config_ecc_crc_parity_sticky._PDAStickyCrcErr()
            allFields["PDAStickyLoTdmParErr"] = _AF6CCI0012_RD_PDA._pda_config_ecc_crc_parity_sticky._PDAStickyLoTdmParErr()
            allFields["PDAStickyTdmLkParErr"] = _AF6CCI0012_RD_PDA._pda_config_ecc_crc_parity_sticky._PDAStickyTdmLkParErr()
            allFields["PDAStickyJitBufParErr"] = _AF6CCI0012_RD_PDA._pda_config_ecc_crc_parity_sticky._PDAStickyJitBufParErr()
            allFields["PDAStickyReorderParErr"] = _AF6CCI0012_RD_PDA._pda_config_ecc_crc_parity_sticky._PDAStickyReorderParErr()
            return allFields

    class _rdha3_0_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit3_0 Control"
    
        def description(self):
            return "This register is used to send HA read address bit3_0 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x01000 + HaAddr3_0"
            
        def startAddress(self):
            return 0x00001000
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr3_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr3_0"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr3_0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr3_0"] = _AF6CCI0012_RD_PDA._rdha3_0_control._ReadAddr3_0()
            return allFields

    class _rdha7_4_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit7_4 Control"
    
        def description(self):
            return "This register is used to send HA read address bit7_4 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x01010 + HaAddr7_4"
            
        def startAddress(self):
            return 0x00001010
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr7_4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr7_4"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr7_4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr7_4"] = _AF6CCI0012_RD_PDA._rdha7_4_control._ReadAddr7_4()
            return allFields

    class _rdha11_8_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit11_8 Control"
    
        def description(self):
            return "This register is used to send HA read address bit11_8 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x01020 + HaAddr11_8"
            
        def startAddress(self):
            return 0x00001020
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr11_8(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr11_8"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr11_8"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr11_8"] = _AF6CCI0012_RD_PDA._rdha11_8_control._ReadAddr11_8()
            return allFields

    class _rdha15_12_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit15_12 Control"
    
        def description(self):
            return "This register is used to send HA read address bit15_12 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x01030 + HaAddr15_12"
            
        def startAddress(self):
            return 0x00001030
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr15_12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr15_12"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr15_12"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr15_12"] = _AF6CCI0012_RD_PDA._rdha15_12_control._ReadAddr15_12()
            return allFields

    class _rdha19_16_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit19_16 Control"
    
        def description(self):
            return "This register is used to send HA read address bit19_16 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x01040 + HaAddr19_16"
            
        def startAddress(self):
            return 0x00001040
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr19_16(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr19_16"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr19_16"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr19_16"] = _AF6CCI0012_RD_PDA._rdha19_16_control._ReadAddr19_16()
            return allFields

    class _rdha23_20_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit23_20 Control"
    
        def description(self):
            return "This register is used to send HA read address bit23_20 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x01050 + HaAddr23_20"
            
        def startAddress(self):
            return 0x00001050
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr23_20(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr23_20"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr23_20"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr23_20"] = _AF6CCI0012_RD_PDA._rdha23_20_control._ReadAddr23_20()
            return allFields

    class _rdha24data_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit24 and Data Control"
    
        def description(self):
            return "This register is used to send HA read address bit24 to HA engine to read data"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x01060 + HaAddr24"
            
        def startAddress(self):
            return 0x00001060
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData31_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData31_0"
            
            def description(self):
                return "HA read data bit31_0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData31_0"] = _AF6CCI0012_RD_PDA._rdha24data_control._ReadHaData31_0()
            return allFields

    class _rdha_hold63_32(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data63_32"
    
        def description(self):
            return "This register is used to read HA dword2 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00001070
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData63_32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData63_32"
            
            def description(self):
                return "HA read data bit63_32"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData63_32"] = _AF6CCI0012_RD_PDA._rdha_hold63_32._ReadHaData63_32()
            return allFields

    class _rdindr_hold95_64(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data95_64"
    
        def description(self):
            return "This register is used to read HA dword3 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00001071
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData95_64(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData95_64"
            
            def description(self):
                return "HA read data bit95_64"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData95_64"] = _AF6CCI0012_RD_PDA._rdindr_hold95_64._ReadHaData95_64()
            return allFields

    class _rdindr_hold127_96(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data127_96"
    
        def description(self):
            return "This register is used to read HA dword4 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00001072
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData127_96(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData127_96"
            
            def description(self):
                return "HA read data bit127_96"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData127_96"] = _AF6CCI0012_RD_PDA._rdindr_hold127_96._ReadHaData127_96()
            return allFields

    class _rtldumprxpwid(AtRegister.AtRegister):
        def name(self):
            return "PDA Dump Input Service ID Control"
    
        def description(self):
            return "This register is used to select service ID from CLA to dump data, write to this register will start dump"
            
        def width(self):
            return 12
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000c00
            
        def endAddress(self):
            return 0xffffffff

        class _PDADumpInputSerId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDADumpInputSerId"
            
            def description(self):
                return "PDADumpInputSerId"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDADumpInputSerId"] = _AF6CCI0012_RD_PDA._rtldumprxpwid._PDADumpInputSerId()
            return allFields

    class _rtldumprxdata(AtRegister.AtRegister):
        def name(self):
            return "PDA Dump Input Service Data Control"
    
        def description(self):
            return "This register is used to store dumped data"
            
        def width(self):
            return 36
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00E00 + Entry"
            
        def startAddress(self):
            return 0x00000e00
            
        def endAddress(self):
            return 0xffffffff

        class _PDADumpInputSop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 35
                
            def startBit(self):
                return 35
        
            def name(self):
                return "PDADumpInputSop"
            
            def description(self):
                return "PDADumpInputSop"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDADumpInputEop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 34
                
            def startBit(self):
                return 34
        
            def name(self):
                return "PDADumpInputEop"
            
            def description(self):
                return "PDADumpInputEop"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDADumpInputNob(AtRegister.AtRegisterField):
            def stopBit(self):
                return 33
                
            def startBit(self):
                return 32
        
            def name(self):
                return "PDADumpInputNob"
            
            def description(self):
                return "PDADumpInputNob 0 means 1 byte"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDADumpInputData(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDADumpInputData"
            
            def description(self):
                return "PDADumpInputData MSB"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDADumpInputSop"] = _AF6CCI0012_RD_PDA._rtldumprxdata._PDADumpInputSop()
            allFields["PDADumpInputEop"] = _AF6CCI0012_RD_PDA._rtldumprxdata._PDADumpInputEop()
            allFields["PDADumpInputNob"] = _AF6CCI0012_RD_PDA._rtldumprxdata._PDADumpInputNob()
            allFields["PDADumpInputData"] = _AF6CCI0012_RD_PDA._rtldumprxdata._PDADumpInputData()
            return allFields

    class _rtldumptxpwid(AtRegister.AtRegister):
        def name(self):
            return "PDA Dump Output Service ID Control"
    
        def description(self):
            return "This register is used to select service ID from PDA to dump data, write to this register will start dump"
            
        def width(self):
            return 13
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00064000
            
        def endAddress(self):
            return 0xffffffff

        class _PDADumpOutputSerId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDADumpOutputSerId"
            
            def description(self):
                return "PDADumpOutputSerId LO:          OutputSerId = slice48*0x800 + slice24*0x400 + tdmpwid HO:          OutputSerId = 0x1100 + slice48*0x40 + masterSTS VCAT:        OutputSerId = 0x1000 + vcgid GE bypass:   OutputSerId = 0x1180 + geid (0/1/2/3) XAUI:        OutputSerId = 0x1184 + xauiid (0/1)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDADumpOutputSerId"] = _AF6CCI0012_RD_PDA._rtldumptxpwid._PDADumpOutputSerId()
            return allFields

    class _rtldumptxdata(AtRegister.AtRegister):
        def name(self):
            return "PDA Dump Output Service Data Control"
    
        def description(self):
            return "This register is used to store dumped data"
            
        def width(self):
            return 19
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x66000 + Entry"
            
        def startAddress(self):
            return 0x00066000
            
        def endAddress(self):
            return 0xffffffff

        class _PDADumpOutputSop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "PDADumpOutputSop"
            
            def description(self):
                return "PDADumpOutputSop"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDADumpOutputEop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "PDADumpOutputEop"
            
            def description(self):
                return "PDADumpOutputEop"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDADumpOutputNob(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PDADumpOutputNob"
            
            def description(self):
                return "PDADumpOutputNob 0 means 1 byte"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDADumpOutputData(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDADumpOutputData"
            
            def description(self):
                return "PDADumpOutputData MSB"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDADumpOutputSop"] = _AF6CCI0012_RD_PDA._rtldumptxdata._PDADumpOutputSop()
            allFields["PDADumpOutputEop"] = _AF6CCI0012_RD_PDA._rtldumptxdata._PDADumpOutputEop()
            allFields["PDADumpOutputNob"] = _AF6CCI0012_RD_PDA._rtldumptxdata._PDADumpOutputNob()
            allFields["PDADumpOutputData"] = _AF6CCI0012_RD_PDA._rtldumptxdata._PDADumpOutputData()
            return allFields

    class _ramlotdmsmallds0control(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA per OC48 Low Order TDM CESoP Small DS0 Control"
    
        def description(self):
            return "This register show the PRBS monitor status after PDA"
            
        def width(self):
            return 1
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00080000 + LoOc48ID*0x2000 + LoOc24Slice*0x1000 + TdmOc24PwID"
            
        def startAddress(self):
            return 0x00080000
            
        def endAddress(self):
            return 0x0008ffff

        class _PDASmallDs0En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDASmallDs0En"
            
            def description(self):
                return "PDA CESoP small DS0 enable 1: CESoP Pseodo-wire with NxDS0 <= 3 0: Other Pseodo-wire modes"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDASmallDs0En"] = _AF6CCI0012_RD_PDA._ramlotdmsmallds0control._PDASmallDs0En()
            return allFields

import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0011_RD_eXAUI(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["eth10g_ver_ctr"] = _AF6CCI0011_RD_eXAUI._eth10g_ver_ctr()
        allRegisters["eth10g_flush_ctr"] = _AF6CCI0011_RD_eXAUI._eth10g_flush_ctr()
        allRegisters["eth10g_loop_ctr"] = _AF6CCI0011_RD_eXAUI._eth10g_loop_ctr()
        allRegisters["eth10g_mac_rx_ctr"] = _AF6CCI0011_RD_eXAUI._eth10g_mac_rx_ctr()
        allRegisters["eth10g_mac_tx_ctr"] = _AF6CCI0011_RD_eXAUI._eth10g_mac_tx_ctr()
        allRegisters["TxEth_cnt0_64_ro"] = _AF6CCI0011_RD_eXAUI._TxEth_cnt0_64_ro()
        allRegisters["TxEth_cnt0_64_rc"] = _AF6CCI0011_RD_eXAUI._TxEth_cnt0_64_rc()
        allRegisters["TxEth_cnt65_127_ro"] = _AF6CCI0011_RD_eXAUI._TxEth_cnt65_127_ro()
        allRegisters["TxEth_cnt65_127_rc"] = _AF6CCI0011_RD_eXAUI._TxEth_cnt65_127_rc()
        allRegisters["TxEth_cnt128_255_ro"] = _AF6CCI0011_RD_eXAUI._TxEth_cnt128_255_ro()
        allRegisters["TxEth_cnt128_255_rc"] = _AF6CCI0011_RD_eXAUI._TxEth_cnt128_255_rc()
        allRegisters["TxEth_cnt256_511_ro"] = _AF6CCI0011_RD_eXAUI._TxEth_cnt256_511_ro()
        allRegisters["TxEth_cnt256_511_rc"] = _AF6CCI0011_RD_eXAUI._TxEth_cnt256_511_rc()
        allRegisters["TxEth_cnt512_1024_ro"] = _AF6CCI0011_RD_eXAUI._TxEth_cnt512_1024_ro()
        allRegisters["TxEth_cnt512_1024_rc"] = _AF6CCI0011_RD_eXAUI._TxEth_cnt512_1024_rc()
        allRegisters["Eth_cnt1025_1528_ro"] = _AF6CCI0011_RD_eXAUI._Eth_cnt1025_1528_ro()
        allRegisters["Eth_cnt1025_1528_rc"] = _AF6CCI0011_RD_eXAUI._Eth_cnt1025_1528_rc()
        allRegisters["TxEth_cnt1529_2047_ro"] = _AF6CCI0011_RD_eXAUI._TxEth_cnt1529_2047_ro()
        allRegisters["TxEth_cnt1529_2047_rc"] = _AF6CCI0011_RD_eXAUI._TxEth_cnt1529_2047_rc()
        allRegisters["TxEth_cnt_jumbo_ro"] = _AF6CCI0011_RD_eXAUI._TxEth_cnt_jumbo_ro()
        allRegisters["TxEth_cnt_jumbo_rc"] = _AF6CCI0011_RD_eXAUI._TxEth_cnt_jumbo_rc()
        allRegisters["eth_tx_pkt_cnt_ro"] = _AF6CCI0011_RD_eXAUI._eth_tx_pkt_cnt_ro()
        allRegisters["eth_tx_pkt_cnt_rc"] = _AF6CCI0011_RD_eXAUI._eth_tx_pkt_cnt_rc()
        allRegisters["TxEth_cnt_byte_ro"] = _AF6CCI0011_RD_eXAUI._TxEth_cnt_byte_ro()
        allRegisters["TxEth_cnt_byte_rc"] = _AF6CCI0011_RD_eXAUI._TxEth_cnt_byte_rc()
        allRegisters["RxEth_cnt0_64_ro"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt0_64_ro()
        allRegisters["RxEth_cnt0_64_rc"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt0_64_rc()
        allRegisters["RxEth_cnt65_127_ro"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt65_127_ro()
        allRegisters["RxEth_cnt65_127_rc"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt65_127_rc()
        allRegisters["RxEth_cnt128_255_ro"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt128_255_ro()
        allRegisters["RxEth_cnt128_255_rc"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt128_255_rc()
        allRegisters["RxEth_cnt256_511_ro"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt256_511_ro()
        allRegisters["RxEth_cnt256_511_rc"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt256_511_rc()
        allRegisters["RxEth_cnt512_1024_ro"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt512_1024_ro()
        allRegisters["RxEth_cnt512_1024_rc"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt512_1024_rc()
        allRegisters["RxEth_cnt1025_1528_ro"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt1025_1528_ro()
        allRegisters["RxEth_cnt1025_1528_rc"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt1025_1528_rc()
        allRegisters["RxEth_cnt1529_2047_ro"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt1529_2047_ro()
        allRegisters["RxEth_cnt1529_2047_rc"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt1529_2047_rc()
        allRegisters["Eth_cnt_jumbo_ro"] = _AF6CCI0011_RD_eXAUI._Eth_cnt_jumbo_ro()
        allRegisters["Eth_cnt_jumbo_rc"] = _AF6CCI0011_RD_eXAUI._Eth_cnt_jumbo_rc()
        allRegisters["rx_port_pkt_cnt_ro"] = _AF6CCI0011_RD_eXAUI._rx_port_pkt_cnt_ro()
        allRegisters["rx_port_pkt_cnt_rc"] = _AF6CCI0011_RD_eXAUI._rx_port_pkt_cnt_rc()
        allRegisters["rx_port_under_size_pkt_cnt_ro"] = _AF6CCI0011_RD_eXAUI._rx_port_under_size_pkt_cnt_ro()
        allRegisters["rx_port_under_size_pkt_cnt_rc"] = _AF6CCI0011_RD_eXAUI._rx_port_under_size_pkt_cnt_rc()
        allRegisters["rx_port_over_size_pkt_cnt_ro"] = _AF6CCI0011_RD_eXAUI._rx_port_over_size_pkt_cnt_ro()
        allRegisters["rx_port_over_size_pkt_cnt_rc"] = _AF6CCI0011_RD_eXAUI._rx_port_over_size_pkt_cnt_rc()
        allRegisters["RxEth_cnt_phy_err_ro"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt_phy_err_ro()
        allRegisters["RxEth_cnt_phy_err_rc"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt_phy_err_rc()
        allRegisters["RxEth_cnt_byte_ro"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt_byte_ro()
        allRegisters["RxEth_cnt_byte_rc"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt_byte_rc()
        allRegisters["eth10g_diag_ctr"] = _AF6CCI0011_RD_eXAUI._eth10g_diag_ctr()
        allRegisters["eth10g_diag_max_len_ctr"] = _AF6CCI0011_RD_eXAUI._eth10g_diag_max_len_ctr()
        allRegisters["eth10g_diag_min_len_ctr"] = _AF6CCI0011_RD_eXAUI._eth10g_diag_min_len_ctr()
        allRegisters["eth10g_diag_num_pkt_ctr"] = _AF6CCI0011_RD_eXAUI._eth10g_diag_num_pkt_ctr()
        allRegisters["eth10g_diag_err_stk"] = _AF6CCI0011_RD_eXAUI._eth10g_diag_err_stk()
        allRegisters["diag_rx_pkt_cnt_ro"] = _AF6CCI0011_RD_eXAUI._diag_rx_pkt_cnt_ro()
        allRegisters["diag_rx_pkt_cnt_rc"] = _AF6CCI0011_RD_eXAUI._diag_rx_pkt_cnt_rc()
        allRegisters["diag_rx_cnt_byte_ro"] = _AF6CCI0011_RD_eXAUI._diag_rx_cnt_byte_ro()
        allRegisters["diag_rx_cnt_byte_rc"] = _AF6CCI0011_RD_eXAUI._diag_rx_cnt_byte_rc()
        allRegisters["diag_gen_bw_ctrl"] = _AF6CCI0011_RD_eXAUI._diag_gen_bw_ctrl()
        return allRegisters

    class _eth10g_ver_ctr(AtRegister.AtRegister):
        def name(self):
            return "ETH 10G Version Control"
    
        def description(self):
            return "This register checks version ID"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00001000
            
        def endAddress(self):
            return 0xffffffff

        class _Eth10gVendor(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Eth10gVendor"
            
            def description(self):
                return "Arrive ETH 10G MAC"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Eth10GVer(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Eth10GVer"
            
            def description(self):
                return "Version ID"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Eth10gVendor"] = _AF6CCI0011_RD_eXAUI._eth10g_ver_ctr._Eth10gVendor()
            allFields["Eth10GVer"] = _AF6CCI0011_RD_eXAUI._eth10g_ver_ctr._Eth10GVer()
            return allFields

    class _eth10g_flush_ctr(AtRegister.AtRegister):
        def name(self):
            return "ETH 10G Flush Control"
    
        def description(self):
            return "This register is used to flush engine"
            
        def width(self):
            return 3
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00001001
            
        def endAddress(self):
            return 0xffffffff

        class _Eth10gFlushCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Eth10gFlushCtrl"
            
            def description(self):
                return "Flush engine"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Eth10gFlushCtrl"] = _AF6CCI0011_RD_eXAUI._eth10g_flush_ctr._Eth10gFlushCtrl()
            return allFields

    class _eth10g_loop_ctr(AtRegister.AtRegister):
        def name(self):
            return "ETH 10G Loopback Control"
    
        def description(self):
            return "This register is used to configure loopback funtion"
            
        def width(self):
            return 6
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00001040
            
        def endAddress(self):
            return 0xffffffff

        class _Eth10gActPortLoopoutCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Eth10gActPortLoopoutCtrl"
            
            def description(self):
                return "Active  port loop-out (XGMII rx to XGMII tx)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Eth10gActPortLoopinCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Eth10gActPortLoopinCtrl"
            
            def description(self):
                return "Active  port loop-in (XGMII tx to XGMII rx)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Eth10gActPortLoopoutCtrl"] = _AF6CCI0011_RD_eXAUI._eth10g_loop_ctr._Eth10gActPortLoopoutCtrl()
            allFields["Eth10gActPortLoopinCtrl"] = _AF6CCI0011_RD_eXAUI._eth10g_loop_ctr._Eth10gActPortLoopinCtrl()
            return allFields

    class _eth10g_mac_rx_ctr(AtRegister.AtRegister):
        def name(self):
            return "ETH 10G MAC Reveive Control"
    
        def description(self):
            return "This register is used to configure MAC at the reveiver direction"
            
        def width(self):
            return 12
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00001042
            
        def endAddress(self):
            return 0xffffffff

        class _Eth10gMACRxIpgCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Eth10gMACRxIpgCtrl"
            
            def description(self):
                return "RX inter packet gap"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Eth10gMACRxIpgCtrl"] = _AF6CCI0011_RD_eXAUI._eth10g_mac_rx_ctr._Eth10gMACRxIpgCtrl()
            return allFields

    class _eth10g_mac_tx_ctr(AtRegister.AtRegister):
        def name(self):
            return "ETH 10G MAC Transmit Control"
    
        def description(self):
            return "This register is used to configure MAC at the reveiver direction"
            
        def width(self):
            return 13
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00001043
            
        def endAddress(self):
            return 0xffffffff

        class _Eth10gMACTxIpgCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Eth10gMACTxIpgCtrl"
            
            def description(self):
                return "TX inter packet gap"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Eth10gMACTxIpgCtrl"] = _AF6CCI0011_RD_eXAUI._eth10g_mac_tx_ctr._Eth10gMACTxIpgCtrl()
            return allFields

    class _TxEth_cnt0_64_ro(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count0_64 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 0 to 64 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0002000 + eth_port"
            
        def startAddress(self):
            return 0x00002000
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCnt0_64(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCnt0_64"
            
            def description(self):
                return "This is statistic counter for the packet having 0 to 64 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCnt0_64"] = _AF6CCI0011_RD_eXAUI._TxEth_cnt0_64_ro._TxEthCnt0_64()
            return allFields

    class _TxEth_cnt0_64_rc(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count0_64 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 0 to 64 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0002800 + eth_port"
            
        def startAddress(self):
            return 0x00002800
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCnt0_64(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCnt0_64"
            
            def description(self):
                return "This is statistic counter for the packet having 0 to 64 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCnt0_64"] = _AF6CCI0011_RD_eXAUI._TxEth_cnt0_64_rc._TxEthCnt0_64()
            return allFields

    class _TxEth_cnt65_127_ro(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count65_127 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 65 to 127 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0002002 + eth_port"
            
        def startAddress(self):
            return 0x00002002
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCnt65_127(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCnt65_127"
            
            def description(self):
                return "This is statistic counter for the packet having 65 to 127 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCnt65_127"] = _AF6CCI0011_RD_eXAUI._TxEth_cnt65_127_ro._TxEthCnt65_127()
            return allFields

    class _TxEth_cnt65_127_rc(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count65_127 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 65 to 127 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0002802 + eth_port"
            
        def startAddress(self):
            return 0x00002802
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCnt65_127(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCnt65_127"
            
            def description(self):
                return "This is statistic counter for the packet having 65 to 127 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCnt65_127"] = _AF6CCI0011_RD_eXAUI._TxEth_cnt65_127_rc._TxEthCnt65_127()
            return allFields

    class _TxEth_cnt128_255_ro(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count128_255 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 128 to 255 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0002004 + eth_port"
            
        def startAddress(self):
            return 0x00002004
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCnt128_255(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCnt128_255"
            
            def description(self):
                return "This is statistic counter for the packet having 128 to 255 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCnt128_255"] = _AF6CCI0011_RD_eXAUI._TxEth_cnt128_255_ro._TxEthCnt128_255()
            return allFields

    class _TxEth_cnt128_255_rc(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count128_255 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 128 to 255 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0002804 + eth_port"
            
        def startAddress(self):
            return 0x00002804
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCnt128_255(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCnt128_255"
            
            def description(self):
                return "This is statistic counter for the packet having 128 to 255 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCnt128_255"] = _AF6CCI0011_RD_eXAUI._TxEth_cnt128_255_rc._TxEthCnt128_255()
            return allFields

    class _TxEth_cnt256_511_ro(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count256_511 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 256 to 511 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0002006 + eth_port"
            
        def startAddress(self):
            return 0x00002006
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCnt256_511(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCnt256_511"
            
            def description(self):
                return "This is statistic counter for the packet having 256 to 511 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCnt256_511"] = _AF6CCI0011_RD_eXAUI._TxEth_cnt256_511_ro._TxEthCnt256_511()
            return allFields

    class _TxEth_cnt256_511_rc(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count256_511 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 256 to 511 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0002806 + eth_port"
            
        def startAddress(self):
            return 0x00002806
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCnt256_511(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCnt256_511"
            
            def description(self):
                return "This is statistic counter for the packet having 256 to 511 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCnt256_511"] = _AF6CCI0011_RD_eXAUI._TxEth_cnt256_511_rc._TxEthCnt256_511()
            return allFields

    class _TxEth_cnt512_1024_ro(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count512_1023 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 512 to 1023 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0002008 + eth_port"
            
        def startAddress(self):
            return 0x00002008
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCnt512_1024(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCnt512_1024"
            
            def description(self):
                return "This is statistic counter for the packet having 512 to 1023 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCnt512_1024"] = _AF6CCI0011_RD_eXAUI._TxEth_cnt512_1024_ro._TxEthCnt512_1024()
            return allFields

    class _TxEth_cnt512_1024_rc(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count512_1023 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 512 to 1023 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0002808 + eth_port"
            
        def startAddress(self):
            return 0x00002808
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCnt512_1024(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCnt512_1024"
            
            def description(self):
                return "This is statistic counter for the packet having 512 to 1023 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCnt512_1024"] = _AF6CCI0011_RD_eXAUI._TxEth_cnt512_1024_rc._TxEthCnt512_1024()
            return allFields

    class _Eth_cnt1025_1528_ro(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count1024_1518 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 1024 to 1518 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000200A + eth_port"
            
        def startAddress(self):
            return 0x0000200a
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCnt1025_1528(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCnt1025_1528"
            
            def description(self):
                return "This is statistic counter for the packet having 1024 to 1518 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCnt1025_1528"] = _AF6CCI0011_RD_eXAUI._Eth_cnt1025_1528_ro._TxEthCnt1025_1528()
            return allFields

    class _Eth_cnt1025_1528_rc(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count1024_1518 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 1024 to 1518 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000280A + eth_port"
            
        def startAddress(self):
            return 0x0000280a
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCnt1025_1528(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCnt1025_1528"
            
            def description(self):
                return "This is statistic counter for the packet having 1024 to 1518 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCnt1025_1528"] = _AF6CCI0011_RD_eXAUI._Eth_cnt1025_1528_rc._TxEthCnt1025_1528()
            return allFields

    class _TxEth_cnt1529_2047_ro(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count1519_2047 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 1519 to 2047 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000200C + eth_port"
            
        def startAddress(self):
            return 0x0000200c
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCnt1529_2047(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCnt1529_2047"
            
            def description(self):
                return "This is statistic counter for the packet having 1519 to 2047 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCnt1529_2047"] = _AF6CCI0011_RD_eXAUI._TxEth_cnt1529_2047_ro._TxEthCnt1529_2047()
            return allFields

    class _TxEth_cnt1529_2047_rc(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count1519_2047 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 1519 to 2047 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000280C + eth_port"
            
        def startAddress(self):
            return 0x0000280c
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCnt1529_2047(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCnt1529_2047"
            
            def description(self):
                return "This is statistic counter for the packet having 1519 to 2047 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCnt1529_2047"] = _AF6CCI0011_RD_eXAUI._TxEth_cnt1529_2047_rc._TxEthCnt1529_2047()
            return allFields

    class _TxEth_cnt_jumbo_ro(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count Jumbo packet"
    
        def description(self):
            return "This register is statistic counter for the packet having more than 2048 bytes (jumbo)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000200E + eth_port"
            
        def startAddress(self):
            return 0x0000200e
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCntJumbo(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCntJumbo"
            
            def description(self):
                return "This is statistic counter for the packet more than 2048 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCntJumbo"] = _AF6CCI0011_RD_eXAUI._TxEth_cnt_jumbo_ro._TxEthCntJumbo()
            return allFields

    class _TxEth_cnt_jumbo_rc(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count Jumbo packet"
    
        def description(self):
            return "This register is statistic counter for the packet having more than 2048 bytes (jumbo)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000280E + eth_port"
            
        def startAddress(self):
            return 0x0000280e
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCntJumbo(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCntJumbo"
            
            def description(self):
                return "This is statistic counter for the packet more than 2048 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCntJumbo"] = _AF6CCI0011_RD_eXAUI._TxEth_cnt_jumbo_rc._TxEthCntJumbo()
            return allFields

    class _eth_tx_pkt_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count Total packet"
    
        def description(self):
            return "This register is statistic counter for the total packet at Transmit side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0002012 + eth_port"
            
        def startAddress(self):
            return 0x00002012
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCntTotal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCntTotal"
            
            def description(self):
                return "This is statistic counter total packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCntTotal"] = _AF6CCI0011_RD_eXAUI._eth_tx_pkt_cnt_ro._TxEthCntTotal()
            return allFields

    class _eth_tx_pkt_cnt_rc(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count Total packet"
    
        def description(self):
            return "This register is statistic counter for the total packet at Transmit side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0002812 + eth_port"
            
        def startAddress(self):
            return 0x00002812
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCntTotal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCntTotal"
            
            def description(self):
                return "This is statistic counter total packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCntTotal"] = _AF6CCI0011_RD_eXAUI._eth_tx_pkt_cnt_rc._TxEthCntTotal()
            return allFields

    class _TxEth_cnt_byte_ro(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count number of bytes of packet"
    
        def description(self):
            return "This register is statistic count number of bytes of packet at Transmit side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000201E + eth_port"
            
        def startAddress(self):
            return 0x0000201e
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCntByte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCntByte"
            
            def description(self):
                return "This is statistic counter number of bytes of packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCntByte"] = _AF6CCI0011_RD_eXAUI._TxEth_cnt_byte_ro._TxEthCntByte()
            return allFields

    class _TxEth_cnt_byte_rc(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count number of bytes of packet"
    
        def description(self):
            return "This register is statistic count number of bytes of packet at Transmit side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000281E + eth_port"
            
        def startAddress(self):
            return 0x0000281e
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCntByte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCntByte"
            
            def description(self):
                return "This is statistic counter number of bytes of packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCntByte"] = _AF6CCI0011_RD_eXAUI._TxEth_cnt_byte_rc._TxEthCntByte()
            return allFields

    class _RxEth_cnt0_64_ro(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count0_64 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 0 to 64 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00003000 + eth_port"
            
        def startAddress(self):
            return 0x00003000
            
        def endAddress(self):
            return 0xffffffff

        class _RxEthCnt0_64(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthCnt0_64"
            
            def description(self):
                return "This is statistic counter for the packet having 0 to 64 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthCnt0_64"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt0_64_ro._RxEthCnt0_64()
            return allFields

    class _RxEth_cnt0_64_rc(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count0_64 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 0 to 64 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00003800 + eth_port"
            
        def startAddress(self):
            return 0x00003800
            
        def endAddress(self):
            return 0xffffffff

        class _RxEthCnt0_64(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthCnt0_64"
            
            def description(self):
                return "This is statistic counter for the packet having 0 to 64 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthCnt0_64"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt0_64_rc._RxEthCnt0_64()
            return allFields

    class _RxEth_cnt65_127_ro(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count65_127 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 65 to 127 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00003004 + eth_port"
            
        def startAddress(self):
            return 0x00003004
            
        def endAddress(self):
            return 0xffffffff

        class _RxEthCnt65_127(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthCnt65_127"
            
            def description(self):
                return "This is statistic counter for the packet having 65 to 127 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthCnt65_127"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt65_127_ro._RxEthCnt65_127()
            return allFields

    class _RxEth_cnt65_127_rc(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count65_127 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 65 to 127 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00003804 + eth_port"
            
        def startAddress(self):
            return 0x00003804
            
        def endAddress(self):
            return 0xffffffff

        class _RxEthCnt65_127(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthCnt65_127"
            
            def description(self):
                return "This is statistic counter for the packet having 65 to 127 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthCnt65_127"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt65_127_rc._RxEthCnt65_127()
            return allFields

    class _RxEth_cnt128_255_ro(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count128_255 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 128 to 255 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00003008 + eth_port"
            
        def startAddress(self):
            return 0x00003008
            
        def endAddress(self):
            return 0xffffffff

        class _RxEthCnt128_255(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthCnt128_255"
            
            def description(self):
                return "This is statistic counter for the packet having 128 to 255 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthCnt128_255"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt128_255_ro._RxEthCnt128_255()
            return allFields

    class _RxEth_cnt128_255_rc(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count128_255 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 128 to 255 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00003808 + eth_port"
            
        def startAddress(self):
            return 0x00003808
            
        def endAddress(self):
            return 0xffffffff

        class _RxEthCnt128_255(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthCnt128_255"
            
            def description(self):
                return "This is statistic counter for the packet having 128 to 255 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthCnt128_255"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt128_255_rc._RxEthCnt128_255()
            return allFields

    class _RxEth_cnt256_511_ro(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count256_511 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 256 to 511 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0000300C + eth_port"
            
        def startAddress(self):
            return 0x0000300c
            
        def endAddress(self):
            return 0xffffffff

        class _RxEthCnt256_511(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthCnt256_511"
            
            def description(self):
                return "This is statistic counter for the packet having 256 to 511 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthCnt256_511"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt256_511_ro._RxEthCnt256_511()
            return allFields

    class _RxEth_cnt256_511_rc(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count256_511 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 256 to 511 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0000380C + eth_port"
            
        def startAddress(self):
            return 0x0000380c
            
        def endAddress(self):
            return 0xffffffff

        class _RxEthCnt256_511(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthCnt256_511"
            
            def description(self):
                return "This is statistic counter for the packet having 256 to 511 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthCnt256_511"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt256_511_rc._RxEthCnt256_511()
            return allFields

    class _RxEth_cnt512_1024_ro(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count512_1024 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 512 to 1023 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00003010 + eth_port"
            
        def startAddress(self):
            return 0x00003010
            
        def endAddress(self):
            return 0xffffffff

        class _RxEthCnt512_1024(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthCnt512_1024"
            
            def description(self):
                return "This is statistic counter for the packet having 512 to 1023 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthCnt512_1024"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt512_1024_ro._RxEthCnt512_1024()
            return allFields

    class _RxEth_cnt512_1024_rc(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count512_1024 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 512 to 1023 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00003810 + eth_port"
            
        def startAddress(self):
            return 0x00003810
            
        def endAddress(self):
            return 0xffffffff

        class _RxEthCnt512_1024(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthCnt512_1024"
            
            def description(self):
                return "This is statistic counter for the packet having 512 to 1023 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthCnt512_1024"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt512_1024_rc._RxEthCnt512_1024()
            return allFields

    class _RxEth_cnt1025_1528_ro(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count1025_1528 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 1024 to 1518 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00003014 + eth_port"
            
        def startAddress(self):
            return 0x00003014
            
        def endAddress(self):
            return 0xffffffff

        class _RxEthCnt1025_1528(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthCnt1025_1528"
            
            def description(self):
                return "This is statistic counter for the packet having 1024 to 1518 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthCnt1025_1528"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt1025_1528_ro._RxEthCnt1025_1528()
            return allFields

    class _RxEth_cnt1025_1528_rc(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count1025_1528 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 1024 to 1518 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00003814 + eth_port"
            
        def startAddress(self):
            return 0x00003814
            
        def endAddress(self):
            return 0xffffffff

        class _RxEthCnt1025_1528(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthCnt1025_1528"
            
            def description(self):
                return "This is statistic counter for the packet having 1024 to 1518 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthCnt1025_1528"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt1025_1528_rc._RxEthCnt1025_1528()
            return allFields

    class _RxEth_cnt1529_2047_ro(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count1529_2047 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 1519 to 2047 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00003018 + eth_port"
            
        def startAddress(self):
            return 0x00003018
            
        def endAddress(self):
            return 0xffffffff

        class _RxEthCnt1529_2047(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthCnt1529_2047"
            
            def description(self):
                return "This is statistic counter for the packet having 1519 to 2047 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthCnt1529_2047"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt1529_2047_ro._RxEthCnt1529_2047()
            return allFields

    class _RxEth_cnt1529_2047_rc(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count1529_2047 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 1519 to 2047 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00003818 + eth_port"
            
        def startAddress(self):
            return 0x00003818
            
        def endAddress(self):
            return 0xffffffff

        class _RxEthCnt1529_2047(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthCnt1529_2047"
            
            def description(self):
                return "This is statistic counter for the packet having 1519 to 2047 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthCnt1529_2047"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt1529_2047_rc._RxEthCnt1529_2047()
            return allFields

    class _Eth_cnt_jumbo_ro(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count Jumbo packet"
    
        def description(self):
            return "This register is statistic counter for the packet having more than 2048 bytes (jumbo)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0000301C + eth_port"
            
        def startAddress(self):
            return 0x0000301c
            
        def endAddress(self):
            return 0xffffffff

        class _RxEthCntJumbo(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthCntJumbo"
            
            def description(self):
                return "This is statistic counter for the packet more than 2048 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthCntJumbo"] = _AF6CCI0011_RD_eXAUI._Eth_cnt_jumbo_ro._RxEthCntJumbo()
            return allFields

    class _Eth_cnt_jumbo_rc(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count Jumbo packet"
    
        def description(self):
            return "This register is statistic counter for the packet having more than 2048 bytes (jumbo)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0000381C + eth_port"
            
        def startAddress(self):
            return 0x0000381c
            
        def endAddress(self):
            return 0xffffffff

        class _RxEthCntJumbo(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthCntJumbo"
            
            def description(self):
                return "This is statistic counter for the packet more than 2048 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthCntJumbo"] = _AF6CCI0011_RD_eXAUI._Eth_cnt_jumbo_rc._RxEthCntJumbo()
            return allFields

    class _rx_port_pkt_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count Total packet"
    
        def description(self):
            return "This register is statistic counter for the total packet at receive side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00003024 + eth_port"
            
        def startAddress(self):
            return 0x00003024
            
        def endAddress(self):
            return 0xffffffff

        class _RxEthCntTotal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthCntTotal"
            
            def description(self):
                return "This is statistic counter total packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthCntTotal"] = _AF6CCI0011_RD_eXAUI._rx_port_pkt_cnt_ro._RxEthCntTotal()
            return allFields

    class _rx_port_pkt_cnt_rc(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count Total packet"
    
        def description(self):
            return "This register is statistic counter for the total packet at receive side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00003824 + eth_port"
            
        def startAddress(self):
            return 0x00003824
            
        def endAddress(self):
            return 0xffffffff

        class _RxEthCntTotal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthCntTotal"
            
            def description(self):
                return "This is statistic counter total packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthCntTotal"] = _AF6CCI0011_RD_eXAUI._rx_port_pkt_cnt_rc._RxEthCntTotal()
            return allFields

    class _rx_port_under_size_pkt_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count Under size packet"
    
        def description(self):
            return "This register is statistic counter for the under size packet at receive side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00003030 + eth_port"
            
        def startAddress(self):
            return 0x00003030
            
        def endAddress(self):
            return 0xffffffff

        class _RxEthCntUnderSize(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthCntUnderSize"
            
            def description(self):
                return "This is statistic counter under size packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthCntUnderSize"] = _AF6CCI0011_RD_eXAUI._rx_port_under_size_pkt_cnt_ro._RxEthCntUnderSize()
            return allFields

    class _rx_port_under_size_pkt_cnt_rc(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count Under size packet"
    
        def description(self):
            return "This register is statistic counter for the under size packet at receive side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00003830 + eth_port"
            
        def startAddress(self):
            return 0x00003830
            
        def endAddress(self):
            return 0xffffffff

        class _RxEthCntUnderSize(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthCntUnderSize"
            
            def description(self):
                return "This is statistic counter under size packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthCntUnderSize"] = _AF6CCI0011_RD_eXAUI._rx_port_under_size_pkt_cnt_rc._RxEthCntUnderSize()
            return allFields

    class _rx_port_over_size_pkt_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count Over size packet"
    
        def description(self):
            return "This register is statistic counter for the over size packet at receive side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00003034 + eth_port"
            
        def startAddress(self):
            return 0x00003034
            
        def endAddress(self):
            return 0xffffffff

        class _RxEthCntOverSize(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthCntOverSize"
            
            def description(self):
                return "This is statistic counter over size packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthCntOverSize"] = _AF6CCI0011_RD_eXAUI._rx_port_over_size_pkt_cnt_ro._RxEthCntOverSize()
            return allFields

    class _rx_port_over_size_pkt_cnt_rc(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count Over size packet"
    
        def description(self):
            return "This register is statistic counter for the over size packet at receive side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00003834 + eth_port"
            
        def startAddress(self):
            return 0x00003834
            
        def endAddress(self):
            return 0xffffffff

        class _RxEthCntOverSize(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthCntOverSize"
            
            def description(self):
                return "This is statistic counter over size packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthCntOverSize"] = _AF6CCI0011_RD_eXAUI._rx_port_over_size_pkt_cnt_rc._RxEthCntOverSize()
            return allFields

    class _RxEth_cnt_phy_err_ro(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count FCS error packet"
    
        def description(self):
            return "This register is statistic count FCS error packet at receive side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00003038 + eth_port"
            
        def startAddress(self):
            return 0x00003038
            
        def endAddress(self):
            return 0xffffffff

        class _RxEthCntPhyErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthCntPhyErr"
            
            def description(self):
                return "This is statistic counter FCS error packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthCntPhyErr"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt_phy_err_ro._RxEthCntPhyErr()
            return allFields

    class _RxEth_cnt_phy_err_rc(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count FCS error packet"
    
        def description(self):
            return "This register is statistic count FCS error packet at receive side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00003838 + eth_port"
            
        def startAddress(self):
            return 0x00003838
            
        def endAddress(self):
            return 0xffffffff

        class _RxEthCntPhyErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthCntPhyErr"
            
            def description(self):
                return "This is statistic counter FCS error packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthCntPhyErr"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt_phy_err_rc._RxEthCntPhyErr()
            return allFields

    class _RxEth_cnt_byte_ro(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count number of bytes of packet"
    
        def description(self):
            return "This register is statistic count number of bytes of packet at receive side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0000303C + eth_port"
            
        def startAddress(self):
            return 0x0000303c
            
        def endAddress(self):
            return 0xffffffff

        class _RxEthCntByte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthCntByte"
            
            def description(self):
                return "This is statistic counter number of bytes of packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthCntByte"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt_byte_ro._RxEthCntByte()
            return allFields

    class _RxEth_cnt_byte_rc(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count number of bytes of packet"
    
        def description(self):
            return "This register is statistic count number of bytes of packet at receive side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0000383C + eth_port"
            
        def startAddress(self):
            return 0x0000383c
            
        def endAddress(self):
            return 0xffffffff

        class _RxEthCntByte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthCntByte"
            
            def description(self):
                return "This is statistic counter number of bytes of packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthCntByte"] = _AF6CCI0011_RD_eXAUI._RxEth_cnt_byte_rc._RxEthCntByte()
            return allFields

    class _eth10g_diag_ctr(AtRegister.AtRegister):
        def name(self):
            return "ETH 10G Dignostic Control"
    
        def description(self):
            return "This register is used to config dignostic"
            
        def width(self):
            return 13
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00001060
            
        def endAddress(self):
            return 0xffffffff

        class _Eth10GDiagForceErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Eth10GDiagForceErr"
            
            def description(self):
                return "Dignostic force error"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Eth10GDiagTxSrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Eth10GDiagTxSrc"
            
            def description(self):
                return "Dignostic select source to monitor 1: TX diag 0: tx user"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Eth10GDiagPktLenMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Eth10GDiagPktLenMode"
            
            def description(self):
                return "packet length mode 0: fix length (use configured min length) 1: increase 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Eth10GDiagMonSrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Eth10GDiagMonSrc"
            
            def description(self):
                return "Dignostic select source to monitor 1: TX user 0: RX MAC"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Eth10GDiagGenEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Eth10GDiagGenEn"
            
            def description(self):
                return "Dignostic enable to generate packets 1: enable 0: disable"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Eth10GDiagForceErr"] = _AF6CCI0011_RD_eXAUI._eth10g_diag_ctr._Eth10GDiagForceErr()
            allFields["Eth10GDiagTxSrc"] = _AF6CCI0011_RD_eXAUI._eth10g_diag_ctr._Eth10GDiagTxSrc()
            allFields["Eth10GDiagPktLenMode"] = _AF6CCI0011_RD_eXAUI._eth10g_diag_ctr._Eth10GDiagPktLenMode()
            allFields["Eth10GDiagMonSrc"] = _AF6CCI0011_RD_eXAUI._eth10g_diag_ctr._Eth10GDiagMonSrc()
            allFields["Eth10GDiagGenEn"] = _AF6CCI0011_RD_eXAUI._eth10g_diag_ctr._Eth10GDiagGenEn()
            return allFields

    class _eth10g_diag_max_len_ctr(AtRegister.AtRegister):
        def name(self):
            return "ETH 10G Dignostic Max Length Control"
    
        def description(self):
            return "This register is used to config max packet length in case of increased length dignostic packet"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00001061
            
        def endAddress(self):
            return 0xffffffff

        class _Eth10GDiagMaxLen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Eth10GDiagMaxLen"
            
            def description(self):
                return "Dignostic max length of packets"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Eth10GDiagMaxLen"] = _AF6CCI0011_RD_eXAUI._eth10g_diag_max_len_ctr._Eth10GDiagMaxLen()
            return allFields

    class _eth10g_diag_min_len_ctr(AtRegister.AtRegister):
        def name(self):
            return "ETH 10G Dignostic Min Length Control"
    
        def description(self):
            return "This register is used to config min packet length in case of increased length dignostic packet or length packet in fix length mode"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00001062
            
        def endAddress(self):
            return 0xffffffff

        class _Eth10GDiagMinLen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Eth10GDiagMinLen"
            
            def description(self):
                return "Dignostic min length of packets"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Eth10GDiagMinLen"] = _AF6CCI0011_RD_eXAUI._eth10g_diag_min_len_ctr._Eth10GDiagMinLen()
            return allFields

    class _eth10g_diag_num_pkt_ctr(AtRegister.AtRegister):
        def name(self):
            return "ETH 10G Dignostic number packet Control"
    
        def description(self):
            return "This register is used to config number of packet generated"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00001064
            
        def endAddress(self):
            return 0xffffffff

        class _Eth10GDiagNumPkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Eth10GDiagNumPkt"
            
            def description(self):
                return "Dignostic number of generated packet, value of 16'hFFFF for continuous generating"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Eth10GDiagNumPkt"] = _AF6CCI0011_RD_eXAUI._eth10g_diag_num_pkt_ctr._Eth10GDiagNumPkt()
            return allFields

    class _eth10g_diag_err_stk(AtRegister.AtRegister):
        def name(self):
            return "ETH 10G Dignostic Error Sticky"
    
        def description(self):
            return "This register is used to sticky error for dignostic"
            
        def width(self):
            return 11
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00001048
            
        def endAddress(self):
            return 0xffffffff

        class _Eth10GDiagErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "Eth10GDiagErrStk"
            
            def description(self):
                return "Dignostic error sticky"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Eth10GDiagErrStk"] = _AF6CCI0011_RD_eXAUI._eth10g_diag_err_stk._Eth10GDiagErrStk()
            return allFields

    class _diag_rx_pkt_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Dignostic Receive Ethernet port Count Total packet"
    
        def description(self):
            return "This register is statistic counter for the total packet at receive dignostic"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00004024 + eth_port"
            
        def startAddress(self):
            return 0x00004024
            
        def endAddress(self):
            return 0xffffffff

        class _DiagRxPktCntTotal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DiagRxPktCntTotal"
            
            def description(self):
                return "This is statistic counter total rx packet for diagnotic"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DiagRxPktCntTotal"] = _AF6CCI0011_RD_eXAUI._diag_rx_pkt_cnt_ro._DiagRxPktCntTotal()
            return allFields

    class _diag_rx_pkt_cnt_rc(AtRegister.AtRegister):
        def name(self):
            return "Dignostic Receive Ethernet port Count Total packet"
    
        def description(self):
            return "This register is statistic counter for the total packet at receive dignostic"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00004824 + eth_port"
            
        def startAddress(self):
            return 0x00004824
            
        def endAddress(self):
            return 0xffffffff

        class _DiagRxPktCntTotal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DiagRxPktCntTotal"
            
            def description(self):
                return "This is statistic counter total rx packet for diagnotic"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DiagRxPktCntTotal"] = _AF6CCI0011_RD_eXAUI._diag_rx_pkt_cnt_rc._DiagRxPktCntTotal()
            return allFields

    class _diag_rx_cnt_byte_ro(AtRegister.AtRegister):
        def name(self):
            return "Dignotic Receive Ethernet port Count number of bytes of packet"
    
        def description(self):
            return "This register is statistic count number of bytes of rx packet for dignostic"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0000403C + eth_port"
            
        def startAddress(self):
            return 0x0000403c
            
        def endAddress(self):
            return 0xffffffff

        class _DiagRxRxCntByte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DiagRxRxCntByte"
            
            def description(self):
                return "This is statistic counter number of bytes of rx packet for diagnostic"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DiagRxRxCntByte"] = _AF6CCI0011_RD_eXAUI._diag_rx_cnt_byte_ro._DiagRxRxCntByte()
            return allFields

    class _diag_rx_cnt_byte_rc(AtRegister.AtRegister):
        def name(self):
            return "Dignotic Receive Ethernet port Count number of bytes of packet"
    
        def description(self):
            return "This register is statistic count number of bytes of rx packet for dignostic"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0000483C + eth_port"
            
        def startAddress(self):
            return 0x0000483c
            
        def endAddress(self):
            return 0xffffffff

        class _DiagRxRxCntByte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DiagRxRxCntByte"
            
            def description(self):
                return "This is statistic counter number of bytes of rx packet for diagnostic"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DiagRxRxCntByte"] = _AF6CCI0011_RD_eXAUI._diag_rx_cnt_byte_rc._DiagRxRxCntByte()
            return allFields

    class _diag_gen_bw_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Dignotic genereate bandwidth control"
    
        def description(self):
            return "This register is used to create bandwidth for generating diag packets"
            
        def width(self):
            return 3
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x1002 + eth_port"
            
        def startAddress(self):
            return 0x00001002
            
        def endAddress(self):
            return 0xffffffff

        class _DiagGenBwCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DiagGenBwCtrl"
            
            def description(self):
                return "bandwidth for generating diag packet, unit is 1.2G"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DiagGenBwCtrl"] = _AF6CCI0011_RD_eXAUI._diag_gen_bw_ctrl._DiagGenBwCtrl()
            return allFields

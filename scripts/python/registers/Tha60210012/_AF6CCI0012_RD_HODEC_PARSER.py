import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0012_RD_HODEC_PARSER(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["upen_hdlc_Hocfg"] = _AF6CCI0012_RD_HODEC_PARSER._upen_hdlc_Hocfg()
        allRegisters["upen_hdlc_hoglbcfg"] = _AF6CCI0012_RD_HODEC_PARSER._upen_hdlc_hoglbcfg()
        allRegisters["upen_hdlc_cfgid"] = _AF6CCI0012_RD_HODEC_PARSER._upen_hdlc_cfgid()
        allRegisters["upen_gpkt_cnt"] = _AF6CCI0012_RD_HODEC_PARSER._upen_gpkt_cnt()
        allRegisters["upen_drpkt_cnt"] = _AF6CCI0012_RD_HODEC_PARSER._upen_drpkt_cnt()
        allRegisters["upen_abpkt_cnt"] = _AF6CCI0012_RD_HODEC_PARSER._upen_abpkt_cnt()
        allRegisters["upen_ppppid"] = _AF6CCI0012_RD_HODEC_PARSER._upen_ppppid()
        allRegisters["upen_ethtyp"] = _AF6CCI0012_RD_HODEC_PARSER._upen_ethtyp()
        allRegisters["upen_mactyp"] = _AF6CCI0012_RD_HODEC_PARSER._upen_mactyp()
        allRegisters["upen_version"] = _AF6CCI0012_RD_HODEC_PARSER._upen_version()
        allRegisters["upen_cidcfg"] = _AF6CCI0012_RD_HODEC_PARSER._upen_cidcfg()
        allRegisters["upen_cidcnt"] = _AF6CCI0012_RD_HODEC_PARSER._upen_cidcnt()
        return allRegisters

    class _upen_hdlc_Hocfg(AtRegister.AtRegister):
        def name(self):
            return "CONFIG HDLC HO DEC"
    
        def description(self):
            return "config message MDL BUFFER Channel ID 0-15 HDL_PATH: iaf6cci0012_hodec_core.ihdlc_cfg.imem113x.ram.ram[$STSID]"
            
        def width(self):
            return 3
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x800+ $STSID"
            
        def startAddress(self):
            return 0x00000800
            
        def endAddress(self):
            return 0x0000087f

        class _cfg_scren(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "cfg_scren"
            
            def description(self):
                return "config to enable scramble, (1) is enable, (0) is disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_fcsmsb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfg_fcsmsb"
            
            def description(self):
                return "config to calculate FCS from MSB or LSB, (1) is MSB, (0) is LSB"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_fcsmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_fcsmode"
            
            def description(self):
                return "config to calculate FCS 32 or FCS 16, (1) is FCS 32, (0) is FCS 16"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_scren"] = _AF6CCI0012_RD_HODEC_PARSER._upen_hdlc_Hocfg._cfg_scren()
            allFields["cfg_fcsmsb"] = _AF6CCI0012_RD_HODEC_PARSER._upen_hdlc_Hocfg._cfg_fcsmsb()
            allFields["cfg_fcsmode"] = _AF6CCI0012_RD_HODEC_PARSER._upen_hdlc_Hocfg._cfg_fcsmode()
            return allFields

    class _upen_hdlc_hoglbcfg(AtRegister.AtRegister):
        def name(self):
            return "CONFIG HDLC HO DEC"
    
        def description(self):
            return ""
            
        def width(self):
            return 5
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000884
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_loop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "cfg_loop"
            
            def description(self):
                return "config loopback ENC -DEC, receive directly data from ENC"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_alrmrstint(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfg_alrmrstint"
            
            def description(self):
                return "config reset sticky, (1) is reset, (0) is normal"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_upactive(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_upactive"
            
            def description(self):
                return "config active from CPU (1) is active, (0) is disable active"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_loop"] = _AF6CCI0012_RD_HODEC_PARSER._upen_hdlc_hoglbcfg._cfg_loop()
            allFields["cfg_alrmrstint"] = _AF6CCI0012_RD_HODEC_PARSER._upen_hdlc_hoglbcfg._cfg_alrmrstint()
            allFields["cfg_upactive"] = _AF6CCI0012_RD_HODEC_PARSER._upen_hdlc_hoglbcfg._cfg_upactive()
            return allFields

    class _upen_hdlc_cfgid(AtRegister.AtRegister):
        def name(self):
            return "CONFIG HDLC ID CNT"
    
        def description(self):
            return "config HDLC id cnt"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000888
            
        def endAddress(self):
            return 0xffffffff

        class _upreset(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "upreset"
            
            def description(self):
                return "reset counter, (1) is reset, (0) is normal"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 10
        
            def name(self):
                return "reserve"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfgid"
            
            def description(self):
                return "config ID to count"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["upreset"] = _AF6CCI0012_RD_HODEC_PARSER._upen_hdlc_cfgid._upreset()
            allFields["reserve"] = _AF6CCI0012_RD_HODEC_PARSER._upen_hdlc_cfgid._reserve()
            allFields["cfgid"] = _AF6CCI0012_RD_HODEC_PARSER._upen_hdlc_cfgid._cfgid()
            return allFields

    class _upen_gpkt_cnt(AtRegister.AtRegister):
        def name(self):
            return "COUNTER HDLC GOOD PACKET LO DEC"
    
        def description(self):
            return "counter good packet"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x900+ $CID + $RO * 0x30"
            
        def startAddress(self):
            return 0x00000900
            
        def endAddress(self):
            return 0x000009ff

        class _cnt_gpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_gpkt"
            
            def description(self):
                return "vlaue of counter good packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_gpkt"] = _AF6CCI0012_RD_HODEC_PARSER._upen_gpkt_cnt._cnt_gpkt()
            return allFields

    class _upen_drpkt_cnt(AtRegister.AtRegister):
        def name(self):
            return "COUNTER HDLC DROP PACKET LO DEC"
    
        def description(self):
            return "counter drop packet"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xA00+ $CID + $RO * 0x30"
            
        def startAddress(self):
            return 0x00000a00
            
        def endAddress(self):
            return 0x00000aff

        class _cnt_drpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_drpkt"
            
            def description(self):
                return "vlaue of counter drop packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_drpkt"] = _AF6CCI0012_RD_HODEC_PARSER._upen_drpkt_cnt._cnt_drpkt()
            return allFields

    class _upen_abpkt_cnt(AtRegister.AtRegister):
        def name(self):
            return "COUNTER HDLC ABORT PACKET LO DEC"
    
        def description(self):
            return "counter abort packet"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB00+ $CID + $RO * 0x30"
            
        def startAddress(self):
            return 0x00000b00
            
        def endAddress(self):
            return 0x00000bff

        class _cnt_abpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_abpkt"
            
            def description(self):
                return "vlaue of counter abort packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_abpkt"] = _AF6CCI0012_RD_HODEC_PARSER._upen_abpkt_cnt._cnt_abpkt()
            return allFields

    class _upen_ppppid(AtRegister.AtRegister):
        def name(self):
            return "PPP PID  global config"
    
        def description(self):
            return "Use to configure protocol id for PPP_TYPE Reset_value of ppp_type ppp_type == 0: 0x0021, is Ipv4 packets ppp_type == 1: 0x0057, is Ipv6 packets ppp_type == 2: 0x0281, is MPLS unicast packets ppp_type == 3: 0x0283, is MPLS multicast packets ppp_type == 4: 0x0023, is IS-IS packets ppp_type == 5: 0x0029, is Appletalks ppp_type == 6: 0x002B, is Novell IPX ppp_type == 7: 0x0025, is Xerox NS IDP ppp_type == 8: 0x0041, is Cisco Systems ppp_type == 9: 0x0207, is Cisco Discovery Protocol ppp_type == 10: 0x003F, is NETBIOS Framing ppp_type == 11: 0x004F, is IPv6 header Compression ppp_type == 12: 0x0201, is 802.1d Hello Packet ppp_type == 13: 0x0203, is IBM Source Routing BPDU ppp_type == 14: 0x0205, is DEC LANBridge Spaning Tree ppp_type == 15: 0x0031, is ETH/BCP HDL_PATH: begin: IF($ppptyp==0) ihoparser_core.ippppid0_cfg.xxxfpcfg ELSEIF($ppptyp==1) ihoparser_core.ippppid1_cfg.xxxfpcfg ELSEIF($ppptyp==2) ihoparser_core.ippppid2_cfg.xxxfpcfg ELSEIF($ppptyp==3) ihoparser_core.ippppid3_cfg.xxxfpcfg ELSEIF($ppptyp==4) ihoparser_core.ippppid4_cfg.xxxfpcfg ELSEIF($ppptyp==5) ihoparser_core.ippppid5_cfg.xxxfpcfg ELSEIF($ppptyp==6) ihoparser_core.ippppid6_cfg.xxxfpcfg ELSEIF($ppptyp==7) ihoparser_core.ippppid7_cfg.xxxfpcfg ELSEIF($ppptyp==8) ihoparser_core.ippppid8_cfg.xxxfpcfg ELSEIF($ppptyp==9) ihoparser_core.ippppid9_cfg.xxxfpcfg ELSEIF($ppptyp==10) ihoparser_core.ippppid10_cfg.xxxfpcfg ELSEIF($ppptyp==11) ihoparser_core.ippppid11_cfg.xxxfpcfg ELSEIF($ppptyp==12) ihoparser_core.ippppid12_cfg.xxxfpcfg ELSEIF($ppptyp==13) ihoparser_core.ippppid13_cfg.xxxfpcfg ELSEIF($ppptyp==14) ihoparser_core.ippppid14_cfg.xxxfpcfg ELSE ihoparser_core.ippppid15_cfg.xxxfpcfg HDL_PATH: end:"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0_0000 + $ppptyp"
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0x0000000f

        class _ppppid_mask(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ppppid_mask"
            
            def description(self):
                return "PPP bit  mask"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _ppppid_val(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ppppid_val"
            
            def description(self):
                return "PPP PIO value"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ppppid_mask"] = _AF6CCI0012_RD_HODEC_PARSER._upen_ppppid._ppppid_mask()
            allFields["ppppid_val"] = _AF6CCI0012_RD_HODEC_PARSER._upen_ppppid._ppppid_val()
            return allFields

    class _upen_ethtyp(AtRegister.AtRegister):
        def name(self):
            return "ETH Type  global config"
    
        def description(self):
            return "Use to configure ethernet type for eth_type Reset_value of ppp_type eth_type == 0: 0x0800, is Ipv4 packets eth_type == 1: 0x86DD, is Ipv6 packets eth_type == 2: 0x8847, is MPLS unicast packets eth_type == 3: 0x8848, is MPLS multicast packets eth_type == 4: 0xFEFE, is IS-IS packets eth_type == 5: 0x809B, is Appletalks eth_type == 6: 0x8137, is Novell IPX eth_type == 7: 0x0600, is Xerox NS IDP eth_type == 8: 0x8929, is Cisco Systems eth_type == 9: 0x88CC, is Cisco Discovery Protocol eth_type == 10: 0x888E, is NETBIOS Framing eth_type == 11: 0x8149, is IPv6 header Compression eth_type == 12: 0x8863, is 802.1d Hello Packet eth_type == 13: 0x4242, is IBM Source Routing BPDU eth_type == 14: 0x8038, is DEC LANBridge Spaning Tree eth_type == 15: 0x6558, is BCP/ETH HDL_PATH: begin: IF($ethtyp==0) ihoparser_core.iethtyp0_cfg.xxxfpcfg ELSEIF($ethtyp==1) ihoparser_core.iethtyp1_cfg.xxxfpcfg ELSEIF($ethtyp==2) ihoparser_core.iethtyp2_cfg.xxxfpcfg ELSEIF($ethtyp==3) ihoparser_core.iethtyp3_cfg.xxxfpcfg ELSEIF($ethtyp==4) ihoparser_core.iethtyp4_cfg.xxxfpcfg ELSEIF($ethtyp==5) ihoparser_core.iethtyp5_cfg.xxxfpcfg ELSEIF($ethtyp==6) ihoparser_core.iethtyp6_cfg.xxxfpcfg ELSEIF($ethtyp==7) ihoparser_core.iethtyp7_cfg.xxxfpcfg ELSEIF($ethtyp==8) ihoparser_core.iethtyp8_cfg.xxxfpcfg ELSEIF($ethtyp==9) ihoparser_core.iethtyp9_cfg.xxxfpcfg ELSEIF($ethtyp==10) ihoparser_core.iethtyp10_cfg.xxxfpcfg ELSEIF($ethtyp==11) ihoparser_core.iethtyp11_cfg.xxxfpcfg ELSEIF($ethtyp==12) ihoparser_core.iethtyp12_cfg.xxxfpcfg ELSEIF($ethtyp==13) ihoparser_core.iethtyp13_cfg.xxxfpcfg ELSEIF($ethtyp==14) ihoparser_core.iethtyp14_cfg.xxxfpcfg ELSE ihoparser_core.iethtyp15_cfg.xxxfpcfg HDL_PATH: end:"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0_0010 + $ethtyp"
            
        def startAddress(self):
            return 0x00000010
            
        def endAddress(self):
            return 0x0000001f

        class _ethtyp_mask(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ethtyp_mask"
            
            def description(self):
                return "ETH bit mask"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _ethtyp_val(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ethtyp_val"
            
            def description(self):
                return "ETH Type value"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ethtyp_mask"] = _AF6CCI0012_RD_HODEC_PARSER._upen_ethtyp._ethtyp_mask()
            allFields["ethtyp_val"] = _AF6CCI0012_RD_HODEC_PARSER._upen_ethtyp._ethtyp_val()
            return allFields

    class _upen_mactyp(AtRegister.AtRegister):
        def name(self):
            return "MAC TYPE global config for parser"
    
        def description(self):
            return "Use to configure MACTYPE value for PPP BCP HDL_PATH: ihoparser_core.imactype_cfg.xxxfpcfg"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0_0020"
            
        def startAddress(self):
            return 0x00000020
            
        def endAddress(self):
            return 0xffffffff

        class _mactype(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mactype"
            
            def description(self):
                return "MAC TYPE value"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mactype"] = _AF6CCI0012_RD_HODEC_PARSER._upen_mactyp._mactype()
            return allFields

    class _upen_version(AtRegister.AtRegister):
        def name(self):
            return "Parser version status"
    
        def description(self):
            return "Use to read version status with format : VERSION_NUMBER.UPDATE_NUMBER  MONTH , DAY , 201YEAR + Version 0.0 Aug,08,2016  (value = 0000_8086) - Add verison register - Change dlci_mod to address_mod - Rename some signal"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0_0021"
            
        def startAddress(self):
            return 0x00000021
            
        def endAddress(self):
            return 0xffffffff

        class _ver_num(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 20
        
            def name(self):
                return "ver_num"
            
            def description(self):
                return "version number"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _upd_num(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "upd_num"
            
            def description(self):
                return "Update number"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _month(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "month"
            
            def description(self):
                return "month of version"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _day(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 4
        
            def name(self):
                return "day"
            
            def description(self):
                return "day of version"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _year(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "year"
            
            def description(self):
                return "year of version"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ver_num"] = _AF6CCI0012_RD_HODEC_PARSER._upen_version._ver_num()
            allFields["upd_num"] = _AF6CCI0012_RD_HODEC_PARSER._upen_version._upd_num()
            allFields["month"] = _AF6CCI0012_RD_HODEC_PARSER._upen_version._month()
            allFields["day"] = _AF6CCI0012_RD_HODEC_PARSER._upen_version._day()
            allFields["year"] = _AF6CCI0012_RD_HODEC_PARSER._upen_version._year()
            return allFields

    class _upen_cidcfg(AtRegister.AtRegister):
        def name(self):
            return "Channel id config for parser"
    
        def description(self):
            return "Use to configure for channel id HDL_PATH: ihoparser_core.mem_lklinkcfg.ram.ram[$cid]"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0_0100 + $cid"
            
        def startAddress(self):
            return 0x00000100
            
        def endAddress(self):
            return 0x000001ff

        class _ml_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "ml_typ"
            
            def description(self):
                return "Multilink type MLPPP : 0 : MLPPP short sequence , 1 : MLPPP long sequence"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _ml_enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "ml_enb"
            
            def description(self):
                return "Multilink enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _opt_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "opt_typ"
            
            def description(self):
                return "Option type PPP : 0 : PPP no class , 1 : PPP class PW FR  		: 0 : FR port	   , 1 : FR One2One Others FR   : 0 : MLFR End2End , 1 : MLFR UNI-NNi"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _frm_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 5
        
            def name(self):
                return "frm_typ"
            
            def description(self):
                return "Encap type 0		: PW 1		: PPP TER , IETF FR 2		: Cisco HDLC , Cisco FR 3	 	: Both Cisco FR and IETF FR"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _enc_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 2
        
            def name(self):
                return "enc_typ"
            
            def description(self):
                return "Encap type 0		: HDLC / Cisco HDLC 1		: PPP 2		: FR Others 	: unused"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _addcfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "addcfg"
            
            def description(self):
                return "Address configure PPP:  addcfg [1]  Address compress PPP mode enable addcfg [0]  Protocol compress PPP mode enable FR : addcfg = 00 : 2byte Q922 address addcfg = 01 : 3byte Q922 address addcfg = 10 : 4byte Q922 address"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ml_typ"] = _AF6CCI0012_RD_HODEC_PARSER._upen_cidcfg._ml_typ()
            allFields["ml_enb"] = _AF6CCI0012_RD_HODEC_PARSER._upen_cidcfg._ml_enb()
            allFields["opt_typ"] = _AF6CCI0012_RD_HODEC_PARSER._upen_cidcfg._opt_typ()
            allFields["frm_typ"] = _AF6CCI0012_RD_HODEC_PARSER._upen_cidcfg._frm_typ()
            allFields["enc_typ"] = _AF6CCI0012_RD_HODEC_PARSER._upen_cidcfg._enc_typ()
            allFields["addcfg"] = _AF6CCI0012_RD_HODEC_PARSER._upen_cidcfg._addcfg()
            return allFields

    class _upen_cidcnt(AtRegister.AtRegister):
        def name(self):
            return "Channel ID  Counters for parser"
    
        def description(self):
            return "Use to read/read to clear some counters at each channel id typ = 0 : SOP error counter typ = 1 : Address error counter typ = 2 : Control error counter typ = 3 : Protocol error counter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0_1000 + $cid*4  + $typ"
            
        def startAddress(self):
            return 0x00001000
            
        def endAddress(self):
            return 0x00001fff

        class _cnt_value(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_value"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_value"] = _AF6CCI0012_RD_HODEC_PARSER._upen_cidcnt._cnt_value()
            return allFields

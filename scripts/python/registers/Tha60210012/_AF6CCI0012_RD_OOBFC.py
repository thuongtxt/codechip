import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0012_RD_OOBFC(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["upen_force_pause"] = _AF6CCI0012_RD_OOBFC._upen_force_pause()
        allRegisters["upen_cfg_mode"] = _AF6CCI0012_RD_OOBFC._upen_cfg_mode()
        allRegisters["upen_cbfc_en"] = _AF6CCI0012_RD_OOBFC._upen_cbfc_en()
        allRegisters["upen_pause_interval"] = _AF6CCI0012_RD_OOBFC._upen_pause_interval()
        allRegisters["upen_cfg_opcode"] = _AF6CCI0012_RD_OOBFC._upen_cfg_opcode()
        allRegisters["upen_cfg_sah"] = _AF6CCI0012_RD_OOBFC._upen_cfg_sah()
        allRegisters["upen_cfg_sal"] = _AF6CCI0012_RD_OOBFC._upen_cfg_sal()
        allRegisters["upen_cfg_etype"] = _AF6CCI0012_RD_OOBFC._upen_cfg_etype()
        allRegisters["upen_qt7"] = _AF6CCI0012_RD_OOBFC._upen_qt7()
        allRegisters["upen_qt6"] = _AF6CCI0012_RD_OOBFC._upen_qt6()
        allRegisters["upen_qt5"] = _AF6CCI0012_RD_OOBFC._upen_qt5()
        allRegisters["upen_qt4"] = _AF6CCI0012_RD_OOBFC._upen_qt4()
        allRegisters["upen_qt3"] = _AF6CCI0012_RD_OOBFC._upen_qt3()
        allRegisters["upen_qt2"] = _AF6CCI0012_RD_OOBFC._upen_qt2()
        allRegisters["upen_qt1"] = _AF6CCI0012_RD_OOBFC._upen_qt1()
        allRegisters["upen_qt0"] = _AF6CCI0012_RD_OOBFC._upen_qt0()
        allRegisters["upen_intval7"] = _AF6CCI0012_RD_OOBFC._upen_intval7()
        allRegisters["upen_intval6"] = _AF6CCI0012_RD_OOBFC._upen_intval6()
        allRegisters["upen_intval5"] = _AF6CCI0012_RD_OOBFC._upen_intval5()
        allRegisters["upen_intval4"] = _AF6CCI0012_RD_OOBFC._upen_intval4()
        allRegisters["upen_intval3"] = _AF6CCI0012_RD_OOBFC._upen_intval3()
        allRegisters["upen_intval2"] = _AF6CCI0012_RD_OOBFC._upen_intval2()
        allRegisters["upen_intval1"] = _AF6CCI0012_RD_OOBFC._upen_intval1()
        allRegisters["upen_intval0"] = _AF6CCI0012_RD_OOBFC._upen_intval0()
        allRegisters["upen_dis_fcs"] = _AF6CCI0012_RD_OOBFC._upen_dis_fcs()
        allRegisters["upen_csc_pfc"] = _AF6CCI0012_RD_OOBFC._upen_csc_pfc()
        allRegisters["upen_csc_cla"] = _AF6CCI0012_RD_OOBFC._upen_csc_cla()
        allRegisters["upen_cnt_pfc_r2c"] = _AF6CCI0012_RD_OOBFC._upen_cnt_pfc_r2c()
        allRegisters["upen_cnt_pfc_ro"] = _AF6CCI0012_RD_OOBFC._upen_cnt_pfc_ro()
        allRegisters["upen_cnt_pau_r2c"] = _AF6CCI0012_RD_OOBFC._upen_cnt_pau_r2c()
        allRegisters["upen_cnt_pau_ro"] = _AF6CCI0012_RD_OOBFC._upen_cnt_pau_ro()
        return allRegisters

    class _upen_force_pause(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "This register is used to force triger pause frame"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000003a
            
        def endAddress(self):
            return 0xffffffff

        class _frc802frame(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "frc802frame"
            
            def description(self):
                return "Enable force 802.3 pause frame"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _frcPFCframe(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "frcPFCframe"
            
            def description(self):
                return "bitmap enable from class7 down to class0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["frc802frame"] = _AF6CCI0012_RD_OOBFC._upen_force_pause._frc802frame()
            allFields["frcPFCframe"] = _AF6CCI0012_RD_OOBFC._upen_force_pause._frcPFCframe()
            return allFields

    class _upen_cfg_mode(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "This register is used to sel mode of frame"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000015
            
        def endAddress(self):
            return 0xffffffff

        class _framemode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "framemode"
            
            def description(self):
                return "1: PFC frame, 0: 802.3 frame"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["framemode"] = _AF6CCI0012_RD_OOBFC._upen_cfg_mode._framemode()
            return allFields

    class _upen_cbfc_en(AtRegister.AtRegister):
        def name(self):
            return "cbfc enable"
    
        def description(self):
            return "This register is used to enable cbfc frame"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000014
            
        def endAddress(self):
            return 0xffffffff

        class _cbfcen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cbfcen"
            
            def description(self):
                return "bit map enable 8class (7-0), 1 : enable, 0:disble"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cbfcen"] = _AF6CCI0012_RD_OOBFC._upen_cbfc_en._cbfcen()
            return allFields

    class _upen_pause_interval(AtRegister.AtRegister):
        def name(self):
            return "802.3 interval"
    
        def description(self):
            return "This register is used to cfg time gen packet"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000013
            
        def endAddress(self):
            return 0xffffffff

        class _quantum802frame(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "quantum802frame"
            
            def description(self):
                return "quantum interval of 802.3 frame, value of \"parameter\" octet, it also interval gen packet = ~ 6.4ns * cfg val"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["quantum802frame"] = _AF6CCI0012_RD_OOBFC._upen_pause_interval._quantum802frame()
            return allFields

    class _upen_cfg_opcode(AtRegister.AtRegister):
        def name(self):
            return "opcode of pause frame"
    
        def description(self):
            return "This register is used to cfg opcode of ETH frame"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000012
            
        def endAddress(self):
            return 0xffffffff

        class _opcode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "opcode"
            
            def description(self):
                return "Opcode ETH frame"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["opcode"] = _AF6CCI0012_RD_OOBFC._upen_cfg_opcode._opcode()
            return allFields

    class _upen_cfg_sah(AtRegister.AtRegister):
        def name(self):
            return "SA of pause frame"
    
        def description(self):
            return "This register is used to cfg upper SA  of ETH frame"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000016
            
        def endAddress(self):
            return 0xffffffff

        class _upperSA(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "upperSA"
            
            def description(self):
                return "bit[47:32] of SA of ETH frame"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["upperSA"] = _AF6CCI0012_RD_OOBFC._upen_cfg_sah._upperSA()
            return allFields

    class _upen_cfg_sal(AtRegister.AtRegister):
        def name(self):
            return "SA lower of pause frame"
    
        def description(self):
            return "This register is used to cfg lower SA  of ETH frame"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000017
            
        def endAddress(self):
            return 0xffffffff

        class _lowerSA(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lowerSA"
            
            def description(self):
                return "bit[31:00] of SA of ETH frame"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lowerSA"] = _AF6CCI0012_RD_OOBFC._upen_cfg_sal._lowerSA()
            return allFields

    class _upen_cfg_etype(AtRegister.AtRegister):
        def name(self):
            return "eth type of pause frame"
    
        def description(self):
            return "This register is used to cfg eth type of ETH frame"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000011
            
        def endAddress(self):
            return 0xffffffff

        class _ethtype(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ethtype"
            
            def description(self):
                return "ETH type of ETH frame"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ethtype"] = _AF6CCI0012_RD_OOBFC._upen_cfg_etype._ethtype()
            return allFields

    class _upen_qt7(AtRegister.AtRegister):
        def name(self):
            return "quantum class 7"
    
        def description(self):
            return "This register is used to cfg timer for class7"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000010
            
        def endAddress(self):
            return 0xffffffff

        class _quantum7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "quantum7"
            
            def description(self):
                return "pause timer for class 7"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["quantum7"] = _AF6CCI0012_RD_OOBFC._upen_qt7._quantum7()
            return allFields

    class _upen_qt6(AtRegister.AtRegister):
        def name(self):
            return "quantum class 6"
    
        def description(self):
            return "This register is used to cfg timer for class6"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000f
            
        def endAddress(self):
            return 0xffffffff

        class _quantum6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "quantum6"
            
            def description(self):
                return "pause timer for class 6"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["quantum6"] = _AF6CCI0012_RD_OOBFC._upen_qt6._quantum6()
            return allFields

    class _upen_qt5(AtRegister.AtRegister):
        def name(self):
            return "quantum class 5"
    
        def description(self):
            return "This register is used to cfg timer for class5"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000e
            
        def endAddress(self):
            return 0xffffffff

        class _quantum5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "quantum5"
            
            def description(self):
                return "pause timer for class 5"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["quantum5"] = _AF6CCI0012_RD_OOBFC._upen_qt5._quantum5()
            return allFields

    class _upen_qt4(AtRegister.AtRegister):
        def name(self):
            return "quantum class 4"
    
        def description(self):
            return "This register is used to cfg timer for class4"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000d
            
        def endAddress(self):
            return 0xffffffff

        class _quantum4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "quantum4"
            
            def description(self):
                return "pause timer for class 4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["quantum4"] = _AF6CCI0012_RD_OOBFC._upen_qt4._quantum4()
            return allFields

    class _upen_qt3(AtRegister.AtRegister):
        def name(self):
            return "quantum class 3"
    
        def description(self):
            return "This register is used to cfg timer for class3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000c
            
        def endAddress(self):
            return 0xffffffff

        class _quantum3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "quantum3"
            
            def description(self):
                return "pause timer for class 3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["quantum3"] = _AF6CCI0012_RD_OOBFC._upen_qt3._quantum3()
            return allFields

    class _upen_qt2(AtRegister.AtRegister):
        def name(self):
            return "quantum class 2"
    
        def description(self):
            return "This register is used to cfg timer for class2"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000b
            
        def endAddress(self):
            return 0xffffffff

        class _quantum2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "quantum2"
            
            def description(self):
                return "pause timer for class 2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["quantum2"] = _AF6CCI0012_RD_OOBFC._upen_qt2._quantum2()
            return allFields

    class _upen_qt1(AtRegister.AtRegister):
        def name(self):
            return "quantum class 1"
    
        def description(self):
            return "This register is used to cfg timer for class1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000a
            
        def endAddress(self):
            return 0xffffffff

        class _quantum1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "quantum1"
            
            def description(self):
                return "pause timer for class 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["quantum1"] = _AF6CCI0012_RD_OOBFC._upen_qt1._quantum1()
            return allFields

    class _upen_qt0(AtRegister.AtRegister):
        def name(self):
            return "quantum class 1"
    
        def description(self):
            return "This register is used to cfg timer for class0"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000009
            
        def endAddress(self):
            return 0xffffffff

        class _quantum0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "quantum0"
            
            def description(self):
                return "pause timer for class 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["quantum0"] = _AF6CCI0012_RD_OOBFC._upen_qt0._quantum0()
            return allFields

    class _upen_intval7(AtRegister.AtRegister):
        def name(self):
            return "interval gen pfc frame class7"
    
        def description(self):
            return "This register is used to cfg timer for class7"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000008
            
        def endAddress(self):
            return 0xffffffff

        class _cbfcint7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cbfcint7"
            
            def description(self):
                return "timer gen pfc frame for class 7 = 51.2ns * cfg_val"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cbfcint7"] = _AF6CCI0012_RD_OOBFC._upen_intval7._cbfcint7()
            return allFields

    class _upen_intval6(AtRegister.AtRegister):
        def name(self):
            return "interval gen pfc frame class6"
    
        def description(self):
            return "This register is used to cfg timer for class6"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000007
            
        def endAddress(self):
            return 0xffffffff

        class _cbfcint6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cbfcint6"
            
            def description(self):
                return "timer gen pfc frame for class 6"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cbfcint6"] = _AF6CCI0012_RD_OOBFC._upen_intval6._cbfcint6()
            return allFields

    class _upen_intval5(AtRegister.AtRegister):
        def name(self):
            return "interval gen pfc frame class5"
    
        def description(self):
            return "This register is used to cfg timer for class5"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000006
            
        def endAddress(self):
            return 0xffffffff

        class _cbfcint5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cbfcint5"
            
            def description(self):
                return "timer gen pfc frame for class 5"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cbfcint5"] = _AF6CCI0012_RD_OOBFC._upen_intval5._cbfcint5()
            return allFields

    class _upen_intval4(AtRegister.AtRegister):
        def name(self):
            return "interval gen pfc frame class4"
    
        def description(self):
            return "This register is used to cfg timer for class4"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000005
            
        def endAddress(self):
            return 0xffffffff

        class _cbfcint4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cbfcint4"
            
            def description(self):
                return "timer gen pfc frame for class 4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cbfcint4"] = _AF6CCI0012_RD_OOBFC._upen_intval4._cbfcint4()
            return allFields

    class _upen_intval3(AtRegister.AtRegister):
        def name(self):
            return "interval gen pfc frame class3"
    
        def description(self):
            return "This register is used to cfg timer for class3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000004
            
        def endAddress(self):
            return 0xffffffff

        class _cbfcint3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cbfcint3"
            
            def description(self):
                return "timer gen pfc frame for class 3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cbfcint3"] = _AF6CCI0012_RD_OOBFC._upen_intval3._cbfcint3()
            return allFields

    class _upen_intval2(AtRegister.AtRegister):
        def name(self):
            return "interval gen pfc frame class2"
    
        def description(self):
            return "This register is used to cfg timer for class2"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _cbfcint2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cbfcint2"
            
            def description(self):
                return "timer gen pfc frame for class 2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cbfcint2"] = _AF6CCI0012_RD_OOBFC._upen_intval2._cbfcint2()
            return allFields

    class _upen_intval1(AtRegister.AtRegister):
        def name(self):
            return "interval gen pfc frame class1"
    
        def description(self):
            return "This register is used to cfg timer for class1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0xffffffff

        class _cbfcint1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cbfcint1"
            
            def description(self):
                return "timer gen pfc frame for class 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cbfcint1"] = _AF6CCI0012_RD_OOBFC._upen_intval1._cbfcint1()
            return allFields

    class _upen_intval0(AtRegister.AtRegister):
        def name(self):
            return "interval gen pfc frame class0"
    
        def description(self):
            return "This register is used to cfg timer for class0"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000001
            
        def endAddress(self):
            return 0xffffffff

        class _cbfcint0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cbfcint0"
            
            def description(self):
                return "timer gen pfc frame for class 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cbfcint0"] = _AF6CCI0012_RD_OOBFC._upen_intval0._cbfcint0()
            return allFields

    class _upen_dis_fcs(AtRegister.AtRegister):
        def name(self):
            return "disable fcs of pause frame"
    
        def description(self):
            return "This register is used to en/dis fcs in pause frame"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00008003
            
        def endAddress(self):
            return 0xffffffff

        class _disfcs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "disfcs"
            
            def description(self):
                return "1: dis fcs, and 4byte FCS will be caculated by MAC 0: enable FCS, and MAC must disable calcuale FCS"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["disfcs"] = _AF6CCI0012_RD_OOBFC._upen_dis_fcs._disfcs()
            return allFields

    class _upen_csc_pfc(AtRegister.AtRegister):
        def name(self):
            return "cisco pfc pkt count"
    
        def description(self):
            return "This register is count number of pfc packet"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000003c
            
        def endAddress(self):
            return 0xffffffff

        class _cisPFCcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cisPFCcnt"
            
            def description(self):
                return "IP cisco gen pkt PFC"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cisPFCcnt"] = _AF6CCI0012_RD_OOBFC._upen_csc_pfc._cisPFCcnt()
            return allFields

    class _upen_csc_cla(AtRegister.AtRegister):
        def name(self):
            return "cisco triger 802.3"
    
        def description(self):
            return "This register is count number of triger 802.3 pause frame"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000003b
            
        def endAddress(self):
            return 0xffffffff

        class _cis802trig(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cis802trig"
            
            def description(self):
                return "IP cisco gen triger 802.3 pause frame"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cis802trig"] = _AF6CCI0012_RD_OOBFC._upen_csc_cla._cis802trig()
            return allFields

    class _upen_cnt_pfc_r2c(AtRegister.AtRegister):
        def name(self):
            return "cnt pfc r2c"
    
        def description(self):
            return "This register is used to count PFC frame send to MUX, read 2 clr"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00008400
            
        def endAddress(self):
            return 0xffffffff

        class _cntr2cPFCframe(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2cPFCframe"
            
            def description(self):
                return "number of packet PFC out to MUX,r2c"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2cPFCframe"] = _AF6CCI0012_RD_OOBFC._upen_cnt_pfc_r2c._cntr2cPFCframe()
            return allFields

    class _upen_cnt_pfc_ro(AtRegister.AtRegister):
        def name(self):
            return "cnt pfc ro"
    
        def description(self):
            return "This register is used to count PFC frame send to MUX, read only"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00008600
            
        def endAddress(self):
            return 0xffffffff

        class _cntro802frame(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntro802frame"
            
            def description(self):
                return "number of packet PFC out to MUX, ro"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntro802frame"] = _AF6CCI0012_RD_OOBFC._upen_cnt_pfc_ro._cntro802frame()
            return allFields

    class _upen_cnt_pau_r2c(AtRegister.AtRegister):
        def name(self):
            return "cnt pause r2c"
    
        def description(self):
            return "This register is used to count 802.3 frame send to MUX, read 2 clr"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00008401
            
        def endAddress(self):
            return 0xffffffff

        class _cntr2c802frame(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c802frame"
            
            def description(self):
                return "number of packet 802.3 out to MUX,r2c"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c802frame"] = _AF6CCI0012_RD_OOBFC._upen_cnt_pau_r2c._cntr2c802frame()
            return allFields

    class _upen_cnt_pau_ro(AtRegister.AtRegister):
        def name(self):
            return "cnt pause ro"
    
        def description(self):
            return "This register is used to count 802.3 frame send to MUX, read only"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00008601
            
        def endAddress(self):
            return 0xffffffff

        class _cntro802frame(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntro802frame"
            
            def description(self):
                return "number of packet 802.3 out to MUX, ro"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntro802frame"] = _AF6CCI0012_RD_OOBFC._upen_cnt_pau_ro._cntro802frame()
            return allFields

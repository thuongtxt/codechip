import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0012_RD_HDLCENC(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["upen_mapf"] = _AF6CCI0012_RD_HDLCENC._upen_mapf()
        return allRegisters

    class _upen_mapf(AtRegister.AtRegister):
        def name(self):
            return "ENC Table"
    
        def description(self):
            return "Per STS-ID Control"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000200 + $VCG"
            
        def startAddress(self):
            return 0x00000200
            
        def endAddress(self):
            return 0x000003ff

        class _icfgscr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "icfgscr"
            
            def description(self):
                return "Scramble Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _icfgcrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "icfgcrc"
            
            def description(self):
                return "1 is CRC-16, 0 is CRC-32"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _encicfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 6
        
            def name(self):
                return "encicfg"
            
            def description(self):
                return "1 is STM-1, 2 is STS-3C, 3 is STS-12C, 4 is STS-24C, 5 is STS-48C"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _encivcx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "encivcx"
            
            def description(self):
                return "STS-ID Conversion are Used in Engine"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["icfgscr"] = _AF6CCI0012_RD_HDLCENC._upen_mapf._icfgscr()
            allFields["icfgcrc"] = _AF6CCI0012_RD_HDLCENC._upen_mapf._icfgcrc()
            allFields["encicfg"] = _AF6CCI0012_RD_HDLCENC._upen_mapf._encicfg()
            allFields["encivcx"] = _AF6CCI0012_RD_HDLCENC._upen_mapf._encivcx()
            return allFields

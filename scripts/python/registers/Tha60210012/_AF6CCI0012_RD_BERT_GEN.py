import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0012_RD_BERT_GEN(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["upen_id_reg_gen"] = _AF6CCI0012_RD_BERT_GEN._upen_id_reg_gen()
        allRegisters["glb_pen"] = _AF6CCI0012_RD_BERT_GEN._glb_pen()
        allRegisters["errins_pen"] = _AF6CCI0012_RD_BERT_GEN._errins_pen()
        allRegisters["ctrl_pen"] = _AF6CCI0012_RD_BERT_GEN._ctrl_pen()
        allRegisters["thrnfix_pen"] = _AF6CCI0012_RD_BERT_GEN._thrnfix_pen()
        allRegisters["ber_pen"] = _AF6CCI0012_RD_BERT_GEN._ber_pen()
        allRegisters["status_pen"] = _AF6CCI0012_RD_BERT_GEN._status_pen()
        allRegisters["tsen_pen"] = _AF6CCI0012_RD_BERT_GEN._tsen_pen()
        allRegisters["goodbit_pen"] = _AF6CCI0012_RD_BERT_GEN._goodbit_pen()
        return allRegisters

    class _upen_id_reg_gen(AtRegister.AtRegister):
        def name(self):
            return "Sel Bert ID Gen"
    
        def description(self):
            return "Sel 16 chanel id which enable generate BERT from 12xOC24 channel"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return "0x8500 + $id"
            
        def startAddress(self):
            return 0x00008500
            
        def endAddress(self):
            return 0x0000850f

        class _gen_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "gen_en"
            
            def description(self):
                return "bit enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _slide_id(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 11
        
            def name(self):
                return "slide_id"
            
            def description(self):
                return "OC48 slide"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _gen_id(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 0
        
            def name(self):
                return "gen_id"
            
            def description(self):
                return "channel id"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["gen_en"] = _AF6CCI0012_RD_BERT_GEN._upen_id_reg_gen._gen_en()
            allFields["slide_id"] = _AF6CCI0012_RD_BERT_GEN._upen_id_reg_gen._slide_id()
            allFields["gen_id"] = _AF6CCI0012_RD_BERT_GEN._upen_id_reg_gen._gen_id()
            return allFields

    class _glb_pen(AtRegister.AtRegister):
        def name(self):
            return "Bert Gen Global Register"
    
        def description(self):
            return "global config"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000837f
            
        def endAddress(self):
            return 0xffffffff

        class _global(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "global"
            
            def description(self):
                return "value of global"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["global"] = _AF6CCI0012_RD_BERT_GEN._glb_pen._global()
            return allFields

    class _errins_pen(AtRegister.AtRegister):
        def name(self):
            return "BERT insert error"
    
        def description(self):
            return "Insert single bit error"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000837e
            
        def endAddress(self):
            return 0xffffffff

        class _errins_lat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "errins_lat"
            
            def description(self):
                return "enable id insert error"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["errins_lat"] = _AF6CCI0012_RD_BERT_GEN._errins_pen._errins_lat()
            return allFields

    class _ctrl_pen(AtRegister.AtRegister):
        def name(self):
            return "Control Bert Generate"
    
        def description(self):
            return "Used to select mode operation of bert"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return "0x8300 + $ctrl_id"
            
        def startAddress(self):
            return 0x00008300
            
        def endAddress(self):
            return 0x0000830f

        class _swapmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "swapmode"
            
            def description(self):
                return "swap data"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _invmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "invmode"
            
            def description(self):
                return "invert data"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _patbit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 5
        
            def name(self):
                return "patbit"
            
            def description(self):
                return "number of bit fix patt use"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _patt_mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "patt_mode"
            
            def description(self):
                return "sel pattern gen # 0x0 : prbs9 # 0x1 : prbs11 # 0x2 : prbs15 # 0x3 : prbs20r # 0x4 : prbs20 # 0x5 : qrss20 # 0x6 : prbs23 # 0x7 : dds1 # 0x8 : dds2 # 0x9 : dds3 # 0xA : dds4 # 0xB : dds5 # 0xC : daly # 0XD : octet55_v2 # 0xE : octet55_v3 # 0xF : fix3in24 # 0x10: fix1in8 # 0x11: fix2in8 # 0x12: fixpat # 0x13: sequence # 0x14: ds0prbs # 0x15: fixnin32"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["swapmode"] = _AF6CCI0012_RD_BERT_GEN._ctrl_pen._swapmode()
            allFields["invmode"] = _AF6CCI0012_RD_BERT_GEN._ctrl_pen._invmode()
            allFields["patbit"] = _AF6CCI0012_RD_BERT_GEN._ctrl_pen._patbit()
            allFields["patt_mode"] = _AF6CCI0012_RD_BERT_GEN._ctrl_pen._patt_mode()
            return allFields

    class _thrnfix_pen(AtRegister.AtRegister):
        def name(self):
            return "Config Fix pattern gen"
    
        def description(self):
            return "config fix pattern gen"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return "0x8310 + $genid"
            
        def startAddress(self):
            return 0x00008310
            
        def endAddress(self):
            return 0x0000831f

        class _crrthrnfix(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "crrthrnfix"
            
            def description(self):
                return "config fix pattern gen"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["crrthrnfix"] = _AF6CCI0012_RD_BERT_GEN._thrnfix_pen._crrthrnfix()
            return allFields

    class _ber_pen(AtRegister.AtRegister):
        def name(self):
            return "Config Insert Ber"
    
        def description(self):
            return "Mode of insert error rate"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return "0x8320 + $berid"
            
        def startAddress(self):
            return 0x00008320
            
        def endAddress(self):
            return 0x0000832f

        class _ber_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "ber_en"
            
            def description(self):
                return "enable ber insert"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ber_mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ber_mode"
            
            def description(self):
                return "sel ber insert mode #0x0 BER 10e-2 #0x1 BER 10e-3 #0x2 BER 10e-4 #0x3 BER 10e-5 #0x4 BER 10e-6 #0x5 BER 10e-7"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ber_en"] = _AF6CCI0012_RD_BERT_GEN._ber_pen._ber_en()
            allFields["ber_mode"] = _AF6CCI0012_RD_BERT_GEN._ber_pen._ber_mode()
            return allFields

    class _status_pen(AtRegister.AtRegister):
        def name(self):
            return "Status of bert"
    
        def description(self):
            return "Indicate status of bert gen"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return "0x8340 + $sttid"
            
        def startAddress(self):
            return 0x00008340
            
        def endAddress(self):
            return 0x0000834f

        class _crr_stt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 44
                
            def startBit(self):
                return 0
        
            def name(self):
                return "crr_stt"
            
            def description(self):
                return "status value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["crr_stt"] = _AF6CCI0012_RD_BERT_GEN._status_pen._crr_stt()
            return allFields

    class _tsen_pen(AtRegister.AtRegister):
        def name(self):
            return "Config nxDS0 enable"
    
        def description(self):
            return "enable bert for 32 timeslot E1 or 24timeslot T1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return "0x8360 + $nid"
            
        def startAddress(self):
            return 0x00008360
            
        def endAddress(self):
            return 0x0000836f

        class _tsen_pdo(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tsen_pdo"
            
            def description(self):
                return "set \"1\" to enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tsen_pdo"] = _AF6CCI0012_RD_BERT_GEN._tsen_pen._tsen_pdo()
            return allFields

    class _goodbit_pen(AtRegister.AtRegister):
        def name(self):
            return "Moniter Good Bit"
    
        def description(self):
            return "counter good bit"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return "0x8380 + $gb_id"
            
        def startAddress(self):
            return 0x00008380
            
        def endAddress(self):
            return 0x0000838f

        class _goodbit_pdo(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "goodbit_pdo"
            
            def description(self):
                return "counter goodbit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["goodbit_pdo"] = _AF6CCI0012_RD_BERT_GEN._goodbit_pen._goodbit_pdo()
            return allFields

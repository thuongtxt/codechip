import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0012_RD_MPIG(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["cfg0_pen"] = _AF6CCI0012_RD_MPIG._cfg0_pen()
        allRegisters["cfg2_pen"] = _AF6CCI0012_RD_MPIG._cfg2_pen()
        allRegisters["cfg3_pen"] = _AF6CCI0012_RD_MPIG._cfg3_pen()
        allRegisters["dum0_pen"] = _AF6CCI0012_RD_MPIG._dum0_pen()
        allRegisters["dum1_pen"] = _AF6CCI0012_RD_MPIG._dum1_pen()
        allRegisters["dum2_pen"] = _AF6CCI0012_RD_MPIG._dum2_pen()
        allRegisters["dum3_pen"] = _AF6CCI0012_RD_MPIG._dum3_pen()
        allRegisters["cfg0qthsh_pen"] = _AF6CCI0012_RD_MPIG._cfg0qthsh_pen()
        allRegisters["schelcfg0_pen"] = _AF6CCI0012_RD_MPIG._schelcfg0_pen()
        allRegisters["schelinkicfg1_pen"] = _AF6CCI0012_RD_MPIG._schelinkicfg1_pen()
        allRegisters["sta0_upen"] = _AF6CCI0012_RD_MPIG._sta0_upen()
        allRegisters["sta1_upen"] = _AF6CCI0012_RD_MPIG._sta1_upen()
        allRegisters["pmcnt1_pen"] = _AF6CCI0012_RD_MPIG._pmcnt1_pen()
        allRegisters["pmcnt2_pen"] = _AF6CCI0012_RD_MPIG._pmcnt2_pen()
        allRegisters["pmcnt3_pen"] = _AF6CCI0012_RD_MPIG._pmcnt3_pen()
        allRegisters["pmcnt4_pen"] = _AF6CCI0012_RD_MPIG._pmcnt4_pen()
        allRegisters["stk0_pen"] = _AF6CCI0012_RD_MPIG._stk0_pen()
        allRegisters["stk1en"] = _AF6CCI0012_RD_MPIG._stk1en()
        allRegisters["stk2pen"] = _AF6CCI0012_RD_MPIG._stk2pen()
        allRegisters["stk3en"] = _AF6CCI0012_RD_MPIG._stk3en()
        return allRegisters

    class _cfg0_pen(AtRegister.AtRegister):
        def name(self):
            return "MPIG Global Configuration"
    
        def description(self):
            return "Used to configure MPIG global"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x2901"
            
        def startAddress(self):
            return 0x00002901
            
        def endAddress(self):
            return 0xffffffff

        class _RSQ_Source_Dis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "RSQ_Source_Dis"
            
            def description(self):
                return "RSQ Source Dis - Set 1 to disable - Set 0 to MPIG accept packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _eXAUI1_Source_Dis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "eXAUI1_Source_Dis"
            
            def description(self):
                return "eXAUI1 Source Dis - Set 1 to disable - Set 0 to MPIG accept packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _eXAUI0_Source_Dis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "eXAUI0_Source_Dis"
            
            def description(self):
                return "eXAUI0 Source Dis - Set 1 to disable - Set 0 to MPIG accept packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _PLA_Source_Dis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PLA_Source_Dis"
            
            def description(self):
                return "PLA Source Dis - Set 1 to disable - Set 0 to MPIG accept packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _CPU_Force_MUX_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "CPU_Force_MUX_En"
            
            def description(self):
                return "CPU Force MUX enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _CPU_Force_Source_MUX(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "CPU_Force_Source_MUX"
            
            def description(self):
                return "CPU force Source for MUX - 0: PLA Source (PW/EOP/iSMG) - 1: eXAUI#0 source - 2: eXAUI#1 source - 3: RSQ source"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _CPU_Force_FSM_MUX(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "CPU_Force_FSM_MUX"
            
            def description(self):
                return "CPU force FSM for MUX - 0: IDLE State - 1: SOP State - 2: EOP State - 3: ERR State"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MPIG_active(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MPIG_active"
            
            def description(self):
                return "MPIG Active"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RSQ_Source_Dis"] = _AF6CCI0012_RD_MPIG._cfg0_pen._RSQ_Source_Dis()
            allFields["eXAUI1_Source_Dis"] = _AF6CCI0012_RD_MPIG._cfg0_pen._eXAUI1_Source_Dis()
            allFields["eXAUI0_Source_Dis"] = _AF6CCI0012_RD_MPIG._cfg0_pen._eXAUI0_Source_Dis()
            allFields["PLA_Source_Dis"] = _AF6CCI0012_RD_MPIG._cfg0_pen._PLA_Source_Dis()
            allFields["CPU_Force_MUX_En"] = _AF6CCI0012_RD_MPIG._cfg0_pen._CPU_Force_MUX_En()
            allFields["CPU_Force_Source_MUX"] = _AF6CCI0012_RD_MPIG._cfg0_pen._CPU_Force_Source_MUX()
            allFields["CPU_Force_FSM_MUX"] = _AF6CCI0012_RD_MPIG._cfg0_pen._CPU_Force_FSM_MUX()
            allFields["MPIG_active"] = _AF6CCI0012_RD_MPIG._cfg0_pen._MPIG_active()
            return allFields

    class _cfg2_pen(AtRegister.AtRegister):
        def name(self):
            return "MPIG Configure 1 for debug"
    
        def description(self):
            return "Used to Dump Enable, test on shot for engine MPIG"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x2902"
            
        def startAddress(self):
            return 0x00002902
            
        def endAddress(self):
            return 0xffffffff

        class _MPIG_dump_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "MPIG_dump_en"
            
            def description(self):
                return "MPIG Dump Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MPIG_oneshot_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "MPIG_oneshot_en"
            
            def description(self):
                return "MPIG One Shot Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MPIG_NumPkt_Accept(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 8
        
            def name(self):
                return "MPIG_NumPkt_Accept"
            
            def description(self):
                return "MPIG Number Packet Accept,"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MPIG_NumPkt_Store_Before_Transmit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MPIG_NumPkt_Store_Before_Transmit"
            
            def description(self):
                return "MPIG Number Packet Store before transmiting,"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MPIG_dump_en"] = _AF6CCI0012_RD_MPIG._cfg2_pen._MPIG_dump_en()
            allFields["MPIG_oneshot_en"] = _AF6CCI0012_RD_MPIG._cfg2_pen._MPIG_oneshot_en()
            allFields["MPIG_NumPkt_Accept"] = _AF6CCI0012_RD_MPIG._cfg2_pen._MPIG_NumPkt_Accept()
            allFields["MPIG_NumPkt_Store_Before_Transmit"] = _AF6CCI0012_RD_MPIG._cfg2_pen._MPIG_NumPkt_Store_Before_Transmit()
            return allFields

    class _cfg3_pen(AtRegister.AtRegister):
        def name(self):
            return "MPIG Global Configuration"
    
        def description(self):
            return "Used to Configure Monitor length input and output MPIG and sticky other register"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x2903"
            
        def startAddress(self):
            return 0x00002903
            
        def endAddress(self):
            return 0xffffffff

        class _MPIG_ChannelID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "MPIG_ChannelID"
            
            def description(self):
                return "MPIG Configure Channel ID for Monitor"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MPIG_Length(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MPIG_Length"
            
            def description(self):
                return "MPIG Configure length for Monitor"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MPIG_ChannelID"] = _AF6CCI0012_RD_MPIG._cfg3_pen._MPIG_ChannelID()
            allFields["MPIG_Length"] = _AF6CCI0012_RD_MPIG._cfg3_pen._MPIG_Length()
            return allFields

    class _dum0_pen(AtRegister.AtRegister):
        def name(self):
            return "MPIG Input Packet Information Dump"
    
        def description(self):
            return "Used to Dump Input Packet Information"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xA000+$mem_id"
            
        def startAddress(self):
            return 0x0000a000
            
        def endAddress(self):
            return 0x0000a0ff

        class _Input_PLA_Packet_Length_ID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Input_PLA_Packet_Length_ID"
            
            def description(self):
                return "Input PLA packet Length"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Input_PLA_Channel_ID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Input_PLA_Channel_ID"
            
            def description(self):
                return "Input PLA channel ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Input_PLA_Packet_Length_ID"] = _AF6CCI0012_RD_MPIG._dum0_pen._Input_PLA_Packet_Length_ID()
            allFields["Input_PLA_Channel_ID"] = _AF6CCI0012_RD_MPIG._dum0_pen._Input_PLA_Channel_ID()
            return allFields

    class _dum1_pen(AtRegister.AtRegister):
        def name(self):
            return "MPIG Output Packet Information Dump"
    
        def description(self):
            return "Used to Dump Output Packet Information"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xA100+$mem_id"
            
        def startAddress(self):
            return 0x0000a100
            
        def endAddress(self):
            return 0x0000a1ff

        class _Output_PLA_Packet_Length_ID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Output_PLA_Packet_Length_ID"
            
            def description(self):
                return "Output PLA packet Length"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Output_PLA_Channel_ID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Output_PLA_Channel_ID"
            
            def description(self):
                return "Output PLA channel ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Output_PLA_Packet_Length_ID"] = _AF6CCI0012_RD_MPIG._dum1_pen._Output_PLA_Packet_Length_ID()
            allFields["Output_PLA_Channel_ID"] = _AF6CCI0012_RD_MPIG._dum1_pen._Output_PLA_Channel_ID()
            return allFields

    class _dum2_pen(AtRegister.AtRegister):
        def name(self):
            return "MPIG Write DDR Packet Information Dump"
    
        def description(self):
            return "Used to Dump Output Packet Information"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xA200+$mem_id"
            
        def startAddress(self):
            return 0x0000a200
            
        def endAddress(self):
            return 0x0000a2ff

        class _DDR_Write_Packet_Length(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 12
        
            def name(self):
                return "DDR_Write_Packet_Length"
            
            def description(self):
                return "DDR_Write Packet Length"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _DDR_Write_Queue_ID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "DDR_Write_Queue_ID"
            
            def description(self):
                return "DDR_Write MPIG Queue ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _DDR_Write_Sequence(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DDR_Write_Sequence"
            
            def description(self):
                return "DDR Write Sequence"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DDR_Write_Packet_Length"] = _AF6CCI0012_RD_MPIG._dum2_pen._DDR_Write_Packet_Length()
            allFields["DDR_Write_Queue_ID"] = _AF6CCI0012_RD_MPIG._dum2_pen._DDR_Write_Queue_ID()
            allFields["DDR_Write_Sequence"] = _AF6CCI0012_RD_MPIG._dum2_pen._DDR_Write_Sequence()
            return allFields

    class _dum3_pen(AtRegister.AtRegister):
        def name(self):
            return "MPIG Read DDR Packet Information Dump"
    
        def description(self):
            return "Used to Dump Output Packet Information"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xA300+$mem_id"
            
        def startAddress(self):
            return 0x0000a300
            
        def endAddress(self):
            return 0x0000a3ff

        class _DDR_Read_Packet_Length(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 12
        
            def name(self):
                return "DDR_Read_Packet_Length"
            
            def description(self):
                return "DDR_Write Packet Length"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _DDR_Read_Queue_ID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "DDR_Read_Queue_ID"
            
            def description(self):
                return "DDR_Write MPIG Queue ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _DDR_Read_Sequence(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DDR_Read_Sequence"
            
            def description(self):
                return "DDR Write Sequence"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DDR_Read_Packet_Length"] = _AF6CCI0012_RD_MPIG._dum3_pen._DDR_Read_Packet_Length()
            allFields["DDR_Read_Queue_ID"] = _AF6CCI0012_RD_MPIG._dum3_pen._DDR_Read_Queue_ID()
            allFields["DDR_Read_Sequence"] = _AF6CCI0012_RD_MPIG._dum3_pen._DDR_Read_Sequence()
            return allFields

    class _cfg0qthsh_pen(AtRegister.AtRegister):
        def name(self):
            return "Queue Threshold Control"
    
        def description(self):
            return "Used to configure threshold for Queue : {qid(0-7)} -> 8 Queues HDL_PATH: rtlmpig.qmxff.cfg0mem.ram.ram[$qid]"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1000+$qid"
            
        def startAddress(self):
            return 0x00001000
            
        def endAddress(self):
            return 0x00001007

        class _Queue_Hyb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "Queue_Hyb"
            
            def description(self):
                return "Hybrib Queueing 0: Internal Queue Only (256 packets per Queue) 1: Hybrib Queueing, External (256K packets per Queue) , Internal Queue (256 packets per Queue)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Queue_Enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "Queue_Enb"
            
            def description(self):
                return "Queue Enable 0: Disable Queue and flush Queue 1: Enable Queue"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _EFFQthsh_min(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 10
        
            def name(self):
                return "EFFQthsh_min"
            
            def description(self):
                return "External FiFo minimun queue threshold, step 32 pkts"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _IFFQthsh_min(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IFFQthsh_min"
            
            def description(self):
                return "Internal FiFo minimun queue threshold, step 1 pkts"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Queue_Hyb"] = _AF6CCI0012_RD_MPIG._cfg0qthsh_pen._Queue_Hyb()
            allFields["Queue_Enb"] = _AF6CCI0012_RD_MPIG._cfg0qthsh_pen._Queue_Enb()
            allFields["EFFQthsh_min"] = _AF6CCI0012_RD_MPIG._cfg0qthsh_pen._EFFQthsh_min()
            allFields["IFFQthsh_min"] = _AF6CCI0012_RD_MPIG._cfg0qthsh_pen._IFFQthsh_min()
            return allFields

    class _schelcfg0_pen(AtRegister.AtRegister):
        def name(self):
            return "Scheduler: Group priority Configuration"
    
        def description(self):
            return "Used to configure the priority queue - Support 3 group: Low Priority(LSP), High Priority(HSP), DWRR, mix mode. - HSP: Higest Priority - LSP: Lowest Priority - DWRR: Medium Priority"
            
        def width(self):
            return 24
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0080"
            
        def startAddress(self):
            return 0x00000080
            
        def endAddress(self):
            return 0xffffffff

        class _Hsp_grp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Hsp_grp"
            
            def description(self):
                return "HSP Queue"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _DWRR_grp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "DWRR_grp"
            
            def description(self):
                return "DWRR Queue"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Lsp_grp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Lsp_grp"
            
            def description(self):
                return "LSP Queue"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hsp_grp"] = _AF6CCI0012_RD_MPIG._schelcfg0_pen._Hsp_grp()
            allFields["DWRR_grp"] = _AF6CCI0012_RD_MPIG._schelcfg0_pen._DWRR_grp()
            allFields["Lsp_grp"] = _AF6CCI0012_RD_MPIG._schelcfg0_pen._Lsp_grp()
            return allFields

    class _schelinkicfg1_pen(AtRegister.AtRegister):
        def name(self):
            return "DWRR Quantum Configuration"
    
        def description(self):
            return "Used to configure quantum for queue in DWRR scheduling mode HDL_PATH: rtlmpig.schel.lbitmap.cfg0mem.ram.ram[$qid]"
            
        def width(self):
            return 20
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0000+$qid"
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0x00000007

        class _Linkshce_DWRR_QTum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Linkshce_DWRR_QTum"
            
            def description(self):
                return "DWRR Quantum,Minximun MTU"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Linkshce_DWRR_QTum"] = _AF6CCI0012_RD_MPIG._schelinkicfg1_pen._Linkshce_DWRR_QTum()
            return allFields

    class _sta0_upen(AtRegister.AtRegister):
        def name(self):
            return "HW Internal Queue Status"
    
        def description(self):
            return "Used to read HW External Queue Status : {qid(0-7)} -> 8 Queues"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x29010+$qid"
            
        def startAddress(self):
            return 0x00029010
            
        def endAddress(self):
            return 0x00029017

        class _IFFQ_Write_Cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 19
        
            def name(self):
                return "IFFQ_Write_Cnt"
            
            def description(self):
                return "Internal FiFo Queue Write Counter"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _IFFQ_Write_Len(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 10
        
            def name(self):
                return "IFFQ_Write_Len"
            
            def description(self):
                return "Internal FiFo Queue Write Lenght"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _IFFQ_Read_Len(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 1
        
            def name(self):
                return "IFFQ_Read_Len"
            
            def description(self):
                return "Internal FiFo Queue Read Lenght"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _IFFQ_Ready(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IFFQ_Ready"
            
            def description(self):
                return "Internal FiFo Queue Ready"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IFFQ_Write_Cnt"] = _AF6CCI0012_RD_MPIG._sta0_upen._IFFQ_Write_Cnt()
            allFields["IFFQ_Write_Len"] = _AF6CCI0012_RD_MPIG._sta0_upen._IFFQ_Write_Len()
            allFields["IFFQ_Read_Len"] = _AF6CCI0012_RD_MPIG._sta0_upen._IFFQ_Read_Len()
            allFields["IFFQ_Ready"] = _AF6CCI0012_RD_MPIG._sta0_upen._IFFQ_Ready()
            return allFields

    class _sta1_upen(AtRegister.AtRegister):
        def name(self):
            return "HW External Queue Status"
    
        def description(self):
            return "Used to read HW External Queue Status : {qid(0-7)} -> 8 Queues"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x29018+$qid"
            
        def startAddress(self):
            return 0x00029018
            
        def endAddress(self):
            return 0x0002901f

        class _EFFQ_Write_Pen_Len(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 18
        
            def name(self):
                return "EFFQ_Write_Pen_Len"
            
            def description(self):
                return "External FiFo Queue Write Pending Lenght"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _EFFQ_Read_Pen_Len(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 14
        
            def name(self):
                return "EFFQ_Read_Pen_Len"
            
            def description(self):
                return "External FiFo Queue Read Pending Lenght"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _EFFQ_Buf_Len(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "EFFQ_Buf_Len"
            
            def description(self):
                return "External FiFo Queue DDR Buffer Lenght"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["EFFQ_Write_Pen_Len"] = _AF6CCI0012_RD_MPIG._sta1_upen._EFFQ_Write_Pen_Len()
            allFields["EFFQ_Read_Pen_Len"] = _AF6CCI0012_RD_MPIG._sta1_upen._EFFQ_Read_Pen_Len()
            allFields["EFFQ_Buf_Len"] = _AF6CCI0012_RD_MPIG._sta1_upen._EFFQ_Buf_Len()
            return allFields

    class _pmcnt1_pen(AtRegister.AtRegister):
        def name(self):
            return "Queue Packet Counter"
    
        def description(self):
            return "Support the following Queue Counter: - cntid = 00 : Total good packet enqueue - cntid = 01 : Internal Packet bypass to Internal FiFo directly - cntid = 02 : Internal Packet bypass to Internal FiFo from Taill FiFo - cntid = 03 : External Packet to Internal FiFo - cntid = 04 : Discard because Queue Disable CFG - cntid = 05 : Discard because Input(PLA,write cache) delare drop - cntid = 06 : Discard because Internal FiFo full - cntid = 07 : Discard because External FiFo full"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1400+ $r2c*64+$qid*8+$cntid"
            
        def startAddress(self):
            return 0x00001400
            
        def endAddress(self):
            return 0x0000147f

        class _queue_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "queue_cnt"
            
            def description(self):
                return "Value of counter"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["queue_cnt"] = _AF6CCI0012_RD_MPIG._pmcnt1_pen._queue_cnt()
            return allFields

    class _pmcnt2_pen(AtRegister.AtRegister):
        def name(self):
            return "Queue Byte Counter"
    
        def description(self):
            return "Support the following Queue Counter: - cntid = 00 : Total good packet enqueue - cntid = 01 : Internal Packet bypass to Internal FiFo directly - cntid = 02 : Internal Packet bypass to Internal FiFo from Taill FiFo - cntid = 03 : External Packet to Internal FiFo - cntid = 04 : Discard because Queue Disable CFG - cntid = 05 : Discard because Input(PLA,write cache) delare drop - cntid = 06 : Discard because Internal FiFo full - cntid = 07 : Discard because External FiFo full"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1500+ $r2c*64+$qid*8+$cntid"
            
        def startAddress(self):
            return 0x00001500
            
        def endAddress(self):
            return 0x0000157f

        class _queue_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "queue_cnt"
            
            def description(self):
                return "Value of counter"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["queue_cnt"] = _AF6CCI0012_RD_MPIG._pmcnt2_pen._queue_cnt()
            return allFields

    class _pmcnt3_pen(AtRegister.AtRegister):
        def name(self):
            return "Queue Byte Counter"
    
        def description(self):
            return "Support the following Queue Counter: - cntid = 00 : Total good packet dequeue - cntid = 01 : Total good end of packetdequeue - cntid = 02 : Total good byte dequeue - cntid = 03 : Reserved - cntid = 04 : Reserved - cntid = 05 : Reserved - cntid = 06 : Reserved - cntid = 07 : Reserved"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1600+ $r2c*64+$qid*8+$cntid"
            
        def startAddress(self):
            return 0x00001600
            
        def endAddress(self):
            return 0x0000167f

        class _queue_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "queue_cnt"
            
            def description(self):
                return "Value of counter"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["queue_cnt"] = _AF6CCI0012_RD_MPIG._pmcnt3_pen._queue_cnt()
            return allFields

    class _pmcnt4_pen(AtRegister.AtRegister):
        def name(self):
            return "DDR Access Queue Counter"
    
        def description(self):
            return "Support Global Counter: - cntid = 00 : MPIG enqueue Good Packet - cntid = 01 : MPIG enqueue Drop Packet - cntid = 02 : MPIG dequeue Good Packet - cntid = 03 : MPIG request Write DDR - cntid = 04 : MPIG request Read DDR - cntid = 05 : MPIG Valid Write DDR - cntid = 06 : MPIG Valid Read DDR - cntid = 07 : MPIG ACK DDR - cntid = 08 : QMXFF Write get Valid - cntid = 09 : QMXFF Read get Valid(=3xMPIG request Read DDR) - cntid = 10 : QMXFF Read get first Valid - cntid = 11 : QMXFF Read get end Valid - cntid = 12 : Total Packet to Head internal FiFo - cntid = 13 : Packet Directly to head FiFo - cntid = 14 : Packet from tail FiFo to head FiFo - cntid = 15 : Total Discard"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9020+ $cntid"
            
        def startAddress(self):
            return 0x00009020
            
        def endAddress(self):
            return 0x0000902f

        class _queue_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "queue_cnt"
            
            def description(self):
                return "Value of counter"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["queue_cnt"] = _AF6CCI0012_RD_MPIG._pmcnt4_pen._queue_cnt()
            return allFields

    class _stk0_pen(AtRegister.AtRegister):
        def name(self):
            return "Stick 0"
    
        def description(self):
            return "Used to debug MPEG"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00008000
            
        def endAddress(self):
            return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _RSQ_Valid_Error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "RSQ_Valid_Error"
            
            def description(self):
                return "RSQ Valid Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MPIG_get_RSQ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "MPIG_get_RSQ"
            
            def description(self):
                return "MPIG_get_RSQ"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _RSQ_Valid_SOP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "RSQ_Valid_SOP"
            
            def description(self):
                return "RSQ valid SOP"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _RSQ_Valid_EOP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "RSQ_Valid_EOP"
            
            def description(self):
                return "RSQ valid EOP"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FiFo_Enqueue_Write_Error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "FiFo_Enqueue_Write_Error"
            
            def description(self):
                return "FiFo store enqueue QMXXF Write Err"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FiFo_Enqueue_Read_Error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "FiFo_Enqueue_Read_Error"
            
            def description(self):
                return "FiFo store enqueue QMXXF Read Err"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _DDR_Read_Cache_Write(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "DDR_Read_Cache_Write"
            
            def description(self):
                return "DDR Read Cache Write"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _DDR_Write_Cache_Read(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "DDR_Write_Cache_Read"
            
            def description(self):
                return "DDR Write Cache Read"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _DDR_Return_Write_Valid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "DDR_Return_Write_Valid"
            
            def description(self):
                return "DDR return Write Valid to MPIG"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MPIG_Request_Write(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "MPIG_Request_Write"
            
            def description(self):
                return "MPIG request Write DDR"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MPIG_Request_Read(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "MPIG_Request_Read"
            
            def description(self):
                return "MPIG request Read  DDR"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _DDR_Return_ACK(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "DDR_Return_ACK"
            
            def description(self):
                return "DDR return ACK to MPIG"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _DDR_Return_Read_Valid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "DDR_Return_Read_Valid"
            
            def description(self):
                return "DDR return Read  Valid to MPIG"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MPIG_Output_to_RSQ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "MPIG_Output_to_RSQ"
            
            def description(self):
                return "MPIG output to RSQ engine"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _PLA_Error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PLA_Error"
            
            def description(self):
                return "PLA declare error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _RSQ_ready_to_Get(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "RSQ_ready_to_Get"
            
            def description(self):
                return "RSQ ready to get"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _PLA_Request_Read_Enable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PLA_Request_Read_Enable"
            
            def description(self):
                return "PLA request read enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MPIG_ouput_to_PLA(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "MPIG_ouput_to_PLA"
            
            def description(self):
                return "MPIG output to PLA"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _PLA_Enqueue_Enable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PLA_Enqueue_Enable"
            
            def description(self):
                return "PLA Enqueue to MPIG"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk0_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk0_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk0_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk0_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk0_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk0_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk0_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk0_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk0_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk0_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk0_pen._Reserved()
            allFields["RSQ_Valid_Error"] = _AF6CCI0012_RD_MPIG._stk0_pen._RSQ_Valid_Error()
            allFields["MPIG_get_RSQ"] = _AF6CCI0012_RD_MPIG._stk0_pen._MPIG_get_RSQ()
            allFields["RSQ_Valid_SOP"] = _AF6CCI0012_RD_MPIG._stk0_pen._RSQ_Valid_SOP()
            allFields["RSQ_Valid_EOP"] = _AF6CCI0012_RD_MPIG._stk0_pen._RSQ_Valid_EOP()
            allFields["FiFo_Enqueue_Write_Error"] = _AF6CCI0012_RD_MPIG._stk0_pen._FiFo_Enqueue_Write_Error()
            allFields["FiFo_Enqueue_Read_Error"] = _AF6CCI0012_RD_MPIG._stk0_pen._FiFo_Enqueue_Read_Error()
            allFields["DDR_Read_Cache_Write"] = _AF6CCI0012_RD_MPIG._stk0_pen._DDR_Read_Cache_Write()
            allFields["DDR_Write_Cache_Read"] = _AF6CCI0012_RD_MPIG._stk0_pen._DDR_Write_Cache_Read()
            allFields["DDR_Return_Write_Valid"] = _AF6CCI0012_RD_MPIG._stk0_pen._DDR_Return_Write_Valid()
            allFields["MPIG_Request_Write"] = _AF6CCI0012_RD_MPIG._stk0_pen._MPIG_Request_Write()
            allFields["MPIG_Request_Read"] = _AF6CCI0012_RD_MPIG._stk0_pen._MPIG_Request_Read()
            allFields["DDR_Return_ACK"] = _AF6CCI0012_RD_MPIG._stk0_pen._DDR_Return_ACK()
            allFields["DDR_Return_Read_Valid"] = _AF6CCI0012_RD_MPIG._stk0_pen._DDR_Return_Read_Valid()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk0_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk0_pen._Reserved()
            allFields["MPIG_Output_to_RSQ"] = _AF6CCI0012_RD_MPIG._stk0_pen._MPIG_Output_to_RSQ()
            allFields["PLA_Error"] = _AF6CCI0012_RD_MPIG._stk0_pen._PLA_Error()
            allFields["RSQ_ready_to_Get"] = _AF6CCI0012_RD_MPIG._stk0_pen._RSQ_ready_to_Get()
            allFields["PLA_Request_Read_Enable"] = _AF6CCI0012_RD_MPIG._stk0_pen._PLA_Request_Read_Enable()
            allFields["MPIG_ouput_to_PLA"] = _AF6CCI0012_RD_MPIG._stk0_pen._MPIG_ouput_to_PLA()
            allFields["PLA_Enqueue_Enable"] = _AF6CCI0012_RD_MPIG._stk0_pen._PLA_Enqueue_Enable()
            return allFields

    class _stk1en(AtRegister.AtRegister):
        def name(self):
            return "Stick 1"
    
        def description(self):
            return "Used to debug MPEG"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00008001
            
        def endAddress(self):
            return 0xffffffff

        class _Sche_DWRRCurrent_Queue7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "Sche_DWRRCurrent_Queue7"
            
            def description(self):
                return "Scheduler DWRRCurrent Queue 7"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sche_DWRRCurrent_Queue6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "Sche_DWRRCurrent_Queue6"
            
            def description(self):
                return "Scheduler DWRRCurrent Queue 6"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sche_DWRRCurrent_Queue5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "Sche_DWRRCurrent_Queue5"
            
            def description(self):
                return "Scheduler DWRRCurrent Queue 5"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sche_DWRRCurrent_Queue4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "Sche_DWRRCurrent_Queue4"
            
            def description(self):
                return "Scheduler DWRRCurrent Queue 4"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sche_DWRRCurrent_Queue3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "Sche_DWRRCurrent_Queue3"
            
            def description(self):
                return "Scheduler DWRRCurrent Queue 3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sche_DWRRCurrent_Queue2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "Sche_DWRRCurrent_Queue2"
            
            def description(self):
                return "Scheduler DWRRCurrent Queue 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sche_DWRRCurrent_Queue1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "Sche_DWRRCurrent_Queue1"
            
            def description(self):
                return "Scheduler DWRRCurrent Queue 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sche_DWRRCurrent_Queue0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "Sche_DWRRCurrent_Queue0"
            
            def description(self):
                return "Scheduler DWRRCurrent Queue 0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sche_HSPCurrent_Queue7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "Sche_HSPCurrent_Queue7"
            
            def description(self):
                return "Scheduler HSPCurrent Queue 7"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sche_HSPCurrent_Queue6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "Sche_HSPCurrent_Queue6"
            
            def description(self):
                return "Scheduler HSPCurrent Queue 6"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sche_HSPCurrent_Queue5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "Sche_HSPCurrent_Queue5"
            
            def description(self):
                return "Scheduler HSPCurrent Queue 5"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sche_HSPCurrent_Queue4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "Sche_HSPCurrent_Queue4"
            
            def description(self):
                return "Scheduler HSPCurrent Queue 4"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sche_HSPCurrent_Queue3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "Sche_HSPCurrent_Queue3"
            
            def description(self):
                return "Scheduler HSPCurrent Queue 3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sche_HSPCurrent_Queue2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "Sche_HSPCurrent_Queue2"
            
            def description(self):
                return "Scheduler HSPCurrent Queue 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sche_HSPCurrent_Queue1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "Sche_HSPCurrent_Queue1"
            
            def description(self):
                return "Scheduler HSPCurrent Queue 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sche_HSPCurrent_Queue0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Sche_HSPCurrent_Queue0"
            
            def description(self):
                return "Scheduler HSPCurrent Queue 0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sche_Current_Queue7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "Sche_Current_Queue7"
            
            def description(self):
                return "Scheduler Current Queue 7"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sche_Current_Queue6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "Sche_Current_Queue6"
            
            def description(self):
                return "Scheduler Current Queue 6"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sche_Current_Queue5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "Sche_Current_Queue5"
            
            def description(self):
                return "Scheduler Current Queue 5"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sche_Current_Queue4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Sche_Current_Queue4"
            
            def description(self):
                return "Scheduler Current Queue 4"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sche_Current_Queue3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "Sche_Current_Queue3"
            
            def description(self):
                return "Scheduler Current Queue 3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sche_Current_Queue2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "Sche_Current_Queue2"
            
            def description(self):
                return "Scheduler Current Queue 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sche_Current_Queue1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "Sche_Current_Queue1"
            
            def description(self):
                return "Scheduler Current Queue 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sche_Current_Queue0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Sche_Current_Queue0"
            
            def description(self):
                return "Scheduler Current Queue 0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sche_Request_QMXFF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Sche_Request_QMXFF"
            
            def description(self):
                return "Scheduler Request QMXFF"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sche_Return_Valid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Sche_Return_Valid"
            
            def description(self):
                return "Scheduler Return Valid"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sche_Wrempt_Update(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Sche_Wrempt_Update"
            
            def description(self):
                return "Scheduler Write Empty Update"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sche_Sta1_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Sche_Sta1_err"
            
            def description(self):
                return "Schedule Status1 Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sche_Sta0_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Sche_Sta0_err"
            
            def description(self):
                return "Schedule Status0 Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Sche_DWRRCurrent_Queue7"] = _AF6CCI0012_RD_MPIG._stk1en._Sche_DWRRCurrent_Queue7()
            allFields["Sche_DWRRCurrent_Queue6"] = _AF6CCI0012_RD_MPIG._stk1en._Sche_DWRRCurrent_Queue6()
            allFields["Sche_DWRRCurrent_Queue5"] = _AF6CCI0012_RD_MPIG._stk1en._Sche_DWRRCurrent_Queue5()
            allFields["Sche_DWRRCurrent_Queue4"] = _AF6CCI0012_RD_MPIG._stk1en._Sche_DWRRCurrent_Queue4()
            allFields["Sche_DWRRCurrent_Queue3"] = _AF6CCI0012_RD_MPIG._stk1en._Sche_DWRRCurrent_Queue3()
            allFields["Sche_DWRRCurrent_Queue2"] = _AF6CCI0012_RD_MPIG._stk1en._Sche_DWRRCurrent_Queue2()
            allFields["Sche_DWRRCurrent_Queue1"] = _AF6CCI0012_RD_MPIG._stk1en._Sche_DWRRCurrent_Queue1()
            allFields["Sche_DWRRCurrent_Queue0"] = _AF6CCI0012_RD_MPIG._stk1en._Sche_DWRRCurrent_Queue0()
            allFields["Sche_HSPCurrent_Queue7"] = _AF6CCI0012_RD_MPIG._stk1en._Sche_HSPCurrent_Queue7()
            allFields["Sche_HSPCurrent_Queue6"] = _AF6CCI0012_RD_MPIG._stk1en._Sche_HSPCurrent_Queue6()
            allFields["Sche_HSPCurrent_Queue5"] = _AF6CCI0012_RD_MPIG._stk1en._Sche_HSPCurrent_Queue5()
            allFields["Sche_HSPCurrent_Queue4"] = _AF6CCI0012_RD_MPIG._stk1en._Sche_HSPCurrent_Queue4()
            allFields["Sche_HSPCurrent_Queue3"] = _AF6CCI0012_RD_MPIG._stk1en._Sche_HSPCurrent_Queue3()
            allFields["Sche_HSPCurrent_Queue2"] = _AF6CCI0012_RD_MPIG._stk1en._Sche_HSPCurrent_Queue2()
            allFields["Sche_HSPCurrent_Queue1"] = _AF6CCI0012_RD_MPIG._stk1en._Sche_HSPCurrent_Queue1()
            allFields["Sche_HSPCurrent_Queue0"] = _AF6CCI0012_RD_MPIG._stk1en._Sche_HSPCurrent_Queue0()
            allFields["Sche_Current_Queue7"] = _AF6CCI0012_RD_MPIG._stk1en._Sche_Current_Queue7()
            allFields["Sche_Current_Queue6"] = _AF6CCI0012_RD_MPIG._stk1en._Sche_Current_Queue6()
            allFields["Sche_Current_Queue5"] = _AF6CCI0012_RD_MPIG._stk1en._Sche_Current_Queue5()
            allFields["Sche_Current_Queue4"] = _AF6CCI0012_RD_MPIG._stk1en._Sche_Current_Queue4()
            allFields["Sche_Current_Queue3"] = _AF6CCI0012_RD_MPIG._stk1en._Sche_Current_Queue3()
            allFields["Sche_Current_Queue2"] = _AF6CCI0012_RD_MPIG._stk1en._Sche_Current_Queue2()
            allFields["Sche_Current_Queue1"] = _AF6CCI0012_RD_MPIG._stk1en._Sche_Current_Queue1()
            allFields["Sche_Current_Queue0"] = _AF6CCI0012_RD_MPIG._stk1en._Sche_Current_Queue0()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk1en._Reserved()
            allFields["Sche_Request_QMXFF"] = _AF6CCI0012_RD_MPIG._stk1en._Sche_Request_QMXFF()
            allFields["Sche_Return_Valid"] = _AF6CCI0012_RD_MPIG._stk1en._Sche_Return_Valid()
            allFields["Sche_Wrempt_Update"] = _AF6CCI0012_RD_MPIG._stk1en._Sche_Wrempt_Update()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk1en._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk1en._Reserved()
            allFields["Sche_Sta1_err"] = _AF6CCI0012_RD_MPIG._stk1en._Sche_Sta1_err()
            allFields["Sche_Sta0_err"] = _AF6CCI0012_RD_MPIG._stk1en._Sche_Sta0_err()
            return allFields

    class _stk2pen(AtRegister.AtRegister):
        def name(self):
            return "Stick 2"
    
        def description(self):
            return "Used to debug MPEG"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00008002
            
        def endAddress(self):
            return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MPIG_Discard(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "MPIG_Discard"
            
            def description(self):
                return "MPEG Discard to PLA"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Output_Internal_Q7_Seq_Fail(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "Output_Internal_Q7_Seq_Fail"
            
            def description(self):
                return "Output Internal Queue0 Sequence Fail"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Output_Internal_Q6_Seq_Fail(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "Output_Internal_Q6_Seq_Fail"
            
            def description(self):
                return "Output Internal Queue1 Sequence Fail"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Output_Internal_Q5_Seq_Fail(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "Output_Internal_Q5_Seq_Fail"
            
            def description(self):
                return "Output Internal Queue2 Sequence Fail"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Output_Internal_Q4_Seq_Fail(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Output_Internal_Q4_Seq_Fail"
            
            def description(self):
                return "Output Internal Queue3 Sequence Fail"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Output_Internal_Q3_Seq_Fail(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "Output_Internal_Q3_Seq_Fail"
            
            def description(self):
                return "Output Internal Queue4 Sequence Fail"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Output_Internal_Q2_Seq_Fail(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "Output_Internal_Q2_Seq_Fail"
            
            def description(self):
                return "Output Internal Queue5 Sequence Fail"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Output_Internal_Q1_Seq_Fail(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "Output_Internal_Q1_Seq_Fail"
            
            def description(self):
                return "Output Internal Queue6 Sequence Fail"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Output_Internal_Q0_Seq_Fail(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Output_Internal_Q0_Seq_Fail"
            
            def description(self):
                return "Output Internal Queue7 Sequence Fail"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Output_External_Q7_Seq_Fail(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "Output_External_Q7_Seq_Fail"
            
            def description(self):
                return "Output External Queue0 Sequence Fail"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Output_External_Q6_Seq_Fail(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Output_External_Q6_Seq_Fail"
            
            def description(self):
                return "Output External Queue1 Sequence Fail"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Output_External_Q5_Seq_Fail(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Output_External_Q5_Seq_Fail"
            
            def description(self):
                return "Output External Queue2 Sequence Fail"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Output_External_Q4_Seq_Fail(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Output_External_Q4_Seq_Fail"
            
            def description(self):
                return "Output External Queue3 Sequence Fail"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Output_External_Q3_Seq_Fail(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "Output_External_Q3_Seq_Fail"
            
            def description(self):
                return "Output External Queue4 Sequence Fail"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Output_External_Q2_Seq_Fail(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Output_External_Q2_Seq_Fail"
            
            def description(self):
                return "Output External Queue5 Sequence Fail"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Output_External_Q1_Seq_Fail(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Output_External_Q1_Seq_Fail"
            
            def description(self):
                return "Output External Queue6 Sequence Fail"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Output_External_Q0_Seq_Fail(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Output_External_Q0_Seq_Fail"
            
            def description(self):
                return "Output External Queue7 Sequence Fail"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk2pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk2pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk2pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk2pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk2pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk2pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk2pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk2pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk2pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk2pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk2pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk2pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk2pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk2pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk2pen._Reserved()
            allFields["MPIG_Discard"] = _AF6CCI0012_RD_MPIG._stk2pen._MPIG_Discard()
            allFields["Output_Internal_Q7_Seq_Fail"] = _AF6CCI0012_RD_MPIG._stk2pen._Output_Internal_Q7_Seq_Fail()
            allFields["Output_Internal_Q6_Seq_Fail"] = _AF6CCI0012_RD_MPIG._stk2pen._Output_Internal_Q6_Seq_Fail()
            allFields["Output_Internal_Q5_Seq_Fail"] = _AF6CCI0012_RD_MPIG._stk2pen._Output_Internal_Q5_Seq_Fail()
            allFields["Output_Internal_Q4_Seq_Fail"] = _AF6CCI0012_RD_MPIG._stk2pen._Output_Internal_Q4_Seq_Fail()
            allFields["Output_Internal_Q3_Seq_Fail"] = _AF6CCI0012_RD_MPIG._stk2pen._Output_Internal_Q3_Seq_Fail()
            allFields["Output_Internal_Q2_Seq_Fail"] = _AF6CCI0012_RD_MPIG._stk2pen._Output_Internal_Q2_Seq_Fail()
            allFields["Output_Internal_Q1_Seq_Fail"] = _AF6CCI0012_RD_MPIG._stk2pen._Output_Internal_Q1_Seq_Fail()
            allFields["Output_Internal_Q0_Seq_Fail"] = _AF6CCI0012_RD_MPIG._stk2pen._Output_Internal_Q0_Seq_Fail()
            allFields["Output_External_Q7_Seq_Fail"] = _AF6CCI0012_RD_MPIG._stk2pen._Output_External_Q7_Seq_Fail()
            allFields["Output_External_Q6_Seq_Fail"] = _AF6CCI0012_RD_MPIG._stk2pen._Output_External_Q6_Seq_Fail()
            allFields["Output_External_Q5_Seq_Fail"] = _AF6CCI0012_RD_MPIG._stk2pen._Output_External_Q5_Seq_Fail()
            allFields["Output_External_Q4_Seq_Fail"] = _AF6CCI0012_RD_MPIG._stk2pen._Output_External_Q4_Seq_Fail()
            allFields["Output_External_Q3_Seq_Fail"] = _AF6CCI0012_RD_MPIG._stk2pen._Output_External_Q3_Seq_Fail()
            allFields["Output_External_Q2_Seq_Fail"] = _AF6CCI0012_RD_MPIG._stk2pen._Output_External_Q2_Seq_Fail()
            allFields["Output_External_Q1_Seq_Fail"] = _AF6CCI0012_RD_MPIG._stk2pen._Output_External_Q1_Seq_Fail()
            allFields["Output_External_Q0_Seq_Fail"] = _AF6CCI0012_RD_MPIG._stk2pen._Output_External_Q0_Seq_Fail()
            return allFields

    class _stk3en(AtRegister.AtRegister):
        def name(self):
            return "Stick 3"
    
        def description(self):
            return "Used to debug MPEG"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00008003
            
        def endAddress(self):
            return 0xffffffff

        class _Input_PLA_length_Fail(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "Input_PLA_length_Fail"
            
            def description(self):
                return "Input PLA Length Fail"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Ouput_PLA_Length_Fail(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "Ouput_PLA_Length_Fail"
            
            def description(self):
                return "Output PLA Length Fail"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FiFo_Enqueue_Write_Error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "FiFo_Enqueue_Write_Error"
            
            def description(self):
                return "FiFo store enqueue QMXXF Write Err"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FiFo_Enqueue_Read_Error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "FiFo_Enqueue_Read_Error"
            
            def description(self):
                return "FiFo store enqueue QMXXF Read Err"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _External_Engine_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "External_Engine_Err"
            
            def description(self):
                return "External FiFO Engine Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _External_Write_Req_Buff_Full(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "External_Write_Req_Buff_Full"
            
            def description(self):
                return "External Write Request Buffer Full"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _External_Write_Valid_Buff_Full(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "External_Write_Valid_Buff_Full"
            
            def description(self):
                return "External Write Valid but Buffer Full"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _External_Read_Req_Cache_Full(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "External_Read_Req_Cache_Full"
            
            def description(self):
                return "External Read  Request Cache  Full"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _External_Write_Req_Cache_Full(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "External_Write_Req_Cache_Full"
            
            def description(self):
                return "External Write Request Cache  Full"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Head_FiFo_Write_Error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "Head_FiFo_Write_Error"
            
            def description(self):
                return "Head FiFO Write Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Head_FiFo_Read_Error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "Head_FiFo_Read_Error"
            
            def description(self):
                return "Head FiFO Read  Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FiFO_Request_DDR_Write_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "FiFO_Request_DDR_Write_Err"
            
            def description(self):
                return "FiFo Request DDR Write Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FiFO_Request_DDR_Read_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "FiFO_Request_DDR_Read_Err"
            
            def description(self):
                return "FiFo Request DDR Read  Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _External_FiFO_Engine_Conflict(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "External_FiFO_Engine_Conflict"
            
            def description(self):
                return "External FiFo Engine Conflict"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _QMXFF_Read_Cache_Error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "QMXFF_Read_Cache_Error"
            
            def description(self):
                return "QMXFF Read Cache Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _External_FiFO_Full_Thres_CFG(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "External_FiFO_Full_Thres_CFG"
            
            def description(self):
                return "External FiFo Full Threshold CFG"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FiFo_DDR_Valid_Write_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "FiFo_DDR_Valid_Write_Err"
            
            def description(self):
                return "FiFo store DDR Valid Write Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Internal_FiFO_Engine_Conflict(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "Internal_FiFO_Engine_Conflict"
            
            def description(self):
                return "Internal FiFo Engine Conflict"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Discard_Queue_CFG_Disable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Discard_Queue_CFG_Disable"
            
            def description(self):
                return "Discard becasue Queue Disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Discard_PLA_Drop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Discard_PLA_Drop"
            
            def description(self):
                return "Discard because PLA declare Drop"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Internal_FiFO_Write_Conflict(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Internal_FiFO_Write_Conflict"
            
            def description(self):
                return "Internal FiFo Write Conflict"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FiFo_DDR_Valid_Read_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FiFo_DDR_Valid_Read_Err"
            
            def description(self):
                return "FiFo store DDR Valid Read Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Discard_Head_FiFo_Write_Error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Discard_Head_FiFo_Write_Error"
            
            def description(self):
                return "Discard Because Head FiFO write Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Discard_Internal_External_Full0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Discard_Internal_External_Full0"
            
            def description(self):
                return "Discard Because Internal External Full0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Discard_Internal_External_Full1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Discard_Internal_External_Full1"
            
            def description(self):
                return "Discard Because Internal External Full1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Input_PLA_length_Fail"] = _AF6CCI0012_RD_MPIG._stk3en._Input_PLA_length_Fail()
            allFields["Ouput_PLA_Length_Fail"] = _AF6CCI0012_RD_MPIG._stk3en._Ouput_PLA_Length_Fail()
            allFields["FiFo_Enqueue_Write_Error"] = _AF6CCI0012_RD_MPIG._stk3en._FiFo_Enqueue_Write_Error()
            allFields["FiFo_Enqueue_Read_Error"] = _AF6CCI0012_RD_MPIG._stk3en._FiFo_Enqueue_Read_Error()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk3en._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk3en._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk3en._Reserved()
            allFields["External_Engine_Err"] = _AF6CCI0012_RD_MPIG._stk3en._External_Engine_Err()
            allFields["External_Write_Req_Buff_Full"] = _AF6CCI0012_RD_MPIG._stk3en._External_Write_Req_Buff_Full()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk3en._Reserved()
            allFields["External_Write_Valid_Buff_Full"] = _AF6CCI0012_RD_MPIG._stk3en._External_Write_Valid_Buff_Full()
            allFields["External_Read_Req_Cache_Full"] = _AF6CCI0012_RD_MPIG._stk3en._External_Read_Req_Cache_Full()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk3en._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk3en._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPIG._stk3en._Reserved()
            allFields["External_Write_Req_Cache_Full"] = _AF6CCI0012_RD_MPIG._stk3en._External_Write_Req_Cache_Full()
            allFields["Head_FiFo_Write_Error"] = _AF6CCI0012_RD_MPIG._stk3en._Head_FiFo_Write_Error()
            allFields["Head_FiFo_Read_Error"] = _AF6CCI0012_RD_MPIG._stk3en._Head_FiFo_Read_Error()
            allFields["FiFO_Request_DDR_Write_Err"] = _AF6CCI0012_RD_MPIG._stk3en._FiFO_Request_DDR_Write_Err()
            allFields["FiFO_Request_DDR_Read_Err"] = _AF6CCI0012_RD_MPIG._stk3en._FiFO_Request_DDR_Read_Err()
            allFields["External_FiFO_Engine_Conflict"] = _AF6CCI0012_RD_MPIG._stk3en._External_FiFO_Engine_Conflict()
            allFields["QMXFF_Read_Cache_Error"] = _AF6CCI0012_RD_MPIG._stk3en._QMXFF_Read_Cache_Error()
            allFields["External_FiFO_Full_Thres_CFG"] = _AF6CCI0012_RD_MPIG._stk3en._External_FiFO_Full_Thres_CFG()
            allFields["FiFo_DDR_Valid_Write_Err"] = _AF6CCI0012_RD_MPIG._stk3en._FiFo_DDR_Valid_Write_Err()
            allFields["Internal_FiFO_Engine_Conflict"] = _AF6CCI0012_RD_MPIG._stk3en._Internal_FiFO_Engine_Conflict()
            allFields["Discard_Queue_CFG_Disable"] = _AF6CCI0012_RD_MPIG._stk3en._Discard_Queue_CFG_Disable()
            allFields["Discard_PLA_Drop"] = _AF6CCI0012_RD_MPIG._stk3en._Discard_PLA_Drop()
            allFields["Internal_FiFO_Write_Conflict"] = _AF6CCI0012_RD_MPIG._stk3en._Internal_FiFO_Write_Conflict()
            allFields["FiFo_DDR_Valid_Read_Err"] = _AF6CCI0012_RD_MPIG._stk3en._FiFo_DDR_Valid_Read_Err()
            allFields["Discard_Head_FiFo_Write_Error"] = _AF6CCI0012_RD_MPIG._stk3en._Discard_Head_FiFo_Write_Error()
            allFields["Discard_Internal_External_Full0"] = _AF6CCI0012_RD_MPIG._stk3en._Discard_Internal_External_Full0()
            allFields["Discard_Internal_External_Full1"] = _AF6CCI0012_RD_MPIG._stk3en._Discard_Internal_External_Full1()
            return allFields

import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0012_RD_VCAT(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["vcat5g_version_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_version_pen()
        allRegisters["vcat5g_master_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_master_pen()
        allRegisters["vcat5g_wrhold1_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_wrhold1_pen()
        allRegisters["vcat5g_wrhold2_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_wrhold2_pen()
        allRegisters["vcat5g_rdhold1_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_rdhold1_pen()
        allRegisters["vcat5g_rdhold2_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_rdhold2_pen()
        allRegisters["vcat5g_so_vc4md_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_so_vc4md_pen()
        allRegisters["vcat5g_so_ctrl_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_so_ctrl_pen()
        allRegisters["vcat5g_so_unfrm_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_so_unfrm_pen()
        allRegisters["vcat5g_so_memctrl_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_so_memctrl_pen()
        allRegisters["vcat5g_so_memsta_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_so_memsta_pen()
        allRegisters["vcat5g_so_txseqmem_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_so_txseqmem_pen()
        allRegisters["vcat5g_so_stsxc_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_so_stsxc_pen()
        allRegisters["vcat5g_sk_stsxc_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_stsxc_pen()
        allRegisters["vcat5g_sk_sl0_ctrl_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl0_ctrl_pen()
        allRegisters["vcat5g_sk_sl0_unfrm_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl0_unfrm_pen()
        allRegisters["vcat5g_sk_sl0_mux_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl0_mux_pen()
        allRegisters["vcat5g_sk_sl1_ctrl_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl1_ctrl_pen()
        allRegisters["vcat5g_sk_sl1_unfrm_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl1_unfrm_pen()
        allRegisters["vcat5g_sk_sl1_mux_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl1_mux_pen()
        allRegisters["vcat5g_sk_sl2_ctrl_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl2_ctrl_pen()
        allRegisters["vcat5g_sk_sl2_unfrm_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl2_unfrm_pen()
        allRegisters["vcat5g_sk_sl2_mux_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl2_mux_pen()
        allRegisters["vcat5g_sk_sl3_ctrl_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl3_ctrl_pen()
        allRegisters["vcat5g_sk_sl3_unfrm_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl3_unfrm_pen()
        allRegisters["vcat5g_sk_sl3_mux_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl3_mux_pen()
        allRegisters["vcat5g_sk_memctrl_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_memctrl_pen()
        allRegisters["vcat5g_sk_memsta_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_memsta_pen()
        allRegisters["vcat5g_sk_expseq_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_expseq_pen()
        allRegisters["vcat5g_sk_intrctrl_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_intrctrl_pen()
        allRegisters["vcat5g_sk_intrstk_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_intrstk_pen()
        allRegisters["vcat5g_sk_intrsta_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_intrsta_pen()
        allRegisters["vcat5g_sk_introsta_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_introsta_pen()
        allRegisters["vcat5g_sk_vcintrctrl_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_vcintrctrl_pen()
        allRegisters["vcat5g_sk_vcintrosta_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_vcintrosta_pen()
        allRegisters["vcat5g_dumpctrl_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_dumpctrl_pen()
        allRegisters["vcat5g_dumpdat_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_dumpdat_pen()
        allRegisters["vcat5g_sodbctrl_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_sodbctrl_pen()
        allRegisters["vcat5g_sostk_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_sostk_pen()
        allRegisters["vcat5g_sodat_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_sodat_pen()
        allRegisters["vcat5g_sodatmon_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_sodatmon_pen()
        allRegisters["vcat5g_skstk_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_skstk_pen()
        allRegisters["vcat5g_ddrstk_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_ddrstk_pen()
        allRegisters["vcat5g_ddrblk_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_ddrblk_pen()
        allRegisters["vcat5g_ddrdumpctrl_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_ddrdumpctrl_pen()
        allRegisters["vcat5g_ddrdumpdat_pen"] = _AF6CCI0012_RD_VCAT._vcat5g_ddrdumpdat_pen()
        return allRegisters

    class _vcat5g_version_pen(AtRegister.AtRegister):
        def name(self):
            return "Version ID Register"
    
        def description(self):
            return "This register is version of VCAT block."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _year(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "year"
            
            def description(self):
                return "year"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _week(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 4
        
            def name(self):
                return "week"
            
            def description(self):
                return "week number"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _number(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "number"
            
            def description(self):
                return "number in week"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["year"] = _AF6CCI0012_RD_VCAT._vcat5g_version_pen._year()
            allFields["week"] = _AF6CCI0012_RD_VCAT._vcat5g_version_pen._week()
            allFields["number"] = _AF6CCI0012_RD_VCAT._vcat5g_version_pen._number()
            return allFields

    class _vcat5g_master_pen(AtRegister.AtRegister):
        def name(self):
            return "Master Configuration"
    
        def description(self):
            return "This register control global configuration for MAP."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000001
            
        def endAddress(self):
            return 0xffffffff

        class _VcatLoop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "VcatLoop"
            
            def description(self):
                return "Non-VCAT Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _NonVcatMd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "NonVcatMd"
            
            def description(self):
                return "Non-VCAT Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Active(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Active"
            
            def description(self):
                return "Engine Active"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["VcatLoop"] = _AF6CCI0012_RD_VCAT._vcat5g_master_pen._VcatLoop()
            allFields["NonVcatMd"] = _AF6CCI0012_RD_VCAT._vcat5g_master_pen._NonVcatMd()
            allFields["Active"] = _AF6CCI0012_RD_VCAT._vcat5g_master_pen._Active()
            return allFields

    class _vcat5g_wrhold1_pen(AtRegister.AtRegister):
        def name(self):
            return "Write Access Data Holding 1"
    
        def description(self):
            return "Used to write configuration or status that is greater than 32-bit wide."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0xffffffff

        class _WrDat_Hold1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "WrDat_Hold1"
            
            def description(self):
                return ""
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["WrDat_Hold1"] = _AF6CCI0012_RD_VCAT._vcat5g_wrhold1_pen._WrDat_Hold1()
            return allFields

    class _vcat5g_wrhold2_pen(AtRegister.AtRegister):
        def name(self):
            return "Write Access Data Holding 2"
    
        def description(self):
            return "Used to write configuration or status that is greater than 32-bit wide."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _WrDat_Hold2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "WrDat_Hold2"
            
            def description(self):
                return ""
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["WrDat_Hold2"] = _AF6CCI0012_RD_VCAT._vcat5g_wrhold2_pen._WrDat_Hold2()
            return allFields

    class _vcat5g_rdhold1_pen(AtRegister.AtRegister):
        def name(self):
            return "Read Access Data Holding 1"
    
        def description(self):
            return "Used to read configuration or status that is greater than 32-bit wide."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000004
            
        def endAddress(self):
            return 0xffffffff

        class _RdDat_Hold1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RdDat_Hold1"
            
            def description(self):
                return ""
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RdDat_Hold1"] = _AF6CCI0012_RD_VCAT._vcat5g_rdhold1_pen._RdDat_Hold1()
            return allFields

    class _vcat5g_rdhold2_pen(AtRegister.AtRegister):
        def name(self):
            return "Read Access Data Holding 2"
    
        def description(self):
            return "Used to read configuration or status that is greater than 32-bit wide."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000005
            
        def endAddress(self):
            return 0xffffffff

        class _RdDat_Hold2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RdDat_Hold2"
            
            def description(self):
                return ""
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RdDat_Hold2"] = _AF6CCI0012_RD_VCAT._vcat5g_rdhold2_pen._RdDat_Hold2()
            return allFields

    class _vcat5g_so_vc4md_pen(AtRegister.AtRegister):
        def name(self):
            return "Source VC4 Mode Configuration"
    
        def description(self):
            return "Source VC4 Mode Enable Control. For VC4 mode, if STS master is n, the two slave-STSs will be n+64 and n+128 (where n = 0 - 63)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00021040
            
        def endAddress(self):
            return 0xffffffff

        class _So_VC4_Ena(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "So_VC4_Ena"
            
            def description(self):
                return "Source VC4 Mode Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["So_VC4_Ena"] = _AF6CCI0012_RD_VCAT._vcat5g_so_vc4md_pen._So_VC4_Ena()
            return allFields

    class _vcat5g_so_ctrl_pen(AtRegister.AtRegister):
        def name(self):
            return "Source Mode Configuration"
    
        def description(self):
            return "This register is used to config timing mode for VCAT Source side."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x21200 + $sts_id"
            
        def startAddress(self):
            return 0x00021200
            
        def endAddress(self):
            return 0x0002125f

        class _So_HoMd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 16
        
            def name(self):
                return "So_HoMd"
            
            def description(self):
                return "00: SPE-VC3 Mode, 01: DS3 Mode, 10: E3 Mode, 11: Lo-order Mode (DS1 or E1 or VT1.5 or VT2)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_LoMd_Grp_6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "So_LoMd_Grp_6"
            
            def description(self):
                return "00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_LoMd_Grp_5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "So_LoMd_Grp_5"
            
            def description(self):
                return "00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_LoMd_Grp_4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "So_LoMd_Grp_4"
            
            def description(self):
                return "00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_LoMd_Grp_3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "So_LoMd_Grp_3"
            
            def description(self):
                return "00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_LoMd_Grp_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "So_LoMd_Grp_2"
            
            def description(self):
                return "00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_LoMd_Grp_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "So_LoMd_Grp_1"
            
            def description(self):
                return "00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_LoMd_Grp_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "So_LoMd_Grp_0"
            
            def description(self):
                return "00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["So_HoMd"] = _AF6CCI0012_RD_VCAT._vcat5g_so_ctrl_pen._So_HoMd()
            allFields["So_LoMd_Grp_6"] = _AF6CCI0012_RD_VCAT._vcat5g_so_ctrl_pen._So_LoMd_Grp_6()
            allFields["So_LoMd_Grp_5"] = _AF6CCI0012_RD_VCAT._vcat5g_so_ctrl_pen._So_LoMd_Grp_5()
            allFields["So_LoMd_Grp_4"] = _AF6CCI0012_RD_VCAT._vcat5g_so_ctrl_pen._So_LoMd_Grp_4()
            allFields["So_LoMd_Grp_3"] = _AF6CCI0012_RD_VCAT._vcat5g_so_ctrl_pen._So_LoMd_Grp_3()
            allFields["So_LoMd_Grp_2"] = _AF6CCI0012_RD_VCAT._vcat5g_so_ctrl_pen._So_LoMd_Grp_2()
            allFields["So_LoMd_Grp_1"] = _AF6CCI0012_RD_VCAT._vcat5g_so_ctrl_pen._So_LoMd_Grp_1()
            allFields["So_LoMd_Grp_0"] = _AF6CCI0012_RD_VCAT._vcat5g_so_ctrl_pen._So_LoMd_Grp_0()
            return allFields

    class _vcat5g_so_unfrm_pen(AtRegister.AtRegister):
        def name(self):
            return "Source E1-Unframed Mode Configuration"
    
        def description(self):
            return "This register is used to config mode for VCAT Source side."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x21400 + $sts_id"
            
        def startAddress(self):
            return 0x00021400
            
        def endAddress(self):
            return 0x0002145f

        class _So_E1Unfrm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "So_E1Unfrm"
            
            def description(self):
                return "1: E1-Unframed Mode, 0: E1-Framed Mode (default)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["So_E1Unfrm"] = _AF6CCI0012_RD_VCAT._vcat5g_so_unfrm_pen._So_E1Unfrm()
            return allFields

    class _vcat5g_so_memctrl_pen(AtRegister.AtRegister):
        def name(self):
            return "Source Member Control Configuration"
    
        def description(self):
            return "This register is used to control engine VCAT Source side."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x22000 + $sts_id*32 + $vtg_id*4 + $vt_id"
            
        def startAddress(self):
            return 0x00022000
            
        def endAddress(self):
            return 0x00022bff

        class _So_Mem_Ena(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "So_Mem_Ena"
            
            def description(self):
                return "1: Enable Member, 0: Disable Member"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_G8040_Md(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "So_G8040_Md"
            
            def description(self):
                return "1: ITU G.8040 Mode, 0: ITU G.804 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_Vcat_Md(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "So_Vcat_Md"
            
            def description(self):
                return "1: VCAT Mode, 0: non-VCAT Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_Lcas_Md(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "So_Lcas_Md"
            
            def description(self):
                return "LCAS Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_Nms_Cmd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "So_Nms_Cmd"
            
            def description(self):
                return "Network Management Command"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_VcgId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 0
        
            def name(self):
                return "So_VcgId"
            
            def description(self):
                return "VCG ID 0-511"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["So_Mem_Ena"] = _AF6CCI0012_RD_VCAT._vcat5g_so_memctrl_pen._So_Mem_Ena()
            allFields["So_G8040_Md"] = _AF6CCI0012_RD_VCAT._vcat5g_so_memctrl_pen._So_G8040_Md()
            allFields["So_Vcat_Md"] = _AF6CCI0012_RD_VCAT._vcat5g_so_memctrl_pen._So_Vcat_Md()
            allFields["So_Lcas_Md"] = _AF6CCI0012_RD_VCAT._vcat5g_so_memctrl_pen._So_Lcas_Md()
            allFields["So_Nms_Cmd"] = _AF6CCI0012_RD_VCAT._vcat5g_so_memctrl_pen._So_Nms_Cmd()
            allFields["So_VcgId"] = _AF6CCI0012_RD_VCAT._vcat5g_so_memctrl_pen._So_VcgId()
            return allFields

    class _vcat5g_so_memsta_pen(AtRegister.AtRegister):
        def name(self):
            return "Source Member Status"
    
        def description(self):
            return "This register is used to show status of member at Source side."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x23000 + $sts_id*32 + $vtg_id*4 + $vt_id"
            
        def startAddress(self):
            return 0x00023000
            
        def endAddress(self):
            return 0x00023bff

        class _So_Mem_Fsm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 16
        
            def name(self):
                return "So_Mem_Fsm"
            
            def description(self):
                return "Member FSM"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _So_Mem_Sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "So_Mem_Sta"
            
            def description(self):
                return "Member Status"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _So_Mem_Act(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "So_Mem_Act"
            
            def description(self):
                return "Member Active"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _So_Mem_TxSeq(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 7
        
            def name(self):
                return "So_Mem_TxSeq"
            
            def description(self):
                return "Source Member Sequence Transmitted"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _So_Mem_AcpSeq(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 0
        
            def name(self):
                return "So_Mem_AcpSeq"
            
            def description(self):
                return "Source Member Accept Sequence"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["So_Mem_Fsm"] = _AF6CCI0012_RD_VCAT._vcat5g_so_memsta_pen._So_Mem_Fsm()
            allFields["So_Mem_Sta"] = _AF6CCI0012_RD_VCAT._vcat5g_so_memsta_pen._So_Mem_Sta()
            allFields["So_Mem_Act"] = _AF6CCI0012_RD_VCAT._vcat5g_so_memsta_pen._So_Mem_Act()
            allFields["So_Mem_TxSeq"] = _AF6CCI0012_RD_VCAT._vcat5g_so_memsta_pen._So_Mem_TxSeq()
            allFields["So_Mem_AcpSeq"] = _AF6CCI0012_RD_VCAT._vcat5g_so_memsta_pen._So_Mem_AcpSeq()
            return allFields

    class _vcat5g_so_txseqmem_pen(AtRegister.AtRegister):
        def name(self):
            return "Source Transmit Sequence Configuration"
    
        def description(self):
            return "This register is used to assign sequence transmitted for VCAT mode only."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x24000 + $sts_id*32 + $vtg_id*4 + $vt_id"
            
        def startAddress(self):
            return 0x00024000
            
        def endAddress(self):
            return 0x00024bff

        class _So_TxSeqEna(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "So_TxSeqEna"
            
            def description(self):
                return "0: Disable config, 1: Enable config"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_TxSeq(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 0
        
            def name(self):
                return "So_TxSeq"
            
            def description(self):
                return "Source Member Sequence Transmitted"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["So_TxSeqEna"] = _AF6CCI0012_RD_VCAT._vcat5g_so_txseqmem_pen._So_TxSeqEna()
            allFields["So_TxSeq"] = _AF6CCI0012_RD_VCAT._vcat5g_so_txseqmem_pen._So_TxSeq()
            return allFields

    class _vcat5g_so_stsxc_pen(AtRegister.AtRegister):
        def name(self):
            return "Output OC-24 STS Connection Memory"
    
        def description(self):
            return "Connection Memory for Output 4-Slice OC-24 HDL_PATH     : iso.stsxc_mem.ram.ram[$sts_id]"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x25000 + $sts_id"
            
        def startAddress(self):
            return 0x00025000
            
        def endAddress(self):
            return 0x0002505f

        class _OC24_OutSlice(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "OC24_OutSlice"
            
            def description(self):
                return "Output Slice 0-3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _OC24_OutSTS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "OC24_OutSTS"
            
            def description(self):
                return "Output STS 0-23"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["OC24_OutSlice"] = _AF6CCI0012_RD_VCAT._vcat5g_so_stsxc_pen._OC24_OutSlice()
            allFields["OC24_OutSTS"] = _AF6CCI0012_RD_VCAT._vcat5g_so_stsxc_pen._OC24_OutSTS()
            return allFields

    class _vcat5g_sk_stsxc_pen(AtRegister.AtRegister):
        def name(self):
            return "Input OC-24 STS Connection Memory"
    
        def description(self):
            return "Connection Memory for Input 4-Slice OC-24 to OC-96"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x42000 + $slice_id*32 + $sts_id"
            
        def startAddress(self):
            return 0x00042000
            
        def endAddress(self):
            return 0x0004207f

        class _OC96_OutSTS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 0
        
            def name(self):
                return "OC96_OutSTS"
            
            def description(self):
                return "Output STS 0-95"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["OC96_OutSTS"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_stsxc_pen._OC96_OutSTS()
            return allFields

    class _vcat5g_sk_sl0_ctrl_pen(AtRegister.AtRegister):
        def name(self):
            return "Sink Slice 0 Mode Configuration"
    
        def description(self):
            return "This register is used to config timing mode for VCAT Sink side. HDL_PATH     : isink.itiming.itiming[0].ctrl_buf.ram1.ram.ram[$sts_id]"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x40800 + $sts_id"
            
        def startAddress(self):
            return 0x00040800
            
        def endAddress(self):
            return 0x00040817

        class _Sk_sl0_HoMd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Sk_sl0_HoMd"
            
            def description(self):
                return "00: SPE-VC3 Mode, 01: DS3 Mode, 10: E3 Mode, 11: Lo-order Mode (DS1 or E1 or VT1.5 or VT2)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_sl0_LoMd_Grp_6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Sk_sl0_LoMd_Grp_6"
            
            def description(self):
                return "00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_sl0_LoMd_Grp_5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "Sk_sl0_LoMd_Grp_5"
            
            def description(self):
                return "00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_sl0_LoMd_Grp_4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Sk_sl0_LoMd_Grp_4"
            
            def description(self):
                return "00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_sl0_LoMd_Grp_3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Sk_sl0_LoMd_Grp_3"
            
            def description(self):
                return "00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_sl0_LoMd_Grp_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Sk_sl0_LoMd_Grp_2"
            
            def description(self):
                return "00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_sl0_LoMd_Grp_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Sk_sl0_LoMd_Grp_1"
            
            def description(self):
                return "00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_sl0_LoMd_Grp_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Sk_sl0_LoMd_Grp_0"
            
            def description(self):
                return "00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Sk_sl0_HoMd"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl0_ctrl_pen._Sk_sl0_HoMd()
            allFields["Sk_sl0_LoMd_Grp_6"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl0_ctrl_pen._Sk_sl0_LoMd_Grp_6()
            allFields["Sk_sl0_LoMd_Grp_5"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl0_ctrl_pen._Sk_sl0_LoMd_Grp_5()
            allFields["Sk_sl0_LoMd_Grp_4"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl0_ctrl_pen._Sk_sl0_LoMd_Grp_4()
            allFields["Sk_sl0_LoMd_Grp_3"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl0_ctrl_pen._Sk_sl0_LoMd_Grp_3()
            allFields["Sk_sl0_LoMd_Grp_2"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl0_ctrl_pen._Sk_sl0_LoMd_Grp_2()
            allFields["Sk_sl0_LoMd_Grp_1"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl0_ctrl_pen._Sk_sl0_LoMd_Grp_1()
            allFields["Sk_sl0_LoMd_Grp_0"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl0_ctrl_pen._Sk_sl0_LoMd_Grp_0()
            return allFields

    class _vcat5g_sk_sl0_unfrm_pen(AtRegister.AtRegister):
        def name(self):
            return "Sink Slice 0 E1-Unframed Mode Configuration"
    
        def description(self):
            return "This register is used to config mode for VCAT Sink side. HDL_PATH     : isink.itiming.itiming[0].unfrm_buf.ram.ram[$sts_id]"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x40840 + $sts_id"
            
        def startAddress(self):
            return 0x00040840
            
        def endAddress(self):
            return 0x00040857

        class _Sk_sl0_E1Unfrm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Sk_sl0_E1Unfrm"
            
            def description(self):
                return "1: E1-Unframed Mode, 0: E1-Framed Mode (default)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Sk_sl0_E1Unfrm"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl0_unfrm_pen._Sk_sl0_E1Unfrm()
            return allFields

    class _vcat5g_sk_sl0_mux_pen(AtRegister.AtRegister):
        def name(self):
            return "Sink Slice 0 Input Mux Configuration"
    
        def description(self):
            return "This register is used to select bus running at Sink side input. HDL_PATH     : isink.itiming.itiming[0].imux.selector.ram1.array.ram[$sts_id]"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x40880 + $sts_id"
            
        def startAddress(self):
            return 0x00040880
            
        def endAddress(self):
            return 0x00040897

        class _Sk_sl0_Mux(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Sk_sl0_Mux"
            
            def description(self):
                return "1: Select from PDH bus (DS3/E3/DS1/E1), 0: Select from STS/VT Bus"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Sk_sl0_Mux"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl0_mux_pen._Sk_sl0_Mux()
            return allFields

    class _vcat5g_sk_sl1_ctrl_pen(AtRegister.AtRegister):
        def name(self):
            return "Sink Slice 1 Mode Configuration"
    
        def description(self):
            return "This register is used to config timing mode for VCAT Sink side. HDL_PATH     : isink.itiming.itiming[1].ctrl_buf.ram1.ram.ram[$sts_id]"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x40900 + $sts_id"
            
        def startAddress(self):
            return 0x00040900
            
        def endAddress(self):
            return 0x00040917

        class _Sk_sl1_HoMd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Sk_sl1_HoMd"
            
            def description(self):
                return "00: SPE-VC3 Mode, 01: DS3 Mode, 10: E3 Mode, 11: Lo-order Mode (DS1 or E1 or VT1.5 or VT2)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_sl1_LoMd_Grp_6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Sk_sl1_LoMd_Grp_6"
            
            def description(self):
                return "00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_sl1_LoMd_Grp_5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "Sk_sl1_LoMd_Grp_5"
            
            def description(self):
                return "00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_sl1_LoMd_Grp_4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Sk_sl1_LoMd_Grp_4"
            
            def description(self):
                return "00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_sl1_LoMd_Grp_3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Sk_sl1_LoMd_Grp_3"
            
            def description(self):
                return "00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_sl1_LoMd_Grp_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Sk_sl1_LoMd_Grp_2"
            
            def description(self):
                return "00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_sl1_LoMd_Grp_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Sk_sl1_LoMd_Grp_1"
            
            def description(self):
                return "00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_sl1_LoMd_Grp_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Sk_sl1_LoMd_Grp_0"
            
            def description(self):
                return "00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Sk_sl1_HoMd"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl1_ctrl_pen._Sk_sl1_HoMd()
            allFields["Sk_sl1_LoMd_Grp_6"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl1_ctrl_pen._Sk_sl1_LoMd_Grp_6()
            allFields["Sk_sl1_LoMd_Grp_5"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl1_ctrl_pen._Sk_sl1_LoMd_Grp_5()
            allFields["Sk_sl1_LoMd_Grp_4"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl1_ctrl_pen._Sk_sl1_LoMd_Grp_4()
            allFields["Sk_sl1_LoMd_Grp_3"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl1_ctrl_pen._Sk_sl1_LoMd_Grp_3()
            allFields["Sk_sl1_LoMd_Grp_2"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl1_ctrl_pen._Sk_sl1_LoMd_Grp_2()
            allFields["Sk_sl1_LoMd_Grp_1"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl1_ctrl_pen._Sk_sl1_LoMd_Grp_1()
            allFields["Sk_sl1_LoMd_Grp_0"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl1_ctrl_pen._Sk_sl1_LoMd_Grp_0()
            return allFields

    class _vcat5g_sk_sl1_unfrm_pen(AtRegister.AtRegister):
        def name(self):
            return "Sink Slice 1 E1-Unframed Mode Configuration"
    
        def description(self):
            return "This register is used to config mode for VCAT Sink side. HDL_PATH     : isink.itiming.itiming[1].unfrm_buf.ram.ram[$sts_id]"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x40940 + $sts_id"
            
        def startAddress(self):
            return 0x00040940
            
        def endAddress(self):
            return 0x00040957

        class _Sk_sl1_E1Unfrm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Sk_sl1_E1Unfrm"
            
            def description(self):
                return "1: E1-Unframed Mode, 0: E1-Framed Mode (default)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Sk_sl1_E1Unfrm"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl1_unfrm_pen._Sk_sl1_E1Unfrm()
            return allFields

    class _vcat5g_sk_sl1_mux_pen(AtRegister.AtRegister):
        def name(self):
            return "Sink Slice 1 Input Mux Configuration"
    
        def description(self):
            return "This register is used to select bus running at Sink side input. HDL_PATH     : isink.itiming.itiming[1].imux.selector.ram1.array.ram[$sts_id]"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x40980 + $sts_id"
            
        def startAddress(self):
            return 0x00040980
            
        def endAddress(self):
            return 0x00040997

        class _Sk_sl1_Mux(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Sk_sl1_Mux"
            
            def description(self):
                return "1: Select from PDH bus (DS3/E3/DS1/E1), 0: Select from STS/VT Bus"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Sk_sl1_Mux"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl1_mux_pen._Sk_sl1_Mux()
            return allFields

    class _vcat5g_sk_sl2_ctrl_pen(AtRegister.AtRegister):
        def name(self):
            return "Sink Slice 2 Mode Configuration"
    
        def description(self):
            return "This register is used to config timing mode for VCAT Sink side. HDL_PATH     : isink.itiming.itiming[2].ctrl_buf.ram1.ram.ram[$sts_id]"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x40a00 + $sts_id"
            
        def startAddress(self):
            return 0x00040a00
            
        def endAddress(self):
            return 0x00040a17

        class _Sk_sl2_HoMd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Sk_sl2_HoMd"
            
            def description(self):
                return "00: SPE-VC3 Mode, 01: DS3 Mode, 10: E3 Mode, 11: Lo-order Mode (DS1 or E1 or VT1.5 or VT2)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_sl2_LoMd_Grp_6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Sk_sl2_LoMd_Grp_6"
            
            def description(self):
                return "00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_sl2_LoMd_Grp_5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "Sk_sl2_LoMd_Grp_5"
            
            def description(self):
                return "00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_sl2_LoMd_Grp_4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Sk_sl2_LoMd_Grp_4"
            
            def description(self):
                return "00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_sl2_LoMd_Grp_3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Sk_sl2_LoMd_Grp_3"
            
            def description(self):
                return "00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_sl2_LoMd_Grp_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Sk_sl2_LoMd_Grp_2"
            
            def description(self):
                return "00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_sl2_LoMd_Grp_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Sk_sl2_LoMd_Grp_1"
            
            def description(self):
                return "00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_sl2_LoMd_Grp_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Sk_sl2_LoMd_Grp_0"
            
            def description(self):
                return "00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Sk_sl2_HoMd"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl2_ctrl_pen._Sk_sl2_HoMd()
            allFields["Sk_sl2_LoMd_Grp_6"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl2_ctrl_pen._Sk_sl2_LoMd_Grp_6()
            allFields["Sk_sl2_LoMd_Grp_5"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl2_ctrl_pen._Sk_sl2_LoMd_Grp_5()
            allFields["Sk_sl2_LoMd_Grp_4"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl2_ctrl_pen._Sk_sl2_LoMd_Grp_4()
            allFields["Sk_sl2_LoMd_Grp_3"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl2_ctrl_pen._Sk_sl2_LoMd_Grp_3()
            allFields["Sk_sl2_LoMd_Grp_2"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl2_ctrl_pen._Sk_sl2_LoMd_Grp_2()
            allFields["Sk_sl2_LoMd_Grp_1"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl2_ctrl_pen._Sk_sl2_LoMd_Grp_1()
            allFields["Sk_sl2_LoMd_Grp_0"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl2_ctrl_pen._Sk_sl2_LoMd_Grp_0()
            return allFields

    class _vcat5g_sk_sl2_unfrm_pen(AtRegister.AtRegister):
        def name(self):
            return "Sink Slice 2 E1-Unframed Mode Configuration"
    
        def description(self):
            return "This register is used to config mode for VCAT Sink side. HDL_PATH     : isink.itiming.itiming[2].unfrm_buf.ram.ram[$sts_id]"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x40a40 + $sts_id"
            
        def startAddress(self):
            return 0x00040a40
            
        def endAddress(self):
            return 0x00040a57

        class _Sk_sl2_E1Unfrm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Sk_sl2_E1Unfrm"
            
            def description(self):
                return "1: E1-Unframed Mode, 0: E1-Framed Mode (default)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Sk_sl2_E1Unfrm"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl2_unfrm_pen._Sk_sl2_E1Unfrm()
            return allFields

    class _vcat5g_sk_sl2_mux_pen(AtRegister.AtRegister):
        def name(self):
            return "Sink Slice 2 Input Mux Configuration"
    
        def description(self):
            return "This register is used to select bus running at Sink side input. HDL_PATH     : isink.itiming.itiming[2].imux.selector.ram1.array.ram[$sts_id]"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x40a80 + $sts_id"
            
        def startAddress(self):
            return 0x00040a80
            
        def endAddress(self):
            return 0x00040a97

        class _Sk_sl2_Mux(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Sk_sl2_Mux"
            
            def description(self):
                return "1: Select from PDH bus (DS3/E3/DS1/E1), 0: Select from STS/VT Bus"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Sk_sl2_Mux"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl2_mux_pen._Sk_sl2_Mux()
            return allFields

    class _vcat5g_sk_sl3_ctrl_pen(AtRegister.AtRegister):
        def name(self):
            return "Sink Slice 3 Mode Configuration"
    
        def description(self):
            return "This register is used to config timing mode for VCAT Sink side. HDL_PATH     : isink.itiming.itiming[3].ctrl_buf.ram1.ram.ram[$sts_id]"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x40b00 + $sts_id"
            
        def startAddress(self):
            return 0x00040b00
            
        def endAddress(self):
            return 0x00040b17

        class _Sk_sl3_HoMd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Sk_sl3_HoMd"
            
            def description(self):
                return "00: SPE-VC3 Mode, 01: DS3 Mode, 10: E3 Mode, 11: Lo-order Mode (DS1 or E1 or VT1.5 or VT2)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_sl3_LoMd_Grp_6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Sk_sl3_LoMd_Grp_6"
            
            def description(self):
                return "00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_sl3_LoMd_Grp_5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "Sk_sl3_LoMd_Grp_5"
            
            def description(self):
                return "00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_sl3_LoMd_Grp_4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Sk_sl3_LoMd_Grp_4"
            
            def description(self):
                return "00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_sl3_LoMd_Grp_3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Sk_sl3_LoMd_Grp_3"
            
            def description(self):
                return "00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_sl3_LoMd_Grp_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Sk_sl3_LoMd_Grp_2"
            
            def description(self):
                return "00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_sl3_LoMd_Grp_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Sk_sl3_LoMd_Grp_1"
            
            def description(self):
                return "00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_sl3_LoMd_Grp_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Sk_sl3_LoMd_Grp_0"
            
            def description(self):
                return "00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Sk_sl3_HoMd"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl3_ctrl_pen._Sk_sl3_HoMd()
            allFields["Sk_sl3_LoMd_Grp_6"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl3_ctrl_pen._Sk_sl3_LoMd_Grp_6()
            allFields["Sk_sl3_LoMd_Grp_5"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl3_ctrl_pen._Sk_sl3_LoMd_Grp_5()
            allFields["Sk_sl3_LoMd_Grp_4"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl3_ctrl_pen._Sk_sl3_LoMd_Grp_4()
            allFields["Sk_sl3_LoMd_Grp_3"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl3_ctrl_pen._Sk_sl3_LoMd_Grp_3()
            allFields["Sk_sl3_LoMd_Grp_2"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl3_ctrl_pen._Sk_sl3_LoMd_Grp_2()
            allFields["Sk_sl3_LoMd_Grp_1"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl3_ctrl_pen._Sk_sl3_LoMd_Grp_1()
            allFields["Sk_sl3_LoMd_Grp_0"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl3_ctrl_pen._Sk_sl3_LoMd_Grp_0()
            return allFields

    class _vcat5g_sk_sl3_unfrm_pen(AtRegister.AtRegister):
        def name(self):
            return "Sink Slice 3 E1-Unframed Mode Configuration"
    
        def description(self):
            return "This register is used to config mode for VCAT Sink side. HDL_PATH     : isink.itiming.itiming[3].unfrm_buf.ram.ram[$sts_id]"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x40b40 + $sts_id"
            
        def startAddress(self):
            return 0x00040b40
            
        def endAddress(self):
            return 0x00040b57

        class _Sk_sl3__E1Unfrm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Sk_sl3__E1Unfrm"
            
            def description(self):
                return "1: E1-Unframed Mode, 0: E1-Framed Mode (default)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Sk_sl3__E1Unfrm"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl3_unfrm_pen._Sk_sl3__E1Unfrm()
            return allFields

    class _vcat5g_sk_sl3_mux_pen(AtRegister.AtRegister):
        def name(self):
            return "Sink Slice 3 Input Mux Configuration"
    
        def description(self):
            return "This register is used to select bus running at Sink side input. HDL_PATH     : isink.itiming.itiming[3].imux.selector.ram1.array.ram[$sts_id]"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x40b80 + $sts_id"
            
        def startAddress(self):
            return 0x00040b80
            
        def endAddress(self):
            return 0x00040b97

        class _Sk_sl3_Mux(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Sk_sl3_Mux"
            
            def description(self):
                return "1: Select from PDH bus (DS3/E3/DS1/E1), 0: Select from STS/VT Bus"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Sk_sl3_Mux"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_sl3_mux_pen._Sk_sl3_Mux()
            return allFields

    class _vcat5g_sk_memctrl_pen(AtRegister.AtRegister):
        def name(self):
            return "Sink Member Control Configuration"
    
        def description(self):
            return "This register is used to control engine VCAT Sink side."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x49000 + $sts_id*32 + $vtg_id*4 + $vt_id"
            
        def startAddress(self):
            return 0x00049000
            
        def endAddress(self):
            return 0x00049bff

        class _Sk_Mem_Ena(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Sk_Mem_Ena"
            
            def description(self):
                return "1: Enable Member, 0: Disable Member"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_G8040_Md(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "Sk_G8040_Md"
            
            def description(self):
                return "1: ITU G.8040 Mode, 0: ITU G.804 Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_Vcat_Md(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "Sk_Vcat_Md"
            
            def description(self):
                return "1: VCAT Mode, 0: non-VCAT Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_Lcas_Md(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "Sk_Lcas_Md"
            
            def description(self):
                return "LCAS Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_Nms_Cmd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Sk_Nms_Cmd"
            
            def description(self):
                return "Network Management Command"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_VcgId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Sk_VcgId"
            
            def description(self):
                return "VCG ID 0-511"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Sk_Mem_Ena"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_memctrl_pen._Sk_Mem_Ena()
            allFields["Sk_G8040_Md"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_memctrl_pen._Sk_G8040_Md()
            allFields["Sk_Vcat_Md"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_memctrl_pen._Sk_Vcat_Md()
            allFields["Sk_Lcas_Md"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_memctrl_pen._Sk_Lcas_Md()
            allFields["Sk_Nms_Cmd"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_memctrl_pen._Sk_Nms_Cmd()
            allFields["Sk_VcgId"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_memctrl_pen._Sk_VcgId()
            return allFields

    class _vcat5g_sk_memsta_pen(AtRegister.AtRegister):
        def name(self):
            return "Sink Member Status"
    
        def description(self):
            return "This register is used to show status of member at Sink side."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x51000 + $sts_id*32 + $vtg_id*4 + $vt_id"
            
        def startAddress(self):
            return 0x00051000
            
        def endAddress(self):
            return 0x00051bff

        class _Sk_Mem_EnaSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "Sk_Mem_EnaSta"
            
            def description(self):
                return "Member Enable Status"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_Mem_AcpSeq(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 13
        
            def name(self):
                return "Sk_Mem_AcpSeq"
            
            def description(self):
                return "Sink Member Accepted Sequence"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_Mem_RecSeq(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Sk_Mem_RecSeq"
            
            def description(self):
                return "Sink Member Sequence Received"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_Mem_Fsm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Sk_Mem_Fsm"
            
            def description(self):
                return "Member FSM"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Sk_Mem_EnaSta"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_memsta_pen._Sk_Mem_EnaSta()
            allFields["Sk_Mem_AcpSeq"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_memsta_pen._Sk_Mem_AcpSeq()
            allFields["Sk_Mem_RecSeq"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_memsta_pen._Sk_Mem_RecSeq()
            allFields["Sk_Mem_Fsm"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_memsta_pen._Sk_Mem_Fsm()
            return allFields

    class _vcat5g_sk_expseq_pen(AtRegister.AtRegister):
        def name(self):
            return "Sink Member Expected Sequence Control"
    
        def description(self):
            return "This register is used to Control Expected Sequence of member at Sink side."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x53000 + $sts_id*32 + $vtg_id*4 + $vt_id"
            
        def startAddress(self):
            return 0x00053000
            
        def endAddress(self):
            return 0x00053bff

        class _Sk_Mem_ExpSeq(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Sk_Mem_ExpSeq"
            
            def description(self):
                return "Member Expected Sequence"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Sk_Mem_ExpSeq"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_expseq_pen._Sk_Mem_ExpSeq()
            return allFields

    class _vcat5g_sk_intrctrl_pen(AtRegister.AtRegister):
        def name(self):
            return "Sink Channel Interrupt Enable Control"
    
        def description(self):
            return "This register is used to enable Interrupt, write 1 to enable interrupt for event."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x58000 + $sts_id*32 + $vtg_id*4 + $vt_id"
            
        def startAddress(self):
            return 0x00058000
            
        def endAddress(self):
            return 0x00058bff

        class _SQNC_Intr_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "SQNC_Intr_En"
            
            def description(self):
                return "Interrupt Enable for SQNC event"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MND_Intr_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "MND_Intr_En"
            
            def description(self):
                return "Interrupt Enable for MND event"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _LOA_Intr_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "LOA_Intr_En"
            
            def description(self):
                return "Interrupt Enable for LOA event"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _SQM_Intr_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "SQM_Intr_En"
            
            def description(self):
                return "Interrupt Enable for SQM event"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _OOM_Intr_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "OOM_Intr_En"
            
            def description(self):
                return "Interrupt Enable for OOM event"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _LOM_Intr_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LOM_Intr_En"
            
            def description(self):
                return "Interrupt Enable for LOM event"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SQNC_Intr_En"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_intrctrl_pen._SQNC_Intr_En()
            allFields["MND_Intr_En"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_intrctrl_pen._MND_Intr_En()
            allFields["LOA_Intr_En"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_intrctrl_pen._LOA_Intr_En()
            allFields["SQM_Intr_En"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_intrctrl_pen._SQM_Intr_En()
            allFields["OOM_Intr_En"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_intrctrl_pen._OOM_Intr_En()
            allFields["LOM_Intr_En"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_intrctrl_pen._LOM_Intr_En()
            return allFields

    class _vcat5g_sk_intrstk_pen(AtRegister.AtRegister):
        def name(self):
            return "Sink Channel Interrupt Status"
    
        def description(self):
            return "This register is used to show the status of event,which generate interrupt."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x59000 + $sts_id*32 + $vtg_id*4 + $vt_id"
            
        def startAddress(self):
            return 0x00059000
            
        def endAddress(self):
            return 0x00059bff

        class _SQNC_Intr_Sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "SQNC_Intr_Sta"
            
            def description(self):
                return "SQNC event state change"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MND_Intr_Sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "MND_Intr_Sta"
            
            def description(self):
                return "MND event state change"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _LOA_Intr_Sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "LOA_Intr_Sta"
            
            def description(self):
                return "LOA event state change"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _SQM_Intr_Sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "SQM_Intr_Sta"
            
            def description(self):
                return "SQM event state change"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _OOM_Intr_Sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "OOM_Intr_Sta"
            
            def description(self):
                return "OOM event state change"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _LOM_Intr_Sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LOM_Intr_Sta"
            
            def description(self):
                return "LOM event state change"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SQNC_Intr_Sta"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_intrstk_pen._SQNC_Intr_Sta()
            allFields["MND_Intr_Sta"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_intrstk_pen._MND_Intr_Sta()
            allFields["LOA_Intr_Sta"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_intrstk_pen._LOA_Intr_Sta()
            allFields["SQM_Intr_Sta"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_intrstk_pen._SQM_Intr_Sta()
            allFields["OOM_Intr_Sta"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_intrstk_pen._OOM_Intr_Sta()
            allFields["LOM_Intr_Sta"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_intrstk_pen._LOM_Intr_Sta()
            return allFields

    class _vcat5g_sk_intrsta_pen(AtRegister.AtRegister):
        def name(self):
            return "Sink Channel Current Status"
    
        def description(self):
            return "This register is used to show the current status of event."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x5A000 + $sts_id*32 + $vtg_id*4 + $vt_id"
            
        def startAddress(self):
            return 0x0005a000
            
        def endAddress(self):
            return 0x0005abff

        class _SQNC_Intr_CrrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "SQNC_Intr_CrrSta"
            
            def description(self):
                return "SQNC event Current state"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _MND_Intr_CrrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "MND_Intr_CrrSta"
            
            def description(self):
                return "MND event Current state"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _LOA_Intr_CrrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "LOA_Intr_CrrSta"
            
            def description(self):
                return "LOA event Current state"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _SQM_Intr_CrrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "SQM_Intr_CrrSta"
            
            def description(self):
                return "SQM event Current state"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _OOM_Intr_CrrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "OOM_Intr_CrrSta"
            
            def description(self):
                return "OOM event Current state"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _LOM_Intr_CrrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LOM_Intr_CrrSta"
            
            def description(self):
                return "LOM event Current state"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SQNC_Intr_CrrSta"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_intrsta_pen._SQNC_Intr_CrrSta()
            allFields["MND_Intr_CrrSta"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_intrsta_pen._MND_Intr_CrrSta()
            allFields["LOA_Intr_CrrSta"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_intrsta_pen._LOA_Intr_CrrSta()
            allFields["SQM_Intr_CrrSta"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_intrsta_pen._SQM_Intr_CrrSta()
            allFields["OOM_Intr_CrrSta"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_intrsta_pen._OOM_Intr_CrrSta()
            allFields["LOM_Intr_CrrSta"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_intrsta_pen._LOM_Intr_CrrSta()
            return allFields

    class _vcat5g_sk_introsta_pen(AtRegister.AtRegister):
        def name(self):
            return "Sink Channel Interrupt Or Status"
    
        def description(self):
            return "The register consists of 28 bits for 28 VT/TUs or 28 DS1/E1s of the related STS/VC. Each bit is used to store Interrupt OR status of the related VT/TU/DS1/E1."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x5B000 + $sts_id"
            
        def startAddress(self):
            return 0x0005b000
            
        def endAddress(self):
            return 0x0005b05f

        class _IntrOrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IntrOrSta"
            
            def description(self):
                return "Interrupt Or Status"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IntrOrSta"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_introsta_pen._IntrOrSta()
            return allFields

    class _vcat5g_sk_vcintrctrl_pen(AtRegister.AtRegister):
        def name(self):
            return "Sink STS/VC Interrupt Enable Control"
    
        def description(self):
            return "The register consists of 96 interrupt enable bits for 96 STS/VCs."
            
        def width(self):
            return 128
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0005fffe
            
        def endAddress(self):
            return 0xffffffff

        class _STS_Intr_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 95
                
            def startBit(self):
                return 0
        
            def name(self):
                return "STS_Intr_En"
            
            def description(self):
                return "Interrupt Enable for STS(0-96)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["STS_Intr_En"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_vcintrctrl_pen._STS_Intr_En()
            return allFields

    class _vcat5g_sk_vcintrosta_pen(AtRegister.AtRegister):
        def name(self):
            return "Sink STS/VC Interrupt Or Status"
    
        def description(self):
            return "The register consists of 96 bits for 16 STS/VCs. Each bit is used to store Interrupt OR status of the related STS/VC."
            
        def width(self):
            return 128
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0005ffff
            
        def endAddress(self):
            return 0xffffffff

        class _STS_IntrOrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 95
                
            def startBit(self):
                return 0
        
            def name(self):
                return "STS_IntrOrSta"
            
            def description(self):
                return "Interrupt Status for STS(0-96)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["STS_IntrOrSta"] = _AF6CCI0012_RD_VCAT._vcat5g_sk_vcintrosta_pen._STS_IntrOrSta()
            return allFields

    class _vcat5g_dumpctrl_pen(AtRegister.AtRegister):
        def name(self):
            return "Debug Vcat Interface Dump Control"
    
        def description(self):
            return "."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000a
            
        def endAddress(self):
            return 0xffffffff

        class _Cfg_Dump_Mod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "Cfg_Dump_Mod"
            
            def description(self):
                return "Data dump mode 1'b0     : Dump full 1'b1     : Dump at frame 0 (DS1 mode)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Cfg_Dump_Sel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Cfg_Dump_Sel"
            
            def description(self):
                return "Select bus info to dump 3'd0     : Source to PDH FiFo Input Data 3'd1     : Source PDH FiFo Output Data 3'd2     : Sink PDH Input Data 3'd3     : ENC-VCAT Data 3'd4-3'd7: Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Cfg_Dump_Mod"] = _AF6CCI0012_RD_VCAT._vcat5g_dumpctrl_pen._Cfg_Dump_Mod()
            allFields["Cfg_Dump_Sel"] = _AF6CCI0012_RD_VCAT._vcat5g_dumpctrl_pen._Cfg_Dump_Sel()
            return allFields

    class _vcat5g_dumpdat_pen(AtRegister.AtRegister):
        def name(self):
            return "Debug Vcat Interface Dump Data"
    
        def description(self):
            return "."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x60000 + $dump_id"
            
        def startAddress(self):
            return 0x00060000
            
        def endAddress(self):
            return 0x00061fff

        class _Dump_Dat_Vli(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Dump_Dat_Vli"
            
            def description(self):
                return "VLI Indication"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Dump_Dat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Dump_Dat"
            
            def description(self):
                return "Interface Dump Datat"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Dump_Dat_Vli"] = _AF6CCI0012_RD_VCAT._vcat5g_dumpdat_pen._Dump_Dat_Vli()
            allFields["Dump_Dat"] = _AF6CCI0012_RD_VCAT._vcat5g_dumpdat_pen._Dump_Dat()
            return allFields

    class _vcat5g_sodbctrl_pen(AtRegister.AtRegister):
        def name(self):
            return "Source Debug Control"
    
        def description(self):
            return "This register used to control VCAT debug."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0002010b
            
        def endAddress(self):
            return 0xffffffff

        class _Cfg_Dbg_Ena(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Cfg_Dbg_Ena"
            
            def description(self):
                return "Enable Debug"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Cfg_Dbg_Slc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "Cfg_Dbg_Slc"
            
            def description(self):
                return "Select Slice ID to Debug"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Cfg_Dbg_Lid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Cfg_Dbg_Lid"
            
            def description(self):
                return "Select Channel ID to Debug"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Cfg_Dbg_Ena"] = _AF6CCI0012_RD_VCAT._vcat5g_sodbctrl_pen._Cfg_Dbg_Ena()
            allFields["Cfg_Dbg_Slc"] = _AF6CCI0012_RD_VCAT._vcat5g_sodbctrl_pen._Cfg_Dbg_Slc()
            allFields["Cfg_Dbg_Lid"] = _AF6CCI0012_RD_VCAT._vcat5g_sodbctrl_pen._Cfg_Dbg_Lid()
            return allFields

    class _vcat5g_sostk_pen(AtRegister.AtRegister):
        def name(self):
            return "Debug VCAT Source Sticky"
    
        def description(self):
            return "."
            
        def width(self):
            return 64
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0002010a
            
        def endAddress(self):
            return 0xffffffff

        class _So_Slc3_LO_Empt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "So_Slc3_LO_Empt"
            
            def description(self):
                return "Slice 3 LO PDH Adapt FiFo Empt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_Slc3_HO_Empt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "So_Slc3_HO_Empt"
            
            def description(self):
                return "Slice 3 HO PDH Adapt FiFo Empt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_Slc3_LO_Full(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "So_Slc3_LO_Full"
            
            def description(self):
                return "Slice 3 LO PDH Adapt FiFo Full"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_Slc3_HO_Full(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "So_Slc3_HO_Full"
            
            def description(self):
                return "Slice 3 HO PDH Adapt FiFo Full"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_Slc2_LO_Empt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "So_Slc2_LO_Empt"
            
            def description(self):
                return "Slice 2 LO PDH Adapt FiFo Empt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_Slc2_HO_Empt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "So_Slc2_HO_Empt"
            
            def description(self):
                return "Slice 2 HO PDH Adapt FiFo Empt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_Slc2_LO_Full(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "So_Slc2_LO_Full"
            
            def description(self):
                return "Slice 2 LO PDH Adapt FiFo Full"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_Slc2_HO_Full(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "So_Slc2_HO_Full"
            
            def description(self):
                return "Slice 2 HO PDH Adapt FiFo Full"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_Slc1_LO_Empt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "So_Slc1_LO_Empt"
            
            def description(self):
                return "Slice 1 LO PDH Adapt FiFo Empt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_Slc1_HO_Empt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "So_Slc1_HO_Empt"
            
            def description(self):
                return "Slice 1 HO PDH Adapt FiFo Empt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_Slc1_LO_Full(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "So_Slc1_LO_Full"
            
            def description(self):
                return "Slice 1 LO PDH Adapt FiFo Full"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_Slc1_HO_Full(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "So_Slc1_HO_Full"
            
            def description(self):
                return "Slice 1 HO PDH Adapt FiFo Full"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_Slc0_LO_Empt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "So_Slc0_LO_Empt"
            
            def description(self):
                return "Slice 0 LO PDH Adapt FiFo Empt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_Slc0_HO_Empt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "So_Slc0_HO_Empt"
            
            def description(self):
                return "Slice 0 HO PDH Adapt FiFo Empt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_Slc0_LO_Full(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "So_Slc0_LO_Full"
            
            def description(self):
                return "Slice 0 LO PDH Adapt FiFo Full"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_Slc0_HO_Full(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "So_Slc0_HO_Full"
            
            def description(self):
                return "Slice 0 HO PDH Adapt FiFo Full"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_DS3VLI_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "So_DS3VLI_Err"
            
            def description(self):
                return "Ouput DS3 VLI Position Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_E3VLI_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "So_E3VLI_Err"
            
            def description(self):
                return "Ouput E3 VLI Position Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_DS1VLI_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "So_DS1VLI_Err"
            
            def description(self):
                return "Ouput DS1 VLI Position Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_E1VLI_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "So_E1VLI_Err"
            
            def description(self):
                return "Ouput E1 VLI Position Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_PDH_Vli(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "So_PDH_Vli"
            
            def description(self):
                return "Output VLI from VCAT to PDH"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_PDH_Vld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "So_PDH_Vld"
            
            def description(self):
                return "Output Valid from VCAT to PDH"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _IPDH_Req(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "IPDH_Req"
            
            def description(self):
                return "Request from PDH to VCAT"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_Slc3_Vld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "So_Slc3_Vld"
            
            def description(self):
                return "Source Slice 3 Read Valid"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_Slc2_Vld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "So_Slc2_Vld"
            
            def description(self):
                return "Source Slice 2 Read Valid"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_Slc1_Vld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "So_Slc1_Vld"
            
            def description(self):
                return "Source Slice 1 Read Valid"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_Slc0_Vld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "So_Slc0_Vld"
            
            def description(self):
                return "Source Slice 0 Read Valid"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_Mem_Vld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "So_Mem_Vld"
            
            def description(self):
                return "Source Member Valid"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _So_Enc_Req(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "So_Enc_Req"
            
            def description(self):
                return "Request from VCAT to ENC"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["So_Slc3_LO_Empt"] = _AF6CCI0012_RD_VCAT._vcat5g_sostk_pen._So_Slc3_LO_Empt()
            allFields["So_Slc3_HO_Empt"] = _AF6CCI0012_RD_VCAT._vcat5g_sostk_pen._So_Slc3_HO_Empt()
            allFields["So_Slc3_LO_Full"] = _AF6CCI0012_RD_VCAT._vcat5g_sostk_pen._So_Slc3_LO_Full()
            allFields["So_Slc3_HO_Full"] = _AF6CCI0012_RD_VCAT._vcat5g_sostk_pen._So_Slc3_HO_Full()
            allFields["So_Slc2_LO_Empt"] = _AF6CCI0012_RD_VCAT._vcat5g_sostk_pen._So_Slc2_LO_Empt()
            allFields["So_Slc2_HO_Empt"] = _AF6CCI0012_RD_VCAT._vcat5g_sostk_pen._So_Slc2_HO_Empt()
            allFields["So_Slc2_LO_Full"] = _AF6CCI0012_RD_VCAT._vcat5g_sostk_pen._So_Slc2_LO_Full()
            allFields["So_Slc2_HO_Full"] = _AF6CCI0012_RD_VCAT._vcat5g_sostk_pen._So_Slc2_HO_Full()
            allFields["So_Slc1_LO_Empt"] = _AF6CCI0012_RD_VCAT._vcat5g_sostk_pen._So_Slc1_LO_Empt()
            allFields["So_Slc1_HO_Empt"] = _AF6CCI0012_RD_VCAT._vcat5g_sostk_pen._So_Slc1_HO_Empt()
            allFields["So_Slc1_LO_Full"] = _AF6CCI0012_RD_VCAT._vcat5g_sostk_pen._So_Slc1_LO_Full()
            allFields["So_Slc1_HO_Full"] = _AF6CCI0012_RD_VCAT._vcat5g_sostk_pen._So_Slc1_HO_Full()
            allFields["So_Slc0_LO_Empt"] = _AF6CCI0012_RD_VCAT._vcat5g_sostk_pen._So_Slc0_LO_Empt()
            allFields["So_Slc0_HO_Empt"] = _AF6CCI0012_RD_VCAT._vcat5g_sostk_pen._So_Slc0_HO_Empt()
            allFields["So_Slc0_LO_Full"] = _AF6CCI0012_RD_VCAT._vcat5g_sostk_pen._So_Slc0_LO_Full()
            allFields["So_Slc0_HO_Full"] = _AF6CCI0012_RD_VCAT._vcat5g_sostk_pen._So_Slc0_HO_Full()
            allFields["So_DS3VLI_Err"] = _AF6CCI0012_RD_VCAT._vcat5g_sostk_pen._So_DS3VLI_Err()
            allFields["So_E3VLI_Err"] = _AF6CCI0012_RD_VCAT._vcat5g_sostk_pen._So_E3VLI_Err()
            allFields["So_DS1VLI_Err"] = _AF6CCI0012_RD_VCAT._vcat5g_sostk_pen._So_DS1VLI_Err()
            allFields["So_E1VLI_Err"] = _AF6CCI0012_RD_VCAT._vcat5g_sostk_pen._So_E1VLI_Err()
            allFields["So_PDH_Vli"] = _AF6CCI0012_RD_VCAT._vcat5g_sostk_pen._So_PDH_Vli()
            allFields["So_PDH_Vld"] = _AF6CCI0012_RD_VCAT._vcat5g_sostk_pen._So_PDH_Vld()
            allFields["IPDH_Req"] = _AF6CCI0012_RD_VCAT._vcat5g_sostk_pen._IPDH_Req()
            allFields["So_Slc3_Vld"] = _AF6CCI0012_RD_VCAT._vcat5g_sostk_pen._So_Slc3_Vld()
            allFields["So_Slc2_Vld"] = _AF6CCI0012_RD_VCAT._vcat5g_sostk_pen._So_Slc2_Vld()
            allFields["So_Slc1_Vld"] = _AF6CCI0012_RD_VCAT._vcat5g_sostk_pen._So_Slc1_Vld()
            allFields["So_Slc0_Vld"] = _AF6CCI0012_RD_VCAT._vcat5g_sostk_pen._So_Slc0_Vld()
            allFields["So_Mem_Vld"] = _AF6CCI0012_RD_VCAT._vcat5g_sostk_pen._So_Mem_Vld()
            allFields["So_Enc_Req"] = _AF6CCI0012_RD_VCAT._vcat5g_sostk_pen._So_Enc_Req()
            return allFields

    class _vcat5g_sodat_pen(AtRegister.AtRegister):
        def name(self):
            return "VCAT Data Control"
    
        def description(self):
            return "This register used to control Data Generator/Monitor for VCAT debug."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000000fd
            
        def endAddress(self):
            return 0xffffffff

        class _Cfg_Dat_Mod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 9
        
            def name(self):
                return "Cfg_Dat_Mod"
            
            def description(self):
                return "Select Mode for Data Generator/Monitor"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Cfg_Dat_Ena(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Cfg_Dat_Ena"
            
            def description(self):
                return "Enable Data Generator/Monitor"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Cfg_Dat_VCG(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Cfg_Dat_VCG"
            
            def description(self):
                return "Select VCG for Data Generator/Monitor"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Cfg_Dat_Mod"] = _AF6CCI0012_RD_VCAT._vcat5g_sodat_pen._Cfg_Dat_Mod()
            allFields["Cfg_Dat_Ena"] = _AF6CCI0012_RD_VCAT._vcat5g_sodat_pen._Cfg_Dat_Ena()
            allFields["Cfg_Dat_VCG"] = _AF6CCI0012_RD_VCAT._vcat5g_sodat_pen._Cfg_Dat_VCG()
            return allFields

    class _vcat5g_sodatmon_pen(AtRegister.AtRegister):
        def name(self):
            return "VCAT Data Error Counter"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000000fe
            
        def endAddress(self):
            return 0xffffffff

        class _Dat_Err_Cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Dat_Err_Cnt"
            
            def description(self):
                return "Data Error Counter"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Dat_Err_Cnt"] = _AF6CCI0012_RD_VCAT._vcat5g_sodatmon_pen._Dat_Err_Cnt()
            return allFields

    class _vcat5g_skstk_pen(AtRegister.AtRegister):
        def name(self):
            return "Debug VCAT Sink Sticky"
    
        def description(self):
            return "."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00050004
            
        def endAddress(self):
            return 0xffffffff

        class _Sk_Alig_FFFull(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "Sk_Alig_FFFull"
            
            def description(self):
                return "Sink Aligment FIFO Full"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_Slc3_FFFull(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "Sk_Slc3_FFFull"
            
            def description(self):
                return "Sink Slice 3 FIFO Full"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_Slc2_FFFull(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "Sk_Slc2_FFFull"
            
            def description(self):
                return "Sink Slice 2 FIFO Full"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_Slc1_FFFull(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "Sk_Slc1_FFFull"
            
            def description(self):
                return "Sink Slice 1 FIFO Full"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_Slc0_FFFull(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Sk_Slc0_FFFull"
            
            def description(self):
                return "Sink Slice 0 FIFO Full"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_Seq_oVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Sk_Seq_oVld"
            
            def description(self):
                return "Sequence Processing Output Valid to DEC"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_Seq_iVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Sk_Seq_iVld"
            
            def description(self):
                return "Sequence Processing Input Valid"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sk_Seq_iLom(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Sk_Seq_iLom"
            
            def description(self):
                return "Sequence Processing Input LOM"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Sk_Alig_FFFull"] = _AF6CCI0012_RD_VCAT._vcat5g_skstk_pen._Sk_Alig_FFFull()
            allFields["Sk_Slc3_FFFull"] = _AF6CCI0012_RD_VCAT._vcat5g_skstk_pen._Sk_Slc3_FFFull()
            allFields["Sk_Slc2_FFFull"] = _AF6CCI0012_RD_VCAT._vcat5g_skstk_pen._Sk_Slc2_FFFull()
            allFields["Sk_Slc1_FFFull"] = _AF6CCI0012_RD_VCAT._vcat5g_skstk_pen._Sk_Slc1_FFFull()
            allFields["Sk_Slc0_FFFull"] = _AF6CCI0012_RD_VCAT._vcat5g_skstk_pen._Sk_Slc0_FFFull()
            allFields["Sk_Seq_oVld"] = _AF6CCI0012_RD_VCAT._vcat5g_skstk_pen._Sk_Seq_oVld()
            allFields["Sk_Seq_iVld"] = _AF6CCI0012_RD_VCAT._vcat5g_skstk_pen._Sk_Seq_iVld()
            allFields["Sk_Seq_iLom"] = _AF6CCI0012_RD_VCAT._vcat5g_skstk_pen._Sk_Seq_iLom()
            return allFields

    class _vcat5g_ddrstk_pen(AtRegister.AtRegister):
        def name(self):
            return "Debug DDR Cache Sticky"
    
        def description(self):
            return "."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00001001
            
        def endAddress(self):
            return 0xffffffff

        class _Vcat_Wrca_Sos(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "Vcat_Wrca_Sos"
            
            def description(self):
                return "VCAT Write Cache Start of Segment"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Vcat_Wrca_Eos(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "Vcat_Wrca_Eos"
            
            def description(self):
                return "VCAT Write Cache End of Segment"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Vcat_Rdca_Rdy(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Vcat_Rdca_Rdy"
            
            def description(self):
                return "VCAT Free Read Cache Ready"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Vcat_Rdca_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "Vcat_Rdca_Err"
            
            def description(self):
                return "VCAT Free Cache Read Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Vcat_Rdca_Get(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "Vcat_Rdca_Get"
            
            def description(self):
                return "VCAT Free Cache Read Get"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Vcat_Reqff_Full(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Vcat_Reqff_Full"
            
            def description(self):
                return "VCAT Read Request FiFo Full"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Vcat_Rdca_LLfull(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "Vcat_Rdca_LLfull"
            
            def description(self):
                return "VCAT Read Cache Link FiFo Full"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Vcat_FreeCa_Empt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Vcat_FreeCa_Empt"
            
            def description(self):
                return "VCAT Free Cache FiFo Empty"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _DDR_Wr_Done(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "DDR_Wr_Done"
            
            def description(self):
                return "DDR Data Write Done"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _DDR_Rd_Ena(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "DDR_Rd_Ena"
            
            def description(self):
                return "DDR Read Data Enable from PLA"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _DDR_Rd_Eop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "DDR_Rd_Eop"
            
            def description(self):
                return "DDR Read Data End of Segment"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _DDR_Rd_Vld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "DDR_Rd_Vld"
            
            def description(self):
                return "DDR Read Data Valid"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _VCAT_Read(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "VCAT_Read"
            
            def description(self):
                return "Request Read from VCAT to PLA"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _VCAT_Write(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "VCAT_Write"
            
            def description(self):
                return "Request Write from VCAT to PLA"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Vcat_MFI_Read(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Vcat_MFI_Read"
            
            def description(self):
                return "Delay Compensate request Read"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Vcat_MFI_Write(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Vcat_MFI_Write"
            
            def description(self):
                return "Delay Compensate request Write"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Vcat_Wrca_Sos"] = _AF6CCI0012_RD_VCAT._vcat5g_ddrstk_pen._Vcat_Wrca_Sos()
            allFields["Vcat_Wrca_Eos"] = _AF6CCI0012_RD_VCAT._vcat5g_ddrstk_pen._Vcat_Wrca_Eos()
            allFields["Vcat_Rdca_Rdy"] = _AF6CCI0012_RD_VCAT._vcat5g_ddrstk_pen._Vcat_Rdca_Rdy()
            allFields["Vcat_Rdca_Err"] = _AF6CCI0012_RD_VCAT._vcat5g_ddrstk_pen._Vcat_Rdca_Err()
            allFields["Vcat_Rdca_Get"] = _AF6CCI0012_RD_VCAT._vcat5g_ddrstk_pen._Vcat_Rdca_Get()
            allFields["Vcat_Reqff_Full"] = _AF6CCI0012_RD_VCAT._vcat5g_ddrstk_pen._Vcat_Reqff_Full()
            allFields["Vcat_Rdca_LLfull"] = _AF6CCI0012_RD_VCAT._vcat5g_ddrstk_pen._Vcat_Rdca_LLfull()
            allFields["Vcat_FreeCa_Empt"] = _AF6CCI0012_RD_VCAT._vcat5g_ddrstk_pen._Vcat_FreeCa_Empt()
            allFields["DDR_Wr_Done"] = _AF6CCI0012_RD_VCAT._vcat5g_ddrstk_pen._DDR_Wr_Done()
            allFields["DDR_Rd_Ena"] = _AF6CCI0012_RD_VCAT._vcat5g_ddrstk_pen._DDR_Rd_Ena()
            allFields["DDR_Rd_Eop"] = _AF6CCI0012_RD_VCAT._vcat5g_ddrstk_pen._DDR_Rd_Eop()
            allFields["DDR_Rd_Vld"] = _AF6CCI0012_RD_VCAT._vcat5g_ddrstk_pen._DDR_Rd_Vld()
            allFields["VCAT_Read"] = _AF6CCI0012_RD_VCAT._vcat5g_ddrstk_pen._VCAT_Read()
            allFields["VCAT_Write"] = _AF6CCI0012_RD_VCAT._vcat5g_ddrstk_pen._VCAT_Write()
            allFields["Vcat_MFI_Read"] = _AF6CCI0012_RD_VCAT._vcat5g_ddrstk_pen._Vcat_MFI_Read()
            allFields["Vcat_MFI_Write"] = _AF6CCI0012_RD_VCAT._vcat5g_ddrstk_pen._Vcat_MFI_Write()
            return allFields

    class _vcat5g_ddrblk_pen(AtRegister.AtRegister):
        def name(self):
            return "Debug DDR Free Read Cache Number"
    
        def description(self):
            return "."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00001002
            
        def endAddress(self):
            return 0xffffffff

        class _RDCA_NUM(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RDCA_NUM"
            
            def description(self):
                return "VCAT Free Read Cache Number"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RDCA_NUM"] = _AF6CCI0012_RD_VCAT._vcat5g_ddrblk_pen._RDCA_NUM()
            return allFields

    class _vcat5g_ddrdumpctrl_pen(AtRegister.AtRegister):
        def name(self):
            return "Debug DDR Interface Dump Control"
    
        def description(self):
            return "."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00001004
            
        def endAddress(self):
            return 0xffffffff

        class _DDR_Dump_Lid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 2
        
            def name(self):
                return "DDR_Dump_Lid"
            
            def description(self):
                return "DDR Dump Line ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _DDR_Dump_Sel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DDR_Dump_Sel"
            
            def description(self):
                return "Select bus info to dump 2'd0     : Write Data from VCAT to PLA 2'd1     : Read Data from PLA to VCAT 2'd2-2'd3: Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DDR_Dump_Lid"] = _AF6CCI0012_RD_VCAT._vcat5g_ddrdumpctrl_pen._DDR_Dump_Lid()
            allFields["DDR_Dump_Sel"] = _AF6CCI0012_RD_VCAT._vcat5g_ddrdumpctrl_pen._DDR_Dump_Sel()
            return allFields

    class _vcat5g_ddrdumpdat_pen(AtRegister.AtRegister):
        def name(self):
            return "DDR Dump Data Buffer"
    
        def description(self):
            return "."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x01100 + $dump_id"
            
        def startAddress(self):
            return 0x00001100
            
        def endAddress(self):
            return 0x000011ff

        class _DDR_Dump_Data(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DDR_Dump_Data"
            
            def description(self):
                return "DDR DUmp Data"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DDR_Dump_Data"] = _AF6CCI0012_RD_VCAT._vcat5g_ddrdumpdat_pen._DDR_Dump_Data()
            return allFields

import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0012_RD_IGRULE(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["upen_params"] = _AF6CCI0012_RD_IGRULE._upen_params()
        allRegisters["upen_nlp_profiles"] = _AF6CCI0012_RD_IGRULE._upen_nlp_profiles()
        allRegisters["upen_oam_profiles"] = _AF6CCI0012_RD_IGRULE._upen_oam_profiles()
        allRegisters["upen_err_profiles"] = _AF6CCI0012_RD_IGRULE._upen_err_profiles()
        allRegisters["upen_bcp_profiles"] = _AF6CCI0012_RD_IGRULE._upen_bcp_profiles()
        allRegisters["upen_fcn_profiles"] = _AF6CCI0012_RD_IGRULE._upen_fcn_profiles()
        allRegisters["upen_link_id"] = _AF6CCI0012_RD_IGRULE._upen_link_id()
        return allRegisters

    class _upen_params(AtRegister.AtRegister):
        def name(self):
            return "IgRule Params"
    
        def description(self):
            return "This register is used to get params of Ingress Rule Check Engine We have 5 type profile correspond for type = {FCN,BCP,ERR,OAM,NLP}. Number of profiles eache type is 2^type_bit. Exam NLP_bit = 5 -> NLP have 2^32 = 32 profiles."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _Eng_Id(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 20
        
            def name(self):
                return "Eng_Id"
            
            def description(self):
                return "Engine code"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _FCN_bit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "FCN_bit"
            
            def description(self):
                return "FCN_bit"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _BCP_bit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "BCP_bit"
            
            def description(self):
                return "BCP_bit"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _ERR_bit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "ERR_bit"
            
            def description(self):
                return "ERR_bit"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _OAM_bit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "OAM_bit"
            
            def description(self):
                return "OAM_bit"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _NLP_bit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "NLP_bit"
            
            def description(self):
                return "NLP_bit"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Eng_Id"] = _AF6CCI0012_RD_IGRULE._upen_params._Eng_Id()
            allFields["FCN_bit"] = _AF6CCI0012_RD_IGRULE._upen_params._FCN_bit()
            allFields["BCP_bit"] = _AF6CCI0012_RD_IGRULE._upen_params._BCP_bit()
            allFields["ERR_bit"] = _AF6CCI0012_RD_IGRULE._upen_params._ERR_bit()
            allFields["OAM_bit"] = _AF6CCI0012_RD_IGRULE._upen_params._OAM_bit()
            allFields["NLP_bit"] = _AF6CCI0012_RD_IGRULE._upen_params._NLP_bit()
            return allFields

    class _upen_nlp_profiles(AtRegister.AtRegister):
        def name(self):
            return "IgRule NLP Profiles"
    
        def description(self):
            return "This register is used to config profile forward (fwd) rule for each network layer protocol (NLP)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00_0100 + $nlp_profiles"
            
        def startAddress(self):
            return 0x00000100
            
        def endAddress(self):
            return 0xffffffff

        class _FwdRule_NLP_15(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 30
        
            def name(self):
                return "FwdRule_NLP_15"
            
            def description(self):
                return "FwdRule for NLP#15"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_NLP_14(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 28
        
            def name(self):
                return "FwdRule_NLP_14"
            
            def description(self):
                return "FwdRule for NLP#14"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_NLP_13(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 26
        
            def name(self):
                return "FwdRule_NLP_13"
            
            def description(self):
                return "FwdRule for NLP#13"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_NLP_12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 24
        
            def name(self):
                return "FwdRule_NLP_12"
            
            def description(self):
                return "FwdRule for NLP#12"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_NLP_11(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 22
        
            def name(self):
                return "FwdRule_NLP_11"
            
            def description(self):
                return "FwdRule for NLP#11"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_NLP_10(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 20
        
            def name(self):
                return "FwdRule_NLP_10"
            
            def description(self):
                return "FwdRule for NLP#10"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_NLP_09(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 18
        
            def name(self):
                return "FwdRule_NLP_09"
            
            def description(self):
                return "FwdRule for NLP#09"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_NLP_08(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 16
        
            def name(self):
                return "FwdRule_NLP_08"
            
            def description(self):
                return "FwdRule for NLP#08"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_NLP_07(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 14
        
            def name(self):
                return "FwdRule_NLP_07"
            
            def description(self):
                return "FwdRule for NLP#07"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_NLP_06(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "FwdRule_NLP_06"
            
            def description(self):
                return "FwdRule for NLP#06"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_NLP_05(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "FwdRule_NLP_05"
            
            def description(self):
                return "FwdRule for NLP#05"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_NLP_04(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "FwdRule_NLP_04"
            
            def description(self):
                return "FwdRule for NLP#04"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_NLP_03(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FwdRule_NLP_03"
            
            def description(self):
                return "FwdRule for NLP#03"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_NLP_02(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FwdRule_NLP_02"
            
            def description(self):
                return "FwdRule for NLP#02"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_NLP_01(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FwdRule_NLP_01"
            
            def description(self):
                return "FwdRule for NLP#01"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_NLP_00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FwdRule_NLP_00"
            
            def description(self):
                return "FwdRule for NLP#00"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FwdRule_NLP_15"] = _AF6CCI0012_RD_IGRULE._upen_nlp_profiles._FwdRule_NLP_15()
            allFields["FwdRule_NLP_14"] = _AF6CCI0012_RD_IGRULE._upen_nlp_profiles._FwdRule_NLP_14()
            allFields["FwdRule_NLP_13"] = _AF6CCI0012_RD_IGRULE._upen_nlp_profiles._FwdRule_NLP_13()
            allFields["FwdRule_NLP_12"] = _AF6CCI0012_RD_IGRULE._upen_nlp_profiles._FwdRule_NLP_12()
            allFields["FwdRule_NLP_11"] = _AF6CCI0012_RD_IGRULE._upen_nlp_profiles._FwdRule_NLP_11()
            allFields["FwdRule_NLP_10"] = _AF6CCI0012_RD_IGRULE._upen_nlp_profiles._FwdRule_NLP_10()
            allFields["FwdRule_NLP_09"] = _AF6CCI0012_RD_IGRULE._upen_nlp_profiles._FwdRule_NLP_09()
            allFields["FwdRule_NLP_08"] = _AF6CCI0012_RD_IGRULE._upen_nlp_profiles._FwdRule_NLP_08()
            allFields["FwdRule_NLP_07"] = _AF6CCI0012_RD_IGRULE._upen_nlp_profiles._FwdRule_NLP_07()
            allFields["FwdRule_NLP_06"] = _AF6CCI0012_RD_IGRULE._upen_nlp_profiles._FwdRule_NLP_06()
            allFields["FwdRule_NLP_05"] = _AF6CCI0012_RD_IGRULE._upen_nlp_profiles._FwdRule_NLP_05()
            allFields["FwdRule_NLP_04"] = _AF6CCI0012_RD_IGRULE._upen_nlp_profiles._FwdRule_NLP_04()
            allFields["FwdRule_NLP_03"] = _AF6CCI0012_RD_IGRULE._upen_nlp_profiles._FwdRule_NLP_03()
            allFields["FwdRule_NLP_02"] = _AF6CCI0012_RD_IGRULE._upen_nlp_profiles._FwdRule_NLP_02()
            allFields["FwdRule_NLP_01"] = _AF6CCI0012_RD_IGRULE._upen_nlp_profiles._FwdRule_NLP_01()
            allFields["FwdRule_NLP_00"] = _AF6CCI0012_RD_IGRULE._upen_nlp_profiles._FwdRule_NLP_00()
            return allFields

    class _upen_oam_profiles(AtRegister.AtRegister):
        def name(self):
            return "IgRule OAM Profiles"
    
        def description(self):
            return "This register is used to config profile forward (fwd) rule for OAM layer protocol (OAM)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00_0200 + $oam_profiles"
            
        def startAddress(self):
            return 0x00000200
            
        def endAddress(self):
            return 0xffffffff

        class _FwdRule_OAM_15(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 30
        
            def name(self):
                return "FwdRule_OAM_15"
            
            def description(self):
                return "FwdRule for OAM#15"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_OAM_14(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 28
        
            def name(self):
                return "FwdRule_OAM_14"
            
            def description(self):
                return "FwdRule for OAM#14"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_OAM_13(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 26
        
            def name(self):
                return "FwdRule_OAM_13"
            
            def description(self):
                return "FwdRule for OAM#13"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_OAM_12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 24
        
            def name(self):
                return "FwdRule_OAM_12"
            
            def description(self):
                return "FwdRule for OAM#12"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_OAM_11(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 22
        
            def name(self):
                return "FwdRule_OAM_11"
            
            def description(self):
                return "FwdRule for OAM#11"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_OAM_10(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 20
        
            def name(self):
                return "FwdRule_OAM_10"
            
            def description(self):
                return "FwdRule for OAM#10"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_OAM_09(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 18
        
            def name(self):
                return "FwdRule_OAM_09"
            
            def description(self):
                return "FwdRule for OAM#09"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_OAM_08(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 16
        
            def name(self):
                return "FwdRule_OAM_08"
            
            def description(self):
                return "FwdRule for OAM#08"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_OAM_07(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 14
        
            def name(self):
                return "FwdRule_OAM_07"
            
            def description(self):
                return "FwdRule for OAM#07"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_OAM_06(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "FwdRule_OAM_06"
            
            def description(self):
                return "FwdRule for OAM#06"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_OAM_05(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "FwdRule_OAM_05"
            
            def description(self):
                return "FwdRule for OAM#05"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_OAM_04(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "FwdRule_OAM_04"
            
            def description(self):
                return "FwdRule for OAM#04"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_OAM_03(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FwdRule_OAM_03"
            
            def description(self):
                return "FwdRule for OAM#03"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_OAM_02(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FwdRule_OAM_02"
            
            def description(self):
                return "FwdRule for OAM#02"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_OAM_01(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FwdRule_OAM_01"
            
            def description(self):
                return "FwdRule for OAM#01"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_OAM_00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FwdRule_OAM_00"
            
            def description(self):
                return "FwdRule for OAM#00"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FwdRule_OAM_15"] = _AF6CCI0012_RD_IGRULE._upen_oam_profiles._FwdRule_OAM_15()
            allFields["FwdRule_OAM_14"] = _AF6CCI0012_RD_IGRULE._upen_oam_profiles._FwdRule_OAM_14()
            allFields["FwdRule_OAM_13"] = _AF6CCI0012_RD_IGRULE._upen_oam_profiles._FwdRule_OAM_13()
            allFields["FwdRule_OAM_12"] = _AF6CCI0012_RD_IGRULE._upen_oam_profiles._FwdRule_OAM_12()
            allFields["FwdRule_OAM_11"] = _AF6CCI0012_RD_IGRULE._upen_oam_profiles._FwdRule_OAM_11()
            allFields["FwdRule_OAM_10"] = _AF6CCI0012_RD_IGRULE._upen_oam_profiles._FwdRule_OAM_10()
            allFields["FwdRule_OAM_09"] = _AF6CCI0012_RD_IGRULE._upen_oam_profiles._FwdRule_OAM_09()
            allFields["FwdRule_OAM_08"] = _AF6CCI0012_RD_IGRULE._upen_oam_profiles._FwdRule_OAM_08()
            allFields["FwdRule_OAM_07"] = _AF6CCI0012_RD_IGRULE._upen_oam_profiles._FwdRule_OAM_07()
            allFields["FwdRule_OAM_06"] = _AF6CCI0012_RD_IGRULE._upen_oam_profiles._FwdRule_OAM_06()
            allFields["FwdRule_OAM_05"] = _AF6CCI0012_RD_IGRULE._upen_oam_profiles._FwdRule_OAM_05()
            allFields["FwdRule_OAM_04"] = _AF6CCI0012_RD_IGRULE._upen_oam_profiles._FwdRule_OAM_04()
            allFields["FwdRule_OAM_03"] = _AF6CCI0012_RD_IGRULE._upen_oam_profiles._FwdRule_OAM_03()
            allFields["FwdRule_OAM_02"] = _AF6CCI0012_RD_IGRULE._upen_oam_profiles._FwdRule_OAM_02()
            allFields["FwdRule_OAM_01"] = _AF6CCI0012_RD_IGRULE._upen_oam_profiles._FwdRule_OAM_01()
            allFields["FwdRule_OAM_00"] = _AF6CCI0012_RD_IGRULE._upen_oam_profiles._FwdRule_OAM_00()
            return allFields

    class _upen_err_profiles(AtRegister.AtRegister):
        def name(self):
            return "IgRule ERR Profiles"
    
        def description(self):
            return "This register is used to config profile forward (fwd) rule for ERR status (ERR)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00_0300 + $err_profiles"
            
        def startAddress(self):
            return 0x00000300
            
        def endAddress(self):
            return 0xffffffff

        class _FwdRule_ERR_07(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 14
        
            def name(self):
                return "FwdRule_ERR_07"
            
            def description(self):
                return "FwdRule for ERR#07"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_ERR_06(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "FwdRule_ERR_06"
            
            def description(self):
                return "FwdRule for ERR#06"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_ERR_05(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "FwdRule_ERR_05"
            
            def description(self):
                return "FwdRule for ERR#05"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_ERR_04(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "FwdRule_ERR_04"
            
            def description(self):
                return "FwdRule for ERR#04"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_ERR_03(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FwdRule_ERR_03"
            
            def description(self):
                return "FwdRule for ERR#03"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_ERR_02(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FwdRule_ERR_02"
            
            def description(self):
                return "FwdRule for ERR#02"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_ERR_01(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FwdRule_ERR_01"
            
            def description(self):
                return "FwdRule for ERR#01"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_ERR_00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FwdRule_ERR_00"
            
            def description(self):
                return "FwdRule for ERR#00"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FwdRule_ERR_07"] = _AF6CCI0012_RD_IGRULE._upen_err_profiles._FwdRule_ERR_07()
            allFields["FwdRule_ERR_06"] = _AF6CCI0012_RD_IGRULE._upen_err_profiles._FwdRule_ERR_06()
            allFields["FwdRule_ERR_05"] = _AF6CCI0012_RD_IGRULE._upen_err_profiles._FwdRule_ERR_05()
            allFields["FwdRule_ERR_04"] = _AF6CCI0012_RD_IGRULE._upen_err_profiles._FwdRule_ERR_04()
            allFields["FwdRule_ERR_03"] = _AF6CCI0012_RD_IGRULE._upen_err_profiles._FwdRule_ERR_03()
            allFields["FwdRule_ERR_02"] = _AF6CCI0012_RD_IGRULE._upen_err_profiles._FwdRule_ERR_02()
            allFields["FwdRule_ERR_01"] = _AF6CCI0012_RD_IGRULE._upen_err_profiles._FwdRule_ERR_01()
            allFields["FwdRule_ERR_00"] = _AF6CCI0012_RD_IGRULE._upen_err_profiles._FwdRule_ERR_00()
            return allFields

    class _upen_bcp_profiles(AtRegister.AtRegister):
        def name(self):
            return "IgRule BCP Profiles"
    
        def description(self):
            return "This register is used to config profile forward (fwd) rule for BCP status (BCP)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00_0400 + $bcp_profiles"
            
        def startAddress(self):
            return 0x00000400
            
        def endAddress(self):
            return 0xffffffff

        class _FwdRule_BCP_03(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FwdRule_BCP_03"
            
            def description(self):
                return "FwdRule for BCP#03"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_BCP_02(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FwdRule_BCP_02"
            
            def description(self):
                return "FwdRule for BCP#02"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_BCP_01(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FwdRule_BCP_01"
            
            def description(self):
                return "FwdRule for BCP#01"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_BCP_00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FwdRule_BCP_00"
            
            def description(self):
                return "FwdRule for BCP#00"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FwdRule_BCP_03"] = _AF6CCI0012_RD_IGRULE._upen_bcp_profiles._FwdRule_BCP_03()
            allFields["FwdRule_BCP_02"] = _AF6CCI0012_RD_IGRULE._upen_bcp_profiles._FwdRule_BCP_02()
            allFields["FwdRule_BCP_01"] = _AF6CCI0012_RD_IGRULE._upen_bcp_profiles._FwdRule_BCP_01()
            allFields["FwdRule_BCP_00"] = _AF6CCI0012_RD_IGRULE._upen_bcp_profiles._FwdRule_BCP_00()
            return allFields

    class _upen_fcn_profiles(AtRegister.AtRegister):
        def name(self):
            return "IgRule FCN Profiles"
    
        def description(self):
            return "This register is used to config profile forward (fwd) rule for FCN status (FCN)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00_0500 + $fcn_profiles"
            
        def startAddress(self):
            return 0x00000500
            
        def endAddress(self):
            return 0xffffffff

        class _FwdRule_FCN_03(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FwdRule_FCN_03"
            
            def description(self):
                return "FwdRule for FCN#03"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_FCN_02(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FwdRule_FCN_02"
            
            def description(self):
                return "FwdRule for FCN#02"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_FCN_01(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FwdRule_FCN_01"
            
            def description(self):
                return "FwdRule for FCN#01"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_FCN_00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FwdRule_FCN_00"
            
            def description(self):
                return "FwdRule for FCN#00"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FwdRule_FCN_03"] = _AF6CCI0012_RD_IGRULE._upen_fcn_profiles._FwdRule_FCN_03()
            allFields["FwdRule_FCN_02"] = _AF6CCI0012_RD_IGRULE._upen_fcn_profiles._FwdRule_FCN_02()
            allFields["FwdRule_FCN_01"] = _AF6CCI0012_RD_IGRULE._upen_fcn_profiles._FwdRule_FCN_01()
            allFields["FwdRule_FCN_00"] = _AF6CCI0012_RD_IGRULE._upen_fcn_profiles._FwdRule_FCN_00()
            return allFields

    class _upen_link_id(AtRegister.AtRegister):
        def name(self):
            return "IgRule NLP FwdTable"
    
        def description(self):
            return "This register is used to config profile forward (fwd) for eache link"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00_1000 + $link_id"
            
        def startAddress(self):
            return 0x00001000
            
        def endAddress(self):
            return 0xffffffff

        class _FCN_Profile_Id(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 28
        
            def name(self):
                return "FCN_Profile_Id"
            
            def description(self):
                return "FCN_Profiles_Id"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _BCP_Profile_Id(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 24
        
            def name(self):
                return "BCP_Profile_Id"
            
            def description(self):
                return "BCP_Profiles_Id"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ERR_Profile_Id(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 20
        
            def name(self):
                return "ERR_Profile_Id"
            
            def description(self):
                return "ERR_Profiles_Id"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OAM_Profile_Id(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "OAM_Profile_Id"
            
            def description(self):
                return "OAM_Profiles_Id"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _NLP_Profile_Id(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 8
        
            def name(self):
                return "NLP_Profile_Id"
            
            def description(self):
                return "NLP_Profiles_Id"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FCN_FwdChk_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FCN_FwdChk_En"
            
            def description(self):
                return "FwdFCN check Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _BCP_FwdChk_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "BCP_FwdChk_En"
            
            def description(self):
                return "FwdBCP check Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ERR_FwdChk_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "ERR_FwdChk_En"
            
            def description(self):
                return "FwdERR check Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OAM_FwdChk_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "OAM_FwdChk_En"
            
            def description(self):
                return "FwdOAM check Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _NLP_FwdChk_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "NLP_FwdChk_En"
            
            def description(self):
                return "FwdNLP check Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FCN_Profile_Id"] = _AF6CCI0012_RD_IGRULE._upen_link_id._FCN_Profile_Id()
            allFields["BCP_Profile_Id"] = _AF6CCI0012_RD_IGRULE._upen_link_id._BCP_Profile_Id()
            allFields["ERR_Profile_Id"] = _AF6CCI0012_RD_IGRULE._upen_link_id._ERR_Profile_Id()
            allFields["OAM_Profile_Id"] = _AF6CCI0012_RD_IGRULE._upen_link_id._OAM_Profile_Id()
            allFields["NLP_Profile_Id"] = _AF6CCI0012_RD_IGRULE._upen_link_id._NLP_Profile_Id()
            allFields["FCN_FwdChk_En"] = _AF6CCI0012_RD_IGRULE._upen_link_id._FCN_FwdChk_En()
            allFields["BCP_FwdChk_En"] = _AF6CCI0012_RD_IGRULE._upen_link_id._BCP_FwdChk_En()
            allFields["ERR_FwdChk_En"] = _AF6CCI0012_RD_IGRULE._upen_link_id._ERR_FwdChk_En()
            allFields["OAM_FwdChk_En"] = _AF6CCI0012_RD_IGRULE._upen_link_id._OAM_FwdChk_En()
            allFields["NLP_FwdChk_En"] = _AF6CCI0012_RD_IGRULE._upen_link_id._NLP_FwdChk_En()
            return allFields

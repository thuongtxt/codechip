import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0012_RD_HOENC(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["upenctr1"] = _AF6CCI0012_RD_HOENC._upenctr1()
        allRegisters["upen_prm_txcfglb"] = _AF6CCI0012_RD_HOENC._upen_prm_txcfglb()
        allRegisters["pencfg_sts3c"] = _AF6CCI0012_RD_HOENC._pencfg_sts3c()
        allRegisters["upen_cnt_tdm_pkt_rc"] = _AF6CCI0012_RD_HOENC._upen_cnt_tdm_pkt_rc()
        allRegisters["upen_cnt_tdm_pkt_ro"] = _AF6CCI0012_RD_HOENC._upen_cnt_tdm_pkt_ro()
        allRegisters["upen_cnt_tdm_byte_rc"] = _AF6CCI0012_RD_HOENC._upen_cnt_tdm_byte_rc()
        allRegisters["upen_cnt_tdm_byte_ro"] = _AF6CCI0012_RD_HOENC._upen_cnt_tdm_byte_ro()
        allRegisters["upen_cnt_eop_rc"] = _AF6CCI0012_RD_HOENC._upen_cnt_eop_rc()
        allRegisters["upen_cnt_eop_ro"] = _AF6CCI0012_RD_HOENC._upen_cnt_eop_ro()
        allRegisters["upen_cnt_byte_rc"] = _AF6CCI0012_RD_HOENC._upen_cnt_byte_rc()
        allRegisters["upen_cnt_byte_ro"] = _AF6CCI0012_RD_HOENC._upen_cnt_byte_ro()
        allRegisters["upen_cnt_enc_byteotdm_rc"] = _AF6CCI0012_RD_HOENC._upen_cnt_enc_byteotdm_rc()
        allRegisters["upen_cnt_enc_byteotdm_ro"] = _AF6CCI0012_RD_HOENC._upen_cnt_enc_byteotdm_ro()
        allRegisters["upen_cnt_enc_pktotdm_rc"] = _AF6CCI0012_RD_HOENC._upen_cnt_enc_pktotdm_rc()
        allRegisters["upen_cnt_enc_pktotdm_ro"] = _AF6CCI0012_RD_HOENC._upen_cnt_enc_pktotdm_ro()
        allRegisters["upen_cnt_enc_eopfrmpda_rc"] = _AF6CCI0012_RD_HOENC._upen_cnt_enc_eopfrmpda_rc()
        allRegisters["upen_cnt_enc_eopfrmpda_ro"] = _AF6CCI0012_RD_HOENC._upen_cnt_enc_eopfrmpda_ro()
        allRegisters["upen_cnt_enc_sopfrmpda_rc"] = _AF6CCI0012_RD_HOENC._upen_cnt_enc_sopfrmpda_rc()
        allRegisters["upen_cnt_enc_sopfrmpda_ro"] = _AF6CCI0012_RD_HOENC._upen_cnt_enc_sopfrmpda_ro()
        allRegisters["upen_cnt_enc_bytefrmpda_rc"] = _AF6CCI0012_RD_HOENC._upen_cnt_enc_bytefrmpda_rc()
        allRegisters["upen_cnt_enc_bytefrmpda_ro"] = _AF6CCI0012_RD_HOENC._upen_cnt_enc_bytefrmpda_ro()
        allRegisters["upen_cnt_enc_abort_rc"] = _AF6CCI0012_RD_HOENC._upen_cnt_enc_abort_rc()
        allRegisters["upen_cnt_enc_abort_ro"] = _AF6CCI0012_RD_HOENC._upen_cnt_enc_abort_ro()
        allRegisters["upen_cnt_enc_nstuff_rc"] = _AF6CCI0012_RD_HOENC._upen_cnt_enc_nstuff_rc()
        allRegisters["upen_cnt_enc_nstuff_ro"] = _AF6CCI0012_RD_HOENC._upen_cnt_enc_nstuff_ro()
        return allRegisters

    class _upenctr1(AtRegister.AtRegister):
        def name(self):
            return "upenctr1"
    
        def description(self):
            return "This register is used to configure as below(Add 24bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0_0200 + $chid"
            
        def startAddress(self):
            return 0x00000200
            
        def endAddress(self):
            return 0x0000023f

        class _scrable_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "scrable_en"
            
            def description(self):
                return "scrable_en"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _idle_mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "idle_mode"
            
            def description(self):
                return "0: 0x7E 1:0xFF"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fcsmod32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "fcsmod32"
            
            def description(self):
                return "fcsmod32"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fcsinscfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "fcsinscfg"
            
            def description(self):
                return "fcsinscfg"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fcsmsb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "fcsmsb"
            
            def description(self):
                return "fcsmsb"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _bit_stuff(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "bit_stuff"
            
            def description(self):
                return "bit_stuff"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["scrable_en"] = _AF6CCI0012_RD_HOENC._upenctr1._scrable_en()
            allFields["idle_mode"] = _AF6CCI0012_RD_HOENC._upenctr1._idle_mode()
            allFields["fcsmod32"] = _AF6CCI0012_RD_HOENC._upenctr1._fcsmod32()
            allFields["fcsinscfg"] = _AF6CCI0012_RD_HOENC._upenctr1._fcsinscfg()
            allFields["fcsmsb"] = _AF6CCI0012_RD_HOENC._upenctr1._fcsmsb()
            allFields["bit_stuff"] = _AF6CCI0012_RD_HOENC._upenctr1._bit_stuff()
            return allFields

    class _upen_prm_txcfglb(AtRegister.AtRegister):
        def name(self):
            return "CONFIG HO ENC STSnc"
    
        def description(self):
            return "Config stsnc"
            
        def width(self):
            return 24
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xE00"
            
        def startAddress(self):
            return 0x00000e00
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_txenc_fcsmsd3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "cfg_txenc_fcsmsd3"
            
            def description(self):
                return "config calculation FCS for group 3 (0) is LSB, (1) is MSB"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_txenc_fcsmode3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "cfg_txenc_fcsmode3"
            
            def description(self):
                return "config fcs mode for group 3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_txenc_fcsins3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "cfg_txenc_fcsins3"
            
            def description(self):
                return "config fcs insert for group 3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_txenc_fcsmsd2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "cfg_txenc_fcsmsd2"
            
            def description(self):
                return "config calculation FCS for group 2 (0) is LSB, (1) is MSB"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_txenc_fcsmode2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "cfg_txenc_fcsmode2"
            
            def description(self):
                return "config fcs mode for group 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_txenc_fcsins2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "cfg_txenc_fcsins2"
            
            def description(self):
                return "config fcs insert for group 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_txenc_fcsmsd1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "cfg_txenc_fcsmsd1"
            
            def description(self):
                return "config calculation FCS for group 1 (0) is LSB, (1) is MSB"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_txenc_fcsmode1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "cfg_txenc_fcsmode1"
            
            def description(self):
                return "config fcs mode for group 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_txenc_fcsins1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "cfg_txenc_fcsins1"
            
            def description(self):
                return "config fcs insert for group 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_txenc_fcsmsd0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "cfg_txenc_fcsmsd0"
            
            def description(self):
                return "config calculation FCS for group 0 (0) is LSB, (1) is MSB"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_txenc_fcsmode0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "cfg_txenc_fcsmode0"
            
            def description(self):
                return "config fcs mode for group 0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_txenc_fcsins0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "cfg_txenc_fcsins0"
            
            def description(self):
                return "config fcs insert for group 0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_threshold(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 7
        
            def name(self):
                return "cfg_threshold"
            
            def description(self):
                return "config threshold FIFO store data"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_sts48c(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "cfg_sts48c"
            
            def description(self):
                return "enable sts48c, (0) is disable, (1) is enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_sts24c(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "cfg_sts24c"
            
            def description(self):
                return "[4] : enable group 0 (0,2,4,6..), [5] : group 1 (1,3,5,7..), (0) is disable, (1) is enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_sts12c(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_sts12c"
            
            def description(self):
                return "[3] : group 3 (3,7,11..) , [2] : group 2 (2,6,10..),[1] : group 1 (1,5,9..), [0] : group 0 (0,4,8...), (0) is disable, (1) is enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_txenc_fcsmsd3"] = _AF6CCI0012_RD_HOENC._upen_prm_txcfglb._cfg_txenc_fcsmsd3()
            allFields["cfg_txenc_fcsmode3"] = _AF6CCI0012_RD_HOENC._upen_prm_txcfglb._cfg_txenc_fcsmode3()
            allFields["cfg_txenc_fcsins3"] = _AF6CCI0012_RD_HOENC._upen_prm_txcfglb._cfg_txenc_fcsins3()
            allFields["cfg_txenc_fcsmsd2"] = _AF6CCI0012_RD_HOENC._upen_prm_txcfglb._cfg_txenc_fcsmsd2()
            allFields["cfg_txenc_fcsmode2"] = _AF6CCI0012_RD_HOENC._upen_prm_txcfglb._cfg_txenc_fcsmode2()
            allFields["cfg_txenc_fcsins2"] = _AF6CCI0012_RD_HOENC._upen_prm_txcfglb._cfg_txenc_fcsins2()
            allFields["cfg_txenc_fcsmsd1"] = _AF6CCI0012_RD_HOENC._upen_prm_txcfglb._cfg_txenc_fcsmsd1()
            allFields["cfg_txenc_fcsmode1"] = _AF6CCI0012_RD_HOENC._upen_prm_txcfglb._cfg_txenc_fcsmode1()
            allFields["cfg_txenc_fcsins1"] = _AF6CCI0012_RD_HOENC._upen_prm_txcfglb._cfg_txenc_fcsins1()
            allFields["cfg_txenc_fcsmsd0"] = _AF6CCI0012_RD_HOENC._upen_prm_txcfglb._cfg_txenc_fcsmsd0()
            allFields["cfg_txenc_fcsmode0"] = _AF6CCI0012_RD_HOENC._upen_prm_txcfglb._cfg_txenc_fcsmode0()
            allFields["cfg_txenc_fcsins0"] = _AF6CCI0012_RD_HOENC._upen_prm_txcfglb._cfg_txenc_fcsins0()
            allFields["cfg_threshold"] = _AF6CCI0012_RD_HOENC._upen_prm_txcfglb._cfg_threshold()
            allFields["cfg_sts48c"] = _AF6CCI0012_RD_HOENC._upen_prm_txcfglb._cfg_sts48c()
            allFields["cfg_sts24c"] = _AF6CCI0012_RD_HOENC._upen_prm_txcfglb._cfg_sts24c()
            allFields["cfg_sts12c"] = _AF6CCI0012_RD_HOENC._upen_prm_txcfglb._cfg_sts12c()
            return allFields

    class _pencfg_sts3c(AtRegister.AtRegister):
        def name(self):
            return "pencfg_sts3c"
    
        def description(self):
            return "This register is used to cfg STS3C mode (Add 24bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000007
            
        def endAddress(self):
            return 0xffffffff

        class _sts3c_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "sts3c_en"
            
            def description(self):
                return "[15]group15 (15,33,47)..., [1]:group1 (1,17,33) , [0]:group0 (0,16,32) 1: act 0: dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["sts3c_en"] = _AF6CCI0012_RD_HOENC._pencfg_sts3c._sts3c_en()
            return allFields

    class _upen_cnt_tdm_pkt_rc(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_tdm_pkt"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x280+ $chid"
            
        def startAddress(self):
            return 0x00000280
            
        def endAddress(self):
            return 0x000002bf

        class _updo_cnt_sop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_sop"
            
            def description(self):
                return "sop cnt enc"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_sop"] = _AF6CCI0012_RD_HOENC._upen_cnt_tdm_pkt_rc._updo_cnt_sop()
            return allFields

    class _upen_cnt_tdm_pkt_ro(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_tdm_pkt"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x2C0+ $chid"
            
        def startAddress(self):
            return 0x000002c0
            
        def endAddress(self):
            return 0x000002ff

        class _updo_cnt_sop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_sop"
            
            def description(self):
                return "sop cnt enc"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_sop"] = _AF6CCI0012_RD_HOENC._upen_cnt_tdm_pkt_ro._updo_cnt_sop()
            return allFields

    class _upen_cnt_tdm_byte_rc(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_tdm_byte"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x400+ $chid"
            
        def startAddress(self):
            return 0x00000400
            
        def endAddress(self):
            return 0x0000043f

        class _updo_cnt_tdm_byte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_tdm_byte"
            
            def description(self):
                return "cnt byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_tdm_byte"] = _AF6CCI0012_RD_HOENC._upen_cnt_tdm_byte_rc._updo_cnt_tdm_byte()
            return allFields

    class _upen_cnt_tdm_byte_ro(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_tdm_byte"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x440+ $chid"
            
        def startAddress(self):
            return 0x00000440
            
        def endAddress(self):
            return 0x0000047f

        class _updo_cnt_tdm_byte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_tdm_byte"
            
            def description(self):
                return "cnt byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_tdm_byte"] = _AF6CCI0012_RD_HOENC._upen_cnt_tdm_byte_ro._updo_cnt_tdm_byte()
            return allFields

    class _upen_cnt_eop_rc(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_eop"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x300+ $chid"
            
        def startAddress(self):
            return 0x00000300
            
        def endAddress(self):
            return 0x0000033f

        class _updo_cnt_eop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_eop"
            
            def description(self):
                return "eop cnt enc"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_eop"] = _AF6CCI0012_RD_HOENC._upen_cnt_eop_rc._updo_cnt_eop()
            return allFields

    class _upen_cnt_eop_ro(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_eop"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x340+ $chid"
            
        def startAddress(self):
            return 0x00000340
            
        def endAddress(self):
            return 0x0000037f

        class _updo_cnt_eop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_eop"
            
            def description(self):
                return "eop cnt enc"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_eop"] = _AF6CCI0012_RD_HOENC._upen_cnt_eop_ro._updo_cnt_eop()
            return allFields

    class _upen_cnt_byte_rc(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_byte"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x380+ $chid"
            
        def startAddress(self):
            return 0x00000380
            
        def endAddress(self):
            return 0x000003bf

        class _updo_cnt_byte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_byte"
            
            def description(self):
                return "cnt byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_byte"] = _AF6CCI0012_RD_HOENC._upen_cnt_byte_rc._updo_cnt_byte()
            return allFields

    class _upen_cnt_byte_ro(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_byte"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3C0+ $chid"
            
        def startAddress(self):
            return 0x000003c0
            
        def endAddress(self):
            return 0x000003ff

        class _updo_cnt_byte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_byte"
            
            def description(self):
                return "cnt byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_byte"] = _AF6CCI0012_RD_HOENC._upen_cnt_byte_ro._updo_cnt_byte()
            return allFields

    class _upen_cnt_enc_byteotdm_rc(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_enc_byteotdm"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000010
            
        def endAddress(self):
            return 0xffffffff

        class _updo_cnt_enc_byteotdm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_enc_byteotdm"
            
            def description(self):
                return "cnt byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_enc_byteotdm"] = _AF6CCI0012_RD_HOENC._upen_cnt_enc_byteotdm_rc._updo_cnt_enc_byteotdm()
            return allFields

    class _upen_cnt_enc_byteotdm_ro(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_enc_byteotdm"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000012
            
        def endAddress(self):
            return 0xffffffff

        class _updo_cnt_enc_byteotdm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_enc_byteotdm"
            
            def description(self):
                return "cnt byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_enc_byteotdm"] = _AF6CCI0012_RD_HOENC._upen_cnt_enc_byteotdm_ro._updo_cnt_enc_byteotdm()
            return allFields

    class _upen_cnt_enc_pktotdm_rc(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_enc_pktotdm"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000020
            
        def endAddress(self):
            return 0xffffffff

        class _updo_cnt_enc_pktotdm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_enc_pktotdm"
            
            def description(self):
                return "cnt pkt"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_enc_pktotdm"] = _AF6CCI0012_RD_HOENC._upen_cnt_enc_pktotdm_rc._updo_cnt_enc_pktotdm()
            return allFields

    class _upen_cnt_enc_pktotdm_ro(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_enc_pktotdm"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000022
            
        def endAddress(self):
            return 0xffffffff

        class _updo_cnt_enc_pktotdm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_enc_pktotdm"
            
            def description(self):
                return "cnt pkt"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_enc_pktotdm"] = _AF6CCI0012_RD_HOENC._upen_cnt_enc_pktotdm_ro._updo_cnt_enc_pktotdm()
            return allFields

    class _upen_cnt_enc_eopfrmpda_rc(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_enc_eopfrmpda"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000014
            
        def endAddress(self):
            return 0xffffffff

        class _updo_cnt_enc_eopfrmpda(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_enc_eopfrmpda"
            
            def description(self):
                return "cnt pkt"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_enc_eopfrmpda"] = _AF6CCI0012_RD_HOENC._upen_cnt_enc_eopfrmpda_rc._updo_cnt_enc_eopfrmpda()
            return allFields

    class _upen_cnt_enc_eopfrmpda_ro(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_enc_eopfrmpda"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000016
            
        def endAddress(self):
            return 0xffffffff

        class _updo_cnt_enc_eopfrmpda(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_enc_eopfrmpda"
            
            def description(self):
                return "cnt pkt"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_enc_eopfrmpda"] = _AF6CCI0012_RD_HOENC._upen_cnt_enc_eopfrmpda_ro._updo_cnt_enc_eopfrmpda()
            return allFields

    class _upen_cnt_enc_sopfrmpda_rc(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_enc_sopfrmpda"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000018
            
        def endAddress(self):
            return 0xffffffff

        class _updo_cnt_enc_sopfrmpda(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_enc_sopfrmpda"
            
            def description(self):
                return "cnt pkt"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_enc_sopfrmpda"] = _AF6CCI0012_RD_HOENC._upen_cnt_enc_sopfrmpda_rc._updo_cnt_enc_sopfrmpda()
            return allFields

    class _upen_cnt_enc_sopfrmpda_ro(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_enc_sopfrmpda"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000001a
            
        def endAddress(self):
            return 0xffffffff

        class _updo_cnt_enc_sopfrmpda(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_enc_sopfrmpda"
            
            def description(self):
                return "cnt pkt"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_enc_sopfrmpda"] = _AF6CCI0012_RD_HOENC._upen_cnt_enc_sopfrmpda_ro._updo_cnt_enc_sopfrmpda()
            return allFields

    class _upen_cnt_enc_bytefrmpda_rc(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_enc_bytefrmpda"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000001c
            
        def endAddress(self):
            return 0xffffffff

        class _updo_cnt_enc_bytefrmpda(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_enc_bytefrmpda"
            
            def description(self):
                return "cnt byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_enc_bytefrmpda"] = _AF6CCI0012_RD_HOENC._upen_cnt_enc_bytefrmpda_rc._updo_cnt_enc_bytefrmpda()
            return allFields

    class _upen_cnt_enc_bytefrmpda_ro(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_enc_bytefrmpda"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000001e
            
        def endAddress(self):
            return 0xffffffff

        class _updo_cnt_enc_bytefrmpda(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_enc_bytefrmpda"
            
            def description(self):
                return "cnt byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_enc_bytefrmpda"] = _AF6CCI0012_RD_HOENC._upen_cnt_enc_bytefrmpda_ro._updo_cnt_enc_bytefrmpda()
            return allFields

    class _upen_cnt_enc_abort_rc(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_enc_abort"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000024
            
        def endAddress(self):
            return 0xffffffff

        class _updo_cnt_enc_abort(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_enc_abort"
            
            def description(self):
                return "abort pkt"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_enc_abort"] = _AF6CCI0012_RD_HOENC._upen_cnt_enc_abort_rc._updo_cnt_enc_abort()
            return allFields

    class _upen_cnt_enc_abort_ro(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_enc_abort"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000026
            
        def endAddress(self):
            return 0xffffffff

        class _updo_cnt_enc_abort(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_enc_abort"
            
            def description(self):
                return "abort pkt"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_enc_abort"] = _AF6CCI0012_RD_HOENC._upen_cnt_enc_abort_ro._updo_cnt_enc_abort()
            return allFields

    class _upen_cnt_enc_nstuff_rc(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_enc_nstuff"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000028
            
        def endAddress(self):
            return 0xffffffff

        class _updo_cnt_enc_nstuff(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_enc_nstuff"
            
            def description(self):
                return "num byte stuff"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_enc_nstuff"] = _AF6CCI0012_RD_HOENC._upen_cnt_enc_nstuff_rc._updo_cnt_enc_nstuff()
            return allFields

    class _upen_cnt_enc_nstuff_ro(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_enc_nstuff"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000002a
            
        def endAddress(self):
            return 0xffffffff

        class _updo_cnt_enc_nstuff(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_enc_nstuff"
            
            def description(self):
                return "num byte stuff"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_enc_nstuff"] = _AF6CCI0012_RD_HOENC._upen_cnt_enc_nstuff_ro._updo_cnt_enc_nstuff()
            return allFields

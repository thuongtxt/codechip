import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0012_RD_MPEG(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["cfg0qthsh_pen"] = _AF6CCI0012_RD_MPEG._cfg0qthsh_pen()
        allRegisters["cfg1qthsh_pen"] = _AF6CCI0012_RD_MPEG._cfg1qthsh_pen()
        allRegisters["cfg2qthsh_pen"] = _AF6CCI0012_RD_MPEG._cfg2qthsh_pen()
        allRegisters["cfg3qthsh_pen"] = _AF6CCI0012_RD_MPEG._cfg3qthsh_pen()
        allRegisters["memidcfg0_pen"] = _AF6CCI0012_RD_MPEG._memidcfg0_pen()
        allRegisters["lididcfg1_pen"] = _AF6CCI0012_RD_MPEG._lididcfg1_pen()
        allRegisters["membmidcfg0_pen"] = _AF6CCI0012_RD_MPEG._membmidcfg0_pen()
        allRegisters["schelinkicfg0_pen"] = _AF6CCI0012_RD_MPEG._schelinkicfg0_pen()
        allRegisters["schelinkicfg1_pen"] = _AF6CCI0012_RD_MPEG._schelinkicfg1_pen()
        allRegisters["schebundicfg0_pen"] = _AF6CCI0012_RD_MPEG._schebundicfg0_pen()
        allRegisters["schebundcfg1_pen"] = _AF6CCI0012_RD_MPEG._schebundcfg1_pen()
        allRegisters["schevcaticfg0_pen"] = _AF6CCI0012_RD_MPEG._schevcaticfg0_pen()
        allRegisters["schevcatcfg1_pen"] = _AF6CCI0012_RD_MPEG._schevcatcfg1_pen()
        allRegisters["scheetheicfg0_pen"] = _AF6CCI0012_RD_MPEG._scheetheicfg0_pen()
        allRegisters["scheethercfg1_pen"] = _AF6CCI0012_RD_MPEG._scheethercfg1_pen()
        allRegisters["stk0_pen"] = _AF6CCI0012_RD_MPEG._stk0_pen()
        allRegisters["stk1_pen"] = _AF6CCI0012_RD_MPEG._stk1_pen()
        allRegisters["stk2_pen"] = _AF6CCI0012_RD_MPEG._stk2_pen()
        allRegisters["stk3_pen"] = _AF6CCI0012_RD_MPEG._stk3_pen()
        allRegisters["glbcnt_debug"] = _AF6CCI0012_RD_MPEG._glbcnt_debug()
        return allRegisters

    class _cfg0qthsh_pen(AtRegister.AtRegister):
        def name(self):
            return "Bundle Queue Threshold Control"
    
        def description(self):
            return "Used to configure threshold for Bundle Queue : {qid(0000-2047)} -> For Bundle Queue,512 Bundles, 4 Queues per Bundle HDL_PATH: rtlmpeg.qmxff.cfg0mem.ram.ram[$qid]"
            
        def width(self):
            return 21
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x20000+$qid"
            
        def startAddress(self):
            return 0x00020000
            
        def endAddress(self):
            return 0x000207ff

        class _Queue_Enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "Queue_Enb"
            
            def description(self):
                return "Queue Enable 0: Disable Queue and flush Queue 1: Enable Queue"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _EFFQthsh_max(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 15
        
            def name(self):
                return "EFFQthsh_max"
            
            def description(self):
                return "External FiFo maximun queue threshold, step 128 pkts"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _EFFQthsh_min(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 10
        
            def name(self):
                return "EFFQthsh_min"
            
            def description(self):
                return "External FiFo minimun queue threshold, step 128 pkts"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _IFFQthsh_max(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 5
        
            def name(self):
                return "IFFQthsh_max"
            
            def description(self):
                return "Internal FiFo maximun queue threshold, step 1 pkts"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _IFFQthsh_min(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IFFQthsh_min"
            
            def description(self):
                return "Internal FiFo minimun queue threshold, step 1 pkts"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Queue_Enb"] = _AF6CCI0012_RD_MPEG._cfg0qthsh_pen._Queue_Enb()
            allFields["EFFQthsh_max"] = _AF6CCI0012_RD_MPEG._cfg0qthsh_pen._EFFQthsh_max()
            allFields["EFFQthsh_min"] = _AF6CCI0012_RD_MPEG._cfg0qthsh_pen._EFFQthsh_min()
            allFields["IFFQthsh_max"] = _AF6CCI0012_RD_MPEG._cfg0qthsh_pen._IFFQthsh_max()
            allFields["IFFQthsh_min"] = _AF6CCI0012_RD_MPEG._cfg0qthsh_pen._IFFQthsh_min()
            return allFields

    class _cfg1qthsh_pen(AtRegister.AtRegister):
        def name(self):
            return "Vcat Queue Threshold Control"
    
        def description(self):
            return "Used to configure threshold for Vcat Queue HDL_PATH: rtlmpeg.qmxff.cfg1mem.ram.ram[$qid] : {qid(0-511)} -> For Vcat Queue,256 VCG, 2 queues per VCG (Queue#0: in use,Queue#1: reserved)"
            
        def width(self):
            return 21
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x21000+$qid"
            
        def startAddress(self):
            return 0x00021000
            
        def endAddress(self):
            return 0x000211ff

        class _Queue_Enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "Queue_Enb"
            
            def description(self):
                return "Queue Enable 0: Disable Queue and flush Queue 1: Enable Queue"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _EFFQthsh_max(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 15
        
            def name(self):
                return "EFFQthsh_max"
            
            def description(self):
                return "External FiFo maximun queue threshold, step 128 pkts"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _EFFQthsh_min(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 10
        
            def name(self):
                return "EFFQthsh_min"
            
            def description(self):
                return "External FiFo minimun queue threshold, step 128 pkts"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _IFFQthsh_max(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 5
        
            def name(self):
                return "IFFQthsh_max"
            
            def description(self):
                return "Internal FiFo maximun queue threshold, step 1 pkts"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _IFFQthsh_min(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IFFQthsh_min"
            
            def description(self):
                return "Internal FiFo minimun queue threshold, step 1 pkts"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Queue_Enb"] = _AF6CCI0012_RD_MPEG._cfg1qthsh_pen._Queue_Enb()
            allFields["EFFQthsh_max"] = _AF6CCI0012_RD_MPEG._cfg1qthsh_pen._EFFQthsh_max()
            allFields["EFFQthsh_min"] = _AF6CCI0012_RD_MPEG._cfg1qthsh_pen._EFFQthsh_min()
            allFields["IFFQthsh_max"] = _AF6CCI0012_RD_MPEG._cfg1qthsh_pen._IFFQthsh_max()
            allFields["IFFQthsh_min"] = _AF6CCI0012_RD_MPEG._cfg1qthsh_pen._IFFQthsh_min()
            return allFields

    class _cfg2qthsh_pen(AtRegister.AtRegister):
        def name(self):
            return "Link Queue Threshold Control"
    
        def description(self):
            return "Used to configure threshold for Link Queue : {qid(0-6143)} -> For Link Queue, 3072 Links, 2 Queues per Link HDL_PATH: rtlmpeg.qmxff.cfg2mem.ram.ram[$qid]"
            
        def width(self):
            return 21
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x22000+$qid"
            
        def startAddress(self):
            return 0x00022000
            
        def endAddress(self):
            return 0x000237ff

        class _Queue_Enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "Queue_Enb"
            
            def description(self):
                return "Queue Enable 0: Disable Queue and flush Queue 1: Enable Queue"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _EFFQthsh_max(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 15
        
            def name(self):
                return "EFFQthsh_max"
            
            def description(self):
                return "External FiFo maximun queue threshold, step 128 pkts"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _EFFQthsh_min(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 10
        
            def name(self):
                return "EFFQthsh_min"
            
            def description(self):
                return "External FiFo minimun queue threshold, step 128 pkts"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _IFFQthsh_max(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 5
        
            def name(self):
                return "IFFQthsh_max"
            
            def description(self):
                return "Internal FiFo maximun queue threshold, step 1 pkts"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _IFFQthsh_min(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IFFQthsh_min"
            
            def description(self):
                return "Internal FiFo minimun queue threshold, step 1 pkts"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Queue_Enb"] = _AF6CCI0012_RD_MPEG._cfg2qthsh_pen._Queue_Enb()
            allFields["EFFQthsh_max"] = _AF6CCI0012_RD_MPEG._cfg2qthsh_pen._EFFQthsh_max()
            allFields["EFFQthsh_min"] = _AF6CCI0012_RD_MPEG._cfg2qthsh_pen._EFFQthsh_min()
            allFields["IFFQthsh_max"] = _AF6CCI0012_RD_MPEG._cfg2qthsh_pen._IFFQthsh_max()
            allFields["IFFQthsh_min"] = _AF6CCI0012_RD_MPEG._cfg2qthsh_pen._IFFQthsh_min()
            return allFields

    class _cfg3qthsh_pen(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Bypass Queue Threshold Control"
    
        def description(self):
            return "Used to configure threshold for Link Queue : {qid(0-15)} -> 16 Queues for Bypass Ethernet Port : 0-7: for GE ethernet Bypass, 4 port GE, 2 queue each port : 8-9: for eXAUI#0  Bypass : 10-11: for eXAUI#1  Bypass HDL_PATH: rtlmpeg.qmxff.cfg3mem.ram.ram[$qid]"
            
        def width(self):
            return 21
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x24000+$qid"
            
        def startAddress(self):
            return 0x00024000
            
        def endAddress(self):
            return 0x00240015

        class _Queue_Enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "Queue_Enb"
            
            def description(self):
                return "Queue Enable 0: Disable Queue and flush Queue 1: Enable Queue"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _EFFQthsh_max(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 15
        
            def name(self):
                return "EFFQthsh_max"
            
            def description(self):
                return "External FiFo maximun queue threshold, step 128 pkts"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _EFFQthsh_min(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 10
        
            def name(self):
                return "EFFQthsh_min"
            
            def description(self):
                return "External FiFo minimun queue threshold, step 128 pkts"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _IFFQthsh_max(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 5
        
            def name(self):
                return "IFFQthsh_max"
            
            def description(self):
                return "Internal FiFo maximun queue threshold, step 1 pkts"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _IFFQthsh_min(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IFFQthsh_min"
            
            def description(self):
                return "Internal FiFo minimun queue threshold, step 1 pkts"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Queue_Enb"] = _AF6CCI0012_RD_MPEG._cfg3qthsh_pen._Queue_Enb()
            allFields["EFFQthsh_max"] = _AF6CCI0012_RD_MPEG._cfg3qthsh_pen._EFFQthsh_max()
            allFields["EFFQthsh_min"] = _AF6CCI0012_RD_MPEG._cfg3qthsh_pen._EFFQthsh_min()
            allFields["IFFQthsh_max"] = _AF6CCI0012_RD_MPEG._cfg3qthsh_pen._IFFQthsh_max()
            allFields["IFFQthsh_min"] = _AF6CCI0012_RD_MPEG._cfg3qthsh_pen._IFFQthsh_min()
            return allFields

    class _memidcfg0_pen(AtRegister.AtRegister):
        def name(self):
            return "Member ID of Bundle Configuration"
    
        def description(self):
            return "Support 512 bundle, each Bundle have 32 Members. This used to configure Member Enable and Member ID of Bundle Member ID is used to Link Distribution for Round-Robin mode and strict priority mode Member #0 is higest priority HDL_PATH: rtlmpeg.schel.cfg0mem.ram.ram[$lid]"
            
        def width(self):
            return 6
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0C000+$lid"
            
        def startAddress(self):
            return 0x0000c000
            
        def endAddress(self):
            return 0x0000cbff

        class _MemBundle_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "MemBundle_En"
            
            def description(self):
                return "Member Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MemBundle_ID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MemBundle_ID"
            
            def description(self):
                return "Member ID of Bundle"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MemBundle_En"] = _AF6CCI0012_RD_MPEG._memidcfg0_pen._MemBundle_En()
            allFields["MemBundle_ID"] = _AF6CCI0012_RD_MPEG._memidcfg0_pen._MemBundle_ID()
            return allFields

    class _lididcfg1_pen(AtRegister.AtRegister):
        def name(self):
            return "Link Distribution per Bundle"
    
        def description(self):
            return "Used to configure the mode of Link Distribution HDL_PATH: rtlmpeg.schel.cfg1mem.ram.ram[$bid]"
            
        def width(self):
            return 3
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x10000+$bid"
            
        def startAddress(self):
            return 0x00010000
            
        def endAddress(self):
            return 0x000101ff

        class _LinkDisMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LinkDisMode"
            
            def description(self):
                return "Link Distribution Mode - 0: Random - 1: Round Robin - 2: Strict Priority - 3: Smart"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["LinkDisMode"] = _AF6CCI0012_RD_MPEG._lididcfg1_pen._LinkDisMode()
            return allFields

    class _membmidcfg0_pen(AtRegister.AtRegister):
        def name(self):
            return "Member Bitmap of Bundle"
    
        def description(self):
            return "This is used to support HW engine Round-Robin and know the priority of Member - For Round Robin mode of Link Distribution : this register is used to round robin - For Strict mode of Link Distribution: this register is used to HW engine know the Member Priority HDL_PATH: rtlmpeg.schel.cfg2mem.ram.ram[$bid]"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x11000+$bid"
            
        def startAddress(self):
            return 0x00011000
            
        def endAddress(self):
            return 0x000111ff

        class _MemBundle_ID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MemBundle_ID"
            
            def description(self):
                return "Member Bimap of Bundle"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MemBundle_ID"] = _AF6CCI0012_RD_MPEG._membmidcfg0_pen._MemBundle_ID()
            return allFields

    class _schelinkicfg0_pen(AtRegister.AtRegister):
        def name(self):
            return "Scheduler: Group priority Configuration Per Link"
    
        def description(self):
            return "Used to configure the priority queue per Link. - Support 3 group: Low Priority(LSP), High Priority(HSP), DWRR, mix mode. - HSP: Higest Priority - LSP: Lowest Priority - DWRR: Medium Priority HDL_PATH: rtlmpeg.schel.lbitmap.cfg1mem.ram.ram[$lid]"
            
        def width(self):
            return 6
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x02000 + $lid"
            
        def startAddress(self):
            return 0x00002000
            
        def endAddress(self):
            return 0x00002bff

        class _Hsp_grp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Hsp_grp"
            
            def description(self):
                return "HSP Queue"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _DWRR_grp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "DWRR_grp"
            
            def description(self):
                return "DWRR Queue"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Lsp_grp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Lsp_grp"
            
            def description(self):
                return "LSP Queue"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hsp_grp"] = _AF6CCI0012_RD_MPEG._schelinkicfg0_pen._Hsp_grp()
            allFields["DWRR_grp"] = _AF6CCI0012_RD_MPEG._schelinkicfg0_pen._DWRR_grp()
            allFields["Lsp_grp"] = _AF6CCI0012_RD_MPEG._schelinkicfg0_pen._Lsp_grp()
            return allFields

    class _schelinkicfg1_pen(AtRegister.AtRegister):
        def name(self):
            return "DWRR Quantum Configuration"
    
        def description(self):
            return "Used to configure quantum for per Link queue in DWRR scheduling mode HDL_PATH: rtlmpeg.schel.lbitmap.cfg0mem.ram.ram[$lqid]"
            
        def width(self):
            return 20
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00000+$lqid"
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0x000017ff

        class _Linkshce_DWRR_QTum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Linkshce_DWRR_QTum"
            
            def description(self):
                return "DWRR Quantum,Minximun MTU"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Linkshce_DWRR_QTum"] = _AF6CCI0012_RD_MPEG._schelinkicfg1_pen._Linkshce_DWRR_QTum()
            return allFields

    class _schebundicfg0_pen(AtRegister.AtRegister):
        def name(self):
            return "Scheduler: Group priority Configuration Per Bundle"
    
        def description(self):
            return "Used to configure the priority queue per Link. - Support 3 group: Low Priority(LSP), High Priority(HSP), DWRR, mix mode. - HSP: Higest Priority - LSP: Lowest Priority - DWRR: Medium Priority HDL_PATH: rtlmpeg.schel.bbitmap.cfg1mem.ram.ram[$bid]"
            
        def width(self):
            return 12
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x06000 + $bid"
            
        def startAddress(self):
            return 0x00006000
            
        def endAddress(self):
            return 0x000061ff

        class _Hsp_grp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Hsp_grp"
            
            def description(self):
                return "HSP Queue"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _DWRR_grp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "DWRR_grp"
            
            def description(self):
                return "DWRR Queue"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Lsp_grp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Lsp_grp"
            
            def description(self):
                return "LSP Queue"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hsp_grp"] = _AF6CCI0012_RD_MPEG._schebundicfg0_pen._Hsp_grp()
            allFields["DWRR_grp"] = _AF6CCI0012_RD_MPEG._schebundicfg0_pen._DWRR_grp()
            allFields["Lsp_grp"] = _AF6CCI0012_RD_MPEG._schebundicfg0_pen._Lsp_grp()
            return allFields

    class _schebundcfg1_pen(AtRegister.AtRegister):
        def name(self):
            return "DWRR Quantum Configuration"
    
        def description(self):
            return "Used to configure quantum for per Bundle in DWRR scheduling mode HDL_PATH: rtlmpeg.schel.bbitmap.cfg0mem.ram.ram[$bqid]"
            
        def width(self):
            return 20
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x04000+$bqid"
            
        def startAddress(self):
            return 0x00004000
            
        def endAddress(self):
            return 0x000051ff

        class _Bundshce_DWRR_QTum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Bundshce_DWRR_QTum"
            
            def description(self):
                return "DWRR Quantum,Minximun MTU"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Bundshce_DWRR_QTum"] = _AF6CCI0012_RD_MPEG._schebundcfg1_pen._Bundshce_DWRR_QTum()
            return allFields

    class _schevcaticfg0_pen(AtRegister.AtRegister):
        def name(self):
            return "Scheduler: Group priority Configuration Per Vcat"
    
        def description(self):
            return "Used to configure the priority queue per Link. - Support 3 group: Low Priority(LSP), High Priority(HSP), DWRR, mix mode. - HSP: Higest Priority - LSP: Lowest Priority - DWRR: Medium Priority HDL_PATH: rtlmpeg.schel.vbitmap.cfg1mem.ram.ram[$vid]"
            
        def width(self):
            return 6
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0A000 + $vid"
            
        def startAddress(self):
            return 0x0000a000
            
        def endAddress(self):
            return 0x0000a0ff

        class _Hsp_grp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Hsp_grp"
            
            def description(self):
                return "HSP Queue"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _DWRR_grp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "DWRR_grp"
            
            def description(self):
                return "DWRR Queue"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Lsp_grp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Lsp_grp"
            
            def description(self):
                return "LSP Queue"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hsp_grp"] = _AF6CCI0012_RD_MPEG._schevcaticfg0_pen._Hsp_grp()
            allFields["DWRR_grp"] = _AF6CCI0012_RD_MPEG._schevcaticfg0_pen._DWRR_grp()
            allFields["Lsp_grp"] = _AF6CCI0012_RD_MPEG._schevcaticfg0_pen._Lsp_grp()
            return allFields

    class _schevcatcfg1_pen(AtRegister.AtRegister):
        def name(self):
            return "DWRR Quantum Configuration"
    
        def description(self):
            return "Used to configure quantum for per Vcat Queue in DWRR scheduling mode HDL_PATH: rtlmpeg.schel.vbitmap.cfg0mem.ram.ram[$vqid]"
            
        def width(self):
            return 20
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x08000+$vqid"
            
        def startAddress(self):
            return 0x00008000
            
        def endAddress(self):
            return 0x000081ff

        class _Vcatshce_DWRR_QTum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Vcatshce_DWRR_QTum"
            
            def description(self):
                return "DWRR Quantum,Minximun MTU"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Vcatshce_DWRR_QTum"] = _AF6CCI0012_RD_MPEG._schevcatcfg1_pen._Vcatshce_DWRR_QTum()
            return allFields

    class _scheetheicfg0_pen(AtRegister.AtRegister):
        def name(self):
            return "Scheduler: Group priority Configuration Per Port"
    
        def description(self):
            return "Used to configure the priority queue per Link. - Support 3 group: Low Priority(LSP), High Priority(HSP), DWRR, mix mode. - HSP: Higest Priority - LSP: Lowest Priority - DWRR: Medium Priority - 0-3:reserved - 4:eXAUI#0 - 5:eXAUI#1 HDL_PATH: rtlmpeg.schel.ebitmap.cfg1mem.ram.ram[$eid]"
            
        def width(self):
            return 6
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x16000 + $eid"
            
        def startAddress(self):
            return 0x00016000
            
        def endAddress(self):
            return 0x0001600f

        class _Hsp_grp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Hsp_grp"
            
            def description(self):
                return "HSP Queue"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _DWRR_grp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "DWRR_grp"
            
            def description(self):
                return "DWRR Queue"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Lsp_grp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Lsp_grp"
            
            def description(self):
                return "LSP Queue"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hsp_grp"] = _AF6CCI0012_RD_MPEG._scheetheicfg0_pen._Hsp_grp()
            allFields["DWRR_grp"] = _AF6CCI0012_RD_MPEG._scheetheicfg0_pen._DWRR_grp()
            allFields["Lsp_grp"] = _AF6CCI0012_RD_MPEG._scheetheicfg0_pen._Lsp_grp()
            return allFields

    class _scheethercfg1_pen(AtRegister.AtRegister):
        def name(self):
            return "DWRR Quantum Configuration"
    
        def description(self):
            return "Used to configure quantum for per Ethernet Queue in DWRR scheduling mode HDL_PATH: rtlmpeg.schel.ebitmap.cfg0mem.ram.ram[$eqid]"
            
        def width(self):
            return 20
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x14000+$eqid"
            
        def startAddress(self):
            return 0x00014000
            
        def endAddress(self):
            return 0x0001400f

        class _Ethershce_DWRR_QTum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Ethershce_DWRR_QTum"
            
            def description(self):
                return "DWRR Quantum,Minximun MTU"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Ethershce_DWRR_QTum"] = _AF6CCI0012_RD_MPEG._scheethercfg1_pen._Ethershce_DWRR_QTum()
            return allFields

    class _stk0_pen(AtRegister.AtRegister):
        def name(self):
            return "Stick 0"
    
        def description(self):
            return "Used to debug MPEG"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00040010
            
        def endAddress(self):
            return 0xffffffff

        class _MPEG_Ready(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "MPEG_Ready"
            
            def description(self):
                return "MPEG Ready for PDA Reading"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _CLA_Write(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "CLA_Write"
            
            def description(self):
                return "CLA Enqueue MPEG"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _PDA_Request(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "PDA_Request"
            
            def description(self):
                return "PDA request Read"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MPEG_ACK(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "MPEG_ACK"
            
            def description(self):
                return "MPEG return ACK to PDA"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _External_Sta_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "External_Sta_err"
            
            def description(self):
                return "External Read/Write Status Fail"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _External_Write_Blk_Empty(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "External_Write_Blk_Empty"
            
            def description(self):
                return "External Write but Block Empty"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _External_Write_Blk_Dis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "External_Write_Blk_Dis"
            
            def description(self):
                return "External Write but Block Disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _External_Write_Buffer_Full(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "External_Write_Buffer_Full"
            
            def description(self):
                return "External Write DDR valid return but buffer full"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _External_Read_Pending_Full(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "External_Read_Pending_Full"
            
            def description(self):
                return "External Read Pending Full"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _External_Write_Pending_Full(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "External_Write_Pending_Full"
            
            def description(self):
                return "External Write Pending Full"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _External_Blk_Empty(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "External_Blk_Empty"
            
            def description(self):
                return "External Block Empty"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _External_Blk_Error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "External_Blk_Error"
            
            def description(self):
                return "External Block Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _External_Blk_Same(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "External_Blk_Same"
            
            def description(self):
                return "External Block Same"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Discard_Queue_Disable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "Discard_Queue_Disable"
            
            def description(self):
                return "Discard because Queue CFG Disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Discard_CLA_Drop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "Discard_CLA_Drop"
            
            def description(self):
                return "Discard because CLA declare Drop"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Discard_Internal_FiFO(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "Discard_Internal_FiFO"
            
            def description(self):
                return "Discard because internal FiFO full"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Discard_External_FiFO(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Discard_External_FiFO"
            
            def description(self):
                return "Discard because external FiFO full"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _External_FiFo_Engine_conflict(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "External_FiFo_Engine_conflict"
            
            def description(self):
                return "External FiFo Engine Conflict"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Internal_FiFO_fail(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "Internal_FiFO_fail"
            
            def description(self):
                return "Internal FiFo Fail"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Buffer_Write_Full_Thres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "Buffer_Write_Full_Thres"
            
            def description(self):
                return "Buffer Write Full Threshold CFG"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Buffer_Write_cache_full(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Buffer_Write_cache_full"
            
            def description(self):
                return "Buffer Write Cache Full"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Internal_FiFo_Engine_conflict(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "Internal_FiFo_Engine_conflict"
            
            def description(self):
                return "Internal FiFo Engine Conflict"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Buffer_External_Full(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Buffer_External_Full"
            
            def description(self):
                return "External Buffer Full"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Internal_FiFO_write_Error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Internal_FiFO_write_Error"
            
            def description(self):
                return "Internal FiFo write Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Internal_FiFo_full(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "Internal_FiFo_full"
            
            def description(self):
                return "Internal FiFo full"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Internal_FiFO_write_error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Internal_FiFO_write_error"
            
            def description(self):
                return "Internal FiFo write error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Enqueue_Full(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Enqueue_Full"
            
            def description(self):
                return "Enqueue Full"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Internal_FiFo_read_error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Internal_FiFo_read_error"
            
            def description(self):
                return "Internal_FiFo_read_error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MPEG_Ready"] = _AF6CCI0012_RD_MPEG._stk0_pen._MPEG_Ready()
            allFields["CLA_Write"] = _AF6CCI0012_RD_MPEG._stk0_pen._CLA_Write()
            allFields["PDA_Request"] = _AF6CCI0012_RD_MPEG._stk0_pen._PDA_Request()
            allFields["MPEG_ACK"] = _AF6CCI0012_RD_MPEG._stk0_pen._MPEG_ACK()
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk0_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk0_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk0_pen._Reserved()
            allFields["External_Sta_err"] = _AF6CCI0012_RD_MPEG._stk0_pen._External_Sta_err()
            allFields["External_Write_Blk_Empty"] = _AF6CCI0012_RD_MPEG._stk0_pen._External_Write_Blk_Empty()
            allFields["External_Write_Blk_Dis"] = _AF6CCI0012_RD_MPEG._stk0_pen._External_Write_Blk_Dis()
            allFields["External_Write_Buffer_Full"] = _AF6CCI0012_RD_MPEG._stk0_pen._External_Write_Buffer_Full()
            allFields["External_Read_Pending_Full"] = _AF6CCI0012_RD_MPEG._stk0_pen._External_Read_Pending_Full()
            allFields["External_Write_Pending_Full"] = _AF6CCI0012_RD_MPEG._stk0_pen._External_Write_Pending_Full()
            allFields["External_Blk_Empty"] = _AF6CCI0012_RD_MPEG._stk0_pen._External_Blk_Empty()
            allFields["External_Blk_Error"] = _AF6CCI0012_RD_MPEG._stk0_pen._External_Blk_Error()
            allFields["External_Blk_Same"] = _AF6CCI0012_RD_MPEG._stk0_pen._External_Blk_Same()
            allFields["Discard_Queue_Disable"] = _AF6CCI0012_RD_MPEG._stk0_pen._Discard_Queue_Disable()
            allFields["Discard_CLA_Drop"] = _AF6CCI0012_RD_MPEG._stk0_pen._Discard_CLA_Drop()
            allFields["Discard_Internal_FiFO"] = _AF6CCI0012_RD_MPEG._stk0_pen._Discard_Internal_FiFO()
            allFields["Discard_External_FiFO"] = _AF6CCI0012_RD_MPEG._stk0_pen._Discard_External_FiFO()
            allFields["External_FiFo_Engine_conflict"] = _AF6CCI0012_RD_MPEG._stk0_pen._External_FiFo_Engine_conflict()
            allFields["Internal_FiFO_fail"] = _AF6CCI0012_RD_MPEG._stk0_pen._Internal_FiFO_fail()
            allFields["Buffer_Write_Full_Thres"] = _AF6CCI0012_RD_MPEG._stk0_pen._Buffer_Write_Full_Thres()
            allFields["Buffer_Write_cache_full"] = _AF6CCI0012_RD_MPEG._stk0_pen._Buffer_Write_cache_full()
            allFields["Internal_FiFo_Engine_conflict"] = _AF6CCI0012_RD_MPEG._stk0_pen._Internal_FiFo_Engine_conflict()
            allFields["Buffer_External_Full"] = _AF6CCI0012_RD_MPEG._stk0_pen._Buffer_External_Full()
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk0_pen._Reserved()
            allFields["Internal_FiFO_write_Error"] = _AF6CCI0012_RD_MPEG._stk0_pen._Internal_FiFO_write_Error()
            allFields["Internal_FiFo_full"] = _AF6CCI0012_RD_MPEG._stk0_pen._Internal_FiFo_full()
            allFields["Internal_FiFO_write_error"] = _AF6CCI0012_RD_MPEG._stk0_pen._Internal_FiFO_write_error()
            allFields["Enqueue_Full"] = _AF6CCI0012_RD_MPEG._stk0_pen._Enqueue_Full()
            allFields["Internal_FiFo_read_error"] = _AF6CCI0012_RD_MPEG._stk0_pen._Internal_FiFo_read_error()
            return allFields

    class _stk1_pen(AtRegister.AtRegister):
        def name(self):
            return "Stick 1"
    
        def description(self):
            return "Used to debug MPEG"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00040011
            
        def endAddress(self):
            return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MPEG_Valid_to_PDA(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "MPEG_Valid_to_PDA"
            
            def description(self):
                return "MPEG return Valid to PDA"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FiFo_Req_DDR_Write_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "FiFo_Req_DDR_Write_err"
            
            def description(self):
                return "FiFo Request ACK DDR Write Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FiFo_Req_DDR_Read_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "FiFo_Req_DDR_Read_err"
            
            def description(self):
                return "FiFo Request ACK DDR Read Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _PDA_Discard_Full(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "PDA_Discard_Full"
            
            def description(self):
                return "PDA discard Full"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FiFo_Req_CLA_Write_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "FiFo_Req_CLA_Write_err"
            
            def description(self):
                return "FiFo Request ACK CLA Write Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FiFo_Req_CLA_Read_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "FiFo_Req_CLA_Read_err"
            
            def description(self):
                return "FiFo Request ACK CLA Read Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FiFo_Valid_DDR_Write_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "FiFo_Valid_DDR_Write_err"
            
            def description(self):
                return "FiFo Valid DDR Write Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FiFo_Valid_DDR_Read_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "FiFo_Valid_DDR_Read_err"
            
            def description(self):
                return "FiFo Valid DDR Read Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _QMXFF_request_write_DDR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "QMXFF_request_write_DDR"
            
            def description(self):
                return "QMXFF request write DDR"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _QMXFF_request_Read_DDR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "QMXFF_request_Read_DDR"
            
            def description(self):
                return "QMXFF request Read DDR"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _QMXFF_Receive_DDR_ACK(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "QMXFF_Receive_DDR_ACK"
            
            def description(self):
                return "QMXFF receive DDR ACK"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _QMXFF_Receive_DDR_Valid_Write(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "QMXFF_Receive_DDR_Valid_Write"
            
            def description(self):
                return "QMXFF receive DDR Write Valid"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _QMXFF_Receive_DDR_Valid_Read(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "QMXFF_Receive_DDR_Valid_Read"
            
            def description(self):
                return "QMXFF receive DDR Read Valid"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Internal_FiFO_Engine_Error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Internal_FiFO_Engine_Error"
            
            def description(self):
                return "Internal FiFo Engine Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Internal_FiFo_Write_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "Internal_FiFo_Write_Err"
            
            def description(self):
                return "Internal FiFo Write Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Internal_FiFO_BLK_Empty(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Internal_FiFO_BLK_Empty"
            
            def description(self):
                return "Internal FiFo Block Empty"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Internal_FiFO_BLK_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Internal_FiFO_BLK_Err"
            
            def description(self):
                return "Internal FiFo Block Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Internal_FiFO_BLK_Same(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Internal_FiFO_BLK_Same"
            
            def description(self):
                return "Internal FiFo Block Same"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk1_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk1_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk1_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk1_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk1_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk1_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk1_pen._Reserved()
            allFields["MPEG_Valid_to_PDA"] = _AF6CCI0012_RD_MPEG._stk1_pen._MPEG_Valid_to_PDA()
            allFields["FiFo_Req_DDR_Write_err"] = _AF6CCI0012_RD_MPEG._stk1_pen._FiFo_Req_DDR_Write_err()
            allFields["FiFo_Req_DDR_Read_err"] = _AF6CCI0012_RD_MPEG._stk1_pen._FiFo_Req_DDR_Read_err()
            allFields["PDA_Discard_Full"] = _AF6CCI0012_RD_MPEG._stk1_pen._PDA_Discard_Full()
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk1_pen._Reserved()
            allFields["FiFo_Req_CLA_Write_err"] = _AF6CCI0012_RD_MPEG._stk1_pen._FiFo_Req_CLA_Write_err()
            allFields["FiFo_Req_CLA_Read_err"] = _AF6CCI0012_RD_MPEG._stk1_pen._FiFo_Req_CLA_Read_err()
            allFields["FiFo_Valid_DDR_Write_err"] = _AF6CCI0012_RD_MPEG._stk1_pen._FiFo_Valid_DDR_Write_err()
            allFields["FiFo_Valid_DDR_Read_err"] = _AF6CCI0012_RD_MPEG._stk1_pen._FiFo_Valid_DDR_Read_err()
            allFields["QMXFF_request_write_DDR"] = _AF6CCI0012_RD_MPEG._stk1_pen._QMXFF_request_write_DDR()
            allFields["QMXFF_request_Read_DDR"] = _AF6CCI0012_RD_MPEG._stk1_pen._QMXFF_request_Read_DDR()
            allFields["QMXFF_Receive_DDR_ACK"] = _AF6CCI0012_RD_MPEG._stk1_pen._QMXFF_Receive_DDR_ACK()
            allFields["QMXFF_Receive_DDR_Valid_Write"] = _AF6CCI0012_RD_MPEG._stk1_pen._QMXFF_Receive_DDR_Valid_Write()
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk1_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk1_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk1_pen._Reserved()
            allFields["QMXFF_Receive_DDR_Valid_Read"] = _AF6CCI0012_RD_MPEG._stk1_pen._QMXFF_Receive_DDR_Valid_Read()
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk1_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk1_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk1_pen._Reserved()
            allFields["Internal_FiFO_Engine_Error"] = _AF6CCI0012_RD_MPEG._stk1_pen._Internal_FiFO_Engine_Error()
            allFields["Internal_FiFo_Write_Err"] = _AF6CCI0012_RD_MPEG._stk1_pen._Internal_FiFo_Write_Err()
            allFields["Internal_FiFO_BLK_Empty"] = _AF6CCI0012_RD_MPEG._stk1_pen._Internal_FiFO_BLK_Empty()
            allFields["Internal_FiFO_BLK_Err"] = _AF6CCI0012_RD_MPEG._stk1_pen._Internal_FiFO_BLK_Err()
            allFields["Internal_FiFO_BLK_Same"] = _AF6CCI0012_RD_MPEG._stk1_pen._Internal_FiFO_BLK_Same()
            return allFields

    class _stk2_pen(AtRegister.AtRegister):
        def name(self):
            return "Stick 2"
    
        def description(self):
            return "Used to debug MPEG"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00040012
            
        def endAddress(self):
            return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Link_Sche_CFG1_parity_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "Link_Sche_CFG1_parity_Err"
            
            def description(self):
                return "Link Scheduler CFG1 Parity Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Link_Sche_CFG0_parity_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "Link_Sche_CFG0_parity_Err"
            
            def description(self):
                return "Link Scheduler CFG0 Parity Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Link_Sche_STA1_parity_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "Link_Sche_STA1_parity_Err"
            
            def description(self):
                return "Link Scheduler STA1  Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Link_Sche_STA0_parity_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "Link_Sche_STA0_parity_Err"
            
            def description(self):
                return "Link Scheduler STA0  Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Bundle_Sche_CFG1_parity_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "Bundle_Sche_CFG1_parity_Err"
            
            def description(self):
                return "Bundle Scheduler CFG1 Parity Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Bundle_Sche_CFG0_parity_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "Bundle_Sche_CFG0_parity_Err"
            
            def description(self):
                return "Bundle Scheduler CFG0 Parity Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Bundle_Sche_STA1_parity_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "Bundle_Sche_STA1_parity_Err"
            
            def description(self):
                return "Bundle Scheduler STA1  Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Bundle_Sche_STA0_parity_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Bundle_Sche_STA0_parity_Err"
            
            def description(self):
                return "Bundle Scheduler STA0  Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _EtherBypass_Sche_CFG1_parity_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "EtherBypass_Sche_CFG1_parity_Err"
            
            def description(self):
                return "EtherBypass Scheduler CFG1 Parity Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _EtherBypass_Sche_CFG0_parity_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "EtherBypass_Sche_CFG0_parity_Err"
            
            def description(self):
                return "EtherBypass Scheduler CFG0 Parity Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _EtherBypass_Sche_STA1_parity_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "EtherBypass_Sche_STA1_parity_Err"
            
            def description(self):
                return "EtherBypass Scheduler STA1  Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _EtherBypass_Sche_STA0_parity_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "EtherBypass_Sche_STA0_parity_Err"
            
            def description(self):
                return "EtherBypass Scheduler STA0  Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _VCAT_Sche_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "VCAT_Sche_Err"
            
            def description(self):
                return "VCAT Scheduler Err"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FiFO_PDA_Request_Write_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FiFO_PDA_Request_Write_Err"
            
            def description(self):
                return "FiFO PDA Request Write Err"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FiFO_PDA_Request_Read_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FiFO_PDA_Request_Read_Err"
            
            def description(self):
                return "FiFO PDA Request Read Err"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk2_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk2_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk2_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk2_pen._Reserved()
            allFields["Link_Sche_CFG1_parity_Err"] = _AF6CCI0012_RD_MPEG._stk2_pen._Link_Sche_CFG1_parity_Err()
            allFields["Link_Sche_CFG0_parity_Err"] = _AF6CCI0012_RD_MPEG._stk2_pen._Link_Sche_CFG0_parity_Err()
            allFields["Link_Sche_STA1_parity_Err"] = _AF6CCI0012_RD_MPEG._stk2_pen._Link_Sche_STA1_parity_Err()
            allFields["Link_Sche_STA0_parity_Err"] = _AF6CCI0012_RD_MPEG._stk2_pen._Link_Sche_STA0_parity_Err()
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk2_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk2_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk2_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk2_pen._Reserved()
            allFields["Bundle_Sche_CFG1_parity_Err"] = _AF6CCI0012_RD_MPEG._stk2_pen._Bundle_Sche_CFG1_parity_Err()
            allFields["Bundle_Sche_CFG0_parity_Err"] = _AF6CCI0012_RD_MPEG._stk2_pen._Bundle_Sche_CFG0_parity_Err()
            allFields["Bundle_Sche_STA1_parity_Err"] = _AF6CCI0012_RD_MPEG._stk2_pen._Bundle_Sche_STA1_parity_Err()
            allFields["Bundle_Sche_STA0_parity_Err"] = _AF6CCI0012_RD_MPEG._stk2_pen._Bundle_Sche_STA0_parity_Err()
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk2_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk2_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk2_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk2_pen._Reserved()
            allFields["EtherBypass_Sche_CFG1_parity_Err"] = _AF6CCI0012_RD_MPEG._stk2_pen._EtherBypass_Sche_CFG1_parity_Err()
            allFields["EtherBypass_Sche_CFG0_parity_Err"] = _AF6CCI0012_RD_MPEG._stk2_pen._EtherBypass_Sche_CFG0_parity_Err()
            allFields["EtherBypass_Sche_STA1_parity_Err"] = _AF6CCI0012_RD_MPEG._stk2_pen._EtherBypass_Sche_STA1_parity_Err()
            allFields["EtherBypass_Sche_STA0_parity_Err"] = _AF6CCI0012_RD_MPEG._stk2_pen._EtherBypass_Sche_STA0_parity_Err()
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk2_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk2_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk2_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk2_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk2_pen._Reserved()
            allFields["VCAT_Sche_Err"] = _AF6CCI0012_RD_MPEG._stk2_pen._VCAT_Sche_Err()
            allFields["FiFO_PDA_Request_Write_Err"] = _AF6CCI0012_RD_MPEG._stk2_pen._FiFO_PDA_Request_Write_Err()
            allFields["FiFO_PDA_Request_Read_Err"] = _AF6CCI0012_RD_MPEG._stk2_pen._FiFO_PDA_Request_Read_Err()
            return allFields

    class _stk3_pen(AtRegister.AtRegister):
        def name(self):
            return "Stick 3"
    
        def description(self):
            return "Used to debug MPEG"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00040013
            
        def endAddress(self):
            return 0xffffffff

        class _PDA_Request_Link_Buffer(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "PDA_Request_Link_Buffer"
            
            def description(self):
                return "PDA Request Link Buffer"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _PDA_Request_VCAT_Buffer(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "PDA_Request_VCAT_Buffer"
            
            def description(self):
                return "PDA Request VCAT Buffer"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _PDA_Request_GEBP_Buffer(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "PDA_Request_GEBP_Buffer"
            
            def description(self):
                return "PDA Request GE Bypass Buffer"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _PDA_Request_BUND_Buffer(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "PDA_Request_BUND_Buffer"
            
            def description(self):
                return "PDA Request Bundle Buffer"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MPEG_Return_Link_Valid_PDA(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "MPEG_Return_Link_Valid_PDA"
            
            def description(self):
                return "MPEG Return Link Valid to PDA"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MPEG_Return_VCAT_Valid_PDA(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "MPEG_Return_VCAT_Valid_PDA"
            
            def description(self):
                return "MPEG Return VCAT Valid to PDA"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MPEG_Return_GEBP_Valid_PDA(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "MPEG_Return_GEBP_Valid_PDA"
            
            def description(self):
                return "MPEG Return GE Bypass Valid to PDA"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MPEG_Return_BUND_Valid_PDA(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "MPEG_Return_BUND_Valid_PDA"
            
            def description(self):
                return "MPEG Return Bundle Valid to PDA"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MPEG_Return_Link_ACK_PDA(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "MPEG_Return_Link_ACK_PDA"
            
            def description(self):
                return "MPEG Return Link ACK to PDA"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MPEG_Return_VCAT_ACK_PDA(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "MPEG_Return_VCAT_ACK_PDA"
            
            def description(self):
                return "MPEG Return VCAT ACK to PDA"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MPEG_Return_GEBP_ACK_PDA(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "MPEG_Return_GEBP_ACK_PDA"
            
            def description(self):
                return "MPEG Return GE Bypass ACK to PDA"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MPEG_Return_BUND_ACK_PDA(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "MPEG_Return_BUND_ACK_PDA"
            
            def description(self):
                return "MPEG Return Bundle ACK to PDA"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _CLA_Bund_enqueue(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "CLA_Bund_enqueue"
            
            def description(self):
                return "CLA Bundle Enqueue"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _CLA_Vcat_enqueue(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "CLA_Vcat_enqueue"
            
            def description(self):
                return "CLA VCAT Enqueue"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _CLA_Link_enqueue(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "CLA_Link_enqueue"
            
            def description(self):
                return "CLA Link Enqueue"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _CLA_GEBP_enqueue(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "CLA_GEBP_enqueue"
            
            def description(self):
                return "CLA GE bypass Enqueue"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _CLA_Discard(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "CLA_Discard"
            
            def description(self):
                return "CLA Discard"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _CLA_CES(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "CLA_CES"
            
            def description(self):
                return "CLA CES"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _CLA_EOP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "CLA_EOP"
            
            def description(self):
                return "CLA EOP"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _CLA_CEP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "CLA_CEP"
            
            def description(self):
                return "CLA CEP"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _CLA_HDLC(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "CLA_HDLC"
            
            def description(self):
                return "CLA HDLC"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _CLA_GEBP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "CLA_GEBP"
            
            def description(self):
                return "CLA GEBP"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _CLA_CHDLC(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "CLA_CHDLC"
            
            def description(self):
                return "CLA CHDLC"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _CLA_PPPOAM(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "CLA_PPPOAM"
            
            def description(self):
                return "CLA PPPOAM"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _CLA_PPPDAT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "CLA_PPPDAT"
            
            def description(self):
                return "CLA PPPDAT"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _CLA_MLPPDAT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "CLA_MLPPDAT"
            
            def description(self):
                return "CLA MLPPDAT"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _CLA_MLPPOAM(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "CLA_MLPPOAM"
            
            def description(self):
                return "CLA MLPPOAM"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _CLA_FROAM(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "CLA_FROAM"
            
            def description(self):
                return "CLA FROAM"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _CLA_FRDAT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "CLA_FRDAT"
            
            def description(self):
                return "CLA FRDAT"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _CLA_MLFROAM(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "CLA_MLFROAM"
            
            def description(self):
                return "CLA MLFROAM"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _CLA_MLFRDAT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLA_MLFRDAT"
            
            def description(self):
                return "CLA MLFRDAT"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDA_Request_Link_Buffer"] = _AF6CCI0012_RD_MPEG._stk3_pen._PDA_Request_Link_Buffer()
            allFields["PDA_Request_VCAT_Buffer"] = _AF6CCI0012_RD_MPEG._stk3_pen._PDA_Request_VCAT_Buffer()
            allFields["PDA_Request_GEBP_Buffer"] = _AF6CCI0012_RD_MPEG._stk3_pen._PDA_Request_GEBP_Buffer()
            allFields["PDA_Request_BUND_Buffer"] = _AF6CCI0012_RD_MPEG._stk3_pen._PDA_Request_BUND_Buffer()
            allFields["MPEG_Return_Link_Valid_PDA"] = _AF6CCI0012_RD_MPEG._stk3_pen._MPEG_Return_Link_Valid_PDA()
            allFields["MPEG_Return_VCAT_Valid_PDA"] = _AF6CCI0012_RD_MPEG._stk3_pen._MPEG_Return_VCAT_Valid_PDA()
            allFields["MPEG_Return_GEBP_Valid_PDA"] = _AF6CCI0012_RD_MPEG._stk3_pen._MPEG_Return_GEBP_Valid_PDA()
            allFields["MPEG_Return_BUND_Valid_PDA"] = _AF6CCI0012_RD_MPEG._stk3_pen._MPEG_Return_BUND_Valid_PDA()
            allFields["MPEG_Return_Link_ACK_PDA"] = _AF6CCI0012_RD_MPEG._stk3_pen._MPEG_Return_Link_ACK_PDA()
            allFields["MPEG_Return_VCAT_ACK_PDA"] = _AF6CCI0012_RD_MPEG._stk3_pen._MPEG_Return_VCAT_ACK_PDA()
            allFields["MPEG_Return_GEBP_ACK_PDA"] = _AF6CCI0012_RD_MPEG._stk3_pen._MPEG_Return_GEBP_ACK_PDA()
            allFields["MPEG_Return_BUND_ACK_PDA"] = _AF6CCI0012_RD_MPEG._stk3_pen._MPEG_Return_BUND_ACK_PDA()
            allFields["CLA_Bund_enqueue"] = _AF6CCI0012_RD_MPEG._stk3_pen._CLA_Bund_enqueue()
            allFields["CLA_Vcat_enqueue"] = _AF6CCI0012_RD_MPEG._stk3_pen._CLA_Vcat_enqueue()
            allFields["CLA_Link_enqueue"] = _AF6CCI0012_RD_MPEG._stk3_pen._CLA_Link_enqueue()
            allFields["CLA_GEBP_enqueue"] = _AF6CCI0012_RD_MPEG._stk3_pen._CLA_GEBP_enqueue()
            allFields["Reserved"] = _AF6CCI0012_RD_MPEG._stk3_pen._Reserved()
            allFields["CLA_Discard"] = _AF6CCI0012_RD_MPEG._stk3_pen._CLA_Discard()
            allFields["CLA_CES"] = _AF6CCI0012_RD_MPEG._stk3_pen._CLA_CES()
            allFields["CLA_EOP"] = _AF6CCI0012_RD_MPEG._stk3_pen._CLA_EOP()
            allFields["CLA_CEP"] = _AF6CCI0012_RD_MPEG._stk3_pen._CLA_CEP()
            allFields["CLA_HDLC"] = _AF6CCI0012_RD_MPEG._stk3_pen._CLA_HDLC()
            allFields["CLA_GEBP"] = _AF6CCI0012_RD_MPEG._stk3_pen._CLA_GEBP()
            allFields["CLA_CHDLC"] = _AF6CCI0012_RD_MPEG._stk3_pen._CLA_CHDLC()
            allFields["CLA_PPPOAM"] = _AF6CCI0012_RD_MPEG._stk3_pen._CLA_PPPOAM()
            allFields["CLA_PPPDAT"] = _AF6CCI0012_RD_MPEG._stk3_pen._CLA_PPPDAT()
            allFields["CLA_MLPPDAT"] = _AF6CCI0012_RD_MPEG._stk3_pen._CLA_MLPPDAT()
            allFields["CLA_MLPPOAM"] = _AF6CCI0012_RD_MPEG._stk3_pen._CLA_MLPPOAM()
            allFields["CLA_FROAM"] = _AF6CCI0012_RD_MPEG._stk3_pen._CLA_FROAM()
            allFields["CLA_FRDAT"] = _AF6CCI0012_RD_MPEG._stk3_pen._CLA_FRDAT()
            allFields["CLA_MLFROAM"] = _AF6CCI0012_RD_MPEG._stk3_pen._CLA_MLFROAM()
            allFields["CLA_MLFRDAT"] = _AF6CCI0012_RD_MPEG._stk3_pen._CLA_MLFRDAT()
            return allFields

    class _glbcnt_debug(AtRegister.AtRegister):
        def name(self):
            return "Global Debug Counter"
    
        def description(self):
            return "Support the following Queue Counter: - cntid = 00 : Total good packet enqueue - cntid = 01 : CLA Declare Drop - cntid = 02 : MPEG enable for PDA read - cntid = 03 : PDA request read MPEG - cntid = 04 : MPEG return valid to PDA - cntid = 05 : MPEG packet valid to PDA - cntid = 06 : PDA discard full - cntid = 07 : QMXFF request Write DDR - cntid = 08 : QMXFF request Read DDR - cntid = 09 : DDR return ACK to MPEG - cntid = 10 : DDR return Valid Write to MPEG - cntid = 11 : DDR return Valid Read to MPEG - cntid = 12 : Reserved - cntid = 13 : Reserved - cntid = 14 : Reserved - cntid = 15 : Reserved"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x40030+$cntid"
            
        def startAddress(self):
            return 0x00040030
            
        def endAddress(self):
            return 0x0004003f

        class _glbcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "glbcnt"
            
            def description(self):
                return "Value of counter"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["glbcnt"] = _AF6CCI0012_RD_MPEG._glbcnt_debug._glbcnt()
            return allFields

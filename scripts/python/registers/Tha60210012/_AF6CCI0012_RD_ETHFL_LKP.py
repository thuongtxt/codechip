import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0012_RD_ETHFL_LKP(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["hold_pen0"] = _AF6CCI0012_RD_ETHFL_LKP._hold_pen0()
        allRegisters["hold_pen1"] = _AF6CCI0012_RD_ETHFL_LKP._hold_pen1()
        allRegisters["hold_pen2"] = _AF6CCI0012_RD_ETHFL_LKP._hold_pen2()
        allRegisters["lolink_lup_table"] = _AF6CCI0012_RD_ETHFL_LKP._lolink_lup_table()
        allRegisters["holink_lup_table"] = _AF6CCI0012_RD_ETHFL_LKP._holink_lup_table()
        allRegisters["bundle_lup_table"] = _AF6CCI0012_RD_ETHFL_LKP._bundle_lup_table()
        allRegisters["linkdat_ethflow_table"] = _AF6CCI0012_RD_ETHFL_LKP._linkdat_ethflow_table()
        allRegisters["bundledat_ethflow_table"] = _AF6CCI0012_RD_ETHFL_LKP._bundledat_ethflow_table()
        allRegisters["value_hash_option_table"] = _AF6CCI0012_RD_ETHFL_LKP._value_hash_option_table()
        allRegisters["fr_hash_table0"] = _AF6CCI0012_RD_ETHFL_LKP._fr_hash_table0()
        allRegisters["fr_hash_table1"] = _AF6CCI0012_RD_ETHFL_LKP._fr_hash_table1()
        allRegisters["fr_hash_table2"] = _AF6CCI0012_RD_ETHFL_LKP._fr_hash_table2()
        allRegisters["fr_hash_table3"] = _AF6CCI0012_RD_ETHFL_LKP._fr_hash_table3()
        allRegisters["link_stk"] = _AF6CCI0012_RD_ETHFL_LKP._link_stk()
        allRegisters["upen_params"] = _AF6CCI0012_RD_ETHFL_LKP._upen_params()
        allRegisters["upen_nlp_profiles"] = _AF6CCI0012_RD_ETHFL_LKP._upen_nlp_profiles()
        allRegisters["upen_oam_profiles"] = _AF6CCI0012_RD_ETHFL_LKP._upen_oam_profiles()
        allRegisters["upen_err_profiles"] = _AF6CCI0012_RD_ETHFL_LKP._upen_err_profiles()
        allRegisters["upen_bcp_profiles"] = _AF6CCI0012_RD_ETHFL_LKP._upen_bcp_profiles()
        allRegisters["upen_fcn_profiles"] = _AF6CCI0012_RD_ETHFL_LKP._upen_fcn_profiles()
        allRegisters["upen_link_id"] = _AF6CCI0012_RD_ETHFL_LKP._upen_link_id()
        allRegisters["upen_pkdump_ctl"] = _AF6CCI0012_RD_ETHFL_LKP._upen_pkdump_ctl()
        allRegisters["upen_pkdump_sta"] = _AF6CCI0012_RD_ETHFL_LKP._upen_pkdump_sta()
        allRegisters["upen_pkdump_cid"] = _AF6CCI0012_RD_ETHFL_LKP._upen_pkdump_cid()
        allRegisters["upen_pkdump_pkc"] = _AF6CCI0012_RD_ETHFL_LKP._upen_pkdump_pkc()
        allRegisters["upen_pkdump_pks"] = _AF6CCI0012_RD_ETHFL_LKP._upen_pkdump_pks()
        allRegisters["upen_pkdump_pkd"] = _AF6CCI0012_RD_ETHFL_LKP._upen_pkdump_pkd()
        return allRegisters

    class _hold_pen0(AtRegister.AtRegister):
        def name(self):
            return "Access Data Holding0"
    
        def description(self):
            return "Data Hold for configuration or status that is greater than 32-bit wide."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000000f0
            
        def endAddress(self):
            return 0xffffffff

        class _Dat_Hold0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Dat_Hold0"
            
            def description(self):
                return "data [63:32]"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Dat_Hold0"] = _AF6CCI0012_RD_ETHFL_LKP._hold_pen0._Dat_Hold0()
            return allFields

    class _hold_pen1(AtRegister.AtRegister):
        def name(self):
            return "Access Data Holding1"
    
        def description(self):
            return "Data Hold for configuration or status that is greater than 32-bit wide."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000000f1
            
        def endAddress(self):
            return 0xffffffff

        class _Dat_Hold1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Dat_Hold1"
            
            def description(self):
                return "data [95:64]"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Dat_Hold1"] = _AF6CCI0012_RD_ETHFL_LKP._hold_pen1._Dat_Hold1()
            return allFields

    class _hold_pen2(AtRegister.AtRegister):
        def name(self):
            return "Access Data Holding2"
    
        def description(self):
            return "Data Hold for configuration or status that is greater than 32-bit wide."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000000f2
            
        def endAddress(self):
            return 0xffffffff

        class _Dat_Hold2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Dat_Hold2"
            
            def description(self):
                return "data [127:64]"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Dat_Hold2"] = _AF6CCI0012_RD_ETHFL_LKP._hold_pen2._Dat_Hold2()
            return allFields

    class _lolink_lup_table(AtRegister.AtRegister):
        def name(self):
            return "Lo-order LinkLookUp"
    
        def description(self):
            return "LookUp table: {dec_slice_id[1:0],dec_channel_id[9:0]} ---> link_id[11:0] HDL_PATH     : icfglinklo.iram.iram.ram[$dec_slice_id*1024 + $dec_channel_id]"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x01000 + $dec_slice_id*1024 + $dec_channel_id"
            
        def startAddress(self):
            return 0x00001000
            
        def endAddress(self):
            return 0x00001fff

        class _Link_Valid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Link_Valid"
            
            def description(self):
                return "0: LookUp Fail"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Link_ID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Link_ID"
            
            def description(self):
                return "Link ID: 0-3071"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Link_Valid"] = _AF6CCI0012_RD_ETHFL_LKP._lolink_lup_table._Link_Valid()
            allFields["Link_ID"] = _AF6CCI0012_RD_ETHFL_LKP._lolink_lup_table._Link_ID()
            return allFields

    class _holink_lup_table(AtRegister.AtRegister):
        def name(self):
            return "Ho-order Link LookUp"
    
        def description(self):
            return "LookUp table: {dec_slice_id[0],dec_channel_id[5:0]} ---> link_id[11:0] HDL_PATH     : icfglinkho.iram.iram.ram[$dec_slice_id*64 + $dec_channel_id]"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x02000 + $dec_slice_id*64 + $dec_channel_id"
            
        def startAddress(self):
            return 0x00002000
            
        def endAddress(self):
            return 0x0000205f

        class _Link_Valid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Link_Valid"
            
            def description(self):
                return "0: LookUp Fail"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Link_ID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Link_ID"
            
            def description(self):
                return "Link ID: 0-3071"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Link_Valid"] = _AF6CCI0012_RD_ETHFL_LKP._holink_lup_table._Link_Valid()
            allFields["Link_ID"] = _AF6CCI0012_RD_ETHFL_LKP._holink_lup_table._Link_ID()
            return allFields

    class _bundle_lup_table(AtRegister.AtRegister):
        def name(self):
            return "Bundle LookUp"
    
        def description(self):
            return "LookUp table: link_id[11:0] -> bundle_id[8:0]"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x03000 + $link_id"
            
        def startAddress(self):
            return 0x00003000
            
        def endAddress(self):
            return 0x00003fff

        class _Member_Id(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Member_Id"
            
            def description(self):
                return "Link Member ID on bundle"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Bundle_Valid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Bundle_Valid"
            
            def description(self):
                return "0: LookUp Fail"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Bundle_ID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Bundle_ID"
            
            def description(self):
                return "Bundle 0-511"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Member_Id"] = _AF6CCI0012_RD_ETHFL_LKP._bundle_lup_table._Member_Id()
            allFields["Bundle_Valid"] = _AF6CCI0012_RD_ETHFL_LKP._bundle_lup_table._Bundle_Valid()
            allFields["Bundle_ID"] = _AF6CCI0012_RD_ETHFL_LKP._bundle_lup_table._Bundle_ID()
            return allFields

    class _linkdat_ethflow_table(AtRegister.AtRegister):
        def name(self):
            return "Link to Ethernet Flow LookUp Table"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x04000 + $link_id"
            
        def startAddress(self):
            return 0x00004000
            
        def endAddress(self):
            return 0x00004bff

        class _CtrlVlan_Enable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "CtrlVlan_Enable"
            
            def description(self):
                return "0: LookUp Fail"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _CtrlVlan_ID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 16
        
            def name(self):
                return "CtrlVlan_ID"
            
            def description(self):
                return "VLAN Control: 0-4095"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _EthFlow_Enable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "EthFlow_Enable"
            
            def description(self):
                return "0: LookUp Fail"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _EthFlow_EthFlowID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "EthFlow_EthFlowID"
            
            def description(self):
                return "Ethernet FLow: 0-4095"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CtrlVlan_Enable"] = _AF6CCI0012_RD_ETHFL_LKP._linkdat_ethflow_table._CtrlVlan_Enable()
            allFields["CtrlVlan_ID"] = _AF6CCI0012_RD_ETHFL_LKP._linkdat_ethflow_table._CtrlVlan_ID()
            allFields["EthFlow_Enable"] = _AF6CCI0012_RD_ETHFL_LKP._linkdat_ethflow_table._EthFlow_Enable()
            allFields["EthFlow_EthFlowID"] = _AF6CCI0012_RD_ETHFL_LKP._linkdat_ethflow_table._EthFlow_EthFlowID()
            return allFields

    class _bundledat_ethflow_table(AtRegister.AtRegister):
        def name(self):
            return "Bundle to Ethernet Flow LookUp Table"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x06000 + $bundle_id"
            
        def startAddress(self):
            return 0x00006000
            
        def endAddress(self):
            return 0x000061ff

        class _CtrlVlan_Enable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "CtrlVlan_Enable"
            
            def description(self):
                return "0: LookUp Fail"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _CtrlVlan_ID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 16
        
            def name(self):
                return "CtrlVlan_ID"
            
            def description(self):
                return "VLAN Control: 0-4095"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _EthFlow_Enable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "EthFlow_Enable"
            
            def description(self):
                return "0: LookUp Fail"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _EthFlow_ID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "EthFlow_ID"
            
            def description(self):
                return "Ethernet FLow ID: 0-4095"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CtrlVlan_Enable"] = _AF6CCI0012_RD_ETHFL_LKP._bundledat_ethflow_table._CtrlVlan_Enable()
            allFields["CtrlVlan_ID"] = _AF6CCI0012_RD_ETHFL_LKP._bundledat_ethflow_table._CtrlVlan_ID()
            allFields["EthFlow_Enable"] = _AF6CCI0012_RD_ETHFL_LKP._bundledat_ethflow_table._EthFlow_Enable()
            allFields["EthFlow_ID"] = _AF6CCI0012_RD_ETHFL_LKP._bundledat_ethflow_table._EthFlow_ID()
            return allFields

    class _value_hash_option_table(AtRegister.AtRegister):
        def name(self):
            return "value hash option table"
    
        def description(self):
            return ""
            
        def width(self):
            return 128
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x05000 + $hash_id"
            
        def startAddress(self):
            return 0x00005000
            
        def endAddress(self):
            return 0x00005fff

        class _val_h_option_crc2_tb3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 112
        
            def name(self):
                return "val_h_option_crc2_tb3"
            
            def description(self):
                return "value hash option table3 for crc2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _val_h_option_crc1_tb3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 111
                
            def startBit(self):
                return 96
        
            def name(self):
                return "val_h_option_crc1_tb3"
            
            def description(self):
                return "value hash option table3 for crc1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _val_h_option_crc2_tb2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 95
                
            def startBit(self):
                return 80
        
            def name(self):
                return "val_h_option_crc2_tb2"
            
            def description(self):
                return "value hash option table2 for crc2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _val_h_option_crc1_tb2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 79
                
            def startBit(self):
                return 64
        
            def name(self):
                return "val_h_option_crc1_tb2"
            
            def description(self):
                return "value hash option table2 for crc1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _val_h_option_crc2_tb1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 48
        
            def name(self):
                return "val_h_option_crc2_tb1"
            
            def description(self):
                return "value hash option table1 for crc2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _val_h_option_crc1_tb1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 32
        
            def name(self):
                return "val_h_option_crc1_tb1"
            
            def description(self):
                return "value hash option table1 for crc1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _val_h_option_crc2_tb0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "val_h_option_crc2_tb0"
            
            def description(self):
                return "value hash option table0 for crc2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _val_h_option_crc1_tb0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "val_h_option_crc1_tb0"
            
            def description(self):
                return "value hash option table0 for crc1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["val_h_option_crc2_tb3"] = _AF6CCI0012_RD_ETHFL_LKP._value_hash_option_table._val_h_option_crc2_tb3()
            allFields["val_h_option_crc1_tb3"] = _AF6CCI0012_RD_ETHFL_LKP._value_hash_option_table._val_h_option_crc1_tb3()
            allFields["val_h_option_crc2_tb2"] = _AF6CCI0012_RD_ETHFL_LKP._value_hash_option_table._val_h_option_crc2_tb2()
            allFields["val_h_option_crc1_tb2"] = _AF6CCI0012_RD_ETHFL_LKP._value_hash_option_table._val_h_option_crc1_tb2()
            allFields["val_h_option_crc2_tb1"] = _AF6CCI0012_RD_ETHFL_LKP._value_hash_option_table._val_h_option_crc2_tb1()
            allFields["val_h_option_crc1_tb1"] = _AF6CCI0012_RD_ETHFL_LKP._value_hash_option_table._val_h_option_crc1_tb1()
            allFields["val_h_option_crc2_tb0"] = _AF6CCI0012_RD_ETHFL_LKP._value_hash_option_table._val_h_option_crc2_tb0()
            allFields["val_h_option_crc1_tb0"] = _AF6CCI0012_RD_ETHFL_LKP._value_hash_option_table._val_h_option_crc1_tb0()
            return allFields

    class _fr_hash_table0(AtRegister.AtRegister):
        def name(self):
            return "Frame Relay Hash Table #0"
    
        def description(self):
            return "Dual hash with: - CRC1: x^12+x^6+x^4+x^0 - CRC2: x^12+x^8+x^4+x^2+x^0 Input key  (37bits): key[00:00]: DC_mode key[23:01]: Q922_Add key[24:24]: Bund_Sel key[36:25]: Link_ID or Bundle_ID depend on Bund_Sel"
            
        def width(self):
            return 64
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x08000 + $hash_id"
            
        def startAddress(self):
            return 0x00008000
            
        def endAddress(self):
            return 0x00008fff

        class _Hash_Valid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 60
                
            def startBit(self):
                return 60
        
            def name(self):
                return "Hash_Valid"
            
            def description(self):
                return "0: LookUp Fail"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Link_Bundle_ID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 37
        
            def name(self):
                return "Link_Bundle_ID"
            
            def description(self):
                return "Unused"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Bundle_Sel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 36
                
            def startBit(self):
                return 36
        
            def name(self):
                return "Bundle_Sel"
            
            def description(self):
                return ""
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _DC_mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 35
                
            def startBit(self):
                return 35
        
            def name(self):
                return "DC_mode"
            
            def description(self):
                return "0: D/C = 0 , 1 : D/C = 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Q922_Add(AtRegister.AtRegisterField):
            def stopBit(self):
                return 34
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Q922_Add"
            
            def description(self):
                return "Frame Relay Q922 address format"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _EthFlowID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "EthFlowID"
            
            def description(self):
                return "Ethernet FLow: 0-4095"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hash_Valid"] = _AF6CCI0012_RD_ETHFL_LKP._fr_hash_table0._Hash_Valid()
            allFields["Link_Bundle_ID"] = _AF6CCI0012_RD_ETHFL_LKP._fr_hash_table0._Link_Bundle_ID()
            allFields["Bundle_Sel"] = _AF6CCI0012_RD_ETHFL_LKP._fr_hash_table0._Bundle_Sel()
            allFields["DC_mode"] = _AF6CCI0012_RD_ETHFL_LKP._fr_hash_table0._DC_mode()
            allFields["Q922_Add"] = _AF6CCI0012_RD_ETHFL_LKP._fr_hash_table0._Q922_Add()
            allFields["EthFlowID"] = _AF6CCI0012_RD_ETHFL_LKP._fr_hash_table0._EthFlowID()
            return allFields

    class _fr_hash_table1(AtRegister.AtRegister):
        def name(self):
            return "Frame Relay Hash Table #1"
    
        def description(self):
            return "Dual hash with: - CRC1: x^12+x^7+x^2+x^0 - CRC2: x^12+x^8+x^5+x^3+x^2+x^0 Input key  (37bits): key[00:00]: DC_mode key[23:01]: Q922_Add key[24:24]: Bund_Sel key[36:25]: Link_ID or Bundle_ID depend on Bund_Sel"
            
        def width(self):
            return 64
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x09000 + $hash_id"
            
        def startAddress(self):
            return 0x00009000
            
        def endAddress(self):
            return 0x00009fff

        class _Hash_Valid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 60
                
            def startBit(self):
                return 60
        
            def name(self):
                return "Hash_Valid"
            
            def description(self):
                return "0: LookUp Fail"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Link_Bundle_ID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 37
        
            def name(self):
                return "Link_Bundle_ID"
            
            def description(self):
                return "Unused"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Bundle_Sel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 36
                
            def startBit(self):
                return 36
        
            def name(self):
                return "Bundle_Sel"
            
            def description(self):
                return ""
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _DC_mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 35
                
            def startBit(self):
                return 35
        
            def name(self):
                return "DC_mode"
            
            def description(self):
                return "0: D/C = 0 , 1 : D/C = 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Q922_Add(AtRegister.AtRegisterField):
            def stopBit(self):
                return 34
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Q922_Add"
            
            def description(self):
                return "Frame Relay Q922 address format"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _EthFlowID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "EthFlowID"
            
            def description(self):
                return "Ethernet FLow: 0-4095"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hash_Valid"] = _AF6CCI0012_RD_ETHFL_LKP._fr_hash_table1._Hash_Valid()
            allFields["Link_Bundle_ID"] = _AF6CCI0012_RD_ETHFL_LKP._fr_hash_table1._Link_Bundle_ID()
            allFields["Bundle_Sel"] = _AF6CCI0012_RD_ETHFL_LKP._fr_hash_table1._Bundle_Sel()
            allFields["DC_mode"] = _AF6CCI0012_RD_ETHFL_LKP._fr_hash_table1._DC_mode()
            allFields["Q922_Add"] = _AF6CCI0012_RD_ETHFL_LKP._fr_hash_table1._Q922_Add()
            allFields["EthFlowID"] = _AF6CCI0012_RD_ETHFL_LKP._fr_hash_table1._EthFlowID()
            return allFields

    class _fr_hash_table2(AtRegister.AtRegister):
        def name(self):
            return "Frame Relay Hash Table #2"
    
        def description(self):
            return "Dual hash with: - CRC1: x^12+x^7+x^4+x^3+x^2+x^0 - CRC2: x^12+x^9+x^5+x^3+x^0 Input key  (37bits): key[00:00]: DC_mode key[23:01]: Q922_Add key[24:24]: Bund_Sel key[36:25]: Link_ID or Bundle_ID depend on Bund_Sel"
            
        def width(self):
            return 64
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0A000 + $hash_id"
            
        def startAddress(self):
            return 0x0000a000
            
        def endAddress(self):
            return 0x0000afff

        class _Hash_Valid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 60
                
            def startBit(self):
                return 60
        
            def name(self):
                return "Hash_Valid"
            
            def description(self):
                return "0: LookUp Fail"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Link_Bundle_ID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 37
        
            def name(self):
                return "Link_Bundle_ID"
            
            def description(self):
                return "Unused"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Bundle_Sel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 36
                
            def startBit(self):
                return 36
        
            def name(self):
                return "Bundle_Sel"
            
            def description(self):
                return ""
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _DC_mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 35
                
            def startBit(self):
                return 35
        
            def name(self):
                return "DC_mode"
            
            def description(self):
                return "0: D/C = 0 , 1 : D/C = 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Q922_Add(AtRegister.AtRegisterField):
            def stopBit(self):
                return 34
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Q922_Add"
            
            def description(self):
                return "Frame Relay Q922 address format"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _EthFlowID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "EthFlowID"
            
            def description(self):
                return "Ethernet FLow: 0-4095"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hash_Valid"] = _AF6CCI0012_RD_ETHFL_LKP._fr_hash_table2._Hash_Valid()
            allFields["Link_Bundle_ID"] = _AF6CCI0012_RD_ETHFL_LKP._fr_hash_table2._Link_Bundle_ID()
            allFields["Bundle_Sel"] = _AF6CCI0012_RD_ETHFL_LKP._fr_hash_table2._Bundle_Sel()
            allFields["DC_mode"] = _AF6CCI0012_RD_ETHFL_LKP._fr_hash_table2._DC_mode()
            allFields["Q922_Add"] = _AF6CCI0012_RD_ETHFL_LKP._fr_hash_table2._Q922_Add()
            allFields["EthFlowID"] = _AF6CCI0012_RD_ETHFL_LKP._fr_hash_table2._EthFlowID()
            return allFields

    class _fr_hash_table3(AtRegister.AtRegister):
        def name(self):
            return "Frame Relay Hash Table #3"
    
        def description(self):
            return "Dual hash with: - CRC1: x^12+x^7+x^5+x^2+x^0 - CRC2: x^12+x^9+x^6+x^4+x^1+x^0 Input key  (37bits): key[00:00]: DC_mode key[23:01]: Q922_Add key[24:24]: Bund_Sel key[36:25]: Link_ID or Bundle_ID depend on Bund_Sel"
            
        def width(self):
            return 64
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0B000 + $hash_id"
            
        def startAddress(self):
            return 0x0000b000
            
        def endAddress(self):
            return 0x0000bfff

        class _Hash_Valid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 60
                
            def startBit(self):
                return 60
        
            def name(self):
                return "Hash_Valid"
            
            def description(self):
                return "0: LookUp Fail"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Link_Bundle_ID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 37
        
            def name(self):
                return "Link_Bundle_ID"
            
            def description(self):
                return "Unused"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Bundle_Sel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 36
                
            def startBit(self):
                return 36
        
            def name(self):
                return "Bundle_Sel"
            
            def description(self):
                return ""
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _DC_mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 35
                
            def startBit(self):
                return 35
        
            def name(self):
                return "DC_mode"
            
            def description(self):
                return "0: D/C = 0 , 1 : D/C = 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Q922_Add(AtRegister.AtRegisterField):
            def stopBit(self):
                return 34
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Q922_Add"
            
            def description(self):
                return "Frame Relay Q922 address format"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _EthFlowID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "EthFlowID"
            
            def description(self):
                return "Ethernet FLow: 0-4095"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hash_Valid"] = _AF6CCI0012_RD_ETHFL_LKP._fr_hash_table3._Hash_Valid()
            allFields["Link_Bundle_ID"] = _AF6CCI0012_RD_ETHFL_LKP._fr_hash_table3._Link_Bundle_ID()
            allFields["Bundle_Sel"] = _AF6CCI0012_RD_ETHFL_LKP._fr_hash_table3._Bundle_Sel()
            allFields["DC_mode"] = _AF6CCI0012_RD_ETHFL_LKP._fr_hash_table3._DC_mode()
            allFields["Q922_Add"] = _AF6CCI0012_RD_ETHFL_LKP._fr_hash_table3._Q922_Add()
            allFields["EthFlowID"] = _AF6CCI0012_RD_ETHFL_LKP._fr_hash_table3._EthFlowID()
            return allFields

    class _link_stk(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Flow Look up Link Stiky"
    
        def description(self):
            return "Sticky for each link id , support both mode  read only and read to clear - Read only ,need write 1 to clear sticky - Read to clear"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0E000 + $r2c*4096 + $link_id"
            
        def startAddress(self):
            return 0x0000e000
            
        def endAddress(self):
            return 0x0000fbff

        class _rule_outdrop_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "rule_outdrop_stk"
            
            def description(self):
                return "RULE_OUT_DROP sticky"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        class _lpsr_lkerr_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "lpsr_lkerr_stk"
            
            def description(self):
                return "LPSR_LK_ERR sticky"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        class _lpsr_macerr_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "lpsr_macerr_stk"
            
            def description(self):
                return "LPSR_MAC_ERR sticky"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        class _lpsr_mlerr_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "lpsr_mlerr_stk"
            
            def description(self):
                return "LPSR_MULTILINK_ERR sticky"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        class _lpsr_piderr_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "lpsr_piderr_stk"
            
            def description(self):
                return "LPSR_PROTOCOL_ERR sticky"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        class _lpsr_ctrlerr_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "lpsr_ctrlerr_stk"
            
            def description(self):
                return "LPSR_CONTROL_ERR sticky"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        class _lpsr_adrerr_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "lpsr_adrerr_stk"
            
            def description(self):
                return "LPSR_ADDRESS_ERR sticky"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        class _lpsr_abort_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "lpsr_abort_stk"
            
            def description(self):
                return "LPSR_ABORT sticky"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        class _lpsr_fcserr_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "lpsr_fcserr_stk"
            
            def description(self):
                return "LPSR_FCS_ERR sticky"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        class _lfwd_drop_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "lfwd_drop_stk"
            
            def description(self):
                return "LFWD_DROP sticky"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        class _lfwd_mldata_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "lfwd_mldata_stk"
            
            def description(self):
                return "LFWD_ML_DATA sticky"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        class _lfwd_mlctrl_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "lfwd_mlctrl_stk"
            
            def description(self):
                return "LFWD_ML_CONTROL sticky"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        class _lfwd_dpctrl_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "lfwd_dpctrl_stk"
            
            def description(self):
                return "LFWD_DP_CONTROL sticky"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        class _lfwd_corrupt_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "lfwd_corrupt_stk"
            
            def description(self):
                return "LFWD_CORRUPT sticky"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        class _lfwd_data_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "lfwd_data_stk"
            
            def description(self):
                return "LFWD_DATA sticky"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        class _lfwd_pw_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "lfwd_pw_stk"
            
            def description(self):
                return "LFWD_PW sticky"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        class _lfwd_ctrl_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lfwd_ctrl_stk"
            
            def description(self):
                return "LFWD_CONTROL sticky"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rule_outdrop_stk"] = _AF6CCI0012_RD_ETHFL_LKP._link_stk._rule_outdrop_stk()
            allFields["lpsr_lkerr_stk"] = _AF6CCI0012_RD_ETHFL_LKP._link_stk._lpsr_lkerr_stk()
            allFields["lpsr_macerr_stk"] = _AF6CCI0012_RD_ETHFL_LKP._link_stk._lpsr_macerr_stk()
            allFields["lpsr_mlerr_stk"] = _AF6CCI0012_RD_ETHFL_LKP._link_stk._lpsr_mlerr_stk()
            allFields["lpsr_piderr_stk"] = _AF6CCI0012_RD_ETHFL_LKP._link_stk._lpsr_piderr_stk()
            allFields["lpsr_ctrlerr_stk"] = _AF6CCI0012_RD_ETHFL_LKP._link_stk._lpsr_ctrlerr_stk()
            allFields["lpsr_adrerr_stk"] = _AF6CCI0012_RD_ETHFL_LKP._link_stk._lpsr_adrerr_stk()
            allFields["lpsr_abort_stk"] = _AF6CCI0012_RD_ETHFL_LKP._link_stk._lpsr_abort_stk()
            allFields["lpsr_fcserr_stk"] = _AF6CCI0012_RD_ETHFL_LKP._link_stk._lpsr_fcserr_stk()
            allFields["lfwd_drop_stk"] = _AF6CCI0012_RD_ETHFL_LKP._link_stk._lfwd_drop_stk()
            allFields["lfwd_mldata_stk"] = _AF6CCI0012_RD_ETHFL_LKP._link_stk._lfwd_mldata_stk()
            allFields["lfwd_mlctrl_stk"] = _AF6CCI0012_RD_ETHFL_LKP._link_stk._lfwd_mlctrl_stk()
            allFields["lfwd_dpctrl_stk"] = _AF6CCI0012_RD_ETHFL_LKP._link_stk._lfwd_dpctrl_stk()
            allFields["lfwd_corrupt_stk"] = _AF6CCI0012_RD_ETHFL_LKP._link_stk._lfwd_corrupt_stk()
            allFields["lfwd_data_stk"] = _AF6CCI0012_RD_ETHFL_LKP._link_stk._lfwd_data_stk()
            allFields["lfwd_pw_stk"] = _AF6CCI0012_RD_ETHFL_LKP._link_stk._lfwd_pw_stk()
            allFields["lfwd_ctrl_stk"] = _AF6CCI0012_RD_ETHFL_LKP._link_stk._lfwd_ctrl_stk()
            return allFields

    class _upen_params(AtRegister.AtRegister):
        def name(self):
            return "IgRule Params"
    
        def description(self):
            return "This register is used to get params of Ingress Rule Check Engine We have 5 type profile correspond for type = {FCN,BCP,ERR,OAM,NLP}. Number of profiles eache type is 2^type_bit. Exam NLP_bit = 5 -> NLP have 2^32 = 32 profiles."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000c000
            
        def endAddress(self):
            return 0xffffffff

        class _Eng_Id(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 20
        
            def name(self):
                return "Eng_Id"
            
            def description(self):
                return "Engine code"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _FCN_bit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "FCN_bit"
            
            def description(self):
                return "FCN_bit"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _BCP_bit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "BCP_bit"
            
            def description(self):
                return "BCP_bit"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _ERR_bit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "ERR_bit"
            
            def description(self):
                return "ERR_bit"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _OAM_bit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "OAM_bit"
            
            def description(self):
                return "OAM_bit"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _NLP_bit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "NLP_bit"
            
            def description(self):
                return "NLP_bit"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Eng_Id"] = _AF6CCI0012_RD_ETHFL_LKP._upen_params._Eng_Id()
            allFields["FCN_bit"] = _AF6CCI0012_RD_ETHFL_LKP._upen_params._FCN_bit()
            allFields["BCP_bit"] = _AF6CCI0012_RD_ETHFL_LKP._upen_params._BCP_bit()
            allFields["ERR_bit"] = _AF6CCI0012_RD_ETHFL_LKP._upen_params._ERR_bit()
            allFields["OAM_bit"] = _AF6CCI0012_RD_ETHFL_LKP._upen_params._OAM_bit()
            allFields["NLP_bit"] = _AF6CCI0012_RD_ETHFL_LKP._upen_params._NLP_bit()
            return allFields

    class _upen_nlp_profiles(AtRegister.AtRegister):
        def name(self):
            return "IgRule NLP Profiles"
    
        def description(self):
            return "This register is used to config profile forward (fwd) rule for each network layer protocol (NLP)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00_C100 + $nlp_profile"
            
        def startAddress(self):
            return 0x0000c100
            
        def endAddress(self):
            return 0xffffffff

        class _FwdRule_NLP_15(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 30
        
            def name(self):
                return "FwdRule_NLP_15"
            
            def description(self):
                return "FwdRule for NLP#15"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_NLP_14(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 28
        
            def name(self):
                return "FwdRule_NLP_14"
            
            def description(self):
                return "FwdRule for NLP#14"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_NLP_13(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 26
        
            def name(self):
                return "FwdRule_NLP_13"
            
            def description(self):
                return "FwdRule for NLP#13"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_NLP_12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 24
        
            def name(self):
                return "FwdRule_NLP_12"
            
            def description(self):
                return "FwdRule for NLP#12"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_NLP_11(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 22
        
            def name(self):
                return "FwdRule_NLP_11"
            
            def description(self):
                return "FwdRule for NLP#11"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_NLP_10(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 20
        
            def name(self):
                return "FwdRule_NLP_10"
            
            def description(self):
                return "FwdRule for NLP#10"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_NLP_09(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 18
        
            def name(self):
                return "FwdRule_NLP_09"
            
            def description(self):
                return "FwdRule for NLP#09"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_NLP_08(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 16
        
            def name(self):
                return "FwdRule_NLP_08"
            
            def description(self):
                return "FwdRule for NLP#08"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_NLP_07(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 14
        
            def name(self):
                return "FwdRule_NLP_07"
            
            def description(self):
                return "FwdRule for NLP#07"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_NLP_06(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "FwdRule_NLP_06"
            
            def description(self):
                return "FwdRule for NLP#06"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_NLP_05(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "FwdRule_NLP_05"
            
            def description(self):
                return "FwdRule for NLP#05"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_NLP_04(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "FwdRule_NLP_04"
            
            def description(self):
                return "FwdRule for NLP#04"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_NLP_03(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FwdRule_NLP_03"
            
            def description(self):
                return "FwdRule for NLP#03"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_NLP_02(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FwdRule_NLP_02"
            
            def description(self):
                return "FwdRule for NLP#02"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_NLP_01(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FwdRule_NLP_01"
            
            def description(self):
                return "FwdRule for NLP#01"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_NLP_00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FwdRule_NLP_00"
            
            def description(self):
                return "FwdRule for NLP#00"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FwdRule_NLP_15"] = _AF6CCI0012_RD_ETHFL_LKP._upen_nlp_profiles._FwdRule_NLP_15()
            allFields["FwdRule_NLP_14"] = _AF6CCI0012_RD_ETHFL_LKP._upen_nlp_profiles._FwdRule_NLP_14()
            allFields["FwdRule_NLP_13"] = _AF6CCI0012_RD_ETHFL_LKP._upen_nlp_profiles._FwdRule_NLP_13()
            allFields["FwdRule_NLP_12"] = _AF6CCI0012_RD_ETHFL_LKP._upen_nlp_profiles._FwdRule_NLP_12()
            allFields["FwdRule_NLP_11"] = _AF6CCI0012_RD_ETHFL_LKP._upen_nlp_profiles._FwdRule_NLP_11()
            allFields["FwdRule_NLP_10"] = _AF6CCI0012_RD_ETHFL_LKP._upen_nlp_profiles._FwdRule_NLP_10()
            allFields["FwdRule_NLP_09"] = _AF6CCI0012_RD_ETHFL_LKP._upen_nlp_profiles._FwdRule_NLP_09()
            allFields["FwdRule_NLP_08"] = _AF6CCI0012_RD_ETHFL_LKP._upen_nlp_profiles._FwdRule_NLP_08()
            allFields["FwdRule_NLP_07"] = _AF6CCI0012_RD_ETHFL_LKP._upen_nlp_profiles._FwdRule_NLP_07()
            allFields["FwdRule_NLP_06"] = _AF6CCI0012_RD_ETHFL_LKP._upen_nlp_profiles._FwdRule_NLP_06()
            allFields["FwdRule_NLP_05"] = _AF6CCI0012_RD_ETHFL_LKP._upen_nlp_profiles._FwdRule_NLP_05()
            allFields["FwdRule_NLP_04"] = _AF6CCI0012_RD_ETHFL_LKP._upen_nlp_profiles._FwdRule_NLP_04()
            allFields["FwdRule_NLP_03"] = _AF6CCI0012_RD_ETHFL_LKP._upen_nlp_profiles._FwdRule_NLP_03()
            allFields["FwdRule_NLP_02"] = _AF6CCI0012_RD_ETHFL_LKP._upen_nlp_profiles._FwdRule_NLP_02()
            allFields["FwdRule_NLP_01"] = _AF6CCI0012_RD_ETHFL_LKP._upen_nlp_profiles._FwdRule_NLP_01()
            allFields["FwdRule_NLP_00"] = _AF6CCI0012_RD_ETHFL_LKP._upen_nlp_profiles._FwdRule_NLP_00()
            return allFields

    class _upen_oam_profiles(AtRegister.AtRegister):
        def name(self):
            return "IgRule OAM Profiles"
    
        def description(self):
            return "This register is used to config profile forward (fwd) rule for OAM layer protocol (OAM)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00_C200 + $oam_profile"
            
        def startAddress(self):
            return 0x0000c200
            
        def endAddress(self):
            return 0xffffffff

        class _FwdRule_OAM_15(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 30
        
            def name(self):
                return "FwdRule_OAM_15"
            
            def description(self):
                return "FwdRule for OAM#15"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_OAM_14(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 28
        
            def name(self):
                return "FwdRule_OAM_14"
            
            def description(self):
                return "FwdRule for OAM#14"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_OAM_13(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 26
        
            def name(self):
                return "FwdRule_OAM_13"
            
            def description(self):
                return "FwdRule for OAM#13"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_OAM_12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 24
        
            def name(self):
                return "FwdRule_OAM_12"
            
            def description(self):
                return "FwdRule for OAM#12"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_OAM_11(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 22
        
            def name(self):
                return "FwdRule_OAM_11"
            
            def description(self):
                return "FwdRule for OAM#11"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_OAM_10(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 20
        
            def name(self):
                return "FwdRule_OAM_10"
            
            def description(self):
                return "FwdRule for OAM#10"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_OAM_09(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 18
        
            def name(self):
                return "FwdRule_OAM_09"
            
            def description(self):
                return "FwdRule for OAM#09"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_OAM_08(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 16
        
            def name(self):
                return "FwdRule_OAM_08"
            
            def description(self):
                return "FwdRule for OAM#08"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_OAM_07(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 14
        
            def name(self):
                return "FwdRule_OAM_07"
            
            def description(self):
                return "FwdRule for OAM#07"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_OAM_06(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "FwdRule_OAM_06"
            
            def description(self):
                return "FwdRule for OAM#06"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_OAM_05(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "FwdRule_OAM_05"
            
            def description(self):
                return "FwdRule for OAM#05"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_OAM_04(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "FwdRule_OAM_04"
            
            def description(self):
                return "FwdRule for OAM#04"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_OAM_03(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FwdRule_OAM_03"
            
            def description(self):
                return "FwdRule for OAM#03"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_OAM_02(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FwdRule_OAM_02"
            
            def description(self):
                return "FwdRule for OAM#02"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_OAM_01(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FwdRule_OAM_01"
            
            def description(self):
                return "FwdRule for OAM#01"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_OAM_00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FwdRule_OAM_00"
            
            def description(self):
                return "FwdRule for OAM#00"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FwdRule_OAM_15"] = _AF6CCI0012_RD_ETHFL_LKP._upen_oam_profiles._FwdRule_OAM_15()
            allFields["FwdRule_OAM_14"] = _AF6CCI0012_RD_ETHFL_LKP._upen_oam_profiles._FwdRule_OAM_14()
            allFields["FwdRule_OAM_13"] = _AF6CCI0012_RD_ETHFL_LKP._upen_oam_profiles._FwdRule_OAM_13()
            allFields["FwdRule_OAM_12"] = _AF6CCI0012_RD_ETHFL_LKP._upen_oam_profiles._FwdRule_OAM_12()
            allFields["FwdRule_OAM_11"] = _AF6CCI0012_RD_ETHFL_LKP._upen_oam_profiles._FwdRule_OAM_11()
            allFields["FwdRule_OAM_10"] = _AF6CCI0012_RD_ETHFL_LKP._upen_oam_profiles._FwdRule_OAM_10()
            allFields["FwdRule_OAM_09"] = _AF6CCI0012_RD_ETHFL_LKP._upen_oam_profiles._FwdRule_OAM_09()
            allFields["FwdRule_OAM_08"] = _AF6CCI0012_RD_ETHFL_LKP._upen_oam_profiles._FwdRule_OAM_08()
            allFields["FwdRule_OAM_07"] = _AF6CCI0012_RD_ETHFL_LKP._upen_oam_profiles._FwdRule_OAM_07()
            allFields["FwdRule_OAM_06"] = _AF6CCI0012_RD_ETHFL_LKP._upen_oam_profiles._FwdRule_OAM_06()
            allFields["FwdRule_OAM_05"] = _AF6CCI0012_RD_ETHFL_LKP._upen_oam_profiles._FwdRule_OAM_05()
            allFields["FwdRule_OAM_04"] = _AF6CCI0012_RD_ETHFL_LKP._upen_oam_profiles._FwdRule_OAM_04()
            allFields["FwdRule_OAM_03"] = _AF6CCI0012_RD_ETHFL_LKP._upen_oam_profiles._FwdRule_OAM_03()
            allFields["FwdRule_OAM_02"] = _AF6CCI0012_RD_ETHFL_LKP._upen_oam_profiles._FwdRule_OAM_02()
            allFields["FwdRule_OAM_01"] = _AF6CCI0012_RD_ETHFL_LKP._upen_oam_profiles._FwdRule_OAM_01()
            allFields["FwdRule_OAM_00"] = _AF6CCI0012_RD_ETHFL_LKP._upen_oam_profiles._FwdRule_OAM_00()
            return allFields

    class _upen_err_profiles(AtRegister.AtRegister):
        def name(self):
            return "IgRule ERR Profiles"
    
        def description(self):
            return "This register is used to config profile forward (fwd) rule for ERR status (ERR)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00_C300 + $err_profile"
            
        def startAddress(self):
            return 0x0000c300
            
        def endAddress(self):
            return 0xffffffff

        class _FwdRule_ERR_07(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 14
        
            def name(self):
                return "FwdRule_ERR_07"
            
            def description(self):
                return "FwdRule for ERR#07"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_ERR_06(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "FwdRule_ERR_06"
            
            def description(self):
                return "FwdRule for ERR#06"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_ERR_05(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "FwdRule_ERR_05"
            
            def description(self):
                return "FwdRule for ERR#05"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_ERR_04(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "FwdRule_ERR_04"
            
            def description(self):
                return "FwdRule for ERR#04"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_ERR_03(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FwdRule_ERR_03"
            
            def description(self):
                return "FwdRule for ERR#03"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_ERR_02(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FwdRule_ERR_02"
            
            def description(self):
                return "FwdRule for ERR#02"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_ERR_01(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FwdRule_ERR_01"
            
            def description(self):
                return "FwdRule for ERR#01"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FwdRule_ERR_07"] = _AF6CCI0012_RD_ETHFL_LKP._upen_err_profiles._FwdRule_ERR_07()
            allFields["FwdRule_ERR_06"] = _AF6CCI0012_RD_ETHFL_LKP._upen_err_profiles._FwdRule_ERR_06()
            allFields["FwdRule_ERR_05"] = _AF6CCI0012_RD_ETHFL_LKP._upen_err_profiles._FwdRule_ERR_05()
            allFields["FwdRule_ERR_04"] = _AF6CCI0012_RD_ETHFL_LKP._upen_err_profiles._FwdRule_ERR_04()
            allFields["FwdRule_ERR_03"] = _AF6CCI0012_RD_ETHFL_LKP._upen_err_profiles._FwdRule_ERR_03()
            allFields["FwdRule_ERR_02"] = _AF6CCI0012_RD_ETHFL_LKP._upen_err_profiles._FwdRule_ERR_02()
            allFields["FwdRule_ERR_01"] = _AF6CCI0012_RD_ETHFL_LKP._upen_err_profiles._FwdRule_ERR_01()
            return allFields

    class _upen_bcp_profiles(AtRegister.AtRegister):
        def name(self):
            return "IgRule BCP Profiles"
    
        def description(self):
            return "This register is used to config profile forward (fwd) rule for BCP status (BCP)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00_C400 + $bcp_profile"
            
        def startAddress(self):
            return 0x0000c400
            
        def endAddress(self):
            return 0xffffffff

        class _FwdRule_BCP_03(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FwdRule_BCP_03"
            
            def description(self):
                return "FwdRule for BCP#03"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_BCP_02(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FwdRule_BCP_02"
            
            def description(self):
                return "FwdRule for BCP#02"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_BCP_01(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FwdRule_BCP_01"
            
            def description(self):
                return "FwdRule for BCP#01"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_BCP_00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FwdRule_BCP_00"
            
            def description(self):
                return "FwdRule for BCP#00"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FwdRule_BCP_03"] = _AF6CCI0012_RD_ETHFL_LKP._upen_bcp_profiles._FwdRule_BCP_03()
            allFields["FwdRule_BCP_02"] = _AF6CCI0012_RD_ETHFL_LKP._upen_bcp_profiles._FwdRule_BCP_02()
            allFields["FwdRule_BCP_01"] = _AF6CCI0012_RD_ETHFL_LKP._upen_bcp_profiles._FwdRule_BCP_01()
            allFields["FwdRule_BCP_00"] = _AF6CCI0012_RD_ETHFL_LKP._upen_bcp_profiles._FwdRule_BCP_00()
            return allFields

    class _upen_fcn_profiles(AtRegister.AtRegister):
        def name(self):
            return "IgRule FCN Profiles"
    
        def description(self):
            return "This register is used to config profile forward (fwd) rule for FCN status (FCN)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00_C500 + $fcn_profile"
            
        def startAddress(self):
            return 0x0000c500
            
        def endAddress(self):
            return 0xffffffff

        class _FwdRule_FCN_03(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FwdRule_FCN_03"
            
            def description(self):
                return "FwdRule for FCN#03"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_FCN_02(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FwdRule_FCN_02"
            
            def description(self):
                return "FwdRule for FCN#02"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_FCN_01(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FwdRule_FCN_01"
            
            def description(self):
                return "FwdRule for FCN#01"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FwdRule_FCN_00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FwdRule_FCN_00"
            
            def description(self):
                return "FwdRule for FCN#00"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FwdRule_FCN_03"] = _AF6CCI0012_RD_ETHFL_LKP._upen_fcn_profiles._FwdRule_FCN_03()
            allFields["FwdRule_FCN_02"] = _AF6CCI0012_RD_ETHFL_LKP._upen_fcn_profiles._FwdRule_FCN_02()
            allFields["FwdRule_FCN_01"] = _AF6CCI0012_RD_ETHFL_LKP._upen_fcn_profiles._FwdRule_FCN_01()
            allFields["FwdRule_FCN_00"] = _AF6CCI0012_RD_ETHFL_LKP._upen_fcn_profiles._FwdRule_FCN_00()
            return allFields

    class _upen_link_id(AtRegister.AtRegister):
        def name(self):
            return "IgRule Link FwdTable"
    
        def description(self):
            return "This register is used to config profile forward (fwd) for eache link"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00_D000 + $link_id"
            
        def startAddress(self):
            return 0x0000d000
            
        def endAddress(self):
            return 0xffffffff

        class _FCN_Profile_Id(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 28
        
            def name(self):
                return "FCN_Profile_Id"
            
            def description(self):
                return "FCN_Profiles_Id"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _BCP_Profile_Id(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 24
        
            def name(self):
                return "BCP_Profile_Id"
            
            def description(self):
                return "BCP_Profiles_Id"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ERR_Profile_Id(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 20
        
            def name(self):
                return "ERR_Profile_Id"
            
            def description(self):
                return "ERR_Profiles_Id"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OAM_Profile_Id(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "OAM_Profile_Id"
            
            def description(self):
                return "OAM_Profiles_Id"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _NLP_Profile_Id(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 8
        
            def name(self):
                return "NLP_Profile_Id"
            
            def description(self):
                return "NLP_Profiles_Id"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FCN_FwdChk_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FCN_FwdChk_En"
            
            def description(self):
                return "FwdFCN check Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _BCP_FwdChk_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "BCP_FwdChk_En"
            
            def description(self):
                return "FwdBCP check Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ERR_FwdChk_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "ERR_FwdChk_En"
            
            def description(self):
                return "FwdERR check Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OAM_FwdChk_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "OAM_FwdChk_En"
            
            def description(self):
                return "FwdOAM check Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _NLP_FwdChk_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "NLP_FwdChk_En"
            
            def description(self):
                return "FwdNLP check Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FCN_Profile_Id"] = _AF6CCI0012_RD_ETHFL_LKP._upen_link_id._FCN_Profile_Id()
            allFields["BCP_Profile_Id"] = _AF6CCI0012_RD_ETHFL_LKP._upen_link_id._BCP_Profile_Id()
            allFields["ERR_Profile_Id"] = _AF6CCI0012_RD_ETHFL_LKP._upen_link_id._ERR_Profile_Id()
            allFields["OAM_Profile_Id"] = _AF6CCI0012_RD_ETHFL_LKP._upen_link_id._OAM_Profile_Id()
            allFields["NLP_Profile_Id"] = _AF6CCI0012_RD_ETHFL_LKP._upen_link_id._NLP_Profile_Id()
            allFields["FCN_FwdChk_En"] = _AF6CCI0012_RD_ETHFL_LKP._upen_link_id._FCN_FwdChk_En()
            allFields["BCP_FwdChk_En"] = _AF6CCI0012_RD_ETHFL_LKP._upen_link_id._BCP_FwdChk_En()
            allFields["ERR_FwdChk_En"] = _AF6CCI0012_RD_ETHFL_LKP._upen_link_id._ERR_FwdChk_En()
            allFields["OAM_FwdChk_En"] = _AF6CCI0012_RD_ETHFL_LKP._upen_link_id._OAM_FwdChk_En()
            allFields["NLP_FwdChk_En"] = _AF6CCI0012_RD_ETHFL_LKP._upen_link_id._NLP_FwdChk_En()
            return allFields

    class _upen_pkdump_ctl(AtRegister.AtRegister):
        def name(self):
            return "IgDump Control"
    
        def description(self):
            return "This register is used to config control dump engine"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000800
            
        def endAddress(self):
            return 0xffffffff

        class _Dump_Psize(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Dump_Psize"
            
            def description(self):
                return "Dump packet size 0x0 : dump full packet 0x1 : dump 32 bytes header 0x2 : dump 64 bytes header 0x3 : dump 128 bytes header 0x4 : dump 256 bytes header 0x5 : dump 512 bytes header 0x6 : dump 1024 bytes header other : dump 2048 bytes header"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Dump_Keep(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "Dump_Keep"
            
            def description(self):
                return "Dump keep status 0x0 : Clear Buffer when cpu write the register 0x1 : Keep buffer and continous dump next"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Dump_Roll(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "Dump_Roll"
            
            def description(self):
                return "Dump Roll buffer 0x0 : Stop dump when buffer full 0x1 : Continuous dump when buffer full"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Dump_Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Dump_Type"
            
            def description(self):
                return "Dump Type 0x0 : Dump all packets 0x1 : only dump error packets 0x2 : only dump good packets 0x3 : disable dump"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Dump_Src(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Dump_Src"
            
            def description(self):
                return "Dump source 0x0 : Dump packets 0x1 : Dump output info 0x2 : Dump input info other : Unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Dump_Sel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Dump_Sel"
            
            def description(self):
                return "Dump Seclect ID 0x0 : TDM_ID selected, Connection ID [15:0] ~ Map_ID, [19:16]~xbus 0x1 : Link_ID selected, Connection ID [15:0]~ output link id 0x2 : Mix Bundle_Link_ID selected, Connection ID [15:0] ~link id, [31:16] ~ bundle_id 0x3 : Ethernet Flow ID selected, Connection ID [15:0] ~link id, other : Unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Dump_Psize"] = _AF6CCI0012_RD_ETHFL_LKP._upen_pkdump_ctl._Dump_Psize()
            allFields["Dump_Keep"] = _AF6CCI0012_RD_ETHFL_LKP._upen_pkdump_ctl._Dump_Keep()
            allFields["Dump_Roll"] = _AF6CCI0012_RD_ETHFL_LKP._upen_pkdump_ctl._Dump_Roll()
            allFields["Dump_Type"] = _AF6CCI0012_RD_ETHFL_LKP._upen_pkdump_ctl._Dump_Type()
            allFields["Dump_Src"] = _AF6CCI0012_RD_ETHFL_LKP._upen_pkdump_ctl._Dump_Src()
            allFields["Dump_Sel"] = _AF6CCI0012_RD_ETHFL_LKP._upen_pkdump_ctl._Dump_Sel()
            return allFields

    class _upen_pkdump_sta(AtRegister.AtRegister):
        def name(self):
            return "IgDump Connection"
    
        def description(self):
            return "This register is used to config control dump engine"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000801
            
        def endAddress(self):
            return 0xffffffff

        class _Dump_Add(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Dump_Add"
            
            def description(self):
                return "Dump current address"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Dump_Fsm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Dump_Fsm"
            
            def description(self):
                return "Dump Fsm 0x0: IDLE 0x1: READY 0x2: CLEAR 0x3: START 0x4: STOP 0x5: DUMP 0x6: WIN 0x7: FAIL 0x8: WEOP 0xF: DONE"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Dump_Add"] = _AF6CCI0012_RD_ETHFL_LKP._upen_pkdump_sta._Dump_Add()
            allFields["Dump_Fsm"] = _AF6CCI0012_RD_ETHFL_LKP._upen_pkdump_sta._Dump_Fsm()
            return allFields

    class _upen_pkdump_cid(AtRegister.AtRegister):
        def name(self):
            return "IgDump Connection"
    
        def description(self):
            return "This register is used to config control dump engine"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000802
            
        def endAddress(self):
            return 0xffffffff

        class _Dump_CID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Dump_CID"
            
            def description(self):
                return "Dump Connection ID, depend Dump_Sel field"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Dump_CID"] = _AF6CCI0012_RD_ETHFL_LKP._upen_pkdump_cid._Dump_CID()
            return allFields

    class _upen_pkdump_pkc(AtRegister.AtRegister):
        def name(self):
            return "IgDump Pkt counter"
    
        def description(self):
            return "This register is used to config control dump engine"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000804
            
        def endAddress(self):
            return 0xffffffff

        class _Dump_Pkts(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Dump_Pkts"
            
            def description(self):
                return "Dump packet counters"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Dump_Pkts"] = _AF6CCI0012_RD_ETHFL_LKP._upen_pkdump_pkc._Dump_Pkts()
            return allFields

    class _upen_pkdump_pks(AtRegister.AtRegister):
        def name(self):
            return "IgDump Pkt Information"
    
        def description(self):
            return "This register is used to read packet information, packet_id equa 0 is first packet dump. : each address corresspond for 64 bits data (8 bytes) : number of address Pkt_info_NAdd = Pkt_Info_Len / 8 : location to read data packet dump is {Pkt_info_SAdd,Pkt_info_SAdd+Pkt_info_NAdd}."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00_0900 + $pkt_id"
            
        def startAddress(self):
            return 0x00000900
            
        def endAddress(self):
            return 0xffffffff

        class _Pkt_Info_Len(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Pkt_Info_Len"
            
            def description(self):
                return "Packet length 0x0: zero byte valid 0x1: one byte valid 0x2: two byte valid, ..."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Pkt_Info_Typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Pkt_Info_Typ"
            
            def description(self):
                return "Packet infor type 0x0: Packet 0x1: Word"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Pkt_Info_SAdd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Pkt_Info_SAdd"
            
            def description(self):
                return "Start address of packet dump"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Pkt_Info_Len"] = _AF6CCI0012_RD_ETHFL_LKP._upen_pkdump_pks._Pkt_Info_Len()
            allFields["Pkt_Info_Typ"] = _AF6CCI0012_RD_ETHFL_LKP._upen_pkdump_pks._Pkt_Info_Typ()
            allFields["Pkt_Info_SAdd"] = _AF6CCI0012_RD_ETHFL_LKP._upen_pkdump_pks._Pkt_Info_SAdd()
            return allFields

    class _upen_pkdump_pkd(AtRegister.AtRegister):
        def name(self):
            return "IgDump Pkt Data"
    
        def description(self):
            return "This register is used to read packet data, each address corresspond 2xdword (64 bits) data"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00_0C00 + $add_id*2 + dword_id"
            
        def startAddress(self):
            return 0x00000c00
            
        def endAddress(self):
            return 0xffffffff

        class _Pkt_Dump_Data(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Pkt_Dump_Data"
            
            def description(self):
                return "Packet data, dword_id = 0 ~ data[63:32] dword_id = 1 ~ data[31:00]"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Pkt_Dump_Data"] = _AF6CCI0012_RD_ETHFL_LKP._upen_pkdump_pkd._Pkt_Dump_Data()
            return allFields

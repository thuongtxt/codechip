import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0012_RD_LOENC(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["pencfg_lshift8"] = _AF6CCI0012_RD_LOENC._pencfg_lshift8()
        allRegisters["dhold0_pen"] = _AF6CCI0012_RD_LOENC._dhold0_pen()
        allRegisters["dhold1_pen"] = _AF6CCI0012_RD_LOENC._dhold1_pen()
        allRegisters["dhold2_pen"] = _AF6CCI0012_RD_LOENC._dhold2_pen()
        allRegisters["upenctr1"] = _AF6CCI0012_RD_LOENC._upenctr1()
        allRegisters["upensta0"] = _AF6CCI0012_RD_LOENC._upensta0()
        allRegisters["upensta1"] = _AF6CCI0012_RD_LOENC._upensta1()
        allRegisters["upen_cnt_tdm_pkt_rc"] = _AF6CCI0012_RD_LOENC._upen_cnt_tdm_pkt_rc()
        allRegisters["upen_cnt_tdm_pkt_ro"] = _AF6CCI0012_RD_LOENC._upen_cnt_tdm_pkt_ro()
        allRegisters["upen_cnt_tdm_byte_rc"] = _AF6CCI0012_RD_LOENC._upen_cnt_tdm_byte_rc()
        allRegisters["upen_cnt_tdm_byte_ro"] = _AF6CCI0012_RD_LOENC._upen_cnt_tdm_byte_ro()
        allRegisters["upen_cnt_eop_rc"] = _AF6CCI0012_RD_LOENC._upen_cnt_eop_rc()
        allRegisters["upen_cnt_eop_ro"] = _AF6CCI0012_RD_LOENC._upen_cnt_eop_ro()
        allRegisters["upen_cnt_byte_rc"] = _AF6CCI0012_RD_LOENC._upen_cnt_byte_rc()
        allRegisters["upen_cnt_byte_ro"] = _AF6CCI0012_RD_LOENC._upen_cnt_byte_ro()
        allRegisters["upen_cnt_enc_byteotdm_rc"] = _AF6CCI0012_RD_LOENC._upen_cnt_enc_byteotdm_rc()
        allRegisters["upen_cnt_enc_byteotdm_ro"] = _AF6CCI0012_RD_LOENC._upen_cnt_enc_byteotdm_ro()
        allRegisters["upen_cnt_enc_pktotdm_rc"] = _AF6CCI0012_RD_LOENC._upen_cnt_enc_pktotdm_rc()
        allRegisters["upen_cnt_enc_pktotdm_ro"] = _AF6CCI0012_RD_LOENC._upen_cnt_enc_pktotdm_ro()
        allRegisters["upen_cnt_enc_eopfrmpda_rc"] = _AF6CCI0012_RD_LOENC._upen_cnt_enc_eopfrmpda_rc()
        allRegisters["upen_cnt_enc_eopfrmpda_ro"] = _AF6CCI0012_RD_LOENC._upen_cnt_enc_eopfrmpda_ro()
        allRegisters["upen_cnt_enc_sopfrmpda_rc"] = _AF6CCI0012_RD_LOENC._upen_cnt_enc_sopfrmpda_rc()
        allRegisters["upen_cnt_enc_sopfrmpda_ro"] = _AF6CCI0012_RD_LOENC._upen_cnt_enc_sopfrmpda_ro()
        allRegisters["upen_cnt_enc_bytefrmpda_rc"] = _AF6CCI0012_RD_LOENC._upen_cnt_enc_bytefrmpda_rc()
        allRegisters["upen_cnt_enc_bytefrmpda_ro"] = _AF6CCI0012_RD_LOENC._upen_cnt_enc_bytefrmpda_ro()
        allRegisters["upen_cnt_enc_abort_rc"] = _AF6CCI0012_RD_LOENC._upen_cnt_enc_abort_rc()
        allRegisters["upen_cnt_enc_abort_ro"] = _AF6CCI0012_RD_LOENC._upen_cnt_enc_abort_ro()
        allRegisters["upen_cnt_enc_nstuff_rc"] = _AF6CCI0012_RD_LOENC._upen_cnt_enc_nstuff_rc()
        allRegisters["upen_cnt_enc_nstuff_ro"] = _AF6CCI0012_RD_LOENC._upen_cnt_enc_nstuff_ro()
        return allRegisters

    class _pencfg_lshift8(AtRegister.AtRegister):
        def name(self):
            return "pencfg_lshift8"
    
        def description(self):
            return "This register is used to active low order stuff enc(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _act_lenc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "act_lenc"
            
            def description(self):
                return "1: active enc 0: disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["act_lenc"] = _AF6CCI0012_RD_LOENC._pencfg_lshift8._act_lenc()
            return allFields

    class _dhold0_pen(AtRegister.AtRegister):
        def name(self):
            return "dhold0_pen"
    
        def description(self):
            return "This register is used to hold data when width bus > 32 bit (Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0xffffffff

        class _hold0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "hold0"
            
            def description(self):
                return "hold from [63:32]"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["hold0"] = _AF6CCI0012_RD_LOENC._dhold0_pen._hold0()
            return allFields

    class _dhold1_pen(AtRegister.AtRegister):
        def name(self):
            return "dhold1_pen"
    
        def description(self):
            return "This register is used to hold data when width bus > 32 bit (Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _hold1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "hold1"
            
            def description(self):
                return "hold from [95:64]"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["hold1"] = _AF6CCI0012_RD_LOENC._dhold1_pen._hold1()
            return allFields

    class _dhold2_pen(AtRegister.AtRegister):
        def name(self):
            return "dhold2_pen"
    
        def description(self):
            return "This register is used to hold data when width bus > 32 bit (Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000004
            
        def endAddress(self):
            return 0xffffffff

        class _hold2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "hold2"
            
            def description(self):
                return "hold from [127:96]"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["hold2"] = _AF6CCI0012_RD_LOENC._dhold2_pen._hold2()
            return allFields

    class _upenctr1(AtRegister.AtRegister):
        def name(self):
            return "upenctr1"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD). HDL_PATH: ramctr1.membuf.ram.ram[$chid]"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0800+ $chid"
            
        def startAddress(self):
            return 0x00000800
            
        def endAddress(self):
            return 0x00000bff

        class _scrable_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "scrable_en"
            
            def description(self):
                return "scrable_en"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_lsbfirst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "cfg_lsbfirst"
            
            def description(self):
                return "receive is LSB first, (0) is default MSB"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _flagmod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "flagmod"
            
            def description(self):
                return "0 : one flag 1: two flag"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _idle_mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "idle_mode"
            
            def description(self):
                return "0: 0x7E 1:0xFF"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fcsmod32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "fcsmod32"
            
            def description(self):
                return "fcsmod32"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fcsinscfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "fcsinscfg"
            
            def description(self):
                return "fcsinscfg"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fcsmsb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "fcsmsb"
            
            def description(self):
                return "fcsmsb"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _bit_stuff(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "bit_stuff"
            
            def description(self):
                return "bit_stuff"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["scrable_en"] = _AF6CCI0012_RD_LOENC._upenctr1._scrable_en()
            allFields["cfg_lsbfirst"] = _AF6CCI0012_RD_LOENC._upenctr1._cfg_lsbfirst()
            allFields["flagmod"] = _AF6CCI0012_RD_LOENC._upenctr1._flagmod()
            allFields["idle_mode"] = _AF6CCI0012_RD_LOENC._upenctr1._idle_mode()
            allFields["fcsmod32"] = _AF6CCI0012_RD_LOENC._upenctr1._fcsmod32()
            allFields["fcsinscfg"] = _AF6CCI0012_RD_LOENC._upenctr1._fcsinscfg()
            allFields["fcsmsb"] = _AF6CCI0012_RD_LOENC._upenctr1._fcsmsb()
            allFields["bit_stuff"] = _AF6CCI0012_RD_LOENC._upenctr1._bit_stuff()
            return allFields

    class _upensta0(AtRegister.AtRegister):
        def name(self):
            return "upensta0"
    
        def description(self):
            return "This register save status , SW wr 0 all when reconfig enc (Add 25bit:TBD). HDL_PATH: mem_ramsta0.ram.ram[$chid]"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x2800+ $chid"
            
        def startAddress(self):
            return 0x00002800
            
        def endAddress(self):
            return 0x00002bff

        class _sta0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 109
                
            def startBit(self):
                return 0
        
            def name(self):
                return "sta0"
            
            def description(self):
                return "sta0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["sta0"] = _AF6CCI0012_RD_LOENC._upensta0._sta0()
            return allFields

    class _upensta1(AtRegister.AtRegister):
        def name(self):
            return "upensta1"
    
        def description(self):
            return "This register save status , SW wr 0 all when reconfig enc (Add 25bit:TBD). HDL_PATH: mem_ramsta1.ram.ram[$chid]"
            
        def width(self):
            return 2
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3000+ $chid"
            
        def startAddress(self):
            return 0x00003000
            
        def endAddress(self):
            return 0x00003bff

        class _sta1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "sta1"
            
            def description(self):
                return "sta1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["sta1"] = _AF6CCI0012_RD_LOENC._upensta1._sta1()
            return allFields

    class _upen_cnt_tdm_pkt_rc(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_tdm_pkt"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1000+ $chid"
            
        def startAddress(self):
            return 0x00001000
            
        def endAddress(self):
            return 0x000013ff

        class _updo_cnt_tdm_pkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_tdm_pkt"
            
            def description(self):
                return "cnt pkt"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_tdm_pkt"] = _AF6CCI0012_RD_LOENC._upen_cnt_tdm_pkt_rc._updo_cnt_tdm_pkt()
            return allFields

    class _upen_cnt_tdm_pkt_ro(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_tdm_pkt"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1400+ $chid"
            
        def startAddress(self):
            return 0x00001400
            
        def endAddress(self):
            return 0x000017ff

        class _updo_cnt_tdm_pkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_tdm_pkt"
            
            def description(self):
                return "cnt pkt"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_tdm_pkt"] = _AF6CCI0012_RD_LOENC._upen_cnt_tdm_pkt_ro._updo_cnt_tdm_pkt()
            return allFields

    class _upen_cnt_tdm_byte_rc(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_tdm_byte"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3800+ $chid"
            
        def startAddress(self):
            return 0x00003800
            
        def endAddress(self):
            return 0x00003bff

        class _updo_cnt_tdm_byte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_tdm_byte"
            
            def description(self):
                return "cnt byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_tdm_byte"] = _AF6CCI0012_RD_LOENC._upen_cnt_tdm_byte_rc._updo_cnt_tdm_byte()
            return allFields

    class _upen_cnt_tdm_byte_ro(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_tdm_byte"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3C00+ $chid"
            
        def startAddress(self):
            return 0x00003c00
            
        def endAddress(self):
            return 0x00003fff

        class _updo_cnt_tdm_byte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_tdm_byte"
            
            def description(self):
                return "cnt byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_tdm_byte"] = _AF6CCI0012_RD_LOENC._upen_cnt_tdm_byte_ro._updo_cnt_tdm_byte()
            return allFields

    class _upen_cnt_eop_rc(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_eop"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1800+ $chid"
            
        def startAddress(self):
            return 0x00001800
            
        def endAddress(self):
            return 0x00001bff

        class _updo_cnt_eop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_eop"
            
            def description(self):
                return "eop cnt enc"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_eop"] = _AF6CCI0012_RD_LOENC._upen_cnt_eop_rc._updo_cnt_eop()
            return allFields

    class _upen_cnt_eop_ro(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_eop"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1C00+ $chid"
            
        def startAddress(self):
            return 0x00001c00
            
        def endAddress(self):
            return 0x00001fff

        class _updo_cnt_eop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_eop"
            
            def description(self):
                return "eop cnt enc"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_eop"] = _AF6CCI0012_RD_LOENC._upen_cnt_eop_ro._updo_cnt_eop()
            return allFields

    class _upen_cnt_byte_rc(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_byte"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x2000+ $chid"
            
        def startAddress(self):
            return 0x00002000
            
        def endAddress(self):
            return 0x000023ff

        class _updo_cnt_byte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_byte"
            
            def description(self):
                return "cnt byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_byte"] = _AF6CCI0012_RD_LOENC._upen_cnt_byte_rc._updo_cnt_byte()
            return allFields

    class _upen_cnt_byte_ro(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_byte"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x2400+ $chid"
            
        def startAddress(self):
            return 0x00002400
            
        def endAddress(self):
            return 0x000027ff

        class _updo_cnt_byte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_byte"
            
            def description(self):
                return "cnt byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_byte"] = _AF6CCI0012_RD_LOENC._upen_cnt_byte_ro._updo_cnt_byte()
            return allFields

    class _upen_cnt_enc_byteotdm_rc(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_enc_byteotdm"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000010
            
        def endAddress(self):
            return 0xffffffff

        class _updo_cnt_enc_byteotdm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_enc_byteotdm"
            
            def description(self):
                return "cnt byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_enc_byteotdm"] = _AF6CCI0012_RD_LOENC._upen_cnt_enc_byteotdm_rc._updo_cnt_enc_byteotdm()
            return allFields

    class _upen_cnt_enc_byteotdm_ro(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_enc_byteotdm"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000012
            
        def endAddress(self):
            return 0xffffffff

        class _updo_cnt_enc_byteotdm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_enc_byteotdm"
            
            def description(self):
                return "cnt byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_enc_byteotdm"] = _AF6CCI0012_RD_LOENC._upen_cnt_enc_byteotdm_ro._updo_cnt_enc_byteotdm()
            return allFields

    class _upen_cnt_enc_pktotdm_rc(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_enc_pktotdm"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000020
            
        def endAddress(self):
            return 0xffffffff

        class _updo_cnt_enc_pktotdm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_enc_pktotdm"
            
            def description(self):
                return "cnt pkt"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_enc_pktotdm"] = _AF6CCI0012_RD_LOENC._upen_cnt_enc_pktotdm_rc._updo_cnt_enc_pktotdm()
            return allFields

    class _upen_cnt_enc_pktotdm_ro(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_enc_pktotdm"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000022
            
        def endAddress(self):
            return 0xffffffff

        class _updo_cnt_enc_pktotdm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_enc_pktotdm"
            
            def description(self):
                return "cnt pkt"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_enc_pktotdm"] = _AF6CCI0012_RD_LOENC._upen_cnt_enc_pktotdm_ro._updo_cnt_enc_pktotdm()
            return allFields

    class _upen_cnt_enc_eopfrmpda_rc(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_enc_eopfrmpda"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000014
            
        def endAddress(self):
            return 0xffffffff

        class _updo_cnt_enc_eopfrmpda(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_enc_eopfrmpda"
            
            def description(self):
                return "cnt pkt"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_enc_eopfrmpda"] = _AF6CCI0012_RD_LOENC._upen_cnt_enc_eopfrmpda_rc._updo_cnt_enc_eopfrmpda()
            return allFields

    class _upen_cnt_enc_eopfrmpda_ro(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_enc_eopfrmpda"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000016
            
        def endAddress(self):
            return 0xffffffff

        class _updo_cnt_enc_eopfrmpda(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_enc_eopfrmpda"
            
            def description(self):
                return "cnt pkt"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_enc_eopfrmpda"] = _AF6CCI0012_RD_LOENC._upen_cnt_enc_eopfrmpda_ro._updo_cnt_enc_eopfrmpda()
            return allFields

    class _upen_cnt_enc_sopfrmpda_rc(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_enc_sopfrmpda"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000018
            
        def endAddress(self):
            return 0xffffffff

        class _updo_cnt_enc_sopfrmpda(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_enc_sopfrmpda"
            
            def description(self):
                return "cnt pkt"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_enc_sopfrmpda"] = _AF6CCI0012_RD_LOENC._upen_cnt_enc_sopfrmpda_rc._updo_cnt_enc_sopfrmpda()
            return allFields

    class _upen_cnt_enc_sopfrmpda_ro(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_enc_sopfrmpda"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000001a
            
        def endAddress(self):
            return 0xffffffff

        class _updo_cnt_enc_sopfrmpda(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_enc_sopfrmpda"
            
            def description(self):
                return "cnt pkt"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_enc_sopfrmpda"] = _AF6CCI0012_RD_LOENC._upen_cnt_enc_sopfrmpda_ro._updo_cnt_enc_sopfrmpda()
            return allFields

    class _upen_cnt_enc_bytefrmpda_rc(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_enc_bytefrmpda"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000001c
            
        def endAddress(self):
            return 0xffffffff

        class _updo_cnt_enc_bytefrmpda(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_enc_bytefrmpda"
            
            def description(self):
                return "cnt byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_enc_bytefrmpda"] = _AF6CCI0012_RD_LOENC._upen_cnt_enc_bytefrmpda_rc._updo_cnt_enc_bytefrmpda()
            return allFields

    class _upen_cnt_enc_bytefrmpda_ro(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_enc_bytefrmpda"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000001e
            
        def endAddress(self):
            return 0xffffffff

        class _updo_cnt_enc_bytefrmpda(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_enc_bytefrmpda"
            
            def description(self):
                return "cnt byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_enc_bytefrmpda"] = _AF6CCI0012_RD_LOENC._upen_cnt_enc_bytefrmpda_ro._updo_cnt_enc_bytefrmpda()
            return allFields

    class _upen_cnt_enc_abort_rc(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_enc_abort"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000024
            
        def endAddress(self):
            return 0xffffffff

        class _updo_cnt_enc_abort(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_enc_abort"
            
            def description(self):
                return "abort pkt"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_enc_abort"] = _AF6CCI0012_RD_LOENC._upen_cnt_enc_abort_rc._updo_cnt_enc_abort()
            return allFields

    class _upen_cnt_enc_abort_ro(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_enc_abort"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000026
            
        def endAddress(self):
            return 0xffffffff

        class _updo_cnt_enc_abort(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_enc_abort"
            
            def description(self):
                return "abort pkt"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_enc_abort"] = _AF6CCI0012_RD_LOENC._upen_cnt_enc_abort_ro._updo_cnt_enc_abort()
            return allFields

    class _upen_cnt_enc_nstuff_rc(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_enc_nstuff"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000028
            
        def endAddress(self):
            return 0xffffffff

        class _updo_cnt_enc_nstuff(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_enc_nstuff"
            
            def description(self):
                return "num byte stuff"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_enc_nstuff"] = _AF6CCI0012_RD_LOENC._upen_cnt_enc_nstuff_rc._updo_cnt_enc_nstuff()
            return allFields

    class _upen_cnt_enc_nstuff_ro(AtRegister.AtRegister):
        def name(self):
            return "upen_cnt_enc_nstuff"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000002a
            
        def endAddress(self):
            return 0xffffffff

        class _updo_cnt_enc_nstuff(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_cnt_enc_nstuff"
            
            def description(self):
                return "num byte stuff"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_cnt_enc_nstuff"] = _AF6CCI0012_RD_LOENC._upen_cnt_enc_nstuff_ro._updo_cnt_enc_nstuff()
            return allFields

import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0012_RD_CLA(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["cla_glb_psn"] = _AF6CCI0012_RD_CLA._cla_glb_psn()
        allRegisters["cla_per_pw_ctrl"] = _AF6CCI0012_RD_CLA._cla_per_pw_ctrl()
        allRegisters["cla_per_flow_table"] = _AF6CCI0012_RD_CLA._cla_per_flow_table()
        allRegisters["cla_hbce_hash_table"] = _AF6CCI0012_RD_CLA._cla_hbce_hash_table()
        allRegisters["cla_hbce_lkup_info"] = _AF6CCI0012_RD_CLA._cla_hbce_lkup_info()
        allRegisters["cla_per_grp_enb"] = _AF6CCI0012_RD_CLA._cla_per_grp_enb()
        allRegisters["cla_vlan_lk"] = _AF6CCI0012_RD_CLA._cla_vlan_lk()
        allRegisters["cla_per_eth_enb"] = _AF6CCI0012_RD_CLA._cla_per_eth_enb()
        allRegisters["Eth_cnt0_64_ro"] = _AF6CCI0012_RD_CLA._Eth_cnt0_64_ro()
        allRegisters["Eth_cnt0_64_rc"] = _AF6CCI0012_RD_CLA._Eth_cnt0_64_rc()
        allRegisters["Eth_cnt65_127_ro"] = _AF6CCI0012_RD_CLA._Eth_cnt65_127_ro()
        allRegisters["Eth_cnt65_127_rc"] = _AF6CCI0012_RD_CLA._Eth_cnt65_127_rc()
        allRegisters["Eth_cnt128_255_ro"] = _AF6CCI0012_RD_CLA._Eth_cnt128_255_ro()
        allRegisters["Eth_cnt128_255_rc"] = _AF6CCI0012_RD_CLA._Eth_cnt128_255_rc()
        allRegisters["Eth_cnt256_511_ro"] = _AF6CCI0012_RD_CLA._Eth_cnt256_511_ro()
        allRegisters["Eth_cnt256_511_rc"] = _AF6CCI0012_RD_CLA._Eth_cnt256_511_rc()
        allRegisters["Eth_cnt512_1024_ro"] = _AF6CCI0012_RD_CLA._Eth_cnt512_1024_ro()
        allRegisters["Eth_cnt512_1024_rc"] = _AF6CCI0012_RD_CLA._Eth_cnt512_1024_rc()
        allRegisters["Eth_cnt1025_1528_ro"] = _AF6CCI0012_RD_CLA._Eth_cnt1025_1528_ro()
        allRegisters["Eth_cnt1025_1528_rc"] = _AF6CCI0012_RD_CLA._Eth_cnt1025_1528_rc()
        allRegisters["Eth_cnt1529_2047_ro"] = _AF6CCI0012_RD_CLA._Eth_cnt1529_2047_ro()
        allRegisters["Eth_cnt1529_2047_rc"] = _AF6CCI0012_RD_CLA._Eth_cnt1529_2047_rc()
        allRegisters["Eth_cnt_jumbo_ro"] = _AF6CCI0012_RD_CLA._Eth_cnt_jumbo_ro()
        allRegisters["Eth_cnt_jumbo_rc"] = _AF6CCI0012_RD_CLA._Eth_cnt_jumbo_rc()
        allRegisters["Eth_cnt_Unicast_ro"] = _AF6CCI0012_RD_CLA._Eth_cnt_Unicast_ro()
        allRegisters["Eth_cnt_Unicast_rc"] = _AF6CCI0012_RD_CLA._Eth_cnt_Unicast_rc()
        allRegisters["rx_port_pkt_cnt_ro"] = _AF6CCI0012_RD_CLA._rx_port_pkt_cnt_ro()
        allRegisters["rx_port_pkt_cnt_rc"] = _AF6CCI0012_RD_CLA._rx_port_pkt_cnt_rc()
        allRegisters["rx_port_bcast_pkt_cnt_ro"] = _AF6CCI0012_RD_CLA._rx_port_bcast_pkt_cnt_ro()
        allRegisters["rx_port_bcast_pkt_cnt_rc"] = _AF6CCI0012_RD_CLA._rx_port_bcast_pkt_cnt_rc()
        allRegisters["rx_port_mcast_pkt_cnt_ro"] = _AF6CCI0012_RD_CLA._rx_port_mcast_pkt_cnt_ro()
        allRegisters["rx_port_mcast_pkt_cnt_rc"] = _AF6CCI0012_RD_CLA._rx_port_mcast_pkt_cnt_rc()
        allRegisters["rx_port_under_size_pkt_cnt_ro"] = _AF6CCI0012_RD_CLA._rx_port_under_size_pkt_cnt_ro()
        allRegisters["rx_port_under_size_pkt_cnt_rc"] = _AF6CCI0012_RD_CLA._rx_port_under_size_pkt_cnt_rc()
        allRegisters["rx_port_over_size_pkt_cnt_ro"] = _AF6CCI0012_RD_CLA._rx_port_over_size_pkt_cnt_ro()
        allRegisters["rx_port_over_size_pkt_cnt_rc"] = _AF6CCI0012_RD_CLA._rx_port_over_size_pkt_cnt_rc()
        allRegisters["Eth_cnt_phy_err_ro"] = _AF6CCI0012_RD_CLA._Eth_cnt_phy_err_ro()
        allRegisters["Eth_cnt_phy_err_rc"] = _AF6CCI0012_RD_CLA._Eth_cnt_phy_err_rc()
        allRegisters["Eth_cnt_byte_ro"] = _AF6CCI0012_RD_CLA._Eth_cnt_byte_ro()
        allRegisters["Eth_cnt_byte_rc"] = _AF6CCI0012_RD_CLA._Eth_cnt_byte_rc()
        allRegisters["cla_hbce_lkup_info_extra"] = _AF6CCI0012_RD_CLA._cla_hbce_lkup_info_extra()
        allRegisters["cla_2_cdr_cfg"] = _AF6CCI0012_RD_CLA._cla_2_cdr_cfg()
        allRegisters["cla_hold_status"] = _AF6CCI0012_RD_CLA._cla_hold_status()
        allRegisters["rdha3_0_control"] = _AF6CCI0012_RD_CLA._rdha3_0_control()
        allRegisters["rdha7_4_control"] = _AF6CCI0012_RD_CLA._rdha7_4_control()
        allRegisters["rdha11_8_control"] = _AF6CCI0012_RD_CLA._rdha11_8_control()
        allRegisters["rdha15_12_control"] = _AF6CCI0012_RD_CLA._rdha15_12_control()
        allRegisters["rdha19_16_control"] = _AF6CCI0012_RD_CLA._rdha19_16_control()
        allRegisters["rdha23_20_control"] = _AF6CCI0012_RD_CLA._rdha23_20_control()
        allRegisters["rdha24data_control"] = _AF6CCI0012_RD_CLA._rdha24data_control()
        allRegisters["rdha_hold63_32"] = _AF6CCI0012_RD_CLA._rdha_hold63_32()
        allRegisters["rdindr_hold95_64"] = _AF6CCI0012_RD_CLA._rdindr_hold95_64()
        allRegisters["rdindr_hold127_96"] = _AF6CCI0012_RD_CLA._rdindr_hold127_96()
        allRegisters["cla_Parity_control"] = _AF6CCI0012_RD_CLA._cla_Parity_control()
        allRegisters["cla_Parity_Disable_control"] = _AF6CCI0012_RD_CLA._cla_Parity_Disable_control()
        allRegisters["cla_Parity_stk_err"] = _AF6CCI0012_RD_CLA._cla_Parity_stk_err()
        allRegisters["cla_queue_lk"] = _AF6CCI0012_RD_CLA._cla_queue_lk()
        allRegisters["cla_tpch_table_service"] = _AF6CCI0012_RD_CLA._cla_tpch_table_service()
        allRegisters["cla_oam_link"] = _AF6CCI0012_RD_CLA._cla_oam_link()
        allRegisters["cla_tpch_eos_channel"] = _AF6CCI0012_RD_CLA._cla_tpch_eos_channel()
        allRegisters["Eth_cnt_psn_err_ro"] = _AF6CCI0012_RD_CLA._Eth_cnt_psn_err_ro()
        allRegisters["Eth_cnt_psn_err_rc"] = _AF6CCI0012_RD_CLA._Eth_cnt_psn_err_rc()
        return allRegisters

    class _cla_glb_psn(AtRegister.AtRegister):
        def name(self):
            return "Classify Global PSN Control"
    
        def description(self):
            return "This register configures identification per Ethernet port"
            
        def width(self):
            return 88
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _UserDefineEthType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 87
                
            def startBit(self):
                return 72
        
            def name(self):
                return "UserDefineEthType"
            
            def description(self):
                return "The Eth_type for user define"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPTCHOffset(AtRegister.AtRegisterField):
            def stopBit(self):
                return 71
                
            def startBit(self):
                return 64
        
            def name(self):
                return "RxPTCHOffset"
            
            def description(self):
                return "The offset for channelized XAUI"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPsnDAExp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 16
        
            def name(self):
                return "RxPsnDAExp"
            
            def description(self):
                return "Mac Address expected at Rx Ethernet port"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxVlanCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RxVlanCtrl"
            
            def description(self):
                return "Vlan control to identify packet is OAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxMacCheckDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "RxMacCheckDis"
            
            def description(self):
                return "Disable to check MAC address at Ethernet port receive direction 1: Disable checking 0: Enable checking"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPsnIpUdpMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "RxPsnIpUdpMode"
            
            def description(self):
                return "This bit is applicable for Ipv4/Ipv6 packet from Ethernet side 1: Classify engine uses RxPsnIpUdpSel to decide which UDP port (Source or Destination) is used to identify pseudowire packet 0: Classify engine will automatically search for value 0x85E in source or destination UDP port. The remaining UDP port is used to identify pseudowire packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPsnIpUdpSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "RxPsnIpUdpSel"
            
            def description(self):
                return "This bit is applicable for Ipv4/Ipv6 using to select Source or Destination to identify pseudowire packet from Ethernet side. It is not use when RxPsnIpUdpMode is zero 1: Classify engine selects source UDP port to identify pseudowire packet 0: Classify engine selects destination UDP port to identify pseudowire packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPTCHDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPTCHDis"
            
            def description(self):
                return "Disable PTCH 1: Disable PTCH 0: Enable PTCH"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["UserDefineEthType"] = _AF6CCI0012_RD_CLA._cla_glb_psn._UserDefineEthType()
            allFields["RxPTCHOffset"] = _AF6CCI0012_RD_CLA._cla_glb_psn._RxPTCHOffset()
            allFields["RxPsnDAExp"] = _AF6CCI0012_RD_CLA._cla_glb_psn._RxPsnDAExp()
            allFields["RxVlanCtrl"] = _AF6CCI0012_RD_CLA._cla_glb_psn._RxVlanCtrl()
            allFields["RxMacCheckDis"] = _AF6CCI0012_RD_CLA._cla_glb_psn._RxMacCheckDis()
            allFields["RxPsnIpUdpMode"] = _AF6CCI0012_RD_CLA._cla_glb_psn._RxPsnIpUdpMode()
            allFields["RxPsnIpUdpSel"] = _AF6CCI0012_RD_CLA._cla_glb_psn._RxPsnIpUdpSel()
            allFields["RxPTCHDis"] = _AF6CCI0012_RD_CLA._cla_glb_psn._RxPTCHDis()
            return allFields

    class _cla_per_pw_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Classify Per Pseudowire Type Control"
    
        def description(self):
            return "This register configures identification types per pseudowire HDL_PATH     :cfgpwmod_ram.array.ram[$PWID]"
            
        def width(self):
            return 60
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0008000 +  PWID"
            
        def startAddress(self):
            return 0x00008000
            
        def endAddress(self):
            return 0xffffffff

        class _RxEthPwLen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 59
                
            def startBit(self):
                return 46
        
            def name(self):
                return "RxEthPwLen"
            
            def description(self):
                return "length of packet to check malform"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthSupEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 45
                
            def startBit(self):
                return 45
        
            def name(self):
                return "RxEthSupEn"
            
            def description(self):
                return "Suppress Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthCepMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 44
                
            def startBit(self):
                return 44
        
            def name(self):
                return "RxEthCepMode"
            
            def description(self):
                return "CEP mode working 0,1: CEP basic"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthRtpSsrcValue(AtRegister.AtRegisterField):
            def stopBit(self):
                return 43
                
            def startBit(self):
                return 12
        
            def name(self):
                return "RxEthRtpSsrcValue"
            
            def description(self):
                return "This value is used to compare with SSRC value in RTP header of received TDM PW packets"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthRtpPtValue(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 5
        
            def name(self):
                return "RxEthRtpPtValue"
            
            def description(self):
                return "This value is used to compare with PT value in RTP header of received TDM PW packets"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthRtpEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RxEthRtpEn"
            
            def description(self):
                return "Enable RTP 1: Enable RTP 0: Disable RTP"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthRtpSsrcChkEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "RxEthRtpSsrcChkEn"
            
            def description(self):
                return "Enable checking SSRC field of RTP header in received TDM PW packet 1: Enable checking 0: Disable checking"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthRtpPtChkEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "RxEthRtpPtChkEn"
            
            def description(self):
                return "Enable checking PT field of RTP header in received TDM PW packet 1: Enable checking 0: Disable checking"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthPwType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthPwType"
            
            def description(self):
                return "this is PW type working 0: CES mode without CAS 1: CES mode with CAS 2: CEP mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthPwLen"] = _AF6CCI0012_RD_CLA._cla_per_pw_ctrl._RxEthPwLen()
            allFields["RxEthSupEn"] = _AF6CCI0012_RD_CLA._cla_per_pw_ctrl._RxEthSupEn()
            allFields["RxEthCepMode"] = _AF6CCI0012_RD_CLA._cla_per_pw_ctrl._RxEthCepMode()
            allFields["RxEthRtpSsrcValue"] = _AF6CCI0012_RD_CLA._cla_per_pw_ctrl._RxEthRtpSsrcValue()
            allFields["RxEthRtpPtValue"] = _AF6CCI0012_RD_CLA._cla_per_pw_ctrl._RxEthRtpPtValue()
            allFields["RxEthRtpEn"] = _AF6CCI0012_RD_CLA._cla_per_pw_ctrl._RxEthRtpEn()
            allFields["RxEthRtpSsrcChkEn"] = _AF6CCI0012_RD_CLA._cla_per_pw_ctrl._RxEthRtpSsrcChkEn()
            allFields["RxEthRtpPtChkEn"] = _AF6CCI0012_RD_CLA._cla_per_pw_ctrl._RxEthRtpPtChkEn()
            allFields["RxEthPwType"] = _AF6CCI0012_RD_CLA._cla_per_pw_ctrl._RxEthPwType()
            return allFields

    class _cla_per_flow_table(AtRegister.AtRegister):
        def name(self):
            return "Classify Per flow Table Control"
    
        def description(self):
            return "This register configures table action for flow traffic HDL_PATH     :iaf6msrtlcla_bufdat.ethflow_tablemem.ram.ram[$FLOWID]"
            
        def width(self):
            return 67
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0004000 +  FLOWID"
            
        def startAddress(self):
            return 0x00004000
            
        def endAddress(self):
            return 0xffffffff

        class _RxPwTableLanFcsRmv(AtRegister.AtRegisterField):
            def stopBit(self):
                return 66
                
            def startBit(self):
                return 66
        
            def name(self):
                return "RxPwTableLanFcsRmv"
            
            def description(self):
                return "Remove LAN FCS 1: Remove LAN FCS 0: Keep LAN FCS"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPwTableQ922len(AtRegister.AtRegisterField):
            def stopBit(self):
                return 65
                
            def startBit(self):
                return 64
        
            def name(self):
                return "RxPwTableQ922len"
            
            def description(self):
                return "Q922 length 2/3/4 bytes 0: 2 bytes 1: 3 bytes 2: 4 bytes"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPwTableQ922Field(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 32
        
            def name(self):
                return "RxPwTableQ922Field"
            
            def description(self):
                return "Q922 field 2/3/4 bytes (LSB)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPwTableSubSerType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 28
        
            def name(self):
                return "RxPwTableSubSerType"
            
            def description(self):
                return "Sub-service type 0: None 1: PPPoMPLS 2: IPv4oMPLS 3: IPv6oMPLS 4: ETHoMPLS 5: Reserve"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPwTableTdmAct(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 24
        
            def name(self):
                return "RxPwTableTdmAct"
            
            def description(self):
                return "TDM header action 0: Nothing 1: MLPPP short mode 2: MLPPP long mode 3: IETF MLFR UNI/NNI mode 4: IETF MLFR End2End mode 5: IETF FR mode 6: Cisco MLFR UNI/NNI mode 7: Cisco MLFR End2End mode 8: Cisco FR mode other: Reserve"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPwTableEncAct(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 20
        
            def name(self):
                return "RxPwTableEncAct"
            
            def description(self):
                return "ENC header action 0: Frame relay routed frame NLPID L2 1: Frame relay routed frame SNAP L2 2: Frame relay bridge frame with LAN FCS option 3: Frame relay bridge frame without LAN FCS 4: Frame relay routed frame NLPID L3 5: Frame relay routed frame SNAP L3 other: Reserve"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPwTablePsnAct(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "RxPwTablePsnAct"
            
            def description(self):
                return "PSN action 0: Keep PSN (Remove DA,SA,VLAN tag only, keep ETH_TYPE) 1: Remove PSN in case CES/CEP/iMS-PW (IPv4oMPLS/IPv6oMPLS/ETHoMPLS)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPwTableFrgAct(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 16
        
            def name(self):
                return "RxPwTableFrgAct"
            
            def description(self):
                return "Fragment action for MLPPP/MLFR 0: Not fragment 1: Fragment 128 bytes 2: Fragment 256 bytes 3: Fragment 512 bytes 4: Reserve"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPwTableSerType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "RxPwTableSerType"
            
            def description(self):
                return "Service type 0: Discard 1: CES 2: EoP 3: CEP 4: HDLC 5: ETH Pass through 6: cHDLC 7: PPP Bridged 8: PPP OAM 9: PPP data 10: MLPPP OAM 11: MLPPP data 12: FR OAM 13: FR data 14: MLFR OAM 15: MLFR data"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPwTableSerid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwTableSerid"
            
            def description(self):
                return "Service ID (VCG/Link/PW/Bundle)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwTableLanFcsRmv"] = _AF6CCI0012_RD_CLA._cla_per_flow_table._RxPwTableLanFcsRmv()
            allFields["RxPwTableQ922len"] = _AF6CCI0012_RD_CLA._cla_per_flow_table._RxPwTableQ922len()
            allFields["RxPwTableQ922Field"] = _AF6CCI0012_RD_CLA._cla_per_flow_table._RxPwTableQ922Field()
            allFields["RxPwTableSubSerType"] = _AF6CCI0012_RD_CLA._cla_per_flow_table._RxPwTableSubSerType()
            allFields["RxPwTableTdmAct"] = _AF6CCI0012_RD_CLA._cla_per_flow_table._RxPwTableTdmAct()
            allFields["RxPwTableEncAct"] = _AF6CCI0012_RD_CLA._cla_per_flow_table._RxPwTableEncAct()
            allFields["RxPwTablePsnAct"] = _AF6CCI0012_RD_CLA._cla_per_flow_table._RxPwTablePsnAct()
            allFields["RxPwTableFrgAct"] = _AF6CCI0012_RD_CLA._cla_per_flow_table._RxPwTableFrgAct()
            allFields["RxPwTableSerType"] = _AF6CCI0012_RD_CLA._cla_per_flow_table._RxPwTableSerType()
            allFields["RxPwTableSerid"] = _AF6CCI0012_RD_CLA._cla_per_flow_table._RxPwTableSerid()
            return allFields

    class _cla_hbce_hash_table(AtRegister.AtRegister):
        def name(self):
            return "Classify HBCE Hashing Table Control"
    
        def description(self):
            return "HBCE module uses 13 bits for tab-index. Tab-index is generated by hashing function applied to the original id. %% HDL_PATH     :iaf6msrtlcla_hcbe.mem_p0hshtabinf.ram.ram[$HashID] Hashing function applies an XOR function to all bits of tab-index. %% The indexes to the tab-index are generated by hashing function and therefore collisions may occur. %% There are maximum fours (4) entries for every hash to identify flow traffic whether match or not. %% If the collisions are more than fours (4), they are handled by pointer to link another memory. %% The formula of hash pattern is {label ID(20bits), PSN mode (2bits)}, call HashPattern (22bits)%% #The HashID formula has two case depend on CLAHbceCodingSelectedMode%% #CLAHbceCodingSelectedMode = 1: HashID = HashPattern[13:0] XOR {4'd0,HashPattern[23:14]}, CLAHbceStoreID = HashPattern[23:14]%% #CLAHbceCodingSelectedMode = 0: HashID = HashPattern[13:0], CLAHbceStoreID = HashPattern[23:14] HashID = HashPattern[12:0] XOR {4'd0,HashPattern[21:13]}, CLAHbceStoreID = HashPattern[21:13]"
            
        def width(self):
            return 12
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00020000 + HashID"
            
        def startAddress(self):
            return 0x00020000
            
        def endAddress(self):
            return 0x00021fff

        class _CLAHbceLink(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "CLAHbceLink"
            
            def description(self):
                return "this is pointer to link extra location for conflict hash more than fours 1: Link to another memory 0: not link more"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHbceMemoryExtraStartPointer(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAHbceMemoryExtraStartPointer"
            
            def description(self):
                return "this is a MemExtraPtr to read the Classify HBCE Looking Up Information Extra Control in case the number of collisions are more than 4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAHbceLink"] = _AF6CCI0012_RD_CLA._cla_hbce_hash_table._CLAHbceLink()
            allFields["CLAHbceMemoryExtraStartPointer"] = _AF6CCI0012_RD_CLA._cla_hbce_hash_table._CLAHbceMemoryExtraStartPointer()
            return allFields

    class _cla_hbce_lkup_info(AtRegister.AtRegister):
        def name(self):
            return "Classify HBCE Looking Up Information Control"
    
        def description(self):
            return "This memory contain 4 entries (collisions) to examine one Pseudowire label whether match or not.%% HDL_PATH: begin: IF($CollisHashID==0) iaf6msrtlcla_hcbe.mem_memlk1.ram.ram[$HashID] ELSEIF($CollisHashID==1) iaf6msrtlcla_hcbe.mem_memlk2.ram.ram[$HashID] ELSEIF($CollisHashID==2) iaf6msrtlcla_hcbe.mem_memlk3.ram.ram[$HashID] ELSE iaf6msrtlcla_hcbe.mem_memlk4.ram.ram[$HashID] HDL_PATH: end: In general, One hashing(HashID) contain 4 entries (collisions). If the collisions are over 4, it will jump to the another extra memory."
            
        def width(self):
            return 35
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00080000 + CollisHashID*0x10000 + HashID"
            
        def startAddress(self):
            return 0x00080000
            
        def endAddress(self):
            return 0x00081fff

        class _CLAHbceFlowDirect(AtRegister.AtRegisterField):
            def stopBit(self):
                return 34
                
            def startBit(self):
                return 34
        
            def name(self):
                return "CLAHbceFlowDirect"
            
            def description(self):
                return "this configure FlowID working in normal mode (not in any group)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHbceGrpWorking(AtRegister.AtRegisterField):
            def stopBit(self):
                return 33
                
            def startBit(self):
                return 33
        
            def name(self):
                return "CLAHbceGrpWorking"
            
            def description(self):
                return "this configure group working or protection"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHbceGrpIDFlow(AtRegister.AtRegisterField):
            def stopBit(self):
                return 32
                
            def startBit(self):
                return 22
        
            def name(self):
                return "CLAHbceGrpIDFlow"
            
            def description(self):
                return "this configure a group ID that FlowID following"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHbceFlowID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 10
        
            def name(self):
                return "CLAHbceFlowID"
            
            def description(self):
                return "This is PW ID identification"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHbceFlowEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "CLAHbceFlowEnb"
            
            def description(self):
                return "The flow is identified in this table, it mean the traffic is identified 1: Flow identified 0: Flow look up fail"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHbceStoreID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAHbceStoreID"
            
            def description(self):
                return "this is saving some additional information to identify a certain lookup address rule within a particular table entry HBCE and also to be able to distinguish those that collide"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAHbceFlowDirect"] = _AF6CCI0012_RD_CLA._cla_hbce_lkup_info._CLAHbceFlowDirect()
            allFields["CLAHbceGrpWorking"] = _AF6CCI0012_RD_CLA._cla_hbce_lkup_info._CLAHbceGrpWorking()
            allFields["CLAHbceGrpIDFlow"] = _AF6CCI0012_RD_CLA._cla_hbce_lkup_info._CLAHbceGrpIDFlow()
            allFields["CLAHbceFlowID"] = _AF6CCI0012_RD_CLA._cla_hbce_lkup_info._CLAHbceFlowID()
            allFields["CLAHbceFlowEnb"] = _AF6CCI0012_RD_CLA._cla_hbce_lkup_info._CLAHbceFlowEnb()
            allFields["CLAHbceStoreID"] = _AF6CCI0012_RD_CLA._cla_hbce_lkup_info._CLAHbceStoreID()
            return allFields

    class _cla_per_grp_enb(AtRegister.AtRegister):
        def name(self):
            return "Classify Per Group Enable Control"
    
        def description(self):
            return "This register configures Group that Flowid (or Pseudowire ID) is enable or not HDL_PATH     :iaf6msrtlcla_hcbe.irampwgrp.ram.ram[$Grp_ID]"
            
        def width(self):
            return 1
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00070000 + Working_ID*0x1000 + Grp_ID"
            
        def startAddress(self):
            return 0x00070000
            
        def endAddress(self):
            return 0x00070fff

        class _CLAGrpPWEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAGrpPWEn"
            
            def description(self):
                return "This indicate the FlowID (or Pseudowire ID) is enable or not 1: FlowID enable 0: FlowID disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAGrpPWEn"] = _AF6CCI0012_RD_CLA._cla_per_grp_enb._CLAGrpPWEn()
            return allFields

    class _cla_vlan_lk(AtRegister.AtRegister):
        def name(self):
            return "Classify Vlan Lookup Control"
    
        def description(self):
            return "This register configures VLAN Lookup HDL_PATH     :iaf6msrtlcla_hcbe.iramvlanmod.ram.ram[$VLAN_ID]"
            
        def width(self):
            return 27
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00050000 + VLAN_ID"
            
        def startAddress(self):
            return 0x00050000
            
        def endAddress(self):
            return 0x00050fff

        class _CLAVlanLabelEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "CLAVlanLabelEn"
            
            def description(self):
                return "This configuration to select between VLAN lookup and Label lookup 0: Vlan lookup to flow ID 1: Label lookup to flow ID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAVlanEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "CLAVlanEn"
            
            def description(self):
                return "This indicate the VLAN ID enable or not 0: disable 1: enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAVlanFlowDirect(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "CLAVlanFlowDirect"
            
            def description(self):
                return "this configure FlowID working in normal mode (not in any group)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAVlanGrpWorking(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "CLAVlanGrpWorking"
            
            def description(self):
                return "this configure group working or protection"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAVlanGrpIDFlow(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 12
        
            def name(self):
                return "CLAVlanGrpIDFlow"
            
            def description(self):
                return "this configure a group ID that FlowID following"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAVlanFlowID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAVlanFlowID"
            
            def description(self):
                return "This indicate the FlowID base on VLAN ID, maximum is 4096 flow supported"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAVlanLabelEn"] = _AF6CCI0012_RD_CLA._cla_vlan_lk._CLAVlanLabelEn()
            allFields["CLAVlanEn"] = _AF6CCI0012_RD_CLA._cla_vlan_lk._CLAVlanEn()
            allFields["CLAVlanFlowDirect"] = _AF6CCI0012_RD_CLA._cla_vlan_lk._CLAVlanFlowDirect()
            allFields["CLAVlanGrpWorking"] = _AF6CCI0012_RD_CLA._cla_vlan_lk._CLAVlanGrpWorking()
            allFields["CLAVlanGrpIDFlow"] = _AF6CCI0012_RD_CLA._cla_vlan_lk._CLAVlanGrpIDFlow()
            allFields["CLAVlanFlowID"] = _AF6CCI0012_RD_CLA._cla_vlan_lk._CLAVlanFlowID()
            return allFields

    class _cla_per_eth_enb(AtRegister.AtRegister):
        def name(self):
            return "Classify Per Ethernet port Enable Control"
    
        def description(self):
            return "This register configures specific Ethernet port is enable or not"
            
        def width(self):
            return 29
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000d0000
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthPktMax(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 15
        
            def name(self):
                return "CLAEthPktMax"
            
            def description(self):
                return "This indicate Ethernet packet maximize 1: enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAEthPktMin(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 1
        
            def name(self):
                return "CLAEthPktMin"
            
            def description(self):
                return "This indicate Ethernet packet minimize 1: enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAEthPort1En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthPort1En"
            
            def description(self):
                return "This indicate Ethernet port 1 is enable or not 1: enable 0: disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthPktMax"] = _AF6CCI0012_RD_CLA._cla_per_eth_enb._CLAEthPktMax()
            allFields["CLAEthPktMin"] = _AF6CCI0012_RD_CLA._cla_per_eth_enb._CLAEthPktMin()
            allFields["CLAEthPort1En"] = _AF6CCI0012_RD_CLA._cla_per_eth_enb._CLAEthPort1En()
            return allFields

    class _Eth_cnt0_64_ro(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count0_64 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 0 to 64 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000D1000 + eth_port"
            
        def startAddress(self):
            return 0x000d1000
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCnt0_64(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCnt0_64"
            
            def description(self):
                return "This is statistic counter for the packet having 0 to 64 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCnt0_64"] = _AF6CCI0012_RD_CLA._Eth_cnt0_64_ro._CLAEthCnt0_64()
            return allFields

    class _Eth_cnt0_64_rc(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count0_64 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 0 to 64 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000D1800 + eth_port"
            
        def startAddress(self):
            return 0x000d1800
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCnt0_64(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCnt0_64"
            
            def description(self):
                return "This is statistic counter for the packet having 0 to 64 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCnt0_64"] = _AF6CCI0012_RD_CLA._Eth_cnt0_64_rc._CLAEthCnt0_64()
            return allFields

    class _Eth_cnt65_127_ro(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count65_127 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 65 to 127 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000D1004 + eth_port"
            
        def startAddress(self):
            return 0x000d1004
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCnt65_127(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCnt65_127"
            
            def description(self):
                return "This is statistic counter for the packet having 65 to 127 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCnt65_127"] = _AF6CCI0012_RD_CLA._Eth_cnt65_127_ro._CLAEthCnt65_127()
            return allFields

    class _Eth_cnt65_127_rc(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count65_127 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 65 to 127 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000D1804 + eth_port"
            
        def startAddress(self):
            return 0x000d1804
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCnt65_127(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCnt65_127"
            
            def description(self):
                return "This is statistic counter for the packet having 65 to 127 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCnt65_127"] = _AF6CCI0012_RD_CLA._Eth_cnt65_127_rc._CLAEthCnt65_127()
            return allFields

    class _Eth_cnt128_255_ro(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count128_255 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 128 to 255 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000D1008 + eth_port"
            
        def startAddress(self):
            return 0x000d1008
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCnt128_255(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCnt128_255"
            
            def description(self):
                return "This is statistic counter for the packet having 128 to 255 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCnt128_255"] = _AF6CCI0012_RD_CLA._Eth_cnt128_255_ro._CLAEthCnt128_255()
            return allFields

    class _Eth_cnt128_255_rc(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count128_255 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 128 to 255 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000D1808 + eth_port"
            
        def startAddress(self):
            return 0x000d1808
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCnt128_255(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCnt128_255"
            
            def description(self):
                return "This is statistic counter for the packet having 128 to 255 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCnt128_255"] = _AF6CCI0012_RD_CLA._Eth_cnt128_255_rc._CLAEthCnt128_255()
            return allFields

    class _Eth_cnt256_511_ro(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count256_511 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 256 to 511 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000D100C + eth_port"
            
        def startAddress(self):
            return 0x000d100c
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCnt256_511(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCnt256_511"
            
            def description(self):
                return "This is statistic counter for the packet having 256 to 511 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCnt256_511"] = _AF6CCI0012_RD_CLA._Eth_cnt256_511_ro._CLAEthCnt256_511()
            return allFields

    class _Eth_cnt256_511_rc(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count256_511 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 256 to 511 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000D180C + eth_port"
            
        def startAddress(self):
            return 0x000d180c
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCnt256_511(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCnt256_511"
            
            def description(self):
                return "This is statistic counter for the packet having 256 to 511 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCnt256_511"] = _AF6CCI0012_RD_CLA._Eth_cnt256_511_rc._CLAEthCnt256_511()
            return allFields

    class _Eth_cnt512_1024_ro(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count512_1024 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 512 to 1023 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000D1010 + eth_port"
            
        def startAddress(self):
            return 0x000d1010
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCnt512_1024(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCnt512_1024"
            
            def description(self):
                return "This is statistic counter for the packet having 512 to 1023 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCnt512_1024"] = _AF6CCI0012_RD_CLA._Eth_cnt512_1024_ro._CLAEthCnt512_1024()
            return allFields

    class _Eth_cnt512_1024_rc(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count512_1024 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 512 to 1023 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000D1810 + eth_port"
            
        def startAddress(self):
            return 0x000d1810
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCnt512_1024(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCnt512_1024"
            
            def description(self):
                return "This is statistic counter for the packet having 512 to 1023 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCnt512_1024"] = _AF6CCI0012_RD_CLA._Eth_cnt512_1024_rc._CLAEthCnt512_1024()
            return allFields

    class _Eth_cnt1025_1528_ro(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count1025_1528 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 1024 to 1518 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000D1014 + eth_port"
            
        def startAddress(self):
            return 0x000d1014
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCnt1025_1528(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCnt1025_1528"
            
            def description(self):
                return "This is statistic counter for the packet having 1024 to 1518 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCnt1025_1528"] = _AF6CCI0012_RD_CLA._Eth_cnt1025_1528_ro._CLAEthCnt1025_1528()
            return allFields

    class _Eth_cnt1025_1528_rc(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count1025_1528 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 1024 to 1518 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000D1814 + eth_port"
            
        def startAddress(self):
            return 0x000d1814
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCnt1025_1528(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCnt1025_1528"
            
            def description(self):
                return "This is statistic counter for the packet having 1024 to 1518 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCnt1025_1528"] = _AF6CCI0012_RD_CLA._Eth_cnt1025_1528_rc._CLAEthCnt1025_1528()
            return allFields

    class _Eth_cnt1529_2047_ro(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count1529_2047 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 1519 to 2047 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000D1018 + eth_port"
            
        def startAddress(self):
            return 0x000d1018
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCnt1529_2047(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCnt1529_2047"
            
            def description(self):
                return "This is statistic counter for the packet having 1519 to 2047 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCnt1529_2047"] = _AF6CCI0012_RD_CLA._Eth_cnt1529_2047_ro._CLAEthCnt1529_2047()
            return allFields

    class _Eth_cnt1529_2047_rc(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count1529_2047 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 1519 to 2047 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000D1818 + eth_port"
            
        def startAddress(self):
            return 0x000d1818
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCnt1529_2047(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCnt1529_2047"
            
            def description(self):
                return "This is statistic counter for the packet having 1519 to 2047 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCnt1529_2047"] = _AF6CCI0012_RD_CLA._Eth_cnt1529_2047_rc._CLAEthCnt1529_2047()
            return allFields

    class _Eth_cnt_jumbo_ro(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count Jumbo packet"
    
        def description(self):
            return "This register is statistic counter for the packet having more than 2048 bytes (jumbo)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000D101C + eth_port"
            
        def startAddress(self):
            return 0x000d101c
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCntJumbo(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCntJumbo"
            
            def description(self):
                return "This is statistic counter for the packet more than 2048 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCntJumbo"] = _AF6CCI0012_RD_CLA._Eth_cnt_jumbo_ro._CLAEthCntJumbo()
            return allFields

    class _Eth_cnt_jumbo_rc(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count Jumbo packet"
    
        def description(self):
            return "This register is statistic counter for the packet having more than 2048 bytes (jumbo)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000D181C + eth_port"
            
        def startAddress(self):
            return 0x000d181c
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCntJumbo(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCntJumbo"
            
            def description(self):
                return "This is statistic counter for the packet more than 2048 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCntJumbo"] = _AF6CCI0012_RD_CLA._Eth_cnt_jumbo_rc._CLAEthCntJumbo()
            return allFields

    class _Eth_cnt_Unicast_ro(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count Unicast packet"
    
        def description(self):
            return "This register is statistic counter for the unicast packet"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000D1020 + eth_port"
            
        def startAddress(self):
            return 0x000d1020
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCntUnicast(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCntUnicast"
            
            def description(self):
                return "This is statistic counter for the unicast packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCntUnicast"] = _AF6CCI0012_RD_CLA._Eth_cnt_Unicast_ro._CLAEthCntUnicast()
            return allFields

    class _Eth_cnt_Unicast_rc(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count Unicast packet"
    
        def description(self):
            return "This register is statistic counter for the unicast packet"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000D1820 + eth_port"
            
        def startAddress(self):
            return 0x000d1820
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCntUnicast(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCntUnicast"
            
            def description(self):
                return "This is statistic counter for the unicast packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCntUnicast"] = _AF6CCI0012_RD_CLA._Eth_cnt_Unicast_rc._CLAEthCntUnicast()
            return allFields

    class _rx_port_pkt_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count Total packet"
    
        def description(self):
            return "This register is statistic counter for the total packet at receive side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000D1024 + eth_port"
            
        def startAddress(self):
            return 0x000d1024
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCntTotal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCntTotal"
            
            def description(self):
                return "This is statistic counter total packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCntTotal"] = _AF6CCI0012_RD_CLA._rx_port_pkt_cnt_ro._CLAEthCntTotal()
            return allFields

    class _rx_port_pkt_cnt_rc(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count Total packet"
    
        def description(self):
            return "This register is statistic counter for the total packet at receive side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000D1824 + eth_port"
            
        def startAddress(self):
            return 0x000d1824
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCntTotal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCntTotal"
            
            def description(self):
                return "This is statistic counter total packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCntTotal"] = _AF6CCI0012_RD_CLA._rx_port_pkt_cnt_rc._CLAEthCntTotal()
            return allFields

    class _rx_port_bcast_pkt_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count Broadcast packet"
    
        def description(self):
            return "This register is statistic counter for the Broadcast packet at receive side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000D1028 + eth_port"
            
        def startAddress(self):
            return 0x000d1028
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCntBroadcast(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCntBroadcast"
            
            def description(self):
                return "This is statistic counter broadcast packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCntBroadcast"] = _AF6CCI0012_RD_CLA._rx_port_bcast_pkt_cnt_ro._CLAEthCntBroadcast()
            return allFields

    class _rx_port_bcast_pkt_cnt_rc(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count Broadcast packet"
    
        def description(self):
            return "This register is statistic counter for the Broadcast packet at receive side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000D1828 + eth_port"
            
        def startAddress(self):
            return 0x000d1828
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCntBroadcast(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCntBroadcast"
            
            def description(self):
                return "This is statistic counter broadcast packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCntBroadcast"] = _AF6CCI0012_RD_CLA._rx_port_bcast_pkt_cnt_rc._CLAEthCntBroadcast()
            return allFields

    class _rx_port_mcast_pkt_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count Multicast packet"
    
        def description(self):
            return "This register is statistic counter for the Multicast packet at receive side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000D102C + eth_port"
            
        def startAddress(self):
            return 0x000d102c
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCntMulticast(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCntMulticast"
            
            def description(self):
                return "This is statistic counter multicast packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCntMulticast"] = _AF6CCI0012_RD_CLA._rx_port_mcast_pkt_cnt_ro._CLAEthCntMulticast()
            return allFields

    class _rx_port_mcast_pkt_cnt_rc(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count Multicast packet"
    
        def description(self):
            return "This register is statistic counter for the Multicast packet at receive side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000D182C + eth_port"
            
        def startAddress(self):
            return 0x000d182c
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCntMulticast(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCntMulticast"
            
            def description(self):
                return "This is statistic counter multicast packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCntMulticast"] = _AF6CCI0012_RD_CLA._rx_port_mcast_pkt_cnt_rc._CLAEthCntMulticast()
            return allFields

    class _rx_port_under_size_pkt_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count Under size packet"
    
        def description(self):
            return "This register is statistic counter for the under size packet at receive side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000D1030 + eth_port"
            
        def startAddress(self):
            return 0x000d1030
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCntUnderSize(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCntUnderSize"
            
            def description(self):
                return "This is statistic counter under size packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCntUnderSize"] = _AF6CCI0012_RD_CLA._rx_port_under_size_pkt_cnt_ro._CLAEthCntUnderSize()
            return allFields

    class _rx_port_under_size_pkt_cnt_rc(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count Under size packet"
    
        def description(self):
            return "This register is statistic counter for the under size packet at receive side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000D1830 + eth_port"
            
        def startAddress(self):
            return 0x000d1830
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCntUnderSize(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCntUnderSize"
            
            def description(self):
                return "This is statistic counter under size packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCntUnderSize"] = _AF6CCI0012_RD_CLA._rx_port_under_size_pkt_cnt_rc._CLAEthCntUnderSize()
            return allFields

    class _rx_port_over_size_pkt_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count Over size packet"
    
        def description(self):
            return "This register is statistic counter for the over size packet at receive side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000D1034 + eth_port"
            
        def startAddress(self):
            return 0x000d1034
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCntOverSize(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCntOverSize"
            
            def description(self):
                return "This is statistic counter over size packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCntOverSize"] = _AF6CCI0012_RD_CLA._rx_port_over_size_pkt_cnt_ro._CLAEthCntOverSize()
            return allFields

    class _rx_port_over_size_pkt_cnt_rc(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count Over size packet"
    
        def description(self):
            return "This register is statistic counter for the over size packet at receive side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000D1834 + eth_port"
            
        def startAddress(self):
            return 0x000d1834
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCntOverSize(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCntOverSize"
            
            def description(self):
                return "This is statistic counter over size packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCntOverSize"] = _AF6CCI0012_RD_CLA._rx_port_over_size_pkt_cnt_rc._CLAEthCntOverSize()
            return allFields

    class _Eth_cnt_phy_err_ro(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count FCS error packet"
    
        def description(self):
            return "This register is statistic count FCS error packet at receive side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000D1038 + eth_port"
            
        def startAddress(self):
            return 0x000d1038
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCntPhyErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCntPhyErr"
            
            def description(self):
                return "This is statistic counter FCS error packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCntPhyErr"] = _AF6CCI0012_RD_CLA._Eth_cnt_phy_err_ro._CLAEthCntPhyErr()
            return allFields

    class _Eth_cnt_phy_err_rc(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count FCS error packet"
    
        def description(self):
            return "This register is statistic count FCS error packet at receive side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000D1838 + eth_port"
            
        def startAddress(self):
            return 0x000d1838
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCntPhyErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCntPhyErr"
            
            def description(self):
                return "This is statistic counter FCS error packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCntPhyErr"] = _AF6CCI0012_RD_CLA._Eth_cnt_phy_err_rc._CLAEthCntPhyErr()
            return allFields

    class _Eth_cnt_byte_ro(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count number of bytes of packet"
    
        def description(self):
            return "This register is statistic count number of bytes of packet at receive side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000D103C + eth_port"
            
        def startAddress(self):
            return 0x000d103c
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCntByte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCntByte"
            
            def description(self):
                return "This is statistic counter number of bytes of packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCntByte"] = _AF6CCI0012_RD_CLA._Eth_cnt_byte_ro._CLAEthCntByte()
            return allFields

    class _Eth_cnt_byte_rc(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count number of bytes of packet"
    
        def description(self):
            return "This register is statistic count number of bytes of packet at receive side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000D183C + eth_port"
            
        def startAddress(self):
            return 0x000d183c
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCntByte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCntByte"
            
            def description(self):
                return "This is statistic counter number of bytes of packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCntByte"] = _AF6CCI0012_RD_CLA._Eth_cnt_byte_rc._CLAEthCntByte()
            return allFields

    class _cla_hbce_lkup_info_extra(AtRegister.AtRegister):
        def name(self):
            return "Classify HBCE Looking Up Information Control2"
    
        def description(self):
            return "This memory contain maximum 8 entries extra to examine one Pseudowire label whether match or not"
            
        def width(self):
            return 35
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0040000 + CollisHashIDExtra*0x1000  + MemExtraPtr"
            
        def startAddress(self):
            return 0x00040000
            
        def endAddress(self):
            return 0x000477ff

        class _CLAExtraHbceFlowDirect(AtRegister.AtRegisterField):
            def stopBit(self):
                return 34
                
            def startBit(self):
                return 34
        
            def name(self):
                return "CLAExtraHbceFlowDirect"
            
            def description(self):
                return "this configure FlowID working in normal mode (not in any group)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAExtraHbceGrpWorking(AtRegister.AtRegisterField):
            def stopBit(self):
                return 33
                
            def startBit(self):
                return 33
        
            def name(self):
                return "CLAExtraHbceGrpWorking"
            
            def description(self):
                return "this configure group working or protection"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAExtraHbceGrpIDFlow(AtRegister.AtRegisterField):
            def stopBit(self):
                return 32
                
            def startBit(self):
                return 22
        
            def name(self):
                return "CLAExtraHbceGrpIDFlow"
            
            def description(self):
                return "this configure a group ID that FlowID following"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAExtraHbceFlowID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 10
        
            def name(self):
                return "CLAExtraHbceFlowID"
            
            def description(self):
                return "This is PW ID identification"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAExtraHbceFlowEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "CLAExtraHbceFlowEnb"
            
            def description(self):
                return "The flow is identified in this table, it mean the traffic is identified 1: Flow identified 0: Flow look up fail"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAExtraHbceStoreID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAExtraHbceStoreID"
            
            def description(self):
                return "this is saving some additional information to identify a certain lookup address rule within a particular table entry HBCE and also to be able to distinguish those that collide"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAExtraHbceFlowDirect"] = _AF6CCI0012_RD_CLA._cla_hbce_lkup_info_extra._CLAExtraHbceFlowDirect()
            allFields["CLAExtraHbceGrpWorking"] = _AF6CCI0012_RD_CLA._cla_hbce_lkup_info_extra._CLAExtraHbceGrpWorking()
            allFields["CLAExtraHbceGrpIDFlow"] = _AF6CCI0012_RD_CLA._cla_hbce_lkup_info_extra._CLAExtraHbceGrpIDFlow()
            allFields["CLAExtraHbceFlowID"] = _AF6CCI0012_RD_CLA._cla_hbce_lkup_info_extra._CLAExtraHbceFlowID()
            allFields["CLAExtraHbceFlowEnb"] = _AF6CCI0012_RD_CLA._cla_hbce_lkup_info_extra._CLAExtraHbceFlowEnb()
            allFields["CLAExtraHbceStoreID"] = _AF6CCI0012_RD_CLA._cla_hbce_lkup_info_extra._CLAExtraHbceStoreID()
            return allFields

    class _cla_2_cdr_cfg(AtRegister.AtRegister):
        def name(self):
            return "Classify Per Pseudowire Slice to CDR Control"
    
        def description(self):
            return "This register configures Slice ID to CDR"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00C0000 +  PWID"
            
        def startAddress(self):
            return 0x000c0000
            
        def endAddress(self):
            return 0x000c0fff

        class _PwCdrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "PwCdrEn"
            
            def description(self):
                return "Indicate Pseudowire enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwHoEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "PwHoEn"
            
            def description(self):
                return "Indicate 8x Hi order OC48 enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwHoLoOc48Id(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 11
        
            def name(self):
                return "PwHoLoOc48Id"
            
            def description(self):
                return "Indicate 8x Hi order OC48 or 6x Low order OC48 slice"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwLoSlice24Sel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "PwLoSlice24Sel"
            
            def description(self):
                return "Pseudo-wire Low order OC24 slice selection, only valid in Lo Pseudo-wire 1: This PW belong to Slice24 that trasnport STS 1,3,5,...,47 within a Lo OC48 0: This PW belong to Slice24 that trasnport STS 0,2,4,...,46 within a Lo OC48"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwTdmLineId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PwTdmLineId"
            
            def description(self):
                return "Pseudo-wire(PW) corresponding TDM line ID. If the PW belong to Low order path, this is the OC24 TDM line ID that is using in Lo CDR,PDH and MAP. If the PW belong to Hi order CEP path, this is the OC48 master STS ID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PwCdrEn"] = _AF6CCI0012_RD_CLA._cla_2_cdr_cfg._PwCdrEn()
            allFields["PwHoEn"] = _AF6CCI0012_RD_CLA._cla_2_cdr_cfg._PwHoEn()
            allFields["PwHoLoOc48Id"] = _AF6CCI0012_RD_CLA._cla_2_cdr_cfg._PwHoLoOc48Id()
            allFields["PwLoSlice24Sel"] = _AF6CCI0012_RD_CLA._cla_2_cdr_cfg._PwLoSlice24Sel()
            allFields["PwTdmLineId"] = _AF6CCI0012_RD_CLA._cla_2_cdr_cfg._PwTdmLineId()
            return allFields

    class _cla_hold_status(AtRegister.AtRegister):
        def name(self):
            return "Classify Hold Register Status"
    
        def description(self):
            return "This register using for hold remain that more than 128bits"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00000A +  HID"
            
        def startAddress(self):
            return 0x0000000a
            
        def endAddress(self):
            return 0x0000000c

        class _PwHoldStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PwHoldStatus"
            
            def description(self):
                return "Hold 32bits"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PwHoldStatus"] = _AF6CCI0012_RD_CLA._cla_hold_status._PwHoldStatus()
            return allFields

    class _rdha3_0_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit3_0 Control"
    
        def description(self):
            return "This register is used to send HA read address bit3_0 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xE0000 + HaAddr3_0"
            
        def startAddress(self):
            return 0x000e0000
            
        def endAddress(self):
            return 0x000e000f

        class _ReadAddr3_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr3_0"
            
            def description(self):
                return "Read value will be 0xE00000 plus HaAddr3_0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr3_0"] = _AF6CCI0012_RD_CLA._rdha3_0_control._ReadAddr3_0()
            return allFields

    class _rdha7_4_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit7_4 Control"
    
        def description(self):
            return "This register is used to send HA read address bit7_4 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xE0010 + HaAddr7_4"
            
        def startAddress(self):
            return 0x000e0010
            
        def endAddress(self):
            return 0x000e001f

        class _ReadAddr7_4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr7_4"
            
            def description(self):
                return "Read value will be 0xE00000 plus HaAddr7_4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr7_4"] = _AF6CCI0012_RD_CLA._rdha7_4_control._ReadAddr7_4()
            return allFields

    class _rdha11_8_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit11_8 Control"
    
        def description(self):
            return "This register is used to send HA read address bit11_8 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xE0020 + HaAddr11_8"
            
        def startAddress(self):
            return 0x000e0020
            
        def endAddress(self):
            return 0x000e002f

        class _ReadAddr11_8(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr11_8"
            
            def description(self):
                return "Read value will be 0xE00000 plus HaAddr11_8"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr11_8"] = _AF6CCI0012_RD_CLA._rdha11_8_control._ReadAddr11_8()
            return allFields

    class _rdha15_12_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit15_12 Control"
    
        def description(self):
            return "This register is used to send HA read address bit15_12 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xE0030 + HaAddr15_12"
            
        def startAddress(self):
            return 0x000e0030
            
        def endAddress(self):
            return 0x000e003f

        class _ReadAddr15_12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr15_12"
            
            def description(self):
                return "Read value will be 0xE00000 plus HaAddr15_12"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr15_12"] = _AF6CCI0012_RD_CLA._rdha15_12_control._ReadAddr15_12()
            return allFields

    class _rdha19_16_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit19_16 Control"
    
        def description(self):
            return "This register is used to send HA read address bit19_16 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xE0040 + HaAddr19_16"
            
        def startAddress(self):
            return 0x000e0040
            
        def endAddress(self):
            return 0x000e004f

        class _ReadAddr19_16(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr19_16"
            
            def description(self):
                return "Read value will be 0xE00000 plus HaAddr19_16"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr19_16"] = _AF6CCI0012_RD_CLA._rdha19_16_control._ReadAddr19_16()
            return allFields

    class _rdha23_20_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit23_20 Control"
    
        def description(self):
            return "This register is used to send HA read address bit23_20 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xE0050 + HaAddr23_20"
            
        def startAddress(self):
            return 0x000e0050
            
        def endAddress(self):
            return 0x000e005f

        class _ReadAddr23_20(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr23_20"
            
            def description(self):
                return "Read value will be 0xE00000 plus HaAddr23_20"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr23_20"] = _AF6CCI0012_RD_CLA._rdha23_20_control._ReadAddr23_20()
            return allFields

    class _rdha24data_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit24 and Data Control"
    
        def description(self):
            return "This register is used to send HA read address bit24 to HA engine to read data"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xE0060 + HaAddr24"
            
        def startAddress(self):
            return 0x000e0060
            
        def endAddress(self):
            return 0x000e0061

        class _ReadHaData31_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData31_0"
            
            def description(self):
                return "HA read data bit31_0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData31_0"] = _AF6CCI0012_RD_CLA._rdha24data_control._ReadHaData31_0()
            return allFields

    class _rdha_hold63_32(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data63_32"
    
        def description(self):
            return "This register is used to read HA dword2 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e0070
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData63_32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData63_32"
            
            def description(self):
                return "HA read data bit63_32"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData63_32"] = _AF6CCI0012_RD_CLA._rdha_hold63_32._ReadHaData63_32()
            return allFields

    class _rdindr_hold95_64(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data95_64"
    
        def description(self):
            return "This register is used to read HA dword3 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e0071
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData95_64(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData95_64"
            
            def description(self):
                return "HA read data bit95_64"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData95_64"] = _AF6CCI0012_RD_CLA._rdindr_hold95_64._ReadHaData95_64()
            return allFields

    class _rdindr_hold127_96(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data127_96"
    
        def description(self):
            return "This register is used to read HA dword4 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e0072
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData127_96(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData127_96"
            
            def description(self):
                return "HA read data bit127_96"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData127_96"] = _AF6CCI0012_RD_CLA._rdindr_hold127_96._ReadHaData127_96()
            return allFields

    class _cla_Parity_control(AtRegister.AtRegister):
        def name(self):
            return "Classify Parity Register Control"
    
        def description(self):
            return "This register using for Force Parity"
            
        def width(self):
            return 18
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000f8000
            
        def endAddress(self):
            return 0xffffffff

        class _ClaForceErr8(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "ClaForceErr8"
            
            def description(self):
                return "Enable parity error force for \"Classify Per Pseudowire Table Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ClaForceErr7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ClaForceErr7"
            
            def description(self):
                return "Enable parity error force for \"Classify Per Pseudowire Identification to CDR Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ClaForceErr6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "ClaForceErr6"
            
            def description(self):
                return "Enable parity error force for \"Classify HBCE Hashing Table Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ClaForceErr5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 7
        
            def name(self):
                return "ClaForceErr5"
            
            def description(self):
                return "Enable parity error force for \"Classify HBCE Looking Up Information Control2\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ClaForceErr4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "ClaForceErr4"
            
            def description(self):
                return "Enable parity error force for \"Classify Per Group Enable Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ClaForceErr3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "ClaForceErr3"
            
            def description(self):
                return "Enable parity error force for \"Classify Per Pseudowire Type Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ClaForceErr2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "ClaForceErr2"
            
            def description(self):
                return "Enable parity error force for \"Classify vlan Lookup Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ClaForceErr1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ClaForceErr1"
            
            def description(self):
                return "Enable parity error force for \"Classify HBCE Looking Up Information Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ClaForceErr8"] = _AF6CCI0012_RD_CLA._cla_Parity_control._ClaForceErr8()
            allFields["ClaForceErr7"] = _AF6CCI0012_RD_CLA._cla_Parity_control._ClaForceErr7()
            allFields["ClaForceErr6"] = _AF6CCI0012_RD_CLA._cla_Parity_control._ClaForceErr6()
            allFields["ClaForceErr5"] = _AF6CCI0012_RD_CLA._cla_Parity_control._ClaForceErr5()
            allFields["ClaForceErr4"] = _AF6CCI0012_RD_CLA._cla_Parity_control._ClaForceErr4()
            allFields["ClaForceErr3"] = _AF6CCI0012_RD_CLA._cla_Parity_control._ClaForceErr3()
            allFields["ClaForceErr2"] = _AF6CCI0012_RD_CLA._cla_Parity_control._ClaForceErr2()
            allFields["ClaForceErr1"] = _AF6CCI0012_RD_CLA._cla_Parity_control._ClaForceErr1()
            return allFields

    class _cla_Parity_Disable_control(AtRegister.AtRegister):
        def name(self):
            return "Classify Parity Disable register Control"
    
        def description(self):
            return "This register using for Disable Parity"
            
        def width(self):
            return 18
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000f8001
            
        def endAddress(self):
            return 0xffffffff

        class _ClaDisChkErr8(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "ClaDisChkErr8"
            
            def description(self):
                return "Disable parity error check for \"Classify Per Pseudowire Table Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ClaDisChkErr7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ClaDisChkErr7"
            
            def description(self):
                return "Disable parity error check for \"Classify Per Pseudowire Identification to CDR Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ClaDisChkErr6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "ClaDisChkErr6"
            
            def description(self):
                return "Disable parity error check for \"Classify HBCE Hashing Table Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ClaDisChkErr5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 7
        
            def name(self):
                return "ClaDisChkErr5"
            
            def description(self):
                return "Disable parity error check for \"Classify HBCE Looking Up Information Control2\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ClaDisChkErr4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "ClaDisChkErr4"
            
            def description(self):
                return "Disable parity error check for \"Classify Per Group Enable Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ClaDisChkErr3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "ClaDisChkErr3"
            
            def description(self):
                return "Disable parity error check for \"Classify Per Pseudowire Type Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ClaDisChkErr2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "ClaDisChkErr2"
            
            def description(self):
                return "Disable parity error check for \"Classify vlan Lookup Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ClaDisChkErr1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ClaDisChkErr1"
            
            def description(self):
                return "Disable parity error check for \"Classify HBCE Looking Up Information Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ClaDisChkErr8"] = _AF6CCI0012_RD_CLA._cla_Parity_Disable_control._ClaDisChkErr8()
            allFields["ClaDisChkErr7"] = _AF6CCI0012_RD_CLA._cla_Parity_Disable_control._ClaDisChkErr7()
            allFields["ClaDisChkErr6"] = _AF6CCI0012_RD_CLA._cla_Parity_Disable_control._ClaDisChkErr6()
            allFields["ClaDisChkErr5"] = _AF6CCI0012_RD_CLA._cla_Parity_Disable_control._ClaDisChkErr5()
            allFields["ClaDisChkErr4"] = _AF6CCI0012_RD_CLA._cla_Parity_Disable_control._ClaDisChkErr4()
            allFields["ClaDisChkErr3"] = _AF6CCI0012_RD_CLA._cla_Parity_Disable_control._ClaDisChkErr3()
            allFields["ClaDisChkErr2"] = _AF6CCI0012_RD_CLA._cla_Parity_Disable_control._ClaDisChkErr2()
            allFields["ClaDisChkErr1"] = _AF6CCI0012_RD_CLA._cla_Parity_Disable_control._ClaDisChkErr1()
            return allFields

    class _cla_Parity_stk_err(AtRegister.AtRegister):
        def name(self):
            return "Classify Parity sticky error"
    
        def description(self):
            return "This register using for checking sticky error"
            
        def width(self):
            return 18
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000f8002
            
        def endAddress(self):
            return 0xffffffff

        class _ClaParStkErr8(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "ClaParStkErr8"
            
            def description(self):
                return "Parity sticky error check for \"Classify Per Pseudowire Table Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ClaParStkErr7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ClaParStkErr7"
            
            def description(self):
                return "Parity sticky error check for \"Classify Per Pseudowire Identification to CDR Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ClaParStkErr6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "ClaParStkErr6"
            
            def description(self):
                return "Parity sticky error check for \"Classify HBCE Hashing Table Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ClaParStkErr5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 7
        
            def name(self):
                return "ClaParStkErr5"
            
            def description(self):
                return "Parity sticky error check for \"Classify HBCE Looking Up Information Control2\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ClaParStkErr4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "ClaParStkErr4"
            
            def description(self):
                return "Parity sticky error check for \"Classify Per Group Enable Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ClaParStkErr3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "ClaParStkErr3"
            
            def description(self):
                return "Parity sticky error check for \"Classify Per Pseudowire Type Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ClaParStkErr2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "ClaParStkErr2"
            
            def description(self):
                return "Parity sticky error check for \"Classify vlan Lookup Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ClaParStkErr1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ClaParStkErr1"
            
            def description(self):
                return "Parity sticky error check for \"Classify HBCE Looking Up Information Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ClaParStkErr8"] = _AF6CCI0012_RD_CLA._cla_Parity_stk_err._ClaParStkErr8()
            allFields["ClaParStkErr7"] = _AF6CCI0012_RD_CLA._cla_Parity_stk_err._ClaParStkErr7()
            allFields["ClaParStkErr6"] = _AF6CCI0012_RD_CLA._cla_Parity_stk_err._ClaParStkErr6()
            allFields["ClaParStkErr5"] = _AF6CCI0012_RD_CLA._cla_Parity_stk_err._ClaParStkErr5()
            allFields["ClaParStkErr4"] = _AF6CCI0012_RD_CLA._cla_Parity_stk_err._ClaParStkErr4()
            allFields["ClaParStkErr3"] = _AF6CCI0012_RD_CLA._cla_Parity_stk_err._ClaParStkErr3()
            allFields["ClaParStkErr2"] = _AF6CCI0012_RD_CLA._cla_Parity_stk_err._ClaParStkErr2()
            allFields["ClaParStkErr1"] = _AF6CCI0012_RD_CLA._cla_Parity_stk_err._ClaParStkErr1()
            return allFields

    class _cla_queue_lk(AtRegister.AtRegister):
        def name(self):
            return "Classify Queue Lookup Control"
    
        def description(self):
            return "This register configures Queue Lookup HDL_PATH     :iaf6msrtlcla_queue.cfgqueue_ram.ram.ram[$SER_ID*8 + $CoS]"
            
        def width(self):
            return 2
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000F0000 + SER_ID*0x8 + CoS"
            
        def startAddress(self):
            return 0x000f0000
            
        def endAddress(self):
            return 0x000f7fff

        class _CLAQueueID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAQueueID"
            
            def description(self):
                return "This indicate the Queue ID base on Service and QoS"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAQueueID"] = _AF6CCI0012_RD_CLA._cla_queue_lk._CLAQueueID()
            return allFields

    class _cla_tpch_table_service(AtRegister.AtRegister):
        def name(self):
            return "Classify PTCH Service Control"
    
        def description(self):
            return "This register configures service base on PTCH Port HDL_PATH     :iaf6msrtlcla_ptchpro.cfgptch_ser.ram.ram[$PTCH_Port]"
            
        def width(self):
            return 3
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000C000 + PTCH_Port"
            
        def startAddress(self):
            return 0x0000c000
            
        def endAddress(self):
            return 0x0000c0ff

        class _CLAPTCHService(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAPTCHService"
            
            def description(self):
                return "This indicate the service that the traffic following 0: CEM PW service (always use label to lookup flow id) 1: MS service (use label or vlanid to lookup to flow id) 2: eXAUI service (bypass) 3: EoS service (bypass) else: reserve"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAPTCHService"] = _AF6CCI0012_RD_CLA._cla_tpch_table_service._CLAPTCHService()
            return allFields

    class _cla_oam_link(AtRegister.AtRegister):
        def name(self):
            return "Classify OAM link Control"
    
        def description(self):
            return "This register configures link for OAM when packet carry VLAN control HDL_PATH     :iaf6msrtlcla_ptchpro.cfgvlan_oam.ram.ram[$Vlan_Ctrl]"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000D000 + Vlan_Ctrl"
            
        def startAddress(self):
            return 0x0000d000
            
        def endAddress(self):
            return 0x0000dfff

        class _CLAOamSrv(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "CLAOamSrv"
            
            def description(self):
                return "This indicate the link service of OAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAOamLink(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAOamLink"
            
            def description(self):
                return "This indicate the link of OAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAOamSrv"] = _AF6CCI0012_RD_CLA._cla_oam_link._CLAOamSrv()
            allFields["CLAOamLink"] = _AF6CCI0012_RD_CLA._cla_oam_link._CLAOamLink()
            return allFields

    class _cla_tpch_eos_channel(AtRegister.AtRegister):
        def name(self):
            return "Classify PTCH channel for EoS Service Control"
    
        def description(self):
            return "This register configures port number of EoS bypass service HDL_PATH     :iaf6msrtlcla_ptchpro.cfgvlan_eos.ram.ram[$First_VLAN]"
            
        def width(self):
            return 8
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000E000 + First_VLAN"
            
        def startAddress(self):
            return 0x0000e000
            
        def endAddress(self):
            return 0x0000efff

        class _CLAPTCHChlEoS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAPTCHChlEoS"
            
            def description(self):
                return "This indicate the channel for EoS bypass service"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAPTCHChlEoS"] = _AF6CCI0012_RD_CLA._cla_tpch_eos_channel._CLAPTCHChlEoS()
            return allFields

    class _Eth_cnt_psn_err_ro(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count PSN error packet"
    
        def description(self):
            return "This register is statistic count PSN error packet at receive side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000001
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCntPSNErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCntPSNErr"
            
            def description(self):
                return "This is statistic counter PSN error packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCntPSNErr"] = _AF6CCI0012_RD_CLA._Eth_cnt_psn_err_ro._CLAEthCntPSNErr()
            return allFields

    class _Eth_cnt_psn_err_rc(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet port Count PSN error packet"
    
        def description(self):
            return "This register is statistic count PSN error packet at receive side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000010
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCntPSNErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCntPSNErr"
            
            def description(self):
                return "This is statistic counter PSN error packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCntPSNErr"] = _AF6CCI0012_RD_CLA._Eth_cnt_psn_err_rc._CLAEthCntPSNErr()
            return allFields

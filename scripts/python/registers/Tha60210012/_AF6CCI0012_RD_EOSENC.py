import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0012_RD_EOSENC(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["upen_holdreg1"] = _AF6CCI0012_RD_EOSENC._upen_holdreg1()
        allRegisters["upen_egrext"] = _AF6CCI0012_RD_EOSENC._upen_egrext()
        allRegisters["upen_mapf"] = _AF6CCI0012_RD_EOSENC._upen_mapf()
        allRegisters["upen_csfp"] = _AF6CCI0012_RD_EOSENC._upen_csfp()
        allRegisters["upen_decf"] = _AF6CCI0012_RD_EOSENC._upen_decf()
        allRegisters["upchcfgpen"] = _AF6CCI0012_RD_EOSENC._upchcfgpen()
        allRegisters["upchstypen"] = _AF6CCI0012_RD_EOSENC._upchstypen()
        allRegisters["upchstapen"] = _AF6CCI0012_RD_EOSENC._upchstapen()
        allRegisters["upsegpen"] = _AF6CCI0012_RD_EOSENC._upsegpen()
        allRegisters["cfgglbintenpen"] = _AF6CCI0012_RD_EOSENC._cfgglbintenpen()
        allRegisters["upglbintpen"] = _AF6CCI0012_RD_EOSENC._upglbintpen()
        allRegisters["upen_triggf"] = _AF6CCI0012_RD_EOSENC._upen_triggf()
        allRegisters["upencfm"] = _AF6CCI0012_RD_EOSENC._upencfm()
        allRegisters["upen_tranboxf"] = _AF6CCI0012_RD_EOSENC._upen_tranboxf()
        allRegisters["upen_cfmp"] = _AF6CCI0012_RD_EOSENC._upen_cfmp()
        allRegisters["upencnt_dec_clen"] = _AF6CCI0012_RD_EOSENC._upencnt_dec_clen()
        allRegisters["upencnt_dec_even1"] = _AF6CCI0012_RD_EOSENC._upencnt_dec_even1()
        allRegisters["upencnt_dec_cline_r2c"] = _AF6CCI0012_RD_EOSENC._upencnt_dec_cline_r2c()
        allRegisters["upencnt_dec_cline_ro"] = _AF6CCI0012_RD_EOSENC._upencnt_dec_cline_ro()
        allRegisters["upencnt_dec_even4_r2c"] = _AF6CCI0012_RD_EOSENC._upencnt_dec_even4_r2c()
        allRegisters["upencnt_dec_even4_ro"] = _AF6CCI0012_RD_EOSENC._upencnt_dec_even4_ro()
        allRegisters["upencnt_dec_even5_r2c"] = _AF6CCI0012_RD_EOSENC._upencnt_dec_even5_r2c()
        allRegisters["upencnt_dec_even5_ro"] = _AF6CCI0012_RD_EOSENC._upencnt_dec_even5_ro()
        allRegisters["upencnt_dec_even6_r2c"] = _AF6CCI0012_RD_EOSENC._upencnt_dec_even6_r2c()
        allRegisters["upencnt_dec_even6_ro"] = _AF6CCI0012_RD_EOSENC._upencnt_dec_even6_ro()
        allRegisters["upencnt_dec_even7_r2c"] = _AF6CCI0012_RD_EOSENC._upencnt_dec_even7_r2c()
        allRegisters["upencnt_dec_even7_ro"] = _AF6CCI0012_RD_EOSENC._upencnt_dec_even7_ro()
        allRegisters["upencnt_dec_even8_r2c"] = _AF6CCI0012_RD_EOSENC._upencnt_dec_even8_r2c()
        allRegisters["upencnt_dec_even8_ro"] = _AF6CCI0012_RD_EOSENC._upencnt_dec_even8_ro()
        allRegisters["upencnt_dec_even9_r2c"] = _AF6CCI0012_RD_EOSENC._upencnt_dec_even9_r2c()
        allRegisters["upencnt_dec_even9_ro"] = _AF6CCI0012_RD_EOSENC._upencnt_dec_even9_ro()
        allRegisters["upencnt_oam_even1_r2c"] = _AF6CCI0012_RD_EOSENC._upencnt_oam_even1_r2c()
        allRegisters["upencnt_oam_even1_ro"] = _AF6CCI0012_RD_EOSENC._upencnt_oam_even1_ro()
        allRegisters["upencnt_oam_even2_r2c"] = _AF6CCI0012_RD_EOSENC._upencnt_oam_even2_r2c()
        allRegisters["upencnt_oam_even2_ro"] = _AF6CCI0012_RD_EOSENC._upencnt_oam_even2_ro()
        allRegisters["upencnt_oam_even3_r2c"] = _AF6CCI0012_RD_EOSENC._upencnt_oam_even3_r2c()
        allRegisters["upencnt_oam_even3_ro"] = _AF6CCI0012_RD_EOSENC._upencnt_oam_even3_ro()
        allRegisters["upencnt_oam_even4_r2c"] = _AF6CCI0012_RD_EOSENC._upencnt_oam_even4_r2c()
        allRegisters["upencnt_oam_even4_ro"] = _AF6CCI0012_RD_EOSENC._upencnt_oam_even4_ro()
        allRegisters["upencnt_oam_even5_r2c"] = _AF6CCI0012_RD_EOSENC._upencnt_oam_even5_r2c()
        allRegisters["upencnt_oam_even5_ro"] = _AF6CCI0012_RD_EOSENC._upencnt_oam_even5_ro()
        allRegisters["upencnt_oam_even6_r2c"] = _AF6CCI0012_RD_EOSENC._upencnt_oam_even6_r2c()
        allRegisters["upencnt_oam_even6_ro"] = _AF6CCI0012_RD_EOSENC._upencnt_oam_even6_ro()
        allRegisters["upencnt_enc_clen_r2c"] = _AF6CCI0012_RD_EOSENC._upencnt_enc_clen_r2c()
        allRegisters["upencnt_enc_clen_ro"] = _AF6CCI0012_RD_EOSENC._upencnt_enc_clen_ro()
        allRegisters["upencnt_enc_even1_r2c"] = _AF6CCI0012_RD_EOSENC._upencnt_enc_even1_r2c()
        allRegisters["upencnt_enc_even1_ro"] = _AF6CCI0012_RD_EOSENC._upencnt_enc_even1_ro()
        allRegisters["upencnt_enc_even2_r2c"] = _AF6CCI0012_RD_EOSENC._upencnt_enc_even2_r2c()
        allRegisters["upencnt_enc_even2_ro"] = _AF6CCI0012_RD_EOSENC._upencnt_enc_even2_ro()
        allRegisters["upencnt_enc_even3_r2c"] = _AF6CCI0012_RD_EOSENC._upencnt_enc_even3_r2c()
        allRegisters["upencnt_enc_even3_ro"] = _AF6CCI0012_RD_EOSENC._upencnt_enc_even3_ro()
        allRegisters["upencnt_enc_oamp1_r2c"] = _AF6CCI0012_RD_EOSENC._upencnt_enc_oamp1_r2c()
        allRegisters["upencnt_enc_oamp1_ro"] = _AF6CCI0012_RD_EOSENC._upencnt_enc_oamp1_ro()
        allRegisters["upencnt_enc_oamp2_r2c"] = _AF6CCI0012_RD_EOSENC._upencnt_enc_oamp2_r2c()
        allRegisters["upencnt_enc_oamp2_ro"] = _AF6CCI0012_RD_EOSENC._upencnt_enc_oamp2_ro()
        return allRegisters

    class _upen_holdreg1(AtRegister.AtRegister):
        def name(self):
            return "Hold Data Return REG 1"
    
        def description(self):
            return "Return data when microprocessor accesses to the REG is more than 32 Bits on Read-Phase"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _holdreg1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "holdreg1"
            
            def description(self):
                return "Hold Register 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["holdreg1"] = _AF6CCI0012_RD_EOSENC._upen_holdreg1._holdreg1()
            return allFields

    class _upen_egrext(AtRegister.AtRegister):
        def name(self):
            return "ENC Master Control"
    
        def description(self):
            return "Use to ENC Initial"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _ocfgtocken(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 2
        
            def name(self):
                return "ocfgtocken"
            
            def description(self):
                return "Number of D-word for Buffer Requesting"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _ocfgqdrini(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "ocfgqdrini"
            
            def description(self):
                return "ENC Initial Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _encdoneini(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "encdoneini"
            
            def description(self):
                return "ENC Initial Done"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ocfgtocken"] = _AF6CCI0012_RD_EOSENC._upen_egrext._ocfgtocken()
            allFields["ocfgqdrini"] = _AF6CCI0012_RD_EOSENC._upen_egrext._ocfgqdrini()
            allFields["encdoneini"] = _AF6CCI0012_RD_EOSENC._upen_egrext._encdoneini()
            return allFields

    class _upen_mapf(AtRegister.AtRegister):
        def name(self):
            return "MAP Table"
    
        def description(self):
            return "Per VCG Control HDL_PATH     : begin : af6_map_TB.af5cc_edi_array222x_MAP_TABLE.xil_bram_tdp_2clk_wrap.rem_ram.xil_bram_tdp_rem.inst_0.xil_bram_tdp0.ram_name[$VCG]={pgsapoct,pgfcsena,pgupigfp_7,pgupigfp_60,HDLC_INTER_FLAG,HDLCSCR,ENCMODE,FBIT_THRESHOLD_H} HDL_PATH     : end :"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x010000 + $VCG"
            
        def startAddress(self):
            return 0x00010000
            
        def endAddress(self):
            return 0x00011fff

        class _pgsapoct(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 20
        
            def name(self):
                return "pgsapoct"
            
            def description(self):
                return "Second Octo of SAPI Field are Used in HDLC Byte Stuffing Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _pgfcsena(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "pgfcsena"
            
            def description(self):
                return "GFP - F FCS Payload Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _pgupigfp_7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "pgupigfp_7"
            
            def description(self):
                return "GFP - F UPI Field Control_7 or First Octo of SAPI Field are Used in HDLC Byte Stuffing Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _pgupigfp_60(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 11
        
            def name(self):
                return "pgupigfp_60"
            
            def description(self):
                return "GFP - F UPI Field Control_60 or First Octo of SAPI Field are Used in HDLC Byte Stuffing Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _HDLC_INTER_FLAG(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 9
        
            def name(self):
                return "HDLC_INTER_FLAG"
            
            def description(self):
                return "HDLC Minimun Control for The Number of Byte Inter Packet Flag, Zero is Expected 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _HDLCSCR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "HDLCSCR"
            
            def description(self):
                return "HDLC or GFP-F Scramble Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _ENCMODE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "ENCMODE"
            
            def description(self):
                return "0 is Bypass, 1 is HDLC Byte Stuffing Mode, 3 is GFP Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FBIT_THRESHOLD_H(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FBIT_THRESHOLD_H"
            
            def description(self):
                return "Number of Block of 32 Bytes are Provided for ENC Cache, Total Max Block is 2K"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pgsapoct"] = _AF6CCI0012_RD_EOSENC._upen_mapf._pgsapoct()
            allFields["pgfcsena"] = _AF6CCI0012_RD_EOSENC._upen_mapf._pgfcsena()
            allFields["pgupigfp_7"] = _AF6CCI0012_RD_EOSENC._upen_mapf._pgupigfp_7()
            allFields["pgupigfp_60"] = _AF6CCI0012_RD_EOSENC._upen_mapf._pgupigfp_60()
            allFields["HDLC_INTER_FLAG"] = _AF6CCI0012_RD_EOSENC._upen_mapf._HDLC_INTER_FLAG()
            allFields["HDLCSCR"] = _AF6CCI0012_RD_EOSENC._upen_mapf._HDLCSCR()
            allFields["ENCMODE"] = _AF6CCI0012_RD_EOSENC._upen_mapf._ENCMODE()
            allFields["FBIT_THRESHOLD_H"] = _AF6CCI0012_RD_EOSENC._upen_mapf._FBIT_THRESHOLD_H()
            return allFields

    class _upen_csfp(AtRegister.AtRegister):
        def name(self):
            return "CSF Transmit Control"
    
        def description(self):
            return "Per VCG Control HDL_PATH     :  af6_map_TB.af5cc_edi_array222x_CSF_TABLE.xil_bram_tdp_2clk_wrap.rem_ram.xil_bram_tdp_rem.inst_0.xil_bram_tdp0.ram_name[$VCG]"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x010800 + $VCG"
            
        def startAddress(self):
            return 0x00010800
            
        def endAddress(self):
            return 0x00010bff

        class _decicfmena(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "decicfmena"
            
            def description(self):
                return "Enable Receive CMF"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _encicsfena(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "encicsfena"
            
            def description(self):
                return "Enable Transmit CSF"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _encicsfupi(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "encicsfupi"
            
            def description(self):
                return "GFP - F UPI Field are Used for CSF Frame"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["decicfmena"] = _AF6CCI0012_RD_EOSENC._upen_csfp._decicfmena()
            allFields["encicsfena"] = _AF6CCI0012_RD_EOSENC._upen_csfp._encicsfena()
            allFields["encicsfupi"] = _AF6CCI0012_RD_EOSENC._upen_csfp._encicsfupi()
            return allFields

    class _upen_decf(AtRegister.AtRegister):
        def name(self):
            return "Decapsulation Table"
    
        def description(self):
            return "Per VCG Control HDL_PATH     :  af6_map_TB.af5cc_edi_array222x_DECFTABLE.xil_bram_tdp_2clk_wrap.int_ram.xil_bram_tdp_int[0].inst_0.xil_bram_tdp0.ram_name[$VCG]"
            
        def width(self):
            return 64
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x010400 + $VCG"
            
        def startAddress(self):
            return 0x00010400
            
        def endAddress(self):
            return 0x000107ff

        class _tb_drop_dismisf(AtRegister.AtRegisterField):
            def stopBit(self):
                return 38
                
            def startBit(self):
                return 38
        
            def name(self):
                return "tb_drop_dismisf"
            
            def description(self):
                return "SAPI Data Frame Mismatch Drop Disable are Used in HDLC Byte Stuffing Mode or UPI Mismatch Drop Disable are Used in GFP-F Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tb_addr_oct(AtRegister.AtRegisterField):
            def stopBit(self):
                return 37
                
            def startBit(self):
                return 30
        
            def name(self):
                return "tb_addr_oct"
            
            def description(self):
                return "Second Octo of SAPI Data Link are Used in HDLC Byte Stuffing Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tb_ctrl_oct(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 22
        
            def name(self):
                return "tb_ctrl_oct"
            
            def description(self):
                return "First Octo of SAPI Data Link are Used in HDLC Byte Stuffing Mode or UPI Field Comparing are Used in GFP-F Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tb_sapi_oct(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 6
        
            def name(self):
                return "tb_sapi_oct"
            
            def description(self):
                return "SAPI Data Frame Comparing are Used in HDLC Byte Stuffing Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tb_hdlc_ena(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "tb_hdlc_ena"
            
            def description(self):
                return "HDLC Byte Stuffing Mode Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tb_drop_discrcf(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "tb_drop_discrcf"
            
            def description(self):
                return "Disable Drop Packet When CRC Payload Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tb_drop_disthec(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "tb_drop_disthec"
            
            def description(self):
                return "Disable Drop Packet When T-HEC Error	in GFP Mode or Disable Drop Packet When Mismatch Data Link SAPI Above in HDLC Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tb_scrm_ena(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "tb_scrm_ena"
            
            def description(self):
                return "Scramble Enable on DEC"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tb_thec_correct(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "tb_thec_correct"
            
            def description(self):
                return "T-HEC Correct Enable in GFP Mode, TDM Header Remove Enable in HDLC Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tb_chec_correct(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tb_chec_correct"
            
            def description(self):
                return "C-HEC Correct Enable on DEC"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tb_drop_dismisf"] = _AF6CCI0012_RD_EOSENC._upen_decf._tb_drop_dismisf()
            allFields["tb_addr_oct"] = _AF6CCI0012_RD_EOSENC._upen_decf._tb_addr_oct()
            allFields["tb_ctrl_oct"] = _AF6CCI0012_RD_EOSENC._upen_decf._tb_ctrl_oct()
            allFields["tb_sapi_oct"] = _AF6CCI0012_RD_EOSENC._upen_decf._tb_sapi_oct()
            allFields["tb_hdlc_ena"] = _AF6CCI0012_RD_EOSENC._upen_decf._tb_hdlc_ena()
            allFields["tb_drop_discrcf"] = _AF6CCI0012_RD_EOSENC._upen_decf._tb_drop_discrcf()
            allFields["tb_drop_disthec"] = _AF6CCI0012_RD_EOSENC._upen_decf._tb_drop_disthec()
            allFields["tb_scrm_ena"] = _AF6CCI0012_RD_EOSENC._upen_decf._tb_scrm_ena()
            allFields["tb_thec_correct"] = _AF6CCI0012_RD_EOSENC._upen_decf._tb_thec_correct()
            allFields["tb_chec_correct"] = _AF6CCI0012_RD_EOSENC._upen_decf._tb_chec_correct()
            return allFields

    class _upchcfgpen(AtRegister.AtRegister):
        def name(self):
            return "Alarm Interrupt Enable Control"
    
        def description(self):
            return "Registers is Used to Enable Interrupting."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return "0x001000 + $VCG"
            
        def startAddress(self):
            return 0x00001000
            
        def endAddress(self):
            return 0x000010ff

        class _RCP_CSF_ENB(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "RCP_CSF_ENB"
            
            def description(self):
                return "Received CSF Frame in GFP-F Mode or Address Mismatch in HDLC Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FRMSTATE_ENB(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FRMSTATE_ENB"
            
            def description(self):
                return "Framer State Change per Bit Control in GFP-F Mode or Control, SAPI Mismatch in HDLC Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RCP_CSF_ENB"] = _AF6CCI0012_RD_EOSENC._upchcfgpen._RCP_CSF_ENB()
            allFields["FRMSTATE_ENB"] = _AF6CCI0012_RD_EOSENC._upchcfgpen._FRMSTATE_ENB()
            return allFields

    class _upchstypen(AtRegister.AtRegister):
        def name(self):
            return "Alarm Interrupt Sticky"
    
        def description(self):
            return "Registers are Used to Latch the Interrupt bits."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return "0x001100 + $VCG"
            
        def startAddress(self):
            return 0x00001100
            
        def endAddress(self):
            return 0x000011ff

        class _RCP_CSF_STICKY(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "RCP_CSF_STICKY"
            
            def description(self):
                return "Received CSF Frame in GFP-F Mode or Address Mismatch in HDLC Mode"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _FRMSTATE_STICKY_BIT_B(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FRMSTATE_STICKY_BIT_B"
            
            def description(self):
                return "The Second Bit of Frame State Transition in GFP-F Mode or Control Mismatch in HDLC Mode"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _FRMSTATE_STICKY_BIT_A(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FRMSTATE_STICKY_BIT_A"
            
            def description(self):
                return "The First Bit of Frame State Transition in GFP-F Mode or SAPI Mismatch in HDLC Mode"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RCP_CSF_STICKY"] = _AF6CCI0012_RD_EOSENC._upchstypen._RCP_CSF_STICKY()
            allFields["FRMSTATE_STICKY_BIT_B"] = _AF6CCI0012_RD_EOSENC._upchstypen._FRMSTATE_STICKY_BIT_B()
            allFields["FRMSTATE_STICKY_BIT_A"] = _AF6CCI0012_RD_EOSENC._upchstypen._FRMSTATE_STICKY_BIT_A()
            return allFields

    class _upchstapen(AtRegister.AtRegister):
        def name(self):
            return "Alarm Interrupt Status"
    
        def description(self):
            return "Each register is Current State."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return "0x001200 + $VCG"
            
        def startAddress(self):
            return 0x00001200
            
        def endAddress(self):
            return 0x000012ff

        class _RCP_CSF_STATE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "RCP_CSF_STATE"
            
            def description(self):
                return "Received CSF Frame in GFP-F Mode or Address Mismatch in HDLC Mode"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _FRMSTATE_STATE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FRMSTATE_STATE"
            
            def description(self):
                return "DEC Framer State Change : 0 is HUNT, 1 is PRESYNC, 2 is SYNC	in GFP-F Mode or Control, SAPI Mismatch in HDLC Mode"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RCP_CSF_STATE"] = _AF6CCI0012_RD_EOSENC._upchstapen._RCP_CSF_STATE()
            allFields["FRMSTATE_STATE"] = _AF6CCI0012_RD_EOSENC._upchstapen._FRMSTATE_STATE()
            return allFields

    class _upsegpen(AtRegister.AtRegister):
        def name(self):
            return "OR Interrupt Status per VCG"
    
        def description(self):
            return "The register consists of 32 bits for 256 VCGs. Each bit is used to store Interrupt Status per VCG."
            
        def width(self):
            return 64
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return "0x001300+$addr"
            
        def startAddress(self):
            return 0x00001300
            
        def endAddress(self):
            return 0x00001307

        class _intsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "intsta"
            
            def description(self):
                return "Every bit corresponding with Interrupt Status per VCG"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["intsta"] = _AF6CCI0012_RD_EOSENC._upsegpen._intsta()
            return allFields

    class _cfgglbintenpen(AtRegister.AtRegister):
        def name(self):
            return "Global Interrupt Enable Configuration"
    
        def description(self):
            return "The register consists of 32 bits used to configurate the row of OR Interrupt Status per VCG (32-VCG) Interrup Enable."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000013fe
            
        def endAddress(self):
            return 0xffffffff

        class _intenb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "intenb"
            
            def description(self):
                return "Every bit used to Enable the row of OR Interrupt Status per VCG"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["intenb"] = _AF6CCI0012_RD_EOSENC._cfgglbintenpen._intenb()
            return allFields

    class _upglbintpen(AtRegister.AtRegister):
        def name(self):
            return "Interrupt Status at a Row"
    
        def description(self):
            return "The register consists of 32 bits used to know the row of OR Interrupt Status per VCG (32-VCG) Interrup"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000013ff
            
        def endAddress(self):
            return 0xffffffff

        class _intst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "intst"
            
            def description(self):
                return "Every bit corresponding the row of OR Interrupt Status per VCG"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["intst"] = _AF6CCI0012_RD_EOSENC._upglbintpen._intst()
            return allFields

    class _upen_triggf(AtRegister.AtRegister):
        def name(self):
            return "CFM DEC Trigger Fifo Control and Status"
    
        def description(self):
            return "Info for Getting Packet from Buffer"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000004
            
        def endAddress(self):
            return 0xffffffff

        class _cinfoval(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "cinfoval"
            
            def description(self):
                return "Info Valid on REG"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _cinfoeop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "cinfoeop"
            
            def description(self):
                return "End of Packet"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _cfmvcgx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 12
        
            def name(self):
                return "cfmvcgx"
            
            def description(self):
                return "VCG-ID"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _blockid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "blockid"
            
            def description(self):
                return "Block ID"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _octcntf(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "octcntf"
            
            def description(self):
                return "Number of D-Word"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _ff_i_nob(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 1
        
            def name(self):
                return "ff_i_nob"
            
            def description(self):
                return "Number of Byte"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _ocfg_eve_clre(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ocfg_eve_clre"
            
            def description(self):
                return "Info Valid Clear When Changing from 0 to 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cinfoval"] = _AF6CCI0012_RD_EOSENC._upen_triggf._cinfoval()
            allFields["cinfoeop"] = _AF6CCI0012_RD_EOSENC._upen_triggf._cinfoeop()
            allFields["cfmvcgx"] = _AF6CCI0012_RD_EOSENC._upen_triggf._cfmvcgx()
            allFields["blockid"] = _AF6CCI0012_RD_EOSENC._upen_triggf._blockid()
            allFields["octcntf"] = _AF6CCI0012_RD_EOSENC._upen_triggf._octcntf()
            allFields["ff_i_nob"] = _AF6CCI0012_RD_EOSENC._upen_triggf._ff_i_nob()
            allFields["ocfg_eve_clre"] = _AF6CCI0012_RD_EOSENC._upen_triggf._ocfg_eve_clre()
            return allFields

    class _upencfm(AtRegister.AtRegister):
        def name(self):
            return "CFM DEC Trigger Data Dump"
    
        def description(self):
            return "256 Location Buffer Store CFM Packet"
            
        def width(self):
            return 64
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x001800 + 16*$BID + $CNT"
            
        def startAddress(self):
            return 0x00001800
            
        def endAddress(self):
            return 0x00001fff

        class _ff_i_dat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ff_i_dat"
            
            def description(self):
                return "Data Dump"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ff_i_dat"] = _AF6CCI0012_RD_EOSENC._upencfm._ff_i_dat()
            return allFields

    class _upen_tranboxf(AtRegister.AtRegister):
        def name(self):
            return "CFM ENC Master Control"
    
        def description(self):
            return "Info for Engine Read CFM Packet"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x002000 + $VCG"
            
        def startAddress(self):
            return 0x00002000
            
        def endAddress(self):
            return 0x000027ff

        class _pgcfmilen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 9
        
            def name(self):
                return "pgcfmilen"
            
            def description(self):
                return "Length of CFM Packet, Zero Expected is 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _pgcfmiadd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 1
        
            def name(self):
                return "pgcfmiadd"
            
            def description(self):
                return "Engine Use this Field to Start Read Process at This Location, Next Read Process is Incremented by 1 from This Field"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _pgcfmiena(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pgcfmiena"
            
            def description(self):
                return "Set 1 When CPU Want to Send a CFM Packet, Engine Will Clear This Bit When Send Done"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pgcfmilen"] = _AF6CCI0012_RD_EOSENC._upen_tranboxf._pgcfmilen()
            allFields["pgcfmiadd"] = _AF6CCI0012_RD_EOSENC._upen_tranboxf._pgcfmiadd()
            allFields["pgcfmiena"] = _AF6CCI0012_RD_EOSENC._upen_tranboxf._pgcfmiena()
            return allFields

    class _upen_cfmp(AtRegister.AtRegister):
        def name(self):
            return "CFM ENC Data Buffer"
    
        def description(self):
            return "256 Location Buffer Queuing"
            
        def width(self):
            return 64
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x010C00 + $Location"
            
        def startAddress(self):
            return 0x00010c00
            
        def endAddress(self):
            return 0x00010fff

        class _cfm_o_dat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfm_o_dat"
            
            def description(self):
                return "CFM Frame"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfm_o_dat"] = _AF6CCI0012_RD_EOSENC._upen_cfmp._cfm_o_dat()
            return allFields

    class _upencnt_dec_clen(AtRegister.AtRegister):
        def name(self):
            return "DEC Data Byte Counter"
    
        def description(self):
            return "Byte Packet Count Send to TxBUF"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x020000 + $Location"
            
        def startAddress(self):
            return 0x00020000
            
        def endAddress(self):
            return 0x000200ff

        class _cdeclen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cdeclen"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cdeclen"] = _AF6CCI0012_RD_EOSENC._upencnt_dec_clen._cdeclen()
            return allFields

    class _upencnt_dec_even1(AtRegister.AtRegister):
        def name(self):
            return "DEC Packet Counter"
    
        def description(self):
            return "Packet Count Send to TxBUF"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x020100 + $Location"
            
        def startAddress(self):
            return 0x00020100
            
        def endAddress(self):
            return 0x000201ff

        class _egroeop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "egroeop"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["egroeop"] = _AF6CCI0012_RD_EOSENC._upencnt_dec_even1._egroeop()
            return allFields

    class _upencnt_dec_cline_r2c(AtRegister.AtRegister):
        def name(self):
            return "DEC Line Frame Counter"
    
        def description(self):
            return "IDLE and CHEC Correct Frame Count"
            
        def width(self):
            return 64
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x02FF00 + $Location"
            
        def startAddress(self):
            return 0x0002ff00
            
        def endAddress(self):
            return 0x0002ffff

        class _p_frmi_ochec_c(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 32
        
            def name(self):
                return "p_frmi_ochec_c"
            
            def description(self):
                return "CHEC Correct Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        class _p_frmi_oidle_f(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "p_frmi_oidle_f"
            
            def description(self):
                return "IDLE Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["p_frmi_ochec_c"] = _AF6CCI0012_RD_EOSENC._upencnt_dec_cline_r2c._p_frmi_ochec_c()
            allFields["p_frmi_oidle_f"] = _AF6CCI0012_RD_EOSENC._upencnt_dec_cline_r2c._p_frmi_oidle_f()
            return allFields

    class _upencnt_dec_cline_ro(AtRegister.AtRegister):
        def name(self):
            return "DEC Line Frame Counter"
    
        def description(self):
            return "IDLE and CHEC Correct Frame Count"
            
        def width(self):
            return 64
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x03FF00 + $Location"
            
        def startAddress(self):
            return 0x0003ff00
            
        def endAddress(self):
            return 0x0003ffff

        class _p_frmi_ochec_c(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 32
        
            def name(self):
                return "p_frmi_ochec_c"
            
            def description(self):
                return "CHEC Correct Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        class _p_frmi_oidle_f(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "p_frmi_oidle_f"
            
            def description(self):
                return "IDLE Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["p_frmi_ochec_c"] = _AF6CCI0012_RD_EOSENC._upencnt_dec_cline_ro._p_frmi_ochec_c()
            allFields["p_frmi_oidle_f"] = _AF6CCI0012_RD_EOSENC._upencnt_dec_cline_ro._p_frmi_oidle_f()
            return allFields

    class _upencnt_dec_even4_r2c(AtRegister.AtRegister):
        def name(self):
            return "DEC C-HEC Error Counter"
    
        def description(self):
            return "DEC C-HEC Error Count"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x020200 + $Location"
            
        def startAddress(self):
            return 0x00020200
            
        def endAddress(self):
            return 0x000202ff

        class _p_frmi_ochec_e(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "p_frmi_ochec_e"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["p_frmi_ochec_e"] = _AF6CCI0012_RD_EOSENC._upencnt_dec_even4_r2c._p_frmi_ochec_e()
            return allFields

    class _upencnt_dec_even4_ro(AtRegister.AtRegister):
        def name(self):
            return "DEC C-HEC Error Counter"
    
        def description(self):
            return "DEC C-HEC Error Count"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x030200 + $Location"
            
        def startAddress(self):
            return 0x00030200
            
        def endAddress(self):
            return 0x000302ff

        class _p_frmi_ochec_e(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "p_frmi_ochec_e"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["p_frmi_ochec_e"] = _AF6CCI0012_RD_EOSENC._upencnt_dec_even4_ro._p_frmi_ochec_e()
            return allFields

    class _upencnt_dec_even5_r2c(AtRegister.AtRegister):
        def name(self):
            return "DEC UPI Mismatch Counter"
    
        def description(self):
            return "UPI Mismatch Count in GFP Mode or SAPI Mismatch Count in HDLC Mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x020300 + $Location"
            
        def startAddress(self):
            return 0x00020300
            
        def endAddress(self):
            return 0x000203ff

        class _o_gfpf_omisupi(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "o_gfpf_omisupi"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["o_gfpf_omisupi"] = _AF6CCI0012_RD_EOSENC._upencnt_dec_even5_r2c._o_gfpf_omisupi()
            return allFields

    class _upencnt_dec_even5_ro(AtRegister.AtRegister):
        def name(self):
            return "DEC UPI Mismatch Counter"
    
        def description(self):
            return "UPI Mismatch Count in GFP Mode or SAPI Mismatch Count in HDLC Mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x030300 + $Location"
            
        def startAddress(self):
            return 0x00030300
            
        def endAddress(self):
            return 0x000303ff

        class _o_gfpf_omisupi(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "o_gfpf_omisupi"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["o_gfpf_omisupi"] = _AF6CCI0012_RD_EOSENC._upencnt_dec_even5_ro._o_gfpf_omisupi()
            return allFields

    class _upencnt_dec_even6_r2c(AtRegister.AtRegister):
        def name(self):
            return "DEC CRC Error Counter"
    
        def description(self):
            return "DEC CRC Error Count"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x020400 + $Location"
            
        def startAddress(self):
            return 0x00020400
            
        def endAddress(self):
            return 0x000204ff

        class _o_gfpf_ocrcerr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "o_gfpf_ocrcerr"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["o_gfpf_ocrcerr"] = _AF6CCI0012_RD_EOSENC._upencnt_dec_even6_r2c._o_gfpf_ocrcerr()
            return allFields

    class _upencnt_dec_even6_ro(AtRegister.AtRegister):
        def name(self):
            return "DEC CRC Error Counter"
    
        def description(self):
            return "DEC CRC Error Count"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x030400 + $Location"
            
        def startAddress(self):
            return 0x00030400
            
        def endAddress(self):
            return 0x000304ff

        class _o_gfpf_ocrcerr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "o_gfpf_ocrcerr"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["o_gfpf_ocrcerr"] = _AF6CCI0012_RD_EOSENC._upencnt_dec_even6_ro._o_gfpf_ocrcerr()
            return allFields

    class _upencnt_dec_even7_r2c(AtRegister.AtRegister):
        def name(self):
            return "DEC Packet Drop Counter"
    
        def description(self):
            return "DEC Packet Drop Count"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x020500 + $Location"
            
        def startAddress(self):
            return 0x00020500
            
        def endAddress(self):
            return 0x000205ff

        class _o_gfpf_odrop_e(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "o_gfpf_odrop_e"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["o_gfpf_odrop_e"] = _AF6CCI0012_RD_EOSENC._upencnt_dec_even7_r2c._o_gfpf_odrop_e()
            return allFields

    class _upencnt_dec_even7_ro(AtRegister.AtRegister):
        def name(self):
            return "DEC Packet Drop Counter"
    
        def description(self):
            return "DEC Packet Drop Count"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x030500 + $Location"
            
        def startAddress(self):
            return 0x00030500
            
        def endAddress(self):
            return 0x000305ff

        class _o_gfpf_odrop_e(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "o_gfpf_odrop_e"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["o_gfpf_odrop_e"] = _AF6CCI0012_RD_EOSENC._upencnt_dec_even7_ro._o_gfpf_odrop_e()
            return allFields

    class _upencnt_dec_even8_r2c(AtRegister.AtRegister):
        def name(self):
            return "DEC T-HEC Correct Counter"
    
        def description(self):
            return "DEC T-HEC Correct Count"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x020600 + $Location"
            
        def startAddress(self):
            return 0x00020600
            
        def endAddress(self):
            return 0x000206ff

        class _o_gfpf_othec_c(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "o_gfpf_othec_c"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["o_gfpf_othec_c"] = _AF6CCI0012_RD_EOSENC._upencnt_dec_even8_r2c._o_gfpf_othec_c()
            return allFields

    class _upencnt_dec_even8_ro(AtRegister.AtRegister):
        def name(self):
            return "DEC T-HEC Correct Counter"
    
        def description(self):
            return "DEC T-HEC Correct Count"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x030600 + $Location"
            
        def startAddress(self):
            return 0x00030600
            
        def endAddress(self):
            return 0x000306ff

        class _o_gfpf_othec_c(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "o_gfpf_othec_c"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["o_gfpf_othec_c"] = _AF6CCI0012_RD_EOSENC._upencnt_dec_even8_ro._o_gfpf_othec_c()
            return allFields

    class _upencnt_dec_even9_r2c(AtRegister.AtRegister):
        def name(self):
            return "DEC T-HEC Error Counter"
    
        def description(self):
            return "DEC T-HEC Error Count in GFP Mode or DEC Abort Count in HDLC Mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x020700 + $Location"
            
        def startAddress(self):
            return 0x00020700
            
        def endAddress(self):
            return 0x000207ff

        class _o_gfpf_othec_e(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "o_gfpf_othec_e"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["o_gfpf_othec_e"] = _AF6CCI0012_RD_EOSENC._upencnt_dec_even9_r2c._o_gfpf_othec_e()
            return allFields

    class _upencnt_dec_even9_ro(AtRegister.AtRegister):
        def name(self):
            return "DEC T-HEC Error Counter"
    
        def description(self):
            return "DEC T-HEC Error Count in GFP Mode or DEC Abort Count in HDLC Mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x030700 + $Location"
            
        def startAddress(self):
            return 0x00030700
            
        def endAddress(self):
            return 0x000307ff

        class _o_gfpf_othec_e(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "o_gfpf_othec_e"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["o_gfpf_othec_e"] = _AF6CCI0012_RD_EOSENC._upencnt_dec_even9_ro._o_gfpf_othec_e()
            return allFields

    class _upencnt_oam_even1_r2c(AtRegister.AtRegister):
        def name(self):
            return "DEC CFM loss of client signal frame counter"
    
        def description(self):
            return "CFM Packet With PTI is 4 and UPI is 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x020800 + $Location"
            
        def startAddress(self):
            return 0x00020800
            
        def endAddress(self):
            return 0x000208ff

        class _cntotyp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntotyp"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntotyp"] = _AF6CCI0012_RD_EOSENC._upencnt_oam_even1_r2c._cntotyp()
            return allFields

    class _upencnt_oam_even1_ro(AtRegister.AtRegister):
        def name(self):
            return "DEC CFM loss of client signal frame counter"
    
        def description(self):
            return "CFM Packet With PTI is 4 and UPI is 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x030800 + $Location"
            
        def startAddress(self):
            return 0x00030800
            
        def endAddress(self):
            return 0x000308ff

        class _cntotyp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntotyp"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntotyp"] = _AF6CCI0012_RD_EOSENC._upencnt_oam_even1_ro._cntotyp()
            return allFields

    class _upencnt_oam_even2_r2c(AtRegister.AtRegister):
        def name(self):
            return "DEC CFM loss of character synchronization counter"
    
        def description(self):
            return "CFM Packet With PTI is 4 and UPI is 2"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x020900 + $Location"
            
        def startAddress(self):
            return 0x00020900
            
        def endAddress(self):
            return 0x000209ff

        class _cntotyp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntotyp"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntotyp"] = _AF6CCI0012_RD_EOSENC._upencnt_oam_even2_r2c._cntotyp()
            return allFields

    class _upencnt_oam_even2_ro(AtRegister.AtRegister):
        def name(self):
            return "DEC CFM loss of character synchronization counter"
    
        def description(self):
            return "CFM Packet With PTI is 4 and UPI is 2"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x030900 + $Location"
            
        def startAddress(self):
            return 0x00030900
            
        def endAddress(self):
            return 0x000309ff

        class _cntotyp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntotyp"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntotyp"] = _AF6CCI0012_RD_EOSENC._upencnt_oam_even2_ro._cntotyp()
            return allFields

    class _upencnt_oam_even3_r2c(AtRegister.AtRegister):
        def name(self):
            return "DEC CFM Client defect clear indication counter"
    
        def description(self):
            return "CFM Packet With PTI is 4 and UPI is 3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x020A00 + $Location"
            
        def startAddress(self):
            return 0x00020a00
            
        def endAddress(self):
            return 0x00020aff

        class _cntotyp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntotyp"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntotyp"] = _AF6CCI0012_RD_EOSENC._upencnt_oam_even3_r2c._cntotyp()
            return allFields

    class _upencnt_oam_even3_ro(AtRegister.AtRegister):
        def name(self):
            return "DEC CFM Client defect clear indication counter"
    
        def description(self):
            return "CFM Packet With PTI is 4 and UPI is 3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x030A00 + $Location"
            
        def startAddress(self):
            return 0x00030a00
            
        def endAddress(self):
            return 0x00030aff

        class _cntotyp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntotyp"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntotyp"] = _AF6CCI0012_RD_EOSENC._upencnt_oam_even3_ro._cntotyp()
            return allFields

    class _upencnt_oam_even4_r2c(AtRegister.AtRegister):
        def name(self):
            return "DEC CFM Client forward defect indication counter"
    
        def description(self):
            return "CFM Packet With PTI is 4 and UPI is 4"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x020B00 + $Location"
            
        def startAddress(self):
            return 0x00020b00
            
        def endAddress(self):
            return 0x00020bff

        class _cntotyp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntotyp"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntotyp"] = _AF6CCI0012_RD_EOSENC._upencnt_oam_even4_r2c._cntotyp()
            return allFields

    class _upencnt_oam_even4_ro(AtRegister.AtRegister):
        def name(self):
            return "DEC CFM Client forward defect indication counter"
    
        def description(self):
            return "CFM Packet With PTI is 4 and UPI is 4"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x030B00 + $Location"
            
        def startAddress(self):
            return 0x00030b00
            
        def endAddress(self):
            return 0x00030bff

        class _cntotyp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntotyp"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntotyp"] = _AF6CCI0012_RD_EOSENC._upencnt_oam_even4_ro._cntotyp()
            return allFields

    class _upencnt_oam_even5_r2c(AtRegister.AtRegister):
        def name(self):
            return "DEC CFM Client reverse defect indication counter"
    
        def description(self):
            return "CFM Packet With PTI is 4 and UPI is 5"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x020C00 + $Location"
            
        def startAddress(self):
            return 0x00020c00
            
        def endAddress(self):
            return 0x00020cff

        class _cntotyp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntotyp"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntotyp"] = _AF6CCI0012_RD_EOSENC._upencnt_oam_even5_r2c._cntotyp()
            return allFields

    class _upencnt_oam_even5_ro(AtRegister.AtRegister):
        def name(self):
            return "DEC CFM Client reverse defect indication counter"
    
        def description(self):
            return "CFM Packet With PTI is 4 and UPI is 5"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x030C00 + $Location"
            
        def startAddress(self):
            return 0x00030c00
            
        def endAddress(self):
            return 0x00030cff

        class _cntotyp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntotyp"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntotyp"] = _AF6CCI0012_RD_EOSENC._upencnt_oam_even5_ro._cntotyp()
            return allFields

    class _upencnt_oam_even6_r2c(AtRegister.AtRegister):
        def name(self):
            return "DEC CFM Reserved counter"
    
        def description(self):
            return "CFM Packet With PTI is 4 and UPI are 0x0, 0xFF or 0xE0 to 0xFE"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x020D00 + $Location"
            
        def startAddress(self):
            return 0x00020d00
            
        def endAddress(self):
            return 0x00020dff

        class _cntotyp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntotyp"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntotyp"] = _AF6CCI0012_RD_EOSENC._upencnt_oam_even6_r2c._cntotyp()
            return allFields

    class _upencnt_oam_even6_ro(AtRegister.AtRegister):
        def name(self):
            return "DEC CFM Reserved counter"
    
        def description(self):
            return "CFM Packet With PTI is 4 and UPI are 0x0, 0xFF or 0xE0 to 0xFE"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x030D00 + $Location"
            
        def startAddress(self):
            return 0x00030d00
            
        def endAddress(self):
            return 0x00030dff

        class _cntotyp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntotyp"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntotyp"] = _AF6CCI0012_RD_EOSENC._upencnt_oam_even6_ro._cntotyp()
            return allFields

    class _upencnt_enc_clen_r2c(AtRegister.AtRegister):
        def name(self):
            return "ENC Data Byte Counter"
    
        def description(self):
            return "Byte Packet Count Send to V-CAT"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x020E00 + $Location"
            
        def startAddress(self):
            return 0x00020e00
            
        def endAddress(self):
            return 0x00020eff

        class _cenclen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cenclen"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cenclen"] = _AF6CCI0012_RD_EOSENC._upencnt_enc_clen_r2c._cenclen()
            return allFields

    class _upencnt_enc_clen_ro(AtRegister.AtRegister):
        def name(self):
            return "ENC Data Byte Counter"
    
        def description(self):
            return "Byte Packet Count Send to V-CAT"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x030E00 + $Location"
            
        def startAddress(self):
            return 0x00030e00
            
        def endAddress(self):
            return 0x00030eff

        class _cenclen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cenclen"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cenclen"] = _AF6CCI0012_RD_EOSENC._upencnt_enc_clen_ro._cenclen()
            return allFields

    class _upencnt_enc_even1_r2c(AtRegister.AtRegister):
        def name(self):
            return "ENC Packet Counter"
    
        def description(self):
            return "Packet Count Send to V-CAT"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x020F00 + $Location"
            
        def startAddress(self):
            return 0x00020f00
            
        def endAddress(self):
            return 0x00020fff

        class _encieop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "encieop"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["encieop"] = _AF6CCI0012_RD_EOSENC._upencnt_enc_even1_r2c._encieop()
            return allFields

    class _upencnt_enc_even1_ro(AtRegister.AtRegister):
        def name(self):
            return "ENC Packet Counter"
    
        def description(self):
            return "Packet Count Send to V-CAT"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x030F00 + $Location"
            
        def startAddress(self):
            return 0x00030f00
            
        def endAddress(self):
            return 0x00030fff

        class _encieop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "encieop"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["encieop"] = _AF6CCI0012_RD_EOSENC._upencnt_enc_even1_ro._encieop()
            return allFields

    class _upencnt_enc_even2_r2c(AtRegister.AtRegister):
        def name(self):
            return "ENC IDLE Frame Counter"
    
        def description(self):
            return "IDLE Frame Count"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x021000 + $Location"
            
        def startAddress(self):
            return 0x00021000
            
        def endAddress(self):
            return 0x000210ff

        class _idlegfp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "idlegfp"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["idlegfp"] = _AF6CCI0012_RD_EOSENC._upencnt_enc_even2_r2c._idlegfp()
            return allFields

    class _upencnt_enc_even2_ro(AtRegister.AtRegister):
        def name(self):
            return "ENC IDLE Frame Counter"
    
        def description(self):
            return "IDLE Frame Count"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x031000 + $Location"
            
        def startAddress(self):
            return 0x00031000
            
        def endAddress(self):
            return 0x000310ff

        class _idlegfp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "idlegfp"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["idlegfp"] = _AF6CCI0012_RD_EOSENC._upencnt_enc_even2_ro._idlegfp()
            return allFields

    class _upencnt_enc_even3_r2c(AtRegister.AtRegister):
        def name(self):
            return "ENC Abort Packet Counter"
    
        def description(self):
            return "Abort Frame Count on ENC"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x021100 + $Location"
            
        def startAddress(self):
            return 0x00021100
            
        def endAddress(self):
            return 0x000211ff

        class _iabort(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "iabort"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["iabort"] = _AF6CCI0012_RD_EOSENC._upencnt_enc_even3_r2c._iabort()
            return allFields

    class _upencnt_enc_even3_ro(AtRegister.AtRegister):
        def name(self):
            return "ENC Abort Packet Counter"
    
        def description(self):
            return "Abort Frame Count on ENC"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x031100 + $Location"
            
        def startAddress(self):
            return 0x00031100
            
        def endAddress(self):
            return 0x000311ff

        class _iabort(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "iabort"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["iabort"] = _AF6CCI0012_RD_EOSENC._upencnt_enc_even3_ro._iabort()
            return allFields

    class _upencnt_enc_oamp1_r2c(AtRegister.AtRegister):
        def name(self):
            return "ENC CSF Frame Counter"
    
        def description(self):
            return "CSF Packet Send to V-CAT"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x021200 + $Location"
            
        def startAddress(self):
            return 0x00021200
            
        def endAddress(self):
            return 0x000212ff

        class _cntoice(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntoice"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntoice"] = _AF6CCI0012_RD_EOSENC._upencnt_enc_oamp1_r2c._cntoice()
            return allFields

    class _upencnt_enc_oamp1_ro(AtRegister.AtRegister):
        def name(self):
            return "ENC CSF Frame Counter"
    
        def description(self):
            return "CSF Packet Send to V-CAT"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x031200 + $Location"
            
        def startAddress(self):
            return 0x00031200
            
        def endAddress(self):
            return 0x000312ff

        class _cntoice(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntoice"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntoice"] = _AF6CCI0012_RD_EOSENC._upencnt_enc_oamp1_ro._cntoice()
            return allFields

    class _upencnt_enc_oamp2_r2c(AtRegister.AtRegister):
        def name(self):
            return "ENC CFM Frame Counter"
    
        def description(self):
            return "CFM Packet Send to V-CAT"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x021300 + $Location"
            
        def startAddress(self):
            return 0x00021300
            
        def endAddress(self):
            return 0x000213ff

        class _cntoice(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntoice"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntoice"] = _AF6CCI0012_RD_EOSENC._upencnt_enc_oamp2_r2c._cntoice()
            return allFields

    class _upencnt_enc_oamp2_ro(AtRegister.AtRegister):
        def name(self):
            return "ENC CFM Frame Counter"
    
        def description(self):
            return "CFM Packet Send to V-CAT"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x031300 + $Location"
            
        def startAddress(self):
            return 0x00031300
            
        def endAddress(self):
            return 0x000313ff

        class _cntoice(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntoice"
            
            def description(self):
                return "Count"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntoice"] = _AF6CCI0012_RD_EOSENC._upencnt_enc_oamp2_ro._cntoice()
            return allFields

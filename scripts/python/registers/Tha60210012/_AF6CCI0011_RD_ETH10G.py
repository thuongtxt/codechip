import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0011_RD_ETH10G(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["eth10g_ver_ctr"] = _AF6CCI0011_RD_ETH10G._eth10g_ver_ctr()
        allRegisters["eth10g_flush_ctr"] = _AF6CCI0011_RD_ETH10G._eth10g_flush_ctr()
        allRegisters["eth10g_protect_ctr"] = _AF6CCI0011_RD_ETH10G._eth10g_protect_ctr()
        allRegisters["eth10g_loop_ctr"] = _AF6CCI0011_RD_ETH10G._eth10g_loop_ctr()
        allRegisters["eth10g_mac_rx_ctr"] = _AF6CCI0011_RD_ETH10G._eth10g_mac_rx_ctr()
        allRegisters["eth10g_mac_tx_ctr"] = _AF6CCI0011_RD_ETH10G._eth10g_mac_tx_ctr()
        allRegisters["upen_mac_vlntpid"] = _AF6CCI0011_RD_ETH10G._upen_mac_vlntpid()
        allRegisters["upen_mac_recalfcs"] = _AF6CCI0011_RD_ETH10G._upen_mac_recalfcs()
        allRegisters["TxEth_cnt0_64_ro"] = _AF6CCI0011_RD_ETH10G._TxEth_cnt0_64_ro()
        allRegisters["TxEth_cnt0_64_rc"] = _AF6CCI0011_RD_ETH10G._TxEth_cnt0_64_rc()
        allRegisters["TxEth_cnt65_127_ro"] = _AF6CCI0011_RD_ETH10G._TxEth_cnt65_127_ro()
        allRegisters["TxEth_cnt65_127_rc"] = _AF6CCI0011_RD_ETH10G._TxEth_cnt65_127_rc()
        allRegisters["TxEth_cnt128_255_ro"] = _AF6CCI0011_RD_ETH10G._TxEth_cnt128_255_ro()
        allRegisters["TxEth_cnt128_255_rc"] = _AF6CCI0011_RD_ETH10G._TxEth_cnt128_255_rc()
        allRegisters["TxEth_cnt256_511_ro"] = _AF6CCI0011_RD_ETH10G._TxEth_cnt256_511_ro()
        allRegisters["TxEth_cnt256_511_rc"] = _AF6CCI0011_RD_ETH10G._TxEth_cnt256_511_rc()
        allRegisters["TxEth_cnt512_1024_ro"] = _AF6CCI0011_RD_ETH10G._TxEth_cnt512_1024_ro()
        allRegisters["TxEth_cnt512_1024_rc"] = _AF6CCI0011_RD_ETH10G._TxEth_cnt512_1024_rc()
        allRegisters["Eth_cnt1025_1528_ro"] = _AF6CCI0011_RD_ETH10G._Eth_cnt1025_1528_ro()
        allRegisters["Eth_cnt1025_1528_rc"] = _AF6CCI0011_RD_ETH10G._Eth_cnt1025_1528_rc()
        allRegisters["TxEth_cnt1529_2047_ro"] = _AF6CCI0011_RD_ETH10G._TxEth_cnt1529_2047_ro()
        allRegisters["TxEth_cnt1529_2047_rc"] = _AF6CCI0011_RD_ETH10G._TxEth_cnt1529_2047_rc()
        allRegisters["TxEth_cnt_jumbo_ro"] = _AF6CCI0011_RD_ETH10G._TxEth_cnt_jumbo_ro()
        allRegisters["TxEth_cnt_jumbo_rc"] = _AF6CCI0011_RD_ETH10G._TxEth_cnt_jumbo_rc()
        allRegisters["eth_tx_pkt_cnt_ro"] = _AF6CCI0011_RD_ETH10G._eth_tx_pkt_cnt_ro()
        allRegisters["eth_tx_pkt_cnt_rc"] = _AF6CCI0011_RD_ETH10G._eth_tx_pkt_cnt_rc()
        allRegisters["TxEth_cnt_byte_ro"] = _AF6CCI0011_RD_ETH10G._TxEth_cnt_byte_ro()
        allRegisters["TxEth_cnt_byte_rc"] = _AF6CCI0011_RD_ETH10G._TxEth_cnt_byte_rc()
        return allRegisters

    class _eth10g_ver_ctr(AtRegister.AtRegister):
        def name(self):
            return "ETH 10G Version Control"
    
        def description(self):
            return "This register checks version ID"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _Eth10gVendor(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Eth10gVendor"
            
            def description(self):
                return "Arrive ETH 10G MAC"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Eth10GVer(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Eth10GVer"
            
            def description(self):
                return "Version ID"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Eth10gVendor"] = _AF6CCI0011_RD_ETH10G._eth10g_ver_ctr._Eth10gVendor()
            allFields["Eth10GVer"] = _AF6CCI0011_RD_ETH10G._eth10g_ver_ctr._Eth10GVer()
            return allFields

    class _eth10g_flush_ctr(AtRegister.AtRegister):
        def name(self):
            return "ETH 10G Flush Control"
    
        def description(self):
            return "This register is used to flush engine"
            
        def width(self):
            return 3
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000001
            
        def endAddress(self):
            return 0xffffffff

        class _Eth10gFlushCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Eth10gFlushCtrl"
            
            def description(self):
                return "Flush engine"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Eth10gFlushCtrl"] = _AF6CCI0011_RD_ETH10G._eth10g_flush_ctr._Eth10gFlushCtrl()
            return allFields

    class _eth10g_protect_ctr(AtRegister.AtRegister):
        def name(self):
            return "ETH 10G Protection Control"
    
        def description(self):
            return "This register is used to flush engine"
            
        def width(self):
            return 13
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0xffffffff

        class _Eth10gRxPortCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Eth10gRxPortCtrl"
            
            def description(self):
                return "Active/Standby port"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Eth10gRxDICCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "Eth10gRxDICCtrl"
            
            def description(self):
                return "enable DIC(deficit idle count) funtion"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Eth10gRxPortCtrl"] = _AF6CCI0011_RD_ETH10G._eth10g_protect_ctr._Eth10gRxPortCtrl()
            allFields["Eth10gRxDICCtrl"] = _AF6CCI0011_RD_ETH10G._eth10g_protect_ctr._Eth10gRxDICCtrl()
            return allFields

    class _eth10g_loop_ctr(AtRegister.AtRegister):
        def name(self):
            return "ETH 10G Loopback Control"
    
        def description(self):
            return "This register is used to configure loopback funtion"
            
        def width(self):
            return 7
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000040
            
        def endAddress(self):
            return 0xffffffff

        class _Eth10gStandPortLoopoutCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Eth10gStandPortLoopoutCtrl"
            
            def description(self):
                return "Standby port loop-out (XGMII rx to XGMII tx)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Eth10gActPortLoopoutCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Eth10gActPortLoopoutCtrl"
            
            def description(self):
                return "Active  port loop-out (XGMII rx to XGMII tx)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Eth10gStandPortLoopinCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Eth10gStandPortLoopinCtrl"
            
            def description(self):
                return "Standby port loop-in (XGMII tx to XGMII rx)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Eth10gActPortLoopinCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Eth10gActPortLoopinCtrl"
            
            def description(self):
                return "Active  port loop-in (XGMII tx to XGMII rx)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Eth10gStandPortLoopoutCtrl"] = _AF6CCI0011_RD_ETH10G._eth10g_loop_ctr._Eth10gStandPortLoopoutCtrl()
            allFields["Eth10gActPortLoopoutCtrl"] = _AF6CCI0011_RD_ETH10G._eth10g_loop_ctr._Eth10gActPortLoopoutCtrl()
            allFields["Eth10gStandPortLoopinCtrl"] = _AF6CCI0011_RD_ETH10G._eth10g_loop_ctr._Eth10gStandPortLoopinCtrl()
            allFields["Eth10gActPortLoopinCtrl"] = _AF6CCI0011_RD_ETH10G._eth10g_loop_ctr._Eth10gActPortLoopinCtrl()
            return allFields

    class _eth10g_mac_rx_ctr(AtRegister.AtRegister):
        def name(self):
            return "ETH 10G MAC Reveive Control"
    
        def description(self):
            return "This register is used to configure MAC at the reveiver direction"
            
        def width(self):
            return 12
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000042
            
        def endAddress(self):
            return 0xffffffff

        class _Eth10gMACRxIpgCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Eth10gMACRxIpgCtrl"
            
            def description(self):
                return "RX inter packet gap"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Eth10gMACRxFCSBypassCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Eth10gMACRxFCSBypassCtrl"
            
            def description(self):
                return "Bypass the received FCS 4-byte"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Eth10gMACRxIpgCtrl"] = _AF6CCI0011_RD_ETH10G._eth10g_mac_rx_ctr._Eth10gMACRxIpgCtrl()
            allFields["Eth10gMACRxFCSBypassCtrl"] = _AF6CCI0011_RD_ETH10G._eth10g_mac_rx_ctr._Eth10gMACRxFCSBypassCtrl()
            return allFields

    class _eth10g_mac_tx_ctr(AtRegister.AtRegister):
        def name(self):
            return "ETH 10G MAC Transmit Control"
    
        def description(self):
            return "This register is used to configure MAC at the reveiver direction"
            
        def width(self):
            return 13
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000043
            
        def endAddress(self):
            return 0xffffffff

        class _Eth10gMACTxIpgCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Eth10gMACTxIpgCtrl"
            
            def description(self):
                return "TX inter packet gap"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Eth10gMACTxFCSInsCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Eth10gMACTxFCSInsCtrl"
            
            def description(self):
                return "Insert FCS 4-byte"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Eth10gMACTxIpgCtrl"] = _AF6CCI0011_RD_ETH10G._eth10g_mac_tx_ctr._Eth10gMACTxIpgCtrl()
            allFields["Eth10gMACTxFCSInsCtrl"] = _AF6CCI0011_RD_ETH10G._eth10g_mac_tx_ctr._Eth10gMACTxFCSInsCtrl()
            return allFields

    class _upen_mac_vlntpid(AtRegister.AtRegister):
        def name(self):
            return "ETH 10G MAC VLAN TPID"
    
        def description(self):
            return "This register is used to configure MAC at the reveiver direction"
            
        def width(self):
            return 13
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000005c
            
        def endAddress(self):
            return 0xffffffff

        class _Eth10gMACTxSVlanTpid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Eth10gMACTxSVlanTpid"
            
            def description(self):
                return "SVLAN TPID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Eth10gMACTxCVlanTpid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Eth10gMACTxCVlanTpid"
            
            def description(self):
                return "CVLAN TPID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Eth10gMACTxSVlanTpid"] = _AF6CCI0011_RD_ETH10G._upen_mac_vlntpid._Eth10gMACTxSVlanTpid()
            allFields["Eth10gMACTxCVlanTpid"] = _AF6CCI0011_RD_ETH10G._upen_mac_vlntpid._Eth10gMACTxCVlanTpid()
            return allFields

    class _upen_mac_recalfcs(AtRegister.AtRegister):
        def name(self):
            return "ETH 10G MAC Recalculate FCS"
    
        def description(self):
            return "This register is used to configure MAC at the reveiver direction"
            
        def width(self):
            return 13
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000005d
            
        def endAddress(self):
            return 0xffffffff

        class _Eth10gMACTxReCalFcs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Eth10gMACTxReCalFcs"
            
            def description(self):
                return "Recalculate FCS"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Eth10gMACTxReCalFcs"] = _AF6CCI0011_RD_ETH10G._upen_mac_recalfcs._Eth10gMACTxReCalFcs()
            return allFields

    class _TxEth_cnt0_64_ro(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count0_64 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 0 to 64 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0003000 + eth_port"
            
        def startAddress(self):
            return 0x00003000
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCnt0_64(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCnt0_64"
            
            def description(self):
                return "This is statistic counter for the packet having 0 to 64 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCnt0_64"] = _AF6CCI0011_RD_ETH10G._TxEth_cnt0_64_ro._TxEthCnt0_64()
            return allFields

    class _TxEth_cnt0_64_rc(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count0_64 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 0 to 64 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0003800 + eth_port"
            
        def startAddress(self):
            return 0x00003800
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCnt0_64(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCnt0_64"
            
            def description(self):
                return "This is statistic counter for the packet having 0 to 64 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCnt0_64"] = _AF6CCI0011_RD_ETH10G._TxEth_cnt0_64_rc._TxEthCnt0_64()
            return allFields

    class _TxEth_cnt65_127_ro(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count65_127 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 65 to 127 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0003002 + eth_port"
            
        def startAddress(self):
            return 0x00003002
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCnt65_127(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCnt65_127"
            
            def description(self):
                return "This is statistic counter for the packet having 65 to 127 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCnt65_127"] = _AF6CCI0011_RD_ETH10G._TxEth_cnt65_127_ro._TxEthCnt65_127()
            return allFields

    class _TxEth_cnt65_127_rc(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count65_127 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 65 to 127 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0003802 + eth_port"
            
        def startAddress(self):
            return 0x00003802
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCnt65_127(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCnt65_127"
            
            def description(self):
                return "This is statistic counter for the packet having 65 to 127 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCnt65_127"] = _AF6CCI0011_RD_ETH10G._TxEth_cnt65_127_rc._TxEthCnt65_127()
            return allFields

    class _TxEth_cnt128_255_ro(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count128_255 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 128 to 255 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0003004 + eth_port"
            
        def startAddress(self):
            return 0x00003004
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCnt128_255(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCnt128_255"
            
            def description(self):
                return "This is statistic counter for the packet having 128 to 255 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCnt128_255"] = _AF6CCI0011_RD_ETH10G._TxEth_cnt128_255_ro._TxEthCnt128_255()
            return allFields

    class _TxEth_cnt128_255_rc(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count128_255 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 128 to 255 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0003804 + eth_port"
            
        def startAddress(self):
            return 0x00003804
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCnt128_255(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCnt128_255"
            
            def description(self):
                return "This is statistic counter for the packet having 128 to 255 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCnt128_255"] = _AF6CCI0011_RD_ETH10G._TxEth_cnt128_255_rc._TxEthCnt128_255()
            return allFields

    class _TxEth_cnt256_511_ro(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count256_511 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 256 to 511 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0003006 + eth_port"
            
        def startAddress(self):
            return 0x00003006
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCnt256_511(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCnt256_511"
            
            def description(self):
                return "This is statistic counter for the packet having 256 to 511 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCnt256_511"] = _AF6CCI0011_RD_ETH10G._TxEth_cnt256_511_ro._TxEthCnt256_511()
            return allFields

    class _TxEth_cnt256_511_rc(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count256_511 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 256 to 511 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0003806 + eth_port"
            
        def startAddress(self):
            return 0x00003806
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCnt256_511(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCnt256_511"
            
            def description(self):
                return "This is statistic counter for the packet having 256 to 511 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCnt256_511"] = _AF6CCI0011_RD_ETH10G._TxEth_cnt256_511_rc._TxEthCnt256_511()
            return allFields

    class _TxEth_cnt512_1024_ro(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count512_1023 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 512 to 1023 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0003008 + eth_port"
            
        def startAddress(self):
            return 0x00003008
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCnt512_1024(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCnt512_1024"
            
            def description(self):
                return "This is statistic counter for the packet having 512 to 1023 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCnt512_1024"] = _AF6CCI0011_RD_ETH10G._TxEth_cnt512_1024_ro._TxEthCnt512_1024()
            return allFields

    class _TxEth_cnt512_1024_rc(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count512_1023 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 512 to 1023 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0003808 + eth_port"
            
        def startAddress(self):
            return 0x00003808
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCnt512_1024(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCnt512_1024"
            
            def description(self):
                return "This is statistic counter for the packet having 512 to 1023 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCnt512_1024"] = _AF6CCI0011_RD_ETH10G._TxEth_cnt512_1024_rc._TxEthCnt512_1024()
            return allFields

    class _Eth_cnt1025_1528_ro(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count1024_1518 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 1024 to 1518 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000300A + eth_port"
            
        def startAddress(self):
            return 0x0000300a
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCnt1025_1528(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCnt1025_1528"
            
            def description(self):
                return "This is statistic counter for the packet having 1024 to 1518 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCnt1025_1528"] = _AF6CCI0011_RD_ETH10G._Eth_cnt1025_1528_ro._TxEthCnt1025_1528()
            return allFields

    class _Eth_cnt1025_1528_rc(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count1024_1518 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 1024 to 1518 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000380A + eth_port"
            
        def startAddress(self):
            return 0x0000380a
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCnt1025_1528(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCnt1025_1528"
            
            def description(self):
                return "This is statistic counter for the packet having 1024 to 1518 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCnt1025_1528"] = _AF6CCI0011_RD_ETH10G._Eth_cnt1025_1528_rc._TxEthCnt1025_1528()
            return allFields

    class _TxEth_cnt1529_2047_ro(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count1519_2047 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 1519 to 2047 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000300C + eth_port"
            
        def startAddress(self):
            return 0x0000300c
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCnt1529_2047(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCnt1529_2047"
            
            def description(self):
                return "This is statistic counter for the packet having 1519 to 2047 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCnt1529_2047"] = _AF6CCI0011_RD_ETH10G._TxEth_cnt1529_2047_ro._TxEthCnt1529_2047()
            return allFields

    class _TxEth_cnt1529_2047_rc(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count1519_2047 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 1519 to 2047 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000380C + eth_port"
            
        def startAddress(self):
            return 0x0000380c
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCnt1529_2047(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCnt1529_2047"
            
            def description(self):
                return "This is statistic counter for the packet having 1519 to 2047 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCnt1529_2047"] = _AF6CCI0011_RD_ETH10G._TxEth_cnt1529_2047_rc._TxEthCnt1529_2047()
            return allFields

    class _TxEth_cnt_jumbo_ro(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count Jumbo packet"
    
        def description(self):
            return "This register is statistic counter for the packet having more than 2048 bytes (jumbo)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000300E + eth_port"
            
        def startAddress(self):
            return 0x0000300e
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCntJumbo(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCntJumbo"
            
            def description(self):
                return "This is statistic counter for the packet more than 2048 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCntJumbo"] = _AF6CCI0011_RD_ETH10G._TxEth_cnt_jumbo_ro._TxEthCntJumbo()
            return allFields

    class _TxEth_cnt_jumbo_rc(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count Jumbo packet"
    
        def description(self):
            return "This register is statistic counter for the packet having more than 2048 bytes (jumbo)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000380E + eth_port"
            
        def startAddress(self):
            return 0x0000380e
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCntJumbo(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCntJumbo"
            
            def description(self):
                return "This is statistic counter for the packet more than 2048 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCntJumbo"] = _AF6CCI0011_RD_ETH10G._TxEth_cnt_jumbo_rc._TxEthCntJumbo()
            return allFields

    class _eth_tx_pkt_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count Total packet"
    
        def description(self):
            return "This register is statistic counter for the total packet at Transmit side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0003012 + eth_port"
            
        def startAddress(self):
            return 0x00003012
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCntTotal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCntTotal"
            
            def description(self):
                return "This is statistic counter total packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCntTotal"] = _AF6CCI0011_RD_ETH10G._eth_tx_pkt_cnt_ro._TxEthCntTotal()
            return allFields

    class _eth_tx_pkt_cnt_rc(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count Total packet"
    
        def description(self):
            return "This register is statistic counter for the total packet at Transmit side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0003812 + eth_port"
            
        def startAddress(self):
            return 0x00003812
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCntTotal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCntTotal"
            
            def description(self):
                return "This is statistic counter total packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCntTotal"] = _AF6CCI0011_RD_ETH10G._eth_tx_pkt_cnt_rc._TxEthCntTotal()
            return allFields

    class _TxEth_cnt_byte_ro(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count number of bytes of packet"
    
        def description(self):
            return "This register is statistic count number of bytes of packet at Transmit side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000301E + eth_port"
            
        def startAddress(self):
            return 0x0000301e
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCntByte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCntByte"
            
            def description(self):
                return "This is statistic counter number of bytes of packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCntByte"] = _AF6CCI0011_RD_ETH10G._TxEth_cnt_byte_ro._TxEthCntByte()
            return allFields

    class _TxEth_cnt_byte_rc(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count number of bytes of packet"
    
        def description(self):
            return "This register is statistic count number of bytes of packet at Transmit side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000381E + eth_port"
            
        def startAddress(self):
            return 0x0000381e
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthCntByte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthCntByte"
            
            def description(self):
                return "This is statistic counter number of bytes of packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthCntByte"] = _AF6CCI0011_RD_ETH10G._TxEth_cnt_byte_rc._TxEthCntByte()
            return allFields

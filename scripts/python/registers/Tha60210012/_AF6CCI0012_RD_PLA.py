import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0012_RD_PLA(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["pla_lo_pld_ctrl"] = _AF6CCI0012_RD_PLA._pla_lo_pld_ctrl()
        allRegisters["pla_lo_add_rmv_pw_ctrl"] = _AF6CCI0012_RD_PLA._pla_lo_add_rmv_pw_ctrl()
        allRegisters["pla_lo_lk_ctrl"] = _AF6CCI0012_RD_PLA._pla_lo_lk_ctrl()
        allRegisters["pla_ho_pldd_ctrl"] = _AF6CCI0012_RD_PLA._pla_ho_pldd_ctrl()
        allRegisters["pla_ho_add_rmv_pw_ctrl"] = _AF6CCI0012_RD_PLA._pla_ho_add_rmv_pw_ctrl()
        allRegisters["pla_ho_lk_ctrl"] = _AF6CCI0012_RD_PLA._pla_ho_lk_ctrl()
        allRegisters["pla_comm_pw_ctrl"] = _AF6CCI0012_RD_PLA._pla_comm_pw_ctrl()
        allRegisters["pla_prot_flow_ctrl"] = _AF6CCI0012_RD_PLA._pla_prot_flow_ctrl()
        allRegisters["pla_flow_upsr_ctrl"] = _AF6CCI0012_RD_PLA._pla_flow_upsr_ctrl()
        allRegisters["pla_flow_hspw_ctrl"] = _AF6CCI0012_RD_PLA._pla_flow_hspw_ctrl()
        allRegisters["pla_blk_buf_ctrl"] = _AF6CCI0012_RD_PLA._pla_blk_buf_ctrl()
        allRegisters["pla_lk_flow_ctrl"] = _AF6CCI0012_RD_PLA._pla_lk_flow_ctrl()
        allRegisters["pla_out_flow_ctrl"] = _AF6CCI0012_RD_PLA._pla_out_flow_ctrl()
        allRegisters["pla_out_psnpro_ctrl"] = _AF6CCI0012_RD_PLA._pla_out_psnpro_ctrl()
        allRegisters["pla_out_psnbuf_ctrl"] = _AF6CCI0012_RD_PLA._pla_out_psnbuf_ctrl()
        allRegisters["pla_hold_reg_ctrl"] = _AF6CCI0012_RD_PLA._pla_hold_reg_ctrl()
        allRegisters["rdha3_0_control"] = _AF6CCI0012_RD_PLA._rdha3_0_control()
        allRegisters["rdha7_4_control"] = _AF6CCI0012_RD_PLA._rdha7_4_control()
        allRegisters["rdha11_8_control"] = _AF6CCI0012_RD_PLA._rdha11_8_control()
        allRegisters["rdha15_12_control"] = _AF6CCI0012_RD_PLA._rdha15_12_control()
        allRegisters["rdha19_16_control"] = _AF6CCI0012_RD_PLA._rdha19_16_control()
        allRegisters["rdha23_20_control"] = _AF6CCI0012_RD_PLA._rdha23_20_control()
        allRegisters["rdha24data_control"] = _AF6CCI0012_RD_PLA._rdha24data_control()
        allRegisters["rdha_hold63_32"] = _AF6CCI0012_RD_PLA._rdha_hold63_32()
        allRegisters["rdindr_hold95_64"] = _AF6CCI0012_RD_PLA._rdindr_hold95_64()
        allRegisters["rdindr_hold127_96"] = _AF6CCI0012_RD_PLA._rdindr_hold127_96()
        allRegisters["pla_out_psnpro_ha_ctrl"] = _AF6CCI0012_RD_PLA._pla_out_psnpro_ha_ctrl()
        allRegisters["pla_force_par_err_control0"] = _AF6CCI0012_RD_PLA._pla_force_par_err_control0()
        allRegisters["pla_dis_par_control0"] = _AF6CCI0012_RD_PLA._pla_dis_par_control0()
        allRegisters["pla_par_stk0"] = _AF6CCI0012_RD_PLA._pla_par_stk0()
        allRegisters["pla_force_crc_err_control"] = _AF6CCI0012_RD_PLA._pla_force_crc_err_control()
        allRegisters["pla_dis_crc_control"] = _AF6CCI0012_RD_PLA._pla_dis_crc_control()
        allRegisters["pla_crc_err_stk"] = _AF6CCI0012_RD_PLA._pla_crc_err_stk()
        return allRegisters

    class _pla_lo_pld_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Low-Order Payload Control"
    
        def description(self):
            return "This register is used to configure payload in each LO Pseudowire channels HDL_PATH  : ipwcore.ipwlocore.ilo48core[$LoOc48Slice].ilo24core[$LoOc24Slice].irtlpla.cfgpwctrl.membuf.ram.ram[$LoPwid]"
            
        def width(self):
            return 17
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0_1000 + $LoOc48Slice*32768 + $LoOc24Slice*8192 + $LoPwid"
            
        def startAddress(self):
            return 0x00001000
            
        def endAddress(self):
            return 0x0000b3ff

        class _PlaLoPldTypeCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 15
        
            def name(self):
                return "PlaLoPldTypeCtrl"
            
            def description(self):
                return "Payload Type 0: satop 1: ces without cas 2: cep"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoPldSizeCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaLoPldSizeCtrl"
            
            def description(self):
                return "Payload Size satop/cep mode: bit[13:0] payload size (ex: value as 0x100 is payload size 256 bytes) ces mode: bit[5:0] number of DS0 timeslot bit[14:6] number of NxDS0 frame"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaLoPldTypeCtrl"] = _AF6CCI0012_RD_PLA._pla_lo_pld_ctrl._PlaLoPldTypeCtrl()
            allFields["PlaLoPldSizeCtrl"] = _AF6CCI0012_RD_PLA._pla_lo_pld_ctrl._PlaLoPldSizeCtrl()
            return allFields

    class _pla_lo_add_rmv_pw_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Low-Order Add/Remove Pseudowire Protocol Control"
    
        def description(self):
            return "This register is used to add/remove pseudowire to prevent burst packets to other packet processing engines in each LO OC-24 slice"
            
        def width(self):
            return 2
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0_1800 + $LoOc48Slice*32768 + $LoOc24Slice*8192 + $LoPwid"
            
        def startAddress(self):
            return 0x00001800
            
        def endAddress(self):
            return 0x0000bbff

        class _PlaLoStaAddRmvCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaLoStaAddRmvCtrl"
            
            def description(self):
                return "protocol state to add/remove pw Step1: Add intitial or pw idle, the protocol state value is \"zero\" Step2: For adding the pw, CPU will Write \"1\" value for the protocol state to start adding pw. The protocol state value is \"1\". Step3: CPU enables pw at demap to finish the adding pw process. HW will automatically change the protocol state value to \"2\" to run the pw, and keep this state. Step4: For removing the pw, CPU will write \"3\" value for the protocol state to start removing pw. The protocol state value is \"3\". Step5: Poll the protocol state until return value \"0\" value, after that CPU disables pw at demap to finish the removing pw process"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaLoStaAddRmvCtrl"] = _AF6CCI0012_RD_PLA._pla_lo_add_rmv_pw_ctrl._PlaLoStaAddRmvCtrl()
            return allFields

    class _pla_lo_lk_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Low-Order Lookup Control"
    
        def description(self):
            return "This register is used to lookup pseudowire per OC-24 slice to a common pseudowire number for both HO & LO path (total is 3072 psedowires) HDL_PATH    : ipwcore.ipwlocore.ilo48core[$LoOc48Slice].pwcfg.membuf.ram.ram[$LoOc24Slice*1024 + $LoPwid]"
            
        def width(self):
            return 12
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0_4000 + $LoOc48Slice*32768 + $LoOc24Slice*1024 + $LoPwid"
            
        def startAddress(self):
            return 0x00004000
            
        def endAddress(self):
            return 0x0000c7ff

        class _PlaLoLkPWIDCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaLoLkPWIDCtrl"
            
            def description(self):
                return "Lookup to a common psedowire"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaLoLkPWIDCtrl"] = _AF6CCI0012_RD_PLA._pla_lo_lk_ctrl._PlaLoLkPWIDCtrl()
            return allFields

    class _pla_ho_pldd_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler High-Order Payload Control"
    
        def description(self):
            return "This register is used to configure payload in each HO Pseudowire channels HDL_PATH     : ipwcore.ipwhocore.iho96core[0].iho48core[$HoOc48Slice].irtlpla.cfgpwctrl.membuf.ram.ram[$HoPwid]"
            
        def width(self):
            return 14
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1_0080 + $HoOc48Slice*256 + $HoPwid"
            
        def startAddress(self):
            return 0x00010080
            
        def endAddress(self):
            return 0x000101af

        class _PlaLoPldSizeCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaLoPldSizeCtrl"
            
            def description(self):
                return "Payload Size"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaLoPldSizeCtrl"] = _AF6CCI0012_RD_PLA._pla_ho_pldd_ctrl._PlaLoPldSizeCtrl()
            return allFields

    class _pla_ho_add_rmv_pw_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Hig-Order Add/Remove Pseudowire Protocol Control"
    
        def description(self):
            return "This register is used to add/remove pseudowire to prevent burst packets to other packet processing engines in each HO OC-48 slice"
            
        def width(self):
            return 2
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1_00C0 + $HoOc48Slice*256 + $HoPwid"
            
        def startAddress(self):
            return 0x000100c0
            
        def endAddress(self):
            return 0x000101ef

        class _PlaLoStaAddRmvCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaLoStaAddRmvCtrl"
            
            def description(self):
                return "protocol state to add/remove pw Step1: Add intitial or pw idle, the protocol state value is \"zero\" Step2: For adding the pw, CPU will Write \"1\" value for the protocol state to start adding pw. The protocol state value is \"1\". Step3: CPU enables pw at demap to finish the adding pw process. HW will automatically change the protocol state value to \"2\" to run the pw, and keep this state. Step4: For removing the pw, CPU will write \"3\" value for the protocol state to start removing pw. The protocol state value is \"3\". Step5: Poll the protocol state until return value \"0\" value, after that CPU disables pw at demap to finish the removing pw process"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaLoStaAddRmvCtrl"] = _AF6CCI0012_RD_PLA._pla_ho_add_rmv_pw_ctrl._PlaLoStaAddRmvCtrl()
            return allFields

    class _pla_ho_lk_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler High-Order Payload Control"
    
        def description(self):
            return "This register is used to lookup pseudowire per OC-484 slice to a common pseudowire number for both HO & LO path (total is 3072 psedowires) HDL_PATH     : ipwcore.ipwhocore.iho96core[0].pwcfg.membuf.ram.ram[$HoOc48Slice*64 + $HoPwid]"
            
        def width(self):
            return 12
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1_0200 + $HoOc48Slice*64 + $HoPwid"
            
        def startAddress(self):
            return 0x00010200
            
        def endAddress(self):
            return 0x0001022f

        class _PlaLoLkPWIDCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaLoLkPWIDCtrl"
            
            def description(self):
                return "Lookup to a common psedowire"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoLkPWIDCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaLoLkPWIDCtrl"
            
            def description(self):
                return "Lookup to a common psedowire"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaLoLkPWIDCtrl"] = _AF6CCI0012_RD_PLA._pla_ho_lk_ctrl._PlaLoLkPWIDCtrl()
            allFields["PlaLoLkPWIDCtrl"] = _AF6CCI0012_RD_PLA._pla_ho_lk_ctrl._PlaLoLkPWIDCtrl()
            return allFields

    class _pla_comm_pw_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Common Pseudowire Control"
    
        def description(self):
            return "This register is used to config for the common psedowire channels HDL_PATH     : ipwcore.ipwcfg.membuf.ram.ram[$pwid]"
            
        def width(self):
            return 9
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1_8000 + $pwid"
            
        def startAddress(self):
            return 0x00018000
            
        def endAddress(self):
            return 0x00018bff

        class _PlaLoPwSuprCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PlaLoPwSuprCtrl"
            
            def description(self):
                return "Suppresion Enable 1: enable 0: disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPwRbitDisCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "PlaPwRbitDisCtrl"
            
            def description(self):
                return "R bit disable 1: disable R bit in the Control Word (assign zero in the packet) 0: normal"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPwMbitDisCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PlaPwMbitDisCtrl"
            
            def description(self):
                return "M or NP bits disable 1: disable M or NP bits in the Control Word (assign zero in the packet) 0: normal"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPwLbitDisCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PlaPwLbitDisCtrl"
            
            def description(self):
                return "L bit disable 1: disable L bit in the Control Word (assign zero in the packet) 0: normal"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPwRbitCPUCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PlaPwRbitCPUCtrl"
            
            def description(self):
                return "R bit value from CPU (low priority than PlaPwRbitDisCtrl)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPwMbitCPUCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PlaPwMbitCPUCtrl"
            
            def description(self):
                return "M or NP bits value from CPU (low priority than PlaLoPwMbitDisCtrl)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPwLbitCPUCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PlaPwLbitCPUCtrl"
            
            def description(self):
                return "L bit value from CPU (low priority than PlaLoPwLbitDisCtrl)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPwEnCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaPwEnCtrl"
            
            def description(self):
                return "Pseudowire enable 1: enable 0: disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaLoPwSuprCtrl"] = _AF6CCI0012_RD_PLA._pla_comm_pw_ctrl._PlaLoPwSuprCtrl()
            allFields["PlaPwRbitDisCtrl"] = _AF6CCI0012_RD_PLA._pla_comm_pw_ctrl._PlaPwRbitDisCtrl()
            allFields["PlaPwMbitDisCtrl"] = _AF6CCI0012_RD_PLA._pla_comm_pw_ctrl._PlaPwMbitDisCtrl()
            allFields["PlaPwLbitDisCtrl"] = _AF6CCI0012_RD_PLA._pla_comm_pw_ctrl._PlaPwLbitDisCtrl()
            allFields["PlaPwRbitCPUCtrl"] = _AF6CCI0012_RD_PLA._pla_comm_pw_ctrl._PlaPwRbitCPUCtrl()
            allFields["PlaPwMbitCPUCtrl"] = _AF6CCI0012_RD_PLA._pla_comm_pw_ctrl._PlaPwMbitCPUCtrl()
            allFields["PlaPwLbitCPUCtrl"] = _AF6CCI0012_RD_PLA._pla_comm_pw_ctrl._PlaPwLbitCPUCtrl()
            allFields["PlaPwEnCtrl"] = _AF6CCI0012_RD_PLA._pla_comm_pw_ctrl._PlaPwEnCtrl()
            return allFields

    class _pla_prot_flow_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Protection Flow Control"
    
        def description(self):
            return "This register is used to config UPSR/HSPW for protect flow HDL_PATH     : ideque.irdport.iapslkcfg.membuf.ram.ram[$flow]"
            
        def width(self):
            return 26
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_0000 + $flow"
            
        def startAddress(self):
            return 0x00030000
            
        def endAddress(self):
            return 0x00020fff

        class _PlaOutUpsrGrpCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 14
        
            def name(self):
                return "PlaOutUpsrGrpCtr"
            
            def description(self):
                return "UPRS group"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutHspwGrpCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PlaOutHspwGrpCtr"
            
            def description(self):
                return "HSPW group"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaFlowUPSRUsedCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PlaFlowUPSRUsedCtrl"
            
            def description(self):
                return "Flow UPSR is used or not 0:not used, all configurations for UPSR will be not effected 1:used"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaFlowHSPWUsedCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaFlowHSPWUsedCtrl"
            
            def description(self):
                return "Flow HSPW is used or not 0:not used, all configurations for HSPW will be not effected 1:used"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaOutUpsrGrpCtr"] = _AF6CCI0012_RD_PLA._pla_prot_flow_ctrl._PlaOutUpsrGrpCtr()
            allFields["PlaOutHspwGrpCtr"] = _AF6CCI0012_RD_PLA._pla_prot_flow_ctrl._PlaOutHspwGrpCtr()
            allFields["PlaFlowUPSRUsedCtrl"] = _AF6CCI0012_RD_PLA._pla_prot_flow_ctrl._PlaFlowUPSRUsedCtrl()
            allFields["PlaFlowHSPWUsedCtrl"] = _AF6CCI0012_RD_PLA._pla_prot_flow_ctrl._PlaFlowHSPWUsedCtrl()
            return allFields

    class _pla_flow_upsr_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Flow UPSR Control"
    
        def description(self):
            return "This register is used to config Flow UPSR group HDL_PATH     : ideque.irdport.iupsrcfg.membuf.ram.ram[$upsrgrp]"
            
        def width(self):
            return 1
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_4000 + $upsrgrp"
            
        def startAddress(self):
            return 0x00034000
            
        def endAddress(self):
            return 0x00034fff

        class _PlaFlowUpsrEnCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaFlowUpsrEnCtr"
            
            def description(self):
                return "enable/disable for upsr group 1: enable 0: disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaFlowUpsrEnCtr"] = _AF6CCI0012_RD_PLA._pla_flow_upsr_ctrl._PlaFlowUpsrEnCtr()
            return allFields

    class _pla_flow_hspw_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Flow HSPW Control"
    
        def description(self):
            return "This register is used to config PW HSPW group HDL_PATH     : ideque.irdport.ihspwcfg.membuf.ram.ram[$hspwgrp]"
            
        def width(self):
            return 1
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_8000 + $hspwgrp"
            
        def startAddress(self):
            return 0x00038000
            
        def endAddress(self):
            return 0x00038fff

        class _PlaPWHSPWPSNCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaPWHSPWPSNCtr"
            
            def description(self):
                return "page for hspw grop 1: PSN page#1 0: PSN page#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaPWHSPWPSNCtr"] = _AF6CCI0012_RD_PLA._pla_flow_hspw_ctrl._PlaPWHSPWPSNCtr()
            return allFields

    class _pla_blk_buf_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Block Buffer Control"
    
        def description(self):
            return "This register is used to configure BlockID for the buffer for all services (Psedowire/iMSG/VCAT) HDL_PATH:begin: IF($cid<8192) iblkcfg_ram.iram_tdp_low.ram_name[$cid] ELSE iblkcfg_ram.iram_tdp_high.ram_name[$cid-8192] HDL_PATH:end:"
            
        def width(self):
            return 15
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x2_4000 + $cid"
            
        def startAddress(self):
            return 0x00024000
            
        def endAddress(self):
            return 0x000263ff

        class _PlaBlkIDCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 3
        
            def name(self):
                return "PlaBlkIDCtr"
            
            def description(self):
                return "Block Buffer Number There are 4096 blocks for LO rate (BlkType = 0), a block location for eache LO rate channel. There are 96 block location for HO rate (BlkType != 0), a block location for eache DS3/E3/STS1 rate channel, Formula for HO alocation is HoOc12STS*8 + HoOc12Slice*4 + HoOc24Slice*2 + HoOc48Slice Depend on BlockType, may there some configuration different as below: STS48c: HoOc12STS = 0,HoOc12Slice = 0,HoOc24Slice = 0,HoOc48Slice(0-1) = OC48 ID STS24c: HoOC12STS = 0,HoOc12Slice = 0,HoOc24Slice(0-1) = OC24 ID per OC48,HoOc48Slice(0-1) = OC48 ID STS12c: HoOC12STS = 0,HoOc12Slice(0-1) = OC12 ID per OC24,HoOc24Slice(0-1) = OC24 ID per OC48,HoOc48Slice(0-1) = OC48 ID STS3c : HoOC12STS(0-11) = start STS ID of a STS3c per OC12 ,HoOc12Slice(0-1) = OC12 ID per OC24,HoOc24Slice(0-1) = OC24 ID per OC48,HoOc48Slice(0-1) = OC48 ID DS3/E3/STS1: HoOC12STS(0-11) = STS ID of a STS1 per OC12 ,HoOc12Slice(0-1) = OC12 ID per OC24,HoOc24Slice(0-1) = OC24 ID per OC48,HoOc48Slice(0-1) = OC48 ID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaBlkTypeCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaBlkTypeCtr"
            
            def description(self):
                return "Type of TDM rate used this block 0: DS0/DS1/E1/VC1x 1: DS3/E3/STS1 2: STS3c 3: STS12c 4: STS24c 5: STS48c"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaBlkIDCtr"] = _AF6CCI0012_RD_PLA._pla_blk_buf_ctrl._PlaBlkIDCtr()
            allFields["PlaBlkTypeCtr"] = _AF6CCI0012_RD_PLA._pla_blk_buf_ctrl._PlaBlkTypeCtr()
            return allFields

    class _pla_lk_flow_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Lookup Flow Control"
    
        def description(self):
            return "This register is used to lookup from (PW/VCG) to flow ID HDL_PATH     : iflowcfg.membuf.ram.ram[$cid]"
            
        def width(self):
            return 12
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x2_C000 + $cid"
            
        def startAddress(self):
            return 0x0002c000
            
        def endAddress(self):
            return 0x0002ccff

        class _PlaOutLkFlowCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaOutLkFlowCtrl"
            
            def description(self):
                return "Lookup FlowID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaOutLkFlowCtrl"] = _AF6CCI0012_RD_PLA._pla_lk_flow_ctrl._PlaOutLkFlowCtrl()
            return allFields

    class _pla_out_flow_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Output Flow Control"
    
        def description(self):
            return "This register is used to config flow at output HDL_PATH     : ideque.irdport.iflowcfg.membuf.ram.ram[$flow]"
            
        def width(self):
            return 7
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x2_8000 + $flow"
            
        def startAddress(self):
            return 0x00028000
            
        def endAddress(self):
            return 0x00028fff

        class _PlaOutPSNLenCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaOutPSNLenCtrl"
            
            def description(self):
                return "PSN length"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaOutPSNLenCtrl"] = _AF6CCI0012_RD_PLA._pla_out_flow_ctrl._PlaOutPSNLenCtrl()
            return allFields

    class _pla_out_psnpro_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Output PSN Process Control"
    
        def description(self):
            return "This register is used to control PSN configuration"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00020000
            
        def endAddress(self):
            return 0xffffffff

        class _PlaOutPsnProReqCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "PlaOutPsnProReqCtr"
            
            def description(self):
                return "Request to process PSN 1: request to write/read PSN header buffer. The CPU need to prepare a complete PSN header into PSN buffer before request write OR read out a complete PSN header in PSN buffer after request read done 0: HW will automatically set to 0 when a request done"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutPsnProRnWCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "PlaOutPsnProRnWCtr"
            
            def description(self):
                return "read or write PSN 1: read PSN 0: write PSN"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutPsnProLenCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PlaOutPsnProLenCtr"
            
            def description(self):
                return "Length of a complete PSN need to read/write"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutPsnProPageCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "PlaOutPsnProPageCtr"
            
            def description(self):
                return "there is 2 pages PSN location per PWID, depend on the configuration of \"PlaOutHspwPsnCtr\", the engine will choice which the page to encapsulate into the packet."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutPsnProFlowCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaOutPsnProFlowCtr"
            
            def description(self):
                return "Flow channel"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaOutPsnProReqCtr"] = _AF6CCI0012_RD_PLA._pla_out_psnpro_ctrl._PlaOutPsnProReqCtr()
            allFields["PlaOutPsnProRnWCtr"] = _AF6CCI0012_RD_PLA._pla_out_psnpro_ctrl._PlaOutPsnProRnWCtr()
            allFields["PlaOutPsnProLenCtr"] = _AF6CCI0012_RD_PLA._pla_out_psnpro_ctrl._PlaOutPsnProLenCtr()
            allFields["PlaOutPsnProPageCtr"] = _AF6CCI0012_RD_PLA._pla_out_psnpro_ctrl._PlaOutPsnProPageCtr()
            allFields["PlaOutPsnProFlowCtr"] = _AF6CCI0012_RD_PLA._pla_out_psnpro_ctrl._PlaOutPsnProFlowCtr()
            return allFields

    class _pla_out_psnbuf_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Output PSN Buffer Control"
    
        def description(self):
            return "This register is used to store PSN data which is before CPU request write or after CPU request read %% The header format from entry#0 to entry#4 is as follow: %% {DA(6-byte),SA(6-byte),VLAN1(4-byte,optional),VLAN2(4-byte, optional),EthType(2-byte),PSN Header(4 to 96 bytes)} %% Depends on specific PSN selected for each pseudowire, the EthType and PSN Header field may have the following values and formats: %%  PSN header is MEF-8: %% EthType:  0x88D8 %% 4-byte PSN Header with format: {ECID[19:0], 0x102} where ECID is pseodowire identification for 	remote 	receive side. %% PSN header is MPLS:  %% EthType:  0x8847 %% 4/8/12-byte PSN Header with format: {OuterLabel2[31:0](optional), OuterLabel1[31:0](optional),  InnerLabel[31:0]}   %% Where InnerLabel[31:12] is pseodowire identification for remote receive side. %% Label format: {Idenfifier[19:0],Exp[2:0],StackBit,TTL[7:0]} where StackBit =  for InnerLabel %% PSN header is UDP/Ipv4:  %% EthType:  0x0800 %% 28-byte PSN Header with format {Ipv4 Header(20 bytes), UDP Header(8 byte)} as below: %% {IP_Ver[3:0], IP_IHL[3:0], IP_ToS[7:0], IP_Header_Sum[31:16]} %% {IP_Iden[15:0], IP_Flag[2:0], IP_Frag_Offset[12:0]}  %% {IP_TTL[7:0], IP_Protocol[7:0], IP_Header_Sum[15:0]} %% {IP_SrcAdr[31:0]} %% {IP_DesAdr[31:0]} %% {UDP_SrcPort[15:0], UDP_DesPort[15:0]} %% {UDP_Sum[31:0] (optional)} %% Case: %% IP_Protocol[7:0]: 0x11 to signify UDP  %% IP_Protocol[7:0]: 0x11 to signify UDP  %% IP_Header_Sum[31:0]:  CPU calculate these fields as a temporarily for HW before plus rest fields %% {IP_Ver[3:0], IP_IHL[3:0], IP_ToS[7:0]} +%% IP_Iden[15:0] + {IP_Flag[2:0], IP_Frag_Offset[12:0]}  +%% {IP_TTL[7:0], IP_Protocol[7:0]} + %% IP_SrcAdr[31:16] + IP_SrcAdr[15:0] +%% IP_DesAdr[31:16] + IP_DesAdr[15:0]%% UDP_SrcPort: used as remote pseudowire identification or 0x85E if unused%% UDP_DesPort : used as remote pseudowire identification or 0x85E if unused%% UDP_Sum[31:0] (optional): CPU calculate these fields as a temporarily for HW before plus rest fields%% IP_SrcAdr[127:112] + 	IP_SrcAdr[111:96] + 	%% IP_SrcAdr[95:80]  + IP_SrcAdr[79:64] + 	%% IP_SrcAdr[63:48]  + IP_SrcAdr[47:32] + 	%% IP_SrcAdr[31:16]  + IP_SrcAdr[15:0] + 	%% IP_DesAdr[127:112] + 	IP_DesAdr[111:96] + 	%% IP_DesAdr[95:80]  + IP_DesAdr[79:64] + 	%% IP_DesAdr[63:48]  + IP_DesAdr[47:32] + 	%% IP_DesAdr[31:16]  + IP_DesAdr[15:0] +%% {8-bit zeros, IP_Protocol[7:0]} +%% UDP_SrcPort[15:0] +  UDP_DesPort[15:0]%% PSN header is UDP/Ipv6:  %% EthType:  0x86DD%% 48-byte PSN Header with format {Ipv6 Header(40 bytes), UDP Header(8 byte)} as below:%% {IP_Ver[3:0], IP_Traffic_Class[7:0], IP_Flow_Label[19:0]}%% {16-bit zeros, IP_Next_Header[7:0], IP_Hop_Limit[7:0]} %% {IP_SrcAdr[127:96]}%% {IP_SrcAdr[95:64]}%% {IP_SrcAdr[63:32]}%% {IP_SrcAdr[31:0]}%% {IP_DesAdr[127:96]}%% {IP_DesAdr[95:64]}%% {IP_DesAdr[63:32]}%% {IP_DesAdr[31:0]}%% {UDP_SrcPort[15:0], UDP_DesPort[15:0]}%% {UDP_Sum[31:0]}%% Case:%% IP_Next_Header[7:0]: 0x11 to signify UDP %% UDP_Sum[31:0]:  CPU calculate these fields as a temporarily for HW before plus rest fields%% IP_SrcAdr[127:112] + 	IP_SrcAdr[111:96] + 	%% IP_SrcAdr[95:80]  + IP_SrcAdr[79:64] + 	%% IP_SrcAdr[63:48]  + IP_SrcAdr[47:32] + 	%% IP_SrcAdr[31:16]  + IP_SrcAdr[15:0] + 	%% IP_DesAdr[127:112] + 	IP_DesAdr[111:96] + 	%% IP_DesAdr[95:80]  + IP_DesAdr[79:64] + 	%% IP_DesAdr[63:48]  + IP_DesAdr[47:32] + 	%% IP_DesAdr[31:16]  + IP_DesAdr[15:0] +%% {8-bit zeros, IP_Next_Header[7:0]} +%% UDP_SrcPort[15:0] +  UDP_DesPort[15:0]%% UDP_SrcPort: used as remote pseudowire identification or 0x85E if unused%% UDP_DesPort : used as remote pseudowire identification or 0x85E if unused%% User can select either source or destination port for pseodowire identification. See IP/UDP standards for more description about other field. %% PSN header is MPLS over Ipv4:%% IPv4 header must have IP_Protocol[7:0] value 0x89 to signify MPLS%% After IPv4 header is MPLS labels where inner label is used for PW identification%% PSN header is MPLS over Ipv6:%% IPv6 header must have IP_Next_Header[7:0] value 0x89 to signify MPLS%% After IPv6 header is MPLS labels where inner label is used for PW identification%% PSN header (all modes) with RTP enable: RTP used for TDM PW (define in RFC3550), is in the first 8-byte of this buffer (bit[127:64] of segid == 0), following is PSN header (start from bit[63:0] of segid = 0)%% Case:%% RTPSSRC[31:0] : This is the SSRC value of RTP header%% RtpPtValue[6:0]: This is the PT value of RTP header, define in http://www.iana.org/assignments/rtp-parameters/rtp-parameters.xml"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x2_0010 + $segid"
            
        def startAddress(self):
            return 0x00020010
            
        def endAddress(self):
            return 0x00020015

        class _PlaOutPsnProReqCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaOutPsnProReqCtr"
            
            def description(self):
                return "PSN buffer"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaOutPsnProReqCtr"] = _AF6CCI0012_RD_PLA._pla_out_psnbuf_ctrl._PlaOutPsnProReqCtr()
            return allFields

    class _pla_hold_reg_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Hold Register Control"
    
        def description(self):
            return "This register is used to control hold register."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x2_1000 + $holdreg"
            
        def startAddress(self):
            return 0x00021000
            
        def endAddress(self):
            return 0x00021002

        class _PlaHoldRegCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaHoldRegCtr"
            
            def description(self):
                return "hold register value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaHoldRegCtr"] = _AF6CCI0012_RD_PLA._pla_hold_reg_ctrl._PlaHoldRegCtr()
            return allFields

    class _rdha3_0_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit3_0 Control"
    
        def description(self):
            return "This register is used to send HA read address bit3_0 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x2_1100 + HaAddr3_0"
            
        def startAddress(self):
            return 0x00021100
            
        def endAddress(self):
            return 0x0002110f

        class _ReadAddr3_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr3_0"
            
            def description(self):
                return "Read value will be 0x8C100 plus HaAddr3_0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr3_0"] = _AF6CCI0012_RD_PLA._rdha3_0_control._ReadAddr3_0()
            return allFields

    class _rdha7_4_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit7_4 Control"
    
        def description(self):
            return "This register is used to send HA read address bit7_4 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x2_1110 + HaAddr7_4"
            
        def startAddress(self):
            return 0x00021110
            
        def endAddress(self):
            return 0x0002111f

        class _ReadAddr7_4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr7_4"
            
            def description(self):
                return "Read value will be 0x8C100 plus HaAddr7_4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr7_4"] = _AF6CCI0012_RD_PLA._rdha7_4_control._ReadAddr7_4()
            return allFields

    class _rdha11_8_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit11_8 Control"
    
        def description(self):
            return "This register is used to send HA read address bit11_8 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x2_1120 + HaAddr11_8"
            
        def startAddress(self):
            return 0x00021120
            
        def endAddress(self):
            return 0x0002112f

        class _ReadAddr11_8(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr11_8"
            
            def description(self):
                return "Read value will be 0x8C100 plus HaAddr11_8"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr11_8"] = _AF6CCI0012_RD_PLA._rdha11_8_control._ReadAddr11_8()
            return allFields

    class _rdha15_12_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit15_12 Control"
    
        def description(self):
            return "This register is used to send HA read address bit15_12 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x2_1130 + HaAddr15_12"
            
        def startAddress(self):
            return 0x00021130
            
        def endAddress(self):
            return 0x0002113f

        class _ReadAddr15_12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr15_12"
            
            def description(self):
                return "Read value will be 0x8C100 plus HaAddr15_12"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr15_12"] = _AF6CCI0012_RD_PLA._rdha15_12_control._ReadAddr15_12()
            return allFields

    class _rdha19_16_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit19_16 Control"
    
        def description(self):
            return "This register is used to send HA read address bit19_16 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x2_1140 + HaAddr19_16"
            
        def startAddress(self):
            return 0x00021140
            
        def endAddress(self):
            return 0x0002114f

        class _ReadAddr19_16(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr19_16"
            
            def description(self):
                return "Read value will be 0x8C100 plus HaAddr19_16"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr19_16"] = _AF6CCI0012_RD_PLA._rdha19_16_control._ReadAddr19_16()
            return allFields

    class _rdha23_20_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit23_20 Control"
    
        def description(self):
            return "This register is used to send HA read address bit23_20 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x2_1150 + HaAddr23_20"
            
        def startAddress(self):
            return 0x00021150
            
        def endAddress(self):
            return 0x0002115f

        class _ReadAddr23_20(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr23_20"
            
            def description(self):
                return "Read value will be 0x8C100 plus HaAddr23_20"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr23_20"] = _AF6CCI0012_RD_PLA._rdha23_20_control._ReadAddr23_20()
            return allFields

    class _rdha24data_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit24 and Data Control"
    
        def description(self):
            return "This register is used to send HA read address bit24 to HA engine to read data"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x2_1160 + HaAddr24"
            
        def startAddress(self):
            return 0x00021160
            
        def endAddress(self):
            return 0x00021161

        class _ReadHaData31_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData31_0"
            
            def description(self):
                return "HA read data bit31_0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData31_0"] = _AF6CCI0012_RD_PLA._rdha24data_control._ReadHaData31_0()
            return allFields

    class _rdha_hold63_32(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data63_32"
    
        def description(self):
            return "This register is used to read HA dword2 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00021170
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData63_32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData63_32"
            
            def description(self):
                return "HA read data bit63_32"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData63_32"] = _AF6CCI0012_RD_PLA._rdha_hold63_32._ReadHaData63_32()
            return allFields

    class _rdindr_hold95_64(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data95_64"
    
        def description(self):
            return "This register is used to read HA dword3 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00021171
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData95_64(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData95_64"
            
            def description(self):
                return "HA read data bit95_64"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData95_64"] = _AF6CCI0012_RD_PLA._rdindr_hold95_64._ReadHaData95_64()
            return allFields

    class _rdindr_hold127_96(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data127_96"
    
        def description(self):
            return "This register is used to read HA dword4 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00021172
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData127_96(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData127_96"
            
            def description(self):
                return "HA read data bit127_96"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData127_96"] = _AF6CCI0012_RD_PLA._rdindr_hold127_96._ReadHaData127_96()
            return allFields

    class _pla_out_psnpro_ha_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Output PSN Process Control"
    
        def description(self):
            return "This register is used to control PSN configuration"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00020001
            
        def endAddress(self):
            return 0xffffffff

        class _PlaOutPsnProReqCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "PlaOutPsnProReqCtr"
            
            def description(self):
                return "Request to process PSN 1: request to write/read PSN header buffer. The CPU need to prepare a complete PSN header into PSN buffer before request write OR read out a complete PSN header in PSN buffer after request read done 0: HW will automatically set to 0 when a request done"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutPsnProLenCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PlaOutPsnProLenCtr"
            
            def description(self):
                return "Length of a complete PSN need to read/write"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutPsnProPageCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "PlaOutPsnProPageCtr"
            
            def description(self):
                return "there is 2 pages PSN location per PWID, depend on the configuration of \"PlaOutHspwPsnCtr\", the engine will choice which the page to encapsulate into the packet."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutPsnProFlowCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaOutPsnProFlowCtr"
            
            def description(self):
                return "Flow channel"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaOutPsnProReqCtr"] = _AF6CCI0012_RD_PLA._pla_out_psnpro_ha_ctrl._PlaOutPsnProReqCtr()
            allFields["PlaOutPsnProLenCtr"] = _AF6CCI0012_RD_PLA._pla_out_psnpro_ha_ctrl._PlaOutPsnProLenCtr()
            allFields["PlaOutPsnProPageCtr"] = _AF6CCI0012_RD_PLA._pla_out_psnpro_ha_ctrl._PlaOutPsnProPageCtr()
            allFields["PlaOutPsnProFlowCtr"] = _AF6CCI0012_RD_PLA._pla_out_psnpro_ha_ctrl._PlaOutPsnProFlowCtr()
            return allFields

    class _pla_force_par_err_control0(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Force Parity Error Control0"
    
        def description(self):
            return "This register is used to force parity error"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00021030
            
        def endAddress(self):
            return 0xffffffff

        class _ForceParErrBlkBufCtrlReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "ForceParErrBlkBufCtrlReg"
            
            def description(self):
                return "Force Parity Error Block Buffer Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrLkFlowCtrlReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "ForceParErrLkFlowCtrlReg"
            
            def description(self):
                return "Force Parity Error Lookup Flow Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrCommonPWCtrlReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "ForceParErrCommonPWCtrlReg"
            
            def description(self):
                return "Force Parity Error Common Psedowire Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrUPSRFlowCtrReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "ForceParErrUPSRFlowCtrReg"
            
            def description(self):
                return "Force Parity Error UPSR Flow Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrHSPWFlowCtrReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "ForceParErrHSPWFlowCtrReg"
            
            def description(self):
                return "Force Parity Error HSPW Flow Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrProFlowCtrReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "ForceParErrProFlowCtrReg"
            
            def description(self):
                return "Force Parity Error Protection Flow Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrOutFlowCtrReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ForceParErrOutFlowCtrReg"
            
            def description(self):
                return "Force Parity Error Output Flow Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrHOLkCtrRegOC96_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "ForceParErrHOLkCtrRegOC96_0"
            
            def description(self):
                return "Force Parity Error HO PWE Control Register of OC96#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrHOPldCtrRegOC96_0_OC48_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "ForceParErrHOPldCtrRegOC96_0_OC48_1"
            
            def description(self):
                return "Force Parity Error HO Payload Control Register of OC96#0 OC#48#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrHOPldCtrRegOC96_0_OC48_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "ForceParErrHOPldCtrRegOC96_0_OC48_0"
            
            def description(self):
                return "Force Parity Error HO Payload Control Register of OC96#0 OC#48#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrLOLkCtrRegOC48_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "ForceParErrLOLkCtrRegOC48_1"
            
            def description(self):
                return "Force Parity Error LO PWE Control Register of OC48#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrLOPldCtrRegOC48_1_OC24_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "ForceParErrLOPldCtrRegOC48_1_OC24_1"
            
            def description(self):
                return "Force Parity Error LO Payload Control Register of OC48#1 OC#24#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrLOPldCtrRegOC48_1_OC24_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "ForceParErrLOPldCtrRegOC48_1_OC24_0"
            
            def description(self):
                return "Force Parity Error LO Payload Control Register of OC48#1 OC#24#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrLOLkCtrRegOC48_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "ForceParErrLOLkCtrRegOC48_0"
            
            def description(self):
                return "Force Parity Error LO PWE Control Register of OC48#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrLOPldCtrRegOC48_0_OC24_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "ForceParErrLOPldCtrRegOC48_0_OC24_1"
            
            def description(self):
                return "Force Parity Error LO Payload Control Register of OC48#0 OC#24#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrLOPldCtrRegOC48_0_OC24_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ForceParErrLOPldCtrRegOC48_0_OC24_0"
            
            def description(self):
                return "Force Parity Error LO Payload Control Register of OC48#0 OC#24#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ForceParErrBlkBufCtrlReg"] = _AF6CCI0012_RD_PLA._pla_force_par_err_control0._ForceParErrBlkBufCtrlReg()
            allFields["ForceParErrLkFlowCtrlReg"] = _AF6CCI0012_RD_PLA._pla_force_par_err_control0._ForceParErrLkFlowCtrlReg()
            allFields["ForceParErrCommonPWCtrlReg"] = _AF6CCI0012_RD_PLA._pla_force_par_err_control0._ForceParErrCommonPWCtrlReg()
            allFields["ForceParErrUPSRFlowCtrReg"] = _AF6CCI0012_RD_PLA._pla_force_par_err_control0._ForceParErrUPSRFlowCtrReg()
            allFields["ForceParErrHSPWFlowCtrReg"] = _AF6CCI0012_RD_PLA._pla_force_par_err_control0._ForceParErrHSPWFlowCtrReg()
            allFields["ForceParErrProFlowCtrReg"] = _AF6CCI0012_RD_PLA._pla_force_par_err_control0._ForceParErrProFlowCtrReg()
            allFields["ForceParErrOutFlowCtrReg"] = _AF6CCI0012_RD_PLA._pla_force_par_err_control0._ForceParErrOutFlowCtrReg()
            allFields["ForceParErrHOLkCtrRegOC96_0"] = _AF6CCI0012_RD_PLA._pla_force_par_err_control0._ForceParErrHOLkCtrRegOC96_0()
            allFields["ForceParErrHOPldCtrRegOC96_0_OC48_1"] = _AF6CCI0012_RD_PLA._pla_force_par_err_control0._ForceParErrHOPldCtrRegOC96_0_OC48_1()
            allFields["ForceParErrHOPldCtrRegOC96_0_OC48_0"] = _AF6CCI0012_RD_PLA._pla_force_par_err_control0._ForceParErrHOPldCtrRegOC96_0_OC48_0()
            allFields["ForceParErrLOLkCtrRegOC48_1"] = _AF6CCI0012_RD_PLA._pla_force_par_err_control0._ForceParErrLOLkCtrRegOC48_1()
            allFields["ForceParErrLOPldCtrRegOC48_1_OC24_1"] = _AF6CCI0012_RD_PLA._pla_force_par_err_control0._ForceParErrLOPldCtrRegOC48_1_OC24_1()
            allFields["ForceParErrLOPldCtrRegOC48_1_OC24_0"] = _AF6CCI0012_RD_PLA._pla_force_par_err_control0._ForceParErrLOPldCtrRegOC48_1_OC24_0()
            allFields["ForceParErrLOLkCtrRegOC48_0"] = _AF6CCI0012_RD_PLA._pla_force_par_err_control0._ForceParErrLOLkCtrRegOC48_0()
            allFields["ForceParErrLOPldCtrRegOC48_0_OC24_1"] = _AF6CCI0012_RD_PLA._pla_force_par_err_control0._ForceParErrLOPldCtrRegOC48_0_OC24_1()
            allFields["ForceParErrLOPldCtrRegOC48_0_OC24_0"] = _AF6CCI0012_RD_PLA._pla_force_par_err_control0._ForceParErrLOPldCtrRegOC48_0_OC24_0()
            return allFields

    class _pla_dis_par_control0(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Disable Parity Check Control0"
    
        def description(self):
            return "This register is used to disable parity check"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00021031
            
        def endAddress(self):
            return 0xffffffff

        class _DisParErrBlkBufCtrlReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "DisParErrBlkBufCtrlReg"
            
            def description(self):
                return "Disable Parity Error Block Buffer Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParErrLkFlowCtrlReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "DisParErrLkFlowCtrlReg"
            
            def description(self):
                return "Disable Parity Error Lookup Flow Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParErrCommonPWCtrlReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "DisParErrCommonPWCtrlReg"
            
            def description(self):
                return "Disable Parity Error Common Psedowire Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParErrUPSRFlowCtrReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "DisParErrUPSRFlowCtrReg"
            
            def description(self):
                return "Disable Parity Error UPSR Flow Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParErrHSPWFlowCtrReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "DisParErrHSPWFlowCtrReg"
            
            def description(self):
                return "Disable Parity Error HSPW Flow Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParErrProFlowCtrReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "DisParErrProFlowCtrReg"
            
            def description(self):
                return "Disable Parity Error Protection Flow Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParErrOutFlowCtrReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "DisParErrOutFlowCtrReg"
            
            def description(self):
                return "Disable Parity Error Output Flow Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParErrHOLkCtrRegOC96_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "DisParErrHOLkCtrRegOC96_0"
            
            def description(self):
                return "Disable Parity Error HO PWE Control Register of OC96#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParErrHOPldCtrRegOC96_0_OC48_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "DisParErrHOPldCtrRegOC96_0_OC48_1"
            
            def description(self):
                return "Disable Parity Error HO Payload Control Register of OC96#0 OC#48#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParErrHOPldCtrRegOC96_0_OC48_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "DisParErrHOPldCtrRegOC96_0_OC48_0"
            
            def description(self):
                return "Disable Parity Error HO Payload Control Register of OC96#0 OC#48#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParErrLOLkCtrRegOC48_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "DisParErrLOLkCtrRegOC48_1"
            
            def description(self):
                return "Disable Parity Error LO PWE Control Register of OC48#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParErrLOPldCtrRegOC48_1_OC24_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "DisParErrLOPldCtrRegOC48_1_OC24_1"
            
            def description(self):
                return "Disable Parity Error LO Payload Control Register of OC48#1 OC#24#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParErrLOPldCtrRegOC48_1_OC24_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "DisParErrLOPldCtrRegOC48_1_OC24_0"
            
            def description(self):
                return "Disable Parity Error LO Payload Control Register of OC48#1 OC#24#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParErrLOLkCtrRegOC48_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "DisParErrLOLkCtrRegOC48_0"
            
            def description(self):
                return "Disable Parity Error LO PWE Control Register of OC48#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParErrLOPldCtrRegOC48_0_OC24_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "DisParErrLOPldCtrRegOC48_0_OC24_1"
            
            def description(self):
                return "Disable Parity Error LO Payload Control Register of OC48#0 OC#24#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParErrLOPldCtrRegOC48_0_OC24_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DisParErrLOPldCtrRegOC48_0_OC24_0"
            
            def description(self):
                return "Disable Parity Error LO Payload Control Register of OC48#0 OC#24#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DisParErrBlkBufCtrlReg"] = _AF6CCI0012_RD_PLA._pla_dis_par_control0._DisParErrBlkBufCtrlReg()
            allFields["DisParErrLkFlowCtrlReg"] = _AF6CCI0012_RD_PLA._pla_dis_par_control0._DisParErrLkFlowCtrlReg()
            allFields["DisParErrCommonPWCtrlReg"] = _AF6CCI0012_RD_PLA._pla_dis_par_control0._DisParErrCommonPWCtrlReg()
            allFields["DisParErrUPSRFlowCtrReg"] = _AF6CCI0012_RD_PLA._pla_dis_par_control0._DisParErrUPSRFlowCtrReg()
            allFields["DisParErrHSPWFlowCtrReg"] = _AF6CCI0012_RD_PLA._pla_dis_par_control0._DisParErrHSPWFlowCtrReg()
            allFields["DisParErrProFlowCtrReg"] = _AF6CCI0012_RD_PLA._pla_dis_par_control0._DisParErrProFlowCtrReg()
            allFields["DisParErrOutFlowCtrReg"] = _AF6CCI0012_RD_PLA._pla_dis_par_control0._DisParErrOutFlowCtrReg()
            allFields["DisParErrHOLkCtrRegOC96_0"] = _AF6CCI0012_RD_PLA._pla_dis_par_control0._DisParErrHOLkCtrRegOC96_0()
            allFields["DisParErrHOPldCtrRegOC96_0_OC48_1"] = _AF6CCI0012_RD_PLA._pla_dis_par_control0._DisParErrHOPldCtrRegOC96_0_OC48_1()
            allFields["DisParErrHOPldCtrRegOC96_0_OC48_0"] = _AF6CCI0012_RD_PLA._pla_dis_par_control0._DisParErrHOPldCtrRegOC96_0_OC48_0()
            allFields["DisParErrLOLkCtrRegOC48_1"] = _AF6CCI0012_RD_PLA._pla_dis_par_control0._DisParErrLOLkCtrRegOC48_1()
            allFields["DisParErrLOPldCtrRegOC48_1_OC24_1"] = _AF6CCI0012_RD_PLA._pla_dis_par_control0._DisParErrLOPldCtrRegOC48_1_OC24_1()
            allFields["DisParErrLOPldCtrRegOC48_1_OC24_0"] = _AF6CCI0012_RD_PLA._pla_dis_par_control0._DisParErrLOPldCtrRegOC48_1_OC24_0()
            allFields["DisParErrLOLkCtrRegOC48_0"] = _AF6CCI0012_RD_PLA._pla_dis_par_control0._DisParErrLOLkCtrRegOC48_0()
            allFields["DisParErrLOPldCtrRegOC48_0_OC24_1"] = _AF6CCI0012_RD_PLA._pla_dis_par_control0._DisParErrLOPldCtrRegOC48_0_OC24_1()
            allFields["DisParErrLOPldCtrRegOC48_0_OC24_0"] = _AF6CCI0012_RD_PLA._pla_dis_par_control0._DisParErrLOPldCtrRegOC48_0_OC24_0()
            return allFields

    class _pla_par_stk0(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Parity Error Sticky0"
    
        def description(self):
            return "This register is used to sticky parity check"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00021032
            
        def endAddress(self):
            return 0xffffffff

        class _ParErrBlkBufCtrlRegStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "ParErrBlkBufCtrlRegStk"
            
            def description(self):
                return "Sticky Parity Error Block Buffer Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParErrLkFlowCtrlRegStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "ParErrLkFlowCtrlRegStk"
            
            def description(self):
                return "Sticky Parity Error Lookup Flow Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParErrCommonPWCtrlRegStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "ParErrCommonPWCtrlRegStk"
            
            def description(self):
                return "Sticky Parity Error Common Psedowire Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParErrUPSRFlowCtrRegStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "ParErrUPSRFlowCtrRegStk"
            
            def description(self):
                return "Sticky Parity Error UPSR Flow Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParErrHSPWFlowCtrRegStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "ParErrHSPWFlowCtrRegStk"
            
            def description(self):
                return "Sticky Parity Error HSPW Flow Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParErrProFlowCtrRegStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "ParErrProFlowCtrRegStk"
            
            def description(self):
                return "Sticky Parity Error Protection Flow Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParErrOutFlowCtrRegStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ParErrOutFlowCtrRegStk"
            
            def description(self):
                return "Sticky Parity Error Output Flow Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParErrHOLkCtrRegOC96_0Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "ParErrHOLkCtrRegOC96_0Stk"
            
            def description(self):
                return "Sticky Parity Error HO PWE Control Register of OC96#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParErrHOPldCtrRegOC96_0_OC48_1Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "ParErrHOPldCtrRegOC96_0_OC48_1Stk"
            
            def description(self):
                return "Sticky Parity Error HO Payload Control Register of OC96#0 OC#48#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParErrHOPldCtrRegOC96_0_OC48_0Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "ParErrHOPldCtrRegOC96_0_OC48_0Stk"
            
            def description(self):
                return "Sticky Parity Error HO Payload Control Register of OC96#0 OC#48#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParErrLOLkCtrRegOC48_1Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "ParErrLOLkCtrRegOC48_1Stk"
            
            def description(self):
                return "Sticky Parity Error LO PWE Control Register of OC48#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParErrLOPldCtrRegOC48_1_OC24_1Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "ParErrLOPldCtrRegOC48_1_OC24_1Stk"
            
            def description(self):
                return "Sticky Parity Error LO Payload Control Register of OC48#1 OC#24#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParErrLOPldCtrRegOC48_1_OC24_0Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "ParErrLOPldCtrRegOC48_1_OC24_0Stk"
            
            def description(self):
                return "Sticky Parity Error LO Payload Control Register of OC48#1 OC#24#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParErrLOLkCtrRegOC48_0Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "ParErrLOLkCtrRegOC48_0Stk"
            
            def description(self):
                return "Sticky Parity Error LO PWE Control Register of OC48#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParErrLOPldCtrRegOC48_0_OC24_1Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "ParErrLOPldCtrRegOC48_0_OC24_1Stk"
            
            def description(self):
                return "Sticky Parity Error LO Payload Control Register of OC48#0 OC#24#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParErrLOPldCtrRegOC48_0_OC24_0Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ParErrLOPldCtrRegOC48_0_OC24_0Stk"
            
            def description(self):
                return "Sticky Parity Error LO Payload Control Register of OC48#0 OC#24#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParLOPldCtrRegOC48_0_OC24_0StkStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ParLOPldCtrRegOC48_0_OC24_0StkStk"
            
            def description(self):
                return "Sticky Parity Error LO Payload Control Register of OC48#0 OC#24#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ParErrBlkBufCtrlRegStk"] = _AF6CCI0012_RD_PLA._pla_par_stk0._ParErrBlkBufCtrlRegStk()
            allFields["ParErrLkFlowCtrlRegStk"] = _AF6CCI0012_RD_PLA._pla_par_stk0._ParErrLkFlowCtrlRegStk()
            allFields["ParErrCommonPWCtrlRegStk"] = _AF6CCI0012_RD_PLA._pla_par_stk0._ParErrCommonPWCtrlRegStk()
            allFields["ParErrUPSRFlowCtrRegStk"] = _AF6CCI0012_RD_PLA._pla_par_stk0._ParErrUPSRFlowCtrRegStk()
            allFields["ParErrHSPWFlowCtrRegStk"] = _AF6CCI0012_RD_PLA._pla_par_stk0._ParErrHSPWFlowCtrRegStk()
            allFields["ParErrProFlowCtrRegStk"] = _AF6CCI0012_RD_PLA._pla_par_stk0._ParErrProFlowCtrRegStk()
            allFields["ParErrOutFlowCtrRegStk"] = _AF6CCI0012_RD_PLA._pla_par_stk0._ParErrOutFlowCtrRegStk()
            allFields["ParErrHOLkCtrRegOC96_0Stk"] = _AF6CCI0012_RD_PLA._pla_par_stk0._ParErrHOLkCtrRegOC96_0Stk()
            allFields["ParErrHOPldCtrRegOC96_0_OC48_1Stk"] = _AF6CCI0012_RD_PLA._pla_par_stk0._ParErrHOPldCtrRegOC96_0_OC48_1Stk()
            allFields["ParErrHOPldCtrRegOC96_0_OC48_0Stk"] = _AF6CCI0012_RD_PLA._pla_par_stk0._ParErrHOPldCtrRegOC96_0_OC48_0Stk()
            allFields["ParErrLOLkCtrRegOC48_1Stk"] = _AF6CCI0012_RD_PLA._pla_par_stk0._ParErrLOLkCtrRegOC48_1Stk()
            allFields["ParErrLOPldCtrRegOC48_1_OC24_1Stk"] = _AF6CCI0012_RD_PLA._pla_par_stk0._ParErrLOPldCtrRegOC48_1_OC24_1Stk()
            allFields["ParErrLOPldCtrRegOC48_1_OC24_0Stk"] = _AF6CCI0012_RD_PLA._pla_par_stk0._ParErrLOPldCtrRegOC48_1_OC24_0Stk()
            allFields["ParErrLOLkCtrRegOC48_0Stk"] = _AF6CCI0012_RD_PLA._pla_par_stk0._ParErrLOLkCtrRegOC48_0Stk()
            allFields["ParErrLOPldCtrRegOC48_0_OC24_1Stk"] = _AF6CCI0012_RD_PLA._pla_par_stk0._ParErrLOPldCtrRegOC48_0_OC24_1Stk()
            allFields["ParErrLOPldCtrRegOC48_0_OC24_0Stk"] = _AF6CCI0012_RD_PLA._pla_par_stk0._ParErrLOPldCtrRegOC48_0_OC24_0Stk()
            allFields["ParLOPldCtrRegOC48_0_OC24_0StkStk"] = _AF6CCI0012_RD_PLA._pla_par_stk0._ParLOPldCtrRegOC48_0_OC24_0StkStk()
            return allFields

    class _pla_force_crc_err_control(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Force CRC Error Control"
    
        def description(self):
            return "This register is used to force CRC error"
            
        def width(self):
            return 1
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00021034
            
        def endAddress(self):
            return 0xffffffff

        class _ForceCRCErrCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ForceCRCErrCtr"
            
            def description(self):
                return "Force CRC Error Control"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ForceCRCErrCtr"] = _AF6CCI0012_RD_PLA._pla_force_crc_err_control._ForceCRCErrCtr()
            return allFields

    class _pla_dis_crc_control(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Disable CRC Check Control"
    
        def description(self):
            return "This register is used to disable CRC check"
            
        def width(self):
            return 1
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00021035
            
        def endAddress(self):
            return 0xffffffff

        class _DisCRCChkCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DisCRCChkCtr"
            
            def description(self):
                return "Disable CRC check Control"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DisCRCChkCtr"] = _AF6CCI0012_RD_PLA._pla_dis_crc_control._DisCRCChkCtr()
            return allFields

    class _pla_crc_err_stk(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler CRC Error Sticky"
    
        def description(self):
            return "This register is used to check CRC error sticky"
            
        def width(self):
            return 1
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00021036
            
        def endAddress(self):
            return 0xffffffff

        class _CRCCheckStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CRCCheckStk"
            
            def description(self):
                return "Sticky CRC check Port1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CRCCheckStk"] = _AF6CCI0012_RD_PLA._pla_crc_err_stk._CRCCheckStk()
            return allFields

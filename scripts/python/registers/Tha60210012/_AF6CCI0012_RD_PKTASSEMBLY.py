import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0012_RD_PKTASSEMBLY(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["pcontrolcfg"] = _AF6CCI0012_RD_PKTASSEMBLY._pcontrolcfg()
        allRegisters["dumpstr1_pen"] = _AF6CCI0012_RD_PKTASSEMBLY._dumpstr1_pen()
        allRegisters["dump1_pen"] = _AF6CCI0012_RD_PKTASSEMBLY._dump1_pen()
        allRegisters["dhold0_pen"] = _AF6CCI0012_RD_PKTASSEMBLY._dhold0_pen()
        allRegisters["dhold1_pen"] = _AF6CCI0012_RD_PKTASSEMBLY._dhold1_pen()
        allRegisters["dhold2_pen"] = _AF6CCI0012_RD_PKTASSEMBLY._dhold2_pen()
        return allRegisters

    class _pcontrolcfg(AtRegister.AtRegister):
        def name(self):
            return "pcontrolcfg"
    
        def description(self):
            return "This register is used to configure as below (Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_dumpsel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_dumpsel"
            
            def description(self):
                return "0x0 : dump input data 0x1 : dump output data 0x2 : dump hdr"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_dumpsel"] = _AF6CCI0012_RD_PKTASSEMBLY._pcontrolcfg._cfg_dumpsel()
            return allFields

    class _dumpstr1_pen(AtRegister.AtRegister):
        def name(self):
            return "dumpstr1_pen"
    
        def description(self):
            return "This register is used to control dump module (Add 25bit:TBD)."
            
        def width(self):
            return 1
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000006
            
        def endAddress(self):
            return 0xffffffff

        class _dumpstr1_pen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dumpstr1_pen"
            
            def description(self):
                return "at first clear this bit and then set 1 value when every time dump"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dumpstr1_pen"] = _AF6CCI0012_RD_PKTASSEMBLY._dumpstr1_pen._dumpstr1_pen()
            return allFields

    class _dump1_pen(AtRegister.AtRegister):
        def name(self):
            return "dump1_pen"
    
        def description(self):
            return "This register is used to configure as below(Add 25bit:TBD)."
            
        def width(self):
            return 128
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000800
            
        def endAddress(self):
            return 0x00000fff

        class _data(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "data"
            
            def description(self):
                return "depending on cfg_dumpsel field, in case of : -dump input data : display input from PLA -dump output data : display output to PWE -dump hdr : display info from PLA, MSB to LBS bit as Unused               (84 bit)															    							   fcs nob 				(2 bit) header len 			(4 bit) end of packet		(1 bit) forwarding type 		(3 bit) inlp_id  			(4 bit) psn address			(6 bit) packet service       (3 bit) psn len  			(7 bit) Packet len           (14 bit)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["data"] = _AF6CCI0012_RD_PKTASSEMBLY._dump1_pen._data()
            return allFields

    class _dhold0_pen(AtRegister.AtRegister):
        def name(self):
            return "dhold0_pen"
    
        def description(self):
            return "This register is used to hold data when width bus > 32 bit (Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _hold0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "hold0"
            
            def description(self):
                return "hold from [63:32]"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["hold0"] = _AF6CCI0012_RD_PKTASSEMBLY._dhold0_pen._hold0()
            return allFields

    class _dhold1_pen(AtRegister.AtRegister):
        def name(self):
            return "dhold1_pen"
    
        def description(self):
            return "This register is used to hold data when width bus > 32 bit (Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000001
            
        def endAddress(self):
            return 0xffffffff

        class _hold1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "hold1"
            
            def description(self):
                return "hold from [95:64]"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["hold1"] = _AF6CCI0012_RD_PKTASSEMBLY._dhold1_pen._hold1()
            return allFields

    class _dhold2_pen(AtRegister.AtRegister):
        def name(self):
            return "dhold2_pen"
    
        def description(self):
            return "This register is used to hold data when width bus > 32 bit (Add 25bit:TBD)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0xffffffff

        class _hold2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "hold2"
            
            def description(self):
                return "hold from [127:96]"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["hold2"] = _AF6CCI0012_RD_PKTASSEMBLY._dhold2_pen._hold2()
            return allFields

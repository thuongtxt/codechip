import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0012_RD_ATTETH(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["version"] = _AF6CCI0012_RD_ATTETH._version()
        allRegisters["glb_cfg_active"] = _AF6CCI0012_RD_ATTETH._glb_cfg_active()
        allRegisters["upen_hold1"] = _AF6CCI0012_RD_ATTETH._upen_hold1()
        allRegisters["upen_hold2"] = _AF6CCI0012_RD_ATTETH._upen_hold2()
        allRegisters["upen_hold3"] = _AF6CCI0012_RD_ATTETH._upen_hold3()
        allRegisters["glb_cfg1"] = _AF6CCI0012_RD_ATTETH._glb_cfg1()
        allRegisters["glb_cfgtxspd_active"] = _AF6CCI0012_RD_ATTETH._glb_cfgtxspd_active()
        allRegisters["txtm_pen_cfg0_1gto10g"] = _AF6CCI0012_RD_ATTETH._txtm_pen_cfg0_1gto10g()
        allRegisters["txtm_config_1gto10g_info0"] = _AF6CCI0012_RD_ATTETH._txtm_config_1gto10g_info0()
        allRegisters["txtm_config_1gto10g_info1"] = _AF6CCI0012_RD_ATTETH._txtm_config_1gto10g_info1()
        allRegisters["txtm_config_1gto10g_info2"] = _AF6CCI0012_RD_ATTETH._txtm_config_1gto10g_info2()
        allRegisters["txtm_config_1gto10g_info3"] = _AF6CCI0012_RD_ATTETH._txtm_config_1gto10g_info3()
        allRegisters["txtm_status0_1gto10g"] = _AF6CCI0012_RD_ATTETH._txtm_status0_1gto10g()
        allRegisters["txtm_status1_1gto10g"] = _AF6CCI0012_RD_ATTETH._txtm_status1_1gto10g()
        allRegisters["txtm_status2_1gto10g"] = _AF6CCI0012_RD_ATTETH._txtm_status2_1gto10g()
        allRegisters["txtm_status3_1gto10g"] = _AF6CCI0012_RD_ATTETH._txtm_status3_1gto10g()
        allRegisters["txtm_pen_cfg0_100mto1g_g0"] = _AF6CCI0012_RD_ATTETH._txtm_pen_cfg0_100mto1g_g0()
        allRegisters["txtm_pen_cfg1_100mto1g_g0"] = _AF6CCI0012_RD_ATTETH._txtm_pen_cfg1_100mto1g_g0()
        allRegisters["txtm_config_100mto1g_g0_info0"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g0_info0()
        allRegisters["txtm_config_100mto1g_g0_info1"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g0_info1()
        allRegisters["txtm_config_100mto1g_g0_info2"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g0_info2()
        allRegisters["txtm_config_100mto1g_g0_info3"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g0_info3()
        allRegisters["txtm_status0_100mto1g_g0"] = _AF6CCI0012_RD_ATTETH._txtm_status0_100mto1g_g0()
        allRegisters["txtm_status1_100mto1g_g0"] = _AF6CCI0012_RD_ATTETH._txtm_status1_100mto1g_g0()
        allRegisters["txtm_status2_100mto1g_g0"] = _AF6CCI0012_RD_ATTETH._txtm_status2_100mto1g_g0()
        allRegisters["txtm_status3_100mto1g_g0"] = _AF6CCI0012_RD_ATTETH._txtm_status3_100mto1g_g0()
        allRegisters["txtm_pen_cfg0_100mto1g_g1"] = _AF6CCI0012_RD_ATTETH._txtm_pen_cfg0_100mto1g_g1()
        allRegisters["txtm_pen_cfg1_100mto1g_g1"] = _AF6CCI0012_RD_ATTETH._txtm_pen_cfg1_100mto1g_g1()
        allRegisters["txtm_config_100mto1g_g1_info0"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g1_info0()
        allRegisters["txtm_config_100mto1g_g1_info1"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g1_info1()
        allRegisters["txtm_config_100mto1g_g1_info2"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g1_info2()
        allRegisters["txtm_config_100mto1g_g1_info3"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g1_info3()
        allRegisters["txtm_status0_100mto1g_g1"] = _AF6CCI0012_RD_ATTETH._txtm_status0_100mto1g_g1()
        allRegisters["txtm_status1_100mto1g_g1"] = _AF6CCI0012_RD_ATTETH._txtm_status1_100mto1g_g1()
        allRegisters["txtm_status2_100mto1g_g1"] = _AF6CCI0012_RD_ATTETH._txtm_status2_100mto1g_g1()
        allRegisters["txtm_status3_100mto1g_g1"] = _AF6CCI0012_RD_ATTETH._txtm_status3_100mto1g_g1()
        allRegisters["txtm_pen_cfg0_10mto100m_g0"] = _AF6CCI0012_RD_ATTETH._txtm_pen_cfg0_10mto100m_g0()
        allRegisters["txtm_pen_cfg1_10mto100m_g0"] = _AF6CCI0012_RD_ATTETH._txtm_pen_cfg1_10mto100m_g0()
        allRegisters["txtm_config_10mto100m_g0_info0"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g0_info0()
        allRegisters["txtm_config_10mto100m_g0_info1"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g0_info1()
        allRegisters["txtm_config_10mto100m_g0_info2"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g0_info2()
        allRegisters["txtm_config_10mto100m_g0_info3"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g0_info3()
        allRegisters["txtm_status0_10mto100m_g0"] = _AF6CCI0012_RD_ATTETH._txtm_status0_10mto100m_g0()
        allRegisters["txtm_status1_10mto100m_g0"] = _AF6CCI0012_RD_ATTETH._txtm_status1_10mto100m_g0()
        allRegisters["txtm_status2_10mto100m_g0"] = _AF6CCI0012_RD_ATTETH._txtm_status2_10mto100m_g0()
        allRegisters["txtm_status3_10mto100m_g0"] = _AF6CCI0012_RD_ATTETH._txtm_status3_10mto100m_g0()
        allRegisters["txtm_pen_cfg0_10mto100m_g1"] = _AF6CCI0012_RD_ATTETH._txtm_pen_cfg0_10mto100m_g1()
        allRegisters["txtm_pen_cfg1_10mto100m_g1"] = _AF6CCI0012_RD_ATTETH._txtm_pen_cfg1_10mto100m_g1()
        allRegisters["txtm_config_10mto100m_g1_info0"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g1_info0()
        allRegisters["txtm_config_10mto100m_g1_info1"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g1_info1()
        allRegisters["txtm_config_10mto100m_g1_info2"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g1_info2()
        allRegisters["txtm_config_10mto100m_g1_info3"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g1_info3()
        allRegisters["txtm_status0_10mto100m_g1"] = _AF6CCI0012_RD_ATTETH._txtm_status0_10mto100m_g1()
        allRegisters["txtm_status1_10mto100m_g1"] = _AF6CCI0012_RD_ATTETH._txtm_status1_10mto100m_g1()
        allRegisters["txtm_status2_10mto100m_g1"] = _AF6CCI0012_RD_ATTETH._txtm_status2_10mto100m_g1()
        allRegisters["txtm_status3_10mto100m_g1"] = _AF6CCI0012_RD_ATTETH._txtm_status3_10mto100m_g1()
        allRegisters["txtm_global_cfg1_6mto10m"] = _AF6CCI0012_RD_ATTETH._txtm_global_cfg1_6mto10m()
        allRegisters["txtm_global_cfg2_6mto10m"] = _AF6CCI0012_RD_ATTETH._txtm_global_cfg2_6mto10m()
        allRegisters["txtm_cfg_6to10m"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_6to10m()
        allRegisters["txtm_sta_6to10m"] = _AF6CCI0012_RD_ATTETH._txtm_sta_6to10m()
        allRegisters["txtm_oneshot_cfg_6to10m"] = _AF6CCI0012_RD_ATTETH._txtm_oneshot_cfg_6to10m()
        allRegisters["txtm_global_cfg1_3mto6m"] = _AF6CCI0012_RD_ATTETH._txtm_global_cfg1_3mto6m()
        allRegisters["txtm_global_cfg2_3mto6m"] = _AF6CCI0012_RD_ATTETH._txtm_global_cfg2_3mto6m()
        allRegisters["txtm_cfg_3mto6m"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_3mto6m()
        allRegisters["txtm_sta_3mto6m"] = _AF6CCI0012_RD_ATTETH._txtm_sta_3mto6m()
        allRegisters["txtm_oneshot_cfg_3mto6m"] = _AF6CCI0012_RD_ATTETH._txtm_oneshot_cfg_3mto6m()
        allRegisters["txtm_global_cfg1_128kto3m"] = _AF6CCI0012_RD_ATTETH._txtm_global_cfg1_128kto3m()
        allRegisters["txtm_global_cfg2_128kto3m"] = _AF6CCI0012_RD_ATTETH._txtm_global_cfg2_128kto3m()
        allRegisters["txtm_cfg_128kto3m"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_128kto3m()
        allRegisters["txtm_sta_128kto3m"] = _AF6CCI0012_RD_ATTETH._txtm_sta_128kto3m()
        allRegisters["txtm_oneshot_cfg_128kto3m"] = _AF6CCI0012_RD_ATTETH._txtm_oneshot_cfg_128kto3m()
        allRegisters["txfrm_mode_cfg"] = _AF6CCI0012_RD_ATTETH._txfrm_mode_cfg()
        allRegisters["txfrm_hdr1_cfg"] = _AF6CCI0012_RD_ATTETH._txfrm_hdr1_cfg()
        allRegisters["txfrm_hdr2_cfg"] = _AF6CCI0012_RD_ATTETH._txfrm_hdr2_cfg()
        allRegisters["txfrm_hdr3_cfg"] = _AF6CCI0012_RD_ATTETH._txfrm_hdr3_cfg()
        allRegisters["txfrm_hdr4_cfg"] = _AF6CCI0012_RD_ATTETH._txfrm_hdr4_cfg()
        allRegisters["txfrm_hdr5_cfg"] = _AF6CCI0012_RD_ATTETH._txfrm_hdr5_cfg()
        allRegisters["txfrm_hdr6_cfg"] = _AF6CCI0012_RD_ATTETH._txfrm_hdr6_cfg()
        allRegisters["txfrm_hdr7_cfg"] = _AF6CCI0012_RD_ATTETH._txfrm_hdr7_cfg()
        allRegisters["txfrm_hdr8_cfg"] = _AF6CCI0012_RD_ATTETH._txfrm_hdr8_cfg()
        allRegisters["Datgen_register"] = _AF6CCI0012_RD_ATTETH._Datgen_register()
        allRegisters["rxfrm_hdrsize"] = _AF6CCI0012_RD_ATTETH._rxfrm_hdrsize()
        allRegisters["Datmon_register"] = _AF6CCI0012_RD_ATTETH._Datmon_register()
        allRegisters["Datmon_stareg"] = _AF6CCI0012_RD_ATTETH._Datmon_stareg()
        allRegisters["rxfrm_hdr1_cfg"] = _AF6CCI0012_RD_ATTETH._rxfrm_hdr1_cfg()
        allRegisters["rxfrm_hdr2_cfg"] = _AF6CCI0012_RD_ATTETH._rxfrm_hdr2_cfg()
        allRegisters["rxfrm_hdr3_cfg"] = _AF6CCI0012_RD_ATTETH._rxfrm_hdr3_cfg()
        allRegisters["rxfrm_hdr4_cfg"] = _AF6CCI0012_RD_ATTETH._rxfrm_hdr4_cfg()
        allRegisters["rxfrm_hdr5_cfg"] = _AF6CCI0012_RD_ATTETH._rxfrm_hdr5_cfg()
        allRegisters["rxfrm_hdr6_cfg"] = _AF6CCI0012_RD_ATTETH._rxfrm_hdr6_cfg()
        allRegisters["rxfrm_hdr7_cfg"] = _AF6CCI0012_RD_ATTETH._rxfrm_hdr7_cfg()
        allRegisters["rxfrm_hdr8_cfg"] = _AF6CCI0012_RD_ATTETH._rxfrm_hdr8_cfg()
        allRegisters["rxfrm_hdrerrcnt"] = _AF6CCI0012_RD_ATTETH._rxfrm_hdrerrcnt()
        allRegisters["datmon_prbserrcnt"] = _AF6CCI0012_RD_ATTETH._datmon_prbserrcnt()
        allRegisters["pktmon_seqerrcnt"] = _AF6CCI0012_RD_ATTETH._pktmon_seqerrcnt()
        allRegisters["pktmon_bytecnt"] = _AF6CCI0012_RD_ATTETH._pktmon_bytecnt()
        allRegisters["pktmon_pktcnt"] = _AF6CCI0012_RD_ATTETH._pktmon_pktcnt()
        allRegisters["flowmon_speed"] = _AF6CCI0012_RD_ATTETH._flowmon_speed()
        allRegisters["rx_pktdump_cfg"] = _AF6CCI0012_RD_ATTETH._rx_pktdump_cfg()
        allRegisters["rx_pktdump_dat"] = _AF6CCI0012_RD_ATTETH._rx_pktdump_dat()
        allRegisters["rx_pktdump_exdat"] = _AF6CCI0012_RD_ATTETH._rx_pktdump_exdat()
        allRegisters["tx_pktdump_cfg"] = _AF6CCI0012_RD_ATTETH._tx_pktdump_cfg()
        allRegisters["tx_pktdump_dat"] = _AF6CCI0012_RD_ATTETH._tx_pktdump_dat()
        allRegisters["pktgen_pktcnt"] = _AF6CCI0012_RD_ATTETH._pktgen_pktcnt()
        allRegisters["pktgen_bytecnt"] = _AF6CCI0012_RD_ATTETH._pktgen_bytecnt()
        return allRegisters

    class _version(AtRegister.AtRegister):
        def name(self):
            return "CHIP Version ID Register"
    
        def description(self):
            return "This register is used to indicate Version ID"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _version(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "version"
            
            def description(self):
                return "version"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["version"] = _AF6CCI0012_RD_ATTETH._version._version()
            return allFields

    class _glb_cfg_active(AtRegister.AtRegister):
        def name(self):
            return "Global Active Register"
    
        def description(self):
            return "This register is used to active engine"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000001
            
        def endAddress(self):
            return 0xffffffff

        class _txlenadj(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "txlenadj"
            
            def description(self):
                return "Tx len adjust"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _spdmon_sub(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "spdmon_sub"
            
            def description(self):
                return "Speed mon sub byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _spdmon_add(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "spdmon_add"
            
            def description(self):
                return "Speed mon add byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TxMac_Src(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "TxMac_Src"
            
            def description(self):
                return "0: PW Enc         1: from Tx ATT ETH"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _CLA_Src(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "CLA_Src"
            
            def description(self):
                return "0: CLA from RxMAC 1: from Tx ATT ETH"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _RxAtt_Src(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RxAtt_Src"
            
            def description(self):
                return "0: from PW Enc    1: from Tx ATT ETH   2: from Rx MAC"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _txfcst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "txfcst"
            
            def description(self):
                return "Enable Tx Fcs"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _rxfcst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "rxfcst"
            
            def description(self):
                return "Enable Rx Fcs"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _act(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "act"
            
            def description(self):
                return "active engine"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txlenadj"] = _AF6CCI0012_RD_ATTETH._glb_cfg_active._txlenadj()
            allFields["spdmon_sub"] = _AF6CCI0012_RD_ATTETH._glb_cfg_active._spdmon_sub()
            allFields["spdmon_add"] = _AF6CCI0012_RD_ATTETH._glb_cfg_active._spdmon_add()
            allFields["TxMac_Src"] = _AF6CCI0012_RD_ATTETH._glb_cfg_active._TxMac_Src()
            allFields["CLA_Src"] = _AF6CCI0012_RD_ATTETH._glb_cfg_active._CLA_Src()
            allFields["RxAtt_Src"] = _AF6CCI0012_RD_ATTETH._glb_cfg_active._RxAtt_Src()
            allFields["txfcst"] = _AF6CCI0012_RD_ATTETH._glb_cfg_active._txfcst()
            allFields["rxfcst"] = _AF6CCI0012_RD_ATTETH._glb_cfg_active._rxfcst()
            allFields["act"] = _AF6CCI0012_RD_ATTETH._glb_cfg_active._act()
            return allFields

    class _upen_hold1(AtRegister.AtRegister):
        def name(self):
            return "Hold Register 1"
    
        def description(self):
            return "Used to write configuration or status that is greater than 32-bit wide"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0xffffffff

        class _Hold1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold1"
            
            def description(self):
                return ""
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold1"] = _AF6CCI0012_RD_ATTETH._upen_hold1._Hold1()
            return allFields

    class _upen_hold2(AtRegister.AtRegister):
        def name(self):
            return "Hold Register 2"
    
        def description(self):
            return "Used to write configuration or status that is greater than 32-bit wide"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _Hold2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold2"
            
            def description(self):
                return ""
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold2"] = _AF6CCI0012_RD_ATTETH._upen_hold2._Hold2()
            return allFields

    class _upen_hold3(AtRegister.AtRegister):
        def name(self):
            return "Hold Register 3"
    
        def description(self):
            return "Used to write configuration or status that is greater than 32-bit wide"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000004
            
        def endAddress(self):
            return 0xffffffff

        class _Hold3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold3"
            
            def description(self):
                return ""
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold3"] = _AF6CCI0012_RD_ATTETH._upen_hold3._Hold3()
            return allFields

    class _glb_cfg1(AtRegister.AtRegister):
        def name(self):
            return "Global Control Register1"
    
        def description(self):
            return "This register is used to config global"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000009
            
        def endAddress(self):
            return 0xffffffff

        class _rxhdrrange_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "rxhdrrange_en"
            
            def description(self):
                return "Rx Hdr range enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _rxhdrrange(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "rxhdrrange"
            
            def description(self):
                return "Rx Hdr range 0: [7:0], 1: [15:8], 2: [19:16]"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _flush(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "flush"
            
            def description(self):
                return "debug only, set to 0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _rxptchbyte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxptchbyte"
            
            def description(self):
                return "PTCH byte  0: No PTCH, 1: 1byte, 2: 2byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxhdrrange_en"] = _AF6CCI0012_RD_ATTETH._glb_cfg1._rxhdrrange_en()
            allFields["rxhdrrange"] = _AF6CCI0012_RD_ATTETH._glb_cfg1._rxhdrrange()
            allFields["flush"] = _AF6CCI0012_RD_ATTETH._glb_cfg1._flush()
            allFields["rxptchbyte"] = _AF6CCI0012_RD_ATTETH._glb_cfg1._rxptchbyte()
            return allFields

    class _glb_cfgtxspd_active(AtRegister.AtRegister):
        def name(self):
            return "Global Active Register"
    
        def description(self):
            return "This register is used to active engine"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00080000
            
        def endAddress(self):
            return 0xffffffff

        class _act_lspdctr128_3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "act_lspdctr128_3"
            
            def description(self):
                return "active engine low spd 128k->3M"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _act_lspdctr3_6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "act_lspdctr3_6"
            
            def description(self):
                return "active engine low spd 3->6M"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _act_lspdctr6_10(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "act_lspdctr6_10"
            
            def description(self):
                return "active engine low spd 6->10M"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _act_hsdpctr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "act_hsdpctr"
            
            def description(self):
                return "active engine high spd"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _act_sdparbiter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "act_sdparbiter"
            
            def description(self):
                return "active engine spd arbiter"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["act_lspdctr128_3"] = _AF6CCI0012_RD_ATTETH._glb_cfgtxspd_active._act_lspdctr128_3()
            allFields["act_lspdctr3_6"] = _AF6CCI0012_RD_ATTETH._glb_cfgtxspd_active._act_lspdctr3_6()
            allFields["act_lspdctr6_10"] = _AF6CCI0012_RD_ATTETH._glb_cfgtxspd_active._act_lspdctr6_10()
            allFields["act_hsdpctr"] = _AF6CCI0012_RD_ATTETH._glb_cfgtxspd_active._act_hsdpctr()
            allFields["act_sdparbiter"] = _AF6CCI0012_RD_ATTETH._glb_cfgtxspd_active._act_sdparbiter()
            return allFields

    class _txtm_pen_cfg0_1gto10g(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Global Configuration Speed Control For Group 1G-10G (Max 10 Flows)"
    
        def description(self):
            return "This register is used to configure numflow,pausethres,flowpausethres,interval"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00090000
            
        def endAddress(self):
            return 0xffffffff

        class _numflow(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 28
        
            def name(self):
                return "numflow"
            
            def description(self):
                return "numflow"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _pausethres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 16
        
            def name(self):
                return "pausethres"
            
            def description(self):
                return "pausethres"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _flowpausethres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "flowpausethres"
            
            def description(self):
                return "flowpausethres"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _interval(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "interval"
            
            def description(self):
                return "interval"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["numflow"] = _AF6CCI0012_RD_ATTETH._txtm_pen_cfg0_1gto10g._numflow()
            allFields["pausethres"] = _AF6CCI0012_RD_ATTETH._txtm_pen_cfg0_1gto10g._pausethres()
            allFields["flowpausethres"] = _AF6CCI0012_RD_ATTETH._txtm_pen_cfg0_1gto10g._flowpausethres()
            allFields["interval"] = _AF6CCI0012_RD_ATTETH._txtm_pen_cfg0_1gto10g._interval()
            return allFields

    class _txtm_config_1gto10g_info0(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Configuration 1 Speed Control For Group 1G-10G (Max 10 Flows)"
    
        def description(self):
            return "This register is used to configure lenstep,maxlen,minlen"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_0010 + $idx"
            
        def startAddress(self):
            return 0x00090010
            
        def endAddress(self):
            return 0x00090019

        class _lenstep(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 28
        
            def name(self):
                return "lenstep"
            
            def description(self):
                return "lenstep"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _maxlen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 14
        
            def name(self):
                return "maxlen"
            
            def description(self):
                return "maxlen"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _minlen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "minlen"
            
            def description(self):
                return "minlen"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lenstep"] = _AF6CCI0012_RD_ATTETH._txtm_config_1gto10g_info0._lenstep()
            allFields["maxlen"] = _AF6CCI0012_RD_ATTETH._txtm_config_1gto10g_info0._maxlen()
            allFields["minlen"] = _AF6CCI0012_RD_ATTETH._txtm_config_1gto10g_info0._minlen()
            return allFields

    class _txtm_config_1gto10g_info1(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Configuration 2 Speed Control For Group 1G-10G (Max 10 Flows)"
    
        def description(self):
            return "This register is used to configure  lenmode,cbs,outport,headersize,besteff,burstmode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_0020 + $idx"
            
        def startAddress(self):
            return 0x00090020
            
        def endAddress(self):
            return 0x00090029

        class _high_int_cfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 24
        
            def name(self):
                return "high_int_cfg"
            
            def description(self):
                return "high_int_cfg"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _burstmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "burstmode"
            
            def description(self):
                return "burstmode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _besteff(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "besteff"
            
            def description(self):
                return "besteff"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _headersize(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "headersize"
            
            def description(self):
                return "headersize"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _outport(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 16
        
            def name(self):
                return "outport"
            
            def description(self):
                return "outport"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cbs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 2
        
            def name(self):
                return "cbs"
            
            def description(self):
                return "cbs cfg"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lenmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lenmode"
            
            def description(self):
                return "lenmode  0 - fix, 1 - increase, 2 - decrease, 3 - random"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["high_int_cfg"] = _AF6CCI0012_RD_ATTETH._txtm_config_1gto10g_info1._high_int_cfg()
            allFields["burstmode"] = _AF6CCI0012_RD_ATTETH._txtm_config_1gto10g_info1._burstmode()
            allFields["besteff"] = _AF6CCI0012_RD_ATTETH._txtm_config_1gto10g_info1._besteff()
            allFields["headersize"] = _AF6CCI0012_RD_ATTETH._txtm_config_1gto10g_info1._headersize()
            allFields["outport"] = _AF6CCI0012_RD_ATTETH._txtm_config_1gto10g_info1._outport()
            allFields["cbs"] = _AF6CCI0012_RD_ATTETH._txtm_config_1gto10g_info1._cbs()
            allFields["lenmode"] = _AF6CCI0012_RD_ATTETH._txtm_config_1gto10g_info1._lenmode()
            return allFields

    class _txtm_config_1gto10g_info2(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Configuration 3 Speed Control For Group 1G-10G (Max 10 Flows)"
    
        def description(self):
            return "This register is used to configure"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_0030 + $idx"
            
        def startAddress(self):
            return 0x00090030
            
        def endAddress(self):
            return 0x00090039

        class _low_int_cfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 18
        
            def name(self):
                return "low_int_cfg"
            
            def description(self):
                return "low_int_cfg"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _width_high(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 10
        
            def name(self):
                return "width_high"
            
            def description(self):
                return "width_high"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _width_low(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "width_low"
            
            def description(self):
                return "width_low"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["low_int_cfg"] = _AF6CCI0012_RD_ATTETH._txtm_config_1gto10g_info2._low_int_cfg()
            allFields["width_high"] = _AF6CCI0012_RD_ATTETH._txtm_config_1gto10g_info2._width_high()
            allFields["width_low"] = _AF6CCI0012_RD_ATTETH._txtm_config_1gto10g_info2._width_low()
            return allFields

    class _txtm_config_1gto10g_info3(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Configuration 4 Speed Control For Group 1G-10G (Max 10 Flows)"
    
        def description(self):
            return "This register is used to configure  conv_flow."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_0040 + $idx"
            
        def startAddress(self):
            return 0x00090040
            
        def endAddress(self):
            return 0x00090049

        class _conv_flow(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "conv_flow"
            
            def description(self):
                return "conv_flow"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["conv_flow"] = _AF6CCI0012_RD_ATTETH._txtm_config_1gto10g_info3._conv_flow()
            return allFields

    class _txtm_status0_1gto10g(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Status 1 Speed Control For Group 1G-10G (Max 10 Flows)"
    
        def description(self):
            return "This register is used to config numoneshot (active)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_0050 + $idx"
            
        def startAddress(self):
            return 0x00090050
            
        def endAddress(self):
            return 0x00090059

        class _oneshot(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "oneshot"
            
            def description(self):
                return "oneshot"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["oneshot"] = _AF6CCI0012_RD_ATTETH._txtm_status0_1gto10g._oneshot()
            return allFields

    class _txtm_status1_1gto10g(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Status 2 Speed Control For Group 1G-10G (Max 10 Flows)"
    
        def description(self):
            return "This register is used to save status  cur_low_int_cnt ,cur_high_int_cnt."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_0060 + $idx"
            
        def startAddress(self):
            return 0x00090060
            
        def endAddress(self):
            return 0x00090069

        class _cur_high_int_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 11
        
            def name(self):
                return "cur_high_int_cnt"
            
            def description(self):
                return "current  high interval counter"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cur_low_int_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cur_low_int_cnt"
            
            def description(self):
                return "current low interval counter"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cur_high_int_cnt"] = _AF6CCI0012_RD_ATTETH._txtm_status1_1gto10g._cur_high_int_cnt()
            allFields["cur_low_int_cnt"] = _AF6CCI0012_RD_ATTETH._txtm_status1_1gto10g._cur_low_int_cnt()
            return allFields

    class _txtm_status2_1gto10g(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Status 3 Speed Control For Group 1G-10G (Max 10 Flows)"
    
        def description(self):
            return "This register is used to save status current pktleng, cir_cur"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_0070 + $idx"
            
        def startAddress(self):
            return 0x00090070
            
        def endAddress(self):
            return 0x00090079

        class _cur_cir(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 16
        
            def name(self):
                return "cur_cir"
            
            def description(self):
                return "current cir low"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cur_pktleng(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cur_pktleng"
            
            def description(self):
                return "current pktleng"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cur_cir"] = _AF6CCI0012_RD_ATTETH._txtm_status2_1gto10g._cur_cir()
            allFields["cur_pktleng"] = _AF6CCI0012_RD_ATTETH._txtm_status2_1gto10g._cur_pktleng()
            return allFields

    class _txtm_status3_1gto10g(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Status 4 Speed Control For Group 1G-10G (Max 10 Flows)"
    
        def description(self):
            return "This register is used to save status cur_cnt_burst,cur_width_burst."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_0080 + $idx"
            
        def startAddress(self):
            return 0x00090080
            
        def endAddress(self):
            return 0x00090099

        class _cur_width_burst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 14
        
            def name(self):
                return "cur_width_burst"
            
            def description(self):
                return "cur_width_burst"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cur_cnt_burst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cur_cnt_burst"
            
            def description(self):
                return "cur_cnt_burst"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cur_width_burst"] = _AF6CCI0012_RD_ATTETH._txtm_status3_1gto10g._cur_width_burst()
            allFields["cur_cnt_burst"] = _AF6CCI0012_RD_ATTETH._txtm_status3_1gto10g._cur_cnt_burst()
            return allFields

    class _txtm_pen_cfg0_100mto1g_g0(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Global Configuration 1 Speed Control For Group 100M-1G (Max 80 Flows)"
    
        def description(self):
            return "This register is used to configure pausethres,flowpausethres,"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00091000
            
        def endAddress(self):
            return 0xffffffff

        class _pausethres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 7
        
            def name(self):
                return "pausethres"
            
            def description(self):
                return "pausethres"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _flowpausethres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 0
        
            def name(self):
                return "flowpausethres"
            
            def description(self):
                return "flowpausethres"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pausethres"] = _AF6CCI0012_RD_ATTETH._txtm_pen_cfg0_100mto1g_g0._pausethres()
            allFields["flowpausethres"] = _AF6CCI0012_RD_ATTETH._txtm_pen_cfg0_100mto1g_g0._flowpausethres()
            return allFields

    class _txtm_pen_cfg1_100mto1g_g0(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Global Configuration 2 Speed Control For Group 100M-1G (Max 80 Flows)"
    
        def description(self):
            return "This register is used to configure numflow,interval"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00091001
            
        def endAddress(self):
            return 0xffffffff

        class _numflow(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 12
        
            def name(self):
                return "numflow"
            
            def description(self):
                return "numflow"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _interval(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "interval"
            
            def description(self):
                return "interval"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["numflow"] = _AF6CCI0012_RD_ATTETH._txtm_pen_cfg1_100mto1g_g0._numflow()
            allFields["interval"] = _AF6CCI0012_RD_ATTETH._txtm_pen_cfg1_100mto1g_g0._interval()
            return allFields

    class _txtm_config_100mto1g_g0_info0(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Configuration 1 Speed Control For Group 100M-1G (Max 80 Flows)"
    
        def description(self):
            return "This register is used to configure lenstep,maxlen,minlen"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_1080 + $idx"
            
        def startAddress(self):
            return 0x00091080
            
        def endAddress(self):
            return 0x000910cf

        class _lenstep(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 28
        
            def name(self):
                return "lenstep"
            
            def description(self):
                return "lenstep"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _maxlen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 14
        
            def name(self):
                return "maxlen"
            
            def description(self):
                return "maxlen"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _minlen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "minlen"
            
            def description(self):
                return "minlen"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lenstep"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g0_info0._lenstep()
            allFields["maxlen"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g0_info0._maxlen()
            allFields["minlen"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g0_info0._minlen()
            return allFields

    class _txtm_config_100mto1g_g0_info1(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Configuration 2 Speed Control For Group 100M-1G (Max 80 Flows)"
    
        def description(self):
            return "This register is used to configure  lenmode,cbs,outport,headersize,besteff,burstmode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_1100 + $idx"
            
        def startAddress(self):
            return 0x00091100
            
        def endAddress(self):
            return 0x0009114f

        class _high_int_cfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 24
        
            def name(self):
                return "high_int_cfg"
            
            def description(self):
                return "high_int_cfg"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _burstmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "burstmode"
            
            def description(self):
                return "burstmode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _besteff(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "besteff"
            
            def description(self):
                return "besteff"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _headersize(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "headersize"
            
            def description(self):
                return "headersize"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _outport(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 16
        
            def name(self):
                return "outport"
            
            def description(self):
                return "outport"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cbs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 2
        
            def name(self):
                return "cbs"
            
            def description(self):
                return "cbs cfg"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lenmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lenmode"
            
            def description(self):
                return "lenmode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["high_int_cfg"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g0_info1._high_int_cfg()
            allFields["burstmode"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g0_info1._burstmode()
            allFields["besteff"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g0_info1._besteff()
            allFields["headersize"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g0_info1._headersize()
            allFields["outport"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g0_info1._outport()
            allFields["cbs"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g0_info1._cbs()
            allFields["lenmode"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g0_info1._lenmode()
            return allFields

    class _txtm_config_100mto1g_g0_info2(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Configuration 3 Speed Control For Group 100M-1G (Max 80 Flows)"
    
        def description(self):
            return "This register is used to configure"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_1180 + $idx"
            
        def startAddress(self):
            return 0x00091180
            
        def endAddress(self):
            return 0x000911cf

        class _low_int_cfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 15
        
            def name(self):
                return "low_int_cfg"
            
            def description(self):
                return "low_int_cfg"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _width_high(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 6
        
            def name(self):
                return "width_high"
            
            def description(self):
                return "width_high"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _width_low(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "width_low"
            
            def description(self):
                return "width_low"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["low_int_cfg"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g0_info2._low_int_cfg()
            allFields["width_high"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g0_info2._width_high()
            allFields["width_low"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g0_info2._width_low()
            return allFields

    class _txtm_config_100mto1g_g0_info3(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Configuration 4 Speed Control For Group 100M-1G (Max 80 Flows)"
    
        def description(self):
            return "This register is used to configure  conv_flow."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_1200 + $idx"
            
        def startAddress(self):
            return 0x00091200
            
        def endAddress(self):
            return 0x0009124f

        class _conv_flow(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "conv_flow"
            
            def description(self):
                return "conv_flow"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["conv_flow"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g0_info3._conv_flow()
            return allFields

    class _txtm_status0_100mto1g_g0(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Status 1 Speed Control For Group 100M-1G (Max 80 Flows)"
    
        def description(self):
            return "This register is used to config numoneshot (active)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_1280 + $idx"
            
        def startAddress(self):
            return 0x00091280
            
        def endAddress(self):
            return 0x000912cf

        class _oneshot(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "oneshot"
            
            def description(self):
                return "oneshot"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["oneshot"] = _AF6CCI0012_RD_ATTETH._txtm_status0_100mto1g_g0._oneshot()
            return allFields

    class _txtm_status1_100mto1g_g0(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Status 2 Speed Control For Group 100M-1G (Max 80 Flows)"
    
        def description(self):
            return "This register is used to save status  cur_low_int_cnt ,cur_high_int_cnt."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_1300 + $idx"
            
        def startAddress(self):
            return 0x00091300
            
        def endAddress(self):
            return 0x0009134f

        class _cur_high_int_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 8
        
            def name(self):
                return "cur_high_int_cnt"
            
            def description(self):
                return "current  high interval counter"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cur_low_int_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cur_low_int_cnt"
            
            def description(self):
                return "current low interval counter"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cur_high_int_cnt"] = _AF6CCI0012_RD_ATTETH._txtm_status1_100mto1g_g0._cur_high_int_cnt()
            allFields["cur_low_int_cnt"] = _AF6CCI0012_RD_ATTETH._txtm_status1_100mto1g_g0._cur_low_int_cnt()
            return allFields

    class _txtm_status2_100mto1g_g0(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Status 3 Speed Control For Group 100M-1G (Max 80 Flows)"
    
        def description(self):
            return "This register is used to save status current pktleng, cir_cur"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_1380 + $idx"
            
        def startAddress(self):
            return 0x00091380
            
        def endAddress(self):
            return 0x000913cf

        class _cur_cir(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 16
        
            def name(self):
                return "cur_cir"
            
            def description(self):
                return "current cir low"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cur_pktleng(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cur_pktleng"
            
            def description(self):
                return "current pktleng"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cur_cir"] = _AF6CCI0012_RD_ATTETH._txtm_status2_100mto1g_g0._cur_cir()
            allFields["cur_pktleng"] = _AF6CCI0012_RD_ATTETH._txtm_status2_100mto1g_g0._cur_pktleng()
            return allFields

    class _txtm_status3_100mto1g_g0(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Status 4 Speed Control For Group 100M-1G (Max 80 Flows)"
    
        def description(self):
            return "This register is used to save status cur_cnt_burst,cur_width_burst."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_1400 + $idx"
            
        def startAddress(self):
            return 0x00091400
            
        def endAddress(self):
            return 0x0009144f

        class _cur_width_burst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 14
        
            def name(self):
                return "cur_width_burst"
            
            def description(self):
                return "cur_width_burst"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cur_cnt_burst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cur_cnt_burst"
            
            def description(self):
                return "cur_cnt_burst"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cur_width_burst"] = _AF6CCI0012_RD_ATTETH._txtm_status3_100mto1g_g0._cur_width_burst()
            allFields["cur_cnt_burst"] = _AF6CCI0012_RD_ATTETH._txtm_status3_100mto1g_g0._cur_cnt_burst()
            return allFields

    class _txtm_pen_cfg0_100mto1g_g1(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Global Configuration 1 Speed Control For Group 100M-1G (Max 80 Flows)"
    
        def description(self):
            return "This register is used to configure pausethres,flowpausethres,"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00092000
            
        def endAddress(self):
            return 0xffffffff

        class _pausethres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 7
        
            def name(self):
                return "pausethres"
            
            def description(self):
                return "pausethres"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _flowpausethres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 0
        
            def name(self):
                return "flowpausethres"
            
            def description(self):
                return "flowpausethres"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pausethres"] = _AF6CCI0012_RD_ATTETH._txtm_pen_cfg0_100mto1g_g1._pausethres()
            allFields["flowpausethres"] = _AF6CCI0012_RD_ATTETH._txtm_pen_cfg0_100mto1g_g1._flowpausethres()
            return allFields

    class _txtm_pen_cfg1_100mto1g_g1(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Global Configuration 2 Speed Control For Group 100M-1G (Max 80 Flows)"
    
        def description(self):
            return "This register is used to configure numflow,interval"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00092001
            
        def endAddress(self):
            return 0xffffffff

        class _numflow(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 12
        
            def name(self):
                return "numflow"
            
            def description(self):
                return "numflow"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _interval(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "interval"
            
            def description(self):
                return "interval"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["numflow"] = _AF6CCI0012_RD_ATTETH._txtm_pen_cfg1_100mto1g_g1._numflow()
            allFields["interval"] = _AF6CCI0012_RD_ATTETH._txtm_pen_cfg1_100mto1g_g1._interval()
            return allFields

    class _txtm_config_100mto1g_g1_info0(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Configuration 1 Speed Control For Group 100M-1G (Max 80 Flows)"
    
        def description(self):
            return "This register is used to configure lenstep,maxlen,minlen"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_2080 + $idx"
            
        def startAddress(self):
            return 0x00092080
            
        def endAddress(self):
            return 0x000920cf

        class _lenstep(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 28
        
            def name(self):
                return "lenstep"
            
            def description(self):
                return "lenstep"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _maxlen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 14
        
            def name(self):
                return "maxlen"
            
            def description(self):
                return "maxlen"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _minlen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "minlen"
            
            def description(self):
                return "minlen"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lenstep"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g1_info0._lenstep()
            allFields["maxlen"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g1_info0._maxlen()
            allFields["minlen"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g1_info0._minlen()
            return allFields

    class _txtm_config_100mto1g_g1_info1(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Configuration 2 Speed Control For Group 100M-1G (Max 80 Flows)"
    
        def description(self):
            return "This register is used to configure  lenmode,cbs,outport,headersize,besteff,burstmode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_2100 + $idx"
            
        def startAddress(self):
            return 0x00092100
            
        def endAddress(self):
            return 0x0009214f

        class _high_int_cfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 24
        
            def name(self):
                return "high_int_cfg"
            
            def description(self):
                return "high_int_cfg"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _burstmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "burstmode"
            
            def description(self):
                return "burstmode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _besteff(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "besteff"
            
            def description(self):
                return "besteff"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _headersize(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "headersize"
            
            def description(self):
                return "headersize"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _outport(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 16
        
            def name(self):
                return "outport"
            
            def description(self):
                return "outport"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cbs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 2
        
            def name(self):
                return "cbs"
            
            def description(self):
                return "cbs cfg"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lenmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lenmode"
            
            def description(self):
                return "lenmode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["high_int_cfg"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g1_info1._high_int_cfg()
            allFields["burstmode"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g1_info1._burstmode()
            allFields["besteff"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g1_info1._besteff()
            allFields["headersize"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g1_info1._headersize()
            allFields["outport"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g1_info1._outport()
            allFields["cbs"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g1_info1._cbs()
            allFields["lenmode"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g1_info1._lenmode()
            return allFields

    class _txtm_config_100mto1g_g1_info2(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Configuration 3 Speed Control For Group 100M-1G (Max 80 Flows)"
    
        def description(self):
            return "This register is used to configure"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_2180 + $idx"
            
        def startAddress(self):
            return 0x00092180
            
        def endAddress(self):
            return 0x000921cf

        class _low_int_cfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 15
        
            def name(self):
                return "low_int_cfg"
            
            def description(self):
                return "low_int_cfg"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _width_high(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 6
        
            def name(self):
                return "width_high"
            
            def description(self):
                return "width_high"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _width_low(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "width_low"
            
            def description(self):
                return "width_low"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["low_int_cfg"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g1_info2._low_int_cfg()
            allFields["width_high"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g1_info2._width_high()
            allFields["width_low"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g1_info2._width_low()
            return allFields

    class _txtm_config_100mto1g_g1_info3(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Configuration 4 Speed Control For Group 100M-1G (Max 80 Flows)"
    
        def description(self):
            return "This register is used to configure  conv_flow."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_2200 + $idx"
            
        def startAddress(self):
            return 0x00092200
            
        def endAddress(self):
            return 0x0009224f

        class _conv_flow(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "conv_flow"
            
            def description(self):
                return "conv_flow"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["conv_flow"] = _AF6CCI0012_RD_ATTETH._txtm_config_100mto1g_g1_info3._conv_flow()
            return allFields

    class _txtm_status0_100mto1g_g1(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Status 1 Speed Control For Group 100M-1G (Max 80 Flows)"
    
        def description(self):
            return "This register is used to config numoneshot (active)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_2280 + $idx"
            
        def startAddress(self):
            return 0x00092280
            
        def endAddress(self):
            return 0x000922cf

        class _oneshot(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "oneshot"
            
            def description(self):
                return "oneshot"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["oneshot"] = _AF6CCI0012_RD_ATTETH._txtm_status0_100mto1g_g1._oneshot()
            return allFields

    class _txtm_status1_100mto1g_g1(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Status 2 Speed Control For Group 100M-1G (Max 80 Flows)"
    
        def description(self):
            return "This register is used to save status  cur_low_int_cnt ,cur_high_int_cnt."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_2300 + $idx"
            
        def startAddress(self):
            return 0x00092300
            
        def endAddress(self):
            return 0x0009234f

        class _cur_high_int_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 8
        
            def name(self):
                return "cur_high_int_cnt"
            
            def description(self):
                return "current  high interval counter"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cur_low_int_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cur_low_int_cnt"
            
            def description(self):
                return "current low interval counter"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cur_high_int_cnt"] = _AF6CCI0012_RD_ATTETH._txtm_status1_100mto1g_g1._cur_high_int_cnt()
            allFields["cur_low_int_cnt"] = _AF6CCI0012_RD_ATTETH._txtm_status1_100mto1g_g1._cur_low_int_cnt()
            return allFields

    class _txtm_status2_100mto1g_g1(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Status 3 Speed Control For Group 100M-1G (Max 80 Flows)"
    
        def description(self):
            return "This register is used to save status current pktleng, cir_cur"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_2380 + $idx"
            
        def startAddress(self):
            return 0x00092380
            
        def endAddress(self):
            return 0x000923cf

        class _cur_cir(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 16
        
            def name(self):
                return "cur_cir"
            
            def description(self):
                return "current cir low"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cur_pktleng(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cur_pktleng"
            
            def description(self):
                return "current pktleng"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cur_cir"] = _AF6CCI0012_RD_ATTETH._txtm_status2_100mto1g_g1._cur_cir()
            allFields["cur_pktleng"] = _AF6CCI0012_RD_ATTETH._txtm_status2_100mto1g_g1._cur_pktleng()
            return allFields

    class _txtm_status3_100mto1g_g1(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Status 4 Speed Control For Group 100M-1G (Max 80 Flows)"
    
        def description(self):
            return "This register is used to save status cur_cnt_burst,cur_width_burst."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_2400 + $idx"
            
        def startAddress(self):
            return 0x00092400
            
        def endAddress(self):
            return 0x0009244f

        class _cur_width_burst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 14
        
            def name(self):
                return "cur_width_burst"
            
            def description(self):
                return "cur_width_burst"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cur_cnt_burst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cur_cnt_burst"
            
            def description(self):
                return "cur_cnt_burst"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cur_width_burst"] = _AF6CCI0012_RD_ATTETH._txtm_status3_100mto1g_g1._cur_width_burst()
            allFields["cur_cnt_burst"] = _AF6CCI0012_RD_ATTETH._txtm_status3_100mto1g_g1._cur_cnt_burst()
            return allFields

    class _txtm_pen_cfg0_10mto100m_g0(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Global Configuration 1 Speed Control For Group 10M-100M (Max 800 Flows)"
    
        def description(self):
            return "This register is used to configure pausethres,flowpausethres,"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00094000
            
        def endAddress(self):
            return 0xffffffff

        class _pausethres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 11
        
            def name(self):
                return "pausethres"
            
            def description(self):
                return "pausethres"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _flowpausethres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 0
        
            def name(self):
                return "flowpausethres"
            
            def description(self):
                return "flowpausethres"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pausethres"] = _AF6CCI0012_RD_ATTETH._txtm_pen_cfg0_10mto100m_g0._pausethres()
            allFields["flowpausethres"] = _AF6CCI0012_RD_ATTETH._txtm_pen_cfg0_10mto100m_g0._flowpausethres()
            return allFields

    class _txtm_pen_cfg1_10mto100m_g0(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Global Configuration 3 Speed Control For Group 10M-100M (Max 800 Flows)"
    
        def description(self):
            return "This register is used to configure numflow,interval"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00094001
            
        def endAddress(self):
            return 0xffffffff

        class _numflow(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 12
        
            def name(self):
                return "numflow"
            
            def description(self):
                return "numflow"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _interval(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "interval"
            
            def description(self):
                return "interval"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["numflow"] = _AF6CCI0012_RD_ATTETH._txtm_pen_cfg1_10mto100m_g0._numflow()
            allFields["interval"] = _AF6CCI0012_RD_ATTETH._txtm_pen_cfg1_10mto100m_g0._interval()
            return allFields

    class _txtm_config_10mto100m_g0_info0(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Configuration 1 Speed Control For Group 10M-100M (Max 800 Flows)"
    
        def description(self):
            return "This register is used to configure lenstep,maxlen,minlen"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_4400 + $idx"
            
        def startAddress(self):
            return 0x00094400
            
        def endAddress(self):
            return 0x000947ff

        class _lenstep(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 28
        
            def name(self):
                return "lenstep"
            
            def description(self):
                return "lenstep"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _maxlen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 14
        
            def name(self):
                return "maxlen"
            
            def description(self):
                return "maxlen"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _minlen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "minlen"
            
            def description(self):
                return "minlen"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lenstep"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g0_info0._lenstep()
            allFields["maxlen"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g0_info0._maxlen()
            allFields["minlen"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g0_info0._minlen()
            return allFields

    class _txtm_config_10mto100m_g0_info1(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Configuration 3 Speed Control For Group 10M-100M (Max 800 Flows)"
    
        def description(self):
            return "This register is used to configure  lenmode,cbs,outport,headersize,besteff,burstmode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_4800 + $idx"
            
        def startAddress(self):
            return 0x00094800
            
        def endAddress(self):
            return 0x00094bff

        class _high_int_cfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 24
        
            def name(self):
                return "high_int_cfg"
            
            def description(self):
                return "high_int_cfg"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _burstmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "burstmode"
            
            def description(self):
                return "burstmode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _besteff(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "besteff"
            
            def description(self):
                return "besteff"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _headersize(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "headersize"
            
            def description(self):
                return "headersize"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _outport(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 16
        
            def name(self):
                return "outport"
            
            def description(self):
                return "outport"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cbs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 2
        
            def name(self):
                return "cbs"
            
            def description(self):
                return "cbs cfg"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lenmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lenmode"
            
            def description(self):
                return "lenmode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["high_int_cfg"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g0_info1._high_int_cfg()
            allFields["burstmode"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g0_info1._burstmode()
            allFields["besteff"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g0_info1._besteff()
            allFields["headersize"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g0_info1._headersize()
            allFields["outport"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g0_info1._outport()
            allFields["cbs"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g0_info1._cbs()
            allFields["lenmode"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g0_info1._lenmode()
            return allFields

    class _txtm_config_10mto100m_g0_info2(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Configuration 5 Speed Control For Group 10M-100M (Max 800 Flows)"
    
        def description(self):
            return "This register is used to configure"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_4c00 + $idx"
            
        def startAddress(self):
            return 0x00094c00
            
        def endAddress(self):
            return 0x00094fff

        class _low_int_cfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 12
        
            def name(self):
                return "low_int_cfg"
            
            def description(self):
                return "low_int_cfg"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _width_high(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 3
        
            def name(self):
                return "width_high"
            
            def description(self):
                return "width_high"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _width_low(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "width_low"
            
            def description(self):
                return "width_low"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["low_int_cfg"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g0_info2._low_int_cfg()
            allFields["width_high"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g0_info2._width_high()
            allFields["width_low"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g0_info2._width_low()
            return allFields

    class _txtm_config_10mto100m_g0_info3(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Configuration 7 Speed Control For Group 10M-100M (Max 800 Flows)"
    
        def description(self):
            return "This register is used to configure  conv_flow."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_5000 + $idx"
            
        def startAddress(self):
            return 0x00095000
            
        def endAddress(self):
            return 0x000953ff

        class _conv_flow(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "conv_flow"
            
            def description(self):
                return "conv_flow"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["conv_flow"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g0_info3._conv_flow()
            return allFields

    class _txtm_status0_10mto100m_g0(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Status 1 Speed Control For Group 10M-100M (Max 800 Flows)"
    
        def description(self):
            return "This register is used to config numoneshot (active)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_5400 + $idx"
            
        def startAddress(self):
            return 0x00095400
            
        def endAddress(self):
            return 0x000957ff

        class _oneshot(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "oneshot"
            
            def description(self):
                return "oneshot"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["oneshot"] = _AF6CCI0012_RD_ATTETH._txtm_status0_10mto100m_g0._oneshot()
            return allFields

    class _txtm_status1_10mto100m_g0(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Status 3 Speed Control For Group 10M-100M (Max 800 Flows)"
    
        def description(self):
            return "This register is used to save status  cur_low_int_cnt ,cur_high_int_cnt."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_5800 + $idx"
            
        def startAddress(self):
            return 0x00095800
            
        def endAddress(self):
            return 0x00095bff

        class _cur_high_int_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 5
        
            def name(self):
                return "cur_high_int_cnt"
            
            def description(self):
                return "current  high interval counter"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cur_low_int_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cur_low_int_cnt"
            
            def description(self):
                return "current low interval counter"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cur_high_int_cnt"] = _AF6CCI0012_RD_ATTETH._txtm_status1_10mto100m_g0._cur_high_int_cnt()
            allFields["cur_low_int_cnt"] = _AF6CCI0012_RD_ATTETH._txtm_status1_10mto100m_g0._cur_low_int_cnt()
            return allFields

    class _txtm_status2_10mto100m_g0(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Status 5 Speed Control For Group 10M-100M (Max 800 Flows)"
    
        def description(self):
            return "This register is used to save status current pktleng, cir_cur"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_5C00 + $idx"
            
        def startAddress(self):
            return 0x00095c00
            
        def endAddress(self):
            return 0x00095fff

        class _cur_cir(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 16
        
            def name(self):
                return "cur_cir"
            
            def description(self):
                return "current cir low"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cur_pktleng(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cur_pktleng"
            
            def description(self):
                return "current pktleng"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cur_cir"] = _AF6CCI0012_RD_ATTETH._txtm_status2_10mto100m_g0._cur_cir()
            allFields["cur_pktleng"] = _AF6CCI0012_RD_ATTETH._txtm_status2_10mto100m_g0._cur_pktleng()
            return allFields

    class _txtm_status3_10mto100m_g0(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Status 7 Speed Control For Group 10M-100M (Max 800 Flows)"
    
        def description(self):
            return "This register is used to save status cur_cnt_burst,cur_width_burst."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_6000 + $idx"
            
        def startAddress(self):
            return 0x00096000
            
        def endAddress(self):
            return 0x000963ff

        class _cur_width_burst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 14
        
            def name(self):
                return "cur_width_burst"
            
            def description(self):
                return "cur_width_burst"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cur_cnt_burst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cur_cnt_burst"
            
            def description(self):
                return "cur_cnt_burst"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cur_width_burst"] = _AF6CCI0012_RD_ATTETH._txtm_status3_10mto100m_g0._cur_width_burst()
            allFields["cur_cnt_burst"] = _AF6CCI0012_RD_ATTETH._txtm_status3_10mto100m_g0._cur_cnt_burst()
            return allFields

    class _txtm_pen_cfg0_10mto100m_g1(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Global Configuration 1 Speed Control For Group 10M-100M (Max 800 Flows)"
    
        def description(self):
            return "This register is used to configure pausethres,flowpausethres,"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00098000
            
        def endAddress(self):
            return 0xffffffff

        class _pausethres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 11
        
            def name(self):
                return "pausethres"
            
            def description(self):
                return "pausethres"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _flowpausethres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 0
        
            def name(self):
                return "flowpausethres"
            
            def description(self):
                return "flowpausethres"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pausethres"] = _AF6CCI0012_RD_ATTETH._txtm_pen_cfg0_10mto100m_g1._pausethres()
            allFields["flowpausethres"] = _AF6CCI0012_RD_ATTETH._txtm_pen_cfg0_10mto100m_g1._flowpausethres()
            return allFields

    class _txtm_pen_cfg1_10mto100m_g1(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Global Configuration 3 Speed Control For Group 10M-100M (Max 800 Flows)"
    
        def description(self):
            return "This register is used to configure numflow,interval"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00098001
            
        def endAddress(self):
            return 0xffffffff

        class _numflow(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 12
        
            def name(self):
                return "numflow"
            
            def description(self):
                return "numflow"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _interval(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "interval"
            
            def description(self):
                return "interval"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["numflow"] = _AF6CCI0012_RD_ATTETH._txtm_pen_cfg1_10mto100m_g1._numflow()
            allFields["interval"] = _AF6CCI0012_RD_ATTETH._txtm_pen_cfg1_10mto100m_g1._interval()
            return allFields

    class _txtm_config_10mto100m_g1_info0(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Configuration 1 Speed Control For Group 10M-100M (Max 800 Flows)"
    
        def description(self):
            return "This register is used to configure lenstep,maxlen,minlen"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_8400 + $idx"
            
        def startAddress(self):
            return 0x00098400
            
        def endAddress(self):
            return 0x000987ff

        class _lenstep(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 28
        
            def name(self):
                return "lenstep"
            
            def description(self):
                return "lenstep"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _maxlen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 14
        
            def name(self):
                return "maxlen"
            
            def description(self):
                return "maxlen"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _minlen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "minlen"
            
            def description(self):
                return "minlen"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lenstep"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g1_info0._lenstep()
            allFields["maxlen"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g1_info0._maxlen()
            allFields["minlen"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g1_info0._minlen()
            return allFields

    class _txtm_config_10mto100m_g1_info1(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Configuration 3 Speed Control For Group 10M-100M (Max 800 Flows)"
    
        def description(self):
            return "This register is used to configure  lenmode,cbs,outport,headersize,besteff,burstmode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_8800 + $idx"
            
        def startAddress(self):
            return 0x00098800
            
        def endAddress(self):
            return 0x00098bff

        class _high_int_cfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 24
        
            def name(self):
                return "high_int_cfg"
            
            def description(self):
                return "high_int_cfg"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _burstmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "burstmode"
            
            def description(self):
                return "burstmode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _besteff(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "besteff"
            
            def description(self):
                return "besteff"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _headersize(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "headersize"
            
            def description(self):
                return "headersize"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _outport(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 16
        
            def name(self):
                return "outport"
            
            def description(self):
                return "outport"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cbs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 2
        
            def name(self):
                return "cbs"
            
            def description(self):
                return "cbs cfg"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lenmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lenmode"
            
            def description(self):
                return "lenmode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["high_int_cfg"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g1_info1._high_int_cfg()
            allFields["burstmode"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g1_info1._burstmode()
            allFields["besteff"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g1_info1._besteff()
            allFields["headersize"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g1_info1._headersize()
            allFields["outport"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g1_info1._outport()
            allFields["cbs"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g1_info1._cbs()
            allFields["lenmode"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g1_info1._lenmode()
            return allFields

    class _txtm_config_10mto100m_g1_info2(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Configuration 5 Speed Control For Group 10M-100M (Max 800 Flows)"
    
        def description(self):
            return "This register is used to configure"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_8c00 + $idx"
            
        def startAddress(self):
            return 0x00098c00
            
        def endAddress(self):
            return 0x00098fff

        class _low_int_cfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 12
        
            def name(self):
                return "low_int_cfg"
            
            def description(self):
                return "low_int_cfg"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _width_high(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 3
        
            def name(self):
                return "width_high"
            
            def description(self):
                return "width_high"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _width_low(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "width_low"
            
            def description(self):
                return "width_low"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["low_int_cfg"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g1_info2._low_int_cfg()
            allFields["width_high"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g1_info2._width_high()
            allFields["width_low"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g1_info2._width_low()
            return allFields

    class _txtm_config_10mto100m_g1_info3(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Configuration 7 Speed Control For Group 10M-100M (Max 800 Flows)"
    
        def description(self):
            return "This register is used to configure  conv_flow."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_9000 + $idx"
            
        def startAddress(self):
            return 0x00099000
            
        def endAddress(self):
            return 0x000993ff

        class _conv_flow(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "conv_flow"
            
            def description(self):
                return "conv_flow"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["conv_flow"] = _AF6CCI0012_RD_ATTETH._txtm_config_10mto100m_g1_info3._conv_flow()
            return allFields

    class _txtm_status0_10mto100m_g1(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Status 1 Speed Control For Group 10M-100M (Max 800 Flows)"
    
        def description(self):
            return "This register is used to config numoneshot (active)."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_9400 + $idx"
            
        def startAddress(self):
            return 0x00099400
            
        def endAddress(self):
            return 0x000997ff

        class _oneshot(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "oneshot"
            
            def description(self):
                return "oneshot"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["oneshot"] = _AF6CCI0012_RD_ATTETH._txtm_status0_10mto100m_g1._oneshot()
            return allFields

    class _txtm_status1_10mto100m_g1(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Status 3 Speed Control For Group 10M-100M (Max 800 Flows)"
    
        def description(self):
            return "This register is used to save status  cur_low_int_cnt ,cur_high_int_cnt."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_9800 + $idx"
            
        def startAddress(self):
            return 0x00099800
            
        def endAddress(self):
            return 0x00099bff

        class _cur_high_int_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 5
        
            def name(self):
                return "cur_high_int_cnt"
            
            def description(self):
                return "current  high interval counter"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cur_low_int_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cur_low_int_cnt"
            
            def description(self):
                return "current low interval counter"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cur_high_int_cnt"] = _AF6CCI0012_RD_ATTETH._txtm_status1_10mto100m_g1._cur_high_int_cnt()
            allFields["cur_low_int_cnt"] = _AF6CCI0012_RD_ATTETH._txtm_status1_10mto100m_g1._cur_low_int_cnt()
            return allFields

    class _txtm_status2_10mto100m_g1(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Status 5 Speed Control For Group 10M-100M (Max 800 Flows)"
    
        def description(self):
            return "This register is used to save status current pktleng, cir_cur"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_9C00 + $idx"
            
        def startAddress(self):
            return 0x00099c00
            
        def endAddress(self):
            return 0x00099fff

        class _cur_cir(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 16
        
            def name(self):
                return "cur_cir"
            
            def description(self):
                return "current cir low"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cur_pktleng(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cur_pktleng"
            
            def description(self):
                return "current pktleng"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cur_cir"] = _AF6CCI0012_RD_ATTETH._txtm_status2_10mto100m_g1._cur_cir()
            allFields["cur_pktleng"] = _AF6CCI0012_RD_ATTETH._txtm_status2_10mto100m_g1._cur_pktleng()
            return allFields

    class _txtm_status3_10mto100m_g1(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Status 7 Speed Control For Group 10M-100M (Max 800 Flows)"
    
        def description(self):
            return "This register is used to save status cur_cnt_burst,cur_width_burst."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_A000 + $idx"
            
        def startAddress(self):
            return 0x0009a000
            
        def endAddress(self):
            return 0x0009a3ff

        class _cur_width_burst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 14
        
            def name(self):
                return "cur_width_burst"
            
            def description(self):
                return "cur_width_burst"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cur_cnt_burst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cur_cnt_burst"
            
            def description(self):
                return "cur_cnt_burst"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cur_width_burst"] = _AF6CCI0012_RD_ATTETH._txtm_status3_10mto100m_g1._cur_width_burst()
            allFields["cur_cnt_burst"] = _AF6CCI0012_RD_ATTETH._txtm_status3_10mto100m_g1._cur_cnt_burst()
            return allFields

    class _txtm_global_cfg1_6mto10m(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Global Configuration 1 Speed Control For Group 6M-10M (Max 2K Flows)"
    
        def description(self):
            return "This register is used to configure interval and maxflow"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000a0000
            
        def endAddress(self):
            return 0xffffffff

        class _maxflow(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 16
        
            def name(self):
                return "maxflow"
            
            def description(self):
                return "maximum flow"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _interval(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "interval"
            
            def description(self):
                return "numberof clock in interval"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["maxflow"] = _AF6CCI0012_RD_ATTETH._txtm_global_cfg1_6mto10m._maxflow()
            allFields["interval"] = _AF6CCI0012_RD_ATTETH._txtm_global_cfg1_6mto10m._interval()
            return allFields

    class _txtm_global_cfg2_6mto10m(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Global Configuration 2 Speed Control For Group 6M-10M (Max 2K Flows)"
    
        def description(self):
            return "This register is used to configure total hold threshold and max segment count"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000a0001
            
        def endAddress(self):
            return 0xffffffff

        class _maxsegcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 16
        
            def name(self):
                return "maxsegcnt"
            
            def description(self):
                return "maximum segment count"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _holdthres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "holdthres"
            
            def description(self):
                return "total number of clock can be hold in interval"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["maxsegcnt"] = _AF6CCI0012_RD_ATTETH._txtm_global_cfg2_6mto10m._maxsegcnt()
            allFields["holdthres"] = _AF6CCI0012_RD_ATTETH._txtm_global_cfg2_6mto10m._holdthres()
            return allFields

    class _txtm_cfg_6to10m(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Speed Control For Group 6M-10M (Max 2K Flows)"
    
        def description(self):
            return "This register is used to debug only"
            
        def width(self):
            return 128
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xA_6000 + $flowindex"
            
        def startAddress(self):
            return 0x000a6000
            
        def endAddress(self):
            return 0x000a67ff

        class _maxlen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 102
                
            def startBit(self):
                return 89
        
            def name(self):
                return "maxlen"
            
            def description(self):
                return "Max Length"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _minlen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 88
                
            def startBit(self):
                return 75
        
            def name(self):
                return "minlen"
            
            def description(self):
                return "Min Length"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lenmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 74
                
            def startBit(self):
                return 73
        
            def name(self):
                return "lenmode"
            
            def description(self):
                return "Length Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lenstep(AtRegister.AtRegisterField):
            def stopBit(self):
                return 72
                
            def startBit(self):
                return 69
        
            def name(self):
                return "lenstep"
            
            def description(self):
                return "Length step"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _width(AtRegister.AtRegisterField):
            def stopBit(self):
                return 68
                
            def startBit(self):
                return 55
        
            def name(self):
                return "width"
            
            def description(self):
                return "Width"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _intcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 54
                
            def startBit(self):
                return 50
        
            def name(self):
                return "intcnt"
            
            def description(self):
                return "Interval Cnt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cbs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 49
                
            def startBit(self):
                return 36
        
            def name(self):
                return "cbs"
            
            def description(self):
                return "CBS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _outport(AtRegister.AtRegisterField):
            def stopBit(self):
                return 35
                
            def startBit(self):
                return 31
        
            def name(self):
                return "outport"
            
            def description(self):
                return "outport"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _besteff(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "besteff"
            
            def description(self):
                return "best effort"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _flowthres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 15
        
            def name(self):
                return "flowthres"
            
            def description(self):
                return "flow threshold"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _flowid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "flowid"
            
            def description(self):
                return "flow ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["maxlen"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_6to10m._maxlen()
            allFields["minlen"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_6to10m._minlen()
            allFields["lenmode"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_6to10m._lenmode()
            allFields["lenstep"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_6to10m._lenstep()
            allFields["width"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_6to10m._width()
            allFields["intcnt"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_6to10m._intcnt()
            allFields["cbs"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_6to10m._cbs()
            allFields["outport"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_6to10m._outport()
            allFields["besteff"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_6to10m._besteff()
            allFields["flowthres"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_6to10m._flowthres()
            allFields["flowid"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_6to10m._flowid()
            return allFields

    class _txtm_sta_6to10m(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Status Speed Control For Group 6M-10M (Max 2K Flows)"
    
        def description(self):
            return "This register is used to debug only"
            
        def width(self):
            return 64
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xA_2000 + $flowindex"
            
        def startAddress(self):
            return 0x000a2000
            
        def endAddress(self):
            return 0x000a27ff

        class _resetintcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 41
                
            def startBit(self):
                return 40
        
            def name(self):
                return "resetintcnt"
            
            def description(self):
                return "Count number of interval to reset interval count"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _intcntsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 39
                
            def startBit(self):
                return 28
        
            def name(self):
                return "intcntsta"
            
            def description(self):
                return "Current interval count"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cirsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 14
        
            def name(self):
                return "cirsta"
            
            def description(self):
                return "Current CIR"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lensta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lensta"
            
            def description(self):
                return "Current Length"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["resetintcnt"] = _AF6CCI0012_RD_ATTETH._txtm_sta_6to10m._resetintcnt()
            allFields["intcntsta"] = _AF6CCI0012_RD_ATTETH._txtm_sta_6to10m._intcntsta()
            allFields["cirsta"] = _AF6CCI0012_RD_ATTETH._txtm_sta_6to10m._cirsta()
            allFields["lensta"] = _AF6CCI0012_RD_ATTETH._txtm_sta_6to10m._lensta()
            return allFields

    class _txtm_oneshot_cfg_6to10m(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Oneshot Packet Speed Control For Group 6M-10M (Max 2K Flows)"
    
        def description(self):
            return "This register is used to configure number of packet for oneshot mod If number of packet is maximum (0xFFFF) , engine will all not decrease otherwise this value will be  decreased to zero"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return "0xA_4000 + $flowindex"
            
        def startAddress(self):
            return 0x000a4000
            
        def endAddress(self):
            return 0x000a47ff

        class _numpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "numpkt"
            
            def description(self):
                return "Number of packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["numpkt"] = _AF6CCI0012_RD_ATTETH._txtm_oneshot_cfg_6to10m._numpkt()
            return allFields

    class _txtm_global_cfg1_3mto6m(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Global Configuration 1 Speed Control For Group 3M-6M (Max 4K Flows)"
    
        def description(self):
            return "This register is used to configure interval and maxflow"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000a8000
            
        def endAddress(self):
            return 0xffffffff

        class _maxflow(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 16
        
            def name(self):
                return "maxflow"
            
            def description(self):
                return "maximum flow"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _interval(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "interval"
            
            def description(self):
                return "numberof clock in interval"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["maxflow"] = _AF6CCI0012_RD_ATTETH._txtm_global_cfg1_3mto6m._maxflow()
            allFields["interval"] = _AF6CCI0012_RD_ATTETH._txtm_global_cfg1_3mto6m._interval()
            return allFields

    class _txtm_global_cfg2_3mto6m(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Global Configuration 2 Speed Control For Group 3M-6M (Max 4K Flows)"
    
        def description(self):
            return "This register is used to configure total hold threshold and max segment count"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000a8001
            
        def endAddress(self):
            return 0xffffffff

        class _maxsegcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 16
        
            def name(self):
                return "maxsegcnt"
            
            def description(self):
                return "maximum segment count"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _holdthres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "holdthres"
            
            def description(self):
                return "total number of clock can be hold in interval"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["maxsegcnt"] = _AF6CCI0012_RD_ATTETH._txtm_global_cfg2_3mto6m._maxsegcnt()
            allFields["holdthres"] = _AF6CCI0012_RD_ATTETH._txtm_global_cfg2_3mto6m._holdthres()
            return allFields

    class _txtm_cfg_3mto6m(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Speed Control For Group 3M-6M (Max 4K Flows)"
    
        def description(self):
            return "This register is used to debug only"
            
        def width(self):
            return 128
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xA_E000 + $flowindex"
            
        def startAddress(self):
            return 0x000ae000
            
        def endAddress(self):
            return 0x000aefff

        class _maxlen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 102
                
            def startBit(self):
                return 89
        
            def name(self):
                return "maxlen"
            
            def description(self):
                return "Max Length"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _minlen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 88
                
            def startBit(self):
                return 75
        
            def name(self):
                return "minlen"
            
            def description(self):
                return "Min Length"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lenmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 74
                
            def startBit(self):
                return 73
        
            def name(self):
                return "lenmode"
            
            def description(self):
                return "Length Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lenstep(AtRegister.AtRegisterField):
            def stopBit(self):
                return 72
                
            def startBit(self):
                return 69
        
            def name(self):
                return "lenstep"
            
            def description(self):
                return "Length step"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _width(AtRegister.AtRegisterField):
            def stopBit(self):
                return 68
                
            def startBit(self):
                return 55
        
            def name(self):
                return "width"
            
            def description(self):
                return "Width"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _intcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 54
                
            def startBit(self):
                return 50
        
            def name(self):
                return "intcnt"
            
            def description(self):
                return "Interval Cnt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cbs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 49
                
            def startBit(self):
                return 36
        
            def name(self):
                return "cbs"
            
            def description(self):
                return "CBS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _outport(AtRegister.AtRegisterField):
            def stopBit(self):
                return 35
                
            def startBit(self):
                return 31
        
            def name(self):
                return "outport"
            
            def description(self):
                return "outport"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _besteff(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "besteff"
            
            def description(self):
                return "best effort"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _flowthres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 15
        
            def name(self):
                return "flowthres"
            
            def description(self):
                return "flow threshold"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _flowid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "flowid"
            
            def description(self):
                return "flow ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["maxlen"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_3mto6m._maxlen()
            allFields["minlen"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_3mto6m._minlen()
            allFields["lenmode"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_3mto6m._lenmode()
            allFields["lenstep"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_3mto6m._lenstep()
            allFields["width"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_3mto6m._width()
            allFields["intcnt"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_3mto6m._intcnt()
            allFields["cbs"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_3mto6m._cbs()
            allFields["outport"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_3mto6m._outport()
            allFields["besteff"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_3mto6m._besteff()
            allFields["flowthres"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_3mto6m._flowthres()
            allFields["flowid"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_3mto6m._flowid()
            return allFields

    class _txtm_sta_3mto6m(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Status Speed Control For Group 3M-6M (Max 4K Flows)"
    
        def description(self):
            return "This register is used to debug only"
            
        def width(self):
            return 64
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xA_A000 + $flowindex"
            
        def startAddress(self):
            return 0x000aa000
            
        def endAddress(self):
            return 0x000aafff

        class _resetintcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 41
                
            def startBit(self):
                return 40
        
            def name(self):
                return "resetintcnt"
            
            def description(self):
                return "Count number of interval to reset interval count"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _intcntsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 39
                
            def startBit(self):
                return 28
        
            def name(self):
                return "intcntsta"
            
            def description(self):
                return "Current interval count"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cirsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 14
        
            def name(self):
                return "cirsta"
            
            def description(self):
                return "Current CIR"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lensta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lensta"
            
            def description(self):
                return "Current Length"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["resetintcnt"] = _AF6CCI0012_RD_ATTETH._txtm_sta_3mto6m._resetintcnt()
            allFields["intcntsta"] = _AF6CCI0012_RD_ATTETH._txtm_sta_3mto6m._intcntsta()
            allFields["cirsta"] = _AF6CCI0012_RD_ATTETH._txtm_sta_3mto6m._cirsta()
            allFields["lensta"] = _AF6CCI0012_RD_ATTETH._txtm_sta_3mto6m._lensta()
            return allFields

    class _txtm_oneshot_cfg_3mto6m(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Oneshot Packet Speed Control For Group 3M-6M (Max 4K Flows)"
    
        def description(self):
            return "This register is used to configure number of packet for oneshot mode If number of packet is maximum (0xFFFF) , engine will all not decrease otherwise this value will be  decreased to zero"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return "0xA_C000 + $flowindex"
            
        def startAddress(self):
            return 0x000ac000
            
        def endAddress(self):
            return 0x000acfff

        class _numpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "numpkt"
            
            def description(self):
                return "Number of packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["numpkt"] = _AF6CCI0012_RD_ATTETH._txtm_oneshot_cfg_3mto6m._numpkt()
            return allFields

    class _txtm_global_cfg1_128kto3m(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Global Configuration 1 Speed Control For Group 128K-3M (Max 8K Flows)"
    
        def description(self):
            return "This register is used to configure interval and maxflow"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000b0000
            
        def endAddress(self):
            return 0xffffffff

        class _maxflow(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 16
        
            def name(self):
                return "maxflow"
            
            def description(self):
                return "maximum flow"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _interval(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "interval"
            
            def description(self):
                return "numberof clock in interval"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["maxflow"] = _AF6CCI0012_RD_ATTETH._txtm_global_cfg1_128kto3m._maxflow()
            allFields["interval"] = _AF6CCI0012_RD_ATTETH._txtm_global_cfg1_128kto3m._interval()
            return allFields

    class _txtm_global_cfg2_128kto3m(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Global Configuration 2 Speed Control For Group 128K-3M (Max 8K Flows)"
    
        def description(self):
            return "This register is used to configure total hold threshold and max segment count"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000b0001
            
        def endAddress(self):
            return 0xffffffff

        class _maxsegcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 16
        
            def name(self):
                return "maxsegcnt"
            
            def description(self):
                return "maximum segment count"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _holdthres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "holdthres"
            
            def description(self):
                return "total number of clock can be hold in interval"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["maxsegcnt"] = _AF6CCI0012_RD_ATTETH._txtm_global_cfg2_128kto3m._maxsegcnt()
            allFields["holdthres"] = _AF6CCI0012_RD_ATTETH._txtm_global_cfg2_128kto3m._holdthres()
            return allFields

    class _txtm_cfg_128kto3m(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Speed Control For Group 128K-3M (Max 8K Flows)"
    
        def description(self):
            return "This register is used to debug only"
            
        def width(self):
            return 128
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xB_6000 + $flowindex"
            
        def startAddress(self):
            return 0x000b6000
            
        def endAddress(self):
            return 0x000b7fff

        class _maxlen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 102
                
            def startBit(self):
                return 89
        
            def name(self):
                return "maxlen"
            
            def description(self):
                return "Max Length"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _minlen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 88
                
            def startBit(self):
                return 75
        
            def name(self):
                return "minlen"
            
            def description(self):
                return "Min Length"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lenmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 74
                
            def startBit(self):
                return 73
        
            def name(self):
                return "lenmode"
            
            def description(self):
                return "Length Mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lenstep(AtRegister.AtRegisterField):
            def stopBit(self):
                return 72
                
            def startBit(self):
                return 69
        
            def name(self):
                return "lenstep"
            
            def description(self):
                return "Length step"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _width(AtRegister.AtRegisterField):
            def stopBit(self):
                return 68
                
            def startBit(self):
                return 55
        
            def name(self):
                return "width"
            
            def description(self):
                return "Width"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _intcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 54
                
            def startBit(self):
                return 50
        
            def name(self):
                return "intcnt"
            
            def description(self):
                return "Interval Cnt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cbs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 49
                
            def startBit(self):
                return 36
        
            def name(self):
                return "cbs"
            
            def description(self):
                return "CBS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _outport(AtRegister.AtRegisterField):
            def stopBit(self):
                return 35
                
            def startBit(self):
                return 31
        
            def name(self):
                return "outport"
            
            def description(self):
                return "outport"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _besteff(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "besteff"
            
            def description(self):
                return "best effort"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _flowthres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 15
        
            def name(self):
                return "flowthres"
            
            def description(self):
                return "flow threshold"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _flowid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "flowid"
            
            def description(self):
                return "flow ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["maxlen"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_128kto3m._maxlen()
            allFields["minlen"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_128kto3m._minlen()
            allFields["lenmode"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_128kto3m._lenmode()
            allFields["lenstep"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_128kto3m._lenstep()
            allFields["width"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_128kto3m._width()
            allFields["intcnt"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_128kto3m._intcnt()
            allFields["cbs"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_128kto3m._cbs()
            allFields["outport"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_128kto3m._outport()
            allFields["besteff"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_128kto3m._besteff()
            allFields["flowthres"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_128kto3m._flowthres()
            allFields["flowid"] = _AF6CCI0012_RD_ATTETH._txtm_cfg_128kto3m._flowid()
            return allFields

    class _txtm_sta_128kto3m(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Status Speed Control For Group 128K-3M (Max 8K Flows)"
    
        def description(self):
            return "This register is used to debug only"
            
        def width(self):
            return 64
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xB_2000 + $flowindex"
            
        def startAddress(self):
            return 0x000b2000
            
        def endAddress(self):
            return 0x000b3fff

        class _resetintcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 41
                
            def startBit(self):
                return 40
        
            def name(self):
                return "resetintcnt"
            
            def description(self):
                return "Count number of interval to reset interval count"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _intcntsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 39
                
            def startBit(self):
                return 28
        
            def name(self):
                return "intcntsta"
            
            def description(self):
                return "Current interval count"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cirsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 14
        
            def name(self):
                return "cirsta"
            
            def description(self):
                return "Current CIR"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lensta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lensta"
            
            def description(self):
                return "Current Length"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["resetintcnt"] = _AF6CCI0012_RD_ATTETH._txtm_sta_128kto3m._resetintcnt()
            allFields["intcntsta"] = _AF6CCI0012_RD_ATTETH._txtm_sta_128kto3m._intcntsta()
            allFields["cirsta"] = _AF6CCI0012_RD_ATTETH._txtm_sta_128kto3m._cirsta()
            allFields["lensta"] = _AF6CCI0012_RD_ATTETH._txtm_sta_128kto3m._lensta()
            return allFields

    class _txtm_oneshot_cfg_128kto3m(AtRegister.AtRegister):
        def name(self):
            return "TXTM Register Oneshot Packet Speed Control For Group 128K-3M (Max 8K Flows)"
    
        def description(self):
            return "This register is used to configure number of packet for oneshot mode If number of packet is maximum (0xFFFF) , engine will all not decrease otherwise this value will be  decreased to zero"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return "0xB_4000 + $flowindex"
            
        def startAddress(self):
            return 0x000b4000
            
        def endAddress(self):
            return 0x000b5fff

        class _numpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "numpkt"
            
            def description(self):
                return "Number of packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["numpkt"] = _AF6CCI0012_RD_ATTETH._txtm_oneshot_cfg_128kto3m._numpkt()
            return allFields

    class _txfrm_mode_cfg(AtRegister.AtRegister):
        def name(self):
            return "TX Frame Mode config"
    
        def description(self):
            return "This register is used to configure frame mode HDL_PATH     : begin : txfrmcore.icfg_mod.ramsta.ram.ram[$flowid] HDL_PATH     : end :"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x10_0000 + $flowid"
            
        def startAddress(self):
            return 0x00100000
            
        def endAddress(self):
            return 0x00107fff

        class _shortlen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 20
        
            def name(self):
                return "shortlen"
            
            def description(self):
                return "ShortLen size - insert padding"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _owrhdrsize(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 12
        
            def name(self):
                return "owrhdrsize"
            
            def description(self):
                return "Overwrite Header size"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _frmtype(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 9
        
            def name(self):
                return "frmtype"
            
            def description(self):
                return "1: Eth, 2: Ipv4, 3: ipv6, 0: others"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _atttagen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "atttagen"
            
            def description(self):
                return "ATT Tag Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _hdrsize(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "hdrsize"
            
            def description(self):
                return "Header size"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["shortlen"] = _AF6CCI0012_RD_ATTETH._txfrm_mode_cfg._shortlen()
            allFields["owrhdrsize"] = _AF6CCI0012_RD_ATTETH._txfrm_mode_cfg._owrhdrsize()
            allFields["frmtype"] = _AF6CCI0012_RD_ATTETH._txfrm_mode_cfg._frmtype()
            allFields["atttagen"] = _AF6CCI0012_RD_ATTETH._txfrm_mode_cfg._atttagen()
            allFields["hdrsize"] = _AF6CCI0012_RD_ATTETH._txfrm_mode_cfg._hdrsize()
            return allFields

    class _txfrm_hdr1_cfg(AtRegister.AtRegister):
        def name(self):
            return "TXTM Frame header1 config"
    
        def description(self):
            return "This register is used to configure frame mode HDL_PATH     : begin : txfrmcore.hdcfg1.icfg_hdrcfg1.ramsta.ram.ram[$flowid] HDL_PATH     : end :"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x10_8000 + $flowid"
            
        def startAddress(self):
            return 0x00108000
            
        def endAddress(self):
            return 0x00109fff

        class _header(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "header"
            
            def description(self):
                return "Header buffer"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["header"] = _AF6CCI0012_RD_ATTETH._txfrm_hdr1_cfg._header()
            return allFields

    class _txfrm_hdr2_cfg(AtRegister.AtRegister):
        def name(self):
            return "TXTM Frame header2 config"
    
        def description(self):
            return "This register is used to configure frame mode HDL_PATH     : begin : txfrmcore.hdcfg2.icfg_hdrcfg2.ramsta.ram.ram[$flowid] HDL_PATH     : end :"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x11_0000 + $flowid"
            
        def startAddress(self):
            return 0x00110000
            
        def endAddress(self):
            return 0x00111fff

        class _header(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "header"
            
            def description(self):
                return "Header buffer"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["header"] = _AF6CCI0012_RD_ATTETH._txfrm_hdr2_cfg._header()
            return allFields

    class _txfrm_hdr3_cfg(AtRegister.AtRegister):
        def name(self):
            return "TXTM Frame header3 config"
    
        def description(self):
            return "This register is used to configure frame mode HDL_PATH     : begin : txfrmcore.hdcfg3.icfg_hdrcfg3.ramsta.ram.ram[$flowid] HDL_PATH     : end :"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x11_8000 + $flowid"
            
        def startAddress(self):
            return 0x00118000
            
        def endAddress(self):
            return 0x00119fff

        class _header(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "header"
            
            def description(self):
                return "Header buffer"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["header"] = _AF6CCI0012_RD_ATTETH._txfrm_hdr3_cfg._header()
            return allFields

    class _txfrm_hdr4_cfg(AtRegister.AtRegister):
        def name(self):
            return "TXTM Frame header4 config"
    
        def description(self):
            return "This register is used to configure frame mode HDL_PATH     : begin : txfrmcore.hdcfg4.icfg_hdrcfg4.ramsta.ram.ram[$flowid] HDL_PATH     : end :"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x12_0000 + $flowid"
            
        def startAddress(self):
            return 0x00120000
            
        def endAddress(self):
            return 0x00121fff

        class _header(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "header"
            
            def description(self):
                return "Header buffer"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["header"] = _AF6CCI0012_RD_ATTETH._txfrm_hdr4_cfg._header()
            return allFields

    class _txfrm_hdr5_cfg(AtRegister.AtRegister):
        def name(self):
            return "TXTM Frame header5 config"
    
        def description(self):
            return "This register is used to configure frame mode HDL_PATH     : begin : txfrmcore.hdcfg5.icfg_hdrcfg5.ramsta.ram.ram[$flowid] HDL_PATH     : end :"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x12_8000 + $flowid"
            
        def startAddress(self):
            return 0x00128000
            
        def endAddress(self):
            return 0x00129fff

        class _header(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "header"
            
            def description(self):
                return "Header buffer"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["header"] = _AF6CCI0012_RD_ATTETH._txfrm_hdr5_cfg._header()
            return allFields

    class _txfrm_hdr6_cfg(AtRegister.AtRegister):
        def name(self):
            return "TXTM Frame header6 config"
    
        def description(self):
            return "This register is used to configure frame mode HDL_PATH     : begin : txfrmcore.hdcfg6.icfg_hdrcfg6.ramsta.ram.ram[$flowid] HDL_PATH     : end :"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x13_0000 + $flowid"
            
        def startAddress(self):
            return 0x00130000
            
        def endAddress(self):
            return 0x00131fff

        class _header(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "header"
            
            def description(self):
                return "Header buffer"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["header"] = _AF6CCI0012_RD_ATTETH._txfrm_hdr6_cfg._header()
            return allFields

    class _txfrm_hdr7_cfg(AtRegister.AtRegister):
        def name(self):
            return "TXTM Frame header7 config"
    
        def description(self):
            return "This register is used to configure frame mode"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x13_8000 + $flowid"
            
        def startAddress(self):
            return 0x00138000
            
        def endAddress(self):
            return 0x00139fff

        class _header(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "header"
            
            def description(self):
                return "Header buffer"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["header"] = _AF6CCI0012_RD_ATTETH._txfrm_hdr7_cfg._header()
            return allFields

    class _txfrm_hdr8_cfg(AtRegister.AtRegister):
        def name(self):
            return "TXTM Frame header8 config"
    
        def description(self):
            return "This register is used to configure frame mode"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x14_0000 + $flowid"
            
        def startAddress(self):
            return 0x00140000
            
        def endAddress(self):
            return 0x00141fff

        class _header(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "header"
            
            def description(self):
                return "Header buffer"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["header"] = _AF6CCI0012_RD_ATTETH._txfrm_hdr8_cfg._header()
            return allFields

    class _Datgen_register(AtRegister.AtRegister):
        def name(self):
            return "Datagen control"
    
        def description(self):
            return "This register is used to configure datagen mode HDL_PATH     : begin : txfrmcore.datgen8b.icfg_cfg.ramsta.ram.ram[$flowid] HDL_PATH     : end :"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x14_8000 + $flowid"
            
        def startAddress(self):
            return 0x00148000
            
        def endAddress(self):
            return 0x00149fff

        class _invmod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "invmod"
            
            def description(self):
                return "invert or non-invert dat"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _datfix(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 3
        
            def name(self):
                return "datfix"
            
            def description(self):
                return "data fix pattern"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _datmod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "datmod"
            
            def description(self):
                return "mode data"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["invmod"] = _AF6CCI0012_RD_ATTETH._Datgen_register._invmod()
            allFields["datfix"] = _AF6CCI0012_RD_ATTETH._Datgen_register._datfix()
            allFields["datmod"] = _AF6CCI0012_RD_ATTETH._Datgen_register._datmod()
            return allFields

    class _rxfrm_hdrsize(AtRegister.AtRegister):
        def name(self):
            return "RxHeaderSize"
    
        def description(self):
            return "This register is used to configure datagen mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x02_0000 + $typeid + $lkpid*32"
            
        def startAddress(self):
            return 0x00020000
            
        def endAddress(self):
            return 0x00021fff

        class _rxflowsel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 17
        
            def name(self):
                return "rxflowsel"
            
            def description(self):
                return "Rx flow select - valid when no tag   0: DA, 1: SA, 2: CVLAN, 3: SVLAN, 4: LABEL1, 5: LABEL2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _rxtagen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "rxtagen"
            
            def description(self):
                return "Rx Tag Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _rxmonhdrsize(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "rxmonhdrsize"
            
            def description(self):
                return "Rx header size"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _rxhdrsize(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxhdrsize"
            
            def description(self):
                return "Rx header size"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxflowsel"] = _AF6CCI0012_RD_ATTETH._rxfrm_hdrsize._rxflowsel()
            allFields["rxtagen"] = _AF6CCI0012_RD_ATTETH._rxfrm_hdrsize._rxtagen()
            allFields["rxmonhdrsize"] = _AF6CCI0012_RD_ATTETH._rxfrm_hdrsize._rxmonhdrsize()
            allFields["rxhdrsize"] = _AF6CCI0012_RD_ATTETH._rxfrm_hdrsize._rxhdrsize()
            return allFields

    class _Datmon_register(AtRegister.AtRegister):
        def name(self):
            return "Datamon control"
    
        def description(self):
            return "This register is used to configure datagen mode HDL_PATH     : begin : rxfrmcore.datmon8b.icfg_cfg.ramcfg.array.ram[$flowid] HDL_PATH     : end :"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x18_0000 + $flowid"
            
        def startAddress(self):
            return 0x00180000
            
        def endAddress(self):
            return 0x00181fff

        class _invmod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "invmod"
            
            def description(self):
                return "invert or non-invert dat"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _datfix(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 3
        
            def name(self):
                return "datfix"
            
            def description(self):
                return "data fix pattern"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _datmod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "datmod"
            
            def description(self):
                return "mode data"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["invmod"] = _AF6CCI0012_RD_ATTETH._Datmon_register._invmod()
            allFields["datfix"] = _AF6CCI0012_RD_ATTETH._Datmon_register._datfix()
            allFields["datmod"] = _AF6CCI0012_RD_ATTETH._Datmon_register._datmod()
            return allFields

    class _Datmon_stareg(AtRegister.AtRegister):
        def name(self):
            return "Datamon Status"
    
        def description(self):
            return "This register is used to configure datagen mode"
            
        def width(self):
            return 64
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x18_4000 + $flowid"
            
        def startAddress(self):
            return 0x00184000
            
        def endAddress(self):
            return 0x00185fff

        class _synsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 32
                
            def startBit(self):
                return 32
        
            def name(self):
                return "synsta"
            
            def description(self):
                return "prbs syn"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _prbssta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "prbssta"
            
            def description(self):
                return "status"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["synsta"] = _AF6CCI0012_RD_ATTETH._Datmon_stareg._synsta()
            allFields["prbssta"] = _AF6CCI0012_RD_ATTETH._Datmon_stareg._prbssta()
            return allFields

    class _rxfrm_hdr1_cfg(AtRegister.AtRegister):
        def name(self):
            return "RX Frame header1 config"
    
        def description(self):
            return "This register is used to configure frame mode HDL_PATH     : begin : rxfrmcore.hdcfg1.icfg_hdrcfg1.ramsta.ram.ram[$flowid] HDL_PATH     : end :"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1F_0000 + $flowid"
            
        def startAddress(self):
            return 0x001f0000
            
        def endAddress(self):
            return 0x001f1fff

        class _header(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "header"
            
            def description(self):
                return "Header buffer"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["header"] = _AF6CCI0012_RD_ATTETH._rxfrm_hdr1_cfg._header()
            return allFields

    class _rxfrm_hdr2_cfg(AtRegister.AtRegister):
        def name(self):
            return "RX Frame header2 config"
    
        def description(self):
            return "This register is used to configure frame mode HDL_PATH     : begin : rxfrmcore.hdcfg2.icfg_hdrcfg2.ramsta.ram.ram[$flowid] HDL_PATH     : end :"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1F_2000 + $flowid"
            
        def startAddress(self):
            return 0x001f2000
            
        def endAddress(self):
            return 0x001f3fff

        class _header(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "header"
            
            def description(self):
                return "Header buffer"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["header"] = _AF6CCI0012_RD_ATTETH._rxfrm_hdr2_cfg._header()
            return allFields

    class _rxfrm_hdr3_cfg(AtRegister.AtRegister):
        def name(self):
            return "RX Frame header3 config"
    
        def description(self):
            return "This register is used to configure frame mode HDL_PATH     : begin : rxfrmcore.hdcfg3.icfg_hdrcfg3.ramsta.ram.ram[$flowid] HDL_PATH     : end :"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1F_4000 + $flowid"
            
        def startAddress(self):
            return 0x001f4000
            
        def endAddress(self):
            return 0x001f5fff

        class _header(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "header"
            
            def description(self):
                return "Header buffer"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["header"] = _AF6CCI0012_RD_ATTETH._rxfrm_hdr3_cfg._header()
            return allFields

    class _rxfrm_hdr4_cfg(AtRegister.AtRegister):
        def name(self):
            return "RX Frame header4 config"
    
        def description(self):
            return "This register is used to configure frame mode HDL_PATH     : begin : rxfrmcore.hdcfg4.icfg_hdrcfg4.ramsta.ram.ram[$flowid] HDL_PATH     : end :"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1F_6000 + $flowid"
            
        def startAddress(self):
            return 0x001f6000
            
        def endAddress(self):
            return 0x001f7fff

        class _header(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "header"
            
            def description(self):
                return "Header buffer"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["header"] = _AF6CCI0012_RD_ATTETH._rxfrm_hdr4_cfg._header()
            return allFields

    class _rxfrm_hdr5_cfg(AtRegister.AtRegister):
        def name(self):
            return "RX Frame header5 config"
    
        def description(self):
            return "This register is used to configure frame mode HDL_PATH     : begin : rxfrmcore.hdcfg5.icfg_hdrcfg5.ramsta.ram.ram[$flowid] HDL_PATH     : end :"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1F_8000 + $flowid"
            
        def startAddress(self):
            return 0x001f8000
            
        def endAddress(self):
            return 0x001f9fff

        class _header(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "header"
            
            def description(self):
                return "Header buffer"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["header"] = _AF6CCI0012_RD_ATTETH._rxfrm_hdr5_cfg._header()
            return allFields

    class _rxfrm_hdr6_cfg(AtRegister.AtRegister):
        def name(self):
            return "RX Frame header6 config"
    
        def description(self):
            return "This register is used to configure frame mode HDL_PATH     : begin : rxfrmcore.hdcfg6.icfg_hdrcfg6.ramsta.ram.ram[$flowid] HDL_PATH     : end :"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1F_A000 + $flowid"
            
        def startAddress(self):
            return 0x001fa000
            
        def endAddress(self):
            return 0x001fbfff

        class _header(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "header"
            
            def description(self):
                return "Header buffer"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["header"] = _AF6CCI0012_RD_ATTETH._rxfrm_hdr6_cfg._header()
            return allFields

    class _rxfrm_hdr7_cfg(AtRegister.AtRegister):
        def name(self):
            return "RX Frame header7 config"
    
        def description(self):
            return "This register is used to configure frame mode"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1F_C000 + $flowid"
            
        def startAddress(self):
            return 0x001fc000
            
        def endAddress(self):
            return 0x001fdfff

        class _header(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "header"
            
            def description(self):
                return "Header buffer"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["header"] = _AF6CCI0012_RD_ATTETH._rxfrm_hdr7_cfg._header()
            return allFields

    class _rxfrm_hdr8_cfg(AtRegister.AtRegister):
        def name(self):
            return "RX Frame header8 config"
    
        def description(self):
            return "This register is used to configure frame mode HDL_PATH     : begin : rxfrmcore.icfg_hdrcfg8.ramsta.ram.ram[$flowid] HDL_PATH     : end :"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1F_E000 + $flowid"
            
        def startAddress(self):
            return 0x001fe000
            
        def endAddress(self):
            return 0x001fffff

        class _header(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "header"
            
            def description(self):
                return "Header buffer/mask"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["header"] = _AF6CCI0012_RD_ATTETH._rxfrm_hdr8_cfg._header()
            return allFields

    class _rxfrm_hdrerrcnt(AtRegister.AtRegister):
        def name(self):
            return "Rx  Header error count"
    
        def description(self):
            return "This register is used to monitor Prbs error counter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x1E_8000 +$r2c*8192 +  $flowid"
            
        def startAddress(self):
            return 0x001e8000
            
        def endAddress(self):
            return 0x001ebfff

        class _errcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "errcnt"
            
            def description(self):
                return "Header ErrCnt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["errcnt"] = _AF6CCI0012_RD_ATTETH._rxfrm_hdrerrcnt._errcnt()
            return allFields

    class _datmon_prbserrcnt(AtRegister.AtRegister):
        def name(self):
            return "Dat mon Prbs error count"
    
        def description(self):
            return "This register is used to monitor Prbs error counter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x19_0000 +$r2c*8192 + $flowid"
            
        def startAddress(self):
            return 0x00190000
            
        def endAddress(self):
            return 0x00193fff

        class _errcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "errcnt"
            
            def description(self):
                return "Prbs ErrCnt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["errcnt"] = _AF6CCI0012_RD_ATTETH._datmon_prbserrcnt._errcnt()
            return allFields

    class _pktmon_seqerrcnt(AtRegister.AtRegister):
        def name(self):
            return "Pkt Seq error count"
    
        def description(self):
            return "This register is used to monitor pkt seq error"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x1A_0000 +$r2c*8192 + $flowid"
            
        def startAddress(self):
            return 0x001a0000
            
        def endAddress(self):
            return 0x001a3fff

        class _errcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "errcnt"
            
            def description(self):
                return "Seq  ErrCnt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["errcnt"] = _AF6CCI0012_RD_ATTETH._pktmon_seqerrcnt._errcnt()
            return allFields

    class _pktmon_bytecnt(AtRegister.AtRegister):
        def name(self):
            return "Pkt Byte count"
    
        def description(self):
            return "This register is used to monitor byte counter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x1B_0000 +$r2c*8192 + $flowid"
            
        def startAddress(self):
            return 0x001b0000
            
        def endAddress(self):
            return 0x001b3fff

        class _bytecnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "bytecnt"
            
            def description(self):
                return "Byte  Cnt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["bytecnt"] = _AF6CCI0012_RD_ATTETH._pktmon_bytecnt._bytecnt()
            return allFields

    class _pktmon_pktcnt(AtRegister.AtRegister):
        def name(self):
            return "Pkt count"
    
        def description(self):
            return "This register is used to monitor packet counter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x1C_0000 +$r2c*8192 + $flowid"
            
        def startAddress(self):
            return 0x001c0000
            
        def endAddress(self):
            return 0x001c3fff

        class _pktcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktcnt"
            
            def description(self):
                return "Pkt  Cnt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktcnt"] = _AF6CCI0012_RD_ATTETH._pktmon_pktcnt._pktcnt()
            return allFields

    class _flowmon_speed(AtRegister.AtRegister):
        def name(self):
            return "Flow Speed Mon"
    
        def description(self):
            return "This register is used to monitor speed of flow"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x1D_0000 +$r2c*8192 + $flowid"
            
        def startAddress(self):
            return 0x001d0000
            
        def endAddress(self):
            return 0x001d3fff

        class _speed(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "speed"
            
            def description(self):
                return "speed of flow"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["speed"] = _AF6CCI0012_RD_ATTETH._flowmon_speed._speed()
            return allFields

    class _rx_pktdump_cfg(AtRegister.AtRegister):
        def name(self):
            return "Pkt Dump config"
    
        def description(self):
            return "This register is used to monitor speed of flow"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x001e0000
            
        def endAddress(self):
            return 0xffffffff

        class _oneshot(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "oneshot"
            
            def description(self):
                return "Dump one shot"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _pact(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "pact"
            
            def description(self):
                return "Dump active"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _errstop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "errstop"
            
            def description(self):
                return "Error stop"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _porten(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "porten"
            
            def description(self):
                return "dump all flow"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _flowid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 0
        
            def name(self):
                return "flowid"
            
            def description(self):
                return "data"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["oneshot"] = _AF6CCI0012_RD_ATTETH._rx_pktdump_cfg._oneshot()
            allFields["pact"] = _AF6CCI0012_RD_ATTETH._rx_pktdump_cfg._pact()
            allFields["errstop"] = _AF6CCI0012_RD_ATTETH._rx_pktdump_cfg._errstop()
            allFields["porten"] = _AF6CCI0012_RD_ATTETH._rx_pktdump_cfg._porten()
            allFields["flowid"] = _AF6CCI0012_RD_ATTETH._rx_pktdump_cfg._flowid()
            return allFields

    class _rx_pktdump_dat(AtRegister.AtRegister):
        def name(self):
            return "Pkt Dump Dat"
    
        def description(self):
            return "This register is used to monitor speed of flow"
            
        def width(self):
            return 128
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x1E_0400 +$r2c*1024 + $loc"
            
        def startAddress(self):
            return 0x001e0400
            
        def endAddress(self):
            return 0x001e0bff

        class _prbserr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 70
                
            def startBit(self):
                return 70
        
            def name(self):
                return "prbserr"
            
            def description(self):
                return "start of packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fcserr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 69
                
            def startBit(self):
                return 69
        
            def name(self):
                return "fcserr"
            
            def description(self):
                return "start of packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _nob(AtRegister.AtRegisterField):
            def stopBit(self):
                return 68
                
            def startBit(self):
                return 66
        
            def name(self):
                return "nob"
            
            def description(self):
                return "Number of packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _eop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 65
                
            def startBit(self):
                return 65
        
            def name(self):
                return "eop"
            
            def description(self):
                return "End  of packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _sop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 64
                
            def startBit(self):
                return 64
        
            def name(self):
                return "sop"
            
            def description(self):
                return "start of packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _dat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dat"
            
            def description(self):
                return "data"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prbserr"] = _AF6CCI0012_RD_ATTETH._rx_pktdump_dat._prbserr()
            allFields["fcserr"] = _AF6CCI0012_RD_ATTETH._rx_pktdump_dat._fcserr()
            allFields["nob"] = _AF6CCI0012_RD_ATTETH._rx_pktdump_dat._nob()
            allFields["eop"] = _AF6CCI0012_RD_ATTETH._rx_pktdump_dat._eop()
            allFields["sop"] = _AF6CCI0012_RD_ATTETH._rx_pktdump_dat._sop()
            allFields["dat"] = _AF6CCI0012_RD_ATTETH._rx_pktdump_dat._dat()
            return allFields

    class _rx_pktdump_exdat(AtRegister.AtRegister):
        def name(self):
            return "Pkt Dump Exp"
    
        def description(self):
            return "This register is used to monitor speed of flow"
            
        def width(self):
            return 64
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x1E_0C00 + $loc"
            
        def startAddress(self):
            return 0x001e0c00
            
        def endAddress(self):
            return 0x001e07ff

        class _dat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dat"
            
            def description(self):
                return "data"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dat"] = _AF6CCI0012_RD_ATTETH._rx_pktdump_exdat._dat()
            return allFields

    class _tx_pktdump_cfg(AtRegister.AtRegister):
        def name(self):
            return "Tx Pkt Dump config"
    
        def description(self):
            return "This register is used to monitor speed of flow"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00150000
            
        def endAddress(self):
            return 0xffffffff

        class _oneshot(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "oneshot"
            
            def description(self):
                return "Dump one shot"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _pact(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "pact"
            
            def description(self):
                return "Dump active"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _errstop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "errstop"
            
            def description(self):
                return "Error stop"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _porten(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "porten"
            
            def description(self):
                return "dump all flow"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _flowid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 0
        
            def name(self):
                return "flowid"
            
            def description(self):
                return "data"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["oneshot"] = _AF6CCI0012_RD_ATTETH._tx_pktdump_cfg._oneshot()
            allFields["pact"] = _AF6CCI0012_RD_ATTETH._tx_pktdump_cfg._pact()
            allFields["errstop"] = _AF6CCI0012_RD_ATTETH._tx_pktdump_cfg._errstop()
            allFields["porten"] = _AF6CCI0012_RD_ATTETH._tx_pktdump_cfg._porten()
            allFields["flowid"] = _AF6CCI0012_RD_ATTETH._tx_pktdump_cfg._flowid()
            return allFields

    class _tx_pktdump_dat(AtRegister.AtRegister):
        def name(self):
            return "Tx Pkt Dump Dat"
    
        def description(self):
            return "This register is used to monitor speed of flow"
            
        def width(self):
            return 128
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x15_0400 +$r2c*1024 + $loc"
            
        def startAddress(self):
            return 0x00150400
            
        def endAddress(self):
            return 0x00150bff

        class _prbserr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 70
                
            def startBit(self):
                return 70
        
            def name(self):
                return "prbserr"
            
            def description(self):
                return "start of packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fcserr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 69
                
            def startBit(self):
                return 69
        
            def name(self):
                return "fcserr"
            
            def description(self):
                return "start of packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _nob(AtRegister.AtRegisterField):
            def stopBit(self):
                return 68
                
            def startBit(self):
                return 66
        
            def name(self):
                return "nob"
            
            def description(self):
                return "Number of packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _eop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 65
                
            def startBit(self):
                return 65
        
            def name(self):
                return "eop"
            
            def description(self):
                return "End  of packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _sop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 64
                
            def startBit(self):
                return 64
        
            def name(self):
                return "sop"
            
            def description(self):
                return "start of packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _dat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dat"
            
            def description(self):
                return "data"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prbserr"] = _AF6CCI0012_RD_ATTETH._tx_pktdump_dat._prbserr()
            allFields["fcserr"] = _AF6CCI0012_RD_ATTETH._tx_pktdump_dat._fcserr()
            allFields["nob"] = _AF6CCI0012_RD_ATTETH._tx_pktdump_dat._nob()
            allFields["eop"] = _AF6CCI0012_RD_ATTETH._tx_pktdump_dat._eop()
            allFields["sop"] = _AF6CCI0012_RD_ATTETH._tx_pktdump_dat._sop()
            allFields["dat"] = _AF6CCI0012_RD_ATTETH._tx_pktdump_dat._dat()
            return allFields

    class _pktgen_pktcnt(AtRegister.AtRegister):
        def name(self):
            return "TX Pkt count"
    
        def description(self):
            return "This register is used to monitor packet counter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x15_8000 +$r2c*8192 + $flowid"
            
        def startAddress(self):
            return 0x00158000
            
        def endAddress(self):
            return 0x0015bfff

        class _pktcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktcnt"
            
            def description(self):
                return "Pkt  Cnt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktcnt"] = _AF6CCI0012_RD_ATTETH._pktgen_pktcnt._pktcnt()
            return allFields

    class _pktgen_bytecnt(AtRegister.AtRegister):
        def name(self):
            return "TX Byte count"
    
        def description(self):
            return "This register is used to monitor byte counter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x16_0000 +$r2c*8192 + $flowid"
            
        def startAddress(self):
            return 0x00160000
            
        def endAddress(self):
            return 0x00163fff

        class _bytecnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "bytecnt"
            
            def description(self):
                return "Byte  Cnt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["bytecnt"] = _AF6CCI0012_RD_ATTETH._pktgen_bytecnt._bytecnt()
            return allFields

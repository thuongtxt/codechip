import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0012_RD_GLB(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["VersionID"] = _AF6CCI0012_RD_GLB._VersionID()
        allRegisters["Active"] = _AF6CCI0012_RD_GLB._Active()
        allRegisters["Timeout"] = _AF6CCI0012_RD_GLB._Timeout()
        allRegisters["TestReg"] = _AF6CCI0012_RD_GLB._TestReg()
        return allRegisters

    class _VersionID(AtRegister.AtRegister):
        def name(self):
            return "Device Version ID"
    
        def description(self):
            return "This register indicates the module' name and version."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _Year(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "Year"
            
            def description(self):
                return "2013"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Week(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Week"
            
            def description(self):
                return "Week Synthesized FPGA"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Day(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Day"
            
            def description(self):
                return "Day Synthesized FPGA"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _VerID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "VerID"
            
            def description(self):
                return "FPGA Version"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _NumID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "NumID"
            
            def description(self):
                return "Number of FPGA Version"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Year"] = _AF6CCI0012_RD_GLB._VersionID._Year()
            allFields["Week"] = _AF6CCI0012_RD_GLB._VersionID._Week()
            allFields["Day"] = _AF6CCI0012_RD_GLB._VersionID._Day()
            allFields["VerID"] = _AF6CCI0012_RD_GLB._VersionID._VerID()
            allFields["NumID"] = _AF6CCI0012_RD_GLB._VersionID._NumID()
            return allFields

    class _Active(AtRegister.AtRegister):
        def name(self):
            return "GLB Sub-Core Active"
    
        def description(self):
            return "This register indicates the active ports."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000001
            
        def endAddress(self):
            return 0xffffffff

        class _SubCoreEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SubCoreEn"
            
            def description(self):
                return "Enable per sub-core"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SubCoreEn"] = _AF6CCI0012_RD_GLB._Active._SubCoreEn()
            return allFields

    class _Timeout(AtRegister.AtRegister):
        def name(self):
            return "GLB FPGA Timeout Threshold"
    
        def description(self):
            return "This register is used to flush port's FIFOs."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _DayThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 11
        
            def name(self):
                return "DayThres"
            
            def description(self):
                return "Day Threshold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _HourThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 6
        
            def name(self):
                return "HourThres"
            
            def description(self):
                return "Hour Threshold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MinThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MinThres"
            
            def description(self):
                return "Minute Threshold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DayThres"] = _AF6CCI0012_RD_GLB._Timeout._DayThres()
            allFields["HourThres"] = _AF6CCI0012_RD_GLB._Timeout._HourThres()
            allFields["MinThres"] = _AF6CCI0012_RD_GLB._Timeout._MinThres()
            return allFields

    class _TestReg(AtRegister.AtRegister):
        def name(self):
            return "GLB Test Register"
    
        def description(self):
            return "This register is used to configure port's parameters"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return "0x00_0008 + $regid"
            
        def startAddress(self):
            return 0x00000008
            
        def endAddress(self):
            return 0x0000000f

        class _TestReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TestReg"
            
            def description(self):
                return "Test Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TestReg"] = _AF6CCI0012_RD_GLB._TestReg._TestReg()
            return allFields

import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0012_RD_TOP(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["top_o_control0"] = _AF6CCI0012_RD_TOP._top_o_control0()
        allRegisters["top_o_control1"] = _AF6CCI0012_RD_TOP._top_o_control1()
        return allRegisters

    class _top_o_control0(AtRegister.AtRegister):
        def name(self):
            return "Top ETH-Port Control 0"
    
        def description(self):
            return "This register indicates the active ports."
            
        def width(self):
            return 10
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000040
            
        def endAddress(self):
            return 0xffffffff

        class _E10G_MAC1Sel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "E10G_MAC1Sel"
            
            def description(self):
                return "MAC1 Select port ID 0x0: XFI_Port#0 0x1: XFI_Port#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["E10G_MAC1Sel"] = _AF6CCI0012_RD_TOP._top_o_control0._E10G_MAC1Sel()
            return allFields

    class _top_o_control1(AtRegister.AtRegister):
        def name(self):
            return "Top ETH-Port Control 1"
    
        def description(self):
            return "This register indicates the active ports."
            
        def width(self):
            return 10
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000041
            
        def endAddress(self):
            return 0xffffffff

        class _E10G_TxEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "E10G_TxEn"
            
            def description(self):
                return "Tx XFI Enable bit_00: XFI_Port#0 bit_01: XFI_Port#1 bit_02: XFI_Port#2 bit_03: XFI_Port#3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["E10G_TxEn"] = _AF6CCI0012_RD_TOP._top_o_control1._E10G_TxEn()
            return allFields

import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0012_RD_eXAUI_XFI_Bridge(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["cfg0_pen"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._cfg0_pen()
        allRegisters["cfg1_pen"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._cfg1_pen()
        allRegisters["cfg2_pen"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._cfg2_pen()
        allRegisters["cfg3_pen"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._cfg3_pen()
        allRegisters["pmcnt_pen"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._pmcnt_pen()
        allRegisters["stk0_pen"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk0_pen()
        allRegisters["stk1_pen"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk1_pen()
        allRegisters["stk2_pen"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk2_pen()
        allRegisters["sta0_pen"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._sta0_pen()
        allRegisters["sta1_pen"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._sta1_pen()
        return allRegisters

    class _cfg0_pen(AtRegister.AtRegister):
        def name(self):
            return "Global Configure 0"
    
        def description(self):
            return "Used to configure: - Active MUX and eXAUI#0 Buffer - MTU check"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000"
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _MTU(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 4
        
            def name(self):
                return "MTU"
            
            def description(self):
                return "Maximum Transmit Unit for eXAUI#0 Buffer"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Active(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Active"
            
            def description(self):
                return "Active Engine"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MTU"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._cfg0_pen._MTU()
            allFields["Active"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._cfg0_pen._Active()
            return allFields

    class _cfg1_pen(AtRegister.AtRegister):
        def name(self):
            return "Global Configure 0"
    
        def description(self):
            return "Used to configure: - Force status MUX Engine - Flush MUX Engine"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x001"
            
        def startAddress(self):
            return 0x00000001
            
        def endAddress(self):
            return 0xffffffff

        class _CFG_Force_MUX_Engine_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "CFG_Force_MUX_Engine_En"
            
            def description(self):
                return "CFG_Force_MUX_Engine_En"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _CFG_MUX_Engine_Read_Source_ID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "CFG_MUX_Engine_Read_Source_ID"
            
            def description(self):
                return "CFG_MUX_Engine_Read_Source_ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _CFG_MUX_Engine_State(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CFG_MUX_Engine_State"
            
            def description(self):
                return "CFG_MUX_Engine_State"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CFG_Force_MUX_Engine_En"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._cfg1_pen._CFG_Force_MUX_Engine_En()
            allFields["CFG_MUX_Engine_Read_Source_ID"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._cfg1_pen._CFG_MUX_Engine_Read_Source_ID()
            allFields["CFG_MUX_Engine_State"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._cfg1_pen._CFG_MUX_Engine_State()
            return allFields

    class _cfg2_pen(AtRegister.AtRegister):
        def name(self):
            return "PTCH Number for eXAUI to XFI Direction"
    
        def description(self):
            return "Used to configure Offset for eXAUI#0 to XFI - eXAUI format: Channel_ID[1 Byte] + Type[1 Byte] + Payload + BIP8[1 Byte] - XFI for format for eXAUI#0: 80 + [Channel+Offset][1 Byte] + Payload + FCS[4 Byte] - XFI for format for eXAUI#1(EOS): 80 + PTCH_ID[1 Byte] + Payload + FCS[4 Byte]"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x002"
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0xffffffff

        class _Vlaninsert_Dis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "Vlaninsert_Dis"
            
            def description(self):
                return "VLAN Insert Disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Channel_ID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Channel_ID"
            
            def description(self):
                return "Channel_ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _PTCHID_eXAUI1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PTCHID_eXAUI1"
            
            def description(self):
                return "PTCH number Value"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _OFFSET_eXAUI0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "OFFSET_eXAUI0"
            
            def description(self):
                return "Offset Value"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Vlaninsert_Dis"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._cfg2_pen._Vlaninsert_Dis()
            allFields["Channel_ID"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._cfg2_pen._Channel_ID()
            allFields["PTCHID_eXAUI1"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._cfg2_pen._PTCHID_eXAUI1()
            allFields["OFFSET_eXAUI0"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._cfg2_pen._OFFSET_eXAUI0()
            return allFields

    class _cfg3_pen(AtRegister.AtRegister):
        def name(self):
            return "VLAN LUT for 64 VCG of eXAUI#1 to XFI"
    
        def description(self):
            return "VLAN LUT from channel ID. - eXAUI format: Channel_ID[1 Byte] + Type[1 Byte] + Payload + BIP8[1 Byte] -  XFI for format for eXAUI#0: 80 + [Channel+Offset][1 Byte] + Payload + FCS[4 Byte] -  XFI for format for eXAUI#1(EOS): 80 + PTCH_ID[1 Byte] + Payload[DA+SA+VLAN(insert)+....] + FCS[4 Byte]"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x100+$chid"
            
        def startAddress(self):
            return 0x00000100
            
        def endAddress(self):
            return 0x000001ff

        class _EtheType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "EtheType"
            
            def description(self):
                return "Ethernet type"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _EtherPri(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 13
        
            def name(self):
                return "EtherPri"
            
            def description(self):
                return "Priority"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _EtherCFI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "EtherCFI"
            
            def description(self):
                return "CFI"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _EtherVLANID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "EtherVLANID"
            
            def description(self):
                return "Vlan ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["EtheType"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._cfg3_pen._EtheType()
            allFields["EtherPri"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._cfg3_pen._EtherPri()
            allFields["EtherCFI"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._cfg3_pen._EtherCFI()
            allFields["EtherVLANID"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._cfg3_pen._EtherVLANID()
            return allFields

    class _pmcnt_pen(AtRegister.AtRegister):
        def name(self):
            return "Global Counter for MUX and eXAUI#0 Buffer"
    
        def description(self):
            return "Support Global Counter: - cntid = 00 : Packet Input MUX Source0 for PW - cntid = 01 : Packet Input MUX Source1 for eXAUI#0 - cntid = 02 : Packet Input MUX Source2 for iMSG/EOP - cntid = 03 : Packet Input MUX Source3 for eXAUI#1 - cntid = 04 : Byte Input MUX Source0 for PW - cntid = 05 : Byte Input MUX Source1 for eXAUI#0 - cntid = 06 : Byte Input MUX Source2 for iMSG/EOP - cntid = 07 : Byte Input MUX Source3 for eXAUI#1 - cntid = 08 : Packet Output MUX Source0 for PW - cntid = 09 : Packet Output MUX Source1 for eXAUI#0 - cntid = 10 : Packet Output MUX Source2 for iMSG/EOP - cntid = 11 : Packet Output MUX Source3 for eXAUI#1 - cntid = 12 : Byte Output MUX Source0 for PW - cntid = 13 : Byte Output MUX Source1 for eXAUI#0 - cntid = 14 : Byte Output MUX Source2 for iMSG/EOP - cntid = 15 : Byte Output MUX Source3 for eXAUI#1 - cntid = 16 : Packet Output to TX MAC Source0 for PW - cntid = 17 : Packet Output to TX MAC Source1 for eXAUI#0 - cntid = 18 : Packet Output to TX MAC Source2 for iMSG/EOP - cntid = 19 : Packet Output to TX MAC Source3 for eXAUI#1 - cntid = 20 : Byte Output to TX MAC Source0 for PW - cntid = 21 : Byte Output to TX MAC Source1 for eXAUI#0 - cntid = 22 : Byte Output to TX MAC Source2 for iMSG/EOP - cntid = 23 : Byte Output to TX MAC Source3 for eXAUI#1 - cntid = 24 : Input eXAUI#0 Buffer Error(FCS or MTU over) - cntid = 25 : Input eXAUI#0 Buffer Monitor MTU over - cntid = 26 : eXAUI#0 Buffer Full - cntid = 27 : Reserved - cntid = 28 : Reserved - cntid = 29 : Reserved - cntid = 30 : Reserved - cntid = 31 : Reserved"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x800+ $cntid"
            
        def startAddress(self):
            return 0x00000800
            
        def endAddress(self):
            return 0x0000081f

        class _mux_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mux_cnt"
            
            def description(self):
                return "Value of counter"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mux_cnt"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._pmcnt_pen._mux_cnt()
            return allFields

    class _stk0_pen(AtRegister.AtRegister):
        def name(self):
            return "Stick 0"
    
        def description(self):
            return "Used to debug"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000400
            
        def endAddress(self):
            return 0xffffffff

        class _Input_From_PW_path(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "Input_From_PW_path"
            
            def description(self):
                return "Input From PW Path"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Input_From_eXAUI0_path(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "Input_From_eXAUI0_path"
            
            def description(self):
                return "Input From eXAUI0 Path"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Input_From_iMSG_EOP_path(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "Input_From_iMSG_EOP_path"
            
            def description(self):
                return "Input From iMSG_EOP Path"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Input_From_eXAUI1_path(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "Input_From_eXAUI1_path"
            
            def description(self):
                return "Input From eXAUI1 Path"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Output_Request_To_PW_path(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "Output_Request_To_PW_path"
            
            def description(self):
                return "Output Request to PW Path"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Output_Request_To_eXAUI0_path(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "Output_Request_To_eXAUI0_path"
            
            def description(self):
                return "Output Request to eXAUI0 Path"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Output_Request_To_iMSG_EOP_path(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "Output_Request_To_iMSG_EOP_path"
            
            def description(self):
                return "Output Request to iMSG_EOP Path"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Output_Request_To_eXAUI1_path(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "Output_Request_To_eXAUI1_path"
            
            def description(self):
                return "Output Request to eXAUI1 Path"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Output_MUX_Source0_for_PW_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "Output_MUX_Source0_for_PW_En"
            
            def description(self):
                return "Output MUX Source0 for PW Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Output_MUX_Source1_for_eXAUI0_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "Output_MUX_Source1_for_eXAUI0_En"
            
            def description(self):
                return "Output MUX Source1 for eXAUI0 Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Output_MUX_Source2_for_iMSG_EOP_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "Output_MUX_Source2_for_iMSG_EOP_En"
            
            def description(self):
                return "Output MUX Source2 for iMSG_EOP Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Output_MUX_Source3_for_eXAUI1_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "Output_MUX_Source3_for_eXAUI1_En"
            
            def description(self):
                return "Output MUX Source3 for eXAUI1 Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TX_MAC_Request_to_MUX(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "TX_MAC_Request_to_MUX"
            
            def description(self):
                return "TX MAC Request to MUX"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MUX_Output_Valid_to_tx_MAC(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "MUX_Output_Valid_to_tx_MAC"
            
            def description(self):
                return "MUX Output Valid to tx MAC"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MUX_Request_to_all_Source(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "MUX_Request_to_all_Source"
            
            def description(self):
                return "MUX Request to all Source"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MUX_Output_Valid_to_PTCH_Engine(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "MUX_Output_Valid_to_PTCH_Engine"
            
            def description(self):
                return "MUX Output Valid to PTCH Engine"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Input_Valid_To_MUX_Source0_for_PW_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "Input_Valid_To_MUX_Source0_for_PW_En"
            
            def description(self):
                return "Input Valid To MUX Source0 for PW Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Input_Valid_To_MUX_Source1_for_eXAUI0_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "Input_Valid_To_MUX_Source1_for_eXAUI0_En"
            
            def description(self):
                return "Input Valid To MUX Source1 for eXAUI0 Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Input_Valid_To_MUX_Source2_for_iMSG_EOP_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "Input_Valid_To_MUX_Source2_for_iMSG_EOP_En"
            
            def description(self):
                return "Input Valid To MUX Source2 for iMSG_EOP Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Input_Valid_To_MUX_Source3_for_eXAUI1_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Input_Valid_To_MUX_Source3_for_eXAUI1_En"
            
            def description(self):
                return "Input Valid To MUX Source3 for eXAUI1 Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Output_Request_To_FIFO_Source0_for_PW_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "Output_Request_To_FIFO_Source0_for_PW_En"
            
            def description(self):
                return "Output Request To FIFO Source0 for PW Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Output_Request_To_FIFO_Source1_for_eXAUI0_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "Output_Request_To_FIFO_Source1_for_eXAUI0_En"
            
            def description(self):
                return "Output Request To FIFO Source1 for eXAUI0 Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Output_Request_To_FIFO_Source2_for_iMSG_EOP_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "Output_Request_To_FIFO_Source2_for_iMSG_EOP_En"
            
            def description(self):
                return "Output Request To FIFO Source2 for iMSG_EOP Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Output_Request_To_FIFO_Source3_for_eXAUI1_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Output_Request_To_FIFO_Source3_for_eXAUI1_En"
            
            def description(self):
                return "Output Request To FIFO Source3 for eXAUI1 Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FiFO_Source3_Write_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "FiFO_Source3_Write_Err"
            
            def description(self):
                return "FiFO Source3 Write Err"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FiFO_Source2_Write_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FiFO_Source2_Write_Err"
            
            def description(self):
                return "FiFO Source2 Write Err"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FiFO_Source1_Write_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FiFO_Source1_Write_Err"
            
            def description(self):
                return "FiFO Source1 Write Err"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FiFO_Source0_Write_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FiFO_Source0_Write_Err"
            
            def description(self):
                return "FiFO Source0 Write Err"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FiFO_Source3_Read_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FiFO_Source3_Read_Err"
            
            def description(self):
                return "FiFO Source3 Read Err"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FiFO_Source2_Read_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FiFO_Source2_Read_Err"
            
            def description(self):
                return "FiFO Source2 Read Err"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FiFO_Source1_Read_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FiFO_Source1_Read_Err"
            
            def description(self):
                return "FiFO Source1 Read Err"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FiFO_Source0_Read_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FiFO_Source0_Read_Err"
            
            def description(self):
                return "FiFO Source0 Read Err"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Input_From_PW_path"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk0_pen._Input_From_PW_path()
            allFields["Input_From_eXAUI0_path"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk0_pen._Input_From_eXAUI0_path()
            allFields["Input_From_iMSG_EOP_path"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk0_pen._Input_From_iMSG_EOP_path()
            allFields["Input_From_eXAUI1_path"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk0_pen._Input_From_eXAUI1_path()
            allFields["Output_Request_To_PW_path"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk0_pen._Output_Request_To_PW_path()
            allFields["Output_Request_To_eXAUI0_path"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk0_pen._Output_Request_To_eXAUI0_path()
            allFields["Output_Request_To_iMSG_EOP_path"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk0_pen._Output_Request_To_iMSG_EOP_path()
            allFields["Output_Request_To_eXAUI1_path"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk0_pen._Output_Request_To_eXAUI1_path()
            allFields["Output_MUX_Source0_for_PW_En"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk0_pen._Output_MUX_Source0_for_PW_En()
            allFields["Output_MUX_Source1_for_eXAUI0_En"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk0_pen._Output_MUX_Source1_for_eXAUI0_En()
            allFields["Output_MUX_Source2_for_iMSG_EOP_En"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk0_pen._Output_MUX_Source2_for_iMSG_EOP_En()
            allFields["Output_MUX_Source3_for_eXAUI1_En"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk0_pen._Output_MUX_Source3_for_eXAUI1_En()
            allFields["TX_MAC_Request_to_MUX"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk0_pen._TX_MAC_Request_to_MUX()
            allFields["MUX_Output_Valid_to_tx_MAC"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk0_pen._MUX_Output_Valid_to_tx_MAC()
            allFields["MUX_Request_to_all_Source"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk0_pen._MUX_Request_to_all_Source()
            allFields["MUX_Output_Valid_to_PTCH_Engine"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk0_pen._MUX_Output_Valid_to_PTCH_Engine()
            allFields["Input_Valid_To_MUX_Source0_for_PW_En"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk0_pen._Input_Valid_To_MUX_Source0_for_PW_En()
            allFields["Input_Valid_To_MUX_Source1_for_eXAUI0_En"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk0_pen._Input_Valid_To_MUX_Source1_for_eXAUI0_En()
            allFields["Input_Valid_To_MUX_Source2_for_iMSG_EOP_En"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk0_pen._Input_Valid_To_MUX_Source2_for_iMSG_EOP_En()
            allFields["Input_Valid_To_MUX_Source3_for_eXAUI1_En"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk0_pen._Input_Valid_To_MUX_Source3_for_eXAUI1_En()
            allFields["Output_Request_To_FIFO_Source0_for_PW_En"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk0_pen._Output_Request_To_FIFO_Source0_for_PW_En()
            allFields["Output_Request_To_FIFO_Source1_for_eXAUI0_En"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk0_pen._Output_Request_To_FIFO_Source1_for_eXAUI0_En()
            allFields["Output_Request_To_FIFO_Source2_for_iMSG_EOP_En"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk0_pen._Output_Request_To_FIFO_Source2_for_iMSG_EOP_En()
            allFields["Output_Request_To_FIFO_Source3_for_eXAUI1_En"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk0_pen._Output_Request_To_FIFO_Source3_for_eXAUI1_En()
            allFields["FiFO_Source3_Write_Err"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk0_pen._FiFO_Source3_Write_Err()
            allFields["FiFO_Source2_Write_Err"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk0_pen._FiFO_Source2_Write_Err()
            allFields["FiFO_Source1_Write_Err"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk0_pen._FiFO_Source1_Write_Err()
            allFields["FiFO_Source0_Write_Err"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk0_pen._FiFO_Source0_Write_Err()
            allFields["FiFO_Source3_Read_Err"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk0_pen._FiFO_Source3_Read_Err()
            allFields["FiFO_Source2_Read_Err"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk0_pen._FiFO_Source2_Read_Err()
            allFields["FiFO_Source1_Read_Err"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk0_pen._FiFO_Source1_Read_Err()
            allFields["FiFO_Source0_Read_Err"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk0_pen._FiFO_Source0_Read_Err()
            return allFields

    class _stk1_pen(AtRegister.AtRegister):
        def name(self):
            return "Stick 1"
    
        def description(self):
            return "Used to debug"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000401
            
        def endAddress(self):
            return 0xffffffff

        class _MUX_Engine_State_SOP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "MUX_Engine_State_SOP"
            
            def description(self):
                return "MUX Engine State SOP"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MUX_Engine_State_EOP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "MUX_Engine_State_EOP"
            
            def description(self):
                return "MUX Engine State EOP"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MUX_Engine_State_ERR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "MUX_Engine_State_ERR"
            
            def description(self):
                return "MUX Engine State ERR"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MUX_Engine_State_IDLE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "MUX_Engine_State_IDLE"
            
            def description(self):
                return "MUX Engine State IDLE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MUX_Engine_Source3_State_IDLE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "MUX_Engine_Source3_State_IDLE"
            
            def description(self):
                return "MUX Engine Source3 State IDLE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MUX_Engine_Source2_State_IDLE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "MUX_Engine_Source2_State_IDLE"
            
            def description(self):
                return "MUX Engine Source2 State IDLE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MUX_Engine_Source1_State_IDLE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "MUX_Engine_Source1_State_IDLE"
            
            def description(self):
                return "MUX Engine Source1 State IDLE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MUX_Engine_Source0_State_IDLE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "MUX_Engine_Source0_State_IDLE"
            
            def description(self):
                return "MUX Engine Source0 State IDLE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MUX_Engine_Source3_State_EOP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "MUX_Engine_Source3_State_EOP"
            
            def description(self):
                return "MUX Engine Source3 State EOP"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MUX_Engine_Source2_State_EOP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "MUX_Engine_Source2_State_EOP"
            
            def description(self):
                return "MUX Engine Source2 State EOP"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MUX_Engine_Source1_State_EOP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "MUX_Engine_Source1_State_EOP"
            
            def description(self):
                return "MUX Engine Source1 State EOP"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MUX_Engine_Source0_State_EOP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "MUX_Engine_Source0_State_EOP"
            
            def description(self):
                return "MUX Engine Source0 State EOP"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MUX_Engine_Source3_State_SOP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "MUX_Engine_Source3_State_SOP"
            
            def description(self):
                return "MUX Engine Source3 State SOP"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MUX_Engine_Source2_State_SOP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "MUX_Engine_Source2_State_SOP"
            
            def description(self):
                return "MUX Engine Source2 State SOP"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MUX_Engine_Source1_State_SOP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "MUX_Engine_Source1_State_SOP"
            
            def description(self):
                return "MUX Engine Source1 State SOP"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MUX_Engine_Source0_State_SOP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "MUX_Engine_Source0_State_SOP"
            
            def description(self):
                return "MUX Engine Source0 State SOP"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MUX_Engine_Source3_Error_IDLE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "MUX_Engine_Source3_Error_IDLE"
            
            def description(self):
                return "MUX Engine Source3 Error IDLE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MUX_Engine_Source2_Error_IDLE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "MUX_Engine_Source2_Error_IDLE"
            
            def description(self):
                return "MUX Engine Source2 Error IDLE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MUX_Engine_Source1_Error_IDLE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "MUX_Engine_Source1_Error_IDLE"
            
            def description(self):
                return "MUX Engine Source1 Error IDLE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MUX_Engine_Source0_Error_IDLE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "MUX_Engine_Source0_Error_IDLE"
            
            def description(self):
                return "MUX Engine Source0 Error IDLE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MUX_Engine_Source3_Error_EOP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "MUX_Engine_Source3_Error_EOP"
            
            def description(self):
                return "MUX Engine Source3 Error EOP"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MUX_Engine_Source2_Error_EOP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "MUX_Engine_Source2_Error_EOP"
            
            def description(self):
                return "MUX Engine Source2 Error EOP"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MUX_Engine_Source1_Error_EOP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "MUX_Engine_Source1_Error_EOP"
            
            def description(self):
                return "MUX Engine Source1 Error EOP"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MUX_Engine_Source0_Error_EOP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "MUX_Engine_Source0_Error_EOP"
            
            def description(self):
                return "MUX Engine Source0 Error EOP"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MUX_Engine_Source3_Error_SOP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "MUX_Engine_Source3_Error_SOP"
            
            def description(self):
                return "MUX Engine Source3 Error SOP"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MUX_Engine_Source2_Error_SOP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "MUX_Engine_Source2_Error_SOP"
            
            def description(self):
                return "MUX Engine Source2 Error SOP"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MUX_Engine_Source1_Error_SOP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "MUX_Engine_Source1_Error_SOP"
            
            def description(self):
                return "MUX Engine Source1 Error SOP"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MUX_Engine_Source0_Error_SOP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "MUX_Engine_Source0_Error_SOP"
            
            def description(self):
                return "MUX Engine Source0 Error SOP"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MUX_Engine_Conflict_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "MUX_Engine_Conflict_0"
            
            def description(self):
                return "MUX Engine Conflict 0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MUX_Engine_Conflict_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MUX_Engine_Conflict_1"
            
            def description(self):
                return "MUX Engine Conflict 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MUX_Engine_State_SOP"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk1_pen._MUX_Engine_State_SOP()
            allFields["MUX_Engine_State_EOP"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk1_pen._MUX_Engine_State_EOP()
            allFields["MUX_Engine_State_ERR"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk1_pen._MUX_Engine_State_ERR()
            allFields["MUX_Engine_State_IDLE"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk1_pen._MUX_Engine_State_IDLE()
            allFields["MUX_Engine_Source3_State_IDLE"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk1_pen._MUX_Engine_Source3_State_IDLE()
            allFields["MUX_Engine_Source2_State_IDLE"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk1_pen._MUX_Engine_Source2_State_IDLE()
            allFields["MUX_Engine_Source1_State_IDLE"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk1_pen._MUX_Engine_Source1_State_IDLE()
            allFields["MUX_Engine_Source0_State_IDLE"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk1_pen._MUX_Engine_Source0_State_IDLE()
            allFields["MUX_Engine_Source3_State_EOP"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk1_pen._MUX_Engine_Source3_State_EOP()
            allFields["MUX_Engine_Source2_State_EOP"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk1_pen._MUX_Engine_Source2_State_EOP()
            allFields["MUX_Engine_Source1_State_EOP"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk1_pen._MUX_Engine_Source1_State_EOP()
            allFields["MUX_Engine_Source0_State_EOP"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk1_pen._MUX_Engine_Source0_State_EOP()
            allFields["MUX_Engine_Source3_State_SOP"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk1_pen._MUX_Engine_Source3_State_SOP()
            allFields["MUX_Engine_Source2_State_SOP"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk1_pen._MUX_Engine_Source2_State_SOP()
            allFields["MUX_Engine_Source1_State_SOP"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk1_pen._MUX_Engine_Source1_State_SOP()
            allFields["MUX_Engine_Source0_State_SOP"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk1_pen._MUX_Engine_Source0_State_SOP()
            allFields["MUX_Engine_Source3_Error_IDLE"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk1_pen._MUX_Engine_Source3_Error_IDLE()
            allFields["MUX_Engine_Source2_Error_IDLE"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk1_pen._MUX_Engine_Source2_Error_IDLE()
            allFields["MUX_Engine_Source1_Error_IDLE"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk1_pen._MUX_Engine_Source1_Error_IDLE()
            allFields["MUX_Engine_Source0_Error_IDLE"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk1_pen._MUX_Engine_Source0_Error_IDLE()
            allFields["MUX_Engine_Source3_Error_EOP"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk1_pen._MUX_Engine_Source3_Error_EOP()
            allFields["MUX_Engine_Source2_Error_EOP"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk1_pen._MUX_Engine_Source2_Error_EOP()
            allFields["MUX_Engine_Source1_Error_EOP"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk1_pen._MUX_Engine_Source1_Error_EOP()
            allFields["MUX_Engine_Source0_Error_EOP"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk1_pen._MUX_Engine_Source0_Error_EOP()
            allFields["MUX_Engine_Source3_Error_SOP"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk1_pen._MUX_Engine_Source3_Error_SOP()
            allFields["MUX_Engine_Source2_Error_SOP"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk1_pen._MUX_Engine_Source2_Error_SOP()
            allFields["MUX_Engine_Source1_Error_SOP"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk1_pen._MUX_Engine_Source1_Error_SOP()
            allFields["MUX_Engine_Source0_Error_SOP"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk1_pen._MUX_Engine_Source0_Error_SOP()
            allFields["Reserved"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk1_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk1_pen._Reserved()
            allFields["MUX_Engine_Conflict_0"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk1_pen._MUX_Engine_Conflict_0()
            allFields["MUX_Engine_Conflict_1"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk1_pen._MUX_Engine_Conflict_1()
            return allFields

    class _stk2_pen(AtRegister.AtRegister):
        def name(self):
            return "Stick 2"
    
        def description(self):
            return "Used to debug"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000402
            
        def endAddress(self):
            return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _eXAUI0_Buffer_Full(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "eXAUI0_Buffer_Full"
            
            def description(self):
                return "eXAUI0 Buffer Full"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Input_eXAUI0_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Input_eXAUI0_Err"
            
            def description(self):
                return "Input eXAUI0 Err"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _eXAUI0_Detect_over_MTU(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "eXAUI0_Detect_over_MTU"
            
            def description(self):
                return "eXAUI0 Detect over MTU"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _eXAUI0_Buffer_Full_State(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "eXAUI0_Buffer_Full_State"
            
            def description(self):
                return "eXAUI0 Buffer State"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FF_Packet_Descriptor_eXAUI0_Write_Error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FF_Packet_Descriptor_eXAUI0_Write_Error"
            
            def description(self):
                return "FF_Packet_Descriptor_eXAUI0_Write_Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FF_Packet_Descriptor_eXAUI0_Read_Error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FF_Packet_Descriptor_eXAUI0_Read_Error"
            
            def description(self):
                return "FF_Packet_Descriptor_eXAUI0_Read_Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Reserved"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk2_pen._Reserved()
            allFields["eXAUI0_Buffer_Full"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk2_pen._eXAUI0_Buffer_Full()
            allFields["Input_eXAUI0_Err"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk2_pen._Input_eXAUI0_Err()
            allFields["eXAUI0_Detect_over_MTU"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk2_pen._eXAUI0_Detect_over_MTU()
            allFields["eXAUI0_Buffer_Full_State"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk2_pen._eXAUI0_Buffer_Full_State()
            allFields["Reserved"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk2_pen._Reserved()
            allFields["Reserved"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk2_pen._Reserved()
            allFields["FF_Packet_Descriptor_eXAUI0_Write_Error"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk2_pen._FF_Packet_Descriptor_eXAUI0_Write_Error()
            allFields["FF_Packet_Descriptor_eXAUI0_Read_Error"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._stk2_pen._FF_Packet_Descriptor_eXAUI0_Read_Error()
            return allFields

    class _sta0_pen(AtRegister.AtRegister):
        def name(self):
            return "Status 0"
    
        def description(self):
            return "Used to debug"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000410
            
        def endAddress(self):
            return 0xffffffff

        class _Buffer_Write_Counter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Buffer_Write_Counter"
            
            def description(self):
                return "Buffer Write Counter"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Buffer_Write_Counter"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._sta0_pen._Buffer_Write_Counter()
            return allFields

    class _sta1_pen(AtRegister.AtRegister):
        def name(self):
            return "Status 1"
    
        def description(self):
            return "Used to debug"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000411
            
        def endAddress(self):
            return 0xffffffff

        class _Buffer_Length_Counter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Buffer_Length_Counter"
            
            def description(self):
                return "Buffer Length Counter"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Buffer_Length_Counter"] = _AF6CCI0012_RD_eXAUI_XFI_Bridge._sta1_pen._Buffer_Length_Counter()
            return allFields

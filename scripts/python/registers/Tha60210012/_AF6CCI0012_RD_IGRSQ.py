import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0012_RD_IGRSQ(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["upen_igrsver"] = _AF6CCI0012_RD_IGRSQ._upen_igrsver()
        allRegisters["upen_igrsctl_0"] = _AF6CCI0012_RD_IGRSQ._upen_igrsctl_0()
        allRegisters["upen_igrsctl_1"] = _AF6CCI0012_RD_IGRSQ._upen_igrsctl_1()
        allRegisters["upen_igrssta_0"] = _AF6CCI0012_RD_IGRSQ._upen_igrssta_0()
        allRegisters["upen_igrsparam"] = _AF6CCI0012_RD_IGRSQ._upen_igrsparam()
        allRegisters["upen_cfgrslk"] = _AF6CCI0012_RD_IGRSQ._upen_cfgrslk()
        allRegisters["upen_cfgrfsc"] = _AF6CCI0012_RD_IGRSQ._upen_cfgrfsc()
        allRegisters["upen_cfgrsqc"] = _AF6CCI0012_RD_IGRSQ._upen_cfgrsqc()
        allRegisters["upen_cfgpkac"] = _AF6CCI0012_RD_IGRSQ._upen_cfgpkac()
        allRegisters["upen_cfgrsqs"] = _AF6CCI0012_RD_IGRSQ._upen_cfgrsqs()
        allRegisters["upen_cpuc0"] = _AF6CCI0012_RD_IGRSQ._upen_cpuc0()
        allRegisters["upen_cpuc1"] = _AF6CCI0012_RD_IGRSQ._upen_cpuc1()
        allRegisters["upen_cpuc2"] = _AF6CCI0012_RD_IGRSQ._upen_cpuc2()
        allRegisters["upen_cpudw"] = _AF6CCI0012_RD_IGRSQ._upen_cpudw()
        return allRegisters

    class _upen_igrsver(AtRegister.AtRegister):
        def name(self):
            return "IGRS version"
    
        def description(self):
            return "This register is used to get Version of Ingress Re-Sequence Engine"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _IGRSEngId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 20
        
            def name(self):
                return "IGRSEngId"
            
            def description(self):
                return "Engine ID"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _IGRSVerId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "IGRSVerId"
            
            def description(self):
                return "Version ID"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _IGRSEditId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 14
        
            def name(self):
                return "IGRSEditId"
            
            def description(self):
                return "Editor ID"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _IGRSYearId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 9
        
            def name(self):
                return "IGRSYearId"
            
            def description(self):
                return "Year"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _IGRSMonId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 5
        
            def name(self):
                return "IGRSMonId"
            
            def description(self):
                return "Month"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _IGRSDayId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IGRSDayId"
            
            def description(self):
                return "Day"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IGRSEngId"] = _AF6CCI0012_RD_IGRSQ._upen_igrsver._IGRSEngId()
            allFields["IGRSVerId"] = _AF6CCI0012_RD_IGRSQ._upen_igrsver._IGRSVerId()
            allFields["IGRSEditId"] = _AF6CCI0012_RD_IGRSQ._upen_igrsver._IGRSEditId()
            allFields["IGRSYearId"] = _AF6CCI0012_RD_IGRSQ._upen_igrsver._IGRSYearId()
            allFields["IGRSMonId"] = _AF6CCI0012_RD_IGRSQ._upen_igrsver._IGRSMonId()
            allFields["IGRSDayId"] = _AF6CCI0012_RD_IGRSQ._upen_igrsver._IGRSDayId()
            return allFields

    class _upen_igrsctl_0(AtRegister.AtRegister):
        def name(self):
            return "IGRS Control 0"
    
        def description(self):
            return "This register is used to control operation of RS engine"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000001
            
        def endAddress(self):
            return 0xffffffff

        class _RsqM_AlgEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "RsqM_AlgEn"
            
            def description(self):
                return "Set 1 to enable M-Algorithm (RFC 1990)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RsqTimeStep(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "RsqTimeStep"
            
            def description(self):
                return "Timer Step Config"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RsqTimeDiv(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "RsqTimeDiv"
            
            def description(self):
                return "Timer devide, to faster simualtion"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RsqIntEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RsqIntEn"
            
            def description(self):
                return "Set high to enable Interrupt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RsqAutEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "RsqAutEn"
            
            def description(self):
                return "Set high to enable auto recovery engine"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RsqEnqEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "RsqEnqEn"
            
            def description(self):
                return "Set high to enable receive information from data enque"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RsqActEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RsqActEn"
            
            def description(self):
                return "Active"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RsqM_AlgEn"] = _AF6CCI0012_RD_IGRSQ._upen_igrsctl_0._RsqM_AlgEn()
            allFields["RsqTimeStep"] = _AF6CCI0012_RD_IGRSQ._upen_igrsctl_0._RsqTimeStep()
            allFields["RsqTimeDiv"] = _AF6CCI0012_RD_IGRSQ._upen_igrsctl_0._RsqTimeDiv()
            allFields["RsqIntEn"] = _AF6CCI0012_RD_IGRSQ._upen_igrsctl_0._RsqIntEn()
            allFields["RsqAutEn"] = _AF6CCI0012_RD_IGRSQ._upen_igrsctl_0._RsqAutEn()
            allFields["RsqEnqEn"] = _AF6CCI0012_RD_IGRSQ._upen_igrsctl_0._RsqEnqEn()
            allFields["RsqActEn"] = _AF6CCI0012_RD_IGRSQ._upen_igrsctl_0._RsqActEn()
            return allFields

    class _upen_igrsctl_1(AtRegister.AtRegister):
        def name(self):
            return "IGRS Control 1"
    
        def description(self):
            return "This register is used to control operation of RS engine"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            return allFields

    class _upen_igrssta_0(AtRegister.AtRegister):
        def name(self):
            return "IGRS Status 0"
    
        def description(self):
            return "This register is used to control operation of RS engine"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _RsqInitdone(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RsqInitdone"
            
            def description(self):
                return "HW init done"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RsqInitdone"] = _AF6CCI0012_RD_IGRSQ._upen_igrssta_0._RsqInitdone()
            return allFields

    class _upen_igrsparam(AtRegister.AtRegister):
        def name(self):
            return "IGRS param"
    
        def description(self):
            return "This register is used to get Param of Ingress Re-Sequence Engine"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000004
            
        def endAddress(self):
            return 0xffffffff

        class _FBlk_bit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 28
        
            def name(self):
                return "FBlk_bit"
            
            def description(self):
                return "Fragment Block bit"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _QBlk_bit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 24
        
            def name(self):
                return "QBlk_bit"
            
            def description(self):
                return "Oque Block  bit"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _BBlk_bit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 20
        
            def name(self):
                return "BBlk_bit"
            
            def description(self):
                return "Bitmap Block  bit"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Bund_bit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Bund_bit"
            
            def description(self):
                return "Bund bit"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Link_bit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Link_bit"
            
            def description(self):
                return "Link  bit"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Space_bit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Space_bit"
            
            def description(self):
                return "Sequence space bit"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _RFlow_bit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RFlow_bit"
            
            def description(self):
                return "Re-Order Engine bit"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _EFlow_bit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "EFlow_bit"
            
            def description(self):
                return "Eth_Flow bit"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FBlk_bit"] = _AF6CCI0012_RD_IGRSQ._upen_igrsparam._FBlk_bit()
            allFields["QBlk_bit"] = _AF6CCI0012_RD_IGRSQ._upen_igrsparam._QBlk_bit()
            allFields["BBlk_bit"] = _AF6CCI0012_RD_IGRSQ._upen_igrsparam._BBlk_bit()
            allFields["Bund_bit"] = _AF6CCI0012_RD_IGRSQ._upen_igrsparam._Bund_bit()
            allFields["Link_bit"] = _AF6CCI0012_RD_IGRSQ._upen_igrsparam._Link_bit()
            allFields["Space_bit"] = _AF6CCI0012_RD_IGRSQ._upen_igrsparam._Space_bit()
            allFields["RFlow_bit"] = _AF6CCI0012_RD_IGRSQ._upen_igrsparam._RFlow_bit()
            allFields["EFlow_bit"] = _AF6CCI0012_RD_IGRSQ._upen_igrsparam._EFlow_bit()
            return allFields

    class _upen_cfgrslk(AtRegister.AtRegister):
        def name(self):
            return "IGRS Eth_flow Control"
    
        def description(self):
            return "This register is used to lookup rflow_id (re-order Id)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00_8000 + $eflow_id"
            
        def startAddress(self):
            return 0x00008000
            
        def endAddress(self):
            return 0xffffffff

        class _IGRSRsId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 8
        
            def name(self):
                return "IGRSRsId"
            
            def description(self):
                return "Re-Order ID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IGRSRsEna(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "IGRSRsEna"
            
            def description(self):
                return "Re-Order Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IGRSoQueEna(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "IGRSoQueEna"
            
            def description(self):
                return "output queue Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IGRSoQueId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IGRSoQueId"
            
            def description(self):
                return "output queue Id"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IGRSRsId"] = _AF6CCI0012_RD_IGRSQ._upen_cfgrslk._IGRSRsId()
            allFields["IGRSRsEna"] = _AF6CCI0012_RD_IGRSQ._upen_cfgrslk._IGRSRsEna()
            allFields["IGRSoQueEna"] = _AF6CCI0012_RD_IGRSQ._upen_cfgrslk._IGRSoQueEna()
            allFields["IGRSoQueId"] = _AF6CCI0012_RD_IGRSQ._upen_cfgrslk._IGRSoQueId()
            return allFields

    class _upen_cfgrfsc(AtRegister.AtRegister):
        def name(self):
            return "IGRS Re-Order Buffer Space"
    
        def description(self):
            return "This register is used to config param of Re-Order Engine"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00_1000 + $rflow_id"
            
        def startAddress(self):
            return 0x00001000
            
        def endAddress(self):
            return 0xffffffff

        class _RsqRfsId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RsqRfsId"
            
            def description(self):
                return "Re-Order Space (64K) ID, (0-127), SW must cfg it when RsqRfsMd = 2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RsqRfsMd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RsqRfsMd"
            
            def description(self):
                return "Re-Order Sequence Buffer Space, it must be > 1.25*BT with BT is Buffer-Occupancy Threshold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RsqRfsId"] = _AF6CCI0012_RD_IGRSQ._upen_cfgrfsc._RsqRfsId()
            allFields["RsqRfsMd"] = _AF6CCI0012_RD_IGRSQ._upen_cfgrfsc._RsqRfsMd()
            return allFields

    class _upen_cfgrsqc(AtRegister.AtRegister):
        def name(self):
            return "IGRS Re-Order Control"
    
        def description(self):
            return "This register is used to config param of Re-Order Engine"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00_2000 + $rflow_id"
            
        def startAddress(self):
            return 0x00002000
            
        def endAddress(self):
            return 0xffffffff

        class _RsqNumLink(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 24
        
            def name(self):
                return "RsqNumLink"
            
            def description(self):
                return "Number of link on bundle 0x0: one link on bundle 0x1: two links on bundle, ..."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RsqSeqThsh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 12
        
            def name(self):
                return "RsqSeqThsh"
            
            def description(self):
                return "Threshold Sequence that declaration lost sequence Buffer-Occupancy Threshold (BT) on RFC 5236, with step is RsqStepThsh, true threshold is RsqSeqThsh * RsqResThsh"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RsqResThsh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "RsqResThsh"
            
            def description(self):
                return "Resolution of threshold 0x0: Resolution is 1 0x1: Resolution is 4 0x2: Resolution is 16 0x3: Resolution is 32"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RsqEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "RsqEnable"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RsqTimeout(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RsqTimeout"
            
            def description(self):
                return "Timeout value, unit depend on RsqTimeStep & RsqTimeDiv"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RsqNumLink"] = _AF6CCI0012_RD_IGRSQ._upen_cfgrsqc._RsqNumLink()
            allFields["RsqSeqThsh"] = _AF6CCI0012_RD_IGRSQ._upen_cfgrsqc._RsqSeqThsh()
            allFields["RsqResThsh"] = _AF6CCI0012_RD_IGRSQ._upen_cfgrsqc._RsqResThsh()
            allFields["RsqEnable"] = _AF6CCI0012_RD_IGRSQ._upen_cfgrsqc._RsqEnable()
            allFields["RsqTimeout"] = _AF6CCI0012_RD_IGRSQ._upen_cfgrsqc._RsqTimeout()
            return allFields

    class _upen_cfgpkac(AtRegister.AtRegister):
        def name(self):
            return "IGRS Pkt-Assemble Control"
    
        def description(self):
            return "This register is used to config param of Re-Order Engine"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00_4000 + $rflow_id"
            
        def startAddress(self):
            return 0x00004000
            
        def endAddress(self):
            return 0xffffffff

        class _PkaMsru(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PkaMsru"
            
            def description(self):
                return "Multilink Maximum Sequence Reconstructed Unit, is maximum fragments per packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PkaMrru(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PkaMrru"
            
            def description(self):
                return "Multilink Maximum Received Reconstructed Unit (RFC 1990)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PkaMsru"] = _AF6CCI0012_RD_IGRSQ._upen_cfgpkac._PkaMsru()
            allFields["PkaMrru"] = _AF6CCI0012_RD_IGRSQ._upen_cfgpkac._PkaMrru()
            return allFields

    class _upen_cfgrsqs(AtRegister.AtRegister):
        def name(self):
            return "IGRS Re-Order FSM status"
    
        def description(self):
            return "This register is used to config param of Re-Order Engine"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00_3000 + $rflow_id"
            
        def startAddress(self):
            return 0x00003000
            
        def endAddress(self):
            return 0xffffffff

        class _RsqFlag(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 28
        
            def name(self):
                return "RsqFlag"
            
            def description(self):
                return "Flag status bit_0: long sequence bit_1: Next update Expected bit_2: Do not re-sequence, is flush state bit_3: unused"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _RsqExpt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RsqExpt"
            
            def description(self):
                return "Expected sequence"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _RsqFsm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RsqFsm"
            
            def description(self):
                return "FSM of engine 0x0 - FSM_INITIAL 0x1 - FSM_RESTART 0x2 - FSM_INIRST 0x3 - FSM_RESET 0x4 - FSM_RESYN_1 0x5 - FSM_RESYN_5 0x8 - FSM_SYNC_1 0x9 - FSM_SYNC_2 0xC - FSM_DNRSQ 0xD - FSM_PINIRST 0xE - FSM_PRESET 0xF - FSM_FASTRST"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RsqFlag"] = _AF6CCI0012_RD_IGRSQ._upen_cfgrsqs._RsqFlag()
            allFields["RsqExpt"] = _AF6CCI0012_RD_IGRSQ._upen_cfgrsqs._RsqExpt()
            allFields["RsqFsm"] = _AF6CCI0012_RD_IGRSQ._upen_cfgrsqs._RsqFsm()
            return allFields

    class _upen_cpuc0(AtRegister.AtRegister):
        def name(self):
            return "IGRS Re-Order CPU Access Control"
    
        def description(self):
            return "This register is used to cpu control access memory"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000100
            
        def endAddress(self):
            return 0xffffffff

        class _CPUEcc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "CPUEcc"
            
            def description(self):
                return "ECC enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CPUSize(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "CPUSize"
            
            def description(self):
                return "Burst size access"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CPUCch(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "CPUCch"
            
            def description(self):
                return "Cache Id"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CPUMem(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 4
        
            def name(self):
                return "CPUMem"
            
            def description(self):
                return "Memory access 0x0 - RSQ memory (Resequence status 1Kx256~256Kbits) 0x1 - AFF memory (Arrive Fifo block 4Kx128 ~ 512Kbits) 0x4 - RBM memory (Resequence Bitmap status) 0x5 - RFI memory (Resequence Fragment information)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CPUFsm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CPUFsm"
            
            def description(self):
                return "FSM of cpu engine, 0x0 - FSM_IDLE, HW debug only 0x1 - FSM_READY, indicate Engine can accept one command 0x2 - FSM_INIT, HW debug only 0x3 - FSM_READ, SW set to request read 0x4 - FSM_WRITE, SW set to request write 0x5 - FSM_WAIT_W 0x6 - FSM_WAIT_R 0x7 - FSM_DONE, indicate request is done"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CPUEcc"] = _AF6CCI0012_RD_IGRSQ._upen_cpuc0._CPUEcc()
            allFields["CPUSize"] = _AF6CCI0012_RD_IGRSQ._upen_cpuc0._CPUSize()
            allFields["CPUCch"] = _AF6CCI0012_RD_IGRSQ._upen_cpuc0._CPUCch()
            allFields["CPUMem"] = _AF6CCI0012_RD_IGRSQ._upen_cpuc0._CPUMem()
            allFields["CPUFsm"] = _AF6CCI0012_RD_IGRSQ._upen_cpuc0._CPUFsm()
            return allFields

    class _upen_cpuc1(AtRegister.AtRegister):
        def name(self):
            return "IGRS Re-Order CPU Access Address"
    
        def description(self):
            return "This register is used to cpu control access memory"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000101
            
        def endAddress(self):
            return 0xffffffff

        class _CPUAdd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CPUAdd"
            
            def description(self):
                return "Address access"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CPUAdd"] = _AF6CCI0012_RD_IGRSQ._upen_cpuc1._CPUAdd()
            return allFields

    class _upen_cpuc2(AtRegister.AtRegister):
        def name(self):
            return "IGRS Re-Order CPU Access Burst"
    
        def description(self):
            return "This register is used to cpu control access memory"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000102
            
        def endAddress(self):
            return 0xffffffff

        class _CPUBurst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CPUBurst"
            
            def description(self):
                return "Burst access"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CPUBurst"] = _AF6CCI0012_RD_IGRSQ._upen_cpuc2._CPUBurst()
            return allFields

    class _upen_cpudw(AtRegister.AtRegister):
        def name(self):
            return "IGRS Re-Order CPU Access Data"
    
        def description(self):
            return "This register is used to cpu data access memory"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00_0180 + $dword_id"
            
        def startAddress(self):
            return 0x00000180
            
        def endAddress(self):
            return 0xffffffff

        class _CPUData(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CPUData"
            
            def description(self):
                return "Data"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CPUData"] = _AF6CCI0012_RD_IGRSQ._upen_cpudw._CPUData()
            return allFields

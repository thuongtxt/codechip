import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0011_RD_TOP(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["top_o_control0"] = _AF6CCI0011_RD_TOP._top_o_control0()
        allRegisters["top_o_control1"] = _AF6CCI0011_RD_TOP._top_o_control1()
        allRegisters["top_i_statuse"] = _AF6CCI0011_RD_TOP._top_i_statuse()
        return allRegisters

    class _top_o_control0(AtRegister.AtRegister):
        def name(self):
            return "Top ETH-Port Control 0"
    
        def description(self):
            return "This register indicates the active ports."
            
        def width(self):
            return 10
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000040
            
        def endAddress(self):
            return 0xffffffff

        class _ECSC_MAC3_RxSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 20
        
            def name(self):
                return "ECSC_MAC3_RxSel"
            
            def description(self):
                return "Ethernet CISCO MAC3 Select Serdes ID 0x0: CISCO_XFI_Serdes#08 0x1: CISCO_XFI_Serdes#09 0x2: CISCO_XFI_Serdes#10 0x3: CISCO_XFI_Serdes#11 0x4: CISCO_XFI_Serdes#12 0x5: CISCO_XFI_Serdes#13 0x6: CISCO_XFI_Serdes#14 0x7: CISCO_XFI_Serdes#15"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ECSC_MAC2_RxSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ECSC_MAC2_RxSel"
            
            def description(self):
                return "Ethernet CISCO MAC2 Select port ID 0x0: CISCO_XFI_Serdes#08 0x1: CISCO_XFI_Serdes#09 0x2: CISCO_XFI_Serdes#10 0x3: CISCO_XFI_Serdes#11 0x4: CISCO_XFI_Serdes#12 0x5: CISCO_XFI_Serdes#13 0x6: CISCO_XFI_Serdes#14 0x7: CISCO_XFI_Serdes#15"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _EARR_MAC1_RxSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 4
        
            def name(self):
                return "EARR_MAC1_RxSel"
            
            def description(self):
                return "Ethernet ARRIVE MAC1 Select XFI serdes ID 0x0: ARRIVE_XFI_Serdes#00 0x1: ARRIVE_XFI_Serdes#01 0x2: ARRIVE_XFI_Serdes#02 0x3: ARRIVE_XFI_Serdes#03 0x4: ARRIVE_XFI_Serdes#04 0x5: ARRIVE_XFI_Serdes#05 0x6: ARRIVE_XFI_Serdes#06 0x7: ARRIVE_XFI_Serdes#07"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _EARR_MAC0_RxSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "EARR_MAC0_RxSel"
            
            def description(self):
                return "Ethernet ARRIVE MAC0 Select XFI serdes ID 0x0: ARRIVE_XFI_Serdes#00 0x1: ARRIVE_XFI_Serdes#01 0x2: ARRIVE_XFI_Serdes#02 0x3: ARRIVE_XFI_Serdes#03 0x4: ARRIVE_XFI_Serdes#04 0x5: ARRIVE_XFI_Serdes#05 0x6: ARRIVE_XFI_Serdes#06 0x7: ARRIVE_XFI_Serdes#07"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ECSC_MAC3_RxSel"] = _AF6CCI0011_RD_TOP._top_o_control0._ECSC_MAC3_RxSel()
            allFields["ECSC_MAC2_RxSel"] = _AF6CCI0011_RD_TOP._top_o_control0._ECSC_MAC2_RxSel()
            allFields["EARR_MAC1_RxSel"] = _AF6CCI0011_RD_TOP._top_o_control0._EARR_MAC1_RxSel()
            allFields["EARR_MAC0_RxSel"] = _AF6CCI0011_RD_TOP._top_o_control0._EARR_MAC0_RxSel()
            return allFields

    class _top_o_control1(AtRegister.AtRegister):
        def name(self):
            return "Top ETH-Port Control 1"
    
        def description(self):
            return "This register indicates the active ports."
            
        def width(self):
            return 10
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000041
            
        def endAddress(self):
            return 0xffffffff

        class _ECSC_XFI_TxEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "ECSC_XFI_TxEn"
            
            def description(self):
                return "Ethernet CISCO_XFI_Serdes Tx Enable bit_00 correspond for CISCO_XFI_Serdes#08 0x0: Disable 0x1: Enable bit_01 correspond for CISCO_XFI_Serdes#09 bit_02 correspond for CISCO_XFI_Serdes#10 bit_03 correspond for CISCO_XFI_Serdes#11 bit_04 correspond for CISCO_XFI_Serdes#12 bit_05 correspond for CISCO_XFI_Serdes#13 bit_06 correspond for CISCO_XFI_Serdes#14 bit_07 correspond for CISCO_XFI_Serdes#15"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ECSC_XFI_TxSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ECSC_XFI_TxSel"
            
            def description(self):
                return "Ethernet CISCO_XFI_Serdes Select Data from bit_00 correspond for CISCO_XFI_Serdes#08 0x0: ECSC_MAC2_TX 0x1: ECSC_MAC3_TX bit_01 correspond for CISCO_XFI_Serdes#09 bit_02 correspond for CISCO_XFI_Serdes#10 bit_03 correspond for CISCO_XFI_Serdes#11 bit_04 correspond for CISCO_XFI_Serdes#12 bit_05 correspond for CISCO_XFI_Serdes#13 bit_06 correspond for CISCO_XFI_Serdes#14 bit_07 correspond for CISCO_XFI_Serdes#15"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _EARR_XFI_TxEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "EARR_XFI_TxEn"
            
            def description(self):
                return "Ethernet ARRIVE_XFI_Serdes Tx Enable bit_00 correspond for ARRIVE_XFI_Serdes#00 0x0: Disable 0x1: Enable bit_01 correspond for ARRIVE_XFI_Serdes#01 bit_02 correspond for ARRIVE_XFI_Serdes#02 bit_03 correspond for ARRIVE_XFI_Serdes#03 bit_04 correspond for ARRIVE_XFI_Serdes#04 bit_05 correspond for ARRIVE_XFI_Serdes#05 bit_06 correspond for ARRIVE_XFI_Serdes#06 bit_07 correspond for ARRIVE_XFI_Serdes#07"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _EARR_XFI_TxSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "EARR_XFI_TxSel"
            
            def description(self):
                return "Ethernet ARRIVE_XFI_Serdes Select Data from bit_00 correspond for ARRIVE_XFI_Serdes#00 0x0: EARR_MAC0_TX 0x1: EARR_MAC1_TX bit_01 correspond for ARRIVE_XFI_Serdes#01 bit_02 correspond for ARRIVE_XFI_Serdes#02 bit_03 correspond for ARRIVE_XFI_Serdes#03 bit_04 correspond for ARRIVE_XFI_Serdes#04 bit_05 correspond for ARRIVE_XFI_Serdes#05 bit_06 correspond for ARRIVE_XFI_Serdes#06 bit_07 correspond for ARRIVE_XFI_Serdes#07"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ECSC_XFI_TxEn"] = _AF6CCI0011_RD_TOP._top_o_control1._ECSC_XFI_TxEn()
            allFields["ECSC_XFI_TxSel"] = _AF6CCI0011_RD_TOP._top_o_control1._ECSC_XFI_TxSel()
            allFields["EARR_XFI_TxEn"] = _AF6CCI0011_RD_TOP._top_o_control1._EARR_XFI_TxEn()
            allFields["EARR_XFI_TxSel"] = _AF6CCI0011_RD_TOP._top_o_control1._EARR_XFI_TxSel()
            return allFields

    class _top_i_statuse(AtRegister.AtRegister):
        def name(self):
            return "Top ETH-Port Status e"
    
        def description(self):
            return "This register used to show status of Ethernet Cross-Point"
            
        def width(self):
            return 10
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000006e
            
        def endAddress(self):
            return 0xffffffff

        class _ECSC_XFI_TxEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "ECSC_XFI_TxEn"
            
            def description(self):
                return "Status of Ethernet CISCO_XFI_Serdes Tx Enable bit_00 correspond for CISCO_XFI_Serdes#08 0x0: Disable 0x1: Enable bit_01 correspond for CISCO_XFI_Serdes#09 bit_02 correspond for CISCO_XFI_Serdes#10 bit_03 correspond for CISCO_XFI_Serdes#11 bit_04 correspond for CISCO_XFI_Serdes#12 bit_05 correspond for CISCO_XFI_Serdes#13 bit_06 correspond for CISCO_XFI_Serdes#14 bit_07 correspond for CISCO_XFI_Serdes#15"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ECSC_MAC3_RxSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 20
        
            def name(self):
                return "ECSC_MAC3_RxSel"
            
            def description(self):
                return "Status Ethernet CISCO MAC1 Select XFI serdes ID 0x0: CISCO_XFI_Serdes#08 0x1: CISCO_XFI_Serdes#09 0x2: CISCO_XFI_Serdes#10 0x3: CISCO_XFI_Serdes#11 0x4: CISCO_XFI_Serdes#12 0x5: CISCO_XFI_Serdes#13 0x6: CISCO_XFI_Serdes#14 0x7: CISCO_XFI_Serdes#15"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ECSC_MAC2_RxSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ECSC_MAC2_RxSel"
            
            def description(self):
                return "Status Ethernet CISCO MAC0 Select XFI serdes ID 0x0: CISCO_XFI_Serdes#08 0x1: CISCO_XFI_Serdes#09 0x2: CISCO_XFI_Serdes#10 0x3: CISCO_XFI_Serdes#11 0x4: CISCO_XFI_Serdes#12 0x5: CISCO_XFI_Serdes#13 0x6: CISCO_XFI_Serdes#14 0x7: CISCO_XFI_Serdes#15"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _EARR_XFI_TxEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "EARR_XFI_TxEn"
            
            def description(self):
                return "Status of Ethernet ARRIVE_XFI_Serdes Tx Enable bit_00 correspond for ARRIVE_XFI_Serdes#00 0x0: Disable 0x1: Enable bit_01 correspond for ARRIVE_XFI_Serdes#01 bit_02 correspond for ARRIVE_XFI_Serdes#02 bit_03 correspond for ARRIVE_XFI_Serdes#03 bit_04 correspond for ARRIVE_XFI_Serdes#04 bit_05 correspond for ARRIVE_XFI_Serdes#05 bit_06 correspond for ARRIVE_XFI_Serdes#06 bit_07 correspond for ARRIVE_XFI_Serdes#07"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _EARR_MAC1_RxSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 4
        
            def name(self):
                return "EARR_MAC1_RxSel"
            
            def description(self):
                return "Status Ethernet ARRIVE MAC1 Select XFI serdes ID 0x0: ARRIVE_XFI_Serdes#00 0x1: ARRIVE_XFI_Serdes#01 0x2: ARRIVE_XFI_Serdes#02 0x3: ARRIVE_XFI_Serdes#03 0x4: ARRIVE_XFI_Serdes#04 0x5: ARRIVE_XFI_Serdes#05 0x6: ARRIVE_XFI_Serdes#06 0x7: ARRIVE_XFI_Serdes#07"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _EARR_MAC0_RxSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "EARR_MAC0_RxSel"
            
            def description(self):
                return "Status Ethernet ARRIVE MAC0 Select XFI serdes ID 0x0: ARRIVE_XFI_Serdes#00 0x1: ARRIVE_XFI_Serdes#01 0x2: ARRIVE_XFI_Serdes#02 0x3: ARRIVE_XFI_Serdes#03 0x4: ARRIVE_XFI_Serdes#04 0x5: ARRIVE_XFI_Serdes#05 0x6: ARRIVE_XFI_Serdes#06 0x7: ARRIVE_XFI_Serdes#07"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ECSC_XFI_TxEn"] = _AF6CCI0011_RD_TOP._top_i_statuse._ECSC_XFI_TxEn()
            allFields["ECSC_MAC3_RxSel"] = _AF6CCI0011_RD_TOP._top_i_statuse._ECSC_MAC3_RxSel()
            allFields["ECSC_MAC2_RxSel"] = _AF6CCI0011_RD_TOP._top_i_statuse._ECSC_MAC2_RxSel()
            allFields["EARR_XFI_TxEn"] = _AF6CCI0011_RD_TOP._top_i_statuse._EARR_XFI_TxEn()
            allFields["EARR_MAC1_RxSel"] = _AF6CCI0011_RD_TOP._top_i_statuse._EARR_MAC1_RxSel()
            allFields["EARR_MAC0_RxSel"] = _AF6CCI0011_RD_TOP._top_i_statuse._EARR_MAC0_RxSel()
            return allFields

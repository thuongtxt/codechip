import python.arrive.atsdk.AtRegister as AtRegister

class RegisterProviderFactory(AtRegister.AtRegisterProviderFactory):
    def _allRegisterProviders(self):
        allProviders = {}

        from _AF6CNC0011_SEM import _AF6CNC0011_SEM
        allProviders["_AF6CNC0011_SEM"] = _AF6CNC0011_SEM()

        from _AF6CNC0021_RD_UPSR import _AF6CNC0021_RD_UPSR
        allProviders["_AF6CNC0021_RD_UPSR"] = _AF6CNC0021_RD_UPSR()

        from _AF6CNC0011_RD_PM import _AF6CNC0011_RD_PM
        allProviders["_AF6CNC0011_RD_PM"] = _AF6CNC0011_RD_PM()

        from _AF6CCI0011_RD_MAP import _AF6CCI0011_RD_MAP
        allProviders["_AF6CCI0011_RD_MAP"] = _AF6CCI0011_RD_MAP()

        from _AF6CNC0021_RD_MACDUMP import _AF6CNC0021_RD_MACDUMP
        allProviders["_AF6CNC0021_RD_MACDUMP"] = _AF6CNC0021_RD_MACDUMP()

        from _AF6CNC0021_RD_PRBS import _AF6CNC0021_RD_PRBS
        allProviders["_AF6CNC0021_RD_PRBS"] = _AF6CNC0021_RD_PRBS()

        from _AF6CNC0021_RD_PM import _AF6CNC0021_RD_PM
        allProviders["_AF6CNC0021_RD_PM"] = _AF6CNC0021_RD_PM()

        from _AF6CCI0011_RD_INTR import _AF6CCI0011_RD_INTR
        allProviders["_AF6CCI0011_RD_INTR"] = _AF6CCI0011_RD_INTR()

        from _AF6CCI0011_RD_PLA_DEBUG import _AF6CCI0011_RD_PLA_DEBUG
        allProviders["_AF6CCI0011_RD_PLA_DEBUG"] = _AF6CCI0011_RD_PLA_DEBUG()

        from _AF6CCI0011_RD_OCN import _AF6CCI0011_RD_OCN
        allProviders["_AF6CCI0011_RD_OCN"] = _AF6CCI0011_RD_OCN()

        from _AF6CCI0011_RD_OCN_DIAG import _AF6CCI0011_RD_OCN_DIAG
        allProviders["_AF6CCI0011_RD_OCN_DIAG"] = _AF6CCI0011_RD_OCN_DIAG()

        from _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD import _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD
        allProviders["_AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD()

        from _AF6CNC0021_RD_GLB import _AF6CNC0021_RD_GLB
        allProviders["_AF6CNC0021_RD_GLB"] = _AF6CNC0021_RD_GLB()

        from _AF6CNC0021_RD_OCN import _AF6CNC0021_RD_OCN
        allProviders["_AF6CNC0021_RD_OCN"] = _AF6CNC0021_RD_OCN()

        from _OC192_MUX_OC48_16ch_wrap_RD import _OC192_MUX_OC48_16ch_wrap_RD
        allProviders["_OC192_MUX_OC48_16ch_wrap_RD"] = _OC192_MUX_OC48_16ch_wrap_RD()

        from _AF6CNC0021_RD_POH_BER import _AF6CNC0021_RD_POH_BER
        allProviders["_AF6CNC0021_RD_POH_BER"] = _AF6CNC0021_RD_POH_BER()

        from _AF6CCI0011_RD_PDH_MDL import _AF6CCI0011_RD_PDH_MDL
        allProviders["_AF6CCI0011_RD_PDH_MDL"] = _AF6CCI0011_RD_PDH_MDL()

        from _AF6CNC0022_RD_PWE import _AF6CNC0022_RD_PWE
        allProviders["_AF6CNC0022_RD_PWE"] = _AF6CNC0022_RD_PWE()

        from _AF6CCI0011_RD_CLA import _AF6CCI0011_RD_CLA
        allProviders["_AF6CCI0011_RD_CLA"] = _AF6CCI0011_RD_CLA()

        from _AF6CCI0012_RD_PTP import _AF6CCI0012_RD_PTP
        allProviders["_AF6CCI0012_RD_PTP"] = _AF6CCI0012_RD_PTP()

        from _AF6CCI0011_RD_PARITY_ECC_CRC import _AF6CCI0011_RD_PARITY_ECC_CRC
        allProviders["_AF6CCI0011_RD_PARITY_ECC_CRC"] = _AF6CCI0011_RD_PARITY_ECC_CRC()

        from _AF6CNC0021_RD_SGMII_Multirate import _AF6CNC0021_RD_SGMII_Multirate
        allProviders["_AF6CNC0021_RD_SGMII_Multirate"] = _AF6CNC0021_RD_SGMII_Multirate()

        from _AF6CCI0011_RD_PM import _AF6CCI0011_RD_PM
        allProviders["_AF6CCI0011_RD_PM"] = _AF6CCI0011_RD_PM()

        from _AF6CCI0011_RD_PDH_PRM import _AF6CCI0011_RD_PDH_PRM
        allProviders["_AF6CCI0011_RD_PDH_PRM"] = _AF6CCI0011_RD_PDH_PRM()

        from _AF6CCI0011_RD_PDH import _AF6CCI0011_RD_PDH
        allProviders["_AF6CCI0011_RD_PDH"] = _AF6CCI0011_RD_PDH()

        from _AF6CCI0011_RD_BERT_MON_PW import _AF6CCI0011_RD_BERT_MON_PW
        allProviders["_AF6CCI0011_RD_BERT_MON_PW"] = _AF6CCI0011_RD_BERT_MON_PW()

        from _AF6CNC0022_RD_OCN import _AF6CNC0022_RD_OCN
        allProviders["_AF6CNC0022_RD_OCN"] = _AF6CNC0022_RD_OCN()

        from _AF6CCI0011_RD_INTALM import _AF6CCI0011_RD_INTALM
        allProviders["_AF6CCI0011_RD_INTALM"] = _AF6CCI0011_RD_INTALM()

        from _AF6CNC0021_RD_GLBPMC import _AF6CNC0021_RD_GLBPMC
        allProviders["_AF6CNC0021_RD_GLBPMC"] = _AF6CNC0021_RD_GLBPMC()

        from _AF6CCI0011_RD_BERT_MON import _AF6CCI0011_RD_BERT_MON
        allProviders["_AF6CCI0011_RD_BERT_MON"] = _AF6CCI0011_RD_BERT_MON()

        from _AF6CCI0011_RD_PLA import _AF6CCI0011_RD_PLA
        allProviders["_AF6CCI0011_RD_PLA"] = _AF6CCI0011_RD_PLA()

        from _AF6CCI0011_RD_POH_BER import _AF6CCI0011_RD_POH_BER
        allProviders["_AF6CCI0011_RD_POH_BER"] = _AF6CCI0011_RD_POH_BER()

        from _AF6CCI0011_RD_BERT_GEN import _AF6CCI0011_RD_BERT_GEN
        allProviders["_AF6CCI0011_RD_BERT_GEN"] = _AF6CCI0011_RD_BERT_GEN()

        from _AF6CCI0011_RD_CDR import _AF6CCI0011_RD_CDR
        allProviders["_AF6CCI0011_RD_CDR"] = _AF6CCI0011_RD_CDR()

        from _AF6CNC0021_RD_BERT_MON import _AF6CNC0021_RD_BERT_MON
        allProviders["_AF6CNC0021_RD_BERT_MON"] = _AF6CNC0021_RD_BERT_MON()

        from _AF6CNC0021_RD_BERT_MON_PW import _AF6CNC0021_RD_BERT_MON_PW
        allProviders["_AF6CNC0021_RD_BERT_MON_PW"] = _AF6CNC0021_RD_BERT_MON_PW()

        from _AF6CNC0011_RD_ETH import _AF6CNC0011_RD_ETH
        allProviders["_AF6CNC0011_RD_ETH"] = _AF6CNC0011_RD_ETH()

        from _AF6CNC0021_RD_SEM import _AF6CNC0021_RD_SEM
        allProviders["_AF6CNC0021_RD_SEM"] = _AF6CNC0021_RD_SEM()

        from _AF6CCI0011_RD_MAP_HO import _AF6CCI0011_RD_MAP_HO
        allProviders["_AF6CCI0011_RD_MAP_HO"] = _AF6CCI0011_RD_MAP_HO()

        from _AF6CNC0021_RD_ETHPASS import _AF6CNC0021_RD_ETHPASS
        allProviders["_AF6CNC0021_RD_ETHPASS"] = _AF6CNC0021_RD_ETHPASS()

        from _AF6CNC0022_RD_DCCK import _AF6CNC0022_RD_DCCK
        allProviders["_AF6CNC0022_RD_DCCK"] = _AF6CNC0022_RD_DCCK()

        from _AF6CCI0011_RD_PWE import _AF6CCI0011_RD_PWE
        allProviders["_AF6CCI0011_RD_PWE"] = _AF6CCI0011_RD_PWE()

        from _AF6CCI0051_RD_POH_BER import _AF6CCI0051_RD_POH_BER
        allProviders["_AF6CCI0051_RD_POH_BER"] = _AF6CCI0051_RD_POH_BER()

        from _AF6CCI0011_RD_TOP import _AF6CCI0011_RD_TOP
        allProviders["_AF6CCI0011_RD_TOP"] = _AF6CCI0011_RD_TOP()

        from _AF6CCI0011_RD_ETH10G import _AF6CCI0011_RD_ETH10G
        allProviders["_AF6CCI0011_RD_ETH10G"] = _AF6CCI0011_RD_ETH10G()

        from _AF6CNC0011_ETH40G_RD import _AF6CNC0011_ETH40G_RD
        allProviders["_AF6CNC0011_ETH40G_RD"] = _AF6CNC0011_ETH40G_RD()

        from _AF6CCI0011_RD_PDA import _AF6CCI0011_RD_PDA
        allProviders["_AF6CCI0011_RD_PDA"] = _AF6CCI0011_RD_PDA()

        from _AF6CCI0011_RD_GLB import _AF6CCI0011_RD_GLB
        allProviders["_AF6CCI0011_RD_GLB"] = _AF6CCI0011_RD_GLB()

        from _AF6CCI0011_RD_CDR_HO import _AF6CCI0011_RD_CDR_HO
        allProviders["_AF6CCI0011_RD_CDR_HO"] = _AF6CCI0011_RD_CDR_HO()

        from _AF6CCI0011_RD_PDH_LPC import _AF6CCI0011_RD_PDH_LPC
        allProviders["_AF6CCI0011_RD_PDH_LPC"] = _AF6CCI0011_RD_PDH_LPC()


        return allProviders

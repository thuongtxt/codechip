import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0011_RD_MAP(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["demap_channel_ctrl"] = _AF6CCI0011_RD_MAP._demap_channel_ctrl()
        allRegisters["map_channel_ctrl"] = _AF6CCI0011_RD_MAP._map_channel_ctrl()
        allRegisters["map_line_ctrl"] = _AF6CCI0011_RD_MAP._map_line_ctrl()
        allRegisters["map_idle_code"] = _AF6CCI0011_RD_MAP._map_idle_code()
        allRegisters["RAM_Map_Parity_Force_Control"] = _AF6CCI0011_RD_MAP._RAM_Map_Parity_Force_Control()
        allRegisters["RAM_Map_Parity_Disable_Control"] = _AF6CCI0011_RD_MAP._RAM_Map_Parity_Disable_Control()
        allRegisters["RAM_Map_Parity_Error_Sticky"] = _AF6CCI0011_RD_MAP._RAM_Map_Parity_Error_Sticky()
        allRegisters["RAM_DeMap_Parity_Force_Control"] = _AF6CCI0011_RD_MAP._RAM_DeMap_Parity_Force_Control()
        allRegisters["RAM_DeMap_Parity_Disable_Control"] = _AF6CCI0011_RD_MAP._RAM_DeMap_Parity_Disable_Control()
        allRegisters["RAM_DeMap_Parity_Error_Sticky"] = _AF6CCI0011_RD_MAP._RAM_DeMap_Parity_Error_Sticky()
        return allRegisters

    class _demap_channel_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Demap Channel Control"
    
        def description(self):
            return "The registers are used by the hardware to convert channel identification number to STS/VT/PDH identification number. All STS/VT/PDHs of a concatenation are assigned the same channel identification number."
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000000 + 672*stsid + 96*vtgid + 24*vtid + slotid"
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0x00003fff

        class _DemapFirstTs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "DemapFirstTs"
            
            def description(self):
                return "First timeslot indication. Use for CESoP mode only"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DemapChannelType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 11
        
            def name(self):
                return "DemapChannelType"
            
            def description(self):
                return "Specify which type of mapping for this. Specify which type of mapping for this. 0: SAToP encapsulation from DS1/E1, DS3/E3 1: CESoP encapsulation from DS1/E1 2: ATM over DS1/ E1, SONET/SDH (unused) 3: IMA over DS1/E1, SONET/SDH(unused) 4: HDLC/PPP/LAPS over DS1/E1, SONET/SDH(unused) 5: MLPPP over DS1/E1, SONET/SDH(unused) 6: CEP encapsulation from SONET/SDH 7: PLCP encapsulation from DS3/E3(unused)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DemapChEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "DemapChEn"
            
            def description(self):
                return "PW/Channels Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DemapPWIdField(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DemapPWIdField"
            
            def description(self):
                return "PW ID field that uses for Encapsulation"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DemapFirstTs"] = _AF6CCI0011_RD_MAP._demap_channel_ctrl._DemapFirstTs()
            allFields["DemapChannelType"] = _AF6CCI0011_RD_MAP._demap_channel_ctrl._DemapChannelType()
            allFields["DemapChEn"] = _AF6CCI0011_RD_MAP._demap_channel_ctrl._DemapChEn()
            allFields["DemapPWIdField"] = _AF6CCI0011_RD_MAP._demap_channel_ctrl._DemapPWIdField()
            return allFields

    class _map_channel_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Map Channel Control"
    
        def description(self):
            return "The registers are used by the hardware to convert channel identification number to STS/VT/PDH identification number. All STS/VT/PDHs of a concatenation are assigned the same channel identification number."
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x014000 + 672*stsid + 96*vtgid + 24*vtid + slotid"
            
        def startAddress(self):
            return 0x00014000
            
        def endAddress(self):
            return 0x00017fff

        class _MapFirstTs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "MapFirstTs"
            
            def description(self):
                return "First timeslot indication. Use for CESoP mode only"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MapChannelType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 11
        
            def name(self):
                return "MapChannelType"
            
            def description(self):
                return "Specify which type of mapping for this. Specify which type of mapping for this. 0: SAToP encapsulation from DS1/E1, DS3/E3 1: CESoP encapsulation from DS1/E1 2: CESoP with CAS 3: ATM(unused) 4: HDLC/PPP/LAPS over DS1/E1, SONET/SDH(unused) 5: MLPPP over DS1/E1, SONET/SDH(unused) 6: CEP encapsulation from SONET/SDH 7: PLCP encapsulation from DS3/E3(unused)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MapChEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "MapChEn"
            
            def description(self):
                return "PW/Channels Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MapPWIdField(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MapPWIdField"
            
            def description(self):
                return "PW ID field that uses for Encapsulation."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MapFirstTs"] = _AF6CCI0011_RD_MAP._map_channel_ctrl._MapFirstTs()
            allFields["MapChannelType"] = _AF6CCI0011_RD_MAP._map_channel_ctrl._MapChannelType()
            allFields["MapChEn"] = _AF6CCI0011_RD_MAP._map_channel_ctrl._MapChEn()
            allFields["MapPWIdField"] = _AF6CCI0011_RD_MAP._map_channel_ctrl._MapPWIdField()
            return allFields

    class _map_line_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Map Line Control"
    
        def description(self):
            return "The registers provide the per line configurations for STS/VT/DS1/E1 line."
            
        def width(self):
            return 20
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x010000 + 32*stsid + 4*vtgid + vtid"
            
        def startAddress(self):
            return 0x00010000
            
        def endAddress(self):
            return 0x000103ff

        class _MapDs1LcEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 17
        
            def name(self):
                return "MapDs1LcEn"
            
            def description(self):
                return "DS1E1 Loop code insert enable 0: Disable 1: CSU Loop up 2: CSU Loop down 3: FA1 Loop up 4: FA1 Loop down 5: FA2 Loop up 6: FA2 Loop down"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MapFrameType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 15
        
            def name(self):
                return "MapFrameType"
            
            def description(self):
                return "depend on MapSigType[3:0], the MapFrameType[1:0] bit field can have difference meaning. 0: DS1 SF/E1 BF/E3 G.751(unused) /DS3 framing(unused)/VT1.5,VT2,VT6 normal/STS no POH, no Stuff 1: DS1 ESF/E1 CRC/E3 G.832(unused)/DS1,E1,VT1.5,VT2 fractional 2: CEP DS3/E3/VC3 fractional 3:CEP basic mode or VT with POH/STS with POH, Stuff/DS1,E1,DS3,E3 unframe mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MapSigType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 11
        
            def name(self):
                return "MapSigType"
            
            def description(self):
                return "0: DS1 1: E1 2: DS3 (unused) 3: E3 (unused) 4: VT 1.5 5: VT 2 6: unused 7: unused 8: VC3 9: VC4 10: STS12c/VC4_4c 12: TU3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MapTimeSrcMaster(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "MapTimeSrcMaster"
            
            def description(self):
                return "This bit is used to indicate the master timing or the master VC3 in VC4/VC4-Xc"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MapTimeSrcId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MapTimeSrcId"
            
            def description(self):
                return "The reference line ID used for timing reference or the master VC3 ID in VC4/VC4-Xc"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MapDs1LcEn"] = _AF6CCI0011_RD_MAP._map_line_ctrl._MapDs1LcEn()
            allFields["MapFrameType"] = _AF6CCI0011_RD_MAP._map_line_ctrl._MapFrameType()
            allFields["MapSigType"] = _AF6CCI0011_RD_MAP._map_line_ctrl._MapSigType()
            allFields["MapTimeSrcMaster"] = _AF6CCI0011_RD_MAP._map_line_ctrl._MapTimeSrcMaster()
            allFields["MapTimeSrcId"] = _AF6CCI0011_RD_MAP._map_line_ctrl._MapTimeSrcId()
            return allFields

    class _map_idle_code(AtRegister.AtRegister):
        def name(self):
            return "Map IDLE Code"
    
        def description(self):
            return "The registers provide the idle pattern"
            
        def width(self):
            return 8
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00018200
            
        def endAddress(self):
            return 0xffffffff

        class _IdleCode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IdleCode"
            
            def description(self):
                return "Idle code"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IdleCode"] = _AF6CCI0011_RD_MAP._map_idle_code._IdleCode()
            return allFields

    class _RAM_Map_Parity_Force_Control(AtRegister.AtRegister):
        def name(self):
            return "RAM Map Parity Force Control"
    
        def description(self):
            return "This register configures force parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00018208
            
        def endAddress(self):
            return 0xffffffff

        class _MAPLineCtrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "MAPLineCtrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"MAP Thalassa Map Line Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPChlCtrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MAPChlCtrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"MAP Thalassa Map Channel Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MAPLineCtrl_ParErrFrc"] = _AF6CCI0011_RD_MAP._RAM_Map_Parity_Force_Control._MAPLineCtrl_ParErrFrc()
            allFields["MAPChlCtrl_ParErrFrc"] = _AF6CCI0011_RD_MAP._RAM_Map_Parity_Force_Control._MAPChlCtrl_ParErrFrc()
            return allFields

    class _RAM_Map_Parity_Disable_Control(AtRegister.AtRegister):
        def name(self):
            return "RAM Map Parity Disable Control"
    
        def description(self):
            return "This register configures force parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00018209
            
        def endAddress(self):
            return 0xffffffff

        class _MAPLineCtrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "MAPLineCtrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"MAP Thalassa Map Line Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPChlCtrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MAPChlCtrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"MAP Thalassa Map Channel Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MAPLineCtrl_ParErrDis"] = _AF6CCI0011_RD_MAP._RAM_Map_Parity_Disable_Control._MAPLineCtrl_ParErrDis()
            allFields["MAPChlCtrl_ParErrDis"] = _AF6CCI0011_RD_MAP._RAM_Map_Parity_Disable_Control._MAPChlCtrl_ParErrDis()
            return allFields

    class _RAM_Map_Parity_Error_Sticky(AtRegister.AtRegister):
        def name(self):
            return "RAM Map parity Error Sticky"
    
        def description(self):
            return "This register configures disable parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0001820a
            
        def endAddress(self):
            return 0xffffffff

        class _MAPLineCtrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "MAPLineCtrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"MAP Thalassa Map Line Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPChlCtrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MAPChlCtrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"MAP Thalassa Map Channel Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MAPLineCtrl_ParErrStk"] = _AF6CCI0011_RD_MAP._RAM_Map_Parity_Error_Sticky._MAPLineCtrl_ParErrStk()
            allFields["MAPChlCtrl_ParErrStk"] = _AF6CCI0011_RD_MAP._RAM_Map_Parity_Error_Sticky._MAPChlCtrl_ParErrStk()
            return allFields

    class _RAM_DeMap_Parity_Force_Control(AtRegister.AtRegister):
        def name(self):
            return "RAM DeMap Parity Force Control"
    
        def description(self):
            return "This register configures force parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00004218
            
        def endAddress(self):
            return 0xffffffff

        class _DeMAPChlCtrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DeMAPChlCtrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"DeMAP Thalassa DeMap Channel Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DeMAPChlCtrl_ParErrFrc"] = _AF6CCI0011_RD_MAP._RAM_DeMap_Parity_Force_Control._DeMAPChlCtrl_ParErrFrc()
            return allFields

    class _RAM_DeMap_Parity_Disable_Control(AtRegister.AtRegister):
        def name(self):
            return "RAM DeMap Parity Disable Control"
    
        def description(self):
            return "This register configures force parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00004219
            
        def endAddress(self):
            return 0xffffffff

        class _DeMAPChlCtrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DeMAPChlCtrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"DeMAP Thalassa DeMap Channel Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DeMAPChlCtrl_ParErrDis"] = _AF6CCI0011_RD_MAP._RAM_DeMap_Parity_Disable_Control._DeMAPChlCtrl_ParErrDis()
            return allFields

    class _RAM_DeMap_Parity_Error_Sticky(AtRegister.AtRegister):
        def name(self):
            return "RAM DeMap parity Error Sticky"
    
        def description(self):
            return "This register configures disable parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000421a
            
        def endAddress(self):
            return 0xffffffff

        class _DeMAPChlCtrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DeMAPChlCtrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"DeMAP Thalassa DeMap Channel Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DeMAPChlCtrl_ParErrStk"] = _AF6CCI0011_RD_MAP._RAM_DeMap_Parity_Error_Sticky._DeMAPChlCtrl_ParErrStk()
            return allFields

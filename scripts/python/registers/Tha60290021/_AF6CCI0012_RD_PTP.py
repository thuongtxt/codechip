import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0012_RD_PTP(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["upen_hold1"] = _AF6CCI0012_RD_PTP._upen_hold1()
        allRegisters["upen_hold2"] = _AF6CCI0012_RD_PTP._upen_hold2()
        allRegisters["upen_hold3"] = _AF6CCI0012_RD_PTP._upen_hold3()
        allRegisters["upen_ptp_en"] = _AF6CCI0012_RD_PTP._upen_ptp_en()
        allRegisters["upen_ptp_stmode"] = _AF6CCI0012_RD_PTP._upen_ptp_stmode()
        allRegisters["upen_ptp_ms"] = _AF6CCI0012_RD_PTP._upen_ptp_ms()
        allRegisters["upen_ptp_dev"] = _AF6CCI0012_RD_PTP._upen_ptp_dev()
        allRegisters["upen_ms_srcpid"] = _AF6CCI0012_RD_PTP._upen_ms_srcpid()
        allRegisters["upen_sl_reqpid"] = _AF6CCI0012_RD_PTP._upen_sl_reqpid()
        allRegisters["upen_chsv4_bypass"] = _AF6CCI0012_RD_PTP._upen_chsv4_bypass()
        allRegisters["upen_ingunimac_glb"] = _AF6CCI0012_RD_PTP._upen_ingunimac_glb()
        allRegisters["upen_ingmulmac1_glb"] = _AF6CCI0012_RD_PTP._upen_ingmulmac1_glb()
        allRegisters["upen_ingmulmac2_glb"] = _AF6CCI0012_RD_PTP._upen_ingmulmac2_glb()
        allRegisters["upen_unimac_en"] = _AF6CCI0012_RD_PTP._upen_unimac_en()
        allRegisters["upen_mulmac_en"] = _AF6CCI0012_RD_PTP._upen_mulmac_en()
        allRegisters["upen_anymac_en"] = _AF6CCI0012_RD_PTP._upen_anymac_en()
        allRegisters["upen_v4mulip1_global"] = _AF6CCI0012_RD_PTP._upen_v4mulip1_global()
        allRegisters["upen_v4mulip2_global"] = _AF6CCI0012_RD_PTP._upen_v4mulip2_global()
        allRegisters["upen_v6mulip1_global"] = _AF6CCI0012_RD_PTP._upen_v6mulip1_global()
        allRegisters["upen_v6mulip2_global"] = _AF6CCI0012_RD_PTP._upen_v6mulip2_global()
        allRegisters["upen_anyip_en"] = _AF6CCI0012_RD_PTP._upen_anyip_en()
        allRegisters["upen_ptp_rdy"] = _AF6CCI0012_RD_PTP._upen_ptp_rdy()
        allRegisters["upen_ptl"] = _AF6CCI0012_RD_PTP._upen_ptl()
        allRegisters["upen_tpid"] = _AF6CCI0012_RD_PTP._upen_tpid()
        allRegisters["upen_mac"] = _AF6CCI0012_RD_PTP._upen_mac()
        allRegisters["upen_uniip1"] = _AF6CCI0012_RD_PTP._upen_uniip1()
        allRegisters["upen_uniip2"] = _AF6CCI0012_RD_PTP._upen_uniip2()
        allRegisters["upen_uniip3"] = _AF6CCI0012_RD_PTP._upen_uniip3()
        allRegisters["upen_uniip4"] = _AF6CCI0012_RD_PTP._upen_uniip4()
        allRegisters["upen_mulip"] = _AF6CCI0012_RD_PTP._upen_mulip()
        allRegisters["upen_ipen"] = _AF6CCI0012_RD_PTP._upen_ipen()
        allRegisters["upen_rx_claerr_cnt"] = _AF6CCI0012_RD_PTP._upen_rx_claerr_cnt()
        allRegisters["upen_rx_ptp_cnt"] = _AF6CCI0012_RD_PTP._upen_rx_ptp_cnt()
        allRegisters["upen_rx_sync_cnt"] = _AF6CCI0012_RD_PTP._upen_rx_sync_cnt()
        allRegisters["upen_rx_folu_cnt"] = _AF6CCI0012_RD_PTP._upen_rx_folu_cnt()
        allRegisters["upen_rx_dreq_cnt"] = _AF6CCI0012_RD_PTP._upen_rx_dreq_cnt()
        allRegisters["upen_rx_dres_cnt"] = _AF6CCI0012_RD_PTP._upen_rx_dres_cnt()
        allRegisters["upen_rx_extr_err_syncnt"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_syncnt()
        allRegisters["upen_rx_extr_err_folucnt"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_folucnt()
        allRegisters["upen_rx_extr_err_dreqcnt"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_dreqcnt()
        allRegisters["upen_rx_extr_err_drescnt"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_drescnt()
        allRegisters["upen_rx_chs_err_syncnt"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_syncnt()
        allRegisters["upen_rx_chs_err_folucnt"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_folucnt()
        allRegisters["upen_rx_chs_err_dreqcnt"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_dreqcnt()
        allRegisters["upen_rx_chs_err_drescnt"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_drescnt()
        allRegisters["upen_tx_sync_cnt"] = _AF6CCI0012_RD_PTP._upen_tx_sync_cnt()
        allRegisters["upen_tx_folu_cnt"] = _AF6CCI0012_RD_PTP._upen_tx_folu_cnt()
        allRegisters["upen_tx_dreq_cnt"] = _AF6CCI0012_RD_PTP._upen_tx_dreq_cnt()
        allRegisters["upen_tx_dres_cnt"] = _AF6CCI0012_RD_PTP._upen_tx_dres_cnt()
        allRegisters["upen_staend_num"] = _AF6CCI0012_RD_PTP._upen_staend_num()
        allRegisters["upen_cla_stk"] = _AF6CCI0012_RD_PTP._upen_cla_stk()
        allRegisters["upen_chk_stk"] = _AF6CCI0012_RD_PTP._upen_chk_stk()
        allRegisters["upen_getinf_stk"] = _AF6CCI0012_RD_PTP._upen_getinf_stk()
        allRegisters["upen_trans_stk"] = _AF6CCI0012_RD_PTP._upen_trans_stk()
        allRegisters["upen_cfg_ptp_bypass"] = _AF6CCI0012_RD_PTP._upen_cfg_ptp_bypass()
        allRegisters["upen_cfg_mac_sa"] = _AF6CCI0012_RD_PTP._upen_cfg_mac_sa()
        allRegisters["upen_cfg_mac_da"] = _AF6CCI0012_RD_PTP._upen_cfg_mac_da()
        allRegisters["upen_cfg_ip4_sa1"] = _AF6CCI0012_RD_PTP._upen_cfg_ip4_sa1()
        allRegisters["upen_cfg_ip4_sa2"] = _AF6CCI0012_RD_PTP._upen_cfg_ip4_sa2()
        allRegisters["upen_cfg_ip4_sa3"] = _AF6CCI0012_RD_PTP._upen_cfg_ip4_sa3()
        allRegisters["upen_cfg_ip4_sa4"] = _AF6CCI0012_RD_PTP._upen_cfg_ip4_sa4()
        allRegisters["upen_cfg_ip4_da1"] = _AF6CCI0012_RD_PTP._upen_cfg_ip4_da1()
        allRegisters["upen_cfg_ip4_da2"] = _AF6CCI0012_RD_PTP._upen_cfg_ip4_da2()
        allRegisters["upen_cfg_ip4_da3"] = _AF6CCI0012_RD_PTP._upen_cfg_ip4_da3()
        allRegisters["upen_cfg_ip4_da4"] = _AF6CCI0012_RD_PTP._upen_cfg_ip4_da4()
        allRegisters["upen_cfg_ip6_sa1"] = _AF6CCI0012_RD_PTP._upen_cfg_ip6_sa1()
        allRegisters["upen_cfg_ip6_sa2"] = _AF6CCI0012_RD_PTP._upen_cfg_ip6_sa2()
        allRegisters["upen_cfg_ip6_sa3"] = _AF6CCI0012_RD_PTP._upen_cfg_ip6_sa3()
        allRegisters["upen_cfg_ip6_sa4"] = _AF6CCI0012_RD_PTP._upen_cfg_ip6_sa4()
        allRegisters["upen_cfg_ip6_da1"] = _AF6CCI0012_RD_PTP._upen_cfg_ip6_da1()
        allRegisters["upen_cfg_ip6_da2"] = _AF6CCI0012_RD_PTP._upen_cfg_ip6_da2()
        allRegisters["upen_cfg_ip6_da3"] = _AF6CCI0012_RD_PTP._upen_cfg_ip6_da3()
        allRegisters["upen_cfg_ip6_da4"] = _AF6CCI0012_RD_PTP._upen_cfg_ip6_da4()
        allRegisters["upen_cpuque_stk"] = _AF6CCI0012_RD_PTP._upen_cpuque_stk()
        allRegisters["upen_t1t3mode"] = _AF6CCI0012_RD_PTP._upen_t1t3mode()
        allRegisters["upen_cpu_flush"] = _AF6CCI0012_RD_PTP._upen_cpu_flush()
        allRegisters["upen_cpuque"] = _AF6CCI0012_RD_PTP._upen_cpuque()
        return allRegisters

    class _upen_hold1(AtRegister.AtRegister):
        def name(self):
            return "Hold Register 1"
    
        def description(self):
            return "This register hold value 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_hold1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_hold1"
            
            def description(self):
                return "hold value 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_hold1"] = _AF6CCI0012_RD_PTP._upen_hold1._cfg_hold1()
            return allFields

    class _upen_hold2(AtRegister.AtRegister):
        def name(self):
            return "Hold Register 2"
    
        def description(self):
            return "This register hold value 2"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000001
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_hold2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_hold2"
            
            def description(self):
                return "hold value 2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_hold2"] = _AF6CCI0012_RD_PTP._upen_hold2._cfg_hold2()
            return allFields

    class _upen_hold3(AtRegister.AtRegister):
        def name(self):
            return "Hold Register 3"
    
        def description(self):
            return "This register hold value"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_hold3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_hold3"
            
            def description(self):
                return "hold value 3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_hold3"] = _AF6CCI0012_RD_PTP._upen_hold3._cfg_hold3()
            return allFields

    class _upen_ptp_en(AtRegister.AtRegister):
        def name(self):
            return "PTP enable"
    
        def description(self):
            return "used to enable ptp for 16 port ingress"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ptp_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ptp_en"
            
            def description(self):
                return "'1' : enable, '0': disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ptp_en"] = _AF6CCI0012_RD_PTP._upen_ptp_en._cfg_ptp_en()
            return allFields

    class _upen_ptp_stmode(AtRegister.AtRegister):
        def name(self):
            return "PTP enable"
    
        def description(self):
            return "used to indicate 1-step/2-step mode for each port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000004
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ptp_stmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ptp_stmode"
            
            def description(self):
                return "'1' : 2-step, '0': 1-step"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ptp_stmode"] = _AF6CCI0012_RD_PTP._upen_ptp_stmode._cfg_ptp_stmode()
            return allFields

    class _upen_ptp_ms(AtRegister.AtRegister):
        def name(self):
            return "PTP enable"
    
        def description(self):
            return "used to indicate master/slaver mode for each port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000005
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ptp_ms(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ptp_ms"
            
            def description(self):
                return "'1' : master, '0': slaver"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ptp_ms"] = _AF6CCI0012_RD_PTP._upen_ptp_ms._cfg_ptp_ms()
            return allFields

    class _upen_ptp_dev(AtRegister.AtRegister):
        def name(self):
            return "PTP Device"
    
        def description(self):
            return "This register is used to config PTP device"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000006
            
        def endAddress(self):
            return 0xffffffff

        class _device_mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "device_mode"
            
            def description(self):
                return "'00': BC mode, '01': reserved '10': TC Separate mode, '11': TC General mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["device_mode"] = _AF6CCI0012_RD_PTP._upen_ptp_dev._device_mode()
            return allFields

    class _upen_ms_srcpid(AtRegister.AtRegister):
        def name(self):
            return "Config Master SPID"
    
        def description(self):
            return "Used to checking sourcePortIdentity of Sync, Follow-up and Delay Response packet"
            
        def width(self):
            return 96
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000007
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ms_srcpid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 79
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ms_srcpid"
            
            def description(self):
                return "sourcePortIdentity value of Master"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ms_srcpid"] = _AF6CCI0012_RD_PTP._upen_ms_srcpid._cfg_ms_srcpid()
            return allFields

    class _upen_sl_reqpid(AtRegister.AtRegister):
        def name(self):
            return "Config Slaver ReqPID"
    
        def description(self):
            return "Used to checking requestingPortIdentity of Delay Response packet"
            
        def width(self):
            return 96
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000008
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_sl_reqpid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 79
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_sl_reqpid"
            
            def description(self):
                return "RequestingPortIdentity value of Slaver"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_sl_reqpid"] = _AF6CCI0012_RD_PTP._upen_sl_reqpid._cfg_sl_reqpid()
            return allFields

    class _upen_chsv4_bypass(AtRegister.AtRegister):
        def name(self):
            return "Config chsv4 bypass"
    
        def description(self):
            return "Used to indicate bypass ipv4 header checksum error"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000009
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_chsv4_bypass(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_chsv4_bypass"
            
            def description(self):
                return "'1': enable, '0': disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_chsv4_bypass"] = _AF6CCI0012_RD_PTP._upen_chsv4_bypass._cfg_chsv4_bypass()
            return allFields

    class _upen_ingunimac_glb(AtRegister.AtRegister):
        def name(self):
            return "config uni MAC global ingress"
    
        def description(self):
            return "config uni MAC address global for ingress ports"
            
        def width(self):
            return 48
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000a
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ingunimac_glb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ingunimac_glb"
            
            def description(self):
                return "uni MAC address global for ingress ports"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ingunimac_glb"] = _AF6CCI0012_RD_PTP._upen_ingunimac_glb._cfg_ingunimac_glb()
            return allFields

    class _upen_ingmulmac1_glb(AtRegister.AtRegister):
        def name(self):
            return "config mul MAC global 1 ingress"
    
        def description(self):
            return "config mul MAC address global 1 for ingress ports"
            
        def width(self):
            return 48
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000b
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ingmulmac1_glb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ingmulmac1_glb"
            
            def description(self):
                return "mul MAC address global 1 for ingress ports"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ingmulmac1_glb"] = _AF6CCI0012_RD_PTP._upen_ingmulmac1_glb._cfg_ingmulmac1_glb()
            return allFields

    class _upen_ingmulmac2_glb(AtRegister.AtRegister):
        def name(self):
            return "config mul MAC global 2 ingress"
    
        def description(self):
            return "config mul MAC address global 2 for ingress ports"
            
        def width(self):
            return 48
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000c
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ingmulmac2_glb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ingmulmac2_glb"
            
            def description(self):
                return "mul MAC address global 2 for ingress ports"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ingmulmac2_glb"] = _AF6CCI0012_RD_PTP._upen_ingmulmac2_glb._cfg_ingmulmac2_glb()
            return allFields

    class _upen_unimac_en(AtRegister.AtRegister):
        def name(self):
            return "config uni MAC matching enable"
    
        def description(self):
            return "is used to indicate unicast MAC matching enable"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000d
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_unimac_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_unimac_en"
            
            def description(self):
                return "[16]   : en/dis uni mac matching global for all port [15:0] : en/dis uni mac matching for port 0 to port 15"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_unimac_en"] = _AF6CCI0012_RD_PTP._upen_unimac_en._cfg_unimac_en()
            return allFields

    class _upen_mulmac_en(AtRegister.AtRegister):
        def name(self):
            return "config multi MAC matching enable"
    
        def description(self):
            return "is used to indicate multicast MAC matching enable"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000e
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_mulmac_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_mulmac_en"
            
            def description(self):
                return "[17]   : en/dis mul mac matching global 2 for all port [16]   : en/dis mul mac matching global 1 for all port [15:0] : en/dis mul mac matching for port 0 to port 15"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_mulmac_en"] = _AF6CCI0012_RD_PTP._upen_mulmac_en._cfg_mulmac_en()
            return allFields

    class _upen_anymac_en(AtRegister.AtRegister):
        def name(self):
            return "config any MAC enable"
    
        def description(self):
            return "is used to enable any mac for each port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000f
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_anymac_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_anymac_en"
            
            def description(self):
                return "'1': enable , '0': disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_anymac_en"] = _AF6CCI0012_RD_PTP._upen_anymac_en._cfg_anymac_en()
            return allFields

    class _upen_v4mulip1_global(AtRegister.AtRegister):
        def name(self):
            return "Config mul ipv4 global 1"
    
        def description(self):
            return "config multicast ipv4 address global 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000010
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_v4mulip1_global(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_v4mulip1_global"
            
            def description(self):
                return "used to check multicast ip"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_v4mulip1_global"] = _AF6CCI0012_RD_PTP._upen_v4mulip1_global._cfg_v4mulip1_global()
            return allFields

    class _upen_v4mulip2_global(AtRegister.AtRegister):
        def name(self):
            return "Config mul ipv4 global 2"
    
        def description(self):
            return "config multicast ipv4 address global 2"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000011
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_v4mulip2_global(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_v4mulip2_global"
            
            def description(self):
                return "used to check multicast ip"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_v4mulip2_global"] = _AF6CCI0012_RD_PTP._upen_v4mulip2_global._cfg_v4mulip2_global()
            return allFields

    class _upen_v6mulip1_global(AtRegister.AtRegister):
        def name(self):
            return "Config mul ipv6 global 1"
    
        def description(self):
            return "config multicast ipv6 address global 1"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000012
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_v6mulip1_global(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_v6mulip1_global"
            
            def description(self):
                return "used to check multicast ip"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_v6mulip1_global"] = _AF6CCI0012_RD_PTP._upen_v6mulip1_global._cfg_v6mulip1_global()
            return allFields

    class _upen_v6mulip2_global(AtRegister.AtRegister):
        def name(self):
            return "Config mul ipv6 global 2"
    
        def description(self):
            return "config multicast ipv6 address global 2"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000013
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_v6mulip2_global(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_v6mulip2_global"
            
            def description(self):
                return "used to check multicast ip"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_v6mulip2_global"] = _AF6CCI0012_RD_PTP._upen_v6mulip2_global._cfg_v6mulip2_global()
            return allFields

    class _upen_anyip_en(AtRegister.AtRegister):
        def name(self):
            return "config any IP enable"
    
        def description(self):
            return "is used to enable any ip for each port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000014
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_anyip_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_anyip_en"
            
            def description(self):
                return "'1': enable , '0': disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_anyip_en"] = _AF6CCI0012_RD_PTP._upen_anyip_en._cfg_anyip_en()
            return allFields

    class _upen_ptp_rdy(AtRegister.AtRegister):
        def name(self):
            return "PTP Ready"
    
        def description(self):
            return "This register is used to indicate config ptp done"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000015
            
        def endAddress(self):
            return 0xffffffff

        class _ptp_rdy(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ptp_rdy"
            
            def description(self):
                return "'1' : ready"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ptp_rdy"] = _AF6CCI0012_RD_PTP._upen_ptp_rdy._ptp_rdy()
            return allFields

    class _upen_ptl(AtRegister.AtRegister):
        def name(self):
            return "cfg protocol"
    
        def description(self):
            return "config protocol for each port at BC mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "config"
            
        def fomular(self):
            return "0x00_0060 + $pid"
            
        def startAddress(self):
            return 0x00000060
            
        def endAddress(self):
            return 0x0000006f

        class _cfg_ptl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ptl"
            
            def description(self):
                return "protocol value: '000': L2 '001': ipv4 '010': ipv6 '011': ipv4 vpn '100': ipv6 vpn"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ptl"] = _AF6CCI0012_RD_PTP._upen_ptl._cfg_ptl()
            return allFields

    class _upen_tpid(AtRegister.AtRegister):
        def name(self):
            return "cfg tpid"
    
        def description(self):
            return "config tpid for each port"
            
        def width(self):
            return 64
        
        def type(self):
            return "config"
            
        def fomular(self):
            return "0x00_0070 + $pid"
            
        def startAddress(self):
            return 0x00000070
            
        def endAddress(self):
            return 0x0000007f

        class _cfg_tpid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_tpid"
            
            def description(self):
                return "[63:48]: tpid value 4 [47:32]: tpid value 3 [31:16]: tpid value 2 [15:00]: tpid value 1 tpid value: 88a8, 8100, 9100 or 9200"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_tpid"] = _AF6CCI0012_RD_PTP._upen_tpid._cfg_tpid()
            return allFields

    class _upen_mac(AtRegister.AtRegister):
        def name(self):
            return "cfg uni/mul mac"
    
        def description(self):
            return "config uni/mul MAC for each port"
            
        def width(self):
            return 48
        
        def type(self):
            return "config"
            
        def fomular(self):
            return "0x00_0080 + $transtype*16 + $pid"
            
        def startAddress(self):
            return 0x00000080
            
        def endAddress(self):
            return 0x0000009f

        class _cfg_mac(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_mac"
            
            def description(self):
                return "unicast/multicast mac for each port"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_mac"] = _AF6CCI0012_RD_PTP._upen_mac._cfg_mac()
            return allFields

    class _upen_uniip1(AtRegister.AtRegister):
        def name(self):
            return "cfg uni ip 1"
    
        def description(self):
            return "config unicast IP 1 for each port"
            
        def width(self):
            return 128
        
        def type(self):
            return "config"
            
        def fomular(self):
            return "0x00_00A0 + $pid"
            
        def startAddress(self):
            return 0x000000a0
            
        def endAddress(self):
            return 0x000000af

        class _cfg_uniip1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_uniip1"
            
            def description(self):
                return "unicast ip 1 for each port ipv4:"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_uniip1"] = _AF6CCI0012_RD_PTP._upen_uniip1._cfg_uniip1()
            return allFields

    class _upen_uniip2(AtRegister.AtRegister):
        def name(self):
            return "cfg uni ip 2"
    
        def description(self):
            return "config unicast IP 2 for each port"
            
        def width(self):
            return 128
        
        def type(self):
            return "config"
            
        def fomular(self):
            return "0x00_00B0 + $pid"
            
        def startAddress(self):
            return 0x000000b0
            
        def endAddress(self):
            return 0x000000bf

        class _cfg_uniip2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_uniip2"
            
            def description(self):
                return "unicast ip 2 for each port ipv4:"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_uniip2"] = _AF6CCI0012_RD_PTP._upen_uniip2._cfg_uniip2()
            return allFields

    class _upen_uniip3(AtRegister.AtRegister):
        def name(self):
            return "cfg uni ip 3"
    
        def description(self):
            return "config unicast IP 3 for each port"
            
        def width(self):
            return 128
        
        def type(self):
            return "config"
            
        def fomular(self):
            return "0x00_00C0 + $pid"
            
        def startAddress(self):
            return 0x000000c0
            
        def endAddress(self):
            return 0x000000cf

        class _cfg_uniip3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_uniip3"
            
            def description(self):
                return "unicast ip 3 for each port ipv4:"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_uniip3"] = _AF6CCI0012_RD_PTP._upen_uniip3._cfg_uniip3()
            return allFields

    class _upen_uniip4(AtRegister.AtRegister):
        def name(self):
            return "cfg uni ip 4"
    
        def description(self):
            return "config unicast IP 4 for each port"
            
        def width(self):
            return 128
        
        def type(self):
            return "config"
            
        def fomular(self):
            return "0x00_00D0 + $pid"
            
        def startAddress(self):
            return 0x000000d0
            
        def endAddress(self):
            return 0x000000df

        class _cfg_uniip4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_uniip4"
            
            def description(self):
                return "unicast ip 4 for each port ipv4:"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_uniip4"] = _AF6CCI0012_RD_PTP._upen_uniip4._cfg_uniip4()
            return allFields

    class _upen_mulip(AtRegister.AtRegister):
        def name(self):
            return "cfg mul ip"
    
        def description(self):
            return "config multicast IP for each port"
            
        def width(self):
            return 128
        
        def type(self):
            return "config"
            
        def fomular(self):
            return "0x00_00E0 + $pid"
            
        def startAddress(self):
            return 0x000000e0
            
        def endAddress(self):
            return 0x000000ef

        class _cfg_mulip(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_mulip"
            
            def description(self):
                return "multicast ip for each port ipv4:"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_mulip"] = _AF6CCI0012_RD_PTP._upen_mulip._cfg_mulip()
            return allFields

    class _upen_ipen(AtRegister.AtRegister):
        def name(self):
            return "cfg ip matching enable"
    
        def description(self):
            return "config IPv4/IPv6 matching for each port"
            
        def width(self):
            return 32
        
        def type(self):
            return "config"
            
        def fomular(self):
            return "0x00_00F0 + $pid"
            
        def startAddress(self):
            return 0x000000f0
            
        def endAddress(self):
            return 0x000000ff

        class _cfg_ipen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ipen"
            
            def description(self):
                return "config ip matching for each port [6]: ena/dis mul ip global 2 matching [5]: ena/dis mul ip global 1 matching [4]: ena/dis mul ip matching [3]: ena/dis uni ip 4 matching [2]: ena/dis uni ip 3 matching [1]: ena/dis uni ip 2 matching [0]: ena/dis uni ip 1 matching"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ipen"] = _AF6CCI0012_RD_PTP._upen_ipen._cfg_ipen()
            return allFields

    class _upen_rx_claerr_cnt(AtRegister.AtRegister):
        def name(self):
            return "Counter PTP Classify Err"
    
        def description(self):
            return "Number of classify err packet in classify module"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_0200 + $pid"
            
        def startAddress(self):
            return 0x00000200
            
        def endAddress(self):
            return 0x0000020f

        class _rx_claerr_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_claerr_cnt"
            
            def description(self):
                return "Number classify error packet"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_claerr_cnt"] = _AF6CCI0012_RD_PTP._upen_rx_claerr_cnt._rx_claerr_cnt()
            return allFields

    class _upen_rx_ptp_cnt(AtRegister.AtRegister):
        def name(self):
            return "Counter PTP Packet"
    
        def description(self):
            return "Number of ptp packet in classify module"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_0210 + $pid"
            
        def startAddress(self):
            return 0x00000210
            
        def endAddress(self):
            return 0x0000021f

        class _rx_ptp_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_ptp_cnt"
            
            def description(self):
                return "Number ptp packet"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_ptp_cnt"] = _AF6CCI0012_RD_PTP._upen_rx_ptp_cnt._rx_ptp_cnt()
            return allFields

    class _upen_rx_sync_cnt(AtRegister.AtRegister):
        def name(self):
            return "Counter Sync Received"
    
        def description(self):
            return "Number of Sync message received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_0240 + $pid"
            
        def startAddress(self):
            return 0x00000240
            
        def endAddress(self):
            return 0x0000024f

        class _syncrx_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "syncrx_cnt"
            
            def description(self):
                return "number sync packet at Rx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["syncrx_cnt"] = _AF6CCI0012_RD_PTP._upen_rx_sync_cnt._syncrx_cnt()
            return allFields

    class _upen_rx_folu_cnt(AtRegister.AtRegister):
        def name(self):
            return "Counter Folu Received"
    
        def description(self):
            return "Number of folu message received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_0250 + $pid"
            
        def startAddress(self):
            return 0x00000250
            
        def endAddress(self):
            return 0x0000025f

        class _folurx_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "folurx_cnt"
            
            def description(self):
                return "number folu packet at Rx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["folurx_cnt"] = _AF6CCI0012_RD_PTP._upen_rx_folu_cnt._folurx_cnt()
            return allFields

    class _upen_rx_dreq_cnt(AtRegister.AtRegister):
        def name(self):
            return "Counter dreq Received"
    
        def description(self):
            return "Number of dreq message received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_0260 + $pid"
            
        def startAddress(self):
            return 0x00000260
            
        def endAddress(self):
            return 0x0000026f

        class _dreqrx_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dreqrx_cnt"
            
            def description(self):
                return "number dreq packet at Rx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dreqrx_cnt"] = _AF6CCI0012_RD_PTP._upen_rx_dreq_cnt._dreqrx_cnt()
            return allFields

    class _upen_rx_dres_cnt(AtRegister.AtRegister):
        def name(self):
            return "Counter dres Received"
    
        def description(self):
            return "Number of dres message received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_0270 + $pid"
            
        def startAddress(self):
            return 0x00000270
            
        def endAddress(self):
            return 0x0000027f

        class _dresrx_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dresrx_cnt"
            
            def description(self):
                return "number dres packet at Rx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dresrx_cnt"] = _AF6CCI0012_RD_PTP._upen_rx_dres_cnt._dresrx_cnt()
            return allFields

    class _upen_rx_extr_err_syncnt(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Sync Extract Err"
    
        def description(self):
            return "Number of extr err sync mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_0280 + $pid"
            
        def startAddress(self):
            return 0x00000280
            
        def endAddress(self):
            return 0x0000028f

        class _rx_extrerr_synccnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_extrerr_synccnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_extrerr_synccnt"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_syncnt._rx_extrerr_synccnt()
            return allFields

    class _upen_rx_extr_err_folucnt(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Folu Extract Err"
    
        def description(self):
            return "Number of extr err folu mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_0290 + $pid"
            
        def startAddress(self):
            return 0x00000290
            
        def endAddress(self):
            return 0x0000029f

        class _rx_extrerr_foluccnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_extrerr_foluccnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_extrerr_foluccnt"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_folucnt._rx_extrerr_foluccnt()
            return allFields

    class _upen_rx_extr_err_dreqcnt(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Dreq Extract Err"
    
        def description(self):
            return "Number of extr err dreq mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_02A0 + $pid"
            
        def startAddress(self):
            return 0x000002a0
            
        def endAddress(self):
            return 0x000002af

        class _rx_extrerr_dreqcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_extrerr_dreqcnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_extrerr_dreqcnt"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_dreqcnt._rx_extrerr_dreqcnt()
            return allFields

    class _upen_rx_extr_err_drescnt(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Dres Extract Err"
    
        def description(self):
            return "Number of extr err dres mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_02B0 + $pid"
            
        def startAddress(self):
            return 0x000002b0
            
        def endAddress(self):
            return 0x000002bf

        class _rx_extrerr_drescnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_extrerr_drescnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_extrerr_drescnt"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_drescnt._rx_extrerr_drescnt()
            return allFields

    class _upen_rx_chs_err_syncnt(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Sync Chsum Err"
    
        def description(self):
            return "Number of Chs err sync mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_02C0 + $pid"
            
        def startAddress(self):
            return 0x000002c0
            
        def endAddress(self):
            return 0x000002cf

        class _rx_cherr_synccnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_cherr_synccnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_cherr_synccnt"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_syncnt._rx_cherr_synccnt()
            return allFields

    class _upen_rx_chs_err_folucnt(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Folu Chsum Err"
    
        def description(self):
            return "Number of Chs err folu mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_02D0 + $pid"
            
        def startAddress(self):
            return 0x000002d0
            
        def endAddress(self):
            return 0x000002df

        class _rx_cherr_folucnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_cherr_folucnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_cherr_folucnt"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_folucnt._rx_cherr_folucnt()
            return allFields

    class _upen_rx_chs_err_dreqcnt(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Dreq Chsum Err"
    
        def description(self):
            return "Number of Chs err dreq mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_02E0 + $pid"
            
        def startAddress(self):
            return 0x000002e0
            
        def endAddress(self):
            return 0x000002ef

        class _rx_cherr_dreqcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_cherr_dreqcnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_cherr_dreqcnt"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_dreqcnt._rx_cherr_dreqcnt()
            return allFields

    class _upen_rx_chs_err_drescnt(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Dres Chsum Err"
    
        def description(self):
            return "Number of Chs err dres mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_02F0 + $pid"
            
        def startAddress(self):
            return 0x000002f0
            
        def endAddress(self):
            return 0x000002ff

        class _rx_cherr_drescnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_cherr_drescnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_cherr_drescnt"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_drescnt._rx_cherr_drescnt()
            return allFields

    class _upen_tx_sync_cnt(AtRegister.AtRegister):
        def name(self):
            return "Counter Sync Transmit"
    
        def description(self):
            return "Number of Sync message transmit"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_0300 + $pid"
            
        def startAddress(self):
            return 0x00000300
            
        def endAddress(self):
            return 0x0000030f

        class _synctx_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "synctx_cnt"
            
            def description(self):
                return "number sync packet at Tx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["synctx_cnt"] = _AF6CCI0012_RD_PTP._upen_tx_sync_cnt._synctx_cnt()
            return allFields

    class _upen_tx_folu_cnt(AtRegister.AtRegister):
        def name(self):
            return "Counter Folu Transmit"
    
        def description(self):
            return "Number of folu message transmit"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_0310 + $pid"
            
        def startAddress(self):
            return 0x00000310
            
        def endAddress(self):
            return 0x0000031f

        class _folutx_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "folutx_cnt"
            
            def description(self):
                return "number folu packet at Tx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["folutx_cnt"] = _AF6CCI0012_RD_PTP._upen_tx_folu_cnt._folutx_cnt()
            return allFields

    class _upen_tx_dreq_cnt(AtRegister.AtRegister):
        def name(self):
            return "Counter dreq transmit"
    
        def description(self):
            return "Number of dreq message transmit"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_0320 + $pid"
            
        def startAddress(self):
            return 0x00000320
            
        def endAddress(self):
            return 0x0000032f

        class _dreqtx_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dreqtx_cnt"
            
            def description(self):
                return "number dreq packet at Tx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dreqtx_cnt"] = _AF6CCI0012_RD_PTP._upen_tx_dreq_cnt._dreqtx_cnt()
            return allFields

    class _upen_tx_dres_cnt(AtRegister.AtRegister):
        def name(self):
            return "Counter dres transmit"
    
        def description(self):
            return "Number of dres message transmit"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_0330 + $pid"
            
        def startAddress(self):
            return 0x00000330
            
        def endAddress(self):
            return 0x0000033f

        class _drestx_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "drestx_cnt"
            
            def description(self):
                return "number dres packet at Tx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["drestx_cnt"] = _AF6CCI0012_RD_PTP._upen_tx_dres_cnt._drestx_cnt()
            return allFields

    class _upen_staend_num(AtRegister.AtRegister):
        def name(self):
            return "Counter sop eop"
    
        def description(self):
            return "number of mac_sop and mac_eop for each port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_0220 + $pid"
            
        def startAddress(self):
            return 0x00000220
            
        def endAddress(self):
            return 0x0000022f

        class _num_sta_end(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "num_sta_end"
            
            def description(self):
                return "Number sop eop of packet"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["num_sta_end"] = _AF6CCI0012_RD_PTP._upen_staend_num._num_sta_end()
            return allFields

    class _upen_cla_stk(AtRegister.AtRegister):
        def name(self):
            return "classify sticky"
    
        def description(self):
            return "sticky of classify"
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000230
            
        def endAddress(self):
            return 0xffffffff

        class _cla_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cla_stk"
            
            def description(self):
                return "sticky ge_classify"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cla_stk"] = _AF6CCI0012_RD_PTP._upen_cla_stk._cla_stk()
            return allFields

    class _upen_chk_stk(AtRegister.AtRegister):
        def name(self):
            return "PTP check sticky"
    
        def description(self):
            return "sticky of ptp check"
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000231
            
        def endAddress(self):
            return 0xffffffff

        class _chk_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "chk_stk"
            
            def description(self):
                return "sticky ptp check"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["chk_stk"] = _AF6CCI0012_RD_PTP._upen_chk_stk._chk_stk()
            return allFields

    class _upen_getinf_stk(AtRegister.AtRegister):
        def name(self):
            return "ptp getinfo sticky"
    
        def description(self):
            return "sticky of ptp get info"
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000232
            
        def endAddress(self):
            return 0xffffffff

        class _getinf_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "getinf_stk"
            
            def description(self):
                return "sticky ptp get info"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["getinf_stk"] = _AF6CCI0012_RD_PTP._upen_getinf_stk._getinf_stk()
            return allFields

    class _upen_trans_stk(AtRegister.AtRegister):
        def name(self):
            return "trans sticky"
    
        def description(self):
            return "sticky of ptp trans"
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000233
            
        def endAddress(self):
            return 0xffffffff

        class _trans_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "trans_stk"
            
            def description(self):
                return "sticky ptp trans"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["trans_stk"] = _AF6CCI0012_RD_PTP._upen_trans_stk._trans_stk()
            return allFields

    class _upen_cfg_ptp_bypass(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "enable for any MAC/IPv4/IPv6"
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00003000
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ptp_bypass(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ptp_bypass"
            
            def description(self):
                return "bit[0] l2_sa bit[1] l2_da bit[2] ip4_sa bit[3] ip4_da bit[4] ip6_sa bit[5] ip6_da"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ptp_bypass"] = _AF6CCI0012_RD_PTP._upen_cfg_ptp_bypass._cfg_ptp_bypass()
            return allFields

    class _upen_cfg_mac_sa(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp mac sa"
            
        def width(self):
            return 128
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00003001
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_mac_sa(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_mac_sa"
            
            def description(self):
                return "MAC SA"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_mac_sa"] = _AF6CCI0012_RD_PTP._upen_cfg_mac_sa._cfg_mac_sa()
            return allFields

    class _upen_cfg_mac_da(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp mac da"
            
        def width(self):
            return 128
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00003002
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_mac_da(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_mac_da"
            
            def description(self):
                return "MAC DA"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_mac_da"] = _AF6CCI0012_RD_PTP._upen_cfg_mac_da._cfg_mac_da()
            return allFields

    class _upen_cfg_ip4_sa1(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp ipv4 sa1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00003003
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ipv4_sa1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ipv4_sa1"
            
            def description(self):
                return "IPv4 SA1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ipv4_sa1"] = _AF6CCI0012_RD_PTP._upen_cfg_ip4_sa1._cfg_ipv4_sa1()
            return allFields

    class _upen_cfg_ip4_sa2(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp ipv4 sa2"
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00003004
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ipv4_sa2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ipv4_sa2"
            
            def description(self):
                return "IPv4 SA2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ipv4_sa2"] = _AF6CCI0012_RD_PTP._upen_cfg_ip4_sa2._cfg_ipv4_sa2()
            return allFields

    class _upen_cfg_ip4_sa3(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp ipv4 sa3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00003005
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ipv4_sa3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ipv4_sa3"
            
            def description(self):
                return "IPv4 SA3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ipv4_sa3"] = _AF6CCI0012_RD_PTP._upen_cfg_ip4_sa3._cfg_ipv4_sa3()
            return allFields

    class _upen_cfg_ip4_sa4(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp ipv4 sa4"
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00003006
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ipv4_sa4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ipv4_sa4"
            
            def description(self):
                return "IPv4 SA4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ipv4_sa4"] = _AF6CCI0012_RD_PTP._upen_cfg_ip4_sa4._cfg_ipv4_sa4()
            return allFields

    class _upen_cfg_ip4_da1(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp ip4 da1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00003007
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ip4_da1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ip4_da1"
            
            def description(self):
                return "MAC DA1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ip4_da1"] = _AF6CCI0012_RD_PTP._upen_cfg_ip4_da1._cfg_ip4_da1()
            return allFields

    class _upen_cfg_ip4_da2(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp ip4 da2"
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00003008
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ip4_da2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ip4_da2"
            
            def description(self):
                return "MAC DA2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ip4_da2"] = _AF6CCI0012_RD_PTP._upen_cfg_ip4_da2._cfg_ip4_da2()
            return allFields

    class _upen_cfg_ip4_da3(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp ip4 da3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00003009
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ip4_da3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ip4_da3"
            
            def description(self):
                return "MAC DA3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ip4_da3"] = _AF6CCI0012_RD_PTP._upen_cfg_ip4_da3._cfg_ip4_da3()
            return allFields

    class _upen_cfg_ip4_da4(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp ip4 da4"
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000300a
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ip4_da4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ip4_da4"
            
            def description(self):
                return "MAC DA4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ip4_da4"] = _AF6CCI0012_RD_PTP._upen_cfg_ip4_da4._cfg_ip4_da4()
            return allFields

    class _upen_cfg_ip6_sa1(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp ipv6 sa1"
            
        def width(self):
            return 128
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000300b
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ipv6_sa1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ipv6_sa1"
            
            def description(self):
                return "IPv6 SA1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ipv6_sa1"] = _AF6CCI0012_RD_PTP._upen_cfg_ip6_sa1._cfg_ipv6_sa1()
            return allFields

    class _upen_cfg_ip6_sa2(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp ipv6 sa2"
            
        def width(self):
            return 128
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000300c
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ipv6_sa2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ipv6_sa2"
            
            def description(self):
                return "IPv6 SA2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ipv6_sa2"] = _AF6CCI0012_RD_PTP._upen_cfg_ip6_sa2._cfg_ipv6_sa2()
            return allFields

    class _upen_cfg_ip6_sa3(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp ipv6 sa3"
            
        def width(self):
            return 128
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000300d
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ipv6_sa3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ipv6_sa3"
            
            def description(self):
                return "IPv6 SA3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ipv6_sa3"] = _AF6CCI0012_RD_PTP._upen_cfg_ip6_sa3._cfg_ipv6_sa3()
            return allFields

    class _upen_cfg_ip6_sa4(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp ipv6 sa4"
            
        def width(self):
            return 128
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000300e
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ipv4_sa4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ipv4_sa4"
            
            def description(self):
                return "IPv4 SA4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ipv4_sa4"] = _AF6CCI0012_RD_PTP._upen_cfg_ip6_sa4._cfg_ipv4_sa4()
            return allFields

    class _upen_cfg_ip6_da1(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp ipv6 da1"
            
        def width(self):
            return 128
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000300f
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ipv6_da1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ipv6_da1"
            
            def description(self):
                return "IPv6 DA1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ipv6_da1"] = _AF6CCI0012_RD_PTP._upen_cfg_ip6_da1._cfg_ipv6_da1()
            return allFields

    class _upen_cfg_ip6_da2(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp ipv6 da2"
            
        def width(self):
            return 128
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00003010
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ipv6_da2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ipv6_da2"
            
            def description(self):
                return "IPv6 DA2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ipv6_da2"] = _AF6CCI0012_RD_PTP._upen_cfg_ip6_da2._cfg_ipv6_da2()
            return allFields

    class _upen_cfg_ip6_da3(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp ipv6 da3"
            
        def width(self):
            return 128
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00003011
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ipv6_da3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ipv6_da3"
            
            def description(self):
                return "IPv6 DA3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ipv6_da3"] = _AF6CCI0012_RD_PTP._upen_cfg_ip6_da3._cfg_ipv6_da3()
            return allFields

    class _upen_cfg_ip6_da4(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp ipv6 da4"
            
        def width(self):
            return 128
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00003012
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ipv6_da4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ipv6_da4"
            
            def description(self):
                return "IPv6 DA4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ipv6_da4"] = _AF6CCI0012_RD_PTP._upen_cfg_ip6_da4._cfg_ipv6_da4()
            return allFields

    class _upen_cpuque_stk(AtRegister.AtRegister):
        def name(self):
            return "cpu queue sticky"
    
        def description(self):
            return "sticky of cpu que"
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000234
            
        def endAddress(self):
            return 0xffffffff

        class _cpuque_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cpuque_stk"
            
            def description(self):
                return "sticky of cpu queue"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cpuque_stk"] = _AF6CCI0012_RD_PTP._upen_cpuque_stk._cpuque_stk()
            return allFields

    class _upen_t1t3mode(AtRegister.AtRegister):
        def name(self):
            return "Config t1t3mode"
    
        def description(self):
            return "Used to config t1t3mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000017
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_t1t3mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_t1t3mode"
            
            def description(self):
                return "0: send timestamp to cpu queue 1: send back ptp packet mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_t1t3mode"] = _AF6CCI0012_RD_PTP._upen_t1t3mode._cfg_t1t3mode()
            return allFields

    class _upen_cpu_flush(AtRegister.AtRegister):
        def name(self):
            return "Config cpu flush"
    
        def description(self):
            return "Used to config cpu flush"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000018
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_cpu_flush(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_cpu_flush"
            
            def description(self):
                return "cpu flush"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_cpu_flush"] = _AF6CCI0012_RD_PTP._upen_cpu_flush._cfg_cpu_flush()
            return allFields

    class _upen_cpuque(AtRegister.AtRegister):
        def name(self):
            return "Read CPU Queue"
    
        def description(self):
            return "Used to read CPU queue"
            
        def width(self):
            return 128
        
        def type(self):
            return "status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000016
            
        def endAddress(self):
            return 0xffffffff

        class _typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 101
                
            def startBit(self):
                return 100
        
            def name(self):
                return "typ"
            
            def description(self):
                return "typ of ptp packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _seqid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 99
                
            def startBit(self):
                return 84
        
            def name(self):
                return "seqid"
            
            def description(self):
                return "sequence of packet from sequenceId field"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _egr_pid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 83
                
            def startBit(self):
                return 80
        
            def name(self):
                return "egr_pid"
            
            def description(self):
                return "egress port id"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _egr_tmr_tod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 79
                
            def startBit(self):
                return 0
        
            def name(self):
                return "egr_tmr_tod"
            
            def description(self):
                return "egress timer tod"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["typ"] = _AF6CCI0012_RD_PTP._upen_cpuque._typ()
            allFields["seqid"] = _AF6CCI0012_RD_PTP._upen_cpuque._seqid()
            allFields["egr_pid"] = _AF6CCI0012_RD_PTP._upen_cpuque._egr_pid()
            allFields["egr_tmr_tod"] = _AF6CCI0012_RD_PTP._upen_cpuque._egr_tmr_tod()
            return allFields

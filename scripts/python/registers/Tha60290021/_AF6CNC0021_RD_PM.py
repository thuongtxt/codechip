import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0021_RD_PM(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["FMPM_Block_Version"] = _AF6CNC0021_RD_PM._FMPM_Block_Version()
        allRegisters["FMPM_Read_DDR_Control"] = _AF6CNC0021_RD_PM._FMPM_Read_DDR_Control()
        allRegisters["FMPM_Change_Page_DDR"] = _AF6CNC0021_RD_PM._FMPM_Change_Page_DDR()
        allRegisters["FMPM_Pariry_Force"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Force()
        allRegisters["FMPM_Pariry_Disable"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Disable()
        allRegisters["FMPM_Pariry_Sticky"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Sticky()
        allRegisters["FMPM_Force_Reset_Core"] = _AF6CNC0021_RD_PM._FMPM_Force_Reset_Core()
        allRegisters["FMPM_Config_Number_Clock_Of_125us"] = _AF6CNC0021_RD_PM._FMPM_Config_Number_Clock_Of_125us()
        allRegisters["FMPM_Syn_Get_time"] = _AF6CNC0021_RD_PM._FMPM_Syn_Get_time()
        allRegisters["FMPM_Time_Value"] = _AF6CNC0021_RD_PM._FMPM_Time_Value()
        allRegisters["FMPM_Monitor_Timer"] = _AF6CNC0021_RD_PM._FMPM_Monitor_Timer()
        allRegisters["FMPM_Hold00_Data_Of_Read_DDR"] = _AF6CNC0021_RD_PM._FMPM_Hold00_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold01_Data_Of_Read_DDR"] = _AF6CNC0021_RD_PM._FMPM_Hold01_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold02_Data_Of_Read_DDR"] = _AF6CNC0021_RD_PM._FMPM_Hold02_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold03_Data_Of_Read_DDR"] = _AF6CNC0021_RD_PM._FMPM_Hold03_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold04_Data_Of_Read_DDR"] = _AF6CNC0021_RD_PM._FMPM_Hold04_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold05_Data_Of_Read_DDR"] = _AF6CNC0021_RD_PM._FMPM_Hold05_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold06_Data_Of_Read_DDR"] = _AF6CNC0021_RD_PM._FMPM_Hold06_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold07_Data_Of_Read_DDR"] = _AF6CNC0021_RD_PM._FMPM_Hold07_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold08_Data_Of_Read_DDR"] = _AF6CNC0021_RD_PM._FMPM_Hold08_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold09_Data_Of_Read_DDR"] = _AF6CNC0021_RD_PM._FMPM_Hold09_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold10_Data_Of_Read_DDR"] = _AF6CNC0021_RD_PM._FMPM_Hold10_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold11_Data_Of_Read_DDR"] = _AF6CNC0021_RD_PM._FMPM_Hold11_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold12_Data_Of_Read_DDR"] = _AF6CNC0021_RD_PM._FMPM_Hold12_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold13_Data_Of_Read_DDR"] = _AF6CNC0021_RD_PM._FMPM_Hold13_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold14_Data_Of_Read_DDR"] = _AF6CNC0021_RD_PM._FMPM_Hold14_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold15_Data_Of_Read_DDR"] = _AF6CNC0021_RD_PM._FMPM_Hold15_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold16_Data_Of_Read_DDR"] = _AF6CNC0021_RD_PM._FMPM_Hold16_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold17_Data_Of_Read_DDR"] = _AF6CNC0021_RD_PM._FMPM_Hold17_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold18_Data_Of_Read_DDR"] = _AF6CNC0021_RD_PM._FMPM_Hold18_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold19_Data_Of_Read_DDR"] = _AF6CNC0021_RD_PM._FMPM_Hold19_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold20_Data_Of_Read_DDR"] = _AF6CNC0021_RD_PM._FMPM_Hold20_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold21_Data_Of_Read_DDR"] = _AF6CNC0021_RD_PM._FMPM_Hold21_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold22_Data_Of_Read_DDR"] = _AF6CNC0021_RD_PM._FMPM_Hold22_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold23_Data_Of_Read_DDR"] = _AF6CNC0021_RD_PM._FMPM_Hold23_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold24_Data_Of_Read_DDR"] = _AF6CNC0021_RD_PM._FMPM_Hold24_Data_Of_Read_DDR()
        allRegisters["FMPM_K_threshold_of_Section"] = _AF6CNC0021_RD_PM._FMPM_K_threshold_of_Section()
        allRegisters["FMPM_K_threshold_of_STS"] = _AF6CNC0021_RD_PM._FMPM_K_threshold_of_STS()
        allRegisters["FMPM_K_Threshold_of_VT"] = _AF6CNC0021_RD_PM._FMPM_K_Threshold_of_VT()
        allRegisters["FMPM_K_Threshold_of_DS3"] = _AF6CNC0021_RD_PM._FMPM_K_Threshold_of_DS3()
        allRegisters["FMPM_K_Threshold_of_DS1"] = _AF6CNC0021_RD_PM._FMPM_K_Threshold_of_DS1()
        allRegisters["FMPM_K_Threshold_of_PW"] = _AF6CNC0021_RD_PM._FMPM_K_Threshold_of_PW()
        allRegisters["FMPM_TCA_threshold_of_Section_Part0"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_Section_Part0()
        allRegisters["FMPM_TCA_threshold_of_Section_Part1"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_Section_Part1()
        allRegisters["FMPM_TCA_threshold_of_Section_Part2"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_Section_Part2()
        allRegisters["FMPM_TCA_threshold_of_Section_Part3"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_Section_Part3()
        allRegisters["FMPM_TCA_threshold_of_Section_Part4"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_Section_Part4()
        allRegisters["FMPM_TCA_threshold_of_Section_Part5"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_Section_Part5()
        allRegisters["FMPM_TCA_threshold_of_Section_Part6"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_Section_Part6()
        allRegisters["FMPM_TCA_threshold_of_HO_Part0"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_HO_Part0()
        allRegisters["FMPM_TCA_threshold_of_HO_Part1"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_HO_Part1()
        allRegisters["FMPM_TCA_threshold_of_HO_Part2"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_HO_Part2()
        allRegisters["FMPM_TCA_threshold_of_HO_Part3"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_HO_Part3()
        allRegisters["FMPM_TCA_threshold_of_HO_Part4"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_HO_Part4()
        allRegisters["FMPM_TCA_threshold_of_HO_Part5"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_HO_Part5()
        allRegisters["FMPM_TCA_threshold_of_HO_Part6"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_HO_Part6()
        allRegisters["FMPM_TCA_threshold_of_HO_Part7"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_HO_Part7()
        allRegisters["FMPM_TCA_threshold_of_HO_Part8"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_HO_Part8()
        allRegisters["FMPM_TCA_threshold_of_HO_Part9"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_HO_Part9()
        allRegisters["FMPM_TCA_threshold_of_HO_Part10"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_HO_Part10()
        allRegisters["FMPM_TCA_threshold_of_LO_Part0"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_LO_Part0()
        allRegisters["FMPM_TCA_threshold_of_LO_Part1"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_LO_Part1()
        allRegisters["FMPM_TCA_threshold_of_LO_Part2"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_LO_Part2()
        allRegisters["FMPM_TCA_threshold_of_LO_Part3"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_LO_Part3()
        allRegisters["FMPM_TCA_threshold_of_LO_Part4"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_LO_Part4()
        allRegisters["FMPM_TCA_threshold_of_LO_Part5"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_LO_Part5()
        allRegisters["FMPM_TCA_threshold_of_LO_Part6"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_LO_Part6()
        allRegisters["FMPM_TCA_threshold_of_LO_Part7"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_LO_Part7()
        allRegisters["FMPM_TCA_threshold_of_LO_Part8"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_LO_Part8()
        allRegisters["FMPM_TCA_threshold_of_LO_Part9"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_LO_Part9()
        allRegisters["FMPM_TCA_threshold_of_LO_Part10"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_LO_Part10()
        allRegisters["FMPM_TCA_threshold_of_DS3_Part0"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part0()
        allRegisters["FMPM_TCA_threshold_of_DS3_Part1"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part1()
        allRegisters["FMPM_TCA_threshold_of_DS3_Part2"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part2()
        allRegisters["FMPM_TCA_threshold_of_DS3_Part3"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part3()
        allRegisters["FMPM_TCA_threshold_of_DS3_Part4"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part4()
        allRegisters["FMPM_TCA_threshold_of_DS3_Part5"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part5()
        allRegisters["FMPM_TCA_threshold_of_DS3_Part6"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part6()
        allRegisters["FMPM_TCA_threshold_of_DS3_Part7"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part7()
        allRegisters["FMPM_TCA_threshold_of_DS3_Part8"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part8()
        allRegisters["FMPM_TCA_threshold_of_DS3_Part9"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part9()
        allRegisters["FMPM_TCA_threshold_of_DS3_Part10"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part10()
        allRegisters["FMPM_TCA_threshold_of_DS3_Part11"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part11()
        allRegisters["FMPM_TCA_threshold_of_DS3_Part12"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part12()
        allRegisters["FMPM_TCA_threshold_of_DS1_Part0"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS1_Part0()
        allRegisters["FMPM_TCA_threshold_of_DS1_Part1"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS1_Part1()
        allRegisters["FMPM_TCA_threshold_of_DS1_Part2"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS1_Part2()
        allRegisters["FMPM_TCA_threshold_of_DS1_Part3"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS1_Part3()
        allRegisters["FMPM_TCA_threshold_of_DS1_Part4"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS1_Part4()
        allRegisters["FMPM_TCA_threshold_of_DS1_Part5"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS1_Part5()
        allRegisters["FMPM_TCA_threshold_of_DS1_Part6"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS1_Part6()
        allRegisters["FMPM_TCA_threshold_of_DS1_Part7"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS1_Part7()
        allRegisters["FMPM_TCA_threshold_of_PW_Part0"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_PW_Part0()
        allRegisters["FMPM_TCA_threshold_of_PW_Part1"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_PW_Part1()
        allRegisters["FMPM_TCA_threshold_of_PW_Part2"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_PW_Part2()
        allRegisters["FMPM_Enable_PM_Collection_Section"] = _AF6CNC0021_RD_PM._FMPM_Enable_PM_Collection_Section()
        allRegisters["FMPM_Enable_PM_Collection_STS"] = _AF6CNC0021_RD_PM._FMPM_Enable_PM_Collection_STS()
        allRegisters["FMPM_Enable_PM_Collection_DS3"] = _AF6CNC0021_RD_PM._FMPM_Enable_PM_Collection_DS3()
        allRegisters["FMPM_Enable_PM_Collection_VT"] = _AF6CNC0021_RD_PM._FMPM_Enable_PM_Collection_VT()
        allRegisters["FMPM_Enable_PM_Collection_DS1"] = _AF6CNC0021_RD_PM._FMPM_Enable_PM_Collection_DS1()
        allRegisters["FMPM_Enable_PM_Collection_PW"] = _AF6CNC0021_RD_PM._FMPM_Enable_PM_Collection_PW()
        allRegisters["FMPM_Threshold_Type_of_Section"] = _AF6CNC0021_RD_PM._FMPM_Threshold_Type_of_Section()
        allRegisters["FMPM_Threshold_Type_of_STS"] = _AF6CNC0021_RD_PM._FMPM_Threshold_Type_of_STS()
        allRegisters["FMPM_Threshold_Type_of_DS3"] = _AF6CNC0021_RD_PM._FMPM_Threshold_Type_of_DS3()
        allRegisters["FMPM_Threshold_Type_of_VT"] = _AF6CNC0021_RD_PM._FMPM_Threshold_Type_of_VT()
        allRegisters["FMPM_Threshold_Type_of_DS1"] = _AF6CNC0021_RD_PM._FMPM_Threshold_Type_of_DS1()
        allRegisters["FMPM_Threshold_Type_of_PW"] = _AF6CNC0021_RD_PM._FMPM_Threshold_Type_of_PW()
        allRegisters["FMPM_FM_Interrupt"] = _AF6CNC0021_RD_PM._FMPM_FM_Interrupt()
        allRegisters["FMPM_FM_Interrupt_Mask"] = _AF6CNC0021_RD_PM._FMPM_FM_Interrupt_Mask()
        allRegisters["FMPM_FM_EC1_Interrupt_OR"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1_Interrupt_OR()
        allRegisters["FMPM_FM_EC1_Interrupt_OR_AND_MASK_Per_STS1"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1_Interrupt_OR_AND_MASK_Per_STS1()
        allRegisters["FMPM_FM_EC1_Interrupt_MASK_Per_STS1"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1_Interrupt_MASK_Per_STS1()
        allRegisters["FMPM_FM_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1()
        allRegisters["FMPM_FM_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1()
        allRegisters["FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1()
        allRegisters["FMPM_FM_STS24_Interrupt_OR"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_OR()
        allRegisters["FMPM_FM_STS24_Interrupt_OR_AND_MASK_Per_STS1"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_OR_AND_MASK_Per_STS1()
        allRegisters["FMPM_FM_STS24_Interrupt_MASK_Per_STS1"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_MASK_Per_STS1()
        allRegisters["FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1()
        allRegisters["FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1()
        allRegisters["FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1()
        allRegisters["FMPM_FM_DS3E3_Group_Interrupt_OR"] = _AF6CNC0021_RD_PM._FMPM_FM_DS3E3_Group_Interrupt_OR()
        allRegisters["FMPM_FM_DS3E3_Framer_Interrupt_OR_AND_MASK_Per_DS3E3"] = _AF6CNC0021_RD_PM._FMPM_FM_DS3E3_Framer_Interrupt_OR_AND_MASK_Per_DS3E3()
        allRegisters["FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_DS3E3"] = _AF6CNC0021_RD_PM._FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_DS3E3()
        allRegisters["FMPM_FM_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3"] = _AF6CNC0021_RD_PM._FMPM_FM_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3()
        allRegisters["FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3"] = _AF6CNC0021_RD_PM._FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3()
        allRegisters["FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3"] = _AF6CNC0021_RD_PM._FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3()
        allRegisters["FMPM_FM_STS24_LO_Interrupt_OR"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_LO_Interrupt_OR()
        allRegisters["FMPM_FM_STS1_LO_Interrupt_OR"] = _AF6CNC0021_RD_PM._FMPM_FM_STS1_LO_Interrupt_OR()
        allRegisters["FMPM_FM_VTTU_Interrupt_OR_AND_MASK_Per_VTTU"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_OR_AND_MASK_Per_VTTU()
        allRegisters["FMPM_FM_VTTU_Interrupt_MASK_Per_VTTU"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_MASK_Per_VTTU()
        allRegisters["FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU()
        allRegisters["FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU()
        allRegisters["FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU()
        allRegisters["FMPM_FM_DS1E1_Level1_Group_Interrupt_OR"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Level1_Group_Interrupt_OR()
        allRegisters["FMPM_FM_DS1E1_Level2_Group_Interrupt_OR"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Level2_Group_Interrupt_OR()
        allRegisters["FMPM_FM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1()
        allRegisters["FMPM_FM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1()
        allRegisters["FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1()
        allRegisters["FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1()
        allRegisters["FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1()
        allRegisters["FMPM_FM_PW_Level1_Group_Interrupt_OR"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Level1_Group_Interrupt_OR()
        allRegisters["FMPM_FM_PW_Level2_Group_Interrupt_OR"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Level2_Group_Interrupt_OR()
        allRegisters["FMPM_FM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW()
        allRegisters["FMPM_FM_PW_Framer_Interrupt_MASK_Per_PW"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Framer_Interrupt_MASK_Per_PW()
        allRegisters["FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW()
        allRegisters["FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW()
        allRegisters["FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW()
        allRegisters["FMPM_TCA_Interrupt"] = _AF6CNC0021_RD_PM._FMPM_TCA_Interrupt()
        allRegisters["FMPM_TCA_Interrupt_Mask"] = _AF6CNC0021_RD_PM._FMPM_TCA_Interrupt_Mask()
        allRegisters["FMPM_TCA_EC1_Interrupt_OR"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1_Interrupt_OR()
        allRegisters["FMPM_TCA_EC1_Interrupt_OR_AND_MASK_Per_STS1"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1_Interrupt_OR_AND_MASK_Per_STS1()
        allRegisters["FMPM_TCA_EC1_Interrupt_MASK_Per_STS1"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1_Interrupt_MASK_Per_STS1()
        allRegisters["FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1()
        allRegisters["FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1()
        allRegisters["FMPM_TCA_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1()
        allRegisters["FMPM_TCA_STS24_Interrupt_OR"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_OR()
        allRegisters["FMPM_TCA_STS24_Interrupt_OR_AND_MASK_Per_STS1"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_OR_AND_MASK_Per_STS1()
        allRegisters["FMPM_TCA_STS24_Interrupt_MASK_Per_STS1"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_MASK_Per_STS1()
        allRegisters["FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1()
        allRegisters["FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1()
        allRegisters["FMPM_TCA_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1()
        allRegisters["FMPM_TCA_DS3E3_Group_Interrupt_OR"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Group_Interrupt_OR()
        allRegisters["FMPM_TCA_DS3E3_Framer_Interrupt_OR_AND_MASK_Per_DS3E3"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_OR_AND_MASK_Per_DS3E3()
        allRegisters["FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_DS3E3"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_DS3E3()
        allRegisters["FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3()
        allRegisters["FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3()
        allRegisters["FMPM_TCA_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3()
        allRegisters["FMPM_TCA_STS24_LO_Interrupt_OR"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_LO_Interrupt_OR()
        allRegisters["FMPM_TCA_STS1_LO_Interrupt_OR"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS1_LO_Interrupt_OR()
        allRegisters["FMPM_TCA_VTTU_Interrupt_OR_AND_MASK_Per_VTTU"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_OR_AND_MASK_Per_VTTU()
        allRegisters["FMPM_TCA_VTTU_Interrupt_MASK_Per_VTTU"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_MASK_Per_VTTU()
        allRegisters["FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU()
        allRegisters["FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU()
        allRegisters["FMPM_TCA_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU()
        allRegisters["FMPM_TCA_DS1E1_Level1_Group_Interrupt_OR"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Level1_Group_Interrupt_OR()
        allRegisters["FMPM_TCA_DS1E1_Level2_Group_Interrupt_OR"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Level2_Group_Interrupt_OR()
        allRegisters["FMPM_TCA_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1()
        allRegisters["FMPM_TCA_DS1E1_Framer_Interrupt_MASK_Per_DS1E1"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Framer_Interrupt_MASK_Per_DS1E1()
        allRegisters["FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1()
        allRegisters["FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1()
        allRegisters["FMPM_TCA_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1()
        allRegisters["FMPM_TCA_PW_Level1_Group_Interrupt_OR"] = _AF6CNC0021_RD_PM._FMPM_TCA_PW_Level1_Group_Interrupt_OR()
        allRegisters["FMPM_TCA_PW_Level2_Group_Interrupt_OR"] = _AF6CNC0021_RD_PM._FMPM_TCA_PW_Level2_Group_Interrupt_OR()
        allRegisters["FMPM_TCA_PW_Framer_Interrupt_OR_AND_MASK_Per_PW"] = _AF6CNC0021_RD_PM._FMPM_TCA_PW_Framer_Interrupt_OR_AND_MASK_Per_PW()
        allRegisters["FMPM_TCA_PW_Framer_Interrupt_MASK_Per_PW"] = _AF6CNC0021_RD_PM._FMPM_TCA_PW_Framer_Interrupt_MASK_Per_PW()
        allRegisters["FMPM_TCA_PW_Interrupt_Sticky_Per_Type_Per_PW"] = _AF6CNC0021_RD_PM._FMPM_TCA_PW_Interrupt_Sticky_Per_Type_Per_PW()
        allRegisters["FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW"] = _AF6CNC0021_RD_PM._FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW()
        allRegisters["FMPM_TCA_PW_Interrupt_Current_Status_Per_Type_Per_PW"] = _AF6CNC0021_RD_PM._FMPM_TCA_PW_Interrupt_Current_Status_Per_Type_Per_PW()
        return allRegisters

    class _FMPM_Block_Version(AtRegister.AtRegister):
        def name(self):
            return "FMPM Block Version"
    
        def description(self):
            return "PM Block Version"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _day(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "day"
            
            def description(self):
                return "day, hexdecimal format"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _month(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "month"
            
            def description(self):
                return "month, hexdecimal format"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _year(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "year"
            
            def description(self):
                return "year, hexdecimal format"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _project(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "project"
            
            def description(self):
                return "Project ID, hexdecimal format"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _number(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "number"
            
            def description(self):
                return "number of synthesis FPGA each day, hexdecimal format"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["day"] = _AF6CNC0021_RD_PM._FMPM_Block_Version._day()
            allFields["month"] = _AF6CNC0021_RD_PM._FMPM_Block_Version._month()
            allFields["year"] = _AF6CNC0021_RD_PM._FMPM_Block_Version._year()
            allFields["project"] = _AF6CNC0021_RD_PM._FMPM_Block_Version._project()
            allFields["number"] = _AF6CNC0021_RD_PM._FMPM_Block_Version._number()
            return allFields

    class _FMPM_Read_DDR_Control(AtRegister.AtRegister):
        def name(self):
            return "FMPM Read DDR Control"
    
        def description(self):
            return "Read Parameters or Statistic counters of DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000001
            
        def endAddress(self):
            return 0xffffffff

        class _Done(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "Done"
            
            def description(self):
                return "Access DDR Is Done"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _Start(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "Start"
            
            def description(self):
                return "Trigger 0->1 to start access DDR"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Page(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "Page"
            
            def description(self):
                return "Page access"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _R2C(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "R2C"
            
            def description(self):
                return "Read mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 20
        
            def name(self):
                return "Type"
            
            def description(self):
                return "Counter Type"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _ADR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ADR"
            
            def description(self):
                return "Address of Counter"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Done"] = _AF6CNC0021_RD_PM._FMPM_Read_DDR_Control._Done()
            allFields["Start"] = _AF6CNC0021_RD_PM._FMPM_Read_DDR_Control._Start()
            allFields["Page"] = _AF6CNC0021_RD_PM._FMPM_Read_DDR_Control._Page()
            allFields["R2C"] = _AF6CNC0021_RD_PM._FMPM_Read_DDR_Control._R2C()
            allFields["Type"] = _AF6CNC0021_RD_PM._FMPM_Read_DDR_Control._Type()
            allFields["ADR"] = _AF6CNC0021_RD_PM._FMPM_Read_DDR_Control._ADR()
            return allFields

    class _FMPM_Change_Page_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Change Page DDR"
    
        def description(self):
            return "Change Page for DDR when SW want to get Current Period Parameters"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0xffffffff

        class _CHG_Page_enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "CHG_Page_enb"
            
            def description(self):
                return "Enable change page"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _HW_Page(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "HW_Page"
            
            def description(self):
                return "Get Current Page of DDR"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _CHG_DONE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "CHG_DONE"
            
            def description(self):
                return "Change page is done"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _SEL_CHG_PAGE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "SEL_CHG_PAGE"
            
            def description(self):
                return "Select change page mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _CHG_START(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CHG_START"
            
            def description(self):
                return "Trigger 0->1 to request change page, this bit is available when bit SEL_CHG_PAGE is 0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CHG_Page_enb"] = _AF6CNC0021_RD_PM._FMPM_Change_Page_DDR._CHG_Page_enb()
            allFields["HW_Page"] = _AF6CNC0021_RD_PM._FMPM_Change_Page_DDR._HW_Page()
            allFields["CHG_DONE"] = _AF6CNC0021_RD_PM._FMPM_Change_Page_DDR._CHG_DONE()
            allFields["SEL_CHG_PAGE"] = _AF6CNC0021_RD_PM._FMPM_Change_Page_DDR._SEL_CHG_PAGE()
            allFields["CHG_START"] = _AF6CNC0021_RD_PM._FMPM_Change_Page_DDR._CHG_START()
            return allFields

    class _FMPM_Pariry_Force(AtRegister.AtRegister):
        def name(self):
            return "FMPM Parity Force"
    
        def description(self):
            return "Force parity for RAM configuration"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000060
            
        def endAddress(self):
            return 0xffffffff

        class _Par_For_pw_def_msk_chn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "Par_For_pw_def_msk_chn"
            
            def description(self):
                return "FMPM PW Def Framer Interrupt MASK Per PW"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_pw_def_msk_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "Par_For_pw_def_msk_typ"
            
            def description(self):
                return "FMPM PW Def Interrupt MASK Per Type Per PW"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_sts_msk_chn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "Par_For_sts_msk_chn"
            
            def description(self):
                return "FMPM FM STS24 Interrupt MASK Per STS1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_sts_msk_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "Par_For_sts_msk_typ"
            
            def description(self):
                return "FMPM FM STS24 Interrupt MASK Per Type Of Per STS1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_vt_msk_chn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "Par_For_vt_msk_chn"
            
            def description(self):
                return "FMPM FM VTTU Interrupt MASK Per VTTU"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_vt_msk_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Par_For_vt_msk_typ"
            
            def description(self):
                return "FMPM FM VTTU Interrupt MASK Per Type Per VTTU"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_ds3_msk_chn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "Par_For_ds3_msk_chn"
            
            def description(self):
                return "FMPM FM DS3E3 Framer Interrupt MASK Per DS3E3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_ds3_msk_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "Par_For_ds3_msk_typ"
            
            def description(self):
                return "FMPM FM DS3E3 Framer Interrupt MASK Per Type of Per DS3E3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_ds1_msk_chn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "Par_For_ds1_msk_chn"
            
            def description(self):
                return "FMPM FM DS1E1 Interrupt MASK Per Type Per DS1E1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_ds1_msk_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Par_For_ds1_msk_typ"
            
            def description(self):
                return "FMPM FM DS1E1 Interrupt MASK Per Type Per DS1E1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_pw_msk_chn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "Par_For_pw_msk_chn"
            
            def description(self):
                return "FMPM FM PW Framer Interrupt MASK Per PW"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_pw_msk_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "Par_For_pw_msk_typ"
            
            def description(self):
                return "FMPM FM PW Interrupt MASK Per Type Per PW"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_config_sts_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "Par_For_config_sts_typ"
            
            def description(self):
                return "FMPM Threshold Type of STS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_config_vt_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Par_For_config_vt_typ"
            
            def description(self):
                return "FMPM Threshold Type of VT"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_config_ds3_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "Par_For_config_ds3_typ"
            
            def description(self):
                return "FMPM Threshold Type of DS3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_config_ds1_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Par_For_config_ds1_typ"
            
            def description(self):
                return "FMPM Threshold Type of DS1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_config_pw_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Par_For_config_pw_typ"
            
            def description(self):
                return "FMPM Threshold Type of PW"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_config_sts_k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Par_For_config_sts_k"
            
            def description(self):
                return "FMPM K threshold of STS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_config_vt_k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "Par_For_config_vt_k"
            
            def description(self):
                return "FMPM K Threshold of VT"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_config_ds3_k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Par_For_config_ds3_k"
            
            def description(self):
                return "FMPM K Threshold of DS3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_config_ds1_k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Par_For_config_ds1_k"
            
            def description(self):
                return "FMPM K Threshold of DS1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_config_pw_k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Par_For_config_pw_k"
            
            def description(self):
                return "FMPM K Threshold of PW"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Par_For_pw_def_msk_chn"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Force._Par_For_pw_def_msk_chn()
            allFields["Par_For_pw_def_msk_typ"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Force._Par_For_pw_def_msk_typ()
            allFields["Par_For_sts_msk_chn"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Force._Par_For_sts_msk_chn()
            allFields["Par_For_sts_msk_typ"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Force._Par_For_sts_msk_typ()
            allFields["Par_For_vt_msk_chn"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Force._Par_For_vt_msk_chn()
            allFields["Par_For_vt_msk_typ"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Force._Par_For_vt_msk_typ()
            allFields["Par_For_ds3_msk_chn"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Force._Par_For_ds3_msk_chn()
            allFields["Par_For_ds3_msk_typ"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Force._Par_For_ds3_msk_typ()
            allFields["Par_For_ds1_msk_chn"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Force._Par_For_ds1_msk_chn()
            allFields["Par_For_ds1_msk_typ"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Force._Par_For_ds1_msk_typ()
            allFields["Par_For_pw_msk_chn"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Force._Par_For_pw_msk_chn()
            allFields["Par_For_pw_msk_typ"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Force._Par_For_pw_msk_typ()
            allFields["Par_For_config_sts_typ"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Force._Par_For_config_sts_typ()
            allFields["Par_For_config_vt_typ"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Force._Par_For_config_vt_typ()
            allFields["Par_For_config_ds3_typ"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Force._Par_For_config_ds3_typ()
            allFields["Par_For_config_ds1_typ"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Force._Par_For_config_ds1_typ()
            allFields["Par_For_config_pw_typ"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Force._Par_For_config_pw_typ()
            allFields["Par_For_config_sts_k"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Force._Par_For_config_sts_k()
            allFields["Par_For_config_vt_k"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Force._Par_For_config_vt_k()
            allFields["Par_For_config_ds3_k"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Force._Par_For_config_ds3_k()
            allFields["Par_For_config_ds1_k"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Force._Par_For_config_ds1_k()
            allFields["Par_For_config_pw_k"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Force._Par_For_config_pw_k()
            return allFields

    class _FMPM_Pariry_Disable(AtRegister.AtRegister):
        def name(self):
            return "FMPM Parity Disable"
    
        def description(self):
            return "Force parity for RAM configuration"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000061
            
        def endAddress(self):
            return 0xffffffff

        class _Par_For_sts_msk_chn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "Par_For_sts_msk_chn"
            
            def description(self):
                return "FMPM FM STS24 Interrupt MASK Per STS1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_sts_msk_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "Par_For_sts_msk_typ"
            
            def description(self):
                return "FMPM FM STS24 Interrupt MASK Per Type Of Per STS1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_vt_msk_chn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "Par_For_vt_msk_chn"
            
            def description(self):
                return "FMPM FM VTTU Interrupt MASK Per VTTU"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_vt_msk_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Par_For_vt_msk_typ"
            
            def description(self):
                return "FMPM FM VTTU Interrupt MASK Per Type Per VTTU"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_ds3_msk_chn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "Par_For_ds3_msk_chn"
            
            def description(self):
                return "FMPM FM DS3E3 Framer Interrupt MASK Per DS3E3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_ds3_msk_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "Par_For_ds3_msk_typ"
            
            def description(self):
                return "FMPM FM DS3E3 Framer Interrupt MASK Per Type of Per DS3E3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_ds1_msk_chn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "Par_For_ds1_msk_chn"
            
            def description(self):
                return "FMPM FM DS1E1 Interrupt MASK Per Type Per DS1E1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_ds1_msk_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Par_For_ds1_msk_typ"
            
            def description(self):
                return "FMPM FM DS1E1 Interrupt MASK Per Type Per DS1E1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_pw_msk_chn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "Par_For_pw_msk_chn"
            
            def description(self):
                return "FMPM FM PW Framer Interrupt MASK Per PW"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_pw_msk_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "Par_For_pw_msk_typ"
            
            def description(self):
                return "FMPM FM PW Interrupt MASK Per Type Per PW"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_config_sts_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "Par_For_config_sts_typ"
            
            def description(self):
                return "FMPM Threshold Type of STS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_config_vt_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Par_For_config_vt_typ"
            
            def description(self):
                return "FMPM Threshold Type of VT"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_config_ds3_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "Par_For_config_ds3_typ"
            
            def description(self):
                return "FMPM Threshold Type of DS3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_config_ds1_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Par_For_config_ds1_typ"
            
            def description(self):
                return "FMPM Threshold Type of DS1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_config_pw_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Par_For_config_pw_typ"
            
            def description(self):
                return "FMPM Threshold Type of PW"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_config_sts_k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Par_For_config_sts_k"
            
            def description(self):
                return "FMPM K threshold of STS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_config_vt_k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "Par_For_config_vt_k"
            
            def description(self):
                return "FMPM K Threshold of VT"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_config_ds3_k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Par_For_config_ds3_k"
            
            def description(self):
                return "FMPM K Threshold of DS3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_config_ds1_k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Par_For_config_ds1_k"
            
            def description(self):
                return "FMPM K Threshold of DS1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_config_pw_k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Par_For_config_pw_k"
            
            def description(self):
                return "FMPM K Threshold of PW"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Par_For_sts_msk_chn"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Disable._Par_For_sts_msk_chn()
            allFields["Par_For_sts_msk_typ"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Disable._Par_For_sts_msk_typ()
            allFields["Par_For_vt_msk_chn"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Disable._Par_For_vt_msk_chn()
            allFields["Par_For_vt_msk_typ"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Disable._Par_For_vt_msk_typ()
            allFields["Par_For_ds3_msk_chn"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Disable._Par_For_ds3_msk_chn()
            allFields["Par_For_ds3_msk_typ"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Disable._Par_For_ds3_msk_typ()
            allFields["Par_For_ds1_msk_chn"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Disable._Par_For_ds1_msk_chn()
            allFields["Par_For_ds1_msk_typ"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Disable._Par_For_ds1_msk_typ()
            allFields["Par_For_pw_msk_chn"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Disable._Par_For_pw_msk_chn()
            allFields["Par_For_pw_msk_typ"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Disable._Par_For_pw_msk_typ()
            allFields["Par_For_config_sts_typ"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Disable._Par_For_config_sts_typ()
            allFields["Par_For_config_vt_typ"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Disable._Par_For_config_vt_typ()
            allFields["Par_For_config_ds3_typ"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Disable._Par_For_config_ds3_typ()
            allFields["Par_For_config_ds1_typ"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Disable._Par_For_config_ds1_typ()
            allFields["Par_For_config_pw_typ"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Disable._Par_For_config_pw_typ()
            allFields["Par_For_config_sts_k"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Disable._Par_For_config_sts_k()
            allFields["Par_For_config_vt_k"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Disable._Par_For_config_vt_k()
            allFields["Par_For_config_ds3_k"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Disable._Par_For_config_ds3_k()
            allFields["Par_For_config_ds1_k"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Disable._Par_For_config_ds1_k()
            allFields["Par_For_config_pw_k"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Disable._Par_For_config_pw_k()
            return allFields

    class _FMPM_Pariry_Sticky(AtRegister.AtRegister):
        def name(self):
            return "FMPM Parity Sticky"
    
        def description(self):
            return "Change Page for DDR when SW want to get Current Period Parameters"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000062
            
        def endAddress(self):
            return 0xffffffff

        class _Par_Stk_sts_msk_chn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "Par_Stk_sts_msk_chn"
            
            def description(self):
                return "FMPM FM STS24 Interrupt MASK Per STS1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_Stk_sts_msk_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "Par_Stk_sts_msk_typ"
            
            def description(self):
                return "FMPM FM STS24 Interrupt MASK Per Type Of Per STS1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_Stk_vt_msk_chn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "Par_Stk_vt_msk_chn"
            
            def description(self):
                return "FMPM FM VTTU Interrupt MASK Per VTTU"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_Stk_vt_msk_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Par_Stk_vt_msk_typ"
            
            def description(self):
                return "FMPM FM VTTU Interrupt MASK Per Type Per VTTU"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_Stk_ds3_msk_chn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "Par_Stk_ds3_msk_chn"
            
            def description(self):
                return "FMPM FM DS3E3 Framer Interrupt MASK Per DS3E3"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_Stk_ds3_msk_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "Par_Stk_ds3_msk_typ"
            
            def description(self):
                return "FMPM FM DS3E3 Framer Interrupt MASK Per Type of Per DS3E3"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_Stk_ds1_msk_chn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "Par_Stk_ds1_msk_chn"
            
            def description(self):
                return "FMPM FM DS1E1 Interrupt MASK Per Type Per DS1E1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_Stk_ds1_msk_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Par_Stk_ds1_msk_typ"
            
            def description(self):
                return "FMPM FM DS1E1 Interrupt MASK Per Type Per DS1E1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_Stk_pw_msk_chn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "Par_Stk_pw_msk_chn"
            
            def description(self):
                return "FMPM FM PW Framer Interrupt MASK Per PW"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_Stk_pw_msk_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "Par_Stk_pw_msk_typ"
            
            def description(self):
                return "FMPM FM PW Interrupt MASK Per Type Per PW"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_Stk_config_sts_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "Par_Stk_config_sts_typ"
            
            def description(self):
                return "FMPM Threshold Type of STS"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_Stk_config_vt_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Par_Stk_config_vt_typ"
            
            def description(self):
                return "FMPM Threshold Type of VT"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_Stk_config_ds3_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "Par_Stk_config_ds3_typ"
            
            def description(self):
                return "FMPM Threshold Type of DS3"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_Stk_config_ds1_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Par_Stk_config_ds1_typ"
            
            def description(self):
                return "FMPM Threshold Type of DS1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_Stk_config_pw_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Par_Stk_config_pw_typ"
            
            def description(self):
                return "FMPM Threshold Type of PW"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_Stk_config_sts_k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Par_Stk_config_sts_k"
            
            def description(self):
                return "FMPM K threshold of STS"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_Stk_config_vt_k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "Par_Stk_config_vt_k"
            
            def description(self):
                return "FMPM K Threshold of VT"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_Stk_config_ds3_k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Par_Stk_config_ds3_k"
            
            def description(self):
                return "FMPM K Threshold of DS3"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_Stk_config_ds1_k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Par_Stk_config_ds1_k"
            
            def description(self):
                return "FMPM K Threshold of DS1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_Stk_config_pw_k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Par_Stk_config_pw_k"
            
            def description(self):
                return "FMPM K Threshold of PW"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Par_Stk_sts_msk_chn"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Sticky._Par_Stk_sts_msk_chn()
            allFields["Par_Stk_sts_msk_typ"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Sticky._Par_Stk_sts_msk_typ()
            allFields["Par_Stk_vt_msk_chn"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Sticky._Par_Stk_vt_msk_chn()
            allFields["Par_Stk_vt_msk_typ"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Sticky._Par_Stk_vt_msk_typ()
            allFields["Par_Stk_ds3_msk_chn"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Sticky._Par_Stk_ds3_msk_chn()
            allFields["Par_Stk_ds3_msk_typ"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Sticky._Par_Stk_ds3_msk_typ()
            allFields["Par_Stk_ds1_msk_chn"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Sticky._Par_Stk_ds1_msk_chn()
            allFields["Par_Stk_ds1_msk_typ"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Sticky._Par_Stk_ds1_msk_typ()
            allFields["Par_Stk_pw_msk_chn"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Sticky._Par_Stk_pw_msk_chn()
            allFields["Par_Stk_pw_msk_typ"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Sticky._Par_Stk_pw_msk_typ()
            allFields["Par_Stk_config_sts_typ"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Sticky._Par_Stk_config_sts_typ()
            allFields["Par_Stk_config_vt_typ"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Sticky._Par_Stk_config_vt_typ()
            allFields["Par_Stk_config_ds3_typ"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Sticky._Par_Stk_config_ds3_typ()
            allFields["Par_Stk_config_ds1_typ"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Sticky._Par_Stk_config_ds1_typ()
            allFields["Par_Stk_config_pw_typ"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Sticky._Par_Stk_config_pw_typ()
            allFields["Par_Stk_config_sts_k"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Sticky._Par_Stk_config_sts_k()
            allFields["Par_Stk_config_vt_k"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Sticky._Par_Stk_config_vt_k()
            allFields["Par_Stk_config_ds3_k"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Sticky._Par_Stk_config_ds3_k()
            allFields["Par_Stk_config_ds1_k"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Sticky._Par_Stk_config_ds1_k()
            allFields["Par_Stk_config_pw_k"] = _AF6CNC0021_RD_PM._FMPM_Pariry_Sticky._Par_Stk_config_pw_k()
            return allFields

    class _FMPM_Force_Reset_Core(AtRegister.AtRegister):
        def name(self):
            return "FMPM Force Reset Core"
    
        def description(self):
            return "Force Reset PM Core"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000005
            
        def endAddress(self):
            return 0xffffffff

        class _sel_1s_mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "sel_1s_mode"
            
            def description(self):
                return "Select source of 1s for PM function"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _param_enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "param_enb"
            
            def description(self):
                return "Enable Parameter counters"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _drst_enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "drst_enb"
            
            def description(self):
                return "Enable PM block, SW has to do reset PM block when this bit is transition from 0/1 to 1/0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _drdy_chg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "drdy_chg"
            
            def description(self):
                return "Ready signal has changed 0/1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _drst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "drst"
            
            def description(self):
                return "PM Core reset done"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _frst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "frst"
            
            def description(self):
                return "Trigger 0->1 to reset PM Core"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["sel_1s_mode"] = _AF6CNC0021_RD_PM._FMPM_Force_Reset_Core._sel_1s_mode()
            allFields["param_enb"] = _AF6CNC0021_RD_PM._FMPM_Force_Reset_Core._param_enb()
            allFields["drst_enb"] = _AF6CNC0021_RD_PM._FMPM_Force_Reset_Core._drst_enb()
            allFields["drdy_chg"] = _AF6CNC0021_RD_PM._FMPM_Force_Reset_Core._drdy_chg()
            allFields["drst"] = _AF6CNC0021_RD_PM._FMPM_Force_Reset_Core._drst()
            allFields["frst"] = _AF6CNC0021_RD_PM._FMPM_Force_Reset_Core._frst()
            return allFields

    class _FMPM_Config_Number_Clock_Of_125us(AtRegister.AtRegister):
        def name(self):
            return "FMPM Config Number Clock Of 125us"
    
        def description(self):
            return "Configure number clock of 125us. This register is used by HW only."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000007
            
        def endAddress(self):
            return 0xffffffff

        class _time2p5s(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "time2p5s"
            
            def description(self):
                return "Threshold for Failure set  (Ref 2.5s), step 100ms, SW has to minus 1 before configuring to HW"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _time10s(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "time10s"
            
            def description(self):
                return "Threshold for Failure clear(Ref  10s), step 100ms, SW has to minus 1 before configuring to HW"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _config125us(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "config125us"
            
            def description(self):
                return "Number of clocks for one 125us unit. N(mhz) is operation clock of PM block then the configuration value is (N*125-2)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["time2p5s"] = _AF6CNC0021_RD_PM._FMPM_Config_Number_Clock_Of_125us._time2p5s()
            allFields["time10s"] = _AF6CNC0021_RD_PM._FMPM_Config_Number_Clock_Of_125us._time10s()
            allFields["config125us"] = _AF6CNC0021_RD_PM._FMPM_Config_Number_Clock_Of_125us._config125us()
            return allFields

    class _FMPM_Syn_Get_time(AtRegister.AtRegister):
        def name(self):
            return "FMPM Syn Get Time"
    
        def description(self):
            return "SW request HW synchronize timing."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000008
            
        def endAddress(self):
            return 0xffffffff

        class _syntime(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "syntime"
            
            def description(self):
                return "Trigger 0->1 to synchronize timing"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _gettime(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "gettime"
            
            def description(self):
                return "Trigger 0->1 to get time value"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _numclk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "numclk"
            
            def description(self):
                return "number of clocks for one 125us unit"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["syntime"] = _AF6CNC0021_RD_PM._FMPM_Syn_Get_time._syntime()
            allFields["gettime"] = _AF6CNC0021_RD_PM._FMPM_Syn_Get_time._gettime()
            allFields["numclk"] = _AF6CNC0021_RD_PM._FMPM_Syn_Get_time._numclk()
            return allFields

    class _FMPM_Time_Value(AtRegister.AtRegister):
        def name(self):
            return "FMPM Time Value"
    
        def description(self):
            return "Time value after SW request to get time"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000009
            
        def endAddress(self):
            return 0xffffffff

        class _cnt1s(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 16
        
            def name(self):
                return "cnt1s"
            
            def description(self):
                return "counter 1s"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _cnt1ms(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 4
        
            def name(self):
                return "cnt1ms"
            
            def description(self):
                return "counter 1ms"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _cnt125us(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt125us"
            
            def description(self):
                return "counter 125us"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt1s"] = _AF6CNC0021_RD_PM._FMPM_Time_Value._cnt1s()
            allFields["cnt1ms"] = _AF6CNC0021_RD_PM._FMPM_Time_Value._cnt1ms()
            allFields["cnt125us"] = _AF6CNC0021_RD_PM._FMPM_Time_Value._cnt125us()
            return allFields

    class _FMPM_Monitor_Timer(AtRegister.AtRegister):
        def name(self):
            return "FMPM Monitor Timer"
    
        def description(self):
            return "Time value after SW request to get time"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000001c
            
        def endAddress(self):
            return 0xffffffff

        class _mon125us(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mon125us"
            
            def description(self):
                return "monitor timer of HW, resolution is 125us, counter will be roll-over when it get maximum value"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mon125us"] = _AF6CNC0021_RD_PM._FMPM_Monitor_Timer._mon125us()
            return allFields

    class _FMPM_Hold00_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold00 Data Of Read DDR"
    
        def description(self):
            return "Hold00 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000020
            
        def endAddress(self):
            return 0xffffffff

        class _Hold00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold00"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold00"] = _AF6CNC0021_RD_PM._FMPM_Hold00_Data_Of_Read_DDR._Hold00()
            return allFields

    class _FMPM_Hold01_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold01 Data Of Read DDR"
    
        def description(self):
            return "Hold01 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000021
            
        def endAddress(self):
            return 0xffffffff

        class _Hold01(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold01"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold01"] = _AF6CNC0021_RD_PM._FMPM_Hold01_Data_Of_Read_DDR._Hold01()
            return allFields

    class _FMPM_Hold02_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold02 Data Of Read DDR"
    
        def description(self):
            return "Hold02 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000022
            
        def endAddress(self):
            return 0xffffffff

        class _Hold02(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold02"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold02"] = _AF6CNC0021_RD_PM._FMPM_Hold02_Data_Of_Read_DDR._Hold02()
            return allFields

    class _FMPM_Hold03_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold03 Data Of Read DDR"
    
        def description(self):
            return "Hold03 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000023
            
        def endAddress(self):
            return 0xffffffff

        class _Hold03(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold03"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold03"] = _AF6CNC0021_RD_PM._FMPM_Hold03_Data_Of_Read_DDR._Hold03()
            return allFields

    class _FMPM_Hold04_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold04 Data Of Read DDR"
    
        def description(self):
            return "Hold04 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000024
            
        def endAddress(self):
            return 0xffffffff

        class _Hold04(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold04"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold04"] = _AF6CNC0021_RD_PM._FMPM_Hold04_Data_Of_Read_DDR._Hold04()
            return allFields

    class _FMPM_Hold05_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold05 Data Of Read DDR"
    
        def description(self):
            return "Hold05 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000025
            
        def endAddress(self):
            return 0xffffffff

        class _Hold05(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold05"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold05"] = _AF6CNC0021_RD_PM._FMPM_Hold05_Data_Of_Read_DDR._Hold05()
            return allFields

    class _FMPM_Hold06_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold06 Data Of Read DDR"
    
        def description(self):
            return "Hold06 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000026
            
        def endAddress(self):
            return 0xffffffff

        class _Hold06(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold06"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold06"] = _AF6CNC0021_RD_PM._FMPM_Hold06_Data_Of_Read_DDR._Hold06()
            return allFields

    class _FMPM_Hold07_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold07 Data Of Read DDR"
    
        def description(self):
            return "Hold07 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000027
            
        def endAddress(self):
            return 0xffffffff

        class _Hold07(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold07"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold07"] = _AF6CNC0021_RD_PM._FMPM_Hold07_Data_Of_Read_DDR._Hold07()
            return allFields

    class _FMPM_Hold08_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold08 Data Of Read DDR"
    
        def description(self):
            return "Hold08 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000028
            
        def endAddress(self):
            return 0xffffffff

        class _Hold08(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold08"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold08"] = _AF6CNC0021_RD_PM._FMPM_Hold08_Data_Of_Read_DDR._Hold08()
            return allFields

    class _FMPM_Hold09_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold09 Data Of Read DDR"
    
        def description(self):
            return "Hold09 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000029
            
        def endAddress(self):
            return 0xffffffff

        class _Hold09(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold09"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold09"] = _AF6CNC0021_RD_PM._FMPM_Hold09_Data_Of_Read_DDR._Hold09()
            return allFields

    class _FMPM_Hold10_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold10 Data Of Read DDR"
    
        def description(self):
            return "Hold10 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000002a
            
        def endAddress(self):
            return 0xffffffff

        class _Hold10(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold10"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold10"] = _AF6CNC0021_RD_PM._FMPM_Hold10_Data_Of_Read_DDR._Hold10()
            return allFields

    class _FMPM_Hold11_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold11 Data Of Read DDR"
    
        def description(self):
            return "Hold11 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000002b
            
        def endAddress(self):
            return 0xffffffff

        class _Hold11(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold11"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold11"] = _AF6CNC0021_RD_PM._FMPM_Hold11_Data_Of_Read_DDR._Hold11()
            return allFields

    class _FMPM_Hold12_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold12 Data Of Read DDR"
    
        def description(self):
            return "Hold12 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000002c
            
        def endAddress(self):
            return 0xffffffff

        class _Hold12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold12"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold12"] = _AF6CNC0021_RD_PM._FMPM_Hold12_Data_Of_Read_DDR._Hold12()
            return allFields

    class _FMPM_Hold13_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold13 Data Of Read DDR"
    
        def description(self):
            return "Hold13 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000002d
            
        def endAddress(self):
            return 0xffffffff

        class _Hold13(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold13"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold13"] = _AF6CNC0021_RD_PM._FMPM_Hold13_Data_Of_Read_DDR._Hold13()
            return allFields

    class _FMPM_Hold14_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold14 Data Of Read DDR"
    
        def description(self):
            return "Hold14 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000002e
            
        def endAddress(self):
            return 0xffffffff

        class _Hold14(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold14"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold14"] = _AF6CNC0021_RD_PM._FMPM_Hold14_Data_Of_Read_DDR._Hold14()
            return allFields

    class _FMPM_Hold15_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold15 Data Of Read DDR"
    
        def description(self):
            return "Hold15 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000002f
            
        def endAddress(self):
            return 0xffffffff

        class _Hold15(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold15"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold15"] = _AF6CNC0021_RD_PM._FMPM_Hold15_Data_Of_Read_DDR._Hold15()
            return allFields

    class _FMPM_Hold16_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold16 Data Of Read DDR"
    
        def description(self):
            return "Hold16 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000030
            
        def endAddress(self):
            return 0xffffffff

        class _Hold16(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold16"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold16"] = _AF6CNC0021_RD_PM._FMPM_Hold16_Data_Of_Read_DDR._Hold16()
            return allFields

    class _FMPM_Hold17_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold17 Data Of Read DDR"
    
        def description(self):
            return "Hold17 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000031
            
        def endAddress(self):
            return 0xffffffff

        class _Hold17(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold17"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold17"] = _AF6CNC0021_RD_PM._FMPM_Hold17_Data_Of_Read_DDR._Hold17()
            return allFields

    class _FMPM_Hold18_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold18 Data Of Read DDR"
    
        def description(self):
            return "Hold18 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000032
            
        def endAddress(self):
            return 0xffffffff

        class _Hold18(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold18"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold18"] = _AF6CNC0021_RD_PM._FMPM_Hold18_Data_Of_Read_DDR._Hold18()
            return allFields

    class _FMPM_Hold19_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold19 Data Of Read DDR"
    
        def description(self):
            return "Hold19 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000033
            
        def endAddress(self):
            return 0xffffffff

        class _Hold19(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold19"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold19"] = _AF6CNC0021_RD_PM._FMPM_Hold19_Data_Of_Read_DDR._Hold19()
            return allFields

    class _FMPM_Hold20_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold20 Data Of Read DDR"
    
        def description(self):
            return "Hold20 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000034
            
        def endAddress(self):
            return 0xffffffff

        class _Hold20(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold20"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold20"] = _AF6CNC0021_RD_PM._FMPM_Hold20_Data_Of_Read_DDR._Hold20()
            return allFields

    class _FMPM_Hold21_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold21 Data Of Read DDR"
    
        def description(self):
            return "Hold21 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000035
            
        def endAddress(self):
            return 0xffffffff

        class _Hold21(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold21"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold21"] = _AF6CNC0021_RD_PM._FMPM_Hold21_Data_Of_Read_DDR._Hold21()
            return allFields

    class _FMPM_Hold22_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold22 Data Of Read DDR"
    
        def description(self):
            return "Hold22 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000036
            
        def endAddress(self):
            return 0xffffffff

        class _Hold22(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold22"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold22"] = _AF6CNC0021_RD_PM._FMPM_Hold22_Data_Of_Read_DDR._Hold22()
            return allFields

    class _FMPM_Hold23_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold23 Data Of Read DDR"
    
        def description(self):
            return "Hold23 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000037
            
        def endAddress(self):
            return 0xffffffff

        class _Hold23(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold23"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold23"] = _AF6CNC0021_RD_PM._FMPM_Hold23_Data_Of_Read_DDR._Hold23()
            return allFields

    class _FMPM_Hold24_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold24 Data Of Read DDR"
    
        def description(self):
            return "Hold24 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000038
            
        def endAddress(self):
            return 0xffffffff

        class _Hold24(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold24"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold24"] = _AF6CNC0021_RD_PM._FMPM_Hold24_Data_Of_Read_DDR._Hold24()
            return allFields

    class _FMPM_K_threshold_of_Section(AtRegister.AtRegister):
        def name(self):
            return "FMPM K threshold of Section"
    
        def description(self):
            return "K threshold of Section and Line"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000D0+$id"
            
        def startAddress(self):
            return 0x000000d0
            
        def endAddress(self):
            return 0x000000d7

        class _line_kval(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "line_kval"
            
            def description(self):
                return "K threshold of Line"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _section_kval(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "section_kval"
            
            def description(self):
                return "K threshold of Section"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["line_kval"] = _AF6CNC0021_RD_PM._FMPM_K_threshold_of_Section._line_kval()
            allFields["section_kval"] = _AF6CNC0021_RD_PM._FMPM_K_threshold_of_Section._section_kval()
            return allFields

    class _FMPM_K_threshold_of_STS(AtRegister.AtRegister):
        def name(self):
            return "FMPM K threshold of STS"
    
        def description(self):
            return "K threshold of STS"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00080+$id"
            
        def startAddress(self):
            return 0x00000080
            
        def endAddress(self):
            return 0x00000087

        class _kval(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "kval"
            
            def description(self):
                return "K threshold of STS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["kval"] = _AF6CNC0021_RD_PM._FMPM_K_threshold_of_STS._kval()
            return allFields

    class _FMPM_K_Threshold_of_VT(AtRegister.AtRegister):
        def name(self):
            return "FMPM K Threshold of VT"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00090+$id"
            
        def startAddress(self):
            return 0x00000090
            
        def endAddress(self):
            return 0x00000097

        class _kval(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "kval"
            
            def description(self):
                return "K threshold of VT"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["kval"] = _AF6CNC0021_RD_PM._FMPM_K_Threshold_of_VT._kval()
            return allFields

    class _FMPM_K_Threshold_of_DS3(AtRegister.AtRegister):
        def name(self):
            return "FMPM K Threshold of DS3"
    
        def description(self):
            return "K threshold of DS3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000A0+$id"
            
        def startAddress(self):
            return 0x000000a0
            
        def endAddress(self):
            return 0x000000a7

        class _line_kval(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "line_kval"
            
            def description(self):
                return "K threshold of Line DS3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _path_kval(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "path_kval"
            
            def description(self):
                return "K threshold of Path DS3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["line_kval"] = _AF6CNC0021_RD_PM._FMPM_K_Threshold_of_DS3._line_kval()
            allFields["path_kval"] = _AF6CNC0021_RD_PM._FMPM_K_Threshold_of_DS3._path_kval()
            return allFields

    class _FMPM_K_Threshold_of_DS1(AtRegister.AtRegister):
        def name(self):
            return "FMPM K Threshold of DS1"
    
        def description(self):
            return "K threshold of DS1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000B0+$id"
            
        def startAddress(self):
            return 0x000000b0
            
        def endAddress(self):
            return 0x000000b7

        class _line_kval(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "line_kval"
            
            def description(self):
                return "K threshold of Line DS1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _path_kval(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "path_kval"
            
            def description(self):
                return "K threshold of Path DS1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["line_kval"] = _AF6CNC0021_RD_PM._FMPM_K_Threshold_of_DS1._line_kval()
            allFields["path_kval"] = _AF6CNC0021_RD_PM._FMPM_K_Threshold_of_DS1._path_kval()
            return allFields

    class _FMPM_K_Threshold_of_PW(AtRegister.AtRegister):
        def name(self):
            return "FMPM K Threshold of PW"
    
        def description(self):
            return "K threshold of PW"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000C0+$id"
            
        def startAddress(self):
            return 0x000000c0
            
        def endAddress(self):
            return 0x000000c7

        class _kval(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "kval"
            
            def description(self):
                return "K threshold of PW"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["kval"] = _AF6CNC0021_RD_PM._FMPM_K_Threshold_of_PW._kval()
            return allFields

    class _FMPM_TCA_threshold_of_Section_Part0(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of Section Part0"
    
        def description(self):
            return "TCA threshold part0 of Section and Line"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0200+$id*0x2"
            
        def startAddress(self):
            return 0x000b0200
            
        def endAddress(self):
            return 0x000b020e

        class _tca_thres_sec_cv_s(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_sec_cv_s"
            
            def description(self):
                return "TCA threshold of CV_S"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_sec_cv_s"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_Section_Part0._tca_thres_sec_cv_s()
            return allFields

    class _FMPM_TCA_threshold_of_Section_Part1(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of Section Part1"
    
        def description(self):
            return "TCA threshold part1 of Section and Line"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0210+$id*0x2"
            
        def startAddress(self):
            return 0x000b0210
            
        def endAddress(self):
            return 0x000b021e

        class _tca_thres_sec_sefs_s(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 20
        
            def name(self):
                return "tca_thres_sec_sefs_s"
            
            def description(self):
                return "TCA threshold of SEFS_S"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_sec_es_s(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 10
        
            def name(self):
                return "tca_thres_sec_es_s"
            
            def description(self):
                return "TCA threshold of ES_S"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_sec_ses_s(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_sec_ses_s"
            
            def description(self):
                return "TCA threshold of SES_S"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_sec_sefs_s"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_Section_Part1._tca_thres_sec_sefs_s()
            allFields["tca_thres_sec_es_s"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_Section_Part1._tca_thres_sec_es_s()
            allFields["tca_thres_sec_ses_s"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_Section_Part1._tca_thres_sec_ses_s()
            return allFields

    class _FMPM_TCA_threshold_of_Section_Part2(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of Section Part2"
    
        def description(self):
            return "TCA threshold part2 of Section and Line"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0220+$id*0x2"
            
        def startAddress(self):
            return 0x000b0220
            
        def endAddress(self):
            return 0x000b022e

        class _tca_thres_sec_cv_l(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_sec_cv_l"
            
            def description(self):
                return "TCA threshold of CV_L"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_sec_cv_l"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_Section_Part2._tca_thres_sec_cv_l()
            return allFields

    class _FMPM_TCA_threshold_of_Section_Part3(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of Section Part3"
    
        def description(self):
            return "TCA threshold part3 of Section and Line"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0230+$id*0x2"
            
        def startAddress(self):
            return 0x000b0230
            
        def endAddress(self):
            return 0x000b023e

        class _tca_thres_sec_es_l(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 20
        
            def name(self):
                return "tca_thres_sec_es_l"
            
            def description(self):
                return "TCA threshold of ES_L"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_sec_ses_l(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 10
        
            def name(self):
                return "tca_thres_sec_ses_l"
            
            def description(self):
                return "TCA threshold of SES_L"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_sec_uas_l(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_sec_uas_l"
            
            def description(self):
                return "TCA threshold of UAS_L"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_sec_es_l"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_Section_Part3._tca_thres_sec_es_l()
            allFields["tca_thres_sec_ses_l"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_Section_Part3._tca_thres_sec_ses_l()
            allFields["tca_thres_sec_uas_l"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_Section_Part3._tca_thres_sec_uas_l()
            return allFields

    class _FMPM_TCA_threshold_of_Section_Part4(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of Section Part4"
    
        def description(self):
            return "TCA threshold part4 of Section and Line"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0240+$id*0x2"
            
        def startAddress(self):
            return 0x000b0240
            
        def endAddress(self):
            return 0x000b024e

        class _tca_thres_sec_cv_lfe(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_sec_cv_lfe"
            
            def description(self):
                return "TCA threshold of CV_LFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_sec_cv_lfe"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_Section_Part4._tca_thres_sec_cv_lfe()
            return allFields

    class _FMPM_TCA_threshold_of_Section_Part5(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of Section Part5"
    
        def description(self):
            return "TCA threshold part5 of Section and Line"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0250+$id*0x2"
            
        def startAddress(self):
            return 0x000b0250
            
        def endAddress(self):
            return 0x000b025e

        class _tca_thres_sec_fc_l(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 20
        
            def name(self):
                return "tca_thres_sec_fc_l"
            
            def description(self):
                return "TCA threshold of FC_L"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_sec_es_lfe(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 10
        
            def name(self):
                return "tca_thres_sec_es_lfe"
            
            def description(self):
                return "TCA threshold of ES_LFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_sec_ses_lfe(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_sec_ses_lfe"
            
            def description(self):
                return "TCA threshold of SES_LFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_sec_fc_l"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_Section_Part5._tca_thres_sec_fc_l()
            allFields["tca_thres_sec_es_lfe"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_Section_Part5._tca_thres_sec_es_lfe()
            allFields["tca_thres_sec_ses_lfe"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_Section_Part5._tca_thres_sec_ses_lfe()
            return allFields

    class _FMPM_TCA_threshold_of_Section_Part6(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of Section Part6"
    
        def description(self):
            return "TCA threshold part6 of Section and Line"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0260+$id*0x2"
            
        def startAddress(self):
            return 0x000b0260
            
        def endAddress(self):
            return 0x000b026e

        class _tca_thres_sec_uas_lfe(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 10
        
            def name(self):
                return "tca_thres_sec_uas_lfe"
            
            def description(self):
                return "TCA threshold of UAS_LFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_sec_fc_lfe(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_sec_fc_lfe"
            
            def description(self):
                return "TCA threshold of FC_LFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_sec_uas_lfe"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_Section_Part6._tca_thres_sec_uas_lfe()
            allFields["tca_thres_sec_fc_lfe"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_Section_Part6._tca_thres_sec_fc_lfe()
            return allFields

    class _FMPM_TCA_threshold_of_HO_Part0(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of HO Part0"
    
        def description(self):
            return "TCA threshold part0 of HO"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0000+$id*0x2"
            
        def startAddress(self):
            return 0x000b0000
            
        def endAddress(self):
            return 0x000b000e

        class _tca_thres_sts_cv_p(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_sts_cv_p"
            
            def description(self):
                return "TCA threshold of CV_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_sts_cv_p"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_HO_Part0._tca_thres_sts_cv_p()
            return allFields

    class _FMPM_TCA_threshold_of_HO_Part1(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of HO Part1"
    
        def description(self):
            return "TCA threshold part1 of HO"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0010+$id*0x2"
            
        def startAddress(self):
            return 0x000b0010
            
        def endAddress(self):
            return 0x000b001e

        class _tca_thres_sts_es_p(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 20
        
            def name(self):
                return "tca_thres_sts_es_p"
            
            def description(self):
                return "TCA threshold of ES_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_sts_ses_p(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 10
        
            def name(self):
                return "tca_thres_sts_ses_p"
            
            def description(self):
                return "TCA threshold of SES_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_sts_uas_p(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_sts_uas_p"
            
            def description(self):
                return "TCA threshold of UAS_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_sts_es_p"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_HO_Part1._tca_thres_sts_es_p()
            allFields["tca_thres_sts_ses_p"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_HO_Part1._tca_thres_sts_ses_p()
            allFields["tca_thres_sts_uas_p"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_HO_Part1._tca_thres_sts_uas_p()
            return allFields

    class _FMPM_TCA_threshold_of_HO_Part2(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of HO Part2"
    
        def description(self):
            return "TCA threshold part2 of HO"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0020+$id*0x2"
            
        def startAddress(self):
            return 0x000b0020
            
        def endAddress(self):
            return 0x000b002e

        class _tca_thres_sts_ppjc_pdet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_sts_ppjc_pdet"
            
            def description(self):
                return "TCA threshold of PPJC_PDet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_sts_ppjc_pdet"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_HO_Part2._tca_thres_sts_ppjc_pdet()
            return allFields

    class _FMPM_TCA_threshold_of_HO_Part3(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of HO Part3"
    
        def description(self):
            return "TCA threshold part3 of HO"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0030+$id*0x2"
            
        def startAddress(self):
            return 0x000b0030
            
        def endAddress(self):
            return 0x000b003e

        class _tca_thres_sts_npjc_pdet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_sts_npjc_pdet"
            
            def description(self):
                return "TCA threshold of NPJC_PDet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_sts_npjc_pdet"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_HO_Part3._tca_thres_sts_npjc_pdet()
            return allFields

    class _FMPM_TCA_threshold_of_HO_Part4(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of HO Part4"
    
        def description(self):
            return "TCA threshold part4 of HO"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0040+$id*0x2"
            
        def startAddress(self):
            return 0x000b0040
            
        def endAddress(self):
            return 0x000b004e

        class _tca_thres_sts_ppjc_pgen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_sts_ppjc_pgen"
            
            def description(self):
                return "TCA threshold of PPJC_PGen"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_sts_ppjc_pgen"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_HO_Part4._tca_thres_sts_ppjc_pgen()
            return allFields

    class _FMPM_TCA_threshold_of_HO_Part5(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of HO Part5"
    
        def description(self):
            return "TCA threshold part5 of HO"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0050+$id*0x2"
            
        def startAddress(self):
            return 0x000b0050
            
        def endAddress(self):
            return 0x000b005e

        class _tca_thres_sts_npjc_pgen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_sts_npjc_pgen"
            
            def description(self):
                return "TCA threshold of NPJC_PGen"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_sts_npjc_pgen"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_HO_Part5._tca_thres_sts_npjc_pgen()
            return allFields

    class _FMPM_TCA_threshold_of_HO_Part6(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of HO Part6"
    
        def description(self):
            return "TCA threshold part6 of HO"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0060+$id*0x2"
            
        def startAddress(self):
            return 0x000b0060
            
        def endAddress(self):
            return 0x000b006e

        class _tca_thres_sts_pjcdiff_p(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_sts_pjcdiff_p"
            
            def description(self):
                return "TCA threshold of PJCDiff_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_sts_pjcdiff_p"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_HO_Part6._tca_thres_sts_pjcdiff_p()
            return allFields

    class _FMPM_TCA_threshold_of_HO_Part7(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of HO Part7"
    
        def description(self):
            return "TCA threshold part7 of HO"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0070+$id*0x2"
            
        def startAddress(self):
            return 0x000b0070
            
        def endAddress(self):
            return 0x000b007e

        class _tca_thres_sts_pjcs_pdet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 10
        
            def name(self):
                return "tca_thres_sts_pjcs_pdet"
            
            def description(self):
                return "TCA threshold of PJCS_PDet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_sts_pjcs_pgen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_sts_pjcs_pgen"
            
            def description(self):
                return "TCA threshold of PJCS_PGen"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_sts_pjcs_pdet"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_HO_Part7._tca_thres_sts_pjcs_pdet()
            allFields["tca_thres_sts_pjcs_pgen"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_HO_Part7._tca_thres_sts_pjcs_pgen()
            return allFields

    class _FMPM_TCA_threshold_of_HO_Part8(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of HO Part8"
    
        def description(self):
            return "TCA threshold part8 of HO"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0080+$id*0x2"
            
        def startAddress(self):
            return 0x000b0080
            
        def endAddress(self):
            return 0x000b008e

        class _tca_thres_sts_cv_pfe(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_sts_cv_pfe"
            
            def description(self):
                return "TCA threshold of CV_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_sts_cv_pfe"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_HO_Part8._tca_thres_sts_cv_pfe()
            return allFields

    class _FMPM_TCA_threshold_of_HO_Part9(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of HO Part9"
    
        def description(self):
            return "TCA threshold part9 of HO"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0090+$id*0x2"
            
        def startAddress(self):
            return 0x000b0090
            
        def endAddress(self):
            return 0x000b009e

        class _tca_thres_sts_es_pfe(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 20
        
            def name(self):
                return "tca_thres_sts_es_pfe"
            
            def description(self):
                return "TCA threshold of ES_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_sts_ses_pfe(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 10
        
            def name(self):
                return "tca_thres_sts_ses_pfe"
            
            def description(self):
                return "TCA threshold of SES_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_sts_uas_pfe(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_sts_uas_pfe"
            
            def description(self):
                return "TCA threshold of USE_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_sts_es_pfe"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_HO_Part9._tca_thres_sts_es_pfe()
            allFields["tca_thres_sts_ses_pfe"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_HO_Part9._tca_thres_sts_ses_pfe()
            allFields["tca_thres_sts_uas_pfe"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_HO_Part9._tca_thres_sts_uas_pfe()
            return allFields

    class _FMPM_TCA_threshold_of_HO_Part10(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of HO Part10"
    
        def description(self):
            return "TCA threshold part10 of HO"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB00A0+$id*0x2"
            
        def startAddress(self):
            return 0x000b00a0
            
        def endAddress(self):
            return 0x000b00ae

        class _tca_thres_sts_fc_p(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 10
        
            def name(self):
                return "tca_thres_sts_fc_p"
            
            def description(self):
                return "TCA threshold of FC_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_sts_fc_pfe(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_sts_fc_pfe"
            
            def description(self):
                return "TCA threshold of FC_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_sts_fc_p"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_HO_Part10._tca_thres_sts_fc_p()
            allFields["tca_thres_sts_fc_pfe"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_HO_Part10._tca_thres_sts_fc_pfe()
            return allFields

    class _FMPM_TCA_threshold_of_LO_Part0(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of LO Part0"
    
        def description(self):
            return "TCA threshold part0 of LO"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0300+$id*0x2"
            
        def startAddress(self):
            return 0x000b0300
            
        def endAddress(self):
            return 0x000b030e

        class _tca_thres_vt_cv_v(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_vt_cv_v"
            
            def description(self):
                return "TCA threshold of CV_V"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_vt_cv_v"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_LO_Part0._tca_thres_vt_cv_v()
            return allFields

    class _FMPM_TCA_threshold_of_LO_Part1(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of LO Part1"
    
        def description(self):
            return "TCA threshold part1 of LO"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0310+$id*0x2"
            
        def startAddress(self):
            return 0x000b0310
            
        def endAddress(self):
            return 0x000b031e

        class _tca_thres_vt_es_v(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 20
        
            def name(self):
                return "tca_thres_vt_es_v"
            
            def description(self):
                return "TCA threshold of ES_V"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_vt_ses_v(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 10
        
            def name(self):
                return "tca_thres_vt_ses_v"
            
            def description(self):
                return "TCA threshold of SES_V"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_vt_uas_v(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_vt_uas_v"
            
            def description(self):
                return "TCA threshold of UAS_V"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_vt_es_v"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_LO_Part1._tca_thres_vt_es_v()
            allFields["tca_thres_vt_ses_v"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_LO_Part1._tca_thres_vt_ses_v()
            allFields["tca_thres_vt_uas_v"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_LO_Part1._tca_thres_vt_uas_v()
            return allFields

    class _FMPM_TCA_threshold_of_LO_Part2(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of LO Part2"
    
        def description(self):
            return "TCA threshold part2 of LO"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0320+$id*0x2"
            
        def startAddress(self):
            return 0x000b0320
            
        def endAddress(self):
            return 0x000b032e

        class _tca_thres_vt_ppjc_vdet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_vt_ppjc_vdet"
            
            def description(self):
                return "TCA threshold of PPJC_VDet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_vt_ppjc_vdet"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_LO_Part2._tca_thres_vt_ppjc_vdet()
            return allFields

    class _FMPM_TCA_threshold_of_LO_Part3(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of LO Part3"
    
        def description(self):
            return "TCA threshold part3 of LO"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0330+$id*0x2"
            
        def startAddress(self):
            return 0x000b0330
            
        def endAddress(self):
            return 0x000b033e

        class _tca_thres_vt_npjc_vdet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_vt_npjc_vdet"
            
            def description(self):
                return "TCA threshold of NPJC_VDet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_vt_npjc_vdet"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_LO_Part3._tca_thres_vt_npjc_vdet()
            return allFields

    class _FMPM_TCA_threshold_of_LO_Part4(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of LO Part4"
    
        def description(self):
            return "TCA threshold part4 of LO"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0340+$id*0x2"
            
        def startAddress(self):
            return 0x000b0340
            
        def endAddress(self):
            return 0x000b034e

        class _tca_thres_vt_ppjc_vgen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_vt_ppjc_vgen"
            
            def description(self):
                return "TCA threshold of PPJC_VGen"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_vt_ppjc_vgen"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_LO_Part4._tca_thres_vt_ppjc_vgen()
            return allFields

    class _FMPM_TCA_threshold_of_LO_Part5(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of LO Part5"
    
        def description(self):
            return "TCA threshold part5 of LO"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0350+$id*0x2"
            
        def startAddress(self):
            return 0x000b0350
            
        def endAddress(self):
            return 0x000b035e

        class _tca_thres_vt_npjc_vgen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_vt_npjc_vgen"
            
            def description(self):
                return "TCA threshold of NPJC_VGen"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_vt_npjc_vgen"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_LO_Part5._tca_thres_vt_npjc_vgen()
            return allFields

    class _FMPM_TCA_threshold_of_LO_Part6(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of LO Part6"
    
        def description(self):
            return "TCA threshold part6 of LO"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0360+$id*0x2"
            
        def startAddress(self):
            return 0x000b0360
            
        def endAddress(self):
            return 0x000b036e

        class _tca_thres_vt_pjcdiff_v(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_vt_pjcdiff_v"
            
            def description(self):
                return "TCA threshold of PJCDiff_V"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_vt_pjcdiff_v"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_LO_Part6._tca_thres_vt_pjcdiff_v()
            return allFields

    class _FMPM_TCA_threshold_of_LO_Part7(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of LO Part7"
    
        def description(self):
            return "TCA threshold part7 of LO"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0370+$id*0x2"
            
        def startAddress(self):
            return 0x000b0370
            
        def endAddress(self):
            return 0x000b037e

        class _tca_thres_vt_pjcs_vdet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 10
        
            def name(self):
                return "tca_thres_vt_pjcs_vdet"
            
            def description(self):
                return "TCA threshold of PJCS_VDET"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_vt_pjcs_vgen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_vt_pjcs_vgen"
            
            def description(self):
                return "TCA threshold of PJCS_VGEN"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_vt_pjcs_vdet"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_LO_Part7._tca_thres_vt_pjcs_vdet()
            allFields["tca_thres_vt_pjcs_vgen"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_LO_Part7._tca_thres_vt_pjcs_vgen()
            return allFields

    class _FMPM_TCA_threshold_of_LO_Part8(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of LO Part8"
    
        def description(self):
            return "TCA threshold part8 of LO"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0380+$id*0x2"
            
        def startAddress(self):
            return 0x000b0380
            
        def endAddress(self):
            return 0x000b038e

        class _tca_thres_vt_cv_pfe(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_vt_cv_pfe"
            
            def description(self):
                return "TCA threshold of CV_VFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_vt_cv_pfe"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_LO_Part8._tca_thres_vt_cv_pfe()
            return allFields

    class _FMPM_TCA_threshold_of_LO_Part9(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of LO Part9"
    
        def description(self):
            return "TCA threshold part9 of LO"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0390+$id*0x2"
            
        def startAddress(self):
            return 0x000b0390
            
        def endAddress(self):
            return 0x000b039e

        class _tca_thres_vt_es_vfe(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 20
        
            def name(self):
                return "tca_thres_vt_es_vfe"
            
            def description(self):
                return "TCA threshold of ES_VFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_vt_ses_vfe(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 10
        
            def name(self):
                return "tca_thres_vt_ses_vfe"
            
            def description(self):
                return "TCA threshold of SES_VFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_vt_uas_vfe(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_vt_uas_vfe"
            
            def description(self):
                return "TCA threshold of USE_VFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_vt_es_vfe"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_LO_Part9._tca_thres_vt_es_vfe()
            allFields["tca_thres_vt_ses_vfe"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_LO_Part9._tca_thres_vt_ses_vfe()
            allFields["tca_thres_vt_uas_vfe"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_LO_Part9._tca_thres_vt_uas_vfe()
            return allFields

    class _FMPM_TCA_threshold_of_LO_Part10(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of LO Part10"
    
        def description(self):
            return "TCA threshold part10 of LO"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB03A0+$id*0x2"
            
        def startAddress(self):
            return 0x000b03a0
            
        def endAddress(self):
            return 0x000b03ae

        class _tca_thres_vt_fc_v(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 10
        
            def name(self):
                return "tca_thres_vt_fc_v"
            
            def description(self):
                return "TCA threshold of FC_V"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_vt_fc_vfe(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_vt_fc_vfe"
            
            def description(self):
                return "TCA threshold of FC_VFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_vt_fc_v"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_LO_Part10._tca_thres_vt_fc_v()
            allFields["tca_thres_vt_fc_vfe"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_LO_Part10._tca_thres_vt_fc_vfe()
            return allFields

    class _FMPM_TCA_threshold_of_DS3_Part0(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of DS3 Part0"
    
        def description(self):
            return "TCA threshold part0 of DS3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0100+$id*0x2"
            
        def startAddress(self):
            return 0x000b0100
            
        def endAddress(self):
            return 0x000b010e

        class _tca_thres_ds3_cv_l(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_ds3_cv_l"
            
            def description(self):
                return "TCA threshold of CV_L"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_ds3_cv_l"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part0._tca_thres_ds3_cv_l()
            return allFields

    class _FMPM_TCA_threshold_of_DS3_Part1(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of DS3 Part1"
    
        def description(self):
            return "TCA threshold part1 of DS3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0110+$id*0x2"
            
        def startAddress(self):
            return 0x000b0110
            
        def endAddress(self):
            return 0x000b011e

        class _tca_thres_ds3_es_l(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 20
        
            def name(self):
                return "tca_thres_ds3_es_l"
            
            def description(self):
                return "TCA threshold of ES_L"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_ds3_esa_l(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 10
        
            def name(self):
                return "tca_thres_ds3_esa_l"
            
            def description(self):
                return "TCA threshold of ESA_L"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_ds3_esb_l(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_ds3_esb_l"
            
            def description(self):
                return "TCA threshold of ESB_L"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_ds3_es_l"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part1._tca_thres_ds3_es_l()
            allFields["tca_thres_ds3_esa_l"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part1._tca_thres_ds3_esa_l()
            allFields["tca_thres_ds3_esb_l"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part1._tca_thres_ds3_esb_l()
            return allFields

    class _FMPM_TCA_threshold_of_DS3_Part2(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of DS3 Part2"
    
        def description(self):
            return "TCA threshold part2 of DS3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0120+$id*0x2"
            
        def startAddress(self):
            return 0x000b0120
            
        def endAddress(self):
            return 0x000b012e

        class _tca_thres_ds3_ses_l(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 10
        
            def name(self):
                return "tca_thres_ds3_ses_l"
            
            def description(self):
                return "TCA threshold of SES_L"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_ds3_loss_l(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_ds3_loss_l"
            
            def description(self):
                return "TCA threshold of LOSS_L"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_ds3_ses_l"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part2._tca_thres_ds3_ses_l()
            allFields["tca_thres_ds3_loss_l"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part2._tca_thres_ds3_loss_l()
            return allFields

    class _FMPM_TCA_threshold_of_DS3_Part3(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of DS3 Part3"
    
        def description(self):
            return "TCA threshold part3 of DS3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0130+$id*0x2"
            
        def startAddress(self):
            return 0x000b0130
            
        def endAddress(self):
            return 0x000b013e

        class _tca_thres_ds3_cvp_p(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_ds3_cvp_p"
            
            def description(self):
                return "TCA threshold of CVP_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_ds3_cvp_p"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part3._tca_thres_ds3_cvp_p()
            return allFields

    class _FMPM_TCA_threshold_of_DS3_Part4(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of DS3 Part4"
    
        def description(self):
            return "TCA threshold part4 of DS3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0140+$id*0x2"
            
        def startAddress(self):
            return 0x000b0140
            
        def endAddress(self):
            return 0x000b014e

        class _tca_thres_ds3_cvcp_p(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_ds3_cvcp_p"
            
            def description(self):
                return "TCA threshold of CVCP_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_ds3_cvcp_p"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part4._tca_thres_ds3_cvcp_p()
            return allFields

    class _FMPM_TCA_threshold_of_DS3_Part5(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of DS3 Part5"
    
        def description(self):
            return "TCA threshold part5 of DS3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0150+$id*0x2"
            
        def startAddress(self):
            return 0x000b0150
            
        def endAddress(self):
            return 0x000b015e

        class _tca_thres_ds3_esp_p(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 20
        
            def name(self):
                return "tca_thres_ds3_esp_p"
            
            def description(self):
                return "TCA threshold of ESP_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_ds3_escp_p(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 10
        
            def name(self):
                return "tca_thres_ds3_escp_p"
            
            def description(self):
                return "TCA threshold of ESCP_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_ds3_esacp_p(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_ds3_esacp_p"
            
            def description(self):
                return "TCA threshold of ESACP_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_ds3_esp_p"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part5._tca_thres_ds3_esp_p()
            allFields["tca_thres_ds3_escp_p"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part5._tca_thres_ds3_escp_p()
            allFields["tca_thres_ds3_esacp_p"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part5._tca_thres_ds3_esacp_p()
            return allFields

    class _FMPM_TCA_threshold_of_DS3_Part6(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of DS3 Part6"
    
        def description(self):
            return "TCA threshold part6 of DS3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0160+$id*0x2"
            
        def startAddress(self):
            return 0x000b0160
            
        def endAddress(self):
            return 0x000b016e

        class _tca_thres_ds3_esacp_p(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 20
        
            def name(self):
                return "tca_thres_ds3_esacp_p"
            
            def description(self):
                return "TCA threshold of ESACP_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_ds3_esbp_p(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 10
        
            def name(self):
                return "tca_thres_ds3_esbp_p"
            
            def description(self):
                return "TCA threshold of ESBP_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_ds3_esbcp_p(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_ds3_esbcp_p"
            
            def description(self):
                return "TCA threshold of ESBCP_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_ds3_esacp_p"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part6._tca_thres_ds3_esacp_p()
            allFields["tca_thres_ds3_esbp_p"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part6._tca_thres_ds3_esbp_p()
            allFields["tca_thres_ds3_esbcp_p"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part6._tca_thres_ds3_esbcp_p()
            return allFields

    class _FMPM_TCA_threshold_of_DS3_Part7(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of DS3 Part7"
    
        def description(self):
            return "TCA threshold part7 of DS3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0170+$id*0x2"
            
        def startAddress(self):
            return 0x000b0170
            
        def endAddress(self):
            return 0x000b017e

        class _tca_thres_ds3_sesp_p(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 20
        
            def name(self):
                return "tca_thres_ds3_sesp_p"
            
            def description(self):
                return "TCA threshold of SESP_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_ds3_sescp_p(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 10
        
            def name(self):
                return "tca_thres_ds3_sescp_p"
            
            def description(self):
                return "TCA threshold of SESCP_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_ds3_sas_p(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_ds3_sas_p"
            
            def description(self):
                return "TCA threshold of SAS_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_ds3_sesp_p"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part7._tca_thres_ds3_sesp_p()
            allFields["tca_thres_ds3_sescp_p"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part7._tca_thres_ds3_sescp_p()
            allFields["tca_thres_ds3_sas_p"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part7._tca_thres_ds3_sas_p()
            return allFields

    class _FMPM_TCA_threshold_of_DS3_Part8(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of DS3 Part8"
    
        def description(self):
            return "TCA threshold part8 of DS3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0180+$id*0x2"
            
        def startAddress(self):
            return 0x000b0180
            
        def endAddress(self):
            return 0x000b018e

        class _tca_thres_ds3_aiss_p(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 20
        
            def name(self):
                return "tca_thres_ds3_aiss_p"
            
            def description(self):
                return "TCA threshold of AISS_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_ds3_uasp_p(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 10
        
            def name(self):
                return "tca_thres_ds3_uasp_p"
            
            def description(self):
                return "TCA threshold of UASP_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_ds3_uascp_p(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_ds3_uascp_p"
            
            def description(self):
                return "TCA threshold of UASCP_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_ds3_aiss_p"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part8._tca_thres_ds3_aiss_p()
            allFields["tca_thres_ds3_uasp_p"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part8._tca_thres_ds3_uasp_p()
            allFields["tca_thres_ds3_uascp_p"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part8._tca_thres_ds3_uascp_p()
            return allFields

    class _FMPM_TCA_threshold_of_DS3_Part9(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of DS3 Part9"
    
        def description(self):
            return "TCA threshold part9 of DS3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0190+$id*0x2"
            
        def startAddress(self):
            return 0x000b0190
            
        def endAddress(self):
            return 0x000b019e

        class _tca_thres_ds3_cvcp_pfe(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_ds3_cvcp_pfe"
            
            def description(self):
                return "TCA threshold of CVCP_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_ds3_cvcp_pfe"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part9._tca_thres_ds3_cvcp_pfe()
            return allFields

    class _FMPM_TCA_threshold_of_DS3_Part10(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of DS3 Part10"
    
        def description(self):
            return "TCA threshold part10 of DS3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB01A0+$id*0x2"
            
        def startAddress(self):
            return 0x000b01a0
            
        def endAddress(self):
            return 0x000b01ae

        class _tca_thres_ds3_escp_pfe(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 20
        
            def name(self):
                return "tca_thres_ds3_escp_pfe"
            
            def description(self):
                return "TCA threshold of ESCP_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_ds3_esacp_pfe(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 10
        
            def name(self):
                return "tca_thres_ds3_esacp_pfe"
            
            def description(self):
                return "TCA threshold of ESACP_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_ds3_esbcp_pfe(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_ds3_esbcp_pfe"
            
            def description(self):
                return "TCA threshold of ESBCP_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_ds3_escp_pfe"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part10._tca_thres_ds3_escp_pfe()
            allFields["tca_thres_ds3_esacp_pfe"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part10._tca_thres_ds3_esacp_pfe()
            allFields["tca_thres_ds3_esbcp_pfe"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part10._tca_thres_ds3_esbcp_pfe()
            return allFields

    class _FMPM_TCA_threshold_of_DS3_Part11(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of DS3 Part11"
    
        def description(self):
            return "TCA threshold part11 of DS3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB01B0+$id*0x2"
            
        def startAddress(self):
            return 0x000b01b0
            
        def endAddress(self):
            return 0x000b01be

        class _tca_thres_ds3_sescp_pfe(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 20
        
            def name(self):
                return "tca_thres_ds3_sescp_pfe"
            
            def description(self):
                return "TCA threshold of SESCP_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_ds3_sascp_pfe(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 10
        
            def name(self):
                return "tca_thres_ds3_sascp_pfe"
            
            def description(self):
                return "TCA threshold of SASCP_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_ds3_uascp_pfe(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_ds3_uascp_pfe"
            
            def description(self):
                return "TCA threshold of UASCP_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_ds3_sescp_pfe"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part11._tca_thres_ds3_sescp_pfe()
            allFields["tca_thres_ds3_sascp_pfe"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part11._tca_thres_ds3_sascp_pfe()
            allFields["tca_thres_ds3_uascp_pfe"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part11._tca_thres_ds3_uascp_pfe()
            return allFields

    class _FMPM_TCA_threshold_of_DS3_Part12(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of DS3 Part12"
    
        def description(self):
            return "TCA threshold part12 of DS3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB01C0+$id*0x2"
            
        def startAddress(self):
            return 0x000b01c0
            
        def endAddress(self):
            return 0x000b01ce

        class _tca_thres_ds3_fc_p(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 10
        
            def name(self):
                return "tca_thres_ds3_fc_p"
            
            def description(self):
                return "TCA threshold of FC_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_ds3_fccp_pfe(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_ds3_fccp_pfe"
            
            def description(self):
                return "TCA threshold of FCCP_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_ds3_fc_p"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part12._tca_thres_ds3_fc_p()
            allFields["tca_thres_ds3_fccp_pfe"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS3_Part12._tca_thres_ds3_fccp_pfe()
            return allFields

    class _FMPM_TCA_threshold_of_DS1_Part0(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of DS1 Part0"
    
        def description(self):
            return "TCA threshold part0 of DS1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0400+$id*0x2"
            
        def startAddress(self):
            return 0x000b0400
            
        def endAddress(self):
            return 0x000b040e

        class _tca_thres_ds1_cv_l(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_ds1_cv_l"
            
            def description(self):
                return "TCA threshold of CV_L"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_ds1_cv_l"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS1_Part0._tca_thres_ds1_cv_l()
            return allFields

    class _FMPM_TCA_threshold_of_DS1_Part1(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of DS1 Part1"
    
        def description(self):
            return "TCA threshold part1 of DS1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0410+$id*0x2"
            
        def startAddress(self):
            return 0x000b0410
            
        def endAddress(self):
            return 0x000b041e

        class _tca_thres_ds1es_l(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 20
        
            def name(self):
                return "tca_thres_ds1es_l"
            
            def description(self):
                return "TCA threshold of ES_L"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_ds1ses_l(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 10
        
            def name(self):
                return "tca_thres_ds1ses_l"
            
            def description(self):
                return "TCA threshold of SES_L"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_ds1loss_l(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_ds1loss_l"
            
            def description(self):
                return "TCA threshold of LOSS_L"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_ds1es_l"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS1_Part1._tca_thres_ds1es_l()
            allFields["tca_thres_ds1ses_l"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS1_Part1._tca_thres_ds1ses_l()
            allFields["tca_thres_ds1loss_l"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS1_Part1._tca_thres_ds1loss_l()
            return allFields

    class _FMPM_TCA_threshold_of_DS1_Part2(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of DS1 Part2"
    
        def description(self):
            return "TCA threshold part2 of DS1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0420+$id*0x2"
            
        def startAddress(self):
            return 0x000b0420
            
        def endAddress(self):
            return 0x000b042e

        class _tca_thres_ds1_cv_p(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_ds1_cv_p"
            
            def description(self):
                return "TCA threshold of CV_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_ds1_cv_p"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS1_Part2._tca_thres_ds1_cv_p()
            return allFields

    class _FMPM_TCA_threshold_of_DS1_Part3(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of DS1 Part3"
    
        def description(self):
            return "TCA threshold part3 of DS1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0430+$id*0x2"
            
        def startAddress(self):
            return 0x000b0430
            
        def endAddress(self):
            return 0x000b043e

        class _tca_thres_ds1_es_p(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 20
        
            def name(self):
                return "tca_thres_ds1_es_p"
            
            def description(self):
                return "TCA threshold of ES_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_ds1_ses_p(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 10
        
            def name(self):
                return "tca_thres_ds1_ses_p"
            
            def description(self):
                return "TCA threshold of SES_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_ds1_aiss_p(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_ds1_aiss_p"
            
            def description(self):
                return "TCA threshold of AISS_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_ds1_es_p"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS1_Part3._tca_thres_ds1_es_p()
            allFields["tca_thres_ds1_ses_p"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS1_Part3._tca_thres_ds1_ses_p()
            allFields["tca_thres_ds1_aiss_p"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS1_Part3._tca_thres_ds1_aiss_p()
            return allFields

    class _FMPM_TCA_threshold_of_DS1_Part4(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of DS1 Part4"
    
        def description(self):
            return "TCA threshold part4 of DS1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0440+$id*0x2"
            
        def startAddress(self):
            return 0x000b0440
            
        def endAddress(self):
            return 0x000b044e

        class _tca_thres_ds1_sas_p(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 20
        
            def name(self):
                return "tca_thres_ds1_sas_p"
            
            def description(self):
                return "TCA threshold of SAS_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_ds1_css_p(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 10
        
            def name(self):
                return "tca_thres_ds1_css_p"
            
            def description(self):
                return "TCA threshold of CSS_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_ds1_uas_p(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_ds1_uas_p"
            
            def description(self):
                return "TCA threshold of UAS_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_ds1_sas_p"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS1_Part4._tca_thres_ds1_sas_p()
            allFields["tca_thres_ds1_css_p"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS1_Part4._tca_thres_ds1_css_p()
            allFields["tca_thres_ds1_uas_p"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS1_Part4._tca_thres_ds1_uas_p()
            return allFields

    class _FMPM_TCA_threshold_of_DS1_Part5(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of DS1 Part5"
    
        def description(self):
            return "TCA threshold part5 of DS1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0450+$id*0x2"
            
        def startAddress(self):
            return 0x000b0450
            
        def endAddress(self):
            return 0x000b045e

        class _tca_thres_ds1_fc_p(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 20
        
            def name(self):
                return "tca_thres_ds1_fc_p"
            
            def description(self):
                return "TCA threshold of FC_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_ds1_es_lfe(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 10
        
            def name(self):
                return "tca_thres_ds1_es_lfe"
            
            def description(self):
                return "TCA threshold of ES_LFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_ds1_sefs_pfe(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_ds1_sefs_pfe"
            
            def description(self):
                return "TCA threshold of SEFS_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_ds1_fc_p"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS1_Part5._tca_thres_ds1_fc_p()
            allFields["tca_thres_ds1_es_lfe"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS1_Part5._tca_thres_ds1_es_lfe()
            allFields["tca_thres_ds1_sefs_pfe"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS1_Part5._tca_thres_ds1_sefs_pfe()
            return allFields

    class _FMPM_TCA_threshold_of_DS1_Part6(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of DS1 Part6"
    
        def description(self):
            return "TCA threshold part6 of DS1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0460+$id*0x2"
            
        def startAddress(self):
            return 0x000b0460
            
        def endAddress(self):
            return 0x000b046e

        class _tca_thres_ds1_es_pfe(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 20
        
            def name(self):
                return "tca_thres_ds1_es_pfe"
            
            def description(self):
                return "TCA threshold of ES_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_ds1_ses_pfe(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 10
        
            def name(self):
                return "tca_thres_ds1_ses_pfe"
            
            def description(self):
                return "TCA threshold of SES_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_ds1_css_pfe(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_ds1_css_pfe"
            
            def description(self):
                return "TCA threshold of CSS_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_ds1_es_pfe"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS1_Part6._tca_thres_ds1_es_pfe()
            allFields["tca_thres_ds1_ses_pfe"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS1_Part6._tca_thres_ds1_ses_pfe()
            allFields["tca_thres_ds1_css_pfe"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS1_Part6._tca_thres_ds1_css_pfe()
            return allFields

    class _FMPM_TCA_threshold_of_DS1_Part7(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of DS1 Part7"
    
        def description(self):
            return "TCA threshold part6 of DS1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0470+$id*0x2"
            
        def startAddress(self):
            return 0x000b0470
            
        def endAddress(self):
            return 0x000b047e

        class _tca_thres_ds1_fc_pfe(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 10
        
            def name(self):
                return "tca_thres_ds1_fc_pfe"
            
            def description(self):
                return "TCA threshold of FC-PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_ds1_uas_pfe(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_ds1_uas_pfe"
            
            def description(self):
                return "TCA threshold of UAS_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_ds1_fc_pfe"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS1_Part7._tca_thres_ds1_fc_pfe()
            allFields["tca_thres_ds1_uas_pfe"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_DS1_Part7._tca_thres_ds1_uas_pfe()
            return allFields

    class _FMPM_TCA_threshold_of_PW_Part0(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of PW Part0"
    
        def description(self):
            return "TCA threshold part0 of PW"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0500+$id*0x2"
            
        def startAddress(self):
            return 0x000b0500
            
        def endAddress(self):
            return 0x000b050e

        class _tca_thres_pw_es(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 20
        
            def name(self):
                return "tca_thres_pw_es"
            
            def description(self):
                return "TCA threshold of ES"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_pw_ses(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 10
        
            def name(self):
                return "tca_thres_pw_ses"
            
            def description(self):
                return "TCA threshold of SES"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_pw_uas(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_pw_uas"
            
            def description(self):
                return "TCA threshold of UAS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_pw_es"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_PW_Part0._tca_thres_pw_es()
            allFields["tca_thres_pw_ses"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_PW_Part0._tca_thres_pw_ses()
            allFields["tca_thres_pw_uas"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_PW_Part0._tca_thres_pw_uas()
            return allFields

    class _FMPM_TCA_threshold_of_PW_Part1(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of PW Part1"
    
        def description(self):
            return "TCA threshold part1 of PW"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0510+$id*0x2"
            
        def startAddress(self):
            return 0x000b0510
            
        def endAddress(self):
            return 0x000b051e

        class _tca_thres_pw_es_fe(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 20
        
            def name(self):
                return "tca_thres_pw_es_fe"
            
            def description(self):
                return "TCA threshold of ES_FE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_pw_ses_fe(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 10
        
            def name(self):
                return "tca_thres_pw_ses_fe"
            
            def description(self):
                return "TCA threshold of SES_FE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tca_thres_pw_uas_fe(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_pw_uas_fe"
            
            def description(self):
                return "TCA threshold of UAS_CNT"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_pw_es_fe"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_PW_Part1._tca_thres_pw_es_fe()
            allFields["tca_thres_pw_ses_fe"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_PW_Part1._tca_thres_pw_ses_fe()
            allFields["tca_thres_pw_uas_fe"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_PW_Part1._tca_thres_pw_uas_fe()
            return allFields

    class _FMPM_TCA_threshold_of_PW_Part2(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA threshold of PW Part2"
    
        def description(self):
            return "TCA threshold part2 of PW"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB0520+$id*0x2"
            
        def startAddress(self):
            return 0x000b0520
            
        def endAddress(self):
            return 0x000b052e

        class _tca_thres_pw_fc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca_thres_pw_fc"
            
            def description(self):
                return "TCA threshold of FC"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca_thres_pw_fc"] = _AF6CNC0021_RD_PM._FMPM_TCA_threshold_of_PW_Part2._tca_thres_pw_fc()
            return allFields

    class _FMPM_Enable_PM_Collection_Section(AtRegister.AtRegister):
        def name(self):
            return "FMPM Enable PM Collection Section"
    
        def description(self):
            return "Enable PM Collection Section"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB1800+$L*0x40+$M*0x20+$N"
            
        def startAddress(self):
            return 0x000b1800
            
        def endAddress(self):
            return 0x000b19ff

        class _enb_col_sec(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "enb_col_sec"
            
            def description(self):
                return "Enable PM Collection Section"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["enb_col_sec"] = _AF6CNC0021_RD_PM._FMPM_Enable_PM_Collection_Section._enb_col_sec()
            return allFields

    class _FMPM_Enable_PM_Collection_STS(AtRegister.AtRegister):
        def name(self):
            return "FMPM Enable PM Collection STS"
    
        def description(self):
            return "Enable PM STS"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB1000+$L*0x40+$M*0x20+$N"
            
        def startAddress(self):
            return 0x000b1000
            
        def endAddress(self):
            return 0x000b11ff

        class _enb_col_sts(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "enb_col_sts"
            
            def description(self):
                return "Enable PM Collection STS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["enb_col_sts"] = _AF6CNC0021_RD_PM._FMPM_Enable_PM_Collection_STS._enb_col_sts()
            return allFields

    class _FMPM_Enable_PM_Collection_DS3(AtRegister.AtRegister):
        def name(self):
            return "FMPM Enable PM Collection DS3"
    
        def description(self):
            return "Enbale PM DS3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB1400+$G*0x20+$H"
            
        def startAddress(self):
            return 0x000b1400
            
        def endAddress(self):
            return 0x000b15ff

        class _enb_col_ds3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "enb_col_ds3"
            
            def description(self):
                return "Enable PM Collection DS3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["enb_col_ds3"] = _AF6CNC0021_RD_PM._FMPM_Enable_PM_Collection_DS3._enb_col_ds3()
            return allFields

    class _FMPM_Enable_PM_Collection_VT(AtRegister.AtRegister):
        def name(self):
            return "FMPM Enable PM Collection VT"
    
        def description(self):
            return "Enable PM VT"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB4000+$G*0x400+$H*0x20+$I"
            
        def startAddress(self):
            return 0x000b4000
            
        def endAddress(self):
            return 0x000b7fff

        class _enb_col_vt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "enb_col_vt"
            
            def description(self):
                return "Enable PM Collection VT"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["enb_col_vt"] = _AF6CNC0021_RD_PM._FMPM_Enable_PM_Collection_VT._enb_col_vt()
            return allFields

    class _FMPM_Enable_PM_Collection_DS1(AtRegister.AtRegister):
        def name(self):
            return "FMPM Enable PM Collection DS1"
    
        def description(self):
            return "Enable PM DS1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB8000+$G*0x400+$H*0x20+$I"
            
        def startAddress(self):
            return 0x000b8000
            
        def endAddress(self):
            return 0x000bbfff

        class _enb_col_ds1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "enb_col_ds1"
            
            def description(self):
                return "Enable PM Collection DS1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["enb_col_ds1"] = _AF6CNC0021_RD_PM._FMPM_Enable_PM_Collection_DS1._enb_col_ds1()
            return allFields

    class _FMPM_Enable_PM_Collection_PW(AtRegister.AtRegister):
        def name(self):
            return "FMPM Enable PM Collection PW"
    
        def description(self):
            return "Enable PM PW"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xBC000+$id"
            
        def startAddress(self):
            return 0x000bc000
            
        def endAddress(self):
            return 0x000bffff

        class _enb_col_pw(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "enb_col_pw"
            
            def description(self):
                return "Enable PM Collection PW"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["enb_col_pw"] = _AF6CNC0021_RD_PM._FMPM_Enable_PM_Collection_PW._enb_col_pw()
            return allFields

    class _FMPM_Threshold_Type_of_Section(AtRegister.AtRegister):
        def name(self):
            return "FMPM Threshold Type of Section"
    
        def description(self):
            return "Threshold Type of Section"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00600+$L*0x40+$M*0x20+$N"
            
        def startAddress(self):
            return 0x00000600
            
        def endAddress(self):
            return 0x000006ff

        class _K_TYPE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 8
        
            def name(self):
                return "K_TYPE"
            
            def description(self):
                return "Use to read K threshold table"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TCA_TYPE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TCA_TYPE"
            
            def description(self):
                return "Use to read TCA threshold table"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["K_TYPE"] = _AF6CNC0021_RD_PM._FMPM_Threshold_Type_of_Section._K_TYPE()
            allFields["TCA_TYPE"] = _AF6CNC0021_RD_PM._FMPM_Threshold_Type_of_Section._TCA_TYPE()
            return allFields

    class _FMPM_Threshold_Type_of_STS(AtRegister.AtRegister):
        def name(self):
            return "FMPM Threshold Type of STS"
    
        def description(self):
            return "Threshold Type of STS"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00200+$L*0x40+$M*0x20+$N"
            
        def startAddress(self):
            return 0x00000200
            
        def endAddress(self):
            return 0x000003ff

        class _K_TYPE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 8
        
            def name(self):
                return "K_TYPE"
            
            def description(self):
                return "Use to read K threshold table"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TCA_TYPE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TCA_TYPE"
            
            def description(self):
                return "Use to read TCA threshold table"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["K_TYPE"] = _AF6CNC0021_RD_PM._FMPM_Threshold_Type_of_STS._K_TYPE()
            allFields["TCA_TYPE"] = _AF6CNC0021_RD_PM._FMPM_Threshold_Type_of_STS._TCA_TYPE()
            return allFields

    class _FMPM_Threshold_Type_of_DS3(AtRegister.AtRegister):
        def name(self):
            return "FMPM Threshold Type of DS3"
    
        def description(self):
            return "Threshold Type of DS3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00400+$G*0x20+$H"
            
        def startAddress(self):
            return 0x00000400
            
        def endAddress(self):
            return 0x00000520

        class _K_TYPE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 8
        
            def name(self):
                return "K_TYPE"
            
            def description(self):
                return "Use to read K threshold table"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TCA_TYPE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TCA_TYPE"
            
            def description(self):
                return "Use to read TCA threshold table"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["K_TYPE"] = _AF6CNC0021_RD_PM._FMPM_Threshold_Type_of_DS3._K_TYPE()
            allFields["TCA_TYPE"] = _AF6CNC0021_RD_PM._FMPM_Threshold_Type_of_DS3._TCA_TYPE()
            return allFields

    class _FMPM_Threshold_Type_of_VT(AtRegister.AtRegister):
        def name(self):
            return "FMPM Threshold Type of VT"
    
        def description(self):
            return "Threshold Type of VT"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x04000+$G*0x400+$H*0x20+$I"
            
        def startAddress(self):
            return 0x00004000
            
        def endAddress(self):
            return 0x00007fff

        class _K_TYPE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 8
        
            def name(self):
                return "K_TYPE"
            
            def description(self):
                return "Use to read K threshold table"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TCA_TYPE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TCA_TYPE"
            
            def description(self):
                return "Use to read TCA threshold table"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["K_TYPE"] = _AF6CNC0021_RD_PM._FMPM_Threshold_Type_of_VT._K_TYPE()
            allFields["TCA_TYPE"] = _AF6CNC0021_RD_PM._FMPM_Threshold_Type_of_VT._TCA_TYPE()
            return allFields

    class _FMPM_Threshold_Type_of_DS1(AtRegister.AtRegister):
        def name(self):
            return "FMPM Threshold Type of DS1"
    
        def description(self):
            return "Threshold Type of DS1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x08000+$G*0x400+$H*0x20+$I"
            
        def startAddress(self):
            return 0x00008000
            
        def endAddress(self):
            return 0x0000bfff

        class _K_TYPE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 8
        
            def name(self):
                return "K_TYPE"
            
            def description(self):
                return "Use to read K threshold table"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TCA_TYPE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TCA_TYPE"
            
            def description(self):
                return "Use to read TCA threshold table"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["K_TYPE"] = _AF6CNC0021_RD_PM._FMPM_Threshold_Type_of_DS1._K_TYPE()
            allFields["TCA_TYPE"] = _AF6CNC0021_RD_PM._FMPM_Threshold_Type_of_DS1._TCA_TYPE()
            return allFields

    class _FMPM_Threshold_Type_of_PW(AtRegister.AtRegister):
        def name(self):
            return "FMPM Threshold Type of PW"
    
        def description(self):
            return "Threshold Type of PW"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0C000+$id"
            
        def startAddress(self):
            return 0x0000c000
            
        def endAddress(self):
            return 0x0000dfff

        class _K_TYPE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 8
        
            def name(self):
                return "K_TYPE"
            
            def description(self):
                return "Use to read K threshold table"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TCA_TYPE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TCA_TYPE"
            
            def description(self):
                return "Use to read TCA threshold table"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["K_TYPE"] = _AF6CNC0021_RD_PM._FMPM_Threshold_Type_of_PW._K_TYPE()
            allFields["TCA_TYPE"] = _AF6CNC0021_RD_PM._FMPM_Threshold_Type_of_PW._TCA_TYPE()
            return allFields

    class _FMPM_FM_Interrupt(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM Interrupt"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00014000
            
        def endAddress(self):
            return 0xffffffff

        class _PM_SWPAGE_Intr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PM_SWPAGE_Intr"
            
            def description(self):
                return "PM switch page status"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_Intr_PW(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_FM_Intr_PW"
            
            def description(self):
                return "PW    Interrupt"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_Intr_DS1E1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_FM_Intr_DS1E1"
            
            def description(self):
                return "DS1E1 interrupt"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_Intr_DS3E3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_FM_Intr_DS3E3"
            
            def description(self):
                return "DS3E3  interrupt"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_Intr_VTGTUG(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_FM_Intr_VTGTUG"
            
            def description(self):
                return "VTGTUGinterrupt"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_Intr_STSAU(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_FM_Intr_STSAU"
            
            def description(self):
                return "STSAU interrupt"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_Intr_ECSTM(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_Intr_ECSTM"
            
            def description(self):
                return "ECSTM interrupt"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PM_SWPAGE_Intr"] = _AF6CNC0021_RD_PM._FMPM_FM_Interrupt._PM_SWPAGE_Intr()
            allFields["FMPM_FM_Intr_PW"] = _AF6CNC0021_RD_PM._FMPM_FM_Interrupt._FMPM_FM_Intr_PW()
            allFields["FMPM_FM_Intr_DS1E1"] = _AF6CNC0021_RD_PM._FMPM_FM_Interrupt._FMPM_FM_Intr_DS1E1()
            allFields["FMPM_FM_Intr_DS3E3"] = _AF6CNC0021_RD_PM._FMPM_FM_Interrupt._FMPM_FM_Intr_DS3E3()
            allFields["FMPM_FM_Intr_VTGTUG"] = _AF6CNC0021_RD_PM._FMPM_FM_Interrupt._FMPM_FM_Intr_VTGTUG()
            allFields["FMPM_FM_Intr_STSAU"] = _AF6CNC0021_RD_PM._FMPM_FM_Interrupt._FMPM_FM_Intr_STSAU()
            allFields["FMPM_FM_Intr_ECSTM"] = _AF6CNC0021_RD_PM._FMPM_FM_Interrupt._FMPM_FM_Intr_ECSTM()
            return allFields

    class _FMPM_FM_Interrupt_Mask(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM Interrupt Mask"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00014001
            
        def endAddress(self):
            return 0xffffffff

        class _PM_SWPAGE_Intr_MSK(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PM_SWPAGE_Intr_MSK"
            
            def description(self):
                return "PM switch page mask"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_Intr_MSK_PW(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_FM_Intr_MSK_PW"
            
            def description(self):
                return "PW    Interrupt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_Intr_MSK_DS1E1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_FM_Intr_MSK_DS1E1"
            
            def description(self):
                return "DS1E1 interrupt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_Intr_MSK_DS3E3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_FM_Intr_MSK_DS3E3"
            
            def description(self):
                return "DS3E3  interrupt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_Intr_MSK_VTGTUG(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_FM_Intr_MSK_VTGTUG"
            
            def description(self):
                return "VTGTUG interrupt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_Intr_MSK_STSAU(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_FM_Intr_MSK_STSAU"
            
            def description(self):
                return "STSAU interrupt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_Intr_MSK_ECSTM(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_Intr_MSK_ECSTM"
            
            def description(self):
                return "ECSTM interrupt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PM_SWPAGE_Intr_MSK"] = _AF6CNC0021_RD_PM._FMPM_FM_Interrupt_Mask._PM_SWPAGE_Intr_MSK()
            allFields["FMPM_FM_Intr_MSK_PW"] = _AF6CNC0021_RD_PM._FMPM_FM_Interrupt_Mask._FMPM_FM_Intr_MSK_PW()
            allFields["FMPM_FM_Intr_MSK_DS1E1"] = _AF6CNC0021_RD_PM._FMPM_FM_Interrupt_Mask._FMPM_FM_Intr_MSK_DS1E1()
            allFields["FMPM_FM_Intr_MSK_DS3E3"] = _AF6CNC0021_RD_PM._FMPM_FM_Interrupt_Mask._FMPM_FM_Intr_MSK_DS3E3()
            allFields["FMPM_FM_Intr_MSK_VTGTUG"] = _AF6CNC0021_RD_PM._FMPM_FM_Interrupt_Mask._FMPM_FM_Intr_MSK_VTGTUG()
            allFields["FMPM_FM_Intr_MSK_STSAU"] = _AF6CNC0021_RD_PM._FMPM_FM_Interrupt_Mask._FMPM_FM_Intr_MSK_STSAU()
            allFields["FMPM_FM_Intr_MSK_ECSTM"] = _AF6CNC0021_RD_PM._FMPM_FM_Interrupt_Mask._FMPM_FM_Intr_MSK_ECSTM()
            return allFields

    class _FMPM_FM_EC1_Interrupt_OR(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM EC1 Interrupt OR"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00018005
            
        def endAddress(self):
            return 0xffffffff

        class _FMPM_FM_EC1_OR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_EC1_OR"
            
            def description(self):
                return "Interrupt EC1 OR"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_EC1_OR"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1_Interrupt_OR._FMPM_FM_EC1_OR()
            return allFields

    class _FMPM_FM_EC1_Interrupt_OR_AND_MASK_Per_STS1(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM EC1 Interrupt OR AND MASK Per STS1"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x18100+$L*0x2+$M"
            
        def startAddress(self):
            return 0x00018100
            
        def endAddress(self):
            return 0x0001810f

        class _FMPM_FM_EC1_OAMSK(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_EC1_OAMSK"
            
            def description(self):
                return "Interrupt STS1 OR AND MASK"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_EC1_OAMSK"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1_Interrupt_OR_AND_MASK_Per_STS1._FMPM_FM_EC1_OAMSK()
            return allFields

    class _FMPM_FM_EC1_Interrupt_MASK_Per_STS1(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM EC1 Interrupt MASK Per STS1"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x18110+$L*0x2+$M"
            
        def startAddress(self):
            return 0x00018110
            
        def endAddress(self):
            return 0x0001811f

        class _FMPM_FM_EC1_MSK(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_EC1_MSK"
            
            def description(self):
                return "Interrupt MASK per STS1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_EC1_MSK"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1_Interrupt_MASK_Per_STS1._FMPM_FM_EC1_MSK()
            return allFields

    class _FMPM_FM_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM EC1STM0 Interrupt Sticky Per Type Of Per STS1"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x1B600+$L*0x40+$M*0x20+$N"
            
        def startAddress(self):
            return 0x0001b600
            
        def endAddress(self):
            return 0x0001b7ff

        class _FMPM_FM_EC1STM0_STK_LOS_S(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_FM_EC1STM0_STK_LOS_S"
            
            def description(self):
                return "LOS_S"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_EC1STM0_STK_LOF_S(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_FM_EC1STM0_STK_LOF_S"
            
            def description(self):
                return "LOF_S"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_EC1STM0_STK_AIS_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_FM_EC1STM0_STK_AIS_L"
            
            def description(self):
                return "AIS_L"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_EC1STM0_STK_RFI_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_FM_EC1STM0_STK_RFI_L"
            
            def description(self):
                return "RFI_L"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_EC1STM0_STK_TIM_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_FM_EC1STM0_STK_TIM_L"
            
            def description(self):
                return "TIM_L"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_EC1STM0_STK_BERSD_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_FM_EC1STM0_STK_BERSD_L"
            
            def description(self):
                return "BERSD_L"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_EC1STM0_STK_BERSF_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_EC1STM0_STK_BERSF_L"
            
            def description(self):
                return "BERSF_L"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_EC1STM0_STK_LOS_S"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_FM_EC1STM0_STK_LOS_S()
            allFields["FMPM_FM_EC1STM0_STK_LOF_S"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_FM_EC1STM0_STK_LOF_S()
            allFields["FMPM_FM_EC1STM0_STK_AIS_L"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_FM_EC1STM0_STK_AIS_L()
            allFields["FMPM_FM_EC1STM0_STK_RFI_L"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_FM_EC1STM0_STK_RFI_L()
            allFields["FMPM_FM_EC1STM0_STK_TIM_L"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_FM_EC1STM0_STK_TIM_L()
            allFields["FMPM_FM_EC1STM0_STK_BERSD_L"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_FM_EC1STM0_STK_BERSD_L()
            allFields["FMPM_FM_EC1STM0_STK_BERSF_L"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_FM_EC1STM0_STK_BERSF_L()
            return allFields

    class _FMPM_FM_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM EC1STM0 Interrupt MASK Per Type Of Per STS1"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1B800+$L*0x40+$M*0x20+$N"
            
        def startAddress(self):
            return 0x0001b800
            
        def endAddress(self):
            return 0x0001b9ff

        class _FMPM_FM_EC1STM0_HI_AIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "FMPM_FM_EC1STM0_HI_AIS"
            
            def description(self):
                return "High level AIS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_EC1STM0_LOS_S(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_FM_EC1STM0_LOS_S"
            
            def description(self):
                return "LOS_S"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_EC1STM0_LOF_S(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_FM_EC1STM0_LOF_S"
            
            def description(self):
                return "LOF_S"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_EC1STM0_AIS_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_FM_EC1STM0_AIS_L"
            
            def description(self):
                return "AIS_L"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_EC1STM0_RFI_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_FM_EC1STM0_RFI_L"
            
            def description(self):
                return "RFI_L"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_EC1STM0_TIM_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_FM_EC1STM0_TIM_L"
            
            def description(self):
                return "TIM_L"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_EC1STM0_BERSD_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_FM_EC1STM0_BERSD_L"
            
            def description(self):
                return "BERSD_L"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_EC1STM0_BERSF_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_EC1STM0_BERSF_L"
            
            def description(self):
                return "BERSF_L"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_EC1STM0_HI_AIS"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_FM_EC1STM0_HI_AIS()
            allFields["FMPM_FM_EC1STM0_LOS_S"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_FM_EC1STM0_LOS_S()
            allFields["FMPM_FM_EC1STM0_LOF_S"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_FM_EC1STM0_LOF_S()
            allFields["FMPM_FM_EC1STM0_AIS_L"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_FM_EC1STM0_AIS_L()
            allFields["FMPM_FM_EC1STM0_RFI_L"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_FM_EC1STM0_RFI_L()
            allFields["FMPM_FM_EC1STM0_TIM_L"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_FM_EC1STM0_TIM_L()
            allFields["FMPM_FM_EC1STM0_BERSD_L"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_FM_EC1STM0_BERSD_L()
            allFields["FMPM_FM_EC1STM0_BERSF_L"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_FM_EC1STM0_BERSF_L()
            return allFields

    class _FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM EC1STM0 Interrupt Current Status Per Type Of Per STS1"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x1BA00+$L*0x40+$M*0x20+$N"
            
        def startAddress(self):
            return 0x0001ba00
            
        def endAddress(self):
            return 0x0001bbff

        class _FMPM_FM_EC1STM0_AMSK_HI_AIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "FMPM_FM_EC1STM0_AMSK_HI_AIS"
            
            def description(self):
                return "High level AIS"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_EC1STM0_AMSK_LOS_S(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "FMPM_FM_EC1STM0_AMSK_LOS_S"
            
            def description(self):
                return "LOS_S"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_EC1STM0_AMSK_LOF_S(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "FMPM_FM_EC1STM0_AMSK_LOF_S"
            
            def description(self):
                return "LOF_S"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_EC1STM0_AMSK_AIS_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "FMPM_FM_EC1STM0_AMSK_AIS_L"
            
            def description(self):
                return "AIS_L"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_EC1STM0_AMSK_RFI_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "FMPM_FM_EC1STM0_AMSK_RFI_L"
            
            def description(self):
                return "RFI_L"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_EC1STM0_AMSK_TIM_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "FMPM_FM_EC1STM0_AMSK_TIM_L"
            
            def description(self):
                return "TIM_L"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_EC1STM0_AMSK_BERSD_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "FMPM_FM_EC1STM0_AMSK_BERSD_L"
            
            def description(self):
                return "BERSD_L"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_EC1STM0_AMSK_BERSF_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "FMPM_FM_EC1STM0_AMSK_BERSF_L"
            
            def description(self):
                return "BERSF_L"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_EC1STM0_CUR_HI_AIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "FMPM_FM_EC1STM0_CUR_HI_AIS"
            
            def description(self):
                return "High level AIS"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_EC1STM0_CUR_LOS_S(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_FM_EC1STM0_CUR_LOS_S"
            
            def description(self):
                return "LOS_S"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_EC1STM0_CUR_LOF_S(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_FM_EC1STM0_CUR_LOF_S"
            
            def description(self):
                return "LOF_S"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_EC1STM0_CUR_AIS_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_FM_EC1STM0_CUR_AIS_L"
            
            def description(self):
                return "AIS_L"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_EC1STM0_CUR_RFI_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_FM_EC1STM0_CUR_RFI_L"
            
            def description(self):
                return "RFI_L"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_EC1STM0_CUR_TIM_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_FM_EC1STM0_CUR_TIM_L"
            
            def description(self):
                return "TIM_L"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_EC1STM0_CUR_BERSD_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_FM_EC1STM0_CUR_BERSD_L"
            
            def description(self):
                return "BERSD_L"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_EC1STM0_CUR_BERSF_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_EC1STM0_CUR_BERSF_L"
            
            def description(self):
                return "BERSF_L"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_EC1STM0_AMSK_HI_AIS"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_EC1STM0_AMSK_HI_AIS()
            allFields["FMPM_FM_EC1STM0_AMSK_LOS_S"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_EC1STM0_AMSK_LOS_S()
            allFields["FMPM_FM_EC1STM0_AMSK_LOF_S"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_EC1STM0_AMSK_LOF_S()
            allFields["FMPM_FM_EC1STM0_AMSK_AIS_L"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_EC1STM0_AMSK_AIS_L()
            allFields["FMPM_FM_EC1STM0_AMSK_RFI_L"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_EC1STM0_AMSK_RFI_L()
            allFields["FMPM_FM_EC1STM0_AMSK_TIM_L"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_EC1STM0_AMSK_TIM_L()
            allFields["FMPM_FM_EC1STM0_AMSK_BERSD_L"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_EC1STM0_AMSK_BERSD_L()
            allFields["FMPM_FM_EC1STM0_AMSK_BERSF_L"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_EC1STM0_AMSK_BERSF_L()
            allFields["FMPM_FM_EC1STM0_CUR_HI_AIS"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_EC1STM0_CUR_HI_AIS()
            allFields["FMPM_FM_EC1STM0_CUR_LOS_S"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_EC1STM0_CUR_LOS_S()
            allFields["FMPM_FM_EC1STM0_CUR_LOF_S"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_EC1STM0_CUR_LOF_S()
            allFields["FMPM_FM_EC1STM0_CUR_AIS_L"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_EC1STM0_CUR_AIS_L()
            allFields["FMPM_FM_EC1STM0_CUR_RFI_L"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_EC1STM0_CUR_RFI_L()
            allFields["FMPM_FM_EC1STM0_CUR_TIM_L"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_EC1STM0_CUR_TIM_L()
            allFields["FMPM_FM_EC1STM0_CUR_BERSD_L"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_EC1STM0_CUR_BERSD_L()
            allFields["FMPM_FM_EC1STM0_CUR_BERSF_L"] = _AF6CNC0021_RD_PM._FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_EC1STM0_CUR_BERSF_L()
            return allFields

    class _FMPM_FM_STS24_Interrupt_OR(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM STS24 Interrupt OR"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00018000
            
        def endAddress(self):
            return 0xffffffff

        class _FMPM_FM_STS24_OR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_STS24_OR"
            
            def description(self):
                return "Interrupt STS24 OR"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_STS24_OR"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_OR._FMPM_FM_STS24_OR()
            return allFields

    class _FMPM_FM_STS24_Interrupt_OR_AND_MASK_Per_STS1(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM STS24 Interrupt OR AND MASK Per STS1"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x18080+$L*0x2+$M"
            
        def startAddress(self):
            return 0x00018080
            
        def endAddress(self):
            return 0x0001808f

        class _FMPM_FM_STS1_OAMSK(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_STS1_OAMSK"
            
            def description(self):
                return "Interrupt STS1 OR AND MASK"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_STS1_OAMSK"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_OR_AND_MASK_Per_STS1._FMPM_FM_STS1_OAMSK()
            return allFields

    class _FMPM_FM_STS24_Interrupt_MASK_Per_STS1(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM STS24 Interrupt MASK Per STS1"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x18090+$L*0x2+$M"
            
        def startAddress(self):
            return 0x00018090
            
        def endAddress(self):
            return 0x0001809f

        class _FMPM_FM_STS1_MSK(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_STS1_MSK"
            
            def description(self):
                return "Interrupt MASK per STS1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_STS1_MSK"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_MASK_Per_STS1._FMPM_FM_STS1_MSK()
            return allFields

    class _FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM STS24 Interrupt Sticky Per Type Of Per STS1"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x1A000+$L*0x40+$M*0x20+$N"
            
        def startAddress(self):
            return 0x0001a000
            
        def endAddress(self):
            return 0x0001a1ff

        class _FMPM_FM_STS1_STK_RFI_SER(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "FMPM_FM_STS1_STK_RFI_SER"
            
            def description(self):
                return "RFI server"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_STK_RFI_CON(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "FMPM_FM_STS1_STK_RFI_CON"
            
            def description(self):
                return "RFI connectivity"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_STK_RFI_PAY(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "FMPM_FM_STS1_STK_RFI_PAY"
            
            def description(self):
                return "RFI payload"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_STK_LOM(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "FMPM_FM_STS1_STK_LOM"
            
            def description(self):
                return "LOM"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_STK_RFI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "FMPM_FM_STS1_STK_RFI"
            
            def description(self):
                return "one-bit RFI"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_STK_AIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_FM_STS1_STK_AIS"
            
            def description(self):
                return "AIS"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_STK_TIM(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_FM_STS1_STK_TIM"
            
            def description(self):
                return "TIM"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_STK_UNEQ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_FM_STS1_STK_UNEQ"
            
            def description(self):
                return "UNEQ"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_STK_PLM(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_FM_STS1_STK_PLM"
            
            def description(self):
                return "PLM"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_STK_LOP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_FM_STS1_STK_LOP"
            
            def description(self):
                return "LOP"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_STK_BERSD(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_FM_STS1_STK_BERSD"
            
            def description(self):
                return "BERSD"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_STK_BERSF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_STS1_STK_BERSF"
            
            def description(self):
                return "BERSF"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_STS1_STK_RFI_SER"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_FM_STS1_STK_RFI_SER()
            allFields["FMPM_FM_STS1_STK_RFI_CON"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_FM_STS1_STK_RFI_CON()
            allFields["FMPM_FM_STS1_STK_RFI_PAY"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_FM_STS1_STK_RFI_PAY()
            allFields["FMPM_FM_STS1_STK_LOM"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_FM_STS1_STK_LOM()
            allFields["FMPM_FM_STS1_STK_RFI"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_FM_STS1_STK_RFI()
            allFields["FMPM_FM_STS1_STK_AIS"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_FM_STS1_STK_AIS()
            allFields["FMPM_FM_STS1_STK_TIM"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_FM_STS1_STK_TIM()
            allFields["FMPM_FM_STS1_STK_UNEQ"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_FM_STS1_STK_UNEQ()
            allFields["FMPM_FM_STS1_STK_PLM"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_FM_STS1_STK_PLM()
            allFields["FMPM_FM_STS1_STK_LOP"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_FM_STS1_STK_LOP()
            allFields["FMPM_FM_STS1_STK_BERSD"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_FM_STS1_STK_BERSD()
            allFields["FMPM_FM_STS1_STK_BERSF"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_FM_STS1_STK_BERSF()
            return allFields

    class _FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM STS24 Interrupt MASK Per Type Of Per STS1"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1A200+$L*0x40+$M*0x20+$N"
            
        def startAddress(self):
            return 0x0001a200
            
        def endAddress(self):
            return 0x0001a3ff

        class _FMPM_FM_STS1_MSK_HI_AIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "FMPM_FM_STS1_MSK_HI_AIS"
            
            def description(self):
                return "High level AIS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_MSK_RFI_SER(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "FMPM_FM_STS1_MSK_RFI_SER"
            
            def description(self):
                return "RFI server"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_MSK_RFI_CON(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "FMPM_FM_STS1_MSK_RFI_CON"
            
            def description(self):
                return "RFI connectivity"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_MSK_RFI_PAY(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "FMPM_FM_STS1_MSK_RFI_PAY"
            
            def description(self):
                return "RFI payload"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_MSK_LOM(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "FMPM_FM_STS1_MSK_LOM"
            
            def description(self):
                return "LOM"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_MSK_RFI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "FMPM_FM_STS1_MSK_RFI"
            
            def description(self):
                return "one-bit RFI"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_MSK_AIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_FM_STS1_MSK_AIS"
            
            def description(self):
                return "AIS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_MSK_TIM(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_FM_STS1_MSK_TIM"
            
            def description(self):
                return "TIM"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_MSK_UNEQ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_FM_STS1_MSK_UNEQ"
            
            def description(self):
                return "UNEQ"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_MSK_PLM(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_FM_STS1_MSK_PLM"
            
            def description(self):
                return "PLM"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_MSK_LOP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_FM_STS1_MSK_LOP"
            
            def description(self):
                return "LOP"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_MSK_BERSD(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_FM_STS1_MSK_BERSD"
            
            def description(self):
                return "BERSD"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_MSK_BERSF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_STS1_MSK_BERSF"
            
            def description(self):
                return "BERSF"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_STS1_MSK_HI_AIS"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_FM_STS1_MSK_HI_AIS()
            allFields["FMPM_FM_STS1_MSK_RFI_SER"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_FM_STS1_MSK_RFI_SER()
            allFields["FMPM_FM_STS1_MSK_RFI_CON"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_FM_STS1_MSK_RFI_CON()
            allFields["FMPM_FM_STS1_MSK_RFI_PAY"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_FM_STS1_MSK_RFI_PAY()
            allFields["FMPM_FM_STS1_MSK_LOM"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_FM_STS1_MSK_LOM()
            allFields["FMPM_FM_STS1_MSK_RFI"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_FM_STS1_MSK_RFI()
            allFields["FMPM_FM_STS1_MSK_AIS"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_FM_STS1_MSK_AIS()
            allFields["FMPM_FM_STS1_MSK_TIM"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_FM_STS1_MSK_TIM()
            allFields["FMPM_FM_STS1_MSK_UNEQ"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_FM_STS1_MSK_UNEQ()
            allFields["FMPM_FM_STS1_MSK_PLM"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_FM_STS1_MSK_PLM()
            allFields["FMPM_FM_STS1_MSK_LOP"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_FM_STS1_MSK_LOP()
            allFields["FMPM_FM_STS1_MSK_BERSD"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_FM_STS1_MSK_BERSD()
            allFields["FMPM_FM_STS1_MSK_BERSF"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_FM_STS1_MSK_BERSF()
            return allFields

    class _FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM STS24 Interrupt Current Status Per Type Of Per STS1"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x1A400+$L*0x40+$M*0x20+$N"
            
        def startAddress(self):
            return 0x0001a400
            
        def endAddress(self):
            return 0x0001a5ff

        class _FMPM_FM_STS1_AMSK_HI_AIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "FMPM_FM_STS1_AMSK_HI_AIS"
            
            def description(self):
                return "High level AIS"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_AMSK_RFI_SER(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "FMPM_FM_STS1_AMSK_RFI_SER"
            
            def description(self):
                return "RFI server"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_AMSK_RFI_CON(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "FMPM_FM_STS1_AMSK_RFI_CON"
            
            def description(self):
                return "RFI connectivity"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_AMSK_RFI_PAY(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "FMPM_FM_STS1_AMSK_RFI_PAY"
            
            def description(self):
                return "RFI payload"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_AMSK_LOM(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "FMPM_FM_STS1_AMSK_LOM"
            
            def description(self):
                return "LOM"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_AMSK_RFI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "FMPM_FM_STS1_AMSK_RFI"
            
            def description(self):
                return "one-bit RFI"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_AMSK_AIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "FMPM_FM_STS1_AMSK_AIS"
            
            def description(self):
                return "AIS"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_AMSK_TIM(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "FMPM_FM_STS1_AMSK_TIM"
            
            def description(self):
                return "TIM"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_AMSK_UNEQ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "FMPM_FM_STS1_AMSK_UNEQ"
            
            def description(self):
                return "UNEQ"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_AMSK_PLM(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "FMPM_FM_STS1_AMSK_PLM"
            
            def description(self):
                return "PLM"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_AMSK_LOP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "FMPM_FM_STS1_AMSK_LOP"
            
            def description(self):
                return "LOP"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_AMSK_BERSD(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "FMPM_FM_STS1_AMSK_BERSD"
            
            def description(self):
                return "BERSD"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_AMSK_BERSF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "FMPM_FM_STS1_AMSK_BERSF"
            
            def description(self):
                return "BERSF"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_CUR_HI_AIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "FMPM_FM_STS1_CUR_HI_AIS"
            
            def description(self):
                return "High level AIS"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_CUR_RFI_SER(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "FMPM_FM_STS1_CUR_RFI_SER"
            
            def description(self):
                return "RFI server"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_CUR_RFI_CON(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "FMPM_FM_STS1_CUR_RFI_CON"
            
            def description(self):
                return "RFI connectivity"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_CUR_RFI_PAY(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "FMPM_FM_STS1_CUR_RFI_PAY"
            
            def description(self):
                return "RFI payload"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_CUR_LOM(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "FMPM_FM_STS1_CUR_LOM"
            
            def description(self):
                return "LOM"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_CUR_RFI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "FMPM_FM_STS1_CUR_RFI"
            
            def description(self):
                return "one-bit RFI"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_CUR_AIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_FM_STS1_CUR_AIS"
            
            def description(self):
                return "AIS"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_CUR_TIM(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_FM_STS1_CUR_TIM"
            
            def description(self):
                return "TIM"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_CUR_UNEQ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_FM_STS1_CUR_UNEQ"
            
            def description(self):
                return "UNEQ"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_CUR_PLM(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_FM_STS1_CUR_PLM"
            
            def description(self):
                return "PLM"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_CUR_LOP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_FM_STS1_CUR_LOP"
            
            def description(self):
                return "LOP"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_CUR_BERSD(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_FM_STS1_CUR_BERSD"
            
            def description(self):
                return "BERSD"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_STS1_CUR_BERSF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_STS1_CUR_BERSF"
            
            def description(self):
                return "BERSF"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_STS1_AMSK_HI_AIS"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_STS1_AMSK_HI_AIS()
            allFields["FMPM_FM_STS1_AMSK_RFI_SER"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_STS1_AMSK_RFI_SER()
            allFields["FMPM_FM_STS1_AMSK_RFI_CON"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_STS1_AMSK_RFI_CON()
            allFields["FMPM_FM_STS1_AMSK_RFI_PAY"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_STS1_AMSK_RFI_PAY()
            allFields["FMPM_FM_STS1_AMSK_LOM"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_STS1_AMSK_LOM()
            allFields["FMPM_FM_STS1_AMSK_RFI"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_STS1_AMSK_RFI()
            allFields["FMPM_FM_STS1_AMSK_AIS"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_STS1_AMSK_AIS()
            allFields["FMPM_FM_STS1_AMSK_TIM"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_STS1_AMSK_TIM()
            allFields["FMPM_FM_STS1_AMSK_UNEQ"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_STS1_AMSK_UNEQ()
            allFields["FMPM_FM_STS1_AMSK_PLM"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_STS1_AMSK_PLM()
            allFields["FMPM_FM_STS1_AMSK_LOP"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_STS1_AMSK_LOP()
            allFields["FMPM_FM_STS1_AMSK_BERSD"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_STS1_AMSK_BERSD()
            allFields["FMPM_FM_STS1_AMSK_BERSF"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_STS1_AMSK_BERSF()
            allFields["FMPM_FM_STS1_CUR_HI_AIS"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_STS1_CUR_HI_AIS()
            allFields["FMPM_FM_STS1_CUR_RFI_SER"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_STS1_CUR_RFI_SER()
            allFields["FMPM_FM_STS1_CUR_RFI_CON"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_STS1_CUR_RFI_CON()
            allFields["FMPM_FM_STS1_CUR_RFI_PAY"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_STS1_CUR_RFI_PAY()
            allFields["FMPM_FM_STS1_CUR_LOM"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_STS1_CUR_LOM()
            allFields["FMPM_FM_STS1_CUR_RFI"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_STS1_CUR_RFI()
            allFields["FMPM_FM_STS1_CUR_AIS"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_STS1_CUR_AIS()
            allFields["FMPM_FM_STS1_CUR_TIM"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_STS1_CUR_TIM()
            allFields["FMPM_FM_STS1_CUR_UNEQ"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_STS1_CUR_UNEQ()
            allFields["FMPM_FM_STS1_CUR_PLM"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_STS1_CUR_PLM()
            allFields["FMPM_FM_STS1_CUR_LOP"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_STS1_CUR_LOP()
            allFields["FMPM_FM_STS1_CUR_BERSD"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_STS1_CUR_BERSD()
            allFields["FMPM_FM_STS1_CUR_BERSF"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_FM_STS1_CUR_BERSF()
            return allFields

    class _FMPM_FM_DS3E3_Group_Interrupt_OR(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM DS3E3 Group Interrupt OR"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00018001
            
        def endAddress(self):
            return 0xffffffff

        class _FMPM_FM_DS3E3_OR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_DS3E3_OR"
            
            def description(self):
                return "Interrupt DS3/E3 OR"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_DS3E3_OR"] = _AF6CNC0021_RD_PM._FMPM_FM_DS3E3_Group_Interrupt_OR._FMPM_FM_DS3E3_OR()
            return allFields

    class _FMPM_FM_DS3E3_Framer_Interrupt_OR_AND_MASK_Per_DS3E3(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM DS3E3 Framer Interrupt OR AND MASK Per DS3E3"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x180A0+$G"
            
        def startAddress(self):
            return 0x000180a0
            
        def endAddress(self):
            return 0x000180af

        class _FMPM_FM_DS3E3_OAMSK(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_DS3E3_OAMSK"
            
            def description(self):
                return "Interrupt DS3/E3 Group"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_DS3E3_OAMSK"] = _AF6CNC0021_RD_PM._FMPM_FM_DS3E3_Framer_Interrupt_OR_AND_MASK_Per_DS3E3._FMPM_FM_DS3E3_OAMSK()
            return allFields

    class _FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_DS3E3(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM DS3E3 Framer Interrupt MASK Per DS3E3"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x180B0+$G"
            
        def startAddress(self):
            return 0x000180b0
            
        def endAddress(self):
            return 0x000180bf

        class _FMPM_FM_DS3E3_MSK(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_DS3E3_MSK"
            
            def description(self):
                return "MASK per DS3/E3 Framer"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_DS3E3_MSK"] = _AF6CNC0021_RD_PM._FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_DS3E3._FMPM_FM_DS3E3_MSK()
            return allFields

    class _FMPM_FM_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM DS3E3 Framer Interrupt Sticky Per Type of Per DS3E3"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x1A600+$G*0x20+$H"
            
        def startAddress(self):
            return 0x0001a600
            
        def endAddress(self):
            return 0x0001a7ff

        class _FMPM_FM_DS3E3_STK_RAI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_FM_DS3E3_STK_RAI"
            
            def description(self):
                return "RAI"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS3E3_STK_AIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_FM_DS3E3_STK_AIS"
            
            def description(self):
                return "AIS"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS3E3_STK_SEF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_FM_DS3E3_STK_SEF"
            
            def description(self):
                return "SEF"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS3E3_STK_LOF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_FM_DS3E3_STK_LOF"
            
            def description(self):
                return "LOF"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS3E3_STK_LOS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_DS3E3_STK_LOS"
            
            def description(self):
                return "LOS"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_DS3E3_STK_RAI"] = _AF6CNC0021_RD_PM._FMPM_FM_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3._FMPM_FM_DS3E3_STK_RAI()
            allFields["FMPM_FM_DS3E3_STK_AIS"] = _AF6CNC0021_RD_PM._FMPM_FM_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3._FMPM_FM_DS3E3_STK_AIS()
            allFields["FMPM_FM_DS3E3_STK_SEF"] = _AF6CNC0021_RD_PM._FMPM_FM_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3._FMPM_FM_DS3E3_STK_SEF()
            allFields["FMPM_FM_DS3E3_STK_LOF"] = _AF6CNC0021_RD_PM._FMPM_FM_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3._FMPM_FM_DS3E3_STK_LOF()
            allFields["FMPM_FM_DS3E3_STK_LOS"] = _AF6CNC0021_RD_PM._FMPM_FM_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3._FMPM_FM_DS3E3_STK_LOS()
            return allFields

    class _FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM DS3E3 Framer Interrupt MASK Per Type of Per DS3E3"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1A800+$G*0x20+$H"
            
        def startAddress(self):
            return 0x0001a800
            
        def endAddress(self):
            return 0x0001a9ff

        class _FMPM_FM_DS3E3_MSK_HI_AIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_FM_DS3E3_MSK_HI_AIS"
            
            def description(self):
                return "High level AIS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS3E3_MSK_RAI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_FM_DS3E3_MSK_RAI"
            
            def description(self):
                return "RAI"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS3E3_MSK_AIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_FM_DS3E3_MSK_AIS"
            
            def description(self):
                return "AIS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS3E3_MSK_SEF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_FM_DS3E3_MSK_SEF"
            
            def description(self):
                return "SEF"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS3E3_MSK_LOF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_FM_DS3E3_MSK_LOF"
            
            def description(self):
                return "LOF"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS3E3_MSK_LOS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_DS3E3_MSK_LOS"
            
            def description(self):
                return "LOS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_DS3E3_MSK_HI_AIS"] = _AF6CNC0021_RD_PM._FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3._FMPM_FM_DS3E3_MSK_HI_AIS()
            allFields["FMPM_FM_DS3E3_MSK_RAI"] = _AF6CNC0021_RD_PM._FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3._FMPM_FM_DS3E3_MSK_RAI()
            allFields["FMPM_FM_DS3E3_MSK_AIS"] = _AF6CNC0021_RD_PM._FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3._FMPM_FM_DS3E3_MSK_AIS()
            allFields["FMPM_FM_DS3E3_MSK_SEF"] = _AF6CNC0021_RD_PM._FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3._FMPM_FM_DS3E3_MSK_SEF()
            allFields["FMPM_FM_DS3E3_MSK_LOF"] = _AF6CNC0021_RD_PM._FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3._FMPM_FM_DS3E3_MSK_LOF()
            allFields["FMPM_FM_DS3E3_MSK_LOS"] = _AF6CNC0021_RD_PM._FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3._FMPM_FM_DS3E3_MSK_LOS()
            return allFields

    class _FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM DS3E3 Framer Interrupt Current Status Per Type of Per DS3E3"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x1AA00+$G*0x20+$H"
            
        def startAddress(self):
            return 0x0001aa00
            
        def endAddress(self):
            return 0x0001abff

        class _FMPM_FM_DS3E3_AMSK_HI_AIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "FMPM_FM_DS3E3_AMSK_HI_AIS"
            
            def description(self):
                return "High level AIS"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS3E3_AMSK_RAI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "FMPM_FM_DS3E3_AMSK_RAI"
            
            def description(self):
                return "RAI"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS3E3_AMSK_AIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "FMPM_FM_DS3E3_AMSK_AIS"
            
            def description(self):
                return "AIS"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS3E3_AMSK_SEF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "FMPM_FM_DS3E3_AMSK_SEF"
            
            def description(self):
                return "SEF"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS3E3_AMSK_LOF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "FMPM_FM_DS3E3_AMSK_LOF"
            
            def description(self):
                return "LOF"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS3E3_AMSK_LOS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "FMPM_FM_DS3E3_AMSK_LOS"
            
            def description(self):
                return "LOS"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS3E3_CUR_HI_AIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_FM_DS3E3_CUR_HI_AIS"
            
            def description(self):
                return "High level AIS"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS3E3_CUR_RAI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_FM_DS3E3_CUR_RAI"
            
            def description(self):
                return "RAI"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS3E3_CUR_AIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_FM_DS3E3_CUR_AIS"
            
            def description(self):
                return "AIS"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS3E3_CUR_SEF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_FM_DS3E3_CUR_SEF"
            
            def description(self):
                return "SEF"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS3E3_CUR_LOF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_FM_DS3E3_CUR_LOF"
            
            def description(self):
                return "LOF"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS3E3_CUR_LOS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_DS3E3_CUR_LOS"
            
            def description(self):
                return "LOS"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_DS3E3_AMSK_HI_AIS"] = _AF6CNC0021_RD_PM._FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_FM_DS3E3_AMSK_HI_AIS()
            allFields["FMPM_FM_DS3E3_AMSK_RAI"] = _AF6CNC0021_RD_PM._FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_FM_DS3E3_AMSK_RAI()
            allFields["FMPM_FM_DS3E3_AMSK_AIS"] = _AF6CNC0021_RD_PM._FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_FM_DS3E3_AMSK_AIS()
            allFields["FMPM_FM_DS3E3_AMSK_SEF"] = _AF6CNC0021_RD_PM._FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_FM_DS3E3_AMSK_SEF()
            allFields["FMPM_FM_DS3E3_AMSK_LOF"] = _AF6CNC0021_RD_PM._FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_FM_DS3E3_AMSK_LOF()
            allFields["FMPM_FM_DS3E3_AMSK_LOS"] = _AF6CNC0021_RD_PM._FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_FM_DS3E3_AMSK_LOS()
            allFields["FMPM_FM_DS3E3_CUR_HI_AIS"] = _AF6CNC0021_RD_PM._FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_FM_DS3E3_CUR_HI_AIS()
            allFields["FMPM_FM_DS3E3_CUR_RAI"] = _AF6CNC0021_RD_PM._FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_FM_DS3E3_CUR_RAI()
            allFields["FMPM_FM_DS3E3_CUR_AIS"] = _AF6CNC0021_RD_PM._FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_FM_DS3E3_CUR_AIS()
            allFields["FMPM_FM_DS3E3_CUR_SEF"] = _AF6CNC0021_RD_PM._FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_FM_DS3E3_CUR_SEF()
            allFields["FMPM_FM_DS3E3_CUR_LOF"] = _AF6CNC0021_RD_PM._FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_FM_DS3E3_CUR_LOF()
            allFields["FMPM_FM_DS3E3_CUR_LOS"] = _AF6CNC0021_RD_PM._FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_FM_DS3E3_CUR_LOS()
            return allFields

    class _FMPM_FM_STS24_LO_Interrupt_OR(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM STS24 LO Interrupt OR"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00018002
            
        def endAddress(self):
            return 0xffffffff

        class _FMPM_FM_STS24LO_OR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_STS24LO_OR"
            
            def description(self):
                return "Interrupt STS24 LO OR"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_STS24LO_OR"] = _AF6CNC0021_RD_PM._FMPM_FM_STS24_LO_Interrupt_OR._FMPM_FM_STS24LO_OR()
            return allFields

    class _FMPM_FM_STS1_LO_Interrupt_OR(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM STS1 LO Interrupt OR"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x180C0+$G"
            
        def startAddress(self):
            return 0x000180c0
            
        def endAddress(self):
            return 0x000180cf

        class _FMPM_FM_STS1LO_OR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_STS1LO_OR"
            
            def description(self):
                return "Interrupt STS1 LO OR"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_STS1LO_OR"] = _AF6CNC0021_RD_PM._FMPM_FM_STS1_LO_Interrupt_OR._FMPM_FM_STS1LO_OR()
            return allFields

    class _FMPM_FM_VTTU_Interrupt_OR_AND_MASK_Per_VTTU(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM VTTU Interrupt OR AND MASK Per VTTU"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x1AC00+$G*0x20+$H"
            
        def startAddress(self):
            return 0x0001ac00
            
        def endAddress(self):
            return 0x0001adff

        class _FMPM_FM_VTTU_OAMSK(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_VTTU_OAMSK"
            
            def description(self):
                return "VT/TU Interrupt"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_VTTU_OAMSK"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_OR_AND_MASK_Per_VTTU._FMPM_FM_VTTU_OAMSK()
            return allFields

    class _FMPM_FM_VTTU_Interrupt_MASK_Per_VTTU(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM VTTU Interrupt MASK Per VTTU"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1AE00+$G*0x20+$H"
            
        def startAddress(self):
            return 0x0001ae00
            
        def endAddress(self):
            return 0x0001afff

        class _FMPM_FM_VTTU_MSK(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_VTTU_MSK"
            
            def description(self):
                return "VT/TU Interrupt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_VTTU_MSK"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_MASK_Per_VTTU._FMPM_FM_VTTU_MSK()
            return allFields

    class _FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM VTTU Interrupt Sticky Per Type Per VTTU"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x24000+$G*0x400+$H*0x20+$I"
            
        def startAddress(self):
            return 0x00024000
            
        def endAddress(self):
            return 0x00027fff

        class _FMPM_FM_VTTU_STK_RFI_SER(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "FMPM_FM_VTTU_STK_RFI_SER"
            
            def description(self):
                return "RFI server"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_STK_RFI_CON(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "FMPM_FM_VTTU_STK_RFI_CON"
            
            def description(self):
                return "RFI connectivity"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_STK_RFI_PAY(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "FMPM_FM_VTTU_STK_RFI_PAY"
            
            def description(self):
                return "RFI payload"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_STK_RFI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "FMPM_FM_VTTU_STK_RFI"
            
            def description(self):
                return "one-bit RFI"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_STK_AIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_FM_VTTU_STK_AIS"
            
            def description(self):
                return "AIS"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_STK_TIM(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_FM_VTTU_STK_TIM"
            
            def description(self):
                return "TIM"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_STK_UNEQ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_FM_VTTU_STK_UNEQ"
            
            def description(self):
                return "UNEQ"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_STK_PLM(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_FM_VTTU_STK_PLM"
            
            def description(self):
                return "PLM"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_STK_LOP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_FM_VTTU_STK_LOP"
            
            def description(self):
                return "LOP"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_STK_BERSD(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_FM_VTTU_STK_BERSD"
            
            def description(self):
                return "BERSD"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_STK_BERSF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_VTTU_STK_BERSF"
            
            def description(self):
                return "BERSF"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_VTTU_STK_RFI_SER"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU._FMPM_FM_VTTU_STK_RFI_SER()
            allFields["FMPM_FM_VTTU_STK_RFI_CON"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU._FMPM_FM_VTTU_STK_RFI_CON()
            allFields["FMPM_FM_VTTU_STK_RFI_PAY"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU._FMPM_FM_VTTU_STK_RFI_PAY()
            allFields["FMPM_FM_VTTU_STK_RFI"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU._FMPM_FM_VTTU_STK_RFI()
            allFields["FMPM_FM_VTTU_STK_AIS"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU._FMPM_FM_VTTU_STK_AIS()
            allFields["FMPM_FM_VTTU_STK_TIM"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU._FMPM_FM_VTTU_STK_TIM()
            allFields["FMPM_FM_VTTU_STK_UNEQ"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU._FMPM_FM_VTTU_STK_UNEQ()
            allFields["FMPM_FM_VTTU_STK_PLM"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU._FMPM_FM_VTTU_STK_PLM()
            allFields["FMPM_FM_VTTU_STK_LOP"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU._FMPM_FM_VTTU_STK_LOP()
            allFields["FMPM_FM_VTTU_STK_BERSD"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU._FMPM_FM_VTTU_STK_BERSD()
            allFields["FMPM_FM_VTTU_STK_BERSF"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU._FMPM_FM_VTTU_STK_BERSF()
            return allFields

    class _FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM VTTU Interrupt MASK Per Type Per VTTU"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x28000+$G*0x400+$H*0x20+$I"
            
        def startAddress(self):
            return 0x00028000
            
        def endAddress(self):
            return 0x0002bfff

        class _FMPM_FM_VTTU_MSK_HI_AIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "FMPM_FM_VTTU_MSK_HI_AIS"
            
            def description(self):
                return "High level AIS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_MSK_RFI_SER(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "FMPM_FM_VTTU_MSK_RFI_SER"
            
            def description(self):
                return "RFI server"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_MSK_RFI_CON(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "FMPM_FM_VTTU_MSK_RFI_CON"
            
            def description(self):
                return "RFI connectivity"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_MSK_RFI_PAY(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "FMPM_FM_VTTU_MSK_RFI_PAY"
            
            def description(self):
                return "RFI payload"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_MSK_RFI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "FMPM_FM_VTTU_MSK_RFI"
            
            def description(self):
                return "one-bit RFI"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_MSK_AIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_FM_VTTU_MSK_AIS"
            
            def description(self):
                return "AIS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_MSK_TIM(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_FM_VTTU_MSK_TIM"
            
            def description(self):
                return "TIM"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_MSK_UNEQ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_FM_VTTU_MSK_UNEQ"
            
            def description(self):
                return "UNEQ"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_MSK_PLM(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_FM_VTTU_MSK_PLM"
            
            def description(self):
                return "PLM"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_MSK_LOP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_FM_VTTU_MSK_LOP"
            
            def description(self):
                return "LOP"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_MSK_BERSD(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_FM_VTTU_MSK_BERSD"
            
            def description(self):
                return "BERSD"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_MSK_BERSF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_VTTU_MSK_BERSF"
            
            def description(self):
                return "BERSF"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_VTTU_MSK_HI_AIS"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU._FMPM_FM_VTTU_MSK_HI_AIS()
            allFields["FMPM_FM_VTTU_MSK_RFI_SER"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU._FMPM_FM_VTTU_MSK_RFI_SER()
            allFields["FMPM_FM_VTTU_MSK_RFI_CON"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU._FMPM_FM_VTTU_MSK_RFI_CON()
            allFields["FMPM_FM_VTTU_MSK_RFI_PAY"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU._FMPM_FM_VTTU_MSK_RFI_PAY()
            allFields["FMPM_FM_VTTU_MSK_RFI"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU._FMPM_FM_VTTU_MSK_RFI()
            allFields["FMPM_FM_VTTU_MSK_AIS"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU._FMPM_FM_VTTU_MSK_AIS()
            allFields["FMPM_FM_VTTU_MSK_TIM"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU._FMPM_FM_VTTU_MSK_TIM()
            allFields["FMPM_FM_VTTU_MSK_UNEQ"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU._FMPM_FM_VTTU_MSK_UNEQ()
            allFields["FMPM_FM_VTTU_MSK_PLM"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU._FMPM_FM_VTTU_MSK_PLM()
            allFields["FMPM_FM_VTTU_MSK_LOP"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU._FMPM_FM_VTTU_MSK_LOP()
            allFields["FMPM_FM_VTTU_MSK_BERSD"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU._FMPM_FM_VTTU_MSK_BERSD()
            allFields["FMPM_FM_VTTU_MSK_BERSF"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU._FMPM_FM_VTTU_MSK_BERSF()
            return allFields

    class _FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM VTTU Interrupt Current Status Per Type Per VTTU"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x2C000+$G*0x400+$H*0x20+$I"
            
        def startAddress(self):
            return 0x0002c000
            
        def endAddress(self):
            return 0x0002ffff

        class _FMPM_FM_VTTU_AMSK_HI_AIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "FMPM_FM_VTTU_AMSK_HI_AIS"
            
            def description(self):
                return "High level AIS"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_AMSK_RFI_SER(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "FMPM_FM_VTTU_AMSK_RFI_SER"
            
            def description(self):
                return "RFI server"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_AMSK_RFI_CON(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "FMPM_FM_VTTU_AMSK_RFI_CON"
            
            def description(self):
                return "RFI connectivity"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_AMSK_RFI_PAY(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "FMPM_FM_VTTU_AMSK_RFI_PAY"
            
            def description(self):
                return "RFI payload"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_AMSK_RFI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "FMPM_FM_VTTU_AMSK_RFI"
            
            def description(self):
                return "one-bit"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_AMSK_AIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "FMPM_FM_VTTU_AMSK_AIS"
            
            def description(self):
                return "AIS"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_AMSK_TIM(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "FMPM_FM_VTTU_AMSK_TIM"
            
            def description(self):
                return "TIM"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_AMSK_UNEQ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "FMPM_FM_VTTU_AMSK_UNEQ"
            
            def description(self):
                return "UNEQ"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_AMSK_PLM(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "FMPM_FM_VTTU_AMSK_PLM"
            
            def description(self):
                return "PLM"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_AMSK_LOP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "FMPM_FM_VTTU_AMSK_LOP"
            
            def description(self):
                return "LOP"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_AMSK_BERSD(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "FMPM_FM_VTTU_AMSK_BERSD"
            
            def description(self):
                return "BERSD"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_AMSK_BERSF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "FMPM_FM_VTTU_AMSK_BERSF"
            
            def description(self):
                return "BERSF"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_CUR_HI_AIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "FMPM_FM_VTTU_CUR_HI_AIS"
            
            def description(self):
                return "High level AIS"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_CUR_RFI_SER(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "FMPM_FM_VTTU_CUR_RFI_SER"
            
            def description(self):
                return "RFI server"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_CUR_RFI_CON(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "FMPM_FM_VTTU_CUR_RFI_CON"
            
            def description(self):
                return "RFI connectivity"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_CUR_RFI_PAY(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "FMPM_FM_VTTU_CUR_RFI_PAY"
            
            def description(self):
                return "RFI payload"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_CUR_RFI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "FMPM_FM_VTTU_CUR_RFI"
            
            def description(self):
                return "one-bit"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_CUR_AIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_FM_VTTU_CUR_AIS"
            
            def description(self):
                return "AIS"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_CUR_TIM(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_FM_VTTU_CUR_TIM"
            
            def description(self):
                return "TIM"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_CUR_UNEQ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_FM_VTTU_CUR_UNEQ"
            
            def description(self):
                return "UNEQ"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_CUR_PLM(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_FM_VTTU_CUR_PLM"
            
            def description(self):
                return "PLM"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_CUR_LOP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_FM_VTTU_CUR_LOP"
            
            def description(self):
                return "LOP"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_CUR_BERSD(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_FM_VTTU_CUR_BERSD"
            
            def description(self):
                return "BERSD"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_VTTU_CUR_BERSF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_VTTU_CUR_BERSF"
            
            def description(self):
                return "BERSF"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_VTTU_AMSK_HI_AIS"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_FM_VTTU_AMSK_HI_AIS()
            allFields["FMPM_FM_VTTU_AMSK_RFI_SER"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_FM_VTTU_AMSK_RFI_SER()
            allFields["FMPM_FM_VTTU_AMSK_RFI_CON"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_FM_VTTU_AMSK_RFI_CON()
            allFields["FMPM_FM_VTTU_AMSK_RFI_PAY"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_FM_VTTU_AMSK_RFI_PAY()
            allFields["FMPM_FM_VTTU_AMSK_RFI"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_FM_VTTU_AMSK_RFI()
            allFields["FMPM_FM_VTTU_AMSK_AIS"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_FM_VTTU_AMSK_AIS()
            allFields["FMPM_FM_VTTU_AMSK_TIM"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_FM_VTTU_AMSK_TIM()
            allFields["FMPM_FM_VTTU_AMSK_UNEQ"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_FM_VTTU_AMSK_UNEQ()
            allFields["FMPM_FM_VTTU_AMSK_PLM"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_FM_VTTU_AMSK_PLM()
            allFields["FMPM_FM_VTTU_AMSK_LOP"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_FM_VTTU_AMSK_LOP()
            allFields["FMPM_FM_VTTU_AMSK_BERSD"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_FM_VTTU_AMSK_BERSD()
            allFields["FMPM_FM_VTTU_AMSK_BERSF"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_FM_VTTU_AMSK_BERSF()
            allFields["FMPM_FM_VTTU_CUR_HI_AIS"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_FM_VTTU_CUR_HI_AIS()
            allFields["FMPM_FM_VTTU_CUR_RFI_SER"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_FM_VTTU_CUR_RFI_SER()
            allFields["FMPM_FM_VTTU_CUR_RFI_CON"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_FM_VTTU_CUR_RFI_CON()
            allFields["FMPM_FM_VTTU_CUR_RFI_PAY"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_FM_VTTU_CUR_RFI_PAY()
            allFields["FMPM_FM_VTTU_CUR_RFI"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_FM_VTTU_CUR_RFI()
            allFields["FMPM_FM_VTTU_CUR_AIS"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_FM_VTTU_CUR_AIS()
            allFields["FMPM_FM_VTTU_CUR_TIM"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_FM_VTTU_CUR_TIM()
            allFields["FMPM_FM_VTTU_CUR_UNEQ"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_FM_VTTU_CUR_UNEQ()
            allFields["FMPM_FM_VTTU_CUR_PLM"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_FM_VTTU_CUR_PLM()
            allFields["FMPM_FM_VTTU_CUR_LOP"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_FM_VTTU_CUR_LOP()
            allFields["FMPM_FM_VTTU_CUR_BERSD"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_FM_VTTU_CUR_BERSD()
            allFields["FMPM_FM_VTTU_CUR_BERSF"] = _AF6CNC0021_RD_PM._FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_FM_VTTU_CUR_BERSF()
            return allFields

    class _FMPM_FM_DS1E1_Level1_Group_Interrupt_OR(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM DS1E1 Level1 Group Interrupt OR"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00018003
            
        def endAddress(self):
            return 0xffffffff

        class _FMPM_FM_DS1E1_LEV1_OR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_DS1E1_LEV1_OR"
            
            def description(self):
                return "Interrupt DS1E1 level1 group OR"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_DS1E1_LEV1_OR"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Level1_Group_Interrupt_OR._FMPM_FM_DS1E1_LEV1_OR()
            return allFields

    class _FMPM_FM_DS1E1_Level2_Group_Interrupt_OR(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM DS1E1 Level2 Group Interrupt OR"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x180D0+$G"
            
        def startAddress(self):
            return 0x000180d0
            
        def endAddress(self):
            return 0x000180df

        class _FMPM_FM_DS1E1_LEV2_OR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_DS1E1_LEV2_OR"
            
            def description(self):
                return "Level2 Group Interrupt OR"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_DS1E1_LEV2_OR"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Level2_Group_Interrupt_OR._FMPM_FM_DS1E1_LEV2_OR()
            return allFields

    class _FMPM_FM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM DS1E1 Framer Interrupt OR AND MASK Per DS1E1"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x1B000+$G*0x20+$H"
            
        def startAddress(self):
            return 0x0001b000
            
        def endAddress(self):
            return 0x0001b1ff

        class _FMPM_FM_DS1E1_OAMSK(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_DS1E1_OAMSK"
            
            def description(self):
                return "DS1/E1 Framer Interrupt"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_DS1E1_OAMSK"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1._FMPM_FM_DS1E1_OAMSK()
            return allFields

    class _FMPM_FM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM DS1E1 Framer Interrupt MASK Per DS1E1"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1B200+$G*0x20+$H"
            
        def startAddress(self):
            return 0x0001b200
            
        def endAddress(self):
            return 0x0001b3ff

        class _FMPM_FM_DS1E1_MSK(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_DS1E1_MSK"
            
            def description(self):
                return "DS1/E1 Framer Interrupt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_DS1E1_MSK"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1._FMPM_FM_DS1E1_MSK()
            return allFields

    class _FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM DS1E1 Interrupt Sticky Per Type Per DS1E1"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x30000+$G*0x400+$H*0x20+$I"
            
        def startAddress(self):
            return 0x00030000
            
        def endAddress(self):
            return 0x00033fff

        class _FMPM_FM_DS1E1_STK_RAI_CI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_FM_DS1E1_STK_RAI_CI"
            
            def description(self):
                return "RAI_CI"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_STK_RAI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_FM_DS1E1_STK_RAI"
            
            def description(self):
                return "RAI"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_STK_LOF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_FM_DS1E1_STK_LOF"
            
            def description(self):
                return "LOF"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_STK_AIS_CI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_FM_DS1E1_STK_AIS_CI"
            
            def description(self):
                return "AIS_CI"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_STK_AIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_FM_DS1E1_STK_AIS"
            
            def description(self):
                return "AIS"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_STK_LOS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_DS1E1_STK_LOS"
            
            def description(self):
                return "LOS"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_DS1E1_STK_RAI_CI"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_STK_RAI_CI()
            allFields["FMPM_FM_DS1E1_STK_RAI"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_STK_RAI()
            allFields["FMPM_FM_DS1E1_STK_LOF"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_STK_LOF()
            allFields["FMPM_FM_DS1E1_STK_AIS_CI"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_STK_AIS_CI()
            allFields["FMPM_FM_DS1E1_STK_AIS"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_STK_AIS()
            allFields["FMPM_FM_DS1E1_STK_LOS"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_STK_LOS()
            return allFields

    class _FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM DS1E1 Interrupt MASK Per Type Per DS1E1"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x34000+$G*0x400+$H*0x20+$I"
            
        def startAddress(self):
            return 0x00034000
            
        def endAddress(self):
            return 0x00037fff

        class _FMPM_FM_DS1E1_MSK_HI_AIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_FM_DS1E1_MSK_HI_AIS"
            
            def description(self):
                return "High level AIS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_MSK_RAI_CI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_FM_DS1E1_MSK_RAI_CI"
            
            def description(self):
                return "RAI_CI"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_MSK_RAI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_FM_DS1E1_MSK_RAI"
            
            def description(self):
                return "RAI"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_MSK_LOF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_FM_DS1E1_MSK_LOF"
            
            def description(self):
                return "LOF"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_MSK_AIS_CI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_FM_DS1E1_MSK_AIS_CI"
            
            def description(self):
                return "AIS_CI"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_MSK_AIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_FM_DS1E1_MSK_AIS"
            
            def description(self):
                return "AIS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_MSK_LOS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_DS1E1_MSK_LOS"
            
            def description(self):
                return "LOS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_DS1E1_MSK_HI_AIS"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_MSK_HI_AIS()
            allFields["FMPM_FM_DS1E1_MSK_RAI_CI"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_MSK_RAI_CI()
            allFields["FMPM_FM_DS1E1_MSK_RAI"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_MSK_RAI()
            allFields["FMPM_FM_DS1E1_MSK_LOF"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_MSK_LOF()
            allFields["FMPM_FM_DS1E1_MSK_AIS_CI"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_MSK_AIS_CI()
            allFields["FMPM_FM_DS1E1_MSK_AIS"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_MSK_AIS()
            allFields["FMPM_FM_DS1E1_MSK_LOS"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_MSK_LOS()
            return allFields

    class _FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM DS1E1 Interrupt Current Status Per Type Per DS1E1"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x38000+$G*0x400+$H*0x20+$I"
            
        def startAddress(self):
            return 0x00038000
            
        def endAddress(self):
            return 0x0003bfff

        class _FMPM_FM_DS1E1_AMSK_HI_AIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "FMPM_FM_DS1E1_AMSK_HI_AIS"
            
            def description(self):
                return "High level AIS"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_AMSK_RAI_CI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "FMPM_FM_DS1E1_AMSK_RAI_CI"
            
            def description(self):
                return "RAI_CI"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_AMSK_RAI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "FMPM_FM_DS1E1_AMSK_RAI"
            
            def description(self):
                return "RAI"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_AMSK_LOF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "FMPM_FM_DS1E1_AMSK_LOF"
            
            def description(self):
                return "LOF"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_AMSK_AIS_CI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "FMPM_FM_DS1E1_AMSK_AIS_CI"
            
            def description(self):
                return "AIS_CI"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_AMSK_AIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "FMPM_FM_DS1E1_AMSK_AIS"
            
            def description(self):
                return "AIS"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_AMSK_LOS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "FMPM_FM_DS1E1_AMSK_LOS"
            
            def description(self):
                return "LOS"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_CUR_HI_AIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_FM_DS1E1_CUR_HI_AIS"
            
            def description(self):
                return "High level AIS"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_CUR_RAI_CI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_FM_DS1E1_CUR_RAI_CI"
            
            def description(self):
                return "RAI_CI"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_CUR_RAI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_FM_DS1E1_CUR_RAI"
            
            def description(self):
                return "RAI"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_CUR_LOF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_FM_DS1E1_CUR_LOF"
            
            def description(self):
                return "LOF"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_CUR_AIS_CI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_FM_DS1E1_CUR_AIS_CI"
            
            def description(self):
                return "AIS_CI"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_CUR_AIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_FM_DS1E1_CUR_AIS"
            
            def description(self):
                return "AIS"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_CUR_LOS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_DS1E1_CUR_LOS"
            
            def description(self):
                return "LOS"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_DS1E1_AMSK_HI_AIS"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_AMSK_HI_AIS()
            allFields["FMPM_FM_DS1E1_AMSK_RAI_CI"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_AMSK_RAI_CI()
            allFields["FMPM_FM_DS1E1_AMSK_RAI"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_AMSK_RAI()
            allFields["FMPM_FM_DS1E1_AMSK_LOF"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_AMSK_LOF()
            allFields["FMPM_FM_DS1E1_AMSK_AIS_CI"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_AMSK_AIS_CI()
            allFields["FMPM_FM_DS1E1_AMSK_AIS"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_AMSK_AIS()
            allFields["FMPM_FM_DS1E1_AMSK_LOS"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_AMSK_LOS()
            allFields["FMPM_FM_DS1E1_CUR_HI_AIS"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_CUR_HI_AIS()
            allFields["FMPM_FM_DS1E1_CUR_RAI_CI"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_CUR_RAI_CI()
            allFields["FMPM_FM_DS1E1_CUR_RAI"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_CUR_RAI()
            allFields["FMPM_FM_DS1E1_CUR_LOF"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_CUR_LOF()
            allFields["FMPM_FM_DS1E1_CUR_AIS_CI"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_CUR_AIS_CI()
            allFields["FMPM_FM_DS1E1_CUR_AIS"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_CUR_AIS()
            allFields["FMPM_FM_DS1E1_CUR_LOS"] = _AF6CNC0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_CUR_LOS()
            return allFields

    class _FMPM_FM_PW_Level1_Group_Interrupt_OR(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM PW Level1 Group Interrupt OR"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00018004
            
        def endAddress(self):
            return 0xffffffff

        class _FMPM_FM_PW_LEV1_OR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_PW_LEV1_OR"
            
            def description(self):
                return "Interrupt PW level1 group OR"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_PW_LEV1_OR"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Level1_Group_Interrupt_OR._FMPM_FM_PW_LEV1_OR()
            return allFields

    class _FMPM_FM_PW_Level2_Group_Interrupt_OR(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM PW Level2 Group Interrupt OR"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x180E0+$D"
            
        def startAddress(self):
            return 0x000180e0
            
        def endAddress(self):
            return 0x000180e7

        class _FMPM_FM_PW_LEV2_OR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_PW_LEV2_OR"
            
            def description(self):
                return "PW Level2 Group Interrupt OR, bit per group, the last Level-1 group has 12 Level-2 group"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_PW_LEV2_OR"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Level2_Group_Interrupt_OR._FMPM_FM_PW_LEV2_OR()
            return allFields

    class _FMPM_FM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM PW Framer Interrupt OR AND MASK Per PW"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x1B400+$D*0x20+$E"
            
        def startAddress(self):
            return 0x0001b400
            
        def endAddress(self):
            return 0x0001b4ff

        class _FMPM_FM_PW_OAMSK(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_PW_OAMSK"
            
            def description(self):
                return "Interrupt OR AND MASK per PW, bit per PW"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_PW_OAMSK"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW._FMPM_FM_PW_OAMSK()
            return allFields

    class _FMPM_FM_PW_Framer_Interrupt_MASK_Per_PW(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM PW Framer Interrupt MASK Per PW"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1B500+$D*0x20+$E"
            
        def startAddress(self):
            return 0x0001b500
            
        def endAddress(self):
            return 0x0001b5ff

        class _FMPM_FM_PW_MSK(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_PW_MSK"
            
            def description(self):
                return "Interrupt OR AND MASK per PW, bit per PW"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_PW_MSK"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Framer_Interrupt_MASK_Per_PW._FMPM_FM_PW_MSK()
            return allFields

    class _FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM PW Interrupt Sticky Per Type Per PW"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x1C000+$D*0x400+$E*0x20+$F"
            
        def startAddress(self):
            return 0x0001c000
            
        def endAddress(self):
            return 0x0001dfff

        class _FMPM_FM_PW_STK_RBIT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "FMPM_FM_PW_STK_RBIT"
            
            def description(self):
                return "RBIT"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_STK_STRAY(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_FM_PW_STK_STRAY"
            
            def description(self):
                return "STRAY"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_STK_MALF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_FM_PW_STK_MALF"
            
            def description(self):
                return "MALF"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_STK_BufUder(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_FM_PW_STK_BufUder"
            
            def description(self):
                return "BufUder"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_STK_BufOver(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_FM_PW_STK_BufOver"
            
            def description(self):
                return "BufOver"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_STK_LBIT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_FM_PW_STK_LBIT"
            
            def description(self):
                return "LBIT"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_STK_LOPSYN(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_FM_PW_STK_LOPSYN"
            
            def description(self):
                return "LOPSYN"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_STK_LOPSTA(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_PW_STK_LOPSTA"
            
            def description(self):
                return "LOPSTA"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_PW_STK_RBIT"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW._FMPM_FM_PW_STK_RBIT()
            allFields["FMPM_FM_PW_STK_STRAY"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW._FMPM_FM_PW_STK_STRAY()
            allFields["FMPM_FM_PW_STK_MALF"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW._FMPM_FM_PW_STK_MALF()
            allFields["FMPM_FM_PW_STK_BufUder"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW._FMPM_FM_PW_STK_BufUder()
            allFields["FMPM_FM_PW_STK_BufOver"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW._FMPM_FM_PW_STK_BufOver()
            allFields["FMPM_FM_PW_STK_LBIT"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW._FMPM_FM_PW_STK_LBIT()
            allFields["FMPM_FM_PW_STK_LOPSYN"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW._FMPM_FM_PW_STK_LOPSYN()
            allFields["FMPM_FM_PW_STK_LOPSTA"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW._FMPM_FM_PW_STK_LOPSTA()
            return allFields

    class _FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM PW Interrupt MASK Per Type Per PW"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1E000+$D*0x400+$E*0x20+$F"
            
        def startAddress(self):
            return 0x0001e000
            
        def endAddress(self):
            return 0x0001ffff

        class _FMPM_FM_PW_MSK_RBIT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "FMPM_FM_PW_MSK_RBIT"
            
            def description(self):
                return "RBIT"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_MSK_STRAY(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_FM_PW_MSK_STRAY"
            
            def description(self):
                return "STRAY"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_MSK_MALF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_FM_PW_MSK_MALF"
            
            def description(self):
                return "MALF"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_MSK_BufUder(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_FM_PW_MSK_BufUder"
            
            def description(self):
                return "BufUder"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_MSK_BufOver(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_FM_PW_MSK_BufOver"
            
            def description(self):
                return "BufOver"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_MSK_LBIT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_FM_PW_MSK_LBIT"
            
            def description(self):
                return "LBIT"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_MSK_LOPSYN(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_FM_PW_MSK_LOPSYN"
            
            def description(self):
                return "LOPSYN"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_MSK_LOPSTA(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_PW_MSK_LOPSTA"
            
            def description(self):
                return "LOPSTA"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_PW_MSK_RBIT"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW._FMPM_FM_PW_MSK_RBIT()
            allFields["FMPM_FM_PW_MSK_STRAY"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW._FMPM_FM_PW_MSK_STRAY()
            allFields["FMPM_FM_PW_MSK_MALF"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW._FMPM_FM_PW_MSK_MALF()
            allFields["FMPM_FM_PW_MSK_BufUder"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW._FMPM_FM_PW_MSK_BufUder()
            allFields["FMPM_FM_PW_MSK_BufOver"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW._FMPM_FM_PW_MSK_BufOver()
            allFields["FMPM_FM_PW_MSK_LBIT"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW._FMPM_FM_PW_MSK_LBIT()
            allFields["FMPM_FM_PW_MSK_LOPSYN"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW._FMPM_FM_PW_MSK_LOPSYN()
            allFields["FMPM_FM_PW_MSK_LOPSTA"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW._FMPM_FM_PW_MSK_LOPSTA()
            return allFields

    class _FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM PW Interrupt Current Status Per Type Per PW"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x20000+$D*0x400+$E*0x20+$F"
            
        def startAddress(self):
            return 0x00020000
            
        def endAddress(self):
            return 0x00021fff

        class _FMPM_FM_PW_AMSK_RBIT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "FMPM_FM_PW_AMSK_RBIT"
            
            def description(self):
                return "RBIT"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_AMSK_STRAY(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "FMPM_FM_PW_AMSK_STRAY"
            
            def description(self):
                return "STRAY"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_AMSK_MALF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "FMPM_FM_PW_AMSK_MALF"
            
            def description(self):
                return "MALF"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_AMSK_BufUder(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "FMPM_FM_PW_AMSK_BufUder"
            
            def description(self):
                return "BufUder"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_AMSK_BufOver(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "FMPM_FM_PW_AMSK_BufOver"
            
            def description(self):
                return "BufOver"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_AMSK_LBIT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "FMPM_FM_PW_AMSK_LBIT"
            
            def description(self):
                return "LBIT"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_AMSK_LOPSYN(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "FMPM_FM_PW_AMSK_LOPSYN"
            
            def description(self):
                return "LOPSYN"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_AMSK_LOPSTA(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "FMPM_FM_PW_AMSK_LOPSTA"
            
            def description(self):
                return "LOPSTA"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_CUR_RBIT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "FMPM_FM_PW_CUR_RBIT"
            
            def description(self):
                return "RBIT"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_CUR_STRAY(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_FM_PW_CUR_STRAY"
            
            def description(self):
                return "STRAY"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_CUR_MALF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_FM_PW_CUR_MALF"
            
            def description(self):
                return "MALF"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_CUR_BufUder(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_FM_PW_CUR_BufUder"
            
            def description(self):
                return "BufUder"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_CUR_BufOver(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_FM_PW_CUR_BufOver"
            
            def description(self):
                return "BufOver"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_CUR_LBIT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_FM_PW_CUR_LBIT"
            
            def description(self):
                return "LBIT"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_CUR_LOPSYN(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_FM_PW_CUR_LOPSYN"
            
            def description(self):
                return "LOPSYN"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_CUR_LOPSTA(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_PW_CUR_LOPSTA"
            
            def description(self):
                return "LOPSTA"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_PW_AMSK_RBIT"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_AMSK_RBIT()
            allFields["FMPM_FM_PW_AMSK_STRAY"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_AMSK_STRAY()
            allFields["FMPM_FM_PW_AMSK_MALF"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_AMSK_MALF()
            allFields["FMPM_FM_PW_AMSK_BufUder"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_AMSK_BufUder()
            allFields["FMPM_FM_PW_AMSK_BufOver"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_AMSK_BufOver()
            allFields["FMPM_FM_PW_AMSK_LBIT"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_AMSK_LBIT()
            allFields["FMPM_FM_PW_AMSK_LOPSYN"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_AMSK_LOPSYN()
            allFields["FMPM_FM_PW_AMSK_LOPSTA"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_AMSK_LOPSTA()
            allFields["FMPM_FM_PW_CUR_RBIT"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_CUR_RBIT()
            allFields["FMPM_FM_PW_CUR_STRAY"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_CUR_STRAY()
            allFields["FMPM_FM_PW_CUR_MALF"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_CUR_MALF()
            allFields["FMPM_FM_PW_CUR_BufUder"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_CUR_BufUder()
            allFields["FMPM_FM_PW_CUR_BufOver"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_CUR_BufOver()
            allFields["FMPM_FM_PW_CUR_LBIT"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_CUR_LBIT()
            allFields["FMPM_FM_PW_CUR_LOPSYN"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_CUR_LOPSYN()
            allFields["FMPM_FM_PW_CUR_LOPSTA"] = _AF6CNC0021_RD_PM._FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_CUR_LOPSTA()
            return allFields

    class _FMPM_TCA_Interrupt(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA Interrupt"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00054000
            
        def endAddress(self):
            return 0xffffffff

        class _FMPM_TCA_Intr_PW(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_TCA_Intr_PW"
            
            def description(self):
                return "PW     interrupt"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_Intr_DS1E1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_TCA_Intr_DS1E1"
            
            def description(self):
                return "DS1E1  interrupt"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_Intr_DS3E3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_TCA_Intr_DS3E3"
            
            def description(self):
                return "DS3E3  interrupt"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_Intr_VTGTUG(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_TCA_Intr_VTGTUG"
            
            def description(self):
                return "VTGTUG interrupt"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_Intr_STSAU(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_TCA_Intr_STSAU"
            
            def description(self):
                return "STSAU  interrupt"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_Intr_ECSTM(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_Intr_ECSTM"
            
            def description(self):
                return "ECSTM  interrupt"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_Intr_PW"] = _AF6CNC0021_RD_PM._FMPM_TCA_Interrupt._FMPM_TCA_Intr_PW()
            allFields["FMPM_TCA_Intr_DS1E1"] = _AF6CNC0021_RD_PM._FMPM_TCA_Interrupt._FMPM_TCA_Intr_DS1E1()
            allFields["FMPM_TCA_Intr_DS3E3"] = _AF6CNC0021_RD_PM._FMPM_TCA_Interrupt._FMPM_TCA_Intr_DS3E3()
            allFields["FMPM_TCA_Intr_VTGTUG"] = _AF6CNC0021_RD_PM._FMPM_TCA_Interrupt._FMPM_TCA_Intr_VTGTUG()
            allFields["FMPM_TCA_Intr_STSAU"] = _AF6CNC0021_RD_PM._FMPM_TCA_Interrupt._FMPM_TCA_Intr_STSAU()
            allFields["FMPM_TCA_Intr_ECSTM"] = _AF6CNC0021_RD_PM._FMPM_TCA_Interrupt._FMPM_TCA_Intr_ECSTM()
            return allFields

    class _FMPM_TCA_Interrupt_Mask(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA Interrupt Mask"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00054001
            
        def endAddress(self):
            return 0xffffffff

        class _FMPM_TCA_Intr_MSK_PW(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_TCA_Intr_MSK_PW"
            
            def description(self):
                return "PW    Interrupt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_Intr_MSK_DS1E1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_TCA_Intr_MSK_DS1E1"
            
            def description(self):
                return "DS1E1 interrupt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_Intr_MSK_DS3E3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_TCA_Intr_MSK_DS3E3"
            
            def description(self):
                return "DS3E3  interrupt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_Intr_MSK_VTGTUG(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_TCA_Intr_MSK_VTGTUG"
            
            def description(self):
                return "VTGTUG interrupt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_Intr_MSK_STSAU(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_TCA_Intr_MSK_STSAU"
            
            def description(self):
                return "STSAU interrupt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_Intr_MSK_ECSTM(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_Intr_MSK_ECSTM"
            
            def description(self):
                return "ECSTM interrupt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_Intr_MSK_PW"] = _AF6CNC0021_RD_PM._FMPM_TCA_Interrupt_Mask._FMPM_TCA_Intr_MSK_PW()
            allFields["FMPM_TCA_Intr_MSK_DS1E1"] = _AF6CNC0021_RD_PM._FMPM_TCA_Interrupt_Mask._FMPM_TCA_Intr_MSK_DS1E1()
            allFields["FMPM_TCA_Intr_MSK_DS3E3"] = _AF6CNC0021_RD_PM._FMPM_TCA_Interrupt_Mask._FMPM_TCA_Intr_MSK_DS3E3()
            allFields["FMPM_TCA_Intr_MSK_VTGTUG"] = _AF6CNC0021_RD_PM._FMPM_TCA_Interrupt_Mask._FMPM_TCA_Intr_MSK_VTGTUG()
            allFields["FMPM_TCA_Intr_MSK_STSAU"] = _AF6CNC0021_RD_PM._FMPM_TCA_Interrupt_Mask._FMPM_TCA_Intr_MSK_STSAU()
            allFields["FMPM_TCA_Intr_MSK_ECSTM"] = _AF6CNC0021_RD_PM._FMPM_TCA_Interrupt_Mask._FMPM_TCA_Intr_MSK_ECSTM()
            return allFields

    class _FMPM_TCA_EC1_Interrupt_OR(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA EC1 Interrupt OR"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00058005
            
        def endAddress(self):
            return 0xffffffff

        class _FMPM_TCA_EC1_OR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_EC1_OR"
            
            def description(self):
                return "Interrupt EC1 OR"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_EC1_OR"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1_Interrupt_OR._FMPM_TCA_EC1_OR()
            return allFields

    class _FMPM_TCA_EC1_Interrupt_OR_AND_MASK_Per_STS1(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA EC1 Interrupt OR AND MASK Per STS1"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x58100+$L*0x2+$M"
            
        def startAddress(self):
            return 0x00058100
            
        def endAddress(self):
            return 0x0005810f

        class _FMPM_TCA_EC1_OAMSK(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_EC1_OAMSK"
            
            def description(self):
                return "Interrupt STS1 OR AND MASK"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_EC1_OAMSK"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1_Interrupt_OR_AND_MASK_Per_STS1._FMPM_TCA_EC1_OAMSK()
            return allFields

    class _FMPM_TCA_EC1_Interrupt_MASK_Per_STS1(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA EC1 Interrupt MASK Per STS1"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x58110+$L*0x2+$M"
            
        def startAddress(self):
            return 0x00058110
            
        def endAddress(self):
            return 0x0005811f

        class _FMPM_TCA_EC1_MSK(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_EC1_MSK"
            
            def description(self):
                return "Interrupt MASK per STS1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_EC1_MSK"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1_Interrupt_MASK_Per_STS1._FMPM_TCA_EC1_MSK()
            return allFields

    class _FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA EC1STM0 Interrupt Sticky Per Type Of Per STS1"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x5B600+$L*0x40+$M*0x20+$N"
            
        def startAddress(self):
            return 0x0005b600
            
        def endAddress(self):
            return 0x0005b7ff

        class _FMPM_TCA_EC1STM0_STK_CV_S(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "FMPM_TCA_EC1STM0_STK_CV_S"
            
            def description(self):
                return "CV_S"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_STK_SEFS_S(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "FMPM_TCA_EC1STM0_STK_SEFS_S"
            
            def description(self):
                return "SEFS_S"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_STK_ES_S(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "FMPM_TCA_EC1STM0_STK_ES_S"
            
            def description(self):
                return "ES_S"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_STK_SES_S(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "FMPM_TCA_EC1STM0_STK_SES_S"
            
            def description(self):
                return "SES_S"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_STK_CV_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "FMPM_TCA_EC1STM0_STK_CV_L"
            
            def description(self):
                return "CV_L"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_STK_ES_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "FMPM_TCA_EC1STM0_STK_ES_L"
            
            def description(self):
                return "ES_L"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_STK_SES_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "FMPM_TCA_EC1STM0_STK_SES_L"
            
            def description(self):
                return "SES_L"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_STK_UAS_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_TCA_EC1STM0_STK_UAS_L"
            
            def description(self):
                return "UAS_L"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_STK_CV_LFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_TCA_EC1STM0_STK_CV_LFE"
            
            def description(self):
                return "CV_LFE"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_STK_FC_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_TCA_EC1STM0_STK_FC_L"
            
            def description(self):
                return "FC_L"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_STK_ES_LFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_TCA_EC1STM0_STK_ES_LFE"
            
            def description(self):
                return "ES_LFE"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_STK_SES_LFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_TCA_EC1STM0_STK_SES_LFE"
            
            def description(self):
                return "SES_LFE"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_STK_UAS_LFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_TCA_EC1STM0_STK_UAS_LFE"
            
            def description(self):
                return "UAS_LFE"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_STK_FC_LFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_EC1STM0_STK_FC_LFE"
            
            def description(self):
                return "FC_LFE"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_EC1STM0_STK_CV_S"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_STK_CV_S()
            allFields["FMPM_TCA_EC1STM0_STK_SEFS_S"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_STK_SEFS_S()
            allFields["FMPM_TCA_EC1STM0_STK_ES_S"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_STK_ES_S()
            allFields["FMPM_TCA_EC1STM0_STK_SES_S"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_STK_SES_S()
            allFields["FMPM_TCA_EC1STM0_STK_CV_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_STK_CV_L()
            allFields["FMPM_TCA_EC1STM0_STK_ES_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_STK_ES_L()
            allFields["FMPM_TCA_EC1STM0_STK_SES_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_STK_SES_L()
            allFields["FMPM_TCA_EC1STM0_STK_UAS_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_STK_UAS_L()
            allFields["FMPM_TCA_EC1STM0_STK_CV_LFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_STK_CV_LFE()
            allFields["FMPM_TCA_EC1STM0_STK_FC_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_STK_FC_L()
            allFields["FMPM_TCA_EC1STM0_STK_ES_LFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_STK_ES_LFE()
            allFields["FMPM_TCA_EC1STM0_STK_SES_LFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_STK_SES_LFE()
            allFields["FMPM_TCA_EC1STM0_STK_UAS_LFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_STK_UAS_LFE()
            allFields["FMPM_TCA_EC1STM0_STK_FC_LFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_STK_FC_LFE()
            return allFields

    class _FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA EC1STM0 Interrupt MASK Per Type Of Per STS1"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x5B800+$L*0x40+$M*0x20+$N"
            
        def startAddress(self):
            return 0x0005b800
            
        def endAddress(self):
            return 0x0005b9ff

        class _FMPM_TCA_EC1STM0_MSK_CV_S(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "FMPM_TCA_EC1STM0_MSK_CV_S"
            
            def description(self):
                return "CV_S"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_MSK_SEFS_S(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "FMPM_TCA_EC1STM0_MSK_SEFS_S"
            
            def description(self):
                return "SEFS_S"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_MSK_ES_S(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "FMPM_TCA_EC1STM0_MSK_ES_S"
            
            def description(self):
                return "ES_S"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_MSK_SES_S(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "FMPM_TCA_EC1STM0_MSK_SES_S"
            
            def description(self):
                return "SES_S"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_MSK_CV_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "FMPM_TCA_EC1STM0_MSK_CV_L"
            
            def description(self):
                return "CV_L"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_MSK_ES_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "FMPM_TCA_EC1STM0_MSK_ES_L"
            
            def description(self):
                return "ES_L"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_MSK_SES_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "FMPM_TCA_EC1STM0_MSK_SES_L"
            
            def description(self):
                return "SES_L"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_MSK_UAS_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_TCA_EC1STM0_MSK_UAS_L"
            
            def description(self):
                return "UAS_L"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_MSK_CV_LFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_TCA_EC1STM0_MSK_CV_LFE"
            
            def description(self):
                return "CV_LFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_MSK_FC_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_TCA_EC1STM0_MSK_FC_L"
            
            def description(self):
                return "FC_L"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_MSK_ES_LFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_TCA_EC1STM0_MSK_ES_LFE"
            
            def description(self):
                return "ES_LFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_MSK_SES_LFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_TCA_EC1STM0_MSK_SES_LFE"
            
            def description(self):
                return "SES_LFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_MSK_UAS_LFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_TCA_EC1STM0_MSK_UAS_LFE"
            
            def description(self):
                return "UAS_LFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_MSK_FC_LFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_EC1STM0_MSK_FC_LFE"
            
            def description(self):
                return "FC_LFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_EC1STM0_MSK_CV_S"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_MSK_CV_S()
            allFields["FMPM_TCA_EC1STM0_MSK_SEFS_S"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_MSK_SEFS_S()
            allFields["FMPM_TCA_EC1STM0_MSK_ES_S"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_MSK_ES_S()
            allFields["FMPM_TCA_EC1STM0_MSK_SES_S"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_MSK_SES_S()
            allFields["FMPM_TCA_EC1STM0_MSK_CV_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_MSK_CV_L()
            allFields["FMPM_TCA_EC1STM0_MSK_ES_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_MSK_ES_L()
            allFields["FMPM_TCA_EC1STM0_MSK_SES_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_MSK_SES_L()
            allFields["FMPM_TCA_EC1STM0_MSK_UAS_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_MSK_UAS_L()
            allFields["FMPM_TCA_EC1STM0_MSK_CV_LFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_MSK_CV_LFE()
            allFields["FMPM_TCA_EC1STM0_MSK_FC_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_MSK_FC_L()
            allFields["FMPM_TCA_EC1STM0_MSK_ES_LFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_MSK_ES_LFE()
            allFields["FMPM_TCA_EC1STM0_MSK_SES_LFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_MSK_SES_LFE()
            allFields["FMPM_TCA_EC1STM0_MSK_UAS_LFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_MSK_UAS_LFE()
            allFields["FMPM_TCA_EC1STM0_MSK_FC_LFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_MSK_FC_LFE()
            return allFields

    class _FMPM_TCA_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA EC1STM0 Interrupt Current Status Per Type Of Per STS1"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x5BA00+$L*0x40+$M*0x20+$N"
            
        def startAddress(self):
            return 0x0005ba00
            
        def endAddress(self):
            return 0x0005bbff

        class _FMPM_TCA_EC1STM0_CUR_CV_S(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "FMPM_TCA_EC1STM0_CUR_CV_S"
            
            def description(self):
                return "CV_S"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_CUR_SEFS_S(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "FMPM_TCA_EC1STM0_CUR_SEFS_S"
            
            def description(self):
                return "SEFS_S"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_CUR_ES_S(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "FMPM_TCA_EC1STM0_CUR_ES_S"
            
            def description(self):
                return "ES_S"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_CUR_SES_S(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "FMPM_TCA_EC1STM0_CUR_SES_S"
            
            def description(self):
                return "SES_S"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_CUR_CV_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "FMPM_TCA_EC1STM0_CUR_CV_L"
            
            def description(self):
                return "CV_L"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_CUR_ES_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "FMPM_TCA_EC1STM0_CUR_ES_L"
            
            def description(self):
                return "ES_L"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_CUR_SES_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "FMPM_TCA_EC1STM0_CUR_SES_L"
            
            def description(self):
                return "SES_L"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_CUR_UAS_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_TCA_EC1STM0_CUR_UAS_L"
            
            def description(self):
                return "UAS_L"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_CUR_CV_LFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_TCA_EC1STM0_CUR_CV_LFE"
            
            def description(self):
                return "CV_LFE"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_CUR_FC_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_TCA_EC1STM0_CUR_FC_L"
            
            def description(self):
                return "FC_L"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_CUR_ES_LFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_TCA_EC1STM0_CUR_ES_LFE"
            
            def description(self):
                return "ES_LFE"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_CUR_SES_LFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_TCA_EC1STM0_CUR_SES_LFE"
            
            def description(self):
                return "SES_LFE"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_CUR_UAS_LFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_TCA_EC1STM0_CUR_UAS_LFE"
            
            def description(self):
                return "UAS_LFE"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_EC1STM0_CUR_FC_LFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_EC1STM0_CUR_FC_LFE"
            
            def description(self):
                return "FC_LFE"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_EC1STM0_CUR_CV_S"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_CUR_CV_S()
            allFields["FMPM_TCA_EC1STM0_CUR_SEFS_S"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_CUR_SEFS_S()
            allFields["FMPM_TCA_EC1STM0_CUR_ES_S"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_CUR_ES_S()
            allFields["FMPM_TCA_EC1STM0_CUR_SES_S"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_CUR_SES_S()
            allFields["FMPM_TCA_EC1STM0_CUR_CV_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_CUR_CV_L()
            allFields["FMPM_TCA_EC1STM0_CUR_ES_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_CUR_ES_L()
            allFields["FMPM_TCA_EC1STM0_CUR_SES_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_CUR_SES_L()
            allFields["FMPM_TCA_EC1STM0_CUR_UAS_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_CUR_UAS_L()
            allFields["FMPM_TCA_EC1STM0_CUR_CV_LFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_CUR_CV_LFE()
            allFields["FMPM_TCA_EC1STM0_CUR_FC_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_CUR_FC_L()
            allFields["FMPM_TCA_EC1STM0_CUR_ES_LFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_CUR_ES_LFE()
            allFields["FMPM_TCA_EC1STM0_CUR_SES_LFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_CUR_SES_LFE()
            allFields["FMPM_TCA_EC1STM0_CUR_UAS_LFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_CUR_UAS_LFE()
            allFields["FMPM_TCA_EC1STM0_CUR_FC_LFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_TCA_EC1STM0_CUR_FC_LFE()
            return allFields

    class _FMPM_TCA_STS24_Interrupt_OR(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA STS24 Interrupt OR"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00058000
            
        def endAddress(self):
            return 0xffffffff

        class _FMPM_TCA_STS24_OR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_STS24_OR"
            
            def description(self):
                return "Interrupt STS24 OR"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_STS24_OR"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_OR._FMPM_TCA_STS24_OR()
            return allFields

    class _FMPM_TCA_STS24_Interrupt_OR_AND_MASK_Per_STS1(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA STS24 Interrupt OR AND MASK Per STS1"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x58080+$L*0x2+$M"
            
        def startAddress(self):
            return 0x00058080
            
        def endAddress(self):
            return 0x0005808f

        class _FMPM_TCA_STS1_OAMSK(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_STS1_OAMSK"
            
            def description(self):
                return "Interrupt STS1 OR AND MASK"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_STS1_OAMSK"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_OR_AND_MASK_Per_STS1._FMPM_TCA_STS1_OAMSK()
            return allFields

    class _FMPM_TCA_STS24_Interrupt_MASK_Per_STS1(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA STS24 Interrupt MASK Per STS1"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x58090+$L*0x2+$M"
            
        def startAddress(self):
            return 0x00058090
            
        def endAddress(self):
            return 0x0005809f

        class _FMPM_TCA_STS1_MSK(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_STS1_MSK"
            
            def description(self):
                return "Interrupt MASK per STS1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_STS1_MSK"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_MASK_Per_STS1._FMPM_TCA_STS1_MSK()
            return allFields

    class _FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA STS24 Interrupt Sticky Per Type Of Per STS1"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x5A000+$L*0x40+$M*0x20+$N"
            
        def startAddress(self):
            return 0x0005a000
            
        def endAddress(self):
            return 0x0005a1ff

        class _FMPM_TCA_STS1_STK_CV_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "FMPM_TCA_STS1_STK_CV_P"
            
            def description(self):
                return "CV_P"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_STK_ES_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "FMPM_TCA_STS1_STK_ES_P"
            
            def description(self):
                return "ES_P"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_STK_SES_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "FMPM_TCA_STS1_STK_SES_P"
            
            def description(self):
                return "SES_P"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_STK_UAS_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "FMPM_TCA_STS1_STK_UAS_P"
            
            def description(self):
                return "UAS_P"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_STK_PPJC_PDet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "FMPM_TCA_STS1_STK_PPJC_PDet"
            
            def description(self):
                return "PPJC_PDet"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_STK_NPJC_PDet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "FMPM_TCA_STS1_STK_NPJC_PDet"
            
            def description(self):
                return "NPJC_PDet"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_STK_PPJC_PGen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "FMPM_TCA_STS1_STK_PPJC_PGen"
            
            def description(self):
                return "PPJC_PGen"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_STK_NPJC_PGen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "FMPM_TCA_STS1_STK_NPJC_PGen"
            
            def description(self):
                return "NPJC_PGen"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_STK_PJCDiff_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "FMPM_TCA_STS1_STK_PJCDiff_P"
            
            def description(self):
                return "PJCDiff_P"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_STK_PJCS_Det(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "FMPM_TCA_STS1_STK_PJCS_Det"
            
            def description(self):
                return "PJCS_Det"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_STK_PJCS_Gen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_TCA_STS1_STK_PJCS_Gen"
            
            def description(self):
                return "PJCS_Gen"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_STK_CV_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_TCA_STS1_STK_CV_PFE"
            
            def description(self):
                return "CV_PFE"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_STK_ES_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_TCA_STS1_STK_ES_PFE"
            
            def description(self):
                return "ES_PFE"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_STK_SES_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_TCA_STS1_STK_SES_PFE"
            
            def description(self):
                return "SES_PFE"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_STK_UAS_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_TCA_STS1_STK_UAS_PFE"
            
            def description(self):
                return "UAS_PFE"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_STK_FC_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_TCA_STS1_STK_FC_P"
            
            def description(self):
                return "FC_P"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_STK_FC_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_STS1_STK_FC_PFE"
            
            def description(self):
                return "FC_PFE"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_STS1_STK_CV_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_STK_CV_P()
            allFields["FMPM_TCA_STS1_STK_ES_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_STK_ES_P()
            allFields["FMPM_TCA_STS1_STK_SES_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_STK_SES_P()
            allFields["FMPM_TCA_STS1_STK_UAS_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_STK_UAS_P()
            allFields["FMPM_TCA_STS1_STK_PPJC_PDet"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_STK_PPJC_PDet()
            allFields["FMPM_TCA_STS1_STK_NPJC_PDet"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_STK_NPJC_PDet()
            allFields["FMPM_TCA_STS1_STK_PPJC_PGen"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_STK_PPJC_PGen()
            allFields["FMPM_TCA_STS1_STK_NPJC_PGen"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_STK_NPJC_PGen()
            allFields["FMPM_TCA_STS1_STK_PJCDiff_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_STK_PJCDiff_P()
            allFields["FMPM_TCA_STS1_STK_PJCS_Det"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_STK_PJCS_Det()
            allFields["FMPM_TCA_STS1_STK_PJCS_Gen"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_STK_PJCS_Gen()
            allFields["FMPM_TCA_STS1_STK_CV_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_STK_CV_PFE()
            allFields["FMPM_TCA_STS1_STK_ES_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_STK_ES_PFE()
            allFields["FMPM_TCA_STS1_STK_SES_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_STK_SES_PFE()
            allFields["FMPM_TCA_STS1_STK_UAS_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_STK_UAS_PFE()
            allFields["FMPM_TCA_STS1_STK_FC_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_STK_FC_P()
            allFields["FMPM_TCA_STS1_STK_FC_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_STK_FC_PFE()
            return allFields

    class _FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA STS24 Interrupt MASK Per Type Of Per STS1"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x5A200+$L*0x40+$M*0x20+$N"
            
        def startAddress(self):
            return 0x0005a200
            
        def endAddress(self):
            return 0x0005a3ff

        class _FMPM_TCA_STS1_MSK_CV_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "FMPM_TCA_STS1_MSK_CV_P"
            
            def description(self):
                return "CV_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_MSK_ES_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "FMPM_TCA_STS1_MSK_ES_P"
            
            def description(self):
                return "ES_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_MSK_SES_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "FMPM_TCA_STS1_MSK_SES_P"
            
            def description(self):
                return "SES_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_MSK_UAS_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "FMPM_TCA_STS1_MSK_UAS_P"
            
            def description(self):
                return "UAS_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_MSK_PPJC_PDet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "FMPM_TCA_STS1_MSK_PPJC_PDet"
            
            def description(self):
                return "PPJC_PDet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_MSK_NPJC_PDet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "FMPM_TCA_STS1_MSK_NPJC_PDet"
            
            def description(self):
                return "NPJC_PDet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_MSK_PPJC_PGen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "FMPM_TCA_STS1_MSK_PPJC_PGen"
            
            def description(self):
                return "PPJC_PGen"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_MSK_NPJC_PGen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "FMPM_TCA_STS1_MSK_NPJC_PGen"
            
            def description(self):
                return "NPJC_PGen"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_MSK_PJCDiff_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "FMPM_TCA_STS1_MSK_PJCDiff_P"
            
            def description(self):
                return "PJCDiff_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_MSK_PJCS_Det(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "FMPM_TCA_STS1_MSK_PJCS_Det"
            
            def description(self):
                return "PJCS_Det"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_MSK_PJCS_Gen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_TCA_STS1_MSK_PJCS_Gen"
            
            def description(self):
                return "PJCS_Gen"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_MSK_CV_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_TCA_STS1_MSK_CV_PFE"
            
            def description(self):
                return "CV_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_MSK_ES_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_TCA_STS1_MSK_ES_PFE"
            
            def description(self):
                return "ES_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_MSK_SES_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_TCA_STS1_MSK_SES_PFE"
            
            def description(self):
                return "SES_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_MSK_UAS_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_TCA_STS1_MSK_UAS_PFE"
            
            def description(self):
                return "UAS_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_MSK_FC_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_TCA_STS1_MSK_FC_P"
            
            def description(self):
                return "FC_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_MSK_FC_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_STS1_MSK_FC_PFE"
            
            def description(self):
                return "FC_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_STS1_MSK_CV_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_MSK_CV_P()
            allFields["FMPM_TCA_STS1_MSK_ES_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_MSK_ES_P()
            allFields["FMPM_TCA_STS1_MSK_SES_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_MSK_SES_P()
            allFields["FMPM_TCA_STS1_MSK_UAS_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_MSK_UAS_P()
            allFields["FMPM_TCA_STS1_MSK_PPJC_PDet"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_MSK_PPJC_PDet()
            allFields["FMPM_TCA_STS1_MSK_NPJC_PDet"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_MSK_NPJC_PDet()
            allFields["FMPM_TCA_STS1_MSK_PPJC_PGen"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_MSK_PPJC_PGen()
            allFields["FMPM_TCA_STS1_MSK_NPJC_PGen"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_MSK_NPJC_PGen()
            allFields["FMPM_TCA_STS1_MSK_PJCDiff_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_MSK_PJCDiff_P()
            allFields["FMPM_TCA_STS1_MSK_PJCS_Det"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_MSK_PJCS_Det()
            allFields["FMPM_TCA_STS1_MSK_PJCS_Gen"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_MSK_PJCS_Gen()
            allFields["FMPM_TCA_STS1_MSK_CV_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_MSK_CV_PFE()
            allFields["FMPM_TCA_STS1_MSK_ES_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_MSK_ES_PFE()
            allFields["FMPM_TCA_STS1_MSK_SES_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_MSK_SES_PFE()
            allFields["FMPM_TCA_STS1_MSK_UAS_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_MSK_UAS_PFE()
            allFields["FMPM_TCA_STS1_MSK_FC_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_MSK_FC_P()
            allFields["FMPM_TCA_STS1_MSK_FC_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_MSK_FC_PFE()
            return allFields

    class _FMPM_TCA_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA STS24 Interrupt Current Status Per Type Of Per STS1"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x5A400+$L*0x40+$M*0x20+$N"
            
        def startAddress(self):
            return 0x0005a400
            
        def endAddress(self):
            return 0x0005a5ff

        class _FMPM_TCA_STS1_CUR_CV_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "FMPM_TCA_STS1_CUR_CV_P"
            
            def description(self):
                return "CV_P"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_CUR_ES_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "FMPM_TCA_STS1_CUR_ES_P"
            
            def description(self):
                return "ES_P"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_CUR_SES_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "FMPM_TCA_STS1_CUR_SES_P"
            
            def description(self):
                return "SES_P"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_CUR_UAS_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "FMPM_TCA_STS1_CUR_UAS_P"
            
            def description(self):
                return "UAS_P"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_CUR_PPJC_PDet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "FMPM_TCA_STS1_CUR_PPJC_PDet"
            
            def description(self):
                return "PPJC_PDet"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_CUR_NPJC_PDet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "FMPM_TCA_STS1_CUR_NPJC_PDet"
            
            def description(self):
                return "NPJC_PDet"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_CUR_PPJC_PGen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "FMPM_TCA_STS1_CUR_PPJC_PGen"
            
            def description(self):
                return "PPJC_PGen"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_CUR_NPJC_PGen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "FMPM_TCA_STS1_CUR_NPJC_PGen"
            
            def description(self):
                return "NPJC_PGen"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_CUR_PJCDiff_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "FMPM_TCA_STS1_CUR_PJCDiff_P"
            
            def description(self):
                return "PJCDiff_P"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_CUR_PJCS_Det(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "FMPM_TCA_STS1_CUR_PJCS_Det"
            
            def description(self):
                return "PJCS_Det"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_CUR_PJCS_Gen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_TCA_STS1_CUR_PJCS_Gen"
            
            def description(self):
                return "PJCS_Gen"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_CUR_CV_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_TCA_STS1_CUR_CV_PFE"
            
            def description(self):
                return "CV_PFE"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_CUR_ES_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_TCA_STS1_CUR_ES_PFE"
            
            def description(self):
                return "ES_PFE"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_CUR_SES_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_TCA_STS1_CUR_SES_PFE"
            
            def description(self):
                return "SES_PFE"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_CUR_UAS_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_TCA_STS1_CUR_UAS_PFE"
            
            def description(self):
                return "UAS_PFE"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_CUR_FC_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_TCA_STS1_CUR_FC_P"
            
            def description(self):
                return "FC_P"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_STS1_CUR_FC_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_STS1_CUR_FC_PFE"
            
            def description(self):
                return "FC_PFE"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_STS1_CUR_CV_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_CUR_CV_P()
            allFields["FMPM_TCA_STS1_CUR_ES_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_CUR_ES_P()
            allFields["FMPM_TCA_STS1_CUR_SES_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_CUR_SES_P()
            allFields["FMPM_TCA_STS1_CUR_UAS_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_CUR_UAS_P()
            allFields["FMPM_TCA_STS1_CUR_PPJC_PDet"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_CUR_PPJC_PDet()
            allFields["FMPM_TCA_STS1_CUR_NPJC_PDet"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_CUR_NPJC_PDet()
            allFields["FMPM_TCA_STS1_CUR_PPJC_PGen"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_CUR_PPJC_PGen()
            allFields["FMPM_TCA_STS1_CUR_NPJC_PGen"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_CUR_NPJC_PGen()
            allFields["FMPM_TCA_STS1_CUR_PJCDiff_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_CUR_PJCDiff_P()
            allFields["FMPM_TCA_STS1_CUR_PJCS_Det"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_CUR_PJCS_Det()
            allFields["FMPM_TCA_STS1_CUR_PJCS_Gen"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_CUR_PJCS_Gen()
            allFields["FMPM_TCA_STS1_CUR_CV_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_CUR_CV_PFE()
            allFields["FMPM_TCA_STS1_CUR_ES_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_CUR_ES_PFE()
            allFields["FMPM_TCA_STS1_CUR_SES_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_CUR_SES_PFE()
            allFields["FMPM_TCA_STS1_CUR_UAS_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_CUR_UAS_PFE()
            allFields["FMPM_TCA_STS1_CUR_FC_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_CUR_FC_P()
            allFields["FMPM_TCA_STS1_CUR_FC_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1._FMPM_TCA_STS1_CUR_FC_PFE()
            return allFields

    class _FMPM_TCA_DS3E3_Group_Interrupt_OR(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA DS3E3 Group Interrupt OR"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00058001
            
        def endAddress(self):
            return 0xffffffff

        class _FMPM_TCA_DS3E3_OR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_DS3E3_OR"
            
            def description(self):
                return "Interrupt DS3/E3 OR"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_DS3E3_OR"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Group_Interrupt_OR._FMPM_TCA_DS3E3_OR()
            return allFields

    class _FMPM_TCA_DS3E3_Framer_Interrupt_OR_AND_MASK_Per_DS3E3(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA DS3E3 Framer Interrupt OR AND MASK Per DS3E3"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x580A0+$G"
            
        def startAddress(self):
            return 0x000580a0
            
        def endAddress(self):
            return 0x000580af

        class _FMPM_TCA_DS3E3_OAMSK(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_DS3E3_OAMSK"
            
            def description(self):
                return "Interrupt DS3/E3 Group"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_DS3E3_OAMSK"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_OR_AND_MASK_Per_DS3E3._FMPM_TCA_DS3E3_OAMSK()
            return allFields

    class _FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_DS3E3(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA DS3E3 Framer Interrupt MASK Per DS3E3"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x580B0+$G"
            
        def startAddress(self):
            return 0x000580b0
            
        def endAddress(self):
            return 0x000580bf

        class _FMPM_TCA_DS3E3_MSK(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_DS3E3_MSK"
            
            def description(self):
                return "MASK per DS3/E3 Framer"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_DS3E3_MSK"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_DS3E3._FMPM_TCA_DS3E3_MSK()
            return allFields

    class _FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA DS3E3 Framer Interrupt Sticky Per Type of Per DS3E3"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x5A600+$G*0x20+$H"
            
        def startAddress(self):
            return 0x0005a600
            
        def endAddress(self):
            return 0x0005a7ff

        class _FMPM_TCA_DS3E3_STK_CV_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "FMPM_TCA_DS3E3_STK_CV_L"
            
            def description(self):
                return "CV_L"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_STK_ES_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "FMPM_TCA_DS3E3_STK_ES_L"
            
            def description(self):
                return "ES_L"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_STK_ESA_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "FMPM_TCA_DS3E3_STK_ESA_L"
            
            def description(self):
                return "ESA_L"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_STK_ESB_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "FMPM_TCA_DS3E3_STK_ESB_L"
            
            def description(self):
                return "ESB_L"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_STK_SES_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "FMPM_TCA_DS3E3_STK_SES_L"
            
            def description(self):
                return "SES_L"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_STK_LOSS_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "FMPM_TCA_DS3E3_STK_LOSS_L"
            
            def description(self):
                return "LOSS_L"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_STK_CVP_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "FMPM_TCA_DS3E3_STK_CVP_P"
            
            def description(self):
                return "CVP_P"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_STK_CVCP_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "FMPM_TCA_DS3E3_STK_CVCP_P"
            
            def description(self):
                return "CVCP_P"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_STK_ESP_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "FMPM_TCA_DS3E3_STK_ESP_P"
            
            def description(self):
                return "ESP_P"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_STK_ESCP_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "FMPM_TCA_DS3E3_STK_ESCP_P"
            
            def description(self):
                return "ESCP_P"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_STK_ESAP_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "FMPM_TCA_DS3E3_STK_ESAP_P"
            
            def description(self):
                return "ESAP_P"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_STK_ESACP_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "FMPM_TCA_DS3E3_STK_ESACP_P"
            
            def description(self):
                return "ESACP_P"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_STK_ESBP_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "FMPM_TCA_DS3E3_STK_ESBP_P"
            
            def description(self):
                return "ESBP_P"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_STK_ESBCP_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "FMPM_TCA_DS3E3_STK_ESBCP_P"
            
            def description(self):
                return "ESBCP_P"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_STK_SESP_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "FMPM_TCA_DS3E3_STK_SESP_P"
            
            def description(self):
                return "SESP_P"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_STK_SESCP_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "FMPM_TCA_DS3E3_STK_SESCP_P"
            
            def description(self):
                return "SESCP_P"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_STK_SAS_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "FMPM_TCA_DS3E3_STK_SAS_P"
            
            def description(self):
                return "SAS_P"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_STK_AISS_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "FMPM_TCA_DS3E3_STK_AISS_P"
            
            def description(self):
                return "AISS_P"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_STK_UAS_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "FMPM_TCA_DS3E3_STK_UAS_P"
            
            def description(self):
                return "UAS_P"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_STK_UASCP_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "FMPM_TCA_DS3E3_STK_UASCP_P"
            
            def description(self):
                return "UASCP_P"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_STK_CVCP_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "FMPM_TCA_DS3E3_STK_CVCP_PFE"
            
            def description(self):
                return "CVCP_PFE"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_STK_ESCP_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "FMPM_TCA_DS3E3_STK_ESCP_PFE"
            
            def description(self):
                return "ESCP_PFE"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_STK_ESACP_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_TCA_DS3E3_STK_ESACP_PFE"
            
            def description(self):
                return "ESACP_PFE"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_STK_ESBCP_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_TCA_DS3E3_STK_ESBCP_PFE"
            
            def description(self):
                return "ESBCP_PFE"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_STK_SESCP_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_TCA_DS3E3_STK_SESCP_PFE"
            
            def description(self):
                return "SESCP_PFE"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_STK_SASCP_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_TCA_DS3E3_STK_SASCP_PFE"
            
            def description(self):
                return "SASCP_PFE"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_STK_UASCP_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_TCA_DS3E3_STK_UASCP_PFE"
            
            def description(self):
                return "UASCP_PFE"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_STK_FC_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_TCA_DS3E3_STK_FC_P"
            
            def description(self):
                return "FC_P"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_STK_FCCP_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_DS3E3_STK_FCCP_PFE"
            
            def description(self):
                return "FCCP_PFE"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_DS3E3_STK_CV_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_STK_CV_L()
            allFields["FMPM_TCA_DS3E3_STK_ES_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_STK_ES_L()
            allFields["FMPM_TCA_DS3E3_STK_ESA_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_STK_ESA_L()
            allFields["FMPM_TCA_DS3E3_STK_ESB_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_STK_ESB_L()
            allFields["FMPM_TCA_DS3E3_STK_SES_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_STK_SES_L()
            allFields["FMPM_TCA_DS3E3_STK_LOSS_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_STK_LOSS_L()
            allFields["FMPM_TCA_DS3E3_STK_CVP_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_STK_CVP_P()
            allFields["FMPM_TCA_DS3E3_STK_CVCP_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_STK_CVCP_P()
            allFields["FMPM_TCA_DS3E3_STK_ESP_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_STK_ESP_P()
            allFields["FMPM_TCA_DS3E3_STK_ESCP_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_STK_ESCP_P()
            allFields["FMPM_TCA_DS3E3_STK_ESAP_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_STK_ESAP_P()
            allFields["FMPM_TCA_DS3E3_STK_ESACP_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_STK_ESACP_P()
            allFields["FMPM_TCA_DS3E3_STK_ESBP_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_STK_ESBP_P()
            allFields["FMPM_TCA_DS3E3_STK_ESBCP_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_STK_ESBCP_P()
            allFields["FMPM_TCA_DS3E3_STK_SESP_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_STK_SESP_P()
            allFields["FMPM_TCA_DS3E3_STK_SESCP_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_STK_SESCP_P()
            allFields["FMPM_TCA_DS3E3_STK_SAS_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_STK_SAS_P()
            allFields["FMPM_TCA_DS3E3_STK_AISS_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_STK_AISS_P()
            allFields["FMPM_TCA_DS3E3_STK_UAS_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_STK_UAS_P()
            allFields["FMPM_TCA_DS3E3_STK_UASCP_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_STK_UASCP_P()
            allFields["FMPM_TCA_DS3E3_STK_CVCP_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_STK_CVCP_PFE()
            allFields["FMPM_TCA_DS3E3_STK_ESCP_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_STK_ESCP_PFE()
            allFields["FMPM_TCA_DS3E3_STK_ESACP_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_STK_ESACP_PFE()
            allFields["FMPM_TCA_DS3E3_STK_ESBCP_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_STK_ESBCP_PFE()
            allFields["FMPM_TCA_DS3E3_STK_SESCP_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_STK_SESCP_PFE()
            allFields["FMPM_TCA_DS3E3_STK_SASCP_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_STK_SASCP_PFE()
            allFields["FMPM_TCA_DS3E3_STK_UASCP_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_STK_UASCP_PFE()
            allFields["FMPM_TCA_DS3E3_STK_FC_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_STK_FC_P()
            allFields["FMPM_TCA_DS3E3_STK_FCCP_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_STK_FCCP_PFE()
            return allFields

    class _FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA DS3E3 Framer Interrupt MASK Per Type of Per DS3E3"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x5A800+$G*0x20+$H"
            
        def startAddress(self):
            return 0x0005a800
            
        def endAddress(self):
            return 0x0005a9ff

        class _FMPM_TCA_DS3E3_MSK_CV_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "FMPM_TCA_DS3E3_MSK_CV_L"
            
            def description(self):
                return "CV_L"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_MSK_ES_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "FMPM_TCA_DS3E3_MSK_ES_L"
            
            def description(self):
                return "ES_L"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_MSK_ESA_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "FMPM_TCA_DS3E3_MSK_ESA_L"
            
            def description(self):
                return "ESA_L"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_MSK_ESB_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "FMPM_TCA_DS3E3_MSK_ESB_L"
            
            def description(self):
                return "ESB_L"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_MSK_SES_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "FMPM_TCA_DS3E3_MSK_SES_L"
            
            def description(self):
                return "SES_L"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_MSK_LOSS_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "FMPM_TCA_DS3E3_MSK_LOSS_L"
            
            def description(self):
                return "LOSS_L"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_MSK_CVP_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "FMPM_TCA_DS3E3_MSK_CVP_P"
            
            def description(self):
                return "CVP_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_MSK_CVCP_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "FMPM_TCA_DS3E3_MSK_CVCP_P"
            
            def description(self):
                return "CVCP_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_MSK_ESP_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "FMPM_TCA_DS3E3_MSK_ESP_P"
            
            def description(self):
                return "ESP_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_MSK_ESCP_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "FMPM_TCA_DS3E3_MSK_ESCP_P"
            
            def description(self):
                return "ESCP_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_MSK_ESAP_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "FMPM_TCA_DS3E3_MSK_ESAP_P"
            
            def description(self):
                return "ESAP_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_MSK_ESACP_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "FMPM_TCA_DS3E3_MSK_ESACP_P"
            
            def description(self):
                return "ESACP_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_MSK_ESBP_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "FMPM_TCA_DS3E3_MSK_ESBP_P"
            
            def description(self):
                return "ESBP_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_MSK_ESBCP_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "FMPM_TCA_DS3E3_MSK_ESBCP_P"
            
            def description(self):
                return "ESBCP_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_MSK_SESP_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "FMPM_TCA_DS3E3_MSK_SESP_P"
            
            def description(self):
                return "SESP_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_MSK_SESCP_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "FMPM_TCA_DS3E3_MSK_SESCP_P"
            
            def description(self):
                return "SESCP_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_MSK_SAS_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "FMPM_TCA_DS3E3_MSK_SAS_P"
            
            def description(self):
                return "SAS_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_MSK_AISS_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "FMPM_TCA_DS3E3_MSK_AISS_P"
            
            def description(self):
                return "AISS_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_MSK_UAS_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "FMPM_TCA_DS3E3_MSK_UAS_P"
            
            def description(self):
                return "UAS_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_MSK_UASCP_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "FMPM_TCA_DS3E3_MSK_UASCP_P"
            
            def description(self):
                return "UASCP_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_MSK_CVCP_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "FMPM_TCA_DS3E3_MSK_CVCP_PFE"
            
            def description(self):
                return "CVCP_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_MSK_ESCP_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "FMPM_TCA_DS3E3_MSK_ESCP_PFE"
            
            def description(self):
                return "ESCP_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_MSK_ESACP_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_TCA_DS3E3_MSK_ESACP_PFE"
            
            def description(self):
                return "ESACP_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_MSK_ESBCP_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_TCA_DS3E3_MSK_ESBCP_PFE"
            
            def description(self):
                return "ESBCP_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_MSK_SESCP_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_TCA_DS3E3_MSK_SESCP_PFE"
            
            def description(self):
                return "SESCP_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_MSK_SASCP_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_TCA_DS3E3_MSK_SASCP_PFE"
            
            def description(self):
                return "SASCP_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_MSK_UASCP_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_TCA_DS3E3_MSK_UASCP_PFE"
            
            def description(self):
                return "UASCP_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_MSK_FC_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_TCA_DS3E3_MSK_FC_P"
            
            def description(self):
                return "FC_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_MSK_FCCP_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_DS3E3_MSK_FCCP_PFE"
            
            def description(self):
                return "FCCP_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_DS3E3_MSK_CV_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_MSK_CV_L()
            allFields["FMPM_TCA_DS3E3_MSK_ES_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_MSK_ES_L()
            allFields["FMPM_TCA_DS3E3_MSK_ESA_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_MSK_ESA_L()
            allFields["FMPM_TCA_DS3E3_MSK_ESB_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_MSK_ESB_L()
            allFields["FMPM_TCA_DS3E3_MSK_SES_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_MSK_SES_L()
            allFields["FMPM_TCA_DS3E3_MSK_LOSS_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_MSK_LOSS_L()
            allFields["FMPM_TCA_DS3E3_MSK_CVP_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_MSK_CVP_P()
            allFields["FMPM_TCA_DS3E3_MSK_CVCP_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_MSK_CVCP_P()
            allFields["FMPM_TCA_DS3E3_MSK_ESP_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_MSK_ESP_P()
            allFields["FMPM_TCA_DS3E3_MSK_ESCP_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_MSK_ESCP_P()
            allFields["FMPM_TCA_DS3E3_MSK_ESAP_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_MSK_ESAP_P()
            allFields["FMPM_TCA_DS3E3_MSK_ESACP_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_MSK_ESACP_P()
            allFields["FMPM_TCA_DS3E3_MSK_ESBP_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_MSK_ESBP_P()
            allFields["FMPM_TCA_DS3E3_MSK_ESBCP_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_MSK_ESBCP_P()
            allFields["FMPM_TCA_DS3E3_MSK_SESP_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_MSK_SESP_P()
            allFields["FMPM_TCA_DS3E3_MSK_SESCP_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_MSK_SESCP_P()
            allFields["FMPM_TCA_DS3E3_MSK_SAS_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_MSK_SAS_P()
            allFields["FMPM_TCA_DS3E3_MSK_AISS_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_MSK_AISS_P()
            allFields["FMPM_TCA_DS3E3_MSK_UAS_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_MSK_UAS_P()
            allFields["FMPM_TCA_DS3E3_MSK_UASCP_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_MSK_UASCP_P()
            allFields["FMPM_TCA_DS3E3_MSK_CVCP_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_MSK_CVCP_PFE()
            allFields["FMPM_TCA_DS3E3_MSK_ESCP_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_MSK_ESCP_PFE()
            allFields["FMPM_TCA_DS3E3_MSK_ESACP_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_MSK_ESACP_PFE()
            allFields["FMPM_TCA_DS3E3_MSK_ESBCP_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_MSK_ESBCP_PFE()
            allFields["FMPM_TCA_DS3E3_MSK_SESCP_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_MSK_SESCP_PFE()
            allFields["FMPM_TCA_DS3E3_MSK_SASCP_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_MSK_SASCP_PFE()
            allFields["FMPM_TCA_DS3E3_MSK_UASCP_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_MSK_UASCP_PFE()
            allFields["FMPM_TCA_DS3E3_MSK_FC_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_MSK_FC_P()
            allFields["FMPM_TCA_DS3E3_MSK_FCCP_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_MSK_FCCP_PFE()
            return allFields

    class _FMPM_TCA_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA DS3E3 Framer Interrupt Current Status Per Type of Per DS3E3"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x5AA00+$G*0x20+$H"
            
        def startAddress(self):
            return 0x0005aa00
            
        def endAddress(self):
            return 0x0005abff

        class _FMPM_TCA_DS3E3_CUR_CV_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "FMPM_TCA_DS3E3_CUR_CV_L"
            
            def description(self):
                return "CV_L"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_CUR_ES_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "FMPM_TCA_DS3E3_CUR_ES_L"
            
            def description(self):
                return "ES_L"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_CUR_ESA_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "FMPM_TCA_DS3E3_CUR_ESA_L"
            
            def description(self):
                return "ESA_L"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_CUR_ESB_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "FMPM_TCA_DS3E3_CUR_ESB_L"
            
            def description(self):
                return "ESB_L"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_CUR_SES_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "FMPM_TCA_DS3E3_CUR_SES_L"
            
            def description(self):
                return "SES_L"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_CUR_LOSS_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "FMPM_TCA_DS3E3_CUR_LOSS_L"
            
            def description(self):
                return "LOSS_L"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_CUR_CVP_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "FMPM_TCA_DS3E3_CUR_CVP_P"
            
            def description(self):
                return "CVP_P"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_CUR_CVCP_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "FMPM_TCA_DS3E3_CUR_CVCP_P"
            
            def description(self):
                return "CVCP_P"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_CUR_ESP_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "FMPM_TCA_DS3E3_CUR_ESP_P"
            
            def description(self):
                return "ESP_P"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_CUR_ESCP_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "FMPM_TCA_DS3E3_CUR_ESCP_P"
            
            def description(self):
                return "ESCP_P"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_CUR_ESAP_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "FMPM_TCA_DS3E3_CUR_ESAP_P"
            
            def description(self):
                return "ESAP_P"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_CUR_ESACP_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "FMPM_TCA_DS3E3_CUR_ESACP_P"
            
            def description(self):
                return "ESACP_P"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_CUR_ESBP_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "FMPM_TCA_DS3E3_CUR_ESBP_P"
            
            def description(self):
                return "ESBP_P"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_CUR_ESBCP_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "FMPM_TCA_DS3E3_CUR_ESBCP_P"
            
            def description(self):
                return "ESBCP_P"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_CUR_SESP_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "FMPM_TCA_DS3E3_CUR_SESP_P"
            
            def description(self):
                return "SESP_P"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_CUR_SESCP_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "FMPM_TCA_DS3E3_CUR_SESCP_P"
            
            def description(self):
                return "SESCP_P"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_CUR_SAS_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "FMPM_TCA_DS3E3_CUR_SAS_P"
            
            def description(self):
                return "SAS_P"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_CUR_AISS_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "FMPM_TCA_DS3E3_CUR_AISS_P"
            
            def description(self):
                return "AISS_P"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_CUR_UAS_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "FMPM_TCA_DS3E3_CUR_UAS_P"
            
            def description(self):
                return "UAS_P"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_CUR_UASCP_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "FMPM_TCA_DS3E3_CUR_UASCP_P"
            
            def description(self):
                return "UASCP_P"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_CUR_CVCP_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "FMPM_TCA_DS3E3_CUR_CVCP_PFE"
            
            def description(self):
                return "CVCP_PFE"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_CUR_ESCP_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "FMPM_TCA_DS3E3_CUR_ESCP_PFE"
            
            def description(self):
                return "ESCP_PFE"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_CUR_ESACP_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_TCA_DS3E3_CUR_ESACP_PFE"
            
            def description(self):
                return "ESACP_PFE"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_CUR_ESBCP_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_TCA_DS3E3_CUR_ESBCP_PFE"
            
            def description(self):
                return "ESBCP_PFE"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_CUR_SESCP_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_TCA_DS3E3_CUR_SESCP_PFE"
            
            def description(self):
                return "SESCP_PFE"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_CUR_SASCP_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_TCA_DS3E3_CUR_SASCP_PFE"
            
            def description(self):
                return "SASCP_PFE"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_CUR_UASCP_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_TCA_DS3E3_CUR_UASCP_PFE"
            
            def description(self):
                return "UASCP_PFE"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_CUR_FC_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_TCA_DS3E3_CUR_FC_P"
            
            def description(self):
                return "FC_P"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS3E3_CUR_FCCP_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_DS3E3_CUR_FCCP_PFE"
            
            def description(self):
                return "FCCP_PFE"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_DS3E3_CUR_CV_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_CUR_CV_L()
            allFields["FMPM_TCA_DS3E3_CUR_ES_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_CUR_ES_L()
            allFields["FMPM_TCA_DS3E3_CUR_ESA_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_CUR_ESA_L()
            allFields["FMPM_TCA_DS3E3_CUR_ESB_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_CUR_ESB_L()
            allFields["FMPM_TCA_DS3E3_CUR_SES_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_CUR_SES_L()
            allFields["FMPM_TCA_DS3E3_CUR_LOSS_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_CUR_LOSS_L()
            allFields["FMPM_TCA_DS3E3_CUR_CVP_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_CUR_CVP_P()
            allFields["FMPM_TCA_DS3E3_CUR_CVCP_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_CUR_CVCP_P()
            allFields["FMPM_TCA_DS3E3_CUR_ESP_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_CUR_ESP_P()
            allFields["FMPM_TCA_DS3E3_CUR_ESCP_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_CUR_ESCP_P()
            allFields["FMPM_TCA_DS3E3_CUR_ESAP_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_CUR_ESAP_P()
            allFields["FMPM_TCA_DS3E3_CUR_ESACP_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_CUR_ESACP_P()
            allFields["FMPM_TCA_DS3E3_CUR_ESBP_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_CUR_ESBP_P()
            allFields["FMPM_TCA_DS3E3_CUR_ESBCP_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_CUR_ESBCP_P()
            allFields["FMPM_TCA_DS3E3_CUR_SESP_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_CUR_SESP_P()
            allFields["FMPM_TCA_DS3E3_CUR_SESCP_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_CUR_SESCP_P()
            allFields["FMPM_TCA_DS3E3_CUR_SAS_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_CUR_SAS_P()
            allFields["FMPM_TCA_DS3E3_CUR_AISS_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_CUR_AISS_P()
            allFields["FMPM_TCA_DS3E3_CUR_UAS_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_CUR_UAS_P()
            allFields["FMPM_TCA_DS3E3_CUR_UASCP_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_CUR_UASCP_P()
            allFields["FMPM_TCA_DS3E3_CUR_CVCP_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_CUR_CVCP_PFE()
            allFields["FMPM_TCA_DS3E3_CUR_ESCP_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_CUR_ESCP_PFE()
            allFields["FMPM_TCA_DS3E3_CUR_ESACP_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_CUR_ESACP_PFE()
            allFields["FMPM_TCA_DS3E3_CUR_ESBCP_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_CUR_ESBCP_PFE()
            allFields["FMPM_TCA_DS3E3_CUR_SESCP_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_CUR_SESCP_PFE()
            allFields["FMPM_TCA_DS3E3_CUR_SASCP_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_CUR_SASCP_PFE()
            allFields["FMPM_TCA_DS3E3_CUR_UASCP_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_CUR_UASCP_PFE()
            allFields["FMPM_TCA_DS3E3_CUR_FC_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_CUR_FC_P()
            allFields["FMPM_TCA_DS3E3_CUR_FCCP_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3._FMPM_TCA_DS3E3_CUR_FCCP_PFE()
            return allFields

    class _FMPM_TCA_STS24_LO_Interrupt_OR(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA STS24 LO Interrupt OR"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00058002
            
        def endAddress(self):
            return 0xffffffff

        class _FMPM_TCA_STS24LO_OR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_STS24LO_OR"
            
            def description(self):
                return "Interrupt STS24 LO OR"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_STS24LO_OR"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS24_LO_Interrupt_OR._FMPM_TCA_STS24LO_OR()
            return allFields

    class _FMPM_TCA_STS1_LO_Interrupt_OR(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA STS1 LO Interrupt OR"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x580C0+$G"
            
        def startAddress(self):
            return 0x000580c0
            
        def endAddress(self):
            return 0x000580cf

        class _FMPM_TCA_STS1LO_OR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_STS1LO_OR"
            
            def description(self):
                return "Interrupt STS1 LO OR"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_STS1LO_OR"] = _AF6CNC0021_RD_PM._FMPM_TCA_STS1_LO_Interrupt_OR._FMPM_TCA_STS1LO_OR()
            return allFields

    class _FMPM_TCA_VTTU_Interrupt_OR_AND_MASK_Per_VTTU(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA VTTU Interrupt OR AND MASK Per VTTU"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x5AC00+$G*0x20+$H"
            
        def startAddress(self):
            return 0x0005ac00
            
        def endAddress(self):
            return 0x0005adff

        class _FMPM_TCA_VTTU_OAMSK(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_VTTU_OAMSK"
            
            def description(self):
                return "VT/TU Interrupt"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_VTTU_OAMSK"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_OR_AND_MASK_Per_VTTU._FMPM_TCA_VTTU_OAMSK()
            return allFields

    class _FMPM_TCA_VTTU_Interrupt_MASK_Per_VTTU(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA VTTU Interrupt MASK Per VTTU"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x5AE00+$G*0x20+$H"
            
        def startAddress(self):
            return 0x0005ae00
            
        def endAddress(self):
            return 0x0005afff

        class _FMPM_TCA_VTTU_MSK(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_VTTU_MSK"
            
            def description(self):
                return "VT/TU Interrupt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_VTTU_MSK"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_MASK_Per_VTTU._FMPM_TCA_VTTU_MSK()
            return allFields

    class _FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA VTTU Interrupt Sticky Per Type Per VTTU"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x64000+$G*0x400+$H*0x20+$I"
            
        def startAddress(self):
            return 0x00064000
            
        def endAddress(self):
            return 0x00067fff

        class _FMPM_TCA_VTTU_STK_CV_V(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "FMPM_TCA_VTTU_STK_CV_V"
            
            def description(self):
                return "CV_V"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_STK_ES_V(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "FMPM_TCA_VTTU_STK_ES_V"
            
            def description(self):
                return "ES_V"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_STK_SES_V(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "FMPM_TCA_VTTU_STK_SES_V"
            
            def description(self):
                return "SES_V"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_STK_UAS_V(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "FMPM_TCA_VTTU_STK_UAS_V"
            
            def description(self):
                return "UAS_V"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_STK_PPJC_VDet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "FMPM_TCA_VTTU_STK_PPJC_VDet"
            
            def description(self):
                return "PPJC_VDet"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_STK_NPJC_VDet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "FMPM_TCA_VTTU_STK_NPJC_VDet"
            
            def description(self):
                return "NPJC_VDet"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_STK_PPJC_VGen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "FMPM_TCA_VTTU_STK_PPJC_VGen"
            
            def description(self):
                return "PPJC_VGen"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_STK_NPJC_VGen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "FMPM_TCA_VTTU_STK_NPJC_VGen"
            
            def description(self):
                return "NPJC_VGen"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_STK_PJCDiff_V(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "FMPM_TCA_VTTU_STK_PJCDiff_V"
            
            def description(self):
                return "PJCDiff_V"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_STK_PJCS_VDet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "FMPM_TCA_VTTU_STK_PJCS_VDet"
            
            def description(self):
                return "PJCS_VDet"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_STK_PJCS_VGen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_TCA_VTTU_STK_PJCS_VGen"
            
            def description(self):
                return "PJCS_VGen"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_STK_CV_VFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_TCA_VTTU_STK_CV_VFE"
            
            def description(self):
                return "CV_VFE"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_STK_ES_VFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_TCA_VTTU_STK_ES_VFE"
            
            def description(self):
                return "ES_VFE"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_STK_SES_VFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_TCA_VTTU_STK_SES_VFE"
            
            def description(self):
                return "SES_VFE"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_STK_UAS_VFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_TCA_VTTU_STK_UAS_VFE"
            
            def description(self):
                return "UAS_VFE"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_STK_FC_V(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_TCA_VTTU_STK_FC_V"
            
            def description(self):
                return "FC_V"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_STK_FC_VFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_VTTU_STK_FC_VFE"
            
            def description(self):
                return "FC_VFE"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_VTTU_STK_CV_V"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU._FMPM_TCA_VTTU_STK_CV_V()
            allFields["FMPM_TCA_VTTU_STK_ES_V"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU._FMPM_TCA_VTTU_STK_ES_V()
            allFields["FMPM_TCA_VTTU_STK_SES_V"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU._FMPM_TCA_VTTU_STK_SES_V()
            allFields["FMPM_TCA_VTTU_STK_UAS_V"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU._FMPM_TCA_VTTU_STK_UAS_V()
            allFields["FMPM_TCA_VTTU_STK_PPJC_VDet"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU._FMPM_TCA_VTTU_STK_PPJC_VDet()
            allFields["FMPM_TCA_VTTU_STK_NPJC_VDet"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU._FMPM_TCA_VTTU_STK_NPJC_VDet()
            allFields["FMPM_TCA_VTTU_STK_PPJC_VGen"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU._FMPM_TCA_VTTU_STK_PPJC_VGen()
            allFields["FMPM_TCA_VTTU_STK_NPJC_VGen"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU._FMPM_TCA_VTTU_STK_NPJC_VGen()
            allFields["FMPM_TCA_VTTU_STK_PJCDiff_V"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU._FMPM_TCA_VTTU_STK_PJCDiff_V()
            allFields["FMPM_TCA_VTTU_STK_PJCS_VDet"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU._FMPM_TCA_VTTU_STK_PJCS_VDet()
            allFields["FMPM_TCA_VTTU_STK_PJCS_VGen"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU._FMPM_TCA_VTTU_STK_PJCS_VGen()
            allFields["FMPM_TCA_VTTU_STK_CV_VFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU._FMPM_TCA_VTTU_STK_CV_VFE()
            allFields["FMPM_TCA_VTTU_STK_ES_VFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU._FMPM_TCA_VTTU_STK_ES_VFE()
            allFields["FMPM_TCA_VTTU_STK_SES_VFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU._FMPM_TCA_VTTU_STK_SES_VFE()
            allFields["FMPM_TCA_VTTU_STK_UAS_VFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU._FMPM_TCA_VTTU_STK_UAS_VFE()
            allFields["FMPM_TCA_VTTU_STK_FC_V"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU._FMPM_TCA_VTTU_STK_FC_V()
            allFields["FMPM_TCA_VTTU_STK_FC_VFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU._FMPM_TCA_VTTU_STK_FC_VFE()
            return allFields

    class _FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA VTTU Interrupt MASK Per Type Per VTTU"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x68000+$G*0x400+$H*0x20+$I"
            
        def startAddress(self):
            return 0x00068000
            
        def endAddress(self):
            return 0x0006bfff

        class _FMPM_TCA_VTTU_MSK_CV_V(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "FMPM_TCA_VTTU_MSK_CV_V"
            
            def description(self):
                return "CV_V"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_MSK_ES_V(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "FMPM_TCA_VTTU_MSK_ES_V"
            
            def description(self):
                return "ES_V"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_MSK_SES_V(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "FMPM_TCA_VTTU_MSK_SES_V"
            
            def description(self):
                return "SES_V"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_MSK_UAS_V(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "FMPM_TCA_VTTU_MSK_UAS_V"
            
            def description(self):
                return "UAS_V"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_MSK_PPJC_VDet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "FMPM_TCA_VTTU_MSK_PPJC_VDet"
            
            def description(self):
                return "PPJC_VDet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_MSK_NPJC_VDet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "FMPM_TCA_VTTU_MSK_NPJC_VDet"
            
            def description(self):
                return "NPJC_VDet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_MSK_PPJC_VGen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "FMPM_TCA_VTTU_MSK_PPJC_VGen"
            
            def description(self):
                return "PPJC_VGen"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_MSK_NPJC_VGen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "FMPM_TCA_VTTU_MSK_NPJC_VGen"
            
            def description(self):
                return "NPJC_VGen"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_MSK_PJCDiff_V(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "FMPM_TCA_VTTU_MSK_PJCDiff_V"
            
            def description(self):
                return "PJCDiff_V"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_MSK_PJCS_VDet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "FMPM_TCA_VTTU_MSK_PJCS_VDet"
            
            def description(self):
                return "PJCS_VDet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_MSK_PJCS_VGen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_TCA_VTTU_MSK_PJCS_VGen"
            
            def description(self):
                return "PJCS_VGen"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_MSK_CV_VFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_TCA_VTTU_MSK_CV_VFE"
            
            def description(self):
                return "CV_VFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_MSK_ES_VFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_TCA_VTTU_MSK_ES_VFE"
            
            def description(self):
                return "ES_VFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_MSK_SES_VFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_TCA_VTTU_MSK_SES_VFE"
            
            def description(self):
                return "SES_VFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_MSK_UAS_VFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_TCA_VTTU_MSK_UAS_VFE"
            
            def description(self):
                return "UAS_VFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_MSK_FC_V(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_TCA_VTTU_MSK_FC_V"
            
            def description(self):
                return "FC_V"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_MSK_FC_VFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_VTTU_MSK_FC_VFE"
            
            def description(self):
                return "FC_VFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_VTTU_MSK_CV_V"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU._FMPM_TCA_VTTU_MSK_CV_V()
            allFields["FMPM_TCA_VTTU_MSK_ES_V"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU._FMPM_TCA_VTTU_MSK_ES_V()
            allFields["FMPM_TCA_VTTU_MSK_SES_V"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU._FMPM_TCA_VTTU_MSK_SES_V()
            allFields["FMPM_TCA_VTTU_MSK_UAS_V"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU._FMPM_TCA_VTTU_MSK_UAS_V()
            allFields["FMPM_TCA_VTTU_MSK_PPJC_VDet"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU._FMPM_TCA_VTTU_MSK_PPJC_VDet()
            allFields["FMPM_TCA_VTTU_MSK_NPJC_VDet"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU._FMPM_TCA_VTTU_MSK_NPJC_VDet()
            allFields["FMPM_TCA_VTTU_MSK_PPJC_VGen"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU._FMPM_TCA_VTTU_MSK_PPJC_VGen()
            allFields["FMPM_TCA_VTTU_MSK_NPJC_VGen"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU._FMPM_TCA_VTTU_MSK_NPJC_VGen()
            allFields["FMPM_TCA_VTTU_MSK_PJCDiff_V"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU._FMPM_TCA_VTTU_MSK_PJCDiff_V()
            allFields["FMPM_TCA_VTTU_MSK_PJCS_VDet"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU._FMPM_TCA_VTTU_MSK_PJCS_VDet()
            allFields["FMPM_TCA_VTTU_MSK_PJCS_VGen"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU._FMPM_TCA_VTTU_MSK_PJCS_VGen()
            allFields["FMPM_TCA_VTTU_MSK_CV_VFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU._FMPM_TCA_VTTU_MSK_CV_VFE()
            allFields["FMPM_TCA_VTTU_MSK_ES_VFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU._FMPM_TCA_VTTU_MSK_ES_VFE()
            allFields["FMPM_TCA_VTTU_MSK_SES_VFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU._FMPM_TCA_VTTU_MSK_SES_VFE()
            allFields["FMPM_TCA_VTTU_MSK_UAS_VFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU._FMPM_TCA_VTTU_MSK_UAS_VFE()
            allFields["FMPM_TCA_VTTU_MSK_FC_V"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU._FMPM_TCA_VTTU_MSK_FC_V()
            allFields["FMPM_TCA_VTTU_MSK_FC_VFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU._FMPM_TCA_VTTU_MSK_FC_VFE()
            return allFields

    class _FMPM_TCA_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA VTTU Interrupt Current Status Per Type Per VTTU"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x6C000+$G*0x400+$H*0x20+$I"
            
        def startAddress(self):
            return 0x0006c000
            
        def endAddress(self):
            return 0x0006ffff

        class _FMPM_TCA_VTTU_CUR_CV_V(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "FMPM_TCA_VTTU_CUR_CV_V"
            
            def description(self):
                return "CV_V"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_CUR_ES_V(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "FMPM_TCA_VTTU_CUR_ES_V"
            
            def description(self):
                return "ES_V"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_CUR_SES_V(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "FMPM_TCA_VTTU_CUR_SES_V"
            
            def description(self):
                return "SES_V"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_CUR_UAS_V(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "FMPM_TCA_VTTU_CUR_UAS_V"
            
            def description(self):
                return "UAS_V"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_CUR_PPJC_VDet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "FMPM_TCA_VTTU_CUR_PPJC_VDet"
            
            def description(self):
                return "PPJC_VDet"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_CUR_NPJC_VDet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "FMPM_TCA_VTTU_CUR_NPJC_VDet"
            
            def description(self):
                return "NPJC_VDet"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_CUR_PPJC_VGen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "FMPM_TCA_VTTU_CUR_PPJC_VGen"
            
            def description(self):
                return "PPJC_VGen"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_CUR_NPJC_VGen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "FMPM_TCA_VTTU_CUR_NPJC_VGen"
            
            def description(self):
                return "NPJC_VGen"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_CUR_PJCDiff_V(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "FMPM_TCA_VTTU_CUR_PJCDiff_V"
            
            def description(self):
                return "PJCDiff_V"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_CUR_PJCS_VDet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "FMPM_TCA_VTTU_CUR_PJCS_VDet"
            
            def description(self):
                return "PJCS_VDet"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_CUR_PJCS_VGen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_TCA_VTTU_CUR_PJCS_VGen"
            
            def description(self):
                return "PJCS_VGen"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_CUR_CV_VFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_TCA_VTTU_CUR_CV_VFE"
            
            def description(self):
                return "CV_VFE"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_CUR_ES_VFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_TCA_VTTU_CUR_ES_VFE"
            
            def description(self):
                return "ES_VFE"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_CUR_SES_VFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_TCA_VTTU_CUR_SES_VFE"
            
            def description(self):
                return "SES_VFE"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_CUR_UAS_VFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_TCA_VTTU_CUR_UAS_VFE"
            
            def description(self):
                return "UAS_VFE"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_CUR_FC_V(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_TCA_VTTU_CUR_FC_V"
            
            def description(self):
                return "FC_V"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_VTTU_CUR_FC_VFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_VTTU_CUR_FC_VFE"
            
            def description(self):
                return "FC_VFE"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_VTTU_CUR_CV_V"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_TCA_VTTU_CUR_CV_V()
            allFields["FMPM_TCA_VTTU_CUR_ES_V"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_TCA_VTTU_CUR_ES_V()
            allFields["FMPM_TCA_VTTU_CUR_SES_V"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_TCA_VTTU_CUR_SES_V()
            allFields["FMPM_TCA_VTTU_CUR_UAS_V"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_TCA_VTTU_CUR_UAS_V()
            allFields["FMPM_TCA_VTTU_CUR_PPJC_VDet"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_TCA_VTTU_CUR_PPJC_VDet()
            allFields["FMPM_TCA_VTTU_CUR_NPJC_VDet"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_TCA_VTTU_CUR_NPJC_VDet()
            allFields["FMPM_TCA_VTTU_CUR_PPJC_VGen"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_TCA_VTTU_CUR_PPJC_VGen()
            allFields["FMPM_TCA_VTTU_CUR_NPJC_VGen"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_TCA_VTTU_CUR_NPJC_VGen()
            allFields["FMPM_TCA_VTTU_CUR_PJCDiff_V"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_TCA_VTTU_CUR_PJCDiff_V()
            allFields["FMPM_TCA_VTTU_CUR_PJCS_VDet"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_TCA_VTTU_CUR_PJCS_VDet()
            allFields["FMPM_TCA_VTTU_CUR_PJCS_VGen"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_TCA_VTTU_CUR_PJCS_VGen()
            allFields["FMPM_TCA_VTTU_CUR_CV_VFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_TCA_VTTU_CUR_CV_VFE()
            allFields["FMPM_TCA_VTTU_CUR_ES_VFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_TCA_VTTU_CUR_ES_VFE()
            allFields["FMPM_TCA_VTTU_CUR_SES_VFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_TCA_VTTU_CUR_SES_VFE()
            allFields["FMPM_TCA_VTTU_CUR_UAS_VFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_TCA_VTTU_CUR_UAS_VFE()
            allFields["FMPM_TCA_VTTU_CUR_FC_V"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_TCA_VTTU_CUR_FC_V()
            allFields["FMPM_TCA_VTTU_CUR_FC_VFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU._FMPM_TCA_VTTU_CUR_FC_VFE()
            return allFields

    class _FMPM_TCA_DS1E1_Level1_Group_Interrupt_OR(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA DS1E1 Level1 Group Interrupt OR"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00058003
            
        def endAddress(self):
            return 0xffffffff

        class _FMPM_FM_DS1E1_LEV1_OR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_DS1E1_LEV1_OR"
            
            def description(self):
                return "Interrupt DS1E1 level1 group OR"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_DS1E1_LEV1_OR"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Level1_Group_Interrupt_OR._FMPM_FM_DS1E1_LEV1_OR()
            return allFields

    class _FMPM_TCA_DS1E1_Level2_Group_Interrupt_OR(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA DS1E1 Level2 Group Interrupt OR"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x580D0+$G"
            
        def startAddress(self):
            return 0x000580d0
            
        def endAddress(self):
            return 0x000580df

        class _FMPM_TCA_DS1E1_LEV2_OR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_DS1E1_LEV2_OR"
            
            def description(self):
                return "Level2 Group Interrupt OR"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_DS1E1_LEV2_OR"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Level2_Group_Interrupt_OR._FMPM_TCA_DS1E1_LEV2_OR()
            return allFields

    class _FMPM_TCA_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA DS1E1 Framer Interrupt OR AND MASK Per DS1E1"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x5B000+$G*0x20+$H"
            
        def startAddress(self):
            return 0x0005b000
            
        def endAddress(self):
            return 0x0005b1ff

        class _FMPM_TCA_DS1E1_OAMSK(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_DS1E1_OAMSK"
            
            def description(self):
                return "DS1/E1 Framer Interrupt"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_DS1E1_OAMSK"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1._FMPM_TCA_DS1E1_OAMSK()
            return allFields

    class _FMPM_TCA_DS1E1_Framer_Interrupt_MASK_Per_DS1E1(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA DS1E1 Framer Interrupt MASK Per DS1E1"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x5B200+$G*0x20+$H"
            
        def startAddress(self):
            return 0x0005b200
            
        def endAddress(self):
            return 0x0005b3ff

        class _FMPM_TCA_DS1E1_MSK(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_DS1E1_MSK"
            
            def description(self):
                return "DS1/E1 Framer Interrupt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_DS1E1_MSK"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Framer_Interrupt_MASK_Per_DS1E1._FMPM_TCA_DS1E1_MSK()
            return allFields

    class _FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA DS1E1 Interrupt Sticky Per Type Per DS1E1"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x70000+$G*0x400+$H*0x20+$I"
            
        def startAddress(self):
            return 0x00070000
            
        def endAddress(self):
            return 0x00073fff

        class _FMPM_TCA_DS1E1_STK_CV_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "FMPM_TCA_DS1E1_STK_CV_L"
            
            def description(self):
                return "CV_L"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_STK_ES_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "FMPM_TCA_DS1E1_STK_ES_L"
            
            def description(self):
                return "ES_L"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_STK_SES_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "FMPM_TCA_DS1E1_STK_SES_L"
            
            def description(self):
                return "SES_L"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_STK_LOSS_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "FMPM_TCA_DS1E1_STK_LOSS_L"
            
            def description(self):
                return "LOSS_L"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_STK_CV_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "FMPM_TCA_DS1E1_STK_CV_P"
            
            def description(self):
                return "CV_P"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_STK_ES_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "FMPM_TCA_DS1E1_STK_ES_P"
            
            def description(self):
                return "ES_P"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_STK_SES_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "FMPM_TCA_DS1E1_STK_SES_P"
            
            def description(self):
                return "SES_P"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_STK_AISS_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "FMPM_TCA_DS1E1_STK_AISS_P"
            
            def description(self):
                return "AISS_P"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_STK_SAS_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "FMPM_TCA_DS1E1_STK_SAS_P"
            
            def description(self):
                return "SAS_P"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_STK_CSS_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "FMPM_TCA_DS1E1_STK_CSS_P"
            
            def description(self):
                return "CSS_P"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_STK_UAS_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "FMPM_TCA_DS1E1_STK_UAS_P"
            
            def description(self):
                return "UAS_P"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_STK_FC_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "FMPM_TCA_DS1E1_STK_FC_P"
            
            def description(self):
                return "FC_P"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_STK_ES_LFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_TCA_DS1E1_STK_ES_LFE"
            
            def description(self):
                return "ES_LFE"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_STK_SEFS_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_TCA_DS1E1_STK_SEFS_PFE"
            
            def description(self):
                return "SEFS_PFE"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_STK_ES_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_TCA_DS1E1_STK_ES_PFE"
            
            def description(self):
                return "ES_PFE"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_STK_SES_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_TCA_DS1E1_STK_SES_PFE"
            
            def description(self):
                return "SES_PFE"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_STK_CSS_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_TCA_DS1E1_STK_CSS_PFE"
            
            def description(self):
                return "CSS_PFE"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_STK_FC_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_TCA_DS1E1_STK_FC_PFE"
            
            def description(self):
                return "FC_PFE"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_STK_UAS_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_DS1E1_STK_UAS_PFE"
            
            def description(self):
                return "UAS_PFE"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_DS1E1_STK_CV_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_STK_CV_L()
            allFields["FMPM_TCA_DS1E1_STK_ES_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_STK_ES_L()
            allFields["FMPM_TCA_DS1E1_STK_SES_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_STK_SES_L()
            allFields["FMPM_TCA_DS1E1_STK_LOSS_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_STK_LOSS_L()
            allFields["FMPM_TCA_DS1E1_STK_CV_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_STK_CV_P()
            allFields["FMPM_TCA_DS1E1_STK_ES_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_STK_ES_P()
            allFields["FMPM_TCA_DS1E1_STK_SES_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_STK_SES_P()
            allFields["FMPM_TCA_DS1E1_STK_AISS_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_STK_AISS_P()
            allFields["FMPM_TCA_DS1E1_STK_SAS_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_STK_SAS_P()
            allFields["FMPM_TCA_DS1E1_STK_CSS_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_STK_CSS_P()
            allFields["FMPM_TCA_DS1E1_STK_UAS_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_STK_UAS_P()
            allFields["FMPM_TCA_DS1E1_STK_FC_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_STK_FC_P()
            allFields["FMPM_TCA_DS1E1_STK_ES_LFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_STK_ES_LFE()
            allFields["FMPM_TCA_DS1E1_STK_SEFS_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_STK_SEFS_PFE()
            allFields["FMPM_TCA_DS1E1_STK_ES_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_STK_ES_PFE()
            allFields["FMPM_TCA_DS1E1_STK_SES_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_STK_SES_PFE()
            allFields["FMPM_TCA_DS1E1_STK_CSS_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_STK_CSS_PFE()
            allFields["FMPM_TCA_DS1E1_STK_FC_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_STK_FC_PFE()
            allFields["FMPM_TCA_DS1E1_STK_UAS_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_STK_UAS_PFE()
            return allFields

    class _FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA DS1E1 Interrupt MASK Per Type Per DS1E1"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x74000+$G*0x400+$H*0x20+$I"
            
        def startAddress(self):
            return 0x00074000
            
        def endAddress(self):
            return 0x00077fff

        class _FMPM_TCA_DS1E1_MSK_CV_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "FMPM_TCA_DS1E1_MSK_CV_L"
            
            def description(self):
                return "CV_L"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_MSK_ES_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "FMPM_TCA_DS1E1_MSK_ES_L"
            
            def description(self):
                return "ES_L"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_MSK_SES_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "FMPM_TCA_DS1E1_MSK_SES_L"
            
            def description(self):
                return "SES_L"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_MSK_LOSS_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "FMPM_TCA_DS1E1_MSK_LOSS_L"
            
            def description(self):
                return "LOSS_L"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_MSK_CV_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "FMPM_TCA_DS1E1_MSK_CV_P"
            
            def description(self):
                return "CV_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_MSK_ES_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "FMPM_TCA_DS1E1_MSK_ES_P"
            
            def description(self):
                return "ES_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_MSK_SES_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "FMPM_TCA_DS1E1_MSK_SES_P"
            
            def description(self):
                return "SES_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_MSK_AISS_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "FMPM_TCA_DS1E1_MSK_AISS_P"
            
            def description(self):
                return "AISS_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_MSK_SAS_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "FMPM_TCA_DS1E1_MSK_SAS_P"
            
            def description(self):
                return "SAS_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_MSK_CSS_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "FMPM_TCA_DS1E1_MSK_CSS_P"
            
            def description(self):
                return "CSS_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_MSK_UAS_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "FMPM_TCA_DS1E1_MSK_UAS_P"
            
            def description(self):
                return "UAS_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_MSK_FC_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "FMPM_TCA_DS1E1_MSK_FC_P"
            
            def description(self):
                return "FC_P"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_MSK_ES_LFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_TCA_DS1E1_MSK_ES_LFE"
            
            def description(self):
                return "ES_LFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_MSK_SEFS_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_TCA_DS1E1_MSK_SEFS_PFE"
            
            def description(self):
                return "SEFS_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_MSK_ES_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_TCA_DS1E1_MSK_ES_PFE"
            
            def description(self):
                return "ES_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_MSK_SES_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_TCA_DS1E1_MSK_SES_PFE"
            
            def description(self):
                return "SES_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_MSK_CSS_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_TCA_DS1E1_MSK_CSS_PFE"
            
            def description(self):
                return "CSS_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_MSK_FC_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_TCA_DS1E1_MSK_FC_PFE"
            
            def description(self):
                return "FC_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_MSK_UAS_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_DS1E1_MSK_UAS_PFE"
            
            def description(self):
                return "UAS_PFE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_DS1E1_MSK_CV_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_MSK_CV_L()
            allFields["FMPM_TCA_DS1E1_MSK_ES_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_MSK_ES_L()
            allFields["FMPM_TCA_DS1E1_MSK_SES_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_MSK_SES_L()
            allFields["FMPM_TCA_DS1E1_MSK_LOSS_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_MSK_LOSS_L()
            allFields["FMPM_TCA_DS1E1_MSK_CV_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_MSK_CV_P()
            allFields["FMPM_TCA_DS1E1_MSK_ES_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_MSK_ES_P()
            allFields["FMPM_TCA_DS1E1_MSK_SES_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_MSK_SES_P()
            allFields["FMPM_TCA_DS1E1_MSK_AISS_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_MSK_AISS_P()
            allFields["FMPM_TCA_DS1E1_MSK_SAS_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_MSK_SAS_P()
            allFields["FMPM_TCA_DS1E1_MSK_CSS_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_MSK_CSS_P()
            allFields["FMPM_TCA_DS1E1_MSK_UAS_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_MSK_UAS_P()
            allFields["FMPM_TCA_DS1E1_MSK_FC_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_MSK_FC_P()
            allFields["FMPM_TCA_DS1E1_MSK_ES_LFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_MSK_ES_LFE()
            allFields["FMPM_TCA_DS1E1_MSK_SEFS_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_MSK_SEFS_PFE()
            allFields["FMPM_TCA_DS1E1_MSK_ES_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_MSK_ES_PFE()
            allFields["FMPM_TCA_DS1E1_MSK_SES_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_MSK_SES_PFE()
            allFields["FMPM_TCA_DS1E1_MSK_CSS_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_MSK_CSS_PFE()
            allFields["FMPM_TCA_DS1E1_MSK_FC_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_MSK_FC_PFE()
            allFields["FMPM_TCA_DS1E1_MSK_UAS_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_MSK_UAS_PFE()
            return allFields

    class _FMPM_TCA_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA DS1E1 Interrupt Current Status Per Type Per DS1E1"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x78000+$G*0x400+$H*0x20+$I"
            
        def startAddress(self):
            return 0x00078000
            
        def endAddress(self):
            return 0x0007bfff

        class _FMPM_TCA_DS1E1_CUR_CV_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "FMPM_TCA_DS1E1_CUR_CV_L"
            
            def description(self):
                return "CV_L"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_CUR_ES_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "FMPM_TCA_DS1E1_CUR_ES_L"
            
            def description(self):
                return "ES_L"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_CUR_SES_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "FMPM_TCA_DS1E1_CUR_SES_L"
            
            def description(self):
                return "SES_L"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_CUR_LOSS_L(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "FMPM_TCA_DS1E1_CUR_LOSS_L"
            
            def description(self):
                return "LOSS_L"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_CUR_CV_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "FMPM_TCA_DS1E1_CUR_CV_P"
            
            def description(self):
                return "CV_P"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_CUR_ES_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "FMPM_TCA_DS1E1_CUR_ES_P"
            
            def description(self):
                return "ES_P"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_CUR_SES_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "FMPM_TCA_DS1E1_CUR_SES_P"
            
            def description(self):
                return "SES_P"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_CUR_AISS_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "FMPM_TCA_DS1E1_CUR_AISS_P"
            
            def description(self):
                return "AISS_P"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_CUR_SAS_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "FMPM_TCA_DS1E1_CUR_SAS_P"
            
            def description(self):
                return "SAS_P"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_CUR_CSS_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "FMPM_TCA_DS1E1_CUR_CSS_P"
            
            def description(self):
                return "CSS_P"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_CUR_UAS_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "FMPM_TCA_DS1E1_CUR_UAS_P"
            
            def description(self):
                return "UAS_P"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_CUR_FC_P(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "FMPM_TCA_DS1E1_CUR_FC_P"
            
            def description(self):
                return "FC_P"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_CUR_ES_LFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_TCA_DS1E1_CUR_ES_LFE"
            
            def description(self):
                return "ES_LFE"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_CUR_SEFS_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_TCA_DS1E1_CUR_SEFS_PFE"
            
            def description(self):
                return "SEFS_PFE"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_CUR_ES_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_TCA_DS1E1_CUR_ES_PFE"
            
            def description(self):
                return "ES_PFE"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_CUR_SES_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_TCA_DS1E1_CUR_SES_PFE"
            
            def description(self):
                return "SES_PFE"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_CUR_CSS_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_TCA_DS1E1_CUR_CSS_PFE"
            
            def description(self):
                return "CSS_PFE"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_CUR_FC_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_TCA_DS1E1_CUR_FC_PFE"
            
            def description(self):
                return "FC_PFE"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_DS1E1_CUR_UAS_PFE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_DS1E1_CUR_UAS_PFE"
            
            def description(self):
                return "UAS_PFE"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_DS1E1_CUR_CV_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_CUR_CV_L()
            allFields["FMPM_TCA_DS1E1_CUR_ES_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_CUR_ES_L()
            allFields["FMPM_TCA_DS1E1_CUR_SES_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_CUR_SES_L()
            allFields["FMPM_TCA_DS1E1_CUR_LOSS_L"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_CUR_LOSS_L()
            allFields["FMPM_TCA_DS1E1_CUR_CV_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_CUR_CV_P()
            allFields["FMPM_TCA_DS1E1_CUR_ES_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_CUR_ES_P()
            allFields["FMPM_TCA_DS1E1_CUR_SES_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_CUR_SES_P()
            allFields["FMPM_TCA_DS1E1_CUR_AISS_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_CUR_AISS_P()
            allFields["FMPM_TCA_DS1E1_CUR_SAS_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_CUR_SAS_P()
            allFields["FMPM_TCA_DS1E1_CUR_CSS_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_CUR_CSS_P()
            allFields["FMPM_TCA_DS1E1_CUR_UAS_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_CUR_UAS_P()
            allFields["FMPM_TCA_DS1E1_CUR_FC_P"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_CUR_FC_P()
            allFields["FMPM_TCA_DS1E1_CUR_ES_LFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_CUR_ES_LFE()
            allFields["FMPM_TCA_DS1E1_CUR_SEFS_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_CUR_SEFS_PFE()
            allFields["FMPM_TCA_DS1E1_CUR_ES_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_CUR_ES_PFE()
            allFields["FMPM_TCA_DS1E1_CUR_SES_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_CUR_SES_PFE()
            allFields["FMPM_TCA_DS1E1_CUR_CSS_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_CUR_CSS_PFE()
            allFields["FMPM_TCA_DS1E1_CUR_FC_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_CUR_FC_PFE()
            allFields["FMPM_TCA_DS1E1_CUR_UAS_PFE"] = _AF6CNC0021_RD_PM._FMPM_TCA_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_TCA_DS1E1_CUR_UAS_PFE()
            return allFields

    class _FMPM_TCA_PW_Level1_Group_Interrupt_OR(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA PW Level1 Group Interrupt OR"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00058004
            
        def endAddress(self):
            return 0xffffffff

        class _FMPM_TCA_PW_LEV1_OR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_PW_LEV1_OR"
            
            def description(self):
                return "Interrupt PW level1 group OR"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_PW_LEV1_OR"] = _AF6CNC0021_RD_PM._FMPM_TCA_PW_Level1_Group_Interrupt_OR._FMPM_TCA_PW_LEV1_OR()
            return allFields

    class _FMPM_TCA_PW_Level2_Group_Interrupt_OR(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA PW Level2 Group Interrupt OR"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x580E0+$D"
            
        def startAddress(self):
            return 0x000580e0
            
        def endAddress(self):
            return 0x000580e7

        class _FMPM_TCA_PW_LEV2_OR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_PW_LEV2_OR"
            
            def description(self):
                return "PW Level2 Group Interrupt OR, bit per group, the last Level-1 group has 12 Level-2 group"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_PW_LEV2_OR"] = _AF6CNC0021_RD_PM._FMPM_TCA_PW_Level2_Group_Interrupt_OR._FMPM_TCA_PW_LEV2_OR()
            return allFields

    class _FMPM_TCA_PW_Framer_Interrupt_OR_AND_MASK_Per_PW(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA PW Framer Interrupt OR AND MASK Per PW"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x5B400+$D*0x20+$E"
            
        def startAddress(self):
            return 0x0005b400
            
        def endAddress(self):
            return 0x0005b4ff

        class _FMPM_TCA_PW_OAMSK(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_PW_OAMSK"
            
            def description(self):
                return "Interrupt OR AND MASK per PW, bit per PW"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_PW_OAMSK"] = _AF6CNC0021_RD_PM._FMPM_TCA_PW_Framer_Interrupt_OR_AND_MASK_Per_PW._FMPM_TCA_PW_OAMSK()
            return allFields

    class _FMPM_TCA_PW_Framer_Interrupt_MASK_Per_PW(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA PW Framer Interrupt MASK Per PW"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x5B500+$D*0x20+$E"
            
        def startAddress(self):
            return 0x0005b500
            
        def endAddress(self):
            return 0x0005b5ff

        class _FMPM_TCA_PW_MSK(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_PW_MSK"
            
            def description(self):
                return "Interrupt OR AND MASK per PW, bit per PW"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_PW_MSK"] = _AF6CNC0021_RD_PM._FMPM_TCA_PW_Framer_Interrupt_MASK_Per_PW._FMPM_TCA_PW_MSK()
            return allFields

    class _FMPM_TCA_PW_Interrupt_Sticky_Per_Type_Per_PW(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA PW Interrupt Sticky Per Type Per PW"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x5C000+$D*0x400+$E*0x20+$F"
            
        def startAddress(self):
            return 0x0005c000
            
        def endAddress(self):
            return 0x0005dfff

        class _FMPM_TCA_PW_STK_ES(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_TCA_PW_STK_ES"
            
            def description(self):
                return "ES"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_PW_STK_SES(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_TCA_PW_STK_SES"
            
            def description(self):
                return "SES"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_PW_STK_UAS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_TCA_PW_STK_UAS"
            
            def description(self):
                return "UAS"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_PW_STK_ES_FE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_TCA_PW_STK_ES_FE"
            
            def description(self):
                return "ES_FE"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_PW_STK_SES_FE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_TCA_PW_STK_SES_FE"
            
            def description(self):
                return "SES_FE"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_PW_STK_UAS_FE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_TCA_PW_STK_UAS_FE"
            
            def description(self):
                return "UAS_FE"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_PW_STK_FC(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_PW_STK_FC"
            
            def description(self):
                return "FC"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_PW_STK_ES"] = _AF6CNC0021_RD_PM._FMPM_TCA_PW_Interrupt_Sticky_Per_Type_Per_PW._FMPM_TCA_PW_STK_ES()
            allFields["FMPM_TCA_PW_STK_SES"] = _AF6CNC0021_RD_PM._FMPM_TCA_PW_Interrupt_Sticky_Per_Type_Per_PW._FMPM_TCA_PW_STK_SES()
            allFields["FMPM_TCA_PW_STK_UAS"] = _AF6CNC0021_RD_PM._FMPM_TCA_PW_Interrupt_Sticky_Per_Type_Per_PW._FMPM_TCA_PW_STK_UAS()
            allFields["FMPM_TCA_PW_STK_ES_FE"] = _AF6CNC0021_RD_PM._FMPM_TCA_PW_Interrupt_Sticky_Per_Type_Per_PW._FMPM_TCA_PW_STK_ES_FE()
            allFields["FMPM_TCA_PW_STK_SES_FE"] = _AF6CNC0021_RD_PM._FMPM_TCA_PW_Interrupt_Sticky_Per_Type_Per_PW._FMPM_TCA_PW_STK_SES_FE()
            allFields["FMPM_TCA_PW_STK_UAS_FE"] = _AF6CNC0021_RD_PM._FMPM_TCA_PW_Interrupt_Sticky_Per_Type_Per_PW._FMPM_TCA_PW_STK_UAS_FE()
            allFields["FMPM_TCA_PW_STK_FC"] = _AF6CNC0021_RD_PM._FMPM_TCA_PW_Interrupt_Sticky_Per_Type_Per_PW._FMPM_TCA_PW_STK_FC()
            return allFields

    class _FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA PW Interrupt MASK Per Type Per PW"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x5E000+$D*0x400+$E*0x20+$F"
            
        def startAddress(self):
            return 0x0005e000
            
        def endAddress(self):
            return 0x0005ffff

        class _FMPM_TCA_PW_MSK_ES(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_TCA_PW_MSK_ES"
            
            def description(self):
                return "ES"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_PW_MSK_SES(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_TCA_PW_MSK_SES"
            
            def description(self):
                return "SES"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_PW_MSK_UAS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_TCA_PW_MSK_UAS"
            
            def description(self):
                return "UAS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_PW_MSK_ES_FE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_TCA_PW_MSK_ES_FE"
            
            def description(self):
                return "ES_FE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_PW_MSK_SES_FE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_TCA_PW_MSK_SES_FE"
            
            def description(self):
                return "SES_FE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_PW_MSK_UAS_FE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_TCA_PW_MSK_UAS_FE"
            
            def description(self):
                return "UAS_FE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_PW_MSK_FC(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_PW_MSK_FC"
            
            def description(self):
                return "FC"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_PW_MSK_ES"] = _AF6CNC0021_RD_PM._FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW._FMPM_TCA_PW_MSK_ES()
            allFields["FMPM_TCA_PW_MSK_SES"] = _AF6CNC0021_RD_PM._FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW._FMPM_TCA_PW_MSK_SES()
            allFields["FMPM_TCA_PW_MSK_UAS"] = _AF6CNC0021_RD_PM._FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW._FMPM_TCA_PW_MSK_UAS()
            allFields["FMPM_TCA_PW_MSK_ES_FE"] = _AF6CNC0021_RD_PM._FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW._FMPM_TCA_PW_MSK_ES_FE()
            allFields["FMPM_TCA_PW_MSK_SES_FE"] = _AF6CNC0021_RD_PM._FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW._FMPM_TCA_PW_MSK_SES_FE()
            allFields["FMPM_TCA_PW_MSK_UAS_FE"] = _AF6CNC0021_RD_PM._FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW._FMPM_TCA_PW_MSK_UAS_FE()
            allFields["FMPM_TCA_PW_MSK_FC"] = _AF6CNC0021_RD_PM._FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW._FMPM_TCA_PW_MSK_FC()
            return allFields

    class _FMPM_TCA_PW_Interrupt_Current_Status_Per_Type_Per_PW(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA PW Interrupt Current Status Per Type Per PW"
    
        def description(self):
            return "FMPM TCA Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x60000+$D*0x400+$E*0x20+$F"
            
        def startAddress(self):
            return 0x00060000
            
        def endAddress(self):
            return 0x00061fff

        class _FMPM_TCA_PW_CUR_ES(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_TCA_PW_CUR_ES"
            
            def description(self):
                return "ES"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_PW_CUR_SES(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_TCA_PW_CUR_SES"
            
            def description(self):
                return "SES"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_PW_CUR_UAS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_TCA_PW_CUR_UAS"
            
            def description(self):
                return "UAS"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_PW_CUR_ES_FE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_TCA_PW_CUR_ES_FE"
            
            def description(self):
                return "ES_FE"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_PW_CUR_SES_FE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_TCA_PW_CUR_SES_FE"
            
            def description(self):
                return "SES_FE"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_PW_CUR_UAS_FE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_TCA_PW_CUR_UAS_FE"
            
            def description(self):
                return "UAS_FE"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_TCA_PW_CUR_FC(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_TCA_PW_CUR_FC"
            
            def description(self):
                return "FC"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_TCA_PW_CUR_ES"] = _AF6CNC0021_RD_PM._FMPM_TCA_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_TCA_PW_CUR_ES()
            allFields["FMPM_TCA_PW_CUR_SES"] = _AF6CNC0021_RD_PM._FMPM_TCA_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_TCA_PW_CUR_SES()
            allFields["FMPM_TCA_PW_CUR_UAS"] = _AF6CNC0021_RD_PM._FMPM_TCA_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_TCA_PW_CUR_UAS()
            allFields["FMPM_TCA_PW_CUR_ES_FE"] = _AF6CNC0021_RD_PM._FMPM_TCA_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_TCA_PW_CUR_ES_FE()
            allFields["FMPM_TCA_PW_CUR_SES_FE"] = _AF6CNC0021_RD_PM._FMPM_TCA_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_TCA_PW_CUR_SES_FE()
            allFields["FMPM_TCA_PW_CUR_UAS_FE"] = _AF6CNC0021_RD_PM._FMPM_TCA_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_TCA_PW_CUR_UAS_FE()
            allFields["FMPM_TCA_PW_CUR_FC"] = _AF6CNC0021_RD_PM._FMPM_TCA_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_TCA_PW_CUR_FC()
            return allFields

import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0021_RD_GLBPMC(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["hold_reg0"] = _AF6CNC0021_RD_GLBPMC._hold_reg0()
        allRegisters["upen_txpwcnt_rw"] = _AF6CNC0021_RD_GLBPMC._upen_txpwcnt_rw()
        allRegisters["upen_txpwcnt_rw"] = _AF6CNC0021_RD_GLBPMC._upen_txpwcnt_rw()
        allRegisters["upen_rxpwcnt_info0_rw"] = _AF6CNC0021_RD_GLBPMC._upen_rxpwcnt_info0_rw()
        allRegisters["upen_rxpwcnt_info0_rw"] = _AF6CNC0021_RD_GLBPMC._upen_rxpwcnt_info0_rw()
        allRegisters["upen_rxpwcnt_info1_rw"] = _AF6CNC0021_RD_GLBPMC._upen_rxpwcnt_info1_rw()
        allRegisters["upen_rxpwcnt_info1_rw"] = _AF6CNC0021_RD_GLBPMC._upen_rxpwcnt_info1_rw()
        allRegisters["upen_rxpwcnt_info2_rw"] = _AF6CNC0021_RD_GLBPMC._upen_rxpwcnt_info2_rw()
        allRegisters["upen_rxpwcnt_info2_rw"] = _AF6CNC0021_RD_GLBPMC._upen_rxpwcnt_info2_rw()
        allRegisters["upen_pwcntbyte_rw"] = _AF6CNC0021_RD_GLBPMC._upen_pwcntbyte_rw()
        allRegisters["upen_pwcntbyte_rw"] = _AF6CNC0021_RD_GLBPMC._upen_pwcntbyte_rw()
        allRegisters["upen_poh_pmr_cnt0_rw"] = _AF6CNC0021_RD_GLBPMC._upen_poh_pmr_cnt0_rw()
        allRegisters["upen_poh_pmr_cnt0_rw"] = _AF6CNC0021_RD_GLBPMC._upen_poh_pmr_cnt0_rw()
        allRegisters["upen_pohpmdat_sts_ber_erdi1_rw"] = _AF6CNC0021_RD_GLBPMC._upen_pohpmdat_sts_ber_erdi1_rw()
        allRegisters["upen_pohpmdat_sts_ber_erdi1_rw"] = _AF6CNC0021_RD_GLBPMC._upen_pohpmdat_sts_ber_erdi1_rw()
        allRegisters["upen_pohpmdat_vt_ber_erdi0_rw"] = _AF6CNC0021_RD_GLBPMC._upen_pohpmdat_vt_ber_erdi0_rw()
        allRegisters["upen_pohpmdat_vt_ber_erdi0_rw"] = _AF6CNC0021_RD_GLBPMC._upen_pohpmdat_vt_ber_erdi0_rw()
        allRegisters["upen_pohpmdat_vt_ber_erdi1_rw"] = _AF6CNC0021_RD_GLBPMC._upen_pohpmdat_vt_ber_erdi1_rw()
        allRegisters["upen_pohpmdat_vt_ber_erdi1_rw"] = _AF6CNC0021_RD_GLBPMC._upen_pohpmdat_vt_ber_erdi1_rw()
        allRegisters["upen_poh_vt_cnt0_rw"] = _AF6CNC0021_RD_GLBPMC._upen_poh_vt_cnt0_rw()
        allRegisters["upen_poh_vt_cnt0_rw"] = _AF6CNC0021_RD_GLBPMC._upen_poh_vt_cnt0_rw()
        allRegisters["upen_poh_vt_cnt1_rw"] = _AF6CNC0021_RD_GLBPMC._upen_poh_vt_cnt1_rw()
        allRegisters["upen_poh_vt_cnt1_rw"] = _AF6CNC0021_RD_GLBPMC._upen_poh_vt_cnt1_rw()
        allRegisters["upen_poh_sts_cnt_rw"] = _AF6CNC0021_RD_GLBPMC._upen_poh_sts_cnt_rw()
        allRegisters["upen_poh_sts_cnt_rw"] = _AF6CNC0021_RD_GLBPMC._upen_poh_sts_cnt_rw()
        allRegisters["upen_pdh_de1cntval30_rw"] = _AF6CNC0021_RD_GLBPMC._upen_pdh_de1cntval30_rw()
        allRegisters["upen_pdh_de1cntval30_rw"] = _AF6CNC0021_RD_GLBPMC._upen_pdh_de1cntval30_rw()
        allRegisters["upen_pdh_de1cntval74_rw"] = _AF6CNC0021_RD_GLBPMC._upen_pdh_de1cntval74_rw()
        allRegisters["upen_pdh_de1cntval74_rw"] = _AF6CNC0021_RD_GLBPMC._upen_pdh_de1cntval74_rw()
        allRegisters["upen_pdh_de3cnt0_rw"] = _AF6CNC0021_RD_GLBPMC._upen_pdh_de3cnt0_rw()
        allRegisters["upen_pdh_de3cnt0_rw"] = _AF6CNC0021_RD_GLBPMC._upen_pdh_de3cnt0_rw()
        allRegisters["upen_glbpmcint"] = _AF6CNC0021_RD_GLBPMC._upen_glbpmcint()
        return allRegisters

    class _hold_reg0(AtRegister.AtRegister):
        def name(self):
            return "CPU Reg Hold 0"
    
        def description(self):
            return "The register provides hold register from [63:32]"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0003b319
            
        def endAddress(self):
            return 0xffffffff

        class _hold0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "hold0"
            
            def description(self):
                return "Hold from  [63:32]bit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["hold0"] = _AF6CNC0021_RD_GLBPMC._hold_reg0._hold0()
            return allFields

    class _upen_txpwcnt_rw(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0_4000 + 5376*$offset + $pwid"
            
        def startAddress(self):
            return 0x00004000
            
        def endAddress(self):
            return 0x00007eff

        class _txpwcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txpwcnt"
            
            def description(self):
                return "in case of txpwcnt0 side: + offset = 0 : txrbit + offset = 1 : txnbit + offset = 2 : Unused in case of txpwcnt1 side: + offset = 0 : txpbit + offset = 1 : txlbit + offset = 2 : txpkt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txpwcnt"] = _AF6CNC0021_RD_GLBPMC._upen_txpwcnt_rw._txpwcnt()
            return allFields

    class _upen_txpwcnt_rw(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_4000 + 5376*$offset + $pwid"
            
        def startAddress(self):
            return 0x00084000
            
        def endAddress(self):
            return 0x00087eff

        class _txpwcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txpwcnt"
            
            def description(self):
                return "in case of txpwcnt0 side: + offset = 0 : txrbit + offset = 1 : txnbit + offset = 2 : Unused in case of txpwcnt1 side: + offset = 0 : txpbit + offset = 1 : txlbit + offset = 2 : txpkt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txpwcnt"] = _AF6CNC0021_RD_GLBPMC._upen_txpwcnt_rw._txpwcnt()
            return allFields

    class _upen_rxpwcnt_info0_rw(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receiver Counter Info0"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0_C000 + 5376*$offset + $pwid"
            
        def startAddress(self):
            return 0x0000c000
            
        def endAddress(self):
            return 0x0000feff

        class _rxpwcnt_info0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpwcnt_info0"
            
            def description(self):
                return "in case of rxpwcnt_info0_cnt0 side: + offset = 0 : rxlate + offset = 1 : rxpbit + offset = 2 : rxlbit in case of rxpwcnt_info0_cnt1 side: + offset = 0 : rxrbit + offset = 1 : rxnbit + offset = 2 : rxpkt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpwcnt_info0"] = _AF6CNC0021_RD_GLBPMC._upen_rxpwcnt_info0_rw._rxpwcnt_info0()
            return allFields

    class _upen_rxpwcnt_info0_rw(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receiver Counter Info0"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_C000 + 5376*$offset + $pwid"
            
        def startAddress(self):
            return 0x0008c000
            
        def endAddress(self):
            return 0x0008feff

        class _rxpwcnt_info0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpwcnt_info0"
            
            def description(self):
                return "in case of rxpwcnt_info0_cnt0 side: + offset = 0 : rxlate + offset = 1 : rxpbit + offset = 2 : rxlbit in case of rxpwcnt_info0_cnt1 side: + offset = 0 : rxrbit + offset = 1 : rxnbit + offset = 2 : rxpkt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpwcnt_info0"] = _AF6CNC0021_RD_GLBPMC._upen_rxpwcnt_info0_rw._rxpwcnt_info0()
            return allFields

    class _upen_rxpwcnt_info1_rw(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receiver Counter Info1"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1_4000 + 5376*$offset + $pwid"
            
        def startAddress(self):
            return 0x00014000
            
        def endAddress(self):
            return 0x00017eff

        class _rxpwcnt_info1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpwcnt_info1"
            
            def description(self):
                return "in case of rxpwcnt_info1_cnt0 side: + offset = 0 : rxearly + offset = 1 : rxlops + offset = 2 : Unused in case of rxpwcnt_info1_cnt1 side: + offset = 0 : rxlost + offset = 1 : Unused + offset = 2 : Unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpwcnt_info1"] = _AF6CNC0021_RD_GLBPMC._upen_rxpwcnt_info1_rw._rxpwcnt_info1()
            return allFields

    class _upen_rxpwcnt_info1_rw(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receiver Counter Info1"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_4000 + 5376*$offset + $pwid"
            
        def startAddress(self):
            return 0x00094000
            
        def endAddress(self):
            return 0x00097eff

        class _rxpwcnt_info1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpwcnt_info1"
            
            def description(self):
                return "in case of rxpwcnt_info1_cnt0 side: + offset = 0 : rxearly + offset = 1 : rxlops + offset = 2 : Unused in case of rxpwcnt_info1_cnt1 side: + offset = 0 : rxlost + offset = 1 : Unused + offset = 2 : Unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpwcnt_info1"] = _AF6CNC0021_RD_GLBPMC._upen_rxpwcnt_info1_rw._rxpwcnt_info1()
            return allFields

    class _upen_rxpwcnt_info2_rw(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receiver Counter Info2"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1_C000 + 5376*$offset + $pwid"
            
        def startAddress(self):
            return 0x0001c000
            
        def endAddress(self):
            return 0x0001feff

        class _rxpwcnt_info2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpwcnt_info2"
            
            def description(self):
                return "in case of rxpwcnt_info2_cnt0 side: + offset = 0 : rxoverrun + offset = 1 : rxunderrun + offset = 2 : rxmalform in case of rxpwcnt_info2_cnt1 side: + offset = 0 : Unused + offset = 1 : Unused + offset = 2 : rxstray"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpwcnt_info2"] = _AF6CNC0021_RD_GLBPMC._upen_rxpwcnt_info2_rw._rxpwcnt_info2()
            return allFields

    class _upen_rxpwcnt_info2_rw(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receiver Counter Info2"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_C000 + 5376*$offset + $pwid"
            
        def startAddress(self):
            return 0x0009c000
            
        def endAddress(self):
            return 0x0009feff

        class _rxpwcnt_info2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpwcnt_info2"
            
            def description(self):
                return "in case of rxpwcnt_info2_cnt0 side: + offset = 0 : rxoverrun + offset = 1 : rxunderrun + offset = 2 : rxmalform in case of rxpwcnt_info2_cnt1 side: + offset = 0 : Unused + offset = 1 : Unused + offset = 2 : rxstray"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpwcnt_info2"] = _AF6CNC0021_RD_GLBPMC._upen_rxpwcnt_info2_rw._rxpwcnt_info2()
            return allFields

    class _upen_pwcntbyte_rw(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Byte Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x6_A000 + $pwid"
            
        def startAddress(self):
            return 0x0006a000
            
        def endAddress(self):
            return 0x0006b500

        class _pwcntbyte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pwcntbyte"
            
            def description(self):
                return "txpwcntbyte (rxpwcntbyte)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pwcntbyte"] = _AF6CNC0021_RD_GLBPMC._upen_pwcntbyte_rw._pwcntbyte()
            return allFields

    class _upen_pwcntbyte_rw(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Byte Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x7_2000 + $pwid"
            
        def startAddress(self):
            return 0x00072000
            
        def endAddress(self):
            return 0x00073500

        class _pwcntbyte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pwcntbyte"
            
            def description(self):
                return "txpwcntbyte (rxpwcntbyte)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pwcntbyte"] = _AF6CNC0021_RD_GLBPMC._upen_pwcntbyte_rw._pwcntbyte()
            return allFields

    class _upen_poh_pmr_cnt0_rw(AtRegister.AtRegister):
        def name(self):
            return "PMR Error Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_B040 + 16*$offset + $lineid"
            
        def startAddress(self):
            return 0x0003b040
            
        def endAddress(self):
            return 0x0003b06f

        class _pmr_err_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pmr_err_cnt"
            
            def description(self):
                return "in case of pmr_err_cnt0 side: + offset = 0 : rei_l + offset = 1 : b2 + offset = 2 : b1 in case of pmr_err_cnt1 side: + offset = 0 : rei_l_block_err + offset = 1 : b2_block_err + offset = 2 : b1_block_err"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pmr_err_cnt"] = _AF6CNC0021_RD_GLBPMC._upen_poh_pmr_cnt0_rw._pmr_err_cnt()
            return allFields

    class _upen_poh_pmr_cnt0_rw(AtRegister.AtRegister):
        def name(self):
            return "PMR Error Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB_B040 + 16*$offset + $lineid"
            
        def startAddress(self):
            return 0x000bb040
            
        def endAddress(self):
            return 0x000bb06f

        class _pmr_err_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pmr_err_cnt"
            
            def description(self):
                return "in case of pmr_err_cnt0 side: + offset = 0 : rei_l + offset = 1 : b2 + offset = 2 : b1 in case of pmr_err_cnt1 side: + offset = 0 : rei_l_block_err + offset = 1 : b2_block_err + offset = 2 : b1_block_err"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pmr_err_cnt"] = _AF6CNC0021_RD_GLBPMC._upen_poh_pmr_cnt0_rw._pmr_err_cnt()
            return allFields

    class _upen_pohpmdat_sts_ber_erdi1_rw(AtRegister.AtRegister):
        def name(self):
            return "POH path sts ERDI Counter1"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_D800 + 512*$offset +  64*$slcid + $stsid"
            
        def startAddress(self):
            return 0x0003d800
            
        def endAddress(self):
            return 0x0003ddff

        class _pohpath_ber_erdi(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pohpath_ber_erdi"
            
            def description(self):
                return "in case of pohpath_ber_erdi_cnt2 side: + offset = 0 : sts_rei + offset = 1 : Unused + offset = 2 : Unused in case of pohpath_ber_erdi_cnt3 side: + offset = 0 : sts_bip(B3) + offset = 1 : Unused + offset = 2 : Unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pohpath_ber_erdi"] = _AF6CNC0021_RD_GLBPMC._upen_pohpmdat_sts_ber_erdi1_rw._pohpath_ber_erdi()
            return allFields

    class _upen_pohpmdat_sts_ber_erdi1_rw(AtRegister.AtRegister):
        def name(self):
            return "POH path sts ERDI Counter1"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB_D800 + 512*$offset +  64*$slcid + $stsid"
            
        def startAddress(self):
            return 0x000bd800
            
        def endAddress(self):
            return 0x000bddff

        class _pohpath_ber_erdi(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pohpath_ber_erdi"
            
            def description(self):
                return "in case of pohpath_ber_erdi_cnt2 side: + offset = 0 : sts_rei + offset = 1 : Unused + offset = 2 : Unused in case of pohpath_ber_erdi_cnt3 side: + offset = 0 : sts_bip(B3) + offset = 1 : Unused + offset = 2 : Unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pohpath_ber_erdi"] = _AF6CNC0021_RD_GLBPMC._upen_pohpmdat_sts_ber_erdi1_rw._pohpath_ber_erdi()
            return allFields

    class _upen_pohpmdat_vt_ber_erdi0_rw(AtRegister.AtRegister):
        def name(self):
            return "POH path vt ERDI Counter0"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x6_4000 + 5376*$offset + 672*$slcid +  28*stsid + 4*vtgid + vtid"
            
        def startAddress(self):
            return 0x00064000
            
        def endAddress(self):
            return 0x00067fff

        class _pohpath_ber_erdi(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pohpath_ber_erdi"
            
            def description(self):
                return "in case of pohpath_ber_erdi_cnt2 side: + offset = 0 : vt_rei + offset = 1 : Unused + offset = 2 : Unused in case of pohpath_ber_erdi_cnt3 side: + offset = 0 : vt_bip + offset = 1 : Unused + offset = 2 : Unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pohpath_ber_erdi"] = _AF6CNC0021_RD_GLBPMC._upen_pohpmdat_vt_ber_erdi0_rw._pohpath_ber_erdi()
            return allFields

    class _upen_pohpmdat_vt_ber_erdi0_rw(AtRegister.AtRegister):
        def name(self):
            return "POH path vt ERDI Counter0"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xE_4000 + 5376*$offset + 672*$slcid +  28*stsid + 4*vtgid + vtid"
            
        def startAddress(self):
            return 0x000e4000
            
        def endAddress(self):
            return 0x000e7fff

        class _pohpath_ber_erdi(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pohpath_ber_erdi"
            
            def description(self):
                return "in case of pohpath_ber_erdi_cnt2 side: + offset = 0 : vt_rei + offset = 1 : Unused + offset = 2 : Unused in case of pohpath_ber_erdi_cnt3 side: + offset = 0 : vt_bip + offset = 1 : Unused + offset = 2 : Unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pohpath_ber_erdi"] = _AF6CNC0021_RD_GLBPMC._upen_pohpmdat_vt_ber_erdi0_rw._pohpath_ber_erdi()
            return allFields

    class _upen_pohpmdat_vt_ber_erdi1_rw(AtRegister.AtRegister):
        def name(self):
            return "POH path vt ERDI Counter1"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x4_4000 + 5376*$offset + 672*$slcid +  28*stsid + 4*vtgid + vtid"
            
        def startAddress(self):
            return 0x00044000
            
        def endAddress(self):
            return 0x00047fff

        class _pohpath_ber_erdi(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pohpath_ber_erdi"
            
            def description(self):
                return "in case of pohpath_ber_erdi_cnt2 side: + offset = 0 : vt_rei + offset = 1 : Unused + offset = 2 : Unused in case of pohpath_ber_erdi_cnt3 side: + offset = 0 : vt_bip + offset = 1 : Unused + offset = 2 : Unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pohpath_ber_erdi"] = _AF6CNC0021_RD_GLBPMC._upen_pohpmdat_vt_ber_erdi1_rw._pohpath_ber_erdi()
            return allFields

    class _upen_pohpmdat_vt_ber_erdi1_rw(AtRegister.AtRegister):
        def name(self):
            return "POH path vt ERDI Counter1"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xC_4000 + 5376*$offset + 672*$slcid +  28*stsid + 4*vtgid + vtid"
            
        def startAddress(self):
            return 0x000c4000
            
        def endAddress(self):
            return 0x000c7fff

        class _pohpath_ber_erdi(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pohpath_ber_erdi"
            
            def description(self):
                return "in case of pohpath_ber_erdi_cnt2 side: + offset = 0 : vt_rei + offset = 1 : Unused + offset = 2 : Unused in case of pohpath_ber_erdi_cnt3 side: + offset = 0 : vt_bip + offset = 1 : Unused + offset = 2 : Unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pohpath_ber_erdi"] = _AF6CNC0021_RD_GLBPMC._upen_pohpmdat_vt_ber_erdi1_rw._pohpath_ber_erdi()
            return allFields

    class _upen_poh_vt_cnt0_rw(AtRegister.AtRegister):
        def name(self):
            return "POH vt pointer Counter 0"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x5_4000 + 5376*$offset + 672*$slcid +  28*stsid + 4*vtgid + vtid"
            
        def startAddress(self):
            return 0x00054000
            
        def endAddress(self):
            return 0x00057eff

        class _poh_vt_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "poh_vt_cnt"
            
            def description(self):
                return "in case of poh_vt_cnt0 side: + offset = 0 : pohtx_vt_dec + offset = 1 : pohrx_vt_dec + offset = 2 : Unused in case of poh_vt_cnt1 side: + offset = 0 : pohtx_vt_inc + offset = 1 : pohrx_vt_inc + offset = 2 : Unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["poh_vt_cnt"] = _AF6CNC0021_RD_GLBPMC._upen_poh_vt_cnt0_rw._poh_vt_cnt()
            return allFields

    class _upen_poh_vt_cnt0_rw(AtRegister.AtRegister):
        def name(self):
            return "POH vt pointer Counter 0"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xD_4000 + 5376*$offset + 672*$slcid +  28*stsid + 4*vtgid + vtid"
            
        def startAddress(self):
            return 0x000d4000
            
        def endAddress(self):
            return 0x000d7eff

        class _poh_vt_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "poh_vt_cnt"
            
            def description(self):
                return "in case of poh_vt_cnt0 side: + offset = 0 : pohtx_vt_dec + offset = 1 : pohrx_vt_dec + offset = 2 : Unused in case of poh_vt_cnt1 side: + offset = 0 : pohtx_vt_inc + offset = 1 : pohrx_vt_inc + offset = 2 : Unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["poh_vt_cnt"] = _AF6CNC0021_RD_GLBPMC._upen_poh_vt_cnt0_rw._poh_vt_cnt()
            return allFields

    class _upen_poh_vt_cnt1_rw(AtRegister.AtRegister):
        def name(self):
            return "POH vt pointer Counter 1"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x4_C000 + 5376*$offset + 672*$slcid +  28*stsid + 4*vtgid + vtid"
            
        def startAddress(self):
            return 0x0004c000
            
        def endAddress(self):
            return 0x0004feff

        class _poh_vt_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "poh_vt_cnt"
            
            def description(self):
                return "in case of poh_vt_cnt0 side: + offset = 0 : pohtx_vt_dec + offset = 1 : pohrx_vt_dec + offset = 2 : Unused in case of poh_vt_cnt1 side: + offset = 0 : pohtx_vt_inc + offset = 1 : pohrx_vt_inc + offset = 2 : Unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["poh_vt_cnt"] = _AF6CNC0021_RD_GLBPMC._upen_poh_vt_cnt1_rw._poh_vt_cnt()
            return allFields

    class _upen_poh_vt_cnt1_rw(AtRegister.AtRegister):
        def name(self):
            return "POH vt pointer Counter 1"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xC_C000 + 5376*$offset + 672*$slcid +  28*stsid + 4*vtgid + vtid"
            
        def startAddress(self):
            return 0x000cc000
            
        def endAddress(self):
            return 0x000cfeff

        class _poh_vt_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "poh_vt_cnt"
            
            def description(self):
                return "in case of poh_vt_cnt0 side: + offset = 0 : pohtx_vt_dec + offset = 1 : pohrx_vt_dec + offset = 2 : Unused in case of poh_vt_cnt1 side: + offset = 0 : pohtx_vt_inc + offset = 1 : pohrx_vt_inc + offset = 2 : Unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["poh_vt_cnt"] = _AF6CNC0021_RD_GLBPMC._upen_poh_vt_cnt1_rw._poh_vt_cnt()
            return allFields

    class _upen_poh_sts_cnt_rw(AtRegister.AtRegister):
        def name(self):
            return "POH sts pointer Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_F800 + 512*$offset +  64*$slcid + $stsid"
            
        def startAddress(self):
            return 0x0003f800
            
        def endAddress(self):
            return 0x0003fdff

        class _poh_sts_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "poh_sts_cnt"
            
            def description(self):
                return "in case of poh_sts_cnt0 side: + offset = 0 :  pohtx_sts_dec + offset = 1 :  pohrx_sts_dec + offset = 2 :  Unused in case of poh_sts_cnt1 side: + offset = 0 :  pohtx_sts_inc + offset = 1 :  pohrx_sts_inc + offset = 2 :  Unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["poh_sts_cnt"] = _AF6CNC0021_RD_GLBPMC._upen_poh_sts_cnt_rw._poh_sts_cnt()
            return allFields

    class _upen_poh_sts_cnt_rw(AtRegister.AtRegister):
        def name(self):
            return "POH sts pointer Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB_F800 + 512*$offset +  64*$slcid + $stsid"
            
        def startAddress(self):
            return 0x000bf800
            
        def endAddress(self):
            return 0x000bfdff

        class _poh_sts_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "poh_sts_cnt"
            
            def description(self):
                return "in case of poh_sts_cnt0 side: + offset = 0 :  pohtx_sts_dec + offset = 1 :  pohrx_sts_dec + offset = 2 :  Unused in case of poh_sts_cnt1 side: + offset = 0 :  pohtx_sts_inc + offset = 1 :  pohrx_sts_inc + offset = 2 :  Unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["poh_sts_cnt"] = _AF6CNC0021_RD_GLBPMC._upen_poh_sts_cnt_rw._poh_sts_cnt()
            return allFields

    class _upen_pdh_de1cntval30_rw(AtRegister.AtRegister):
        def name(self):
            return "PDH ds1 cntval Counter Bus1"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_0800 + 4096*$slcid + 672*$offset + 28*stsid + 4*vtgid + vtid"
            
        def startAddress(self):
            return 0x00030800
            
        def endAddress(self):
            return 0x00030fdf

        class _ds1_cntval_bus1_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ds1_cntval_bus1_cnt"
            
            def description(self):
                return "in case of ds1_cntval_bus1_cnt0 side: + offset = 0 : fe + offset = 1 : rei + offset = 2 : unused in case of ds1_cntval_bus1_cnt1 side: + offset = 0 : crc + offset = 1 : lcv + offset = 2 : unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ds1_cntval_bus1_cnt"] = _AF6CNC0021_RD_GLBPMC._upen_pdh_de1cntval30_rw._ds1_cntval_bus1_cnt()
            return allFields

    class _upen_pdh_de1cntval30_rw(AtRegister.AtRegister):
        def name(self):
            return "PDH ds1 cntval Counter Bus1"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_1800 + 4096*$slcid + 672*$offset + 28*stsid + 4*vtgid + vtid"
            
        def startAddress(self):
            return 0x00031800
            
        def endAddress(self):
            return 0x00031fdf

        class _ds1_cntval_bus1_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ds1_cntval_bus1_cnt"
            
            def description(self):
                return "in case of ds1_cntval_bus1_cnt0 side: + offset = 0 : fe + offset = 1 : rei + offset = 2 : unused in case of ds1_cntval_bus1_cnt1 side: + offset = 0 : crc + offset = 1 : lcv + offset = 2 : unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ds1_cntval_bus1_cnt"] = _AF6CNC0021_RD_GLBPMC._upen_pdh_de1cntval30_rw._ds1_cntval_bus1_cnt()
            return allFields

    class _upen_pdh_de1cntval74_rw(AtRegister.AtRegister):
        def name(self):
            return "PDH ds1 cntval Counter Bus2"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_4800 + 4096*$slcid + 672*$offset + 28*stsid + 4*vtgid + vtid"
            
        def startAddress(self):
            return 0x00034800
            
        def endAddress(self):
            return 0x00034fdf

        class _ds1_cntval_bus2_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ds1_cntval_bus2_cnt"
            
            def description(self):
                return "in case of ds1_cntval_bus2_cnt0 side: + offset = 0 : fe + offset = 1 : rei + offset = 2 : unused in case of ds1_cntval_bus2_cnt1 side: + offset = 0 : crc + offset = 1 : lcv + offset = 2 : unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ds1_cntval_bus2_cnt"] = _AF6CNC0021_RD_GLBPMC._upen_pdh_de1cntval74_rw._ds1_cntval_bus2_cnt()
            return allFields

    class _upen_pdh_de1cntval74_rw(AtRegister.AtRegister):
        def name(self):
            return "PDH ds1 cntval Counter Bus2"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_5800 + 4096*$slcid + 672*$offset + 28*stsid + 4*vtgid + vtid"
            
        def startAddress(self):
            return 0x00035800
            
        def endAddress(self):
            return 0x00035fdf

        class _ds1_cntval_bus2_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ds1_cntval_bus2_cnt"
            
            def description(self):
                return "in case of ds1_cntval_bus2_cnt0 side: + offset = 0 : fe + offset = 1 : rei + offset = 2 : unused in case of ds1_cntval_bus2_cnt1 side: + offset = 0 : crc + offset = 1 : lcv + offset = 2 : unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ds1_cntval_bus2_cnt"] = _AF6CNC0021_RD_GLBPMC._upen_pdh_de1cntval74_rw._ds1_cntval_bus2_cnt()
            return allFields

    class _upen_pdh_de3cnt0_rw(AtRegister.AtRegister):
        def name(self):
            return "PDH ds3 cntval Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_8800 + 512*$offset +  64*$slcid + $stsid"
            
        def startAddress(self):
            return 0x00038800
            
        def endAddress(self):
            return 0x00038dff

        class _ds3_cntval_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ds3_cntval_cnt"
            
            def description(self):
                return "in case of ds3_cntval_cnt0 side: + offset = 0 : fe + offset = 1 : pb + offset = 2 : lcv in case of ds3_cntval_cnt1 side: + offset = 0 : rei + offset = 1 : cb + offset = 2 : unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ds3_cntval_cnt"] = _AF6CNC0021_RD_GLBPMC._upen_pdh_de3cnt0_rw._ds3_cntval_cnt()
            return allFields

    class _upen_pdh_de3cnt0_rw(AtRegister.AtRegister):
        def name(self):
            return "PDH ds3 cntval Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB_8800 + 512*$offset +  64*$slcid + $stsid"
            
        def startAddress(self):
            return 0x000b8800
            
        def endAddress(self):
            return 0x000b8dff

        class _ds3_cntval_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ds3_cntval_cnt"
            
            def description(self):
                return "in case of ds3_cntval_cnt0 side: + offset = 0 : fe + offset = 1 : pb + offset = 2 : lcv in case of ds3_cntval_cnt1 side: + offset = 0 : rei + offset = 1 : cb + offset = 2 : unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ds3_cntval_cnt"] = _AF6CNC0021_RD_GLBPMC._upen_pdh_de3cnt0_rw._ds3_cntval_cnt()
            return allFields

    class _upen_glbpmcint(AtRegister.AtRegister):
        def name(self):
            return "Global PMC 2page Interrupt Sticky"
    
        def description(self):
            return "The register provides hold register from [63:32]"
            
        def width(self):
            return 7
        
        def type(self):
            return "R1C"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0003bffb
            
        def endAddress(self):
            return 0xffffffff

        class _PmrIntSticky(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PmrIntSticky"
            
            def description(self):
                return "Pmr     Interrupt Sticky  1: Set  0: Clear"
            
            def type(self):
                return "R1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PohpmvtIntSticky(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PohpmvtIntSticky"
            
            def description(self):
                return "Pohpmvt Interrupt Sticky  1: Set  0: Clear"
            
            def type(self):
                return "R1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PohpmstsIntSticky(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PohpmstsIntSticky"
            
            def description(self):
                return "Pohpmsts Interrupt Sticky  1: Set  0: Clear"
            
            def type(self):
                return "R1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PohIntSticky(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "PohIntSticky"
            
            def description(self):
                return "Poh    Interrupt Sticky  1: Set  0: Clear"
            
            def type(self):
                return "R1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Pdhde1IntSticky(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Pdhde1IntSticky"
            
            def description(self):
                return "Pdhde1 Interrupt Sticky  1: Set  0: Clear"
            
            def type(self):
                return "R1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Pdhde3IntSticky(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Pdhde3IntSticky"
            
            def description(self):
                return "Pdhde3 Interrupt Sticky  1: Set  0: Clear"
            
            def type(self):
                return "R1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PwcntIntSticky(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PwcntIntSticky"
            
            def description(self):
                return "Pwcnt  Interrupt Sticky  1: Set  0: Clear"
            
            def type(self):
                return "R1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PmrIntSticky"] = _AF6CNC0021_RD_GLBPMC._upen_glbpmcint._PmrIntSticky()
            allFields["PohpmvtIntSticky"] = _AF6CNC0021_RD_GLBPMC._upen_glbpmcint._PohpmvtIntSticky()
            allFields["PohpmstsIntSticky"] = _AF6CNC0021_RD_GLBPMC._upen_glbpmcint._PohpmstsIntSticky()
            allFields["PohIntSticky"] = _AF6CNC0021_RD_GLBPMC._upen_glbpmcint._PohIntSticky()
            allFields["Pdhde1IntSticky"] = _AF6CNC0021_RD_GLBPMC._upen_glbpmcint._Pdhde1IntSticky()
            allFields["Pdhde3IntSticky"] = _AF6CNC0021_RD_GLBPMC._upen_glbpmcint._Pdhde3IntSticky()
            allFields["PwcntIntSticky"] = _AF6CNC0021_RD_GLBPMC._upen_glbpmcint._PwcntIntSticky()
            return allFields

import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0011_RD_INTALM(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["Counter_Per_Alarm_Interrupt_Enable_Control"] = _AF6CCI0011_RD_INTALM._Counter_Per_Alarm_Interrupt_Enable_Control()
        allRegisters["Counter_Per_Alarm_Interrupt_Status"] = _AF6CCI0011_RD_INTALM._Counter_Per_Alarm_Interrupt_Status()
        allRegisters["Counter_Per_Alarm_Current_Status"] = _AF6CCI0011_RD_INTALM._Counter_Per_Alarm_Current_Status()
        allRegisters["Counter_Interrupt_OR_Status"] = _AF6CCI0011_RD_INTALM._Counter_Interrupt_OR_Status()
        allRegisters["counter_per_group_intr_or_stat"] = _AF6CCI0011_RD_INTALM._counter_per_group_intr_or_stat()
        allRegisters["counter_per_group_intr_en_ctrl"] = _AF6CCI0011_RD_INTALM._counter_per_group_intr_en_ctrl()
        allRegisters["counter_per_slice_intr_or_stat"] = _AF6CCI0011_RD_INTALM._counter_per_slice_intr_or_stat()
        return allRegisters

    class _Counter_Per_Alarm_Interrupt_Enable_Control(AtRegister.AtRegister):
        def name(self):
            return "Counter Per Alarm Interrupt Enable Control"
    
        def description(self):
            return "This is per Alarm interrupt enable of pseudowires. Each register is used to store 4 bits to enable interrupts when the related alarms in pseudowires happen."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x060000 + Grp2048ID*8192 + GrpID*32 + BitID"
            
        def startAddress(self):
            return 0x00060000
            
        def endAddress(self):
            return 0xffffffff

        class _StrayStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "StrayStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable Stray packet event to generate an interrupt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MalformStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "MalformStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable Malform packet event to generate an interrupt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MbitStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "MbitStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable Mbit packet event to generate an interrupt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RbitStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "RbitStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable Rbit packet event to generate an interrupt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LofsSyncStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "LofsSyncStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable Lost of frame Sync event to generate an interrupt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _UnderrunStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "UnderrunStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable change jitter buffer state event from normal to underrun and vice versa in the related pseudowire to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OverrunStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "OverrunStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable change jitter buffer state event from normal to overrun and vice versa in the related pseudowire to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LofsStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "LofsStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable change lost of frame state(LOFS) event from normal to LOFS and vice versa in the related pseudowire to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LbitStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LbitStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable Lbit packet event to generate an interrupt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["StrayStateChgIntrEn"] = _AF6CCI0011_RD_INTALM._Counter_Per_Alarm_Interrupt_Enable_Control._StrayStateChgIntrEn()
            allFields["MalformStateChgIntrEn"] = _AF6CCI0011_RD_INTALM._Counter_Per_Alarm_Interrupt_Enable_Control._MalformStateChgIntrEn()
            allFields["MbitStateChgIntrEn"] = _AF6CCI0011_RD_INTALM._Counter_Per_Alarm_Interrupt_Enable_Control._MbitStateChgIntrEn()
            allFields["RbitStateChgIntrEn"] = _AF6CCI0011_RD_INTALM._Counter_Per_Alarm_Interrupt_Enable_Control._RbitStateChgIntrEn()
            allFields["LofsSyncStateChgIntrEn"] = _AF6CCI0011_RD_INTALM._Counter_Per_Alarm_Interrupt_Enable_Control._LofsSyncStateChgIntrEn()
            allFields["UnderrunStateChgIntrEn"] = _AF6CCI0011_RD_INTALM._Counter_Per_Alarm_Interrupt_Enable_Control._UnderrunStateChgIntrEn()
            allFields["OverrunStateChgIntrEn"] = _AF6CCI0011_RD_INTALM._Counter_Per_Alarm_Interrupt_Enable_Control._OverrunStateChgIntrEn()
            allFields["LofsStateChgIntrEn"] = _AF6CCI0011_RD_INTALM._Counter_Per_Alarm_Interrupt_Enable_Control._LofsStateChgIntrEn()
            allFields["LbitStateChgIntrEn"] = _AF6CCI0011_RD_INTALM._Counter_Per_Alarm_Interrupt_Enable_Control._LbitStateChgIntrEn()
            return allFields

    class _Counter_Per_Alarm_Interrupt_Status(AtRegister.AtRegister):
        def name(self):
            return "Counter Per Alarm Interrupt Status"
    
        def description(self):
            return "This is per Alarm interrupt enable of pseudowires. Each register is used to store 4 bits to enable interrupts when the related alarms in pseudowires happen."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x060800 + Grp2048ID*8192 +  GrpID*32 + BitID"
            
        def startAddress(self):
            return 0x00060800
            
        def endAddress(self):
            return 0xffffffff

        class _StrayStateChgIntrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "StrayStateChgIntrSta"
            
            def description(self):
                return "Set 1 when Stray packet event is detected in the pseudowire"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MalformStateChgIntrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "MalformStateChgIntrSta"
            
            def description(self):
                return "Set 1 when Malform packet event is detected in the pseudowire"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MbitStateChgIntrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "MbitStateChgIntrSta"
            
            def description(self):
                return "Set 1 when a Mbit packet event is detected in the pseudowire"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RbitStateChgIntrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "RbitStateChgIntrSta"
            
            def description(self):
                return "Set 1 when a Rbit packet event is detected in the pseudowire"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LofsSyncStateChgIntrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "LofsSyncStateChgIntrSta"
            
            def description(self):
                return "Set 1 when a lost of frame Sync event is detected in the pseudowire"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _UnderrunStateChgIntrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "UnderrunStateChgIntrSta"
            
            def description(self):
                return "Set 1 when there is a change in jitter buffer underrun state  in the related pseudowire"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OverrunStateChgIntrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "OverrunStateChgIntrSta"
            
            def description(self):
                return "Set 1 when there is a change in jitter buffer overrun state in the related pseudowire"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LofsStateChgIntrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "LofsStateChgIntrSta"
            
            def description(self):
                return "Set 1 when there is a change in lost of frame state(LOFS) in the related pseudowire"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LbitStateChgIntrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LbitStateChgIntrSta"
            
            def description(self):
                return "Set 1 when a Lbit packet event is detected in the pseudowire"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["StrayStateChgIntrSta"] = _AF6CCI0011_RD_INTALM._Counter_Per_Alarm_Interrupt_Status._StrayStateChgIntrSta()
            allFields["MalformStateChgIntrSta"] = _AF6CCI0011_RD_INTALM._Counter_Per_Alarm_Interrupt_Status._MalformStateChgIntrSta()
            allFields["MbitStateChgIntrSta"] = _AF6CCI0011_RD_INTALM._Counter_Per_Alarm_Interrupt_Status._MbitStateChgIntrSta()
            allFields["RbitStateChgIntrSta"] = _AF6CCI0011_RD_INTALM._Counter_Per_Alarm_Interrupt_Status._RbitStateChgIntrSta()
            allFields["LofsSyncStateChgIntrSta"] = _AF6CCI0011_RD_INTALM._Counter_Per_Alarm_Interrupt_Status._LofsSyncStateChgIntrSta()
            allFields["UnderrunStateChgIntrSta"] = _AF6CCI0011_RD_INTALM._Counter_Per_Alarm_Interrupt_Status._UnderrunStateChgIntrSta()
            allFields["OverrunStateChgIntrSta"] = _AF6CCI0011_RD_INTALM._Counter_Per_Alarm_Interrupt_Status._OverrunStateChgIntrSta()
            allFields["LofsStateChgIntrSta"] = _AF6CCI0011_RD_INTALM._Counter_Per_Alarm_Interrupt_Status._LofsStateChgIntrSta()
            allFields["LbitStateChgIntrSta"] = _AF6CCI0011_RD_INTALM._Counter_Per_Alarm_Interrupt_Status._LbitStateChgIntrSta()
            return allFields

    class _Counter_Per_Alarm_Current_Status(AtRegister.AtRegister):
        def name(self):
            return "Counter Per Alarm Current Status"
    
        def description(self):
            return "This is per Alarm interrupt enable of pseudowires. Each register is used to store 4 bits to enable interrupts when the related alarms in pseudowires happen."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x061000 + Grp2048ID*8192 +  GrpID*32 + BitID"
            
        def startAddress(self):
            return 0x00061000
            
        def endAddress(self):
            return 0xffffffff

        class _StrayCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "StrayCurStatus"
            
            def description(self):
                return "Stray packet state current status in the related pseudowire."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MalformCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "MalformCurStatus"
            
            def description(self):
                return "Malform packet state current status in the related pseudowire."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MbitCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "MbitCurStatus"
            
            def description(self):
                return "Mbit packet state current status in the related pseudowire."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RbitCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "RbitCurStatus"
            
            def description(self):
                return "Rbit packet state current status in the related pseudowire."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LofsSyncCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "LofsSyncCurStatus"
            
            def description(self):
                return "Lost of frame Sync state current status in the related pseudowire."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _UnderrunCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "UnderrunCurStatus"
            
            def description(self):
                return "Jitter buffer underrun current status in the related pseudowire."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OverrunCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "OverrunCurStatus"
            
            def description(self):
                return "Jitter buffer overrun current status in the related pseudowire."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LofsStateCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "LofsStateCurStatus"
            
            def description(self):
                return "Lost of frame state current status in the related pseudowire."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LbitStateCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LbitStateCurStatus"
            
            def description(self):
                return "a Lbit packet state current status in the related pseudowire"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["StrayCurStatus"] = _AF6CCI0011_RD_INTALM._Counter_Per_Alarm_Current_Status._StrayCurStatus()
            allFields["MalformCurStatus"] = _AF6CCI0011_RD_INTALM._Counter_Per_Alarm_Current_Status._MalformCurStatus()
            allFields["MbitCurStatus"] = _AF6CCI0011_RD_INTALM._Counter_Per_Alarm_Current_Status._MbitCurStatus()
            allFields["RbitCurStatus"] = _AF6CCI0011_RD_INTALM._Counter_Per_Alarm_Current_Status._RbitCurStatus()
            allFields["LofsSyncCurStatus"] = _AF6CCI0011_RD_INTALM._Counter_Per_Alarm_Current_Status._LofsSyncCurStatus()
            allFields["UnderrunCurStatus"] = _AF6CCI0011_RD_INTALM._Counter_Per_Alarm_Current_Status._UnderrunCurStatus()
            allFields["OverrunCurStatus"] = _AF6CCI0011_RD_INTALM._Counter_Per_Alarm_Current_Status._OverrunCurStatus()
            allFields["LofsStateCurStatus"] = _AF6CCI0011_RD_INTALM._Counter_Per_Alarm_Current_Status._LofsStateCurStatus()
            allFields["LbitStateCurStatus"] = _AF6CCI0011_RD_INTALM._Counter_Per_Alarm_Current_Status._LbitStateCurStatus()
            return allFields

    class _Counter_Interrupt_OR_Status(AtRegister.AtRegister):
        def name(self):
            return "Counter Interrupt OR Status"
    
        def description(self):
            return "This is per Alarm interrupt enable of pseudowires. Each register is used to store 4 bits to enable interrupts when the related alarms in pseudowires happen."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x061800 + Grp2048ID*8192 +  Slice*1024 + GrpID"
            
        def startAddress(self):
            return 0x00061800
            
        def endAddress(self):
            return 0xffffffff

        class _IntrORStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IntrORStatus"
            
            def description(self):
                return "Set to 1 to indicate that there is any interrupt status bit in the Counter per Alarm Interrupt Status register of the related pseudowires to be set and they are enabled to raise interrupt. Bit 0 of GrpID#0 for pseudowire 0, bit 31 of GrpID#0 for pseudowire 31, bit 0 of GrpID#1 for pseudowire 32, respectively."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IntrORStatus"] = _AF6CCI0011_RD_INTALM._Counter_Interrupt_OR_Status._IntrORStatus()
            return allFields

    class _counter_per_group_intr_or_stat(AtRegister.AtRegister):
        def name(self):
            return "Counter per Group Interrupt OR Status"
    
        def description(self):
            return "The register consists of 8 bits for 8 Group of the PW Counter. Each bit is used to store Interrupt OR status of the related Group."
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return "0x061BFF + Grp2048ID*8192 +  Slice*1024"
            
        def startAddress(self):
            return 0x00061bff
            
        def endAddress(self):
            return 0xffffffff

        class _GroupIntrOrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "GroupIntrOrSta"
            
            def description(self):
                return "Set to 1 if any interrupt bit of corresponding Group is set and its interrupt is enabled"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["GroupIntrOrSta"] = _AF6CCI0011_RD_INTALM._counter_per_group_intr_or_stat._GroupIntrOrSta()
            return allFields

    class _counter_per_group_intr_en_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Counter per Group Interrupt Enable Control"
    
        def description(self):
            return "The register consists of 8 interrupt enable bits for 8 group in the PW counter."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x061BFE + Grp2048ID*8192 +  Slice*1024"
            
        def startAddress(self):
            return 0x00061bfe
            
        def endAddress(self):
            return 0xffffffff

        class _GroupIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "GroupIntrEn"
            
            def description(self):
                return "Set to 1 to enable the related Group to generate interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["GroupIntrEn"] = _AF6CCI0011_RD_INTALM._counter_per_group_intr_en_ctrl._GroupIntrEn()
            return allFields

    class _counter_per_slice_intr_or_stat(AtRegister.AtRegister):
        def name(self):
            return "Counter per Slice Interrupt OR Status"
    
        def description(self):
            return "The register consists of 6 bits for 6 Slice of the PW Counter. Each bit is used to store Interrupt OR status of the related 1024 PWs Group."
            
        def width(self):
            return 6
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00068000
            
        def endAddress(self):
            return 0xffffffff

        class _GroupIntrOrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "GroupIntrOrSta"
            
            def description(self):
                return "Set to 1 if any interrupt bit of corresponding Slice is set and its interrupt is enabled"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["GroupIntrOrSta"] = _AF6CCI0011_RD_INTALM._counter_per_slice_intr_or_stat._GroupIntrOrSta()
            return allFields

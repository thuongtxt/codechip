import AtRegister

class _AF6CNC0021_DDR_additional_RD_DIAG(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["pcfg_ctl0"] = _AF6CNC0021_DDR_additional_RD_DIAG._pcfg_ctl0()
        allRegisters["pcfg_cnt0"] = _AF6CNC0021_DDR_additional_RD_DIAG._pcfg_cnt0()
        allRegisters["pcfg_cnt1"] = _AF6CNC0021_DDR_additional_RD_DIAG._pcfg_cnt1()
        allRegisters["pcfg_cnt2"] = _AF6CNC0021_DDR_additional_RD_DIAG._pcfg_cnt2()
        allRegisters["upen_err_dat"] = _AF6CNC0021_DDR_additional_RD_DIAG._upen_err_dat()
        allRegisters["upen_exp_dat"] = _AF6CNC0021_DDR_additional_RD_DIAG._upen_exp_dat()
        allRegisters["upen_err_pat"] = _AF6CNC0021_DDR_additional_RD_DIAG._upen_err_pat()
        return allRegisters

    class _pcfg_ctl0(AtRegister.AtRegister):
        def name(self):
            return "DDR PRBS Force Error"
    
        def description(self):
            return "PRBS Force Error Enable"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _force_error_enable(AtRegister.AtRegisterField):
            def startBit(self):
                return 2
                
            def stopBit(self):
                return 2
        
            def name(self):
                return "force_error_enable"
            
            def description(self):
                return "PRBS Force Error Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["force_error_enable"] = _AF6CNC0021_DDR_additional_RD_DIAG._pcfg_ctl0._force_error_enable()
            return allFields

    class _pcfg_cnt0(AtRegister.AtRegister):
        def name(self):
            return "DDR PRBS Error Counter"
    
        def description(self):
            return "PRBS Error counters"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x40"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _err_cnt(AtRegister.AtRegisterField):
            def startBit(self):
                return 31
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "err_cnt"
            
            def description(self):
                return "DDR PRBS error counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["err_cnt"] = _AF6CNC0021_DDR_additional_RD_DIAG._pcfg_cnt0._err_cnt()
            return allFields

    class _pcfg_cnt1(AtRegister.AtRegister):
        def name(self):
            return "DDR PRBS read command counter"
    
        def description(self):
            return "PRBS read command counters"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x41"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _rdvl_cnt(AtRegister.AtRegisterField):
            def startBit(self):
                return 31
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "rdvl_cnt"
            
            def description(self):
                return "DDR PRBS read counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rdvl_cnt"] = _AF6CNC0021_DDR_additional_RD_DIAG._pcfg_cnt1._rdvl_cnt()
            return allFields

    class _pcfg_cnt2(AtRegister.AtRegister):
        def name(self):
            return "DDR PRBS Write command counter"
    
        def description(self):
            return "PRBS Write command counters"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x42"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _wrvl_cnt(AtRegister.AtRegisterField):
            def startBit(self):
                return 31
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "wrvl_cnt"
            
            def description(self):
                return "DDR PRBS Write counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["wrvl_cnt"] = _AF6CNC0021_DDR_additional_RD_DIAG._pcfg_cnt2._wrvl_cnt()
            return allFields

    class _upen_err_dat(AtRegister.AtRegister):
        def name(self):
            return "DDR Error Data Latch"
    
        def description(self):
            return "Fist error pattern latch register, includes maximum 16x32bit words for 64b DDR interface. Write to clear the latched value, and start the next latched pattern"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x80 + word_id"
            
        def startAddress(self):
            return 0x00000080
            
        def endAddress(self):
            return 0x0000008f

        class _err_data(AtRegister.AtRegisterField):
            def startBit(self):
                return 31
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "err_data"
            
            def description(self):
                return "DDR Error Data  Latch"
            
            def type(self):
                return "RWC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["err_data"] = _AF6CNC0021_DDR_additional_RD_DIAG._upen_err_dat._err_data()
            return allFields

    class _upen_exp_dat(AtRegister.AtRegister):
        def name(self):
            return "DDR Expected Data Latch"
    
        def description(self):
            return "Expected pattern latch register, latched with the first error occur, includes maximumx 16x32bit words for 64b DDR interface. This register is cleared and latched for the next error/expected value, when the Error Data Latch register is cleared."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x90 + word_id"
            
        def startAddress(self):
            return 0x00000090
            
        def endAddress(self):
            return 0x0000009f

        class _exp_data(AtRegister.AtRegisterField):
            def startBit(self):
                return 31
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "exp_data"
            
            def description(self):
                return "DDR Expected Data Latched"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["exp_data"] = _AF6CNC0021_DDR_additional_RD_DIAG._upen_exp_dat._exp_data()
            return allFields

    class _upen_err_pat(AtRegister.AtRegister):
        def name(self):
            return "DDR Error Data Bit"
    
        def description(self):
            return "maximum 16x32bit words for 64b DDR interface. Used to accumulated indicate the error position. When Error occur, the correlative bit of this register is set when the read data and the expected data is unmatch. Write 0 to clear the whole pattern."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xA0 + word_id"
            
        def startAddress(self):
            return 0x000000a0
            
        def endAddress(self):
            return 0x000000af

        class _err_pat(AtRegister.AtRegisterField):
            def startBit(self):
                return 31
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "err_pat"
            
            def description(self):
                return "DDR Error Data Bit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["err_pat"] = _AF6CNC0021_DDR_additional_RD_DIAG._upen_err_pat._err_pat()
            return allFields

import python.arrive.atsdk.AtRegister as AtRegister

class _ES_MII_RD_DIAG(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["MiiAlarmGlb"] = _ES_MII_RD_DIAG._MiiAlarmGlb()
        allRegisters["MiiStatusGlb"] = _ES_MII_RD_DIAG._MiiStatusGlb()
        allRegisters["EthPacketConfig"] = _ES_MII_RD_DIAG._EthPacketConfig()
        allRegisters["MiiEthTestControl"] = _ES_MII_RD_DIAG._MiiEthTestControl()
        allRegisters["gatetime_cfg"] = _ES_MII_RD_DIAG._gatetime_cfg()
        allRegisters["Gatetime_current"] = _ES_MII_RD_DIAG._Gatetime_current()
        allRegisters["itxpkr2c"] = _ES_MII_RD_DIAG._itxpkr2c()
        allRegisters["counter2"] = _ES_MII_RD_DIAG._counter2()
        allRegisters["counter4"] = _ES_MII_RD_DIAG._counter4()
        allRegisters["counter5"] = _ES_MII_RD_DIAG._counter5()
        return allRegisters

    class _MiiAlarmGlb(AtRegister.AtRegister):
        def name(self):
            return "MiiAlarmGlb"
    
        def description(self):
            return "This is status of packet diagnostic"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _opkterr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "opkterr"
            
            def description(self):
                return "Packet Error"
            
            def type(self):
                return "R1W"
            
            def resetValue(self):
                return 0xffffffff

        class _olenerr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "olenerr"
            
            def description(self):
                return "Packet Len Error"
            
            def type(self):
                return "R1W"
            
            def resetValue(self):
                return 0xffffffff

        class _odaterr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "odaterr"
            
            def description(self):
                return "PRBS payload Data Error ( Not Syn)"
            
            def type(self):
                return "R1W"
            
            def resetValue(self):
                return 0xffffffff

        class _link_sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "link_sta"
            
            def description(self):
                return "PHY Link up status"
            
            def type(self):
                return "R1W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["opkterr"] = _ES_MII_RD_DIAG._MiiAlarmGlb._opkterr()
            allFields["olenerr"] = _ES_MII_RD_DIAG._MiiAlarmGlb._olenerr()
            allFields["odaterr"] = _ES_MII_RD_DIAG._MiiAlarmGlb._odaterr()
            allFields["link_sta"] = _ES_MII_RD_DIAG._MiiAlarmGlb._link_sta()
            return allFields

    class _MiiStatusGlb(AtRegister.AtRegister):
        def name(self):
            return "MiiStatusGlb"
    
        def description(self):
            return "This is status of packet diagnostic"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000010
            
        def endAddress(self):
            return 0xffffffff

        class _opkterr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "opkterr"
            
            def description(self):
                return "Packet Error"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _olenerr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "olenerr"
            
            def description(self):
                return "Packet Len Error"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _odaterr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "odaterr"
            
            def description(self):
                return "PRBS payload Data Error ( Not Syn)"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _link_sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "link_sta"
            
            def description(self):
                return "PHY Link up status"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["opkterr"] = _ES_MII_RD_DIAG._MiiStatusGlb._opkterr()
            allFields["olenerr"] = _ES_MII_RD_DIAG._MiiStatusGlb._olenerr()
            allFields["odaterr"] = _ES_MII_RD_DIAG._MiiStatusGlb._odaterr()
            allFields["link_sta"] = _ES_MII_RD_DIAG._MiiStatusGlb._link_sta()
            return allFields

    class _EthPacketConfig(AtRegister.AtRegister):
        def name(self):
            return "EthPacketConfig"
    
        def description(self):
            return "Configuration Packet Diagnostic,"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000001
            
        def endAddress(self):
            return 0xffffffff

        class _icfgpat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 20
        
            def name(self):
                return "icfgpat"
            
            def description(self):
                return "Data fix value for  idatamod = 0x2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _ibandwidth(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ibandwidth"
            
            def description(self):
                return "Band Width"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _idatamod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 12
        
            def name(self):
                return "idatamod"
            
            def description(self):
                return "Payload Data Mode 0:PRBS7 1:PRBS15 2:PRBS23 3:PRBS31"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _ipklen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ipklen"
            
            def description(self):
                return "Packet len"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["icfgpat"] = _ES_MII_RD_DIAG._EthPacketConfig._icfgpat()
            allFields["ibandwidth"] = _ES_MII_RD_DIAG._EthPacketConfig._ibandwidth()
            allFields["idatamod"] = _ES_MII_RD_DIAG._EthPacketConfig._idatamod()
            allFields["ipklen"] = _ES_MII_RD_DIAG._EthPacketConfig._ipklen()
            return allFields

    class _MiiEthTestControl(AtRegister.AtRegister):
        def name(self):
            return "MiiEthTestControl"
    
        def description(self):
            return "Configuration Diagnostic,"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _start_diag(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "start_diag"
            
            def description(self):
                return "Config start Diagnostic trigger 0 to 1 for Start auto run with Gatetime Configuration"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _iforceerr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "iforceerr"
            
            def description(self):
                return "PRBS Data Error Insert"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _test_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "test_en"
            
            def description(self):
                return "PRBS test Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["start_diag"] = _ES_MII_RD_DIAG._MiiEthTestControl._start_diag()
            allFields["iforceerr"] = _ES_MII_RD_DIAG._MiiEthTestControl._iforceerr()
            allFields["test_en"] = _ES_MII_RD_DIAG._MiiEthTestControl._test_en()
            return allFields

    class _gatetime_cfg(AtRegister.AtRegister):
        def name(self):
            return "gatetime config value"
    
        def description(self):
            return "This is Mate serdeses Gatetime for PRBS Raw Diagnostic port 1 to port 4"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000004
            
        def endAddress(self):
            return 0xffffffff

        class _time_cfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 0
        
            def name(self):
                return "time_cfg"
            
            def description(self):
                return "Gatetime Configuration 1-86400 second"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["time_cfg"] = _ES_MII_RD_DIAG._gatetime_cfg._time_cfg()
            return allFields

    class _Gatetime_current(AtRegister.AtRegister):
        def name(self):
            return "Gatetime Current"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000005
            
        def endAddress(self):
            return 0xffffffff

        class _status_gatetime_diag(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "status_gatetime_diag"
            
            def description(self):
                return "Status Gatetime diagnostic port 16 to port1 bit per port 1:Running 0:Done-Ready"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _currert_gatetime_diag(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 0
        
            def name(self):
                return "currert_gatetime_diag"
            
            def description(self):
                return "Current running time of Gatetime diagnostic"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["status_gatetime_diag"] = _ES_MII_RD_DIAG._Gatetime_current._status_gatetime_diag()
            allFields["currert_gatetime_diag"] = _ES_MII_RD_DIAG._Gatetime_current._currert_gatetime_diag()
            return allFields

    class _itxpkr2c(AtRegister.AtRegister):
        def name(self):
            return "MiiTxCounterR2C."
    
        def description(self):
            return "Register to test MII interface"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000c
            
        def endAddress(self):
            return 0xffffffff

        class _Tx_counter_R2C(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Tx_counter_R2C"
            
            def description(self):
                return "Tx Packet counter R2C"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Tx_counter_R2C"] = _ES_MII_RD_DIAG._itxpkr2c._Tx_counter_R2C()
            return allFields

    class _counter2(AtRegister.AtRegister):
        def name(self):
            return "MiiRxCounterR2C.."
    
        def description(self):
            return "Register to test MII interface"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000d
            
        def endAddress(self):
            return 0xffffffff

        class _Rx_counter_R2C(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Rx_counter_R2C"
            
            def description(self):
                return "Rx counter R2C"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Rx_counter_R2C"] = _ES_MII_RD_DIAG._counter2._Rx_counter_R2C()
            return allFields

    class _counter4(AtRegister.AtRegister):
        def name(self):
            return "MiiTxCounterRO."
    
        def description(self):
            return "Register to test MII interface"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000008
            
        def endAddress(self):
            return 0xffffffff

        class _Tx_counter_RO(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Tx_counter_RO"
            
            def description(self):
                return "Tx counter RO"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Tx_counter_RO"] = _ES_MII_RD_DIAG._counter4._Tx_counter_RO()
            return allFields

    class _counter5(AtRegister.AtRegister):
        def name(self):
            return "MiiRxCounterRO."
    
        def description(self):
            return "Register to test MII interface"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000009
            
        def endAddress(self):
            return 0xffffffff

        class _Rx_counter_RO(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Rx_counter_RO"
            
            def description(self):
                return "Rx counter RO"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Rx_counter_RO"] = _ES_MII_RD_DIAG._counter5._Rx_counter_RO()
            return allFields

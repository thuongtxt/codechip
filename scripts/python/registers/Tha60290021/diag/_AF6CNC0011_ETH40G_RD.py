import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0011_ETH40G_RD(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["OETH_40G_DRP"] = _AF6CNC0011_ETH40G_RD._OETH_40G_DRP()
        allRegisters["ETH_40G_LoopBack"] = _AF6CNC0011_ETH40G_RD._ETH_40G_LoopBack()
        allRegisters["ETH_40G_QLL_Status"] = _AF6CNC0011_ETH40G_RD._ETH_40G_QLL_Status()
        allRegisters["ETH_40G_TX_Reset"] = _AF6CNC0011_ETH40G_RD._ETH_40G_TX_Reset()
        allRegisters["ETH_49G_RX_Reset"] = _AF6CNC0011_ETH40G_RD._ETH_49G_RX_Reset()
        allRegisters["ETH_40G_LPMDFE_Mode"] = _AF6CNC0011_ETH40G_RD._ETH_40G_LPMDFE_Mode()
        allRegisters["ETH_40G_LPMDFE_Reset"] = _AF6CNC0011_ETH40G_RD._ETH_40G_LPMDFE_Reset()
        allRegisters["ETH_40G_TXDIFFCTRL"] = _AF6CNC0011_ETH40G_RD._ETH_40G_TXDIFFCTRL()
        allRegisters["ETH_40G_TXPOSTCURSOR"] = _AF6CNC0011_ETH40G_RD._ETH_40G_TXPOSTCURSOR()
        allRegisters["ETH_40G_TXPRECURSOR"] = _AF6CNC0011_ETH40G_RD._ETH_40G_TXPRECURSOR()
        allRegisters["ETH_40G_Ctrl_FCS"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Ctrl_FCS()
        allRegisters["ETH_40G_AutoNeg"] = _AF6CNC0011_ETH40G_RD._ETH_40G_AutoNeg()
        allRegisters["ETH_40G_Diag_ctrl0"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Diag_ctrl0()
        allRegisters["ETH_40G_Diag_ctrl1"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Diag_ctrl1()
        allRegisters["ETH_40G_Diag_ctrl2"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Diag_ctrl2()
        allRegisters["ETH_40G_Diag_ctrl3"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Diag_ctrl3()
        allRegisters["ETH_40G_Diag_ctrl4"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Diag_ctrl4()
        allRegisters["ETH_40G_Diag_Sta0"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Diag_Sta0()
        allRegisters["ETH_40G_Diag_TXPKT"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Diag_TXPKT()
        allRegisters["ETH_40G_Diag_TXNOB"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Diag_TXNOB()
        allRegisters["ETH_40G_Diag_RXPKT"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Diag_RXPKT()
        allRegisters["ETH_40G_Diag_RXNOB"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Diag_RXNOB()
        return allRegisters

    class _OETH_40G_DRP(AtRegister.AtRegister):
        def name(self):
            return "ETH 40G DRP"
    
        def description(self):
            return "Read/Write DRP address of SERDES"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1000+$P*0x400+$DRP"
            
        def startAddress(self):
            return 0x00001000
            
        def endAddress(self):
            return 0x00001fff

        class _drp_rw(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "drp_rw"
            
            def description(self):
                return "DRP read/write value"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["drp_rw"] = _AF6CNC0011_ETH40G_RD._OETH_40G_DRP._drp_rw()
            return allFields

    class _ETH_40G_LoopBack(AtRegister.AtRegister):
        def name(self):
            return "ETH 40G LoopBack"
    
        def description(self):
            return "Configurate LoopBack"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0xffffffff

        class _lpback_lane3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "lpback_lane3"
            
            def description(self):
                return "Loopback lane3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lpback_lane2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "lpback_lane2"
            
            def description(self):
                return "Loopback lane2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lpback_lane1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "lpback_lane1"
            
            def description(self):
                return "Loopback lane1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lpback_lane0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lpback_lane0"
            
            def description(self):
                return "Loopback lane0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lpback_lane3"] = _AF6CNC0011_ETH40G_RD._ETH_40G_LoopBack._lpback_lane3()
            allFields["lpback_lane2"] = _AF6CNC0011_ETH40G_RD._ETH_40G_LoopBack._lpback_lane2()
            allFields["lpback_lane1"] = _AF6CNC0011_ETH40G_RD._ETH_40G_LoopBack._lpback_lane1()
            allFields["lpback_lane0"] = _AF6CNC0011_ETH40G_RD._ETH_40G_LoopBack._lpback_lane0()
            return allFields

    class _ETH_40G_QLL_Status(AtRegister.AtRegister):
        def name(self):
            return "ETH 40G QLL Status"
    
        def description(self):
            return "QPLL status"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000b
            
        def endAddress(self):
            return 0xffffffff

        class _QPLL1_Lock_change(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "QPLL1_Lock_change"
            
            def description(self):
                return "QPLL1 has transition lock/unlock, Group 0-3"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _QPLL0_Lock_change(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "QPLL0_Lock_change"
            
            def description(self):
                return "QPLL0 has transition lock/unlock, Group 0-3"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _QPLL1_Lock(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "QPLL1_Lock"
            
            def description(self):
                return "QPLL0 is Locked, Group 0-3"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _QPLL0_Lock(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "QPLL0_Lock"
            
            def description(self):
                return "QPLL0 is Locked, Group 0-3"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["QPLL1_Lock_change"] = _AF6CNC0011_ETH40G_RD._ETH_40G_QLL_Status._QPLL1_Lock_change()
            allFields["QPLL0_Lock_change"] = _AF6CNC0011_ETH40G_RD._ETH_40G_QLL_Status._QPLL0_Lock_change()
            allFields["QPLL1_Lock"] = _AF6CNC0011_ETH40G_RD._ETH_40G_QLL_Status._QPLL1_Lock()
            allFields["QPLL0_Lock"] = _AF6CNC0011_ETH40G_RD._ETH_40G_QLL_Status._QPLL0_Lock()
            return allFields

    class _ETH_40G_TX_Reset(AtRegister.AtRegister):
        def name(self):
            return "ETH 40G TX Reset"
    
        def description(self):
            return "Reset TX SERDES"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000c
            
        def endAddress(self):
            return 0xffffffff

        class _txrst_done(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "txrst_done"
            
            def description(self):
                return "TX Reset Done"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _txrst_trig(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txrst_trig"
            
            def description(self):
                return "Trige 0->1 to start reset TX SERDES"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txrst_done"] = _AF6CNC0011_ETH40G_RD._ETH_40G_TX_Reset._txrst_done()
            allFields["txrst_trig"] = _AF6CNC0011_ETH40G_RD._ETH_40G_TX_Reset._txrst_trig()
            return allFields

    class _ETH_49G_RX_Reset(AtRegister.AtRegister):
        def name(self):
            return "ETH 40G RX Reset"
    
        def description(self):
            return "Reset RX SERDES"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000d
            
        def endAddress(self):
            return 0xffffffff

        class _rxrst_done(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "rxrst_done"
            
            def description(self):
                return "RX Reset Done"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _rxrst_trig(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxrst_trig"
            
            def description(self):
                return "Trige 0->1 to start reset RX SERDES"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxrst_done"] = _AF6CNC0011_ETH40G_RD._ETH_49G_RX_Reset._rxrst_done()
            allFields["rxrst_trig"] = _AF6CNC0011_ETH40G_RD._ETH_49G_RX_Reset._rxrst_trig()
            return allFields

    class _ETH_40G_LPMDFE_Mode(AtRegister.AtRegister):
        def name(self):
            return "ETH 40G LPMDFE Mode"
    
        def description(self):
            return "Configure LPM/DFE mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000e
            
        def endAddress(self):
            return 0xffffffff

        class _lpmdfe_mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lpmdfe_mode"
            
            def description(self):
                return "LPM/DFE mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lpmdfe_mode"] = _AF6CNC0011_ETH40G_RD._ETH_40G_LPMDFE_Mode._lpmdfe_mode()
            return allFields

    class _ETH_40G_LPMDFE_Reset(AtRegister.AtRegister):
        def name(self):
            return "ETH 40G LPMDFE Reset"
    
        def description(self):
            return "Reset LPM/DFE"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000f
            
        def endAddress(self):
            return 0xffffffff

        class _lpmdfe_reset(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lpmdfe_reset"
            
            def description(self):
                return "LPM/DFE reset"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lpmdfe_reset"] = _AF6CNC0011_ETH40G_RD._ETH_40G_LPMDFE_Reset._lpmdfe_reset()
            return allFields

    class _ETH_40G_TXDIFFCTRL(AtRegister.AtRegister):
        def name(self):
            return "ETH 40G TXDIFFCTRL"
    
        def description(self):
            return "Driver Swing Control, see \"Table 3-35: TX Configurable Driver Ports\" page 158 of UG578 for more detail"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000010
            
        def endAddress(self):
            return 0xffffffff

        class _TXDIFFCTRL(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TXDIFFCTRL"
            
            def description(self):
                return "TXDIFFCTRL"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TXDIFFCTRL"] = _AF6CNC0011_ETH40G_RD._ETH_40G_TXDIFFCTRL._TXDIFFCTRL()
            return allFields

    class _ETH_40G_TXPOSTCURSOR(AtRegister.AtRegister):
        def name(self):
            return "ETH 40G TXPOSTCURSOR"
    
        def description(self):
            return "Transmitter post-cursor TX pre-emphasis control, see \"Table 3-35: TX Configurable Driver Ports\" page 160 of UG578 for more detail"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000011
            
        def endAddress(self):
            return 0xffffffff

        class _TXPOSTCURSOR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TXPOSTCURSOR"
            
            def description(self):
                return "TXPOSTCURSOR"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TXPOSTCURSOR"] = _AF6CNC0011_ETH40G_RD._ETH_40G_TXPOSTCURSOR._TXPOSTCURSOR()
            return allFields

    class _ETH_40G_TXPRECURSOR(AtRegister.AtRegister):
        def name(self):
            return "ETH 40G TXPRECURSOR"
    
        def description(self):
            return "Transmitter pre-cursor TX pre-emphasis control, see \"Table 3-35: TX Configurable Driver Ports\" page 161 of UG578 for more detail"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000012
            
        def endAddress(self):
            return 0xffffffff

        class _TXPRECURSOR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TXPRECURSOR"
            
            def description(self):
                return "TXPRECURSOR"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TXPRECURSOR"] = _AF6CNC0011_ETH40G_RD._ETH_40G_TXPRECURSOR._TXPRECURSOR()
            return allFields

    class _ETH_40G_Ctrl_FCS(AtRegister.AtRegister):
        def name(self):
            return "ETH 40G Ctrl FCS"
    
        def description(self):
            return "configure FCS mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000080
            
        def endAddress(self):
            return 0xffffffff

        class _txfcs_ignore(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "txfcs_ignore"
            
            def description(self):
                return "TX ignore check FCS when txfcs_ins is low"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _txfcs_ins(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "txfcs_ins"
            
            def description(self):
                return "TX inserts 4bytes FCS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _rxfcs_ignore(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "rxfcs_ignore"
            
            def description(self):
                return "RX ignore check FCS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _rxfcs_rmv(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxfcs_rmv"
            
            def description(self):
                return "RX remove 4bytes FCS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txfcs_ignore"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Ctrl_FCS._txfcs_ignore()
            allFields["txfcs_ins"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Ctrl_FCS._txfcs_ins()
            allFields["rxfcs_ignore"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Ctrl_FCS._rxfcs_ignore()
            allFields["rxfcs_rmv"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Ctrl_FCS._rxfcs_rmv()
            return allFields

    class _ETH_40G_AutoNeg(AtRegister.AtRegister):
        def name(self):
            return "ETH 40G AutoNeg"
    
        def description(self):
            return "configure Auto-Neg"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000081
            
        def endAddress(self):
            return 0xffffffff

        class _an_lt_sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "an_lt_sta"
            
            def description(self):
                return "Link Control outputs from the auto-negotiationcontroller for the various Ethernet protocols."
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lt_restart(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "lt_restart"
            
            def description(self):
                return "This signal triggers a restart of link training regardless of the current state."
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lt_enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "lt_enb"
            
            def description(self):
                return "Enables link training. When link training is disabled, all PCS lanes function in mission mode."
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _an_restart(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "an_restart"
            
            def description(self):
                return "This input is used to trigger a restart of the auto-negotiation, regardless of what state the circuit is currently in."
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _an_bypass(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "an_bypass"
            
            def description(self):
                return "Input to disable auto-negotiation and bypass the auto-negotiation function. If this input is asserted, auto-negotiation is turned off, but the PCS is connected to the output to allow operation."
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _an_enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "an_enb"
            
            def description(self):
                return "Enable signal for auto-negotiation"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["an_lt_sta"] = _AF6CNC0011_ETH40G_RD._ETH_40G_AutoNeg._an_lt_sta()
            allFields["lt_restart"] = _AF6CNC0011_ETH40G_RD._ETH_40G_AutoNeg._lt_restart()
            allFields["lt_enb"] = _AF6CNC0011_ETH40G_RD._ETH_40G_AutoNeg._lt_enb()
            allFields["an_restart"] = _AF6CNC0011_ETH40G_RD._ETH_40G_AutoNeg._an_restart()
            allFields["an_bypass"] = _AF6CNC0011_ETH40G_RD._ETH_40G_AutoNeg._an_bypass()
            allFields["an_enb"] = _AF6CNC0011_ETH40G_RD._ETH_40G_AutoNeg._an_enb()
            return allFields

    class _ETH_40G_Diag_ctrl0(AtRegister.AtRegister):
        def name(self):
            return "ETH 40G Diag Ctrl0"
    
        def description(self):
            return "Diagnostic control 0"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000020
            
        def endAddress(self):
            return 0xffffffff

        class _diag_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "diag_err"
            
            def description(self):
                return "Error detection"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _diag_ferr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "diag_ferr"
            
            def description(self):
                return "Enable force error data of diagnostic packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _diag_datmod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "diag_datmod"
            
            def description(self):
                return "payload mod of ethernet frame"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _diag_enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "diag_enb"
            
            def description(self):
                return "enable diagnostic block"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["diag_err"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Diag_ctrl0._diag_err()
            allFields["diag_ferr"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Diag_ctrl0._diag_ferr()
            allFields["diag_datmod"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Diag_ctrl0._diag_datmod()
            allFields["diag_enb"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Diag_ctrl0._diag_enb()
            return allFields

    class _ETH_40G_Diag_ctrl1(AtRegister.AtRegister):
        def name(self):
            return "ETH 40G Diag Ctrl1"
    
        def description(self):
            return "Diagnostic control 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000021
            
        def endAddress(self):
            return 0xffffffff

        class _diag_lenmax(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "diag_lenmax"
            
            def description(self):
                return "Maximum length of diagnostic packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _diag_lenmin(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "diag_lenmin"
            
            def description(self):
                return "Minimum length of diagnostic packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["diag_lenmax"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Diag_ctrl1._diag_lenmax()
            allFields["diag_lenmin"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Diag_ctrl1._diag_lenmin()
            return allFields

    class _ETH_40G_Diag_ctrl2(AtRegister.AtRegister):
        def name(self):
            return "ETH 40G Diag Ctrl2"
    
        def description(self):
            return "Diagnostic control 2"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000022
            
        def endAddress(self):
            return 0xffffffff

        class _diag_dalsb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "diag_dalsb"
            
            def description(self):
                return "32bit-LSB DA of diagnostic packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["diag_dalsb"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Diag_ctrl2._diag_dalsb()
            return allFields

    class _ETH_40G_Diag_ctrl3(AtRegister.AtRegister):
        def name(self):
            return "ETH 40G Diag Ctrl3"
    
        def description(self):
            return "Diagnostic control 3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000023
            
        def endAddress(self):
            return 0xffffffff

        class _diag_salsb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "diag_salsb"
            
            def description(self):
                return "32bit-LSB SA of diagnostic packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["diag_salsb"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Diag_ctrl3._diag_salsb()
            return allFields

    class _ETH_40G_Diag_ctrl4(AtRegister.AtRegister):
        def name(self):
            return "ETH 40G Diag Ctrl4"
    
        def description(self):
            return "Diagnostic control 4"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000024
            
        def endAddress(self):
            return 0xffffffff

        class _diag_damsb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "diag_damsb"
            
            def description(self):
                return "16bit-MSB DA of diagnostic packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _diag_samsb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "diag_samsb"
            
            def description(self):
                return "16bit-MSB SA of diagnostic packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["diag_damsb"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Diag_ctrl4._diag_damsb()
            allFields["diag_samsb"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Diag_ctrl4._diag_samsb()
            return allFields

    class _ETH_40G_Diag_Sta0(AtRegister.AtRegister):
        def name(self):
            return "ETH 40G Diag Sta0"
    
        def description(self):
            return "Diagnostic Sta0"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000040
            
        def endAddress(self):
            return 0xffffffff

        class _diag_txmis_sop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "diag_txmis_sop"
            
            def description(self):
                return "Packet miss SOP"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _diag_txmis_eop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "diag_txmis_eop"
            
            def description(self):
                return "Packet miss EOP"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _diag_txsop_eop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "diag_txsop_eop"
            
            def description(self):
                return "Short packet, length is less than 16bytes"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _diag_txwff_ful(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "diag_txwff_ful"
            
            def description(self):
                return "TX-Fifo is full"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _diag_rxmis_sop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "diag_rxmis_sop"
            
            def description(self):
                return "Packet miss SOP"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _diag_rxmis_eop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "diag_rxmis_eop"
            
            def description(self):
                return "Packet miss EOP"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _diag_rxsop_eop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "diag_rxsop_eop"
            
            def description(self):
                return "Short packet, length is less than 16bytes  t"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _diag_rxwff_ful(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "diag_rxwff_ful"
            
            def description(self):
                return "RX-Fifo is full"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["diag_txmis_sop"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Diag_Sta0._diag_txmis_sop()
            allFields["diag_txmis_eop"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Diag_Sta0._diag_txmis_eop()
            allFields["diag_txsop_eop"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Diag_Sta0._diag_txsop_eop()
            allFields["diag_txwff_ful"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Diag_Sta0._diag_txwff_ful()
            allFields["diag_rxmis_sop"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Diag_Sta0._diag_rxmis_sop()
            allFields["diag_rxmis_eop"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Diag_Sta0._diag_rxmis_eop()
            allFields["diag_rxsop_eop"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Diag_Sta0._diag_rxsop_eop()
            allFields["diag_rxwff_ful"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Diag_Sta0._diag_rxwff_ful()
            return allFields

    class _ETH_40G_Diag_TXPKT(AtRegister.AtRegister):
        def name(self):
            return "ETH 40G Diag TXPKT"
    
        def description(self):
            return "Diagnostic TX packet counter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000042
            
        def endAddress(self):
            return 0xffffffff

        class _diag_txpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "diag_txpkt"
            
            def description(self):
                return "TX packet counter"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["diag_txpkt"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Diag_TXPKT._diag_txpkt()
            return allFields

    class _ETH_40G_Diag_TXNOB(AtRegister.AtRegister):
        def name(self):
            return "ETH 40G Diag TXNOB"
    
        def description(self):
            return "Diagnostic TX number of bytes counter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000043
            
        def endAddress(self):
            return 0xffffffff

        class _diag_txnob(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "diag_txnob"
            
            def description(self):
                return "TX number of byte counter"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["diag_txnob"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Diag_TXNOB._diag_txnob()
            return allFields

    class _ETH_40G_Diag_RXPKT(AtRegister.AtRegister):
        def name(self):
            return "ETH 40G Diag RXPKT"
    
        def description(self):
            return "Diagnostic RX packet counter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000044
            
        def endAddress(self):
            return 0xffffffff

        class _diag_rxpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "diag_rxpkt"
            
            def description(self):
                return "RX packet counter"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["diag_rxpkt"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Diag_RXPKT._diag_rxpkt()
            return allFields

    class _ETH_40G_Diag_RXNOB(AtRegister.AtRegister):
        def name(self):
            return "ETH 40G Diag RXNOB"
    
        def description(self):
            return "Diagnostic RX number of bytes counter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000045
            
        def endAddress(self):
            return 0xffffffff

        class _diag_rxnob(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "diag_rxnob"
            
            def description(self):
                return "RX number of byte counter"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["diag_rxnob"] = _AF6CNC0011_ETH40G_RD._ETH_40G_Diag_RXNOB._diag_rxnob()
            return allFields

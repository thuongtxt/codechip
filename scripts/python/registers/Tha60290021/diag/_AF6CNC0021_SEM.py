import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0021_SEM(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["sem_cmd_ctrl"] = _AF6CNC0021_SEM._sem_cmd_ctrl()
        allRegisters["sem_cmd_val0"] = _AF6CNC0021_SEM._sem_cmd_val0()
        allRegisters["sem_ctrl_stk_sta"] = _AF6CNC0021_SEM._sem_ctrl_stk_sta()
        allRegisters["sem_ctrl_cur_sta"] = _AF6CNC0021_SEM._sem_ctrl_cur_sta()
        allRegisters["sem_int_enb"] = _AF6CNC0021_SEM._sem_int_enb()
        allRegisters["sem_int_or"] = _AF6CNC0021_SEM._sem_int_or()
        allRegisters["sem_essen_cnt_r2c"] = _AF6CNC0021_SEM._sem_essen_cnt_r2c()
        allRegisters["sem_essen_cnt_ro"] = _AF6CNC0021_SEM._sem_essen_cnt_ro()
        allRegisters["sem_uncorr_cnt_r2c"] = _AF6CNC0021_SEM._sem_uncorr_cnt_r2c()
        allRegisters["sem_uncorr_cnt_ro"] = _AF6CNC0021_SEM._sem_uncorr_cnt_ro()
        allRegisters["sem_corr_cnt_r2c"] = _AF6CNC0021_SEM._sem_corr_cnt_r2c()
        allRegisters["sem_corr_cnt_ro"] = _AF6CNC0021_SEM._sem_corr_cnt_ro()
        allRegisters["sem_slrid"] = _AF6CNC0021_SEM._sem_slrid()
        allRegisters["sem_mon_sta"] = _AF6CNC0021_SEM._sem_mon_sta()
        allRegisters["sem_mon_val"] = _AF6CNC0021_SEM._sem_mon_val()
        allRegisters["sem_mon_ctrl"] = _AF6CNC0021_SEM._sem_mon_ctrl()
        allRegisters["sem_mon_cmd_part0"] = _AF6CNC0021_SEM._sem_mon_cmd_part0()
        allRegisters["sem_mon_cmd_part1"] = _AF6CNC0021_SEM._sem_mon_cmd_part1()
        allRegisters["sem_mon_cmd_part2"] = _AF6CNC0021_SEM._sem_mon_cmd_part2()
        allRegisters["sem_mon_cmd_part3"] = _AF6CNC0021_SEM._sem_mon_cmd_part3()
        allRegisters["sem_hrtb_sta"] = _AF6CNC0021_SEM._sem_hrtb_sta()
        return allRegisters

    class _sem_cmd_ctrl(AtRegister.AtRegister):
        def name(self):
            return "SEU PCI Configuration 0"
    
        def description(self):
            return "Control Command Interface bus of each SLR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return "0x000+$slrid*0x100+0x00"
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0x00000200

        class _cmd_ctrl_done(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "cmd_ctrl_done"
            
            def description(self):
                return "Command Interface status"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _cmd_ctrl_busy(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "cmd_ctrl_busy"
            
            def description(self):
                return "Command Interface status"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _SEU_INJECT_STROBE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "SEU_INJECT_STROBE"
            
            def description(self):
                return "Trigger 1->0 to start command"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _OP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "OP"
            
            def description(self):
                return "Command type, other values are unused"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cmd_ctrl_done"] = _AF6CNC0021_SEM._sem_cmd_ctrl._cmd_ctrl_done()
            allFields["cmd_ctrl_busy"] = _AF6CNC0021_SEM._sem_cmd_ctrl._cmd_ctrl_busy()
            allFields["SEU_INJECT_STROBE"] = _AF6CNC0021_SEM._sem_cmd_ctrl._SEU_INJECT_STROBE()
            allFields["OP"] = _AF6CNC0021_SEM._sem_cmd_ctrl._OP()
            return allFields

    class _sem_cmd_val0(AtRegister.AtRegister):
        def name(self):
            return "SEU PCI Configuration 1"
    
        def description(self):
            return "Command Interface bus value part0 of each SLR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x001+$slrid*0x100+0x01"
            
        def startAddress(self):
            return 0x00000001
            
        def endAddress(self):
            return 0x00000201

        class _sem_cmd_slr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 29
        
            def name(self):
                return "sem_cmd_slr"
            
            def description(self):
                return "Hardware slr number (2-bit),valid range 0-2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _sem_cmd_fradr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 12
        
            def name(self):
                return "sem_cmd_fradr"
            
            def description(self):
                return "Frame address 17bit, valid range 0-(MAX Frame-2), Max Frame of VU190 is 0x000934A"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _sem_cmd_wradr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 5
        
            def name(self):
                return "sem_cmd_wradr"
            
            def description(self):
                return "Word Address 7bit, valid range 0-122"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _sem_cmd_bitadr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "sem_cmd_bitadr"
            
            def description(self):
                return "Bit Address  5bit, valid range 0-31"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["sem_cmd_slr"] = _AF6CNC0021_SEM._sem_cmd_val0._sem_cmd_slr()
            allFields["sem_cmd_fradr"] = _AF6CNC0021_SEM._sem_cmd_val0._sem_cmd_fradr()
            allFields["sem_cmd_wradr"] = _AF6CNC0021_SEM._sem_cmd_val0._sem_cmd_wradr()
            allFields["sem_cmd_bitadr"] = _AF6CNC0021_SEM._sem_cmd_val0._sem_cmd_bitadr()
            return allFields

    class _sem_ctrl_stk_sta(AtRegister.AtRegister):
        def name(self):
            return "SEU Alarm"
    
        def description(self):
            return "SEM controller sticky state of each SLR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x002+$slrid*0x100+0x02"
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0x00000202

        class _status_heartbeat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "status_heartbeat"
            
            def description(self):
                return "status_heartbeat : Set 1 indicate to SEM IP got heartbeat event"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _status_uncorrectable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "status_uncorrectable"
            
            def description(self):
                return "status_uncorrectable : Set 1 indicate to SEM IP got uncorrectable event"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _status_correctable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "status_correctable"
            
            def description(self):
                return "status_correctable : Set 1 indicate to SEM IP got correctable event"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _status_essential(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "status_essential"
            
            def description(self):
                return "status_essential : Set 1 indicate to SEM IP got essential event"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Sem_Act(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "Sem_Act"
            
            def description(self):
                return "Sem_Act : Set 1 indicate to SEM IP got Active State event"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _status_idle(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "status_idle"
            
            def description(self):
                return "status_idle : Set 1 indicate to SEM IP got IDLE State event"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _status_initialization(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "status_initialization"
            
            def description(self):
                return "status_initialization : Set 1 indicate to SEM IP got initialization State event"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _status_injection(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "status_injection"
            
            def description(self):
                return "status_injection : Set 1 indicate to SEM IP got injection State event"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _status_classification(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "status_classification"
            
            def description(self):
                return "status_classification : Set 1 indicate to SEM IP got classification State event"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Fatal_Sem_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Fatal_Sem_err"
            
            def description(self):
                return "Fatal_Sem_Err : Set 1 indicate to SEM IP has fatal error"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _status_observation(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "status_observation"
            
            def description(self):
                return "status_observation : Set 1 indicate to SEM IP got observation State event"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _status_correction(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "status_correction"
            
            def description(self):
                return "status_correction : Set 1 indicate to SEM IP got Correction State event"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["status_heartbeat"] = _AF6CNC0021_SEM._sem_ctrl_stk_sta._status_heartbeat()
            allFields["status_uncorrectable"] = _AF6CNC0021_SEM._sem_ctrl_stk_sta._status_uncorrectable()
            allFields["status_correctable"] = _AF6CNC0021_SEM._sem_ctrl_stk_sta._status_correctable()
            allFields["status_essential"] = _AF6CNC0021_SEM._sem_ctrl_stk_sta._status_essential()
            allFields["Sem_Act"] = _AF6CNC0021_SEM._sem_ctrl_stk_sta._Sem_Act()
            allFields["status_idle"] = _AF6CNC0021_SEM._sem_ctrl_stk_sta._status_idle()
            allFields["status_initialization"] = _AF6CNC0021_SEM._sem_ctrl_stk_sta._status_initialization()
            allFields["status_injection"] = _AF6CNC0021_SEM._sem_ctrl_stk_sta._status_injection()
            allFields["status_classification"] = _AF6CNC0021_SEM._sem_ctrl_stk_sta._status_classification()
            allFields["Fatal_Sem_err"] = _AF6CNC0021_SEM._sem_ctrl_stk_sta._Fatal_Sem_err()
            allFields["status_observation"] = _AF6CNC0021_SEM._sem_ctrl_stk_sta._status_observation()
            allFields["status_correction"] = _AF6CNC0021_SEM._sem_ctrl_stk_sta._status_correction()
            return allFields

    class _sem_ctrl_cur_sta(AtRegister.AtRegister):
        def name(self):
            return "SEU PCI Status"
    
        def description(self):
            return "SEM controller current state of each SLR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x003+$slrid*0x100+0x03"
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0x00000203

        class _Sem_Act(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "Sem_Act"
            
            def description(self):
                return "Sem_Act: Set 1 indicate  status FSM of SEM IP is active"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _status_idle(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "status_idle"
            
            def description(self):
                return "status_idle: Set 1 indicate status FSM of SEM IP is IDLE"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _status_Initialization(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "status_Initialization"
            
            def description(self):
                return "status_Initialization: Set 1 indicate status FSM of SEM IP is INIITIALIZATION"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _status_injection(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "status_injection"
            
            def description(self):
                return "status_injection: Set 1 indicate status FSM of SEM IP is injection"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _status_classification(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "status_classification"
            
            def description(self):
                return "status_classification: Set 1 indicate status FSM of SEM IP is Classification"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _fatal_sem_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "fatal_sem_err"
            
            def description(self):
                return "fatal_sem_err: Set 1 indicate status FSM of SEM IP is Falal Error"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _status_observation(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "status_observation"
            
            def description(self):
                return "status_observation: Set 1 indicate status FSM of SEM IP is OBSERVATION"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _status_correction(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "status_correction"
            
            def description(self):
                return "status_correction: Set 1 indicate status FSM of SEM IP is Correction"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Sem_Act"] = _AF6CNC0021_SEM._sem_ctrl_cur_sta._Sem_Act()
            allFields["status_idle"] = _AF6CNC0021_SEM._sem_ctrl_cur_sta._status_idle()
            allFields["status_Initialization"] = _AF6CNC0021_SEM._sem_ctrl_cur_sta._status_Initialization()
            allFields["status_injection"] = _AF6CNC0021_SEM._sem_ctrl_cur_sta._status_injection()
            allFields["status_classification"] = _AF6CNC0021_SEM._sem_ctrl_cur_sta._status_classification()
            allFields["fatal_sem_err"] = _AF6CNC0021_SEM._sem_ctrl_cur_sta._fatal_sem_err()
            allFields["status_observation"] = _AF6CNC0021_SEM._sem_ctrl_cur_sta._status_observation()
            allFields["status_correction"] = _AF6CNC0021_SEM._sem_ctrl_cur_sta._status_correction()
            return allFields

    class _sem_int_enb(AtRegister.AtRegister):
        def name(self):
            return "SEU Interrupt enable"
    
        def description(self):
            return "SEM controller enable interrupt of each SLR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x004+$slrid*0x100+0x04"
            
        def startAddress(self):
            return 0x00000004
            
        def endAddress(self):
            return 0x00000204

        class _status_heartbeat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "status_heartbeat"
            
            def description(self):
                return "Set 1 to enable heartbeat interrupt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _status_uncorrectable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "status_uncorrectable"
            
            def description(self):
                return "Set 1 to enable uncorrectable interrupt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _status_correctable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "status_correctable"
            
            def description(self):
                return "Set 1 to enable correctable interrupt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _status_essential(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "status_essential"
            
            def description(self):
                return "Set 1 to enable essential interrupt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sem_Act(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "Sem_Act"
            
            def description(self):
                return "Set 1 to enable SEM active interrupt"
            
            def type(self):
                return ""
            
            def resetValue(self):
                return 0xffffffff

        class _status_idle(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "status_idle"
            
            def description(self):
                return "Set 1 to enable idle interrupt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _status_initialization(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "status_initialization"
            
            def description(self):
                return "Set 1 to enable initialization interrupt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _status_injection(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "status_injection"
            
            def description(self):
                return "Set 1 to enable injection interrupt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _status_classification(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "status_classification"
            
            def description(self):
                return "Set 1 to enable classification interrupt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Fatal_Sem_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Fatal_Sem_err"
            
            def description(self):
                return "Set 1 to enable fatal interrupt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _status_observation(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "status_observation"
            
            def description(self):
                return "Set 1 to enable observation interrupt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _CorSt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CorSt"
            
            def description(self):
                return "Set 1 to enable correction interrupt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["status_heartbeat"] = _AF6CNC0021_SEM._sem_int_enb._status_heartbeat()
            allFields["status_uncorrectable"] = _AF6CNC0021_SEM._sem_int_enb._status_uncorrectable()
            allFields["status_correctable"] = _AF6CNC0021_SEM._sem_int_enb._status_correctable()
            allFields["status_essential"] = _AF6CNC0021_SEM._sem_int_enb._status_essential()
            allFields["Sem_Act"] = _AF6CNC0021_SEM._sem_int_enb._Sem_Act()
            allFields["status_idle"] = _AF6CNC0021_SEM._sem_int_enb._status_idle()
            allFields["status_initialization"] = _AF6CNC0021_SEM._sem_int_enb._status_initialization()
            allFields["status_injection"] = _AF6CNC0021_SEM._sem_int_enb._status_injection()
            allFields["status_classification"] = _AF6CNC0021_SEM._sem_int_enb._status_classification()
            allFields["Fatal_Sem_err"] = _AF6CNC0021_SEM._sem_int_enb._Fatal_Sem_err()
            allFields["status_observation"] = _AF6CNC0021_SEM._sem_int_enb._status_observation()
            allFields["CorSt"] = _AF6CNC0021_SEM._sem_int_enb._CorSt()
            return allFields

    class _sem_int_or(AtRegister.AtRegister):
        def name(self):
            return "SEU Interrupt OR"
    
        def description(self):
            return "SEM controller interrupt OR of each SLR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x005+$slrid*0x100+0x04"
            
        def startAddress(self):
            return 0x00000005
            
        def endAddress(self):
            return 0x00000205

        class _int_or(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "int_or"
            
            def description(self):
                return "interrupt state change of SEM's State"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["int_or"] = _AF6CNC0021_SEM._sem_int_or._int_or()
            return allFields

    class _sem_essen_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "SEU Essential Counter R2C"
    
        def description(self):
            return "SEU Essential Counter R2C of each SLR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x008+$slrid*0x100+0x08"
            
        def startAddress(self):
            return 0x00000008
            
        def endAddress(self):
            return 0x00000208

        class _Sem_Essential_Cnt_r2c(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Sem_Essential_Cnt_r2c"
            
            def description(self):
                return "Sem_Essential_Cnt: Counter Essential of SEM IP"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Sem_Essential_Cnt_r2c"] = _AF6CNC0021_SEM._sem_essen_cnt_r2c._Sem_Essential_Cnt_r2c()
            return allFields

    class _sem_essen_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "SEU Essential Counter RO"
    
        def description(self):
            return "SEU Essential Counter RO of each SLR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x009+$slrid*0x100+0x09"
            
        def startAddress(self):
            return 0x00000009
            
        def endAddress(self):
            return 0x00000209

        class _Sem_Essential_Cnt_ro(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Sem_Essential_Cnt_ro"
            
            def description(self):
                return "Sem_Essential_Cnt: Counter Essential of SEM IP"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Sem_Essential_Cnt_ro"] = _AF6CNC0021_SEM._sem_essen_cnt_ro._Sem_Essential_Cnt_ro()
            return allFields

    class _sem_uncorr_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "SEU Uncorrectable Counter R2C"
    
        def description(self):
            return "SEU Uncorrectable Counter R2C of each SLR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00A+$slrid*0x100+0x0A"
            
        def startAddress(self):
            return 0x0000000a
            
        def endAddress(self):
            return 0x0000020a

        class _Sem_UnCorrectable_Cnt_r2c(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Sem_UnCorrectable_Cnt_r2c"
            
            def description(self):
                return "Sem_UnCorrectable_Cnt: Counter UnCorrectable of SEM IP"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Sem_UnCorrectable_Cnt_r2c"] = _AF6CNC0021_SEM._sem_uncorr_cnt_r2c._Sem_UnCorrectable_Cnt_r2c()
            return allFields

    class _sem_uncorr_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "SEU Uncorrectable Counter RO"
    
        def description(self):
            return "SEU Uncorrectable Counter RO of each SLR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00B+$slrid*0x100+0x0B"
            
        def startAddress(self):
            return 0x0000000b
            
        def endAddress(self):
            return 0x0000020b

        class _Sem_UnCorrectable_Cnt_ro(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Sem_UnCorrectable_Cnt_ro"
            
            def description(self):
                return "Sem_UnCorrectable_Cnt: Counter UnCorrectable of SEM IP"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Sem_UnCorrectable_Cnt_ro"] = _AF6CNC0021_SEM._sem_uncorr_cnt_ro._Sem_UnCorrectable_Cnt_ro()
            return allFields

    class _sem_corr_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "SEU Correctable Counter R2C"
    
        def description(self):
            return "SEU Correctable Counter R2C of each SLR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00C+$slrid*0x100+0x0C"
            
        def startAddress(self):
            return 0x0000000c
            
        def endAddress(self):
            return 0x0000020c

        class _Sem_Correctable_Cnt_r2c(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Sem_Correctable_Cnt_r2c"
            
            def description(self):
                return "Sem_Correctable_Cnt: Counter Correctable of SEM IP"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Sem_Correctable_Cnt_r2c"] = _AF6CNC0021_SEM._sem_corr_cnt_r2c._Sem_Correctable_Cnt_r2c()
            return allFields

    class _sem_corr_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "SEU Correctable Counter RO"
    
        def description(self):
            return "SEU Correctable Counter RO of each SLR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00D+$slrid*0x100+0x0D"
            
        def startAddress(self):
            return 0x0000000d
            
        def endAddress(self):
            return 0x0000020d

        class _Sem_Correctable_Cnt_ro(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Sem_Correctable_Cnt_ro"
            
            def description(self):
                return "Sem_Correctable_Cnt: Counter Correctable of SEM IP"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Sem_Correctable_Cnt_ro"] = _AF6CNC0021_SEM._sem_corr_cnt_ro._Sem_Correctable_Cnt_ro()
            return allFields

    class _sem_slrid(AtRegister.AtRegister):
        def name(self):
            return "SEM SLRID"
    
        def description(self):
            return "Read HW SLR of each SLR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x010+$slrid*0x100+0x10"
            
        def startAddress(self):
            return 0x00000010
            
        def endAddress(self):
            return 0x00000210

        class _slrid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "slrid"
            
            def description(self):
                return "HW SLR ID"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["slrid"] = _AF6CNC0021_SEM._sem_slrid._slrid()
            return allFields

    class _sem_mon_sta(AtRegister.AtRegister):
        def name(self):
            return "SEM Monitor Interface Status FiFo TX Side"
    
        def description(self):
            return "Monitor Interface bus at TX side of each SLR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return "0x011+$slrid*0x100+0x11"
            
        def startAddress(self):
            return 0x00000011
            
        def endAddress(self):
            return 0x00000211

        class _mon_tx_ctrl_trig(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "mon_tx_ctrl_trig"
            
            def description(self):
                return "Trigger 0->1 to start reseting FiFo"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _mon_tx_eful_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "mon_tx_eful_stk"
            
            def description(self):
                return "Sticky Commnon TX fifo is early full"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _mon_tx_full_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "mon_tx_full_stk"
            
            def description(self):
                return "Sticky Common TX fifo is full"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _mon_tx_ept_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "mon_tx_ept_stk"
            
            def description(self):
                return "Sticky Common TX fifo is empty"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _mon_tx_nept_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "mon_tx_nept_stk"
            
            def description(self):
                return "Sticky Common TX fifo is not empty"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _mon_tx_eful_cur(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "mon_tx_eful_cur"
            
            def description(self):
                return "Current Commnon TX fifo is early full"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _mon_tx_full_cur(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "mon_tx_full_cur"
            
            def description(self):
                return "Current Common TX fifo is full"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _mon_tx_ept_cur(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "mon_tx_ept_cur"
            
            def description(self):
                return "Current Common TX fifo is empty"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _mon_tx_nept_cur(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "mon_tx_nept_cur"
            
            def description(self):
                return "Current Common TX fifo is not empty"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _mon_tx_len_cur(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mon_tx_len_cur"
            
            def description(self):
                return "Current length of Common fifo"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mon_tx_ctrl_trig"] = _AF6CNC0021_SEM._sem_mon_sta._mon_tx_ctrl_trig()
            allFields["mon_tx_eful_stk"] = _AF6CNC0021_SEM._sem_mon_sta._mon_tx_eful_stk()
            allFields["mon_tx_full_stk"] = _AF6CNC0021_SEM._sem_mon_sta._mon_tx_full_stk()
            allFields["mon_tx_ept_stk"] = _AF6CNC0021_SEM._sem_mon_sta._mon_tx_ept_stk()
            allFields["mon_tx_nept_stk"] = _AF6CNC0021_SEM._sem_mon_sta._mon_tx_nept_stk()
            allFields["mon_tx_eful_cur"] = _AF6CNC0021_SEM._sem_mon_sta._mon_tx_eful_cur()
            allFields["mon_tx_full_cur"] = _AF6CNC0021_SEM._sem_mon_sta._mon_tx_full_cur()
            allFields["mon_tx_ept_cur"] = _AF6CNC0021_SEM._sem_mon_sta._mon_tx_ept_cur()
            allFields["mon_tx_nept_cur"] = _AF6CNC0021_SEM._sem_mon_sta._mon_tx_nept_cur()
            allFields["mon_tx_len_cur"] = _AF6CNC0021_SEM._sem_mon_sta._mon_tx_len_cur()
            return allFields

    class _sem_mon_val(AtRegister.AtRegister):
        def name(self):
            return "SEM Monitor Interface Value FiFo TX Side"
    
        def description(self):
            return "Monitor Interface bus at TX side of each SLR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x012+$slrid*0x100+0x12"
            
        def startAddress(self):
            return 0x00000012
            
        def endAddress(self):
            return 0x00000212

        class _mon_tx_val(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mon_tx_val"
            
            def description(self):
                return "Status of SEM controller, ASCII format, you have to monitor status of FiFo TX to get valid value"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mon_tx_val"] = _AF6CNC0021_SEM._sem_mon_val._mon_tx_val()
            return allFields

    class _sem_mon_ctrl(AtRegister.AtRegister):
        def name(self):
            return "SEM Monitor Interface Control RX Side"
    
        def description(self):
            return "Monitor Interface bus at RX side of each SLR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return "0x013+$slrid*0x100+0x13"
            
        def startAddress(self):
            return 0x00000013
            
        def endAddress(self):
            return 0x00000213

        class _mon_rx_ctrl_done(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "mon_rx_ctrl_done"
            
            def description(self):
                return "Trigger is done"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _mon_rx_ctrl_trig(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "mon_rx_ctrl_trig"
            
            def description(self):
                return "Trigger 1->0 to HW start sending command to SEM controller via Monitor interface"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _mon_rx_ctrl_len(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mon_rx_ctrl_len"
            
            def description(self):
                return "Command length"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mon_rx_ctrl_done"] = _AF6CNC0021_SEM._sem_mon_ctrl._mon_rx_ctrl_done()
            allFields["mon_rx_ctrl_trig"] = _AF6CNC0021_SEM._sem_mon_ctrl._mon_rx_ctrl_trig()
            allFields["mon_rx_ctrl_len"] = _AF6CNC0021_SEM._sem_mon_ctrl._mon_rx_ctrl_len()
            return allFields

    class _sem_mon_cmd_part0(AtRegister.AtRegister):
        def name(self):
            return "SEM Monitor Interface Command Value Part0 RX Side"
    
        def description(self):
            return "Monitor Interface bus at RX side of each SLR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x014+$slrid*0x100+0x14"
            
        def startAddress(self):
            return 0x00000014
            
        def endAddress(self):
            return 0x00000214

        class _mon_rx_cmd_val0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mon_rx_cmd_val0"
            
            def description(self):
                return "Command value part0, this is MSB, ASCII format"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mon_rx_cmd_val0"] = _AF6CNC0021_SEM._sem_mon_cmd_part0._mon_rx_cmd_val0()
            return allFields

    class _sem_mon_cmd_part1(AtRegister.AtRegister):
        def name(self):
            return "SEM Monitor Interface Command Value Part1 RX Side"
    
        def description(self):
            return "Monitor Interface bus at RX side of each SLR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x015+$slrid*0x100+0x15"
            
        def startAddress(self):
            return 0x00000015
            
        def endAddress(self):
            return 0x00000215

        class _mon_rx_cmd_val1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mon_rx_cmd_val1"
            
            def description(self):
                return "Command value part1, it is followed Part0, ASCII format"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mon_rx_cmd_val1"] = _AF6CNC0021_SEM._sem_mon_cmd_part1._mon_rx_cmd_val1()
            return allFields

    class _sem_mon_cmd_part2(AtRegister.AtRegister):
        def name(self):
            return "SEM Monitor Interface Command Value Part2 RX Side"
    
        def description(self):
            return "Monitor Interface bus at RX side of each SLR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x016+$slrid*0x100+0x16"
            
        def startAddress(self):
            return 0x00000016
            
        def endAddress(self):
            return 0x00000216

        class _mon_rx_cmd_val2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mon_rx_cmd_val2"
            
            def description(self):
                return "Command value part2, it is followed Part1, ASCII format"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mon_rx_cmd_val2"] = _AF6CNC0021_SEM._sem_mon_cmd_part2._mon_rx_cmd_val2()
            return allFields

    class _sem_mon_cmd_part3(AtRegister.AtRegister):
        def name(self):
            return "SEM Monitor Interface Command Value Part3 RX Side"
    
        def description(self):
            return "Monitor Interface bus at RX side of each SLR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x017+$slrid*0x100+0x17"
            
        def startAddress(self):
            return 0x00000017
            
        def endAddress(self):
            return 0x00000217

        class _mon_rx_cmd_val3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mon_rx_cmd_val3"
            
            def description(self):
                return "Command value part3, it is followed Part2, this is LSB part, ASCII format"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mon_rx_cmd_val3"] = _AF6CNC0021_SEM._sem_mon_cmd_part3._mon_rx_cmd_val3()
            return allFields

    class _sem_hrtb_sta(AtRegister.AtRegister):
        def name(self):
            return "SEM Heartbeat Status"
    
        def description(self):
            return "Heartbeat status of each SLR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x018+$slrid*0x100+0x18"
            
        def startAddress(self):
            return 0x00000018
            
        def endAddress(self):
            return 0x00000218

        class _hrtb_err_obsv(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "hrtb_err_obsv"
            
            def description(self):
                return "Heartbeat is observation error"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _hrtb_err_diag(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "hrtb_err_diag"
            
            def description(self):
                return "Heartbeat is diagnostic error"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _hrtb_err_dete(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "hrtb_err_dete"
            
            def description(self):
                return "Heartbeat is detect-only error"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["hrtb_err_obsv"] = _AF6CNC0021_SEM._sem_hrtb_sta._hrtb_err_obsv()
            allFields["hrtb_err_diag"] = _AF6CNC0021_SEM._sem_hrtb_sta._hrtb_err_diag()
            allFields["hrtb_err_dete"] = _AF6CNC0021_SEM._sem_hrtb_sta._hrtb_err_dete()
            return allFields

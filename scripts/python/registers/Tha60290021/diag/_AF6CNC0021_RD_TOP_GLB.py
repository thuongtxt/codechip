import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0021_RD_TOP_GLB(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["ProductID"] = _AF6CNC0021_RD_TOP_GLB._ProductID()
        allRegisters["YYMMDD_VerID"] = _AF6CNC0021_RD_TOP_GLB._YYMMDD_VerID()
        allRegisters["InternalID"] = _AF6CNC0021_RD_TOP_GLB._InternalID()
        allRegisters["o_reset_ctr"] = _AF6CNC0021_RD_TOP_GLB._o_reset_ctr()
        allRegisters["o_control0"] = _AF6CNC0021_RD_TOP_GLB._o_control0()
        allRegisters["o_control1"] = _AF6CNC0021_RD_TOP_GLB._o_control1()
        allRegisters["o_control2"] = _AF6CNC0021_RD_TOP_GLB._o_control2()
        allRegisters["o_control3"] = _AF6CNC0021_RD_TOP_GLB._o_control3()
        allRegisters["o_control4"] = _AF6CNC0021_RD_TOP_GLB._o_control4()
        allRegisters["o_control5"] = _AF6CNC0021_RD_TOP_GLB._o_control5()
        allRegisters["o_control6"] = _AF6CNC0021_RD_TOP_GLB._o_control6()
        allRegisters["o_control7"] = _AF6CNC0021_RD_TOP_GLB._o_control7()
        allRegisters["o_control8"] = _AF6CNC0021_RD_TOP_GLB._o_control8()
        allRegisters["o_control9"] = _AF6CNC0021_RD_TOP_GLB._o_control9()
        allRegisters["o_control10"] = _AF6CNC0021_RD_TOP_GLB._o_control10()
        allRegisters["o_control13"] = _AF6CNC0021_RD_TOP_GLB._o_control13()
        allRegisters["o_control14"] = _AF6CNC0021_RD_TOP_GLB._o_control14()
        allRegisters["c_uart"] = _AF6CNC0021_RD_TOP_GLB._c_uart()
        allRegisters["unused"] = _AF6CNC0021_RD_TOP_GLB._unused()
        allRegisters["i_sticky0"] = _AF6CNC0021_RD_TOP_GLB._i_sticky0()
        allRegisters["i_sticky1"] = _AF6CNC0021_RD_TOP_GLB._i_sticky1()
        allRegisters["clock_mon_high"] = _AF6CNC0021_RD_TOP_GLB._clock_mon_high()
        allRegisters["clock_mon_highclock_mon_multi"] = _AF6CNC0021_RD_TOP_GLB._clock_mon_highclock_mon_multi()
        allRegisters["clock_mon_slow"] = _AF6CNC0021_RD_TOP_GLB._clock_mon_slow()
        return allRegisters

    class _ProductID(AtRegister.AtRegister):
        def name(self):
            return "Device Product ID"
    
        def description(self):
            return "This register indicates Product ID."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00f00000
            
        def endAddress(self):
            return 0xffffffff

        class _ProductID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ProductID"
            
            def description(self):
                return "ProductId"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ProductID"] = _AF6CNC0021_RD_TOP_GLB._ProductID._ProductID()
            return allFields

    class _YYMMDD_VerID(AtRegister.AtRegister):
        def name(self):
            return "Device Year Month Day Version ID"
    
        def description(self):
            return "This register indicates Year Month Day and main version ID."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00f00001
            
        def endAddress(self):
            return 0xffffffff

        class _Year(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "Year"
            
            def description(self):
                return "Year"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Month(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Month"
            
            def description(self):
                return "Month"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Day(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Day"
            
            def description(self):
                return "Day"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Version(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Version"
            
            def description(self):
                return "Version"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Year"] = _AF6CNC0021_RD_TOP_GLB._YYMMDD_VerID._Year()
            allFields["Month"] = _AF6CNC0021_RD_TOP_GLB._YYMMDD_VerID._Month()
            allFields["Day"] = _AF6CNC0021_RD_TOP_GLB._YYMMDD_VerID._Day()
            allFields["Version"] = _AF6CNC0021_RD_TOP_GLB._YYMMDD_VerID._Version()
            return allFields

    class _InternalID(AtRegister.AtRegister):
        def name(self):
            return "Device Internal ID"
    
        def description(self):
            return "This register indicates internal ID."
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00f00003
            
        def endAddress(self):
            return 0xffffffff

        class _InternalID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "InternalID"
            
            def description(self):
                return "InternalID"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["InternalID"] = _AF6CNC0021_RD_TOP_GLB._InternalID._InternalID()
            return allFields

    class _o_reset_ctr(AtRegister.AtRegister):
        def name(self):
            return "general purpose inputs for TX Uart"
    
        def description(self):
            return "This is the global configuration register for the MIG Reset Control"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x5"
            
        def startAddress(self):
            return 0x00000005
            
        def endAddress(self):
            return 0xffffffff

        class _QDR1_Reset(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "QDR1_Reset"
            
            def description(self):
                return "Reset Control for QDR#1 0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DDR4_Reset(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "DDR4_Reset"
            
            def description(self):
                return "Reset Control for DDR#4 0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DDR3_Reset(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "DDR3_Reset"
            
            def description(self):
                return "Reset Control for DDR#3 0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DDR2_Reset(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "DDR2_Reset"
            
            def description(self):
                return "Reset Control for DDR#2 0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DDR1_Reset(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DDR1_Reset"
            
            def description(self):
                return "Reset Control for DDR#1 0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["QDR1_Reset"] = _AF6CNC0021_RD_TOP_GLB._o_reset_ctr._QDR1_Reset()
            allFields["DDR4_Reset"] = _AF6CNC0021_RD_TOP_GLB._o_reset_ctr._DDR4_Reset()
            allFields["DDR3_Reset"] = _AF6CNC0021_RD_TOP_GLB._o_reset_ctr._DDR3_Reset()
            allFields["DDR2_Reset"] = _AF6CNC0021_RD_TOP_GLB._o_reset_ctr._DDR2_Reset()
            allFields["DDR1_Reset"] = _AF6CNC0021_RD_TOP_GLB._o_reset_ctr._DDR1_Reset()
            return allFields

    class _o_control0(AtRegister.AtRegister):
        def name(self):
            return "general purpose inputs for TX Uart"
    
        def description(self):
            return "This is the global configuration register for the Global Serdes Control"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x40"
            
        def startAddress(self):
            return 0x00000040
            
        def endAddress(self):
            return 0xffffffff

        class _eth_40g_tx_dup(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "eth_40g_tx_dup"
            
            def description(self):
                return "Duplicate TX for 2 eth 40G"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _fsm_eth_40g_rx_gsel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "fsm_eth_40g_rx_gsel"
            
            def description(self):
                return "global selection between external FSM-RXMAC and inernal selection"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _fsm_eth_40g_rx_isel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "fsm_eth_40g_rx_isel"
            
            def description(self):
                return "internal selection"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _fsm_card_pw_tx_gsel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "fsm_card_pw_tx_gsel"
            
            def description(self):
                return "global selection between external FSM-CARD and inernal selection"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _fsm_card_pw_tx_isel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "fsm_card_pw_tx_isel"
            
            def description(self):
                return "internal selection"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _multirate_sel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "multirate_sel"
            
            def description(self):
                return "select 2 two RX group of Multirate serdes to CORE"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _o_control0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "o_control0"
            
            def description(self):
                return "Uart TX 8-bit data out"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["eth_40g_tx_dup"] = _AF6CNC0021_RD_TOP_GLB._o_control0._eth_40g_tx_dup()
            allFields["fsm_eth_40g_rx_gsel"] = _AF6CNC0021_RD_TOP_GLB._o_control0._fsm_eth_40g_rx_gsel()
            allFields["fsm_eth_40g_rx_isel"] = _AF6CNC0021_RD_TOP_GLB._o_control0._fsm_eth_40g_rx_isel()
            allFields["fsm_card_pw_tx_gsel"] = _AF6CNC0021_RD_TOP_GLB._o_control0._fsm_card_pw_tx_gsel()
            allFields["fsm_card_pw_tx_isel"] = _AF6CNC0021_RD_TOP_GLB._o_control0._fsm_card_pw_tx_isel()
            allFields["multirate_sel"] = _AF6CNC0021_RD_TOP_GLB._o_control0._multirate_sel()
            allFields["o_control0"] = _AF6CNC0021_RD_TOP_GLB._o_control0._o_control0()
            return allFields

    class _o_control1(AtRegister.AtRegister):
        def name(self):
            return "Flow Control Mechanism for Ethernet pass-through Diagnostic Value"
    
        def description(self):
            return "This is the global configuration register for Flow Control Mechanism for Ethernet pass-through Diagnostic"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x41"
            
        def startAddress(self):
            return 0x00000041
            
        def endAddress(self):
            return 0xffffffff

        class _flowctl_gr2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "flowctl_gr2"
            
            def description(self):
                return "FIFO status of Group 2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _flowctl_gr1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "flowctl_gr1"
            
            def description(self):
                return "FIFO status of Group 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["flowctl_gr2"] = _AF6CNC0021_RD_TOP_GLB._o_control1._flowctl_gr2()
            allFields["flowctl_gr1"] = _AF6CNC0021_RD_TOP_GLB._o_control1._flowctl_gr1()
            return allFields

    class _o_control2(AtRegister.AtRegister):
        def name(self):
            return "Force Error for Flow Control Mechanism Diagnostic"
    
        def description(self):
            return "This is the global configuration Error register for Flow Control Mechanism for Ethernet pass-through Diagnostic"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x42"
            
        def startAddress(self):
            return 0x00000042
            
        def endAddress(self):
            return 0xffffffff

        class _flowctl_diagen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "flowctl_diagen"
            
            def description(self):
                return "Ethernet pass through flow Control Diagnostic Enable 0: Normal operation, select engine flow control 1: Diagnostic operation, select diagnostic flow control"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _flowctl_force(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "flowctl_force"
            
            def description(self):
                return "Force Error for Flow Control Mechanism Diagnostic 0: Release 1: Insert Error"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["flowctl_diagen"] = _AF6CNC0021_RD_TOP_GLB._o_control2._flowctl_diagen()
            allFields["flowctl_force"] = _AF6CNC0021_RD_TOP_GLB._o_control2._flowctl_force()
            return allFields

    class _o_control3(AtRegister.AtRegister):
        def name(self):
            return "Diagnostic Enable Control"
    
        def description(self):
            return "This is the global configuration register for the MIG (DDR/QDR) Diagnostic Control"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x43"
            
        def startAddress(self):
            return 0x00000043
            
        def endAddress(self):
            return 0xffffffff

        class _SFP_OCN_ETH_DiagEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "SFP_OCN_ETH_DiagEn"
            
            def description(self):
                return "Enable Daignostic for SFP_OCN_ETH #1-#16 (Per bit per port) 0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TSI_Mate_DiagEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TSI_Mate_DiagEn"
            
            def description(self):
                return "Enable Daignostic for TSI_Mate #1-#8 (Per bit per port) 0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DDR4_DiagEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "DDR4_DiagEn"
            
            def description(self):
                return "Enable Daignostic for DDR#2 0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DDR3_DiagEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "DDR3_DiagEn"
            
            def description(self):
                return "Enable Daignostic for DDR#1 0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DDR2_DiagEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "DDR2_DiagEn"
            
            def description(self):
                return "Enable Daignostic for DDR#2 0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DDR1_DiagEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "DDR1_DiagEn"
            
            def description(self):
                return "Enable Daignostic for DDR#1 0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _QDR1_DiagEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "QDR1_DiagEn"
            
            def description(self):
                return "Enable Daignostic for QDR#1 0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SFP_OCN_ETH_DiagEn"] = _AF6CNC0021_RD_TOP_GLB._o_control3._SFP_OCN_ETH_DiagEn()
            allFields["TSI_Mate_DiagEn"] = _AF6CNC0021_RD_TOP_GLB._o_control3._TSI_Mate_DiagEn()
            allFields["DDR4_DiagEn"] = _AF6CNC0021_RD_TOP_GLB._o_control3._DDR4_DiagEn()
            allFields["DDR3_DiagEn"] = _AF6CNC0021_RD_TOP_GLB._o_control3._DDR3_DiagEn()
            allFields["DDR2_DiagEn"] = _AF6CNC0021_RD_TOP_GLB._o_control3._DDR2_DiagEn()
            allFields["DDR1_DiagEn"] = _AF6CNC0021_RD_TOP_GLB._o_control3._DDR1_DiagEn()
            allFields["QDR1_DiagEn"] = _AF6CNC0021_RD_TOP_GLB._o_control3._QDR1_DiagEn()
            return allFields

    class _o_control4(AtRegister.AtRegister):
        def name(self):
            return "OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Bus Type port 1 to 8"
    
        def description(self):
            return "This is the global configuration register for the Global Serdes Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag port 1 to 8"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x44"
            
        def startAddress(self):
            return 0x00000044
            
        def endAddress(self):
            return 0xffffffff

        class _Bus_Type_Sel8(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 28
        
            def name(self):
                return "Bus_Type_Sel8"
            
            def description(self):
                return "Configure Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 8 0: OC192 1: XFI 2: OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31 8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Bus_Type_Sel7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 24
        
            def name(self):
                return "Bus_Type_Sel7"
            
            def description(self):
                return "Configure Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 7 0: unused 1: unused 2: OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31 8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Bus_Type_Sel6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 20
        
            def name(self):
                return "Bus_Type_Sel6"
            
            def description(self):
                return "Configure Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 6 0: unused 1: unused 2: OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31 8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Bus_Type_Sel5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Bus_Type_Sel5"
            
            def description(self):
                return "Configure Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 5 0: unused 1: unused 2: OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31 8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Bus_Type_Sel4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Bus_Type_Sel4"
            
            def description(self):
                return "Configure Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 4 0: unused 1: unused 2: OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31 8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Bus_Type_Sel3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Bus_Type_Sel3"
            
            def description(self):
                return "Configure Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 3 0: unused 1: unused 2: OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31 8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Bus_Type_Sel2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Bus_Type_Sel2"
            
            def description(self):
                return "Configure Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 2 0: unused 1: unused 2: OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31 8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Bus_Type_Sel1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Bus_Type_Sel1"
            
            def description(self):
                return "Configure Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 1 0: OC192 1: XFI 2: OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31 8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Bus_Type_Sel8"] = _AF6CNC0021_RD_TOP_GLB._o_control4._Bus_Type_Sel8()
            allFields["Bus_Type_Sel7"] = _AF6CNC0021_RD_TOP_GLB._o_control4._Bus_Type_Sel7()
            allFields["Bus_Type_Sel6"] = _AF6CNC0021_RD_TOP_GLB._o_control4._Bus_Type_Sel6()
            allFields["Bus_Type_Sel5"] = _AF6CNC0021_RD_TOP_GLB._o_control4._Bus_Type_Sel5()
            allFields["Bus_Type_Sel4"] = _AF6CNC0021_RD_TOP_GLB._o_control4._Bus_Type_Sel4()
            allFields["Bus_Type_Sel3"] = _AF6CNC0021_RD_TOP_GLB._o_control4._Bus_Type_Sel3()
            allFields["Bus_Type_Sel2"] = _AF6CNC0021_RD_TOP_GLB._o_control4._Bus_Type_Sel2()
            allFields["Bus_Type_Sel1"] = _AF6CNC0021_RD_TOP_GLB._o_control4._Bus_Type_Sel1()
            return allFields

    class _o_control5(AtRegister.AtRegister):
        def name(self):
            return "Raw PRSB test for OC192_OC48_OC12_10G_1G_100FX_16ch Serdes"
    
        def description(self):
            return "This is the TOP global configuration register unused"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x45"
            
        def startAddress(self):
            return 0x00000045
            
        def endAddress(self):
            return 0xffffffff

        class _prbs_ier(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "prbs_ier"
            
            def description(self):
                return "Insert Error for Raw PRBS for port 16 to port 1 (per port per bit) 1: Insert 0: Normal"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _prbs_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "prbs_en"
            
            def description(self):
                return "Enable test for Raw PRBS ( Serdes should init mode OC192 )for port 16 to port 1 (per port per bit) 1: Enable (Must config PRBS Bus_Type_Sel at  o_control4) 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prbs_ier"] = _AF6CNC0021_RD_TOP_GLB._o_control5._prbs_ier()
            allFields["prbs_en"] = _AF6CNC0021_RD_TOP_GLB._o_control5._prbs_en()
            return allFields

    class _o_control6(AtRegister.AtRegister):
        def name(self):
            return "unused"
    
        def description(self):
            return "This is the TOP global configuration registerunused"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x46"
            
        def startAddress(self):
            return 0x00000046
            
        def endAddress(self):
            return 0xffffffff

        class _Bus_Type_Sel16(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 28
        
            def name(self):
                return "Bus_Type_Sel16"
            
            def description(self):
                return "Configure Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 16 0: unused 1: unused 2: OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31 8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Bus_Type_Sel15(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 24
        
            def name(self):
                return "Bus_Type_Sel15"
            
            def description(self):
                return "Configure Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 15 0: unused 1: unused 2: OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31 8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Bus_Type_Sel14(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 20
        
            def name(self):
                return "Bus_Type_Sel14"
            
            def description(self):
                return "Configure Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 14 0: unused 1: unused 2: OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31 8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Bus_Type_Sel13(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Bus_Type_Sel13"
            
            def description(self):
                return "Configure Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 13 0: unused 1: unused 2: OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31 8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Bus_Type_Sel12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Bus_Type_Sel12"
            
            def description(self):
                return "Configure Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 12 0: unused 1: unused 2: OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31 8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Bus_Type_Sel11(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Bus_Type_Sel11"
            
            def description(self):
                return "Configure Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 11 0: unused 1: unused 2: OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31 8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Bus_Type_Sel10(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Bus_Type_Sel10"
            
            def description(self):
                return "Configure Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 10 0: unused 1: unused 2: OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31 8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Bus_Type_Sel9(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Bus_Type_Sel9"
            
            def description(self):
                return "Configure Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 9 0: unused 1: unused 2: OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31 8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Bus_Type_Sel16"] = _AF6CNC0021_RD_TOP_GLB._o_control6._Bus_Type_Sel16()
            allFields["Bus_Type_Sel15"] = _AF6CNC0021_RD_TOP_GLB._o_control6._Bus_Type_Sel15()
            allFields["Bus_Type_Sel14"] = _AF6CNC0021_RD_TOP_GLB._o_control6._Bus_Type_Sel14()
            allFields["Bus_Type_Sel13"] = _AF6CNC0021_RD_TOP_GLB._o_control6._Bus_Type_Sel13()
            allFields["Bus_Type_Sel12"] = _AF6CNC0021_RD_TOP_GLB._o_control6._Bus_Type_Sel12()
            allFields["Bus_Type_Sel11"] = _AF6CNC0021_RD_TOP_GLB._o_control6._Bus_Type_Sel11()
            allFields["Bus_Type_Sel10"] = _AF6CNC0021_RD_TOP_GLB._o_control6._Bus_Type_Sel10()
            allFields["Bus_Type_Sel9"] = _AF6CNC0021_RD_TOP_GLB._o_control6._Bus_Type_Sel9()
            return allFields

    class _o_control7(AtRegister.AtRegister):
        def name(self):
            return "8Khz clock out enable for Diagnostic"
    
        def description(self):
            return "This is the TOP global configuration register"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x47"
            
        def startAddress(self):
            return 0x00000047
            
        def endAddress(self):
            return 0xffffffff

        class _cfgref2mod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "cfgref2mod"
            
            def description(self):
                return "Configure refout2 is output from rxlclk of Faceplate_Serdeses or refout2 is PLL lock status"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgref2pid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 20
        
            def name(self):
                return "cfgref2pid"
            
            def description(self):
                return "Configure refout2 source from Faceplate_Serdeses value 0 to 15 for 16 port"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgref1pid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "cfgref1pid"
            
            def description(self):
                return "Configure refout1 source from Faceplate_Serdeses value 0 to 15 for 16 port"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgdiagen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfgdiagen"
            
            def description(self):
                return "select diag refout8k or user refout8k from core,bit per sub port,"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfgref2mod"] = _AF6CNC0021_RD_TOP_GLB._o_control7._cfgref2mod()
            allFields["cfgref2pid"] = _AF6CNC0021_RD_TOP_GLB._o_control7._cfgref2pid()
            allFields["cfgref1pid"] = _AF6CNC0021_RD_TOP_GLB._o_control7._cfgref1pid()
            allFields["cfgdiagen"] = _AF6CNC0021_RD_TOP_GLB._o_control7._cfgdiagen()
            return allFields

    class _o_control8(AtRegister.AtRegister):
        def name(self):
            return "Configure Frequency of Faceplate_Serdeses RX CLK"
    
        def description(self):
            return "This is the TOP global configuration register"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x48"
            
        def startAddress(self):
            return 0x00000048
            
        def endAddress(self):
            return 0xffffffff

        class _cfgrate(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfgrate"
            
            def description(self):
                return "Configure rate for Faceplate Serdeses RX CLK  2 bit per sub port <exp : [1:0] for port 0.....[31:30] for port 15>"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfgrate"] = _AF6CNC0021_RD_TOP_GLB._o_control8._cfgrate()
            return allFields

    class _o_control9(AtRegister.AtRegister):
        def name(self):
            return "Configure Clock monitor output"
    
        def description(self):
            return "This is the TOP global configuration register"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x49"
            
        def startAddress(self):
            return 0x00000049
            
        def endAddress(self):
            return 0xffffffff

        class _cfgrefpid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfgrefpid"
            
            def description(self):
                return "Configure Source Clock monitor output for clock_mon"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfgrefpid"] = _AF6CNC0021_RD_TOP_GLB._o_control9._cfgrefpid()
            return allFields

    class _o_control10(AtRegister.AtRegister):
        def name(self):
            return "Configure MII test enable"
    
        def description(self):
            return "This is the TOP global configuration register"
            
        def width(self):
            return 4
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return "0x4A"
            
        def startAddress(self):
            return 0x0000004a
            
        def endAddress(self):
            return 0xffffffff

        class _Channel0_Testen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Channel0_Testen"
            
            def description(self):
                return "Set 1 to enable test MII channel 0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Channel1_Testen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Channel1_Testen"
            
            def description(self):
                return "Set 1 to enable test MII channel 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Channel2_Testen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Channel2_Testen"
            
            def description(self):
                return "Set 1 to enable test MII channel 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Channel3_Testen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "Channel3_Testen"
            
            def description(self):
                return "Set 1 to enable test MII channel 3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Channel0_Testen"] = _AF6CNC0021_RD_TOP_GLB._o_control10._Channel0_Testen()
            allFields["Channel1_Testen"] = _AF6CNC0021_RD_TOP_GLB._o_control10._Channel1_Testen()
            allFields["Channel2_Testen"] = _AF6CNC0021_RD_TOP_GLB._o_control10._Channel2_Testen()
            allFields["Channel3_Testen"] = _AF6CNC0021_RD_TOP_GLB._o_control10._Channel3_Testen()
            return allFields

    class _o_control13(AtRegister.AtRegister):
        def name(self):
            return "Configure Lost of Clock Threshold For 10Ge"
    
        def description(self):
            return "This is the TOP global configuration register"
            
        def width(self):
            return 28
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return "0x4D"
            
        def startAddress(self):
            return 0x0000004d
            
        def endAddress(self):
            return 0xffffffff

        class _Lost_clock_thres_10g(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Lost_clock_thres_10g"
            
            def description(self):
                return "This value will be multiply PPM value by 156"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Lost_clock_thres_10g"] = _AF6CNC0021_RD_TOP_GLB._o_control13._Lost_clock_thres_10g()
            return allFields

    class _o_control14(AtRegister.AtRegister):
        def name(self):
            return "Configure Out of Frequency Threshold For 10Ge"
    
        def description(self):
            return "This is the TOP global configuration register"
            
        def width(self):
            return 28
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return "0x4E"
            
        def startAddress(self):
            return 0x0000004e
            
        def endAddress(self):
            return 0xffffffff

        class _Out_of_freq_thres_10g(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Out_of_freq_thres_10g"
            
            def description(self):
                return "This value will be multiply PPM value by 156"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Out_of_freq_thres_10g"] = _AF6CNC0021_RD_TOP_GLB._o_control14._Out_of_freq_thres_10g()
            return allFields

    class _c_uart(AtRegister.AtRegister):
        def name(self):
            return "general purpose outputs from RX Uart"
    
        def description(self):
            return "This is value output from RX Uart"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x60"
            
        def startAddress(self):
            return 0x00000060
            
        def endAddress(self):
            return 0xffffffff

        class _fsm_rxmac(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "fsm_rxmac"
            
            def description(self):
                return "0: external FSM pin tongle rate 1Mhz to select first mac40g 1: external FSM pin tongle rate 2Mhz to select second mac40g"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _fsm_rxmac_cur(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "fsm_rxmac_cur"
            
            def description(self):
                return "0: current selected first mac40g 1: current selected second mac40g"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _fsm_txpw(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "fsm_txpw"
            
            def description(self):
                return "0: external FSM pin tongle rate 1Mhz to active card 1: external FSM pin tongle rate 2Mhz to standby card"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _fsm_txpw_cur(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "fsm_txpw_cur"
            
            def description(self):
                return "0: current card is in active state 1: current card is in standby state"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _i_status0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "i_status0"
            
            def description(self):
                return "Uart RX 8-bit data in"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["fsm_rxmac"] = _AF6CNC0021_RD_TOP_GLB._c_uart._fsm_rxmac()
            allFields["fsm_rxmac_cur"] = _AF6CNC0021_RD_TOP_GLB._c_uart._fsm_rxmac_cur()
            allFields["fsm_txpw"] = _AF6CNC0021_RD_TOP_GLB._c_uart._fsm_txpw()
            allFields["fsm_txpw_cur"] = _AF6CNC0021_RD_TOP_GLB._c_uart._fsm_txpw_cur()
            allFields["i_status0"] = _AF6CNC0021_RD_TOP_GLB._c_uart._i_status0()
            return allFields

    class _unused(AtRegister.AtRegister):
        def name(self):
            return "unused"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x61"
            
        def startAddress(self):
            return 0x00000061
            
        def endAddress(self):
            return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            return allFields

    class _i_sticky0(AtRegister.AtRegister):
        def name(self):
            return "Top Interface Alarm Sticky"
    
        def description(self):
            return "Top Interface Alarm Sticky"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000050
            
        def endAddress(self):
            return 0xffffffff

        class _ot_out(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "ot_out"
            
            def description(self):
                return "Set 1 When Over-Temperature alarm Happens."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _vbram_alarm_out(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "vbram_alarm_out"
            
            def description(self):
                return "Set 1 When VCCBRAM-sensor alarm Happens."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _vccaux_alarm_out(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "vccaux_alarm_out"
            
            def description(self):
                return "Set 1 When VCCAUX-sensor alarm Happens."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _vccint_alarm_out(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "vccint_alarm_out"
            
            def description(self):
                return "Set 1 When VCCINT-sensor alarm Happens."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _user_temp_alarm_out(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "user_temp_alarm_out"
            
            def description(self):
                return "Set 1 When Temperature-sensor alarm Happens."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _alarm_out(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "alarm_out"
            
            def description(self):
                return "Set 1 When SysMon Error Happens."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _SystemPll(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "SystemPll"
            
            def description(self):
                return "Set 1 while PLL Locked state change event happens when System PLL not locked."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _QdrUrst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "QdrUrst"
            
            def description(self):
                return "Set 1 while user Reset state Change Happens when QDR   Reset User."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Ddr44Urst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "Ddr44Urst"
            
            def description(self):
                return "Set 1 while user Reset State Change Happens When DDR#4 Reset User."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Ddr43Urst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "Ddr43Urst"
            
            def description(self):
                return "Set 1 while user Reset State Change Happens When DDR#3 Reset User."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Ddr42Urst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "Ddr42Urst"
            
            def description(self):
                return "Set 1 while user Reset State Change Happens When DDR#2 Reset User."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Ddr41Urst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Ddr41Urst"
            
            def description(self):
                return "Set 1 while user Reset State Change Happens When DDR#1 Reset User."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _QdrCalib(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "QdrCalib"
            
            def description(self):
                return "Set 1 while Calib state change event happens when QDR Calib Fail."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Ddr44Calib(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "Ddr44Calib"
            
            def description(self):
                return "Set 1 while Calib State Change Event Happens When DDR#4 Calib Fail."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Ddr43Calib(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Ddr43Calib"
            
            def description(self):
                return "Set 1 while Calib State Change Event Happens When DDR#3 Calib Fail."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Ddr42Calib(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Ddr42Calib"
            
            def description(self):
                return "Set 1 while Calib State Change Event Happens When DDR#2 Calib Fail."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Ddr41Calib(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Ddr41Calib"
            
            def description(self):
                return "Set 1 while Calib State Change Event Happens When DDR#1 Calib Fail."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ot_out"] = _AF6CNC0021_RD_TOP_GLB._i_sticky0._ot_out()
            allFields["vbram_alarm_out"] = _AF6CNC0021_RD_TOP_GLB._i_sticky0._vbram_alarm_out()
            allFields["vccaux_alarm_out"] = _AF6CNC0021_RD_TOP_GLB._i_sticky0._vccaux_alarm_out()
            allFields["vccint_alarm_out"] = _AF6CNC0021_RD_TOP_GLB._i_sticky0._vccint_alarm_out()
            allFields["user_temp_alarm_out"] = _AF6CNC0021_RD_TOP_GLB._i_sticky0._user_temp_alarm_out()
            allFields["alarm_out"] = _AF6CNC0021_RD_TOP_GLB._i_sticky0._alarm_out()
            allFields["SystemPll"] = _AF6CNC0021_RD_TOP_GLB._i_sticky0._SystemPll()
            allFields["QdrUrst"] = _AF6CNC0021_RD_TOP_GLB._i_sticky0._QdrUrst()
            allFields["Ddr44Urst"] = _AF6CNC0021_RD_TOP_GLB._i_sticky0._Ddr44Urst()
            allFields["Ddr43Urst"] = _AF6CNC0021_RD_TOP_GLB._i_sticky0._Ddr43Urst()
            allFields["Ddr42Urst"] = _AF6CNC0021_RD_TOP_GLB._i_sticky0._Ddr42Urst()
            allFields["Ddr41Urst"] = _AF6CNC0021_RD_TOP_GLB._i_sticky0._Ddr41Urst()
            allFields["QdrCalib"] = _AF6CNC0021_RD_TOP_GLB._i_sticky0._QdrCalib()
            allFields["Ddr44Calib"] = _AF6CNC0021_RD_TOP_GLB._i_sticky0._Ddr44Calib()
            allFields["Ddr43Calib"] = _AF6CNC0021_RD_TOP_GLB._i_sticky0._Ddr43Calib()
            allFields["Ddr42Calib"] = _AF6CNC0021_RD_TOP_GLB._i_sticky0._Ddr42Calib()
            allFields["Ddr41Calib"] = _AF6CNC0021_RD_TOP_GLB._i_sticky0._Ddr41Calib()
            return allFields

    class _i_sticky1(AtRegister.AtRegister):
        def name(self):
            return "Raw PRSB test for OC192_OC48_OC12_10G_1G_100FX_16ch Serdes Status"
    
        def description(self):
            return "Top Interface Alarm Sticky"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000051
            
        def endAddress(self):
            return 0xffffffff

        class _prbs_sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "prbs_sta"
            
            def description(self):
                return "Per bit Set 1 while PRBS Monitor not syn Event Happens(per port per bit)."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prbs_sta"] = _AF6CNC0021_RD_TOP_GLB._i_sticky1._prbs_sta()
            return allFields

    class _clock_mon_high(AtRegister.AtRegister):
        def name(self):
            return "Clock Monitoring Status"
    
        def description(self):
            return "0x1f : mate_txclk[7]        (155.52 Mhz) 0x1e : mate_txclk[6]        (155.52 Mhz) 0x1d : mate_txclk[5]        (155.52 Mhz) 0x1c : mate_txclk[4]        (155.52 Mhz) 0x1b : mate_txclk[3]        (155.52 Mhz) 0x1a : mate_txclk[2]        (155.52 Mhz) 0x19 : mate_txclk[1]        (155.52 Mhz) 0x18 : mate_txclk[0]        (155.52 Mhz) 0x17 : mate_rxclk[7]        (155.52 Mhz) 0x16 : mate_rxclk[6]        (155.52 Mhz) 0x15 : mate_rxclk[5]        (155.52 Mhz) 0x14 : mate_rxclk[4]        (155.52 Mhz) 0x13 : mate_rxclk[3]        (155.52 Mhz) 0x12 : mate_rxclk[2]        (155.52 Mhz) 0x11 : mate_rxclk[1]        (155.52 Mhz) 0x10 : mate_rxclk[0]        (155.52 Mhz) 0x0f : unused               (0 Mhz) 0x0e : ext_ref clock        (19.44 Mhz) 0x0d : prc_ref clock        (19.44 Mhz) 0x0c : spgmii_clk           (125 Mhz) 0x0b : kbgmii_clk           (125 Mhz) 0x0a : dccgmii_clk          (125 Mhz) 0x09 : eth_40g_1_rxclk      (312.5 Mhz) 0x08 : eth_40g_0_rxclk      (312.5 Mhz) 0x07 : eth_40g_1_rxclk      (312.5 Mhz) 0x06 : eth_40g_0_rxclk      (312.5 Mhz) 0x05 : DDR4#4 user clock    (300 Mhz) 0x04 : DDR4#3 user clock    (300 Mhz) 0x03 : DDR4#2 user clock    (300 Mhz) 0x02 : DDR4#1 user clock    (300 Mhz) 0x01 : qdr1_clk user clock  (250 Mhz) 0x00 : pcie_upclk clock     (62.5 Mhz)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x46000 + port_id"
            
        def startAddress(self):
            return 0x00046000
            
        def endAddress(self):
            return 0x0004601f

        class _i_statusx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "i_statusx"
            
            def description(self):
                return "Clock Value Monitor Change from hex format to DEC format to get Clock Value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["i_statusx"] = _AF6CNC0021_RD_TOP_GLB._clock_mon_high._i_statusx()
            return allFields

    class _clock_mon_highclock_mon_multi(AtRegister.AtRegister):
        def name(self):
            return "Clock Monitoring Status"
    
        def description(self):
            return "0x1f : serdes_multi_rate_txclk[15] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz) 0x1e : serdes_multi_rate_txclk[14] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz) 0x1d : serdes_multi_rate_txclk[13] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz) 0x1c : serdes_multi_rate_txclk[12] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz) 0x1b : serdes_multi_rate_txclk[11] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz) 0x1a : serdes_multi_rate_txclk[10] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz) 0x19 : serdes_multi_rate_txclk[9]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz) 0x18 : serdes_multi_rate_txclk[8]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz) 0x17 : serdes_multi_rate_txclk[7]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz) 0x16 : serdes_multi_rate_txclk[6]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz) 0x15 : serdes_multi_rate_txclk[5]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz) 0x14 : serdes_multi_rate_txclk[4]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz) 0x13 : serdes_multi_rate_txclk[3]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz) 0x12 : serdes_multi_rate_txclk[2]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz) 0x11 : serdes_multi_rate_txclk[1]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz) 0x10 : serdes_multi_rate_txclk[0]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz) 0x0f : serdes_multi_rate_rxclk[15] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz) 0x0e : serdes_multi_rate_rxclk[14] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz) 0x0d : serdes_multi_rate_rxclk[13] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz) 0x0c : serdes_multi_rate_rxclk[12] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz) 0x0b : serdes_multi_rate_rxclk[11] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz) 0x0a : serdes_multi_rate_rxclk[10] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz) 0x09 : serdes_multi_rate_rxclk[9]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz) 0x08 : serdes_multi_rate_rxclk[8]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz) 0x07 : serdes_multi_rate_rxclk[7]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz) 0x06 : serdes_multi_rate_rxclk[6]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz) 0x05 : serdes_multi_rate_rxclk[5]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz) 0x04 : serdes_multi_rate_rxclk[4]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz) 0x03 : serdes_multi_rate_rxclk[3]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz) 0x02 : serdes_multi_rate_rxclk[2]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz) 0x01 : serdes_multi_rate_rxclk[1]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz) 0x00 : serdes_multi_rate_rxclk[0]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x43000 + port_id"
            
        def startAddress(self):
            return 0x00043000
            
        def endAddress(self):
            return 0x0004301f

        class _i_statusx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "i_statusx"
            
            def description(self):
                return "Clock Value Monitor Change from hex format to DEC format to get Clock Value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["i_statusx"] = _AF6CNC0021_RD_TOP_GLB._clock_mon_highclock_mon_multi._i_statusx()
            return allFields

    class _clock_mon_slow(AtRegister.AtRegister):
        def name(self):
            return "Clock Monitoring Status"
    
        def description(self):
            return "0x1f : unused (0 Mhz) 0x1e : unused (0 Mhz) 0x1d : pm_tick              (1 Hz) for get value frequency SW must use : F = 155520000/register value 0x1c : one_pps              (1 Hz) for get value frequency SW must use : F = 155520000/register value 0x1b : unused (0 Mhz) 0x1a : unused (0 Mhz) 0x19 : unused (0 Mhz) 0x18 : unused (0 Mhz) 0x17 : unused (0 Mhz) 0x16 : unused (0 Mhz) 0x15 : refout8k_2 (0 Mhz) 0x14 : refout8k_1 (0 Mhz) 0x13 : fsm_pcp[2]			 (1 Mhz or 2 Mhz  frequency clk Diagnostic) 0x12 : fsm_pcp[1]			 (1 Mhz or 2 Mhz  frequency clk Diagnostic) 0x11 : fsm_pcp[0]			 (1 Mhz or 2 Mhz  frequency clk Diagnostic) 0x10 : spare_gpio[13]       (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN) 0x0f : spare_gpio[12]       (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN) 0x0e : spare_gpio[11]       (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN) 0x0d : spare_gpio[10]       (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN) 0x0c : spare_gpio[9]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN) 0x0b : spare_gpio[8]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN) 0x0a : spare_gpio[7]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN) 0x09 : spare_gpio[6]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN) 0x08 : spare_gpio[5]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN) 0x07 : spare_gpio[4]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN) 0x06 : spare_gpio[3]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN) 0x05 : spare_gpio[2]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN) 0x04 : spare_gpio[1]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN) 0x03 : spare_gpio[0]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN) 0x02 : spare_clk[3]         (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN) 0x01 : spare_clk[2]         (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN) 0x00 : spare_clk0 clock     (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x47000 + port_id"
            
        def startAddress(self):
            return 0x00047000
            
        def endAddress(self):
            return 0x0004701f

        class _i_statusx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "i_statusx"
            
            def description(self):
                return "Clock Value Monitor Change from hex format to DEC format to get Clock Value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["i_statusx"] = _AF6CNC0021_RD_TOP_GLB._clock_mon_slow._i_statusx()
            return allFields

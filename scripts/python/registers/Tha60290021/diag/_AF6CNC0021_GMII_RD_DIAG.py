import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0021_GMII_RD_DIAG(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["control_pen9"] = _AF6CNC0021_GMII_RD_DIAG._control_pen9()
        allRegisters["control_pen1"] = _AF6CNC0021_GMII_RD_DIAG._control_pen1()
        allRegisters["control_pen2"] = _AF6CNC0021_GMII_RD_DIAG._control_pen2()
        allRegisters["Alarm_sticky"] = _AF6CNC0021_GMII_RD_DIAG._Alarm_sticky()
        allRegisters["Alarm_status"] = _AF6CNC0021_GMII_RD_DIAG._Alarm_status()
        allRegisters["control_pen3"] = _AF6CNC0021_GMII_RD_DIAG._control_pen3()
        allRegisters["control_pen4"] = _AF6CNC0021_GMII_RD_DIAG._control_pen4()
        allRegisters["control_pen5"] = _AF6CNC0021_GMII_RD_DIAG._control_pen5()
        allRegisters["control_pen6"] = _AF6CNC0021_GMII_RD_DIAG._control_pen6()
        allRegisters["control_pen7"] = _AF6CNC0021_GMII_RD_DIAG._control_pen7()
        allRegisters["control_pen8"] = _AF6CNC0021_GMII_RD_DIAG._control_pen8()
        allRegisters["counter1"] = _AF6CNC0021_GMII_RD_DIAG._counter1()
        allRegisters["counter2"] = _AF6CNC0021_GMII_RD_DIAG._counter2()
        allRegisters["counter3"] = _AF6CNC0021_GMII_RD_DIAG._counter3()
        allRegisters["counter8"] = _AF6CNC0021_GMII_RD_DIAG._counter8()
        allRegisters["counter4"] = _AF6CNC0021_GMII_RD_DIAG._counter4()
        allRegisters["counter5"] = _AF6CNC0021_GMII_RD_DIAG._counter5()
        allRegisters["counter6"] = _AF6CNC0021_GMII_RD_DIAG._counter6()
        allRegisters["counter7"] = _AF6CNC0021_GMII_RD_DIAG._counter7()
        return allRegisters

    class _control_pen9(AtRegister.AtRegister):
        def name(self):
            return "MII Test Enable."
    
        def description(self):
            return "Register to enable test MII interface"
            
        def width(self):
            return 4
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return "0x302"
            
        def startAddress(self):
            return 0x00000302
            
        def endAddress(self):
            return 0xffffffff

        class _Channel0_Testen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Channel0_Testen"
            
            def description(self):
                return "Set 1 to enable test MII channel 0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Channel1_Testen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Channel1_Testen"
            
            def description(self):
                return "Set 1 to enable test MII channel 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Channel2_Testen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Channel2_Testen"
            
            def description(self):
                return "Set 1 to enable test MII channel 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Channel3_Testen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "Channel3_Testen"
            
            def description(self):
                return "Set 1 to enable test MII channel 3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Channel0_Testen"] = _AF6CNC0021_GMII_RD_DIAG._control_pen9._Channel0_Testen()
            allFields["Channel1_Testen"] = _AF6CNC0021_GMII_RD_DIAG._control_pen9._Channel1_Testen()
            allFields["Channel2_Testen"] = _AF6CNC0021_GMII_RD_DIAG._control_pen9._Channel2_Testen()
            allFields["Channel3_Testen"] = _AF6CNC0021_GMII_RD_DIAG._control_pen9._Channel3_Testen()
            return allFields

    class _control_pen1(AtRegister.AtRegister):
        def name(self):
            return "MII Channels Enable."
    
        def description(self):
            return "Register to test MII interface"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return "0x00 + channel_id*16"
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0x00000030

        class _Test_enable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Test_enable"
            
            def description(self):
                return "Set 1 to enable Tx test generator"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _start_gatetime_diag(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "start_gatetime_diag"
            
            def description(self):
                return "Config start Diagnostic trigger 0 to 1 for Start auto run with Gatetime Configuration"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _status_gatetime_diag(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "status_gatetime_diag"
            
            def description(self):
                return "Status Gatetime diagnostic 1:Running 0:Done-Ready"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Unsed(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Unsed"
            
            def description(self):
                return "Unsed"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _time_cfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 15
        
            def name(self):
                return "time_cfg"
            
            def description(self):
                return "Gatetime Configuration 1-86400 second"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Test_enable"] = _AF6CNC0021_GMII_RD_DIAG._control_pen1._Test_enable()
            allFields["start_gatetime_diag"] = _AF6CNC0021_GMII_RD_DIAG._control_pen1._start_gatetime_diag()
            allFields["status_gatetime_diag"] = _AF6CNC0021_GMII_RD_DIAG._control_pen1._status_gatetime_diag()
            allFields["Unsed"] = _AF6CNC0021_GMII_RD_DIAG._control_pen1._Unsed()
            allFields["time_cfg"] = _AF6CNC0021_GMII_RD_DIAG._control_pen1._time_cfg()
            return allFields

    class _control_pen2(AtRegister.AtRegister):
        def name(self):
            return "MII Burst test enable gen packet."
    
        def description(self):
            return "Register to test MII interface"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return "0x0f + channel_id*16"
            
        def startAddress(self):
            return 0x0000000f
            
        def endAddress(self):
            return 0x0000003f

        class _Burst_gen_packet_number(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Burst_gen_packet_number"
            
            def description(self):
                return "Burst gen packet number, must configure burst number > 0 to generator packet, hardware will clear number packet configure when finish."
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Burst_gen_packet_number"] = _AF6CNC0021_GMII_RD_DIAG._control_pen2._Burst_gen_packet_number()
            return allFields

    class _Alarm_sticky(AtRegister.AtRegister):
        def name(self):
            return "MII Channels Sticky alarm."
    
        def description(self):
            return "Register to test MII interface"
            
        def width(self):
            return 32
        
        def type(self):
            return "Sticky"
            
        def fomular(self):
            return "0x01 + channel_id*16"
            
        def startAddress(self):
            return 0x00000001
            
        def endAddress(self):
            return 0x00000031

        class _Mon_packet_error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Mon_packet_error"
            
            def description(self):
                return "Set 1 when monitor packet error."
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _Mon_packet_len_error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Mon_packet_len_error"
            
            def description(self):
                return "Set 1 when monitor packet len error."
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _Mon_FCS_error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Mon_FCS_error"
            
            def description(self):
                return "Set 1 when monitor FCS error."
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _Mon_data_error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "Mon_data_error"
            
            def description(self):
                return "Set 1 when monitor data error."
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _Mon_MAC_header_error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Mon_MAC_header_error"
            
            def description(self):
                return "Set 1 when monitor MAC header error."
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _Mon_Data_sync(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Mon_Data_sync"
            
            def description(self):
                return "Set 1 when monitor Data sync."
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Mon_packet_error"] = _AF6CNC0021_GMII_RD_DIAG._Alarm_sticky._Mon_packet_error()
            allFields["Mon_packet_len_error"] = _AF6CNC0021_GMII_RD_DIAG._Alarm_sticky._Mon_packet_len_error()
            allFields["Mon_FCS_error"] = _AF6CNC0021_GMII_RD_DIAG._Alarm_sticky._Mon_FCS_error()
            allFields["Mon_data_error"] = _AF6CNC0021_GMII_RD_DIAG._Alarm_sticky._Mon_data_error()
            allFields["Mon_MAC_header_error"] = _AF6CNC0021_GMII_RD_DIAG._Alarm_sticky._Mon_MAC_header_error()
            allFields["Mon_Data_sync"] = _AF6CNC0021_GMII_RD_DIAG._Alarm_sticky._Mon_Data_sync()
            return allFields

    class _Alarm_status(AtRegister.AtRegister):
        def name(self):
            return "MII Channels Status alarm."
    
        def description(self):
            return "Register to test MII interface"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0E + channel_id*16"
            
        def startAddress(self):
            return 0x0000000e
            
        def endAddress(self):
            return 0x0000003e

        class _Mon_packet_error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Mon_packet_error"
            
            def description(self):
                return "Set 1 when monitor packet error."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Mon_packet_len_error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Mon_packet_len_error"
            
            def description(self):
                return "Set 1 when monitor packet len error."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Mon_FCS_error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Mon_FCS_error"
            
            def description(self):
                return "Set 1 when monitor FCS error."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Mon_data_error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "Mon_data_error"
            
            def description(self):
                return "Set 1 when monitor data error."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Mon_MAC_header_error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Mon_MAC_header_error"
            
            def description(self):
                return "Set 1 when monitor MAC header error."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Mon_Data_sync(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Mon_Data_sync"
            
            def description(self):
                return "Set 1 when monitor Data sync."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _currert_gatetime_diag(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 7
        
            def name(self):
                return "currert_gatetime_diag"
            
            def description(self):
                return "Current running time of Gatetime diagnostic"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Mon_packet_error"] = _AF6CNC0021_GMII_RD_DIAG._Alarm_status._Mon_packet_error()
            allFields["Mon_packet_len_error"] = _AF6CNC0021_GMII_RD_DIAG._Alarm_status._Mon_packet_len_error()
            allFields["Mon_FCS_error"] = _AF6CNC0021_GMII_RD_DIAG._Alarm_status._Mon_FCS_error()
            allFields["Mon_data_error"] = _AF6CNC0021_GMII_RD_DIAG._Alarm_status._Mon_data_error()
            allFields["Mon_MAC_header_error"] = _AF6CNC0021_GMII_RD_DIAG._Alarm_status._Mon_MAC_header_error()
            allFields["Mon_Data_sync"] = _AF6CNC0021_GMII_RD_DIAG._Alarm_status._Mon_Data_sync()
            allFields["currert_gatetime_diag"] = _AF6CNC0021_GMII_RD_DIAG._Alarm_status._currert_gatetime_diag()
            return allFields

    class _control_pen3(AtRegister.AtRegister):
        def name(self):
            return "MII Channels Test mode control."
    
        def description(self):
            return "Register to test MII interface"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return "0x02 + channel_id*16"
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0x00000032

        class _Loopout_enable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Loopout_enable"
            
            def description(self):
                return "FPGA loopout enable: 0/1 -> Dis/En."
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Mac_header_check_enable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Mac_header_check_enable"
            
            def description(self):
                return "Mac header check enable: 0/1 -> Dis/En."
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Configure_test_mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Configure_test_mode"
            
            def description(self):
                return "Configure test mode: 0/1 -> Continues/Burst."
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Invert_FCS_enable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "Invert_FCS_enable"
            
            def description(self):
                return "Invert FCS enable: 0/1 -> Dis/En."
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Force_error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Force_error"
            
            def description(self):
                return "Force error: 0/1/2/3 -> Normal/Data/Force FCS/Force Framer."
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Band_width(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Band_width"
            
            def description(self):
                return "Bandwidth: 0/1/2/3 -> 100%/90%/80%/70%."
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Data_mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Data_mode"
            
            def description(self):
                return "Data mode: 0:PRBS7 1:PRBS15 2:PRBS23 3:PRBS31 4:FIX"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Fix_data_value(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Fix_data_value"
            
            def description(self):
                return "Fix data value."
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Payload_size(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 20
        
            def name(self):
                return "Payload_size"
            
            def description(self):
                return "Payload size."
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Loopout_enable"] = _AF6CNC0021_GMII_RD_DIAG._control_pen3._Loopout_enable()
            allFields["Mac_header_check_enable"] = _AF6CNC0021_GMII_RD_DIAG._control_pen3._Mac_header_check_enable()
            allFields["Configure_test_mode"] = _AF6CNC0021_GMII_RD_DIAG._control_pen3._Configure_test_mode()
            allFields["Invert_FCS_enable"] = _AF6CNC0021_GMII_RD_DIAG._control_pen3._Invert_FCS_enable()
            allFields["Force_error"] = _AF6CNC0021_GMII_RD_DIAG._control_pen3._Force_error()
            allFields["Band_width"] = _AF6CNC0021_GMII_RD_DIAG._control_pen3._Band_width()
            allFields["Data_mode"] = _AF6CNC0021_GMII_RD_DIAG._control_pen3._Data_mode()
            allFields["Fix_data_value"] = _AF6CNC0021_GMII_RD_DIAG._control_pen3._Fix_data_value()
            allFields["Payload_size"] = _AF6CNC0021_GMII_RD_DIAG._control_pen3._Payload_size()
            return allFields

    class _control_pen4(AtRegister.AtRegister):
        def name(self):
            return "MII Channels Configure Mac DA MSB."
    
        def description(self):
            return "Register to test MII interface"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return "0x03 + channel_id*16"
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0x00000033

        class _Mac_DA_configure_MSB(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Mac_DA_configure_MSB"
            
            def description(self):
                return "Mac DA MSB configure value"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Mac_DA_configure_MSB"] = _AF6CNC0021_GMII_RD_DIAG._control_pen4._Mac_DA_configure_MSB()
            return allFields

    class _control_pen5(AtRegister.AtRegister):
        def name(self):
            return "MII Channels Configure Mac DA LSB."
    
        def description(self):
            return "Register to test MII interface"
            
        def width(self):
            return 16
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return "0x04 + channel_id*16"
            
        def startAddress(self):
            return 0x00000004
            
        def endAddress(self):
            return 0x00000034

        class _Mac_DA_configure_LSB(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Mac_DA_configure_LSB"
            
            def description(self):
                return "Mac DA LSB configure value"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Mac_DA_configure_LSB"] = _AF6CNC0021_GMII_RD_DIAG._control_pen5._Mac_DA_configure_LSB()
            return allFields

    class _control_pen6(AtRegister.AtRegister):
        def name(self):
            return "MII Channels Configure Mac SA MSB."
    
        def description(self):
            return "Register to test MII interface"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return "0x05 + channel_id*16"
            
        def startAddress(self):
            return 0x00000005
            
        def endAddress(self):
            return 0x00000035

        class _Mac_SA_configure_MSB(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Mac_SA_configure_MSB"
            
            def description(self):
                return "Mac SA MSB configure value"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Mac_SA_configure_MSB"] = _AF6CNC0021_GMII_RD_DIAG._control_pen6._Mac_SA_configure_MSB()
            return allFields

    class _control_pen7(AtRegister.AtRegister):
        def name(self):
            return "MII Channels Configure Mac SA LSB."
    
        def description(self):
            return "Register to test MII interface"
            
        def width(self):
            return 16
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return "0x06 + channel_id*16"
            
        def startAddress(self):
            return 0x00000006
            
        def endAddress(self):
            return 0x00000036

        class _Mac_SA_configure_LSB(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Mac_SA_configure_LSB"
            
            def description(self):
                return "Mac SA LSB configure value"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Mac_SA_configure_LSB"] = _AF6CNC0021_GMII_RD_DIAG._control_pen7._Mac_SA_configure_LSB()
            return allFields

    class _control_pen8(AtRegister.AtRegister):
        def name(self):
            return "MII Channels Configure Mac Type."
    
        def description(self):
            return "Register to test MII interface"
            
        def width(self):
            return 16
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return "0x07 + channel_id*16"
            
        def startAddress(self):
            return 0x00000007
            
        def endAddress(self):
            return 0x00000037

        class _Mac_Type_configure(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Mac_Type_configure"
            
            def description(self):
                return "Mac Type configure value"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Mac_Type_configure"] = _AF6CNC0021_GMII_RD_DIAG._control_pen8._Mac_Type_configure()
            return allFields

    class _counter1(AtRegister.AtRegister):
        def name(self):
            return "MII Tx counter R2C."
    
        def description(self):
            return "Register to test MII interface"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x08 + channel_id*16"
            
        def startAddress(self):
            return 0x00000008
            
        def endAddress(self):
            return 0x00000038

        class _Tx_counter_R2C(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Tx_counter_R2C"
            
            def description(self):
                return "Tx counter R2C"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Tx_counter_R2C"] = _AF6CNC0021_GMII_RD_DIAG._counter1._Tx_counter_R2C()
            return allFields

    class _counter2(AtRegister.AtRegister):
        def name(self):
            return "MII Rx counter R2C."
    
        def description(self):
            return "Register to test MII interface"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x09 + channel_id*16"
            
        def startAddress(self):
            return 0x00000009
            
        def endAddress(self):
            return 0x00000039

        class _Rx_counter_R2C(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Rx_counter_R2C"
            
            def description(self):
                return "Rx counter R2C"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Rx_counter_R2C"] = _AF6CNC0021_GMII_RD_DIAG._counter2._Rx_counter_R2C()
            return allFields

    class _counter3(AtRegister.AtRegister):
        def name(self):
            return "MII FCS error counter R2C."
    
        def description(self):
            return "Register to test MII interface"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x0A + channel_id*16"
            
        def startAddress(self):
            return 0x0000000a
            
        def endAddress(self):
            return 0x0000003a

        class _FCS_error_counter_R2C(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FCS_error_counter_R2C"
            
            def description(self):
                return "FCS error counter R2C"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FCS_error_counter_R2C"] = _AF6CNC0021_GMII_RD_DIAG._counter3._FCS_error_counter_R2C()
            return allFields

    class _counter8(AtRegister.AtRegister):
        def name(self):
            return "MII PRBS error counter R2C."
    
        def description(self):
            return "Register to test MII interface"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x401 + channel_id*16"
            
        def startAddress(self):
            return 0x00000401
            
        def endAddress(self):
            return 0x00000431

        class _PRBS_error_Counter_R2C(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PRBS_error_Counter_R2C"
            
            def description(self):
                return "PRBS error counter R2C"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PRBS_error_Counter_R2C"] = _AF6CNC0021_GMII_RD_DIAG._counter8._PRBS_error_Counter_R2C()
            return allFields

    class _counter4(AtRegister.AtRegister):
        def name(self):
            return "MII Tx counter RO."
    
        def description(self):
            return "Register to test MII interface"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x0B + channel_id*16"
            
        def startAddress(self):
            return 0x0000000b
            
        def endAddress(self):
            return 0x0000003b

        class _Tx_counter_RO(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Tx_counter_RO"
            
            def description(self):
                return "Tx counter RO"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Tx_counter_RO"] = _AF6CNC0021_GMII_RD_DIAG._counter4._Tx_counter_RO()
            return allFields

    class _counter5(AtRegister.AtRegister):
        def name(self):
            return "MII Rx counter RO."
    
        def description(self):
            return "Register to test MII interface"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x0C + channel_id*16"
            
        def startAddress(self):
            return 0x0000000c
            
        def endAddress(self):
            return 0x0000003c

        class _Rx_counter_RO(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Rx_counter_RO"
            
            def description(self):
                return "Rx counter RO"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Rx_counter_RO"] = _AF6CNC0021_GMII_RD_DIAG._counter5._Rx_counter_RO()
            return allFields

    class _counter6(AtRegister.AtRegister):
        def name(self):
            return "MII FCS error counter RO."
    
        def description(self):
            return "Register to test MII interface"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x0D + channel_id*16"
            
        def startAddress(self):
            return 0x0000000d
            
        def endAddress(self):
            return 0x0000003d

        class _FCS_error_counter_RO(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FCS_error_counter_RO"
            
            def description(self):
                return "FCS error counter RO"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FCS_error_counter_RO"] = _AF6CNC0021_GMII_RD_DIAG._counter6._FCS_error_counter_RO()
            return allFields

    class _counter7(AtRegister.AtRegister):
        def name(self):
            return "MII PRBS error counter RO."
    
        def description(self):
            return "Register to test MII interface"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x400 + channel_id*16"
            
        def startAddress(self):
            return 0x00000400
            
        def endAddress(self):
            return 0x00000430

        class _PRBS_error_Counter_RO(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PRBS_error_Counter_RO"
            
            def description(self):
                return "PRBS error counter RO"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PRBS_error_Counter_RO"] = _AF6CNC0021_GMII_RD_DIAG._counter7._PRBS_error_Counter_RO()
            return allFields

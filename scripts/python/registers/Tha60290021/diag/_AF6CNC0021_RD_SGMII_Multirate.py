import AtRegister

class _AF6CNC0021_RD_SGMII_Multirate(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["version_pen"] = _AF6CNC0021_RD_SGMII_Multirate._version_pen()
        allRegisters["an_mod_pen0"] = _AF6CNC0021_RD_SGMII_Multirate._an_mod_pen0()
        allRegisters["an_rst_pen0"] = _AF6CNC0021_RD_SGMII_Multirate._an_rst_pen0()
        allRegisters["an_enb_pen0"] = _AF6CNC0021_RD_SGMII_Multirate._an_enb_pen0()
        allRegisters["an_spd_pen0"] = _AF6CNC0021_RD_SGMII_Multirate._an_spd_pen0()
        allRegisters["an_sta_pen00"] = _AF6CNC0021_RD_SGMII_Multirate._an_sta_pen00()
        allRegisters["an_sta_pen01"] = _AF6CNC0021_RD_SGMII_Multirate._an_sta_pen01()
        allRegisters["lnk_sync_pen0"] = _AF6CNC0021_RD_SGMII_Multirate._lnk_sync_pen0()
        allRegisters["an_rxab_pen00"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen00()
        allRegisters["an_rxab_pen01"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen01()
        allRegisters["an_rxab_pen02"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen02()
        allRegisters["an_rxab_pen03"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen03()
        allRegisters["an_rxab_pen04"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen04()
        allRegisters["an_rxab_pen05"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen05()
        allRegisters["an_rxab_pen06"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen06()
        allRegisters["an_rxab_pen07"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen07()
        allRegisters["intmsk_grp0_pen"] = _AF6CNC0021_RD_SGMII_Multirate._intmsk_grp0_pen()
        allRegisters["intsta_grp0_pen"] = _AF6CNC0021_RD_SGMII_Multirate._intsta_grp0_pen()
        allRegisters["intstk_status_grp0_pen"] = _AF6CNC0021_RD_SGMII_Multirate._intstk_status_grp0_pen()
        return allRegisters

    class _version_pen(AtRegister.AtRegister):
        def name(self):
            return "Pedit Block Version"
    
        def description(self):
            return "Pedit Block Version"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _day(AtRegister.AtRegisterField):
            def startBit(self):
                return 31
                
            def stopBit(self):
                return 24
        
            def name(self):
                return "day"
            
            def description(self):
                return "day"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _month(AtRegister.AtRegisterField):
            def startBit(self):
                return 23
                
            def stopBit(self):
                return 16
        
            def name(self):
                return "month"
            
            def description(self):
                return "month"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _year(AtRegister.AtRegisterField):
            def startBit(self):
                return 15
                
            def stopBit(self):
                return 8
        
            def name(self):
                return "year"
            
            def description(self):
                return "year"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _number(AtRegister.AtRegisterField):
            def startBit(self):
                return 7
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "number"
            
            def description(self):
                return "number"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["day"] = _AF6CNC0021_RD_SGMII_Multirate._version_pen._day()
            allFields["month"] = _AF6CNC0021_RD_SGMII_Multirate._version_pen._month()
            allFields["year"] = _AF6CNC0021_RD_SGMII_Multirate._version_pen._year()
            allFields["number"] = _AF6CNC0021_RD_SGMII_Multirate._version_pen._number()
            return allFields

    class _an_mod_pen0(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg-group0 mode"
    
        def description(self):
            return "Select Auto-neg mode for 1000Basex or SGMII"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _tx_enb0(AtRegister.AtRegisterField):
            def startBit(self):
                return 31
                
            def stopBit(self):
                return 16
        
            def name(self):
                return "tx_enb0"
            
            def description(self):
                return "Enable TX side bit per port, bit[16] port 0, bit[31] port 15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _an_mod0(AtRegister.AtRegisterField):
            def startBit(self):
                return 15
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "an_mod0"
            
            def description(self):
                return "Auto-neg mode, bit per port, bit[0] port 0, bit[15] port 15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tx_enb0"] = _AF6CNC0021_RD_SGMII_Multirate._an_mod_pen0._tx_enb0()
            allFields["an_mod0"] = _AF6CNC0021_RD_SGMII_Multirate._an_mod_pen0._an_mod0()
            return allFields

    class _an_rst_pen0(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg-group0 reset"
    
        def description(self):
            return "Restart Auto-neg process of group0 from port 0 to port 15"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _an_rst0(AtRegister.AtRegisterField):
            def startBit(self):
                return 15
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "an_rst0"
            
            def description(self):
                return "Auto-neg reset, bit per port, bit[0] port 0, bit[15] port 15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["an_rst0"] = _AF6CNC0021_RD_SGMII_Multirate._an_rst_pen0._an_rst0()
            return allFields

    class _an_enb_pen0(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg-group0 enable"
    
        def description(self):
            return "enable/disbale Auto-neg of group 0 from port 0 to port 15"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _an_enb0(AtRegister.AtRegisterField):
            def startBit(self):
                return 15
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "an_enb0"
            
            def description(self):
                return "Auto-neg enb , bit per port, bit[0] port 0, bit[15] port 15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["an_enb0"] = _AF6CNC0021_RD_SGMII_Multirate._an_enb_pen0._an_enb0()
            return allFields

    class _an_spd_pen0(AtRegister.AtRegister):
        def name(self):
            return "Config_speed-group0"
    
        def description(self):
            return "configure speed when disable Auto-neg of group 0 from port 0 to 15"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_spd15(AtRegister.AtRegisterField):
            def startBit(self):
                return 31
                
            def stopBit(self):
                return 30
        
            def name(self):
                return "cfg_spd15"
            
            def description(self):
                return "Speed port 15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd14(AtRegister.AtRegisterField):
            def startBit(self):
                return 29
                
            def stopBit(self):
                return 28
        
            def name(self):
                return "cfg_spd14"
            
            def description(self):
                return "Speed port 14"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd13(AtRegister.AtRegisterField):
            def startBit(self):
                return 27
                
            def stopBit(self):
                return 26
        
            def name(self):
                return "cfg_spd13"
            
            def description(self):
                return "Speed port 13"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd12(AtRegister.AtRegisterField):
            def startBit(self):
                return 25
                
            def stopBit(self):
                return 24
        
            def name(self):
                return "cfg_spd12"
            
            def description(self):
                return "Speed port 12"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd11(AtRegister.AtRegisterField):
            def startBit(self):
                return 23
                
            def stopBit(self):
                return 22
        
            def name(self):
                return "cfg_spd11"
            
            def description(self):
                return "Speed port 11"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd10(AtRegister.AtRegisterField):
            def startBit(self):
                return 21
                
            def stopBit(self):
                return 20
        
            def name(self):
                return "cfg_spd10"
            
            def description(self):
                return "Speed port 10"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd09(AtRegister.AtRegisterField):
            def startBit(self):
                return 19
                
            def stopBit(self):
                return 18
        
            def name(self):
                return "cfg_spd09"
            
            def description(self):
                return "Speed port 9"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd08(AtRegister.AtRegisterField):
            def startBit(self):
                return 17
                
            def stopBit(self):
                return 16
        
            def name(self):
                return "cfg_spd08"
            
            def description(self):
                return "Speed port 8"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd07(AtRegister.AtRegisterField):
            def startBit(self):
                return 15
                
            def stopBit(self):
                return 14
        
            def name(self):
                return "cfg_spd07"
            
            def description(self):
                return "Speed port 7"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd06(AtRegister.AtRegisterField):
            def startBit(self):
                return 13
                
            def stopBit(self):
                return 12
        
            def name(self):
                return "cfg_spd06"
            
            def description(self):
                return "Speed port 6"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd05(AtRegister.AtRegisterField):
            def startBit(self):
                return 11
                
            def stopBit(self):
                return 10
        
            def name(self):
                return "cfg_spd05"
            
            def description(self):
                return "Speed port 5"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd04(AtRegister.AtRegisterField):
            def startBit(self):
                return 9
                
            def stopBit(self):
                return 8
        
            def name(self):
                return "cfg_spd04"
            
            def description(self):
                return "Speed port 4"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd03(AtRegister.AtRegisterField):
            def startBit(self):
                return 7
                
            def stopBit(self):
                return 6
        
            def name(self):
                return "cfg_spd03"
            
            def description(self):
                return "Speed port 3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd02(AtRegister.AtRegisterField):
            def startBit(self):
                return 5
                
            def stopBit(self):
                return 4
        
            def name(self):
                return "cfg_spd02"
            
            def description(self):
                return "Speed port 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd01(AtRegister.AtRegisterField):
            def startBit(self):
                return 3
                
            def stopBit(self):
                return 2
        
            def name(self):
                return "cfg_spd01"
            
            def description(self):
                return "Speed port 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd00(AtRegister.AtRegisterField):
            def startBit(self):
                return 1
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "cfg_spd00"
            
            def description(self):
                return "Speed port 0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_spd15"] = _AF6CNC0021_RD_SGMII_Multirate._an_spd_pen0._cfg_spd15()
            allFields["cfg_spd14"] = _AF6CNC0021_RD_SGMII_Multirate._an_spd_pen0._cfg_spd14()
            allFields["cfg_spd13"] = _AF6CNC0021_RD_SGMII_Multirate._an_spd_pen0._cfg_spd13()
            allFields["cfg_spd12"] = _AF6CNC0021_RD_SGMII_Multirate._an_spd_pen0._cfg_spd12()
            allFields["cfg_spd11"] = _AF6CNC0021_RD_SGMII_Multirate._an_spd_pen0._cfg_spd11()
            allFields["cfg_spd10"] = _AF6CNC0021_RD_SGMII_Multirate._an_spd_pen0._cfg_spd10()
            allFields["cfg_spd09"] = _AF6CNC0021_RD_SGMII_Multirate._an_spd_pen0._cfg_spd09()
            allFields["cfg_spd08"] = _AF6CNC0021_RD_SGMII_Multirate._an_spd_pen0._cfg_spd08()
            allFields["cfg_spd07"] = _AF6CNC0021_RD_SGMII_Multirate._an_spd_pen0._cfg_spd07()
            allFields["cfg_spd06"] = _AF6CNC0021_RD_SGMII_Multirate._an_spd_pen0._cfg_spd06()
            allFields["cfg_spd05"] = _AF6CNC0021_RD_SGMII_Multirate._an_spd_pen0._cfg_spd05()
            allFields["cfg_spd04"] = _AF6CNC0021_RD_SGMII_Multirate._an_spd_pen0._cfg_spd04()
            allFields["cfg_spd03"] = _AF6CNC0021_RD_SGMII_Multirate._an_spd_pen0._cfg_spd03()
            allFields["cfg_spd02"] = _AF6CNC0021_RD_SGMII_Multirate._an_spd_pen0._cfg_spd02()
            allFields["cfg_spd01"] = _AF6CNC0021_RD_SGMII_Multirate._an_spd_pen0._cfg_spd01()
            allFields["cfg_spd00"] = _AF6CNC0021_RD_SGMII_Multirate._an_spd_pen0._cfg_spd00()
            return allFields

    class _an_sta_pen00(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg-group00 status"
    
        def description(self):
            return "status of Auto-neg of group0 from port 0 to port 7"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _an_sta07(AtRegister.AtRegisterField):
            def startBit(self):
                return 31
                
            def stopBit(self):
                return 28
        
            def name(self):
                return "an_sta07"
            
            def description(self):
                return "Speed port 7"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_sta06(AtRegister.AtRegisterField):
            def startBit(self):
                return 27
                
            def stopBit(self):
                return 24
        
            def name(self):
                return "an_sta06"
            
            def description(self):
                return "Speed port 6"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_sta05(AtRegister.AtRegisterField):
            def startBit(self):
                return 23
                
            def stopBit(self):
                return 20
        
            def name(self):
                return "an_sta05"
            
            def description(self):
                return "Speed port 5"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_sta04(AtRegister.AtRegisterField):
            def startBit(self):
                return 19
                
            def stopBit(self):
                return 16
        
            def name(self):
                return "an_sta04"
            
            def description(self):
                return "Speed port 4"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_sta03(AtRegister.AtRegisterField):
            def startBit(self):
                return 15
                
            def stopBit(self):
                return 12
        
            def name(self):
                return "an_sta03"
            
            def description(self):
                return "Speed port 3"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_sta02(AtRegister.AtRegisterField):
            def startBit(self):
                return 11
                
            def stopBit(self):
                return 8
        
            def name(self):
                return "an_sta02"
            
            def description(self):
                return "Speed port 2"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_sta01(AtRegister.AtRegisterField):
            def startBit(self):
                return 7
                
            def stopBit(self):
                return 4
        
            def name(self):
                return "an_sta01"
            
            def description(self):
                return "Speed port 1"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_sta00(AtRegister.AtRegisterField):
            def startBit(self):
                return 3
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "an_sta00"
            
            def description(self):
                return "Speed port 0"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["an_sta07"] = _AF6CNC0021_RD_SGMII_Multirate._an_sta_pen00._an_sta07()
            allFields["an_sta06"] = _AF6CNC0021_RD_SGMII_Multirate._an_sta_pen00._an_sta06()
            allFields["an_sta05"] = _AF6CNC0021_RD_SGMII_Multirate._an_sta_pen00._an_sta05()
            allFields["an_sta04"] = _AF6CNC0021_RD_SGMII_Multirate._an_sta_pen00._an_sta04()
            allFields["an_sta03"] = _AF6CNC0021_RD_SGMII_Multirate._an_sta_pen00._an_sta03()
            allFields["an_sta02"] = _AF6CNC0021_RD_SGMII_Multirate._an_sta_pen00._an_sta02()
            allFields["an_sta01"] = _AF6CNC0021_RD_SGMII_Multirate._an_sta_pen00._an_sta01()
            allFields["an_sta00"] = _AF6CNC0021_RD_SGMII_Multirate._an_sta_pen00._an_sta00()
            return allFields

    class _an_sta_pen01(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg-group01 status"
    
        def description(self):
            return "status of Auto-neg of group0 from port 8 to port 15"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _an_sta15(AtRegister.AtRegisterField):
            def startBit(self):
                return 31
                
            def stopBit(self):
                return 28
        
            def name(self):
                return "an_sta15"
            
            def description(self):
                return "Speed port 15"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_sta14(AtRegister.AtRegisterField):
            def startBit(self):
                return 27
                
            def stopBit(self):
                return 24
        
            def name(self):
                return "an_sta14"
            
            def description(self):
                return "Speed port 14"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_sta13(AtRegister.AtRegisterField):
            def startBit(self):
                return 23
                
            def stopBit(self):
                return 20
        
            def name(self):
                return "an_sta13"
            
            def description(self):
                return "Speed port 13"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_sta12(AtRegister.AtRegisterField):
            def startBit(self):
                return 19
                
            def stopBit(self):
                return 16
        
            def name(self):
                return "an_sta12"
            
            def description(self):
                return "Speed port 12"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_sta11(AtRegister.AtRegisterField):
            def startBit(self):
                return 15
                
            def stopBit(self):
                return 12
        
            def name(self):
                return "an_sta11"
            
            def description(self):
                return "Speed port 11"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_sta10(AtRegister.AtRegisterField):
            def startBit(self):
                return 11
                
            def stopBit(self):
                return 8
        
            def name(self):
                return "an_sta10"
            
            def description(self):
                return "Speed port 10"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_sta09(AtRegister.AtRegisterField):
            def startBit(self):
                return 7
                
            def stopBit(self):
                return 4
        
            def name(self):
                return "an_sta09"
            
            def description(self):
                return "Speed port 9"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_sta08(AtRegister.AtRegisterField):
            def startBit(self):
                return 3
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "an_sta08"
            
            def description(self):
                return "Speed port 8"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["an_sta15"] = _AF6CNC0021_RD_SGMII_Multirate._an_sta_pen01._an_sta15()
            allFields["an_sta14"] = _AF6CNC0021_RD_SGMII_Multirate._an_sta_pen01._an_sta14()
            allFields["an_sta13"] = _AF6CNC0021_RD_SGMII_Multirate._an_sta_pen01._an_sta13()
            allFields["an_sta12"] = _AF6CNC0021_RD_SGMII_Multirate._an_sta_pen01._an_sta12()
            allFields["an_sta11"] = _AF6CNC0021_RD_SGMII_Multirate._an_sta_pen01._an_sta11()
            allFields["an_sta10"] = _AF6CNC0021_RD_SGMII_Multirate._an_sta_pen01._an_sta10()
            allFields["an_sta09"] = _AF6CNC0021_RD_SGMII_Multirate._an_sta_pen01._an_sta09()
            allFields["an_sta08"] = _AF6CNC0021_RD_SGMII_Multirate._an_sta_pen01._an_sta08()
            return allFields

    class _lnk_sync_pen0(AtRegister.AtRegister):
        def name(self):
            return "Link-status-group0"
    
        def description(self):
            return "Link status of group 0 from port 0  to port 15"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _lnk_sta0(AtRegister.AtRegisterField):
            def startBit(self):
                return 15
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "lnk_sta0"
            
            def description(self):
                return "Link status, bit per port, bit[0] port  8, bit[9] port 17"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lnk_sta0"] = _AF6CNC0021_RD_SGMII_Multirate._lnk_sync_pen0._lnk_sta0()
            return allFields

    class _an_rxab_pen00(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg RX00-RX01 ability"
    
        def description(self):
            return "RX ability of RX-Port00-01 after Auto-neg succeed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _link01(AtRegister.AtRegisterField):
            def startBit(self):
                return 31
                
            def stopBit(self):
                return 31
        
            def name(self):
                return "link01"
            
            def description(self):
                return "Link status of port1"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex01(AtRegister.AtRegisterField):
            def startBit(self):
                return 28
                
            def stopBit(self):
                return 28
        
            def name(self):
                return "duplex01"
            
            def description(self):
                return "duplex mode of port1"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed01(AtRegister.AtRegisterField):
            def startBit(self):
                return 27
                
            def stopBit(self):
                return 26
        
            def name(self):
                return "speed01"
            
            def description(self):
                return "speed of SGMII of port1"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _link00(AtRegister.AtRegisterField):
            def startBit(self):
                return 15
                
            def stopBit(self):
                return 15
        
            def name(self):
                return "link00"
            
            def description(self):
                return "Link status of port0"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex00(AtRegister.AtRegisterField):
            def startBit(self):
                return 12
                
            def stopBit(self):
                return 12
        
            def name(self):
                return "duplex00"
            
            def description(self):
                return "duplex mode of port0"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed00(AtRegister.AtRegisterField):
            def startBit(self):
                return 11
                
            def stopBit(self):
                return 10
        
            def name(self):
                return "speed00"
            
            def description(self):
                return "speed of SGMII of port0"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["link01"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen00._link01()
            allFields["duplex01"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen00._duplex01()
            allFields["speed01"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen00._speed01()
            allFields["link00"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen00._link00()
            allFields["duplex00"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen00._duplex00()
            allFields["speed00"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen00._speed00()
            return allFields

    class _an_rxab_pen01(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg RX02-RX03 ability"
    
        def description(self):
            return "RX ability of RX-Port02-03 after Auto-neg succeed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _link03(AtRegister.AtRegisterField):
            def startBit(self):
                return 31
                
            def stopBit(self):
                return 31
        
            def name(self):
                return "link03"
            
            def description(self):
                return "Link status of port3"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex03(AtRegister.AtRegisterField):
            def startBit(self):
                return 28
                
            def stopBit(self):
                return 28
        
            def name(self):
                return "duplex03"
            
            def description(self):
                return "duplex mode of port3"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed03(AtRegister.AtRegisterField):
            def startBit(self):
                return 27
                
            def stopBit(self):
                return 26
        
            def name(self):
                return "speed03"
            
            def description(self):
                return "speed of SGMII of port3"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _link02(AtRegister.AtRegisterField):
            def startBit(self):
                return 15
                
            def stopBit(self):
                return 15
        
            def name(self):
                return "link02"
            
            def description(self):
                return "Link status of port2"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex02(AtRegister.AtRegisterField):
            def startBit(self):
                return 12
                
            def stopBit(self):
                return 12
        
            def name(self):
                return "duplex02"
            
            def description(self):
                return "duplex mode of port2"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed02(AtRegister.AtRegisterField):
            def startBit(self):
                return 11
                
            def stopBit(self):
                return 10
        
            def name(self):
                return "speed02"
            
            def description(self):
                return "speed of SGMII of port2"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["link03"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen01._link03()
            allFields["duplex03"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen01._duplex03()
            allFields["speed03"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen01._speed03()
            allFields["link02"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen01._link02()
            allFields["duplex02"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen01._duplex02()
            allFields["speed02"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen01._speed02()
            return allFields

    class _an_rxab_pen02(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg RX04-RX05 ability"
    
        def description(self):
            return "RX ability of RX-Port04-05 after Auto-neg succeed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _link05(AtRegister.AtRegisterField):
            def startBit(self):
                return 31
                
            def stopBit(self):
                return 31
        
            def name(self):
                return "link05"
            
            def description(self):
                return "Link status of port5"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex05(AtRegister.AtRegisterField):
            def startBit(self):
                return 28
                
            def stopBit(self):
                return 28
        
            def name(self):
                return "duplex05"
            
            def description(self):
                return "duplex mode of port5"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed05(AtRegister.AtRegisterField):
            def startBit(self):
                return 27
                
            def stopBit(self):
                return 26
        
            def name(self):
                return "speed05"
            
            def description(self):
                return "speed of SGMII of port5"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _link04(AtRegister.AtRegisterField):
            def startBit(self):
                return 15
                
            def stopBit(self):
                return 15
        
            def name(self):
                return "link04"
            
            def description(self):
                return "Link status of port4"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex04(AtRegister.AtRegisterField):
            def startBit(self):
                return 12
                
            def stopBit(self):
                return 12
        
            def name(self):
                return "duplex04"
            
            def description(self):
                return "duplex mode of port4"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed04(AtRegister.AtRegisterField):
            def startBit(self):
                return 11
                
            def stopBit(self):
                return 10
        
            def name(self):
                return "speed04"
            
            def description(self):
                return "speed of SGMII of port4"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["link05"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen02._link05()
            allFields["duplex05"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen02._duplex05()
            allFields["speed05"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen02._speed05()
            allFields["link04"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen02._link04()
            allFields["duplex04"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen02._duplex04()
            allFields["speed04"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen02._speed04()
            return allFields

    class _an_rxab_pen03(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg RX06-RX07 ability"
    
        def description(self):
            return "RX ability of RX-Port00-01 after Auto-neg succeed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _link07(AtRegister.AtRegisterField):
            def startBit(self):
                return 31
                
            def stopBit(self):
                return 31
        
            def name(self):
                return "link07"
            
            def description(self):
                return "Link status of port7"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex07(AtRegister.AtRegisterField):
            def startBit(self):
                return 28
                
            def stopBit(self):
                return 28
        
            def name(self):
                return "duplex07"
            
            def description(self):
                return "duplex mode of port7"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed07(AtRegister.AtRegisterField):
            def startBit(self):
                return 27
                
            def stopBit(self):
                return 26
        
            def name(self):
                return "speed07"
            
            def description(self):
                return "speed of SGMII of port7"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _link06(AtRegister.AtRegisterField):
            def startBit(self):
                return 15
                
            def stopBit(self):
                return 15
        
            def name(self):
                return "link06"
            
            def description(self):
                return "Link status of port6"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex06(AtRegister.AtRegisterField):
            def startBit(self):
                return 12
                
            def stopBit(self):
                return 12
        
            def name(self):
                return "duplex06"
            
            def description(self):
                return "duplex mode of port6"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed06(AtRegister.AtRegisterField):
            def startBit(self):
                return 11
                
            def stopBit(self):
                return 10
        
            def name(self):
                return "speed06"
            
            def description(self):
                return "speed of SGMII of port6"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["link07"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen03._link07()
            allFields["duplex07"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen03._duplex07()
            allFields["speed07"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen03._speed07()
            allFields["link06"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen03._link06()
            allFields["duplex06"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen03._duplex06()
            allFields["speed06"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen03._speed06()
            return allFields

    class _an_rxab_pen04(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg RX08-RX09 ability"
    
        def description(self):
            return "RX ability of RX-Port08-09 after Auto-neg succeed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _link09(AtRegister.AtRegisterField):
            def startBit(self):
                return 31
                
            def stopBit(self):
                return 31
        
            def name(self):
                return "link09"
            
            def description(self):
                return "Link status of port9"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex09(AtRegister.AtRegisterField):
            def startBit(self):
                return 28
                
            def stopBit(self):
                return 28
        
            def name(self):
                return "duplex09"
            
            def description(self):
                return "duplex mode of port9"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed09(AtRegister.AtRegisterField):
            def startBit(self):
                return 27
                
            def stopBit(self):
                return 26
        
            def name(self):
                return "speed09"
            
            def description(self):
                return "speed of SGMII of port9"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _link08(AtRegister.AtRegisterField):
            def startBit(self):
                return 15
                
            def stopBit(self):
                return 15
        
            def name(self):
                return "link08"
            
            def description(self):
                return "Link status of port8"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex08(AtRegister.AtRegisterField):
            def startBit(self):
                return 12
                
            def stopBit(self):
                return 12
        
            def name(self):
                return "duplex08"
            
            def description(self):
                return "duplex mode of port8"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed08(AtRegister.AtRegisterField):
            def startBit(self):
                return 11
                
            def stopBit(self):
                return 10
        
            def name(self):
                return "speed08"
            
            def description(self):
                return "speed of SGMII of port8"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["link09"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen04._link09()
            allFields["duplex09"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen04._duplex09()
            allFields["speed09"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen04._speed09()
            allFields["link08"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen04._link08()
            allFields["duplex08"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen04._duplex08()
            allFields["speed08"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen04._speed08()
            return allFields

    class _an_rxab_pen05(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg RX10-RX11 ability"
    
        def description(self):
            return "RX ability of RX-Port10-11 after Auto-neg succeed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _link11(AtRegister.AtRegisterField):
            def startBit(self):
                return 31
                
            def stopBit(self):
                return 31
        
            def name(self):
                return "link11"
            
            def description(self):
                return "Link status of port11"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex11(AtRegister.AtRegisterField):
            def startBit(self):
                return 28
                
            def stopBit(self):
                return 28
        
            def name(self):
                return "duplex11"
            
            def description(self):
                return "duplex mode of port11"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed11(AtRegister.AtRegisterField):
            def startBit(self):
                return 27
                
            def stopBit(self):
                return 26
        
            def name(self):
                return "speed11"
            
            def description(self):
                return "speed of SGMII of port11"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _link10(AtRegister.AtRegisterField):
            def startBit(self):
                return 15
                
            def stopBit(self):
                return 15
        
            def name(self):
                return "link10"
            
            def description(self):
                return "Link status of port10"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex10(AtRegister.AtRegisterField):
            def startBit(self):
                return 12
                
            def stopBit(self):
                return 12
        
            def name(self):
                return "duplex10"
            
            def description(self):
                return "duplex mode of port10"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed10(AtRegister.AtRegisterField):
            def startBit(self):
                return 11
                
            def stopBit(self):
                return 10
        
            def name(self):
                return "speed10"
            
            def description(self):
                return "speed of SGMII of port10"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["link11"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen05._link11()
            allFields["duplex11"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen05._duplex11()
            allFields["speed11"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen05._speed11()
            allFields["link10"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen05._link10()
            allFields["duplex10"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen05._duplex10()
            allFields["speed10"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen05._speed10()
            return allFields

    class _an_rxab_pen06(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg RX12-RX13 ability"
    
        def description(self):
            return "RX ability of RX-Port12-13 after Auto-neg succeed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _link13(AtRegister.AtRegisterField):
            def startBit(self):
                return 31
                
            def stopBit(self):
                return 31
        
            def name(self):
                return "link13"
            
            def description(self):
                return "Link status of port13"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex13(AtRegister.AtRegisterField):
            def startBit(self):
                return 28
                
            def stopBit(self):
                return 28
        
            def name(self):
                return "duplex13"
            
            def description(self):
                return "duplex mode of port13"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed13(AtRegister.AtRegisterField):
            def startBit(self):
                return 27
                
            def stopBit(self):
                return 26
        
            def name(self):
                return "speed13"
            
            def description(self):
                return "speed of SGMII of port13"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _link12(AtRegister.AtRegisterField):
            def startBit(self):
                return 15
                
            def stopBit(self):
                return 15
        
            def name(self):
                return "link12"
            
            def description(self):
                return "Link status of port12"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex12(AtRegister.AtRegisterField):
            def startBit(self):
                return 12
                
            def stopBit(self):
                return 12
        
            def name(self):
                return "duplex12"
            
            def description(self):
                return "duplex mode of port12"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed12(AtRegister.AtRegisterField):
            def startBit(self):
                return 11
                
            def stopBit(self):
                return 10
        
            def name(self):
                return "speed12"
            
            def description(self):
                return "speed of SGMII of port12"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["link13"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen06._link13()
            allFields["duplex13"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen06._duplex13()
            allFields["speed13"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen06._speed13()
            allFields["link12"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen06._link12()
            allFields["duplex12"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen06._duplex12()
            allFields["speed12"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen06._speed12()
            return allFields

    class _an_rxab_pen07(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg RX14-RX15 ability"
    
        def description(self):
            return "RX ability of RX-Port14-15 after Auto-neg succeed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _link15(AtRegister.AtRegisterField):
            def startBit(self):
                return 31
                
            def stopBit(self):
                return 31
        
            def name(self):
                return "link15"
            
            def description(self):
                return "Link status of port15"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex15(AtRegister.AtRegisterField):
            def startBit(self):
                return 28
                
            def stopBit(self):
                return 28
        
            def name(self):
                return "duplex15"
            
            def description(self):
                return "duplex mode of port15"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed15(AtRegister.AtRegisterField):
            def startBit(self):
                return 27
                
            def stopBit(self):
                return 26
        
            def name(self):
                return "speed15"
            
            def description(self):
                return "speed of SGMII of port15"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _link14(AtRegister.AtRegisterField):
            def startBit(self):
                return 15
                
            def stopBit(self):
                return 15
        
            def name(self):
                return "link14"
            
            def description(self):
                return "Link status of port14"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex14(AtRegister.AtRegisterField):
            def startBit(self):
                return 12
                
            def stopBit(self):
                return 12
        
            def name(self):
                return "duplex14"
            
            def description(self):
                return "duplex mode of port14"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed14(AtRegister.AtRegisterField):
            def startBit(self):
                return 11
                
            def stopBit(self):
                return 10
        
            def name(self):
                return "speed14"
            
            def description(self):
                return "speed of SGMII of port14"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["link15"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen07._link15()
            allFields["duplex15"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen07._duplex15()
            allFields["speed15"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen07._speed15()
            allFields["link14"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen07._link14()
            allFields["duplex14"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen07._duplex14()
            allFields["speed14"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen07._speed14()
            return allFields

    class _intmsk_grp0_pen(AtRegister.AtRegister):
        def name(self):
            return "Port Interrupt Enable Control Group0"
    
        def description(self):
            return "This register provides Interrupt Enable for 16 ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _LinkSyncIntEn(AtRegister.AtRegisterField):
            def startBit(self):
                return 18
                
            def stopBit(self):
                return 18
        
            def name(self):
                return "LinkSyncIntEn"
            
            def description(self):
                return "Link-Sync Interrupt Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _LossSignalIntEn(AtRegister.AtRegisterField):
            def startBit(self):
                return 17
                
            def stopBit(self):
                return 17
        
            def name(self):
                return "LossSignalIntEn"
            
            def description(self):
                return "Loss-of-Signal Interrupt Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _ReAlignIntEn(AtRegister.AtRegisterField):
            def startBit(self):
                return 16
                
            def stopBit(self):
                return 16
        
            def name(self):
                return "ReAlignIntEn"
            
            def description(self):
                return "Re-align Interrupt Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _PortIntEn(AtRegister.AtRegisterField):
            def startBit(self):
                return 15
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "PortIntEn"
            
            def description(self):
                return "GMAC Port Interrupt Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["LinkSyncIntEn"] = _AF6CNC0021_RD_SGMII_Multirate._intmsk_grp0_pen._LinkSyncIntEn()
            allFields["LossSignalIntEn"] = _AF6CNC0021_RD_SGMII_Multirate._intmsk_grp0_pen._LossSignalIntEn()
            allFields["ReAlignIntEn"] = _AF6CNC0021_RD_SGMII_Multirate._intmsk_grp0_pen._ReAlignIntEn()
            allFields["PortIntEn"] = _AF6CNC0021_RD_SGMII_Multirate._intmsk_grp0_pen._PortIntEn()
            return allFields

    class _intsta_grp0_pen(AtRegister.AtRegister):
        def name(self):
            return "Port Interrupt Status Group0"
    
        def description(self):
            return "This register provides Interrupt Status on 16 ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _PortIntSta(AtRegister.AtRegisterField):
            def startBit(self):
                return 15
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "PortIntSta"
            
            def description(self):
                return "Port Interrupt Status"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PortIntSta"] = _AF6CNC0021_RD_SGMII_Multirate._intsta_grp0_pen._PortIntSta()
            return allFields

    class _intstk_status_grp0_pen(AtRegister.AtRegister):
        def name(self):
            return "Port  Event Status Sticky Group0"
    
        def description(self):
            return "This register provides Status Sticky on 16 ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0040 + $pid"
            
        def startAddress(self):
            return 0x00000040
            
        def endAddress(self):
            return 0x0000004f

        class _Linksync_sta(AtRegister.AtRegisterField):
            def startBit(self):
                return 5
                
            def stopBit(self):
                return 5
        
            def name(self):
                return "Linksync_sta"
            
            def description(self):
                return "Link-SYnc State"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Lossignal_sta(AtRegister.AtRegisterField):
            def startBit(self):
                return 4
                
            def stopBit(self):
                return 4
        
            def name(self):
                return "Lossignal_sta"
            
            def description(self):
                return "Loss-Signal State"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _ReAlign_sta(AtRegister.AtRegisterField):
            def startBit(self):
                return 3
                
            def stopBit(self):
                return 3
        
            def name(self):
                return "ReAlign_sta"
            
            def description(self):
                return "Realign State"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Linksync_stk(AtRegister.AtRegisterField):
            def startBit(self):
                return 2
                
            def stopBit(self):
                return 2
        
            def name(self):
                return "Linksync_stk"
            
            def description(self):
                return "Link-SYnc State Change Alarm"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Lossignal_stk(AtRegister.AtRegisterField):
            def startBit(self):
                return 1
                
            def stopBit(self):
                return 1
        
            def name(self):
                return "Lossignal_stk"
            
            def description(self):
                return "Loss-Signal State Change Alarm"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _ReAlign_stk(AtRegister.AtRegisterField):
            def startBit(self):
                return 0
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "ReAlign_stk"
            
            def description(self):
                return "Realign State Change Alarm"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Linksync_sta"] = _AF6CNC0021_RD_SGMII_Multirate._intstk_status_grp0_pen._Linksync_sta()
            allFields["Lossignal_sta"] = _AF6CNC0021_RD_SGMII_Multirate._intstk_status_grp0_pen._Lossignal_sta()
            allFields["ReAlign_sta"] = _AF6CNC0021_RD_SGMII_Multirate._intstk_status_grp0_pen._ReAlign_sta()
            allFields["Linksync_stk"] = _AF6CNC0021_RD_SGMII_Multirate._intstk_status_grp0_pen._Linksync_stk()
            allFields["Lossignal_stk"] = _AF6CNC0021_RD_SGMII_Multirate._intstk_status_grp0_pen._Lossignal_stk()
            allFields["ReAlign_stk"] = _AF6CNC0021_RD_SGMII_Multirate._intstk_status_grp0_pen._ReAlign_stk()
            return allFields

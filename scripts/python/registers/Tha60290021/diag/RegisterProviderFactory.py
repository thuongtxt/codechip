import python.arrive.atsdk.AtRegister as AtRegister

class RegisterProviderFactory(AtRegister.AtRegisterProviderFactory):
    def _allRegisterProviders(self):
        allProviders = {}

        from _AF6CNC0021_Interrupt_Diag import _AF6CNC0021_Interrupt_Diag
        allProviders["_AF6CNC0021_Interrupt_Diag"] = _AF6CNC0021_Interrupt_Diag()

        from _AF6CNC0021_Faceplate_Serdeses_Gatetime import _AF6CNC0021_Faceplate_Serdeses_Gatetime
        allProviders["_AF6CNC0021_Faceplate_Serdeses_Gatetime"] = _AF6CNC0021_Faceplate_Serdeses_Gatetime()

        from _AF6CNC0021_DDR_additional_RD_DIAG import _AF6CNC0021_DDR_additional_RD_DIAG
        allProviders["_AF6CNC0021_DDR_additional_RD_DIAG"] = _AF6CNC0021_DDR_additional_RD_DIAG()

        from _AF6CNC0021_RD_TOP_GLB import _AF6CNC0021_RD_TOP_GLB
        allProviders["_AF6CNC0021_RD_TOP_GLB"] = _AF6CNC0021_RD_TOP_GLB()

        from _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII import _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII
        allProviders["_AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII()

        from _AF6CNC0021_Xilinx_Serdes_Turning_Configuration_RD import _AF6CNC0021_Xilinx_Serdes_Turning_Configuration_RD
        allProviders["_AF6CNC0021_Xilinx_Serdes_Turning_Configuration_RD"] = _AF6CNC0021_Xilinx_Serdes_Turning_Configuration_RD()

        from _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD import _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD
        allProviders["_AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD()

        from _AF6CNC0021_OC192_OC48_OC12_OC3_RD_DIAG import _AF6CNC0021_OC192_OC48_OC12_OC3_RD_DIAG
        allProviders["_AF6CNC0021_OC192_OC48_OC12_OC3_RD_DIAG"] = _AF6CNC0021_OC192_OC48_OC12_OC3_RD_DIAG()

        from _ES_Local_Bus_2_1G_MDIO import _ES_Local_Bus_2_1G_MDIO
        allProviders["_ES_Local_Bus_2_1G_MDIO"] = _ES_Local_Bus_2_1G_MDIO()

        from _ES_MII_RD_DIAG import _ES_MII_RD_DIAG
        allProviders["_ES_MII_RD_DIAG"] = _ES_MII_RD_DIAG()

        from _AF6CNC0021_SEM import _AF6CNC0021_SEM
        allProviders["_AF6CNC0021_SEM"] = _AF6CNC0021_SEM()

        from _AF6CNC0021_XFI_RD_DIAG import _AF6CNC0021_XFI_RD_DIAG
        allProviders["_AF6CNC0021_XFI_RD_DIAG"] = _AF6CNC0021_XFI_RD_DIAG()

        from _AF6CNC0021_GMII_RD_DIAG import _AF6CNC0021_GMII_RD_DIAG
        allProviders["_AF6CNC0021_GMII_RD_DIAG"] = _AF6CNC0021_GMII_RD_DIAG()

        from _AF6CNC0021_ETH40G_RD import _AF6CNC0021_ETH40G_RD
        allProviders["_AF6CNC0021_ETH40G_RD"] = _AF6CNC0021_ETH40G_RD()


        return allProviders

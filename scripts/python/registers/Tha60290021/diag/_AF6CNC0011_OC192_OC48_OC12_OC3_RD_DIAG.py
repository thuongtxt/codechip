import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["glbrfm_reg"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._glbrfm_reg()
        allRegisters["glbclkmon_reg"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._glbclkmon_reg()
        allRegisters["glbdetlos_pen"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._glbdetlos_pen()
        allRegisters["glblofthr_reg"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._glblofthr_reg()
        allRegisters["glbtfm_reg"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._glbtfm_reg()
        allRegisters["rxfrmsta"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._rxfrmsta()
        allRegisters["rxfrmstk"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._rxfrmstk()
        allRegisters["rxfrmb1cntro"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._rxfrmb1cntro()
        allRegisters["rxfrmb1cntr2c"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._rxfrmb1cntr2c()
        return allRegisters

    class _glbrfm_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Rx Framer Control"
    
        def description(self):
            return "This is the global configuration register for the Rx Framer"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _RxFrmBadFrmThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 12
        
            def name(self):
                return "RxFrmBadFrmThresh"
            
            def description(self):
                return "Threshold for A1A2 missing counter, that is used to change state from FRAMED to HUNT."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmDescrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "RxFrmDescrEn"
            
            def description(self):
                return "Enable/disable de-scrambling of the Rx coming data stream for 4 lines. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmStmOcnMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxFrmStmOcnMode"
            
            def description(self):
                return "STM rate mode for Rx of  4 lines, each line use 2 bits.Bits[1:0] for line 0 0: OC48 1: OC12 2: OC3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxFrmBadFrmThresh"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._glbrfm_reg._RxFrmBadFrmThresh()
            allFields["RxFrmDescrEn"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._glbrfm_reg._RxFrmDescrEn()
            allFields["RxFrmStmOcnMode"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._glbrfm_reg._RxFrmStmOcnMode()
            return allFields

    class _glbclkmon_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Rx Framer LOS Detecting Control 1"
    
        def description(self):
            return "This is the global configuration register for the Rx Framer LOS detecting 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000006
            
        def endAddress(self):
            return 0xffffffff

        class _RxFrmLosDetMod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "RxFrmLosDetMod"
            
            def description(self):
                return "Detected LOS mode. 1: the LOS declare when all zero or all one detected 0: the LOS declare when all zero detected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmLosDetDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "RxFrmLosDetDis"
            
            def description(self):
                return "Disable detect LOS. 1: Disable 0: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmClkMonDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "RxFrmClkMonDis"
            
            def description(self):
                return "Disable to generate LOS to Rx Framer if detecting error on Rx line clock. 1: Disable 0: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmClkMonThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxFrmClkMonThr"
            
            def description(self):
                return "Threshold to generate LOS to Rx Framer if detecting error on Rx line clock.(Threshold = PPM * 155.52/8 for STM16 and STM1). Default 0x3cc ~ 50ppm for STM16 and STM1. With STM4 must config value that is this default value divide by 4."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxFrmLosDetMod"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._glbclkmon_reg._RxFrmLosDetMod()
            allFields["RxFrmLosDetDis"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._glbclkmon_reg._RxFrmLosDetDis()
            allFields["RxFrmClkMonDis"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._glbclkmon_reg._RxFrmClkMonDis()
            allFields["RxFrmClkMonThr"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._glbclkmon_reg._RxFrmClkMonThr()
            return allFields

    class _glbdetlos_pen(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Rx Framer LOS Detecting Control 2"
    
        def description(self):
            return "This is the global configuration register for the Rx Framer LOS detecting 2"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000007
            
        def endAddress(self):
            return 0xffffffff

        class _RxFrmLosClr2Thr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "RxFrmLosClr2Thr"
            
            def description(self):
                return "Configure the period of time during which there is no period of consecutive data without transition, used for reset no transition counter. The recommended value is 0x30 (~2.5ms) for STM16 and STM1. With STM4 must config value that is this default value divide by 4."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmLosSetThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 12
        
            def name(self):
                return "RxFrmLosSetThr"
            
            def description(self):
                return "Configure the value that define the period of time in which there is no transition detected from incoming data from SERDES. The recommended value is 0x3cc (~50ms) for STM16 and STM1. With STM4 must config value that is this default value divide by 4."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmLosClr1Thr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxFrmLosClr1Thr"
            
            def description(self):
                return "Configure the period of time during which there is no period of consecutive data without transition, used for counting at INFRM to change state to OOF The recommended value is 0x3cc (~50ms) for STM16 and STM1. With STM4 must config value that is this default value divide by 4."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxFrmLosClr2Thr"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._glbdetlos_pen._RxFrmLosClr2Thr()
            allFields["RxFrmLosSetThr"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._glbdetlos_pen._RxFrmLosSetThr()
            allFields["RxFrmLosClr1Thr"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._glbdetlos_pen._RxFrmLosClr1Thr()
            return allFields

    class _glblofthr_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Rx Framer LOF Threshold"
    
        def description(self):
            return "Configure thresholds for LOF detection at receive SONET/SDH framer,There are two thresholds"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000008
            
        def endAddress(self):
            return 0xffffffff

        class _RxFrmLofDetMod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "RxFrmLofDetMod"
            
            def description(self):
                return "Detected LOF mode.Set 1 to clear OOF counter when state into INFRAMED"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmLofSetThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "RxFrmLofSetThr"
            
            def description(self):
                return "Configure the OOF time counter threshold for entering LOF state. Resolution of this threshold is one frame."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmLofClrThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxFrmLofClrThr"
            
            def description(self):
                return "Configure the In-frame time counter threshold for exiting LOF state. Resolution of this threshold is one frame."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxFrmLofDetMod"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._glblofthr_reg._RxFrmLofDetMod()
            allFields["RxFrmLofSetThr"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._glblofthr_reg._RxFrmLofSetThr()
            allFields["RxFrmLofClrThr"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._glblofthr_reg._RxFrmLofClrThr()
            return allFields

    class _glbtfm_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Tx Framer Control"
    
        def description(self):
            return "This is the global configuration register for the Tx Framer"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000001
            
        def endAddress(self):
            return 0xffffffff

        class _TxLineOofFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "TxLineOofFrc"
            
            def description(self):
                return "Enable/disable force OOF for 4 tx lines. Bit[0] for line 0."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxLineB1ErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "TxLineB1ErrFrc"
            
            def description(self):
                return "Enable/disable force B1 error for 4 tx lines. Bit[0] for line 0."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxFrmScrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TxFrmScrEn"
            
            def description(self):
                return "Enable/disable scrambling for 4 tx lines. Bit[0] for line 0. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxFrmStmOcnMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxFrmStmOcnMode"
            
            def description(self):
                return "STM rate mode for Tx of 4 lines, each line use 2 bits.Bits[1:0] for line 0 0: OC48 1: OC12 2: OC3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxLineOofFrc"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._glbtfm_reg._TxLineOofFrc()
            allFields["TxLineB1ErrFrc"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._glbtfm_reg._TxLineB1ErrFrc()
            allFields["TxFrmScrEn"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._glbtfm_reg._TxFrmScrEn()
            allFields["TxFrmStmOcnMode"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._glbtfm_reg._TxFrmStmOcnMode()
            return allFields

    class _rxfrmsta(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx Framer Status"
    
        def description(self):
            return "Rx Framer status"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00010 + 16*LineId"
            
        def startAddress(self):
            return 0x00000010
            
        def endAddress(self):
            return 0x00000040

        class _OofCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "OofCurStatus"
            
            def description(self):
                return "OOF current status in the related line. 1: OOF state 0: Not OOF state."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LofCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "LofCurStatus"
            
            def description(self):
                return "LOF current status in the related line. 1: LOF state 0: Not LOF state."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LosCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LosCurStatus"
            
            def description(self):
                return "LOS current status in the related line. 1: LOS state 0: Not LOS state."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["OofCurStatus"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._rxfrmsta._OofCurStatus()
            allFields["LofCurStatus"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._rxfrmsta._LofCurStatus()
            allFields["LosCurStatus"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._rxfrmsta._LosCurStatus()
            return allFields

    class _rxfrmstk(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx Framer Sticky"
    
        def description(self):
            return "Rx Framer sticky"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00011 + 16*LineId"
            
        def startAddress(self):
            return 0x00000011
            
        def endAddress(self):
            return 0x00000041

        class _Oc3AlgErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Oc3AlgErrStk"
            
            def description(self):
                return "Set 1 while any error detected at OC3 Aligner engine."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _ClkMonErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "ClkMonErrStk"
            
            def description(self):
                return "Set 1 while any error detected at Error clock monitor engine."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _OofStateChgStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "OofStateChgStk"
            
            def description(self):
                return "Set 1 while OOF state change event happens in the related line."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _LofStateChgStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "LofStateChgStk"
            
            def description(self):
                return "Set 1 while LOF state change event happens in the related line."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _LosStateChgStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LosStateChgStk"
            
            def description(self):
                return "Set 1 while LOS state change event happens in the related line."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Oc3AlgErrStk"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._rxfrmstk._Oc3AlgErrStk()
            allFields["ClkMonErrStk"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._rxfrmstk._ClkMonErrStk()
            allFields["OofStateChgStk"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._rxfrmstk._OofStateChgStk()
            allFields["LofStateChgStk"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._rxfrmstk._LofStateChgStk()
            allFields["LosStateChgStk"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._rxfrmstk._LosStateChgStk()
            return allFields

    class _rxfrmb1cntro(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx Framer B1 error counter read only"
    
        def description(self):
            return "Rx Framer B1 error counter read only"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00012 + 16*LineId"
            
        def startAddress(self):
            return 0x00000012
            
        def endAddress(self):
            return 0x00000042

        class _RxFrmB1ErrRo(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxFrmB1ErrRo"
            
            def description(self):
                return "Number of B1 error - read only"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxFrmB1ErrRo"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._rxfrmb1cntro._RxFrmB1ErrRo()
            return allFields

    class _rxfrmb1cntr2c(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx Framer B1 error counter read to clear"
    
        def description(self):
            return "Rx Framer B1 error counter read to clear"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00013 + 16*LineId"
            
        def startAddress(self):
            return 0x00000013
            
        def endAddress(self):
            return 0x00000043

        class _RxFrmB1ErrR2C(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxFrmB1ErrR2C"
            
            def description(self):
                return "Number of B1 error - read to clear"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxFrmB1ErrR2C"] = _AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG._rxfrmb1cntr2c._RxFrmB1ErrR2C()
            return allFields

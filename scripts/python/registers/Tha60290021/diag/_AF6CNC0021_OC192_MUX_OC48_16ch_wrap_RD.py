import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["OC192_MUX_OC48_Change_Mode"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_Change_Mode()
        allRegisters["OC192_MUX_OC48_Current_Mode"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_Current_Mode()
        allRegisters["OC192_MUX_OC48_DRP"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_DRP()
        allRegisters["OC192_MUX_OC48_LoopBack"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_LoopBack()
        allRegisters["Async_GearBox_Enable_of_10Ge_mode"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._Async_GearBox_Enable_of_10Ge_mode()
        allRegisters["OC192_MUX_OC48_PLL_Status"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_PLL_Status()
        allRegisters["OC192_MUX_OC48_TX_Reset"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TX_Reset()
        allRegisters["OC192_MUX_OC48_RX_Reset"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_RX_Reset()
        allRegisters["OC192_MUX_OC48_LPMDFE_Mode"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_LPMDFE_Mode()
        allRegisters["OC192_MUX_OC48_LPMDFE_Reset"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_LPMDFE_Reset()
        allRegisters["OC192_MUX_OC48_TXDIFFCTRL"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TXDIFFCTRL()
        allRegisters["OC192_MUX_OC48_TXPOSTCURSOR"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TXPOSTCURSOR()
        allRegisters["OC192_MUX_OC48_TXPRECURSOR"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TXPRECURSOR()
        allRegisters["SERDES_POWER_DOWN"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._SERDES_POWER_DOWN()
        allRegisters["Force_Running_Disparity_Error"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._Force_Running_Disparity_Error()
        allRegisters["RX_CDR_Lock_to_Reference"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_CDR_Lock_to_Reference()
        allRegisters["RX_Monitor_PPM_Ref_Value"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_Monitor_PPM_Ref_Value()
        allRegisters["RX_Monitor_PPM_Mon_Value"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_Monitor_PPM_Mon_Value()
        allRegisters["RX_Monitor_PPM_Control"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_Monitor_PPM_Control()
        allRegisters["RX_RXLPMHFOVRDEN_Control"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_RXLPMHFOVRDEN_Control()
        allRegisters["RX_RXLPMLFKLOVRDEN_Control"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_RXLPMLFKLOVRDEN_Control()
        allRegisters["RX_RXOSOVRDEN_Control"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_RXOSOVRDEN_Control()
        allRegisters["RX_RXLPMOSHOLD_Control"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_RXLPMOSHOLD_Control()
        allRegisters["RX_RXLPMOSOVRDEN_Control"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_RXLPMOSOVRDEN_Control()
        allRegisters["RX_RXLPM_OS_CFG1_Control"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_RXLPM_OS_CFG1_Control()
        allRegisters["RX_RXLPMGCHOLD_Control"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_RXLPMGCHOLD_Control()
        allRegisters["RX_RXLPMGCOVRDEN_CONTROL"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_RXLPMGCOVRDEN_CONTROL()
        allRegisters["RX_RXLPM_GC_CFG_Control"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_RXLPM_GC_CFG_Control()
        return allRegisters

    class _OC192_MUX_OC48_Change_Mode(AtRegister.AtRegister):
        def name(self):
            return "OC192 MUX OC48 Change Mode"
    
        def description(self):
            return "Configurate Port mode, there are 4 groups (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0080+$G*0x2000"
            
        def startAddress(self):
            return 0x00000080
            
        def endAddress(self):
            return 0x00006080

        class _chg_done(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "chg_done"
            
            def description(self):
                return "Change mode has done"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _chg_trig(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "chg_trig"
            
            def description(self):
                return "Trigger 0->1 to start changing mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _chg_mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "chg_mode"
            
            def description(self):
                return "Port mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _chg_port(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "chg_port"
            
            def description(self):
                return "Sub Port ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["chg_done"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_Change_Mode._chg_done()
            allFields["chg_trig"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_Change_Mode._chg_trig()
            allFields["chg_mode"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_Change_Mode._chg_mode()
            allFields["chg_port"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_Change_Mode._chg_port()
            return allFields

    class _OC192_MUX_OC48_Current_Mode(AtRegister.AtRegister):
        def name(self):
            return "OC192 MUX OC48 Current Mode"
    
        def description(self):
            return "Current Port mode, there are 4 groups (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0090+$G*0x2000"
            
        def startAddress(self):
            return 0x00000090
            
        def endAddress(self):
            return 0x00006090

        class _cur_mode_subport3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "cur_mode_subport3"
            
            def description(self):
                return "Current mode subport3, Group 0 => Port3, Group 1 => Port 7, Group 2 => Port 11, Group 3 => Port 15"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _cur_mode_subport2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "cur_mode_subport2"
            
            def description(self):
                return "Current mode subport2, Group 0 => Port2, Group 1 => Port 6, Group 2 => Port 10, Group 3 => Port 14"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _cur_mode_subport1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "cur_mode_subport1"
            
            def description(self):
                return "Current mode subport1, Group 0 => Port1, Group 1 => Port 5, Group 2 => Port 9, Group 3 => Port 13"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _cur_mode_subport0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cur_mode_subport0"
            
            def description(self):
                return "Current mode subport0, Group 0 => Port0, Group 1 => Port 4, Group 2 => Port 8, Group 3 => Port 12"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cur_mode_subport3"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_Current_Mode._cur_mode_subport3()
            allFields["cur_mode_subport2"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_Current_Mode._cur_mode_subport2()
            allFields["cur_mode_subport1"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_Current_Mode._cur_mode_subport1()
            allFields["cur_mode_subport0"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_Current_Mode._cur_mode_subport0()
            return allFields

    class _OC192_MUX_OC48_DRP(AtRegister.AtRegister):
        def name(self):
            return "OC192 MUX OC48 DRP"
    
        def description(self):
            return "Read/Write DRP address of SERDES, there are 4 groups (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1000+$G*0x2000+$P*0x400+$DRP"
            
        def startAddress(self):
            return 0x00001000
            
        def endAddress(self):
            return 0x00007fff

        class _drp_rw(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "drp_rw"
            
            def description(self):
                return "DRP read/write value"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["drp_rw"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_DRP._drp_rw()
            return allFields

    class _OC192_MUX_OC48_LoopBack(AtRegister.AtRegister):
        def name(self):
            return "OC192 MUX OC48 LoopBack"
    
        def description(self):
            return "Configurate LoopBack, there are 4 groups (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0002+$G*0x2000"
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0x00006002

        class _lpback_subport3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "lpback_subport3"
            
            def description(self):
                return "Loopback subport 3, Group 0 => Port3, Group 1 => Port 7, Group 2 => Port 11, Group 3 => Port 15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lpback_subport2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "lpback_subport2"
            
            def description(self):
                return "Loopback subport 2, Group 0 => Port2, Group 1 => Port 6, Group 2 => Port 10, Group 3 => Port 14"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lpback_subport1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "lpback_subport1"
            
            def description(self):
                return "Loopback subport 1, Group 0 => Port1, Group 1 => Port 5, Group 2 => Port 9, Group 3 => Port 13"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lpback_subport0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lpback_subport0"
            
            def description(self):
                return "Loopback subport 0, Group 0 => Port0, Group 1 => Port 4, Group 2 => Port 8, Group 3 => Port 12"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lpback_subport3"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_LoopBack._lpback_subport3()
            allFields["lpback_subport2"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_LoopBack._lpback_subport2()
            allFields["lpback_subport1"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_LoopBack._lpback_subport1()
            allFields["lpback_subport0"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_LoopBack._lpback_subport0()
            return allFields

    class _Async_GearBox_Enable_of_10Ge_mode(AtRegister.AtRegister):
        def name(self):
            return "Async GearBox Enable of 10Ge mode"
    
        def description(self):
            return "Configurate Enable/Disable Async GearBox of 10Ge mode, Async GearBox has to be disable when using remote loopback far-end PMA"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x107C+$G*0x2000"
            
        def startAddress(self):
            return 0x0000107c
            
        def endAddress(self):
            return 0x00007c7c

        class _async_gearbix_enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "async_gearbix_enb"
            
            def description(self):
                return "Eanble/disable async gearbox of 10Ge mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["async_gearbix_enb"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._Async_GearBox_Enable_of_10Ge_mode._async_gearbix_enb()
            return allFields

    class _OC192_MUX_OC48_PLL_Status(AtRegister.AtRegister):
        def name(self):
            return "OC192 MUX OC48 PLL Status"
    
        def description(self):
            return "QPLL/CPLL status, there are 4 groups (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000B+$G*0x2000"
            
        def startAddress(self):
            return 0x0000000b
            
        def endAddress(self):
            return 0x0000600b

        class _QPLL1_Lock_change(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "QPLL1_Lock_change"
            
            def description(self):
                return "QPLL1 has transition lock/unlock, Group 0-3"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _QPLL0_Lock_change(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "QPLL0_Lock_change"
            
            def description(self):
                return "QPLL0 has transition lock/unlock, Group 0-3"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _QPLL1_Lock(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "QPLL1_Lock"
            
            def description(self):
                return "QPLL0 is Locked, Group 0-3"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _QPLL0_Lock(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "QPLL0_Lock"
            
            def description(self):
                return "QPLL0 is Locked, Group 0-3"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _CPLL_Lock_Change(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "CPLL_Lock_Change"
            
            def description(self):
                return "CPLL has transition lock/unlock, bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, Group 2 => Port 8-11, Group 3 => Port 12-15"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _CPLL_Lock(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CPLL_Lock"
            
            def description(self):
                return "CPLL is Locked, bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, Group 2 => Port 8-11, Group 3 => Port 12-15"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["QPLL1_Lock_change"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_PLL_Status._QPLL1_Lock_change()
            allFields["QPLL0_Lock_change"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_PLL_Status._QPLL0_Lock_change()
            allFields["QPLL1_Lock"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_PLL_Status._QPLL1_Lock()
            allFields["QPLL0_Lock"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_PLL_Status._QPLL0_Lock()
            allFields["CPLL_Lock_Change"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_PLL_Status._CPLL_Lock_Change()
            allFields["CPLL_Lock"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_PLL_Status._CPLL_Lock()
            return allFields

    class _OC192_MUX_OC48_TX_Reset(AtRegister.AtRegister):
        def name(self):
            return "OC192 MUX OC48 TX Reset"
    
        def description(self):
            return "Reset TX SERDES, there are 4 groups (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000C+$G*0x2000"
            
        def startAddress(self):
            return 0x0000000c
            
        def endAddress(self):
            return 0x0000600c

        class _txrst_done(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "txrst_done"
            
            def description(self):
                return "TX Reset Done, bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, Group 2 => Port 8-11, Group 3 => Port 12-15"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _txrst_trig(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txrst_trig"
            
            def description(self):
                return "Trige 0->1 to start reset TX SERDES, bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, Group 2 => Port 8-11, Group 3 => Port 12-15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txrst_done"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TX_Reset._txrst_done()
            allFields["txrst_trig"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TX_Reset._txrst_trig()
            return allFields

    class _OC192_MUX_OC48_RX_Reset(AtRegister.AtRegister):
        def name(self):
            return "OC192 MUX OC48 RX Reset"
    
        def description(self):
            return "Reset RX SERDES, there are 4 groups (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000D+$G*0x2000"
            
        def startAddress(self):
            return 0x0000000d
            
        def endAddress(self):
            return 0x0000600d

        class _rxrst_done(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "rxrst_done"
            
            def description(self):
                return "RX Reset Done, bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, Group 2 => Port 8-11, Group 3 => Port 12-15"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _rxrst_trig(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxrst_trig"
            
            def description(self):
                return "Trige 0->1 to start reset RX SERDES, bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, Group 2 => Port 8-11, Group 3 => Port 12-15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxrst_done"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_RX_Reset._rxrst_done()
            allFields["rxrst_trig"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_RX_Reset._rxrst_trig()
            return allFields

    class _OC192_MUX_OC48_LPMDFE_Mode(AtRegister.AtRegister):
        def name(self):
            return "OC192 MUX OC48 LPMDFE Mode"
    
        def description(self):
            return "Configure LPM/DFE mode , there are 4 groups (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return "0x000E+$G*0x2000"
            
        def startAddress(self):
            return 0x0000000e
            
        def endAddress(self):
            return 0x0000600e

        class _lpmdfe_mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lpmdfe_mode"
            
            def description(self):
                return "bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, , Group 2 => Port 8-11, Group 3 => Port 12-15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lpmdfe_mode"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_LPMDFE_Mode._lpmdfe_mode()
            return allFields

    class _OC192_MUX_OC48_LPMDFE_Reset(AtRegister.AtRegister):
        def name(self):
            return "OC192 MUX OC48 LPMDFE Reset"
    
        def description(self):
            return "Reset LPM/DFE , there are 4 groups (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return "0x000F+$G*0x2000"
            
        def startAddress(self):
            return 0x0000000f
            
        def endAddress(self):
            return 0x0000600f

        class _lpmdfe_reset(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lpmdfe_reset"
            
            def description(self):
                return "bit per sub port, Must be toggled after switching between modes to initialize adaptation, Group 0 => Port0-3, Group 1 => Port 4-7, , Group 2 => Port 8-11, Group 3 => Port 12-15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lpmdfe_reset"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_LPMDFE_Reset._lpmdfe_reset()
            return allFields

    class _OC192_MUX_OC48_TXDIFFCTRL(AtRegister.AtRegister):
        def name(self):
            return "OC192 MUX OC48 TXDIFFCTRL"
    
        def description(self):
            return "Driver Swing Control, see \"Table 3-35: TX Configurable Driver Ports\" page 158 of UG578 for more detail, there are 4 groups (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return "0x0010+$G*0x2000"
            
        def startAddress(self):
            return 0x00000010
            
        def endAddress(self):
            return 0x00006010

        class _TXDIFFCTRL_subport3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 15
        
            def name(self):
                return "TXDIFFCTRL_subport3"
            
            def description(self):
                return "Group 3 => Port 12-15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TXDIFFCTRL_subport2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 10
        
            def name(self):
                return "TXDIFFCTRL_subport2"
            
            def description(self):
                return "Group 2 => Port 8-11"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TXDIFFCTRL_subport1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 5
        
            def name(self):
                return "TXDIFFCTRL_subport1"
            
            def description(self):
                return "Group 1 => Port 4-7"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TXDIFFCTRL_subport0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TXDIFFCTRL_subport0"
            
            def description(self):
                return "Group 0 => Port 0-3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TXDIFFCTRL_subport3"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TXDIFFCTRL._TXDIFFCTRL_subport3()
            allFields["TXDIFFCTRL_subport2"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TXDIFFCTRL._TXDIFFCTRL_subport2()
            allFields["TXDIFFCTRL_subport1"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TXDIFFCTRL._TXDIFFCTRL_subport1()
            allFields["TXDIFFCTRL_subport0"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TXDIFFCTRL._TXDIFFCTRL_subport0()
            return allFields

    class _OC192_MUX_OC48_TXPOSTCURSOR(AtRegister.AtRegister):
        def name(self):
            return "OC192 MUX OC48 TXPOSTCURSOR"
    
        def description(self):
            return "Transmitter post-cursor TX pre-emphasis control, see \"Table 3-35: TX Configurable Driver Ports\" page 160 of UG578 for more detail, there are 4 groups (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return "0x0011+$G*0x2000"
            
        def startAddress(self):
            return 0x00000011
            
        def endAddress(self):
            return 0x00006011

        class _TXPOSTCURSOR_subport3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 15
        
            def name(self):
                return "TXPOSTCURSOR_subport3"
            
            def description(self):
                return "Group 3 => Port 12-15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TXPOSTCURSOR_subport2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 10
        
            def name(self):
                return "TXPOSTCURSOR_subport2"
            
            def description(self):
                return "Group 2 => Port 8-11"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TXPOSTCURSOR_subport1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 5
        
            def name(self):
                return "TXPOSTCURSOR_subport1"
            
            def description(self):
                return "Group 1 => Port 4-7"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TXPOSTCURSOR_subport0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TXPOSTCURSOR_subport0"
            
            def description(self):
                return "Group 0 => Port 0-3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TXPOSTCURSOR_subport3"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TXPOSTCURSOR._TXPOSTCURSOR_subport3()
            allFields["TXPOSTCURSOR_subport2"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TXPOSTCURSOR._TXPOSTCURSOR_subport2()
            allFields["TXPOSTCURSOR_subport1"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TXPOSTCURSOR._TXPOSTCURSOR_subport1()
            allFields["TXPOSTCURSOR_subport0"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TXPOSTCURSOR._TXPOSTCURSOR_subport0()
            return allFields

    class _OC192_MUX_OC48_TXPRECURSOR(AtRegister.AtRegister):
        def name(self):
            return "OC192 MUX OC48 TXPRECURSOR"
    
        def description(self):
            return "Transmitter pre-cursor TX pre-emphasis control, see \"Table 3-35: TX Configurable Driver Ports\" page 161 of UG578 for more detail, there are 4 groups (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return "0x0012+$G*0x2000"
            
        def startAddress(self):
            return 0x00000012
            
        def endAddress(self):
            return 0x00006012

        class _TXPRECURSOR_subport3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 15
        
            def name(self):
                return "TXPRECURSOR_subport3"
            
            def description(self):
                return "Group 3 => Port 12-15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TXPRECURSOR_subport2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 10
        
            def name(self):
                return "TXPRECURSOR_subport2"
            
            def description(self):
                return "Group 2 => Port 8-11"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TXPRECURSOR_subport1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 5
        
            def name(self):
                return "TXPRECURSOR_subport1"
            
            def description(self):
                return "Group 1 => Port 4-7"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TXPRECURSOR_subport0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TXPRECURSOR_subport0"
            
            def description(self):
                return "Group 0 => Port 0-3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TXPRECURSOR_subport3"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TXPRECURSOR._TXPRECURSOR_subport3()
            allFields["TXPRECURSOR_subport2"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TXPRECURSOR._TXPRECURSOR_subport2()
            allFields["TXPRECURSOR_subport1"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TXPRECURSOR._TXPRECURSOR_subport1()
            allFields["TXPRECURSOR_subport0"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TXPRECURSOR._TXPRECURSOR_subport0()
            return allFields

    class _SERDES_POWER_DOWN(AtRegister.AtRegister):
        def name(self):
            return "SERDES POWER DOWN"
    
        def description(self):
            return "TX/RX power down control, see \"UG578 for more detail, there are 4 groups (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0014+$G*0x2000"
            
        def startAddress(self):
            return 0x00000014
            
        def endAddress(self):
            return 0x00006014

        class _TXRXPD3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "TXRXPD3"
            
            def description(self):
                return "Power Down subport 3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TXRXPD2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "TXRXPD2"
            
            def description(self):
                return "Power Down subport 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TXRXPD1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "TXRXPD1"
            
            def description(self):
                return "Power Down subport 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TXRXPD0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TXRXPD0"
            
            def description(self):
                return "Power Down subport 0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TXRXPD3"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._SERDES_POWER_DOWN._TXRXPD3()
            allFields["TXRXPD2"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._SERDES_POWER_DOWN._TXRXPD2()
            allFields["TXRXPD1"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._SERDES_POWER_DOWN._TXRXPD1()
            allFields["TXRXPD0"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._SERDES_POWER_DOWN._TXRXPD0()
            return allFields

    class _Force_Running_Disparity_Error(AtRegister.AtRegister):
        def name(self):
            return "Force Running Disparity Error"
    
        def description(self):
            return "Force running disparity error, there are 4 groups (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0015+$G*0x2000"
            
        def startAddress(self):
            return 0x00000015
            
        def endAddress(self):
            return 0x00006015

        class _Fdisp0_pid3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Fdisp0_pid3"
            
            def description(self):
                return "Force running disparity error for port 3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Fdisp0_pid2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Fdisp0_pid2"
            
            def description(self):
                return "Force running disparity error for port 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Fdisp0_pid1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Fdisp0_pid1"
            
            def description(self):
                return "Force running disparity error for port 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Fdisp0_pid0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Fdisp0_pid0"
            
            def description(self):
                return "Force running disparity error for port 0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Fdisp0_pid3"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._Force_Running_Disparity_Error._Fdisp0_pid3()
            allFields["Fdisp0_pid2"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._Force_Running_Disparity_Error._Fdisp0_pid2()
            allFields["Fdisp0_pid1"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._Force_Running_Disparity_Error._Fdisp0_pid1()
            allFields["Fdisp0_pid0"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._Force_Running_Disparity_Error._Fdisp0_pid0()
            return allFields

    class _RX_CDR_Lock_to_Reference(AtRegister.AtRegister):
        def name(self):
            return "RX CDR Lock to Reference"
    
        def description(self):
            return "RX CDR Lock to Reference, there are 4 groups (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0017+$G*0x2000"
            
        def startAddress(self):
            return 0x00000017
            
        def endAddress(self):
            return 0x00006017

        class _lock2ref_pid3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "lock2ref_pid3"
            
            def description(self):
                return "RX CDR Lock to Reference for port 3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lock2ref_pid2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "lock2ref_pid2"
            
            def description(self):
                return "RX CDR Lock to Reference for port 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lock2ref_pid1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "lock2ref_pid1"
            
            def description(self):
                return "RX CDR Lock to Reference for port 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lock2ref_pid0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lock2ref_pid0"
            
            def description(self):
                return "RX CDR Lock to Reference for port 0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lock2ref_pid3"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_CDR_Lock_to_Reference._lock2ref_pid3()
            allFields["lock2ref_pid2"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_CDR_Lock_to_Reference._lock2ref_pid2()
            allFields["lock2ref_pid1"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_CDR_Lock_to_Reference._lock2ref_pid1()
            allFields["lock2ref_pid0"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_CDR_Lock_to_Reference._lock2ref_pid0()
            return allFields

    class _RX_Monitor_PPM_Ref_Value(AtRegister.AtRegister):
        def name(self):
            return "RX Monitor PPM Ref Value"
    
        def description(self):
            return "RX Monitor PPM Ref Value, this reg is used to reset RX side base PPM difference between TX & RX, there are 4 groups (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x001C+$G*0x2000"
            
        def startAddress(self):
            return 0x0000001c
            
        def endAddress(self):
            return 0x0000601c

        class _ppmref_pid3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "ppmref_pid3"
            
            def description(self):
                return "RX Monitor PPM Ref Value for port 3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _ppmref_pid2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ppmref_pid2"
            
            def description(self):
                return "RX Monitor PPM Ref Value for port 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _ppmref_pid1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "ppmref_pid1"
            
            def description(self):
                return "RX Monitor PPM Ref Value for port 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _ppmref_pid0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ppmref_pid0"
            
            def description(self):
                return "RX Monitor PPM Ref Value for port 0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ppmref_pid3"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_Monitor_PPM_Ref_Value._ppmref_pid3()
            allFields["ppmref_pid2"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_Monitor_PPM_Ref_Value._ppmref_pid2()
            allFields["ppmref_pid1"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_Monitor_PPM_Ref_Value._ppmref_pid1()
            allFields["ppmref_pid0"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_Monitor_PPM_Ref_Value._ppmref_pid0()
            return allFields

    class _RX_Monitor_PPM_Mon_Value(AtRegister.AtRegister):
        def name(self):
            return "RX Monitor PPM Mon Value"
    
        def description(self):
            return "RX Monitor PPM Value, this reg is used to reset RX side base PPM difference between TX & RX, it will be compared to reg \"RX Monitor PPM Ref Value\", there are 4 groups (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x001D+$G*0x2000"
            
        def startAddress(self):
            return 0x0000001d
            
        def endAddress(self):
            return 0x0000601d

        class _ppmval_pid3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "ppmval_pid3"
            
            def description(self):
                return "RX Monitor PPM Mon Value for port 3"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _ppmval_pid2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ppmval_pid2"
            
            def description(self):
                return "RX Monitor PPM Mon Value for port 2"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _ppmval_pid1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "ppmval_pid1"
            
            def description(self):
                return "RX Monitor PPM Mon Value for port 1"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _ppmval_pid0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ppmval_pid0"
            
            def description(self):
                return "RX Monitor PPM Mon Value for port 0"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ppmval_pid3"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_Monitor_PPM_Mon_Value._ppmval_pid3()
            allFields["ppmval_pid2"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_Monitor_PPM_Mon_Value._ppmval_pid2()
            allFields["ppmval_pid1"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_Monitor_PPM_Mon_Value._ppmval_pid1()
            allFields["ppmval_pid0"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_Monitor_PPM_Mon_Value._ppmval_pid0()
            return allFields

    class _RX_Monitor_PPM_Control(AtRegister.AtRegister):
        def name(self):
            return "RX Monitor PPM Control"
    
        def description(self):
            return "RX Monitor PPM Control, this reg is used to reset RX side base PPM difference between TX & RX, there are 4 groups (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x001E+$G*0x2000"
            
        def startAddress(self):
            return 0x0000001e
            
        def endAddress(self):
            return 0x0000601e

        class _ppmctrl_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ppmctrl_err"
            
            def description(self):
                return "RX Monitor PPM gets an error, bit per port of each group, bit0 is port0, bit3 is port3"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _ppmctrl_enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ppmctrl_enb"
            
            def description(self):
                return "Enable monitor PPM difference, bit per port of each group, bit0 is port0, bit3 is port3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ppmctrl_err"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_Monitor_PPM_Control._ppmctrl_err()
            allFields["ppmctrl_enb"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_Monitor_PPM_Control._ppmctrl_enb()
            return allFields

    class _RX_RXLPMHFOVRDEN_Control(AtRegister.AtRegister):
        def name(self):
            return "RX RXLPMHFOVRDEN Control"
    
        def description(self):
            return "RX RXLPMHFOVRDEN Control is used for lock-to-ref mode, there are 4 groups (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0018+$G*0x2000"
            
        def startAddress(self):
            return 0x00000018
            
        def endAddress(self):
            return 0x00006018

        class _rxlpmhfovrden(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxlpmhfovrden"
            
            def description(self):
                return "RX RXLPMHFOVRDEN Control, bit per port of each group, bit0 is port0, bit3 is port3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxlpmhfovrden"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_RXLPMHFOVRDEN_Control._rxlpmhfovrden()
            return allFields

    class _RX_RXLPMLFKLOVRDEN_Control(AtRegister.AtRegister):
        def name(self):
            return "RX RXLPMLFKLOVRDEN Control"
    
        def description(self):
            return "RX RXLPMLFKLOVRDEN Control is used for lock-to-ref mode, there are 4 groups (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0019+$G*0x2000"
            
        def startAddress(self):
            return 0x00000019
            
        def endAddress(self):
            return 0x00006019

        class _rxlpmlfklovrden(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxlpmlfklovrden"
            
            def description(self):
                return "RX RXLPMLFKLOVRDEN Control, bit per port of each group, bit0 is port0, bit3 is port3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxlpmlfklovrden"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_RXLPMLFKLOVRDEN_Control._rxlpmlfklovrden()
            return allFields

    class _RX_RXOSOVRDEN_Control(AtRegister.AtRegister):
        def name(self):
            return "RX RXOSOVRDEN Control"
    
        def description(self):
            return "RX RXOSOVRDEN Control is used for lock-to-ref mode, there are 4 groups (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x001A+$G*0x2000"
            
        def startAddress(self):
            return 0x0000001a
            
        def endAddress(self):
            return 0x0000601a

        class _rxosovrden(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxosovrden"
            
            def description(self):
                return "RX RXOSOVRDEN Control, bit per port of each group, bit0 is port0, bit3 is port3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxosovrden"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_RXOSOVRDEN_Control._rxosovrden()
            return allFields

    class _RX_RXLPMOSHOLD_Control(AtRegister.AtRegister):
        def name(self):
            return "RX RXLPMOSHOLD Control"
    
        def description(self):
            return "RX RXLPMOSHOLD Control, there are 4 groups (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0030+$G*0x2000"
            
        def startAddress(self):
            return 0x00000030
            
        def endAddress(self):
            return 0x00006030

        class _rxlpmoshold(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxlpmoshold"
            
            def description(self):
                return "RX RXLPMOSHOLD Control, bit per port of each group, bit0 is port0, bit3 is port3, refer to UG578 for usage"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxlpmoshold"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_RXLPMOSHOLD_Control._rxlpmoshold()
            return allFields

    class _RX_RXLPMOSOVRDEN_Control(AtRegister.AtRegister):
        def name(self):
            return "RX RXLPMOSOVRDEN Control"
    
        def description(self):
            return "RX RXLPMOSOVRDEN Control, there are 4 groups (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0031+$G*0x2000"
            
        def startAddress(self):
            return 0x00000031
            
        def endAddress(self):
            return 0x00006031

        class _rxlpmosovrden(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxlpmosovrden"
            
            def description(self):
                return "RX RXLPMOSOVRDEN Control, bit per port of each group, bit0 is port0, bit3 is port3, refer to UG578 for usage"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxlpmosovrden"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_RXLPMOSOVRDEN_Control._rxlpmosovrden()
            return allFields

    class _RX_RXLPM_OS_CFG1_Control(AtRegister.AtRegister):
        def name(self):
            return "RX RXLPM_OS_CFG1 Control"
    
        def description(self):
            return "RX RXLPM_OS_CFG1 Control, there are 4 groups (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1038+$G*0x2000+$P*0x400"
            
        def startAddress(self):
            return 0x00001038
            
        def endAddress(self):
            return 0x00007c38

        class _rxlpm_os_cfg1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxlpm_os_cfg1"
            
            def description(self):
                return "RX RXLPM_OS_CFG1 Control, refer to UG578 for usage"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxlpm_os_cfg1"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_RXLPM_OS_CFG1_Control._rxlpm_os_cfg1()
            return allFields

    class _RX_RXLPMGCHOLD_Control(AtRegister.AtRegister):
        def name(self):
            return "RX RXLPMGCHOLD Control"
    
        def description(self):
            return "RX RXLPMGCHOLD Control, there are 4 groups (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0032+$G*0x2000"
            
        def startAddress(self):
            return 0x00000032
            
        def endAddress(self):
            return 0x00006032

        class _rxlpmgchold(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxlpmgchold"
            
            def description(self):
                return "RX RXLPMGCHOLD Control, bit per port of each group, bit0 is port0, bit3 is port3, refer to UG578 for usage"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxlpmgchold"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_RXLPMGCHOLD_Control._rxlpmgchold()
            return allFields

    class _RX_RXLPMGCOVRDEN_CONTROL(AtRegister.AtRegister):
        def name(self):
            return "RX RXLPMGCOVRDEN Control"
    
        def description(self):
            return "RX RXLPMGCOVRDEN Control, there are 4 groups (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0033+$G*0x2000"
            
        def startAddress(self):
            return 0x00000033
            
        def endAddress(self):
            return 0x00006033

        class _rxlpmgcovrden(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxlpmgcovrden"
            
            def description(self):
                return "RX RXLPMGCOVRDEN Control, bit per port of each group, bit0 is port0, bit3 is port3, refer to UG578 for usage"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxlpmgcovrden"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_RXLPMGCOVRDEN_CONTROL._rxlpmgcovrden()
            return allFields

    class _RX_RXLPM_GC_CFG_Control(AtRegister.AtRegister):
        def name(self):
            return "RX RXLPM_GC_CFG Control"
    
        def description(self):
            return "RX RXLPM_GC_CFG Control, there are 4 groups (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1039+$G*0x2000+$P*0x400"
            
        def startAddress(self):
            return 0x00001039
            
        def endAddress(self):
            return 0x00007c39

        class _rxlpm_gc_cfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxlpm_gc_cfg"
            
            def description(self):
                return "RX RXLPM_GC_CFG Control, refer to UG578 for usage"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxlpm_gc_cfg"] = _AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD._RX_RXLPM_GC_CFG_Control._rxlpm_gc_cfg()
            return allFields

import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["OC192_MUX_OC48_Change_Mode"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_Change_Mode()
        allRegisters["OC192_MUX_OC48_Current_Mode"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_Current_Mode()
        allRegisters["OC192_MUX_OC48_DRP"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_DRP()
        allRegisters["OC192_MUX_OC48_LoopBack"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_LoopBack()
        allRegisters["OC192_MUX_OC48_PLL_Status"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_PLL_Status()
        allRegisters["OC192_MUX_OC48_TX_Reset"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TX_Reset()
        allRegisters["OC192_MUX_OC48_RX_Reset"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_RX_Reset()
        allRegisters["OC192_MUX_OC48_LPMDFE_Mode"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_LPMDFE_Mode()
        allRegisters["OC192_MUX_OC48_LPMDFE_Reset"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_LPMDFE_Reset()
        allRegisters["OC192_MUX_OC48_TXDIFFCTRL"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TXDIFFCTRL()
        allRegisters["OC192_MUX_OC48_TXPOSTCURSOR"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TXPOSTCURSOR()
        allRegisters["OC192_MUX_OC48_TXPRECURSOR"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TXPRECURSOR()
        return allRegisters

    class _OC192_MUX_OC48_Change_Mode(AtRegister.AtRegister):
        def name(self):
            return "OC192 MUX OC48 Change Mode"
    
        def description(self):
            return "Configurate Port mode, there is 4 group (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0080+$G*0x2000"
            
        def startAddress(self):
            return 0x00000080
            
        def endAddress(self):
            return 0x00006080

        class _chg_done(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "chg_done"
            
            def description(self):
                return "Change mode has done"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _chg_trig(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "chg_trig"
            
            def description(self):
                return "Trigger 0->1 to start changing mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _chg_mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "chg_mode"
            
            def description(self):
                return "Port mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _chg_port(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "chg_port"
            
            def description(self):
                return "Sub Port ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["chg_done"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_Change_Mode._chg_done()
            allFields["chg_trig"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_Change_Mode._chg_trig()
            allFields["chg_mode"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_Change_Mode._chg_mode()
            allFields["chg_port"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_Change_Mode._chg_port()
            return allFields

    class _OC192_MUX_OC48_Current_Mode(AtRegister.AtRegister):
        def name(self):
            return "OC192 MUX OC48 Current Mode"
    
        def description(self):
            return "Current Port mode, there is 4 group (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0090+$G*0x2000"
            
        def startAddress(self):
            return 0x00000090
            
        def endAddress(self):
            return 0x00006090

        class _cur_mode_subport3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "cur_mode_subport3"
            
            def description(self):
                return "Current mode subport3, Group 0 => Port3, Group 1 => Port 7, Group 2 => Port 11, Group 3 => Port 15"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _cur_mode_subport2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "cur_mode_subport2"
            
            def description(self):
                return "Current mode subport2, Group 0 => Port2, Group 1 => Port 6, Group 2 => Port 10, Group 3 => Port 14"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _cur_mode_subport1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "cur_mode_subport1"
            
            def description(self):
                return "Current mode subport1, Group 0 => Port1, Group 1 => Port 5, Group 2 => Port 9, Group 3 => Port 13"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _cur_mode_subport0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cur_mode_subport0"
            
            def description(self):
                return "Current mode subport0, Group 0 => Port0, Group 1 => Port 4, Group 2 => Port 8, Group 3 => Port 12"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cur_mode_subport3"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_Current_Mode._cur_mode_subport3()
            allFields["cur_mode_subport2"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_Current_Mode._cur_mode_subport2()
            allFields["cur_mode_subport1"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_Current_Mode._cur_mode_subport1()
            allFields["cur_mode_subport0"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_Current_Mode._cur_mode_subport0()
            return allFields

    class _OC192_MUX_OC48_DRP(AtRegister.AtRegister):
        def name(self):
            return "OC192 MUX OC48 DRP"
    
        def description(self):
            return "Read/Write DRP address of SERDES, there is 4 group (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1000+$G*0x2000+$P*0x400+$DRP"
            
        def startAddress(self):
            return 0x00001000
            
        def endAddress(self):
            return 0x00007fff

        class _drp_rw(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "drp_rw"
            
            def description(self):
                return "DRP read/write value"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["drp_rw"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_DRP._drp_rw()
            return allFields

    class _OC192_MUX_OC48_LoopBack(AtRegister.AtRegister):
        def name(self):
            return "OC192 MUX OC48 LoopBack"
    
        def description(self):
            return "Configurate LoopBack, there is 4 group (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0002+$G*0x2000"
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0x00006002

        class _lpback_subport3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "lpback_subport3"
            
            def description(self):
                return "Loopback subport 3, Group 0 => Port3, Group 1 => Port 7, Group 2 => Port 11, Group 3 => Port 15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lpback_subport2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "lpback_subport2"
            
            def description(self):
                return "Loopback subport 2, Group 0 => Port2, Group 1 => Port 6, Group 2 => Port 10, Group 3 => Port 14"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lpback_subport1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "lpback_subport1"
            
            def description(self):
                return "Loopback subport 1, Group 0 => Port1, Group 1 => Port 5, Group 2 => Port 9, Group 3 => Port 13"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lpback_subport0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lpback_subport0"
            
            def description(self):
                return "Loopback subport 0, Group 0 => Port0, Group 1 => Port 4, Group 2 => Port 8, Group 3 => Port 12"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lpback_subport3"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_LoopBack._lpback_subport3()
            allFields["lpback_subport2"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_LoopBack._lpback_subport2()
            allFields["lpback_subport1"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_LoopBack._lpback_subport1()
            allFields["lpback_subport0"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_LoopBack._lpback_subport0()
            return allFields

    class _OC192_MUX_OC48_PLL_Status(AtRegister.AtRegister):
        def name(self):
            return "OC192 MUX OC48 PLL Status"
    
        def description(self):
            return "QPLL/CPLL status, there is 4 group (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000B+$G*0x2000"
            
        def startAddress(self):
            return 0x0000000b
            
        def endAddress(self):
            return 0x0000600b

        class _QPLL1_Lock_change(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "QPLL1_Lock_change"
            
            def description(self):
                return "QPLL1 has transition lock/unlock, Group 0-3"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _QPLL0_Lock_change(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "QPLL0_Lock_change"
            
            def description(self):
                return "QPLL0 has transition lock/unlock, Group 0-3"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _QPLL1_Lock(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "QPLL1_Lock"
            
            def description(self):
                return "QPLL0 is Locked, Group 0-3"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _QPLL0_Lock(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "QPLL0_Lock"
            
            def description(self):
                return "QPLL0 is Locked, Group 0-3"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _CPLL_Lock_Change(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "CPLL_Lock_Change"
            
            def description(self):
                return "CPLL has transition lock/unlock, bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, Group 2 => Port 8-11, Group 3 => Port 12-15"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _CPLL_Lock(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CPLL_Lock"
            
            def description(self):
                return "CPLL is Locked, bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, Group 2 => Port 8-11, Group 3 => Port 12-15"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["QPLL1_Lock_change"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_PLL_Status._QPLL1_Lock_change()
            allFields["QPLL0_Lock_change"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_PLL_Status._QPLL0_Lock_change()
            allFields["QPLL1_Lock"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_PLL_Status._QPLL1_Lock()
            allFields["QPLL0_Lock"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_PLL_Status._QPLL0_Lock()
            allFields["CPLL_Lock_Change"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_PLL_Status._CPLL_Lock_Change()
            allFields["CPLL_Lock"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_PLL_Status._CPLL_Lock()
            return allFields

    class _OC192_MUX_OC48_TX_Reset(AtRegister.AtRegister):
        def name(self):
            return "OC192 MUX OC48 TX Reset"
    
        def description(self):
            return "Reset TX SERDES, there is 4 group (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000C+$G*0x2000"
            
        def startAddress(self):
            return 0x0000000c
            
        def endAddress(self):
            return 0x0000600c

        class _txrst_done(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "txrst_done"
            
            def description(self):
                return "TX Reset Done, bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, Group 2 => Port 8-11, Group 3 => Port 12-15"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _txrst_trig(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txrst_trig"
            
            def description(self):
                return "Trige 0->1 to start reset TX SERDES, bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, Group 2 => Port 8-11, Group 3 => Port 12-15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txrst_done"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TX_Reset._txrst_done()
            allFields["txrst_trig"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TX_Reset._txrst_trig()
            return allFields

    class _OC192_MUX_OC48_RX_Reset(AtRegister.AtRegister):
        def name(self):
            return "OC192 MUX OC48 RX Reset"
    
        def description(self):
            return "Reset RX SERDES, there is 4 group (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000D+$G*0x2000"
            
        def startAddress(self):
            return 0x0000000d
            
        def endAddress(self):
            return 0x0000600d

        class _rxrst_done(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "rxrst_done"
            
            def description(self):
                return "RX Reset Done, bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, Group 2 => Port 8-11, Group 3 => Port 12-15"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _rxrst_trig(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxrst_trig"
            
            def description(self):
                return "Trige 0->1 to start reset RX SERDES, bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, Group 2 => Port 8-11, Group 3 => Port 12-15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxrst_done"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_RX_Reset._rxrst_done()
            allFields["rxrst_trig"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_RX_Reset._rxrst_trig()
            return allFields

    class _OC192_MUX_OC48_LPMDFE_Mode(AtRegister.AtRegister):
        def name(self):
            return "OC192 MUX OC48 LPMDFE Mode"
    
        def description(self):
            return "Configure LPM/DFE mode , there is 4 group (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return "0x000E+$G*0x2000"
            
        def startAddress(self):
            return 0x0000000e
            
        def endAddress(self):
            return 0x0000600e

        class _lpmdfe_mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lpmdfe_mode"
            
            def description(self):
                return "bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, , Group 2 => Port 8-11, Group 3 => Port 12-15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lpmdfe_mode"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_LPMDFE_Mode._lpmdfe_mode()
            return allFields

    class _OC192_MUX_OC48_LPMDFE_Reset(AtRegister.AtRegister):
        def name(self):
            return "OC192 MUX OC48 LPMDFE Reset"
    
        def description(self):
            return "Reset LPM/DFE , there is 4 group (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return "0x000F+$G*0x2000"
            
        def startAddress(self):
            return 0x0000000f
            
        def endAddress(self):
            return 0x0000600f

        class _lpmdfe_reset(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lpmdfe_reset"
            
            def description(self):
                return "bit per sub port, Must be toggled after switching between modes to initialize adaptation, Group 0 => Port0-3, Group 1 => Port 4-7, , Group 2 => Port 8-11, Group 3 => Port 12-15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lpmdfe_reset"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_LPMDFE_Reset._lpmdfe_reset()
            return allFields

    class _OC192_MUX_OC48_TXDIFFCTRL(AtRegister.AtRegister):
        def name(self):
            return "OC192 MUX OC48 TXDIFFCTRL"
    
        def description(self):
            return "Driver Swing Control, see \"Table 3-35: TX Configurable Driver Ports\" page 158 of UG578 for more detail, there is 4 group (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return "0x0010+$G*0x2000"
            
        def startAddress(self):
            return 0x00000010
            
        def endAddress(self):
            return 0x00006010

        class _TXDIFFCTRL_subport3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 15
        
            def name(self):
                return "TXDIFFCTRL_subport3"
            
            def description(self):
                return "Group 3 => Port 12-15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TXDIFFCTRL_subport2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 10
        
            def name(self):
                return "TXDIFFCTRL_subport2"
            
            def description(self):
                return "Group 2 => Port 8-11"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TXDIFFCTRL_subport1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 5
        
            def name(self):
                return "TXDIFFCTRL_subport1"
            
            def description(self):
                return "Group 1 => Port 4-7"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TXDIFFCTRL_subport0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TXDIFFCTRL_subport0"
            
            def description(self):
                return "Group 0 => Port 0-3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TXDIFFCTRL_subport3"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TXDIFFCTRL._TXDIFFCTRL_subport3()
            allFields["TXDIFFCTRL_subport2"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TXDIFFCTRL._TXDIFFCTRL_subport2()
            allFields["TXDIFFCTRL_subport1"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TXDIFFCTRL._TXDIFFCTRL_subport1()
            allFields["TXDIFFCTRL_subport0"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TXDIFFCTRL._TXDIFFCTRL_subport0()
            return allFields

    class _OC192_MUX_OC48_TXPOSTCURSOR(AtRegister.AtRegister):
        def name(self):
            return "OC192 MUX OC48 TXPOSTCURSOR"
    
        def description(self):
            return "Transmitter post-cursor TX pre-emphasis control, see \"Table 3-35: TX Configurable Driver Ports\" page 160 of UG578 for more detail, there is 4 group (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return "0x0011+$G*0x2000"
            
        def startAddress(self):
            return 0x00000011
            
        def endAddress(self):
            return 0x00006011

        class _TXPOSTCURSOR_subport3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 15
        
            def name(self):
                return "TXPOSTCURSOR_subport3"
            
            def description(self):
                return "Group 3 => Port 12-15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TXPOSTCURSOR_subport2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 10
        
            def name(self):
                return "TXPOSTCURSOR_subport2"
            
            def description(self):
                return "Group 2 => Port 8-11"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TXPOSTCURSOR_subport1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 5
        
            def name(self):
                return "TXPOSTCURSOR_subport1"
            
            def description(self):
                return "Group 1 => Port 4-7"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TXPOSTCURSOR_subport0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TXPOSTCURSOR_subport0"
            
            def description(self):
                return "Group 0 => Port 0-3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TXPOSTCURSOR_subport3"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TXPOSTCURSOR._TXPOSTCURSOR_subport3()
            allFields["TXPOSTCURSOR_subport2"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TXPOSTCURSOR._TXPOSTCURSOR_subport2()
            allFields["TXPOSTCURSOR_subport1"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TXPOSTCURSOR._TXPOSTCURSOR_subport1()
            allFields["TXPOSTCURSOR_subport0"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TXPOSTCURSOR._TXPOSTCURSOR_subport0()
            return allFields

    class _OC192_MUX_OC48_TXPRECURSOR(AtRegister.AtRegister):
        def name(self):
            return "OC192 MUX OC48 TXPRECURSOR"
    
        def description(self):
            return "Transmitter pre-cursor TX pre-emphasis control, see \"Table 3-35: TX Configurable Driver Ports\" page 161 of UG578 for more detail, there is 4 group (0-3), each group has 4 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return "0x0011+$G*0x2000"
            
        def startAddress(self):
            return 0x00000011
            
        def endAddress(self):
            return 0x00006011

        class _TXPRECURSOR_subport3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 15
        
            def name(self):
                return "TXPRECURSOR_subport3"
            
            def description(self):
                return "Group 3 => Port 12-15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TXPRECURSOR_subport2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 10
        
            def name(self):
                return "TXPRECURSOR_subport2"
            
            def description(self):
                return "Group 2 => Port 8-11"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TXPRECURSOR_subport1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 5
        
            def name(self):
                return "TXPRECURSOR_subport1"
            
            def description(self):
                return "Group 1 => Port 4-7"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TXPRECURSOR_subport0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TXPRECURSOR_subport0"
            
            def description(self):
                return "Group 0 => Port 0-3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TXPRECURSOR_subport3"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TXPRECURSOR._TXPRECURSOR_subport3()
            allFields["TXPRECURSOR_subport2"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TXPRECURSOR._TXPRECURSOR_subport2()
            allFields["TXPRECURSOR_subport1"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TXPRECURSOR._TXPRECURSOR_subport1()
            allFields["TXPRECURSOR_subport0"] = _AF6CNC0011_OC192_MUX_OC48_16ch_wrap_RD._OC192_MUX_OC48_TXPRECURSOR._TXPRECURSOR_subport0()
            return allFields

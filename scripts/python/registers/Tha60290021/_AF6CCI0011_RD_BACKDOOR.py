import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0011_RD_BACKDOOR(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["rtljitbuf.ramjitbufcfg.ram.ram[]"] = _AF6CCI0011_RD_BACKDOOR._rtljitbuf.ramjitbufcfg.ram.ram[]()
        allRegisters["ramreorcfg.ram.ram[]"] = _AF6CCI0011_RD_BACKDOOR._ramreorcfg.ram.ram[]()
        allRegisters["rtlpdatdm.rtl20glotdm.rtllodeas.ramdeascfg0.ram.ram[],_this_is_for_slice48_1_4_rtlpdatdm.rtl20glotdm.rtllodeas.ramdeascfg1.ram.ram[],_this_is_for_slice48_5_6"] = _AF6CCI0011_RD_BACKDOOR._rtlpdatdm.rtl20glotdm.rtllodeas.ramdeascfg0.ram.ram[],_this_is_for_slice48_1_4_rtlpdatdm.rtl20glotdm.rtllodeas.ramdeascfg1.ram.ram[],_this_is_for_slice48_5_6()
        allRegisters["rtlpdatdm.rampwlkcfglo.ram.ram[]"] = _AF6CCI0011_RD_BACKDOOR._rtlpdatdm.rampwlkcfglo.ram.ram[]()
        allRegisters["rtlpdatdm.rampwlkcfgho.ram.ram[]"] = _AF6CCI0011_RD_BACKDOOR._rtlpdatdm.rampwlkcfgho.ram.ram[]()
        allRegisters["rtlpdahotdm.rtl48hodeas[].ramdeascfg.ram.ram[],_where_rtl48hodeas[]_is_module_instant"] = _AF6CCI0011_RD_BACKDOOR._rtlpdahotdm.rtl48hodeas[].ramdeascfg.ram.ram[],_where_rtl48hodeas[]_is_module_instant()
        allRegisters[""] = _AF6CCI0011_RD_BACKDOOR._()
        allRegisters[""] = _AF6CCI0011_RD_BACKDOOR._()
        allRegisters[""] = _AF6CCI0011_RD_BACKDOOR._()
        allRegisters[""] = _AF6CCI0011_RD_BACKDOOR._()
        return allRegisters

    class _rtljitbuf.ramjitbufcfg.ram.ram[](AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA Jitter Buffer Control"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return ""
            
        def fomular(self):
            return "0x00010000 +  PWID"
            
        def startAddress(self):
            return 0x00010000
            
        def endAddress(self):
            return 0xffffffff

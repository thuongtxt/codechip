import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0011_RD_OCN_DIAG(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["glbrfm_reg"] = _AF6CCI0011_RD_OCN_DIAG._glbrfm_reg()
        allRegisters["glbtfm_reg"] = _AF6CCI0011_RD_OCN_DIAG._glbtfm_reg()
        allRegisters["rxfrmsta"] = _AF6CCI0011_RD_OCN_DIAG._rxfrmsta()
        allRegisters["rxfrmstk"] = _AF6CCI0011_RD_OCN_DIAG._rxfrmstk()
        allRegisters["rxfrmb1cntro"] = _AF6CCI0011_RD_OCN_DIAG._rxfrmb1cntro()
        allRegisters["rxfrmb1cntr2c"] = _AF6CCI0011_RD_OCN_DIAG._rxfrmb1cntr2c()
        return allRegisters

    class _glbrfm_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Rx Framer Control"
    
        def description(self):
            return "This is the global configuration register for the Rx Framer"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00f43000
            
        def endAddress(self):
            return 0xffffffff

        class _RxLineSyncSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "RxLineSyncSel"
            
            def description(self):
                return "Select line id for synchronization 8 rx lines. Bit[0] for line 0. Note that must only 1 bit is set"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmLosAisEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "RxFrmLosAisEn"
            
            def description(self):
                return "Enable/disable forwarding P_AIS when LOS detected at Rx Framer. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmOofAisEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "RxFrmOofAisEn"
            
            def description(self):
                return "Enable/disable forwarding P_AIS when OOF detected at Rx Framer. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmB1BlockCntMod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "RxFrmB1BlockCntMod"
            
            def description(self):
                return "B1 Counter Mode. 1: Block mode 0: Bit-wise mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmBadFrmThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 8
        
            def name(self):
                return "RxFrmBadFrmThresh"
            
            def description(self):
                return "Threshold for A1A2 missing counter, that is used to change state from FRAMED to HUNT."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmB1GoodThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RxFrmB1GoodThresh"
            
            def description(self):
                return "Threshold for B1 good counter, that is used to change state from CHECK to FRAMED."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmDescrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "RxFrmDescrEn"
            
            def description(self):
                return "Enable/disable de-scrambling of the Rx coming data stream. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmB1ChkFrmEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "RxFrmB1ChkFrmEn"
            
            def description(self):
                return "Enable/disable B1 check option is added to the required framing algorithm. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmEssiModeEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxFrmEssiModeEn"
            
            def description(self):
                return "TFI-5 mode. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxLineSyncSel"] = _AF6CCI0011_RD_OCN_DIAG._glbrfm_reg._RxLineSyncSel()
            allFields["RxFrmLosAisEn"] = _AF6CCI0011_RD_OCN_DIAG._glbrfm_reg._RxFrmLosAisEn()
            allFields["RxFrmOofAisEn"] = _AF6CCI0011_RD_OCN_DIAG._glbrfm_reg._RxFrmOofAisEn()
            allFields["RxFrmB1BlockCntMod"] = _AF6CCI0011_RD_OCN_DIAG._glbrfm_reg._RxFrmB1BlockCntMod()
            allFields["RxFrmBadFrmThresh"] = _AF6CCI0011_RD_OCN_DIAG._glbrfm_reg._RxFrmBadFrmThresh()
            allFields["RxFrmB1GoodThresh"] = _AF6CCI0011_RD_OCN_DIAG._glbrfm_reg._RxFrmB1GoodThresh()
            allFields["RxFrmDescrEn"] = _AF6CCI0011_RD_OCN_DIAG._glbrfm_reg._RxFrmDescrEn()
            allFields["RxFrmB1ChkFrmEn"] = _AF6CCI0011_RD_OCN_DIAG._glbrfm_reg._RxFrmB1ChkFrmEn()
            allFields["RxFrmEssiModeEn"] = _AF6CCI0011_RD_OCN_DIAG._glbrfm_reg._RxFrmEssiModeEn()
            return allFields

    class _glbtfm_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Tx Framer Control"
    
        def description(self):
            return "This is the global configuration register for the Tx Framer"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00f43001
            
        def endAddress(self):
            return 0xffffffff

        class _TxLineOofFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "TxLineOofFrc"
            
            def description(self):
                return "Enable/disble force OOF for 8 tx lines. Bit[0] for line 0."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxLineB1ErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "TxLineB1ErrFrc"
            
            def description(self):
                return "Enable/disble force B1 error for 8 tx lines. Bit[0] for line 0."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxLineSyncSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 4
        
            def name(self):
                return "TxLineSyncSel"
            
            def description(self):
                return "Select line id for synchronization 8 tx lines. Bit[0] for line 0. Note that must only 1 bit is set"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxFrmScrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxFrmScrEn"
            
            def description(self):
                return "Enable/disable scrambling of the Tx data stream. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxLineOofFrc"] = _AF6CCI0011_RD_OCN_DIAG._glbtfm_reg._TxLineOofFrc()
            allFields["TxLineB1ErrFrc"] = _AF6CCI0011_RD_OCN_DIAG._glbtfm_reg._TxLineB1ErrFrc()
            allFields["TxLineSyncSel"] = _AF6CCI0011_RD_OCN_DIAG._glbtfm_reg._TxLineSyncSel()
            allFields["TxFrmScrEn"] = _AF6CCI0011_RD_OCN_DIAG._glbtfm_reg._TxFrmScrEn()
            return allFields

    class _rxfrmsta(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx Framer Status"
    
        def description(self):
            return "Rx Framer status"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xf43100 + 16*LineId"
            
        def startAddress(self):
            return 0x00f43100
            
        def endAddress(self):
            return 0x00f43170

        class _StaOof(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "StaOof"
            
            def description(self):
                return "Out of Frame that is detected at RxFramer"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["StaOof"] = _AF6CCI0011_RD_OCN_DIAG._rxfrmsta._StaOof()
            return allFields

    class _rxfrmstk(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx Framer Sticky"
    
        def description(self):
            return "Rx Framer sticky"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xf43101 + 16*LineId"
            
        def startAddress(self):
            return 0x00f43101
            
        def endAddress(self):
            return 0x00f43171

        class _StkOof(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "StkOof"
            
            def description(self):
                return "Out of Frame sticky change"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["StkOof"] = _AF6CCI0011_RD_OCN_DIAG._rxfrmstk._StkOof()
            return allFields

    class _rxfrmb1cntro(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx Framer B1 error counter read only"
    
        def description(self):
            return "Rx Framer B1 error counter read only"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xf43102 + 16*LineId"
            
        def startAddress(self):
            return 0x00f43102
            
        def endAddress(self):
            return 0x00f43172

        class _RxFrmB1ErrRo(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxFrmB1ErrRo"
            
            def description(self):
                return "Number of B1 error - read only"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxFrmB1ErrRo"] = _AF6CCI0011_RD_OCN_DIAG._rxfrmb1cntro._RxFrmB1ErrRo()
            return allFields

    class _rxfrmb1cntr2c(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx Framer B1 error counter read to clear"
    
        def description(self):
            return "Rx Framer B1 error counter read to clear"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xf43103 + 16*LineId"
            
        def startAddress(self):
            return 0x00f43103
            
        def endAddress(self):
            return 0x00f43173

        class _RxFrmB1ErrR2C(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxFrmB1ErrR2C"
            
            def description(self):
                return "Number of B1 error - read to clear"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxFrmB1ErrR2C"] = _AF6CCI0011_RD_OCN_DIAG._rxfrmb1cntr2c._RxFrmB1ErrR2C()
            return allFields

import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0022_RD_OCN(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["glbspi_reg"] = _AF6CNC0022_RD_OCN._glbspi_reg()
        allRegisters["glbvpi_reg"] = _AF6CNC0022_RD_OCN._glbvpi_reg()
        allRegisters["glbtpg_reg"] = _AF6CNC0022_RD_OCN._glbtpg_reg()
        allRegisters["glbloop_reg"] = _AF6CNC0022_RD_OCN._glbloop_reg()
        allRegisters["glbapsgrppage_reg"] = _AF6CNC0022_RD_OCN._glbapsgrppage_reg()
        allRegisters["glbpasgrpenb_reg"] = _AF6CNC0022_RD_OCN._glbpasgrpenb_reg()
        allRegisters["glbtxholosel1_reg"] = _AF6CNC0022_RD_OCN._glbtxholosel1_reg()
        allRegisters["glbtxholosel2_reg"] = _AF6CNC0022_RD_OCN._glbtxholosel2_reg()
        allRegisters["tfi5glbrfm_reg"] = _AF6CNC0022_RD_OCN._tfi5glbrfm_reg()
        allRegisters["tfi5glbclkmon_reg"] = _AF6CNC0022_RD_OCN._tfi5glbclkmon_reg()
        allRegisters["tfi5glbdetlos_pen"] = _AF6CNC0022_RD_OCN._tfi5glbdetlos_pen()
        allRegisters["tfi5glbtfm_reg"] = _AF6CNC0022_RD_OCN._tfi5glbtfm_reg()
        allRegisters["tfi5spiramctl"] = _AF6CNC0022_RD_OCN._tfi5spiramctl()
        allRegisters["tfi5spgramctl"] = _AF6CNC0022_RD_OCN._tfi5spgramctl()
        allRegisters["tfi5rxbarsxcramctl"] = _AF6CNC0022_RD_OCN._tfi5rxbarsxcramctl()
        allRegisters["tfi5txbarsxcramctl"] = _AF6CNC0022_RD_OCN._tfi5txbarsxcramctl()
        allRegisters["tfi5rxfrmsta"] = _AF6CNC0022_RD_OCN._tfi5rxfrmsta()
        allRegisters["tfi5rxfrmstk"] = _AF6CNC0022_RD_OCN._tfi5rxfrmstk()
        allRegisters["tfi5rxfrmb1cntro"] = _AF6CNC0022_RD_OCN._tfi5rxfrmb1cntro()
        allRegisters["tfi5rxfrmb1cntr2c"] = _AF6CNC0022_RD_OCN._tfi5rxfrmb1cntr2c()
        allRegisters["tfi5upstschstkram"] = _AF6CNC0022_RD_OCN._tfi5upstschstkram()
        allRegisters["tfi5upstschstaram"] = _AF6CNC0022_RD_OCN._tfi5upstschstaram()
        allRegisters["tfi5adjcntperstsram"] = _AF6CNC0022_RD_OCN._tfi5adjcntperstsram()
        allRegisters["tfi5stspgstkram"] = _AF6CNC0022_RD_OCN._tfi5stspgstkram()
        allRegisters["tfi5adjcntpgperstsram"] = _AF6CNC0022_RD_OCN._tfi5adjcntpgperstsram()
        allRegisters["tfi5rxstsconcdetreg"] = _AF6CNC0022_RD_OCN._tfi5rxstsconcdetreg()
        allRegisters["tfi5txstsconcdetreg"] = _AF6CNC0022_RD_OCN._tfi5txstsconcdetreg()
        allRegisters["glbrfm_reg"] = _AF6CNC0022_RD_OCN._glbrfm_reg()
        allRegisters["glbclkmon_reg"] = _AF6CNC0022_RD_OCN._glbclkmon_reg()
        allRegisters["glbdetlos_reg"] = _AF6CNC0022_RD_OCN._glbdetlos_reg()
        allRegisters["glblofthr_reg"] = _AF6CNC0022_RD_OCN._glblofthr_reg()
        allRegisters["glbrfmaisfwd_reg"] = _AF6CNC0022_RD_OCN._glbrfmaisfwd_reg()
        allRegisters["glbrfmrdibwd_reg"] = _AF6CNC0022_RD_OCN._glbrfmrdibwd_reg()
        allRegisters["glbtfm_reg"] = _AF6CNC0022_RD_OCN._glbtfm_reg()
        allRegisters["glbtfmfrc_reg"] = _AF6CNC0022_RD_OCN._glbtfmfrc_reg()
        allRegisters["glbrdiinsthr_reg"] = _AF6CNC0022_RD_OCN._glbrdiinsthr_reg()
        allRegisters["glbrxslcppsel1_reg"] = _AF6CNC0022_RD_OCN._glbrxslcppsel1_reg()
        allRegisters["glbrxslcppsel2_reg"] = _AF6CNC0022_RD_OCN._glbrxslcppsel2_reg()
        allRegisters["glbtxslcppsel1_reg"] = _AF6CNC0022_RD_OCN._glbtxslcppsel1_reg()
        allRegisters["glbtxslcppsel2_reg"] = _AF6CNC0022_RD_OCN._glbtxslcppsel2_reg()
        allRegisters["glbrxtohbusctl_reg"] = _AF6CNC0022_RD_OCN._glbrxtohbusctl_reg()
        allRegisters["glbtohbusthr_reg"] = _AF6CNC0022_RD_OCN._glbtohbusthr_reg()
        allRegisters["glbtxtohbusctl_reg"] = _AF6CNC0022_RD_OCN._glbtxtohbusctl_reg()
        allRegisters["spiramctl"] = _AF6CNC0022_RD_OCN._spiramctl()
        allRegisters["spgramctl"] = _AF6CNC0022_RD_OCN._spgramctl()
        allRegisters["upstschstkram"] = _AF6CNC0022_RD_OCN._upstschstkram()
        allRegisters["upstschstaram"] = _AF6CNC0022_RD_OCN._upstschstaram()
        allRegisters["adjcntperstsram"] = _AF6CNC0022_RD_OCN._adjcntperstsram()
        allRegisters["stspgstkram"] = _AF6CNC0022_RD_OCN._stspgstkram()
        allRegisters["adjcntpgperstsram"] = _AF6CNC0022_RD_OCN._adjcntpgperstsram()
        allRegisters["linerxstsconcdetreg"] = _AF6CNC0022_RD_OCN._linerxstsconcdetreg()
        allRegisters["linetxstsconcdetreg"] = _AF6CNC0022_RD_OCN._linetxstsconcdetreg()
        allRegisters["tfmregctl"] = _AF6CNC0022_RD_OCN._tfmregctl()
        allRegisters["tfmj0insctl"] = _AF6CNC0022_RD_OCN._tfmj0insctl()
        allRegisters["tohglbk1stbthr_reg"] = _AF6CNC0022_RD_OCN._tohglbk1stbthr_reg()
        allRegisters["tohglbk2stbthr_reg"] = _AF6CNC0022_RD_OCN._tohglbk2stbthr_reg()
        allRegisters["tohglbs1stbthr_reg"] = _AF6CNC0022_RD_OCN._tohglbs1stbthr_reg()
        allRegisters["tohglbrdidetthr_reg"] = _AF6CNC0022_RD_OCN._tohglbrdidetthr_reg()
        allRegisters["tohglbaisdetthr_reg"] = _AF6CNC0022_RD_OCN._tohglbaisdetthr_reg()
        allRegisters["tohglbk1smpthr_reg"] = _AF6CNC0022_RD_OCN._tohglbk1smpthr_reg()
        allRegisters["tohglberrcntmod_reg"] = _AF6CNC0022_RD_OCN._tohglberrcntmod_reg()
        allRegisters["tohglbaffen_reg"] = _AF6CNC0022_RD_OCN._tohglbaffen_reg()
        allRegisters["tohramctl"] = _AF6CNC0022_RD_OCN._tohramctl()
        allRegisters["tohb1errrocnt"] = _AF6CNC0022_RD_OCN._tohb1errrocnt()
        allRegisters["tohb1errr2ccnt"] = _AF6CNC0022_RD_OCN._tohb1errr2ccnt()
        allRegisters["tohb1blkerrrocnt"] = _AF6CNC0022_RD_OCN._tohb1blkerrrocnt()
        allRegisters["tohb1blkerrr2ccnt"] = _AF6CNC0022_RD_OCN._tohb1blkerrr2ccnt()
        allRegisters["tohb2errrocnt"] = _AF6CNC0022_RD_OCN._tohb2errrocnt()
        allRegisters["tohb2errr2ccnt"] = _AF6CNC0022_RD_OCN._tohb2errr2ccnt()
        allRegisters["tohb2blkerrrocnt"] = _AF6CNC0022_RD_OCN._tohb2blkerrrocnt()
        allRegisters["tohb2blkerrr2ccnt"] = _AF6CNC0022_RD_OCN._tohb2blkerrr2ccnt()
        allRegisters["tohreierrrocnt"] = _AF6CNC0022_RD_OCN._tohreierrrocnt()
        allRegisters["tohreierrr2ccnt"] = _AF6CNC0022_RD_OCN._tohreierrr2ccnt()
        allRegisters["tohreiblkerrrocnt"] = _AF6CNC0022_RD_OCN._tohreiblkerrrocnt()
        allRegisters["tohreiblkerrr2ccnt"] = _AF6CNC0022_RD_OCN._tohreiblkerrr2ccnt()
        allRegisters["tohk1monsta"] = _AF6CNC0022_RD_OCN._tohk1monsta()
        allRegisters["tohk2monsta"] = _AF6CNC0022_RD_OCN._tohk2monsta()
        allRegisters["tohs1monsta"] = _AF6CNC0022_RD_OCN._tohs1monsta()
        allRegisters["tohintperalrenbctl"] = _AF6CNC0022_RD_OCN._tohintperalrenbctl()
        allRegisters["tohintsta"] = _AF6CNC0022_RD_OCN._tohintsta()
        allRegisters["tohcursta"] = _AF6CNC0022_RD_OCN._tohcursta()
        allRegisters["tohintperlineenctl"] = _AF6CNC0022_RD_OCN._tohintperlineenctl()
        allRegisters["tohintperline"] = _AF6CNC0022_RD_OCN._tohintperline()
        allRegisters["sxcramctl0"] = _AF6CNC0022_RD_OCN._sxcramctl0()
        allRegisters["sxcramctl1"] = _AF6CNC0022_RD_OCN._sxcramctl1()
        allRegisters["apsramctl"] = _AF6CNC0022_RD_OCN._apsramctl()
        allRegisters["rxhomapramctl"] = _AF6CNC0022_RD_OCN._rxhomapramctl()
        allRegisters["demramctl"] = _AF6CNC0022_RD_OCN._demramctl()
        allRegisters["vpiramctl"] = _AF6CNC0022_RD_OCN._vpiramctl()
        allRegisters["pgdemramctl"] = _AF6CNC0022_RD_OCN._pgdemramctl()
        allRegisters["vpgramctl"] = _AF6CNC0022_RD_OCN._vpgramctl()
        allRegisters["upvtchstkram"] = _AF6CNC0022_RD_OCN._upvtchstkram()
        allRegisters["upvtchstaram"] = _AF6CNC0022_RD_OCN._upvtchstaram()
        allRegisters["adjcntperstkram"] = _AF6CNC0022_RD_OCN._adjcntperstkram()
        allRegisters["vtpgstkram"] = _AF6CNC0022_RD_OCN._vtpgstkram()
        allRegisters["adjcntpgpervtram"] = _AF6CNC0022_RD_OCN._adjcntpgpervtram()
        allRegisters["parfrccfg1"] = _AF6CNC0022_RD_OCN._parfrccfg1()
        allRegisters["pardiscfg1"] = _AF6CNC0022_RD_OCN._pardiscfg1()
        allRegisters["parerrstk1"] = _AF6CNC0022_RD_OCN._parerrstk1()
        allRegisters["parfrccfg2"] = _AF6CNC0022_RD_OCN._parfrccfg2()
        allRegisters["pardiscfg2"] = _AF6CNC0022_RD_OCN._pardiscfg2()
        allRegisters["parerrstk2"] = _AF6CNC0022_RD_OCN._parerrstk2()
        allRegisters["parfrccfg3"] = _AF6CNC0022_RD_OCN._parfrccfg3()
        allRegisters["pardiscfg3"] = _AF6CNC0022_RD_OCN._pardiscfg3()
        allRegisters["parerrstk3"] = _AF6CNC0022_RD_OCN._parerrstk3()
        allRegisters["parfrccfg4"] = _AF6CNC0022_RD_OCN._parfrccfg4()
        allRegisters["pardiscfg4"] = _AF6CNC0022_RD_OCN._pardiscfg4()
        allRegisters["parerrstk4"] = _AF6CNC0022_RD_OCN._parerrstk4()
        allRegisters["parfrccfg5"] = _AF6CNC0022_RD_OCN._parfrccfg5()
        allRegisters["pardiscfg5"] = _AF6CNC0022_RD_OCN._pardiscfg5()
        allRegisters["parerrstk5"] = _AF6CNC0022_RD_OCN._parerrstk5()
        allRegisters["parfrccfg6"] = _AF6CNC0022_RD_OCN._parfrccfg6()
        allRegisters["pardiscfg6"] = _AF6CNC0022_RD_OCN._pardiscfg6()
        allRegisters["parerrstk6"] = _AF6CNC0022_RD_OCN._parerrstk6()
        allRegisters["parfrccfg7"] = _AF6CNC0022_RD_OCN._parfrccfg7()
        allRegisters["pardiscfg7"] = _AF6CNC0022_RD_OCN._pardiscfg7()
        allRegisters["parerrstk7"] = _AF6CNC0022_RD_OCN._parerrstk7()
        return allRegisters

    class _glbspi_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global STS Pointer Interpreter Control"
    
        def description(self):
            return "This is the global configuration register for the STS Pointer Interpreter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000f0002
            
        def endAddress(self):
            return 0xffffffff

        class _RxPgFlowThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 24
        
            def name(self):
                return "RxPgFlowThresh"
            
            def description(self):
                return "Overflow/underflow threshold to resynchronize read/write pointer."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPgAdjThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 20
        
            def name(self):
                return "RxPgAdjThresh"
            
            def description(self):
                return "Adjustment threshold to make a pointer increment/decrement."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiAisAisPEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "StsPiAisAisPEn"
            
            def description(self):
                return "Enable/Disable forwarding P_AIS when AIS state is detected at STS Pointer interpreter. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiLopAisPEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "StsPiLopAisPEn"
            
            def description(self):
                return "Enable/Disable forwarding P_AIS when LOP state is detected at STS Pointer interpreter. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiMajorMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "StsPiMajorMode"
            
            def description(self):
                return "Majority mode for detecting increment/decrement at STS pointer Interpreter. It is used for rule n of 5. 1: n = 3 0: n = 5"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiNorPtrThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "StsPiNorPtrThresh"
            
            def description(self):
                return "Threshold of number of normal pointers between two contiguous frames within pointer adjustments."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiNdfPtrThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "StsPiNdfPtrThresh"
            
            def description(self):
                return "Threshold of number of contiguous NDF pointers for entering LOP state at FSM."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiBadPtrThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "StsPiBadPtrThresh"
            
            def description(self):
                return "Threshold of number of contiguous invalid pointers for entering LOP state at FSM."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiPohAisType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "StsPiPohAisType"
            
            def description(self):
                return "Enable/disable STS POH defect types to downstream AIS in case of terminating the related STS such as the STS carries VT/TU. [0]: Enable for TIM defect [1]: Enable for Unequiped defect [2]: Enable for VC-AIS [3]: Enable for PLM defect"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPgFlowThresh"] = _AF6CNC0022_RD_OCN._glbspi_reg._RxPgFlowThresh()
            allFields["RxPgAdjThresh"] = _AF6CNC0022_RD_OCN._glbspi_reg._RxPgAdjThresh()
            allFields["StsPiAisAisPEn"] = _AF6CNC0022_RD_OCN._glbspi_reg._StsPiAisAisPEn()
            allFields["StsPiLopAisPEn"] = _AF6CNC0022_RD_OCN._glbspi_reg._StsPiLopAisPEn()
            allFields["StsPiMajorMode"] = _AF6CNC0022_RD_OCN._glbspi_reg._StsPiMajorMode()
            allFields["StsPiNorPtrThresh"] = _AF6CNC0022_RD_OCN._glbspi_reg._StsPiNorPtrThresh()
            allFields["StsPiNdfPtrThresh"] = _AF6CNC0022_RD_OCN._glbspi_reg._StsPiNdfPtrThresh()
            allFields["StsPiBadPtrThresh"] = _AF6CNC0022_RD_OCN._glbspi_reg._StsPiBadPtrThresh()
            allFields["StsPiPohAisType"] = _AF6CNC0022_RD_OCN._glbspi_reg._StsPiPohAisType()
            return allFields

    class _glbvpi_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global VTTU Pointer Interpreter Control"
    
        def description(self):
            return "This is the global configuration register for the VTTU Pointer Interpreter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000f0003
            
        def endAddress(self):
            return 0xffffffff

        class _VtPiLomAisPEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "VtPiLomAisPEn"
            
            def description(self):
                return "Enable/Disable forwarding AIS when LOM is detected. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiLomInvlCntMod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "VtPiLomInvlCntMod"
            
            def description(self):
                return "H4 monitoring mode. 1: Expected H4 is current frame in the validated sequence plus one. 0: Expected H4 is the last received value plus one."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiLomGoodThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 24
        
            def name(self):
                return "VtPiLomGoodThresh"
            
            def description(self):
                return "Threshold of number of contiguous frames with validated sequence	of multi framers in LOM state for condition to entering IM state."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiLomInvlThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 20
        
            def name(self):
                return "VtPiLomInvlThresh"
            
            def description(self):
                return "Threshold of number of contiguous frames with invalidated sequence of multi framers in IM state  for condition to entering LOM state."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiAisAisPEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "VtPiAisAisPEn"
            
            def description(self):
                return "Enable/Disable forwarding AIS when AIS state is detected at VTTU Pointer interpreter. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiLopAisPEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "VtPiLopAisPEn"
            
            def description(self):
                return "Enable/Disable forwarding AIS when LOP state is detected at VTTU Pointer interpreter. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiMajorMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "VtPiMajorMode"
            
            def description(self):
                return "Majority mode detecting increment/decrement in VTTU pointer Interpreter. It is used for rule n of 5. 1: n = 3 0: n = 5"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiNorPtrThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "VtPiNorPtrThresh"
            
            def description(self):
                return "Threshold of number of normal pointers between two contiguous frames within pointer adjustments."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiNdfPtrThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "VtPiNdfPtrThresh"
            
            def description(self):
                return "Threshold of number of contiguous NDF pointers for entering LOP state at FSM."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiBadPtrThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "VtPiBadPtrThresh"
            
            def description(self):
                return "Threshold of number of contiguous invalid pointers for entering LOP state at FSM."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiPohAisType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "VtPiPohAisType"
            
            def description(self):
                return "Enable/disable VTTU POH defect types to downstream AIS in case of terminating the related VTTU. [0]: Enable for TIM defect [1]: Enable for Un-equipment defect [2]: Enable for VC-AIS [3]: Enable for PLM defect"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["VtPiLomAisPEn"] = _AF6CNC0022_RD_OCN._glbvpi_reg._VtPiLomAisPEn()
            allFields["VtPiLomInvlCntMod"] = _AF6CNC0022_RD_OCN._glbvpi_reg._VtPiLomInvlCntMod()
            allFields["VtPiLomGoodThresh"] = _AF6CNC0022_RD_OCN._glbvpi_reg._VtPiLomGoodThresh()
            allFields["VtPiLomInvlThresh"] = _AF6CNC0022_RD_OCN._glbvpi_reg._VtPiLomInvlThresh()
            allFields["VtPiAisAisPEn"] = _AF6CNC0022_RD_OCN._glbvpi_reg._VtPiAisAisPEn()
            allFields["VtPiLopAisPEn"] = _AF6CNC0022_RD_OCN._glbvpi_reg._VtPiLopAisPEn()
            allFields["VtPiMajorMode"] = _AF6CNC0022_RD_OCN._glbvpi_reg._VtPiMajorMode()
            allFields["VtPiNorPtrThresh"] = _AF6CNC0022_RD_OCN._glbvpi_reg._VtPiNorPtrThresh()
            allFields["VtPiNdfPtrThresh"] = _AF6CNC0022_RD_OCN._glbvpi_reg._VtPiNdfPtrThresh()
            allFields["VtPiBadPtrThresh"] = _AF6CNC0022_RD_OCN._glbvpi_reg._VtPiBadPtrThresh()
            allFields["VtPiPohAisType"] = _AF6CNC0022_RD_OCN._glbvpi_reg._VtPiPohAisType()
            return allFields

    class _glbtpg_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Pointer Generator Control"
    
        def description(self):
            return "This is the global configuration register for the Tx Pointer Generator"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000f0004
            
        def endAddress(self):
            return 0xffffffff

        class _TxPgNorPtrThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TxPgNorPtrThresh"
            
            def description(self):
                return "Threshold of number of normal pointers between two contiguous frames to make a condition of pointer adjustments."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxPgFlowThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "TxPgFlowThresh"
            
            def description(self):
                return "Overflow/underflow threshold to resynchronize read/write pointer of TxFiFo."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxPgAdjThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxPgAdjThresh"
            
            def description(self):
                return "Adjustment threshold to make a condition of pointer increment/decrement."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxPgNorPtrThresh"] = _AF6CNC0022_RD_OCN._glbtpg_reg._TxPgNorPtrThresh()
            allFields["TxPgFlowThresh"] = _AF6CNC0022_RD_OCN._glbtpg_reg._TxPgFlowThresh()
            allFields["TxPgAdjThresh"] = _AF6CNC0022_RD_OCN._glbtpg_reg._TxPgAdjThresh()
            return allFields

    class _glbloop_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Loopback"
    
        def description(self):
            return "This is the global configuration register for internal loopback"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000f0005
            
        def endAddress(self):
            return 0xffffffff

        class _SxcApsSwFsm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "SxcApsSwFsm"
            
            def description(self):
                return "SW-FSM for APS. 1: Page ID 1 is selected 0: Page ID 0 is selected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPohSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "StsPohSel"
            
            def description(self):
                return "STS POH Selection. 1: TFI5 is selected to Mon/Insert POH 0: FacePlate is selected to Mon/Insert POH"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _InternalDebug(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 1
        
            def name(self):
                return "InternalDebug"
            
            def description(self):
                return "Internal Debug"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LoLoopBack(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LoLoopBack"
            
            def description(self):
                return "LoBus Loopback. 1: Loopback in 0: Normal (No loopback)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SxcApsSwFsm"] = _AF6CNC0022_RD_OCN._glbloop_reg._SxcApsSwFsm()
            allFields["StsPohSel"] = _AF6CNC0022_RD_OCN._glbloop_reg._StsPohSel()
            allFields["InternalDebug"] = _AF6CNC0022_RD_OCN._glbloop_reg._InternalDebug()
            allFields["LoLoopBack"] = _AF6CNC0022_RD_OCN._glbloop_reg._LoLoopBack()
            return allFields

    class _glbapsgrppage_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global APS Grouping SXC_Page Selection"
    
        def description(self):
            return "This register use 16 bits to configure SXC_Page ID for 16 APS Group. Bit 0 for APS Group 0"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000f0006
            
        def endAddress(self):
            return 0xffffffff

        class _ApsGrpPageId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ApsGrpPageId"
            
            def description(self):
                return "Each bit is used to configure SXC_Page ID for 16 APS Group. Bit 0 for APS Group 0 1: SXC_Page ID 1 is selected 0: SXC_Page ID 0 is selected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ApsGrpPageId"] = _AF6CNC0022_RD_OCN._glbapsgrppage_reg._ApsGrpPageId()
            return allFields

    class _glbpasgrpenb_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Passthrough Grouping Enable"
    
        def description(self):
            return "This register use 32 bits to configure for 16 Passthrough Groups at Rx direction and 16 Passthrough Groups at Tx direction."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000f0007
            
        def endAddress(self):
            return 0xffffffff

        class _TxSpgPassThrGrpEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "TxSpgPassThrGrpEnb"
            
            def description(self):
                return "Each bit is used to configure enable/disable Passthrough mode at Tx SPG for 1 Passthrough Group. Bit 16 for Passthrough Group Tx0 1: Enable 0: Disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxSpiPassThrGrpEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxSpiPassThrGrpEnb"
            
            def description(self):
                return "Each bit is used to configure enable/disable Passthrough mode at Rx SPI for 1 Passthrough Group. Bit 0 for Passthrough Group Rx0 1: Enable 0: Disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxSpgPassThrGrpEnb"] = _AF6CNC0022_RD_OCN._glbpasgrpenb_reg._TxSpgPassThrGrpEnb()
            allFields["RxSpiPassThrGrpEnb"] = _AF6CNC0022_RD_OCN._glbpasgrpenb_reg._RxSpiPassThrGrpEnb()
            return allFields

    class _glbtxholosel1_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Tx HO/LO Selection Control1"
    
        def description(self):
            return "Configure Tx HO/LO Selection for 8 slices HO/LO OC48 for STS from 0-31."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xf0010 + SliceOc48Id"
            
        def startAddress(self):
            return 0x000f0010
            
        def endAddress(self):
            return 0x000f0017

        class _TxHoLoSelCtl1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxHoLoSelCtl1"
            
            def description(self):
                return "Each bit will be config per STS in any slices HO/LO OC48, bit #0 for STS#0. 1: Select from Ho_Bus 0: Select from Lo_Bus"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxHoLoSelCtl1"] = _AF6CNC0022_RD_OCN._glbtxholosel1_reg._TxHoLoSelCtl1()
            return allFields

    class _glbtxholosel2_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Tx HO/LO Selection Control2"
    
        def description(self):
            return "Configure Tx HO/LO Selection for 8 slices HO/LO OC48 for STS from 32-47."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xf0018 + SliceOc48Id"
            
        def startAddress(self):
            return 0x000f0018
            
        def endAddress(self):
            return 0x000f001f

        class _TxHoLoSelCtl2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxHoLoSelCtl2"
            
            def description(self):
                return "Each bit will be config per STS in any slices HO/LO OC48, bit #0 for STS#32. 1: Select from Ho_Bus 0: Select from Lo_Bus"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxHoLoSelCtl2"] = _AF6CNC0022_RD_OCN._glbtxholosel2_reg._TxHoLoSelCtl2()
            return allFields

    class _tfi5glbrfm_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Rx Framer Control - TFI5 Side"
    
        def description(self):
            return "This is the global configuration register for the Rx Framer"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000f000
            
        def endAddress(self):
            return 0xffffffff

        class _Tfi5RxLineSyncSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Tfi5RxLineSyncSel"
            
            def description(self):
                return "Select line id for synchronization 8 rx lines. Bit[0] for line 0. Note that must only 1 bit is set"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5RxFrmLosAisEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "Tfi5RxFrmLosAisEn"
            
            def description(self):
                return "Enable/disable forwarding P_AIS when LOS detected at Rx Framer. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5RxFrmOofAisEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "Tfi5RxFrmOofAisEn"
            
            def description(self):
                return "Enable/disable forwarding P_AIS when OOF detected at Rx Framer. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5RxFrmB1BlockCntMod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Tfi5RxFrmB1BlockCntMod"
            
            def description(self):
                return "B1 Counter Mode. 1: Block mode 0: Bit-wise mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5RxFrmBadFrmThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Tfi5RxFrmBadFrmThresh"
            
            def description(self):
                return "Threshold for A1A2 missing counter, that is used to change state from FRAMED to HUNT."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5RxFrmB1GoodThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Tfi5RxFrmB1GoodThresh"
            
            def description(self):
                return "Threshold for B1 good counter, that is used to change state from CHECK to FRAMED."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5RxFrmDescrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Tfi5RxFrmDescrEn"
            
            def description(self):
                return "Enable/disable de-scrambling of the Rx coming data stream. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5RxFrmB1ChkFrmEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Tfi5RxFrmB1ChkFrmEn"
            
            def description(self):
                return "Enable/disable B1 check option is added to the required framing algorithm. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5RxFrmModeEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Tfi5RxFrmModeEn"
            
            def description(self):
                return "TFI-5 mode. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Tfi5RxLineSyncSel"] = _AF6CNC0022_RD_OCN._tfi5glbrfm_reg._Tfi5RxLineSyncSel()
            allFields["Tfi5RxFrmLosAisEn"] = _AF6CNC0022_RD_OCN._tfi5glbrfm_reg._Tfi5RxFrmLosAisEn()
            allFields["Tfi5RxFrmOofAisEn"] = _AF6CNC0022_RD_OCN._tfi5glbrfm_reg._Tfi5RxFrmOofAisEn()
            allFields["Tfi5RxFrmB1BlockCntMod"] = _AF6CNC0022_RD_OCN._tfi5glbrfm_reg._Tfi5RxFrmB1BlockCntMod()
            allFields["Tfi5RxFrmBadFrmThresh"] = _AF6CNC0022_RD_OCN._tfi5glbrfm_reg._Tfi5RxFrmBadFrmThresh()
            allFields["Tfi5RxFrmB1GoodThresh"] = _AF6CNC0022_RD_OCN._tfi5glbrfm_reg._Tfi5RxFrmB1GoodThresh()
            allFields["Tfi5RxFrmDescrEn"] = _AF6CNC0022_RD_OCN._tfi5glbrfm_reg._Tfi5RxFrmDescrEn()
            allFields["Tfi5RxFrmB1ChkFrmEn"] = _AF6CNC0022_RD_OCN._tfi5glbrfm_reg._Tfi5RxFrmB1ChkFrmEn()
            allFields["Tfi5RxFrmModeEn"] = _AF6CNC0022_RD_OCN._tfi5glbrfm_reg._Tfi5RxFrmModeEn()
            return allFields

    class _tfi5glbclkmon_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Rx Framer LOS Detecting Control 1 - TFI5 Side"
    
        def description(self):
            return "This is the global configuration register for the Rx Framer LOS detecting 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000f006
            
        def endAddress(self):
            return 0xffffffff

        class _Tfi5RxFrmLosDetMod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "Tfi5RxFrmLosDetMod"
            
            def description(self):
                return "Detected LOS mode. 1: the LOS declare when all zero or all one detected 0: the LOS declare when all zero detected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5RxFrmLosDetDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "Tfi5RxFrmLosDetDis"
            
            def description(self):
                return "Disable detect LOS. 1: Disable 0: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5RxFrmClkMonDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Tfi5RxFrmClkMonDis"
            
            def description(self):
                return "Disable to generate LOS to Rx Framer if detecting error on Rx line clock. 1: Disable 0: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5RxFrmClkMonThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Tfi5RxFrmClkMonThr"
            
            def description(self):
                return "Threshold to generate LOS to Rx Framer if detecting error on Rx line clock.(Threshold = PPM * 155.52/8). Default 0x3cc ~ 50ppm"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Tfi5RxFrmLosDetMod"] = _AF6CNC0022_RD_OCN._tfi5glbclkmon_reg._Tfi5RxFrmLosDetMod()
            allFields["Tfi5RxFrmLosDetDis"] = _AF6CNC0022_RD_OCN._tfi5glbclkmon_reg._Tfi5RxFrmLosDetDis()
            allFields["Tfi5RxFrmClkMonDis"] = _AF6CNC0022_RD_OCN._tfi5glbclkmon_reg._Tfi5RxFrmClkMonDis()
            allFields["Tfi5RxFrmClkMonThr"] = _AF6CNC0022_RD_OCN._tfi5glbclkmon_reg._Tfi5RxFrmClkMonThr()
            return allFields

    class _tfi5glbdetlos_pen(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Rx Framer LOS Detecting Control 2 - TFI5 Side"
    
        def description(self):
            return "This is the global configuration register for the Rx Framer LOS detecting 2"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000f007
            
        def endAddress(self):
            return 0xffffffff

        class _Tfi5RxFrmLosClr2Thr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "Tfi5RxFrmLosClr2Thr"
            
            def description(self):
                return "Configure the period of time during which there is no period of consecutive data without transition, used for reset no transition counter. The recommended value is 0x30 (~2.5ms)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5RxFrmLosSetThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Tfi5RxFrmLosSetThr"
            
            def description(self):
                return "Configure the value that define the period of time in which there is no transition detected from incoming data from SERDES. The recommended value is 0x3cc (~50ms)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5RxFrmLosClr1Thr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Tfi5RxFrmLosClr1Thr"
            
            def description(self):
                return "Configure the period of time during which there is no period of consecutive data without transition, used for counting at INFRM to change state to OOF The recommended value is 0x3cc (~50ms)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Tfi5RxFrmLosClr2Thr"] = _AF6CNC0022_RD_OCN._tfi5glbdetlos_pen._Tfi5RxFrmLosClr2Thr()
            allFields["Tfi5RxFrmLosSetThr"] = _AF6CNC0022_RD_OCN._tfi5glbdetlos_pen._Tfi5RxFrmLosSetThr()
            allFields["Tfi5RxFrmLosClr1Thr"] = _AF6CNC0022_RD_OCN._tfi5glbdetlos_pen._Tfi5RxFrmLosClr1Thr()
            return allFields

    class _tfi5glbtfm_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Tx Framer Control - TFI5 Side"
    
        def description(self):
            return "This is the global configuration register for the Tx Framer"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000f001
            
        def endAddress(self):
            return 0xffffffff

        class _Tfi5TxLineOofFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "Tfi5TxLineOofFrc"
            
            def description(self):
                return "Enable/disable force OOF for 8 tx lines. Bit[0] for line 0."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5TxLineB1ErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Tfi5TxLineB1ErrFrc"
            
            def description(self):
                return "Enable/disable force B1 error for 8 tx lines. Bit[0] for line 0."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5TxLineSyncSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Tfi5TxLineSyncSel"
            
            def description(self):
                return "Select line id for synchronization 8 tx lines. Bit[0] for line 0. Note that must only 1 bit is set"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5TxFrmScrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Tfi5TxFrmScrEn"
            
            def description(self):
                return "Enable/disable scrambling of the Tx data stream. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Tfi5TxLineOofFrc"] = _AF6CNC0022_RD_OCN._tfi5glbtfm_reg._Tfi5TxLineOofFrc()
            allFields["Tfi5TxLineB1ErrFrc"] = _AF6CNC0022_RD_OCN._tfi5glbtfm_reg._Tfi5TxLineB1ErrFrc()
            allFields["Tfi5TxLineSyncSel"] = _AF6CNC0022_RD_OCN._tfi5glbtfm_reg._Tfi5TxLineSyncSel()
            allFields["Tfi5TxFrmScrEn"] = _AF6CNC0022_RD_OCN._tfi5glbtfm_reg._Tfi5TxFrmScrEn()
            return allFields

    class _tfi5spiramctl(AtRegister.AtRegister):
        def name(self):
            return "OCN STS Pointer Interpreter Per Channel Control - TFI5 Side"
    
        def description(self):
            return "Each register is used to configure for STS pointer interpreter engines of the STS-1/VC-3."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x02000 + 512*LineId + StsId"
            
        def startAddress(self):
            return 0x00002000
            
        def endAddress(self):
            return 0x00002e2f

        class _Tfi5PassThrEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "Tfi5PassThrEnb"
            
            def description(self):
                return "Enable/disable Passthrough mode for BLSR at Rx SPI of TFI5 Side 1: Enable. 0: Disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5PassThrGrp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Tfi5PassThrGrp"
            
            def description(self):
                return "Passthrough Group ID for BLSR at Rx SPI of TFI5 Side."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5StsPiChkLom(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "Tfi5StsPiChkLom"
            
            def description(self):
                return "Enable/disable LOM checking. This field will be set to 1 when payload type of VC3/VC4 includes any VC11/VC12 1: Enable. 0: Disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5StsPiSSDetPatt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Tfi5StsPiSSDetPatt"
            
            def description(self):
                return "Configure pattern SS bits that is used to compare with the extracted SS bits from receive direction."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5StsPiAisFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "Tfi5StsPiAisFrc"
            
            def description(self):
                return "Forcing SFM to AIS state. 1: Force 0: Not force"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5StsPiSSDetEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "Tfi5StsPiSSDetEn"
            
            def description(self):
                return "Enable/disable checking SS bits in STSPI state machine. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5StsPiAdjRule(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "Tfi5StsPiAdjRule"
            
            def description(self):
                return "Configure the rule for detecting adjustment condition. 1: The n of 5 rule is selected. This mode is applied for SDH mode 0: The 8 of 10 rule is selected. This mode is applied for SONET mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5StsPiStsSlvInd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Tfi5StsPiStsSlvInd"
            
            def description(self):
                return "This is used to configure STS is slaver or master. 1: Slaver."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5StsPiStsMstId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Tfi5StsPiStsMstId"
            
            def description(self):
                return "This is the ID of the master STS-1 in the concatenation that contains this STS-1."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Tfi5PassThrEnb"] = _AF6CNC0022_RD_OCN._tfi5spiramctl._Tfi5PassThrEnb()
            allFields["Tfi5PassThrGrp"] = _AF6CNC0022_RD_OCN._tfi5spiramctl._Tfi5PassThrGrp()
            allFields["Tfi5StsPiChkLom"] = _AF6CNC0022_RD_OCN._tfi5spiramctl._Tfi5StsPiChkLom()
            allFields["Tfi5StsPiSSDetPatt"] = _AF6CNC0022_RD_OCN._tfi5spiramctl._Tfi5StsPiSSDetPatt()
            allFields["Tfi5StsPiAisFrc"] = _AF6CNC0022_RD_OCN._tfi5spiramctl._Tfi5StsPiAisFrc()
            allFields["Tfi5StsPiSSDetEn"] = _AF6CNC0022_RD_OCN._tfi5spiramctl._Tfi5StsPiSSDetEn()
            allFields["Tfi5StsPiAdjRule"] = _AF6CNC0022_RD_OCN._tfi5spiramctl._Tfi5StsPiAdjRule()
            allFields["Tfi5StsPiStsSlvInd"] = _AF6CNC0022_RD_OCN._tfi5spiramctl._Tfi5StsPiStsSlvInd()
            allFields["Tfi5StsPiStsMstId"] = _AF6CNC0022_RD_OCN._tfi5spiramctl._Tfi5StsPiStsMstId()
            return allFields

    class _tfi5spgramctl(AtRegister.AtRegister):
        def name(self):
            return "OCN STS Pointer Generator Per Channel Control - TFI5 Side"
    
        def description(self):
            return "Each register is used to configure for STS pointer Generator engines. # HDL_PATH		: itfi5ho.itxpp_stspp_inst.itxpp_stspp[0].txpg_stspgctl.array"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x03000 + 512*LineId + StsId"
            
        def startAddress(self):
            return 0x00003000
            
        def endAddress(self):
            return 0x00003e2f

        class _Tfi5StsPgStsSlvInd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Tfi5StsPgStsSlvInd"
            
            def description(self):
                return "This is used to configure STS is slaver or master. 1: Slaver."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5StsPgStsMstId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Tfi5StsPgStsMstId"
            
            def description(self):
                return "This is the ID of the master STS-1 in the concatenation that contains this STS-1."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5StsPgB3BipErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "Tfi5StsPgB3BipErrFrc"
            
            def description(self):
                return "Forcing B3 Bip error. 1: Force 0: Not force"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5StsPgLopFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Tfi5StsPgLopFrc"
            
            def description(self):
                return "Forcing LOP. 1: Force 0: Not force"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5StsPgUeqFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Tfi5StsPgUeqFrc"
            
            def description(self):
                return "Forcing SFM to UEQ state. 1: Force 0: Not force"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5StsPgAisFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Tfi5StsPgAisFrc"
            
            def description(self):
                return "Forcing SFM to AIS state. 1: Force 0: Not force"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5StsPgSSInsPatt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Tfi5StsPgSSInsPatt"
            
            def description(self):
                return "Configure pattern SS bits that is used to insert to pointer value."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5StsPgSSInsEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Tfi5StsPgSSInsEn"
            
            def description(self):
                return "Enable/disable SS bits insertion. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5StsPgPohIns(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Tfi5StsPgPohIns"
            
            def description(self):
                return "Enable/disable POH Insertion. High to enable insertion of POH."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Tfi5StsPgStsSlvInd"] = _AF6CNC0022_RD_OCN._tfi5spgramctl._Tfi5StsPgStsSlvInd()
            allFields["Tfi5StsPgStsMstId"] = _AF6CNC0022_RD_OCN._tfi5spgramctl._Tfi5StsPgStsMstId()
            allFields["Tfi5StsPgB3BipErrFrc"] = _AF6CNC0022_RD_OCN._tfi5spgramctl._Tfi5StsPgB3BipErrFrc()
            allFields["Tfi5StsPgLopFrc"] = _AF6CNC0022_RD_OCN._tfi5spgramctl._Tfi5StsPgLopFrc()
            allFields["Tfi5StsPgUeqFrc"] = _AF6CNC0022_RD_OCN._tfi5spgramctl._Tfi5StsPgUeqFrc()
            allFields["Tfi5StsPgAisFrc"] = _AF6CNC0022_RD_OCN._tfi5spgramctl._Tfi5StsPgAisFrc()
            allFields["Tfi5StsPgSSInsPatt"] = _AF6CNC0022_RD_OCN._tfi5spgramctl._Tfi5StsPgSSInsPatt()
            allFields["Tfi5StsPgSSInsEn"] = _AF6CNC0022_RD_OCN._tfi5spgramctl._Tfi5StsPgSSInsEn()
            allFields["Tfi5StsPgPohIns"] = _AF6CNC0022_RD_OCN._tfi5spgramctl._Tfi5StsPgPohIns()
            return allFields

    class _tfi5rxbarsxcramctl(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx Bridge and Roll SXC Control - TFI5 Side"
    
        def description(self):
            return "Each register is used for each outgoing STS (to Rx SPI) of any line (8 lines) can be randomly configured to //connect to any ingoing STS of any ingoing line (from TFI-5 line - 8 lines). # HDL_PATH		: itfi5ho.irxfrm_inst.isdhbar_sxc.isdhbar_sxcrd[0].rxsxcramctl.array"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x06000 + 256*LineId + StsId"
            
        def startAddress(self):
            return 0x00006000
            
        def endAddress(self):
            return 0x0000672f

        class _Tfi5RxBarSxcLineId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Tfi5RxBarSxcLineId"
            
            def description(self):
                return "Contains the ingoing LineID (0-7). Disconnect (output is all one) if value LineID is other value (recommend is 14)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5RxBarSxcStsId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Tfi5RxBarSxcStsId"
            
            def description(self):
                return "Contains the ingoing STSID (0-47)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Tfi5RxBarSxcLineId"] = _AF6CNC0022_RD_OCN._tfi5rxbarsxcramctl._Tfi5RxBarSxcLineId()
            allFields["Tfi5RxBarSxcStsId"] = _AF6CNC0022_RD_OCN._tfi5rxbarsxcramctl._Tfi5RxBarSxcStsId()
            return allFields

    class _tfi5txbarsxcramctl(AtRegister.AtRegister):
        def name(self):
            return "OCN Tx Bridge and Roll SXC Control - TFI5 Side"
    
        def description(self):
            return "Each register is used for each outgoing STS (to TFI-5 Line) of any line (8 lines) can be randomly configured to connect to any ingoing STS of any ingoing line (from Tx SPG) . # HDL_PATH		: itfi5ho.itxpp_stspp_inst.isdhtxbar_sxc.isdhbar_sxcrd[0].rxsxcramctl.array"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x07000 + 256*LineId + StsId"
            
        def startAddress(self):
            return 0x00007000
            
        def endAddress(self):
            return 0x0000772f

        class _Tfi5TxBarSxcLineId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Tfi5TxBarSxcLineId"
            
            def description(self):
                return "Contains the ingoing LineID (0-7). Disconnect (output is all one) if value LineID is other value (recommend is 14)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5TxSxcStsId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Tfi5TxSxcStsId"
            
            def description(self):
                return "Contains the ingoing STSID (0-47)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Tfi5TxBarSxcLineId"] = _AF6CNC0022_RD_OCN._tfi5txbarsxcramctl._Tfi5TxBarSxcLineId()
            allFields["Tfi5TxSxcStsId"] = _AF6CNC0022_RD_OCN._tfi5txbarsxcramctl._Tfi5TxSxcStsId()
            return allFields

    class _tfi5rxfrmsta(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx Framer Status - TFI5 Side"
    
        def description(self):
            return "Rx Framer status"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x05000 + 512*LineId"
            
        def startAddress(self):
            return 0x00005000
            
        def endAddress(self):
            return 0x00005e00

        class _Tfi5StaFFCnvFul(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Tfi5StaFFCnvFul"
            
            def description(self):
                return "Status fifo convert clock domain is full"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5StaLos(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Tfi5StaLos"
            
            def description(self):
                return "Loss of Signal status"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5StaOof(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Tfi5StaOof"
            
            def description(self):
                return "Out of Frame that is detected at RxFramer"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Tfi5StaFFCnvFul"] = _AF6CNC0022_RD_OCN._tfi5rxfrmsta._Tfi5StaFFCnvFul()
            allFields["Tfi5StaLos"] = _AF6CNC0022_RD_OCN._tfi5rxfrmsta._Tfi5StaLos()
            allFields["Tfi5StaOof"] = _AF6CNC0022_RD_OCN._tfi5rxfrmsta._Tfi5StaOof()
            return allFields

    class _tfi5rxfrmstk(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx Framer Sticky - TFI5 Side"
    
        def description(self):
            return "Rx Framer sticky"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x05001 + 512*LineId"
            
        def startAddress(self):
            return 0x00005001
            
        def endAddress(self):
            return 0x00005e01

        class _Tfi5StkFFCnvFul(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Tfi5StkFFCnvFul"
            
            def description(self):
                return "Sticky fifo convert clock domain is full"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5StkLos(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Tfi5StkLos"
            
            def description(self):
                return "Loss of Signal  sticky change"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5StkOof(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Tfi5StkOof"
            
            def description(self):
                return "Out of Frame sticky  change"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Tfi5StkFFCnvFul"] = _AF6CNC0022_RD_OCN._tfi5rxfrmstk._Tfi5StkFFCnvFul()
            allFields["Tfi5StkLos"] = _AF6CNC0022_RD_OCN._tfi5rxfrmstk._Tfi5StkLos()
            allFields["Tfi5StkOof"] = _AF6CNC0022_RD_OCN._tfi5rxfrmstk._Tfi5StkOof()
            return allFields

    class _tfi5rxfrmb1cntro(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx Framer B1 error counter read only - TFI5 Side"
    
        def description(self):
            return "Rx Framer B1 error counter read only"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x05002 + 512*LineId"
            
        def startAddress(self):
            return 0x00005002
            
        def endAddress(self):
            return 0x00005e02

        class _Tfi5RxFrmB1ErrRo(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Tfi5RxFrmB1ErrRo"
            
            def description(self):
                return "Number of B1 error - read only"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Tfi5RxFrmB1ErrRo"] = _AF6CNC0022_RD_OCN._tfi5rxfrmb1cntro._Tfi5RxFrmB1ErrRo()
            return allFields

    class _tfi5rxfrmb1cntr2c(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx Framer B1 error counter read to clear - TFI5 Side"
    
        def description(self):
            return "Rx Framer B1 error counter read to clear"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x05003 + 512*LineId"
            
        def startAddress(self):
            return 0x00005003
            
        def endAddress(self):
            return 0x00005e03

        class _Tfi5RxFrmB1ErrR2C(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Tfi5RxFrmB1ErrR2C"
            
            def description(self):
                return "Number of B1 error - read to clear"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Tfi5RxFrmB1ErrR2C"] = _AF6CNC0022_RD_OCN._tfi5rxfrmb1cntr2c._Tfi5RxFrmB1ErrR2C()
            return allFields

    class _tfi5upstschstkram(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx STS/VC per Alarm Interrupt Status - TFI5 Side"
    
        def description(self):
            return "This is the per Alarm interrupt status of STS/VC pointer interpreter.  %% Each register is used to store 6 sticky bits for 6 alarms in the STS/VC."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x02140 + 512*LineId + StsId"
            
        def startAddress(self):
            return 0x00002140
            
        def endAddress(self):
            return 0x00002f6f

        class _Tfi5StsPiStsConcDetIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Tfi5StsPiStsConcDetIntr"
            
            def description(self):
                return "Set to 1 while an Concatenation Detection event is detected at STS/VC pointer interpreter. This event doesn't raise interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5StsPiStsNewDetIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Tfi5StsPiStsNewDetIntr"
            
            def description(self):
                return "Set to 1 while an New Pointer Detection event is detected at STS/VC pointer interpreter. This event doesn't raise interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5StsPiStsNdfIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "Tfi5StsPiStsNdfIntr"
            
            def description(self):
                return "Set to 1 while an NDF event is detected at STS/VC pointer interpreter. This event doesn't raise interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5StsPiCepUneqStatePChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Tfi5StsPiCepUneqStatePChgIntr"
            
            def description(self):
                return "Set to 1  while there is change in CEP Un-equip Path in the related STS/VC to generate an interrupt. Read the OCN Rx STS/VC per Alarm Current Status register of the related STS/VC to know the STS/VC whether in CEP Un-equip Path state or not."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5StsPiStsAISStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Tfi5StsPiStsAISStateChgIntr"
            
            def description(self):
                return "Set to 1 while there is change in AIS state in the related STS/VC to generate an interrupt. Read the OCN Rx STS/VC per Alarm Current Status register of the related STS/VC to know the STS/VC whether in AIS state or not."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5StsPiStsLopStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Tfi5StsPiStsLopStateChgIntr"
            
            def description(self):
                return "Set 1 to while there is change in LOP state in the related STS/VC to generate an interrupt. Read the OCN Rx STS/VC per Alarm Current Status register of the related STS/VC to know the STS/VC whether in LOP state or not."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Tfi5StsPiStsConcDetIntr"] = _AF6CNC0022_RD_OCN._tfi5upstschstkram._Tfi5StsPiStsConcDetIntr()
            allFields["Tfi5StsPiStsNewDetIntr"] = _AF6CNC0022_RD_OCN._tfi5upstschstkram._Tfi5StsPiStsNewDetIntr()
            allFields["Tfi5StsPiStsNdfIntr"] = _AF6CNC0022_RD_OCN._tfi5upstschstkram._Tfi5StsPiStsNdfIntr()
            allFields["Tfi5StsPiCepUneqStatePChgIntr"] = _AF6CNC0022_RD_OCN._tfi5upstschstkram._Tfi5StsPiCepUneqStatePChgIntr()
            allFields["Tfi5StsPiStsAISStateChgIntr"] = _AF6CNC0022_RD_OCN._tfi5upstschstkram._Tfi5StsPiStsAISStateChgIntr()
            allFields["Tfi5StsPiStsLopStateChgIntr"] = _AF6CNC0022_RD_OCN._tfi5upstschstkram._Tfi5StsPiStsLopStateChgIntr()
            return allFields

    class _tfi5upstschstaram(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx STS/VC per Alarm Current Status - TFI5 Side"
    
        def description(self):
            return "This is the per Alarm current status of STS/VC pointer interpreter.  %% Each register is used to store 3 bits to store current status of 3 alarms in the STS/VC."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x02180 + 512*LineId + StsId"
            
        def startAddress(self):
            return 0x00002180
            
        def endAddress(self):
            return 0x00002faf

        class _Tfi5StsPiStsCepUneqPCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Tfi5StsPiStsCepUneqPCurStatus"
            
            def description(self):
                return "CEP Un-eqip Path current status in the related STS/VC. When it changes for 0 to 1 or vice versa, the Tfi5StsPiStsCepUeqPStateChgIntr bit in the OCN Rx STS/VC per Alarm Interrupt Status register of the related STS/VC is set."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5StsPiStsAisCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Tfi5StsPiStsAisCurStatus"
            
            def description(self):
                return "AIS current status in the related STS/VC. When it changes for 0 to 1 or vice versa, the  Tfi5StsPiStsAISStateChgIntr bit in the OCN Rx STS/VC per Alarm Interrupt Status register of the related STS/VC is set."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5StsPiStsLopCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Tfi5StsPiStsLopCurStatus"
            
            def description(self):
                return "LOP current status in the related STS/VC. When it changes for 0 to 1 or vice versa, the  Tfi5StsPiStsLopStateChgIntr bit in the OCN Rx STS/VC per Alarm Interrupt Status register of the related STS/VC is set."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Tfi5StsPiStsCepUneqPCurStatus"] = _AF6CNC0022_RD_OCN._tfi5upstschstaram._Tfi5StsPiStsCepUneqPCurStatus()
            allFields["Tfi5StsPiStsAisCurStatus"] = _AF6CNC0022_RD_OCN._tfi5upstschstaram._Tfi5StsPiStsAisCurStatus()
            allFields["Tfi5StsPiStsLopCurStatus"] = _AF6CNC0022_RD_OCN._tfi5upstschstaram._Tfi5StsPiStsLopCurStatus()
            return allFields

    class _tfi5adjcntperstsram(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx PP STS/VC Pointer interpreter pointer adjustment per channel counter - TFI5 Side"
    
        def description(self):
            return "Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in STS/VC pointer interpreter. These counters are in saturation mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x02080 + 512*LineId + 64*AdjMode + StsId"
            
        def startAddress(self):
            return 0x00002080
            
        def endAddress(self):
            return 0x00002eaf

        class _Tfi5RxPpStsPiPtAdjCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Tfi5RxPpStsPiPtAdjCnt"
            
            def description(self):
                return "The pointer Increment counter or decrease counter. Bit [6] of address is used to indicate that the counter is pointer increment or decrement counter. The counter will stop at maximum value (0x3FFFF)."
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Tfi5RxPpStsPiPtAdjCnt"] = _AF6CNC0022_RD_OCN._tfi5adjcntperstsram._Tfi5RxPpStsPiPtAdjCnt()
            return allFields

    class _tfi5stspgstkram(AtRegister.AtRegister):
        def name(self):
            return "OCN TxPg STS per Alarm Interrupt Status - TFI5 Side"
    
        def description(self):
            return "This is the per Alarm interrupt status of STS pointer generator . Each register is used to store 3 sticky bits for 3 alarms"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x03140 + 512*LineId + StsId"
            
        def startAddress(self):
            return 0x00003140
            
        def endAddress(self):
            return 0x00003f6f

        class _Tfi5StsPgAisIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "Tfi5StsPgAisIntr"
            
            def description(self):
                return "Set to 1 while an AIS status event (at h2pos) is detected at Tx STS Pointer Generator."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5StsPgNdfIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Tfi5StsPgNdfIntr"
            
            def description(self):
                return "Set to 1 while an NDF status event (at h2pos) is detected at Tx STS Pointer Generator."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Tfi5StsPgAisIntr"] = _AF6CNC0022_RD_OCN._tfi5stspgstkram._Tfi5StsPgAisIntr()
            allFields["Tfi5StsPgNdfIntr"] = _AF6CNC0022_RD_OCN._tfi5stspgstkram._Tfi5StsPgNdfIntr()
            return allFields

    class _tfi5adjcntpgperstsram(AtRegister.AtRegister):
        def name(self):
            return "OCN TxPg STS pointer adjustment per channel counter - TFI5 Side"
    
        def description(self):
            return "Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in STS pointer generator. These counters are in saturation mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x03080 + 512*LineId + 64*AdjMode + StsId"
            
        def startAddress(self):
            return 0x00003080
            
        def endAddress(self):
            return 0x00003eaf

        class _Tfi5StsPgPtAdjCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Tfi5StsPgPtAdjCnt"
            
            def description(self):
                return "The pointer Increment counter or decrease counter. Bit [11] of address is used to indicate that the counter is pointer increment or decrement counter. The counter will stop at maximum value (0x3FFFF)."
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Tfi5StsPgPtAdjCnt"] = _AF6CNC0022_RD_OCN._tfi5adjcntpgperstsram._Tfi5StsPgPtAdjCnt()
            return allFields

    class _tfi5rxstsconcdetreg(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx STS/VC Automatically Concatenation Detection - TFI5 Side"
    
        def description(self):
            return "This is the Automatically Concatenation Detection Status at Rx Pointer Interpreter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x02040 + 512*LineId + StsId"
            
        def startAddress(self):
            return 0x00002040
            
        def endAddress(self):
            return 0x00002e6f

        class _Tfi5SpiConDetVc416C(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "Tfi5SpiConDetVc416C"
            
            def description(self):
                return "This is the indicator that this STS is member of 1 VC4_16C, which is automatically detected."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5SpiConDetVc44C(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "Tfi5SpiConDetVc44C"
            
            def description(self):
                return "This is the indicator that this STS is member of 1 VC4_4C, which is automatically detected."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5SpiConDetVc4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "Tfi5SpiConDetVc4"
            
            def description(self):
                return "This is the indicator that this STS is member of 1 VC4, which is automatically detected."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5SpiConDetSlvInd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Tfi5SpiConDetSlvInd"
            
            def description(self):
                return "This is the indicator that this STS is slaver or master which is automatically detected. 1: Slaver."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5SpiConDetMstId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Tfi5SpiConDetMstId"
            
            def description(self):
                return "This is the ID of the master STS-1 in the concatenation that contains this STS-1 which is automatically detected."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Tfi5SpiConDetVc416C"] = _AF6CNC0022_RD_OCN._tfi5rxstsconcdetreg._Tfi5SpiConDetVc416C()
            allFields["Tfi5SpiConDetVc44C"] = _AF6CNC0022_RD_OCN._tfi5rxstsconcdetreg._Tfi5SpiConDetVc44C()
            allFields["Tfi5SpiConDetVc4"] = _AF6CNC0022_RD_OCN._tfi5rxstsconcdetreg._Tfi5SpiConDetVc4()
            allFields["Tfi5SpiConDetSlvInd"] = _AF6CNC0022_RD_OCN._tfi5rxstsconcdetreg._Tfi5SpiConDetSlvInd()
            allFields["Tfi5SpiConDetMstId"] = _AF6CNC0022_RD_OCN._tfi5rxstsconcdetreg._Tfi5SpiConDetMstId()
            return allFields

    class _tfi5txstsconcdetreg(AtRegister.AtRegister):
        def name(self):
            return "OCN Tx STS/VC Automatically Concatenation Detection - TFI5 Side"
    
        def description(self):
            return "This is the Automatically Concatenation Detection Status at Tx Pointer Generator based on Concatenation indicator from SXC."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x03040 + 512*LineId + StsId"
            
        def startAddress(self):
            return 0x00003040
            
        def endAddress(self):
            return 0x00003e6f

        class _Tfi5SpgConDetVc416C(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "Tfi5SpgConDetVc416C"
            
            def description(self):
                return "This is the indicator that this STS is member of 1 VC4_16C, which is automatically detected."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5SpgConDetVc44C(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "Tfi5SpgConDetVc44C"
            
            def description(self):
                return "This is the indicator that this STS is member of 1 VC4_4C, which is automatically detected."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5SpgConDetVc4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "Tfi5SpgConDetVc4"
            
            def description(self):
                return "This is the indicator that this STS is member of 1 VC4, which is automatically detected."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5SpgConDetSlvInd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Tfi5SpgConDetSlvInd"
            
            def description(self):
                return "This is the indicator that this STS is slaver or master which is automatically detected. 1: Slaver."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Tfi5SpgConDetMstId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Tfi5SpgConDetMstId"
            
            def description(self):
                return "This is the ID of the master STS-1 in the concatenation that contains this STS-1 which is automatically detected."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Tfi5SpgConDetVc416C"] = _AF6CNC0022_RD_OCN._tfi5txstsconcdetreg._Tfi5SpgConDetVc416C()
            allFields["Tfi5SpgConDetVc44C"] = _AF6CNC0022_RD_OCN._tfi5txstsconcdetreg._Tfi5SpgConDetVc44C()
            allFields["Tfi5SpgConDetVc4"] = _AF6CNC0022_RD_OCN._tfi5txstsconcdetreg._Tfi5SpgConDetVc4()
            allFields["Tfi5SpgConDetSlvInd"] = _AF6CNC0022_RD_OCN._tfi5txstsconcdetreg._Tfi5SpgConDetSlvInd()
            allFields["Tfi5SpgConDetMstId"] = _AF6CNC0022_RD_OCN._tfi5txstsconcdetreg._Tfi5SpgConDetMstId()
            return allFields

    class _glbrfm_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Rx Framer Control"
    
        def description(self):
            return "This is the global configuration register for the Rx Framer"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x20000 + 65536*GroupID"
            
        def startAddress(self):
            return 0x00020000
            
        def endAddress(self):
            return 0xffffffff

        class _RxFrmOc192Enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "RxFrmOc192Enb"
            
            def description(self):
                return "Enable mode OC192 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmDescrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "RxFrmDescrEn"
            
            def description(self):
                return "Enable/disable de-scrambling of the Rx coming data stream. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmStmOcnMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxFrmStmOcnMode"
            
            def description(self):
                return "STM rate mode for Rx of  8 lines, each line use 2 bits.Bits[1:0] for line 0 0: OC48 1: OC12 2: OC3 3: OC1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxFrmOc192Enb"] = _AF6CNC0022_RD_OCN._glbrfm_reg._RxFrmOc192Enb()
            allFields["RxFrmDescrEn"] = _AF6CNC0022_RD_OCN._glbrfm_reg._RxFrmDescrEn()
            allFields["RxFrmStmOcnMode"] = _AF6CNC0022_RD_OCN._glbrfm_reg._RxFrmStmOcnMode()
            return allFields

    class _glbclkmon_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Rx Framer LOS Detecting Control 1"
    
        def description(self):
            return "This is the global configuration register for the Rx Framer LOS detecting 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x20006 + 65536*GroupID"
            
        def startAddress(self):
            return 0x00020006
            
        def endAddress(self):
            return 0xffffffff

        class _RxFrmLosDetMod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "RxFrmLosDetMod"
            
            def description(self):
                return "Detected LOS mode. 1: the LOS declare when all zero or all one detected 0: the LOS declare when all zero detected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmLosDetDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "RxFrmLosDetDis"
            
            def description(self):
                return "Disable detect LOS. 1: Disable 0: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmClkMonDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "RxFrmClkMonDis"
            
            def description(self):
                return "Disable to generate LOS to Rx Framer if detecting error on Rx line clock. 1: Disable 0: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmClkMonThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxFrmClkMonThr"
            
            def description(self):
                return "Threshold to generate LOS to Rx Framer if detecting error on Rx line clock.(Threshold = PPM * 155.52/8). Default 0x3cc ~ 50ppm"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxFrmLosDetMod"] = _AF6CNC0022_RD_OCN._glbclkmon_reg._RxFrmLosDetMod()
            allFields["RxFrmLosDetDis"] = _AF6CNC0022_RD_OCN._glbclkmon_reg._RxFrmLosDetDis()
            allFields["RxFrmClkMonDis"] = _AF6CNC0022_RD_OCN._glbclkmon_reg._RxFrmClkMonDis()
            allFields["RxFrmClkMonThr"] = _AF6CNC0022_RD_OCN._glbclkmon_reg._RxFrmClkMonThr()
            return allFields

    class _glbdetlos_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Rx Framer LOS Detecting Control 2"
    
        def description(self):
            return "This is the global configuration register for the Rx Framer LOS detecting 2"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x20007 + 65536*GroupID"
            
        def startAddress(self):
            return 0x00020007
            
        def endAddress(self):
            return 0xffffffff

        class _RxFrmLosClr2Thr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "RxFrmLosClr2Thr"
            
            def description(self):
                return "Configure the period of time during which there is no period of consecutive data without transition, used for reset no transition counter. The recommended value is 0x18 (~2.5ms)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmLosSetThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 12
        
            def name(self):
                return "RxFrmLosSetThr"
            
            def description(self):
                return "Configure the value that define the period of time in which there is no transition detected from incoming data from SERDES. The recommended value is 0x1e6 (~50ms)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmLosClr1Thr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxFrmLosClr1Thr"
            
            def description(self):
                return "Configure the period of time during which there is no period of consecutive data without transition, used for counting at INFRM to change state to OOF The recommended value is 0x3cc (~50ms)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxFrmLosClr2Thr"] = _AF6CNC0022_RD_OCN._glbdetlos_reg._RxFrmLosClr2Thr()
            allFields["RxFrmLosSetThr"] = _AF6CNC0022_RD_OCN._glbdetlos_reg._RxFrmLosSetThr()
            allFields["RxFrmLosClr1Thr"] = _AF6CNC0022_RD_OCN._glbdetlos_reg._RxFrmLosClr1Thr()
            return allFields

    class _glblofthr_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Rx Framer LOF Threshold"
    
        def description(self):
            return "Configure thresholds for LOF detection at receive SONET/SDH framer,There are two thresholds"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x20008 + 65536*GroupID"
            
        def startAddress(self):
            return 0x00020008
            
        def endAddress(self):
            return 0xffffffff

        class _RxFrmLofDetMod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "RxFrmLofDetMod"
            
            def description(self):
                return "Detected LOF mode.Set 1 to clear OOF counter when state into INFRAMED"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmLofSetThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "RxFrmLofSetThr"
            
            def description(self):
                return "Configure the OOF time counter threshold for entering LOF state. Resolution of this threshold is one frame."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmLofClrThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxFrmLofClrThr"
            
            def description(self):
                return "Configure the In-frame time counter threshold for exiting LOF state. Resolution of this threshold is one frame."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxFrmLofDetMod"] = _AF6CNC0022_RD_OCN._glblofthr_reg._RxFrmLofDetMod()
            allFields["RxFrmLofSetThr"] = _AF6CNC0022_RD_OCN._glblofthr_reg._RxFrmLofSetThr()
            allFields["RxFrmLofClrThr"] = _AF6CNC0022_RD_OCN._glblofthr_reg._RxFrmLofClrThr()
            return allFields

    class _glbrfmaisfwd_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Rx Framer - AIS dowstream enable"
    
        def description(self):
            return "Enable/disable for AIS dowstream at receive SONET/SDH framer of 8 lines, each line used 4bits,Bits[3:0] for line 0"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x2000d + 65536*GroupID"
            
        def startAddress(self):
            return 0x0002000d
            
        def endAddress(self):
            return 0xffffffff

        class _RxFrmAisFwdEn2_8(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RxFrmAisFwdEn2_8"
            
            def description(self):
                return "Enable/disable for AIS dowstream at receive SONET/SDH framer of line2-8, each line used 4bits,Bits[3:0] for line 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmAislAisPEn1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "RxFrmAislAisPEn1"
            
            def description(self):
                return "Enable/disable the insertion of P-AIS at STS pointer interpreter when AIS_L condition detected. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmTimAisPEn1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "RxFrmTimAisPEn1"
            
            def description(self):
                return "Enable/disable the insertion of P-AIS at STS pointer interpreter when TIM condition detected. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmLofAisPEn1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "RxFrmLofAisPEn1"
            
            def description(self):
                return "Enable/disable the insertion of P-AIS at STS pointer interpreter when LOF condition detected. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmLosAisPEn1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxFrmLosAisPEn1"
            
            def description(self):
                return "Enable/disable the insertion of P-AIS at STS pointer interpreter when LOS condition detected. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxFrmAisFwdEn2_8"] = _AF6CNC0022_RD_OCN._glbrfmaisfwd_reg._RxFrmAisFwdEn2_8()
            allFields["RxFrmAislAisPEn1"] = _AF6CNC0022_RD_OCN._glbrfmaisfwd_reg._RxFrmAislAisPEn1()
            allFields["RxFrmTimAisPEn1"] = _AF6CNC0022_RD_OCN._glbrfmaisfwd_reg._RxFrmTimAisPEn1()
            allFields["RxFrmLofAisPEn1"] = _AF6CNC0022_RD_OCN._glbrfmaisfwd_reg._RxFrmLofAisPEn1()
            allFields["RxFrmLosAisPEn1"] = _AF6CNC0022_RD_OCN._glbrfmaisfwd_reg._RxFrmLosAisPEn1()
            return allFields

    class _glbrfmrdibwd_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Rx Framer - RDI Back toward enable"
    
        def description(self):
            return "Enable/disable for RDI Back toward at transceive SONET/SDH framer of 8 lines, each line used 4bits,Bits[3:0] for line 0"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x2000e + 65536*GroupID"
            
        def startAddress(self):
            return 0x0002000e
            
        def endAddress(self):
            return 0xffffffff

        class _RxFrmAisFwdEn2_8(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RxFrmAisFwdEn2_8"
            
            def description(self):
                return "Enable/disable for RDI Back toward at transceive SONET/SDH framer of line2-8, each line used 4bits,Bits[3:0] for line 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmAislRdiLEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "RxFrmAislRdiLEn"
            
            def description(self):
                return "Enable/disable the insertion of RDI-L at TOH insertion when AIS_L condition detected. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmTimRdiLEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "RxFrmTimRdiLEn"
            
            def description(self):
                return "Enable/disable the insertion of RDI-L at TOH insertion when TIM condition detected. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmLofRdiLEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "RxFrmLofRdiLEn"
            
            def description(self):
                return "Enable/disable the insertion of RDI-L at TOH insertion when LOF condition detected. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmLosRdiLEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxFrmLosRdiLEn"
            
            def description(self):
                return "Enable/disable the insertion of RDI-L at TOH insertion when LOS condition detected. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxFrmAisFwdEn2_8"] = _AF6CNC0022_RD_OCN._glbrfmrdibwd_reg._RxFrmAisFwdEn2_8()
            allFields["RxFrmAislRdiLEn"] = _AF6CNC0022_RD_OCN._glbrfmrdibwd_reg._RxFrmAislRdiLEn()
            allFields["RxFrmTimRdiLEn"] = _AF6CNC0022_RD_OCN._glbrfmrdibwd_reg._RxFrmTimRdiLEn()
            allFields["RxFrmLofRdiLEn"] = _AF6CNC0022_RD_OCN._glbrfmrdibwd_reg._RxFrmLofRdiLEn()
            allFields["RxFrmLosRdiLEn"] = _AF6CNC0022_RD_OCN._glbrfmrdibwd_reg._RxFrmLosRdiLEn()
            return allFields

    class _glbtfm_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Tx Framer Control"
    
        def description(self):
            return "This is the global configuration register for the Tx Framer"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x20001 + 65536*GroupID"
            
        def startAddress(self):
            return 0x00020001
            
        def endAddress(self):
            return 0xffffffff

        class _TxFrmOc192Enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "TxFrmOc192Enb"
            
            def description(self):
                return "Enable mode OC192 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxFrmScrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "TxFrmScrEn"
            
            def description(self):
                return "Enable/disable scrambling of the Tx data stream. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxFrmStmOcnMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxFrmStmOcnMode"
            
            def description(self):
                return "STM rate mode for Tx of  8 lines, each line use 2 bits.Bits[1:0] for line 0 0: OC48 1: OC12 2: OC3 3: OC1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxFrmOc192Enb"] = _AF6CNC0022_RD_OCN._glbtfm_reg._TxFrmOc192Enb()
            allFields["TxFrmScrEn"] = _AF6CNC0022_RD_OCN._glbtfm_reg._TxFrmScrEn()
            allFields["TxFrmStmOcnMode"] = _AF6CNC0022_RD_OCN._glbtfm_reg._TxFrmStmOcnMode()
            return allFields

    class _glbtfmfrc_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Tx Framer Control"
    
        def description(self):
            return "This is the global configuration register for error forcing to Tx Framer"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x2000a + 65536*GroupID"
            
        def startAddress(self):
            return 0x0002000a
            
        def endAddress(self):
            return 0xffffffff

        class _TxLineOofFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TxLineOofFrc"
            
            def description(self):
                return "Enable/disable force OOF      for 8 lines. Bit[0] for line 0."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxLineB1ErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxLineB1ErrFrc"
            
            def description(self):
                return "Enable/disable force B1 error for 8 lines. Bit[0] for line 0."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxLineOofFrc"] = _AF6CNC0022_RD_OCN._glbtfmfrc_reg._TxLineOofFrc()
            allFields["TxLineB1ErrFrc"] = _AF6CNC0022_RD_OCN._glbtfmfrc_reg._TxLineB1ErrFrc()
            return allFields

    class _glbrdiinsthr_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Tx Framer RDI-L Insertion Threshold Control"
    
        def description(self):
            return "Configure the number of frames in which L-RDI will be inserted when an L-RDI event is triggered. And Pattern to insert into Z0 bytes. The register contains two numbers"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x20009 + 65536*GroupID"
            
        def startAddress(self):
            return 0x00020009
            
        def endAddress(self):
            return 0xffffffff

        class _TxFrmRdiInsThr2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 24
        
            def name(self):
                return "TxFrmRdiInsThr2"
            
            def description(self):
                return "Threshold 2 for SDH mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxFrmRdiInsThr1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 16
        
            def name(self):
                return "TxFrmRdiInsThr1"
            
            def description(self):
                return "Threshold 1 for Sonet mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxFrmOhBusDccEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TxFrmOhBusDccEnb"
            
            def description(self):
                return "Set 1 to enable insert DCC bytes from OHBUS per 8 lines. Bit[0] for line 0."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNTxZ0Pat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "OCNTxZ0Pat"
            
            def description(self):
                return "Pattern 1 to insert into Z0 bytes."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxFrmRdiInsThr2"] = _AF6CNC0022_RD_OCN._glbrdiinsthr_reg._TxFrmRdiInsThr2()
            allFields["TxFrmRdiInsThr1"] = _AF6CNC0022_RD_OCN._glbrdiinsthr_reg._TxFrmRdiInsThr1()
            allFields["TxFrmOhBusDccEnb"] = _AF6CNC0022_RD_OCN._glbrdiinsthr_reg._TxFrmOhBusDccEnb()
            allFields["OCNTxZ0Pat"] = _AF6CNC0022_RD_OCN._glbrdiinsthr_reg._OCNTxZ0Pat()
            return allFields

    class _glbrxslcppsel1_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Rx Framer Located Slice Pointer Selection 1"
    
        def description(self):
            return "Configure location in Slice Pointer for Rx lines. This regester config for line 0-3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x20010 + 65536*GroupID"
            
        def startAddress(self):
            return 0x00020010
            
        def endAddress(self):
            return 0xffffffff

        class _RxFrmSlcPpEnb3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "RxFrmSlcPpEnb3"
            
            def description(self):
                return "Write 1 to enable for Line 3."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmSlcPpSel3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 24
        
            def name(self):
                return "RxFrmSlcPpSel3"
            
            def description(self):
                return "Configure location in Slice Pointer for Rx line 3, It includes"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmSlcPpEnb2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "RxFrmSlcPpEnb2"
            
            def description(self):
                return "Write 1 to enable for Line 2."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmSlcPpSel2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 16
        
            def name(self):
                return "RxFrmSlcPpSel2"
            
            def description(self):
                return "Configure location in Slice Pointer for Rx line 2, It includes"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmSlcPpEnb1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "RxFrmSlcPpEnb1"
            
            def description(self):
                return "Write 1 to enable for Line 1."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmSlcPpSel1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 8
        
            def name(self):
                return "RxFrmSlcPpSel1"
            
            def description(self):
                return "Configure location in Slice Pointer for Rx line 1, It includes"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmSlcPpEnb0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "RxFrmSlcPpEnb0"
            
            def description(self):
                return "Write 1 to enable for Line 0."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmSlcPpSel0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxFrmSlcPpSel0"
            
            def description(self):
                return "Configure location in Slice Pointer for Rx line 0, It includes"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxFrmSlcPpEnb3"] = _AF6CNC0022_RD_OCN._glbrxslcppsel1_reg._RxFrmSlcPpEnb3()
            allFields["RxFrmSlcPpSel3"] = _AF6CNC0022_RD_OCN._glbrxslcppsel1_reg._RxFrmSlcPpSel3()
            allFields["RxFrmSlcPpEnb2"] = _AF6CNC0022_RD_OCN._glbrxslcppsel1_reg._RxFrmSlcPpEnb2()
            allFields["RxFrmSlcPpSel2"] = _AF6CNC0022_RD_OCN._glbrxslcppsel1_reg._RxFrmSlcPpSel2()
            allFields["RxFrmSlcPpEnb1"] = _AF6CNC0022_RD_OCN._glbrxslcppsel1_reg._RxFrmSlcPpEnb1()
            allFields["RxFrmSlcPpSel1"] = _AF6CNC0022_RD_OCN._glbrxslcppsel1_reg._RxFrmSlcPpSel1()
            allFields["RxFrmSlcPpEnb0"] = _AF6CNC0022_RD_OCN._glbrxslcppsel1_reg._RxFrmSlcPpEnb0()
            allFields["RxFrmSlcPpSel0"] = _AF6CNC0022_RD_OCN._glbrxslcppsel1_reg._RxFrmSlcPpSel0()
            return allFields

    class _glbrxslcppsel2_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Rx Framer Located Slice Pointer Selection 2"
    
        def description(self):
            return "Configure location in Slice Pointer for Rx lines. This regester config for line 4-7"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x20011 + 65536*GroupID"
            
        def startAddress(self):
            return 0x00020011
            
        def endAddress(self):
            return 0xffffffff

        class _RxFrmSlcPpEnb7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "RxFrmSlcPpEnb7"
            
            def description(self):
                return "Write 1 to enable for Line 7."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmSlcPpSel7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 24
        
            def name(self):
                return "RxFrmSlcPpSel7"
            
            def description(self):
                return "Configure location in Slice Pointer for Rx line 7, It includes"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmSlcPpEnb6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "RxFrmSlcPpEnb6"
            
            def description(self):
                return "Write 1 to enable for Line 6."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmSlcPpSel6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 16
        
            def name(self):
                return "RxFrmSlcPpSel6"
            
            def description(self):
                return "Configure location in Slice Pointer for Rx line 6, It includes"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmSlcPpEnb5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "RxFrmSlcPpEnb5"
            
            def description(self):
                return "Write 1 to enable for Line 5."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmSlcPpSel5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 8
        
            def name(self):
                return "RxFrmSlcPpSel5"
            
            def description(self):
                return "Configure location in Slice Pointer for Rx line 5, It includes"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmSlcPpEnb4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "RxFrmSlcPpEnb4"
            
            def description(self):
                return "Write 1 to enable for Line 4."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmSlcPpSel4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxFrmSlcPpSel4"
            
            def description(self):
                return "Configure location in Slice Pointer for Rx line 4, It includes"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxFrmSlcPpEnb7"] = _AF6CNC0022_RD_OCN._glbrxslcppsel2_reg._RxFrmSlcPpEnb7()
            allFields["RxFrmSlcPpSel7"] = _AF6CNC0022_RD_OCN._glbrxslcppsel2_reg._RxFrmSlcPpSel7()
            allFields["RxFrmSlcPpEnb6"] = _AF6CNC0022_RD_OCN._glbrxslcppsel2_reg._RxFrmSlcPpEnb6()
            allFields["RxFrmSlcPpSel6"] = _AF6CNC0022_RD_OCN._glbrxslcppsel2_reg._RxFrmSlcPpSel6()
            allFields["RxFrmSlcPpEnb5"] = _AF6CNC0022_RD_OCN._glbrxslcppsel2_reg._RxFrmSlcPpEnb5()
            allFields["RxFrmSlcPpSel5"] = _AF6CNC0022_RD_OCN._glbrxslcppsel2_reg._RxFrmSlcPpSel5()
            allFields["RxFrmSlcPpEnb4"] = _AF6CNC0022_RD_OCN._glbrxslcppsel2_reg._RxFrmSlcPpEnb4()
            allFields["RxFrmSlcPpSel4"] = _AF6CNC0022_RD_OCN._glbrxslcppsel2_reg._RxFrmSlcPpSel4()
            return allFields

    class _glbtxslcppsel1_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Tx Framer Located Slice Pointer Selection 1"
    
        def description(self):
            return "Configure location in Slice Pointer for Tx lines. This regester config for line 0-3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x20012 + 65536*GroupID"
            
        def startAddress(self):
            return 0x00020012
            
        def endAddress(self):
            return 0xffffffff

        class _TxFrmSlcPpEnb3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "TxFrmSlcPpEnb3"
            
            def description(self):
                return "Write 1 to enable for Line 3."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxFrmSlcPpSel3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 24
        
            def name(self):
                return "TxFrmSlcPpSel3"
            
            def description(self):
                return "Configure location in Slice Pointer for Tx line 3, It includes"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxFrmSlcPpEnb2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "TxFrmSlcPpEnb2"
            
            def description(self):
                return "Write 1 to enable for Line 2."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxFrmSlcPpSel2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 16
        
            def name(self):
                return "TxFrmSlcPpSel2"
            
            def description(self):
                return "Configure location in Slice Pointer for Tx line 2, It includes"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxFrmSlcPpEnb1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "TxFrmSlcPpEnb1"
            
            def description(self):
                return "Write 1 to enable for Line 1."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxFrmSlcPpSel1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TxFrmSlcPpSel1"
            
            def description(self):
                return "Configure location in Slice Pointer for Tx line 1, It includes"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxFrmSlcPpEnb0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "TxFrmSlcPpEnb0"
            
            def description(self):
                return "Write 1 to enable for Line 0."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxFrmSlcPpSel0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxFrmSlcPpSel0"
            
            def description(self):
                return "Configure location in Slice Pointer for Tx line 0, It includes"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxFrmSlcPpEnb3"] = _AF6CNC0022_RD_OCN._glbtxslcppsel1_reg._TxFrmSlcPpEnb3()
            allFields["TxFrmSlcPpSel3"] = _AF6CNC0022_RD_OCN._glbtxslcppsel1_reg._TxFrmSlcPpSel3()
            allFields["TxFrmSlcPpEnb2"] = _AF6CNC0022_RD_OCN._glbtxslcppsel1_reg._TxFrmSlcPpEnb2()
            allFields["TxFrmSlcPpSel2"] = _AF6CNC0022_RD_OCN._glbtxslcppsel1_reg._TxFrmSlcPpSel2()
            allFields["TxFrmSlcPpEnb1"] = _AF6CNC0022_RD_OCN._glbtxslcppsel1_reg._TxFrmSlcPpEnb1()
            allFields["TxFrmSlcPpSel1"] = _AF6CNC0022_RD_OCN._glbtxslcppsel1_reg._TxFrmSlcPpSel1()
            allFields["TxFrmSlcPpEnb0"] = _AF6CNC0022_RD_OCN._glbtxslcppsel1_reg._TxFrmSlcPpEnb0()
            allFields["TxFrmSlcPpSel0"] = _AF6CNC0022_RD_OCN._glbtxslcppsel1_reg._TxFrmSlcPpSel0()
            return allFields

    class _glbtxslcppsel2_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Tx Framer Located Slice Pointer Selection 2"
    
        def description(self):
            return "Configure location in Slice Pointer for Tx lines. This regester config for line 4-7"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x20013 + 65536*GroupID"
            
        def startAddress(self):
            return 0x00020013
            
        def endAddress(self):
            return 0xffffffff

        class _TxFrmSlcPpEnb7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "TxFrmSlcPpEnb7"
            
            def description(self):
                return "Write 1 to enable for Line 7."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxFrmSlcPpSel7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 24
        
            def name(self):
                return "TxFrmSlcPpSel7"
            
            def description(self):
                return "Configure location in Slice Pointer for Tx line 7, It includes"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxFrmSlcPpEnb6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "TxFrmSlcPpEnb6"
            
            def description(self):
                return "Write 1 to enable for Line 6."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxFrmSlcPpSel6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 16
        
            def name(self):
                return "TxFrmSlcPpSel6"
            
            def description(self):
                return "Configure location in Slice Pointer for Tx line 6, It includes"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxFrmSlcPpEnb5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "TxFrmSlcPpEnb5"
            
            def description(self):
                return "Write 1 to enable for Line 5."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxFrmSlcPpSel5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TxFrmSlcPpSel5"
            
            def description(self):
                return "Configure location in Slice Pointer for Tx line 5, It includes"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxFrmSlcPpEnb4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "TxFrmSlcPpEnb4"
            
            def description(self):
                return "Write 1 to enable for Line 4."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxFrmSlcPpSel4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxFrmSlcPpSel4"
            
            def description(self):
                return "Configure location in Slice Pointer for Tx line 4, It includes"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxFrmSlcPpEnb7"] = _AF6CNC0022_RD_OCN._glbtxslcppsel2_reg._TxFrmSlcPpEnb7()
            allFields["TxFrmSlcPpSel7"] = _AF6CNC0022_RD_OCN._glbtxslcppsel2_reg._TxFrmSlcPpSel7()
            allFields["TxFrmSlcPpEnb6"] = _AF6CNC0022_RD_OCN._glbtxslcppsel2_reg._TxFrmSlcPpEnb6()
            allFields["TxFrmSlcPpSel6"] = _AF6CNC0022_RD_OCN._glbtxslcppsel2_reg._TxFrmSlcPpSel6()
            allFields["TxFrmSlcPpEnb5"] = _AF6CNC0022_RD_OCN._glbtxslcppsel2_reg._TxFrmSlcPpEnb5()
            allFields["TxFrmSlcPpSel5"] = _AF6CNC0022_RD_OCN._glbtxslcppsel2_reg._TxFrmSlcPpSel5()
            allFields["TxFrmSlcPpEnb4"] = _AF6CNC0022_RD_OCN._glbtxslcppsel2_reg._TxFrmSlcPpEnb4()
            allFields["TxFrmSlcPpSel4"] = _AF6CNC0022_RD_OCN._glbtxslcppsel2_reg._TxFrmSlcPpSel4()
            return allFields

    class _glbrxtohbusctl_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Rx_TOHBUS K Byte Control"
    
        def description(self):
            return "Configure TOHBUS for K Bytes at RX OCN."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x20014 + 65536*GroupID"
            
        def startAddress(self):
            return 0x00020014
            
        def endAddress(self):
            return 0xffffffff

        class _RxTohBusKextCtl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxTohBusKextCtl"
            
            def description(self):
                return "K Byte Configuration for 8 Lines at Rx OCN. Each line use 4 bits[3:0]. Bit[3] :  Unused Bit[2] :  Disable Masking FF to Rx_OHBUS when Section alarm happen at Rx OCN. Set 1 to disable. Bit[1] :  Enable K_extention Byte  at Rx OCN. Set 1 to enable. Bit[0] :  Select K_extention Byte is D1_4 byte or D1_10 byte at Rx OCN. Set 1 to choose D1_4 byte."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxTohBusKextCtl"] = _AF6CNC0022_RD_OCN._glbrxtohbusctl_reg._RxTohBusKextCtl()
            return allFields

    class _glbtohbusthr_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global TOHBUS K_Byte Threshold"
    
        def description(self):
            return "Configure TOHBUS for K_extention Bytes."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x20015 + 65536*GroupID"
            
        def startAddress(self):
            return 0x00020015
            
        def endAddress(self):
            return 0xffffffff

        class _RxTohBusKeStbThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "RxTohBusKeStbThr"
            
            def description(self):
                return "Stable threshold Configuration for K_extention Byte."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxTohBusK2StbThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RxTohBusK2StbThr"
            
            def description(self):
                return "Stable threshold Configuration for K2 Byte."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxTohBusK1StbThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxTohBusK1StbThr"
            
            def description(self):
                return "Stable threshold Configuration for K1 Byte."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxTohBusKeStbThr"] = _AF6CNC0022_RD_OCN._glbtohbusthr_reg._RxTohBusKeStbThr()
            allFields["RxTohBusK2StbThr"] = _AF6CNC0022_RD_OCN._glbtohbusthr_reg._RxTohBusK2StbThr()
            allFields["RxTohBusK1StbThr"] = _AF6CNC0022_RD_OCN._glbtohbusthr_reg._RxTohBusK1StbThr()
            return allFields

    class _glbtxtohbusctl_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Tx_TOHBUS K Byte Control"
    
        def description(self):
            return "Configure TOHBUS for K Bytes at TX OCN."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x20016 + 65536*GroupID"
            
        def startAddress(self):
            return 0x00020016
            
        def endAddress(self):
            return 0xffffffff

        class _TxTohBusKextCtl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxTohBusKextCtl"
            
            def description(self):
                return "K Byte Configuration for 8 Lines at Tx OCN. Each line use 4 bits[3:0]. Bit[3] :  Unused Bit[2] :  Disable propagation suppression RDI,AIS in K2[2:0] at Tx OCN. Default is enable this function - when detect RDI/AIS at K2[2:0] from OHBUS, we must overwrite 3'b000 into K2[2:0]. Set 1 to disable. Bit[1] :  Enable K_extention Byte  at Tx OCN. Set 1 to enable. Bit[0] :  Select K_extention Byte is D1_4 byte or D1_10 byte at Tx OCN. Set 1 to choose D1_4 byte."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxTohBusKextCtl"] = _AF6CNC0022_RD_OCN._glbtxtohbusctl_reg._TxTohBusKextCtl()
            return allFields

    class _spiramctl(AtRegister.AtRegister):
        def name(self):
            return "OCN STS Pointer Interpreter Per Channel Control"
    
        def description(self):
            return "Each register is used to configure for STS pointer interpreter engines of the STS-1/VC-3. # HDL_PATH		: itfi5ho.irxpp_stspp_inst.irxpp_stspp[0].rxpp_stspiramctl.array"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x22000 + 65536*GroupID + 512*LineId + StsId"
            
        def startAddress(self):
            return 0x00022000
            
        def endAddress(self):
            return 0x00022e2f

        class _LinePassThrEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "LinePassThrEnb"
            
            def description(self):
                return "Enable/disable Passthrough mode for BLSR at Rx SPI of FacePlate Side 1: Enable. 0: Disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LinePassThrGrp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "LinePassThrGrp"
            
            def description(self):
                return "Passthrough Group ID for BLSR at Rx SPI of FacePlate Side."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LineStsPiChkLom(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "LineStsPiChkLom"
            
            def description(self):
                return "Enable/disable LOM checking. This field will be set to 1 when payload type of VC3/VC4 includes any VC11/VC12 1: Enable. 0: Disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LineStsPiSSDetPatt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "LineStsPiSSDetPatt"
            
            def description(self):
                return "Configure pattern SS bits that is used to compare with the extracted SS bits from receive direction."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LineStsPiAisFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "LineStsPiAisFrc"
            
            def description(self):
                return "Forcing SFM to AIS state. 1: Force 0: Not force"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LineStsPiSSDetEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "LineStsPiSSDetEn"
            
            def description(self):
                return "Enable/disable checking SS bits in STSPI state machine. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LineStsPiAdjRule(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "LineStsPiAdjRule"
            
            def description(self):
                return "Configure the rule for detecting adjustment condition. 1: The n of 5 rule is selected. This mode is applied for SDH mode 0: The 8 of 10 rule is selected. This mode is applied for SONET mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LineStsPiStsSlvInd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "LineStsPiStsSlvInd"
            
            def description(self):
                return "This is used to configure STS is slaver or master. 1: Slaver."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LineStsPiStsMstId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LineStsPiStsMstId"
            
            def description(self):
                return "This is the ID of the master STS-1 in the concatenation that contains this STS-1."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["LinePassThrEnb"] = _AF6CNC0022_RD_OCN._spiramctl._LinePassThrEnb()
            allFields["LinePassThrGrp"] = _AF6CNC0022_RD_OCN._spiramctl._LinePassThrGrp()
            allFields["LineStsPiChkLom"] = _AF6CNC0022_RD_OCN._spiramctl._LineStsPiChkLom()
            allFields["LineStsPiSSDetPatt"] = _AF6CNC0022_RD_OCN._spiramctl._LineStsPiSSDetPatt()
            allFields["LineStsPiAisFrc"] = _AF6CNC0022_RD_OCN._spiramctl._LineStsPiAisFrc()
            allFields["LineStsPiSSDetEn"] = _AF6CNC0022_RD_OCN._spiramctl._LineStsPiSSDetEn()
            allFields["LineStsPiAdjRule"] = _AF6CNC0022_RD_OCN._spiramctl._LineStsPiAdjRule()
            allFields["LineStsPiStsSlvInd"] = _AF6CNC0022_RD_OCN._spiramctl._LineStsPiStsSlvInd()
            allFields["LineStsPiStsMstId"] = _AF6CNC0022_RD_OCN._spiramctl._LineStsPiStsMstId()
            return allFields

    class _spgramctl(AtRegister.AtRegister):
        def name(self):
            return "OCN STS Pointer Generator Per Channel Control"
    
        def description(self):
            return "Each register is used to configure for STS pointer Generator engines. # HDL_PATH		: itfi5ho.itxpp_stspp_inst.itxpp_stspp[0].txpg_stspgctl.array"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x23000 + 65536*GroupID + 512*LineId + StsId"
            
        def startAddress(self):
            return 0x00023000
            
        def endAddress(self):
            return 0x00023e2f

        class _LineStsPgStsSlvInd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "LineStsPgStsSlvInd"
            
            def description(self):
                return "This is used to configure STS is slaver or master. 1: Slaver."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LineStsPgStsMstId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 8
        
            def name(self):
                return "LineStsPgStsMstId"
            
            def description(self):
                return "This is the ID of the master STS-1 in the concatenation that contains this STS-1."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LineStsPgB3BipErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "LineStsPgB3BipErrFrc"
            
            def description(self):
                return "Forcing B3 Bip error. 1: Force 0: Not force"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LineStsPgLopFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "LineStsPgLopFrc"
            
            def description(self):
                return "Forcing LOP. 1: Force 0: Not force"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LineStsPgUeqFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "LineStsPgUeqFrc"
            
            def description(self):
                return "Forcing SFM to UEQ state. 1: Force 0: Not force"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LineStsPgAisFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "LineStsPgAisFrc"
            
            def description(self):
                return "Forcing SFM to AIS state. 1: Force 0: Not force"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LineStsPgSSInsPatt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "LineStsPgSSInsPatt"
            
            def description(self):
                return "Configure pattern SS bits that is used to insert to pointer value."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LineStsPgSSInsEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "LineStsPgSSInsEn"
            
            def description(self):
                return "Enable/disable SS bits insertion. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LineStsPgPohIns(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LineStsPgPohIns"
            
            def description(self):
                return "Enable/disable POH Insertion. High to enable insertion of POH."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["LineStsPgStsSlvInd"] = _AF6CNC0022_RD_OCN._spgramctl._LineStsPgStsSlvInd()
            allFields["LineStsPgStsMstId"] = _AF6CNC0022_RD_OCN._spgramctl._LineStsPgStsMstId()
            allFields["LineStsPgB3BipErrFrc"] = _AF6CNC0022_RD_OCN._spgramctl._LineStsPgB3BipErrFrc()
            allFields["LineStsPgLopFrc"] = _AF6CNC0022_RD_OCN._spgramctl._LineStsPgLopFrc()
            allFields["LineStsPgUeqFrc"] = _AF6CNC0022_RD_OCN._spgramctl._LineStsPgUeqFrc()
            allFields["LineStsPgAisFrc"] = _AF6CNC0022_RD_OCN._spgramctl._LineStsPgAisFrc()
            allFields["LineStsPgSSInsPatt"] = _AF6CNC0022_RD_OCN._spgramctl._LineStsPgSSInsPatt()
            allFields["LineStsPgSSInsEn"] = _AF6CNC0022_RD_OCN._spgramctl._LineStsPgSSInsEn()
            allFields["LineStsPgPohIns"] = _AF6CNC0022_RD_OCN._spgramctl._LineStsPgPohIns()
            return allFields

    class _upstschstkram(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx STS/VC per Alarm Interrupt Status"
    
        def description(self):
            return "This is the per Alarm interrupt status of STS/VC pointer interpreter.  %% Each register is used to store 6 sticky bits for 6 alarms in the STS/VC."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x22140 + 65536*GroupID + 512*LineId + StsId"
            
        def startAddress(self):
            return 0x00022140
            
        def endAddress(self):
            return 0x00022f6f

        class _LineStsPiStsConcDetIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "LineStsPiStsConcDetIntr"
            
            def description(self):
                return "Set to 1 while an Concatenation Detection event is detected at STS/VC pointer interpreter. This event doesn't raise interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _LineStsPiStsNewDetIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "LineStsPiStsNewDetIntr"
            
            def description(self):
                return "Set to 1 while an New Pointer Detection event is detected at STS/VC pointer interpreter. This event doesn't raise interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _LineStsPiStsNdfIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "LineStsPiStsNdfIntr"
            
            def description(self):
                return "Set to 1 while an NDF event is detected at STS/VC pointer interpreter. This event doesn't raise interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _LineStsPiCepUneqStatePChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "LineStsPiCepUneqStatePChgIntr"
            
            def description(self):
                return "Set to 1  while there is change in CEP Un-equip Path in the related STS/VC to generate an interrupt. Read the OCN Rx STS/VC per Alarm Current Status register of the related STS/VC to know the STS/VC whether in CEP Un-equip Path state or not."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _LineStsPiStsAISStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "LineStsPiStsAISStateChgIntr"
            
            def description(self):
                return "Set to 1 while there is change in AIS state in the related STS/VC to generate an interrupt. Read the OCN Rx STS/VC per Alarm Current Status register of the related STS/VC to know the STS/VC whether in AIS state or not."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _LineStsPiStsLopStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LineStsPiStsLopStateChgIntr"
            
            def description(self):
                return "Set 1 to while there is change in LOP state in the related STS/VC to generate an interrupt. Read the OCN Rx STS/VC per Alarm Current Status register of the related STS/VC to know the STS/VC whether in LOP state or not."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["LineStsPiStsConcDetIntr"] = _AF6CNC0022_RD_OCN._upstschstkram._LineStsPiStsConcDetIntr()
            allFields["LineStsPiStsNewDetIntr"] = _AF6CNC0022_RD_OCN._upstschstkram._LineStsPiStsNewDetIntr()
            allFields["LineStsPiStsNdfIntr"] = _AF6CNC0022_RD_OCN._upstschstkram._LineStsPiStsNdfIntr()
            allFields["LineStsPiCepUneqStatePChgIntr"] = _AF6CNC0022_RD_OCN._upstschstkram._LineStsPiCepUneqStatePChgIntr()
            allFields["LineStsPiStsAISStateChgIntr"] = _AF6CNC0022_RD_OCN._upstschstkram._LineStsPiStsAISStateChgIntr()
            allFields["LineStsPiStsLopStateChgIntr"] = _AF6CNC0022_RD_OCN._upstschstkram._LineStsPiStsLopStateChgIntr()
            return allFields

    class _upstschstaram(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx STS/VC per Alarm Current Status"
    
        def description(self):
            return "This is the per Alarm current status of STS/VC pointer interpreter.  %% Each register is used to store 3 bits to store current status of 3 alarms in the STS/VC."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x22180 + 65536*GroupID + 512*LineId + StsId"
            
        def startAddress(self):
            return 0x00022180
            
        def endAddress(self):
            return 0x00022faf

        class _LineStsPiStsCepUneqPCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "LineStsPiStsCepUneqPCurStatus"
            
            def description(self):
                return "CEP Un-eqip Path current status in the related STS/VC. When it changes for 0 to 1 or vice versa, the LineStsPiStsCepUeqPStateChgIntr bit in the OCN Rx STS/VC per Alarm Interrupt Status register of the related STS/VC is set."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _LineStsPiStsAisCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "LineStsPiStsAisCurStatus"
            
            def description(self):
                return "AIS current status in the related STS/VC. When it changes for 0 to 1 or vice versa, the  LineStsPiStsAISStateChgIntr bit in the OCN Rx STS/VC per Alarm Interrupt Status register of the related STS/VC is set."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _LineStsPiStsLopCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LineStsPiStsLopCurStatus"
            
            def description(self):
                return "LOP current status in the related STS/VC. When it changes for 0 to 1 or vice versa, the  LineStsPiStsLopStateChgIntr bit in the OCN Rx STS/VC per Alarm Interrupt Status register of the related STS/VC is set."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["LineStsPiStsCepUneqPCurStatus"] = _AF6CNC0022_RD_OCN._upstschstaram._LineStsPiStsCepUneqPCurStatus()
            allFields["LineStsPiStsAisCurStatus"] = _AF6CNC0022_RD_OCN._upstschstaram._LineStsPiStsAisCurStatus()
            allFields["LineStsPiStsLopCurStatus"] = _AF6CNC0022_RD_OCN._upstschstaram._LineStsPiStsLopCurStatus()
            return allFields

    class _adjcntperstsram(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx PP STS/VC Pointer interpreter pointer adjustment per channel counter"
    
        def description(self):
            return "Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in STS/VC pointer interpreter. These counters are in saturation mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x22080 + 65536*GroupID + 512*LineId + 64*AdjMode + StsId"
            
        def startAddress(self):
            return 0x00022080
            
        def endAddress(self):
            return 0x00022eaf

        class _LineRxPpStsPiPtAdjCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LineRxPpStsPiPtAdjCnt"
            
            def description(self):
                return "The pointer Increment counter or decrease counter. Bit [6] of address is used to indicate that the counter is pointer increment or decrement counter. The counter will stop at maximum value (0x3FFFF)."
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["LineRxPpStsPiPtAdjCnt"] = _AF6CNC0022_RD_OCN._adjcntperstsram._LineRxPpStsPiPtAdjCnt()
            return allFields

    class _stspgstkram(AtRegister.AtRegister):
        def name(self):
            return "OCN TxPg STS per Alarm Interrupt Status"
    
        def description(self):
            return "This is the per Alarm interrupt status of STS pointer generator . Each register is used to store 3 sticky bits for 3 alarms"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x23140 + 65536*GroupID + 512*LineId + StsId"
            
        def startAddress(self):
            return 0x00023140
            
        def endAddress(self):
            return 0x00023f6f

        class _LineStsPgAisIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "LineStsPgAisIntr"
            
            def description(self):
                return "Set to 1 while an AIS status event (at h2pos) is detected at Tx STS Pointer Generator."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _LineStsPgNdfIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "LineStsPgNdfIntr"
            
            def description(self):
                return "Set to 1 while an NDF status event (at h2pos) is detected at Tx STS Pointer Generator."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["LineStsPgAisIntr"] = _AF6CNC0022_RD_OCN._stspgstkram._LineStsPgAisIntr()
            allFields["LineStsPgNdfIntr"] = _AF6CNC0022_RD_OCN._stspgstkram._LineStsPgNdfIntr()
            return allFields

    class _adjcntpgperstsram(AtRegister.AtRegister):
        def name(self):
            return "OCN TxPg STS pointer adjustment per channel counter"
    
        def description(self):
            return "Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in STS pointer generator. These counters are in saturation mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x23080 + 65536*GroupID + 512*LineId + 64*AdjMode + StsId"
            
        def startAddress(self):
            return 0x00023080
            
        def endAddress(self):
            return 0x00023eaf

        class _LineStsPgPtAdjCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LineStsPgPtAdjCnt"
            
            def description(self):
                return "The pointer Increment counter or decrease counter. Bit [11] of address is used to indicate that the counter is pointer increment or decrement counter. The counter will stop at maximum value (0x3FFFF)."
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["LineStsPgPtAdjCnt"] = _AF6CNC0022_RD_OCN._adjcntpgperstsram._LineStsPgPtAdjCnt()
            return allFields

    class _linerxstsconcdetreg(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx STS/VC Automatically Concatenation Detection"
    
        def description(self):
            return "This is the Automatically Concatenation Detection Status at Rx Pointer Interpreter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x22040 + 65536*GroupID + 512*LineId + StsId"
            
        def startAddress(self):
            return 0x00022040
            
        def endAddress(self):
            return 0x00022e6f

        class _LineSpiConDetVc416C(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "LineSpiConDetVc416C"
            
            def description(self):
                return "This is the indicator that this STS is member of 1 VC4_16C, which is automatically detected."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _LineSpiConDetVc44C(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "LineSpiConDetVc44C"
            
            def description(self):
                return "This is the indicator that this STS is member of 1 VC4_4C, which is automatically detected."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _LineSpiConDetVc4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "LineSpiConDetVc4"
            
            def description(self):
                return "This is the indicator that this STS is member of 1 VC4, which is automatically detected."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _LineSpiConDetSlvInd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "LineSpiConDetSlvInd"
            
            def description(self):
                return "This is the indicator that this STS is slaver or master which is automatically detected. 1: Slaver."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LineSpiConDetMstId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LineSpiConDetMstId"
            
            def description(self):
                return "This is the ID of the master STS-1 in the concatenation that contains this STS-1 which is automatically detected."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["LineSpiConDetVc416C"] = _AF6CNC0022_RD_OCN._linerxstsconcdetreg._LineSpiConDetVc416C()
            allFields["LineSpiConDetVc44C"] = _AF6CNC0022_RD_OCN._linerxstsconcdetreg._LineSpiConDetVc44C()
            allFields["LineSpiConDetVc4"] = _AF6CNC0022_RD_OCN._linerxstsconcdetreg._LineSpiConDetVc4()
            allFields["LineSpiConDetSlvInd"] = _AF6CNC0022_RD_OCN._linerxstsconcdetreg._LineSpiConDetSlvInd()
            allFields["LineSpiConDetMstId"] = _AF6CNC0022_RD_OCN._linerxstsconcdetreg._LineSpiConDetMstId()
            return allFields

    class _linetxstsconcdetreg(AtRegister.AtRegister):
        def name(self):
            return "OCN Tx STS/VC Automatically Concatenation Detection"
    
        def description(self):
            return "This is the Automatically Concatenation Detection Status at Tx Pointer Generator based on Concatenation indicator from SXC."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x23040 + 65536*GroupID + 512*LineId + StsId"
            
        def startAddress(self):
            return 0x00023040
            
        def endAddress(self):
            return 0x00023e6f

        class _LineSpgConDetVc416C(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "LineSpgConDetVc416C"
            
            def description(self):
                return "This is the indicator that this STS is member of 1 VC4_16C, which is automatically detected."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _LineSpgConDetVc44C(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "LineSpgConDetVc44C"
            
            def description(self):
                return "This is the indicator that this STS is member of 1 VC4_4C, which is automatically detected."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _LineSpgConDetVc4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "LineSpgConDetVc4"
            
            def description(self):
                return "This is the indicator that this STS is member of 1 VC4, which is automatically detected."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _LineSpgConDetSlvInd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "LineSpgConDetSlvInd"
            
            def description(self):
                return "This is the indicator that this STS is slaver or master which is automatically detected. 1: Slaver."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LineSpgConDetMstId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LineSpgConDetMstId"
            
            def description(self):
                return "This is the ID of the master STS-1 in the concatenation that contains this STS-1 which is automatically detected."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["LineSpgConDetVc416C"] = _AF6CNC0022_RD_OCN._linetxstsconcdetreg._LineSpgConDetVc416C()
            allFields["LineSpgConDetVc44C"] = _AF6CNC0022_RD_OCN._linetxstsconcdetreg._LineSpgConDetVc44C()
            allFields["LineSpgConDetVc4"] = _AF6CNC0022_RD_OCN._linetxstsconcdetreg._LineSpgConDetVc4()
            allFields["LineSpgConDetSlvInd"] = _AF6CNC0022_RD_OCN._linetxstsconcdetreg._LineSpgConDetSlvInd()
            allFields["LineSpgConDetMstId"] = _AF6CNC0022_RD_OCN._linetxstsconcdetreg._LineSpgConDetMstId()
            return allFields

    class _tfmregctl(AtRegister.AtRegister):
        def name(self):
            return "OCN Tx Framer Per Line Control"
    
        def description(self):
            return "Each register is used to configure operation modes for Tx framer engine and APS pattern inserted into APS bytes (K1 and K2 bytes) of the SONET/SDH line being relative with the address of the address of the register."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x21000 + 65536*GroupID + 256*LineId"
            
        def startAddress(self):
            return 0x00021000
            
        def endAddress(self):
            return 0x00021700

        class _OCNTxS1Pat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "OCNTxS1Pat"
            
            def description(self):
                return "Pattern to insert into S1 byte."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNTxApsPat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 8
        
            def name(self):
                return "OCNTxApsPat"
            
            def description(self):
                return "Pattern to insert into APS bytes (K1, K2)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNTxS1LDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "OCNTxS1LDis"
            
            def description(self):
                return "S1 insertion disable 1: Disable 0: Enable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNTxApsDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "OCNTxApsDis"
            
            def description(self):
                return "Disable to process APS 1: Disable 0: Enable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNTxReiLDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "OCNTxReiLDis"
            
            def description(self):
                return "Auto REI_L insertion disable 1: Disable inserting REI_L automatically. 0: Enable automatically inserting REI_L."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNTxRdiLDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "OCNTxRdiLDis"
            
            def description(self):
                return "Auto RDI_L insertion disable 1: Disable inserting RDI_L automatically. 0: Enable automatically inserting RDI_L."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNTxAutoB2Dis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "OCNTxAutoB2Dis"
            
            def description(self):
                return "Auto B2 disable 1: Disable inserting calculated B2 values into B2 positions automatically. 0: Enable automatically inserting calculated B2 values into B2 positions."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNTxRdiLThresSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "OCNTxRdiLThresSel"
            
            def description(self):
                return "Select the number of frames being inserted RDI-L defects when receive direction requests generating RDI-L at transmit direction. 1: Threshold2 is selected. 0: Threshold1 is selected."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNTxRdiLFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "OCNTxRdiLFrc"
            
            def description(self):
                return "RDI-L force. 1: Force RDI-L defect into transmit data. 0: Not force RDI-L"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNTxAisLFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "OCNTxAisLFrc"
            
            def description(self):
                return "AIS-L force. 1: Force AIS-L defect into transmit data. 0: Not force AIS-L"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["OCNTxS1Pat"] = _AF6CNC0022_RD_OCN._tfmregctl._OCNTxS1Pat()
            allFields["OCNTxApsPat"] = _AF6CNC0022_RD_OCN._tfmregctl._OCNTxApsPat()
            allFields["OCNTxS1LDis"] = _AF6CNC0022_RD_OCN._tfmregctl._OCNTxS1LDis()
            allFields["OCNTxApsDis"] = _AF6CNC0022_RD_OCN._tfmregctl._OCNTxApsDis()
            allFields["OCNTxReiLDis"] = _AF6CNC0022_RD_OCN._tfmregctl._OCNTxReiLDis()
            allFields["OCNTxRdiLDis"] = _AF6CNC0022_RD_OCN._tfmregctl._OCNTxRdiLDis()
            allFields["OCNTxAutoB2Dis"] = _AF6CNC0022_RD_OCN._tfmregctl._OCNTxAutoB2Dis()
            allFields["OCNTxRdiLThresSel"] = _AF6CNC0022_RD_OCN._tfmregctl._OCNTxRdiLThresSel()
            allFields["OCNTxRdiLFrc"] = _AF6CNC0022_RD_OCN._tfmregctl._OCNTxRdiLFrc()
            allFields["OCNTxAisLFrc"] = _AF6CNC0022_RD_OCN._tfmregctl._OCNTxAisLFrc()
            return allFields

    class _tfmj0insctl(AtRegister.AtRegister):
        def name(self):
            return "OCN Tx J0 Insertion Buffer"
    
        def description(self):
            return "Each register is used to store one J0 double word of in 64-byte message of the SONET/SDH line being relative with the address of the address of the register inserted into J0 positions."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x21010 + 65536*GroupID + 256*LineId + DwId"
            
        def startAddress(self):
            return 0x00021010
            
        def endAddress(self):
            return 0x0002171f

        class _OCNTxJ0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "OCNTxJ0"
            
            def description(self):
                return "J0 pattern for insertion."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["OCNTxJ0"] = _AF6CNC0022_RD_OCN._tfmj0insctl._OCNTxJ0()
            return allFields

    class _tohglbk1stbthr_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Global K1 Stable Monitoring Threshold Control"
    
        def description(self):
            return "Configure thresholds for monitoring change of K1 state at TOH monitoring. There are two thresholds for each kind of threshold."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x27000 + 65536*GroupID"
            
        def startAddress(self):
            return 0x00027000
            
        def endAddress(self):
            return 0xffffffff

        class _TohK1StbThr2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TohK1StbThr2"
            
            def description(self):
                return "The second threshold for detecting stable K1 status."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TohK1StbThr1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TohK1StbThr1"
            
            def description(self):
                return "The first threshold for detecting stable K1 status."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TohK1StbThr2"] = _AF6CNC0022_RD_OCN._tohglbk1stbthr_reg._TohK1StbThr2()
            allFields["TohK1StbThr1"] = _AF6CNC0022_RD_OCN._tohglbk1stbthr_reg._TohK1StbThr1()
            return allFields

    class _tohglbk2stbthr_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Global K2 Stable Monitoring Threshold Control"
    
        def description(self):
            return "Configure thresholds for monitoring change of K2 state at TOH monitoring. There are two thresholds for each kind of threshold."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x27001 + 65536*GroupID"
            
        def startAddress(self):
            return 0x00027001
            
        def endAddress(self):
            return 0xffffffff

        class _TohK2StbThr2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TohK2StbThr2"
            
            def description(self):
                return "The second threshold for detecting stable K2 status."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TohK2StbThr1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TohK2StbThr1"
            
            def description(self):
                return "The first threshold for detecting stable K2 status."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TohK2StbThr2"] = _AF6CNC0022_RD_OCN._tohglbk2stbthr_reg._TohK2StbThr2()
            allFields["TohK2StbThr1"] = _AF6CNC0022_RD_OCN._tohglbk2stbthr_reg._TohK2StbThr1()
            return allFields

    class _tohglbs1stbthr_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Global S1 Stable Monitoring Threshold Control"
    
        def description(self):
            return "Configure thresholds for monitoring change of S1 state at TOH monitoring. There are two thresholds for each kind of threshold."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x27002 + 65536*GroupID"
            
        def startAddress(self):
            return 0x00027002
            
        def endAddress(self):
            return 0xffffffff

        class _TohS1StbThr2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TohS1StbThr2"
            
            def description(self):
                return "The second threshold for detecting stable S1 status."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TohS1StbThr1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TohS1StbThr1"
            
            def description(self):
                return "The first threshold for detecting stable S1 status."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TohS1StbThr2"] = _AF6CNC0022_RD_OCN._tohglbs1stbthr_reg._TohS1StbThr2()
            allFields["TohS1StbThr1"] = _AF6CNC0022_RD_OCN._tohglbs1stbthr_reg._TohS1StbThr1()
            return allFields

    class _tohglbrdidetthr_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Global RDI_L Detecting Threshold Control"
    
        def description(self):
            return "Configure thresholds for detecting RDI_L at TOH monitoring. There are two thresholds for each kind of threshold."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x27003 + 65536*GroupID"
            
        def startAddress(self):
            return 0x00027003
            
        def endAddress(self):
            return 0xffffffff

        class _TohRdiDetThr2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TohRdiDetThr2"
            
            def description(self):
                return "The second threshold for detecting RDI_L."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TohRdiDetThr1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TohRdiDetThr1"
            
            def description(self):
                return "The first threshold for detecting RDI_L."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TohRdiDetThr2"] = _AF6CNC0022_RD_OCN._tohglbrdidetthr_reg._TohRdiDetThr2()
            allFields["TohRdiDetThr1"] = _AF6CNC0022_RD_OCN._tohglbrdidetthr_reg._TohRdiDetThr1()
            return allFields

    class _tohglbaisdetthr_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Global AIS_L Detecting Threshold Control"
    
        def description(self):
            return "Configure thresholds for detecting AIS_L at TOH monitoring. There are two thresholds for each kind of threshold."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x27004 + 65536*GroupID"
            
        def startAddress(self):
            return 0x00027004
            
        def endAddress(self):
            return 0xffffffff

        class _TohAisDetThr2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TohAisDetThr2"
            
            def description(self):
                return "The second threshold for detecting AIS_L."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TohAisDetThr1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TohAisDetThr1"
            
            def description(self):
                return "The first threshold for detecting AIS_L."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TohAisDetThr2"] = _AF6CNC0022_RD_OCN._tohglbaisdetthr_reg._TohAisDetThr2()
            allFields["TohAisDetThr1"] = _AF6CNC0022_RD_OCN._tohglbaisdetthr_reg._TohAisDetThr1()
            return allFields

    class _tohglbk1smpthr_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Global K1 Sampling Threshold Control"
    
        def description(self):
            return "Configure thresholds for sampling K1 bytes to detect APS defect at TOH monitoring. There are two thresholds."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x27005 + 65536*GroupID"
            
        def startAddress(self):
            return 0x00027005
            
        def endAddress(self):
            return 0xffffffff

        class _TohK1SmpThr2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TohK1SmpThr2"
            
            def description(self):
                return "The second threshold for sampling K1 to detect APS defect."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TohK1SmpThr1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TohK1SmpThr1"
            
            def description(self):
                return "The first threshold for sampling K1 to detect APS defect."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TohK1SmpThr2"] = _AF6CNC0022_RD_OCN._tohglbk1smpthr_reg._TohK1SmpThr2()
            allFields["TohK1SmpThr1"] = _AF6CNC0022_RD_OCN._tohglbk1smpthr_reg._TohK1SmpThr1()
            return allFields

    class _tohglberrcntmod_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Global Error Counter Control"
    
        def description(self):
            return "Configure mode for counters in TOH monitoring."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x27006 + 65536*GroupID"
            
        def startAddress(self):
            return 0x00027006
            
        def endAddress(self):
            return 0xffffffff

        class _TohReiErrCntMod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "TohReiErrCntMod"
            
            def description(self):
                return "REI counter Mode. 1: Saturation mode 0: Roll over mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TohB2ErrCntMod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "TohB2ErrCntMod"
            
            def description(self):
                return "B2 counter Mode. 1: Saturation mode 0: Roll over mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TohB1ErrCntMod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TohB1ErrCntMod"
            
            def description(self):
                return "B1 counter Mode. 1: Saturation mode 0: Roll over mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TohReiErrCntMod"] = _AF6CNC0022_RD_OCN._tohglberrcntmod_reg._TohReiErrCntMod()
            allFields["TohB2ErrCntMod"] = _AF6CNC0022_RD_OCN._tohglberrcntmod_reg._TohB2ErrCntMod()
            allFields["TohB1ErrCntMod"] = _AF6CNC0022_RD_OCN._tohglberrcntmod_reg._TohB1ErrCntMod()
            return allFields

    class _tohglbaffen_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Montoring Affect Control"
    
        def description(self):
            return "Configure affective mode for TOH monitoring."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x27007 + 65536*GroupID"
            
        def startAddress(self):
            return 0x00027007
            
        def endAddress(self):
            return 0xffffffff

        class _TohAisAffStbMon(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "TohAisAffStbMon"
            
            def description(self):
                return "AIS affects to Stable monitoring status of the line at which LOF or LOS is detected 1: Disable 0: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TohAisAffRdilMon(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "TohAisAffRdilMon"
            
            def description(self):
                return "AIS affects to RDI-L monitoring status of the line at which LOF or LOS is detected. 1: Disable 0: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TohAisAffAislMon(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "TohAisAffAislMon"
            
            def description(self):
                return "AIS affects to AIS-L monitoring  status of the line at which LOF or LOS is detected. 1: Disable 0: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TohAisAffErrCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TohAisAffErrCnt"
            
            def description(self):
                return "AIS affects to error counters (B1,B2,REI) of the line at which LOF or LOS is detected. 1: Disable 0: Enable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TohAisAffStbMon"] = _AF6CNC0022_RD_OCN._tohglbaffen_reg._TohAisAffStbMon()
            allFields["TohAisAffRdilMon"] = _AF6CNC0022_RD_OCN._tohglbaffen_reg._TohAisAffRdilMon()
            allFields["TohAisAffAislMon"] = _AF6CNC0022_RD_OCN._tohglbaffen_reg._TohAisAffAislMon()
            allFields["TohAisAffErrCnt"] = _AF6CNC0022_RD_OCN._tohglbaffen_reg._TohAisAffErrCnt()
            return allFields

    class _tohramctl(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Monitoring Per Line Control"
    
        def description(self):
            return "Each register is used to configure for TOH monitoring engine of the related line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x27010 + 65536*GroupID + LineId"
            
        def startAddress(self):
            return 0x00027010
            
        def endAddress(self):
            return 0x00027017

        class _B1ErrCntBlkMod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "B1ErrCntBlkMod"
            
            def description(self):
                return "B1 counter Block Mode for fedding PM. 1: Block count mode 0: Bit count mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _B2ErrCntBlkMod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "B2ErrCntBlkMod"
            
            def description(self):
                return "B2 counter Block Mode for fedding PM. 1: Block count mode 0: Bit count mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ReiErrCntBlkMod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "ReiErrCntBlkMod"
            
            def description(self):
                return "REI counter Block Mode for fedding PM. 1: Block count mode 0: Bit count mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StbRdiisLThresSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "StbRdiisLThresSel"
            
            def description(self):
                return "Select the thresholds for detecting RDI_L 1: Threshold 2 is selected 0: Threshold 1 is selected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StbAisLThresSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "StbAisLThresSel"
            
            def description(self):
                return "Select the thresholds for detecting AIS_L 1: Threshold 2 is selected 0: Threshold 1 is selected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _K2StbMd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "K2StbMd"
            
            def description(self):
                return "Select K2[7:3] or K2[7:0] to detect validated K2 value 1: K2[7:0] value is selected 0: K2[7:3] value is selected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StbK1K2ThresSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "StbK1K2ThresSel"
            
            def description(self):
                return "Select the thresholds for detecting stable or non-stable K1/K2 status 1: Threshold 2 is selected 0: Threshold 1 is selected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StbS1ThresSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "StbS1ThresSel"
            
            def description(self):
                return "Select the thresholds for detecting stable or non-stable S1 status 1: Threshold 2 is selected 0: Threshold 1 is selected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SmpK1ThresSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SmpK1ThresSel"
            
            def description(self):
                return "Select the sample threshold for detecting APS defect. 1: Threshold 2 is selected 0: Threshold 1 is selected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["B1ErrCntBlkMod"] = _AF6CNC0022_RD_OCN._tohramctl._B1ErrCntBlkMod()
            allFields["B2ErrCntBlkMod"] = _AF6CNC0022_RD_OCN._tohramctl._B2ErrCntBlkMod()
            allFields["ReiErrCntBlkMod"] = _AF6CNC0022_RD_OCN._tohramctl._ReiErrCntBlkMod()
            allFields["StbRdiisLThresSel"] = _AF6CNC0022_RD_OCN._tohramctl._StbRdiisLThresSel()
            allFields["StbAisLThresSel"] = _AF6CNC0022_RD_OCN._tohramctl._StbAisLThresSel()
            allFields["K2StbMd"] = _AF6CNC0022_RD_OCN._tohramctl._K2StbMd()
            allFields["StbK1K2ThresSel"] = _AF6CNC0022_RD_OCN._tohramctl._StbK1K2ThresSel()
            allFields["StbS1ThresSel"] = _AF6CNC0022_RD_OCN._tohramctl._StbS1ThresSel()
            allFields["SmpK1ThresSel"] = _AF6CNC0022_RD_OCN._tohramctl._SmpK1ThresSel()
            return allFields

    class _tohb1errrocnt(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Monitoring B1 Error Read Only Counter"
    
        def description(self):
            return "Each register is used to store B1 error read only counter of the related line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x27100 + 65536*GroupID + LineId"
            
        def startAddress(self):
            return 0x00027100
            
        def endAddress(self):
            return 0x00027107

        class _B1ErrRoCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 0
        
            def name(self):
                return "B1ErrRoCnt"
            
            def description(self):
                return "B1 Error Read Only Counter."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["B1ErrRoCnt"] = _AF6CNC0022_RD_OCN._tohb1errrocnt._B1ErrRoCnt()
            return allFields

    class _tohb1errr2ccnt(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Monitoring B1 Error Read to Clear Counter"
    
        def description(self):
            return "Each register is used to store B1 error read to clear counter of the related line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x27140 + 65536*GroupID + LineId"
            
        def startAddress(self):
            return 0x00027140
            
        def endAddress(self):
            return 0x00027147

        class _B1ErrR2cCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 0
        
            def name(self):
                return "B1ErrR2cCnt"
            
            def description(self):
                return "B1 Error Read To Clear Counter."
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["B1ErrR2cCnt"] = _AF6CNC0022_RD_OCN._tohb1errr2ccnt._B1ErrR2cCnt()
            return allFields

    class _tohb1blkerrrocnt(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Monitoring B1 Block Error Read Only Counter"
    
        def description(self):
            return "Each register is used to store B1 Block error read only counter of the related line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x27180 + 65536*GroupID + LineId"
            
        def startAddress(self):
            return 0x00027180
            
        def endAddress(self):
            return 0x00027187

        class _B1BlkErrRoCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "B1BlkErrRoCnt"
            
            def description(self):
                return "B1 Block Error Read Only Counter."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["B1BlkErrRoCnt"] = _AF6CNC0022_RD_OCN._tohb1blkerrrocnt._B1BlkErrRoCnt()
            return allFields

    class _tohb1blkerrr2ccnt(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Monitoring B1 Block Error Read to Clear Counter"
    
        def description(self):
            return "Each register is used to store B1 Block error read to clear counter of the related line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x271c0 + 65536*GroupID + LineId"
            
        def startAddress(self):
            return 0x000271c0
            
        def endAddress(self):
            return 0x000271c7

        class _B1BlkErrR2cCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "B1BlkErrR2cCnt"
            
            def description(self):
                return "B1 Block Error Read To Clear Counter."
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["B1BlkErrR2cCnt"] = _AF6CNC0022_RD_OCN._tohb1blkerrr2ccnt._B1BlkErrR2cCnt()
            return allFields

    class _tohb2errrocnt(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Monitoring B2 Error Read Only Counter"
    
        def description(self):
            return "Each register is used to store B2 error read only counter of the related line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x27108 + 65536*GroupID + LineId"
            
        def startAddress(self):
            return 0x00027108
            
        def endAddress(self):
            return 0x0002710f

        class _B2ErrRoCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 0
        
            def name(self):
                return "B2ErrRoCnt"
            
            def description(self):
                return "B2 Error Read Only Counter."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["B2ErrRoCnt"] = _AF6CNC0022_RD_OCN._tohb2errrocnt._B2ErrRoCnt()
            return allFields

    class _tohb2errr2ccnt(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Monitoring B2 Error Read to Clear Counter"
    
        def description(self):
            return "Each register is used to store B2 error read to clear counter of the related line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x27148 + 65536*GroupID + LineId"
            
        def startAddress(self):
            return 0x00027148
            
        def endAddress(self):
            return 0x0002714f

        class _B2ErrR2cCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 0
        
            def name(self):
                return "B2ErrR2cCnt"
            
            def description(self):
                return "B2 Error Read To Clear Counter."
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["B2ErrR2cCnt"] = _AF6CNC0022_RD_OCN._tohb2errr2ccnt._B2ErrR2cCnt()
            return allFields

    class _tohb2blkerrrocnt(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Monitoring B2 Block Error Read Only Counter"
    
        def description(self):
            return "Each register is used to store B2 Block error read only counter of the related line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x27188 + 65536*GroupID + LineId"
            
        def startAddress(self):
            return 0x00027188
            
        def endAddress(self):
            return 0x0002718f

        class _B2BlkErrRoCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "B2BlkErrRoCnt"
            
            def description(self):
                return "B2 Block Error Read Only Counter."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["B2BlkErrRoCnt"] = _AF6CNC0022_RD_OCN._tohb2blkerrrocnt._B2BlkErrRoCnt()
            return allFields

    class _tohb2blkerrr2ccnt(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Monitoring B2 Block Error Read to Clear Counter"
    
        def description(self):
            return "Each register is used to store B2 Block error read to clear counter of the related line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x271c8 + 65536*GroupID + LineId"
            
        def startAddress(self):
            return 0x000271c8
            
        def endAddress(self):
            return 0x000271cf

        class _B2BlkErrR2cCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "B2BlkErrR2cCnt"
            
            def description(self):
                return "B2 Block Error Read To Clear Counter."
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["B2BlkErrR2cCnt"] = _AF6CNC0022_RD_OCN._tohb2blkerrr2ccnt._B2BlkErrR2cCnt()
            return allFields

    class _tohreierrrocnt(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Monitoring REI Error Read Only Counter"
    
        def description(self):
            return "Each register is used to store REI error read only counter of the related line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x27128 + 65536*GroupID + LineId"
            
        def startAddress(self):
            return 0x00027128
            
        def endAddress(self):
            return 0x0002712f

        class _ReiErrRoCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReiErrRoCnt"
            
            def description(self):
                return "REI Error Read Only Counter."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReiErrRoCnt"] = _AF6CNC0022_RD_OCN._tohreierrrocnt._ReiErrRoCnt()
            return allFields

    class _tohreierrr2ccnt(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Monitoring REI Error Read to Clear Counter"
    
        def description(self):
            return "Each register is used to store REI error read to clear counter of the related line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x27168 + 65536*GroupID + LineId"
            
        def startAddress(self):
            return 0x00027168
            
        def endAddress(self):
            return 0x0002716f

        class _ReiErrR2cCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReiErrR2cCnt"
            
            def description(self):
                return "REI Error Read To Clear Counter."
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReiErrR2cCnt"] = _AF6CNC0022_RD_OCN._tohreierrr2ccnt._ReiErrR2cCnt()
            return allFields

    class _tohreiblkerrrocnt(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Monitoring REI Block Error Read Only Counter"
    
        def description(self):
            return "Each register is used to store REI Block error read only counter of the related line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x271a8 + 65536*GroupID + LineId"
            
        def startAddress(self):
            return 0x000271a8
            
        def endAddress(self):
            return 0x000271af

        class _ReiBlkErrRoCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReiBlkErrRoCnt"
            
            def description(self):
                return "REI Block Error Read Only Counter."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReiBlkErrRoCnt"] = _AF6CNC0022_RD_OCN._tohreiblkerrrocnt._ReiBlkErrRoCnt()
            return allFields

    class _tohreiblkerrr2ccnt(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Monitoring REI Block Error Read to Clear Counter"
    
        def description(self):
            return "Each register is used to store REI Block error read to clear counter of the related line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x271e8 + 65536*GroupID + LineId"
            
        def startAddress(self):
            return 0x000271e8
            
        def endAddress(self):
            return 0x000270ef

        class _ReiBlkErrR2cCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReiBlkErrR2cCnt"
            
            def description(self):
                return "REI Block Error Read To Clear Counter."
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReiBlkErrR2cCnt"] = _AF6CNC0022_RD_OCN._tohreiblkerrr2ccnt._ReiBlkErrR2cCnt()
            return allFields

    class _tohk1monsta(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH K1 Monitoring Status"
    
        def description(self):
            return "Each register is used to store K1 Monitoring Status of the related line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x27110 + 65536*GroupID + LineId"
            
        def startAddress(self):
            return 0x00027110
            
        def endAddress(self):
            return 0x00027117

        class _CurApsDef(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "CurApsDef"
            
            def description(self):
                return "current APS Defect. 1: APS defect is detected from APS bytes. 0: APS defect is not detected from APS bytes."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _K1SmpCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 19
        
            def name(self):
                return "K1SmpCnt"
            
            def description(self):
                return "Sampling counter."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SameK1Cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 16
        
            def name(self):
                return "SameK1Cnt"
            
            def description(self):
                return "The number of same contiguous K1 bytes. It is held at StbK1Thr value when the number of same contiguous K1 bytes is equal to or more than the StbK1Thr value.In this case, K1 bytes are stable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _K1StbVal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "K1StbVal"
            
            def description(self):
                return "Stable K1 value. It is updated when detecting a new stable value."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _K1CurVal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "K1CurVal"
            
            def description(self):
                return "Current K1 byte."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CurApsDef"] = _AF6CNC0022_RD_OCN._tohk1monsta._CurApsDef()
            allFields["K1SmpCnt"] = _AF6CNC0022_RD_OCN._tohk1monsta._K1SmpCnt()
            allFields["SameK1Cnt"] = _AF6CNC0022_RD_OCN._tohk1monsta._SameK1Cnt()
            allFields["K1StbVal"] = _AF6CNC0022_RD_OCN._tohk1monsta._K1StbVal()
            allFields["K1CurVal"] = _AF6CNC0022_RD_OCN._tohk1monsta._K1CurVal()
            return allFields

    class _tohk2monsta(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH K2 Monitoring Status"
    
        def description(self):
            return "Each register is used to store K2 Monitoring Status of the related line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x27118 + 65536*GroupID + LineId"
            
        def startAddress(self):
            return 0x00027118
            
        def endAddress(self):
            return 0x0002711f

        class _Internal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 21
        
            def name(self):
                return "Internal"
            
            def description(self):
                return "Internal."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CurAisL(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "CurAisL"
            
            def description(self):
                return "current AIS-L Defect. 1: AIS-L defect is detected from K2 bytes. 0: AIS-L defect is not detected from K2 bytes."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CurRdiL(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "CurRdiL"
            
            def description(self):
                return "current RDI-L Defect. 1: RDI-L defect is detected from K2 bytes. 0: RDI-L defect is not detected from K2 bytes."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SameK2Cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 16
        
            def name(self):
                return "SameK2Cnt"
            
            def description(self):
                return "The number of same contiguous K2 bytes. It is held at StbK2Thr value when the number of same contiguous K2 bytes is equal to or more than the StbK2Thr value.In this case, K2 bytes are stable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _K2StbVal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "K2StbVal"
            
            def description(self):
                return "Stable K2 value. It is updated when detecting a new stable value."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _K2CurVal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "K2CurVal"
            
            def description(self):
                return "Current K2 byte."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Internal"] = _AF6CNC0022_RD_OCN._tohk2monsta._Internal()
            allFields["CurAisL"] = _AF6CNC0022_RD_OCN._tohk2monsta._CurAisL()
            allFields["CurRdiL"] = _AF6CNC0022_RD_OCN._tohk2monsta._CurRdiL()
            allFields["SameK2Cnt"] = _AF6CNC0022_RD_OCN._tohk2monsta._SameK2Cnt()
            allFields["K2StbVal"] = _AF6CNC0022_RD_OCN._tohk2monsta._K2StbVal()
            allFields["K2CurVal"] = _AF6CNC0022_RD_OCN._tohk2monsta._K2CurVal()
            return allFields

    class _tohs1monsta(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH S1 Monitoring Status"
    
        def description(self):
            return "Each register is used to store S1 Monitoring Status of the related line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x27120 + 65536*GroupID + LineId"
            
        def startAddress(self):
            return 0x00027120
            
        def endAddress(self):
            return 0x00027127

        class _SameS1Cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "SameS1Cnt"
            
            def description(self):
                return "The number of same contiguous S1 bytes. It is held at StbS1Thr value when the number of same contiguous S1 bytes is equal to or more than the StbS1Thr value.In this case, S1 bytes are stable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _S1StbVal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "S1StbVal"
            
            def description(self):
                return "Stable S1 value. It is updated when detecting a new stable value."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _S1CurVal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "S1CurVal"
            
            def description(self):
                return "Current S1 byte."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SameS1Cnt"] = _AF6CNC0022_RD_OCN._tohs1monsta._SameS1Cnt()
            allFields["S1StbVal"] = _AF6CNC0022_RD_OCN._tohs1monsta._S1StbVal()
            allFields["S1CurVal"] = _AF6CNC0022_RD_OCN._tohs1monsta._S1CurVal()
            return allFields

    class _tohintperalrenbctl(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx Line per Alarm Interrupt Enable Control"
    
        def description(self):
            return "This is the per Alarm interrupt enable of Rx framer and TOH monitoring. Each register is used to store 9 bits to enable interrupts when the related alarms in related line happen."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x27080 + 65536*GroupID + LineId"
            
        def startAddress(self):
            return 0x00027080
            
        def endAddress(self):
            return 0x00027087

        class _TcaLStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "TcaLStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable TCA-L state change event in the related line to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SdLStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "SdLStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable SD-L state change event in the related line to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SfLStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "SfLStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable SF-L state change event in the related line to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TimLStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "TimLStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable TIM-L state change event in the related line to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _S1StbStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "S1StbStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable S1 Stable state change event in the related line to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _K1StbStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "K1StbStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable K1 Stable state change event in the related line to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ApsLStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "ApsLStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable APS-L Defect Stable state change event in the related line to generate an interrupt.."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _K2StbStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "K2StbStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable K2 Stable state change event in the related line to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RdiLStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RdiLStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable RDI-L Defect Stable state change event in the related line to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _AisLStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "AisLStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable AIS-L Defect Stable state change event in the related line to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OofStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "OofStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable OOF Defect Stable state change event in the related line to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LofStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "LofStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable LOF Defect Stable state change event in the related line to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LosStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LosStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable LOS Defect Stable state change event in the related line to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TcaLStateChgIntrEn"] = _AF6CNC0022_RD_OCN._tohintperalrenbctl._TcaLStateChgIntrEn()
            allFields["SdLStateChgIntrEn"] = _AF6CNC0022_RD_OCN._tohintperalrenbctl._SdLStateChgIntrEn()
            allFields["SfLStateChgIntrEn"] = _AF6CNC0022_RD_OCN._tohintperalrenbctl._SfLStateChgIntrEn()
            allFields["TimLStateChgIntrEn"] = _AF6CNC0022_RD_OCN._tohintperalrenbctl._TimLStateChgIntrEn()
            allFields["S1StbStateChgIntrEn"] = _AF6CNC0022_RD_OCN._tohintperalrenbctl._S1StbStateChgIntrEn()
            allFields["K1StbStateChgIntrEn"] = _AF6CNC0022_RD_OCN._tohintperalrenbctl._K1StbStateChgIntrEn()
            allFields["ApsLStateChgIntrEn"] = _AF6CNC0022_RD_OCN._tohintperalrenbctl._ApsLStateChgIntrEn()
            allFields["K2StbStateChgIntrEn"] = _AF6CNC0022_RD_OCN._tohintperalrenbctl._K2StbStateChgIntrEn()
            allFields["RdiLStateChgIntrEn"] = _AF6CNC0022_RD_OCN._tohintperalrenbctl._RdiLStateChgIntrEn()
            allFields["AisLStateChgIntrEn"] = _AF6CNC0022_RD_OCN._tohintperalrenbctl._AisLStateChgIntrEn()
            allFields["OofStateChgIntrEn"] = _AF6CNC0022_RD_OCN._tohintperalrenbctl._OofStateChgIntrEn()
            allFields["LofStateChgIntrEn"] = _AF6CNC0022_RD_OCN._tohintperalrenbctl._LofStateChgIntrEn()
            allFields["LosStateChgIntrEn"] = _AF6CNC0022_RD_OCN._tohintperalrenbctl._LosStateChgIntrEn()
            return allFields

    class _tohintsta(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx Line per Alarm Interrupt Status"
    
        def description(self):
            return "This is the per Alarm interrupt status of Rx framer and TOH monitoring. Each register is used to store 9 sticky bits for 9 alarms in the line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x27088 + 65536*GroupID + LineId"
            
        def startAddress(self):
            return 0x00027088
            
        def endAddress(self):
            return 0x0002708f

        class _TcaLStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "TcaLStateChgIntr"
            
            def description(self):
                return "Set 1 while TCA-L state change detected in the related line, and it is generated an interrupt if it is enabled."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _SdLStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "SdLStateChgIntr"
            
            def description(self):
                return "Set 1 while SD-L state change detected in the related line, and it is generated an interrupt if it is enabled."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _SfLStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "SfLStateChgIntr"
            
            def description(self):
                return "Set 1 while SF-L state change detected in the related line, and it is generated an interrupt if it is enabled."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _TimLStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "TimLStateChgIntr"
            
            def description(self):
                return "Set 1 while Tim-L state change detected in the related line, and it is generated an interrupt if it is enabled."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _S1StbStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "S1StbStateChgIntr"
            
            def description(self):
                return "Set 1 one new stable S1 value detected in the related line, and it is generated an interrupt if it is enabled."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _K1StbStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "K1StbStateChgIntr"
            
            def description(self):
                return "Set 1 one new stable K1 value detected in the related line, and it is generated an interrupt if it is enabled."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _ApsLStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "ApsLStateChgIntr"
            
            def description(self):
                return "Set 1 while APS-L Defect state change event happens in the related line, and it is generated an interrupt if it is enabled."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _K2StbStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "K2StbStateChgIntr"
            
            def description(self):
                return "Set 1 one new stable K2 value detected in the related line, and it is generated an interrupt if it is enabled."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _RdiLStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RdiLStateChgIntr"
            
            def description(self):
                return "Set 1 while RDI-L Defect state change event happens in the related line, and it is generated an interrupt if it is enabled."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _AisLStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "AisLStateChgIntr"
            
            def description(self):
                return "Set 1 while AIS-L Defect state change event happens in the related line, and it is generated an interrupt if it is enabled."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _OofStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "OofStateChgIntr"
            
            def description(self):
                return "Set 1 while OOF state change event happens in the related line, and it is generated an interrupt if it is enabled."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _LofStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "LofStateChgIntr"
            
            def description(self):
                return "Set 1 while LOF state change event happens in the related line, and it is generated an interrupt if it is enabled."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _LosStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LosStateChgIntr"
            
            def description(self):
                return "Set 1 while LOS state change event happens in the related line, and it is generated an interrupt if it is enabled."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TcaLStateChgIntr"] = _AF6CNC0022_RD_OCN._tohintsta._TcaLStateChgIntr()
            allFields["SdLStateChgIntr"] = _AF6CNC0022_RD_OCN._tohintsta._SdLStateChgIntr()
            allFields["SfLStateChgIntr"] = _AF6CNC0022_RD_OCN._tohintsta._SfLStateChgIntr()
            allFields["TimLStateChgIntr"] = _AF6CNC0022_RD_OCN._tohintsta._TimLStateChgIntr()
            allFields["S1StbStateChgIntr"] = _AF6CNC0022_RD_OCN._tohintsta._S1StbStateChgIntr()
            allFields["K1StbStateChgIntr"] = _AF6CNC0022_RD_OCN._tohintsta._K1StbStateChgIntr()
            allFields["ApsLStateChgIntr"] = _AF6CNC0022_RD_OCN._tohintsta._ApsLStateChgIntr()
            allFields["K2StbStateChgIntr"] = _AF6CNC0022_RD_OCN._tohintsta._K2StbStateChgIntr()
            allFields["RdiLStateChgIntr"] = _AF6CNC0022_RD_OCN._tohintsta._RdiLStateChgIntr()
            allFields["AisLStateChgIntr"] = _AF6CNC0022_RD_OCN._tohintsta._AisLStateChgIntr()
            allFields["OofStateChgIntr"] = _AF6CNC0022_RD_OCN._tohintsta._OofStateChgIntr()
            allFields["LofStateChgIntr"] = _AF6CNC0022_RD_OCN._tohintsta._LofStateChgIntr()
            allFields["LosStateChgIntr"] = _AF6CNC0022_RD_OCN._tohintsta._LosStateChgIntr()
            return allFields

    class _tohcursta(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx Line per Alarm Current Status"
    
        def description(self):
            return "This is the per Alarm interrupt status of Rx framer and TOH monitoring. Each register is used to store 9 sticky bits for 9 alarms in the line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x27090 + 65536*GroupID + LineId"
            
        def startAddress(self):
            return 0x00027090
            
        def endAddress(self):
            return 0x00027097

        class _TcaLDefCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "TcaLDefCurStatus"
            
            def description(self):
                return "TCA-L Defect  in the related line. 1: TCA-L defect is set 0: TCA-L defect is cleared."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SdLDefCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "SdLDefCurStatus"
            
            def description(self):
                return "SD-L Defect  in the related line. 1: SD-L defect is set 0: SD-L defect is cleared."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SfLDefCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "SfLDefCurStatus"
            
            def description(self):
                return "SF-L Defect  in the related line. 1: SF-L defect is set 0: SF-L defect is cleared."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TimLDefCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "TimLDefCurStatus"
            
            def description(self):
                return "TIM-L Defect  in the related line. 1: TIM-L defect is set 0: TIM-L defect is cleared."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _S1StbStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "S1StbStateChgIntr"
            
            def description(self):
                return "S1 stable status  in the related line."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _K1StbCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "K1StbCurStatus"
            
            def description(self):
                return "K1 stable status  in the related line."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ApsLCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "ApsLCurStatus"
            
            def description(self):
                return "APS-L Defect  in the related line. 1: APS-L defect is set 0: APS-L defect is cleared."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _K2StbCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "K2StbCurStatus"
            
            def description(self):
                return "K2 stable status  in the related line."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RdiLDefCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RdiLDefCurStatus"
            
            def description(self):
                return "RDI-L Defect  in the related line. 1: RDI-L defect is set 0: RDI-L defect is cleared."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _AisLDefCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "AisLDefCurStatus"
            
            def description(self):
                return "AIS-L Defect  in the related line. 1: AIS-L defect is set 0: AIS-L defect is cleared."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OofCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "OofCurStatus"
            
            def description(self):
                return "OOF current status in the related line. 1: OOF state 0: Not OOF state."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LofCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "LofCurStatus"
            
            def description(self):
                return "LOF current status in the related line. 1: LOF state 0: Not LOF state."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LosCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LosCurStatus"
            
            def description(self):
                return "LOS current status in the related line. 1: LOS state 0: Not LOS state."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TcaLDefCurStatus"] = _AF6CNC0022_RD_OCN._tohcursta._TcaLDefCurStatus()
            allFields["SdLDefCurStatus"] = _AF6CNC0022_RD_OCN._tohcursta._SdLDefCurStatus()
            allFields["SfLDefCurStatus"] = _AF6CNC0022_RD_OCN._tohcursta._SfLDefCurStatus()
            allFields["TimLDefCurStatus"] = _AF6CNC0022_RD_OCN._tohcursta._TimLDefCurStatus()
            allFields["S1StbStateChgIntr"] = _AF6CNC0022_RD_OCN._tohcursta._S1StbStateChgIntr()
            allFields["K1StbCurStatus"] = _AF6CNC0022_RD_OCN._tohcursta._K1StbCurStatus()
            allFields["ApsLCurStatus"] = _AF6CNC0022_RD_OCN._tohcursta._ApsLCurStatus()
            allFields["K2StbCurStatus"] = _AF6CNC0022_RD_OCN._tohcursta._K2StbCurStatus()
            allFields["RdiLDefCurStatus"] = _AF6CNC0022_RD_OCN._tohcursta._RdiLDefCurStatus()
            allFields["AisLDefCurStatus"] = _AF6CNC0022_RD_OCN._tohcursta._AisLDefCurStatus()
            allFields["OofCurStatus"] = _AF6CNC0022_RD_OCN._tohcursta._OofCurStatus()
            allFields["LofCurStatus"] = _AF6CNC0022_RD_OCN._tohcursta._LofCurStatus()
            allFields["LosCurStatus"] = _AF6CNC0022_RD_OCN._tohcursta._LosCurStatus()
            return allFields

    class _tohintperlineenctl(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx Line Per Line Interrupt Enable Control"
    
        def description(self):
            return "The register consists of 4 or 8 bits per lines (STM1: 8bits,STM4: 4bits) at Rx side to enable interrupts when alarms of related lines happen. It is noted that this register is higher priority than the OCN Rx Line per Alarm Interrupt Enable Control register."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x2709e + 65536*GroupID"
            
        def startAddress(self):
            return 0x0002709e
            
        def endAddress(self):
            return 0xffffffff

        class _OCNRxLLineIntrEn1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "OCNRxLLineIntrEn1"
            
            def description(self):
                return "Bit #0 to enable for line #0."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["OCNRxLLineIntrEn1"] = _AF6CNC0022_RD_OCN._tohintperlineenctl._OCNRxLLineIntrEn1()
            return allFields

    class _tohintperline(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx Line per Line Interrupt OR Status"
    
        def description(self):
            return "The register consists of 4 or 8 bits per lines (STM1: 8bits,STM4: 4bits) at Rx side. Each bit is used to store Interrupt OR status of the related line. If there are any bits in this register and they are enabled to raise interrupt, the Rx line level interrupt bit is set."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x2709f + 65536*GroupID"
            
        def startAddress(self):
            return 0x0002709f
            
        def endAddress(self):
            return 0xffffffff

        class _OCNRxLLineIntr1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "OCNRxLLineIntr1"
            
            def description(self):
                return "Set to 1 to indicate that there is any interrupt status bit in the OCN Rx Line per Alarm Interrupt Status register of the related STS/VC to be set and they are enabled to raise interrupt. Bit 0 for line #0, respectively."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["OCNRxLLineIntr1"] = _AF6CNC0022_RD_OCN._tohintperline._OCNRxLLineIntr1()
            return allFields

    class _sxcramctl0(AtRegister.AtRegister):
        def name(self):
            return "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC"
    
        def description(self):
            return "Each register is used for each outgoing STS of any line (24 lines: 0-7:TFI5 side, 8-15: Line Side, 16-23: Loho) can be randomly configured to //connect  to any ingoing STS of any ingoing line (24 lines: 0-7:TFI5 side, 8-15: Line Side, 16-23: Loho). These registers are used for config Pape0 and page1 of SXC. # HDL_PATH		: isdhsxc.isxc_rxrd[0].rxsxcramctl0.array"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x44000 + 256*LineId + StsId"
            
        def startAddress(self):
            return 0x00044000
            
        def endAddress(self):
            return 0x0004572f

        class _SxcLineIdPage1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 24
        
            def name(self):
                return "SxcLineIdPage1"
            
            def description(self):
                return "Contains the ingoing LineID Page1 (0-23: 0-7:TFI5 side, 8-15: Line Side, 16-23: Loho). Disconnect (output is all one) if value LineID is other value (recommend is 24)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SxcStsIdPage1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 16
        
            def name(self):
                return "SxcStsIdPage1"
            
            def description(self):
                return "Contains the ingoing STSID Page1 (0-47)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SxcLineIdPage0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 8
        
            def name(self):
                return "SxcLineIdPage0"
            
            def description(self):
                return "Contains the ingoing LineID Page0 (0-23: 0-7:TFI5 side, 8-15: Line Side, 16-23: Loho). Disconnect (output is all one) if value LineID is other value (recommend is 24)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SxcStsIdPage0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SxcStsIdPage0"
            
            def description(self):
                return "Contains the ingoing STSID Page0 (0-47)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SxcLineIdPage1"] = _AF6CNC0022_RD_OCN._sxcramctl0._SxcLineIdPage1()
            allFields["SxcStsIdPage1"] = _AF6CNC0022_RD_OCN._sxcramctl0._SxcStsIdPage1()
            allFields["SxcLineIdPage0"] = _AF6CNC0022_RD_OCN._sxcramctl0._SxcLineIdPage0()
            allFields["SxcStsIdPage0"] = _AF6CNC0022_RD_OCN._sxcramctl0._SxcStsIdPage0()
            return allFields

    class _sxcramctl1(AtRegister.AtRegister):
        def name(self):
            return "OCN SXC Control 2 - Config Page 2 of SXC"
    
        def description(self):
            return "Each register is used for each outgoing STS of any line (24 lines: 0-7:TFI5 side, 8-15: Line Side, 16-23: Loho) can be randomly configured to //connect  to any ingoing STS of any ingoing line (24 lines: 0-7:TFI5 side, 8-15: Line Side, 16-23: Loho). These registers are used for config Pape2 of SXC. # HDL_PATH		: isdhsxc.isxc_rxrd[0].rxsxcramctl1.array"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x44040 + 256*LineId + StsId"
            
        def startAddress(self):
            return 0x00044040
            
        def endAddress(self):
            return 0x0004576f

        class _SxcLineIdPage2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 8
        
            def name(self):
                return "SxcLineIdPage2"
            
            def description(self):
                return "Contains the ingoing LineID Page2 (0-23: 0-7:TFI5 side, 8-15: Line Side, 16-23: Loho). Disconnect (output is all one) if value LineID is other value (recommend is 24)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SxcStsIdPage2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SxcStsIdPage2"
            
            def description(self):
                return "Contains the ingoing STSID Page2 (0-47)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SxcLineIdPage2"] = _AF6CNC0022_RD_OCN._sxcramctl1._SxcLineIdPage2()
            allFields["SxcStsIdPage2"] = _AF6CNC0022_RD_OCN._sxcramctl1._SxcStsIdPage2()
            return allFields

    class _apsramctl(AtRegister.AtRegister):
        def name(self):
            return "OCN SXC Control 3 - Config APS"
    
        def description(self):
            return "Each register is used for APS configuration # HDL_PATH		: isdhsxc.isxc_rxrd[0].apsctl_ram.array"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x44080 + 256*LineId + StsId"
            
        def startAddress(self):
            return 0x00044080
            
        def endAddress(self):
            return 0x000457af

        class _SelectorType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "SelectorType"
            
            def description(self):
                return "Selector Type. 3: SFM. Choose based SFM input 2: APS Grouping. 1: UPSR/SNCP Grouping. 0: Disable APS/UPSR."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SelectorID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SelectorID"
            
            def description(self):
                return "Contains Selector ID. When Selector Type is: - SFM: Unused this field. - APS: [8]  : Passthrough Enable for BLSR [7:4]: Passthrough Group ID for BLSR [3:0]: APS Group ID - UPSR/SNCP: Select UPSR ID."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SelectorType"] = _AF6CNC0022_RD_OCN._apsramctl._SelectorType()
            allFields["SelectorID"] = _AF6CNC0022_RD_OCN._apsramctl._SelectorID()
            return allFields

    class _rxhomapramctl(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx High Order Map concatenate configuration"
    
        def description(self):
            return "Each register is used to configure concatenation for each STS to High Order Map path. # HDL_PATH		: irxpp_outho_inst.irxpp_outputho [0].ohoramctl.array"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x48000 + 512*LineId + StsId"
            
        def startAddress(self):
            return 0x00048000
            
        def endAddress(self):
            return 0x00048e2f

        class _HoMapStsSlvInd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "HoMapStsSlvInd"
            
            def description(self):
                return "This is used to configure STS is slaver or master. 1: Slaver."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _HoMapStsMstId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "HoMapStsMstId"
            
            def description(self):
                return "This is the ID of the master STS-1 in the concatenation that contains this STS-1."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["HoMapStsSlvInd"] = _AF6CNC0022_RD_OCN._rxhomapramctl._HoMapStsSlvInd()
            allFields["HoMapStsMstId"] = _AF6CNC0022_RD_OCN._rxhomapramctl._HoMapStsMstId()
            return allFields

    class _demramctl(AtRegister.AtRegister):
        def name(self):
            return "OCN RXPP Per STS payload Control"
    
        def description(self):
            return "Each register is used to configure VT payload mode per STS. # HDL_PATH		: itfi5ho.irxpp_vtpp_inst.irxpp_vtpp[0].rxpp_demramctl.array"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x60000 + 16384*LineId + StsId"
            
        def startAddress(self):
            return 0x00060000
            
        def endAddress(self):
            return 0x0007c02f

        class _PiDemStsTerm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PiDemStsTerm"
            
            def description(self):
                return "Enable to terminate the related STS/VC. It means that STS POH defects related to the STS/VC to generate AIS to downstream. Must be set to 1. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PiDemSpeType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 14
        
            def name(self):
                return "PiDemSpeType"
            
            def description(self):
                return "Configure types of SPE. 0: Disable processing pointers of all VT/TUs in this SPE. 1: VC-3 type or STS SPE containing VT/TU exception TU-3 2: TUG-3 type containing VT/TU exception TU-3 3: TUG-3 type containing TU-3."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PiDemTug26Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PiDemTug26Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #6. 0: TU11 1: TU12"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PiDemTug25Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "PiDemTug25Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #5."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PiDemTug24Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PiDemTug24Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #4."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PiDemTug23Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PiDemTug23Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #3."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PiDemTug22Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PiDemTug22Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #2."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PiDemTug21Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PiDemTug21Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #1."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PiDemTug20Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PiDemTug20Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #0."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PiDemStsTerm"] = _AF6CNC0022_RD_OCN._demramctl._PiDemStsTerm()
            allFields["PiDemSpeType"] = _AF6CNC0022_RD_OCN._demramctl._PiDemSpeType()
            allFields["PiDemTug26Type"] = _AF6CNC0022_RD_OCN._demramctl._PiDemTug26Type()
            allFields["PiDemTug25Type"] = _AF6CNC0022_RD_OCN._demramctl._PiDemTug25Type()
            allFields["PiDemTug24Type"] = _AF6CNC0022_RD_OCN._demramctl._PiDemTug24Type()
            allFields["PiDemTug23Type"] = _AF6CNC0022_RD_OCN._demramctl._PiDemTug23Type()
            allFields["PiDemTug22Type"] = _AF6CNC0022_RD_OCN._demramctl._PiDemTug22Type()
            allFields["PiDemTug21Type"] = _AF6CNC0022_RD_OCN._demramctl._PiDemTug21Type()
            allFields["PiDemTug20Type"] = _AF6CNC0022_RD_OCN._demramctl._PiDemTug20Type()
            return allFields

    class _vpiramctl(AtRegister.AtRegister):
        def name(self):
            return "OCN VTTU Pointer Interpreter Per Channel Control"
    
        def description(self):
            return "Each register is used to configure for VTTU pointer interpreter engines. # HDL_PATH		: itfi5ho.irxpp_vtpp_inst.irxpp_vtpp[0].rxpp_vpiramctl.array"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x60800 + 16384*LineId + 32*StsId + 4*VtgId + VtId"
            
        def startAddress(self):
            return 0x00060800
            
        def endAddress(self):
            return 0x0007cfff

        class _LineVtPiLoTerm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "LineVtPiLoTerm"
            
            def description(self):
                return "Enable to terminate the related VTTU. It means that VTTU POH defects related to the VTTU to generate AIS to downstream."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LineVtPiAisFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "LineVtPiAisFrc"
            
            def description(self):
                return "Forcing SFM to AIS state. 1: Force 0: Not force"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LineVtPiSSDetPatt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "LineVtPiSSDetPatt"
            
            def description(self):
                return "Configure pattern SS bits that is used to compare with the extracted SS bits from receive direction."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LineVtPiSSDetEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "LineVtPiSSDetEn"
            
            def description(self):
                return "Enable/disable checking SS bits in PI State Machine. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LineVtPiAdjRule(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LineVtPiAdjRule"
            
            def description(self):
                return "Configure the rule for detecting adjustment condition. 1: The n of 5 rule is selected. This mode is applied for SDH mode 0: The 8 of 10 rule is selected. This mode is applied for SONET mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["LineVtPiLoTerm"] = _AF6CNC0022_RD_OCN._vpiramctl._LineVtPiLoTerm()
            allFields["LineVtPiAisFrc"] = _AF6CNC0022_RD_OCN._vpiramctl._LineVtPiAisFrc()
            allFields["LineVtPiSSDetPatt"] = _AF6CNC0022_RD_OCN._vpiramctl._LineVtPiSSDetPatt()
            allFields["LineVtPiSSDetEn"] = _AF6CNC0022_RD_OCN._vpiramctl._LineVtPiSSDetEn()
            allFields["LineVtPiAdjRule"] = _AF6CNC0022_RD_OCN._vpiramctl._LineVtPiAdjRule()
            return allFields

    class _pgdemramctl(AtRegister.AtRegister):
        def name(self):
            return "OCN TXPP Per STS Multiplexing Control"
    
        def description(self):
            return "Each register is used to configure VT payload mode per STS at Tx pointer generator. # HDL_PATH		: itfi5ho.itxpp_vtpp_inst.itxpp_vtpp[0].txpg_pgdmctl.array"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x80000 + 16384*LineId + StsId"
            
        def startAddress(self):
            return 0x00080000
            
        def endAddress(self):
            return 0x0009c02f

        class _PgDemSpeType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 14
        
            def name(self):
                return "PgDemSpeType"
            
            def description(self):
                return "Configure types of SPE. 0: Disable processing pointers of all VT/TUs in this SPE. 1: VC-3 type or STS SPE containing VT/TU exception TU-3 2: TUG-3 type containing VT/TU exception TU-3 3: TUG-3 type containing TU-3."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PgDemTug26Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PgDemTug26Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #6. 0: TU11 1: TU12"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PgDemTug25Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "PgDemTug25Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #5."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PgDemTug24Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PgDemTug24Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #4."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PgDemTug23Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PgDemTug23Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #3."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PgDemTug22Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PgDemTug22Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #2."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PgDemTug21Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PgDemTug21Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #1."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PgDemTug20Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PgDemTug20Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #0."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PgDemSpeType"] = _AF6CNC0022_RD_OCN._pgdemramctl._PgDemSpeType()
            allFields["PgDemTug26Type"] = _AF6CNC0022_RD_OCN._pgdemramctl._PgDemTug26Type()
            allFields["PgDemTug25Type"] = _AF6CNC0022_RD_OCN._pgdemramctl._PgDemTug25Type()
            allFields["PgDemTug24Type"] = _AF6CNC0022_RD_OCN._pgdemramctl._PgDemTug24Type()
            allFields["PgDemTug23Type"] = _AF6CNC0022_RD_OCN._pgdemramctl._PgDemTug23Type()
            allFields["PgDemTug22Type"] = _AF6CNC0022_RD_OCN._pgdemramctl._PgDemTug22Type()
            allFields["PgDemTug21Type"] = _AF6CNC0022_RD_OCN._pgdemramctl._PgDemTug21Type()
            allFields["PgDemTug20Type"] = _AF6CNC0022_RD_OCN._pgdemramctl._PgDemTug20Type()
            return allFields

    class _vpgramctl(AtRegister.AtRegister):
        def name(self):
            return "OCN VTTU Pointer Generator Per Channel Control"
    
        def description(self):
            return "Each register is used to configure for VTTU pointer Generator engines. # HDL_PATH		: itfi5ho.itxpp_vtpp_inst.itxpp_vtpp[0].txpg_pgctl.array"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x80800 + 16384*LineId + 32*StsId + 4*VtgId + VtId"
            
        def startAddress(self):
            return 0x00080800
            
        def endAddress(self):
            return 0x0009cfff

        class _VtPgBipErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "VtPgBipErrFrc"
            
            def description(self):
                return "Forcing Bip error. 1: Force 0: Not force"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPgLopFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "VtPgLopFrc"
            
            def description(self):
                return "Forcing LOP. 1: Force 0: Not force"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPgUeqFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "VtPgUeqFrc"
            
            def description(self):
                return "Forcing SFM to UEQ state. 1: Force 0: Not force"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPgAisFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "VtPgAisFrc"
            
            def description(self):
                return "Forcing SFM to AIS state. 1: Force 0: Not force"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPgSSInsPatt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "VtPgSSInsPatt"
            
            def description(self):
                return "Configure pattern SS bits that is used to insert to Pointer value."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPgSSInsEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "VtPgSSInsEn"
            
            def description(self):
                return "Enable/disable SS bits insertion. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPgPohIns(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "VtPgPohIns"
            
            def description(self):
                return "Enable/ disable POH Insertion. High to enable insertion of POH."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["VtPgBipErrFrc"] = _AF6CNC0022_RD_OCN._vpgramctl._VtPgBipErrFrc()
            allFields["VtPgLopFrc"] = _AF6CNC0022_RD_OCN._vpgramctl._VtPgLopFrc()
            allFields["VtPgUeqFrc"] = _AF6CNC0022_RD_OCN._vpgramctl._VtPgUeqFrc()
            allFields["VtPgAisFrc"] = _AF6CNC0022_RD_OCN._vpgramctl._VtPgAisFrc()
            allFields["VtPgSSInsPatt"] = _AF6CNC0022_RD_OCN._vpgramctl._VtPgSSInsPatt()
            allFields["VtPgSSInsEn"] = _AF6CNC0022_RD_OCN._vpgramctl._VtPgSSInsEn()
            allFields["VtPgPohIns"] = _AF6CNC0022_RD_OCN._vpgramctl._VtPgPohIns()
            return allFields

    class _upvtchstkram(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx VT/TU per Alarm Interrupt Status"
    
        def description(self):
            return "This is the per Alarm interrupt status of VT/TU pointer interpreter . Each register is used to store 5 sticky bits for 5 alarms in the VT/TU."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x62800 + 16384*LineId + 32*StsId + 4*VtgId + VtId"
            
        def startAddress(self):
            return 0x00062800
            
        def endAddress(self):
            return 0x0007efff

        class _VtPiStsNewDetIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "VtPiStsNewDetIntr"
            
            def description(self):
                return "Set to 1 while an New Pointer Detection event is detected at VT/TU pointer interpreter. This event doesn't raise interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiStsNdfIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "VtPiStsNdfIntr"
            
            def description(self):
                return "Set to 1 while an NDF event is detected at VT/TU pointer interpreter. This event doesn't raise interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiStsCepUneqVStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "VtPiStsCepUneqVStateChgIntr"
            
            def description(self):
                return "Set 1 to while there is change in Unequip state in the related VT/TU to generate an interrupt. Read the OCN Rx VT/TU per Alarm Current Status register of the related VT/TU to know the VT/TU whether in Uneqip state or not."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiStsAISStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "VtPiStsAISStateChgIntr"
            
            def description(self):
                return "Set 1 to while there is change in AIS state in the related VT/TU to generate an interrupt. Read the OCN Rx VT/TU per Alarm Current Status register of the related VT/TU to know the VT/TU whether in LOP state or not."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiStsLopStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "VtPiStsLopStateChgIntr"
            
            def description(self):
                return "Set 1 to while there is change in LOP state in the related VT/TU to generate an interrupt. Read the OCN Rx VT/TU per Alarm Current Status register of the related VT/TU to know the VT/TU whether in AIS state or not."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["VtPiStsNewDetIntr"] = _AF6CNC0022_RD_OCN._upvtchstkram._VtPiStsNewDetIntr()
            allFields["VtPiStsNdfIntr"] = _AF6CNC0022_RD_OCN._upvtchstkram._VtPiStsNdfIntr()
            allFields["VtPiStsCepUneqVStateChgIntr"] = _AF6CNC0022_RD_OCN._upvtchstkram._VtPiStsCepUneqVStateChgIntr()
            allFields["VtPiStsAISStateChgIntr"] = _AF6CNC0022_RD_OCN._upvtchstkram._VtPiStsAISStateChgIntr()
            allFields["VtPiStsLopStateChgIntr"] = _AF6CNC0022_RD_OCN._upvtchstkram._VtPiStsLopStateChgIntr()
            return allFields

    class _upvtchstaram(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx VT/TU per Alarm Current Status"
    
        def description(self):
            return "This is the per Alarm current status of VT/TU pointer interpreter. Each register is used to store 3 bits to store current status of 3 alarms in the VT/TU."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x63000 + 16384*LineId + 32*StsId + 4*VtgId + VtId"
            
        def startAddress(self):
            return 0x00063000
            
        def endAddress(self):
            return 0x0007ffff

        class _VtPiStsCepUneqCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "VtPiStsCepUneqCurStatus"
            
            def description(self):
                return "Unequip current status in the related VT/TU. When it changes for 0 to 1 or vice versa, the VtPiStsCepUneqVStateChgIntr bit in the OCN Rx VT/TU per Alarm Interrupt Status register of the related VT/TU is set."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiStsAisCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "VtPiStsAisCurStatus"
            
            def description(self):
                return "AIS current status in the related VT/TU. When it changes for 0 to 1 or vice versa, the VtPiStsAISStateChgIntr bit in the OCN Rx VT/TU per Alarm Interrupt Status register of the related VT/TU is set."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiStsLopCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "VtPiStsLopCurStatus"
            
            def description(self):
                return "LOP current status in the related VT/TU. When it changes for 0 to 1 or vice versa, the VtPiStsLopStateChgIntr bit in the OCN Rx VT/TU per Alarm Interrupt Status register of the related VT/TU is set."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["VtPiStsCepUneqCurStatus"] = _AF6CNC0022_RD_OCN._upvtchstaram._VtPiStsCepUneqCurStatus()
            allFields["VtPiStsAisCurStatus"] = _AF6CNC0022_RD_OCN._upvtchstaram._VtPiStsAisCurStatus()
            allFields["VtPiStsLopCurStatus"] = _AF6CNC0022_RD_OCN._upvtchstaram._VtPiStsLopCurStatus()
            return allFields

    class _adjcntperstkram(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx PP VT/TU Pointer interpreter pointer adjustment per channel counter"
    
        def description(self):
            return "Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in VT/TU pointer interpreter. These counters are in saturation mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x61000 + 16384*LineId + 2048*AdjMode + 32*StsId + 4*VtgId + VtId"
            
        def startAddress(self):
            return 0x00061000
            
        def endAddress(self):
            return 0x0007dfff

        class _RxPpViPiPtAdjCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPpViPiPtAdjCnt"
            
            def description(self):
                return "The pointer Increment counter or decrease counter. Bit [11] of address is used to indicate that the counter is pointer increment or decrement counter. The counter will stop at maximum value (0x3FFFF)."
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPpViPiPtAdjCnt"] = _AF6CNC0022_RD_OCN._adjcntperstkram._RxPpViPiPtAdjCnt()
            return allFields

    class _vtpgstkram(AtRegister.AtRegister):
        def name(self):
            return "OCN TxPg VTTU per Alarm Interrupt Status"
    
        def description(self):
            return "This is the per Alarm interrupt status of STS/VT/TU pointer generator . Each register is used to store 3 sticky bits for 3 alarms"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x82800 + 16384*LineId + 32*StsId + 4*VtgId + VtId"
            
        def startAddress(self):
            return 0x00082800
            
        def endAddress(self):
            return 0x0009efff

        class _VtPgAisIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "VtPgAisIntr"
            
            def description(self):
                return "Set to 1 while an AIS status event (at h2pos) is detected at Tx VTTU Pointer Generator."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPgFiFoOvfIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "VtPgFiFoOvfIntr"
            
            def description(self):
                return "Set to 1 while an FIFO Overflowed event is detected at Tx VTTU Pointer Generator."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPgNdfIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "VtPgNdfIntr"
            
            def description(self):
                return "Set to 1 while an NDF status event (at h2pos) is detected at Tx VTTU Pointer Generator."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["VtPgAisIntr"] = _AF6CNC0022_RD_OCN._vtpgstkram._VtPgAisIntr()
            allFields["VtPgFiFoOvfIntr"] = _AF6CNC0022_RD_OCN._vtpgstkram._VtPgFiFoOvfIntr()
            allFields["VtPgNdfIntr"] = _AF6CNC0022_RD_OCN._vtpgstkram._VtPgNdfIntr()
            return allFields

    class _adjcntpgpervtram(AtRegister.AtRegister):
        def name(self):
            return "OCN TxPg VTTU pointer adjustment per channel counter"
    
        def description(self):
            return "Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in VTTU pointer generator. These counters are in saturation mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x81000 + 16384*LineId + 2048*AdjMode + 32*StsId + 4*VtgId + VtId"
            
        def startAddress(self):
            return 0x00081000
            
        def endAddress(self):
            return 0x0009dfff

        class _VtpgPtAdjCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "VtpgPtAdjCnt"
            
            def description(self):
                return "The pointer Increment counter or decrease counter. Bit [11] of address is used to indicate that the counter is pointer increment or decrement counter. The counter will stop at maximum value (0x3FFFF)."
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["VtpgPtAdjCnt"] = _AF6CNC0022_RD_OCN._adjcntpgpervtram._VtpgPtAdjCnt()
            return allFields

    class _parfrccfg1(AtRegister.AtRegister):
        def name(self):
            return "OCN Parity Force Config 1 - TFI5 Side"
    
        def description(self):
            return "OCN Parity Force Config 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000f020
            
        def endAddress(self):
            return 0xffffffff

        class _EsSpgParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "EsSpgParErrFrc"
            
            def description(self):
                return "Force Parity for ram config \"OCN STS Pointer Generator Per Channel Control - TFI5 Side\". Each bit correspond with each LineId. Set 1 to force."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _EsSpiParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "EsSpiParErrFrc"
            
            def description(self):
                return "Force Parity for ram config \"OCN STS Pointer Interpreter Per Channel Control - TFI5 Side\". Each bit correspond with each LineId. Set 1 to force."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxBarParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TxBarParErrFrc"
            
            def description(self):
                return "Force Parity for ram config \"OCN Tx Bridge and Roll SXC Control - TFI5 Side\". Each bit correspond with each LineId. Set 1 to force."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxBarParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxBarParErrFrc"
            
            def description(self):
                return "Force Parity for ram config \"OCN Rx Bridge and Roll SXC Control - TFI5 Side\". Each bit correspond with each LineId. Set 1 to force."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["EsSpgParErrFrc"] = _AF6CNC0022_RD_OCN._parfrccfg1._EsSpgParErrFrc()
            allFields["EsSpiParErrFrc"] = _AF6CNC0022_RD_OCN._parfrccfg1._EsSpiParErrFrc()
            allFields["TxBarParErrFrc"] = _AF6CNC0022_RD_OCN._parfrccfg1._TxBarParErrFrc()
            allFields["RxBarParErrFrc"] = _AF6CNC0022_RD_OCN._parfrccfg1._RxBarParErrFrc()
            return allFields

    class _pardiscfg1(AtRegister.AtRegister):
        def name(self):
            return "OCN Parity Disable Config 1 - TFI5 Side"
    
        def description(self):
            return "OCN Parity Disable Config 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000f021
            
        def endAddress(self):
            return 0xffffffff

        class _EsSpgParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "EsSpgParErrDis"
            
            def description(self):
                return "Disable Parity for ram config \"OCN STS Pointer Generator Per Channel Control - TFI5 Side\". Each bit correspond with each LineId. Set 1 to disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _EsSpiParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "EsSpiParErrDis"
            
            def description(self):
                return "Disable Parity for ram config \"OCN STS Pointer Interpreter Per Channel Control - TFI5 Side\". Each bit correspond with each LineId. Set 1 to disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxBarParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TxBarParErrDis"
            
            def description(self):
                return "Disable Parity for ram config \"OCN Tx Bridge and Roll SXC Control - TFI5 Side\". Each bit correspond with each LineId. Set 1 to disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxBarParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxBarParErrDis"
            
            def description(self):
                return "Disable Parity for ram config \"OCN Rx Bridge and Roll SXC Control - TFI5 Side\". Each bit correspond with each LineId. Set 1 to disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["EsSpgParErrDis"] = _AF6CNC0022_RD_OCN._pardiscfg1._EsSpgParErrDis()
            allFields["EsSpiParErrDis"] = _AF6CNC0022_RD_OCN._pardiscfg1._EsSpiParErrDis()
            allFields["TxBarParErrDis"] = _AF6CNC0022_RD_OCN._pardiscfg1._TxBarParErrDis()
            allFields["RxBarParErrDis"] = _AF6CNC0022_RD_OCN._pardiscfg1._RxBarParErrDis()
            return allFields

    class _parerrstk1(AtRegister.AtRegister):
        def name(self):
            return "OCN Parity Error Sticky 1 - TFI5 Side"
    
        def description(self):
            return "OCN Parity Error Sticky 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000f022
            
        def endAddress(self):
            return 0xffffffff

        class _EsSpgParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "EsSpgParErrStk"
            
            def description(self):
                return "Error Sticky for ram config \"OCN STS Pointer Generator Per Channel Control - TFI5 Side\". Each bit correspond with each LineId."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _EsSpiParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "EsSpiParErrStk"
            
            def description(self):
                return "Error Sticky for ram config \"OCN STS Pointer Interpreter Per Channel Control - TFI5 Side\". Each bit correspond with each LineId."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _TxBarParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TxBarParErrStk"
            
            def description(self):
                return "Error Sticky for ram config \"OCN Tx Bridge and Roll SXC Control - TFI5 Side\". Each bit correspond with each LineId."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _RxBarParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxBarParErrStk"
            
            def description(self):
                return "Error Sticky for ram config \"OCN Rx Bridge and Roll SXC Control - TFI5 Side\". Each bit correspond with each LineId."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["EsSpgParErrStk"] = _AF6CNC0022_RD_OCN._parerrstk1._EsSpgParErrStk()
            allFields["EsSpiParErrStk"] = _AF6CNC0022_RD_OCN._parerrstk1._EsSpiParErrStk()
            allFields["TxBarParErrStk"] = _AF6CNC0022_RD_OCN._parerrstk1._TxBarParErrStk()
            allFields["RxBarParErrStk"] = _AF6CNC0022_RD_OCN._parerrstk1._RxBarParErrStk()
            return allFields

    class _parfrccfg2(AtRegister.AtRegister):
        def name(self):
            return "OCN Parity Force Config 2 - Line Side"
    
        def description(self):
            return "OCN Parity Force Config 2"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x20020 + 65536*GroupID"
            
        def startAddress(self):
            return 0x00020020
            
        def endAddress(self):
            return 0xffffffff

        class _TohParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "TohParErrFrc"
            
            def description(self):
                return "Force Parity for ram config \"OCN TOH Monitoring Per Line Control\".Each bit correspond with each LineId. Set 1 to force."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SpgParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "SpgParErrFrc"
            
            def description(self):
                return "Force Parity for ram config \"OCN STS Pointer Generator Per Channel Control\". Each bit correspond with each LineId. Set 1 to force."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SpiParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "SpiParErrFrc"
            
            def description(self):
                return "Force Parity for ram config \"OCN STS Pointer Interpreter Per Channel Control\". Each bit correspond with each LineId. Set 1 to force."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxFrmParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxFrmParErrFrc"
            
            def description(self):
                return "Force Parity for ram config \"OCN Tx J0 Insertion Buffer\". Each bit correspond with each LineId. Set 1 to force."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TohParErrFrc"] = _AF6CNC0022_RD_OCN._parfrccfg2._TohParErrFrc()
            allFields["SpgParErrFrc"] = _AF6CNC0022_RD_OCN._parfrccfg2._SpgParErrFrc()
            allFields["SpiParErrFrc"] = _AF6CNC0022_RD_OCN._parfrccfg2._SpiParErrFrc()
            allFields["TxFrmParErrFrc"] = _AF6CNC0022_RD_OCN._parfrccfg2._TxFrmParErrFrc()
            return allFields

    class _pardiscfg2(AtRegister.AtRegister):
        def name(self):
            return "OCN Parity Disable Config 2 - Line Side"
    
        def description(self):
            return "OCN Parity Disable Config 2"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x20021 + 65536*GroupID"
            
        def startAddress(self):
            return 0x00020021
            
        def endAddress(self):
            return 0xffffffff

        class _TohParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "TohParErrDis"
            
            def description(self):
                return "Disable Parity for ram config \"OCN TOH Monitoring Per Line Control\".Each bit correspond with each LineId. Set 1 to disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SpiParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "SpiParErrDis"
            
            def description(self):
                return "Disable Parity for ram config \"OCN STS Pointer Interpreter Per Channel Control\". Each bit correspond with each LineId. Set 1 to disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxBarParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TxBarParErrDis"
            
            def description(self):
                return "Disable Parity for ram config \"OCN Tx Bridge and Roll SXC Control\". Each bit correspond with each LineId. Set 1 to disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxFrmParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxFrmParErrDis"
            
            def description(self):
                return "Disable Parity for ram config \"OCN Tx J0 Insertion Buffer\". Each bit correspond with each LineId. Set 1 to disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TohParErrDis"] = _AF6CNC0022_RD_OCN._pardiscfg2._TohParErrDis()
            allFields["SpiParErrDis"] = _AF6CNC0022_RD_OCN._pardiscfg2._SpiParErrDis()
            allFields["TxBarParErrDis"] = _AF6CNC0022_RD_OCN._pardiscfg2._TxBarParErrDis()
            allFields["TxFrmParErrDis"] = _AF6CNC0022_RD_OCN._pardiscfg2._TxFrmParErrDis()
            return allFields

    class _parerrstk2(AtRegister.AtRegister):
        def name(self):
            return "OCN Parity Error Sticky 2 - Line Side"
    
        def description(self):
            return "OCN Parity Error Sticky 2"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x20022 + 65536*GroupID"
            
        def startAddress(self):
            return 0x00020022
            
        def endAddress(self):
            return 0xffffffff

        class _TohParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "TohParErrStk"
            
            def description(self):
                return "Error Sticky for ram config \"OCN TOH Monitoring Per Line Control\".Each bit correspond with each LineId."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _SpiParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "SpiParErrStk"
            
            def description(self):
                return "Error Sticky for ram config \"OCN STS Pointer Interpreter Per Channel Control\". Each bit correspond with each LineId."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _TxBarParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TxBarParErrStk"
            
            def description(self):
                return "Error Sticky for ram config \"OCN Tx Bridge and Roll SXC Control\". Each bit correspond with each LineId."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _TxFrmParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxFrmParErrStk"
            
            def description(self):
                return "Error Sticky for ram config \"OCN Tx J0 Insertion Buffer\". Each bit correspond with each LineId."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TohParErrStk"] = _AF6CNC0022_RD_OCN._parerrstk2._TohParErrStk()
            allFields["SpiParErrStk"] = _AF6CNC0022_RD_OCN._parerrstk2._SpiParErrStk()
            allFields["TxBarParErrStk"] = _AF6CNC0022_RD_OCN._parerrstk2._TxBarParErrStk()
            allFields["TxFrmParErrStk"] = _AF6CNC0022_RD_OCN._parerrstk2._TxFrmParErrStk()
            return allFields

    class _parfrccfg3(AtRegister.AtRegister):
        def name(self):
            return "OCN Parity Force Config 3 - VPI+VPG+OHO"
    
        def description(self):
            return "OCN Parity Force Config 3 for 4 lines 0-3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000f0020
            
        def endAddress(self):
            return 0xffffffff

        class _RxHoParErrFrc0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "RxHoParErrFrc0"
            
            def description(self):
                return "Force Parity for ram config \"OCN Rx High Order Map concatenate configuration\".Each bit correspond with each LineId. Set 1 to force."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VpgCtlParErrFrc0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "VpgCtlParErrFrc0"
            
            def description(self):
                return "Force Parity for ram config \"OCN VTTU Pointer Generator Per Channel Control\". Each bit correspond with each LineId. Set 1 to force."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VpgDemParErrFrc0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "VpgDemParErrFrc0"
            
            def description(self):
                return "Force Parity for ram config \"OCN TXPP Per STS Multiplexing Control\". Each bit correspond with each LineId. Set 1 to force."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VpiCtlParErrFrc0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "VpiCtlParErrFrc0"
            
            def description(self):
                return "Force Parity for ram config \"OCN VTTU Pointer Interpreter Per Channel Control\". Each bit correspond with each LineId. Set 1 to force."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VpiDemParErrFrc0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "VpiDemParErrFrc0"
            
            def description(self):
                return "Force Parity for ram config \"OCN RXPP Per STS payload Control\". Each bit correspond with each LineId. Set 1 to force."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxHoParErrFrc0"] = _AF6CNC0022_RD_OCN._parfrccfg3._RxHoParErrFrc0()
            allFields["VpgCtlParErrFrc0"] = _AF6CNC0022_RD_OCN._parfrccfg3._VpgCtlParErrFrc0()
            allFields["VpgDemParErrFrc0"] = _AF6CNC0022_RD_OCN._parfrccfg3._VpgDemParErrFrc0()
            allFields["VpiCtlParErrFrc0"] = _AF6CNC0022_RD_OCN._parfrccfg3._VpiCtlParErrFrc0()
            allFields["VpiDemParErrFrc0"] = _AF6CNC0022_RD_OCN._parfrccfg3._VpiDemParErrFrc0()
            return allFields

    class _pardiscfg3(AtRegister.AtRegister):
        def name(self):
            return "OCN Parity Disable Config 3 - VPI+VPG+OHO"
    
        def description(self):
            return "OCN Parity Disable Config 3 for 4 lines 0-3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000f0021
            
        def endAddress(self):
            return 0xffffffff

        class _RxHoParErrDis0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "RxHoParErrDis0"
            
            def description(self):
                return "Disable Parity for ram config \"OCN Rx High Order Map concatenate configuration\".Each bit correspond with each LineId. Set 1 to disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VpgCtlParErrDis0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "VpgCtlParErrDis0"
            
            def description(self):
                return "Disable Parity for ram config \"OCN VTTU Pointer Generator Per Channel Control\". Each bit correspond with each LineId. Set 1 to disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VpgDemParErrDis0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "VpgDemParErrDis0"
            
            def description(self):
                return "Disable Parity for ram config \"OCN TXPP Per STS Multiplexing Control\". Each bit correspond with each LineId. Set 1 to disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VpiCtlParErrDis0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "VpiCtlParErrDis0"
            
            def description(self):
                return "Disable Parity for ram config \"OCN VTTU Pointer Interpreter Per Channel Control\". Each bit correspond with each LineId. Set 1 to disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VpiDemParErrDis0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "VpiDemParErrDis0"
            
            def description(self):
                return "Disable Parity for ram config \"OCN RXPP Per STS payload Control\". Each bit correspond with each LineId. Set 1 to disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxHoParErrDis0"] = _AF6CNC0022_RD_OCN._pardiscfg3._RxHoParErrDis0()
            allFields["VpgCtlParErrDis0"] = _AF6CNC0022_RD_OCN._pardiscfg3._VpgCtlParErrDis0()
            allFields["VpgDemParErrDis0"] = _AF6CNC0022_RD_OCN._pardiscfg3._VpgDemParErrDis0()
            allFields["VpiCtlParErrDis0"] = _AF6CNC0022_RD_OCN._pardiscfg3._VpiCtlParErrDis0()
            allFields["VpiDemParErrDis0"] = _AF6CNC0022_RD_OCN._pardiscfg3._VpiDemParErrDis0()
            return allFields

    class _parerrstk3(AtRegister.AtRegister):
        def name(self):
            return "OCN Parity Error Sticky 3 - VPI+VPG+OHO"
    
        def description(self):
            return "OCN Parity Error Sticky 3 for 4 lines 0-3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000f0022
            
        def endAddress(self):
            return 0xffffffff

        class _RxHoParErrStk0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "RxHoParErrStk0"
            
            def description(self):
                return "Error Sticky for ram config \"OCN Rx High Order Map concatenate configuration\".Each bit correspond with each LineId."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _VpgCtlParErrStk0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "VpgCtlParErrStk0"
            
            def description(self):
                return "Error Sticky for ram config \"OCN VTTU Pointer Generator Per Channel Control\". Each bit correspond with each LineId."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _VpgDemParErrStk0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "VpgDemParErrStk0"
            
            def description(self):
                return "Error Sticky for ram config \"OCN TXPP Per STS Multiplexing Control\". Each bit correspond with each LineId."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _VpiCtlParErrStk0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "VpiCtlParErrStk0"
            
            def description(self):
                return "Error Sticky for ram config \"OCN VTTU Pointer Interpreter Per Channel Control\". Each bit correspond with each LineId."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _VpiDemParErrStk0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "VpiDemParErrStk0"
            
            def description(self):
                return "Error Sticky for ram config \"OCN RXPP Per STS payload Control\". Each bit correspond with each LineId."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxHoParErrStk0"] = _AF6CNC0022_RD_OCN._parerrstk3._RxHoParErrStk0()
            allFields["VpgCtlParErrStk0"] = _AF6CNC0022_RD_OCN._parerrstk3._VpgCtlParErrStk0()
            allFields["VpgDemParErrStk0"] = _AF6CNC0022_RD_OCN._parerrstk3._VpgDemParErrStk0()
            allFields["VpiCtlParErrStk0"] = _AF6CNC0022_RD_OCN._parerrstk3._VpiCtlParErrStk0()
            allFields["VpiDemParErrStk0"] = _AF6CNC0022_RD_OCN._parerrstk3._VpiDemParErrStk0()
            return allFields

    class _parfrccfg4(AtRegister.AtRegister):
        def name(self):
            return "OCN Parity Force Config 4 - VPI+VPG+OHO"
    
        def description(self):
            return "OCN Parity Force Config 4 for 4 lines 4-7"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000f0030
            
        def endAddress(self):
            return 0xffffffff

        class _RxHoParErrFrc1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "RxHoParErrFrc1"
            
            def description(self):
                return "Force Parity for ram config \"OCN Rx High Order Map concatenate configuration\".Each bit correspond with each LineId. Set 1 to force."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VpgCtlParErrFrc1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "VpgCtlParErrFrc1"
            
            def description(self):
                return "Force Parity for ram config \"OCN VTTU Pointer Generator Per Channel Control\". Each bit correspond with each LineId. Set 1 to force."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VpgDemParErrFrc1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "VpgDemParErrFrc1"
            
            def description(self):
                return "Force Parity for ram config \"OCN TXPP Per STS Multiplexing Control\". Each bit correspond with each LineId. Set 1 to force."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VpiCtlParErrFrc1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "VpiCtlParErrFrc1"
            
            def description(self):
                return "Force Parity for ram config \"OCN VTTU Pointer Interpreter Per Channel Control\". Each bit correspond with each LineId. Set 1 to force."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VpiDemParErrFrc1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "VpiDemParErrFrc1"
            
            def description(self):
                return "Force Parity for ram config \"OCN RXPP Per STS payload Control\". Each bit correspond with each LineId. Set 1 to force."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxHoParErrFrc1"] = _AF6CNC0022_RD_OCN._parfrccfg4._RxHoParErrFrc1()
            allFields["VpgCtlParErrFrc1"] = _AF6CNC0022_RD_OCN._parfrccfg4._VpgCtlParErrFrc1()
            allFields["VpgDemParErrFrc1"] = _AF6CNC0022_RD_OCN._parfrccfg4._VpgDemParErrFrc1()
            allFields["VpiCtlParErrFrc1"] = _AF6CNC0022_RD_OCN._parfrccfg4._VpiCtlParErrFrc1()
            allFields["VpiDemParErrFrc1"] = _AF6CNC0022_RD_OCN._parfrccfg4._VpiDemParErrFrc1()
            return allFields

    class _pardiscfg4(AtRegister.AtRegister):
        def name(self):
            return "OCN Parity Disable Config 4 - VPI+VPG+OHO"
    
        def description(self):
            return "OCN Parity Disable Config 4 for 4 lines 4-7"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000f0031
            
        def endAddress(self):
            return 0xffffffff

        class _RxHoParErrDis1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "RxHoParErrDis1"
            
            def description(self):
                return "Disable Parity for ram config \"OCN Rx High Order Map concatenate configuration\".Each bit correspond with each LineId. Set 1 to disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VpgCtlParErrDis1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "VpgCtlParErrDis1"
            
            def description(self):
                return "Disable Parity for ram config \"OCN VTTU Pointer Generator Per Channel Control\". Each bit correspond with each LineId. Set 1 to disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VpgDemParErrDis1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "VpgDemParErrDis1"
            
            def description(self):
                return "Disable Parity for ram config \"OCN TXPP Per STS Multiplexing Control\". Each bit correspond with each LineId. Set 1 to disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VpiCtlParErrDis1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "VpiCtlParErrDis1"
            
            def description(self):
                return "Disable Parity for ram config \"OCN VTTU Pointer Interpreter Per Channel Control\". Each bit correspond with each LineId. Set 1 to disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VpiDemParErrDis1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "VpiDemParErrDis1"
            
            def description(self):
                return "Disable Parity for ram config \"OCN RXPP Per STS payload Control\". Each bit correspond with each LineId. Set 1 to disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxHoParErrDis1"] = _AF6CNC0022_RD_OCN._pardiscfg4._RxHoParErrDis1()
            allFields["VpgCtlParErrDis1"] = _AF6CNC0022_RD_OCN._pardiscfg4._VpgCtlParErrDis1()
            allFields["VpgDemParErrDis1"] = _AF6CNC0022_RD_OCN._pardiscfg4._VpgDemParErrDis1()
            allFields["VpiCtlParErrDis1"] = _AF6CNC0022_RD_OCN._pardiscfg4._VpiCtlParErrDis1()
            allFields["VpiDemParErrDis1"] = _AF6CNC0022_RD_OCN._pardiscfg4._VpiDemParErrDis1()
            return allFields

    class _parerrstk4(AtRegister.AtRegister):
        def name(self):
            return "OCN Parity Error Sticky 4 - VPI+VPG+OHO"
    
        def description(self):
            return "OCN Parity Error Sticky 4 for 4 lines 4-7"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000f0032
            
        def endAddress(self):
            return 0xffffffff

        class _RxHoParErrStk1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "RxHoParErrStk1"
            
            def description(self):
                return "Error Sticky for ram config \"OCN Rx High Order Map concatenate configuration\".Each bit correspond with each LineId."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _VpgCtlParErrStk1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "VpgCtlParErrStk1"
            
            def description(self):
                return "Error Sticky for ram config \"OCN VTTU Pointer Generator Per Channel Control\". Each bit correspond with each LineId."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _VpgDemParErrStk1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "VpgDemParErrStk1"
            
            def description(self):
                return "Error Sticky for ram config \"OCN TXPP Per STS Multiplexing Control\". Each bit correspond with each LineId."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _VpiCtlParErrStk1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "VpiCtlParErrStk1"
            
            def description(self):
                return "Error Sticky for ram config \"OCN VTTU Pointer Interpreter Per Channel Control\". Each bit correspond with each LineId."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _VpiDemParErrStk1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "VpiDemParErrStk1"
            
            def description(self):
                return "Error Sticky for ram config \"OCN RXPP Per STS payload Control\". Each bit correspond with each LineId."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxHoParErrStk1"] = _AF6CNC0022_RD_OCN._parerrstk4._RxHoParErrStk1()
            allFields["VpgCtlParErrStk1"] = _AF6CNC0022_RD_OCN._parerrstk4._VpgCtlParErrStk1()
            allFields["VpgDemParErrStk1"] = _AF6CNC0022_RD_OCN._parerrstk4._VpgDemParErrStk1()
            allFields["VpiCtlParErrStk1"] = _AF6CNC0022_RD_OCN._parerrstk4._VpiCtlParErrStk1()
            allFields["VpiDemParErrStk1"] = _AF6CNC0022_RD_OCN._parerrstk4._VpiDemParErrStk1()
            return allFields

    class _parfrccfg5(AtRegister.AtRegister):
        def name(self):
            return "OCN Parity Force Config 5 - SXC selector page0+1"
    
        def description(self):
            return "OCN Parity Force Config 5"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000f0024
            
        def endAddress(self):
            return 0xffffffff

        class _SxcParErrFrc0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SxcParErrFrc0"
            
            def description(self):
                return "Force Parity for ram config \"OCN SXC Control 1 - Config Page 0 and Page 1 of SXC\". Each bit correspond with each LineId. Set 1 to force."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SxcParErrFrc0"] = _AF6CNC0022_RD_OCN._parfrccfg5._SxcParErrFrc0()
            return allFields

    class _pardiscfg5(AtRegister.AtRegister):
        def name(self):
            return "OCN Parity Disable Config 5 - SXC selector page0+1"
    
        def description(self):
            return "OCN Parity Disable Config 5"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000f0025
            
        def endAddress(self):
            return 0xffffffff

        class _SxcParErrDis0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SxcParErrDis0"
            
            def description(self):
                return "Disable Parity for ram config \"OCN SXC Control 1 - Config Page 0 and Page 1 of SXC\". Each bit correspond with each LineId. Set 1 to disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SxcParErrDis0"] = _AF6CNC0022_RD_OCN._pardiscfg5._SxcParErrDis0()
            return allFields

    class _parerrstk5(AtRegister.AtRegister):
        def name(self):
            return "OCN Parity Error Sticky 5 - SXC selector page0+1"
    
        def description(self):
            return "OCN Parity Error Sticky 5"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000f0026
            
        def endAddress(self):
            return 0xffffffff

        class _SxcParErrStk0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SxcParErrStk0"
            
            def description(self):
                return "Error Sticky for ram config \"OCN SXC Control 1 - Config Page 0 and Page 1 of SXC\". Each bit correspond with each LineId."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SxcParErrStk0"] = _AF6CNC0022_RD_OCN._parerrstk5._SxcParErrStk0()
            return allFields

    class _parfrccfg6(AtRegister.AtRegister):
        def name(self):
            return "OCN Parity Force Config 6 - SXC selector page2"
    
        def description(self):
            return "OCN Parity Force Config 6"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000f0028
            
        def endAddress(self):
            return 0xffffffff

        class _SxcParErrFrc1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SxcParErrFrc1"
            
            def description(self):
                return "Force Parity for ram config \"OCN SXC Control 2 - Config Page 2 of SXC\". Each bit correspond with each LineId. Set 1 to force."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SxcParErrFrc1"] = _AF6CNC0022_RD_OCN._parfrccfg6._SxcParErrFrc1()
            return allFields

    class _pardiscfg6(AtRegister.AtRegister):
        def name(self):
            return "OCN Parity Disable Config 6 - SXC selector page2"
    
        def description(self):
            return "OCN Parity Disable Config 6"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000f0029
            
        def endAddress(self):
            return 0xffffffff

        class _SxcParErrDis1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SxcParErrDis1"
            
            def description(self):
                return "Disable Parity for ram config \"OCN SXC Control 2 - Config Page 2 of SXC\". Each bit correspond with each LineId. Set 1 to disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SxcParErrDis1"] = _AF6CNC0022_RD_OCN._pardiscfg6._SxcParErrDis1()
            return allFields

    class _parerrstk6(AtRegister.AtRegister):
        def name(self):
            return "OCN Parity Error Sticky 6 - SXC selector page2"
    
        def description(self):
            return "OCN Parity Error Sticky 6"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000f002a
            
        def endAddress(self):
            return 0xffffffff

        class _SxcParErrStk1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SxcParErrStk1"
            
            def description(self):
                return "Error Sticky for ram config \"OCN SXC Control 2 - Config Page 2 of SXC\". Each bit correspond with each LineId."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SxcParErrStk1"] = _AF6CNC0022_RD_OCN._parerrstk6._SxcParErrStk1()
            return allFields

    class _parfrccfg7(AtRegister.AtRegister):
        def name(self):
            return "OCN Parity Force Config 7 - SXC Config APS"
    
        def description(self):
            return "OCN Parity Force Config 7"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000f002c
            
        def endAddress(self):
            return 0xffffffff

        class _SxcParErrFrc2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SxcParErrFrc2"
            
            def description(self):
                return "Force Parity for ram config \"OCN SXC Control 3 - Config APS\". Each bit correspond with each LineId. Set 1 to force."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SxcParErrFrc2"] = _AF6CNC0022_RD_OCN._parfrccfg7._SxcParErrFrc2()
            return allFields

    class _pardiscfg7(AtRegister.AtRegister):
        def name(self):
            return "OCN Parity Disable Config 7 - SXC Config APS"
    
        def description(self):
            return "OCN Parity Disable Config 7"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000f002d
            
        def endAddress(self):
            return 0xffffffff

        class _SxcParErrDis2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SxcParErrDis2"
            
            def description(self):
                return "Disable Parity for ram config \"OCN SXC Control 3 - Config APS\". Each bit correspond with each LineId. Set 1 to disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SxcParErrDis2"] = _AF6CNC0022_RD_OCN._pardiscfg7._SxcParErrDis2()
            return allFields

    class _parerrstk7(AtRegister.AtRegister):
        def name(self):
            return "OCN Parity Error Sticky 7 - SXC Config APS"
    
        def description(self):
            return "OCN Parity Error Sticky 7"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000f002e
            
        def endAddress(self):
            return 0xffffffff

        class _SxcParErrStk2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SxcParErrStk2"
            
            def description(self):
                return "Error Sticky for ram config \"OCN SXC Control 3 - Config APS\". Each bit correspond with each LineId."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SxcParErrStk2"] = _AF6CNC0022_RD_OCN._parerrstk7._SxcParErrStk2()
            return allFields

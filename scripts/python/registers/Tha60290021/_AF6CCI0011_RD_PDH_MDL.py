import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0011_RD_PDH_MDL(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["upen_mdl_buffer1"] = _AF6CCI0011_RD_PDH_MDL._upen_mdl_buffer1()
        allRegisters["upen_mdl_buffer1_2"] = _AF6CCI0011_RD_PDH_MDL._upen_mdl_buffer1_2()
        allRegisters["upen_mdl_tp1"] = _AF6CCI0011_RD_PDH_MDL._upen_mdl_tp1()
        allRegisters["upen_mdl_tp2"] = _AF6CCI0011_RD_PDH_MDL._upen_mdl_tp2()
        allRegisters["upen_sta_idle_alren"] = _AF6CCI0011_RD_PDH_MDL._upen_sta_idle_alren()
        allRegisters["upen_sta_tp_alren"] = _AF6CCI0011_RD_PDH_MDL._upen_sta_tp_alren()
        allRegisters["upen_cfg_mdl"] = _AF6CCI0011_RD_PDH_MDL._upen_cfg_mdl()
        allRegisters["upen_txmdl_cntr2c_byteidle1"] = _AF6CCI0011_RD_PDH_MDL._upen_txmdl_cntr2c_byteidle1()
        allRegisters["upen_txmdl_cntr2c_bytepath"] = _AF6CCI0011_RD_PDH_MDL._upen_txmdl_cntr2c_bytepath()
        allRegisters["upen_txmdl_cntr2c_bytetest"] = _AF6CCI0011_RD_PDH_MDL._upen_txmdl_cntr2c_bytetest()
        allRegisters["upen_txmdl_cntr2c_valididle"] = _AF6CCI0011_RD_PDH_MDL._upen_txmdl_cntr2c_valididle()
        allRegisters["upen_txmdl_cntr2c_validpath"] = _AF6CCI0011_RD_PDH_MDL._upen_txmdl_cntr2c_validpath()
        allRegisters["upen_txmdl_cntr2c_validtest"] = _AF6CCI0011_RD_PDH_MDL._upen_txmdl_cntr2c_validtest()
        allRegisters["upen_rxmdl_typebuff"] = _AF6CCI0011_RD_PDH_MDL._upen_rxmdl_typebuff()
        allRegisters["upen_rxmdl_cfgtype"] = _AF6CCI0011_RD_PDH_MDL._upen_rxmdl_cfgtype()
        allRegisters["upen_destuff_ctrl0"] = _AF6CCI0011_RD_PDH_MDL._upen_destuff_ctrl0()
        allRegisters["upen_mdl_stk_cfg1"] = _AF6CCI0011_RD_PDH_MDL._upen_mdl_stk_cfg1()
        allRegisters["upen_mdl_stk_cfg2"] = _AF6CCI0011_RD_PDH_MDL._upen_mdl_stk_cfg2()
        allRegisters["upen_mdl_stk_cfg3"] = _AF6CCI0011_RD_PDH_MDL._upen_mdl_stk_cfg3()
        allRegisters["upen_mdl_stk_cfg4"] = _AF6CCI0011_RD_PDH_MDL._upen_mdl_stk_cfg4()
        allRegisters["upen_mdl_stk_cfg5"] = _AF6CCI0011_RD_PDH_MDL._upen_mdl_stk_cfg5()
        allRegisters["upen_mdl_stk_cfg6"] = _AF6CCI0011_RD_PDH_MDL._upen_mdl_stk_cfg6()
        allRegisters["upen_rxmdl_cntr2c_byteidle"] = _AF6CCI0011_RD_PDH_MDL._upen_rxmdl_cntr2c_byteidle()
        allRegisters["upen_rxmdl_cntr2c_bytepath"] = _AF6CCI0011_RD_PDH_MDL._upen_rxmdl_cntr2c_bytepath()
        allRegisters["upen_rxmdl_cntr2c_bytetest"] = _AF6CCI0011_RD_PDH_MDL._upen_rxmdl_cntr2c_bytetest()
        allRegisters["upen_rxmdl_cntr2c_goodidle"] = _AF6CCI0011_RD_PDH_MDL._upen_rxmdl_cntr2c_goodidle()
        allRegisters["upen_rxmdl_cntr2c_goodpath"] = _AF6CCI0011_RD_PDH_MDL._upen_rxmdl_cntr2c_goodpath()
        allRegisters["upen_rxmdl_cntr2c_goodtest"] = _AF6CCI0011_RD_PDH_MDL._upen_rxmdl_cntr2c_goodtest()
        allRegisters["upen_rxmdl_cntr2c_dropidle"] = _AF6CCI0011_RD_PDH_MDL._upen_rxmdl_cntr2c_dropidle()
        allRegisters["upen_rxmdl_cntr2c_droppath"] = _AF6CCI0011_RD_PDH_MDL._upen_rxmdl_cntr2c_droppath()
        allRegisters["upen_rxmdl_cntr2c_droptest"] = _AF6CCI0011_RD_PDH_MDL._upen_rxmdl_cntr2c_droptest()
        allRegisters["mdl_cfgen_int"] = _AF6CCI0011_RD_PDH_MDL._mdl_cfgen_int()
        allRegisters["mdl_int_sta"] = _AF6CCI0011_RD_PDH_MDL._mdl_int_sta()
        allRegisters["mdl_int_crrsta"] = _AF6CCI0011_RD_PDH_MDL._mdl_int_crrsta()
        allRegisters["mdl_intsta"] = _AF6CCI0011_RD_PDH_MDL._mdl_intsta()
        allRegisters["mdl_sta_int"] = _AF6CCI0011_RD_PDH_MDL._mdl_sta_int()
        allRegisters["mdl_en_int"] = _AF6CCI0011_RD_PDH_MDL._mdl_en_int()
        return allRegisters

    class _upen_mdl_buffer1(AtRegister.AtRegister):
        def name(self):
            return "Buff Message MDL BUFFER1"
    
        def description(self):
            return "config message MDL BUFFER Channel ID 0-15"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0000+ $SLICEID*0x800 + $DWORDID*16 + $DE3ID1"
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0x0000012f

        class _idle_byte13(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "idle_byte13"
            
            def description(self):
                return "MS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _ilde_byte12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ilde_byte12"
            
            def description(self):
                return "BYTE 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _idle_byte11(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "idle_byte11"
            
            def description(self):
                return "BYTE 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _idle_byte10(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "idle_byte10"
            
            def description(self):
                return "LS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["idle_byte13"] = _AF6CCI0011_RD_PDH_MDL._upen_mdl_buffer1._idle_byte13()
            allFields["ilde_byte12"] = _AF6CCI0011_RD_PDH_MDL._upen_mdl_buffer1._ilde_byte12()
            allFields["idle_byte11"] = _AF6CCI0011_RD_PDH_MDL._upen_mdl_buffer1._idle_byte11()
            allFields["idle_byte10"] = _AF6CCI0011_RD_PDH_MDL._upen_mdl_buffer1._idle_byte10()
            return allFields

    class _upen_mdl_buffer1_2(AtRegister.AtRegister):
        def name(self):
            return "Buff Message MDL BUFFER1_2"
    
        def description(self):
            return "config message MDL BUFFER Channel ID 16-23"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0130+ $SLICEID*0x800 + $DWORDID*8 + $DE3ID2"
            
        def startAddress(self):
            return 0x00000130
            
        def endAddress(self):
            return 0x000001c7

        class _idle_byte23(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "idle_byte23"
            
            def description(self):
                return "MS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _idle_byte22(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "idle_byte22"
            
            def description(self):
                return "BYTE 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _idle_byte21(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "idle_byte21"
            
            def description(self):
                return "BYTE 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _idle_byte20(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "idle_byte20"
            
            def description(self):
                return "LS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["idle_byte23"] = _AF6CCI0011_RD_PDH_MDL._upen_mdl_buffer1_2._idle_byte23()
            allFields["idle_byte22"] = _AF6CCI0011_RD_PDH_MDL._upen_mdl_buffer1_2._idle_byte22()
            allFields["idle_byte21"] = _AF6CCI0011_RD_PDH_MDL._upen_mdl_buffer1_2._idle_byte21()
            allFields["idle_byte20"] = _AF6CCI0011_RD_PDH_MDL._upen_mdl_buffer1_2._idle_byte20()
            return allFields

    class _upen_mdl_tp1(AtRegister.AtRegister):
        def name(self):
            return "Config Buff Message MDL BUFFER2"
    
        def description(self):
            return "config message MDL BUFFER2 Channel ID 0-15"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0200+$SLICEID*0x800 + $DE3ID1 + $DWORDID*16"
            
        def startAddress(self):
            return 0x00000200
            
        def endAddress(self):
            return 0x0000032f

        class _tp_byte13(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "tp_byte13"
            
            def description(self):
                return "MS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tp_byte12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "tp_byte12"
            
            def description(self):
                return "BYTE 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tp_byte11(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "tp_byte11"
            
            def description(self):
                return "BYTE 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tp_byte10(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tp_byte10"
            
            def description(self):
                return "LS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tp_byte13"] = _AF6CCI0011_RD_PDH_MDL._upen_mdl_tp1._tp_byte13()
            allFields["tp_byte12"] = _AF6CCI0011_RD_PDH_MDL._upen_mdl_tp1._tp_byte12()
            allFields["tp_byte11"] = _AF6CCI0011_RD_PDH_MDL._upen_mdl_tp1._tp_byte11()
            allFields["tp_byte10"] = _AF6CCI0011_RD_PDH_MDL._upen_mdl_tp1._tp_byte10()
            return allFields

    class _upen_mdl_tp2(AtRegister.AtRegister):
        def name(self):
            return "Config Buff Message MDL BUFFER2_2"
    
        def description(self):
            return "config message MDL BUFFER2 Channel ID 16-23"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0330+$SLICEID*0x800 + $DWORDID*8+ $DE3ID2"
            
        def startAddress(self):
            return 0x00000330
            
        def endAddress(self):
            return 0x000003c7

        class _tp_byte23(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "tp_byte23"
            
            def description(self):
                return "MS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tp_byte22(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "tp_byte22"
            
            def description(self):
                return "BYTE 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tp_byte21(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "tp_byte21"
            
            def description(self):
                return "BYTE 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tp_byte20(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tp_byte20"
            
            def description(self):
                return "LS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tp_byte23"] = _AF6CCI0011_RD_PDH_MDL._upen_mdl_tp2._tp_byte23()
            allFields["tp_byte22"] = _AF6CCI0011_RD_PDH_MDL._upen_mdl_tp2._tp_byte22()
            allFields["tp_byte21"] = _AF6CCI0011_RD_PDH_MDL._upen_mdl_tp2._tp_byte21()
            allFields["tp_byte20"] = _AF6CCI0011_RD_PDH_MDL._upen_mdl_tp2._tp_byte20()
            return allFields

    class _upen_sta_idle_alren(AtRegister.AtRegister):
        def name(self):
            return "SET TO HAVE PACKET MDL BUFFER 1"
    
        def description(self):
            return "SET/CLEAR to ALRM STATUS MESSAGE in BUFFER 1"
            
        def width(self):
            return 1
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0420+$SLICEID*0x800 +$DE3ID"
            
        def startAddress(self):
            return 0x00000420
            
        def endAddress(self):
            return 0x00000437

        class _idle_cfgen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "idle_cfgen"
            
            def description(self):
                return "(0) : engine clear for indication to have sent, (1) CPU set for indication to have new message which must send for buffer 0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["idle_cfgen"] = _AF6CCI0011_RD_PDH_MDL._upen_sta_idle_alren._idle_cfgen()
            return allFields

    class _upen_sta_tp_alren(AtRegister.AtRegister):
        def name(self):
            return "SET TO HAVE PACKET MDL BUFFER 2"
    
        def description(self):
            return "SET/CLEAR to ALRM STATUS MESSAGE in BUFFER 2"
            
        def width(self):
            return 1
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0440+$SLICEID*0x800 +$DE3ID"
            
        def startAddress(self):
            return 0x00000440
            
        def endAddress(self):
            return 0x00000457

        class _tp_cfgen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tp_cfgen"
            
            def description(self):
                return "(0) : engine clear for indication to have sent, (1) CPU set for indication to have new message which must send for buffer 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tp_cfgen"] = _AF6CCI0011_RD_PDH_MDL._upen_sta_tp_alren._tp_cfgen()
            return allFields

    class _upen_cfg_mdl(AtRegister.AtRegister):
        def name(self):
            return "CONFIG MDL Tx"
    
        def description(self):
            return "Config Tx MDL"
            
        def width(self):
            return 6
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0460+$SLICEID*0x800 +$DE3ID"
            
        def startAddress(self):
            return 0x00000460
            
        def endAddress(self):
            return 0x00000477

        class _reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "reserve"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_seq_tx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "cfg_seq_tx"
            
            def description(self):
                return "config enable Tx continous, (0) is disable, (1) is enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_entx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "cfg_entx"
            
            def description(self):
                return "config enable Tx, (0) is disable, (1) is enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_fcs_tx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "cfg_fcs_tx"
            
            def description(self):
                return "config mode transmit FCS, (1) is T.403-MSB, (0) is T.107-LSB"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_mdlstd_tx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfg_mdlstd_tx"
            
            def description(self):
                return "config standard Tx, (0) is ANSI, (1) is AT&T"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_mdl_cr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_mdl_cr"
            
            def description(self):
                return "config bit command/respond MDL message, bit 0 is channel 0 of DS3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["reserve"] = _AF6CCI0011_RD_PDH_MDL._upen_cfg_mdl._reserve()
            allFields["cfg_seq_tx"] = _AF6CCI0011_RD_PDH_MDL._upen_cfg_mdl._cfg_seq_tx()
            allFields["cfg_entx"] = _AF6CCI0011_RD_PDH_MDL._upen_cfg_mdl._cfg_entx()
            allFields["cfg_fcs_tx"] = _AF6CCI0011_RD_PDH_MDL._upen_cfg_mdl._cfg_fcs_tx()
            allFields["cfg_mdlstd_tx"] = _AF6CCI0011_RD_PDH_MDL._upen_cfg_mdl._cfg_mdlstd_tx()
            allFields["cfg_mdl_cr"] = _AF6CCI0011_RD_PDH_MDL._upen_cfg_mdl._cfg_mdl_cr()
            return allFields

    class _upen_txmdl_cntr2c_byteidle1(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE TX MDL IDLE"
    
        def description(self):
            return "counter byte read to clear IDLE message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0700+ $SLICEID*0x800 + $DE3ID + $UPRO * 128"
            
        def startAddress(self):
            return 0x00000700
            
        def endAddress(self):
            return 0x00000717

        class _cntr2c_byte_idle_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_byte_idle_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_byte_idle_mdl"] = _AF6CCI0011_RD_PDH_MDL._upen_txmdl_cntr2c_byteidle1._cntr2c_byte_idle_mdl()
            return allFields

    class _upen_txmdl_cntr2c_bytepath(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE TX MDL PATH"
    
        def description(self):
            return "counter byte read to clear PATH message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0720+ $SLICEID*0x800 + $DE3ID + $UPRO * 128"
            
        def startAddress(self):
            return 0x00000720
            
        def endAddress(self):
            return 0x00000737

        class _cntr2c_byte_path_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_byte_path_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_byte_path_mdl"] = _AF6CCI0011_RD_PDH_MDL._upen_txmdl_cntr2c_bytepath._cntr2c_byte_path_mdl()
            return allFields

    class _upen_txmdl_cntr2c_bytetest(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE TX MDL TEST"
    
        def description(self):
            return "counter byte read to clear TEST message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0740+ $SLICEID*0x800 + $DE3ID + $UPRO * 128"
            
        def startAddress(self):
            return 0x00000740
            
        def endAddress(self):
            return 0x00000757

        class _cntr2c_byte_test_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_byte_test_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_byte_test_mdl"] = _AF6CCI0011_RD_PDH_MDL._upen_txmdl_cntr2c_bytetest._cntr2c_byte_test_mdl()
            return allFields

    class _upen_txmdl_cntr2c_valididle(AtRegister.AtRegister):
        def name(self):
            return "COUNTER VALID MESSAGE TX MDL IDLE"
    
        def description(self):
            return "counter valid read to clear IDLE message MDL"
            
        def width(self):
            return 18
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0600+ $SLICEID*0x800 + $DE3ID + $UPRO * 128"
            
        def startAddress(self):
            return 0x00000600
            
        def endAddress(self):
            return 0x00000697

        class _cntr2c_valid_idle_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_valid_idle_mdl"
            
            def description(self):
                return "value counter valid"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_valid_idle_mdl"] = _AF6CCI0011_RD_PDH_MDL._upen_txmdl_cntr2c_valididle._cntr2c_valid_idle_mdl()
            return allFields

    class _upen_txmdl_cntr2c_validpath(AtRegister.AtRegister):
        def name(self):
            return "COUNTER VALID MESSAGE TX MDL PATH"
    
        def description(self):
            return "counter valid read to clear PATH message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0620+ $SLICEID*0x800 + $DE3ID + $UPRO * 128"
            
        def startAddress(self):
            return 0x00000620
            
        def endAddress(self):
            return 0x000006b7

        class _cntr2c_valid_path_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_valid_path_mdl"
            
            def description(self):
                return "value counter valid"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_valid_path_mdl"] = _AF6CCI0011_RD_PDH_MDL._upen_txmdl_cntr2c_validpath._cntr2c_valid_path_mdl()
            return allFields

    class _upen_txmdl_cntr2c_validtest(AtRegister.AtRegister):
        def name(self):
            return "COUNTER VALID MESSAGE TX MDL TEST R2C"
    
        def description(self):
            return "counter valid read to clear TEST message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0640+ $SLICEID*0x800 + $DE3ID + $UPRO * 128"
            
        def startAddress(self):
            return 0x00000640
            
        def endAddress(self):
            return 0x000006d7

        class _cntr2c_valid_test_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_valid_test_mdl"
            
            def description(self):
                return "value counter valid"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_valid_test_mdl"] = _AF6CCI0011_RD_PDH_MDL._upen_txmdl_cntr2c_validtest._cntr2c_valid_test_mdl()
            return allFields

    class _upen_rxmdl_typebuff(AtRegister.AtRegister):
        def name(self):
            return "Buff Message MDL with configuration type"
    
        def description(self):
            return "buffer message MDL which is configured type"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x24000 + $STSID* 256 + $SLICEID*32 + $RXDWORDID"
            
        def startAddress(self):
            return 0x00024000
            
        def endAddress(self):
            return 0x00025fff

        class _mdl_tbyte3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "mdl_tbyte3"
            
            def description(self):
                return "MS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _mdl_tbyte2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "mdl_tbyte2"
            
            def description(self):
                return "BYTE 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _mdl_tbyte1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "mdl_tbyte1"
            
            def description(self):
                return "BYTE 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _mdl_tbyte0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdl_tbyte0"
            
            def description(self):
                return "LS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdl_tbyte3"] = _AF6CCI0011_RD_PDH_MDL._upen_rxmdl_typebuff._mdl_tbyte3()
            allFields["mdl_tbyte2"] = _AF6CCI0011_RD_PDH_MDL._upen_rxmdl_typebuff._mdl_tbyte2()
            allFields["mdl_tbyte1"] = _AF6CCI0011_RD_PDH_MDL._upen_rxmdl_typebuff._mdl_tbyte1()
            allFields["mdl_tbyte0"] = _AF6CCI0011_RD_PDH_MDL._upen_rxmdl_typebuff._mdl_tbyte0()
            return allFields

    class _upen_rxmdl_cfgtype(AtRegister.AtRegister):
        def name(self):
            return "Config Buff Message MDL TYPE"
    
        def description(self):
            return "config type message MDL"
            
        def width(self):
            return 10
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x23000+ $STSID*8 + $SLICEID"
            
        def startAddress(self):
            return 0x00023000
            
        def endAddress(self):
            return 0x000230bf

        class _cfg_fcs_rx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "cfg_fcs_rx"
            
            def description(self):
                return "config mode receive FCS, (1) is T.403-MSB, (0) is T.107-LSB"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_mdl_cr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "cfg_mdl_cr"
            
            def description(self):
                return "config C/R bit expected"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_mdl_std(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "cfg_mdl_std"
            
            def description(self):
                return "(0) is ANSI, (1) is AT&T"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_mdl_mask_test(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "cfg_mdl_mask_test"
            
            def description(self):
                return "config enable mask to moitor test massage (1): enable, (0): disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_mdl_mask_path(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfg_mdl_mask_path"
            
            def description(self):
                return "config enable mask to moitor path massage (1): enable, (0): disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_mdl_mask_idle(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_mdl_mask_idle"
            
            def description(self):
                return "config enable mask to moitor idle massage (1): enable, (0): disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_fcs_rx"] = _AF6CCI0011_RD_PDH_MDL._upen_rxmdl_cfgtype._cfg_fcs_rx()
            allFields["cfg_mdl_cr"] = _AF6CCI0011_RD_PDH_MDL._upen_rxmdl_cfgtype._cfg_mdl_cr()
            allFields["cfg_mdl_std"] = _AF6CCI0011_RD_PDH_MDL._upen_rxmdl_cfgtype._cfg_mdl_std()
            allFields["cfg_mdl_mask_test"] = _AF6CCI0011_RD_PDH_MDL._upen_rxmdl_cfgtype._cfg_mdl_mask_test()
            allFields["cfg_mdl_mask_path"] = _AF6CCI0011_RD_PDH_MDL._upen_rxmdl_cfgtype._cfg_mdl_mask_path()
            allFields["cfg_mdl_mask_idle"] = _AF6CCI0011_RD_PDH_MDL._upen_rxmdl_cfgtype._cfg_mdl_mask_idle()
            return allFields

    class _upen_destuff_ctrl0(AtRegister.AtRegister):
        def name(self):
            return "CONFIG CONTROL DESTUFF 0"
    
        def description(self):
            return "config control DeStuff global"
            
        def width(self):
            return 6
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00023200
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_crmon(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "cfg_crmon"
            
            def description(self):
                return "(0) : disable, (1) : enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _reserve1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "reserve1"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fcsmon(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "fcsmon"
            
            def description(self):
                return "(0) : disable monitor, (1) : enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 1
        
            def name(self):
                return "reserve"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _headermon_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "headermon_en"
            
            def description(self):
                return "(1) : enable monitor, (0) disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_crmon"] = _AF6CCI0011_RD_PDH_MDL._upen_destuff_ctrl0._cfg_crmon()
            allFields["reserve1"] = _AF6CCI0011_RD_PDH_MDL._upen_destuff_ctrl0._reserve1()
            allFields["fcsmon"] = _AF6CCI0011_RD_PDH_MDL._upen_destuff_ctrl0._fcsmon()
            allFields["reserve"] = _AF6CCI0011_RD_PDH_MDL._upen_destuff_ctrl0._reserve()
            allFields["headermon_en"] = _AF6CCI0011_RD_PDH_MDL._upen_destuff_ctrl0._headermon_en()
            return allFields

    class _upen_mdl_stk_cfg1(AtRegister.AtRegister):
        def name(self):
            return "STICKY TO RECEIVE PACKET MDL BUFF CONFIG1"
    
        def description(self):
            return "engine set to alarm message event, and CPU clear to be received new messase"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00023210
            
        def endAddress(self):
            return 0xffffffff

        class _mdl_cfg_alr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdl_cfg_alr"
            
            def description(self):
                return "sticky for new message channel 0-31"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdl_cfg_alr"] = _AF6CCI0011_RD_PDH_MDL._upen_mdl_stk_cfg1._mdl_cfg_alr()
            return allFields

    class _upen_mdl_stk_cfg2(AtRegister.AtRegister):
        def name(self):
            return "STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 2"
    
        def description(self):
            return "engine set to alarm message event, and CPU clear to be received new messase"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00023211
            
        def endAddress(self):
            return 0xffffffff

        class _mdl_cfg_alr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdl_cfg_alr"
            
            def description(self):
                return "sticky for new message channel 32 -63"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdl_cfg_alr"] = _AF6CCI0011_RD_PDH_MDL._upen_mdl_stk_cfg2._mdl_cfg_alr()
            return allFields

    class _upen_mdl_stk_cfg3(AtRegister.AtRegister):
        def name(self):
            return "STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 3"
    
        def description(self):
            return "engine set to alarm message event, and CPU clear to be received new messase"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00023212
            
        def endAddress(self):
            return 0xffffffff

        class _mdl_cfg_alr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdl_cfg_alr"
            
            def description(self):
                return "sticky for new message channel 64 -95"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdl_cfg_alr"] = _AF6CCI0011_RD_PDH_MDL._upen_mdl_stk_cfg3._mdl_cfg_alr()
            return allFields

    class _upen_mdl_stk_cfg4(AtRegister.AtRegister):
        def name(self):
            return "STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 4"
    
        def description(self):
            return "engine set to alarm message event, and CPU clear to be received new messase"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00023213
            
        def endAddress(self):
            return 0xffffffff

        class _mdl_cfg_alr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdl_cfg_alr"
            
            def description(self):
                return "sticky for new message channel 96 -127"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdl_cfg_alr"] = _AF6CCI0011_RD_PDH_MDL._upen_mdl_stk_cfg4._mdl_cfg_alr()
            return allFields

    class _upen_mdl_stk_cfg5(AtRegister.AtRegister):
        def name(self):
            return "STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 5"
    
        def description(self):
            return "engine set to alarm message event, and CPU clear to be received new messase"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00023214
            
        def endAddress(self):
            return 0xffffffff

        class _mdl_cfg_alr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdl_cfg_alr"
            
            def description(self):
                return "sticky for new message channel 128 -159"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdl_cfg_alr"] = _AF6CCI0011_RD_PDH_MDL._upen_mdl_stk_cfg5._mdl_cfg_alr()
            return allFields

    class _upen_mdl_stk_cfg6(AtRegister.AtRegister):
        def name(self):
            return "STICKY TO RECEIVE PACKET MDL BUFF CONFIG 6"
    
        def description(self):
            return "engine set to alarm message event, and CPU clear to be received new messase"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00023215
            
        def endAddress(self):
            return 0xffffffff

        class _mdl_cfg_alr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdl_cfg_alr"
            
            def description(self):
                return "sticky for new message channel 160 -191"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdl_cfg_alr"] = _AF6CCI0011_RD_PDH_MDL._upen_mdl_stk_cfg6._mdl_cfg_alr()
            return allFields

    class _upen_rxmdl_cntr2c_byteidle(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE RX MDL IDLE"
    
        def description(self):
            return "counter byte read to clear IDLE message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x26000+ $STSID*8 + $SLICEID + $UPRO * 1024"
            
        def startAddress(self):
            return 0x00026000
            
        def endAddress(self):
            return 0x000264bf

        class _cntr2c_byte_idle_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_byte_idle_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_byte_idle_mdl"] = _AF6CCI0011_RD_PDH_MDL._upen_rxmdl_cntr2c_byteidle._cntr2c_byte_idle_mdl()
            return allFields

    class _upen_rxmdl_cntr2c_bytepath(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE RX MDL PATH"
    
        def description(self):
            return "counter byte read to clear PATH message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x26100+ $STSID*8 + $SLICEID + $UPRO * 1024"
            
        def startAddress(self):
            return 0x00026100
            
        def endAddress(self):
            return 0x000265bf

        class _cntr2c_byte_path_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_byte_path_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_byte_path_mdl"] = _AF6CCI0011_RD_PDH_MDL._upen_rxmdl_cntr2c_bytepath._cntr2c_byte_path_mdl()
            return allFields

    class _upen_rxmdl_cntr2c_bytetest(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE RX MDL TEST"
    
        def description(self):
            return "counter byte read to clear TEST message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x26200+ $STSID*8 + $SLICEID + $UPRO * 1024"
            
        def startAddress(self):
            return 0x00026200
            
        def endAddress(self):
            return 0x000266bf

        class _cntr2c_byte_test_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_byte_test_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_byte_test_mdl"] = _AF6CCI0011_RD_PDH_MDL._upen_rxmdl_cntr2c_bytetest._cntr2c_byte_test_mdl()
            return allFields

    class _upen_rxmdl_cntr2c_goodidle(AtRegister.AtRegister):
        def name(self):
            return "COUNTER GOOD MESSAGE RX MDL IDLE"
    
        def description(self):
            return "counter good read to clear IDLE message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x27000+ $STSID*8 + $SLICEID + $UPRO * 2048"
            
        def startAddress(self):
            return 0x00027000
            
        def endAddress(self):
            return 0x000278bf

        class _cntr2c_good_idle_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_good_idle_mdl"
            
            def description(self):
                return "value counter good"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_good_idle_mdl"] = _AF6CCI0011_RD_PDH_MDL._upen_rxmdl_cntr2c_goodidle._cntr2c_good_idle_mdl()
            return allFields

    class _upen_rxmdl_cntr2c_goodpath(AtRegister.AtRegister):
        def name(self):
            return "COUNTER GOOD MESSAGE RX MDL PATH"
    
        def description(self):
            return "counter good read to clear PATH message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x27100+ $STSID*8 + $SLICEID + $UPRO * 2048"
            
        def startAddress(self):
            return 0x00027100
            
        def endAddress(self):
            return 0x000279bf

        class _cntr2c_good_path_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_good_path_mdl"
            
            def description(self):
                return "value counter good"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_good_path_mdl"] = _AF6CCI0011_RD_PDH_MDL._upen_rxmdl_cntr2c_goodpath._cntr2c_good_path_mdl()
            return allFields

    class _upen_rxmdl_cntr2c_goodtest(AtRegister.AtRegister):
        def name(self):
            return "COUNTER GOOD MESSAGE RX MDL TEST"
    
        def description(self):
            return "counter good read to clear TEST message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x27200+ $STSID*8 + $SLICEID + $UPRO * 2048"
            
        def startAddress(self):
            return 0x00027200
            
        def endAddress(self):
            return 0x00027abf

        class _cntr2c_good_test_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_good_test_mdl"
            
            def description(self):
                return "value counter good"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_good_test_mdl"] = _AF6CCI0011_RD_PDH_MDL._upen_rxmdl_cntr2c_goodtest._cntr2c_good_test_mdl()
            return allFields

    class _upen_rxmdl_cntr2c_dropidle(AtRegister.AtRegister):
        def name(self):
            return "COUNTER DROP MESSAGE RX MDL IDLE"
    
        def description(self):
            return "counter drop read to clear IDLE message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x27300+ $STSID*8 + $SLICEID + $UPRO * 2048"
            
        def startAddress(self):
            return 0x00027300
            
        def endAddress(self):
            return 0x00027bbf

        class _cntr2c_drop_idle_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_drop_idle_mdl"
            
            def description(self):
                return "value counter drop"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_drop_idle_mdl"] = _AF6CCI0011_RD_PDH_MDL._upen_rxmdl_cntr2c_dropidle._cntr2c_drop_idle_mdl()
            return allFields

    class _upen_rxmdl_cntr2c_droppath(AtRegister.AtRegister):
        def name(self):
            return "COUNTER DROP MESSAGE RX MDL PATH"
    
        def description(self):
            return "counter drop read to clear PATH message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x27400+ $STSID*8 + $SLICEID + $UPRO * 2048"
            
        def startAddress(self):
            return 0x00027400
            
        def endAddress(self):
            return 0x00027cbf

        class _cntr2c_drop_path_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_drop_path_mdl"
            
            def description(self):
                return "value counter drop"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_drop_path_mdl"] = _AF6CCI0011_RD_PDH_MDL._upen_rxmdl_cntr2c_droppath._cntr2c_drop_path_mdl()
            return allFields

    class _upen_rxmdl_cntr2c_droptest(AtRegister.AtRegister):
        def name(self):
            return "COUNTER DROP MESSAGE RX MDL TEST R2C"
    
        def description(self):
            return "counter drop read to clear TEST message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x27500+ $STSID*8 + $SLICEID + $UPRO * 2048"
            
        def startAddress(self):
            return 0x00027500
            
        def endAddress(self):
            return 0x00027dbf

        class _cntr2c_drop_test_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_drop_test_mdl"
            
            def description(self):
                return "value counter drop"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_drop_test_mdl"] = _AF6CCI0011_RD_PDH_MDL._upen_rxmdl_cntr2c_droptest._cntr2c_drop_test_mdl()
            return allFields

    class _mdl_cfgen_int(AtRegister.AtRegister):
        def name(self):
            return "MDL per Channel Interrupt Enable Control"
    
        def description(self):
            return "This is the per Channel interrupt enable"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x29000 +  $STSID + $SLICEID * 32"
            
        def startAddress(self):
            return 0x00029000
            
        def endAddress(self):
            return 0x000290ff

        class _mdl_cfgen_int(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdl_cfgen_int"
            
            def description(self):
                return "Set 1 to enable change event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdl_cfgen_int"] = _AF6CCI0011_RD_PDH_MDL._mdl_cfgen_int._mdl_cfgen_int()
            return allFields

    class _mdl_int_sta(AtRegister.AtRegister):
        def name(self):
            return "MDL Interrupt per Channel Interrupt Status"
    
        def description(self):
            return "This is the per Channel interrupt status."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x29100 +  $STSID + $SLICEID * 32"
            
        def startAddress(self):
            return 0x00029100
            
        def endAddress(self):
            return 0x000291ff

        class _mdlint_sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdlint_sta"
            
            def description(self):
                return "Set 1 if there is a change event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdlint_sta"] = _AF6CCI0011_RD_PDH_MDL._mdl_int_sta._mdlint_sta()
            return allFields

    class _mdl_int_crrsta(AtRegister.AtRegister):
        def name(self):
            return "MDL Interrupt per Channel Current Status"
    
        def description(self):
            return "This is the per Channel Current status."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x29200 +  $STSID + $SLICEID * 32"
            
        def startAddress(self):
            return 0x00029200
            
        def endAddress(self):
            return 0x000292ff

        class _mdlint_crrsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdlint_crrsta"
            
            def description(self):
                return "Current status of event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdlint_crrsta"] = _AF6CCI0011_RD_PDH_MDL._mdl_int_crrsta._mdlint_crrsta()
            return allFields

    class _mdl_intsta(AtRegister.AtRegister):
        def name(self):
            return "MDL Interrupt per Channel Interrupt OR Status"
    
        def description(self):
            return "The register consists of 32 bits for 32 VT/TUs of the related STS/VC in the Rx DS1/E1/J1 Framer. Each bit is used to store Interrupt OR tus of the related DS1/E1/J1."
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return "0x29300 +  $GID"
            
        def startAddress(self):
            return 0x00029300
            
        def endAddress(self):
            return 0x00029307

        class _mdlintsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdlintsta"
            
            def description(self):
                return "Set to 1 if any interrupt status bit of corresponding channel is set and its interrupt is enabled."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdlintsta"] = _AF6CCI0011_RD_PDH_MDL._mdl_intsta._mdlintsta()
            return allFields

    class _mdl_sta_int(AtRegister.AtRegister):
        def name(self):
            return "MDL Interrupt OR Status"
    
        def description(self):
            return "The register consists of 2 bits. Each bit is used to store Interrupt OR status."
            
        def width(self):
            return 8
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000293ff
            
        def endAddress(self):
            return 0xffffffff

        class _mdlsta_int(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdlsta_int"
            
            def description(self):
                return "Set to 1 if any interrupt status bit is set and its interrupt is enabled"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdlsta_int"] = _AF6CCI0011_RD_PDH_MDL._mdl_sta_int._mdlsta_int()
            return allFields

    class _mdl_en_int(AtRegister.AtRegister):
        def name(self):
            return "MDL Interrupt Enable Control"
    
        def description(self):
            return "The register consists of 2 interrupt enable bits ."
            
        def width(self):
            return 8
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000293fe
            
        def endAddress(self):
            return 0xffffffff

        class _mdlen_int(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdlen_int"
            
            def description(self):
                return "Set to 1 to enable to generate interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdlen_int"] = _AF6CCI0011_RD_PDH_MDL._mdl_en_int._mdlen_int()
            return allFields

import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0011_RD_PDH(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["glb"] = _AF6CCI0011_RD_PDH._glb()
        allRegisters["stsvt_demap_ctrl"] = _AF6CCI0011_RD_PDH._stsvt_demap_ctrl()
        allRegisters["stsvt_demap_hw_stat"] = _AF6CCI0011_RD_PDH._stsvt_demap_hw_stat()
        allRegisters["stsvt_demap_debug_prbs"] = _AF6CCI0011_RD_PDH._stsvt_demap_debug_prbs()
        allRegisters["stsvt_demap_dbg_testid_cfg"] = _AF6CCI0011_RD_PDH._stsvt_demap_dbg_testid_cfg()
        allRegisters["stsvt_demap_dbg_testrep_sticky"] = _AF6CCI0011_RD_PDH._stsvt_demap_dbg_testrep_sticky()
        allRegisters["stsvt_demap_dbg_testpat_cfg"] = _AF6CCI0011_RD_PDH._stsvt_demap_dbg_testpat_cfg()
        allRegisters["stsvt_demap_conc_ctrl"] = _AF6CCI0011_RD_PDH._stsvt_demap_conc_ctrl()
        allRegisters["stsvt_demap_cep_sts_hw_stat"] = _AF6CCI0011_RD_PDH._stsvt_demap_cep_sts_hw_stat()
        allRegisters["stsvt_demap_cep_vc4_frac_vc3_ctrl"] = _AF6CCI0011_RD_PDH._stsvt_demap_cep_vc4_frac_vc3_ctrl()
        allRegisters["dej1_rxfrm_mux_ctrl"] = _AF6CCI0011_RD_PDH._dej1_rxfrm_mux_ctrl()
        allRegisters["rx_stsvc_payload_ctrl"] = _AF6CCI0011_RD_PDH._rx_stsvc_payload_ctrl()
        allRegisters["rxm23e23_ctrl"] = _AF6CCI0011_RD_PDH._rxm23e23_ctrl()
        allRegisters["rxm23e23_hw_stat"] = _AF6CCI0011_RD_PDH._rxm23e23_hw_stat()
        allRegisters["rx_de3_oh_ctrl"] = _AF6CCI0011_RD_PDH._rx_de3_oh_ctrl()
        allRegisters["rxde3_oh_thrsh_cfg"] = _AF6CCI0011_RD_PDH._rxde3_oh_thrsh_cfg()
        allRegisters["rxm23e23_rei_thrsh_cfg"] = _AF6CCI0011_RD_PDH._rxm23e23_rei_thrsh_cfg()
        allRegisters["rxm23e23_fbe_thrsh_cfg"] = _AF6CCI0011_RD_PDH._rxm23e23_fbe_thrsh_cfg()
        allRegisters["rxm23e23_parity_thrsh_cfg"] = _AF6CCI0011_RD_PDH._rxm23e23_parity_thrsh_cfg()
        allRegisters["rxm23e23_cbit_parity_thrsh_cfg"] = _AF6CCI0011_RD_PDH._rxm23e23_cbit_parity_thrsh_cfg()
        allRegisters["rxm23e23_rei_cnt"] = _AF6CCI0011_RD_PDH._rxm23e23_rei_cnt()
        allRegisters["rxm23e23_fbe_cnt"] = _AF6CCI0011_RD_PDH._rxm23e23_fbe_cnt()
        allRegisters["rxm23e23_parity_cnt"] = _AF6CCI0011_RD_PDH._rxm23e23_parity_cnt()
        allRegisters["rxm23e23_cbit_parity_cnt"] = _AF6CCI0011_RD_PDH._rxm23e23_cbit_parity_cnt()
        allRegisters["rx_de3_feac_buffer"] = _AF6CCI0011_RD_PDH._rx_de3_feac_buffer()
        allRegisters["rxm23e23_chn_intr_cfg"] = _AF6CCI0011_RD_PDH._rxm23e23_chn_intr_cfg()
        allRegisters["rxm23e23_chn_intr_stat"] = _AF6CCI0011_RD_PDH._rxm23e23_chn_intr_stat()
        allRegisters["rxm23e23_per_chn_intr_cfg"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_intr_cfg()
        allRegisters["rxm23e23_per_chn_intr_stat"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_intr_stat()
        allRegisters["rxm23e23_per_chn_curr_stat"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_curr_stat()
        allRegisters["rxde3_oh_dlk_swap_bit_cfg"] = _AF6CCI0011_RD_PDH._rxde3_oh_dlk_swap_bit_cfg()
        allRegisters["de3_testpat_mon_glb_cfg"] = _AF6CCI0011_RD_PDH._de3_testpat_mon_glb_cfg()
        allRegisters["de3_testpat_mon_sticky_en"] = _AF6CCI0011_RD_PDH._de3_testpat_mon_sticky_en()
        allRegisters["de3_testpat_mon_err_cnt"] = _AF6CCI0011_RD_PDH._de3_testpat_mon_err_cnt()
        allRegisters["de3_testpat_mon_goodbit_cnt"] = _AF6CCI0011_RD_PDH._de3_testpat_mon_goodbit_cnt()
        allRegisters["de3_testpat_mon_lostbit_cnt"] = _AF6CCI0011_RD_PDH._de3_testpat_mon_lostbit_cnt()
        allRegisters["de3_testpat_mon_cnt_cfg"] = _AF6CCI0011_RD_PDH._de3_testpat_mon_cnt_cfg()
        allRegisters["de3_testpat_mon_errbit_cnt"] = _AF6CCI0011_RD_PDH._de3_testpat_mon_errbit_cnt()
        allRegisters["de3_testpat_mon_goodbit_cnt_loading"] = _AF6CCI0011_RD_PDH._de3_testpat_mon_goodbit_cnt_loading()
        allRegisters["de3_testpat_mon_lostbit_cnt_loading"] = _AF6CCI0011_RD_PDH._de3_testpat_mon_lostbit_cnt_loading()
        allRegisters["rxm12e12_ctrl"] = _AF6CCI0011_RD_PDH._rxm12e12_ctrl()
        allRegisters["rxm12e12_stsvc_payload_ctrl"] = _AF6CCI0011_RD_PDH._rxm12e12_stsvc_payload_ctrl()
        allRegisters["rxm12e12_hw_stat"] = _AF6CCI0011_RD_PDH._rxm12e12_hw_stat()
        allRegisters["RxM12E12_chn_intr_cfg"] = _AF6CCI0011_RD_PDH._RxM12E12_chn_intr_cfg()
        allRegisters["RxM12E12_chn_intr_stat"] = _AF6CCI0011_RD_PDH._RxM12E12_chn_intr_stat()
        allRegisters["RxM12E12_per_chn_intr_cfg"] = _AF6CCI0011_RD_PDH._RxM12E12_per_chn_intr_cfg()
        allRegisters["RxM12E12_per_chn_intr_stat"] = _AF6CCI0011_RD_PDH._RxM12E12_per_chn_intr_stat()
        allRegisters["RxM12E12_per_chn_curr_stat"] = _AF6CCI0011_RD_PDH._RxM12E12_per_chn_curr_stat()
        allRegisters["RxM12E12_glb_cfg"] = _AF6CCI0011_RD_PDH._RxM12E12_glb_cfg()
        allRegisters["dej1_fbe_thrsh_cfg"] = _AF6CCI0011_RD_PDH._dej1_fbe_thrsh_cfg()
        allRegisters["dej1_rx_crc_thrsh_cfg"] = _AF6CCI0011_RD_PDH._dej1_rx_crc_thrsh_cfg()
        allRegisters["dej1_rei_thrsh_cfg"] = _AF6CCI0011_RD_PDH._dej1_rei_thrsh_cfg()
        allRegisters["dej1_rx_framer_ctrl"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_ctrl()
        allRegisters["dej1_rx_framer_hw_stat"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_hw_stat()
        allRegisters["dej1_rx_framer_crcerr_cnt"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_crcerr_cnt()
        allRegisters["dej1_rx_framer_rei_cnt"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_rei_cnt()
        allRegisters["dej1_rx_framer_fbe_cnt"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_fbe_cnt()
        allRegisters["dej1_rx_framer_per_chn_intr_en_ctrl"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_intr_en_ctrl()
        allRegisters["dej1_rx_framer_per_chn_intr_stat"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_intr_stat()
        allRegisters["dej1_rx_framer_per_chn_curr_stat"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_curr_stat()
        allRegisters["dej1_rx_framer_per_chn_intr_or_stat"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_intr_or_stat()
        allRegisters["dej1_rx_framer_per_stsvc_intr_or_stat"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_stsvc_intr_or_stat()
        allRegisters["dej1_rx_framer_per_stsvc_intr_en_ctrl"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_stsvc_intr_en_ctrl()
        allRegisters["dej1_rx_framer_dlk_ctrl"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_ctrl()
        allRegisters["dej1_rx_framer_dlk_stat"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_stat()
        allRegisters["dej1_rx_framer_dlk_flush_msg_ctrl"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_flush_msg_ctrl()
        allRegisters["dej1_framer_clk_acc_ddr_ctrl"] = _AF6CCI0011_RD_PDH._dej1_framer_clk_acc_ddr_ctrl()
        allRegisters["dej1_rx_framer_clk_msg0"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_clk_msg0()
        allRegisters["dej1_rx_framer_msg1"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_msg1()
        allRegisters["dej1_rx_framer_dkl_msg2"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dkl_msg2()
        allRegisters["dej1_rx_framer_dlk_msg3"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_msg3()
        allRegisters["dej1_rx_framer_dlk_intr_en_ctrl"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_intr_en_ctrl()
        allRegisters["dej1_rx_framer_dlk_intr_sticky"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_intr_sticky()
        allRegisters["dej1_testpat_mon_glb_ctrl"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_glb_ctrl()
        allRegisters["dej1_testpat_mon_sticky_en"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_sticky_en()
        allRegisters["dej1_testpat_mon_sticky"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_sticky()
        allRegisters["dej1_testpat_mon_mode"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_mode()
        allRegisters["dej1_testpat_mon_fixed_pattern_cfg"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_fixed_pattern_cfg()
        allRegisters["dej1_testpat_mon_sel_chn_cfg"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_sel_chn_cfg()
        allRegisters["dej1_testpat_mon_nxds0_ctrl"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_nxds0_ctrl()
        allRegisters["dej1_testpat_mon_nxds0_conc_ctrl"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_nxds0_conc_ctrl()
        allRegisters["dej1_testpat_mon_err_cnt"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_err_cnt()
        allRegisters["dej1_testpat_mon_goodbit_cnt"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_goodbit_cnt()
        allRegisters["dej1_testpat_mon_lossbit_cnt"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_lossbit_cnt()
        allRegisters["dej1_testpat_mon_cntid_cfg"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_cntid_cfg()
        allRegisters["dej1_testpat_mon_errbit_cnt"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_errbit_cnt()
        allRegisters["dej1_testpat_mon_goodbit_cnt_loading"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_goodbit_cnt_loading()
        allRegisters["dej1_testpat_mon_lostbit_cnt"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_lostbit_cnt()
        allRegisters["dej1_testpat_mon_timing_stat"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_timing_stat()
        allRegisters["dej1_testpat_mon_stat"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_stat()
        allRegisters["spevt_map_fflen_watermask"] = _AF6CCI0011_RD_PDH._spevt_map_fflen_watermask()
        allRegisters["stsvt_map_ctrl"] = _AF6CCI0011_RD_PDH._stsvt_map_ctrl()
        allRegisters["vt_async_map_ctrl"] = _AF6CCI0011_RD_PDH._vt_async_map_ctrl()
        allRegisters["stsvt_map_ff_center_sticky"] = _AF6CCI0011_RD_PDH._stsvt_map_ff_center_sticky()
        allRegisters["stsvt_map_hw_stat"] = _AF6CCI0011_RD_PDH._stsvt_map_hw_stat()
        allRegisters["ts_vt_map_jitter_cfg"] = _AF6CCI0011_RD_PDH._ts_vt_map_jitter_cfg()
        allRegisters["dej1_tx_framer_ctrl"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_ctrl()
        allRegisters["dej1_tx_framer_stat"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_stat()
        allRegisters["dej1_tx_framer_sign_insertion_ctrl"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_sign_insertion_ctrl()
        allRegisters["dej1_tx_framer_sign_id_conv_ctrl"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_sign_id_conv_ctrl()
        allRegisters["dej1_tx_framer_sign_cpuins_en_ctrl"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_sign_cpuins_en_ctrl()
        allRegisters["dej1_tx_framer_sign_cpu_de1_wr_ctrl"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_sign_cpu_de1_wr_ctrl()
        allRegisters["dej1_tx_framer_sign_buffer"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_sign_buffer()
        allRegisters["dej1_testpat_gen_ctrl"] = _AF6CCI0011_RD_PDH._dej1_testpat_gen_ctrl()
        allRegisters["dej1_testpat_gen_single_biterr_ins"] = _AF6CCI0011_RD_PDH._dej1_testpat_gen_single_biterr_ins()
        allRegisters["dej1_testpat_gen_mode"] = _AF6CCI0011_RD_PDH._dej1_testpat_gen_mode()
        allRegisters["dej1_testpat_gen_sel_chn"] = _AF6CCI0011_RD_PDH._dej1_testpat_gen_sel_chn()
        allRegisters["dej1_testpat_gen_nxds0_ctrl"] = _AF6CCI0011_RD_PDH._dej1_testpat_gen_nxds0_ctrl()
        allRegisters["dej1_testpat_gen_nxds0_conc_ctrl"] = _AF6CCI0011_RD_PDH._dej1_testpat_gen_nxds0_conc_ctrl()
        allRegisters["dej1_testpat_gen_fixedpat_ctrl"] = _AF6CCI0011_RD_PDH._dej1_testpat_gen_fixedpat_ctrl()
        allRegisters["dej1_testpat_gen_errrate_inst"] = _AF6CCI0011_RD_PDH._dej1_testpat_gen_errrate_inst()
        allRegisters["dej1_testpat_gen_timing_stat"] = _AF6CCI0011_RD_PDH._dej1_testpat_gen_timing_stat()
        allRegisters["dej1_testpat_gen_stat"] = _AF6CCI0011_RD_PDH._dej1_testpat_gen_stat()
        allRegisters["dej1_tx_framer_dlk_indir_acc_ctrl"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_dlk_indir_acc_ctrl()
        allRegisters["dej1_tx_framer_dlk_indir_acc_wr_data_ctrl"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_dlk_indir_acc_wr_data_ctrl()
        allRegisters["dej1_tx_framer_dlk_indir_acc_rd_data_stat"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_dlk_indir_acc_rd_data_stat()
        allRegisters["dej1_tx_framer_dlk_ins_ctrl"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_dlk_ins_ctrl()
        allRegisters["dej1_tx_framer_dlk_ins_en"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_dlk_ins_en()
        allRegisters["dej1_tx_framer_dlk_ins_newmsg_start_en"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_dlk_ins_newmsg_start_en()
        allRegisters["dej1_tx_framer_dlk_ins_eng_stat"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_dlk_ins_eng_stat()
        allRegisters["dej1_tx_framer_dlk_ins_msg_mem"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_dlk_ins_msg_mem()
        allRegisters["dej1_tx_framer_dlk_ins_bom_ctrl"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_dlk_ins_bom_ctrl()
        allRegisters["txm23e23_ctrl"] = _AF6CCI0011_RD_PDH._txm23e23_ctrl()
        allRegisters["txm23e23_hw_stat"] = _AF6CCI0011_RD_PDH._txm23e23_hw_stat()
        allRegisters["txm23e23_e3g832_tracebyte"] = _AF6CCI0011_RD_PDH._txm23e23_e3g832_tracebyte()
        allRegisters["de3_testpat_gen_glb_ctrl"] = _AF6CCI0011_RD_PDH._de3_testpat_gen_glb_ctrl()
        allRegisters["txm12e12_ctrl"] = _AF6CCI0011_RD_PDH._txm12e12_ctrl()
        allRegisters["txm12e12_hw_stat"] = _AF6CCI0011_RD_PDH._txm12e12_hw_stat()
        allRegisters["txm12e12_jitter_cfg"] = _AF6CCI0011_RD_PDH._txm12e12_jitter_cfg()
        allRegisters["upen_test1"] = _AF6CCI0011_RD_PDH._upen_test1()
        allRegisters["upen_test2"] = _AF6CCI0011_RD_PDH._upen_test2()
        allRegisters["upen_lim_err"] = _AF6CCI0011_RD_PDH._upen_lim_err()
        allRegisters["upen_lim_mat"] = _AF6CCI0011_RD_PDH._upen_lim_mat()
        allRegisters["upen_th_fdl_mat"] = _AF6CCI0011_RD_PDH._upen_th_fdl_mat()
        allRegisters["upen_th_err"] = _AF6CCI0011_RD_PDH._upen_th_err()
        allRegisters["upen_stk"] = _AF6CCI0011_RD_PDH._upen_stk()
        allRegisters["upen_len"] = _AF6CCI0011_RD_PDH._upen_len()
        allRegisters["upen_info"] = _AF6CCI0011_RD_PDH._upen_info()
        allRegisters["upen_fdl_stk"] = _AF6CCI0011_RD_PDH._upen_fdl_stk()
        allRegisters["upen_fdlpat"] = _AF6CCI0011_RD_PDH._upen_fdlpat()
        allRegisters["upen_len_mess"] = _AF6CCI0011_RD_PDH._upen_len_mess()
        allRegisters["upen_fdl_info"] = _AF6CCI0011_RD_PDH._upen_fdl_info()
        allRegisters["upen_th_stt_int"] = _AF6CCI0011_RD_PDH._upen_th_stt_int()
        allRegisters["upen_ibstk_rs"] = _AF6CCI0011_RD_PDH._upen_ibstk_rs()
        allRegisters["upen_ibstk_fl"] = _AF6CCI0011_RD_PDH._upen_ibstk_fl()
        allRegisters["upen_obstk_rs"] = _AF6CCI0011_RD_PDH._upen_obstk_rs()
        allRegisters["upen_obstk_fl"] = _AF6CCI0011_RD_PDH._upen_obstk_fl()
        allRegisters["RAM_Parity_Force_Control"] = _AF6CCI0011_RD_PDH._RAM_Parity_Force_Control()
        allRegisters["RAM_Parity_Disable_Control"] = _AF6CCI0011_RD_PDH._RAM_Parity_Disable_Control()
        allRegisters["RAM_Parity_Error_Sticky"] = _AF6CCI0011_RD_PDH._RAM_Parity_Error_Sticky()
        allRegisters["dej1_errins_en_cfg"] = _AF6CCI0011_RD_PDH._dej1_errins_en_cfg()
        allRegisters["dej1_errins_thr_cfg"] = _AF6CCI0011_RD_PDH._dej1_errins_thr_cfg()
        allRegisters["dej1_rx_errins_en_cfg"] = _AF6CCI0011_RD_PDH._dej1_rx_errins_en_cfg()
        allRegisters["dej1_rx_errins_thr_cfg"] = _AF6CCI0011_RD_PDH._dej1_rx_errins_thr_cfg()
        allRegisters["de3_errins_en_cfg"] = _AF6CCI0011_RD_PDH._de3_errins_en_cfg()
        allRegisters["de3_errins_thr_cfg"] = _AF6CCI0011_RD_PDH._de3_errins_thr_cfg()
        allRegisters["de3_rx_errins_en_cfg"] = _AF6CCI0011_RD_PDH._de3_rx_errins_en_cfg()
        allRegisters["de3_rx_errins_thr_cfg"] = _AF6CCI0011_RD_PDH._de3_rx_errins_thr_cfg()
        return allRegisters

    class _glb(AtRegister.AtRegister):
        def name(self):
            return "PDH Global Registers"
    
        def description(self):
            return "This is the global configuration register for the PDH"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0x00000000

        class _PDHDE3MaskLOF4OUT8K(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PDHDE3MaskLOF4OUT8K"
            
            def description(self):
                return "Set 1 to ignore DE3 LOF to mask out8k"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHDE3MaskDS3AISSignal4OUT8K(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "PDHDE3MaskDS3AISSignal4OUT8K"
            
            def description(self):
                return "Set 1 to ignore DS3 AIS signal to mask out8k"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHDE3MaskLOSAIS4OUT8K(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "PDHDE3MaskLOSAIS4OUT8K"
            
            def description(self):
                return "Set 1 to ignore DS3 LOS all0s and AIS all1s to mask out8k"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHE13IdConv(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "PDHE13IdConv"
            
            def description(self):
                return "Set 1 to enable ID conversion of the E1 in an E3 as following ID in an E3 (bit 4:0) ID at Data Map (bit 4:0) 5'd0                  5'd0 5'd1                  5'd1 5'd2                  5'd2 5'd3                  5'd4 5'd4                  5'd5 5'd5                  5'd6 5'd6                  5'd8 5'd7                  5'd9 5'd8                  5'd10 5'd9                  5'd12 5'd10                 5'd13 5'd11                 5'd14 5'd12                 5'd16 5'd13                 5'd17 5'd14                 5'd18 5'd15                 5'd20"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHFullSonetSDH(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PDHFullSonetSDH"
            
            def description(self):
                return "Set 1 to enable full SONET/SDH mode (no traffic on PDH interface)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHFullLineLoop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "PDHFullLineLoop"
            
            def description(self):
                return "Set 1 to enable full Line Loop Back at Data map side"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHFullPayLoop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PDHFullPayLoop"
            
            def description(self):
                return "Set 1 to enable full Payload Loop back at Data map side"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHFullPDHBus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PDHFullPDHBus"
            
            def description(self):
                return "Set 1 to enable Full traffic on PDH Bus"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHParallelPDHMd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PDHParallelPDHMd"
            
            def description(self):
                return "Set 1 to enable Parallel PDH Bus mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHSaturation(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "PDHSaturation"
            
            def description(self):
                return "Set 1 to enable Saturation mode of all PDH counters. Set 0 for Roll-Over mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHReservedBit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PDHReservedBit"
            
            def description(self):
                return "Transmit Reserved Bit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHStufftBit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PDHStufftBit"
            
            def description(self):
                return "Transmit Stuffing Bit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHNoIDConv(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDHNoIDConv"
            
            def description(self):
                return "Set 1 to enable no ID convert at PDH Block"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDHDE3MaskLOF4OUT8K"] = _AF6CCI0011_RD_PDH._glb._PDHDE3MaskLOF4OUT8K()
            allFields["PDHDE3MaskDS3AISSignal4OUT8K"] = _AF6CCI0011_RD_PDH._glb._PDHDE3MaskDS3AISSignal4OUT8K()
            allFields["PDHDE3MaskLOSAIS4OUT8K"] = _AF6CCI0011_RD_PDH._glb._PDHDE3MaskLOSAIS4OUT8K()
            allFields["PDHE13IdConv"] = _AF6CCI0011_RD_PDH._glb._PDHE13IdConv()
            allFields["PDHFullSonetSDH"] = _AF6CCI0011_RD_PDH._glb._PDHFullSonetSDH()
            allFields["PDHFullLineLoop"] = _AF6CCI0011_RD_PDH._glb._PDHFullLineLoop()
            allFields["PDHFullPayLoop"] = _AF6CCI0011_RD_PDH._glb._PDHFullPayLoop()
            allFields["PDHFullPDHBus"] = _AF6CCI0011_RD_PDH._glb._PDHFullPDHBus()
            allFields["PDHParallelPDHMd"] = _AF6CCI0011_RD_PDH._glb._PDHParallelPDHMd()
            allFields["PDHSaturation"] = _AF6CCI0011_RD_PDH._glb._PDHSaturation()
            allFields["PDHReservedBit"] = _AF6CCI0011_RD_PDH._glb._PDHReservedBit()
            allFields["PDHStufftBit"] = _AF6CCI0011_RD_PDH._glb._PDHStufftBit()
            allFields["PDHNoIDConv"] = _AF6CCI0011_RD_PDH._glb._PDHNoIDConv()
            return allFields

    class _stsvt_demap_ctrl(AtRegister.AtRegister):
        def name(self):
            return "STS/VT Demap Control"
    
        def description(self):
            return "The STS/VT Demap Control is use to configure for per channel STS/VT demap operation."
            
        def width(self):
            return 9
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00024000 +  32*stsid + 4*vtgid + vtid"
            
        def startAddress(self):
            return 0x00024000
            
        def endAddress(self):
            return 0x000243ff

        class _StsVtDemapFrcAis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "StsVtDemapFrcAis"
            
            def description(self):
                return "This bit is set to 1 to force the AIS to downstream data. 1: force AIS. 0: not force AIS."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsVtDemapAutoAis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "StsVtDemapAutoAis"
            
            def description(self):
                return "This bit is set to 1 to automatically generate AIS to downstream data whenever the high-order STS/VT is in AIS. 1: auto AIS generation. 0: not auto AIS generation"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsVtDemapCepMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "StsVtDemapCepMode"
            
            def description(self):
                return "STS/VC CEP fractional mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsVtDemapFrmtype(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "StsVtDemapFrmtype"
            
            def description(self):
                return "STS/VT Frmtype configure"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsVtDemapSigtype(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "StsVtDemapSigtype"
            
            def description(self):
                return "STS/VT Sigtype configure Sigtype[3:0] Frmtype[1:0] CepMode Operation Mode 0 x x VT/TU to DS1 Demap 1 x x VT/TU to E1 Demap 2 x 0 STS1/VC3 to DS3 Demap 2 x 1 TUG3/TU3 to DS3 Demap 3 x 0 STS1/VC3 to E3 Demap 3 x 1 TUG3/TU3 to E3 Demap 4 0 x Packet over VT1.5/TU11 4 1 x Reserved 4 2 x Reserved 4 3 x VT1.5/TU11 Basic CEP 5 0 x Packet over VT2/TU12 5 1 x Reserved 5 2 x Reserved 5 3 x VT2/TU12 Basic CEP 6 x x Reserved 7 x x Reserved 8 0 x Packet over STS1/VC3 8 1 0 STS1/VC3 Fractional CEP carrying VT2/TU12 8 1 1 STS1/VC3 Fractional CEP carrying VT15/TU11 8 2 0 STS1/VC3 Fractional CEP carrying E3 8 2 1 STS1/VC3 Fractional CEP carrying DS3 8 3 x STS1/VC3 Basic CEP 9 0 x Packet over STS3c/VC4 9 1 0 STS3c/VC4 Fractional CEP carrying VT2/TU12 9 1 1 STS3c/VC4 Fractional CEP carrying VT15/TU11 9 2 0 STS3c/VC4 Fractional CEP carrying E3 9 2 1 STS3c/VC4 Fractional CEP carrying DS3 9 3 x STS3c/VC4 Basic CEP 10 3 x STS12c/VC4_4c Basic CEP 11 x x Reserved 12 x x Packet over TU3 13 x x Reserved 14 x x Reserved 15 x x Disable STS/VC/VT/TU/DS1/E1/DS3/E3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["StsVtDemapFrcAis"] = _AF6CCI0011_RD_PDH._stsvt_demap_ctrl._StsVtDemapFrcAis()
            allFields["StsVtDemapAutoAis"] = _AF6CCI0011_RD_PDH._stsvt_demap_ctrl._StsVtDemapAutoAis()
            allFields["StsVtDemapCepMode"] = _AF6CCI0011_RD_PDH._stsvt_demap_ctrl._StsVtDemapCepMode()
            allFields["StsVtDemapFrmtype"] = _AF6CCI0011_RD_PDH._stsvt_demap_ctrl._StsVtDemapFrmtype()
            allFields["StsVtDemapSigtype"] = _AF6CCI0011_RD_PDH._stsvt_demap_ctrl._StsVtDemapSigtype()
            return allFields

    class _stsvt_demap_hw_stat(AtRegister.AtRegister):
        def name(self):
            return "STS/VT Demap HW Status"
    
        def description(self):
            return "for HW debug only"
            
        def width(self):
            return 27
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00024400
            
        def endAddress(self):
            return 0x000247ff

        class _STSVTDemapStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 0
        
            def name(self):
                return "STSVTDemapStatus"
            
            def description(self):
                return "for HW debug only"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["STSVTDemapStatus"] = _AF6CCI0011_RD_PDH._stsvt_demap_hw_stat._STSVTDemapStatus()
            return allFields

    class _stsvt_demap_debug_prbs(AtRegister.AtRegister):
        def name(self):
            return "STS/VT Demap Output Debug PRBS"
    
        def description(self):
            return "for HW debug only"
            
        def width(self):
            return 10
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00020110
            
        def endAddress(self):
            return 0x00020117

        class _OutputDebugPRBS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "OutputDebugPRBS"
            
            def description(self):
                return "for HW debug only"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["OutputDebugPRBS"] = _AF6CCI0011_RD_PDH._stsvt_demap_debug_prbs._OutputDebugPRBS()
            return allFields

    class _stsvt_demap_dbg_testid_cfg(AtRegister.AtRegister):
        def name(self):
            return "STS/VT Demap Output Debug Test ID Configuration"
    
        def description(self):
            return "for HW debug only"
            
        def width(self):
            return 10
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00020000
            
        def endAddress(self):
            return 0x00020000

        class _TestIDCfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TestIDCfg"
            
            def description(self):
                return "for HW debug only"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TestIDCfg"] = _AF6CCI0011_RD_PDH._stsvt_demap_dbg_testid_cfg._TestIDCfg()
            return allFields

    class _stsvt_demap_dbg_testrep_sticky(AtRegister.AtRegister):
        def name(self):
            return "STS/VT Demap Output Debug Test Report Sticky"
    
        def description(self):
            return "for HW debug only"
            
        def width(self):
            return 1
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00020001
            
        def endAddress(self):
            return 0x00020001

        class _TestRptStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TestRptStk"
            
            def description(self):
                return "for HW debug only"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TestRptStk"] = _AF6CCI0011_RD_PDH._stsvt_demap_dbg_testrep_sticky._TestRptStk()
            return allFields

    class _stsvt_demap_dbg_testpat_cfg(AtRegister.AtRegister):
        def name(self):
            return "STS/VT Demap Output Debug Test Pattern Configuration"
    
        def description(self):
            return "for HW debug only"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00020002
            
        def endAddress(self):
            return 0x00020002

        class _TestPatternCfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TestPatternCfg"
            
            def description(self):
                return "for HW debug only"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TestPatternCfg"] = _AF6CCI0011_RD_PDH._stsvt_demap_dbg_testpat_cfg._TestPatternCfg()
            return allFields

    class _stsvt_demap_conc_ctrl(AtRegister.AtRegister):
        def name(self):
            return "STS/VT Demap Concatenation Control"
    
        def description(self):
            return "The STS/VT Demap Concatenation Control is use to configure for per STS1/VC3 Concatenation operation."
            
        def width(self):
            return 6
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00025000 +  stsid"
            
        def startAddress(self):
            return 0x00025000
            
        def endAddress(self):
            return 0x0002501f

        class _StsVtDemapCcatEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "StsVtDemapCcatEn"
            
            def description(self):
                return "This bit is set to 1 to configure the STS to Concatenation mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsVTDemapCcatMstID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "StsVTDemapCcatMstID"
            
            def description(self):
                return "Master ID of the concatenated STS1/VC3. Write the same STS1/VC3 ID to indicate that STS1/VC3 is the master STS1/VC3 of a concatenated group"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["StsVtDemapCcatEn"] = _AF6CCI0011_RD_PDH._stsvt_demap_conc_ctrl._StsVtDemapCcatEn()
            allFields["StsVTDemapCcatMstID"] = _AF6CCI0011_RD_PDH._stsvt_demap_conc_ctrl._StsVTDemapCcatMstID()
            return allFields

    class _stsvt_demap_cep_sts_hw_stat(AtRegister.AtRegister):
        def name(self):
            return "STS/VT Demap CEP STS HW Status"
    
        def description(self):
            return "for HW debug only"
            
        def width(self):
            return 127
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00025080
            
        def endAddress(self):
            return 0x0002509f

        class _EBMVtg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 118
                
            def startBit(self):
                return 105
        
            def name(self):
                return "EBMVtg"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _EBMBit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 104
                
            def startBit(self):
                return 77
        
            def name(self):
                return "EBMBit"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _STSPos(AtRegister.AtRegisterField):
            def stopBit(self):
                return 76
                
            def startBit(self):
                return 49
        
            def name(self):
                return "STSPos"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _STSNeg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 48
                
            def startBit(self):
                return 21
        
            def name(self):
                return "STSNeg"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _B3Calc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 13
        
            def name(self):
                return "B3Calc"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _B3Lat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 5
        
            def name(self):
                return "B3Lat"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtgID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 2
        
            def name(self):
                return "VtgID"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "VtID"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["EBMVtg"] = _AF6CCI0011_RD_PDH._stsvt_demap_cep_sts_hw_stat._EBMVtg()
            allFields["EBMBit"] = _AF6CCI0011_RD_PDH._stsvt_demap_cep_sts_hw_stat._EBMBit()
            allFields["STSPos"] = _AF6CCI0011_RD_PDH._stsvt_demap_cep_sts_hw_stat._STSPos()
            allFields["STSNeg"] = _AF6CCI0011_RD_PDH._stsvt_demap_cep_sts_hw_stat._STSNeg()
            allFields["B3Calc"] = _AF6CCI0011_RD_PDH._stsvt_demap_cep_sts_hw_stat._B3Calc()
            allFields["B3Lat"] = _AF6CCI0011_RD_PDH._stsvt_demap_cep_sts_hw_stat._B3Lat()
            allFields["VtgID"] = _AF6CCI0011_RD_PDH._stsvt_demap_cep_sts_hw_stat._VtgID()
            allFields["VtID"] = _AF6CCI0011_RD_PDH._stsvt_demap_cep_sts_hw_stat._VtID()
            return allFields

    class _stsvt_demap_cep_vc4_frac_vc3_ctrl(AtRegister.AtRegister):
        def name(self):
            return "STS/VT Demap CEP VC4 fractional VC3 Control"
    
        def description(self):
            return "The STS/VT Demap CEP VC4 fractional VC3 Control is use to configure for per STS1/VC3 Concatenation operation."
            
        def width(self):
            return 24
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000251ff
            
        def endAddress(self):
            return 0xffffffff

        class _VC4FracVC3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "VC4FracVC3"
            
            def description(self):
                return "Each bit for each STS. Set 1 is VC4 fractional VC3 mode. Set 0 for other mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["VC4FracVC3"] = _AF6CCI0011_RD_PDH._stsvt_demap_cep_vc4_frac_vc3_ctrl._VC4FracVC3()
            return allFields

    class _dej1_rxfrm_mux_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer Mux Control"
    
        def description(self):
            return "These registers are used to configure for the source data of Ds1/E1 which may be get from Serial PDH interface or from SONET/SDH."
            
        def width(self):
            return 28
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00030000 +  stsid"
            
        def startAddress(self):
            return 0x00030000
            
        def endAddress(self):
            return 0x0003001f

        class _PDHRxMuxSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDHRxMuxSel"
            
            def description(self):
                return "28 selector bit for 28 DS1/E1/J1 in a STS/VC 1: source selected from PDH Interface 0 (default): source selected from SONET/SDH Interface"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDHRxMuxSel"] = _AF6CCI0011_RD_PDH._dej1_rxfrm_mux_ctrl._PDHRxMuxSel()
            return allFields

    class _rx_stsvc_payload_ctrl(AtRegister.AtRegister):
        def name(self):
            return "PDH Rx Per STS/VC Payload Control"
    
        def description(self):
            return "The STS configuration registers are used to configure kinds of STS-1s and the kinds of VT/TUs in each STS-1."
            
        def width(self):
            return 14
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00030020 +  stsid"
            
        def startAddress(self):
            return 0x00030020
            
        def endAddress(self):
            return 0x0003003f

        class _PDHVtRxMapTug26Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PDHVtRxMapTug26Type"
            
            def description(self):
                return "Define types of VT/TUs in TUG-2-6. 0: TU11/VT1.5 type 1: TU12/VT2 type 2: reserved 3: VC/STS/DS3/E3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHVtRxMapTug25Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "PDHVtRxMapTug25Type"
            
            def description(self):
                return "Define types of VT/TUs in TUG-2-5"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHVtRxMapTug24Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PDHVtRxMapTug24Type"
            
            def description(self):
                return "Define types of VT/TUs in TUG-2-4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHVtRxMapTug23Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PDHVtRxMapTug23Type"
            
            def description(self):
                return "Define types of VT/TUs in TUG-2-3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHVtRxMapTug22Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PDHVtRxMapTug22Type"
            
            def description(self):
                return "Define types of VT/TUs in TUG-2-2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHVtRxMapTug21Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PDHVtRxMapTug21Type"
            
            def description(self):
                return "Define types of VT/TUs in TUG-2-1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHVtRxMapTug20Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDHVtRxMapTug20Type"
            
            def description(self):
                return "Define types of VT/TUs in TUG-2-0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDHVtRxMapTug26Type"] = _AF6CCI0011_RD_PDH._rx_stsvc_payload_ctrl._PDHVtRxMapTug26Type()
            allFields["PDHVtRxMapTug25Type"] = _AF6CCI0011_RD_PDH._rx_stsvc_payload_ctrl._PDHVtRxMapTug25Type()
            allFields["PDHVtRxMapTug24Type"] = _AF6CCI0011_RD_PDH._rx_stsvc_payload_ctrl._PDHVtRxMapTug24Type()
            allFields["PDHVtRxMapTug23Type"] = _AF6CCI0011_RD_PDH._rx_stsvc_payload_ctrl._PDHVtRxMapTug23Type()
            allFields["PDHVtRxMapTug22Type"] = _AF6CCI0011_RD_PDH._rx_stsvc_payload_ctrl._PDHVtRxMapTug22Type()
            allFields["PDHVtRxMapTug21Type"] = _AF6CCI0011_RD_PDH._rx_stsvc_payload_ctrl._PDHVtRxMapTug21Type()
            allFields["PDHVtRxMapTug20Type"] = _AF6CCI0011_RD_PDH._rx_stsvc_payload_ctrl._PDHVtRxMapTug20Type()
            return allFields

    class _rxm23e23_ctrl(AtRegister.AtRegister):
        def name(self):
            return "RxM23E23 Control"
    
        def description(self):
            return "The RxM23E23 Control is use to configure for per channel Rx DS3/E3 operation."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00040000 +  de3id"
            
        def startAddress(self):
            return 0x00040000
            
        def endAddress(self):
            return 0x0004001f

        class _DS3LOFFrwDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "DS3LOFFrwDis"
            
            def description(self):
                return "Forward DS3 LOF to generate L-bit to PSN in case of MonOnly Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DS3AISSigFrwEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "DS3AISSigFrwEnb"
            
            def description(self):
                return "Forward DS3 AIS signal to generate L-bit to PSN in case of MonOnly Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RXDS3ForceAISLbit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "RXDS3ForceAISLbit"
            
            def description(self):
                return "Force DS3 AIS, L-bit to PSN, data all one to PSN for Payload/Line Remote Loopback"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RXDE3MonOnly(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "RXDE3MonOnly"
            
            def description(self):
                return "Framer monitor only"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS3E3ChEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "RxDS3E3ChEnb"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS3E3LosThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 14
        
            def name(self):
                return "RxDS3E3LosThres"
            
            def description(self):
                return "Threshold to detect LOS all zero, Set 0 to disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS3E3AisThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 11
        
            def name(self):
                return "RxDS3E3AisThres"
            
            def description(self):
                return "Threshold to detect AIS all one, Set 0 to disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS3E3LofThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 8
        
            def name(self):
                return "RxDS3E3LofThres"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS3E3PayAllOne(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "RxDS3E3PayAllOne"
            
            def description(self):
                return "This bit is set to 1 to force the DS3/E3 framer payload AIS downstream"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS3E3LineAllOne(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "RxDS3E3LineAllOne"
            
            def description(self):
                return "This bit is set to 1 to force the DS3/E3 framer line AIS downstream"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS3E3LineLoop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "RxDS3E3LineLoop"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS3E3FrcLof(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RxDS3E3FrcLof"
            
            def description(self):
                return "This bit is set to 1 to force the Rx DS3/E3 framer into LOF status. 1: force LOF. 0: not force LOF."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS3E3Md(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDS3E3Md"
            
            def description(self):
                return "Rx DS3/E3 framing mode 0000: DS3 Unframed 0001: DS3 C-bit parity carrying 28 DS1s or 21 E1s 0010: DS3 M23 carrying 28 DS1s or 21 E1s 0011: DS3 C-bit map 0100: E3 Unframed 0101: E3 G832 0110: E3 G.751 carrying 16 E1s 0111: E3 G.751 Map 1000: Bypass RxM13E13 (DS1/E1 mode)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DS3LOFFrwDis"] = _AF6CCI0011_RD_PDH._rxm23e23_ctrl._DS3LOFFrwDis()
            allFields["DS3AISSigFrwEnb"] = _AF6CCI0011_RD_PDH._rxm23e23_ctrl._DS3AISSigFrwEnb()
            allFields["RXDS3ForceAISLbit"] = _AF6CCI0011_RD_PDH._rxm23e23_ctrl._RXDS3ForceAISLbit()
            allFields["RXDE3MonOnly"] = _AF6CCI0011_RD_PDH._rxm23e23_ctrl._RXDE3MonOnly()
            allFields["RxDS3E3ChEnb"] = _AF6CCI0011_RD_PDH._rxm23e23_ctrl._RxDS3E3ChEnb()
            allFields["RxDS3E3LosThres"] = _AF6CCI0011_RD_PDH._rxm23e23_ctrl._RxDS3E3LosThres()
            allFields["RxDS3E3AisThres"] = _AF6CCI0011_RD_PDH._rxm23e23_ctrl._RxDS3E3AisThres()
            allFields["RxDS3E3LofThres"] = _AF6CCI0011_RD_PDH._rxm23e23_ctrl._RxDS3E3LofThres()
            allFields["RxDS3E3PayAllOne"] = _AF6CCI0011_RD_PDH._rxm23e23_ctrl._RxDS3E3PayAllOne()
            allFields["RxDS3E3LineAllOne"] = _AF6CCI0011_RD_PDH._rxm23e23_ctrl._RxDS3E3LineAllOne()
            allFields["RxDS3E3LineLoop"] = _AF6CCI0011_RD_PDH._rxm23e23_ctrl._RxDS3E3LineLoop()
            allFields["RxDS3E3FrcLof"] = _AF6CCI0011_RD_PDH._rxm23e23_ctrl._RxDS3E3FrcLof()
            allFields["RxDS3E3Md"] = _AF6CCI0011_RD_PDH._rxm23e23_ctrl._RxDS3E3Md()
            return allFields

    class _rxm23e23_hw_stat(AtRegister.AtRegister):
        def name(self):
            return "RxM23E23 HW Status"
    
        def description(self):
            return "for HW debug only"
            
        def width(self):
            return 7
        
        def type(self):
            return "Stat"
            
        def fomular(self):
            return "0x00040080 +  de3id"
            
        def startAddress(self):
            return 0x00040080
            
        def endAddress(self):
            return 0x0004009f

        class _RxDS3_CrrAIC(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "RxDS3_CrrAIC"
            
            def description(self):
                return "Current AIC Bit value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS3_CrrAISDownStr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "RxDS3_CrrAISDownStr"
            
            def description(self):
                return "AIS Down Stream due to HI_Level AIS of DS3E3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS3_CrrLosAllZero(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RxDS3_CrrLosAllZero"
            
            def description(self):
                return "LOS All zero of DS3E3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS3_CrrAISAllOne(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "RxDS3_CrrAISAllOne"
            
            def description(self):
                return "AIS All one of DS3E3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS3_CrrAISSignal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "RxDS3_CrrAISSignal"
            
            def description(self):
                return "AIS signal of DS3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS3E3State(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDS3E3State"
            
            def description(self):
                return "0:LOF, 1:Searching, 2: Verify, 3:InFrame"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDS3_CrrAIC"] = _AF6CCI0011_RD_PDH._rxm23e23_hw_stat._RxDS3_CrrAIC()
            allFields["RxDS3_CrrAISDownStr"] = _AF6CCI0011_RD_PDH._rxm23e23_hw_stat._RxDS3_CrrAISDownStr()
            allFields["RxDS3_CrrLosAllZero"] = _AF6CCI0011_RD_PDH._rxm23e23_hw_stat._RxDS3_CrrLosAllZero()
            allFields["RxDS3_CrrAISAllOne"] = _AF6CCI0011_RD_PDH._rxm23e23_hw_stat._RxDS3_CrrAISAllOne()
            allFields["RxDS3_CrrAISSignal"] = _AF6CCI0011_RD_PDH._rxm23e23_hw_stat._RxDS3_CrrAISSignal()
            allFields["RxDS3E3State"] = _AF6CCI0011_RD_PDH._rxm23e23_hw_stat._RxDS3E3State()
            return allFields

    class _rx_de3_oh_ctrl(AtRegister.AtRegister):
        def name(self):
            return "RxDS3E3 OH Pro Control"
    
        def description(self):
            return ""
            
        def width(self):
            return 6
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x040420 +  de3id"
            
        def startAddress(self):
            return 0x00040420
            
        def endAddress(self):
            return 0x0004043f

        class _DLEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "DLEn"
            
            def description(self):
                return "DLEn 1: Data Link send to HDLC 0: Data Link not send to HDLC"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Ds3E3DLSL(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 3
        
            def name(self):
                return "Ds3E3DLSL"
            
            def description(self):
                return "00: DL 01: UDL 10: FEAC 11: NA"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Ds3FEACEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Ds3FEACEn"
            
            def description(self):
                return "Enable detection FEAC state change"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FBEFbitCntEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FBEFbitCntEn"
            
            def description(self):
                return "Enable FBE counts F-Bit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FBEMbitCntEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FBEMbitCntEn"
            
            def description(self):
                return "Enable FBE counts M-Bit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DLEn"] = _AF6CCI0011_RD_PDH._rx_de3_oh_ctrl._DLEn()
            allFields["Ds3E3DLSL"] = _AF6CCI0011_RD_PDH._rx_de3_oh_ctrl._Ds3E3DLSL()
            allFields["Ds3FEACEn"] = _AF6CCI0011_RD_PDH._rx_de3_oh_ctrl._Ds3FEACEn()
            allFields["FBEFbitCntEn"] = _AF6CCI0011_RD_PDH._rx_de3_oh_ctrl._FBEFbitCntEn()
            allFields["FBEMbitCntEn"] = _AF6CCI0011_RD_PDH._rx_de3_oh_ctrl._FBEMbitCntEn()
            return allFields

    class _rxde3_oh_thrsh_cfg(AtRegister.AtRegister):
        def name(self):
            return "RxDS3E3 OH Pro Threshold"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00040401
            
        def endAddress(self):
            return 0xffffffff

        class _IdleThresDS3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 21
        
            def name(self):
                return "IdleThresDS3"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _AisThresDS3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 18
        
            def name(self):
                return "AisThresDS3"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TimingMakerE3G832(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 15
        
            def name(self):
                return "TimingMakerE3G832"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _NewPayTypeE3G832(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 12
        
            def name(self):
                return "NewPayTypeE3G832"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _AICThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 9
        
            def name(self):
                return "AICThres"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RdiE3G832Thres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 6
        
            def name(self):
                return "RdiE3G832Thres"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RdiE3G751Thres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 3
        
            def name(self):
                return "RdiE3G751Thres"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RdiDS3Thres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RdiDS3Thres"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IdleThresDS3"] = _AF6CCI0011_RD_PDH._rxde3_oh_thrsh_cfg._IdleThresDS3()
            allFields["AisThresDS3"] = _AF6CCI0011_RD_PDH._rxde3_oh_thrsh_cfg._AisThresDS3()
            allFields["TimingMakerE3G832"] = _AF6CCI0011_RD_PDH._rxde3_oh_thrsh_cfg._TimingMakerE3G832()
            allFields["NewPayTypeE3G832"] = _AF6CCI0011_RD_PDH._rxde3_oh_thrsh_cfg._NewPayTypeE3G832()
            allFields["AICThres"] = _AF6CCI0011_RD_PDH._rxde3_oh_thrsh_cfg._AICThres()
            allFields["RdiE3G832Thres"] = _AF6CCI0011_RD_PDH._rxde3_oh_thrsh_cfg._RdiE3G832Thres()
            allFields["RdiE3G751Thres"] = _AF6CCI0011_RD_PDH._rxde3_oh_thrsh_cfg._RdiE3G751Thres()
            allFields["RdiDS3Thres"] = _AF6CCI0011_RD_PDH._rxde3_oh_thrsh_cfg._RdiDS3Thres()
            return allFields

    class _rxm23e23_rei_thrsh_cfg(AtRegister.AtRegister):
        def name(self):
            return "RxM23E23 Framer REI Threshold Control"
    
        def description(self):
            return "This is the REI threshold configuration register for the PDH DS3/E3 Rx framer"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00040403
            
        def endAddress(self):
            return 0xffffffff

        class _RxM23E23ReiThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxM23E23ReiThres"
            
            def description(self):
                return "Threshold to generate interrupt if the REI counter exceed this threshold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxM23E23ReiThres"] = _AF6CCI0011_RD_PDH._rxm23e23_rei_thrsh_cfg._RxM23E23ReiThres()
            return allFields

    class _rxm23e23_fbe_thrsh_cfg(AtRegister.AtRegister):
        def name(self):
            return "RxM23E23 Framer FBE Threshold Control"
    
        def description(self):
            return "This is the REI threshold configuration register for the PDH DS3/E3 Rx framer"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00040404
            
        def endAddress(self):
            return 0xffffffff

        class _RxM23E23FbeThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxM23E23FbeThres"
            
            def description(self):
                return "Threshold to generate interrupt if the FBE counter exceed this threshold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxM23E23FbeThres"] = _AF6CCI0011_RD_PDH._rxm23e23_fbe_thrsh_cfg._RxM23E23FbeThres()
            return allFields

    class _rxm23e23_parity_thrsh_cfg(AtRegister.AtRegister):
        def name(self):
            return "RxM23E23 Framer Parity Threshold Control"
    
        def description(self):
            return "This is the REI threshold configuration register for the PDH DS3/E3 Rx framer"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00040405
            
        def endAddress(self):
            return 0xffffffff

        class _RxM23E23ParThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxM23E23ParThres"
            
            def description(self):
                return "Threshold to generate interrupt if the PAR counter exceed this threshold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxM23E23ParThres"] = _AF6CCI0011_RD_PDH._rxm23e23_parity_thrsh_cfg._RxM23E23ParThres()
            return allFields

    class _rxm23e23_cbit_parity_thrsh_cfg(AtRegister.AtRegister):
        def name(self):
            return "RxM23E23 Framer Cbit Parity Threshold Control"
    
        def description(self):
            return "This is the REI threshold configuration register for the PDH DS3/E3 Rx framer"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00040406
            
        def endAddress(self):
            return 0xffffffff

        class _RxM23E23CParThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxM23E23CParThres"
            
            def description(self):
                return "Threshold to generate interrupt if the Cbit Parity counter exceed this threshold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxM23E23CParThres"] = _AF6CCI0011_RD_PDH._rxm23e23_cbit_parity_thrsh_cfg._RxM23E23CParThres()
            return allFields

    class _rxm23e23_rei_cnt(AtRegister.AtRegister):
        def name(self):
            return "RxM23E23 Framer REI Counter"
    
        def description(self):
            return "This is the per channel REI counter for DS3/E3 receive framer"
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00040440 +  32*Rd2Clr + de3id"
            
        def startAddress(self):
            return 0x00040440
            
        def endAddress(self):
            return 0x0004047f

        class _RxM23E23ReiCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxM23E23ReiCnt"
            
            def description(self):
                return "DS3/E3 REI error accumulator"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxM23E23ReiCnt"] = _AF6CCI0011_RD_PDH._rxm23e23_rei_cnt._RxM23E23ReiCnt()
            return allFields

    class _rxm23e23_fbe_cnt(AtRegister.AtRegister):
        def name(self):
            return "RxM23E23 Framer FBE Counter"
    
        def description(self):
            return "This is the per channel FBE counter for DS3/E3 receive framer"
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00040480 +  32*Rd2Clr + de3id"
            
        def startAddress(self):
            return 0x00040480
            
        def endAddress(self):
            return 0x000404bf

        class _count(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "count"
            
            def description(self):
                return "frame FBE counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["count"] = _AF6CCI0011_RD_PDH._rxm23e23_fbe_cnt._count()
            return allFields

    class _rxm23e23_parity_cnt(AtRegister.AtRegister):
        def name(self):
            return "RxM23E23 Framer Parity Counter"
    
        def description(self):
            return "This is the per channel Parity counter for DS3/E3 receive framer"
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000404C0 +  32*Rd2Clr + de3id"
            
        def startAddress(self):
            return 0x000404c0
            
        def endAddress(self):
            return 0x000404ff

        class _RxM23E23ParCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxM23E23ParCnt"
            
            def description(self):
                return "DS3/E3 Parity error accumulator"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxM23E23ParCnt"] = _AF6CCI0011_RD_PDH._rxm23e23_parity_cnt._RxM23E23ParCnt()
            return allFields

    class _rxm23e23_cbit_parity_cnt(AtRegister.AtRegister):
        def name(self):
            return "RxM23E23 Framer Cbit Parity Counter"
    
        def description(self):
            return "This is the per channel Cbit Parity counter for DS3/E3 receive framer"
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00040500 +  32*Rd2Clr + de3id"
            
        def startAddress(self):
            return 0x00040500
            
        def endAddress(self):
            return 0x0004053f

        class _RxM23E23CParCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxM23E23CParCnt"
            
            def description(self):
                return "DS3/E3 Cbit Parity error accumulator"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxM23E23CParCnt"] = _AF6CCI0011_RD_PDH._rxm23e23_cbit_parity_cnt._RxM23E23CParCnt()
            return allFields

    class _rx_de3_feac_buffer(AtRegister.AtRegister):
        def name(self):
            return "RxDS3E3 FEAC Buffer"
    
        def description(self):
            return ""
            
        def width(self):
            return 9
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x040560 +  de3id"
            
        def startAddress(self):
            return 0x00040560
            
        def endAddress(self):
            return 0x0004057f

        class _FEAC_State(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FEAC_State"
            
            def description(self):
                return "1xx: idle codeword 001: active codeword 010: de-active codeword 011: alarm/status codeword"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FEAC_Codeword(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FEAC_Codeword"
            
            def description(self):
                return "6 x-bits cw in format 111111110xxxxxx0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FEAC_State"] = _AF6CCI0011_RD_PDH._rx_de3_feac_buffer._FEAC_State()
            allFields["FEAC_Codeword"] = _AF6CCI0011_RD_PDH._rx_de3_feac_buffer._FEAC_Codeword()
            return allFields

    class _rxm23e23_chn_intr_cfg(AtRegister.AtRegister):
        def name(self):
            return "RxM23E23 Framer Channel Interrupt Enable Control"
    
        def description(self):
            return "This is the Channel interrupt enable of DS3/E3 Rx Framer."
            
        def width(self):
            return 24
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000405fe
            
        def endAddress(self):
            return 0xffffffff

        class _RxM23E23IntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxM23E23IntrEn"
            
            def description(self):
                return "Each bit correspond with each channel DS3/E3 0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxM23E23IntrEn"] = _AF6CCI0011_RD_PDH._rxm23e23_chn_intr_cfg._RxM23E23IntrEn()
            return allFields

    class _rxm23e23_chn_intr_stat(AtRegister.AtRegister):
        def name(self):
            return "RxM23E23 Framer Channel Interrupt Status"
    
        def description(self):
            return "This is the Channel interrupt tus of DS3/E3 Rx Framer."
            
        def width(self):
            return 24
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000405ff
            
        def endAddress(self):
            return 0xffffffff

        class _RxM23E23IntrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxM23E23IntrSta"
            
            def description(self):
                return "Each bit correspond with each channel DS3/E3 0: No Interrupt 1: Have Interrupt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxM23E23IntrSta"] = _AF6CCI0011_RD_PDH._rxm23e23_chn_intr_stat._RxM23E23IntrSta()
            return allFields

    class _rxm23e23_per_chn_intr_cfg(AtRegister.AtRegister):
        def name(self):
            return "RxM23E23 Framer per Channel Interrupt Enable Control"
    
        def description(self):
            return "This is the per Channel interrupt enable of DS1/E1/J1 Rx Framer."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00040580 +  de3id"
            
        def startAddress(self):
            return 0x00040580
            
        def endAddress(self):
            return 0x0004059f

        class _DE3BERTCA_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "DE3BERTCA_En"
            
            def description(self):
                return "Set 1 to enable change DE3 BER TCA event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3BERSD_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "DE3BERSD_En"
            
            def description(self):
                return "Set 1 to enable change DE3 BER SD event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3BERSF_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "DE3BERSF_En"
            
            def description(self):
                return "Set 1 to enable change DE3 BER SF event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3LCVCnt_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "DE3LCVCnt_En"
            
            def description(self):
                return "Set 1 to enable change DE3 LCV counter event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DS3CP_E3Tr_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "DS3CP_E3Tr_En"
            
            def description(self):
                return "Set 1 to enable change DS3 CP counter or E3G831 Trace event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3ParCnt_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "DE3ParCnt_En"
            
            def description(self):
                return "Set 1 to enable change DE3 Parity counter event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3FBECnt_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "DE3FBECnt_En"
            
            def description(self):
                return "Set 1 to enable change DE3 FBE counter event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3REICnt_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "DE3REICnt_En"
            
            def description(self):
                return "Set 1 to enable change DE3 REI counter event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DS3FEAC_E3SSM_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "DS3FEAC_E3SSM_En"
            
            def description(self):
                return "Set 1 to enable change DS3 FEAC or E3G832 SSM event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DS3AIC_E3TM_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "DS3AIC_E3TM_En"
            
            def description(self):
                return "Set 1 to enable change DS3 AIC or E3G832 TM event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DS3IDLESig_E3PT_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "DS3IDLESig_E3PT_En"
            
            def description(self):
                return "Set 1 to enable change DS3 IDLE signal or E3G832 PayloadType event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3AisAllOneIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "DE3AisAllOneIntrEn"
            
            def description(self):
                return "Set 1 to enable change AIS All One event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3LosAllZeroIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "DE3LosAllZeroIntrEn"
            
            def description(self):
                return "Set 1 to enable change LOS All Zero event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DS3AISSigIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "DS3AISSigIntrEn"
            
            def description(self):
                return "Set 1 to enable change AIS Signal event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3RDIIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "DE3RDIIntrEn"
            
            def description(self):
                return "Set 1 to enable change RDI event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3LofIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE3LofIntrEn"
            
            def description(self):
                return "Set 1 to enable change LOF event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE3BERTCA_En"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_intr_cfg._DE3BERTCA_En()
            allFields["DE3BERSD_En"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_intr_cfg._DE3BERSD_En()
            allFields["DE3BERSF_En"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_intr_cfg._DE3BERSF_En()
            allFields["DE3LCVCnt_En"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_intr_cfg._DE3LCVCnt_En()
            allFields["DS3CP_E3Tr_En"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_intr_cfg._DS3CP_E3Tr_En()
            allFields["DE3ParCnt_En"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_intr_cfg._DE3ParCnt_En()
            allFields["DE3FBECnt_En"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_intr_cfg._DE3FBECnt_En()
            allFields["DE3REICnt_En"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_intr_cfg._DE3REICnt_En()
            allFields["DS3FEAC_E3SSM_En"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_intr_cfg._DS3FEAC_E3SSM_En()
            allFields["DS3AIC_E3TM_En"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_intr_cfg._DS3AIC_E3TM_En()
            allFields["DS3IDLESig_E3PT_En"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_intr_cfg._DS3IDLESig_E3PT_En()
            allFields["DE3AisAllOneIntrEn"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_intr_cfg._DE3AisAllOneIntrEn()
            allFields["DE3LosAllZeroIntrEn"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_intr_cfg._DE3LosAllZeroIntrEn()
            allFields["DS3AISSigIntrEn"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_intr_cfg._DS3AISSigIntrEn()
            allFields["DE3RDIIntrEn"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_intr_cfg._DE3RDIIntrEn()
            allFields["DE3LofIntrEn"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_intr_cfg._DE3LofIntrEn()
            return allFields

    class _rxm23e23_per_chn_intr_stat(AtRegister.AtRegister):
        def name(self):
            return "RxM23E23 Framer per Channel Interrupt Change Status"
    
        def description(self):
            return "This is the per Channel interrupt tus of DS3/E3 Rx Framer."
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return "0x000405A0 +  de3id"
            
        def startAddress(self):
            return 0x000405a0
            
        def endAddress(self):
            return 0x000405bf

        class _DE3BERTCA_Intr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "DE3BERTCA_Intr"
            
            def description(self):
                return "Set 1 if there is change DE3 BER TCA event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3BERSD_Intr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "DE3BERSD_Intr"
            
            def description(self):
                return "Set 1 if there is change DE3 BER SD event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3BERSF_Intr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "DE3BERSF_Intr"
            
            def description(self):
                return "Set 1 if there is change DE3 BER SF event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3LCVCnt_Intr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "DE3LCVCnt_Intr"
            
            def description(self):
                return "Set 1 if there is change DE3 LCV counter event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DS3CP_E3Tr_Intr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "DS3CP_E3Tr_Intr"
            
            def description(self):
                return "Set 1 if there is change DS3 CP counter or E3G831 Trace event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3ParCnt_Intr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "DE3ParCnt_Intr"
            
            def description(self):
                return "Set 1 if there is change DE3 Parity counter event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3FBECnt_Intr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "DE3FBECnt_Intr"
            
            def description(self):
                return "Set 1 if there is change DE3 FBE counter event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3REICnt_Intr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "DE3REICnt_Intr"
            
            def description(self):
                return "Set 1 to enable change DE3 REI counter event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DS3FEAC_E3SSM_Intr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "DS3FEAC_E3SSM_Intr"
            
            def description(self):
                return "Set 1 if there is a change DS3 FEAC or E3G832 SSM event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DS3AIC_E3TM_Intr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "DS3AIC_E3TM_Intr"
            
            def description(self):
                return "Set 1 if there is a change DS3 AIC or E3G832 TM event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DS3IDLESig_E3PT_Intr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "DS3IDLESig_E3PT_Intr"
            
            def description(self):
                return "Set 1 if there is a change DS3 IDLE signal or E3G832 PayloadType event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3AisAllOne_Intr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "DE3AisAllOne_Intr"
            
            def description(self):
                return "Set 1 if there is a change AIS All One event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3LosAllZero_Intr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "DE3LosAllZero_Intr"
            
            def description(self):
                return "Set 1 if there is a change LOS All Zero event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DS3AISSig_Intr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "DS3AISSig_Intr"
            
            def description(self):
                return "Set 1 if there is a change AIS Signal event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3RDI_Intr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "DE3RDI_Intr"
            
            def description(self):
                return "Set 1 if there is a  change RDI event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3Lof_Intr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE3Lof_Intr"
            
            def description(self):
                return "Set 1 if there is a  change LOF event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE3BERTCA_Intr"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_intr_stat._DE3BERTCA_Intr()
            allFields["DE3BERSD_Intr"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_intr_stat._DE3BERSD_Intr()
            allFields["DE3BERSF_Intr"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_intr_stat._DE3BERSF_Intr()
            allFields["DE3LCVCnt_Intr"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_intr_stat._DE3LCVCnt_Intr()
            allFields["DS3CP_E3Tr_Intr"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_intr_stat._DS3CP_E3Tr_Intr()
            allFields["DE3ParCnt_Intr"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_intr_stat._DE3ParCnt_Intr()
            allFields["DE3FBECnt_Intr"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_intr_stat._DE3FBECnt_Intr()
            allFields["DE3REICnt_Intr"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_intr_stat._DE3REICnt_Intr()
            allFields["DS3FEAC_E3SSM_Intr"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_intr_stat._DS3FEAC_E3SSM_Intr()
            allFields["DS3AIC_E3TM_Intr"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_intr_stat._DS3AIC_E3TM_Intr()
            allFields["DS3IDLESig_E3PT_Intr"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_intr_stat._DS3IDLESig_E3PT_Intr()
            allFields["DE3AisAllOne_Intr"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_intr_stat._DE3AisAllOne_Intr()
            allFields["DE3LosAllZero_Intr"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_intr_stat._DE3LosAllZero_Intr()
            allFields["DS3AISSig_Intr"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_intr_stat._DS3AISSig_Intr()
            allFields["DE3RDI_Intr"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_intr_stat._DE3RDI_Intr()
            allFields["DE3Lof_Intr"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_intr_stat._DE3Lof_Intr()
            return allFields

    class _rxm23e23_per_chn_curr_stat(AtRegister.AtRegister):
        def name(self):
            return "RxM23E23 Framer per Channel Current Status"
    
        def description(self):
            return "This is the per Channel Current tus of DS3/E3 Rx Framer."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000405C0 +  de3id"
            
        def startAddress(self):
            return 0x000405c0
            
        def endAddress(self):
            return 0x000405df

        class _DE3BERTCA_CurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "DE3BERTCA_CurrSta"
            
            def description(self):
                return "Current status DE3 BER TCA event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3BERSD_CurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "DE3BERSD_CurrSta"
            
            def description(self):
                return "Current status DE3 BER SD event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3BERSF_CurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "DE3BERSF_CurrSta"
            
            def description(self):
                return "Current status DE3 BER SF event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3LCVCnt_CurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "DE3LCVCnt_CurrSta"
            
            def description(self):
                return "Current status DE3 LCV counter event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DS3CP_E3Tr_CurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "DS3CP_E3Tr_CurrSta"
            
            def description(self):
                return "Current status DS3 CP counter or E3G831 Trace event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3ParCnt_CurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "DE3ParCnt_CurrSta"
            
            def description(self):
                return "Current status DE3 Parity counter event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3FBECnt_CurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "DE3FBECnt_CurrSta"
            
            def description(self):
                return "Current status DE3 FBE counter event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3REICnt_CurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "DE3REICnt_CurrSta"
            
            def description(self):
                return "Current status DE3 REI counter event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DS3FEAC_E3SSM_CurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "DS3FEAC_E3SSM_CurrSta"
            
            def description(self):
                return "Current status of DS3 FEAC or E3G832 SSM event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DS3AIC_E3TM_CurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "DS3AIC_E3TM_CurrSta"
            
            def description(self):
                return "Current status of DS3 AIC or E3G832 TM event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DS3IDLESig_E3PT_CurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "DS3IDLESig_E3PT_CurrSta"
            
            def description(self):
                return "Current status of DS3 IDLE signal or E3G832 PayloadType event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3AisAllOneCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "DE3AisAllOneCurrSta"
            
            def description(self):
                return "Current status of AIS All One event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3LosAllZeroCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "DE3LosAllZeroCurrSta"
            
            def description(self):
                return "Current status of LOS All Zero event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DS3AISSigCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "DS3AISSigCurrSta"
            
            def description(self):
                return "Current status of AIS Signal event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3RDICurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "DE3RDICurrSta"
            
            def description(self):
                return "Current status of RDI event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3LofCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE3LofCurrSta"
            
            def description(self):
                return "Current status of LOF event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE3BERTCA_CurrSta"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_curr_stat._DE3BERTCA_CurrSta()
            allFields["DE3BERSD_CurrSta"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_curr_stat._DE3BERSD_CurrSta()
            allFields["DE3BERSF_CurrSta"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_curr_stat._DE3BERSF_CurrSta()
            allFields["DE3LCVCnt_CurrSta"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_curr_stat._DE3LCVCnt_CurrSta()
            allFields["DS3CP_E3Tr_CurrSta"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_curr_stat._DS3CP_E3Tr_CurrSta()
            allFields["DE3ParCnt_CurrSta"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_curr_stat._DE3ParCnt_CurrSta()
            allFields["DE3FBECnt_CurrSta"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_curr_stat._DE3FBECnt_CurrSta()
            allFields["DE3REICnt_CurrSta"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_curr_stat._DE3REICnt_CurrSta()
            allFields["DS3FEAC_E3SSM_CurrSta"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_curr_stat._DS3FEAC_E3SSM_CurrSta()
            allFields["DS3AIC_E3TM_CurrSta"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_curr_stat._DS3AIC_E3TM_CurrSta()
            allFields["DS3IDLESig_E3PT_CurrSta"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_curr_stat._DS3IDLESig_E3PT_CurrSta()
            allFields["DE3AisAllOneCurrSta"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_curr_stat._DE3AisAllOneCurrSta()
            allFields["DE3LosAllZeroCurrSta"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_curr_stat._DE3LosAllZeroCurrSta()
            allFields["DS3AISSigCurrSta"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_curr_stat._DS3AISSigCurrSta()
            allFields["DE3RDICurrSta"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_curr_stat._DE3RDICurrSta()
            allFields["DE3LofCurrSta"] = _AF6CCI0011_RD_PDH._rxm23e23_per_chn_curr_stat._DE3LofCurrSta()
            return allFields

    class _rxde3_oh_dlk_swap_bit_cfg(AtRegister.AtRegister):
        def name(self):
            return "RxDS3E3 OH Pro DLK Swap Bit Enable"
    
        def description(self):
            return ""
            
        def width(self):
            return 3
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0004040b
            
        def endAddress(self):
            return 0xffffffff

        class _DlkSwapBitDs3En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "DlkSwapBitDs3En"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DlkSwapBitE3g751En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "DlkSwapBitE3g751En"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DlkSwapBitE3g823En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DlkSwapBitE3g823En"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DlkSwapBitDs3En"] = _AF6CCI0011_RD_PDH._rxde3_oh_dlk_swap_bit_cfg._DlkSwapBitDs3En()
            allFields["DlkSwapBitE3g751En"] = _AF6CCI0011_RD_PDH._rxde3_oh_dlk_swap_bit_cfg._DlkSwapBitE3g751En()
            allFields["DlkSwapBitE3g823En"] = _AF6CCI0011_RD_PDH._rxde3_oh_dlk_swap_bit_cfg._DlkSwapBitE3g823En()
            return allFields

    class _de3_testpat_mon_glb_cfg(AtRegister.AtRegister):
        def name(self):
            return "DS3/E3 Test Pattern Monitoring Global Control"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x040700 + id"
            
        def startAddress(self):
            return 0x00040700
            
        def endAddress(self):
            return 0xffffffff

        class _RxDe3PtmEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "RxDe3PtmEn"
            
            def description(self):
                return "Bit Enable Test Pattern Monitoring"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDe3PtmID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 18
        
            def name(self):
                return "RxDe3PtmID"
            
            def description(self):
                return "To configure which DS3/E3 line is selected  to monitor the Test Pattern"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDe3PtmIntEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "RxDe3PtmIntEn"
            
            def description(self):
                return "Interrupt enable to output upint"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDe3PtmSatura(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "RxDe3PtmSatura"
            
            def description(self):
                return "Counter BERT mode 1: PTM Counter Saturation Mode 0: PTM Counter Roll-Over Mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDe3PtmStkCfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "RxDe3PtmStkCfg"
            
            def description(self):
                return "BERT error sticky"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDe3PtmLopThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 11
        
            def name(self):
                return "RxDe3PtmLopThr"
            
            def description(self):
                return "Threshold for Loss Of Pattern detection. Status will go LOP if the number of 		error patterns (8 bits) in 16 consecutive patterns is more than or equal the RxDe3PtmLopThr"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDe3PtmSynThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 7
        
            def name(self):
                return "RxDe3PtmSynThr"
            
            def description(self):
                return "Threshold for Sync detection. Status will go SYN if the number of corrected 		consecutive patterns (8 bits) is more than or equal the RxDe3PtmSynThr."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDe3PtmSwap(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "RxDe3PtmSwap"
            
            def description(self):
                return "PTM swap Bit. If it is set to 1, pattern is transmitted LSB first, then MSB. 		Normal is set to 0."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDe3PtmInv(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "RxDe3PtmInv"
            
            def description(self):
                return "PTM inversion Bit. Normal is set to 0."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDe3PtmReSync(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RxDe3PtmReSync"
            
            def description(self):
                return "Bit force Ptm engine n from a certain te to loss sync state. Engine will 		re-search pattern test. Note: if this bit is hold the value 1, engine is always in loss sync state."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDe3PtmMod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDe3PtmMod"
            
            def description(self):
                return "Monitor DS3/E3 BERT mode 0000: Prbs9 0001: Prbs11 0010: Prbs15 0011: Prbs20 0100: Prbs20 0101: Qrss20 0110: Prbs23 0111: Prbs31 1000: Sequence 1001: AllOne 1010: AllZero 1011: AA55"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDe3PtmEn"] = _AF6CCI0011_RD_PDH._de3_testpat_mon_glb_cfg._RxDe3PtmEn()
            allFields["RxDe3PtmID"] = _AF6CCI0011_RD_PDH._de3_testpat_mon_glb_cfg._RxDe3PtmID()
            allFields["RxDe3PtmIntEn"] = _AF6CCI0011_RD_PDH._de3_testpat_mon_glb_cfg._RxDe3PtmIntEn()
            allFields["RxDe3PtmSatura"] = _AF6CCI0011_RD_PDH._de3_testpat_mon_glb_cfg._RxDe3PtmSatura()
            allFields["RxDe3PtmStkCfg"] = _AF6CCI0011_RD_PDH._de3_testpat_mon_glb_cfg._RxDe3PtmStkCfg()
            allFields["RxDe3PtmLopThr"] = _AF6CCI0011_RD_PDH._de3_testpat_mon_glb_cfg._RxDe3PtmLopThr()
            allFields["RxDe3PtmSynThr"] = _AF6CCI0011_RD_PDH._de3_testpat_mon_glb_cfg._RxDe3PtmSynThr()
            allFields["RxDe3PtmSwap"] = _AF6CCI0011_RD_PDH._de3_testpat_mon_glb_cfg._RxDe3PtmSwap()
            allFields["RxDe3PtmInv"] = _AF6CCI0011_RD_PDH._de3_testpat_mon_glb_cfg._RxDe3PtmInv()
            allFields["RxDe3PtmReSync"] = _AF6CCI0011_RD_PDH._de3_testpat_mon_glb_cfg._RxDe3PtmReSync()
            allFields["RxDe3PtmMod"] = _AF6CCI0011_RD_PDH._de3_testpat_mon_glb_cfg._RxDe3PtmMod()
            return allFields

    class _de3_testpat_mon_sticky_en(AtRegister.AtRegister):
        def name(self):
            return "DS3/E3 Test Pattern Monitoring Sticky Enable"
    
        def description(self):
            return ""
            
        def width(self):
            return 1
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x040701 + 16*id"
            
        def startAddress(self):
            return 0x00040701
            
        def endAddress(self):
            return 0xffffffff

        class _RxDe3PtmStkEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDe3PtmStkEn"
            
            def description(self):
                return "Enable PTM Sticky"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDe3PtmStkEn"] = _AF6CCI0011_RD_PDH._de3_testpat_mon_sticky_en._RxDe3PtmStkEn()
            return allFields

    class _de3_testpat_mon_err_cnt(AtRegister.AtRegister):
        def name(self):
            return "DS3/E3 Test Pattern Monitoring Error Counter"
    
        def description(self):
            return "These registers are used to be error counter report of PTM while the monitor is in frame"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x040702 +  16*id +  R2Clr"
            
        def startAddress(self):
            return 0x00040702
            
        def endAddress(self):
            return 0xffffffff

        class _RxDE3PtmErrcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE3PtmErrcnt"
            
            def description(self):
                return "Error counter for BERT monitoring."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE3PtmErrcnt"] = _AF6CCI0011_RD_PDH._de3_testpat_mon_err_cnt._RxDE3PtmErrcnt()
            return allFields

    class _de3_testpat_mon_goodbit_cnt(AtRegister.AtRegister):
        def name(self):
            return "DS3/E3 Test Pattern Monitoring Good Bit Counter"
    
        def description(self):
            return "These registers are used to be good bit counter report of PTM while the monitor is in frame"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x040704 +  16*id +  R2Clr"
            
        def startAddress(self):
            return 0x00040704
            
        def endAddress(self):
            return 0xffffffff

        class _RxDE3PtmGoodCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE3PtmGoodCnt"
            
            def description(self):
                return "Good bit counter for BERT monitoring."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE3PtmGoodCnt"] = _AF6CCI0011_RD_PDH._de3_testpat_mon_goodbit_cnt._RxDE3PtmGoodCnt()
            return allFields

    class _de3_testpat_mon_lostbit_cnt(AtRegister.AtRegister):
        def name(self):
            return "DS3/E3 Test Pattern Monitoring Lost Bit Counter"
    
        def description(self):
            return "These registers are used to be bit counter report of PTM while the monitor is in loss of frame"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x040706 +  16*id +  R2Clr"
            
        def startAddress(self):
            return 0x00040706
            
        def endAddress(self):
            return 0xffffffff

        class _RxDE3PtmLostCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE3PtmLostCnt"
            
            def description(self):
                return "Counter for BERT monitoring."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE3PtmLostCnt"] = _AF6CCI0011_RD_PDH._de3_testpat_mon_lostbit_cnt._RxDE3PtmLostCnt()
            return allFields

    class _de3_testpat_mon_cnt_cfg(AtRegister.AtRegister):
        def name(self):
            return "DS3/E3 Test Pattern Monitoring Counter Loading"
    
        def description(self):
            return "This register is used to configure the enable to load all the test pattern monitor to the loading registers at the same time."
            
        def width(self):
            return 1
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x040708 + 16*id"
            
        def startAddress(self):
            return 0x00040708
            
        def endAddress(self):
            return 0xffffffff

        class _RxDE3PtmCntLoadEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE3PtmCntLoadEn"
            
            def description(self):
                return "DS3/E3 Pattern Monitoring Counter Load Enable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE3PtmCntLoadEn"] = _AF6CCI0011_RD_PDH._de3_testpat_mon_cnt_cfg._RxDE3PtmCntLoadEn()
            return allFields

    class _de3_testpat_mon_errbit_cnt(AtRegister.AtRegister):
        def name(self):
            return "DS3/E3 Test Pattern Monitoring Error-Bit Counter Loading"
    
        def description(self):
            return "This register stores the value of Error-Bit counter after counter loading action"
            
        def width(self):
            return 32
        
        def type(self):
            return "status"
            
        def fomular(self):
            return "0x040709 + 16*id"
            
        def startAddress(self):
            return 0x00040709
            
        def endAddress(self):
            return 0xffffffff

        class _RxDE3PtmLoadErrCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE3PtmLoadErrCnt"
            
            def description(self):
                return "Load Error counter."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE3PtmLoadErrCnt"] = _AF6CCI0011_RD_PDH._de3_testpat_mon_errbit_cnt._RxDE3PtmLoadErrCnt()
            return allFields

    class _de3_testpat_mon_goodbit_cnt_loading(AtRegister.AtRegister):
        def name(self):
            return "DS3/E3 Test Pattern Monitoring Good-Bit Counter Loading"
    
        def description(self):
            return "This register stores the value of Good-Bit counter after counter loading action"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x04070A +  16*id"
            
        def startAddress(self):
            return 0x0004070a
            
        def endAddress(self):
            return 0xffffffff

        class _RxDE3PtmGdCntLoad(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE3PtmGdCntLoad"
            
            def description(self):
                return "Load Good counter."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE3PtmGdCntLoad"] = _AF6CCI0011_RD_PDH._de3_testpat_mon_goodbit_cnt_loading._RxDE3PtmGdCntLoad()
            return allFields

    class _de3_testpat_mon_lostbit_cnt_loading(AtRegister.AtRegister):
        def name(self):
            return "DS3/E3 Test Pattern Monitoring Lost-Bit Counter Loading"
    
        def description(self):
            return "This register stores the value of Lost-Bit counter after counter loading action"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x04070B +  16*id"
            
        def startAddress(self):
            return 0x0004070b
            
        def endAddress(self):
            return 0xffffffff

        class _RxDE3PtmLossCntLoad(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE3PtmLossCntLoad"
            
            def description(self):
                return "Load Loss counter."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE3PtmLossCntLoad"] = _AF6CCI0011_RD_PDH._de3_testpat_mon_lostbit_cnt_loading._RxDE3PtmLossCntLoad()
            return allFields

    class _rxm12e12_ctrl(AtRegister.AtRegister):
        def name(self):
            return "RxM12E12 Control"
    
        def description(self):
            return "The RxM12E12 Control is use to configure for per channel Rx DS2/E2 operation."
            
        def width(self):
            return 4
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00041000 +  8*de3id + de2id"
            
        def startAddress(self):
            return 0x00041000
            
        def endAddress(self):
            return 0x000410ff

        class _RxDS2E2AISMask(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "RxDS2E2AISMask"
            
            def description(self):
                return "This bit is set to 1 to disable AIS downstream masking to RxDE1 into LOF status. 1: UnMask. 0: Mask."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS2E2FrcLof(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "RxDS2E2FrcLof"
            
            def description(self):
                return "This bit is set to 1 to force the Rx DS3/E3 framer into LOF tus. 1: force LOF. 0: not force LOF."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS2E2Md(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDS2E2Md"
            
            def description(self):
                return "Rx DS2/E2 framing mode 00: DS2 T1.107 carrying 4 DS1s 01: DS2 G.747 carrying 3 E1s 10: E2 G.742 carrying 4 E1s 11: Reserved"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDS2E2AISMask"] = _AF6CCI0011_RD_PDH._rxm12e12_ctrl._RxDS2E2AISMask()
            allFields["RxDS2E2FrcLof"] = _AF6CCI0011_RD_PDH._rxm12e12_ctrl._RxDS2E2FrcLof()
            allFields["RxDS2E2Md"] = _AF6CCI0011_RD_PDH._rxm12e12_ctrl._RxDS2E2Md()
            return allFields

    class _rxm12e12_stsvc_payload_ctrl(AtRegister.AtRegister):
        def name(self):
            return "PDH RxM12E12 Per STS/VC Payload Control"
    
        def description(self):
            return "The STS configuration registers are used to configure kinds of STS-1s and the kinds of VT/TUs in each STS-1."
            
        def width(self):
            return 14
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00041300 +  stsid"
            
        def startAddress(self):
            return 0x00041300
            
        def endAddress(self):
            return 0x0004131f

        class _PDHVtRxMapTug26Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PDHVtRxMapTug26Type"
            
            def description(self):
                return "Define types of VT/TUs in TUG-2-6. 0: TU11/VT1.5 type 1: TU12/VT2 type 2: reserved 3: reserved"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHVtRxMapTug25Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "PDHVtRxMapTug25Type"
            
            def description(self):
                return "Define types of VT/TUs in TUG-2-5"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHVtRxMapTug24Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PDHVtRxMapTug24Type"
            
            def description(self):
                return "Define types of VT/TUs in TUG-2-4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHVtRxMapTug23Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PDHVtRxMapTug23Type"
            
            def description(self):
                return "Define types of VT/TUs in TUG-2-3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHVtRxMapTug22Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PDHVtRxMapTug22Type"
            
            def description(self):
                return "Define types of VT/TUs in TUG-2-2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHVtRxMapTug21Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PDHVtRxMapTug21Type"
            
            def description(self):
                return "Define types of VT/TUs in TUG-2-1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHVtRxMapTug20Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDHVtRxMapTug20Type"
            
            def description(self):
                return "Define types of VT/TUs in TUG-2-0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDHVtRxMapTug26Type"] = _AF6CCI0011_RD_PDH._rxm12e12_stsvc_payload_ctrl._PDHVtRxMapTug26Type()
            allFields["PDHVtRxMapTug25Type"] = _AF6CCI0011_RD_PDH._rxm12e12_stsvc_payload_ctrl._PDHVtRxMapTug25Type()
            allFields["PDHVtRxMapTug24Type"] = _AF6CCI0011_RD_PDH._rxm12e12_stsvc_payload_ctrl._PDHVtRxMapTug24Type()
            allFields["PDHVtRxMapTug23Type"] = _AF6CCI0011_RD_PDH._rxm12e12_stsvc_payload_ctrl._PDHVtRxMapTug23Type()
            allFields["PDHVtRxMapTug22Type"] = _AF6CCI0011_RD_PDH._rxm12e12_stsvc_payload_ctrl._PDHVtRxMapTug22Type()
            allFields["PDHVtRxMapTug21Type"] = _AF6CCI0011_RD_PDH._rxm12e12_stsvc_payload_ctrl._PDHVtRxMapTug21Type()
            allFields["PDHVtRxMapTug20Type"] = _AF6CCI0011_RD_PDH._rxm12e12_stsvc_payload_ctrl._PDHVtRxMapTug20Type()
            return allFields

    class _rxm12e12_hw_stat(AtRegister.AtRegister):
        def name(self):
            return "RxM12E12 HW Status"
    
        def description(self):
            return "for HW debug only"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00041200 +  8*de3id + de2id"
            
        def startAddress(self):
            return 0x00041200
            
        def endAddress(self):
            return 0x000412fe

        class _RxM12E12Status(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxM12E12Status"
            
            def description(self):
                return "for HW debug only"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxM12E12Status"] = _AF6CCI0011_RD_PDH._rxm12e12_hw_stat._RxM12E12Status()
            return allFields

    class _RxM12E12_chn_intr_cfg(AtRegister.AtRegister):
        def name(self):
            return "RxM12E12 Framer Channel Interrupt Enable Control"
    
        def description(self):
            return "This is the Channel interrupt enable of DS2/E2 Rx Framer."
            
        def width(self):
            return 12
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000413fe
            
        def endAddress(self):
            return 0xffffffff

        class _RxM12E12IntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxM12E12IntrEn"
            
            def description(self):
                return "Each bit correspond with each channel DS2/E2 in DS3/E3 0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxM12E12IntrEn"] = _AF6CCI0011_RD_PDH._RxM12E12_chn_intr_cfg._RxM12E12IntrEn()
            return allFields

    class _RxM12E12_chn_intr_stat(AtRegister.AtRegister):
        def name(self):
            return "RxM12E12 Framer Channel Interrupt Status"
    
        def description(self):
            return "This is the Channel interrupt tus of DS2/E2 Rx Framer."
            
        def width(self):
            return 7
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000413ff
            
        def endAddress(self):
            return 0xffffffff

        class _RxM12E12IntrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxM12E12IntrSta"
            
            def description(self):
                return "Each bit correspond with each channel DS2/E2 in DS3/E3 0: No Interrupt 1: Have Interrupt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxM12E12IntrSta"] = _AF6CCI0011_RD_PDH._RxM12E12_chn_intr_stat._RxM12E12IntrSta()
            return allFields

    class _RxM12E12_per_chn_intr_cfg(AtRegister.AtRegister):
        def name(self):
            return "RxM12E12 Framer per Channel Interrupt Enable Control"
    
        def description(self):
            return "This is the per Channel interrupt enable of DS2/E2 Rx Framer."
            
        def width(self):
            return 7
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00041380 +  de3id"
            
        def startAddress(self):
            return 0x00041380
            
        def endAddress(self):
            return 0x0004139f

        class _DE2LOFIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE2LOFIntrEn"
            
            def description(self):
                return "Each bit correspond with each channel DS2/E2 in DS3/E3 0: No Interrupt 1: Have Interrupt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE2LOFIntrEn"] = _AF6CCI0011_RD_PDH._RxM12E12_per_chn_intr_cfg._DE2LOFIntrEn()
            return allFields

    class _RxM12E12_per_chn_intr_stat(AtRegister.AtRegister):
        def name(self):
            return "RxM12E12 Framer per Channel Interrupt Change Status"
    
        def description(self):
            return "This is the per Channel interrupt status of DS2/E2 Rx Framer."
            
        def width(self):
            return 7
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return "0x000413A0 +  de3id"
            
        def startAddress(self):
            return 0x000413a0
            
        def endAddress(self):
            return 0x000413bf

        class _DE2LOFIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE2LOFIntr"
            
            def description(self):
                return "Each bit correspond with each channel DS2/E2 in DS3/E3 0: No Interrupt 1: Have Interrupt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE2LOFIntr"] = _AF6CCI0011_RD_PDH._RxM12E12_per_chn_intr_stat._DE2LOFIntr()
            return allFields

    class _RxM12E12_per_chn_curr_stat(AtRegister.AtRegister):
        def name(self):
            return "RxM12E12 Framer per Channel Current Status"
    
        def description(self):
            return "This is the per Channel Current status of DS2/E2 Rx Framer."
            
        def width(self):
            return 7
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000413C0 +  de3id"
            
        def startAddress(self):
            return 0x000413c0
            
        def endAddress(self):
            return 0x000413df

        class _DE2LOFCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE2LOFCurrSta"
            
            def description(self):
                return "Each bit correspond with each channel DS2/E2 in DS3/E3 0: No Interrupt 1: Have Interrupt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE2LOFCurrSta"] = _AF6CCI0011_RD_PDH._RxM12E12_per_chn_curr_stat._DE2LOFCurrSta()
            return allFields

    class _RxM12E12_glb_cfg(AtRegister.AtRegister):
        def name(self):
            return "RxM12E12 Global Control"
    
        def description(self):
            return "This is the Channel interrupt enable of DS2/E2 Rx Framer."
            
        def width(self):
            return 12
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00041340
            
        def endAddress(self):
            return 0xffffffff

        class _RxM12E12NobMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxM12E12NobMode"
            
            def description(self):
                return "Number of bit mode for DCR Tx Shapper 0: Bit mode 1: Byte mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxM12E12NobMode"] = _AF6CCI0011_RD_PDH._RxM12E12_glb_cfg._RxM12E12NobMode()
            return allFields

    class _dej1_fbe_thrsh_cfg(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer FBE Threshold Control"
    
        def description(self):
            return "This is the FBE threshold configuration register for the PDH DS1/E1/J1 Rx framer"
            
        def width(self):
            return 8
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00050003
            
        def endAddress(self):
            return 0x00050003

        class _RxDE1FbeThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1FbeThres"
            
            def description(self):
                return "Threshold to generate interrupt if the FBE counter exceed this threshold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1FbeThres"] = _AF6CCI0011_RD_PDH._dej1_fbe_thrsh_cfg._RxDE1FbeThres()
            return allFields

    class _dej1_rx_crc_thrsh_cfg(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer CRC Threshold Control"
    
        def description(self):
            return "This is the CRC threshold configuration register for the PDH DS1/E1/J1 Rx framer"
            
        def width(self):
            return 8
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00050004
            
        def endAddress(self):
            return 0x00050004

        class _RxDE1CrcThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1CrcThres"
            
            def description(self):
                return "Threshold to generate interrupt if the CRC counter exceed this threshold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1CrcThres"] = _AF6CCI0011_RD_PDH._dej1_rx_crc_thrsh_cfg._RxDE1CrcThres()
            return allFields

    class _dej1_rei_thrsh_cfg(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer REI Threshold Control"
    
        def description(self):
            return "This is the REI threshold configuration register for the PDH DS1/E1/J1 Rx framer"
            
        def width(self):
            return 8
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00050005
            
        def endAddress(self):
            return 0x00050005

        class _RxDE1ReiThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1ReiThres"
            
            def description(self):
                return "Threshold to generate interrupt if the FBE counter exceed this threshold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1ReiThres"] = _AF6CCI0011_RD_PDH._dej1_rei_thrsh_cfg._RxDE1ReiThres()
            return allFields

    class _dej1_rx_framer_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer Control"
    
        def description(self):
            return "DS1/E1/J1 Rx framer control is used to configure for Frame mode (DS1, E1, J1) at the DS1/E1 framer."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00054000 +  32*de3id + 4*de2id + de1id"
            
        def startAddress(self):
            return 0x00054000
            
        def endAddress(self):
            return 0x000543ff

        class _RxDE1LOFFrwDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "RxDE1LOFFrwDis"
            
            def description(self):
                return "Forward DE1 LOF to generate L-bit to PSN in case of MonOnly, set 1 to disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1FrcAISLBit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "RxDE1FrcAISLBit"
            
            def description(self):
                return "Force AIS alarm here,L-bit, data all one to PSN when Line/Payload Remote Loopback"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1FrcAISLineDwn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "RxDE1FrcAISLineDwn"
            
            def description(self):
                return "Set 1 to force AIS Line DownStream"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1FrcAISPldDwn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "RxDE1FrcAISPldDwn"
            
            def description(self):
                return "Set 1 to force AIS payload DownStream"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1FrcLos(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "RxDE1FrcLos"
            
            def description(self):
                return "Set 1 to force LOS"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1LomfDstrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "RxDE1LomfDstrEn"
            
            def description(self):
                return "Set 1 to generate AIS forwarding dowstream in case E1CRC LOMF defect is present."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1LosCntThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 23
        
            def name(self):
                return "RxDE1LosCntThres"
            
            def description(self):
                return "Threshold to detect LOS all one, Set 0 to disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1MonOnly(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "RxDE1MonOnly"
            
            def description(self):
                return "Set 1 to enable the Monitor Only mode at the RXDS1/E1 framer"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1LocalLineLoop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "RxDE1LocalLineLoop"
            
            def description(self):
                return "Set 1 to enable Local Line loop back"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDe1LocalPayLoop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "RxDe1LocalPayLoop"
            
            def description(self):
                return "Set 1 to enable Local Payload loop back. Sharing for VT Loopback in case of CEP path"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1E1SaCfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 17
        
            def name(self):
                return "RxDE1E1SaCfg"
            
            def description(self):
                return "Incase E1 Sa used for FDL configuration 0x0: No Sa bit used 0x3: Sa4 is used for SSM 0x4: Sa5 is used for SSM 0x5: Sa6 is used for SSM 0x6: Sa7 is used for SSM 0x7: Sa8 is used for SSM Incase DS1 bit[19:18] dont care bit [17]   set \" 1\" to enable Rx DLK"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1LofThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 13
        
            def name(self):
                return "RxDE1LofThres"
            
            def description(self):
                return "Threshold for FAS error to declare LOF in DS1/E1/J1 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1AisCntThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 10
        
            def name(self):
                return "RxDE1AisCntThres"
            
            def description(self):
                return "Threshold to detect AIS all one, Set 0 to disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1RaiCntThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 7
        
            def name(self):
                return "RxDE1RaiCntThres"
            
            def description(self):
                return "Threshold of RAI monitoring block"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1AisMaskEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "RxDE1AisMaskEn"
            
            def description(self):
                return "Set 1 to enable output data of the DS1/E1/J1 framer to be set to all one (1) during LOF"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "RxDE1En"
            
            def description(self):
                return "Set 1 to enable the DS1/E1/J1 framer."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1FrcLof(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RxDE1FrcLof"
            
            def description(self):
                return "Set 1 to force Re-frame"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDs1Md(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDs1Md"
            
            def description(self):
                return "Receive DS1/J1 framing mode 0000: DS1/J1 Unframe 0001: DS1 SF (D4) 0010: DS1 ESF 0011: DS1 DDS 0100: DS1 SLC 0101: J1 SF 0110: J1 ESF 1000: E1 Unframe 1001: E1 Basic Frame 1010: E1 CRC4 Frame"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1LOFFrwDis"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_ctrl._RxDE1LOFFrwDis()
            allFields["RxDE1FrcAISLBit"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_ctrl._RxDE1FrcAISLBit()
            allFields["RxDE1FrcAISLineDwn"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_ctrl._RxDE1FrcAISLineDwn()
            allFields["RxDE1FrcAISPldDwn"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_ctrl._RxDE1FrcAISPldDwn()
            allFields["RxDE1FrcLos"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_ctrl._RxDE1FrcLos()
            allFields["RxDE1LomfDstrEn"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_ctrl._RxDE1LomfDstrEn()
            allFields["RxDE1LosCntThres"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_ctrl._RxDE1LosCntThres()
            allFields["RxDE1MonOnly"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_ctrl._RxDE1MonOnly()
            allFields["RxDE1LocalLineLoop"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_ctrl._RxDE1LocalLineLoop()
            allFields["RxDe1LocalPayLoop"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_ctrl._RxDe1LocalPayLoop()
            allFields["RxDE1E1SaCfg"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_ctrl._RxDE1E1SaCfg()
            allFields["RxDE1LofThres"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_ctrl._RxDE1LofThres()
            allFields["RxDE1AisCntThres"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_ctrl._RxDE1AisCntThres()
            allFields["RxDE1RaiCntThres"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_ctrl._RxDE1RaiCntThres()
            allFields["RxDE1AisMaskEn"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_ctrl._RxDE1AisMaskEn()
            allFields["RxDE1En"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_ctrl._RxDE1En()
            allFields["RxDE1FrcLof"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_ctrl._RxDE1FrcLof()
            allFields["RxDs1Md"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_ctrl._RxDs1Md()
            return allFields

    class _dej1_rx_framer_hw_stat(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer HW Status"
    
        def description(self):
            return "These registers are used for Hardware tus only"
            
        def width(self):
            return 7
        
        def type(self):
            return "Stat"
            
        def fomular(self):
            return "0x00054400 +  32*de3id + 4*de2id + de1id"
            
        def startAddress(self):
            return 0x00054400
            
        def endAddress(self):
            return 0x000547ff

        class _RxDS1E1_CrrAISDownStr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "RxDS1E1_CrrAISDownStr"
            
            def description(self):
                return "AIS Down Stream due to HI_Level AIS of DS1E1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS1E1_CrrLosAllZero(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "RxDS1E1_CrrLosAllZero"
            
            def description(self):
                return "LOS All zero of DS1E1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS1E1_CrrAISAllOne(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RxDS1E1_CrrAISAllOne"
            
            def description(self):
                return "AIS All one of DS1E1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1Sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1Sta"
            
            def description(self):
                return "RxDE1Sta 000: Search 001: Shift 010: Wait 011: Verify 100: Fs search 101: Inframe"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDS1E1_CrrAISDownStr"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_hw_stat._RxDS1E1_CrrAISDownStr()
            allFields["RxDS1E1_CrrLosAllZero"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_hw_stat._RxDS1E1_CrrLosAllZero()
            allFields["RxDS1E1_CrrAISAllOne"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_hw_stat._RxDS1E1_CrrAISAllOne()
            allFields["RxDE1Sta"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_hw_stat._RxDE1Sta()
            return allFields

    class _dej1_rx_framer_crcerr_cnt(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer CRC Error Counter"
    
        def description(self):
            return "This is the per channel CRC error counter for DS1/E1/J1 receive framer"
            
        def width(self):
            return 8
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00056800 +  1024*Rd2Clr + 32*de3id + 4*de2id + de1id"
            
        def startAddress(self):
            return 0x00056800
            
        def endAddress(self):
            return 0x00056fff

        class _DE1CrcErrCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE1CrcErrCnt"
            
            def description(self):
                return "DS1/E1/J1 CRC Error Accumulator"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE1CrcErrCnt"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_crcerr_cnt._DE1CrcErrCnt()
            return allFields

    class _dej1_rx_framer_rei_cnt(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer REI Counter"
    
        def description(self):
            return "This is the per channel REI counter for DS1/E1/J1 receive framer"
            
        def width(self):
            return 8
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00057000 +  1024*Rd2Clr + 32*de3id + 4*de2id + de1id"
            
        def startAddress(self):
            return 0x00057000
            
        def endAddress(self):
            return 0x000577ff

        class _DE1ReiCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE1ReiCnt"
            
            def description(self):
                return "DS1/E1/J1 REI error accumulator"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE1ReiCnt"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_rei_cnt._DE1ReiCnt()
            return allFields

    class _dej1_rx_framer_fbe_cnt(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer FBE Counter"
    
        def description(self):
            return "This is the per channel REI counter for DS1/E1/J1 receive framer"
            
        def width(self):
            return 8
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00056000 +  1024*Rd2Clr + 32*de3id + 4*de2id + de1id"
            
        def startAddress(self):
            return 0x00056000
            
        def endAddress(self):
            return 0x000567ff

        class _DE1FbeCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE1FbeCnt"
            
            def description(self):
                return "DS1/E1/J1 F-bit error accumulator"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE1FbeCnt"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_fbe_cnt._DE1FbeCnt()
            return allFields

    class _dej1_rx_framer_per_chn_intr_en_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer per Channel Interrupt Enable Control"
    
        def description(self):
            return "This is the per Channel interrupt enable of DS1/E1/J1 Rx Framer."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00055000 +  StsID*32 + Tug2ID*4 + VtnID"
            
        def startAddress(self):
            return 0x00055000
            
        def endAddress(self):
            return 0x000553ff

        class _DE1BerSdIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "DE1BerSdIntrEn"
            
            def description(self):
                return "Set 1 to enable the BER-SD interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1BerSfIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "DE1BerSfIntrEn"
            
            def description(self):
                return "Set 1 to enable the BER-SF interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1oblcIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "DE1oblcIntrEn"
            
            def description(self):
                return "Set 1 to enable the new Outband LoopCode detected to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1iblcIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "DE1iblcIntrEn"
            
            def description(self):
                return "Set 1 to enable the new Inband LoopCode detected to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1SigRAIIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "DE1SigRAIIntrEn"
            
            def description(self):
                return "Set 1 to enable Signalling RAI event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1SigLOFIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "DE1SigLOFIntrEn"
            
            def description(self):
                return "Set 1 to enable Signalling LOF event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1FbeIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "DE1FbeIntrEn"
            
            def description(self):
                return "Set 1 to enable change FBE te event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1CrcIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "DE1CrcIntrEn"
            
            def description(self):
                return "Set 1 to enable change CRC te event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1ReiIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "DE1ReiIntrEn"
            
            def description(self):
                return "Set 1 to enable change REI te event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1LomfIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "DE1LomfIntrEn"
            
            def description(self):
                return "Set 1 to enable change LOMF te event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1LosIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "DE1LosIntrEn"
            
            def description(self):
                return "Set 1 to enable change LOS te event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1AisIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "DE1AisIntrEn"
            
            def description(self):
                return "Set 1 to enable change AIS te event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1RaiIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "DE1RaiIntrEn"
            
            def description(self):
                return "Set 1 to enable change RAI te event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1LofIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE1LofIntrEn"
            
            def description(self):
                return "Set 1 to enable change LOF te event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE1BerSdIntrEn"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_intr_en_ctrl._DE1BerSdIntrEn()
            allFields["DE1BerSfIntrEn"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_intr_en_ctrl._DE1BerSfIntrEn()
            allFields["DE1oblcIntrEn"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_intr_en_ctrl._DE1oblcIntrEn()
            allFields["DE1iblcIntrEn"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_intr_en_ctrl._DE1iblcIntrEn()
            allFields["DE1SigRAIIntrEn"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_intr_en_ctrl._DE1SigRAIIntrEn()
            allFields["DE1SigLOFIntrEn"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_intr_en_ctrl._DE1SigLOFIntrEn()
            allFields["DE1FbeIntrEn"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_intr_en_ctrl._DE1FbeIntrEn()
            allFields["DE1CrcIntrEn"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_intr_en_ctrl._DE1CrcIntrEn()
            allFields["DE1ReiIntrEn"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_intr_en_ctrl._DE1ReiIntrEn()
            allFields["DE1LomfIntrEn"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_intr_en_ctrl._DE1LomfIntrEn()
            allFields["DE1LosIntrEn"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_intr_en_ctrl._DE1LosIntrEn()
            allFields["DE1AisIntrEn"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_intr_en_ctrl._DE1AisIntrEn()
            allFields["DE1RaiIntrEn"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_intr_en_ctrl._DE1RaiIntrEn()
            allFields["DE1LofIntrEn"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_intr_en_ctrl._DE1LofIntrEn()
            return allFields

    class _dej1_rx_framer_per_chn_intr_stat(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer per Channel Interrupt Status"
    
        def description(self):
            return "This is the per Channel interrupt tus of DS1/E1/J1 Rx Framer."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00055400 +  StsID*32 + Tug2ID*4 + VtnID"
            
        def startAddress(self):
            return 0x00055400
            
        def endAddress(self):
            return 0x000557ff

        class _DE1BerSdIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "DE1BerSdIntr"
            
            def description(self):
                return "Set 1 if there is a change of BER-SD."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1BerSfIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "DE1BerSfIntr"
            
            def description(self):
                return "Set 1 if there is a change of BER-SF."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1oblcIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "DE1oblcIntr"
            
            def description(self):
                return "Set 1 if there is a change the new Outband LoopCode detected to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1iblcIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "DE1iblcIntr"
            
            def description(self):
                return "Set 1 if there is a change the new Inband LoopCode detected to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1SigRAIIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "DE1SigRAIIntr"
            
            def description(self):
                return "Set 1 if there is a change Signalling RAI event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1SigLOFIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "DE1SigLOFIntr"
            
            def description(self):
                return "Set 1 if there is a change Signalling LOF event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1FbeIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "DE1FbeIntr"
            
            def description(self):
                return "Set 1 if there is a change in FBE exceed threshold te event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1CrcIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "DE1CrcIntr"
            
            def description(self):
                return "Set 1 if there is a change in CRC exceed threshold te event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1ReiIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "DE1ReiIntr"
            
            def description(self):
                return "Set 1 if there is a change in REI exceed threshold te event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1LomfIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "DE1LomfIntr"
            
            def description(self):
                return "Set 1 if there is a change in LOMF te event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1LosIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "DE1LosIntr"
            
            def description(self):
                return "Set 1 if there is a change in LOS te event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1AisIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "DE1AisIntr"
            
            def description(self):
                return "Set 1 if there is a change in AIS te event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1RaiIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "DE1RaiIntr"
            
            def description(self):
                return "Set 1 if there is a change in RAI te event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1LofIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE1LofIntr"
            
            def description(self):
                return "Set 1 if there is a change in LOF te event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE1BerSdIntr"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_intr_stat._DE1BerSdIntr()
            allFields["DE1BerSfIntr"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_intr_stat._DE1BerSfIntr()
            allFields["DE1oblcIntr"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_intr_stat._DE1oblcIntr()
            allFields["DE1iblcIntr"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_intr_stat._DE1iblcIntr()
            allFields["DE1SigRAIIntr"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_intr_stat._DE1SigRAIIntr()
            allFields["DE1SigLOFIntr"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_intr_stat._DE1SigLOFIntr()
            allFields["DE1FbeIntr"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_intr_stat._DE1FbeIntr()
            allFields["DE1CrcIntr"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_intr_stat._DE1CrcIntr()
            allFields["DE1ReiIntr"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_intr_stat._DE1ReiIntr()
            allFields["DE1LomfIntr"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_intr_stat._DE1LomfIntr()
            allFields["DE1LosIntr"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_intr_stat._DE1LosIntr()
            allFields["DE1AisIntr"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_intr_stat._DE1AisIntr()
            allFields["DE1RaiIntr"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_intr_stat._DE1RaiIntr()
            allFields["DE1LofIntr"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_intr_stat._DE1LofIntr()
            return allFields

    class _dej1_rx_framer_per_chn_curr_stat(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer per Channel Current Status"
    
        def description(self):
            return "This is the per Channel Current tus of DS1/E1/J1 Rx Framer."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00055800 +  StsID*32 + Tug2ID*4 + VtnID"
            
        def startAddress(self):
            return 0x00055800
            
        def endAddress(self):
            return 0x00055bff

        class _DE1BerSdIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "DE1BerSdIntr"
            
            def description(self):
                return "Set 1 if there is a change of BER-SD."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1BerSfIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "DE1BerSfIntr"
            
            def description(self):
                return "Set 1 if there is a change of BER-SF."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1oblcIntrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "DE1oblcIntrSta"
            
            def description(self):
                return "Current status the new Outband LoopCode detected to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1iblcIntrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "DE1iblcIntrSta"
            
            def description(self):
                return "Current status the new Inband LoopCode detected to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1SigRAIIntrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "DE1SigRAIIntrSta"
            
            def description(self):
                return "Current status Signalling RAI event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1SigLOFIntrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "DE1SigLOFIntrSta"
            
            def description(self):
                return "Current status Signalling LOF event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1FbeCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "DE1FbeCurrSta"
            
            def description(self):
                return "Current tus of FBE exceed threshold event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1CrcCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "DE1CrcCurrSta"
            
            def description(self):
                return "Current tus of CRC exceed threshold event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1ReiCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "DE1ReiCurrSta"
            
            def description(self):
                return "Current tus of REI exceed threshold event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1LomfCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "DE1LomfCurrSta"
            
            def description(self):
                return "Current tus of LOMF event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1LosCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "DE1LosCurrSta"
            
            def description(self):
                return "Current tus of LOS event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1AisCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "DE1AisCurrSta"
            
            def description(self):
                return "Current tus of AIS event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1RaiCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "DE1RaiCurrSta"
            
            def description(self):
                return "Current tus of RAI event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1LofCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE1LofCurrSta"
            
            def description(self):
                return "Current tus of LOF event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE1BerSdIntr"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_curr_stat._DE1BerSdIntr()
            allFields["DE1BerSfIntr"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_curr_stat._DE1BerSfIntr()
            allFields["DE1oblcIntrSta"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_curr_stat._DE1oblcIntrSta()
            allFields["DE1iblcIntrSta"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_curr_stat._DE1iblcIntrSta()
            allFields["DE1SigRAIIntrSta"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_curr_stat._DE1SigRAIIntrSta()
            allFields["DE1SigLOFIntrSta"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_curr_stat._DE1SigLOFIntrSta()
            allFields["DE1FbeCurrSta"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_curr_stat._DE1FbeCurrSta()
            allFields["DE1CrcCurrSta"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_curr_stat._DE1CrcCurrSta()
            allFields["DE1ReiCurrSta"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_curr_stat._DE1ReiCurrSta()
            allFields["DE1LomfCurrSta"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_curr_stat._DE1LomfCurrSta()
            allFields["DE1LosCurrSta"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_curr_stat._DE1LosCurrSta()
            allFields["DE1AisCurrSta"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_curr_stat._DE1AisCurrSta()
            allFields["DE1RaiCurrSta"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_curr_stat._DE1RaiCurrSta()
            allFields["DE1LofCurrSta"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_curr_stat._DE1LofCurrSta()
            return allFields

    class _dej1_rx_framer_per_chn_intr_or_stat(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer per Channel Interrupt OR Status"
    
        def description(self):
            return "The register consists of 28 bits for 28 VT/TUs of the related STS/VC in the Rx DS1/E1/J1 Framer. Each bit is used to store Interrupt OR tus of the related DS1/E1/J1."
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return "0x00055C00 +  StsID"
            
        def startAddress(self):
            return 0x00055c00
            
        def endAddress(self):
            return 0x00055c1f

        class _RxDE1VtIntrOrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1VtIntrOrSta"
            
            def description(self):
                return "Set to 1 if any interrupt tus bit of corresponding DS1/E1/J1 is set and its interrupt is enabled."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1VtIntrOrSta"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_chn_intr_or_stat._RxDE1VtIntrOrSta()
            return allFields

    class _dej1_rx_framer_per_stsvc_intr_or_stat(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer per STS/VC Interrupt OR Status"
    
        def description(self):
            return "The register consists of 12 bits for 12 STS/VCs of the Rx DS1/E1/J1 Framer. Each bit is used to store Interrupt OR tus of the related STS/VC."
            
        def width(self):
            return 24
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00055fff
            
        def endAddress(self):
            return 0xffffffff

        class _RxDE1StsIntrOrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1StsIntrOrSta"
            
            def description(self):
                return "Set to 1 if any interrupt tus bit of corresponding STS/VC is set and its interrupt is enabled"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1StsIntrOrSta"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_stsvc_intr_or_stat._RxDE1StsIntrOrSta()
            return allFields

    class _dej1_rx_framer_per_stsvc_intr_en_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer per STS/VC Interrupt Enable Control"
    
        def description(self):
            return "The register consists of 12 interrupt enable bits for 12 STS/VCs in the Rx DS1/E1/J1 Framer."
            
        def width(self):
            return 24
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00055ffe
            
        def endAddress(self):
            return 0xffffffff

        class _RxDE1StsIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1StsIntrEn"
            
            def description(self):
                return "Set to 1 to enable the related STS/VC to generate interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1StsIntrEn"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_per_stsvc_intr_en_ctrl._RxDE1StsIntrEn()
            return allFields

    class _dej1_rx_framer_dlk_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer DLK Control"
    
        def description(self):
            return "The register is used to configuration for  Rx DS1/E1/J1 Framer DLK."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000d8000
            
        def endAddress(self):
            return 0x000d82f7

        class _RxDE1DlkStkThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 11
        
            def name(self):
                return "RxDE1DlkStkThres"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkBomThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 6
        
            def name(self):
                return "RxDE1DlkBomThres"
            
            def description(self):
                return "Threshold to generate interrupt if the BOM counter exceed this threshold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkFcsInvEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "RxDE1DlkFcsInvEn"
            
            def description(self):
                return "Set 1 to enable FCS invert."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkPayInvEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RxDE1DlkPayInvEn"
            
            def description(self):
                return "Set 1 to enable payload invert."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkFcsEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "RxDE1DlkFcsEn"
            
            def description(self):
                return "reserved"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkByteStuff(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "RxDE1DlkByteStuff"
            
            def description(self):
                return "reserved"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkMsbLsb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "RxDE1DlkMsbLsb"
            
            def description(self):
                return "set 1 to enable MSB and LSB convert"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1DlkEn"
            
            def description(self):
                return "Set 1 to enable monitor DS1/E1/J1 Rx DLK"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1DlkStkThres"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_ctrl._RxDE1DlkStkThres()
            allFields["RxDE1DlkBomThres"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_ctrl._RxDE1DlkBomThres()
            allFields["RxDE1DlkFcsInvEn"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_ctrl._RxDE1DlkFcsInvEn()
            allFields["RxDE1DlkPayInvEn"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_ctrl._RxDE1DlkPayInvEn()
            allFields["RxDE1DlkFcsEn"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_ctrl._RxDE1DlkFcsEn()
            allFields["RxDE1DlkByteStuff"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_ctrl._RxDE1DlkByteStuff()
            allFields["RxDE1DlkMsbLsb"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_ctrl._RxDE1DlkMsbLsb()
            allFields["RxDE1DlkEn"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_ctrl._RxDE1DlkEn()
            return allFields

    class _dej1_rx_framer_dlk_stat(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer DLK Status"
    
        def description(self):
            return "The register is used to configuration for  Rx DS1/E1/J1 Framer DLK. For HW use only"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000d9000
            
        def endAddress(self):
            return 0x000d92f7

        class _RxDE1DlkNewBomCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "RxDE1DlkNewBomCnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkNewCacheByteCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "RxDE1DlkNewCacheByteCnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkNewDataStoreEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "RxDE1DlkNewDataStoreEn"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkNewDataStore(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "RxDE1DlkNewDataStore"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkNewCacheFill(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "RxDE1DlkNewCacheFill"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkNewMsgCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "RxDE1DlkNewMsgCnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkNewRdAdd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "RxDE1DlkNewRdAdd"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkNewWrAdd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "RxDE1DlkNewWrAdd"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkNewFcs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "RxDE1DlkNewFcs"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkNewMsgShift(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "RxDE1DlkNewMsgShift"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkNewBitCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "RxDE1DlkNewBitCnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkNewStuffCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "RxDE1DlkNewStuffCnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkNewFlagOff(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "RxDE1DlkNewFlagOff"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkNewByte3Store(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RxDE1DlkNewByte3Store"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkNewByte2Store(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "RxDE1DlkNewByte2Store"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkNewByte1Store(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "RxDE1DlkNewByte1Store"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkNewByteShf(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "RxDE1DlkNewByteShf"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkNewState(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1DlkNewState"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1DlkNewBomCnt"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_stat._RxDE1DlkNewBomCnt()
            allFields["RxDE1DlkNewCacheByteCnt"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_stat._RxDE1DlkNewCacheByteCnt()
            allFields["RxDE1DlkNewDataStoreEn"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_stat._RxDE1DlkNewDataStoreEn()
            allFields["RxDE1DlkNewDataStore"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_stat._RxDE1DlkNewDataStore()
            allFields["RxDE1DlkNewCacheFill"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_stat._RxDE1DlkNewCacheFill()
            allFields["RxDE1DlkNewMsgCnt"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_stat._RxDE1DlkNewMsgCnt()
            allFields["RxDE1DlkNewRdAdd"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_stat._RxDE1DlkNewRdAdd()
            allFields["RxDE1DlkNewWrAdd"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_stat._RxDE1DlkNewWrAdd()
            allFields["RxDE1DlkNewFcs"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_stat._RxDE1DlkNewFcs()
            allFields["RxDE1DlkNewMsgShift"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_stat._RxDE1DlkNewMsgShift()
            allFields["RxDE1DlkNewBitCnt"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_stat._RxDE1DlkNewBitCnt()
            allFields["RxDE1DlkNewStuffCnt"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_stat._RxDE1DlkNewStuffCnt()
            allFields["RxDE1DlkNewFlagOff"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_stat._RxDE1DlkNewFlagOff()
            allFields["RxDE1DlkNewByte3Store"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_stat._RxDE1DlkNewByte3Store()
            allFields["RxDE1DlkNewByte2Store"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_stat._RxDE1DlkNewByte2Store()
            allFields["RxDE1DlkNewByte1Store"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_stat._RxDE1DlkNewByte1Store()
            allFields["RxDE1DlkNewByteShf"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_stat._RxDE1DlkNewByteShf()
            allFields["RxDE1DlkNewState"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_stat._RxDE1DlkNewState()
            return allFields

    class _dej1_rx_framer_dlk_flush_msg_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer DLK Flush Message"
    
        def description(self):
            return "This register is used to flush all messages of a channel."
            
        def width(self):
            return 12
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000d0011
            
        def endAddress(self):
            return 0xffffffff

        class _RxDE1DlkFlushEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "RxDE1DlkFlushEn"
            
            def description(self):
                return "Set 1 to enable flush buffer."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkChid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1DlkChid"
            
            def description(self):
                return "Channel DLK which is flushed."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1DlkFlushEn"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_flush_msg_ctrl._RxDE1DlkFlushEn()
            allFields["RxDE1DlkChid"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_flush_msg_ctrl._RxDE1DlkChid()
            return allFields

    class _dej1_framer_clk_acc_ddr_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer DLK Access DDR Control"
    
        def description(self):
            return "."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000d0010
            
        def endAddress(self):
            return 0xffffffff

        class _RxDE1DlkDDRReady(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "RxDE1DlkDDRReady"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkChid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1DlkChid"
            
            def description(self):
                return "Channel DLK which is read."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1DlkDDRReady"] = _AF6CCI0011_RD_PDH._dej1_framer_clk_acc_ddr_ctrl._RxDE1DlkDDRReady()
            allFields["RxDE1DlkChid"] = _AF6CCI0011_RD_PDH._dej1_framer_clk_acc_ddr_ctrl._RxDE1DlkChid()
            return allFields

    class _dej1_rx_framer_clk_msg0(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer DLK Message0"
    
        def description(self):
            return "."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000d0016
            
        def endAddress(self):
            return 0xffffffff

        class _RxDE1DlkCache1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 24
        
            def name(self):
                return "RxDE1DlkCache1"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkByte1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "RxDE1DlkByte1"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkCache0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 8
        
            def name(self):
                return "RxDE1DlkCache0"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkByte0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1DlkByte0"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1DlkCache1"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_clk_msg0._RxDE1DlkCache1()
            allFields["RxDE1DlkByte1"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_clk_msg0._RxDE1DlkByte1()
            allFields["RxDE1DlkCache0"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_clk_msg0._RxDE1DlkCache0()
            allFields["RxDE1DlkByte0"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_clk_msg0._RxDE1DlkByte0()
            return allFields

    class _dej1_rx_framer_msg1(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer DLK Message1"
    
        def description(self):
            return "."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000d0017
            
        def endAddress(self):
            return 0xffffffff

        class _RxDE1DlkCache3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 24
        
            def name(self):
                return "RxDE1DlkCache3"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkByte3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "RxDE1DlkByte3"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkCache2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 8
        
            def name(self):
                return "RxDE1DlkCache2"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkByte2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1DlkByte2"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1DlkCache3"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_msg1._RxDE1DlkCache3()
            allFields["RxDE1DlkByte3"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_msg1._RxDE1DlkByte3()
            allFields["RxDE1DlkCache2"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_msg1._RxDE1DlkCache2()
            allFields["RxDE1DlkByte2"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_msg1._RxDE1DlkByte2()
            return allFields

    class _dej1_rx_framer_dkl_msg2(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer DLK Message2"
    
        def description(self):
            return "."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000d0018
            
        def endAddress(self):
            return 0xffffffff

        class _RxDE1DlkCache5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 24
        
            def name(self):
                return "RxDE1DlkCache5"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkByte5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "RxDE1DlkByte5"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkCache4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 8
        
            def name(self):
                return "RxDE1DlkCache4"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkByte4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1DlkByte4"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1DlkCache5"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dkl_msg2._RxDE1DlkCache5()
            allFields["RxDE1DlkByte5"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dkl_msg2._RxDE1DlkByte5()
            allFields["RxDE1DlkCache4"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dkl_msg2._RxDE1DlkCache4()
            allFields["RxDE1DlkByte4"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dkl_msg2._RxDE1DlkByte4()
            return allFields

    class _dej1_rx_framer_dlk_msg3(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer DLK Message3"
    
        def description(self):
            return "."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000d0019
            
        def endAddress(self):
            return 0xffffffff

        class _RxDE1DlkCache7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 24
        
            def name(self):
                return "RxDE1DlkCache7"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkByte7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "RxDE1DlkByte7"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkCache6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 8
        
            def name(self):
                return "RxDE1DlkCache6"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkByte6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1DlkByte6"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1DlkCache7"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_msg3._RxDE1DlkCache7()
            allFields["RxDE1DlkByte7"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_msg3._RxDE1DlkByte7()
            allFields["RxDE1DlkCache6"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_msg3._RxDE1DlkCache6()
            allFields["RxDE1DlkByte6"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_msg3._RxDE1DlkByte6()
            return allFields

    class _dej1_rx_framer_dlk_intr_en_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer DLK Interrupt Enable"
    
        def description(self):
            return "."
            
        def width(self):
            return 5
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000de000
            
        def endAddress(self):
            return 0xffffffff

        class _RxDE1DlkBOMEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RxDE1DlkBOMEn"
            
            def description(self):
                return "Set 1 if there is a the new BOM detected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkCacheErrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "RxDE1DlkCacheErrEn"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkAbortEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "RxDE1DlkAbortEn"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkOverThresEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "RxDE1DlkOverThresEn"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkNewMsgEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1DlkNewMsgEn"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1DlkBOMEn"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_intr_en_ctrl._RxDE1DlkBOMEn()
            allFields["RxDE1DlkCacheErrEn"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_intr_en_ctrl._RxDE1DlkCacheErrEn()
            allFields["RxDE1DlkAbortEn"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_intr_en_ctrl._RxDE1DlkAbortEn()
            allFields["RxDE1DlkOverThresEn"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_intr_en_ctrl._RxDE1DlkOverThresEn()
            allFields["RxDE1DlkNewMsgEn"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_intr_en_ctrl._RxDE1DlkNewMsgEn()
            return allFields

    class _dej1_rx_framer_dlk_intr_sticky(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer DLK Interrupt Sticky"
    
        def description(self):
            return "."
            
        def width(self):
            return 5
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000de400
            
        def endAddress(self):
            return 0xffffffff

        class _RxDE1DlkBOMIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RxDE1DlkBOMIntr"
            
            def description(self):
                return "Set 1 if there is a the new BOM detected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkCacheErrIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "RxDE1DlkCacheErrIntr"
            
            def description(self):
                return "Set 1 if there is a the new Cache Error detected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkAbortIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "RxDE1DlkAbortIntr"
            
            def description(self):
                return "Set 1 if there is a the new Abort detected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkOverThresIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "RxDE1DlkOverThresIntr"
            
            def description(self):
                return "Set 1 if there is a the new Over Threshold detected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1DlkNewMsgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1DlkNewMsgIntr"
            
            def description(self):
                return "Set 1 if there is a the new message detected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1DlkBOMIntr"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_intr_sticky._RxDE1DlkBOMIntr()
            allFields["RxDE1DlkCacheErrIntr"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_intr_sticky._RxDE1DlkCacheErrIntr()
            allFields["RxDE1DlkAbortIntr"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_intr_sticky._RxDE1DlkAbortIntr()
            allFields["RxDE1DlkOverThresIntr"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_intr_sticky._RxDE1DlkOverThresIntr()
            allFields["RxDE1DlkNewMsgIntr"] = _AF6CCI0011_RD_PDH._dej1_rx_framer_dlk_intr_sticky._RxDE1DlkNewMsgIntr()
            return allFields

    class _dej1_testpat_mon_glb_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Test Pattern Monitoring Global Control"
    
        def description(self):
            return "This register is used to global configuration for PTM block."
            
        def width(self):
            return 12
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00054900
            
        def endAddress(self):
            return 0x00054900

        class _RxDE3PtmLosThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "RxDE3PtmLosThr"
            
            def description(self):
                return "Threshold for Loss Of Sync detection. Status will go LOS if the number of error patterns (8 bits) in 16 consecutive patterns is more than or equal the RxDE3PtmLosThr."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE3PtmSynThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RxDE3PtmSynThr"
            
            def description(self):
                return "Threshold for Sync detection. Status will go SYN if the number of corrected consecutive patterns (8 bits) is more than or equal the RxDE3PtmLosThr."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1PtmSwap(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "RxDE1PtmSwap"
            
            def description(self):
                return "PTM swap Bit. If it is set to 1, pattern is shifted LSB first, then MSB. Normal is set to 0."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1PtmInv(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1PtmInv"
            
            def description(self):
                return "PTM inversion Bit."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE3PtmLosThr"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_glb_ctrl._RxDE3PtmLosThr()
            allFields["RxDE3PtmSynThr"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_glb_ctrl._RxDE3PtmSynThr()
            allFields["RxDE1PtmSwap"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_glb_ctrl._RxDE1PtmSwap()
            allFields["RxDE1PtmInv"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_glb_ctrl._RxDE1PtmInv()
            return allFields

    class _dej1_testpat_mon_sticky_en(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Test Pattern Monitoring Sticky Enable"
    
        def description(self):
            return "This register is used to configure the test pattern sticky mode"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00054901
            
        def endAddress(self):
            return 0x00054901

        class _RxDE1PtmStkEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1PtmStkEn"
            
            def description(self):
                return "Test Pattern Error Sticky Control"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1PtmStkEn"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_sticky_en._RxDE1PtmStkEn()
            return allFields

    class _dej1_testpat_mon_sticky(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Test Pattern Monitoring Sticky"
    
        def description(self):
            return "This register is used to be tus sticky report of 4 PTM engines."
            
        def width(self):
            return 16
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00054902
            
        def endAddress(self):
            return 0x00054902

        class _RxDE1PtmStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1PtmStk"
            
            def description(self):
                return "BERT Error Sticky"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1PtmStk"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_sticky._RxDE1PtmStk()
            return allFields

    class _dej1_testpat_mon_mode(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Test Pattern Monitoring Mode"
    
        def description(self):
            return "This register is used to data mode for PTM block."
            
        def width(self):
            return 10
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x054910 +  index"
            
        def startAddress(self):
            return 0x00054910
            
        def endAddress(self):
            return 0x0005491f

        class _RxDE1PtmPatBit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 5
        
            def name(self):
                return "RxDE1PtmPatBit"
            
            def description(self):
                return "The number of bit pattern is used."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1PtmMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1PtmMode"
            
            def description(self):
                return "Receive DS1/E1/J1 BERT mode 00000: Prbs9 00001: Prbs11 00010: Prbs15 00011: Prbs20 (X19 + X2 + 1) 00100: Prbs20 (X19 + X16 + 1) 00101: Qrss20 (X19 + X16 + 1) 00110: Prbs23 00111: DDS1 01000: DDS2 01001: DDS3 01010: DDS4 01011: DDS5 01100: Daly55 01101: Fix 3 in 24 01110: Fix 1 in 8 01111: Octet55 10000: Fix pattern n-bit 10001: Sequence 10010: Prbs7"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1PtmPatBit"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_mode._RxDE1PtmPatBit()
            allFields["RxDE1PtmMode"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_mode._RxDE1PtmMode()
            return allFields

    class _dej1_testpat_mon_fixed_pattern_cfg(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Test Pattern Monitoring Fixed Pattern"
    
        def description(self):
            return "This register is used to be expected data in fix pattern mode for engine #0 - 15"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x054920 +  index"
            
        def startAddress(self):
            return 0x00054920
            
        def endAddress(self):
            return 0x0005492f

        class _RxDE1FixPat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1FixPat"
            
            def description(self):
                return "Configurable fixed pattern for BERT monitoring."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1FixPat"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_fixed_pattern_cfg._RxDE1FixPat()
            return allFields

    class _dej1_testpat_mon_sel_chn_cfg(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Test Pattern Monitoring Selected Channel"
    
        def description(self):
            return "This register is used to control configuration for PTM engine #0 - 15"
            
        def width(self):
            return 10
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x054800 +  index"
            
        def startAddress(self):
            return 0x00054800
            
        def endAddress(self):
            return 0x0005480f

        class _RxDE1PtgEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "RxDE1PtgEn"
            
            def description(self):
                return "PTG Bit Enable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1PtmID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1PtmID"
            
            def description(self):
                return "Configure which channel in 336 DS1s/E1s/J1s is monitored."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1PtgEn"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_sel_chn_cfg._RxDE1PtgEn()
            allFields["RxDE1PtmID"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_sel_chn_cfg._RxDE1PtmID()
            return allFields

    class _dej1_testpat_mon_nxds0_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Test Pattern Monitoring nxDS0 Control"
    
        def description(self):
            return "These registers are used to control the mode of nxDS0 in a DS1/E1/J1"
            
        def width(self):
            return 3
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x054810 +  index"
            
        def startAddress(self):
            return 0x00054810
            
        def endAddress(self):
            return 0x0005481f

        class _RxDE1PtmFbitOVR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "RxDE1PtmFbitOVR"
            
            def description(self):
                return "F- bit overwrite"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1PtmDS0_6b(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "RxDE1PtmDS0_6b"
            
            def description(self):
                return "Receive Test Pattern from the 2nd bit to the 8th bit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1PtmDS0_7b(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1PtmDS0_7b"
            
            def description(self):
                return "Receive Test Pattern from the 2nd bit to the 7h bit. If DS0_7b and DS0_6b are not active, engine will receive test pattern from the 1st bit to the 8th bit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1PtmFbitOVR"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_nxds0_ctrl._RxDE1PtmFbitOVR()
            allFields["RxDE1PtmDS0_6b"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_nxds0_ctrl._RxDE1PtmDS0_6b()
            allFields["RxDE1PtmDS0_7b"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_nxds0_ctrl._RxDE1PtmDS0_7b()
            return allFields

    class _dej1_testpat_mon_nxds0_conc_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Test Pattern Monitoring nxDS0 Concatenation Control"
    
        def description(self):
            return "This register is used to be active DSO for PTG engine: 0-3. A 32 bits register is dedicated for 32 timeslot in case of frame E1, and 24 bits (bit 24 - 1) is given for 24 timeslot in case of frame DS1/J1. The 32-bits are set 1 to indicate full channel extraction."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x054820 +  index"
            
        def startAddress(self):
            return 0x00054820
            
        def endAddress(self):
            return 0x0005482f

        class _RxDE1PtgDS0ConCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1PtgDS0ConCtrl"
            
            def description(self):
                return "Per time slot receive DS1/E1/J1 Concatenation enable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1PtgDS0ConCtrl"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_nxds0_conc_ctrl._RxDE1PtgDS0ConCtrl()
            return allFields

    class _dej1_testpat_mon_err_cnt(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Test Pattern Monitoring Error Counter"
    
        def description(self):
            return "This is the error counter report of PTM engine #0 - 15 during monitor period."
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x054960 +  index"
            
        def startAddress(self):
            return 0x00054960
            
        def endAddress(self):
            return 0x0005497f

        class _RxDE1PtmErrCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1PtmErrCnt"
            
            def description(self):
                return "Error counter for BERT monitoring."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1PtmErrCnt"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_err_cnt._RxDE1PtmErrCnt()
            return allFields

    class _dej1_testpat_mon_goodbit_cnt(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Test Pattern Monitoring Good Bit Counter"
    
        def description(self):
            return "This is the good counter report of PTM engine #0 - 15 during monitor period."
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x054980 +  index"
            
        def startAddress(self):
            return 0x00054980
            
        def endAddress(self):
            return 0x0005499f

        class _RxDE1PtmGoodCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1PtmGoodCnt"
            
            def description(self):
                return "Good counter for BERT monitoring."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1PtmGoodCnt"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_goodbit_cnt._RxDE1PtmGoodCnt()
            return allFields

    class _dej1_testpat_mon_lossbit_cnt(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Test Pattern Monitoring Loss Bit Counter"
    
        def description(self):
            return "This is the loss counter report of PTM engine #0 - 16 during monitor period."
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0549A0 +  index"
            
        def startAddress(self):
            return 0x000549a0
            
        def endAddress(self):
            return 0x000549bf

        class _RxDE1PtmLossCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1PtmLossCnt"
            
            def description(self):
                return "Loss counter for BERT monitoring."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1PtmLossCnt"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_lossbit_cnt._RxDE1PtmLossCnt()
            return allFields

    class _dej1_testpat_mon_cntid_cfg(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Test Pattern Monitoring Counter Load ID"
    
        def description(self):
            return "This register is used to load the error/good/loss counter report of PTM engine: 0-15 which the channel ID is value of register."
            
        def width(self):
            return 4
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00054903
            
        def endAddress(self):
            return 0x00054903

        class _RxDE1PtmLoadID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1PtmLoadID"
            
            def description(self):
                return "Load ID counter."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1PtmLoadID"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_cntid_cfg._RxDE1PtmLoadID()
            return allFields

    class _dej1_testpat_mon_errbit_cnt(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Test Pattern Monitoring Error-Bit Counter Loading"
    
        def description(self):
            return "This is the loaded error counter report of PTM."
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00054904
            
        def endAddress(self):
            return 0x00649b04

        class _RxDE1PtmLoadErrCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1PtmLoadErrCnt"
            
            def description(self):
                return "Load Error counter."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1PtmLoadErrCnt"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_errbit_cnt._RxDE1PtmLoadErrCnt()
            return allFields

    class _dej1_testpat_mon_goodbit_cnt_loading(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Test Pattern Monitoring Good-Bit Counter Loading"
    
        def description(self):
            return "This is the loaded good counter report of PTM."
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00054905
            
        def endAddress(self):
            return 0x00649b05

        class _RxDE1PtmLoadGoodCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1PtmLoadGoodCnt"
            
            def description(self):
                return "Load Good counter."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1PtmLoadGoodCnt"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_goodbit_cnt_loading._RxDE1PtmLoadGoodCnt()
            return allFields

    class _dej1_testpat_mon_lostbit_cnt(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Test Pattern Monitoring Lost-Bit Counter Loading"
    
        def description(self):
            return "This is the loaded loss counter report of PTM."
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00054906
            
        def endAddress(self):
            return 0x00649b06

        class _RxDE1PtmLoadLossCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1PtmLoadLossCnt"
            
            def description(self):
                return "Load Loss counter."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1PtmLoadLossCnt"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_lostbit_cnt._RxDE1PtmLoadLossCnt()
            return allFields

    class _dej1_testpat_mon_timing_stat(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Test Pattern Monitoring Timing Status"
    
        def description(self):
            return "These registers are used for Hardware tus only"
            
        def width(self):
            return 10
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x054830 +  index"
            
        def startAddress(self):
            return 0x00054830
            
        def endAddress(self):
            return 0x0005483f

        class _Status(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Status"
            
            def description(self):
                return "This field is used for Hw tus only."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Status"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_timing_stat._Status()
            return allFields

    class _dej1_testpat_mon_stat(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Test Pattern Monitoring Status"
    
        def description(self):
            return "These registers are used for Hardware tus only"
            
        def width(self):
            return 48
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x054940 +  index"
            
        def startAddress(self):
            return 0x00054940
            
        def endAddress(self):
            return 0x0005495f

        class _Status(AtRegister.AtRegisterField):
            def stopBit(self):
                return 36
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Status"
            
            def description(self):
                return "This field is used for Hw tus only."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Status"] = _AF6CCI0011_RD_PDH._dej1_testpat_mon_stat._Status()
            return allFields

    class _spevt_map_fflen_watermask(AtRegister.AtRegister):
        def name(self):
            return "SPE/VT Map Fifo Length Water Mark"
    
        def description(self):
            return "This register is used for Hardware tus only."
            
        def width(self):
            return 24
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00070002
            
        def endAddress(self):
            return 0x00070002

        class _STSVTFifoLenMark(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "STSVTFifoLenMark"
            
            def description(self):
                return "For Hardware use only"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["STSVTFifoLenMark"] = _AF6CCI0011_RD_PDH._spevt_map_fflen_watermask._STSVTFifoLenMark()
            return allFields

    class _stsvt_map_ctrl(AtRegister.AtRegister):
        def name(self):
            return "STS/VT Map Control"
    
        def description(self):
            return "The STS/VT Map Control is use to configure for per channel STS/VT Map operation."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00076000 +  32*stsid + 4*vtgid + vtid"
            
        def startAddress(self):
            return 0x00076000
            
        def endAddress(self):
            return 0x000763ff

        class _StsVtMapAismask(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "StsVtMapAismask"
            
            def description(self):
                return "Set 1 to mask AIS from PDH to OCN"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsVtMapJitIDCfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 15
        
            def name(self):
                return "StsVtMapJitIDCfg"
            
            def description(self):
                return "STS/VT Mapping Jitter ID configure, default value is 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsVtMapStuffThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 10
        
            def name(self):
                return "StsVtMapStuffThres"
            
            def description(self):
                return "STS/VT Mapping Stuff Threshold Configure, default value is 12"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsVtMapJitterCfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 6
        
            def name(self):
                return "StsVtMapJitterCfg"
            
            def description(self):
                return "STS/VT Mapping Jitter configure - Vtmap: StsVtMapJitterCfg: Bit FiFo mode - 1'b1: Bit FiFo stuff mode: Only in case of ACR/DCR timing mode or Slaver mode from  ACR/DCR master timing ID - 1'b0: Byte FiFO stuff mode: default value StsVtMapJitterCfg: Master Enable. Only valid in Bit FiFo stuff_mode StsVtMapJitterCfg: AIS insert enable StsVtMapJitterCfg: Byte FiFo mode In case of Byte FiFo Stuff Mode: + 1'b1: Byte FiFo mode Enable: default value + 1'b0: Byte LoopTime FiFo mode Enable: only in case of Loop timing mode - Spe Map: 0001: old_stuff_mode 0010: holdover_en 0100: aisins_en 1001: fine_stuff_mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsVtMapCenterFifo(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "StsVtMapCenterFifo"
            
            def description(self):
                return "This bit is set to 1 to force center the mapping FIFO."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsVtMapBypass(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "StsVtMapBypass"
            
            def description(self):
                return "This bit is set to 1 to bypass the STS/VT map"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsVtMapMd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "StsVtMapMd"
            
            def description(self):
                return "STS/VT Mapping mode StsVtMapMd	StsVtMapBypass	Operation Mode 0	          0	E1 to VT2 Map 0	          1	E1 to Bus 1	          0	DS1 to VT1.5 Map 1	          1	DS1 to Bus 2	          0	E3 to VC3 Map 2	          1	E3 to TU3 Map 3	          0	DS3 to VC3 Map 3	          1	DS3 to TU3 Map 4	          x	VT2/TU12 Basic CEP 5	          x	Packet over STS1/VC3 6	          0	STS1/VC3 Fractional CEP carrying VT2/TU12 6	          1	STS1/VC3 Fractional CEP carrying VT15/TU11 7	          0	STS1/VC3 Fractional CEP carrying E3 7	          1	STS1/VC3 Fractional CEP carrying DS3 8	          x	STS1/VC3 Basic CEP 9	          x	Packet over STS3c/VC4 10	        0	STS3c/VC4 Fractional CEP carrying VT2/TU12 10	        1	STS3c/VC4 Fractional CEP carrying VT15/TU11 11	        0	STS3c/VC4 Fractional CEP carrying E3 11	        1	STS3c/VC4 Fractional CEP carrying DS3 12	        x	STS3c/VC4/STS12c/VC4_4c Basic CEP 13	        x	Packet over TU3 14	        0	E3 to Bus 14	        1	DS3 to Bus 15	        x	Disable STS/VC/VT/TU/DS1/E1/DS3/E3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["StsVtMapAismask"] = _AF6CCI0011_RD_PDH._stsvt_map_ctrl._StsVtMapAismask()
            allFields["StsVtMapJitIDCfg"] = _AF6CCI0011_RD_PDH._stsvt_map_ctrl._StsVtMapJitIDCfg()
            allFields["StsVtMapStuffThres"] = _AF6CCI0011_RD_PDH._stsvt_map_ctrl._StsVtMapStuffThres()
            allFields["StsVtMapJitterCfg"] = _AF6CCI0011_RD_PDH._stsvt_map_ctrl._StsVtMapJitterCfg()
            allFields["StsVtMapCenterFifo"] = _AF6CCI0011_RD_PDH._stsvt_map_ctrl._StsVtMapCenterFifo()
            allFields["StsVtMapBypass"] = _AF6CCI0011_RD_PDH._stsvt_map_ctrl._StsVtMapBypass()
            allFields["StsVtMapMd"] = _AF6CCI0011_RD_PDH._stsvt_map_ctrl._StsVtMapMd()
            return allFields

    class _vt_async_map_ctrl(AtRegister.AtRegister):
        def name(self):
            return "VT Async Map Control"
    
        def description(self):
            return "The VT Async Map Control is use to configure for per channel VT Async Map operation."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00076800 +  32*stsid + 4*vtgid + vtid"
            
        def startAddress(self):
            return 0x00076800
            
        def endAddress(self):
            return 0x00076bff

        class _BitFiFoID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "BitFiFoID"
            
            def description(self):
                return "Bit FiFo ID, related to ACR/DCR ID engine. Valid from 0 to 255"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VTStuffThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "VTStuffThr"
            
            def description(self):
                return "The number of VT Multiframe (500us) threshold for VT async stuffing. Using for BitFiFo mode and ByteFiFo mode. Default value is 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["BitFiFoID"] = _AF6CCI0011_RD_PDH._vt_async_map_ctrl._BitFiFoID()
            allFields["VTStuffThr"] = _AF6CCI0011_RD_PDH._vt_async_map_ctrl._VTStuffThr()
            return allFields

    class _stsvt_map_ff_center_sticky(AtRegister.AtRegister):
        def name(self):
            return "STS/VT Map FIFO Center Sticky"
    
        def description(self):
            return "These registers are used for Hardware tus only"
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return "0x00070020 +  index"
            
        def startAddress(self):
            return 0x00070020
            
        def endAddress(self):
            return 0x0007003b

        class _STSVTFifoCenStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "STSVTFifoCenStk"
            
            def description(self):
                return "This field is used for Hw tus only."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["STSVTFifoCenStk"] = _AF6CCI0011_RD_PDH._stsvt_map_ff_center_sticky._STSVTFifoCenStk()
            return allFields

    class _stsvt_map_hw_stat(AtRegister.AtRegister):
        def name(self):
            return "STS/VT Map HW Status"
    
        def description(self):
            return "for HW debug only"
            
        def width(self):
            return 48
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00076400 +  32*stsid + 4*vtgid + vtid"
            
        def startAddress(self):
            return 0x00076400
            
        def endAddress(self):
            return 0x000767ff

        class _STSVTStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 35
                
            def startBit(self):
                return 0
        
            def name(self):
                return "STSVTStatus"
            
            def description(self):
                return "for HW debug only"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["STSVTStatus"] = _AF6CCI0011_RD_PDH._stsvt_map_hw_stat._STSVTStatus()
            return allFields

    class _ts_vt_map_jitter_cfg(AtRegister.AtRegister):
        def name(self):
            return "TS/VT Map Jitter Configuration"
    
        def description(self):
            return "This is the configuration for STS/VT Mapping."
            
        def width(self):
            return 96
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00070080
            
        def endAddress(self):
            return 0xffffffff

        class _SWThreshold1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 69
                
            def startBit(self):
                return 63
        
            def name(self):
                return "SWThreshold1"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SWThreshold2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 62
                
            def startBit(self):
                return 56
        
            def name(self):
                return "SWThreshold2"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SWThreshold3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 55
                
            def startBit(self):
                return 49
        
            def name(self):
                return "SWThreshold3"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SWThreshold4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 48
                
            def startBit(self):
                return 42
        
            def name(self):
                return "SWThreshold4"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SWThreshold5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 41
                
            def startBit(self):
                return 35
        
            def name(self):
                return "SWThreshold5"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SWCntMax1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 34
                
            def startBit(self):
                return 28
        
            def name(self):
                return "SWCntMax1"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SWCntMax2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 21
        
            def name(self):
                return "SWCntMax2"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SWCntMax3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 14
        
            def name(self):
                return "SWCntMax3"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SWCntMax4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 7
        
            def name(self):
                return "SWCntMax4"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SWCntMax5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SWCntMax5"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SWThreshold1"] = _AF6CCI0011_RD_PDH._ts_vt_map_jitter_cfg._SWThreshold1()
            allFields["SWThreshold2"] = _AF6CCI0011_RD_PDH._ts_vt_map_jitter_cfg._SWThreshold2()
            allFields["SWThreshold3"] = _AF6CCI0011_RD_PDH._ts_vt_map_jitter_cfg._SWThreshold3()
            allFields["SWThreshold4"] = _AF6CCI0011_RD_PDH._ts_vt_map_jitter_cfg._SWThreshold4()
            allFields["SWThreshold5"] = _AF6CCI0011_RD_PDH._ts_vt_map_jitter_cfg._SWThreshold5()
            allFields["SWCntMax1"] = _AF6CCI0011_RD_PDH._ts_vt_map_jitter_cfg._SWCntMax1()
            allFields["SWCntMax2"] = _AF6CCI0011_RD_PDH._ts_vt_map_jitter_cfg._SWCntMax2()
            allFields["SWCntMax3"] = _AF6CCI0011_RD_PDH._ts_vt_map_jitter_cfg._SWCntMax3()
            allFields["SWCntMax4"] = _AF6CCI0011_RD_PDH._ts_vt_map_jitter_cfg._SWCntMax4()
            allFields["SWCntMax5"] = _AF6CCI0011_RD_PDH._ts_vt_map_jitter_cfg._SWCntMax5()
            return allFields

    class _dej1_tx_framer_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Tx Framer Control"
    
        def description(self):
            return "DS1/E1/J1 Rx framer control is used to configure for Frame mode (DS1, E1, J1) at the DS1/E1 framer."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00094000 +  32*de3id + 4*de2id + de1id"
            
        def startAddress(self):
            return 0x00094000
            
        def endAddress(self):
            return 0x000943ff

        class _TxDE1ForceAllOne(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "TxDE1ForceAllOne"
            
            def description(self):
                return "Force all one to TDM side when Line Local Loopback only side"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1FbitBypass(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "TxDE1FbitBypass"
            
            def description(self):
                return "Set 1 to bypass Fbit insertion in Tx DS1/E1 Framer"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1LineAisIns(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "TxDE1LineAisIns"
            
            def description(self):
                return "Set 1 to enable Line AIS Insert.This bit force all one to Line Local Loopback"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1PayAisIns(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "TxDE1PayAisIns"
            
            def description(self):
                return "Set 1 to enable Payload AIS Insert"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1RmtLineloop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "TxDE1RmtLineloop"
            
            def description(self):
                return "Set 1 to enable remote Line Loop back"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1RmtPayloop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "TxDE1RmtPayloop"
            
            def description(self):
                return "Set 1 to enable remote Payload Loop back. Sharing for VT Loopback in case of CEP path"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1AutoAis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "TxDE1AutoAis"
            
            def description(self):
                return "Set 1 to enable AIS indication from data map block to automatically transmit all 1s at DS1/E1/J1 Tx framer"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1DLCfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 9
        
            def name(self):
                return "TxDE1DLCfg"
            
            def description(self):
                return "Incase E1 mode : use for SSM in Sa, bit[15:13] is Sa position 0=disable, 3=Sa4, 4=Sa5, 5=Sa6, 6=Sa7,7=Sa8. Bit[12:9] is SSM value. Incase DS1 mode : bit[12] Set 1 to Enable FDL transmit , dont care other bits"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1AutoYel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TxDE1AutoYel"
            
            def description(self):
                return "Auto Yellow generation enable 1: Yellow alarm detected from Rx Framer will be automatically transmitted in Tx Framer 0: No automatically Yellow alarm transmitted"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1FrcYel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "TxDE1FrcYel"
            
            def description(self):
                return "SW force to Tx Yellow alarm"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1AutoCrcErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "TxDE1AutoCrcErr"
            
            def description(self):
                return "Auto CRC error enable 1: CRC Error detected from Rx Framer will be automatically transmitted in REI bit of Tx Framer 0: No automatically CRC Error alarm transmitted"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1FrcCrcErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "TxDE1FrcCrcErr"
            
            def description(self):
                return "SW force to Tx CRC Error alarm (REI bit)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "TxDE1En"
            
            def description(self):
                return "Set 1 to enable the DS1/E1/J1 framer."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1Md(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1Md"
            
            def description(self):
                return "Receive DS1/J1 framing mode 0000: DS1/J1 Unframe 0001: DS1 SF (D4) 0010: DS1 ESF 0011: DS1 DDS 0100: DS1 SLC 0101: J1 SF 0110: J1 ESF 1000: E1 Unframe 1001: E1 Basic Frame 1010: E1 CRC4 Frame"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDE1ForceAllOne"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_ctrl._TxDE1ForceAllOne()
            allFields["TxDE1FbitBypass"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_ctrl._TxDE1FbitBypass()
            allFields["TxDE1LineAisIns"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_ctrl._TxDE1LineAisIns()
            allFields["TxDE1PayAisIns"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_ctrl._TxDE1PayAisIns()
            allFields["TxDE1RmtLineloop"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_ctrl._TxDE1RmtLineloop()
            allFields["TxDE1RmtPayloop"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_ctrl._TxDE1RmtPayloop()
            allFields["TxDE1AutoAis"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_ctrl._TxDE1AutoAis()
            allFields["TxDE1DLCfg"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_ctrl._TxDE1DLCfg()
            allFields["TxDE1AutoYel"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_ctrl._TxDE1AutoYel()
            allFields["TxDE1FrcYel"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_ctrl._TxDE1FrcYel()
            allFields["TxDE1AutoCrcErr"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_ctrl._TxDE1AutoCrcErr()
            allFields["TxDE1FrcCrcErr"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_ctrl._TxDE1FrcCrcErr()
            allFields["TxDE1En"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_ctrl._TxDE1En()
            allFields["RxDE1Md"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_ctrl._RxDE1Md()
            return allFields

    class _dej1_tx_framer_stat(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Tx Framer Status"
    
        def description(self):
            return "These registers are used for Hardware tus only"
            
        def width(self):
            return 3
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00094400 +  32*de3id + 4*de2id + de1id"
            
        def startAddress(self):
            return 0x00094400
            
        def endAddress(self):
            return 0x000947ff

        class _RxDE1Sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1Sta"
            
            def description(self):
                return "000: Search 001: Shift 010: Wait 011: Verify 100: Fs search 101: Inframe"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1Sta"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_stat._RxDE1Sta()
            return allFields

    class _dej1_tx_framer_sign_insertion_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Tx Framer Signaling Insertion Control"
    
        def description(self):
            return "DS1/E1/J1 Rx framer signaling insertion control is used to configure for signaling operation modes at the DS1/E1 framer."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00094800 +  32*de3id + 4*de2id + de1id"
            
        def startAddress(self):
            return 0x00094800
            
        def endAddress(self):
            return 0x00094bff

        class _TxDE1SigMfrmEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "TxDE1SigMfrmEn"
            
            def description(self):
                return "Set 1 to enable the Tx DS1/E1/J1 framer to sync the signaling multiframe to the incoming data flow. No applicable for DS1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1SigBypass(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDE1SigBypass"
            
            def description(self):
                return "30 signaling bypass bit for 32 DS0 in an E1 frame. In DS1 mode, only 24 LSB bits is used. Set 1 to bypass Signaling insertion in Tx DS1/E1 Framer"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDE1SigMfrmEn"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_sign_insertion_ctrl._TxDE1SigMfrmEn()
            allFields["TxDE1SigBypass"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_sign_insertion_ctrl._TxDE1SigBypass()
            return allFields

    class _dej1_tx_framer_sign_id_conv_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Tx Framer Signaling ID Conversion Control"
    
        def description(self):
            return "DS1/E1/J1 Rx framer signaling ID conversion control is used to convert the PW ID to DS1/E1/J1 Line ID at the DS1/E1 framer."
            
        def width(self):
            return 48
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000C2000 +  pwid"
            
        def startAddress(self):
            return 0x000c2000
            
        def endAddress(self):
            return 0x000c23ff

        class _TxDE1SigDS1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 41
                
            def startBit(self):
                return 41
        
            def name(self):
                return "TxDE1SigDS1"
            
            def description(self):
                return "Set 1 if the PW carry DS0 for the DS1/J1, set 0 for E1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1SigLineID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 40
                
            def startBit(self):
                return 32
        
            def name(self):
                return "TxDE1SigLineID"
            
            def description(self):
                return "Output DS1/E1/J1 Line ID of the conversion."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1SigEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDE1SigEnb"
            
            def description(self):
                return "32 signaling enable bit for 32 DS0 in an E1 frame. In DS1 mode, only 24 LSB bits is used."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDE1SigDS1"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_sign_id_conv_ctrl._TxDE1SigDS1()
            allFields["TxDE1SigLineID"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_sign_id_conv_ctrl._TxDE1SigLineID()
            allFields["TxDE1SigEnb"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_sign_id_conv_ctrl._TxDE1SigEnb()
            return allFields

    class _dej1_tx_framer_sign_cpuins_en_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Tx Framer Signaling CPU Insert Enable Control"
    
        def description(self):
            return "This is the CPU signaling insert global enable bit"
            
        def width(self):
            return 1
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000cff01
            
        def endAddress(self):
            return 0xffffffff

        class _TxDE1SigCPUMd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDE1SigCPUMd"
            
            def description(self):
                return "Set 1 to enable CPU mode (all signaling is from CPU)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDE1SigCPUMd"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_sign_cpuins_en_ctrl._TxDE1SigCPUMd()
            return allFields

    class _dej1_tx_framer_sign_cpu_de1_wr_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Tx Framer Signaling CPU DS1/E1 signaling Write Control"
    
        def description(self):
            return "This is the CPU signaling write control"
            
        def width(self):
            return 1
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000cff02
            
        def endAddress(self):
            return 0xffffffff

        class _TxDE1SigCPUWrCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDE1SigCPUWrCtrl"
            
            def description(self):
                return "Set 1 if signaling value written to the Signaling buffer in next CPU cylce is for DS1/J1, set 0 for E1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDE1SigCPUWrCtrl"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_sign_cpu_de1_wr_ctrl._TxDE1SigCPUWrCtrl()
            return allFields

    class _dej1_tx_framer_sign_buffer(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Tx Framer Signaling Buffer"
    
        def description(self):
            return "DS1/E1/J1 Rx framer signaling ABCD bit store for each DS0 at the DS1/E1 framer."
            
        def width(self):
            return 4
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000C8000 +  1024*de3id + 128*de2id + 32*de1id + tscnt"
            
        def startAddress(self):
            return 0x000c8000
            
        def endAddress(self):
            return 0x000cff00

        class _TxDE1SigABCD(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDE1SigABCD"
            
            def description(self):
                return "4-bit ABCD signaling for each DS0 (Note: for DS1/J1, must write the 0x000C8002 value 1 before writing this ABCD, for E1  must write the 0x000C8002 value 0 before writing this ABCD)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDE1SigABCD"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_sign_buffer._TxDE1SigABCD()
            return allFields

    class _dej1_testpat_gen_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Test Pattern Generation Control"
    
        def description(self):
            return "This register is used to global configuration for PTG engines"
            
        def width(self):
            return 2
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000950ff
            
        def endAddress(self):
            return 0x000950ff

        class _TxDE1PtgSwap(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "TxDE1PtgSwap"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1PtgInv(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDE1PtgInv"
            
            def description(self):
                return "PTG inversion Bit."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDE1PtgSwap"] = _AF6CCI0011_RD_PDH._dej1_testpat_gen_ctrl._TxDE1PtgSwap()
            allFields["TxDE1PtgInv"] = _AF6CCI0011_RD_PDH._dej1_testpat_gen_ctrl._TxDE1PtgInv()
            return allFields

    class _dej1_testpat_gen_single_biterr_ins(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Test Pattern Generation Single Bit Error Insertion"
    
        def description(self):
            return "This register is used to insert a single erroneous bit to DS1/E1/J1 Test Pattern generator."
            
        def width(self):
            return 4
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000950fe
            
        def endAddress(self):
            return 0x000950fe

        class _TxDE1PtmErrIDIns(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDE1PtmErrIDIns"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDE1PtmErrIDIns"] = _AF6CCI0011_RD_PDH._dej1_testpat_gen_single_biterr_ins._TxDE1PtmErrIDIns()
            return allFields

    class _dej1_testpat_gen_mode(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Test Pattern Generation Mode"
    
        def description(self):
            return "This register is used to data mode for PTG block."
            
        def width(self):
            return 10
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x095080 +  index"
            
        def startAddress(self):
            return 0x00095080
            
        def endAddress(self):
            return 0x0009508f

        class _TxDE1PtmPatBit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 5
        
            def name(self):
                return "TxDE1PtmPatBit"
            
            def description(self):
                return "The number of bit pattern is used."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1PtgMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDE1PtgMode"
            
            def description(self):
                return "00000: Prbs9 00001: Prbs11 00010: Prbs15 00011: Prbs20 (X19 + X2 + 1) 00100: Prbs20 (X19 + X16 + 1) 00101: Qrss20 (X19 + X16 + 1) 00110: Prbs23 00111: DDS1 01000: DDS2 01001: DDS3 01010: DDS4 01011: DDS5 01100: Daly55 01101: Fix 3 in 24 01110: Fix 1 in 8 01111: Octet55 10000: Fix pattern n-bit 10001: Sequence 10010: Prbs7"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDE1PtmPatBit"] = _AF6CCI0011_RD_PDH._dej1_testpat_gen_mode._TxDE1PtmPatBit()
            allFields["TxDE1PtgMode"] = _AF6CCI0011_RD_PDH._dej1_testpat_gen_mode._TxDE1PtgMode()
            return allFields

    class _dej1_testpat_gen_sel_chn(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Test Pattern Generation Selected Channel"
    
        def description(self):
            return "This register is used to global configuration for PTG engine #0 - 15"
            
        def width(self):
            return 10
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x095000 +  index"
            
        def startAddress(self):
            return 0x00095000
            
        def endAddress(self):
            return 0x0009500f

        class _TxDE1PtgEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "TxDE1PtgEn"
            
            def description(self):
                return "PTG Bit Enable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1PtgID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDE1PtgID"
            
            def description(self):
                return "Configure which channel in 336 DS1s/E1s/J1s is inserted a test pattern."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDE1PtgEn"] = _AF6CCI0011_RD_PDH._dej1_testpat_gen_sel_chn._TxDE1PtgEn()
            allFields["TxDE1PtgID"] = _AF6CCI0011_RD_PDH._dej1_testpat_gen_sel_chn._TxDE1PtgID()
            return allFields

    class _dej1_testpat_gen_nxds0_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Test Pattern Generation nxDS0 Control"
    
        def description(self):
            return "This register is used to configure the mode of nxDS0 in DS1/E1/J1 test pattern generator."
            
        def width(self):
            return 2
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x095010 +  index"
            
        def startAddress(self):
            return 0x00095010
            
        def endAddress(self):
            return 0x0009501f

        class _TxDE1PtgDS0_6b(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "TxDE1PtgDS0_6b"
            
            def description(self):
                return "Transmit Test Pattern from the 2nd bit to the 8th bit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1PtgDS0_7b(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDE1PtgDS0_7b"
            
            def description(self):
                return "Transmit Test Pattern from the 2nd bit to the 7h bit. If DS0_7b and DS0_6b are not active, engine will transmit Test Pattern from the 1st bit to the 8th bit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDE1PtgDS0_6b"] = _AF6CCI0011_RD_PDH._dej1_testpat_gen_nxds0_ctrl._TxDE1PtgDS0_6b()
            allFields["TxDE1PtgDS0_7b"] = _AF6CCI0011_RD_PDH._dej1_testpat_gen_nxds0_ctrl._TxDE1PtgDS0_7b()
            return allFields

    class _dej1_testpat_gen_nxds0_conc_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Test Pattern Generation nxDS0 Concatenation Control"
    
        def description(self):
            return "This register is used to be active bit at level DS0 or PTG engine 0. A 32 bits register is dedicated for 32 timeslot in case of frame E1, and 24 bits (bit 24 - 1) is given for 24 timeslot in case of frame DS1. The 32-bits are set 1 to indicate full channel insertion."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x095020 +  index"
            
        def startAddress(self):
            return 0x00095020
            
        def endAddress(self):
            return 0x0009502f

        class _RxDE1FixPat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1FixPat"
            
            def description(self):
                return "Configurable fixed pattern for BERT monitoring"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1FixPat"] = _AF6CCI0011_RD_PDH._dej1_testpat_gen_nxds0_conc_ctrl._RxDE1FixPat()
            return allFields

    class _dej1_testpat_gen_fixedpat_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Test Pattern Generation Fixed Pattern Control"
    
        def description(self):
            return "This register is used to be transmitted data in fix pattern mode for engine #0 - 15"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x095090 +  index"
            
        def startAddress(self):
            return 0x00095090
            
        def endAddress(self):
            return 0x0009509f

        class _RxDE1FixPat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1FixPat"
            
            def description(self):
                return "Configurable fixed pattern for BERT monitoring."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1FixPat"] = _AF6CCI0011_RD_PDH._dej1_testpat_gen_fixedpat_ctrl._RxDE1FixPat()
            return allFields

    class _dej1_testpat_gen_errrate_inst(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Test Pattern Generation Error Rate Insertion"
    
        def description(self):
            return "This register is used to control register for error insertion"
            
        def width(self):
            return 4
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0950A0 +  index"
            
        def startAddress(self):
            return 0x000950a0
            
        def endAddress(self):
            return 0x000950af

        class _TxDE1BerErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "TxDE1BerErr"
            
            def description(self):
                return "BER enable insertion for engine n. Single bit error can occur during BER insertion."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1BerMd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDE1BerMd"
            
            def description(self):
                return "Bit Error Rate inserted to Pattern Generator 000: BER 10-2 001: BER 10-3 010: BER 10-4 011: BER 10-5 100: BER 10-6 101: BER 10-7 11x: BER 10-7"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDE1BerErr"] = _AF6CCI0011_RD_PDH._dej1_testpat_gen_errrate_inst._TxDE1BerErr()
            allFields["TxDE1BerMd"] = _AF6CCI0011_RD_PDH._dej1_testpat_gen_errrate_inst._TxDE1BerMd()
            return allFields

    class _dej1_testpat_gen_timing_stat(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Test Pattern Generation Timing Status"
    
        def description(self):
            return "These registers are used for Hardware tus only"
            
        def width(self):
            return 4
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x615030 +  index"
            
        def startAddress(self):
            return 0x00615030
            
        def endAddress(self):
            return 0x0065503f

        class _tus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tus"
            
            def description(self):
                return "status"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tus"] = _AF6CCI0011_RD_PDH._dej1_testpat_gen_timing_stat._tus()
            return allFields

    class _dej1_testpat_gen_stat(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Test Pattern Generation Status"
    
        def description(self):
            return "These registers are used for Hardware tus only"
            
        def width(self):
            return 4
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0950C0 + 16*high + index"
            
        def startAddress(self):
            return 0x000950c0
            
        def endAddress(self):
            return 0x000950ef

        class _tus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tus"
            
            def description(self):
                return "status"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tus"] = _AF6CCI0011_RD_PDH._dej1_testpat_gen_stat._tus()
            return allFields

    class _dej1_tx_framer_dlk_indir_acc_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Tx Framer DLK Indirect Access Control"
    
        def description(self):
            return "The register holds read/write/address command to access registers that are required to read or write indirectly. %% When writing a value into indirect registers, data needs to be written into the DLK Indirect Access Write Data Control register, address and command are then written into this register with 1 on bit DLKRdWrPend. Until hardware completes the write cycle, bit DLKRdWrPend is clear to 0.  %% When reading indirect registers, address and command are written into this register with 1 on bit DLKRdWrPend. Until hardware completes the read cycle, bit DLKRdWrPend is clear to 0. Read data is ready on the DLK Indirect Access Read Data Status register."
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00090008
            
        def endAddress(self):
            return 0xffffffff

        class _DLKRdWrPend(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "DLKRdWrPend"
            
            def description(self):
                return "Read/Write Pending 1: Read/Write has been pending 0: Read/Write is done"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DLKWnR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "DLKWnR"
            
            def description(self):
                return "Write not Read. This bit is used to control the hardware to read or write an indirect register. 1: Read an indirect register 0: Write to an indirect register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DLKRegAdr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DLKRegAdr"
            
            def description(self):
                return "Register Address"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DLKRdWrPend"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_dlk_indir_acc_ctrl._DLKRdWrPend()
            allFields["DLKWnR"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_dlk_indir_acc_ctrl._DLKWnR()
            allFields["DLKRegAdr"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_dlk_indir_acc_ctrl._DLKRegAdr()
            return allFields

    class _dej1_tx_framer_dlk_indir_acc_wr_data_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Tx Framer DLK Indirect Access Write Data Control"
    
        def description(self):
            return "The registers hold data value in order to write this value to indirect registers. This register is used in conjunction with the DLK Indirect Access Control register."
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00090009
            
        def endAddress(self):
            return 0xffffffff

        class _WrHoldDat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "WrHoldDat"
            
            def description(self):
                return "Write Access Holding Data"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["WrHoldDat"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_dlk_indir_acc_wr_data_ctrl._WrHoldDat()
            return allFields

    class _dej1_tx_framer_dlk_indir_acc_rd_data_stat(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Tx Framer DLK Indirect Access Read Data Status"
    
        def description(self):
            return "The registers hold data value after reading indirect registers. This register is used in conjunction with the DLK Indirect Access Control register."
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0009000a
            
        def endAddress(self):
            return 0xffffffff

        class _RdHoldDat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RdHoldDat"
            
            def description(self):
                return "Read Access Holding Data"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RdHoldDat"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_dlk_indir_acc_rd_data_stat._RdHoldDat()
            return allFields

    class _dej1_tx_framer_dlk_ins_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Tx Framer DLK Insertion Control"
    
        def description(self):
            return "These registers are used to control DLK insertion engines."
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00096020
            
        def endAddress(self):
            return 0x0009602f

        class _DlkMsgLength(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 9
        
            def name(self):
                return "DlkMsgLength"
            
            def description(self):
                return "length of message to transmit in byte. The maximum length is 128 in byte."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DlkDE1ID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DlkDE1ID"
            
            def description(self):
                return "To configure which DS1/E1 ID is selected to insert DLK. At a time, user can choose 16 DS1/E1s to carry DLK message."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DlkMsgLength"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_dlk_ins_ctrl._DlkMsgLength()
            allFields["DlkDE1ID"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_dlk_ins_ctrl._DlkDE1ID()
            return allFields

    class _dej1_tx_framer_dlk_ins_en(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Tx Framer DLK Insertion Enable"
    
        def description(self):
            return "This register is used to configure enable or disable DLK insertion engines. This configuration is on per engine."
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00096010
            
        def endAddress(self):
            return 0xffffffff

        class _DlkInsEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DlkInsEn"
            
            def description(self):
                return "1: engine is ready to insert DLK 0: engine is disable to insert DLK."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DlkInsEn"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_dlk_ins_en._DlkInsEn()
            return allFields

    class _dej1_tx_framer_dlk_ins_newmsg_start_en(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Tx Framer DLK Insertion New Message Start Enable"
    
        def description(self):
            return "This register is used to configure DLK insertion engines to transmit a new message. By setting a bit in this register to 1, DLK engine (appropriate to that bit) will get message in the message memory to insert into the configured DS1/E1 frame. When transmitting finishes, engine automatically clears this bit to inform to CPU that the DLK message has been already transmitted."
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00096011
            
        def endAddress(self):
            return 0xffffffff

        class _DlkNewMsgStartEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DlkNewMsgStartEn"
            
            def description(self):
                return "DlkNewMsgStartEn"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DlkNewMsgStartEn"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_dlk_ins_newmsg_start_en._DlkNewMsgStartEn()
            return allFields

    class _dej1_tx_framer_dlk_ins_eng_stat(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Tx Framer DLK Insertion Engine Status"
    
        def description(self):
            return "This register is used to inform to CPU the tus of DLK insertion engine. Before using an engine to transmit DLK for a DS1/E1 channel, CPU must read this register to get the status of that engin"
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00096012
            
        def endAddress(self):
            return 0xffffffff

        class _DlkEngSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DlkEngSta"
            
            def description(self):
                return "16-bit corresponds to 16 engines 1: Engine is busy. 0: Engine is free"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DlkEngSta"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_dlk_ins_eng_stat._DlkEngSta()
            return allFields

    class _dej1_tx_framer_dlk_ins_msg_mem(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Tx Framer DLK Insertion Message Memory"
    
        def description(self):
            return "This memory contains data messages; each DLK insertion engine has a 128-byte message.e whether or not it is being usued by another DS1/E1 channel."
            
        def width(self):
            return 8
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x096800 +  msgid*128 + byteid"
            
        def startAddress(self):
            return 0x00096800
            
        def endAddress(self):
            return 0x00096fff

        class _DlkInsMsg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DlkInsMsg"
            
            def description(self):
                return "DlkInsMsg"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DlkInsMsg"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_dlk_ins_msg_mem._DlkInsMsg()
            return allFields

    class _dej1_tx_framer_dlk_ins_bom_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Tx Framer DLK Insertion BOM Control"
    
        def description(self):
            return "This memory is used to control transmitting Bit-Oriented Message (BOM). This memory is accessed indirectly via indirect Access registers"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x097000 +  de3id*32 + de2id*4 + de1id"
            
        def startAddress(self):
            return 0x00097000
            
        def endAddress(self):
            return 0x000973ff

        class _DlkBOMEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "DlkBOMEn"
            
            def description(self):
                return "enable transmitting BOM. By setting this field to 1, DLK engine will rt to insert 6-bit into the BOM pattern 111111110xxxxxx0 with the MSB is transmitted first. When transmitting finishes, engine automatically clears this bit to inform to CPU that the BOM message has been already transmitted."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DlkBOMRptTime(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 6
        
            def name(self):
                return "DlkBOMRptTime"
            
            def description(self):
                return "indicate the number of time that the BOM is transmitted repeatedly, 0xF for send continuos."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DlkBOMMsg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DlkBOMMsg"
            
            def description(self):
                return "6-bit BOM message in pattern BOM 111111110xxxxxx0 in which the MSB is transmitted first."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DlkBOMEn"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_dlk_ins_bom_ctrl._DlkBOMEn()
            allFields["DlkBOMRptTime"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_dlk_ins_bom_ctrl._DlkBOMRptTime()
            allFields["DlkBOMMsg"] = _AF6CCI0011_RD_PDH._dej1_tx_framer_dlk_ins_bom_ctrl._DlkBOMMsg()
            return allFields

    class _txm23e23_ctrl(AtRegister.AtRegister):
        def name(self):
            return "TxM23E23 Control"
    
        def description(self):
            return "The TxM23E23 Control is use to configure for per channel Tx DS3/E3 operation."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00080000 +  de3id"
            
        def startAddress(self):
            return 0x00080000
            
        def endAddress(self):
            return 0x0008001f

        class _TxDS3UnfrmAISMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "TxDS3UnfrmAISMode"
            
            def description(self):
                return "Set 1 to send out AIS signal pattern in DS3Unframe, default set 0 to send out AIS all 1's pattern in DS3Unframe"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDS3E3FrcAllone(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "TxDS3E3FrcAllone"
            
            def description(self):
                return "Force All One to remote only for Line Local Loopback(control bit5 of RX M23 0x40000)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDS3E3ChEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "TxDS3E3ChEnb"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDS3E3DLKMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 26
        
            def name(self):
                return "TxDS3E3DLKMode"
            
            def description(self):
                return "0:DS3DL/E3G751NA/E3G832GC; 1:DS3UDL/E3G832NR; 2: DS3FEAC; 3:DS3NA"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDS3E3DLKEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "TxDS3E3DLKEnb"
            
            def description(self):
                return "Set 1 to enable DLK DS3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDS3E3FeacThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 21
        
            def name(self):
                return "TxDS3E3FeacThres"
            
            def description(self):
                return "Number of word repeat, 0xF for send continuos"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDS3E3LineAllOne(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "TxDS3E3LineAllOne"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDS3E3PayLoop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "TxDS3E3PayLoop"
            
            def description(self):
                return "Payload Remote Loopback"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDS3E3LineLoop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "TxDS3E3LineLoop"
            
            def description(self):
                return "Line Remote Loopback"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDS3E3FrcYel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "TxDS3E3FrcYel"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDS3E3AutoYel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "TxDS3E3AutoYel"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDS3E3LoopMd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 14
        
            def name(self):
                return "TxDS3E3LoopMd"
            
            def description(self):
                return "DS3 C-bit mode: [15:14]: FE Control: 00: idle code; 01: activate cw; 10: de-activate cw; 11: alrm/status E3G832 mode: [15:14]: DL mode Normal: Loop Mode configuration"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDS3E3LoopEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 7
        
            def name(self):
                return "TxDS3E3LoopEn"
            
            def description(self):
                return "DS3 C-bit mode: [12:7]: FEAC Control Word E3G832 mode: [13:11]: Payload type [10:7]:  SSM Config Normal: [13:7]: Loop en configuration per channel"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDS3E3Ohbypass(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "TxDS3E3Ohbypass"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDS3E3IdleSet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "TxDS3E3IdleSet"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDS3E3AisSet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "TxDS3E3AisSet"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDS3E3Md(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDS3E3Md"
            
            def description(self):
                return "Tx DS3/E3 framing mode 0000: DS3 Unframed 0001: DS3 C-bit parity carrying 28 DS1s or 21 E1s 0010: DS3 M23 carrying 28 DS1s or 21 E1s 0011: DS3 C-bit map 0100: E3 Unframed 0101: E3 G832 0110: E3 G.751 carrying 16 E1s 0111: E3 G751 Map 1000: Bypass RxM13E13 (DS1/E1 mode)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDS3UnfrmAISMode"] = _AF6CCI0011_RD_PDH._txm23e23_ctrl._TxDS3UnfrmAISMode()
            allFields["TxDS3E3FrcAllone"] = _AF6CCI0011_RD_PDH._txm23e23_ctrl._TxDS3E3FrcAllone()
            allFields["TxDS3E3ChEnb"] = _AF6CCI0011_RD_PDH._txm23e23_ctrl._TxDS3E3ChEnb()
            allFields["TxDS3E3DLKMode"] = _AF6CCI0011_RD_PDH._txm23e23_ctrl._TxDS3E3DLKMode()
            allFields["TxDS3E3DLKEnb"] = _AF6CCI0011_RD_PDH._txm23e23_ctrl._TxDS3E3DLKEnb()
            allFields["TxDS3E3FeacThres"] = _AF6CCI0011_RD_PDH._txm23e23_ctrl._TxDS3E3FeacThres()
            allFields["TxDS3E3LineAllOne"] = _AF6CCI0011_RD_PDH._txm23e23_ctrl._TxDS3E3LineAllOne()
            allFields["TxDS3E3PayLoop"] = _AF6CCI0011_RD_PDH._txm23e23_ctrl._TxDS3E3PayLoop()
            allFields["TxDS3E3LineLoop"] = _AF6CCI0011_RD_PDH._txm23e23_ctrl._TxDS3E3LineLoop()
            allFields["TxDS3E3FrcYel"] = _AF6CCI0011_RD_PDH._txm23e23_ctrl._TxDS3E3FrcYel()
            allFields["TxDS3E3AutoYel"] = _AF6CCI0011_RD_PDH._txm23e23_ctrl._TxDS3E3AutoYel()
            allFields["TxDS3E3LoopMd"] = _AF6CCI0011_RD_PDH._txm23e23_ctrl._TxDS3E3LoopMd()
            allFields["TxDS3E3LoopEn"] = _AF6CCI0011_RD_PDH._txm23e23_ctrl._TxDS3E3LoopEn()
            allFields["TxDS3E3Ohbypass"] = _AF6CCI0011_RD_PDH._txm23e23_ctrl._TxDS3E3Ohbypass()
            allFields["TxDS3E3IdleSet"] = _AF6CCI0011_RD_PDH._txm23e23_ctrl._TxDS3E3IdleSet()
            allFields["TxDS3E3AisSet"] = _AF6CCI0011_RD_PDH._txm23e23_ctrl._TxDS3E3AisSet()
            allFields["TxDS3E3Md"] = _AF6CCI0011_RD_PDH._txm23e23_ctrl._TxDS3E3Md()
            return allFields

    class _txm23e23_hw_stat(AtRegister.AtRegister):
        def name(self):
            return "TxM23E23 HW Status"
    
        def description(self):
            return "for HW debug only"
            
        def width(self):
            return 48
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00080080 +  de3id"
            
        def startAddress(self):
            return 0x00080080
            
        def endAddress(self):
            return 0x0008009f

        class _TxM23E23Status(AtRegister.AtRegisterField):
            def stopBit(self):
                return 42
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxM23E23Status"
            
            def description(self):
                return "for HW debug only"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxM23E23Status"] = _AF6CCI0011_RD_PDH._txm23e23_hw_stat._TxM23E23Status()
            return allFields

    class _txm23e23_e3g832_tracebyte(AtRegister.AtRegister):
        def name(self):
            return "TxM23E23 E3g832 Trace Byte"
    
        def description(self):
            return ""
            
        def width(self):
            return 8
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00080200 +  de3id*16 + bytecnt"
            
        def startAddress(self):
            return 0x00080200
            
        def endAddress(self):
            return 0x000803ff

        class _TxM23E23_E3g832_Trace_Byte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxM23E23_E3g832_Trace_Byte"
            
            def description(self):
                return "TxM23E23 E3g832 Trace Byte"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxM23E23_E3g832_Trace_Byte"] = _AF6CCI0011_RD_PDH._txm23e23_e3g832_tracebyte._TxM23E23_E3g832_Trace_Byte()
            return allFields

    class _de3_testpat_gen_glb_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS3/E3 Test Pattern Generation Global Control"
    
        def description(self):
            return ""
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000800d0
            
        def endAddress(self):
            return 0x007800d3

        class _TxDe3PtgBerEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "TxDe3PtgBerEn"
            
            def description(self):
                return "Enable transmit DS3/E3 BER insert"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDe3PtgBerMod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 12
        
            def name(self):
                return "TxDe3PtgBerMod"
            
            def description(self):
                return "BER mode configuration 000: BER 10-2 001: BER 10-3 010: BER 10-4 011: BER 10-5 100: BER 10-6 1x1: BER 10-7"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDe3PtgSwap(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "TxDe3PtgSwap"
            
            def description(self):
                return "PTG swap Bit. If it is set to 1, pattern is transmitted LSB first, then MSB. 		Normal is set to 0."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDe3PtgInv(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "TxDe3PtgInv"
            
            def description(self):
                return "PTG inversion Bit. Normal is set to 0."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDe3PtgBitErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "TxDe3PtgBitErr"
            
            def description(self):
                return "Enable for single bit error insertion."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDe3PtgMod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 5
        
            def name(self):
                return "TxDe3PtgMod"
            
            def description(self):
                return "Generation DS3/E3 BERT mode 0000: Prbs9 0001: Prbs11 0010: Prbs15 0011: Prbs20 0100: Prbs20 0101: Qrss20 0110: Prbs23 0111: Prbs31 1000: Sequence 1001: AllOne 1010: AllZero 1011: AA55"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDe3PtgEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "TxDe3PtgEn"
            
            def description(self):
                return "Bit Enable Test Pattern Generation"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDe3PtgID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDe3PtgID"
            
            def description(self):
                return "To configure which DS3/E3 line is selected  to test the Pattern GenerationID Configuration"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDe3PtgBerEn"] = _AF6CCI0011_RD_PDH._de3_testpat_gen_glb_ctrl._TxDe3PtgBerEn()
            allFields["TxDe3PtgBerMod"] = _AF6CCI0011_RD_PDH._de3_testpat_gen_glb_ctrl._TxDe3PtgBerMod()
            allFields["TxDe3PtgSwap"] = _AF6CCI0011_RD_PDH._de3_testpat_gen_glb_ctrl._TxDe3PtgSwap()
            allFields["TxDe3PtgInv"] = _AF6CCI0011_RD_PDH._de3_testpat_gen_glb_ctrl._TxDe3PtgInv()
            allFields["TxDe3PtgBitErr"] = _AF6CCI0011_RD_PDH._de3_testpat_gen_glb_ctrl._TxDe3PtgBitErr()
            allFields["TxDe3PtgMod"] = _AF6CCI0011_RD_PDH._de3_testpat_gen_glb_ctrl._TxDe3PtgMod()
            allFields["TxDe3PtgEn"] = _AF6CCI0011_RD_PDH._de3_testpat_gen_glb_ctrl._TxDe3PtgEn()
            allFields["TxDe3PtgID"] = _AF6CCI0011_RD_PDH._de3_testpat_gen_glb_ctrl._TxDe3PtgID()
            return allFields

    class _txm12e12_ctrl(AtRegister.AtRegister):
        def name(self):
            return "TxM12E12 Control"
    
        def description(self):
            return "The TxM12E12 Control is use to configure for per channel Tx DS2/E2 operation."
            
        def width(self):
            return 2
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00081000 +  8*de3id + de2id"
            
        def startAddress(self):
            return 0x00081000
            
        def endAddress(self):
            return 0x000810ff

        class _TxDS2E2Md(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDS2E2Md"
            
            def description(self):
                return "Tx DS2/E2 framing mode 00: DS2 T1.107 carrying 4 DS1s 01: DS2 G.747 carrying 3 E1s 10: E2 G.742 carrying 4 E1s 11: Reserved"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDS2E2Md"] = _AF6CCI0011_RD_PDH._txm12e12_ctrl._TxDS2E2Md()
            return allFields

    class _txm12e12_hw_stat(AtRegister.AtRegister):
        def name(self):
            return "TxM12E12 HW Status"
    
        def description(self):
            return "for HW debug only"
            
        def width(self):
            return 96
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00081200 +  8*de3id + de2id"
            
        def startAddress(self):
            return 0x00081200
            
        def endAddress(self):
            return 0x000812ff

        class _TxM12E12Status(AtRegister.AtRegisterField):
            def stopBit(self):
                return 80
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxM12E12Status"
            
            def description(self):
                return "for HW debug only"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxM12E12Status"] = _AF6CCI0011_RD_PDH._txm12e12_hw_stat._TxM12E12Status()
            return allFields

    class _txm12e12_jitter_cfg(AtRegister.AtRegister):
        def name(self):
            return "Tx M12E12 Jitter Configuration"
    
        def description(self):
            return "This is the configuration for Tx M13/E13"
            
        def width(self):
            return 96
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00081380
            
        def endAddress(self):
            return 0xffffffff

        class _SWThreshold1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 69
                
            def startBit(self):
                return 63
        
            def name(self):
                return "SWThreshold1"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SWThreshold2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 62
                
            def startBit(self):
                return 56
        
            def name(self):
                return "SWThreshold2"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SWThreshold3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 55
                
            def startBit(self):
                return 49
        
            def name(self):
                return "SWThreshold3"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SWThreshold4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 48
                
            def startBit(self):
                return 42
        
            def name(self):
                return "SWThreshold4"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SWThreshold5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 41
                
            def startBit(self):
                return 35
        
            def name(self):
                return "SWThreshold5"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SWCntMax1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 34
                
            def startBit(self):
                return 28
        
            def name(self):
                return "SWCntMax1"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SWCntMax2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 21
        
            def name(self):
                return "SWCntMax2"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SWCntMax3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 14
        
            def name(self):
                return "SWCntMax3"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SWCntMax4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 7
        
            def name(self):
                return "SWCntMax4"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SWCntMax5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SWCntMax5"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SWThreshold1"] = _AF6CCI0011_RD_PDH._txm12e12_jitter_cfg._SWThreshold1()
            allFields["SWThreshold2"] = _AF6CCI0011_RD_PDH._txm12e12_jitter_cfg._SWThreshold2()
            allFields["SWThreshold3"] = _AF6CCI0011_RD_PDH._txm12e12_jitter_cfg._SWThreshold3()
            allFields["SWThreshold4"] = _AF6CCI0011_RD_PDH._txm12e12_jitter_cfg._SWThreshold4()
            allFields["SWThreshold5"] = _AF6CCI0011_RD_PDH._txm12e12_jitter_cfg._SWThreshold5()
            allFields["SWCntMax1"] = _AF6CCI0011_RD_PDH._txm12e12_jitter_cfg._SWCntMax1()
            allFields["SWCntMax2"] = _AF6CCI0011_RD_PDH._txm12e12_jitter_cfg._SWCntMax2()
            allFields["SWCntMax3"] = _AF6CCI0011_RD_PDH._txm12e12_jitter_cfg._SWCntMax3()
            allFields["SWCntMax4"] = _AF6CCI0011_RD_PDH._txm12e12_jitter_cfg._SWCntMax4()
            allFields["SWCntMax5"] = _AF6CCI0011_RD_PDH._txm12e12_jitter_cfg._SWCntMax5()
            return allFields

    class _upen_test1(AtRegister.AtRegister):
        def name(self):
            return "LC test reg1"
    
        def description(self):
            return "For testing only"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e0000
            
        def endAddress(self):
            return 0xffffffff

        class _test_reg1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "test_reg1"
            
            def description(self):
                return "value of reg1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["test_reg1"] = _AF6CCI0011_RD_PDH._upen_test1._test_reg1()
            return allFields

    class _upen_test2(AtRegister.AtRegister):
        def name(self):
            return "LC test reg2"
    
        def description(self):
            return "For testing only"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e0001
            
        def endAddress(self):
            return 0xffffffff

        class _test_reg2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "test_reg2"
            
            def description(self):
                return "value of reg2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["test_reg2"] = _AF6CCI0011_RD_PDH._upen_test2._test_reg2()
            return allFields

    class _upen_lim_err(AtRegister.AtRegister):
        def name(self):
            return "Config Limit error"
    
        def description(self):
            return "Check pattern error in state matching pattern"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e0002
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_lim_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_lim_err"
            
            def description(self):
                return "if cnt err more than limit error,search failed"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_lim_err"] = _AF6CCI0011_RD_PDH._upen_lim_err._cfg_lim_err()
            return allFields

    class _upen_lim_mat(AtRegister.AtRegister):
        def name(self):
            return "Config limit match"
    
        def description(self):
            return "Used to check parttern match"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e0003
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_lim_mat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_lim_mat"
            
            def description(self):
                return "if cnt match more than limit mat, serch ok"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_lim_mat"] = _AF6CCI0011_RD_PDH._upen_lim_mat._cfg_lim_mat()
            return allFields

    class _upen_th_fdl_mat(AtRegister.AtRegister):
        def name(self):
            return "Config thresh fdl parttern"
    
        def description(self):
            return "Check fdl message codes match"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e0004
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_th_fdl_mat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_th_fdl_mat"
            
            def description(self):
                return "if message codes repeat more than thresh, search ok,"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_th_fdl_mat"] = _AF6CCI0011_RD_PDH._upen_th_fdl_mat._cfg_th_fdl_mat()
            return allFields

    class _upen_th_err(AtRegister.AtRegister):
        def name(self):
            return "Config thersh error"
    
        def description(self):
            return "Max error for one pattern"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e0005
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_th_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_th_err"
            
            def description(self):
                return "if pattern error more than thresh , change to check another pattern"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_th_err"] = _AF6CCI0011_RD_PDH._upen_th_err._cfg_th_err()
            return allFields

    class _upen_stk(AtRegister.AtRegister):
        def name(self):
            return "Sticky pattern detected"
    
        def description(self):
            return "For HW debug"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e20ff
            
        def endAddress(self):
            return 0xffffffff

        class _updo_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_stk"
            
            def description(self):
                return "bit[0] = 1 :empty bit[1] = 1 :full, bit[2] = 1 :interrupt"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_stk"] = _AF6CCI0011_RD_PDH._upen_stk._updo_stk()
            return allFields

    class _upen_len(AtRegister.AtRegister):
        def name(self):
            return "Number of pattern"
    
        def description(self):
            return "For HW debug"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e20fe
            
        def endAddress(self):
            return 0xffffffff

        class _info_len_l(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 0
        
            def name(self):
                return "info_len_l"
            
            def description(self):
                return "number patt detected"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["info_len_l"] = _AF6CCI0011_RD_PDH._upen_len._info_len_l()
            return allFields

    class _upen_info(AtRegister.AtRegister):
        def name(self):
            return "Current Inband Code"
    
        def description(self):
            return "Used to report current status of chid"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return "0xE_2100 + id"
            
        def startAddress(self):
            return 0x000e2100
            
        def endAddress(self):
            return 0x000e24ff

        class _updo_sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_sta"
            
            def description(self):
                return "bit [2:0] == 0x0  CSU UP bit [2:0] == 0x1  CSU DOWN bit [2:0] == 0x2  FAC1 UP bit [2:0] == 0x3  FAC1 DOWN bit [2:0] == 0x4  FAC2 UP bit [2:0] == 0x5  FAC2 DOWN bit [2:0] == 0x7  Change from LoopCode to nomal"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_sta"] = _AF6CCI0011_RD_PDH._upen_info._updo_sta()
            return allFields

    class _upen_fdl_stk(AtRegister.AtRegister):
        def name(self):
            return "Sticky FDL message detected"
    
        def description(self):
            return "For HW debug"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e08ff
            
        def endAddress(self):
            return 0xffffffff

        class _updo_fdlstk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_fdlstk"
            
            def description(self):
                return "bit[0] = 1 :empty, bit[1] = 1 :full, bit[2] = 1 :interrupt"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_fdlstk"] = _AF6CCI0011_RD_PDH._upen_fdl_stk._updo_fdlstk()
            return allFields

    class _upen_fdlpat(AtRegister.AtRegister):
        def name(self):
            return "Config User programmble Pattern User Codes"
    
        def description(self):
            return "check fdl message codes"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e08f8
            
        def endAddress(self):
            return 0xffffffff

        class _updo_fdl_mess(AtRegister.AtRegisterField):
            def stopBit(self):
                return 55
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_fdl_mess"
            
            def description(self):
                return "mess codes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_fdl_mess"] = _AF6CCI0011_RD_PDH._upen_fdlpat._updo_fdl_mess()
            return allFields

    class _upen_len_mess(AtRegister.AtRegister):
        def name(self):
            return "Number of message"
    
        def description(self):
            return "For HW debug"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e08fe
            
        def endAddress(self):
            return 0xffffffff

        class _info_len_l(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 0
        
            def name(self):
                return "info_len_l"
            
            def description(self):
                return "number patt detected"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["info_len_l"] = _AF6CCI0011_RD_PDH._upen_len_mess._info_len_l()
            return allFields

    class _upen_fdl_info(AtRegister.AtRegister):
        def name(self):
            return "Currenr Message FDL Detected"
    
        def description(self):
            return "Info fdl message detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return "0xE_1800 + $fdl_len"
            
        def startAddress(self):
            return 0x000e1800
            
        def endAddress(self):
            return 0x000e1bff

        class _mess_info_l(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mess_info_l"
            
            def description(self):
                return "message info bit[15:8] :	8bit BOM pattern format 0xxxxxx0 bit[4]    : set 1 indicate BOM message  bit[3:0]== 0x0 11111111_01111110 bit[3:0]== 0x1 line loop up bit[3:0]== 0x2 line loop down bit[3:0]== 0x3 payload loop up bit[3:0]== 0x4 payload loop down bit[3:0]== 0x5 smartjack act bit[3:0]== 0x6 smartjact deact bit[3:0]== 0x7 idle"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mess_info_l"] = _AF6CCI0011_RD_PDH._upen_fdl_info._mess_info_l()
            return allFields

    class _upen_th_stt_int(AtRegister.AtRegister):
        def name(self):
            return "Config Theshold Pattern Report"
    
        def description(self):
            return "Maximum Pattern Report"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e0006
            
        def endAddress(self):
            return 0xffffffff

        class _updo_th_stt_int(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_th_stt_int"
            
            def description(self):
                return "if number of pattern detected more than threshold, interrupt enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_th_stt_int"] = _AF6CCI0011_RD_PDH._upen_th_stt_int._updo_th_stt_int()
            return allFields

    class _upen_ibstk_rs(AtRegister.AtRegister):
        def name(self):
            return "Sticky Loopcode detected"
    
        def description(self):
            return "Indicate loopcode is detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return "0xE_2800 + $base"
            
        def startAddress(self):
            return 0x000e2800
            
        def endAddress(self):
            return 0x000e281f

        class _updo_ibstk_rs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_ibstk_rs"
            
            def description(self):
                return "sticky inband loopcode detect base 0 : chid0 - chid31 ... base 31: chid991 - chid1023"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_ibstk_rs"] = _AF6CCI0011_RD_PDH._upen_ibstk_rs._updo_ibstk_rs()
            return allFields

    class _upen_ibstk_fl(AtRegister.AtRegister):
        def name(self):
            return "Sticky Loopcode change"
    
        def description(self):
            return "Indicate loopcode is detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return "0xE_3000 + $base"
            
        def startAddress(self):
            return 0x000e3000
            
        def endAddress(self):
            return 0x000e371f

        class _updo_ibstk_fl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_ibstk_fl"
            
            def description(self):
                return "sticky inband change base 0 : chid0 - chid31 ... base 31: chid991 - chid1023"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_ibstk_fl"] = _AF6CCI0011_RD_PDH._upen_ibstk_fl._updo_ibstk_fl()
            return allFields

    class _upen_obstk_rs(AtRegister.AtRegister):
        def name(self):
            return "Sticky Outband LoopCode"
    
        def description(self):
            return "Indicate outband loopcode is detect"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return "0xE_0900 + $base"
            
        def startAddress(self):
            return 0x000e0900
            
        def endAddress(self):
            return 0x000e091f

        class _updo_obstk_rs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_obstk_rs"
            
            def description(self):
                return "Sticky outband detected base 0 : chid0 - chid31 ... base 31: chid991 - chid1023"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_obstk_rs"] = _AF6CCI0011_RD_PDH._upen_obstk_rs._updo_obstk_rs()
            return allFields

    class _upen_obstk_fl(AtRegister.AtRegister):
        def name(self):
            return "Sticky Outband LoopCode change"
    
        def description(self):
            return "Indicate outband loopcode is changed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e1000
            
        def endAddress(self):
            return 0x0000101f

        class _updo_obstk_fl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_obstk_fl"
            
            def description(self):
                return "Sticky outband change base 0 : chid0 - chid31 ... base 31: chid991 - chid1023"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_obstk_fl"] = _AF6CCI0011_RD_PDH._upen_obstk_fl._updo_obstk_fl()
            return allFields

    class _RAM_Parity_Force_Control(AtRegister.AtRegister):
        def name(self):
            return "RAM Parity Force Control"
    
        def description(self):
            return "This register configures force parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000010
            
        def endAddress(self):
            return 0xffffffff

        class _PDHVTAsyncMapCtrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PDHVTAsyncMapCtrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"PDH VT Async Map Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHSTSVTMapCtrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "PDHSTSVTMapCtrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"PDH STS/VT Map Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM23E23Trace_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "PDHTxM23E23Trace_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"PDH TxM23E23 E3g832 Trace Byte\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM23E23Ctrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "PDHTxM23E23Ctrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"PDH TxM23E23 Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM12E12Ctrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PDHTxM12E12Ctrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"PDH TxM12E12 Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxDE1SigCtrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "PDHTxDE1SigCtrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"PDH DS1/E1/J1 Tx Framer Signalling Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxDE1FrmCtrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PDHTxDE1FrmCtrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"PDH DS1/E1/J1 Tx Framer Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxDE1FrmCtrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PDHRxDE1FrmCtrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"PDH DS1/E1/J1 Rx Framer Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxM12E12Ctrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PDHRxM12E12Ctrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"PDH RxM12E12 Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxDS3E3OHCtrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "PDHRxDS3E3OHCtrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"PDH RxDS3E3 OH Pro Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxM23E23Ctrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PDHRxM23E23Ctrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"PDH RxM23E23 Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHSTSVTDeMapCtrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PDHSTSVTDeMapCtrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"PDH STS/VT Demap Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHMuxCtrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDHMuxCtrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"PDH DS1/E1/J1 Rx Framer Mux Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDHVTAsyncMapCtrl_ParErrFrc"] = _AF6CCI0011_RD_PDH._RAM_Parity_Force_Control._PDHVTAsyncMapCtrl_ParErrFrc()
            allFields["PDHSTSVTMapCtrl_ParErrFrc"] = _AF6CCI0011_RD_PDH._RAM_Parity_Force_Control._PDHSTSVTMapCtrl_ParErrFrc()
            allFields["PDHTxM23E23Trace_ParErrFrc"] = _AF6CCI0011_RD_PDH._RAM_Parity_Force_Control._PDHTxM23E23Trace_ParErrFrc()
            allFields["PDHTxM23E23Ctrl_ParErrFrc"] = _AF6CCI0011_RD_PDH._RAM_Parity_Force_Control._PDHTxM23E23Ctrl_ParErrFrc()
            allFields["PDHTxM12E12Ctrl_ParErrFrc"] = _AF6CCI0011_RD_PDH._RAM_Parity_Force_Control._PDHTxM12E12Ctrl_ParErrFrc()
            allFields["PDHTxDE1SigCtrl_ParErrFrc"] = _AF6CCI0011_RD_PDH._RAM_Parity_Force_Control._PDHTxDE1SigCtrl_ParErrFrc()
            allFields["PDHTxDE1FrmCtrl_ParErrFrc"] = _AF6CCI0011_RD_PDH._RAM_Parity_Force_Control._PDHTxDE1FrmCtrl_ParErrFrc()
            allFields["PDHRxDE1FrmCtrl_ParErrFrc"] = _AF6CCI0011_RD_PDH._RAM_Parity_Force_Control._PDHRxDE1FrmCtrl_ParErrFrc()
            allFields["PDHRxM12E12Ctrl_ParErrFrc"] = _AF6CCI0011_RD_PDH._RAM_Parity_Force_Control._PDHRxM12E12Ctrl_ParErrFrc()
            allFields["PDHRxDS3E3OHCtrl_ParErrFrc"] = _AF6CCI0011_RD_PDH._RAM_Parity_Force_Control._PDHRxDS3E3OHCtrl_ParErrFrc()
            allFields["PDHRxM23E23Ctrl_ParErrFrc"] = _AF6CCI0011_RD_PDH._RAM_Parity_Force_Control._PDHRxM23E23Ctrl_ParErrFrc()
            allFields["PDHSTSVTDeMapCtrl_ParErrFrc"] = _AF6CCI0011_RD_PDH._RAM_Parity_Force_Control._PDHSTSVTDeMapCtrl_ParErrFrc()
            allFields["PDHMuxCtrl_ParErrFrc"] = _AF6CCI0011_RD_PDH._RAM_Parity_Force_Control._PDHMuxCtrl_ParErrFrc()
            return allFields

    class _RAM_Parity_Disable_Control(AtRegister.AtRegister):
        def name(self):
            return "RAM Parity Disable Control"
    
        def description(self):
            return "This register configures force parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000011
            
        def endAddress(self):
            return 0xffffffff

        class _PDHVTAsyncMapCtrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PDHVTAsyncMapCtrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH VT Async Map Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHSTSVTMapCtrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "PDHSTSVTMapCtrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH STS/VT Map Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM23E23Trace_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "PDHTxM23E23Trace_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH TxM23E23 E3g832 Trace Byte\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM23E23Ctrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "PDHTxM23E23Ctrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH TxM23E23 Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM12E12Ctrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PDHTxM12E12Ctrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH TxM12E12 Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxDE1SigCtrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "PDHTxDE1SigCtrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH DS1/E1/J1 Tx Framer Signalling Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxDE1FrmCtrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PDHTxDE1FrmCtrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH DS1/E1/J1 Tx Framer Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxDE1FrmCtrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PDHRxDE1FrmCtrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH DS1/E1/J1 Rx Framer Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxM12E12Ctrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PDHRxM12E12Ctrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH RxM12E12 Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxDS3E3OHCtrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "PDHRxDS3E3OHCtrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH RxDS3E3 OH Pro Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxM23E23Ctrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PDHRxM23E23Ctrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH RxM23E23 Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHSTSVTDeMapCtrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PDHSTSVTDeMapCtrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH STS/VT Demap Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHMuxCtrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDHMuxCtrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH DS1/E1/J1 Rx Framer Mux Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDHVTAsyncMapCtrl_ParErrDis"] = _AF6CCI0011_RD_PDH._RAM_Parity_Disable_Control._PDHVTAsyncMapCtrl_ParErrDis()
            allFields["PDHSTSVTMapCtrl_ParErrDis"] = _AF6CCI0011_RD_PDH._RAM_Parity_Disable_Control._PDHSTSVTMapCtrl_ParErrDis()
            allFields["PDHTxM23E23Trace_ParErrDis"] = _AF6CCI0011_RD_PDH._RAM_Parity_Disable_Control._PDHTxM23E23Trace_ParErrDis()
            allFields["PDHTxM23E23Ctrl_ParErrDis"] = _AF6CCI0011_RD_PDH._RAM_Parity_Disable_Control._PDHTxM23E23Ctrl_ParErrDis()
            allFields["PDHTxM12E12Ctrl_ParErrDis"] = _AF6CCI0011_RD_PDH._RAM_Parity_Disable_Control._PDHTxM12E12Ctrl_ParErrDis()
            allFields["PDHTxDE1SigCtrl_ParErrDis"] = _AF6CCI0011_RD_PDH._RAM_Parity_Disable_Control._PDHTxDE1SigCtrl_ParErrDis()
            allFields["PDHTxDE1FrmCtrl_ParErrDis"] = _AF6CCI0011_RD_PDH._RAM_Parity_Disable_Control._PDHTxDE1FrmCtrl_ParErrDis()
            allFields["PDHRxDE1FrmCtrl_ParErrDis"] = _AF6CCI0011_RD_PDH._RAM_Parity_Disable_Control._PDHRxDE1FrmCtrl_ParErrDis()
            allFields["PDHRxM12E12Ctrl_ParErrDis"] = _AF6CCI0011_RD_PDH._RAM_Parity_Disable_Control._PDHRxM12E12Ctrl_ParErrDis()
            allFields["PDHRxDS3E3OHCtrl_ParErrDis"] = _AF6CCI0011_RD_PDH._RAM_Parity_Disable_Control._PDHRxDS3E3OHCtrl_ParErrDis()
            allFields["PDHRxM23E23Ctrl_ParErrDis"] = _AF6CCI0011_RD_PDH._RAM_Parity_Disable_Control._PDHRxM23E23Ctrl_ParErrDis()
            allFields["PDHSTSVTDeMapCtrl_ParErrDis"] = _AF6CCI0011_RD_PDH._RAM_Parity_Disable_Control._PDHSTSVTDeMapCtrl_ParErrDis()
            allFields["PDHMuxCtrl_ParErrDis"] = _AF6CCI0011_RD_PDH._RAM_Parity_Disable_Control._PDHMuxCtrl_ParErrDis()
            return allFields

    class _RAM_Parity_Error_Sticky(AtRegister.AtRegister):
        def name(self):
            return "RAM parity Error Sticky"
    
        def description(self):
            return "This register configures disable parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000012
            
        def endAddress(self):
            return 0xffffffff

        class _PDHVTAsyncMapCtrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PDHVTAsyncMapCtrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"PDH VT Async Map Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHSTSVTMapCtrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "PDHSTSVTMapCtrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"PDH STS/VT Map Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM23E23Trace_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "PDHTxM23E23Trace_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"PDH TxM23E23 E3g832 Trace Byte\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM23E23Ctrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "PDHTxM23E23Ctrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"PDH TxM23E23 Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM12E12Ctrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PDHTxM12E12Ctrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"PDH TxM12E12 Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxDE1SigCtrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "PDHTxDE1SigCtrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"PDH DS1/E1/J1 Tx Framer Signalling Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxDE1FrmCtrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PDHTxDE1FrmCtrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"PDH DS1/E1/J1 Tx Framer Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxDE1FrmCtrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PDHRxDE1FrmCtrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"PDH DS1/E1/J1 Rx Framer Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxM12E12Ctrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PDHRxM12E12Ctrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"PDH RxM12E12 Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxDS3E3OHCtrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "PDHRxDS3E3OHCtrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"PDH RxDS3E3 OH Pro Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxM23E23Ctrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PDHRxM23E23Ctrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"PDH RxM23E23 Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHSTSVTDeMapCtrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PDHSTSVTDeMapCtrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"PDH STS/VT Demap Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHMuxCtrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDHMuxCtrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"PDH DS1/E1/J1 Rx Framer Mux Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDHVTAsyncMapCtrl_ParErrStk"] = _AF6CCI0011_RD_PDH._RAM_Parity_Error_Sticky._PDHVTAsyncMapCtrl_ParErrStk()
            allFields["PDHSTSVTMapCtrl_ParErrStk"] = _AF6CCI0011_RD_PDH._RAM_Parity_Error_Sticky._PDHSTSVTMapCtrl_ParErrStk()
            allFields["PDHTxM23E23Trace_ParErrStk"] = _AF6CCI0011_RD_PDH._RAM_Parity_Error_Sticky._PDHTxM23E23Trace_ParErrStk()
            allFields["PDHTxM23E23Ctrl_ParErrStk"] = _AF6CCI0011_RD_PDH._RAM_Parity_Error_Sticky._PDHTxM23E23Ctrl_ParErrStk()
            allFields["PDHTxM12E12Ctrl_ParErrStk"] = _AF6CCI0011_RD_PDH._RAM_Parity_Error_Sticky._PDHTxM12E12Ctrl_ParErrStk()
            allFields["PDHTxDE1SigCtrl_ParErrStk"] = _AF6CCI0011_RD_PDH._RAM_Parity_Error_Sticky._PDHTxDE1SigCtrl_ParErrStk()
            allFields["PDHTxDE1FrmCtrl_ParErrStk"] = _AF6CCI0011_RD_PDH._RAM_Parity_Error_Sticky._PDHTxDE1FrmCtrl_ParErrStk()
            allFields["PDHRxDE1FrmCtrl_ParErrStk"] = _AF6CCI0011_RD_PDH._RAM_Parity_Error_Sticky._PDHRxDE1FrmCtrl_ParErrStk()
            allFields["PDHRxM12E12Ctrl_ParErrStk"] = _AF6CCI0011_RD_PDH._RAM_Parity_Error_Sticky._PDHRxM12E12Ctrl_ParErrStk()
            allFields["PDHRxDS3E3OHCtrl_ParErrStk"] = _AF6CCI0011_RD_PDH._RAM_Parity_Error_Sticky._PDHRxDS3E3OHCtrl_ParErrStk()
            allFields["PDHRxM23E23Ctrl_ParErrStk"] = _AF6CCI0011_RD_PDH._RAM_Parity_Error_Sticky._PDHRxM23E23Ctrl_ParErrStk()
            allFields["PDHSTSVTDeMapCtrl_ParErrStk"] = _AF6CCI0011_RD_PDH._RAM_Parity_Error_Sticky._PDHSTSVTDeMapCtrl_ParErrStk()
            allFields["PDHMuxCtrl_ParErrStk"] = _AF6CCI0011_RD_PDH._RAM_Parity_Error_Sticky._PDHMuxCtrl_ParErrStk()
            return allFields

    class _dej1_errins_en_cfg(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Payload Error Insert Enable Configuration"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00090002
            
        def endAddress(self):
            return 0xffffffff

        class _TxDE1ErrInsID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDE1ErrInsID"
            
            def description(self):
                return "Force ID channel"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDE1ErrInsID"] = _AF6CCI0011_RD_PDH._dej1_errins_en_cfg._TxDE1ErrInsID()
            return allFields

    class _dej1_errins_thr_cfg(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Payload Error Insert threhold Configuration"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00090003
            
        def endAddress(self):
            return 0xffffffff

        class _TxDE1errormode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 29
        
            def name(self):
                return "TxDE1errormode"
            
            def description(self):
                return "Force error mode: 3:bit (included Fbit); 2:unused; 1: unused: 0: disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1errorsingle_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "TxDE1errorsingle_en"
            
            def description(self):
                return "0: One-shot with number of errors; 1:Line rate"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1PayErrInsThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDE1PayErrInsThr"
            
            def description(self):
                return "Error Threshold in bit unit or the number of error in one-shot mode. Valid value from 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDE1errormode"] = _AF6CCI0011_RD_PDH._dej1_errins_thr_cfg._TxDE1errormode()
            allFields["TxDE1errorsingle_en"] = _AF6CCI0011_RD_PDH._dej1_errins_thr_cfg._TxDE1errorsingle_en()
            allFields["TxDE1PayErrInsThr"] = _AF6CCI0011_RD_PDH._dej1_errins_thr_cfg._TxDE1PayErrInsThr()
            return allFields

    class _dej1_rx_errins_en_cfg(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Error Insert Enable Configuration"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00050017
            
        def endAddress(self):
            return 0xffffffff

        class _RxDE1ErrInsID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1ErrInsID"
            
            def description(self):
                return "Force ID channel"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1ErrInsID"] = _AF6CCI0011_RD_PDH._dej1_rx_errins_en_cfg._RxDE1ErrInsID()
            return allFields

    class _dej1_rx_errins_thr_cfg(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Payload Error Insert threhold Configuration"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00050018
            
        def endAddress(self):
            return 0xffffffff

        class _RxDE1errormode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 29
        
            def name(self):
                return "RxDE1errormode"
            
            def description(self):
                return "Force error mode: 3:bit (included Fbit); 2:unused; 1: unused: 0: disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1errorsingle_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "RxDE1errorsingle_en"
            
            def description(self):
                return "0: One-shot with number of errors; 1:Line rate"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1PayErrInsThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1PayErrInsThr"
            
            def description(self):
                return "Error Threshold in bit unit or the number of error in one-shot mode. Valid value from 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1errormode"] = _AF6CCI0011_RD_PDH._dej1_rx_errins_thr_cfg._RxDE1errormode()
            allFields["RxDE1errorsingle_en"] = _AF6CCI0011_RD_PDH._dej1_rx_errins_thr_cfg._RxDE1errorsingle_en()
            allFields["RxDE1PayErrInsThr"] = _AF6CCI0011_RD_PDH._dej1_rx_errins_thr_cfg._RxDE1PayErrInsThr()
            return allFields

    class _de3_errins_en_cfg(AtRegister.AtRegister):
        def name(self):
            return "DS3/E3 Payload Error Insert Enable Configuration"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000800c0
            
        def endAddress(self):
            return 0xffffffff

        class _TxDE3ErrInsID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDE3ErrInsID"
            
            def description(self):
                return "Force ID channel"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDE3ErrInsID"] = _AF6CCI0011_RD_PDH._de3_errins_en_cfg._TxDE3ErrInsID()
            return allFields

    class _de3_errins_thr_cfg(AtRegister.AtRegister):
        def name(self):
            return "DS3/E3 Payload Error Insert threhold Configuration"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000800c1
            
        def endAddress(self):
            return 0xffffffff

        class _TxDE3errormode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 29
        
            def name(self):
                return "TxDE3errormode"
            
            def description(self):
                return "Force error mode: 3:bit (included Fbit); 2:unused; 1: unused: 0: disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE3errorsingle_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "TxDE3errorsingle_en"
            
            def description(self):
                return "0: One-shot with number of errors; 1:Line rate"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE3PayErrInsThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDE3PayErrInsThr"
            
            def description(self):
                return "Error Threshold in bit unit or the number of error in one-shot mode. Valid value from 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDE3errormode"] = _AF6CCI0011_RD_PDH._de3_errins_thr_cfg._TxDE3errormode()
            allFields["TxDE3errorsingle_en"] = _AF6CCI0011_RD_PDH._de3_errins_thr_cfg._TxDE3errorsingle_en()
            allFields["TxDE3PayErrInsThr"] = _AF6CCI0011_RD_PDH._de3_errins_thr_cfg._TxDE3PayErrInsThr()
            return allFields

    class _de3_rx_errins_en_cfg(AtRegister.AtRegister):
        def name(self):
            return "DS3/E3 Rx Error Insert Enable Configuration"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000400c0
            
        def endAddress(self):
            return 0xffffffff

        class _RxDE3ErrInsID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE3ErrInsID"
            
            def description(self):
                return "Force ID channel"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE3ErrInsID"] = _AF6CCI0011_RD_PDH._de3_rx_errins_en_cfg._RxDE3ErrInsID()
            return allFields

    class _de3_rx_errins_thr_cfg(AtRegister.AtRegister):
        def name(self):
            return "DS3/E3 Payload Error Insert threhold Configuration"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000400c1
            
        def endAddress(self):
            return 0xffffffff

        class _RxDE3errormode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 29
        
            def name(self):
                return "RxDE3errormode"
            
            def description(self):
                return "Force error mode: 3:bit (included Fbit); 2:unused; 1: unused: 0: disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE3errorsingle_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "RxDE3errorsingle_en"
            
            def description(self):
                return "0: One-shot with number of errors; 1:Line rate"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE3PayErrInsThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE3PayErrInsThr"
            
            def description(self):
                return "Error Threshold in bit unit or the number of error in one-shot mode. Valid value from 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE3errormode"] = _AF6CCI0011_RD_PDH._de3_rx_errins_thr_cfg._RxDE3errormode()
            allFields["RxDE3errorsingle_en"] = _AF6CCI0011_RD_PDH._de3_rx_errins_thr_cfg._RxDE3errorsingle_en()
            allFields["RxDE3PayErrInsThr"] = _AF6CCI0011_RD_PDH._de3_rx_errins_thr_cfg._RxDE3PayErrInsThr()
            return allFields

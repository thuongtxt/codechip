import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0021_RD_PRBS(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["prbslogenctl"] = _AF6CNC0021_RD_PRBS._prbslogenctl()
        allRegisters["prbslomon"] = _AF6CNC0021_RD_PRBS._prbslomon()
        allRegisters["prbshogenctl"] = _AF6CNC0021_RD_PRBS._prbshogenctl()
        allRegisters["prbshomon"] = _AF6CNC0021_RD_PRBS._prbshomon()
        return allRegisters

    class _prbslogenctl(AtRegister.AtRegister):
        def name(self):
            return "PRBS_LOMAP Generator Control"
    
        def description(self):
            return "Each register is used to configure PRBS generator at MAP."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x50_0000 + 65536*2*Slice48Id + 1024*Slice24Id + PwId"
            
        def startAddress(self):
            return 0x00500000
            
        def endAddress(self):
            return 0x00505fff

        class _PrbsLoGenCtlSeqMod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PrbsLoGenCtlSeqMod"
            
            def description(self):
                return "Configure types of PRBS generator . 1: Sequence mode 0: PRBS mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PrbsLoGenCtlEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PrbsLoGenCtlEnb"
            
            def description(self):
                return "Enable PRBS generator. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PrbsLoGenCtlSeqMod"] = _AF6CNC0021_RD_PRBS._prbslogenctl._PrbsLoGenCtlSeqMod()
            allFields["PrbsLoGenCtlEnb"] = _AF6CNC0021_RD_PRBS._prbslogenctl._PrbsLoGenCtlEnb()
            return allFields

    class _prbslomon(AtRegister.AtRegister):
        def name(self):
            return "PRBS_LOMAP Monitor"
    
        def description(self):
            return "Each register is used for PRBS monitor at DEMAP."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x50_0800 + 65536*2*Slice48Id + 1024*Slice24Id + PwId"
            
        def startAddress(self):
            return 0x00500800
            
        def endAddress(self):
            return 0x0050ffff

        class _PrbsLoMonErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "PrbsLoMonErr"
            
            def description(self):
                return "PRBS error. 1: Error 0: Not error"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        class _PrbsLoMonSync(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PrbsLoMonSync"
            
            def description(self):
                return "PRBS monitor sync. Only used for PRBS mode 1: Data PRBS sync (Not error) 0: Not sync"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _PrbsLoMonCtlSeqMod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PrbsLoMonCtlSeqMod"
            
            def description(self):
                return "Configure types of PRBS generator . 1: Sequence mode 0: PRBS mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PrbsLoMonErr"] = _AF6CNC0021_RD_PRBS._prbslomon._PrbsLoMonErr()
            allFields["PrbsLoMonSync"] = _AF6CNC0021_RD_PRBS._prbslomon._PrbsLoMonSync()
            allFields["PrbsLoMonCtlSeqMod"] = _AF6CNC0021_RD_PRBS._prbslomon._PrbsLoMonCtlSeqMod()
            return allFields

    class _prbshogenctl(AtRegister.AtRegister):
        def name(self):
            return "PRBS_HOMAP Generator Control"
    
        def description(self):
            return "Each register is used to configure PRBS generator at MAP."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x06_8000 + 2048*Slice48Id + stsId"
            
        def startAddress(self):
            return 0x00068000
            
        def endAddress(self):
            return 0x0006f03f

        class _PrbsHoGenCtlSeqMod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PrbsHoGenCtlSeqMod"
            
            def description(self):
                return "Configure types of PRBS generator . 1: Sequence mode 0: PRBS mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PrbsHoGenCtlEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PrbsHoGenCtlEnb"
            
            def description(self):
                return "Enable PRBS generator. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PrbsHoGenCtlSeqMod"] = _AF6CNC0021_RD_PRBS._prbshogenctl._PrbsHoGenCtlSeqMod()
            allFields["PrbsHoGenCtlEnb"] = _AF6CNC0021_RD_PRBS._prbshogenctl._PrbsHoGenCtlEnb()
            return allFields

    class _prbshomon(AtRegister.AtRegister):
        def name(self):
            return "PRBS_HOMAP Monitor"
    
        def description(self):
            return "Each register is used for PRBS monitor at DEMAP."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x06_8040 + 2048*Slice48Id + stsId"
            
        def startAddress(self):
            return 0x00068040
            
        def endAddress(self):
            return 0x0006f07f

        class _PrbsHoMonErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "PrbsHoMonErr"
            
            def description(self):
                return "PRBS error. 1: Error 0: Not error"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        class _PrbsHoMonSync(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PrbsHoMonSync"
            
            def description(self):
                return "PRBS monitor sync. Only used for PRBS mode 1: Data PRBS sync (Not error) 0: Not sync"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _PrbsHoMonCtlSeqMod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PrbsHoMonCtlSeqMod"
            
            def description(self):
                return "Configure types of PRBS generator . 1: Sequence mode 0: PRBS mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PrbsHoMonErr"] = _AF6CNC0021_RD_PRBS._prbshomon._PrbsHoMonErr()
            allFields["PrbsHoMonSync"] = _AF6CNC0021_RD_PRBS._prbshomon._PrbsHoMonSync()
            allFields["PrbsHoMonCtlSeqMod"] = _AF6CNC0021_RD_PRBS._prbshomon._PrbsHoMonCtlSeqMod()
            return allFields

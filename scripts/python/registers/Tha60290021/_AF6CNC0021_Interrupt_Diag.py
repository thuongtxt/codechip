import AtRegister

class _AF6CNC0021_Interrupt_Diag(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["pcfg_mode"] = _AF6CNC0021_Interrupt_Diag._pcfg_mode()
        allRegisters["pcfg_glb"] = _AF6CNC0021_Interrupt_Diag._pcfg_glb()
        allRegisters["stickyx_int"] = _AF6CNC0021_Interrupt_Diag._stickyx_int()
        return allRegisters

    class _pcfg_mode(AtRegister.AtRegister):
        def name(self):
            return "Configure Raise mode for Interrupt Pin"
    
        def description(self):
            return "This is Configure Raise mode for Interrupt Pin  1- 2"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "Base_0x0 + 0x0"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _out_mode_1_(AtRegister.AtRegisterField):
            def startBit(self):
                return 1
                
            def stopBit(self):
                return 1
        
            def name(self):
                return "out_mode_1_"
            
            def description(self):
                return "Diagnostic Mode for Interrupt 2 1: Interrupt Level ( Raise when enable signal high ) 0: State change (Raise on pos-edge/neg-edge of config enable signal)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _out_mode_0_(AtRegister.AtRegisterField):
            def startBit(self):
                return 0
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "out_mode_0_"
            
            def description(self):
                return "Diagnostic Mode for Interrupt 1 1: Interrupt Level ( Raise when enable signal high ) 0: State change  (Raise on pos-edge/neg-edge of config enable signal)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["out_mode_1_"] = _AF6CNC0021_Interrupt_Diag._pcfg_mode._out_mode_1_()
            allFields["out_mode_0_"] = _AF6CNC0021_Interrupt_Diag._pcfg_mode._out_mode_0_()
            return allFields

    class _pcfg_glb(AtRegister.AtRegister):
        def name(self):
            return "Enable Diagnostic Interrupt"
    
        def description(self):
            return "This is Enable Diagnostic Interrupt 1-2"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "Base_0x1 + 0x1"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _out_glb_1_(AtRegister.AtRegisterField):
            def startBit(self):
                return 1
                
            def stopBit(self):
                return 1
        
            def name(self):
                return "out_glb_1_"
            
            def description(self):
                return "Trige 0->1 to start Enable Diagnostic for Interrupt 2 0->1 : Active Interrupt 1->0 : De-active Interrupt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _out_glb_0_(AtRegister.AtRegisterField):
            def startBit(self):
                return 0
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "out_glb_0_"
            
            def description(self):
                return "Trige 0->1 to start Diagnostic for Interrupt 1 0->1 : Active Interrupt 1->0 : De-active Interrupt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["out_glb_1_"] = _AF6CNC0021_Interrupt_Diag._pcfg_glb._out_glb_1_()
            allFields["out_glb_0_"] = _AF6CNC0021_Interrupt_Diag._pcfg_glb._out_glb_0_()
            return allFields

    class _stickyx_int(AtRegister.AtRegister):
        def name(self):
            return "Status of Diagnostic Interrupt"
    
        def description(self):
            return "This is Status of Diagnostic Interrupt 1-2"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "Base_0x2 + 0x2"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _oint_out(AtRegister.AtRegisterField):
            def startBit(self):
                return 1
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "oint_out"
            
            def description(self):
                return "Interrupt has transition Enabled/ Disable , bit per sub port,"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["oint_out"] = _AF6CNC0021_Interrupt_Diag._stickyx_int._oint_out()
            return allFields

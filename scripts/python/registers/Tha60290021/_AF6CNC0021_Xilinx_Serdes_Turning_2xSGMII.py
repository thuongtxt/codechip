import AtRegister

class _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["SERDES_DRP_PORT"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_DRP_PORT()
        allRegisters["SERDES_LoopBack"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_LoopBack()
        allRegisters["SERDES_POWER_DOWN"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_POWER_DOWN()
        allRegisters["SERDES_PLL_Status"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_PLL_Status()
        allRegisters["SERDES_TX_Reset"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_TX_Reset()
        allRegisters["SERDES_RX_Reset"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_RX_Reset()
        allRegisters["SERDES_LPMDFE_Mode"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_LPMDFE_Mode()
        allRegisters["SERDES_LPMDFE_Reset"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_LPMDFE_Reset()
        allRegisters["SERDES_TXDIFFCTRL"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_TXDIFFCTRL()
        allRegisters["SERDES_TXPOSTCURSOR"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_TXPOSTCURSOR()
        allRegisters["SERDES_TXPRECURSOR"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_TXPRECURSOR()
        allRegisters["prbsctr_pen"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._prbsctr_pen()
        allRegisters["SERDES_prbsctr_pen"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_prbsctr_pen()
        allRegisters["SERDES_PRBS_Status"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_PRBS_Status()
        allRegisters["SERDES_PRBS_RX_PRBS_ERR_CNT"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_PRBS_RX_PRBS_ERR_CNT()
        return allRegisters

    class _SERDES_DRP_PORT(AtRegister.AtRegister):
        def name(self):
            return "SERDES DRP PORT"
    
        def description(self):
            return "Read/Write DRP address of SERDES,"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x800+$P*0x400+$DRP"
            
        def startAddress(self):
            return 0x00000800
            
        def endAddress(self):
            return 0x00000fff

        class _drp_rw(AtRegister.AtRegisterField):
            def startBit(self):
                return 9
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "drp_rw"
            
            def description(self):
                return "DRP read/write value"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["drp_rw"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_DRP_PORT._drp_rw()
            return allFields

    class _SERDES_LoopBack(AtRegister.AtRegister):
        def name(self):
            return "SERDES LoopBack"
    
        def description(self):
            return "Configurate LoopBack,"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _lpback_subport1(AtRegister.AtRegisterField):
            def startBit(self):
                return 7
                
            def stopBit(self):
                return 4
        
            def name(self):
                return "lpback_subport1"
            
            def description(self):
                return "Loopback subport 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lpback_subport0(AtRegister.AtRegisterField):
            def startBit(self):
                return 3
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "lpback_subport0"
            
            def description(self):
                return "Loopback subport 0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lpback_subport1"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_LoopBack._lpback_subport1()
            allFields["lpback_subport0"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_LoopBack._lpback_subport0()
            return allFields

    class _SERDES_POWER_DOWN(AtRegister.AtRegister):
        def name(self):
            return "SERDES POWER DOWN"
    
        def description(self):
            return "Configurate Power Down ,"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _TXPD1(AtRegister.AtRegisterField):
            def startBit(self):
                return 11
                
            def stopBit(self):
                return 10
        
            def name(self):
                return "TXPD1"
            
            def description(self):
                return "Power Down subport 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TXPD0(AtRegister.AtRegisterField):
            def startBit(self):
                return 9
                
            def stopBit(self):
                return 8
        
            def name(self):
                return "TXPD0"
            
            def description(self):
                return "Power Down subport 0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _RXPD1(AtRegister.AtRegisterField):
            def startBit(self):
                return 3
                
            def stopBit(self):
                return 2
        
            def name(self):
                return "RXPD1"
            
            def description(self):
                return "Power Down subport 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _RXPD0(AtRegister.AtRegisterField):
            def startBit(self):
                return 1
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "RXPD0"
            
            def description(self):
                return "Power Down subport 0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TXPD1"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_POWER_DOWN._TXPD1()
            allFields["TXPD0"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_POWER_DOWN._TXPD0()
            allFields["RXPD1"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_POWER_DOWN._RXPD1()
            allFields["RXPD0"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_POWER_DOWN._RXPD0()
            return allFields

    class _SERDES_PLL_Status(AtRegister.AtRegister):
        def name(self):
            return "SERDES PLL Status"
    
        def description(self):
            return "QPLL/CPLL status,"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _QPLL1_Lock_change(AtRegister.AtRegisterField):
            def startBit(self):
                return 29
                
            def stopBit(self):
                return 29
        
            def name(self):
                return "QPLL1_Lock_change"
            
            def description(self):
                return "QPLL1 has transition lock/unlock, Group 0-1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _QPLL0_Lock_change(AtRegister.AtRegisterField):
            def startBit(self):
                return 28
                
            def stopBit(self):
                return 28
        
            def name(self):
                return "QPLL0_Lock_change"
            
            def description(self):
                return "QPLL0 has transition lock/unlock, Group 0-1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _QPLL1_Lock(AtRegister.AtRegisterField):
            def startBit(self):
                return 25
                
            def stopBit(self):
                return 25
        
            def name(self):
                return "QPLL1_Lock"
            
            def description(self):
                return "QPLL0 is Locked, Group 0-1"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _QPLL0_Lock(AtRegister.AtRegisterField):
            def startBit(self):
                return 24
                
            def stopBit(self):
                return 24
        
            def name(self):
                return "QPLL0_Lock"
            
            def description(self):
                return "QPLL0 is Locked, Group 0-1"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _CPLL_Lock_Change(AtRegister.AtRegisterField):
            def startBit(self):
                return 15
                
            def stopBit(self):
                return 12
        
            def name(self):
                return "CPLL_Lock_Change"
            
            def description(self):
                return "CPLL has transition lock/unlock, bit per sub port,"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _CPLL_Lock(AtRegister.AtRegisterField):
            def startBit(self):
                return 3
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "CPLL_Lock"
            
            def description(self):
                return "CPLL is Locked, bit per sub port,"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["QPLL1_Lock_change"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_PLL_Status._QPLL1_Lock_change()
            allFields["QPLL0_Lock_change"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_PLL_Status._QPLL0_Lock_change()
            allFields["QPLL1_Lock"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_PLL_Status._QPLL1_Lock()
            allFields["QPLL0_Lock"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_PLL_Status._QPLL0_Lock()
            allFields["CPLL_Lock_Change"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_PLL_Status._CPLL_Lock_Change()
            allFields["CPLL_Lock"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_PLL_Status._CPLL_Lock()
            return allFields

    class _SERDES_TX_Reset(AtRegister.AtRegister):
        def name(self):
            return "SERDES TX Reset"
    
        def description(self):
            return "Reset TX SERDES,"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _txrst_done(AtRegister.AtRegisterField):
            def startBit(self):
                return 17
                
            def stopBit(self):
                return 16
        
            def name(self):
                return "txrst_done"
            
            def description(self):
                return "TX Reset Done, bit per sub port"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _txrst_trig(AtRegister.AtRegisterField):
            def startBit(self):
                return 1
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "txrst_trig"
            
            def description(self):
                return "Should reset TX_PMA SERDES about 300-500 ns , bit per sub port"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txrst_done"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_TX_Reset._txrst_done()
            allFields["txrst_trig"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_TX_Reset._txrst_trig()
            return allFields

    class _SERDES_RX_Reset(AtRegister.AtRegister):
        def name(self):
            return "SERDES RX Reset"
    
        def description(self):
            return "Reset RX SERDES,"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _rxrst_done(AtRegister.AtRegisterField):
            def startBit(self):
                return 17
                
            def stopBit(self):
                return 16
        
            def name(self):
                return "rxrst_done"
            
            def description(self):
                return "RX Reset Done, bit per sub port"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _rxrst_trig(AtRegister.AtRegisterField):
            def startBit(self):
                return 1
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "rxrst_trig"
            
            def description(self):
                return "Should reset reset RX_PMA SERDES about 300-500 ns , bit per sub port"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxrst_done"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_RX_Reset._rxrst_done()
            allFields["rxrst_trig"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_RX_Reset._rxrst_trig()
            return allFields

    class _SERDES_LPMDFE_Mode(AtRegister.AtRegister):
        def name(self):
            return "SERDES LPMDFE Mode"
    
        def description(self):
            return "Configure LPM/DFE mode ,"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _lpmdfe_mode(AtRegister.AtRegisterField):
            def startBit(self):
                return 1
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "lpmdfe_mode"
            
            def description(self):
                return "bit per sub port"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lpmdfe_mode"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_LPMDFE_Mode._lpmdfe_mode()
            return allFields

    class _SERDES_LPMDFE_Reset(AtRegister.AtRegister):
        def name(self):
            return "SERDES LPMDFE Reset"
    
        def description(self):
            return "Reset LPM/DFE ,"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _lpmdfe_reset(AtRegister.AtRegisterField):
            def startBit(self):
                return 1
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "lpmdfe_reset"
            
            def description(self):
                return "bit per sub port, Must be toggled after switching between modes to initialize adaptation"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lpmdfe_reset"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_LPMDFE_Reset._lpmdfe_reset()
            return allFields

    class _SERDES_TXDIFFCTRL(AtRegister.AtRegister):
        def name(self):
            return "SERDES TXDIFFCTRL"
    
        def description(self):
            return "Driver Swing Control, see \"Table 3-35: TX Configurable Driver Ports\" page 158 of UG578 for more detail, there is  2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _TXDIFFCTRL_subport1(AtRegister.AtRegisterField):
            def startBit(self):
                return 9
                
            def stopBit(self):
                return 5
        
            def name(self):
                return "TXDIFFCTRL_subport1"
            
            def description(self):
                return ""
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TXDIFFCTRL_subport0(AtRegister.AtRegisterField):
            def startBit(self):
                return 4
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "TXDIFFCTRL_subport0"
            
            def description(self):
                return ""
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TXDIFFCTRL_subport1"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_TXDIFFCTRL._TXDIFFCTRL_subport1()
            allFields["TXDIFFCTRL_subport0"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_TXDIFFCTRL._TXDIFFCTRL_subport0()
            return allFields

    class _SERDES_TXPOSTCURSOR(AtRegister.AtRegister):
        def name(self):
            return "SERDES TXPOSTCURSOR"
    
        def description(self):
            return "Transmitter post-cursor TX pre-emphasis control, see \"Table 3-35: TX Configurable Driver Ports\" page 160 of UG578 for more detail, there is  2 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _TXPOSTCURSOR_subport1(AtRegister.AtRegisterField):
            def startBit(self):
                return 9
                
            def stopBit(self):
                return 5
        
            def name(self):
                return "TXPOSTCURSOR_subport1"
            
            def description(self):
                return ""
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TXPOSTCURSOR_subport0(AtRegister.AtRegisterField):
            def startBit(self):
                return 4
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "TXPOSTCURSOR_subport0"
            
            def description(self):
                return ""
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TXPOSTCURSOR_subport1"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_TXPOSTCURSOR._TXPOSTCURSOR_subport1()
            allFields["TXPOSTCURSOR_subport0"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_TXPOSTCURSOR._TXPOSTCURSOR_subport0()
            return allFields

    class _SERDES_TXPRECURSOR(AtRegister.AtRegister):
        def name(self):
            return "SERDES TXPRECURSOR"
    
        def description(self):
            return "Transmitter pre-cursor TX pre-emphasis control, see \"Table 3-35: TX Configurable Driver Ports\" page 161 of UG578 for more detail, there is  2 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _TXPRECURSOR_subport1(AtRegister.AtRegisterField):
            def startBit(self):
                return 9
                
            def stopBit(self):
                return 5
        
            def name(self):
                return "TXPRECURSOR_subport1"
            
            def description(self):
                return ""
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TXPRECURSOR_subport0(AtRegister.AtRegisterField):
            def startBit(self):
                return 4
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "TXPRECURSOR_subport0"
            
            def description(self):
                return ""
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TXPRECURSOR_subport1"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_TXPRECURSOR._TXPRECURSOR_subport1()
            allFields["TXPRECURSOR_subport0"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_TXPRECURSOR._TXPRECURSOR_subport0()
            return allFields

    class _prbsctr_pen(AtRegister.AtRegister):
        def name(self):
            return "SERDES PRBS generator test pattern control"
    
        def description(self):
            return "Transmitter pre-cursor TX pre-emphasis control, see \"Table 3-35: TX Configurable Driver Ports\" page 161 of UG578 for more detail, there is  2 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _PRBSSEL_subport1(AtRegister.AtRegisterField):
            def startBit(self):
                return 7
                
            def stopBit(self):
                return 4
        
            def name(self):
                return "PRBSSEL_subport1"
            
            def description(self):
                return "Transmitter PRBS generator and Receiver PRBS checker test pattern control.After changing patterns, perform a reset of the RX (GTRXRESET, RXPMARESET) or a reset of the PRBS error counter (RXPRBSCNTRESET) such that the RX pattern checker can attempt to reestablish the link acquired. No checking is done for non-PRBS patterns."
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _PRBSSEL_subport0(AtRegister.AtRegisterField):
            def startBit(self):
                return 3
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "PRBSSEL_subport0"
            
            def description(self):
                return "Transmitter PRBS generator and Receiver PRBS checker test pattern control.After changing patterns, perform a reset of the RX (GTRXRESET, RXPMARESET) or a reset of the PRBS error counter (RXPRBSCNTRESET) such that the RX pattern checker can attempt to reestablish the link acquired. No checking is done for non-PRBS patterns."
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PRBSSEL_subport1"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._prbsctr_pen._PRBSSEL_subport1()
            allFields["PRBSSEL_subport0"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._prbsctr_pen._PRBSSEL_subport0()
            return allFields

    class _SERDES_prbsctr_pen(AtRegister.AtRegister):
        def name(self):
            return "SERDES prbsctr_pen"
    
        def description(self):
            return "Transmitter pre-cursor TX pre-emphasis control, see \"Table 3-35: TX Configurable Driver Ports\" page 161 of UG578 for more detail, there is  2 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _RXPRBSCNTRESET_subport1(AtRegister.AtRegisterField):
            def startBit(self):
                return 5
                
            def stopBit(self):
                return 5
        
            def name(self):
                return "RXPRBSCNTRESET_subport1"
            
            def description(self):
                return "Resets the PRBS error counter."
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _RXPRBSCNTRESET_subport0(AtRegister.AtRegisterField):
            def startBit(self):
                return 4
                
            def stopBit(self):
                return 4
        
            def name(self):
                return "RXPRBSCNTRESET_subport0"
            
            def description(self):
                return "Resets the PRBS error counter."
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TXPRBSFORCEERR_subport1(AtRegister.AtRegisterField):
            def startBit(self):
                return 1
                
            def stopBit(self):
                return 1
        
            def name(self):
                return "TXPRBSFORCEERR_subport1"
            
            def description(self):
                return "When this port is driven High, errors are forced in the PRBS transmitter. While this port is asserted, the output data pattern contains errors. When PRBSSEL_subportx is set to 0, this port does not affect TXDATA"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TXPRBSFORCEERR_subport0(AtRegister.AtRegisterField):
            def startBit(self):
                return 0
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "TXPRBSFORCEERR_subport0"
            
            def description(self):
                return "When this port is driven High, errors are forced in the PRBS transmitter. While this port is asserted, the output data pattern contains errors. When PRBSSEL_subportx is set to 0, this port does not affect TXDATA"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RXPRBSCNTRESET_subport1"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_prbsctr_pen._RXPRBSCNTRESET_subport1()
            allFields["RXPRBSCNTRESET_subport0"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_prbsctr_pen._RXPRBSCNTRESET_subport0()
            allFields["TXPRBSFORCEERR_subport1"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_prbsctr_pen._TXPRBSFORCEERR_subport1()
            allFields["TXPRBSFORCEERR_subport0"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_prbsctr_pen._TXPRBSFORCEERR_subport0()
            return allFields

    class _SERDES_PRBS_Status(AtRegister.AtRegister):
        def name(self):
            return "SERDES PRBS Diag Status"
    
        def description(self):
            return "QPLL/CPLL status,"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _RXPRBSLOCKED(AtRegister.AtRegisterField):
            def startBit(self):
                return 11
                
            def stopBit(self):
                return 10
        
            def name(self):
                return "RXPRBSLOCKED"
            
            def description(self):
                return "Output to indicate that the RX PRBS checker has been error free after reset. Once asserted High, RXPRBSLOCKED does not deassert until reset of the RX pattern checker via a reset of the RX (GTRXRESET, RXPMARESET, or RXPCSRESET in sequential mode) or a reset of the PRBS error counter (RXPRBSCNTRESET), bit per sub port,"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _RXPRBSERR(AtRegister.AtRegisterField):
            def startBit(self):
                return 9
                
            def stopBit(self):
                return 8
        
            def name(self):
                return "RXPRBSERR"
            
            def description(self):
                return "This non-sticky status output indicates that PRBS errors have occurred. , bit per sub port,"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _RXPRBSLOCKED_Change(AtRegister.AtRegisterField):
            def startBit(self):
                return 3
                
            def stopBit(self):
                return 2
        
            def name(self):
                return "RXPRBSLOCKED_Change"
            
            def description(self):
                return "This sticky status output indicates that PRBS not locked, bit per sub port,"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _RXPRBSERR_Change(AtRegister.AtRegisterField):
            def startBit(self):
                return 1
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "RXPRBSERR_Change"
            
            def description(self):
                return "This sticky status output indicates that PRBS errors have occurred. , bit per sub port,"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RXPRBSLOCKED"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_PRBS_Status._RXPRBSLOCKED()
            allFields["RXPRBSERR"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_PRBS_Status._RXPRBSERR()
            allFields["RXPRBSLOCKED_Change"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_PRBS_Status._RXPRBSLOCKED_Change()
            allFields["RXPRBSERR_Change"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_PRBS_Status._RXPRBSERR_Change()
            return allFields

    class _SERDES_PRBS_RX_PRBS_ERR_CNT(AtRegister.AtRegister):
        def name(self):
            return "SERDES PRBS RX_PRBS_ERR_CNT"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000025e
            
        def endAddress(self):
            return 0x0000025f

        class _RX_PRBS_ERR_CNT(AtRegister.AtRegisterField):
            def startBit(self):
                return 31
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "RX_PRBS_ERR_CNT"
            
            def description(self):
                return "PRBS error counter. This counter can be reset by asserting RXPRBSCNTRESET. When a single bit error in incoming data occurs, this counter increments by 1. Single bit errors are counted thus when multiple bit errors occur in incoming data. The counter increments by the actual number of bit errors. Counting begins after config mode and prbs syn . The counter saturates at 32'hFFFFFFFF. This error counter can only be accessed via the DRP interface. Because the DRP only outputs 16 bits of data per operation, two DRP transactions must be completed to read out the complete 32-bit value. To properly read out the error counter, read out the lower 16 bits at address 0x25E first, followed by the upper 16 bits at address 0x25F"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RX_PRBS_ERR_CNT"] = _AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII._SERDES_PRBS_RX_PRBS_ERR_CNT._RX_PRBS_ERR_CNT()
            return allFields

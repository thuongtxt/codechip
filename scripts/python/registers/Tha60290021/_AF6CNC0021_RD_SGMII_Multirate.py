import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0021_RD_SGMII_Multirate(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["version_pen"] = _AF6CNC0021_RD_SGMII_Multirate._version_pen()
        allRegisters["an_mod_pen0"] = _AF6CNC0021_RD_SGMII_Multirate._an_mod_pen0()
        allRegisters["an_rst_pen0"] = _AF6CNC0021_RD_SGMII_Multirate._an_rst_pen0()
        allRegisters["an_enb_pen0"] = _AF6CNC0021_RD_SGMII_Multirate._an_enb_pen0()
        allRegisters["an_spd_pen0"] = _AF6CNC0021_RD_SGMII_Multirate._an_spd_pen0()
        allRegisters["an_sta_pen00"] = _AF6CNC0021_RD_SGMII_Multirate._an_sta_pen00()
        allRegisters["lnk_sync_pen0"] = _AF6CNC0021_RD_SGMII_Multirate._lnk_sync_pen0()
        allRegisters["an_rxab_pen00"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen00()
        allRegisters["an_rxab_pen01"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen01()
        allRegisters["an_rxab_pen02"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen02()
        allRegisters["an_rxab_pen03"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen03()
        allRegisters["an_rxab_pen00"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen00()
        allRegisters["an_rxab_pen01"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen01()
        allRegisters["an_rxab_pen02"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen02()
        allRegisters["an_rxab_pen03"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen03()
        allRegisters["los_sync_stk_pen"] = _AF6CNC0021_RD_SGMII_Multirate._los_sync_stk_pen()
        allRegisters["los_sync_pen"] = _AF6CNC0021_RD_SGMII_Multirate._los_sync_pen()
        allRegisters["los_sync_int_enb_pen"] = _AF6CNC0021_RD_SGMII_Multirate._los_sync_int_enb_pen()
        allRegisters["an_sta_stk_pen"] = _AF6CNC0021_RD_SGMII_Multirate._an_sta_stk_pen()
        allRegisters["an_int_enb_pen"] = _AF6CNC0021_RD_SGMII_Multirate._an_int_enb_pen()
        allRegisters["an_rfi_stk_pen"] = _AF6CNC0021_RD_SGMII_Multirate._an_rfi_stk_pen()
        allRegisters["an_rfi_int_enb_pen"] = _AF6CNC0021_RD_SGMII_Multirate._an_rfi_int_enb_pen()
        allRegisters["rxexcer_sta_pen"] = _AF6CNC0021_RD_SGMII_Multirate._rxexcer_sta_pen()
        allRegisters["rxexcer_stk_pen"] = _AF6CNC0021_RD_SGMII_Multirate._rxexcer_stk_pen()
        allRegisters["rxexcer_enb_pen"] = _AF6CNC0021_RD_SGMII_Multirate._rxexcer_enb_pen()
        allRegisters["GE_Interrupt_OR"] = _AF6CNC0021_RD_SGMII_Multirate._GE_Interrupt_OR()
        allRegisters["GE_Loss_Of_Sync_AND_MASK"] = _AF6CNC0021_RD_SGMII_Multirate._GE_Loss_Of_Sync_AND_MASK()
        allRegisters["GE_Auto_neg_State_Change_AND_MASK"] = _AF6CNC0021_RD_SGMII_Multirate._GE_Auto_neg_State_Change_AND_MASK()
        allRegisters["GE_Remote_Fault_AND_MASK"] = _AF6CNC0021_RD_SGMII_Multirate._GE_Remote_Fault_AND_MASK()
        allRegisters["GE_Excessive_Error_Ratio_AND_MASK"] = _AF6CNC0021_RD_SGMII_Multirate._GE_Excessive_Error_Ratio_AND_MASK()
        allRegisters["GE_1000basex_Force_K30_7"] = _AF6CNC0021_RD_SGMII_Multirate._GE_1000basex_Force_K30_7()
        allRegisters["GE_TenG_Excessive_Error_Timer"] = _AF6CNC0021_RD_SGMII_Multirate._GE_TenG_Excessive_Error_Timer()
        allRegisters["GE_TenG_Excessive_Error_Threshold_Group0"] = _AF6CNC0021_RD_SGMII_Multirate._GE_TenG_Excessive_Error_Threshold_Group0()
        allRegisters["GE_TenG_Excessive_Error_Threshold_Group1"] = _AF6CNC0021_RD_SGMII_Multirate._GE_TenG_Excessive_Error_Threshold_Group1()
        allRegisters["fx_txenb"] = _AF6CNC0021_RD_SGMII_Multirate._fx_txenb()
        allRegisters["fx_patstken"] = _AF6CNC0021_RD_SGMII_Multirate._fx_patstken()
        allRegisters["fx_codestken"] = _AF6CNC0021_RD_SGMII_Multirate._fx_codestken()
        return allRegisters

    class _version_pen(AtRegister.AtRegister):
        def name(self):
            return "Pedit Block Version"
    
        def description(self):
            return "Pedit Block Version"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000800
            
        def endAddress(self):
            return 0xffffffff

        class _day(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "day"
            
            def description(self):
                return "day"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _month(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "month"
            
            def description(self):
                return "month"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _year(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "year"
            
            def description(self):
                return "year"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _number(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "number"
            
            def description(self):
                return "number"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["day"] = _AF6CNC0021_RD_SGMII_Multirate._version_pen._day()
            allFields["month"] = _AF6CNC0021_RD_SGMII_Multirate._version_pen._month()
            allFields["year"] = _AF6CNC0021_RD_SGMII_Multirate._version_pen._year()
            allFields["number"] = _AF6CNC0021_RD_SGMII_Multirate._version_pen._number()
            return allFields

    class _an_mod_pen0(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg-group0 mode"
    
        def description(self):
            return "Select Auto-neg mode for 1000Basex or SGMII"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000801
            
        def endAddress(self):
            return 0xffffffff

        class _tx_enb0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "tx_enb0"
            
            def description(self):
                return "Enable TX side bit per port, bit[16] port 0, bit[31] port 15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _an_mod0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "an_mod0"
            
            def description(self):
                return "Auto-neg mode, bit per port, bit[0] port 0, bit[15] port 15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tx_enb0"] = _AF6CNC0021_RD_SGMII_Multirate._an_mod_pen0._tx_enb0()
            allFields["an_mod0"] = _AF6CNC0021_RD_SGMII_Multirate._an_mod_pen0._an_mod0()
            return allFields

    class _an_rst_pen0(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg-group0 reset"
    
        def description(self):
            return "Restart Auto-neg process of group0 from port 0 to port 15"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000802
            
        def endAddress(self):
            return 0xffffffff

        class _an_rst0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "an_rst0"
            
            def description(self):
                return "Auto-neg reset, sw write 1 then write 0 to restart auto-neg bit per port, bit[0] port 0, bit[15] port 15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["an_rst0"] = _AF6CNC0021_RD_SGMII_Multirate._an_rst_pen0._an_rst0()
            return allFields

    class _an_enb_pen0(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg-group0 enable"
    
        def description(self):
            return "enable/disbale Auto-neg of group 0 from port 0 to port 15"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000803
            
        def endAddress(self):
            return 0xffffffff

        class _an_enb0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "an_enb0"
            
            def description(self):
                return "Auto-neg enb , bit per port, bit[0] port 0, bit[15] port 15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["an_enb0"] = _AF6CNC0021_RD_SGMII_Multirate._an_enb_pen0._an_enb0()
            return allFields

    class _an_spd_pen0(AtRegister.AtRegister):
        def name(self):
            return "Config_speed-group0"
    
        def description(self):
            return "configure speed when disable Auto-neg of group 0 from port 0 to 15"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000804
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_spd15(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 30
        
            def name(self):
                return "cfg_spd15"
            
            def description(self):
                return "Speed port 15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd14(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 28
        
            def name(self):
                return "cfg_spd14"
            
            def description(self):
                return "Speed port 14"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd13(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 26
        
            def name(self):
                return "cfg_spd13"
            
            def description(self):
                return "Speed port 13"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 24
        
            def name(self):
                return "cfg_spd12"
            
            def description(self):
                return "Speed port 12"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd11(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 22
        
            def name(self):
                return "cfg_spd11"
            
            def description(self):
                return "Speed port 11"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd10(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 20
        
            def name(self):
                return "cfg_spd10"
            
            def description(self):
                return "Speed port 10"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd09(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 18
        
            def name(self):
                return "cfg_spd09"
            
            def description(self):
                return "Speed port 9"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd08(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 16
        
            def name(self):
                return "cfg_spd08"
            
            def description(self):
                return "Speed port 8"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd07(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 14
        
            def name(self):
                return "cfg_spd07"
            
            def description(self):
                return "Speed port 7"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd06(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "cfg_spd06"
            
            def description(self):
                return "Speed port 6"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd05(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "cfg_spd05"
            
            def description(self):
                return "Speed port 5"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd04(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "cfg_spd04"
            
            def description(self):
                return "Speed port 4"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd03(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "cfg_spd03"
            
            def description(self):
                return "Speed port 3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd02(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "cfg_spd02"
            
            def description(self):
                return "Speed port 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd01(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "cfg_spd01"
            
            def description(self):
                return "Speed port 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_spd00"
            
            def description(self):
                return "Speed port 0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_spd15"] = _AF6CNC0021_RD_SGMII_Multirate._an_spd_pen0._cfg_spd15()
            allFields["cfg_spd14"] = _AF6CNC0021_RD_SGMII_Multirate._an_spd_pen0._cfg_spd14()
            allFields["cfg_spd13"] = _AF6CNC0021_RD_SGMII_Multirate._an_spd_pen0._cfg_spd13()
            allFields["cfg_spd12"] = _AF6CNC0021_RD_SGMII_Multirate._an_spd_pen0._cfg_spd12()
            allFields["cfg_spd11"] = _AF6CNC0021_RD_SGMII_Multirate._an_spd_pen0._cfg_spd11()
            allFields["cfg_spd10"] = _AF6CNC0021_RD_SGMII_Multirate._an_spd_pen0._cfg_spd10()
            allFields["cfg_spd09"] = _AF6CNC0021_RD_SGMII_Multirate._an_spd_pen0._cfg_spd09()
            allFields["cfg_spd08"] = _AF6CNC0021_RD_SGMII_Multirate._an_spd_pen0._cfg_spd08()
            allFields["cfg_spd07"] = _AF6CNC0021_RD_SGMII_Multirate._an_spd_pen0._cfg_spd07()
            allFields["cfg_spd06"] = _AF6CNC0021_RD_SGMII_Multirate._an_spd_pen0._cfg_spd06()
            allFields["cfg_spd05"] = _AF6CNC0021_RD_SGMII_Multirate._an_spd_pen0._cfg_spd05()
            allFields["cfg_spd04"] = _AF6CNC0021_RD_SGMII_Multirate._an_spd_pen0._cfg_spd04()
            allFields["cfg_spd03"] = _AF6CNC0021_RD_SGMII_Multirate._an_spd_pen0._cfg_spd03()
            allFields["cfg_spd02"] = _AF6CNC0021_RD_SGMII_Multirate._an_spd_pen0._cfg_spd02()
            allFields["cfg_spd01"] = _AF6CNC0021_RD_SGMII_Multirate._an_spd_pen0._cfg_spd01()
            allFields["cfg_spd00"] = _AF6CNC0021_RD_SGMII_Multirate._an_spd_pen0._cfg_spd00()
            return allFields

    class _an_sta_pen00(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg-group00 status"
    
        def description(self):
            return "status of Auto-neg of group0 from port 0 to port 7"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000805
            
        def endAddress(self):
            return 0xffffffff

        class _an_sta07(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 28
        
            def name(self):
                return "an_sta07"
            
            def description(self):
                return "Speed port 7"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_sta06(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 24
        
            def name(self):
                return "an_sta06"
            
            def description(self):
                return "Speed port 6"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_sta05(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 20
        
            def name(self):
                return "an_sta05"
            
            def description(self):
                return "Speed port 5"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_sta04(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "an_sta04"
            
            def description(self):
                return "Speed port 4"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_sta03(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "an_sta03"
            
            def description(self):
                return "Speed port 3"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_sta02(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "an_sta02"
            
            def description(self):
                return "Speed port 2"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_sta01(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "an_sta01"
            
            def description(self):
                return "Speed port 1"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_sta00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "an_sta00"
            
            def description(self):
                return "Speed port 0"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["an_sta07"] = _AF6CNC0021_RD_SGMII_Multirate._an_sta_pen00._an_sta07()
            allFields["an_sta06"] = _AF6CNC0021_RD_SGMII_Multirate._an_sta_pen00._an_sta06()
            allFields["an_sta05"] = _AF6CNC0021_RD_SGMII_Multirate._an_sta_pen00._an_sta05()
            allFields["an_sta04"] = _AF6CNC0021_RD_SGMII_Multirate._an_sta_pen00._an_sta04()
            allFields["an_sta03"] = _AF6CNC0021_RD_SGMII_Multirate._an_sta_pen00._an_sta03()
            allFields["an_sta02"] = _AF6CNC0021_RD_SGMII_Multirate._an_sta_pen00._an_sta02()
            allFields["an_sta01"] = _AF6CNC0021_RD_SGMII_Multirate._an_sta_pen00._an_sta01()
            allFields["an_sta00"] = _AF6CNC0021_RD_SGMII_Multirate._an_sta_pen00._an_sta00()
            return allFields

    class _lnk_sync_pen0(AtRegister.AtRegister):
        def name(self):
            return "Link-status-group0"
    
        def description(self):
            return "Link status of group 0 from port 0  to port 15"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000808
            
        def endAddress(self):
            return 0xffffffff

        class _lnk_sta0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lnk_sta0"
            
            def description(self):
                return "Link status, bit per port, bit[0] port  0, bit[7] port 7"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lnk_sta0"] = _AF6CNC0021_RD_SGMII_Multirate._lnk_sync_pen0._lnk_sta0()
            return allFields

    class _an_rxab_pen00(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg RX00-RX01 SGMII ability"
    
        def description(self):
            return "RX ability of RX-Port00-01 after Auto-neg succeed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000a00
            
        def endAddress(self):
            return 0xffffffff

        class _link01(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "link01"
            
            def description(self):
                return "Link status of port1"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex01(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "duplex01"
            
            def description(self):
                return "duplex mode of port1"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed01(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 26
        
            def name(self):
                return "speed01"
            
            def description(self):
                return "speed of SGMII of port1"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _link00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "link00"
            
            def description(self):
                return "Link status of port0"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "duplex00"
            
            def description(self):
                return "duplex mode of port0"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "speed00"
            
            def description(self):
                return "speed of SGMII of port0"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["link01"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen00._link01()
            allFields["duplex01"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen00._duplex01()
            allFields["speed01"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen00._speed01()
            allFields["link00"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen00._link00()
            allFields["duplex00"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen00._duplex00()
            allFields["speed00"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen00._speed00()
            return allFields

    class _an_rxab_pen01(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg RX02-RX03 SGMII ability"
    
        def description(self):
            return "RX ability of RX-Port02-03 after Auto-neg succeed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000a01
            
        def endAddress(self):
            return 0xffffffff

        class _link03(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "link03"
            
            def description(self):
                return "Link status of port3"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex03(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "duplex03"
            
            def description(self):
                return "duplex mode of port3"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed03(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 26
        
            def name(self):
                return "speed03"
            
            def description(self):
                return "speed of SGMII of port3"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _link02(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "link02"
            
            def description(self):
                return "Link status of port2"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex02(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "duplex02"
            
            def description(self):
                return "duplex mode of port2"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed02(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "speed02"
            
            def description(self):
                return "speed of SGMII of port2"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["link03"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen01._link03()
            allFields["duplex03"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen01._duplex03()
            allFields["speed03"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen01._speed03()
            allFields["link02"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen01._link02()
            allFields["duplex02"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen01._duplex02()
            allFields["speed02"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen01._speed02()
            return allFields

    class _an_rxab_pen02(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg RX04-RX05 SGMII ability"
    
        def description(self):
            return "RX ability of RX-Port04-05 after Auto-neg succeed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000a02
            
        def endAddress(self):
            return 0xffffffff

        class _link05(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "link05"
            
            def description(self):
                return "Link status of port5"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex05(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "duplex05"
            
            def description(self):
                return "duplex mode of port5"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed05(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 26
        
            def name(self):
                return "speed05"
            
            def description(self):
                return "speed of SGMII of port5"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _link04(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "link04"
            
            def description(self):
                return "Link status of port4"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex04(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "duplex04"
            
            def description(self):
                return "duplex mode of port4"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed04(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "speed04"
            
            def description(self):
                return "speed of SGMII of port4"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["link05"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen02._link05()
            allFields["duplex05"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen02._duplex05()
            allFields["speed05"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen02._speed05()
            allFields["link04"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen02._link04()
            allFields["duplex04"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen02._duplex04()
            allFields["speed04"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen02._speed04()
            return allFields

    class _an_rxab_pen03(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg RX06-RX07 SGMII ability"
    
        def description(self):
            return "RX ability of RX-Port06-07 after Auto-neg succeed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000a03
            
        def endAddress(self):
            return 0xffffffff

        class _link07(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "link07"
            
            def description(self):
                return "Link status of port7"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex07(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "duplex07"
            
            def description(self):
                return "duplex mode of port7"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed07(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 26
        
            def name(self):
                return "speed07"
            
            def description(self):
                return "speed of SGMII of port7"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _link06(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "link06"
            
            def description(self):
                return "Link status of port6"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex06(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "duplex06"
            
            def description(self):
                return "duplex mode of port6"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed06(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "speed06"
            
            def description(self):
                return "speed of SGMII of port6"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["link07"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen03._link07()
            allFields["duplex07"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen03._duplex07()
            allFields["speed07"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen03._speed07()
            allFields["link06"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen03._link06()
            allFields["duplex06"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen03._duplex06()
            allFields["speed06"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen03._speed06()
            return allFields

    class _an_rxab_pen00(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg RX00-RX01 1000Basex ability"
    
        def description(self):
            return "RX ability of RX-Port00-01 after Auto-neg succeed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000a00
            
        def endAddress(self):
            return 0xffffffff

        class _NP01(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "NP01"
            
            def description(self):
                return "Next page of port1"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Ack01(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "Ack01"
            
            def description(self):
                return "Acknowledge of port1"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _ReFault01(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 28
        
            def name(self):
                return "ReFault01"
            
            def description(self):
                return "Remote Fault of port1"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Hduplex01(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "Hduplex01"
            
            def description(self):
                return "Half duplex of port1"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Fduplex01(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "Fduplex01"
            
            def description(self):
                return "Full duplex of port1"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _NP00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "NP00"
            
            def description(self):
                return "Next page of port0"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Ack00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "Ack00"
            
            def description(self):
                return "Acknowledge of port0"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _ReFault00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "ReFault00"
            
            def description(self):
                return "Remote Fault of port0"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Hduplex00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Hduplex00"
            
            def description(self):
                return "Half duplex of port0"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Fduplex00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Fduplex00"
            
            def description(self):
                return "Full duplex of port0"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["NP01"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen00._NP01()
            allFields["Ack01"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen00._Ack01()
            allFields["ReFault01"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen00._ReFault01()
            allFields["Hduplex01"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen00._Hduplex01()
            allFields["Fduplex01"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen00._Fduplex01()
            allFields["NP00"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen00._NP00()
            allFields["Ack00"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen00._Ack00()
            allFields["ReFault00"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen00._ReFault00()
            allFields["Hduplex00"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen00._Hduplex00()
            allFields["Fduplex00"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen00._Fduplex00()
            return allFields

    class _an_rxab_pen01(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg RX02-RX03 1000Basex ability"
    
        def description(self):
            return "RX ability of RX-Port02-03 after Auto-neg succeed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000a01
            
        def endAddress(self):
            return 0xffffffff

        class _NP03(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "NP03"
            
            def description(self):
                return "Next page  of port3"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Ack03(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "Ack03"
            
            def description(self):
                return "Acknowledge of port3"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _ReFault03(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 28
        
            def name(self):
                return "ReFault03"
            
            def description(self):
                return "Remote Fault of port3"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Hduplex03(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "Hduplex03"
            
            def description(self):
                return "Half duplex of port3"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Fduplex03(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "Fduplex03"
            
            def description(self):
                return "Full duplex of port3"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _NP02(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "NP02"
            
            def description(self):
                return "Next page of port2"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Ack02(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "Ack02"
            
            def description(self):
                return "Acknowledge of port2"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _ReFault02(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "ReFault02"
            
            def description(self):
                return "Remote Fault of port2"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Hduplex02(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Hduplex02"
            
            def description(self):
                return "Half duplex of port2"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Fduplex02(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Fduplex02"
            
            def description(self):
                return "Full duplex of port2"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["NP03"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen01._NP03()
            allFields["Ack03"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen01._Ack03()
            allFields["ReFault03"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen01._ReFault03()
            allFields["Hduplex03"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen01._Hduplex03()
            allFields["Fduplex03"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen01._Fduplex03()
            allFields["NP02"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen01._NP02()
            allFields["Ack02"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen01._Ack02()
            allFields["ReFault02"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen01._ReFault02()
            allFields["Hduplex02"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen01._Hduplex02()
            allFields["Fduplex02"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen01._Fduplex02()
            return allFields

    class _an_rxab_pen02(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg RX04-RX05 1000Basex ability"
    
        def description(self):
            return "RX ability of RX-Port04-05 after Auto-neg succeed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000a02
            
        def endAddress(self):
            return 0xffffffff

        class _NP05(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "NP05"
            
            def description(self):
                return "Next page  of port5"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Ack05(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "Ack05"
            
            def description(self):
                return "Acknowledge of port5"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _ReFault05(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 28
        
            def name(self):
                return "ReFault05"
            
            def description(self):
                return "Remote Fault of port5"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Hduplex05(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "Hduplex05"
            
            def description(self):
                return "Half duplex of port5"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Fduplex05(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "Fduplex05"
            
            def description(self):
                return "Full duplex of port5"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _NP04(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "NP04"
            
            def description(self):
                return "Next page of port4"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Ack04(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "Ack04"
            
            def description(self):
                return "Acknowledge of port4"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _ReFault04(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "ReFault04"
            
            def description(self):
                return "Remote Fault of port4"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Hduplex04(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Hduplex04"
            
            def description(self):
                return "Half duplex of port4"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Fduplex04(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Fduplex04"
            
            def description(self):
                return "Full duplex of port4"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["NP05"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen02._NP05()
            allFields["Ack05"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen02._Ack05()
            allFields["ReFault05"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen02._ReFault05()
            allFields["Hduplex05"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen02._Hduplex05()
            allFields["Fduplex05"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen02._Fduplex05()
            allFields["NP04"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen02._NP04()
            allFields["Ack04"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen02._Ack04()
            allFields["ReFault04"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen02._ReFault04()
            allFields["Hduplex04"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen02._Hduplex04()
            allFields["Fduplex04"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen02._Fduplex04()
            return allFields

    class _an_rxab_pen03(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg RX06-RX07 1000Basex ability"
    
        def description(self):
            return "RX ability of RX-Port06-07 after Auto-neg succeed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000a03
            
        def endAddress(self):
            return 0xffffffff

        class _NP07(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "NP07"
            
            def description(self):
                return "Next page  of port7"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Ack07(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "Ack07"
            
            def description(self):
                return "Acknowledge of port7"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _ReFault07(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 28
        
            def name(self):
                return "ReFault07"
            
            def description(self):
                return "Remote Fault of port7"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Hduplex07(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "Hduplex07"
            
            def description(self):
                return "Half duplex of port7"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Fduplex07(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "Fduplex07"
            
            def description(self):
                return "Full duplex of port7"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _NP06(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "NP06"
            
            def description(self):
                return "Next page of port6"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Ack06(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "Ack06"
            
            def description(self):
                return "Acknowledge of port6"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _ReFault06(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "ReFault06"
            
            def description(self):
                return "Remote Fault of port6"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Hduplex06(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Hduplex06"
            
            def description(self):
                return "Half duplex of port6"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Fduplex06(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Fduplex06"
            
            def description(self):
                return "Full duplex of port6"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["NP07"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen03._NP07()
            allFields["Ack07"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen03._Ack07()
            allFields["ReFault07"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen03._ReFault07()
            allFields["Hduplex07"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen03._Hduplex07()
            allFields["Fduplex07"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen03._Fduplex07()
            allFields["NP06"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen03._NP06()
            allFields["Ack06"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen03._Ack06()
            allFields["ReFault06"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen03._ReFault06()
            allFields["Hduplex06"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen03._Hduplex06()
            allFields["Fduplex06"] = _AF6CNC0021_RD_SGMII_Multirate._an_rxab_pen03._Fduplex06()
            return allFields

    class _los_sync_stk_pen(AtRegister.AtRegister):
        def name(self):
            return "GE Loss Of Synchronization Sticky"
    
        def description(self):
            return "Sticky state change Loss of synchronization"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000080b
            
        def endAddress(self):
            return 0xffffffff

        class _GeLossOfSync_Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "GeLossOfSync_Stk"
            
            def description(self):
                return "Sticky state change of Loss Of Synchronization, bit per port, bit0 is port0, bit7 is port7"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["GeLossOfSync_Stk"] = _AF6CNC0021_RD_SGMII_Multirate._los_sync_stk_pen._GeLossOfSync_Stk()
            return allFields

    class _los_sync_pen(AtRegister.AtRegister):
        def name(self):
            return "GE Loss Of Synchronization Status"
    
        def description(self):
            return "Current status of Loss of synchronization"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000080a
            
        def endAddress(self):
            return 0xffffffff

        class _GeLossOfSync_Cur(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "GeLossOfSync_Cur"
            
            def description(self):
                return "Current status of Loss Of Synchronization, bit per port, bit0 is port0, bit7 is port7"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["GeLossOfSync_Cur"] = _AF6CNC0021_RD_SGMII_Multirate._los_sync_pen._GeLossOfSync_Cur()
            return allFields

    class _los_sync_int_enb_pen(AtRegister.AtRegister):
        def name(self):
            return "GE Loss Of Synchronization Interrupt Enb"
    
        def description(self):
            return "Interrupt enable of Loss of synchronization"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000818
            
        def endAddress(self):
            return 0xffffffff

        class _GeLossOfSync_Enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "GeLossOfSync_Enb"
            
            def description(self):
                return "Interrupt enable of Loss of synchronization, bit per port, bit0 is port0, bit7 is port7"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["GeLossOfSync_Enb"] = _AF6CNC0021_RD_SGMII_Multirate._los_sync_int_enb_pen._GeLossOfSync_Enb()
            return allFields

    class _an_sta_stk_pen(AtRegister.AtRegister):
        def name(self):
            return "GE Auto-neg Sticky"
    
        def description(self):
            return "Sticky state change Auto-neg"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000807
            
        def endAddress(self):
            return 0xffffffff

        class _GeAuto_neg_Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "GeAuto_neg_Stk"
            
            def description(self):
                return "Sticky state change of Auto-neg , bit per port, bit0 is port0, bit7 is port7"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["GeAuto_neg_Stk"] = _AF6CNC0021_RD_SGMII_Multirate._an_sta_stk_pen._GeAuto_neg_Stk()
            return allFields

    class _an_int_enb_pen(AtRegister.AtRegister):
        def name(self):
            return "GE Auto-neg Interrupt Enb"
    
        def description(self):
            return "Interrupt enable of Loss of synchronization"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000817
            
        def endAddress(self):
            return 0xffffffff

        class _GeAuto_neg_Enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "GeAuto_neg_Enb"
            
            def description(self):
                return "Interrupt enable of Auto-neg, bit per port, bit0 is port0, bit7 is port7"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["GeAuto_neg_Enb"] = _AF6CNC0021_RD_SGMII_Multirate._an_int_enb_pen._GeAuto_neg_Enb()
            return allFields

    class _an_rfi_stk_pen(AtRegister.AtRegister):
        def name(self):
            return "GE Remote Fault Sticky"
    
        def description(self):
            return "Sticky state change Remote Fault"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000819
            
        def endAddress(self):
            return 0xffffffff

        class _GeRemote_Fault_Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "GeRemote_Fault_Stk"
            
            def description(self):
                return "Sticky state change of Remote Fault , bit per port, bit0 is port0, bit7 is port7"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["GeRemote_Fault_Stk"] = _AF6CNC0021_RD_SGMII_Multirate._an_rfi_stk_pen._GeRemote_Fault_Stk()
            return allFields

    class _an_rfi_int_enb_pen(AtRegister.AtRegister):
        def name(self):
            return "GE Remote Fault Interrupt Enb"
    
        def description(self):
            return "Interrupt enable of Remote Fault"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000081a
            
        def endAddress(self):
            return 0xffffffff

        class _GeRemote_Fault_enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "GeRemote_Fault_enb"
            
            def description(self):
                return "Interrupt enable of Remote Fault, bit per port, bit0 is port0, bit7 is port7"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["GeRemote_Fault_enb"] = _AF6CNC0021_RD_SGMII_Multirate._an_rfi_int_enb_pen._GeRemote_Fault_enb()
            return allFields

    class _rxexcer_sta_pen(AtRegister.AtRegister):
        def name(self):
            return "GE Excessive Error Ratio Status"
    
        def description(self):
            return "Interrupt Excessive Error Ratio Status"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000081c
            
        def endAddress(self):
            return 0xffffffff

        class _rxexcer_sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxexcer_sta"
            
            def description(self):
                return "Interrupt status of Excessive Error Ratio, bit per port, bit0 is port0, bit7 is port7"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxexcer_sta"] = _AF6CNC0021_RD_SGMII_Multirate._rxexcer_sta_pen._rxexcer_sta()
            return allFields

    class _rxexcer_stk_pen(AtRegister.AtRegister):
        def name(self):
            return "GE Excessive Error Ratio Sticky"
    
        def description(self):
            return "Interrupt Excessive Error Ratio Sticky"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000081d
            
        def endAddress(self):
            return 0xffffffff

        class _rxexcer_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxexcer_stk"
            
            def description(self):
                return "Interrupt sticky of Excessive Error Ratio, bit per port, bit0 is port0, bit7 is port7"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxexcer_stk"] = _AF6CNC0021_RD_SGMII_Multirate._rxexcer_stk_pen._rxexcer_stk()
            return allFields

    class _rxexcer_enb_pen(AtRegister.AtRegister):
        def name(self):
            return "GE Excessive Error Ratio Enble"
    
        def description(self):
            return "Interrupt Excessive Error Ratio Enable"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000081e
            
        def endAddress(self):
            return 0xffffffff

        class _rxexcer_enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxexcer_enb"
            
            def description(self):
                return "Interrupt Enable of Excessive Error Ratio, bit per port, bit0 is port0, bit7 is port7"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxexcer_enb"] = _AF6CNC0021_RD_SGMII_Multirate._rxexcer_enb_pen._rxexcer_enb()
            return allFields

    class _GE_Interrupt_OR(AtRegister.AtRegister):
        def name(self):
            return "GE Interrupt OR"
    
        def description(self):
            return "Interrupt OR of all port per interrupt type"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000830
            
        def endAddress(self):
            return 0xffffffff

        class _rxexcer_int_or(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "rxexcer_int_or"
            
            def description(self):
                return "Interrupt OR of Excessive Error Ratio"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_rfi_int_or(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "an_rfi_int_or"
            
            def description(self):
                return "Interrupt OR of Remote fault"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _ant_int_or(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "ant_int_or"
            
            def description(self):
                return "Interrupt OR of Auto-neg state change"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _los_sync_int_or(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "los_sync_int_or"
            
            def description(self):
                return "Interrupt OR of Loss of sync"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxexcer_int_or"] = _AF6CNC0021_RD_SGMII_Multirate._GE_Interrupt_OR._rxexcer_int_or()
            allFields["an_rfi_int_or"] = _AF6CNC0021_RD_SGMII_Multirate._GE_Interrupt_OR._an_rfi_int_or()
            allFields["ant_int_or"] = _AF6CNC0021_RD_SGMII_Multirate._GE_Interrupt_OR._ant_int_or()
            allFields["los_sync_int_or"] = _AF6CNC0021_RD_SGMII_Multirate._GE_Interrupt_OR._los_sync_int_or()
            return allFields

    class _GE_Loss_Of_Sync_AND_MASK(AtRegister.AtRegister):
        def name(self):
            return "GE Loss Of Sync AND MASK"
    
        def description(self):
            return "Interrupt Loss of sync current status AND MASK"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000831
            
        def endAddress(self):
            return 0xffffffff

        class _GeLossOfSync_and_mask(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "GeLossOfSync_and_mask"
            
            def description(self):
                return "Interrupt Loss of sync, bit per port, bit0 is port0, bit7 is port7"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["GeLossOfSync_and_mask"] = _AF6CNC0021_RD_SGMII_Multirate._GE_Loss_Of_Sync_AND_MASK._GeLossOfSync_and_mask()
            return allFields

    class _GE_Auto_neg_State_Change_AND_MASK(AtRegister.AtRegister):
        def name(self):
            return "GE Auto_neg State Change AND MASK"
    
        def description(self):
            return "Interrupt Auto-neg state change current status AND MASK"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000832
            
        def endAddress(self):
            return 0xffffffff

        class _GeAuto_neg_and_mask(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "GeAuto_neg_and_mask"
            
            def description(self):
                return "Interrupt Auto-neg state change, bit per port, bit0 is port0, bit7 is port7"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["GeAuto_neg_and_mask"] = _AF6CNC0021_RD_SGMII_Multirate._GE_Auto_neg_State_Change_AND_MASK._GeAuto_neg_and_mask()
            return allFields

    class _GE_Remote_Fault_AND_MASK(AtRegister.AtRegister):
        def name(self):
            return "GE Remote Fault AND MASK"
    
        def description(self):
            return "Interrupt Remote Fault current status AND MASK"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000833
            
        def endAddress(self):
            return 0xffffffff

        class _GeRemote_Fault_and_mask(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "GeRemote_Fault_and_mask"
            
            def description(self):
                return "Interrupt Enable of Remote Fault, bit per port, bit0 is port0, bit7 is port7"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["GeRemote_Fault_and_mask"] = _AF6CNC0021_RD_SGMII_Multirate._GE_Remote_Fault_AND_MASK._GeRemote_Fault_and_mask()
            return allFields

    class _GE_Excessive_Error_Ratio_AND_MASK(AtRegister.AtRegister):
        def name(self):
            return "GE Excessive Error Ratio AND MASK"
    
        def description(self):
            return "Interrupt Excessive Error Ratio current status AND MASK"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000834
            
        def endAddress(self):
            return 0xffffffff

        class _rxexcer_and_mask(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxexcer_and_mask"
            
            def description(self):
                return "Interrupt Enable of Excessive Error Ratio, bit per port, bit0 is port0, bit7 is port7"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxexcer_and_mask"] = _AF6CNC0021_RD_SGMII_Multirate._GE_Excessive_Error_Ratio_AND_MASK._rxexcer_and_mask()
            return allFields

    class _GE_1000basex_Force_K30_7(AtRegister.AtRegister):
        def name(self):
            return "GE 1000basex Force K30_7"
    
        def description(self):
            return "Forcing countinuous K30.7 for 1000basex mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000822
            
        def endAddress(self):
            return 0xffffffff

        class _tx_fk30_7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tx_fk30_7"
            
            def description(self):
                return "Force K30.7 code, bit per port, bit0 is port0, bit7 is port7"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tx_fk30_7"] = _AF6CNC0021_RD_SGMII_Multirate._GE_1000basex_Force_K30_7._tx_fk30_7()
            return allFields

    class _GE_TenG_Excessive_Error_Timer(AtRegister.AtRegister):
        def name(self):
            return "GE_TenG Excessive Error Timer"
    
        def description(self):
            return "Configurate excessive error timer, this reg should be controlled by HW"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000837
            
        def endAddress(self):
            return 0xffffffff

        class _rxexcer_timer_grp1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "rxexcer_timer_grp1"
            
            def description(self):
                return "Timer for group 1 , port9 to port16, this group is use for MRO20G project the default value is 0xF422, it will be 1ms for 1GE mode, and 400us for 10Ge mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _rxexcer_timer_grp0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxexcer_timer_grp0"
            
            def description(self):
                return "Timer for group 0 , port1 to port8, the default value is 0xF422, it will be 1ms for 1GE mode, and 400us for 10Ge mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxexcer_timer_grp1"] = _AF6CNC0021_RD_SGMII_Multirate._GE_TenG_Excessive_Error_Timer._rxexcer_timer_grp1()
            allFields["rxexcer_timer_grp0"] = _AF6CNC0021_RD_SGMII_Multirate._GE_TenG_Excessive_Error_Timer._rxexcer_timer_grp0()
            return allFields

    class _GE_TenG_Excessive_Error_Threshold_Group0(AtRegister.AtRegister):
        def name(self):
            return "GE_TenG Excessive Error Threshold Group0"
    
        def description(self):
            return "GE_TenG Excessive Error Threshold Group0"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000835
            
        def endAddress(self):
            return 0xffffffff

        class _rxexcer_timer_uthres0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "rxexcer_timer_uthres0"
            
            def description(self):
                return "Upper threshold for group 0 , port1 to port8, when error counter is greater than this threshold then current status of excessive error will be set. sw should config 0x11 for 1Ge mode, and 0x7 for 10Ge mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _rxexcer_timer_lthres0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxexcer_timer_lthres0"
            
            def description(self):
                return "Lower threshold for group 0 , port1 to port8, when error counter is less than this threshold then current status of excessive error will be clear. sw should config 0x1 for 1Ge mode, and 0x1 for 10Ge mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxexcer_timer_uthres0"] = _AF6CNC0021_RD_SGMII_Multirate._GE_TenG_Excessive_Error_Threshold_Group0._rxexcer_timer_uthres0()
            allFields["rxexcer_timer_lthres0"] = _AF6CNC0021_RD_SGMII_Multirate._GE_TenG_Excessive_Error_Threshold_Group0._rxexcer_timer_lthres0()
            return allFields

    class _GE_TenG_Excessive_Error_Threshold_Group1(AtRegister.AtRegister):
        def name(self):
            return "GE_TenG Excessive Error Threshold Group1"
    
        def description(self):
            return "GE_TenG Excessive Error Threshold Group1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000838
            
        def endAddress(self):
            return 0xffffffff

        class _rxexcer_timer_uthres1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "rxexcer_timer_uthres1"
            
            def description(self):
                return "Upper threshold for group 1 , port9 to port16, when error counter is greater than this threshold then current status of excessive error will be set. sw should config 0x11 for 1Ge mode, and 0x7 for 10Ge mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _rxexcer_timer_lthres1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxexcer_timer_lthres1"
            
            def description(self):
                return "Lower threshold for group 1 , port9 to port16, when error counter is less than this threshold then current status of excessive error will be clear. sw should config 0x1 for 1Ge mode, and 0x1 for 10Ge mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxexcer_timer_uthres1"] = _AF6CNC0021_RD_SGMII_Multirate._GE_TenG_Excessive_Error_Threshold_Group1._rxexcer_timer_uthres1()
            allFields["rxexcer_timer_lthres1"] = _AF6CNC0021_RD_SGMII_Multirate._GE_TenG_Excessive_Error_Threshold_Group1._rxexcer_timer_lthres1()
            return allFields

    class _fx_txenb(AtRegister.AtRegister):
        def name(self):
            return "ENABLE TX 100base FX"
    
        def description(self):
            return "Interrupt enable of Remote Fault"
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000a08
            
        def endAddress(self):
            return 0xffffffff

        class _fx_entx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "fx_entx"
            
            def description(self):
                return "(1) is enable, (0) is disable,  bit0 is port0, bit15 is port15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["fx_entx"] = _AF6CNC0021_RD_SGMII_Multirate._fx_txenb._fx_entx()
            return allFields

    class _fx_patstken(AtRegister.AtRegister):
        def name(self):
            return "Sticky Rx PATTERN DETECT"
    
        def description(self):
            return "Interrupt enable of Remote Fault"
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000a09
            
        def endAddress(self):
            return 0xffffffff

        class _fx_patstk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "fx_patstk"
            
            def description(self):
                return "sticky pattern detect"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["fx_patstk"] = _AF6CNC0021_RD_SGMII_Multirate._fx_patstken._fx_patstk()
            return allFields

    class _fx_codestken(AtRegister.AtRegister):
        def name(self):
            return "Sticky Rx CODE ERROR DETECT"
    
        def description(self):
            return "Interrupt enable of Remote Fault"
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000a0a
            
        def endAddress(self):
            return 0xffffffff

        class _fx_codeerrstk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "fx_codeerrstk"
            
            def description(self):
                return "sticky code error detect"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["fx_codeerrstk"] = _AF6CNC0021_RD_SGMII_Multirate._fx_codestken._fx_codeerrstk()
            return allFields

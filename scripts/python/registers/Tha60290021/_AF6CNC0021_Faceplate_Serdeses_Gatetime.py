import AtRegister

class _AF6CNC0021_Faceplate_Serdeses_Gatetime(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["gatetime_ctr"] = _AF6CNC0021_Faceplate_Serdeses_Gatetime._gatetime_ctr()
        allRegisters["Faceplate_serdeses_Gatetime_st"] = _AF6CNC0021_Faceplate_Serdeses_Gatetime._Faceplate_serdeses_Gatetime_st()
        allRegisters["Faceplate_serdeses_Gatetime_current"] = _AF6CNC0021_Faceplate_Serdeses_Gatetime._Faceplate_serdeses_Gatetime_current()
        return allRegisters

    class _gatetime_ctr(AtRegister.AtRegister):
        def name(self):
            return "Faceplate serdeses Gatetime for PRBS Raw Diagnostic"
    
        def description(self):
            return "This is Faceplate serdeses Gatetime for PRBS Raw Diagnostic port 1 to port 16"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "Base_0x40 + 0x40 + PortID"
            
        def startAddress(self):
            return 0x00000040
            
        def endAddress(self):
            return 0x0000004f

        class _start_diag(AtRegister.AtRegisterField):
            def startBit(self):
                return 17
                
            def stopBit(self):
                return 17
        
            def name(self):
                return "start_diag"
            
            def description(self):
                return "Config start Diagnostic trigger 0 to 1 for Start auto run with Gatetime Configuration"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _time_cfg(AtRegister.AtRegisterField):
            def startBit(self):
                return 16
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "time_cfg"
            
            def description(self):
                return "Gatetime Configuration 1-86400 second"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["start_diag"] = _AF6CNC0021_Faceplate_Serdeses_Gatetime._gatetime_ctr._start_diag()
            allFields["time_cfg"] = _AF6CNC0021_Faceplate_Serdeses_Gatetime._gatetime_ctr._time_cfg()
            return allFields

    class _Faceplate_serdeses_Gatetime_st(AtRegister.AtRegister):
        def name(self):
            return "Faceplate serdeses Gatetime"
    
        def description(self):
            return "Faceplate_serdeses_Gatetime_Status"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _Unsed(AtRegister.AtRegisterField):
            def startBit(self):
                return 31
                
            def stopBit(self):
                return 19
        
            def name(self):
                return "Unsed"
            
            def description(self):
                return "Unsed"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _status_gatetime_diag(AtRegister.AtRegisterField):
            def startBit(self):
                return 15
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "status_gatetime_diag"
            
            def description(self):
                return "Status Gatetime diagnostic port 16 to port1 bit per port 1:Running 0:Done-Ready"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Unsed"] = _AF6CNC0021_Faceplate_Serdeses_Gatetime._Faceplate_serdeses_Gatetime_st._Unsed()
            allFields["status_gatetime_diag"] = _AF6CNC0021_Faceplate_Serdeses_Gatetime._Faceplate_serdeses_Gatetime_st._status_gatetime_diag()
            return allFields

    class _Faceplate_serdeses_Gatetime_current(AtRegister.AtRegister):
        def name(self):
            return "Faceplate serdeses Gatetime Current"
    
        def description(self):
            return "Faceplate_serdeses_Gatetime_Status"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return "Baas0x61 +0x60+Port ID"
            
        def startAddress(self):
            return 0x00000061
            
        def endAddress(self):
            return 0x00000070

        class _Unsed(AtRegister.AtRegisterField):
            def startBit(self):
                return 31
                
            def stopBit(self):
                return 17
        
            def name(self):
                return "Unsed"
            
            def description(self):
                return "Unsed"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _currert_gatetime_diag(AtRegister.AtRegisterField):
            def startBit(self):
                return 16
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "currert_gatetime_diag"
            
            def description(self):
                return "Current running time of Gatetime diagnostic"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Unsed"] = _AF6CNC0021_Faceplate_Serdeses_Gatetime._Faceplate_serdeses_Gatetime_current._Unsed()
            allFields["currert_gatetime_diag"] = _AF6CNC0021_Faceplate_Serdeses_Gatetime._Faceplate_serdeses_Gatetime_current._currert_gatetime_diag()
            return allFields

import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0011_RD_PLA(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["pla_lo_pld_ctrl"] = _AF6CCI0011_RD_PLA._pla_lo_pld_ctrl()
        allRegisters["pla_lo_pw_ctrl"] = _AF6CCI0011_RD_PLA._pla_lo_pw_ctrl()
        allRegisters["pla_lo_add_rmv_pw_ctrl"] = _AF6CCI0011_RD_PLA._pla_lo_add_rmv_pw_ctrl()
        allRegisters["pla_ho_pldd_ctrl"] = _AF6CCI0011_RD_PLA._pla_ho_pldd_ctrl()
        allRegisters["pla_ho_pw_ctrl"] = _AF6CCI0011_RD_PLA._pla_ho_pw_ctrl()
        allRegisters["pla_ho_add_rmv_pw_ctrl"] = _AF6CCI0011_RD_PLA._pla_ho_add_rmv_pw_ctrl()
        allRegisters["pla_out_pw_ctrl"] = _AF6CCI0011_RD_PLA._pla_out_pw_ctrl()
        allRegisters["pla_out_upsr_ctrl"] = _AF6CCI0011_RD_PLA._pla_out_upsr_ctrl()
        allRegisters["pla_out_hspw_ctrl"] = _AF6CCI0011_RD_PLA._pla_out_hspw_ctrl()
        allRegisters["pla_out_psnpro_ctrl"] = _AF6CCI0011_RD_PLA._pla_out_psnpro_ctrl()
        allRegisters["pla_out_psnbuf_ctrl"] = _AF6CCI0011_RD_PLA._pla_out_psnbuf_ctrl()
        allRegisters["pla_hold_reg_ctrl"] = _AF6CCI0011_RD_PLA._pla_hold_reg_ctrl()
        allRegisters["rdha3_0_control"] = _AF6CCI0011_RD_PLA._rdha3_0_control()
        allRegisters["rdha7_4_control"] = _AF6CCI0011_RD_PLA._rdha7_4_control()
        allRegisters["rdha11_8_control"] = _AF6CCI0011_RD_PLA._rdha11_8_control()
        allRegisters["rdha15_12_control"] = _AF6CCI0011_RD_PLA._rdha15_12_control()
        allRegisters["rdha19_16_control"] = _AF6CCI0011_RD_PLA._rdha19_16_control()
        allRegisters["rdha23_20_control"] = _AF6CCI0011_RD_PLA._rdha23_20_control()
        allRegisters["rdha24data_control"] = _AF6CCI0011_RD_PLA._rdha24data_control()
        allRegisters["rdha_hold63_32"] = _AF6CCI0011_RD_PLA._rdha_hold63_32()
        allRegisters["rdindr_hold95_64"] = _AF6CCI0011_RD_PLA._rdindr_hold95_64()
        allRegisters["rdindr_hold127_96"] = _AF6CCI0011_RD_PLA._rdindr_hold127_96()
        allRegisters["pla_out_psnpro_ha_ctrl"] = _AF6CCI0011_RD_PLA._pla_out_psnpro_ha_ctrl()
        allRegisters["pla_force_par_err_control0"] = _AF6CCI0011_RD_PLA._pla_force_par_err_control0()
        allRegisters["pla_dis_par_control0"] = _AF6CCI0011_RD_PLA._pla_dis_par_control0()
        allRegisters["pla_par_stk0"] = _AF6CCI0011_RD_PLA._pla_par_stk0()
        allRegisters["pla_force_par_err_control1"] = _AF6CCI0011_RD_PLA._pla_force_par_err_control1()
        allRegisters["pla_dis_par_control1"] = _AF6CCI0011_RD_PLA._pla_dis_par_control1()
        allRegisters["pla_par_err_stk1"] = _AF6CCI0011_RD_PLA._pla_par_err_stk1()
        allRegisters["pla_force_crc_err_control"] = _AF6CCI0011_RD_PLA._pla_force_crc_err_control()
        allRegisters["pla_dis_crc_control"] = _AF6CCI0011_RD_PLA._pla_dis_crc_control()
        allRegisters["pla_crc_err_stk"] = _AF6CCI0011_RD_PLA._pla_crc_err_stk()
        allRegisters["pwe_pla_fsm_ctr"] = _AF6CCI0011_RD_PLA._pwe_pla_fsm_ctr()
        return allRegisters

    class _pla_lo_pld_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Low-Order Payload Control"
    
        def description(self):
            return "This register is used to configure payload in each LO Pseudowire channels HDL_PATH: begin: IF(($LoOc48Slice==0) && ($$LoOc24Slice==0)) ilosl1pla1cfgpw_ram.array.ram[$$LoPwid] ELSEIF(($LoOc48Slice==0) && ($$LoOc24Slice==1)) ilosl1pla2cfgpw_ram.array.ram[$$LoPwid] IF(($LoOc48Slice==1) && ($$LoOc24Slice==0)) ilosl2pla1cfgpw_ram.array.ram[$$LoPwid] IF(($LoOc48Slice==1) && ($$LoOc24Slice==1)) ilosl2pla2cfgpw_ram.array.ram[$$LoPwid] IF(($LoOc48Slice==2) && ($$LoOc24Slice==0)) ilosl3pla1cfgpw_ram.array.ram[$$LoPwid] IF(($LoOc48Slice==2) && ($$LoOc24Slice==1)) ilosl3pla2cfgpw_ram.array.ram[$$LoPwid] IF(($LoOc48Slice==3) && ($$LoOc24Slice==0)) ilosl4pla1cfgpw_ram.array.ram[$$LoPwid] IF(($LoOc48Slice==3) && ($$LoOc24Slice==1)) ilosl4pla2cfgpw_ram.array.ram[$$LoPwid] HDL_PATH: end:"
            
        def width(self):
            return 18
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0_2000 + $LoOc48Slice*65536 + $LoOc24Slice*16384 + $LoPwid"
            
        def startAddress(self):
            return 0x00002000
            
        def endAddress(self):
            return 0x000363ff

        class _PlaLoPldTSSrcCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "PlaLoPldTSSrcCtrl"
            
            def description(self):
                return "Timestamp Source 0: PRC global 1: TS from CDR engine"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoPldTypeCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 15
        
            def name(self):
                return "PlaLoPldTypeCtrl"
            
            def description(self):
                return "Payload Type 0: satop 1: ces without cas 2: cep"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoPldSizeCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaLoPldSizeCtrl"
            
            def description(self):
                return "Payload Size satop/cep mode: bit[13:0] payload size (ex: value as 0x100 is payload size 256 bytes) ces mode: bit[5:0] number of DS0 timeslot bit[14:6] number of NxDS0 frame"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaLoPldTSSrcCtrl"] = _AF6CCI0011_RD_PLA._pla_lo_pld_ctrl._PlaLoPldTSSrcCtrl()
            allFields["PlaLoPldTypeCtrl"] = _AF6CCI0011_RD_PLA._pla_lo_pld_ctrl._PlaLoPldTypeCtrl()
            allFields["PlaLoPldSizeCtrl"] = _AF6CCI0011_RD_PLA._pla_lo_pld_ctrl._PlaLoPldSizeCtrl()
            return allFields

    class _pla_lo_pw_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Low-Order Pseudowire Control"
    
        def description(self):
            return "This register is used to configure some pseudowire properties in each LO Pseudowire channels HDL_PATH: begin: IF($LoOc48Slice==0) ilosl1pwblkcfg_ram.array.ram[$LoOc24Slice*1024 + $LoPwid] IF($LoOc48Slice==1) ilosl2pwblkcfg_ram.array.ram[$LoOc24Slice*1024 + $LoPwid] IF($LoOc48Slice==2) ilosl3pwblkcfg_ram.array.ram[$LoOc24Slice*1024 + $LoPwid] IF($LoOc48Slice==3) ilosl4pwblkcfg_ram.array.ram[$LoOc24Slice*1024 + $LoPwid] HDL_PATH: end:"
            
        def width(self):
            return 22
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0_D000 + $LoOc48Slice*65536 + $LoOc24Slice*2048 + $LoPwid"
            
        def startAddress(self):
            return 0x0000d000
            
        def endAddress(self):
            return 0x0003dbff

        class _PlaLoPwPidCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 20
        
            def name(self):
                return "PlaLoPwPidCtrl"
            
            def description(self):
                return "Port ID output"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoPwPWIDCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 7
        
            def name(self):
                return "PlaLoPwPWIDCtrl"
            
            def description(self):
                return "real PW id lookup"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoPwSuprCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PlaLoPwSuprCtrl"
            
            def description(self):
                return "Suppresion Enable 1: enable 0: disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoPwMbitDisCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PlaLoPwMbitDisCtrl"
            
            def description(self):
                return "M or NP bits disable 1: disable M or NP bits in the Control Word (assign zero in the packet) 0: normal"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoPwLbitDisCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PlaLoPwLbitDisCtrl"
            
            def description(self):
                return "L bit disable 1: disable L bit in the Control Word (assign zero in the packet) 0: normal"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoPwMbitCPUCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PlaLoPwMbitCPUCtrl"
            
            def description(self):
                return "M or NP bits value from CPU (low priority than PlaLoPwMbitDisCtrl)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoPwLbitCPUCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PlaLoPwLbitCPUCtrl"
            
            def description(self):
                return "L bit value from CPU (low priority than PlaLoPwLbitDisCtrl)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoPwEnCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaLoPwEnCtrl"
            
            def description(self):
                return "LO Pseudowire enable 1: enable 0: disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaLoPwPidCtrl"] = _AF6CCI0011_RD_PLA._pla_lo_pw_ctrl._PlaLoPwPidCtrl()
            allFields["PlaLoPwPWIDCtrl"] = _AF6CCI0011_RD_PLA._pla_lo_pw_ctrl._PlaLoPwPWIDCtrl()
            allFields["PlaLoPwSuprCtrl"] = _AF6CCI0011_RD_PLA._pla_lo_pw_ctrl._PlaLoPwSuprCtrl()
            allFields["PlaLoPwMbitDisCtrl"] = _AF6CCI0011_RD_PLA._pla_lo_pw_ctrl._PlaLoPwMbitDisCtrl()
            allFields["PlaLoPwLbitDisCtrl"] = _AF6CCI0011_RD_PLA._pla_lo_pw_ctrl._PlaLoPwLbitDisCtrl()
            allFields["PlaLoPwMbitCPUCtrl"] = _AF6CCI0011_RD_PLA._pla_lo_pw_ctrl._PlaLoPwMbitCPUCtrl()
            allFields["PlaLoPwLbitCPUCtrl"] = _AF6CCI0011_RD_PLA._pla_lo_pw_ctrl._PlaLoPwLbitCPUCtrl()
            allFields["PlaLoPwEnCtrl"] = _AF6CCI0011_RD_PLA._pla_lo_pw_ctrl._PlaLoPwEnCtrl()
            return allFields

    class _pla_lo_add_rmv_pw_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Low-Order Add/Remove Pseudowire Protocol Control"
    
        def description(self):
            return "This register is used to add/remove pseudowire to prevent burst packets to other packet processing engines in each LO OC-24 slice"
            
        def width(self):
            return 2
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0_2800 + $LoOc48Slice*65536 + $LoOc24Slice*16384 + $LoPwid"
            
        def startAddress(self):
            return 0x00002800
            
        def endAddress(self):
            return 0x00036bff

        class _PlaLoStaAddRmvCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaLoStaAddRmvCtrl"
            
            def description(self):
                return "protocol state to add/remove pw Step1: Add intitial or pw idle, the protocol state value is \"zero\" Step2: For adding the pw, CPU will Write \"1\" value for the protocol state to start adding pw. The protocol state value is \"1\". Step3: CPU enables pw at demap to finish the adding pw process. HW will automatically change the protocol state value to \"2\" to run the pw, and keep this state. Step4: For removing the pw, CPU will write \"3\" value for the protocol state to start removing pw. The protocol state value is \"3\". Step5: Poll the protocol state until return value \"0\" value, after that CPU disables pw at demap to finish the removing pw process"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaLoStaAddRmvCtrl"] = _AF6CCI0011_RD_PLA._pla_lo_add_rmv_pw_ctrl._PlaLoStaAddRmvCtrl()
            return allFields

    class _pla_ho_pldd_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler High-Order Payload Control"
    
        def description(self):
            return "This register is used to configure payload in each HO Pseudowire channels"
            
        def width(self):
            return 17
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_0100 + $HoOc96Slice*2048 + $HoOc48Slice*512 + $HoPwid"
            
        def startAddress(self):
            return 0x00080100
            
        def endAddress(self):
            return 0x00080b2f

        class _PlaLoPldTypeCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 15
        
            def name(self):
                return "PlaLoPldTypeCtrl"
            
            def description(self):
                return "Payload Type 0: satop 2: cep"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoPldSizeCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaLoPldSizeCtrl"
            
            def description(self):
                return "Payload Size"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaLoPldTypeCtrl"] = _AF6CCI0011_RD_PLA._pla_ho_pldd_ctrl._PlaLoPldTypeCtrl()
            allFields["PlaLoPldSizeCtrl"] = _AF6CCI0011_RD_PLA._pla_ho_pldd_ctrl._PlaLoPldSizeCtrl()
            return allFields

    class _pla_ho_pw_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler High-Order Pseudowire Control"
    
        def description(self):
            return "This register is used to configure some pseudowire properties in each HO Pseudowire channels"
            
        def width(self):
            return 22
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_0680 + $HoOc96Slice*2048 + $HoOc48Slice*64 + $HoPwid"
            
        def startAddress(self):
            return 0x00080680
            
        def endAddress(self):
            return 0x000810af

        class _PlaHoPwPidCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 20
        
            def name(self):
                return "PlaHoPwPidCtrl"
            
            def description(self):
                return "Port ID output"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaHoPwPWIDCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 7
        
            def name(self):
                return "PlaHoPwPWIDCtrl"
            
            def description(self):
                return "real PW id lookup"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaHoPwSuprCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PlaHoPwSuprCtrl"
            
            def description(self):
                return "Suppresion Enable 1: enable 0: disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaHoPwNPbitDisCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PlaHoPwNPbitDisCtrl"
            
            def description(self):
                return "NP bits disable 1: disable NP bits in the Control Word (assign zero in the packet) 0: normal"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaHoPwLbitDisCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PlaHoPwLbitDisCtrl"
            
            def description(self):
                return "L bit disable 1: disable L bit in the Control Word (assign zero in the packet) 0: normal"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaHoPwMbitCPUCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PlaHoPwMbitCPUCtrl"
            
            def description(self):
                return "NP bits value from CPU (low priority than PlaHoPwNPbitDisCtrl)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaHoPwLbitCPUCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PlaHoPwLbitCPUCtrl"
            
            def description(self):
                return "L bit value from CPU (low priority than PlaHoPwLbitDisCtrl)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaHoPwEnCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaHoPwEnCtrl"
            
            def description(self):
                return "HO Pseudowire enable 1: enable 0: disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaHoPwPidCtrl"] = _AF6CCI0011_RD_PLA._pla_ho_pw_ctrl._PlaHoPwPidCtrl()
            allFields["PlaHoPwPWIDCtrl"] = _AF6CCI0011_RD_PLA._pla_ho_pw_ctrl._PlaHoPwPWIDCtrl()
            allFields["PlaHoPwSuprCtrl"] = _AF6CCI0011_RD_PLA._pla_ho_pw_ctrl._PlaHoPwSuprCtrl()
            allFields["PlaHoPwNPbitDisCtrl"] = _AF6CCI0011_RD_PLA._pla_ho_pw_ctrl._PlaHoPwNPbitDisCtrl()
            allFields["PlaHoPwLbitDisCtrl"] = _AF6CCI0011_RD_PLA._pla_ho_pw_ctrl._PlaHoPwLbitDisCtrl()
            allFields["PlaHoPwMbitCPUCtrl"] = _AF6CCI0011_RD_PLA._pla_ho_pw_ctrl._PlaHoPwMbitCPUCtrl()
            allFields["PlaHoPwLbitCPUCtrl"] = _AF6CCI0011_RD_PLA._pla_ho_pw_ctrl._PlaHoPwLbitCPUCtrl()
            allFields["PlaHoPwEnCtrl"] = _AF6CCI0011_RD_PLA._pla_ho_pw_ctrl._PlaHoPwEnCtrl()
            return allFields

    class _pla_ho_add_rmv_pw_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Hig-Order Add/Remove Pseudowire Protocol Control"
    
        def description(self):
            return "This register is used to add/remove pseudowire to prevent burst packets to other packet processing engines in each HO OC-48 slice"
            
        def width(self):
            return 2
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_0140 + $HoOc96Slice*2048 + $HoOc48Slice*512 + $HoPwid"
            
        def startAddress(self):
            return 0x00080140
            
        def endAddress(self):
            return 0x00080b6f

        class _PlaLoStaAddRmvCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaLoStaAddRmvCtrl"
            
            def description(self):
                return "protocol state to add/remove pw Step1: Add intitial or pw idle, the protocol state value is \"zero\" Step2: For adding the pw, CPU will Write \"1\" value for the protocol state to start adding pw. The protocol state value is \"1\". Step3: CPU enables pw at demap to finish the adding pw process. HW will automatically change the protocol state value to \"2\" to run the pw, and keep this state. Step4: For removing the pw, CPU will write \"3\" value for the protocol state to start removing pw. The protocol state value is \"3\". Step5: Poll the protocol state until return value \"0\" value, after that CPU disables pw at demap to finish the removing pw process"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaLoStaAddRmvCtrl"] = _AF6CCI0011_RD_PLA._pla_ho_add_rmv_pw_ctrl._PlaLoStaAddRmvCtrl()
            return allFields

    class _pla_out_pw_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Output Pseudowire Control"
    
        def description(self):
            return "This register is used to config psedowire at output HDL_PATH : ip1pwcfg_ram.array.ram[$pwid]"
            
        def width(self):
            return 37
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_0000 + $pid*65536 + $pwid"
            
        def startAddress(self):
            return 0x00090000
            
        def endAddress(self):
            return 0x000a17ff

        class _PlaOutHspwUsedCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 36
                
            def startBit(self):
                return 36
        
            def name(self):
                return "PlaOutHspwUsedCtr"
            
            def description(self):
                return "HSPW is used or not 0:not used, all configurations for HSPW will be not effected 1:used"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutUpsrUsedCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 35
                
            def startBit(self):
                return 35
        
            def name(self):
                return "PlaOutUpsrUsedCtr"
            
            def description(self):
                return "UPSR is used or not 0:not used, all configurations for UPSR will be not effected 1:used"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutCepCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 34
                
            def startBit(self):
                return 34
        
            def name(self):
                return "PlaOutCepCtr"
            
            def description(self):
                return "PW is in a CEP mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutHspwGrpCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 33
                
            def startBit(self):
                return 22
        
            def name(self):
                return "PlaOutHspwGrpCtr"
            
            def description(self):
                return "HSPW group"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutUpsrGrpCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 9
        
            def name(self):
                return "PlaOutUpsrGrpCtr"
            
            def description(self):
                return "UPRS group"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutRbitDisCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PlaOutRbitDisCtrl"
            
            def description(self):
                return "R bit disable 1: disable R bit in the Control Word (assign zero in the packet) 0: normal"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutRbitCPUCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "PlaOutRbitCPUCtrl"
            
            def description(self):
                return "R bit value from CPU (low priority than PlaOutRbitDisCtrl)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutPSNLenCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaOutPSNLenCtrl"
            
            def description(self):
                return "PSN length"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaOutHspwUsedCtr"] = _AF6CCI0011_RD_PLA._pla_out_pw_ctrl._PlaOutHspwUsedCtr()
            allFields["PlaOutUpsrUsedCtr"] = _AF6CCI0011_RD_PLA._pla_out_pw_ctrl._PlaOutUpsrUsedCtr()
            allFields["PlaOutCepCtr"] = _AF6CCI0011_RD_PLA._pla_out_pw_ctrl._PlaOutCepCtr()
            allFields["PlaOutHspwGrpCtr"] = _AF6CCI0011_RD_PLA._pla_out_pw_ctrl._PlaOutHspwGrpCtr()
            allFields["PlaOutUpsrGrpCtr"] = _AF6CCI0011_RD_PLA._pla_out_pw_ctrl._PlaOutUpsrGrpCtr()
            allFields["PlaOutRbitDisCtrl"] = _AF6CCI0011_RD_PLA._pla_out_pw_ctrl._PlaOutRbitDisCtrl()
            allFields["PlaOutRbitCPUCtrl"] = _AF6CCI0011_RD_PLA._pla_out_pw_ctrl._PlaOutRbitCPUCtrl()
            allFields["PlaOutPSNLenCtrl"] = _AF6CCI0011_RD_PLA._pla_out_pw_ctrl._PlaOutPSNLenCtrl()
            return allFields

    class _pla_out_upsr_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Output UPSR Control"
    
        def description(self):
            return "This register is used to config UPSR group"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_4000 + $upsrgrp"
            
        def startAddress(self):
            return 0x00094000
            
        def endAddress(self):
            return 0x000c5fff

        class _PlaOutUpsrEnCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaOutUpsrEnCtr"
            
            def description(self):
                return "Total is 8092 upsrID, divide into 512 group address for configuration, 16-bit is correlative with 16 upsrID each group. Bit#0 is for upsrID#0, bit#1 is for upsrID#1... 1: enable 0: disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaOutUpsrEnCtr"] = _AF6CCI0011_RD_PLA._pla_out_upsr_ctrl._PlaOutUpsrEnCtr()
            return allFields

    class _pla_out_hspw_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Output HSPW Control"
    
        def description(self):
            return "This register is used to config HSPW group"
            
        def width(self):
            return 2
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_8000 + $hspwid"
            
        def startAddress(self):
            return 0x00098000
            
        def endAddress(self):
            return 0x000c8fff

        class _PlaOutHspwPsnCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PlaOutHspwPsnCtr"
            
            def description(self):
                return "PSN location for HSPW group"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaOutHspwPsnCtr"] = _AF6CCI0011_RD_PLA._pla_out_hspw_ctrl._PlaOutHspwPsnCtr()
            return allFields

    class _pla_out_psnpro_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Output PSN Process Control"
    
        def description(self):
            return "This register is used to control PSN configuration"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00084000
            
        def endAddress(self):
            return 0xffffffff

        class _PlaOutPsnProReqCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "PlaOutPsnProReqCtr"
            
            def description(self):
                return "Request to process PSN 1: request to write/read PSN header buffer. The CPU need to prepare a complete PSN header into PSN buffer before request write OR read out a complete PSN header in PSN buffer after request read done 0: HW will automatically set to 0 when a request done"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutPsnProRnWCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "PlaOutPsnProRnWCtr"
            
            def description(self):
                return "read or write PSN 1: read PSN 0: write PSN"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutPsnProLenCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PlaOutPsnProLenCtr"
            
            def description(self):
                return "Length of a complete PSN need to read/write"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutPsnProPageCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "PlaOutPsnProPageCtr"
            
            def description(self):
                return "there is 2 pages PSN location per PWID, depend on the configuration of \"PlaOutHspwPsnCtr\", the engine will choice which the page to encapsulate into the packet."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutPsnProPWIDCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaOutPsnProPWIDCtr"
            
            def description(self):
                return "Pseudowire channel"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaOutPsnProReqCtr"] = _AF6CCI0011_RD_PLA._pla_out_psnpro_ctrl._PlaOutPsnProReqCtr()
            allFields["PlaOutPsnProRnWCtr"] = _AF6CCI0011_RD_PLA._pla_out_psnpro_ctrl._PlaOutPsnProRnWCtr()
            allFields["PlaOutPsnProLenCtr"] = _AF6CCI0011_RD_PLA._pla_out_psnpro_ctrl._PlaOutPsnProLenCtr()
            allFields["PlaOutPsnProPageCtr"] = _AF6CCI0011_RD_PLA._pla_out_psnpro_ctrl._PlaOutPsnProPageCtr()
            allFields["PlaOutPsnProPWIDCtr"] = _AF6CCI0011_RD_PLA._pla_out_psnpro_ctrl._PlaOutPsnProPWIDCtr()
            return allFields

    class _pla_out_psnbuf_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Output PSN Buffer Control"
    
        def description(self):
            return "This register is used to store PSN data which is before CPU request write or after CPU request read %% The header format from entry#0 to entry#4 is as follow: %% {DA(6-byte),SA(6-byte),VLAN1(4-byte,optional),VLAN2(4-byte, optional),EthType(2-byte),PSN Header(4 to 96 bytes)} %% Depends on specific PSN selected for each pseudowire, the EthType and PSN Header field may have the following values and formats: %%  PSN header is MEF-8: %% EthType:  0x88D8 %% 4-byte PSN Header with format: {ECID[19:0], 0x102} where ECID is pseodowire identification for 	remote 	receive side. %% PSN header is MPLS:  %% EthType:  0x8847 %% 4/8/12-byte PSN Header with format: {OuterLabel2[31:0](optional), OuterLabel1[31:0](optional),  InnerLabel[31:0]}   %% Where InnerLabel[31:12] is pseodowire identification for remote receive side. %% Label format: {Idenfifier[19:0],Exp[2:0],StackBit,TTL[7:0]} where StackBit =  for InnerLabel %% PSN header is UDP/Ipv4:  %% EthType:  0x0800 %% 28-byte PSN Header with format {Ipv4 Header(20 bytes), UDP Header(8 byte)} as below: %% {IP_Ver[3:0], IP_IHL[3:0], IP_ToS[7:0], IP_Header_Sum[31:16]} %% {IP_Iden[15:0], IP_Flag[2:0], IP_Frag_Offset[12:0]}  %% {IP_TTL[7:0], IP_Protocol[7:0], IP_Header_Sum[15:0]} %% {IP_SrcAdr[31:0]} %% {IP_DesAdr[31:0]} %% {UDP_SrcPort[15:0], UDP_DesPort[15:0]} %% {UDP_Sum[31:0] (optional)} %% Case: %% IP_Protocol[7:0]: 0x11 to signify UDP  %% IP_Protocol[7:0]: 0x11 to signify UDP  %% IP_Header_Sum[31:0]:  CPU calculate these fields as a temporarily for HW before plus rest fields %% {IP_Ver[3:0], IP_IHL[3:0], IP_ToS[7:0]} +%% IP_Iden[15:0] + {IP_Flag[2:0], IP_Frag_Offset[12:0]}  +%% {IP_TTL[7:0], IP_Protocol[7:0]} + %% IP_SrcAdr[31:16] + IP_SrcAdr[15:0] +%% IP_DesAdr[31:16] + IP_DesAdr[15:0]%% UDP_SrcPort: used as remote pseudowire identification or 0x85E if unused%% UDP_DesPort : used as remote pseudowire identification or 0x85E if unused%% UDP_Sum[31:0] (optional): CPU calculate these fields as a temporarily for HW before plus rest fields%% IP_SrcAdr[127:112] + 	IP_SrcAdr[111:96] + 	%% IP_SrcAdr[95:80]  + IP_SrcAdr[79:64] + 	%% IP_SrcAdr[63:48]  + IP_SrcAdr[47:32] + 	%% IP_SrcAdr[31:16]  + IP_SrcAdr[15:0] + 	%% IP_DesAdr[127:112] + 	IP_DesAdr[111:96] + 	%% IP_DesAdr[95:80]  + IP_DesAdr[79:64] + 	%% IP_DesAdr[63:48]  + IP_DesAdr[47:32] + 	%% IP_DesAdr[31:16]  + IP_DesAdr[15:0] +%% {8-bit zeros, IP_Protocol[7:0]} +%% UDP_SrcPort[15:0] +  UDP_DesPort[15:0]%% PSN header is UDP/Ipv6:  %% EthType:  0x86DD%% 48-byte PSN Header with format {Ipv6 Header(40 bytes), UDP Header(8 byte)} as below:%% {IP_Ver[3:0], IP_Traffic_Class[7:0], IP_Flow_Label[19:0]}%% {16-bit zeros, IP_Next_Header[7:0], IP_Hop_Limit[7:0]} %% {IP_SrcAdr[127:96]}%% {IP_SrcAdr[95:64]}%% {IP_SrcAdr[63:32]}%% {IP_SrcAdr[31:0]}%% {IP_DesAdr[127:96]}%% {IP_DesAdr[95:64]}%% {IP_DesAdr[63:32]}%% {IP_DesAdr[31:0]}%% {UDP_SrcPort[15:0], UDP_DesPort[15:0]}%% {UDP_Sum[31:0]}%% Case:%% IP_Next_Header[7:0]: 0x11 to signify UDP %% UDP_Sum[31:0]:  CPU calculate these fields as a temporarily for HW before plus rest fields%% IP_SrcAdr[127:112] + 	IP_SrcAdr[111:96] + 	%% IP_SrcAdr[95:80]  + IP_SrcAdr[79:64] + 	%% IP_SrcAdr[63:48]  + IP_SrcAdr[47:32] + 	%% IP_SrcAdr[31:16]  + IP_SrcAdr[15:0] + 	%% IP_DesAdr[127:112] + 	IP_DesAdr[111:96] + 	%% IP_DesAdr[95:80]  + IP_DesAdr[79:64] + 	%% IP_DesAdr[63:48]  + IP_DesAdr[47:32] + 	%% IP_DesAdr[31:16]  + IP_DesAdr[15:0] +%% {8-bit zeros, IP_Next_Header[7:0]} +%% UDP_SrcPort[15:0] +  UDP_DesPort[15:0]%% UDP_SrcPort: used as remote pseudowire identification or 0x85E if unused%% UDP_DesPort : used as remote pseudowire identification or 0x85E if unused%% User can select either source or destination port for pseodowire identification. See IP/UDP standards for more description about other field. %% PSN header is MPLS over Ipv4:%% IPv4 header must have IP_Protocol[7:0] value 0x89 to signify MPLS%% After IPv4 header is MPLS labels where inner label is used for PW identification%% PSN header is MPLS over Ipv6:%% IPv6 header must have IP_Next_Header[7:0] value 0x89 to signify MPLS%% After IPv6 header is MPLS labels where inner label is used for PW identification%% PSN header (all modes) with RTP enable: RTP used for TDM PW (define in RFC3550), is in the first 8-byte of this buffer (bit[127:64] of segid == 0), following is PSN header (start from bit[63:0] of segid = 0)%% Case:%% RTPSSRC[31:0] : This is the SSRC value of RTP header%% RtpPtValue[6:0]: This is the PT value of RTP header, define in http://www.iana.org/assignments/rtp-parameters/rtp-parameters.xml"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_4010 + $segid"
            
        def startAddress(self):
            return 0x00084010
            
        def endAddress(self):
            return 0x00084015

        class _PlaOutPsnProReqCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaOutPsnProReqCtr"
            
            def description(self):
                return "PSN buffer"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaOutPsnProReqCtr"] = _AF6CCI0011_RD_PLA._pla_out_psnbuf_ctrl._PlaOutPsnProReqCtr()
            return allFields

    class _pla_hold_reg_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Hold Register Control"
    
        def description(self):
            return "This register is used to control hold register."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_C000 + $holdreg"
            
        def startAddress(self):
            return 0x0008c000
            
        def endAddress(self):
            return 0x0008c002

        class _PlaHoldRegCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaHoldRegCtr"
            
            def description(self):
                return "hold register value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaHoldRegCtr"] = _AF6CCI0011_RD_PLA._pla_hold_reg_ctrl._PlaHoldRegCtr()
            return allFields

    class _rdha3_0_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit3_0 Control"
    
        def description(self):
            return "This register is used to send HA read address bit3_0 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x8C100 + $HaAddr3_0"
            
        def startAddress(self):
            return 0x0008c100
            
        def endAddress(self):
            return 0x0008c10f

        class _ReadAddr3_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr3_0"
            
            def description(self):
                return "Read value will be 0x8C100 plus HaAddr3_0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr3_0"] = _AF6CCI0011_RD_PLA._rdha3_0_control._ReadAddr3_0()
            return allFields

    class _rdha7_4_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit7_4 Control"
    
        def description(self):
            return "This register is used to send HA read address bit7_4 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x8C110 + $HaAddr7_4"
            
        def startAddress(self):
            return 0x0008c110
            
        def endAddress(self):
            return 0x0008c11f

        class _ReadAddr7_4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr7_4"
            
            def description(self):
                return "Read value will be 0x8C100 plus HaAddr7_4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr7_4"] = _AF6CCI0011_RD_PLA._rdha7_4_control._ReadAddr7_4()
            return allFields

    class _rdha11_8_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit11_8 Control"
    
        def description(self):
            return "This register is used to send HA read address bit11_8 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x8C120 + $HaAddr11_8"
            
        def startAddress(self):
            return 0x0008c120
            
        def endAddress(self):
            return 0x0008c12f

        class _ReadAddr11_8(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr11_8"
            
            def description(self):
                return "Read value will be 0x8C100 plus HaAddr11_8"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr11_8"] = _AF6CCI0011_RD_PLA._rdha11_8_control._ReadAddr11_8()
            return allFields

    class _rdha15_12_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit15_12 Control"
    
        def description(self):
            return "This register is used to send HA read address bit15_12 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x08C130 + $HaAddr15_12"
            
        def startAddress(self):
            return 0x0008c130
            
        def endAddress(self):
            return 0x0008c13f

        class _ReadAddr15_12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr15_12"
            
            def description(self):
                return "Read value will be 0x8C100 plus HaAddr15_12"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr15_12"] = _AF6CCI0011_RD_PLA._rdha15_12_control._ReadAddr15_12()
            return allFields

    class _rdha19_16_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit19_16 Control"
    
        def description(self):
            return "This register is used to send HA read address bit19_16 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x8C140 + $HaAddr19_16"
            
        def startAddress(self):
            return 0x0008c140
            
        def endAddress(self):
            return 0x0008c14f

        class _ReadAddr19_16(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr19_16"
            
            def description(self):
                return "Read value will be 0x8C100 plus HaAddr19_16"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr19_16"] = _AF6CCI0011_RD_PLA._rdha19_16_control._ReadAddr19_16()
            return allFields

    class _rdha23_20_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit23_20 Control"
    
        def description(self):
            return "This register is used to send HA read address bit23_20 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x8C150 + $HaAddr23_20"
            
        def startAddress(self):
            return 0x0008c150
            
        def endAddress(self):
            return 0x0008c15f

        class _ReadAddr23_20(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr23_20"
            
            def description(self):
                return "Read value will be 0x8C100 plus HaAddr23_20"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr23_20"] = _AF6CCI0011_RD_PLA._rdha23_20_control._ReadAddr23_20()
            return allFields

    class _rdha24data_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit24 and Data Control"
    
        def description(self):
            return "This register is used to send HA read address bit24 to HA engine to read data"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x8C160 + $HaAddr24"
            
        def startAddress(self):
            return 0x0008c160
            
        def endAddress(self):
            return 0x0008c161

        class _ReadHaData31_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData31_0"
            
            def description(self):
                return "HA read data bit31_0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData31_0"] = _AF6CCI0011_RD_PLA._rdha24data_control._ReadHaData31_0()
            return allFields

    class _rdha_hold63_32(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data63_32"
    
        def description(self):
            return "This register is used to read HA dword2 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0008c170
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData63_32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData63_32"
            
            def description(self):
                return "HA read data bit63_32"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData63_32"] = _AF6CCI0011_RD_PLA._rdha_hold63_32._ReadHaData63_32()
            return allFields

    class _rdindr_hold95_64(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data95_64"
    
        def description(self):
            return "This register is used to read HA dword3 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0008c171
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData95_64(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData95_64"
            
            def description(self):
                return "HA read data bit95_64"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData95_64"] = _AF6CCI0011_RD_PLA._rdindr_hold95_64._ReadHaData95_64()
            return allFields

    class _rdindr_hold127_96(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data127_96"
    
        def description(self):
            return "This register is used to read HA dword4 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0008c172
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData127_96(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData127_96"
            
            def description(self):
                return "HA read data bit127_96"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData127_96"] = _AF6CCI0011_RD_PLA._rdindr_hold127_96._ReadHaData127_96()
            return allFields

    class _pla_out_psnpro_ha_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Output PSN Process Control"
    
        def description(self):
            return "This register is used to control PSN configuration"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00084001
            
        def endAddress(self):
            return 0xffffffff

        class _PlaOutPsnProReqCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "PlaOutPsnProReqCtr"
            
            def description(self):
                return "Request to process PSN 1: request to write/read PSN header buffer. The CPU need to prepare a complete PSN header into PSN buffer before request write OR read out a complete PSN header in PSN buffer after request read done 0: HW will automatically set to 0 when a request done"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutPsnProLenCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PlaOutPsnProLenCtr"
            
            def description(self):
                return "Length of a complete PSN need to read/write"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutPsnProPageCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "PlaOutPsnProPageCtr"
            
            def description(self):
                return "there is 2 pages PSN location per PWID, depend on the configuration of \"PlaOutHspwPsnCtr\", the engine will choice which the page to encapsulate into the packet."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutPsnProPWIDCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaOutPsnProPWIDCtr"
            
            def description(self):
                return "Pseudowire channel"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaOutPsnProReqCtr"] = _AF6CCI0011_RD_PLA._pla_out_psnpro_ha_ctrl._PlaOutPsnProReqCtr()
            allFields["PlaOutPsnProLenCtr"] = _AF6CCI0011_RD_PLA._pla_out_psnpro_ha_ctrl._PlaOutPsnProLenCtr()
            allFields["PlaOutPsnProPageCtr"] = _AF6CCI0011_RD_PLA._pla_out_psnpro_ha_ctrl._PlaOutPsnProPageCtr()
            allFields["PlaOutPsnProPWIDCtr"] = _AF6CCI0011_RD_PLA._pla_out_psnpro_ha_ctrl._PlaOutPsnProPWIDCtr()
            return allFields

    class _pla_force_par_err_control0(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Force Parity Error Control0"
    
        def description(self):
            return "This register is used to force parity error"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00088040
            
        def endAddress(self):
            return 0xffffffff

        class _ForceParErrLOPWECtrRegOC96_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "ForceParErrLOPWECtrRegOC96_1"
            
            def description(self):
                return "Force Parity Error HO PWE Control Register of OC96#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrLOPldCtrRegOC96_1_OC48_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "ForceParErrLOPldCtrRegOC96_1_OC48_1"
            
            def description(self):
                return "Force Parity Error HO Payload Control Register of OC96#1 OC#48#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrLOPldCtrRegOC96_1_OC48_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "ForceParErrLOPldCtrRegOC96_1_OC48_0"
            
            def description(self):
                return "Force Parity Error HO Payload Control Register of OC96#1 OC#48#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrLOPWECtrRegOC96_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "ForceParErrLOPWECtrRegOC96_0"
            
            def description(self):
                return "Force Parity Error HO PWE Control Register of OC96#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrLOPldCtrRegOC96_0_OC48_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "ForceParErrLOPldCtrRegOC96_0_OC48_1"
            
            def description(self):
                return "Force Parity Error HO Payload Control Register of OC96#0 OC#48#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrLOPldCtrRegOC96_0_OC48_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "ForceParErrLOPldCtrRegOC96_0_OC48_0"
            
            def description(self):
                return "Force Parity Error HO Payload Control Register of OC96#0 OC#48#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrLOPWECtrRegOC48_3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "ForceParErrLOPWECtrRegOC48_3"
            
            def description(self):
                return "Force Parity Error LO PWE Control Register of OC48#3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrLOPldCtrRegOC48_3_OC24_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "ForceParErrLOPldCtrRegOC48_3_OC24_1"
            
            def description(self):
                return "Force Parity Error LO Payload Control Register of OC48#3 OC#24#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrLOPldCtrRegOC48_3_OC24_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "ForceParErrLOPldCtrRegOC48_3_OC24_0"
            
            def description(self):
                return "Force Parity Error LO Payload Control Register of OC48#3 OC#24#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrLOPWECtrRegOC48_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "ForceParErrLOPWECtrRegOC48_2"
            
            def description(self):
                return "Force Parity Error LO PWE Control Register of OC48#2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrLOPldCtrRegOC48_2_OC24_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "ForceParErrLOPldCtrRegOC48_2_OC24_1"
            
            def description(self):
                return "Force Parity Error LO Payload Control Register of OC48#2 OC#24#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrLOPldCtrRegOC48_2_OC24_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "ForceParErrLOPldCtrRegOC48_2_OC24_0"
            
            def description(self):
                return "Force Parity Error LO Payload Control Register of OC48#2 OC#24#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrLOPWECtrRegOC48_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "ForceParErrLOPWECtrRegOC48_1"
            
            def description(self):
                return "Force Parity Error LO PWE Control Register of OC48#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrLOPldCtrRegOC48_1_OC24_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "ForceParErrLOPldCtrRegOC48_1_OC24_1"
            
            def description(self):
                return "Force Parity Error LO Payload Control Register of OC48#1 OC#24#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrLOPldCtrRegOC48_1_OC24_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "ForceParErrLOPldCtrRegOC48_1_OC24_0"
            
            def description(self):
                return "Force Parity Error LO Payload Control Register of OC48#1 OC#24#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrLOPWECtrRegOC48_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "ForceParErrLOPWECtrRegOC48_0"
            
            def description(self):
                return "Force Parity Error LO PWE Control Register of OC48#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrLOPldCtrRegOC48_0_OC24_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "ForceParErrLOPldCtrRegOC48_0_OC24_1"
            
            def description(self):
                return "Force Parity Error LO Payload Control Register of OC48#0 OC#24#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrLOPldCtrRegOC48_0_OC24_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ForceParErrLOPldCtrRegOC48_0_OC24_0"
            
            def description(self):
                return "Force Parity Error LO Payload Control Register of OC48#0 OC#24#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ForceParErrLOPWECtrRegOC96_1"] = _AF6CCI0011_RD_PLA._pla_force_par_err_control0._ForceParErrLOPWECtrRegOC96_1()
            allFields["ForceParErrLOPldCtrRegOC96_1_OC48_1"] = _AF6CCI0011_RD_PLA._pla_force_par_err_control0._ForceParErrLOPldCtrRegOC96_1_OC48_1()
            allFields["ForceParErrLOPldCtrRegOC96_1_OC48_0"] = _AF6CCI0011_RD_PLA._pla_force_par_err_control0._ForceParErrLOPldCtrRegOC96_1_OC48_0()
            allFields["ForceParErrLOPWECtrRegOC96_0"] = _AF6CCI0011_RD_PLA._pla_force_par_err_control0._ForceParErrLOPWECtrRegOC96_0()
            allFields["ForceParErrLOPldCtrRegOC96_0_OC48_1"] = _AF6CCI0011_RD_PLA._pla_force_par_err_control0._ForceParErrLOPldCtrRegOC96_0_OC48_1()
            allFields["ForceParErrLOPldCtrRegOC96_0_OC48_0"] = _AF6CCI0011_RD_PLA._pla_force_par_err_control0._ForceParErrLOPldCtrRegOC96_0_OC48_0()
            allFields["ForceParErrLOPWECtrRegOC48_3"] = _AF6CCI0011_RD_PLA._pla_force_par_err_control0._ForceParErrLOPWECtrRegOC48_3()
            allFields["ForceParErrLOPldCtrRegOC48_3_OC24_1"] = _AF6CCI0011_RD_PLA._pla_force_par_err_control0._ForceParErrLOPldCtrRegOC48_3_OC24_1()
            allFields["ForceParErrLOPldCtrRegOC48_3_OC24_0"] = _AF6CCI0011_RD_PLA._pla_force_par_err_control0._ForceParErrLOPldCtrRegOC48_3_OC24_0()
            allFields["ForceParErrLOPWECtrRegOC48_2"] = _AF6CCI0011_RD_PLA._pla_force_par_err_control0._ForceParErrLOPWECtrRegOC48_2()
            allFields["ForceParErrLOPldCtrRegOC48_2_OC24_1"] = _AF6CCI0011_RD_PLA._pla_force_par_err_control0._ForceParErrLOPldCtrRegOC48_2_OC24_1()
            allFields["ForceParErrLOPldCtrRegOC48_2_OC24_0"] = _AF6CCI0011_RD_PLA._pla_force_par_err_control0._ForceParErrLOPldCtrRegOC48_2_OC24_0()
            allFields["ForceParErrLOPWECtrRegOC48_1"] = _AF6CCI0011_RD_PLA._pla_force_par_err_control0._ForceParErrLOPWECtrRegOC48_1()
            allFields["ForceParErrLOPldCtrRegOC48_1_OC24_1"] = _AF6CCI0011_RD_PLA._pla_force_par_err_control0._ForceParErrLOPldCtrRegOC48_1_OC24_1()
            allFields["ForceParErrLOPldCtrRegOC48_1_OC24_0"] = _AF6CCI0011_RD_PLA._pla_force_par_err_control0._ForceParErrLOPldCtrRegOC48_1_OC24_0()
            allFields["ForceParErrLOPWECtrRegOC48_0"] = _AF6CCI0011_RD_PLA._pla_force_par_err_control0._ForceParErrLOPWECtrRegOC48_0()
            allFields["ForceParErrLOPldCtrRegOC48_0_OC24_1"] = _AF6CCI0011_RD_PLA._pla_force_par_err_control0._ForceParErrLOPldCtrRegOC48_0_OC24_1()
            allFields["ForceParErrLOPldCtrRegOC48_0_OC24_0"] = _AF6CCI0011_RD_PLA._pla_force_par_err_control0._ForceParErrLOPldCtrRegOC48_0_OC24_0()
            return allFields

    class _pla_dis_par_control0(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Disable Parity Check Control0"
    
        def description(self):
            return "This register is used to disable parity check"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00088041
            
        def endAddress(self):
            return 0xffffffff

        class _DisParLOPWECtrRegOC96_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "DisParLOPWECtrRegOC96_1"
            
            def description(self):
                return "Disable Parity HO PWE Control Register of OC96#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParLOPldCtrRegOC96_1_OC48_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "DisParLOPldCtrRegOC96_1_OC48_1"
            
            def description(self):
                return "Disable Parity HO Payload Control Register of OC96#1 OC#48#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParLOPldCtrRegOC96_1_OC48_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "DisParLOPldCtrRegOC96_1_OC48_0"
            
            def description(self):
                return "Disable Parity Error HO Payload Control Register of OC96#1 OC#48#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParLOPWECtrRegOC96_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "DisParLOPWECtrRegOC96_0"
            
            def description(self):
                return "Disable Parity Error HO PWE Control Register of OC96#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParLOPldCtrRegOC96_0_OC48_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "DisParLOPldCtrRegOC96_0_OC48_1"
            
            def description(self):
                return "Disable Parity Error HO Payload Control Register of OC96#0 OC#48#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParLOPldCtrRegOC96_0_OC48_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "DisParLOPldCtrRegOC96_0_OC48_0"
            
            def description(self):
                return "Disable Parity Error HO Payload Control Register of OC96#0 OC#48#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParLOPWECtrRegOC48_3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "DisParLOPWECtrRegOC48_3"
            
            def description(self):
                return "Disable Parity Error LO PWE Control Register of OC48#3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParLOPldCtrRegOC48_3_OC24_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "DisParLOPldCtrRegOC48_3_OC24_1"
            
            def description(self):
                return "Disable Parity Error LO Payload Control Register of OC48#3 OC#24#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParLOPldCtrRegOC48_3_OC24_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "DisParLOPldCtrRegOC48_3_OC24_0"
            
            def description(self):
                return "Disable Parity Error LO Payload Control Register of OC48#3 OC#24#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParLOPWECtrRegOC48_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "DisParLOPWECtrRegOC48_2"
            
            def description(self):
                return "Disable Parity Error LO PWE Control Register of OC48#2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParLOPldCtrRegOC48_2_OC24_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "DisParLOPldCtrRegOC48_2_OC24_1"
            
            def description(self):
                return "Disable Parity Error LO Payload Control Register of OC48#2 OC#24#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParLOPldCtrRegOC48_2_OC24_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "DisParLOPldCtrRegOC48_2_OC24_0"
            
            def description(self):
                return "Disable Parity Error LO Payload Control Register of OC48#2 OC#24#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParLOPWECtrRegOC48_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "DisParLOPWECtrRegOC48_1"
            
            def description(self):
                return "Disable Parity Error LO PWE Control Register of OC48#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParLOPldCtrRegOC48_1_OC24_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "DisParLOPldCtrRegOC48_1_OC24_1"
            
            def description(self):
                return "Disable Parity Error LO Payload Control Register of OC48#1 OC#24#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParLOPldCtrRegOC48_1_OC24_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "DisParLOPldCtrRegOC48_1_OC24_0"
            
            def description(self):
                return "Disable Parity Error LO Payload Control Register of OC48#1 OC#24#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParLOPWECtrRegOC48_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "DisParLOPWECtrRegOC48_0"
            
            def description(self):
                return "Disable Parity Error LO PWE Control Register of OC48#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParLOPldCtrRegOC48_0_OC24_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "DisParLOPldCtrRegOC48_0_OC24_1"
            
            def description(self):
                return "Disable Parity Error LO Payload Control Register of OC48#0 OC#24#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParLOPldCtrRegOC48_0_OC24_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DisParLOPldCtrRegOC48_0_OC24_0"
            
            def description(self):
                return "Disable Parity Error LO Payload Control Register of OC48#0 OC#24#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DisParLOPWECtrRegOC96_1"] = _AF6CCI0011_RD_PLA._pla_dis_par_control0._DisParLOPWECtrRegOC96_1()
            allFields["DisParLOPldCtrRegOC96_1_OC48_1"] = _AF6CCI0011_RD_PLA._pla_dis_par_control0._DisParLOPldCtrRegOC96_1_OC48_1()
            allFields["DisParLOPldCtrRegOC96_1_OC48_0"] = _AF6CCI0011_RD_PLA._pla_dis_par_control0._DisParLOPldCtrRegOC96_1_OC48_0()
            allFields["DisParLOPWECtrRegOC96_0"] = _AF6CCI0011_RD_PLA._pla_dis_par_control0._DisParLOPWECtrRegOC96_0()
            allFields["DisParLOPldCtrRegOC96_0_OC48_1"] = _AF6CCI0011_RD_PLA._pla_dis_par_control0._DisParLOPldCtrRegOC96_0_OC48_1()
            allFields["DisParLOPldCtrRegOC96_0_OC48_0"] = _AF6CCI0011_RD_PLA._pla_dis_par_control0._DisParLOPldCtrRegOC96_0_OC48_0()
            allFields["DisParLOPWECtrRegOC48_3"] = _AF6CCI0011_RD_PLA._pla_dis_par_control0._DisParLOPWECtrRegOC48_3()
            allFields["DisParLOPldCtrRegOC48_3_OC24_1"] = _AF6CCI0011_RD_PLA._pla_dis_par_control0._DisParLOPldCtrRegOC48_3_OC24_1()
            allFields["DisParLOPldCtrRegOC48_3_OC24_0"] = _AF6CCI0011_RD_PLA._pla_dis_par_control0._DisParLOPldCtrRegOC48_3_OC24_0()
            allFields["DisParLOPWECtrRegOC48_2"] = _AF6CCI0011_RD_PLA._pla_dis_par_control0._DisParLOPWECtrRegOC48_2()
            allFields["DisParLOPldCtrRegOC48_2_OC24_1"] = _AF6CCI0011_RD_PLA._pla_dis_par_control0._DisParLOPldCtrRegOC48_2_OC24_1()
            allFields["DisParLOPldCtrRegOC48_2_OC24_0"] = _AF6CCI0011_RD_PLA._pla_dis_par_control0._DisParLOPldCtrRegOC48_2_OC24_0()
            allFields["DisParLOPWECtrRegOC48_1"] = _AF6CCI0011_RD_PLA._pla_dis_par_control0._DisParLOPWECtrRegOC48_1()
            allFields["DisParLOPldCtrRegOC48_1_OC24_1"] = _AF6CCI0011_RD_PLA._pla_dis_par_control0._DisParLOPldCtrRegOC48_1_OC24_1()
            allFields["DisParLOPldCtrRegOC48_1_OC24_0"] = _AF6CCI0011_RD_PLA._pla_dis_par_control0._DisParLOPldCtrRegOC48_1_OC24_0()
            allFields["DisParLOPWECtrRegOC48_0"] = _AF6CCI0011_RD_PLA._pla_dis_par_control0._DisParLOPWECtrRegOC48_0()
            allFields["DisParLOPldCtrRegOC48_0_OC24_1"] = _AF6CCI0011_RD_PLA._pla_dis_par_control0._DisParLOPldCtrRegOC48_0_OC24_1()
            allFields["DisParLOPldCtrRegOC48_0_OC24_0"] = _AF6CCI0011_RD_PLA._pla_dis_par_control0._DisParLOPldCtrRegOC48_0_OC24_0()
            return allFields

    class _pla_par_stk0(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Parity Error Sticky0"
    
        def description(self):
            return "This register is used to sticky parity check"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00088042
            
        def endAddress(self):
            return 0xffffffff

        class _ParLOPWECtrRegOC96_1Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "ParLOPWECtrRegOC96_1Stk"
            
            def description(self):
                return "Sticky Parity HO PWE Control Register of OC96#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParLOPldCtrRegOC96_1_OC48_1Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "ParLOPldCtrRegOC96_1_OC48_1Stk"
            
            def description(self):
                return "Sticky Parity HO Payload Control Register of OC96#1 OC#48#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParLOPldCtrRegOC96_1_OC48_0Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "ParLOPldCtrRegOC96_1_OC48_0Stk"
            
            def description(self):
                return "Sticky Parity Error HO Payload Control Register of OC96#1 OC#48#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParLOPWECtrRegOC96_0Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "ParLOPWECtrRegOC96_0Stk"
            
            def description(self):
                return "Sticky Parity Error HO PWE Control Register of OC96#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParLOPldCtrRegOC96_0_OC48_1Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "ParLOPldCtrRegOC96_0_OC48_1Stk"
            
            def description(self):
                return "Sticky Parity Error HO Payload Control Register of OC96#0 OC#48#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParLOPldCtrRegOC96_0_OC48_0Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "ParLOPldCtrRegOC96_0_OC48_0Stk"
            
            def description(self):
                return "Sticky Parity Error HO Payload Control Register of OC96#0 OC#48#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParLOPWECtrRegOC48_3Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "ParLOPWECtrRegOC48_3Stk"
            
            def description(self):
                return "Sticky Parity Error LO PWE Control Register of OC48#3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParLOPldCtrRegOC48_3_OC24_1Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "ParLOPldCtrRegOC48_3_OC24_1Stk"
            
            def description(self):
                return "Sticky Parity Error LO Payload Control Register of OC48#3 OC#24#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParLOPldCtrRegOC48_3_OC24_0Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "ParLOPldCtrRegOC48_3_OC24_0Stk"
            
            def description(self):
                return "Sticky Parity Error LO Payload Control Register of OC48#3 OC#24#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParLOPWECtrRegOC48_2Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "ParLOPWECtrRegOC48_2Stk"
            
            def description(self):
                return "Sticky Parity Error LO PWE Control Register of OC48#2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParLOPldCtrRegOC48_2_OC24_1Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "ParLOPldCtrRegOC48_2_OC24_1Stk"
            
            def description(self):
                return "Sticky Parity Error LO Payload Control Register of OC48#2 OC#24#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParLOPldCtrRegOC48_2_OC24_0Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "ParLOPldCtrRegOC48_2_OC24_0Stk"
            
            def description(self):
                return "Sticky Parity Error LO Payload Control Register of OC48#2 OC#24#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParLOPWECtrRegOC48_1Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "ParLOPWECtrRegOC48_1Stk"
            
            def description(self):
                return "Sticky Parity Error LO PWE Control Register of OC48#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParLOPldCtrRegOC48_1_OC24_1Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "ParLOPldCtrRegOC48_1_OC24_1Stk"
            
            def description(self):
                return "Sticky Parity Error LO Payload Control Register of OC48#1 OC#24#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParLOPldCtrRegOC48_1_OC24_0Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "ParLOPldCtrRegOC48_1_OC24_0Stk"
            
            def description(self):
                return "Sticky Parity Error LO Payload Control Register of OC48#1 OC#24#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParLOPWECtrRegOC48_0Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "ParLOPWECtrRegOC48_0Stk"
            
            def description(self):
                return "Sticky Parity Error LO PWE Control Register of OC48#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParLOPldCtrRegOC48_0_OC24_1Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "ParLOPldCtrRegOC48_0_OC24_1Stk"
            
            def description(self):
                return "Sticky Parity Error LO Payload Control Register of OC48#0 OC#24#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParLOPldCtrRegOC48_0_OC24_0Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ParLOPldCtrRegOC48_0_OC24_0Stk"
            
            def description(self):
                return "Sticky Parity Error LO Payload Control Register of OC48#0 OC#24#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ParLOPWECtrRegOC96_1Stk"] = _AF6CCI0011_RD_PLA._pla_par_stk0._ParLOPWECtrRegOC96_1Stk()
            allFields["ParLOPldCtrRegOC96_1_OC48_1Stk"] = _AF6CCI0011_RD_PLA._pla_par_stk0._ParLOPldCtrRegOC96_1_OC48_1Stk()
            allFields["ParLOPldCtrRegOC96_1_OC48_0Stk"] = _AF6CCI0011_RD_PLA._pla_par_stk0._ParLOPldCtrRegOC96_1_OC48_0Stk()
            allFields["ParLOPWECtrRegOC96_0Stk"] = _AF6CCI0011_RD_PLA._pla_par_stk0._ParLOPWECtrRegOC96_0Stk()
            allFields["ParLOPldCtrRegOC96_0_OC48_1Stk"] = _AF6CCI0011_RD_PLA._pla_par_stk0._ParLOPldCtrRegOC96_0_OC48_1Stk()
            allFields["ParLOPldCtrRegOC96_0_OC48_0Stk"] = _AF6CCI0011_RD_PLA._pla_par_stk0._ParLOPldCtrRegOC96_0_OC48_0Stk()
            allFields["ParLOPWECtrRegOC48_3Stk"] = _AF6CCI0011_RD_PLA._pla_par_stk0._ParLOPWECtrRegOC48_3Stk()
            allFields["ParLOPldCtrRegOC48_3_OC24_1Stk"] = _AF6CCI0011_RD_PLA._pla_par_stk0._ParLOPldCtrRegOC48_3_OC24_1Stk()
            allFields["ParLOPldCtrRegOC48_3_OC24_0Stk"] = _AF6CCI0011_RD_PLA._pla_par_stk0._ParLOPldCtrRegOC48_3_OC24_0Stk()
            allFields["ParLOPWECtrRegOC48_2Stk"] = _AF6CCI0011_RD_PLA._pla_par_stk0._ParLOPWECtrRegOC48_2Stk()
            allFields["ParLOPldCtrRegOC48_2_OC24_1Stk"] = _AF6CCI0011_RD_PLA._pla_par_stk0._ParLOPldCtrRegOC48_2_OC24_1Stk()
            allFields["ParLOPldCtrRegOC48_2_OC24_0Stk"] = _AF6CCI0011_RD_PLA._pla_par_stk0._ParLOPldCtrRegOC48_2_OC24_0Stk()
            allFields["ParLOPWECtrRegOC48_1Stk"] = _AF6CCI0011_RD_PLA._pla_par_stk0._ParLOPWECtrRegOC48_1Stk()
            allFields["ParLOPldCtrRegOC48_1_OC24_1Stk"] = _AF6CCI0011_RD_PLA._pla_par_stk0._ParLOPldCtrRegOC48_1_OC24_1Stk()
            allFields["ParLOPldCtrRegOC48_1_OC24_0Stk"] = _AF6CCI0011_RD_PLA._pla_par_stk0._ParLOPldCtrRegOC48_1_OC24_0Stk()
            allFields["ParLOPWECtrRegOC48_0Stk"] = _AF6CCI0011_RD_PLA._pla_par_stk0._ParLOPWECtrRegOC48_0Stk()
            allFields["ParLOPldCtrRegOC48_0_OC24_1Stk"] = _AF6CCI0011_RD_PLA._pla_par_stk0._ParLOPldCtrRegOC48_0_OC24_1Stk()
            allFields["ParLOPldCtrRegOC48_0_OC24_0Stk"] = _AF6CCI0011_RD_PLA._pla_par_stk0._ParLOPldCtrRegOC48_0_OC24_0Stk()
            return allFields

    class _pla_force_par_err_control1(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Force Parity Error Control1"
    
        def description(self):
            return "This register is used to force parity error"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00088044
            
        def endAddress(self):
            return 0xffffffff

        class _ForceParErrHSPWP2CtrReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "ForceParErrHSPWP2CtrReg"
            
            def description(self):
                return "Force Parity Error HSPW Port2 Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrUPSRP2CtrReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "ForceParErrUPSRP2CtrReg"
            
            def description(self):
                return "Force Parity Error UPSR Port2 Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrOutPWEP2CtrReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "ForceParErrOutPWEP2CtrReg"
            
            def description(self):
                return "Force Parity Error Output PWE Port2 Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrHSPWP1CtrReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "ForceParErrHSPWP1CtrReg"
            
            def description(self):
                return "Force Parity Error HSPW Port1 Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrUPSRP1CtrReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "ForceParErrUPSRP1CtrReg"
            
            def description(self):
                return "Force Parity Error UPSR Port1 Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrOutPWEP1CtrReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ForceParErrOutPWEP1CtrReg"
            
            def description(self):
                return "Force Parity Error Output PWE Port1 Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ForceParErrHSPWP2CtrReg"] = _AF6CCI0011_RD_PLA._pla_force_par_err_control1._ForceParErrHSPWP2CtrReg()
            allFields["ForceParErrUPSRP2CtrReg"] = _AF6CCI0011_RD_PLA._pla_force_par_err_control1._ForceParErrUPSRP2CtrReg()
            allFields["ForceParErrOutPWEP2CtrReg"] = _AF6CCI0011_RD_PLA._pla_force_par_err_control1._ForceParErrOutPWEP2CtrReg()
            allFields["ForceParErrHSPWP1CtrReg"] = _AF6CCI0011_RD_PLA._pla_force_par_err_control1._ForceParErrHSPWP1CtrReg()
            allFields["ForceParErrUPSRP1CtrReg"] = _AF6CCI0011_RD_PLA._pla_force_par_err_control1._ForceParErrUPSRP1CtrReg()
            allFields["ForceParErrOutPWEP1CtrReg"] = _AF6CCI0011_RD_PLA._pla_force_par_err_control1._ForceParErrOutPWEP1CtrReg()
            return allFields

    class _pla_dis_par_control1(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Disable Parity Check Control1"
    
        def description(self):
            return "This register is used to disable parity check"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00088045
            
        def endAddress(self):
            return 0xffffffff

        class _DisParHSPWP2CtrReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "DisParHSPWP2CtrReg"
            
            def description(self):
                return "Disable Parity HSPW Port2 Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParUPSRP2CtrReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "DisParUPSRP2CtrReg"
            
            def description(self):
                return "Disable Parity UPSR Port2 Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParOutPWEP2CtrReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "DisParOutPWEP2CtrReg"
            
            def description(self):
                return "Disable Parity Output PWE Port2 Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParHSPWP1CtrReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "DisParHSPWP1CtrReg"
            
            def description(self):
                return "Disable Parity HSPW Port1 Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParUPSRP1CtrReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "DisParUPSRP1CtrReg"
            
            def description(self):
                return "Disable Parity UPSR Port1 Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParOutPWEP1CtrReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "DisParOutPWEP1CtrReg"
            
            def description(self):
                return "Disable Parity Output PWE Port1 Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DisParHSPWP2CtrReg"] = _AF6CCI0011_RD_PLA._pla_dis_par_control1._DisParHSPWP2CtrReg()
            allFields["DisParUPSRP2CtrReg"] = _AF6CCI0011_RD_PLA._pla_dis_par_control1._DisParUPSRP2CtrReg()
            allFields["DisParOutPWEP2CtrReg"] = _AF6CCI0011_RD_PLA._pla_dis_par_control1._DisParOutPWEP2CtrReg()
            allFields["DisParHSPWP1CtrReg"] = _AF6CCI0011_RD_PLA._pla_dis_par_control1._DisParHSPWP1CtrReg()
            allFields["DisParUPSRP1CtrReg"] = _AF6CCI0011_RD_PLA._pla_dis_par_control1._DisParUPSRP1CtrReg()
            allFields["DisParOutPWEP1CtrReg"] = _AF6CCI0011_RD_PLA._pla_dis_par_control1._DisParOutPWEP1CtrReg()
            return allFields

    class _pla_par_err_stk1(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Parity Error Sticky1"
    
        def description(self):
            return "This register is used to sticky parity check"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00088046
            
        def endAddress(self):
            return 0xffffffff

        class _ParHSPWP2CtrRegStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "ParHSPWP2CtrRegStk"
            
            def description(self):
                return "Sticky Parity HSPW Port2 Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParUPSRP2CtrRegStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "ParUPSRP2CtrRegStk"
            
            def description(self):
                return "Sticky Parity UPSR Port2 Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParOutPWEP2CtrRegStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "ParOutPWEP2CtrRegStk"
            
            def description(self):
                return "Sticky Parity Output PWE Port2 Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParHSPWP1CtrRegStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "ParHSPWP1CtrRegStk"
            
            def description(self):
                return "Sticky Parity HSPW Port1 Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParUPSRP1CtrRegStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "ParUPSRP1CtrRegStk"
            
            def description(self):
                return "Sticky Parity UPSR Port1 Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParOutPWEP1CtrRegStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ParOutPWEP1CtrRegStk"
            
            def description(self):
                return "Sticky Parity Output PWE Port1 Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ParHSPWP2CtrRegStk"] = _AF6CCI0011_RD_PLA._pla_par_err_stk1._ParHSPWP2CtrRegStk()
            allFields["ParUPSRP2CtrRegStk"] = _AF6CCI0011_RD_PLA._pla_par_err_stk1._ParUPSRP2CtrRegStk()
            allFields["ParOutPWEP2CtrRegStk"] = _AF6CCI0011_RD_PLA._pla_par_err_stk1._ParOutPWEP2CtrRegStk()
            allFields["ParHSPWP1CtrRegStk"] = _AF6CCI0011_RD_PLA._pla_par_err_stk1._ParHSPWP1CtrRegStk()
            allFields["ParUPSRP1CtrRegStk"] = _AF6CCI0011_RD_PLA._pla_par_err_stk1._ParUPSRP1CtrRegStk()
            allFields["ParOutPWEP1CtrRegStk"] = _AF6CCI0011_RD_PLA._pla_par_err_stk1._ParOutPWEP1CtrRegStk()
            return allFields

    class _pla_force_crc_err_control(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Force CRC Error Control"
    
        def description(self):
            return "This register is used to force CRC error"
            
        def width(self):
            return 1
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00088048
            
        def endAddress(self):
            return 0xffffffff

        class _ForceCRCErrCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ForceCRCErrCtr"
            
            def description(self):
                return "Force CRC Error Control"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ForceCRCErrCtr"] = _AF6CCI0011_RD_PLA._pla_force_crc_err_control._ForceCRCErrCtr()
            return allFields

    class _pla_dis_crc_control(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Disable CRC Check Control"
    
        def description(self):
            return "This register is used to disable CRC check"
            
        def width(self):
            return 1
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00088049
            
        def endAddress(self):
            return 0xffffffff

        class _DisCRCChkCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DisCRCChkCtr"
            
            def description(self):
                return "Disable CRC check Control"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DisCRCChkCtr"] = _AF6CCI0011_RD_PLA._pla_dis_crc_control._DisCRCChkCtr()
            return allFields

    class _pla_crc_err_stk(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler CRC Error Sticky"
    
        def description(self):
            return "This register is used to check CRC error sticky"
            
        def width(self):
            return 1
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0008804a
            
        def endAddress(self):
            return 0xffffffff

        class _CRCCheckStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CRCCheckStk"
            
            def description(self):
                return "Sticky CRC check Port1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CRCCheckStk"] = _AF6CCI0011_RD_PLA._pla_crc_err_stk._CRCCheckStk()
            return allFields

    class _pwe_pla_fsm_ctr(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PLA FSM Control"
    
        def description(self):
            return "This register is used to control FSM TX PW"
            
        def width(self):
            return 2
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00088020
            
        def endAddress(self):
            return 0xffffffff

        class _ForceFSM(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "ForceFSM"
            
            def description(self):
                return "Force FSM state for selftest"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FSMPinEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FSMPinEn"
            
            def description(self):
                return "Enable FSM from pin outside"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ForceFSM"] = _AF6CCI0011_RD_PLA._pwe_pla_fsm_ctr._ForceFSM()
            allFields["FSMPinEn"] = _AF6CCI0011_RD_PLA._pwe_pla_fsm_ctr._FSMPinEn()
            return allFields

import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0011_RD_PLA_DEBUG(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["pla_lo_clk155_stk"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_clk155_stk()
        allRegisters["pla_ho_clk311_stk"] = _AF6CCI0011_RD_PLA_DEBUG._pla_ho_clk311_stk()
        allRegisters["pla_ho_block_pro_stk"] = _AF6CCI0011_RD_PLA_DEBUG._pla_ho_block_pro_stk()
        allRegisters["pla_lo_block_pro_stk"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_block_pro_stk()
        allRegisters["pla_interface_stk"] = _AF6CCI0011_RD_PLA_DEBUG._pla_interface_stk()
        return allRegisters

    class _pla_lo_clk155_stk(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Low-Order clk155 Sticky"
    
        def description(self):
            return "This register is used to used to sticky some alarms for debug per OC-48 Lo-order @clk155.52 domain"
            
        def width(self):
            return 14
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _PlaLo155OC24_1ConvErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "PlaLo155OC24_1ConvErr"
            
            def description(self):
                return "OC24#1 convert 155to233 Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo155OC24_0ConvErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PlaLo155OC24_0ConvErr"
            
            def description(self):
                return "OC24#0 convert 155to233 Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo155OC24_1PwVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "PlaLo155OC24_1PwVld"
            
            def description(self):
                return "OC24#1 input of expected pwid"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo155OC24_0PwVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "PlaLo155OC24_0PwVld"
            
            def description(self):
                return "OC24#0 input of expected pwid"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo155OC24_0InVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "PlaLo155OC24_0InVld"
            
            def description(self):
                return "OC24#0 input data from demap"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo155OC24_1InVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PlaLo155OC24_1InVld"
            
            def description(self):
                return "OC24#1 input data from demap"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo155OC24_0InFst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "PlaLo155OC24_0InFst"
            
            def description(self):
                return "OC24#0 input first-timeslot"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo155OC24_1InFst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PlaLo155OC24_1InFst"
            
            def description(self):
                return "OC24#1 input first-timeslot"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo155OC24_0InAis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PlaLo155OC24_0InAis"
            
            def description(self):
                return "OC24#0 input AIS"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo155OC24_1InAIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PlaLo155OC24_1InAIS"
            
            def description(self):
                return "OC24#1 input AIS"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo155OC24_0InPos(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "PlaLo155OC24_0InPos"
            
            def description(self):
                return "OC24#0 input CEP Pos Pointer"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo155OC24_0InNeg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PlaLo155OC24_0InNeg"
            
            def description(self):
                return "OC24#0 input CEP Neg Pointer"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo155OC24_0InPos(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PlaLo155OC24_0InPos"
            
            def description(self):
                return "OC24#1 input CEP Pos Pointer"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo155OC24_0InNeg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaLo155OC24_0InNeg"
            
            def description(self):
                return "OC24#1 input CEP Neg Pointer"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaLo155OC24_1ConvErr"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_clk155_stk._PlaLo155OC24_1ConvErr()
            allFields["PlaLo155OC24_0ConvErr"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_clk155_stk._PlaLo155OC24_0ConvErr()
            allFields["PlaLo155OC24_1PwVld"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_clk155_stk._PlaLo155OC24_1PwVld()
            allFields["PlaLo155OC24_0PwVld"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_clk155_stk._PlaLo155OC24_0PwVld()
            allFields["PlaLo155OC24_0InVld"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_clk155_stk._PlaLo155OC24_0InVld()
            allFields["PlaLo155OC24_1InVld"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_clk155_stk._PlaLo155OC24_1InVld()
            allFields["PlaLo155OC24_0InFst"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_clk155_stk._PlaLo155OC24_0InFst()
            allFields["PlaLo155OC24_1InFst"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_clk155_stk._PlaLo155OC24_1InFst()
            allFields["PlaLo155OC24_0InAis"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_clk155_stk._PlaLo155OC24_0InAis()
            allFields["PlaLo155OC24_1InAIS"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_clk155_stk._PlaLo155OC24_1InAIS()
            allFields["PlaLo155OC24_0InPos"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_clk155_stk._PlaLo155OC24_0InPos()
            allFields["PlaLo155OC24_0InNeg"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_clk155_stk._PlaLo155OC24_0InNeg()
            allFields["PlaLo155OC24_0InPos"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_clk155_stk._PlaLo155OC24_0InPos()
            allFields["PlaLo155OC24_0InNeg"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_clk155_stk._PlaLo155OC24_0InNeg()
            return allFields

    class _pla_ho_clk311_stk(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler High-Order clk311 Sticky"
    
        def description(self):
            return "This register is used to used to sticky some alarms for debug per OC-96 Hi-order @clk311.04 domain"
            
        def width(self):
            return 14
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_0400 + $HoOc96Slice*2048"
            
        def startAddress(self):
            return 0x00080400
            
        def endAddress(self):
            return 0x00082c00

        class _PlaLo311OC48_1ConvErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "PlaLo311OC48_1ConvErr"
            
            def description(self):
                return "OC48#1 convert 311to233 Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo311OC48_0ConvErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PlaLo311OC48_0ConvErr"
            
            def description(self):
                return "OC48#0 convert 311to233 Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo311OC48_1PwVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "PlaLo311OC48_1PwVld"
            
            def description(self):
                return "OC48#1 input of expected pwid"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo311OC48_0PwVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "PlaLo311OC48_0PwVld"
            
            def description(self):
                return "OC48#0 input of expected pwid"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo311OC48_0InVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "PlaLo311OC48_0InVld"
            
            def description(self):
                return "OC48#0 input data from demap"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo311OC48_1InVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PlaLo311OC48_1InVld"
            
            def description(self):
                return "OC48#1 input data from demap"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo311OC48_0InFst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "PlaLo311OC48_0InFst"
            
            def description(self):
                return "OC48#0 input first-timeslot"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo311OC48_1InFst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PlaLo311OC48_1InFst"
            
            def description(self):
                return "OC48#1 input first-timeslot"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo311OC48_0InAis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PlaLo311OC48_0InAis"
            
            def description(self):
                return "OC48#0 input AIS"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo311OC48_1InAIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PlaLo311OC48_1InAIS"
            
            def description(self):
                return "OC48#1 input AIS"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo311OC48_0InPos(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "PlaLo311OC48_0InPos"
            
            def description(self):
                return "OC48#0 input CEP Pos Pointer"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo311OC48_0InNeg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PlaLo311OC48_0InNeg"
            
            def description(self):
                return "OC48#0 input CEP Neg Pointer"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo311OC48_0InPos(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PlaLo311OC48_0InPos"
            
            def description(self):
                return "OC48#1 input CEP Pos Pointer"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLo311OC48_0InNeg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaLo311OC48_0InNeg"
            
            def description(self):
                return "OC48#1 input CEP Neg Pointer"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaLo311OC48_1ConvErr"] = _AF6CCI0011_RD_PLA_DEBUG._pla_ho_clk311_stk._PlaLo311OC48_1ConvErr()
            allFields["PlaLo311OC48_0ConvErr"] = _AF6CCI0011_RD_PLA_DEBUG._pla_ho_clk311_stk._PlaLo311OC48_0ConvErr()
            allFields["PlaLo311OC48_1PwVld"] = _AF6CCI0011_RD_PLA_DEBUG._pla_ho_clk311_stk._PlaLo311OC48_1PwVld()
            allFields["PlaLo311OC48_0PwVld"] = _AF6CCI0011_RD_PLA_DEBUG._pla_ho_clk311_stk._PlaLo311OC48_0PwVld()
            allFields["PlaLo311OC48_0InVld"] = _AF6CCI0011_RD_PLA_DEBUG._pla_ho_clk311_stk._PlaLo311OC48_0InVld()
            allFields["PlaLo311OC48_1InVld"] = _AF6CCI0011_RD_PLA_DEBUG._pla_ho_clk311_stk._PlaLo311OC48_1InVld()
            allFields["PlaLo311OC48_0InFst"] = _AF6CCI0011_RD_PLA_DEBUG._pla_ho_clk311_stk._PlaLo311OC48_0InFst()
            allFields["PlaLo311OC48_1InFst"] = _AF6CCI0011_RD_PLA_DEBUG._pla_ho_clk311_stk._PlaLo311OC48_1InFst()
            allFields["PlaLo311OC48_0InAis"] = _AF6CCI0011_RD_PLA_DEBUG._pla_ho_clk311_stk._PlaLo311OC48_0InAis()
            allFields["PlaLo311OC48_1InAIS"] = _AF6CCI0011_RD_PLA_DEBUG._pla_ho_clk311_stk._PlaLo311OC48_1InAIS()
            allFields["PlaLo311OC48_0InPos"] = _AF6CCI0011_RD_PLA_DEBUG._pla_ho_clk311_stk._PlaLo311OC48_0InPos()
            allFields["PlaLo311OC48_0InNeg"] = _AF6CCI0011_RD_PLA_DEBUG._pla_ho_clk311_stk._PlaLo311OC48_0InNeg()
            allFields["PlaLo311OC48_0InPos"] = _AF6CCI0011_RD_PLA_DEBUG._pla_ho_clk311_stk._PlaLo311OC48_0InPos()
            allFields["PlaLo311OC48_0InNeg"] = _AF6CCI0011_RD_PLA_DEBUG._pla_ho_clk311_stk._PlaLo311OC48_0InNeg()
            return allFields

    class _pla_ho_block_pro_stk(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler High-Order Block Process Sticky"
    
        def description(self):
            return "This register is used to used to sticky some alarms for debug per OC-96 Hi-order block processing"
            
        def width(self):
            return 8
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_0601 + $HoOc96Slice*2048"
            
        def startAddress(self):
            return 0x00080601
            
        def endAddress(self):
            return 0x00082e01

        class _PlaHoBlkPkInfoReq(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "PlaHoBlkPkInfoReq"
            
            def description(self):
                return "OC96 packet infor request"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaHoBlkPkInfoAck(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PlaHoBlkPkInfoAck"
            
            def description(self):
                return "OC96 packet infor Ack"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaHoBlkPkInfoVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PlaHoBlkPkInfoVld"
            
            def description(self):
                return "OC96 packet infor Valid"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaHoBlkMaxLength(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PlaHoBlkMaxLength"
            
            def description(self):
                return "OC96 max length violation"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaHoPla2ConvErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "PlaHoPla2ConvErr"
            
            def description(self):
                return "OC96 OC48#2 Convert Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaHoPla1ConvErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PlaHoPla1ConvErr"
            
            def description(self):
                return "OC96 OC48#1 Convert Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaHoBlkEmp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PlaHoBlkEmp"
            
            def description(self):
                return "OC96 block empty"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaHoBlkSame(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaHoBlkSame"
            
            def description(self):
                return "OC96 block same"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaHoBlkPkInfoReq"] = _AF6CCI0011_RD_PLA_DEBUG._pla_ho_block_pro_stk._PlaHoBlkPkInfoReq()
            allFields["PlaHoBlkPkInfoAck"] = _AF6CCI0011_RD_PLA_DEBUG._pla_ho_block_pro_stk._PlaHoBlkPkInfoAck()
            allFields["PlaHoBlkPkInfoVld"] = _AF6CCI0011_RD_PLA_DEBUG._pla_ho_block_pro_stk._PlaHoBlkPkInfoVld()
            allFields["PlaHoBlkMaxLength"] = _AF6CCI0011_RD_PLA_DEBUG._pla_ho_block_pro_stk._PlaHoBlkMaxLength()
            allFields["PlaHoPla2ConvErr"] = _AF6CCI0011_RD_PLA_DEBUG._pla_ho_block_pro_stk._PlaHoPla2ConvErr()
            allFields["PlaHoPla1ConvErr"] = _AF6CCI0011_RD_PLA_DEBUG._pla_ho_block_pro_stk._PlaHoPla1ConvErr()
            allFields["PlaHoBlkEmp"] = _AF6CCI0011_RD_PLA_DEBUG._pla_ho_block_pro_stk._PlaHoBlkEmp()
            allFields["PlaHoBlkSame"] = _AF6CCI0011_RD_PLA_DEBUG._pla_ho_block_pro_stk._PlaHoBlkSame()
            return allFields

    class _pla_lo_block_pro_stk(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Low-Order Block Process Sticky"
    
        def description(self):
            return "This register is used to used to sticky some alarms for debug low-order block processing"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00088001
            
        def endAddress(self):
            return 0xffffffff

        class _PlaLoBlkAllCacheEmp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "PlaLoBlkAllCacheEmp"
            
            def description(self):
                return "LO all cache empty"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoBlkMaxLengthErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "PlaLoBlkMaxLengthErr"
            
            def description(self):
                return "LO Max Length Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoDDrMuxRdFull(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "PlaLoDDrMuxRdFull"
            
            def description(self):
                return "LO DDR Mux Read Full"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoDDRMuxWrFull(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "PlaLoDDRMuxWrFull"
            
            def description(self):
                return "LO DDR Mux Write Full"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoDDRStaErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "PlaLoDDRStaErr"
            
            def description(self):
                return "LO DDR Status Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoDDRBuf1stCaFul(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "PlaLoDDRBuf1stCaFul"
            
            def description(self):
                return "LO DDR Buffer 1st Cache Full"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoDDRBuf2ndCaFul(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "PlaLoDDRBuf2ndCaFul"
            
            def description(self):
                return "LO DDR Buffer 2nd Cache Full"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoDDRMuxAckFul(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "PlaLoDDRMuxAckFul"
            
            def description(self):
                return "LO DDR Mux Ack Full"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoDDRBak7Ful(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "PlaLoDDRBak7Ful"
            
            def description(self):
                return "LO DDR Buffer Bank7 Full"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoDDRBak6Ful(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "PlaLoDDRBak6Ful"
            
            def description(self):
                return "LO DDR Buffer Bank6 Full"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoDDRBak5Ful(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "PlaLoDDRBak5Ful"
            
            def description(self):
                return "LO DDR Buffer Bank5 Full"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoDDRBak4Ful(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PlaLoDDRBak4Ful"
            
            def description(self):
                return "LO DDR Buffer Bank4 Full"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoDDRBak3Ful(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "PlaLoDDRBak3Ful"
            
            def description(self):
                return "LO DDR Buffer Bank3 Full"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoDDRBak2Ful(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "PlaLoDDRBak2Ful"
            
            def description(self):
                return "LO DDR Buffer Bank2 Full"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoDDRBak1Ful(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "PlaLoDDRBak1Ful"
            
            def description(self):
                return "LO DDR Buffer Bank1 Full"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoDDRBak0Ful(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PlaLoDDRBak0Ful"
            
            def description(self):
                return "LO DDR Buffer Bank0 Full"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoBlkOutVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "PlaLoBlkOutVld"
            
            def description(self):
                return "LO Block output valid"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoBlkCa256Same(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "PlaLoBlkCa256Same"
            
            def description(self):
                return "LO Block Cache256 same"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoBlkCa128Same(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "PlaLoBlkCa128Same"
            
            def description(self):
                return "LO Block Cache128 same"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoBlkCa64Same(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PlaLoBlkCa64Same"
            
            def description(self):
                return "LO Block Cache64 same"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoBlkPkInfoVld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "PlaLoBlkPkInfoVld"
            
            def description(self):
                return "LO Block Pkt Info valid"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoBlkCa256Emp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PlaLoBlkCa256Emp"
            
            def description(self):
                return "LO Block Cache256 Empty"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoBlkCa128Emp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PlaLoBlkCa128Emp"
            
            def description(self):
                return "LO Block Cache128 Empty"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoBlkCa64Emp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PlaLoBlkCa64Emp"
            
            def description(self):
                return "LO Block Cache64 Empty"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoBlkHiBlkEmp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "PlaLoBlkHiBlkEmp"
            
            def description(self):
                return "LO Block High Size Block Empty"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoBlkLoBlkEmp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PlaLoBlkLoBlkEmp"
            
            def description(self):
                return "LO Block Low Size Block Empty"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoBlkHiBlkSame(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PlaLoBlkHiBlkSame"
            
            def description(self):
                return "LO Block High Size Block same"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoBlkLoBlkSame(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaLoBlkLoBlkSame"
            
            def description(self):
                return "LO Block Low Size Block same"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaLoBlkAllCacheEmp"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_block_pro_stk._PlaLoBlkAllCacheEmp()
            allFields["PlaLoBlkMaxLengthErr"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_block_pro_stk._PlaLoBlkMaxLengthErr()
            allFields["PlaLoDDrMuxRdFull"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_block_pro_stk._PlaLoDDrMuxRdFull()
            allFields["PlaLoDDRMuxWrFull"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_block_pro_stk._PlaLoDDRMuxWrFull()
            allFields["PlaLoDDRStaErr"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_block_pro_stk._PlaLoDDRStaErr()
            allFields["PlaLoDDRBuf1stCaFul"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_block_pro_stk._PlaLoDDRBuf1stCaFul()
            allFields["PlaLoDDRBuf2ndCaFul"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_block_pro_stk._PlaLoDDRBuf2ndCaFul()
            allFields["PlaLoDDRMuxAckFul"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_block_pro_stk._PlaLoDDRMuxAckFul()
            allFields["PlaLoDDRBak7Ful"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_block_pro_stk._PlaLoDDRBak7Ful()
            allFields["PlaLoDDRBak6Ful"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_block_pro_stk._PlaLoDDRBak6Ful()
            allFields["PlaLoDDRBak5Ful"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_block_pro_stk._PlaLoDDRBak5Ful()
            allFields["PlaLoDDRBak4Ful"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_block_pro_stk._PlaLoDDRBak4Ful()
            allFields["PlaLoDDRBak3Ful"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_block_pro_stk._PlaLoDDRBak3Ful()
            allFields["PlaLoDDRBak2Ful"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_block_pro_stk._PlaLoDDRBak2Ful()
            allFields["PlaLoDDRBak1Ful"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_block_pro_stk._PlaLoDDRBak1Ful()
            allFields["PlaLoDDRBak0Ful"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_block_pro_stk._PlaLoDDRBak0Ful()
            allFields["PlaLoBlkOutVld"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_block_pro_stk._PlaLoBlkOutVld()
            allFields["PlaLoBlkCa256Same"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_block_pro_stk._PlaLoBlkCa256Same()
            allFields["PlaLoBlkCa128Same"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_block_pro_stk._PlaLoBlkCa128Same()
            allFields["PlaLoBlkCa64Same"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_block_pro_stk._PlaLoBlkCa64Same()
            allFields["PlaLoBlkPkInfoVld"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_block_pro_stk._PlaLoBlkPkInfoVld()
            allFields["PlaLoBlkCa256Emp"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_block_pro_stk._PlaLoBlkCa256Emp()
            allFields["PlaLoBlkCa128Emp"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_block_pro_stk._PlaLoBlkCa128Emp()
            allFields["PlaLoBlkCa64Emp"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_block_pro_stk._PlaLoBlkCa64Emp()
            allFields["PlaLoBlkHiBlkEmp"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_block_pro_stk._PlaLoBlkHiBlkEmp()
            allFields["PlaLoBlkLoBlkEmp"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_block_pro_stk._PlaLoBlkLoBlkEmp()
            allFields["PlaLoBlkHiBlkSame"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_block_pro_stk._PlaLoBlkHiBlkSame()
            allFields["PlaLoBlkLoBlkSame"] = _AF6CCI0011_RD_PLA_DEBUG._pla_lo_block_pro_stk._PlaLoBlkLoBlkSame()
            return allFields

    class _pla_interface_stk(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Interface Sticky"
    
        def description(self):
            return "This register is used to used to sticky some alarms for debug low-order block processing"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00088002
            
        def endAddress(self):
            return 0xffffffff

        class _PlaPort4QueRdy(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "PlaPort4QueRdy"
            
            def description(self):
                return "Port4 output Queue Pkt ready"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPort4QueAck(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "PlaPort4QueAck"
            
            def description(self):
                return "Port4 output Queue Pkt ack"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPort3QueRdy(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "PlaPort3QueRdy"
            
            def description(self):
                return "Port3 output Queue Pkt ready"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPort3QueAck(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "PlaPort3QueAck"
            
            def description(self):
                return "Port3 output Queue Pkt ack"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPort2QueRdy(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "PlaPort2QueRdy"
            
            def description(self):
                return "Port2 output Queue Pkt ready"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPort2QueAck(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "PlaPort2QueAck"
            
            def description(self):
                return "Port2 output Queue Pkt ack"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPort1QueRdy(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "PlaPort1QueRdy"
            
            def description(self):
                return "Port1 output Queue Pkt ready"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPort1QueAck(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "PlaPort1QueAck"
            
            def description(self):
                return "Port1 output Queue Pkt ack"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPSNReqQdr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "PlaPSNReqQdr"
            
            def description(self):
                return "Request PSN to QDR"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPSNAckQdr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "PlaPSNAckQdr"
            
            def description(self):
                return "Ack for PSN request"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPSNVldQdr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "PlaPSNVldQdr"
            
            def description(self):
                return "Valid for PSN request"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPkInfReqQdrLenWrErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "PlaPkInfReqQdrLenWrErr"
            
            def description(self):
                return "Request write pkt info len err"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPkInfReqQdr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "PlaPkInfReqQdr"
            
            def description(self):
                return "Request Pkt info to QDR"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPkInfAckQdr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "PlaPkInfAckQdr"
            
            def description(self):
                return "Ack for pkt info request"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPkInfVldQdr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PlaPkInfVldQdr"
            
            def description(self):
                return "Valid for pkt info request"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPkInfReqQdrLenRdErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "PlaPkInfReqQdrLenRdErr"
            
            def description(self):
                return "Request read pkt info len err"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaRdReqDdr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "PlaRdReqDdr"
            
            def description(self):
                return "Request read data to DDR"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaRdAckDdr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "PlaRdAckDdr"
            
            def description(self):
                return "Ack for read data request"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaRdVldDdr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PlaRdVldDdr"
            
            def description(self):
                return "Valid for read data request"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaP1PWEEoPErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "PlaP1PWEEoPErr"
            
            def description(self):
                return "PWE interface end packet err"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaWrReqDdr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "PlaWrReqDdr"
            
            def description(self):
                return "Request write data to DDR"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaWrAckDdr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "PlaWrAckDdr"
            
            def description(self):
                return "Ack for write data request"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaWrVldDdr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PlaWrVldDdr"
            
            def description(self):
                return "Valid for write data request"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaP4PWEReq(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "PlaP4PWEReq"
            
            def description(self):
                return "Port4 PLA ready packet for PWE"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaP4PWEGet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PlaP4PWEGet"
            
            def description(self):
                return "Port4 PWE get packet"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaP3PWEReq(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PlaP3PWEReq"
            
            def description(self):
                return "Port3 PLA ready packet for PWE"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaP3PWEGet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PlaP3PWEGet"
            
            def description(self):
                return "Port3 PWE get packet"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaP2PWEReq(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "PlaP2PWEReq"
            
            def description(self):
                return "Port2 PLA ready packet for PWE"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaP2PWEGet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PlaP2PWEGet"
            
            def description(self):
                return "Port2 PWE get packet"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaP1PWEReq(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PlaP1PWEReq"
            
            def description(self):
                return "Port1 PLA ready packet for PWE"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaP1PWEGet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaP1PWEGet"
            
            def description(self):
                return "Port1 PWE get packet"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaPort4QueRdy"] = _AF6CCI0011_RD_PLA_DEBUG._pla_interface_stk._PlaPort4QueRdy()
            allFields["PlaPort4QueAck"] = _AF6CCI0011_RD_PLA_DEBUG._pla_interface_stk._PlaPort4QueAck()
            allFields["PlaPort3QueRdy"] = _AF6CCI0011_RD_PLA_DEBUG._pla_interface_stk._PlaPort3QueRdy()
            allFields["PlaPort3QueAck"] = _AF6CCI0011_RD_PLA_DEBUG._pla_interface_stk._PlaPort3QueAck()
            allFields["PlaPort2QueRdy"] = _AF6CCI0011_RD_PLA_DEBUG._pla_interface_stk._PlaPort2QueRdy()
            allFields["PlaPort2QueAck"] = _AF6CCI0011_RD_PLA_DEBUG._pla_interface_stk._PlaPort2QueAck()
            allFields["PlaPort1QueRdy"] = _AF6CCI0011_RD_PLA_DEBUG._pla_interface_stk._PlaPort1QueRdy()
            allFields["PlaPort1QueAck"] = _AF6CCI0011_RD_PLA_DEBUG._pla_interface_stk._PlaPort1QueAck()
            allFields["PlaPSNReqQdr"] = _AF6CCI0011_RD_PLA_DEBUG._pla_interface_stk._PlaPSNReqQdr()
            allFields["PlaPSNAckQdr"] = _AF6CCI0011_RD_PLA_DEBUG._pla_interface_stk._PlaPSNAckQdr()
            allFields["PlaPSNVldQdr"] = _AF6CCI0011_RD_PLA_DEBUG._pla_interface_stk._PlaPSNVldQdr()
            allFields["PlaPkInfReqQdrLenWrErr"] = _AF6CCI0011_RD_PLA_DEBUG._pla_interface_stk._PlaPkInfReqQdrLenWrErr()
            allFields["PlaPkInfReqQdr"] = _AF6CCI0011_RD_PLA_DEBUG._pla_interface_stk._PlaPkInfReqQdr()
            allFields["PlaPkInfAckQdr"] = _AF6CCI0011_RD_PLA_DEBUG._pla_interface_stk._PlaPkInfAckQdr()
            allFields["PlaPkInfVldQdr"] = _AF6CCI0011_RD_PLA_DEBUG._pla_interface_stk._PlaPkInfVldQdr()
            allFields["PlaPkInfReqQdrLenRdErr"] = _AF6CCI0011_RD_PLA_DEBUG._pla_interface_stk._PlaPkInfReqQdrLenRdErr()
            allFields["PlaRdReqDdr"] = _AF6CCI0011_RD_PLA_DEBUG._pla_interface_stk._PlaRdReqDdr()
            allFields["PlaRdAckDdr"] = _AF6CCI0011_RD_PLA_DEBUG._pla_interface_stk._PlaRdAckDdr()
            allFields["PlaRdVldDdr"] = _AF6CCI0011_RD_PLA_DEBUG._pla_interface_stk._PlaRdVldDdr()
            allFields["PlaP1PWEEoPErr"] = _AF6CCI0011_RD_PLA_DEBUG._pla_interface_stk._PlaP1PWEEoPErr()
            allFields["PlaWrReqDdr"] = _AF6CCI0011_RD_PLA_DEBUG._pla_interface_stk._PlaWrReqDdr()
            allFields["PlaWrAckDdr"] = _AF6CCI0011_RD_PLA_DEBUG._pla_interface_stk._PlaWrAckDdr()
            allFields["PlaWrVldDdr"] = _AF6CCI0011_RD_PLA_DEBUG._pla_interface_stk._PlaWrVldDdr()
            allFields["PlaP4PWEReq"] = _AF6CCI0011_RD_PLA_DEBUG._pla_interface_stk._PlaP4PWEReq()
            allFields["PlaP4PWEGet"] = _AF6CCI0011_RD_PLA_DEBUG._pla_interface_stk._PlaP4PWEGet()
            allFields["PlaP3PWEReq"] = _AF6CCI0011_RD_PLA_DEBUG._pla_interface_stk._PlaP3PWEReq()
            allFields["PlaP3PWEGet"] = _AF6CCI0011_RD_PLA_DEBUG._pla_interface_stk._PlaP3PWEGet()
            allFields["PlaP2PWEReq"] = _AF6CCI0011_RD_PLA_DEBUG._pla_interface_stk._PlaP2PWEReq()
            allFields["PlaP2PWEGet"] = _AF6CCI0011_RD_PLA_DEBUG._pla_interface_stk._PlaP2PWEGet()
            allFields["PlaP1PWEReq"] = _AF6CCI0011_RD_PLA_DEBUG._pla_interface_stk._PlaP1PWEReq()
            allFields["PlaP1PWEGet"] = _AF6CCI0011_RD_PLA_DEBUG._pla_interface_stk._PlaP1PWEGet()
            return allFields

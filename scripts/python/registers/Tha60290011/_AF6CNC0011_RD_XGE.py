import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0011_RD_XGE(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["version_pen"] = _AF6CNC0011_RD_XGE._version_pen()
        allRegisters["version_pen"] = _AF6CNC0011_RD_XGE._version_pen()
        allRegisters["Master_pen"] = _AF6CNC0011_RD_XGE._Master_pen()
        allRegisters["Card_Status"] = _AF6CNC0011_RD_XGE._Card_Status()
        allRegisters["Configure_register"] = _AF6CNC0011_RD_XGE._Configure_register()
        allRegisters["Configure_register"] = _AF6CNC0011_RD_XGE._Configure_register()
        allRegisters["Configure_register"] = _AF6CNC0011_RD_XGE._Configure_register()
        allRegisters["Configure_register"] = _AF6CNC0011_RD_XGE._Configure_register()
        allRegisters["Status_register"] = _AF6CNC0011_RD_XGE._Status_register()
        allRegisters["Status_register"] = _AF6CNC0011_RD_XGE._Status_register()
        allRegisters["Configure_register"] = _AF6CNC0011_RD_XGE._Configure_register()
        allRegisters["Configure_register"] = _AF6CNC0011_RD_XGE._Configure_register()
        allRegisters["Configure_register"] = _AF6CNC0011_RD_XGE._Configure_register()
        allRegisters["Status_register"] = _AF6CNC0011_RD_XGE._Status_register()
        allRegisters["Status_register"] = _AF6CNC0011_RD_XGE._Status_register()
        allRegisters["Status_register"] = _AF6CNC0011_RD_XGE._Status_register()
        allRegisters["Status_register"] = _AF6CNC0011_RD_XGE._Status_register()
        allRegisters["Status_register"] = _AF6CNC0011_RD_XGE._Status_register()
        allRegisters["Status_register"] = _AF6CNC0011_RD_XGE._Status_register()
        allRegisters["Status_register"] = _AF6CNC0011_RD_XGE._Status_register()
        allRegisters["Status_register"] = _AF6CNC0011_RD_XGE._Status_register()
        allRegisters["Status_register"] = _AF6CNC0011_RD_XGE._Status_register()
        allRegisters["Status_register"] = _AF6CNC0011_RD_XGE._Status_register()
        allRegisters["Status_register"] = _AF6CNC0011_RD_XGE._Status_register()
        allRegisters["Status_register"] = _AF6CNC0011_RD_XGE._Status_register()
        allRegisters["Status_register"] = _AF6CNC0011_RD_XGE._Status_register()
        allRegisters["Status_register"] = _AF6CNC0011_RD_XGE._Status_register()
        allRegisters["Status_register"] = _AF6CNC0011_RD_XGE._Status_register()
        allRegisters["Status_register"] = _AF6CNC0011_RD_XGE._Status_register()
        allRegisters["Status_register"] = _AF6CNC0011_RD_XGE._Status_register()
        allRegisters["Status_register"] = _AF6CNC0011_RD_XGE._Status_register()
        allRegisters["Status_register"] = _AF6CNC0011_RD_XGE._Status_register()
        allRegisters["Status_register"] = _AF6CNC0011_RD_XGE._Status_register()
        allRegisters["Status_register"] = _AF6CNC0011_RD_XGE._Status_register()
        allRegisters["Status_register"] = _AF6CNC0011_RD_XGE._Status_register()
        return allRegisters

    class _version_pen(AtRegister.AtRegister):
        def name(self):
            return "FPGA version"
    
        def description(self):
            return "Register to test CPU"
            
        def width(self):
            return 32
        
        def type(self):
            return "Version"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00f00000
            
        def endAddress(self):
            return 0xffffffff

        class _Year(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "Year"
            
            def description(self):
                return "Year"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Month(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Month"
            
            def description(self):
                return "Month"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Day(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Day"
            
            def description(self):
                return "Day"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Version(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Version"
            
            def description(self):
                return "Version"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Year"] = _AF6CNC0011_RD_XGE._version_pen._Year()
            allFields["Month"] = _AF6CNC0011_RD_XGE._version_pen._Month()
            allFields["Day"] = _AF6CNC0011_RD_XGE._version_pen._Day()
            allFields["Version"] = _AF6CNC0011_RD_XGE._version_pen._Version()
            return allFields

    class _version_pen(AtRegister.AtRegister):
        def name(self):
            return "FPGA name"
    
        def description(self):
            return "Register to test CPU"
            
        def width(self):
            return 32
        
        def type(self):
            return "Version"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00f00001
            
        def endAddress(self):
            return 0xffffffff

        class _PRODUCT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PRODUCT"
            
            def description(self):
                return "Product code"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PRODUCT"] = _AF6CNC0011_RD_XGE._version_pen._PRODUCT()
            return allFields

    class _Master_pen(AtRegister.AtRegister):
        def name(self):
            return "FPGA soft reset"
    
        def description(self):
            return "Register to test CPU"
            
        def width(self):
            return 32
        
        def type(self):
            return " Write 1 to reset, write 0 to unreset"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00f00004
            
        def endAddress(self):
            return 0xffffffff

        class _Soft_reset(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Soft_reset"
            
            def description(self):
                return "FPGA soft reset internal"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Soft_reset"] = _AF6CNC0011_RD_XGE._Master_pen._Soft_reset()
            return allFields

    class _Card_Status(AtRegister.AtRegister):
        def name(self):
            return "FPGA system alarm"
    
        def description(self):
            return "Register to test FPGA"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00f00052
            
        def endAddress(self):
            return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _XFI_PCS_Block_Lock(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "XFI_PCS_Block_Lock"
            
            def description(self):
                return "PCS Block status locked for port 8 <1 = RX is synchronized  / 0 = RX is not synchronized>"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _XFI_PCS_Block_Lock(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "XFI_PCS_Block_Lock"
            
            def description(self):
                return "PCS Block status locked for port 7 <1 = RX is synchronized  / 0 = RX is not synchronized>"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _XFI_PCS_Block_Lock(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "XFI_PCS_Block_Lock"
            
            def description(self):
                return "PCS Block status locked for port 6 <1 = RX is synchronized  / 0 = RX is not synchronized>"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _XFI_PCS_Block_Lock(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "XFI_PCS_Block_Lock"
            
            def description(self):
                return "PCS Block status locked for port 5 <1 = RX is synchronized  / 0 = RX is not synchronized>"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _XFI_PCS_Block_Lock(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "XFI_PCS_Block_Lock"
            
            def description(self):
                return "PCS Block status locked for port 4 <1 = RX is synchronized  / 0 = RX is not synchronized>"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _XFI_PCS_Block_Lock(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "XFI_PCS_Block_Lock"
            
            def description(self):
                return "PCS Block status locked for port 3 <1 = RX is synchronized  / 0 = RX is not synchronized>"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _XFI_PCS_Block_Lock(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "XFI_PCS_Block_Lock"
            
            def description(self):
                return "PCS Block status locked for port 2 <1 = RX is synchronized  / 0 = RX is not synchronized>"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _XFI_PCS_Block_Lock(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "XFI_PCS_Block_Lock"
            
            def description(self):
                return "PCS Block status locked for port 1 <1 = RX is synchronized  / 0 = RX is not synchronized>"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _ser_init_done(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "ser_init_done"
            
            def description(self):
                return "OCN Serdes init done"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _mig1_2x64_init_calib_complete(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "mig1_2x64_init_calib_complete"
            
            def description(self):
                return "DDR4#1 calib_complete"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _mig2_2x64_init_calib_complete(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "mig2_2x64_init_calib_complete"
            
            def description(self):
                return "DDR4#2 calib_complete"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _mig2_2x36_init_calib_complete(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "mig2_2x36_init_calib_complete"
            
            def description(self):
                return "QDRII calib_complete"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _syspll_locked(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "syspll_locked"
            
            def description(self):
                return "System PLL locked status"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Reserved"] = _AF6CNC0011_RD_XGE._Card_Status._Reserved()
            allFields["XFI_PCS_Block_Lock"] = _AF6CNC0011_RD_XGE._Card_Status._XFI_PCS_Block_Lock()
            allFields["XFI_PCS_Block_Lock"] = _AF6CNC0011_RD_XGE._Card_Status._XFI_PCS_Block_Lock()
            allFields["XFI_PCS_Block_Lock"] = _AF6CNC0011_RD_XGE._Card_Status._XFI_PCS_Block_Lock()
            allFields["XFI_PCS_Block_Lock"] = _AF6CNC0011_RD_XGE._Card_Status._XFI_PCS_Block_Lock()
            allFields["XFI_PCS_Block_Lock"] = _AF6CNC0011_RD_XGE._Card_Status._XFI_PCS_Block_Lock()
            allFields["XFI_PCS_Block_Lock"] = _AF6CNC0011_RD_XGE._Card_Status._XFI_PCS_Block_Lock()
            allFields["XFI_PCS_Block_Lock"] = _AF6CNC0011_RD_XGE._Card_Status._XFI_PCS_Block_Lock()
            allFields["XFI_PCS_Block_Lock"] = _AF6CNC0011_RD_XGE._Card_Status._XFI_PCS_Block_Lock()
            allFields["Reserved"] = _AF6CNC0011_RD_XGE._Card_Status._Reserved()
            allFields["ser_init_done"] = _AF6CNC0011_RD_XGE._Card_Status._ser_init_done()
            allFields["mig1_2x64_init_calib_complete"] = _AF6CNC0011_RD_XGE._Card_Status._mig1_2x64_init_calib_complete()
            allFields["mig2_2x64_init_calib_complete"] = _AF6CNC0011_RD_XGE._Card_Status._mig2_2x64_init_calib_complete()
            allFields["mig2_2x36_init_calib_complete"] = _AF6CNC0011_RD_XGE._Card_Status._mig2_2x36_init_calib_complete()
            allFields["syspll_locked"] = _AF6CNC0011_RD_XGE._Card_Status._syspll_locked()
            return allFields

    class _Configure_register(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Pattern Generate Configure"
    
        def description(self):
            return "XFI PRBS Pattern Generate Configure"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "Base address + 0x0 Base address : 0xF50000"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _cfgdainc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfgdainc"
            
            def description(self):
                return "Configure DA Address mode 1 : INC / 0: fixed"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfglengthinc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfglengthinc"
            
            def description(self):
                return "Configure length mode 1 : INC / 0: fixed"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgnumall(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "cfgnumall"
            
            def description(self):
                return "Configure Packet number mode 1 : continuity   / 0: fixed"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgins_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "cfgins_err"
            
            def description(self):
                return "Configure Packet Insert Error 1 : Insert / 0: Un-insert"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgforcsop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "cfgforcsop"
            
            def description(self):
                return "Configure Force Start of packet 1 : Force / 0: Un-Force"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgforceop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "cfgforceop"
            
            def description(self):
                return "Configure Force End of packet 1 : Force / 0: Un-Force"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfggenen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "cfggenen"
            
            def description(self):
                return "Configure PRBS packet test enable 1 : Enable / 0: Disnable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _flush(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "flush"
            
            def description(self):
                return "Configure flush PRBS packet gen  1 : flush / 0: Un-flush"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfgdainc"] = _AF6CNC0011_RD_XGE._Configure_register._cfgdainc()
            allFields["cfglengthinc"] = _AF6CNC0011_RD_XGE._Configure_register._cfglengthinc()
            allFields["cfgnumall"] = _AF6CNC0011_RD_XGE._Configure_register._cfgnumall()
            allFields["cfgins_err"] = _AF6CNC0011_RD_XGE._Configure_register._cfgins_err()
            allFields["cfgforcsop"] = _AF6CNC0011_RD_XGE._Configure_register._cfgforcsop()
            allFields["cfgforceop"] = _AF6CNC0011_RD_XGE._Configure_register._cfgforceop()
            allFields["Reserved"] = _AF6CNC0011_RD_XGE._Configure_register._Reserved()
            allFields["cfggenen"] = _AF6CNC0011_RD_XGE._Configure_register._cfggenen()
            allFields["flush"] = _AF6CNC0011_RD_XGE._Configure_register._flush()
            return allFields

    class _Configure_register(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Pattern Packet Number Configure"
    
        def description(self):
            return "XFI PRBS Pattern Packet Number Configure"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "Base address + 0x1 Base address : 0xF50000"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _cfgnumpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfgnumpkt"
            
            def description(self):
                return "Configure number of packet generate"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfgnumpkt"] = _AF6CNC0011_RD_XGE._Configure_register._cfgnumpkt()
            return allFields

    class _Configure_register(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Pattern Packet lengthg Configure"
    
        def description(self):
            return "XFI PRBS Pattern Packet lengthg Configure"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "Base address + 0x2 Base address : 0xF50000"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _cfglengthmin(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfglengthmin"
            
            def description(self):
                return "Configure length minimum of packet generate"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfglengthmax(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "cfglengthmax"
            
            def description(self):
                return "Configure length maximum of packet generate"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfglengthmin"] = _AF6CNC0011_RD_XGE._Configure_register._cfglengthmin()
            allFields["cfglengthmax"] = _AF6CNC0011_RD_XGE._Configure_register._cfglengthmax()
            return allFields

    class _Configure_register(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Pattern Packet Data Configure"
    
        def description(self):
            return "XFI PRBS Pattern Packet Data Configure"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "Base address + 0x3 Base address : 0xF50000"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _cfgdatfix(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfgdatfix"
            
            def description(self):
                return "Configure Fixed byte Data generate"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgdatmod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "cfgdatmod"
            
            def description(self):
                return "Configure data mode generate  0x1 fixed data / 0x2 : PRBS31 data"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfgdatfix"] = _AF6CNC0011_RD_XGE._Configure_register._cfgdatfix()
            allFields["cfgdatmod"] = _AF6CNC0011_RD_XGE._Configure_register._cfgdatmod()
            return allFields

    class _Status_register(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Pattern Packet generate Counter RO"
    
        def description(self):
            return "XFI PRBS Pattern Packet generate Counter RO"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "Base address + 0x10 Base address : 0xF50000"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _penr_opkttotal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "penr_opkttotal"
            
            def description(self):
                return "TX Packet Counterer"
            
            def type(self):
                return "R/O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["penr_opkttotal"] = _AF6CNC0011_RD_XGE._Status_register._penr_opkttotal()
            return allFields

    class _Status_register(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Pattern Packet generate Counter R2C"
    
        def description(self):
            return "XFI PRBS Pattern Packet generate Counter R2C"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "Base address + 0x20 Base address : 0xF50000"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _penr2cpkttotal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "penr2cpkttotal"
            
            def description(self):
                return "TX Packet Counterer"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["penr2cpkttotal"] = _AF6CNC0011_RD_XGE._Status_register._penr2cpkttotal()
            return allFields

    class _Configure_register(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Pattern Packet analyzer PRBS Sticky Error"
    
        def description(self):
            return "XFI PRBS Pattern Packet analyzer PRBS Sticky Error"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "Base address + 0xB00 Base address : 0xF50000"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _err_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "err_stk"
            
            def description(self):
                return "Packet analyzer PRBS Sticky Error <0 = RX is synchronized /1 = RX is not synchronized>"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["err_stk"] = _AF6CNC0011_RD_XGE._Configure_register._err_stk()
            return allFields

    class _Configure_register(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Pattern Packet analyzer PRBS Status Error"
    
        def description(self):
            return "XFI PRBS Pattern Packet analyzer PRBS Status Error"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "Base address + 0xB01 Base address : 0xF50000"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _err_sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "err_sta"
            
            def description(self):
                return "Packet analyzer PRBS Status Error <0 = RX is synchronized /1 = RX is not synchronized>"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["err_sta"] = _AF6CNC0011_RD_XGE._Configure_register._err_sta()
            return allFields

    class _Configure_register(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Pattern Packet analyzer Data Configure"
    
        def description(self):
            return "XFI PRBS Pattern Packet analyzer Data Configure"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "Base address + 0xA01 Base address : 0xF50000"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _cfgfcsdrop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "cfgfcsdrop"
            
            def description(self):
                return "Configure FCS Drop"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgdatmod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "cfgdatmod"
            
            def description(self):
                return "Configure data mode analyzer  0x1 fixed data / 0x2 : PRBS31 data"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfgfcsdrop"] = _AF6CNC0011_RD_XGE._Configure_register._cfgfcsdrop()
            allFields["cfgdatmod"] = _AF6CNC0011_RD_XGE._Configure_register._cfgdatmod()
            return allFields

    class _Status_register(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkttotal"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkttotal"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "Base address + 0x800 Base address : 0xF50000"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _pkttotal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pkttotal"
            
            def description(self):
                return "RX Packet total"
            
            def type(self):
                return "R/O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pkttotal"] = _AF6CNC0011_RD_XGE._Status_register._pkttotal()
            return allFields

    class _Status_register(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pktgood"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pktgood"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "Base address + 0x801 Base address : 0xF50000"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _pktgood(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktgood"
            
            def description(self):
                return "RX Packet good"
            
            def type(self):
                return "R/O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktgood"] = _AF6CNC0011_RD_XGE._Status_register._pktgood()
            return allFields

    class _Status_register(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pktfcserr"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pktfcserr"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "Base address + 0x802 Base address : 0xF50000"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _pktfcserr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktfcserr"
            
            def description(self):
                return "RX Packet FCS error"
            
            def type(self):
                return "R/O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktfcserr"] = _AF6CNC0011_RD_XGE._Status_register._pktfcserr()
            return allFields

    class _Status_register(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkt length err"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkt length err"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "Base address + 0x803 Base address : 0xF50000"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _pktlengtherr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktlengtherr"
            
            def description(self):
                return "RX Packet lengthgth error"
            
            def type(self):
                return "R/O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktlengtherr"] = _AF6CNC0011_RD_XGE._Status_register._pktlengtherr()
            return allFields

    class _Status_register(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkt PRBS Data err"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkt PRBS Data err"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "Base address + 0x807 Base address : 0xF50000"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _pktdaterr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktdaterr"
            
            def description(self):
                return "RX Packet PRBS data error"
            
            def type(self):
                return "R/O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktdaterr"] = _AF6CNC0011_RD_XGE._Status_register._pktdaterr()
            return allFields

    class _Status_register(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkt length 0-64"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkt length 0-64"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "Base address + 0x80A Base address : 0xF50000"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _pktlen64(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktlen64"
            
            def description(self):
                return "RX Counter pkt length 0-64"
            
            def type(self):
                return "R/O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktlen64"] = _AF6CNC0011_RD_XGE._Status_register._pktlen64()
            return allFields

    class _Status_register(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkt length 65-128"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkt length 65-128"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "Base address + 0x80B Base address : 0xF50000"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _pktlen128(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktlen128"
            
            def description(self):
                return "RX Counter pkt length 65-128"
            
            def type(self):
                return "R/O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktlen128"] = _AF6CNC0011_RD_XGE._Status_register._pktlen128()
            return allFields

    class _Status_register(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkt length 129-256"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkt length 129-256"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "Base address + 0x80C Base address : 0xF50000"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _pktlen256(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktlen256"
            
            def description(self):
                return "RX Counter pkt length 129-256"
            
            def type(self):
                return "R/O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktlen256"] = _AF6CNC0011_RD_XGE._Status_register._pktlen256()
            return allFields

    class _Status_register(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkt length 257-512"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkt length 257-512"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "Base address + 0x80D Base address : 0xF50000"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _pktlen512(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktlen512"
            
            def description(self):
                return "RX Counter pkt length 257-215"
            
            def type(self):
                return "R/O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktlen512"] = _AF6CNC0011_RD_XGE._Status_register._pktlen512()
            return allFields

    class _Status_register(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkt length 513-1024"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkt length 513-1024"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "Base address + 0x80E Base address : 0xF50000"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _pktlen1024(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktlen1024"
            
            def description(self):
                return "RX Counter pkt length 513-1024"
            
            def type(self):
                return "R/O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktlen1024"] = _AF6CNC0011_RD_XGE._Status_register._pktlen1024()
            return allFields

    class _Status_register(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkt length 1025-2048"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkt length 1025-2048"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "Base address + 0x80F Base address : 0xF50000"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _pktlen2048(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktlen2048"
            
            def description(self):
                return "RX Counter pkt length 1025-2048"
            
            def type(self):
                return "R/O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktlen2048"] = _AF6CNC0011_RD_XGE._Status_register._pktlen2048()
            return allFields

    class _Status_register(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkttotal"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkttotal"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "Base address + 0x900 Base address : 0xF50000"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _pkttotal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pkttotal"
            
            def description(self):
                return "RX Packet total"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pkttotal"] = _AF6CNC0011_RD_XGE._Status_register._pkttotal()
            return allFields

    class _Status_register(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pktgood"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pktgood"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "Base address + 0x901 Base address : 0xF50000"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _pktgood(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktgood"
            
            def description(self):
                return "RX Packet good"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktgood"] = _AF6CNC0011_RD_XGE._Status_register._pktgood()
            return allFields

    class _Status_register(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pktfcserr"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pktfcserr"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "Base address + 0x902 Base address : 0xF50000"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _pktfcserr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktfcserr"
            
            def description(self):
                return "RX Packet FCS error"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktfcserr"] = _AF6CNC0011_RD_XGE._Status_register._pktfcserr()
            return allFields

    class _Status_register(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkt length err"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkt length err"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "Base address + 0x903 Base address : 0xF50000"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _pktlengtherr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktlengtherr"
            
            def description(self):
                return "RX Packet lengthgth error"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktlengtherr"] = _AF6CNC0011_RD_XGE._Status_register._pktlengtherr()
            return allFields

    class _Status_register(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkt PRBS Data err"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkt PRBS Data err"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "Base address + 0x907 Base address : 0xF50000"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _pktdaterr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktdaterr"
            
            def description(self):
                return "RX Packet PRBS data error"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktdaterr"] = _AF6CNC0011_RD_XGE._Status_register._pktdaterr()
            return allFields

    class _Status_register(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkt length 0-64"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkt length 0-64"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "Base address + 0x90A Base address : 0xF50000"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _pktlen64(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktlen64"
            
            def description(self):
                return "RX Counter pkt length 0-64"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktlen64"] = _AF6CNC0011_RD_XGE._Status_register._pktlen64()
            return allFields

    class _Status_register(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkt length 65-128"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkt length 65-128"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "Base address + 0x90B Base address : 0xF50000"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _pktlen128(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktlen128"
            
            def description(self):
                return "RX Counter pkt length 65-128"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktlen128"] = _AF6CNC0011_RD_XGE._Status_register._pktlen128()
            return allFields

    class _Status_register(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkt length 129-256"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkt length 129-256"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "Base address + 0x90C Base address : 0xF50000"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _pktlen256(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktlen256"
            
            def description(self):
                return "RX Counter pkt length 129-256"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktlen256"] = _AF6CNC0011_RD_XGE._Status_register._pktlen256()
            return allFields

    class _Status_register(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkt length 257-512"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkt length 257-512"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "Base address + 0x90D Base address : 0xF50000"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _pktlen512(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktlen512"
            
            def description(self):
                return "RX Counter pkt length 257-215"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktlen512"] = _AF6CNC0011_RD_XGE._Status_register._pktlen512()
            return allFields

    class _Status_register(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkt length 513-1024"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkt length 513-1024"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "Base address + 0x90E Base address : 0xF50000"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _pktlen1024(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktlen1024"
            
            def description(self):
                return "RX Counter pkt length 513-1024"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktlen1024"] = _AF6CNC0011_RD_XGE._Status_register._pktlen1024()
            return allFields

    class _Status_register(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkt length 1025-2048"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkt length 1025-2048"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "Base address + 0x90F Base address : 0xF50000"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _pktlen2048(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktlen2048"
            
            def description(self):
                return "RX Counter pkt length 1025-2048"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktlen2048"] = _AF6CNC0011_RD_XGE._Status_register._pktlen2048()
            return allFields

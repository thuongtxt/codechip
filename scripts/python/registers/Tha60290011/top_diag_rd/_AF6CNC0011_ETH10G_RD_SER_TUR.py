import AtRegister

class _AF6CNC0011_ETH10G_RD_SER_TUR(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["OETH_10G_DRP"] = _AF6CNC0011_ETH10G_RD_SER_TUR._OETH_10G_DRP()
        allRegisters["ETH_10G_LoopBack"] = _AF6CNC0011_ETH10G_RD_SER_TUR._ETH_10G_LoopBack()
        allRegisters["ETH_10G_TX_Reset"] = _AF6CNC0011_ETH10G_RD_SER_TUR._ETH_10G_TX_Reset()
        allRegisters["ETH_49G_RX_Reset"] = _AF6CNC0011_ETH10G_RD_SER_TUR._ETH_49G_RX_Reset()
        allRegisters["ETH_10G_LPMDFE_Mode"] = _AF6CNC0011_ETH10G_RD_SER_TUR._ETH_10G_LPMDFE_Mode()
        allRegisters["ETH_10G_LPMDFE_Reset"] = _AF6CNC0011_ETH10G_RD_SER_TUR._ETH_10G_LPMDFE_Reset()
        allRegisters["ETH_10G_TXDIFFCTRL"] = _AF6CNC0011_ETH10G_RD_SER_TUR._ETH_10G_TXDIFFCTRL()
        allRegisters["ETH_10G_TXPOSTCURSOR"] = _AF6CNC0011_ETH10G_RD_SER_TUR._ETH_10G_TXPOSTCURSOR()
        allRegisters["ETH_10G_TXPRECURSOR"] = _AF6CNC0011_ETH10G_RD_SER_TUR._ETH_10G_TXPRECURSOR()
        allRegisters["ETH_10G_Ctrl_FCS"] = _AF6CNC0011_ETH10G_RD_SER_TUR._ETH_10G_Ctrl_FCS()
        allRegisters["ETH_10G_AutoNeg"] = _AF6CNC0011_ETH10G_RD_SER_TUR._ETH_10G_AutoNeg()
        return allRegisters

    class _OETH_10G_DRP(AtRegister.AtRegister):
        def name(self):
            return "ETH 10G DRP"
    
        def description(self):
            return "Read/Write DRP address of SERDES"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x400+$DRP"
            
        def startAddress(self):
            return 0x00000400
            
        def endAddress(self):
            return 0x000007ff

        class _drp_rw(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "drp_rw"
            
            def description(self):
                return "DRP read/write value"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["drp_rw"] = _AF6CNC0011_ETH10G_RD_SER_TUR._OETH_10G_DRP._drp_rw()
            return allFields

    class _ETH_10G_LoopBack(AtRegister.AtRegister):
        def name(self):
            return "ETH 10G LoopBack"
    
        def description(self):
            return "Configurate LoopBack"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0xffffffff

        class _lpback_lane0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lpback_lane0"
            
            def description(self):
                return "Loopback"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lpback_lane0"] = _AF6CNC0011_ETH10G_RD_SER_TUR._ETH_10G_LoopBack._lpback_lane0()
            return allFields

    class _ETH_10G_TX_Reset(AtRegister.AtRegister):
        def name(self):
            return "ETH 10G TX Reset"
    
        def description(self):
            return "Reset TX SERDES"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000c
            
        def endAddress(self):
            return 0xffffffff

        class _txrst_done(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "txrst_done"
            
            def description(self):
                return "TX Reset Done"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _txrst_trig(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txrst_trig"
            
            def description(self):
                return "Trige 0->1 to start reset TX SERDES"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txrst_done"] = _AF6CNC0011_ETH10G_RD_SER_TUR._ETH_10G_TX_Reset._txrst_done()
            allFields["txrst_trig"] = _AF6CNC0011_ETH10G_RD_SER_TUR._ETH_10G_TX_Reset._txrst_trig()
            return allFields

    class _ETH_49G_RX_Reset(AtRegister.AtRegister):
        def name(self):
            return "ETH 10G RX Reset"
    
        def description(self):
            return "Reset RX SERDES"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000d
            
        def endAddress(self):
            return 0xffffffff

        class _rxrst_done(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "rxrst_done"
            
            def description(self):
                return "RX Reset Done"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _rxrst_trig(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxrst_trig"
            
            def description(self):
                return "Trige 0->1 to start reset RX SERDES"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxrst_done"] = _AF6CNC0011_ETH10G_RD_SER_TUR._ETH_49G_RX_Reset._rxrst_done()
            allFields["rxrst_trig"] = _AF6CNC0011_ETH10G_RD_SER_TUR._ETH_49G_RX_Reset._rxrst_trig()
            return allFields

    class _ETH_10G_LPMDFE_Mode(AtRegister.AtRegister):
        def name(self):
            return "ETH 10G LPMDFE Mode"
    
        def description(self):
            return "Configure LPM/DFE mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000e
            
        def endAddress(self):
            return 0xffffffff

        class _lpmdfe_mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lpmdfe_mode"
            
            def description(self):
                return "LPM/DFE mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lpmdfe_mode"] = _AF6CNC0011_ETH10G_RD_SER_TUR._ETH_10G_LPMDFE_Mode._lpmdfe_mode()
            return allFields

    class _ETH_10G_LPMDFE_Reset(AtRegister.AtRegister):
        def name(self):
            return "ETH 10G LPMDFE Reset"
    
        def description(self):
            return "Reset LPM/DFE"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000f
            
        def endAddress(self):
            return 0xffffffff

        class _lpmdfe_reset(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lpmdfe_reset"
            
            def description(self):
                return "LPM/DFE reset"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lpmdfe_reset"] = _AF6CNC0011_ETH10G_RD_SER_TUR._ETH_10G_LPMDFE_Reset._lpmdfe_reset()
            return allFields

    class _ETH_10G_TXDIFFCTRL(AtRegister.AtRegister):
        def name(self):
            return "ETH 10G TXDIFFCTRL"
    
        def description(self):
            return "Driver Swing Control, see \"Table 3-35: TX Configurable Driver Ports\" page 158 of UG578 for more detail"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000010
            
        def endAddress(self):
            return 0xffffffff

        class _TXDIFFCTRL(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TXDIFFCTRL"
            
            def description(self):
                return "TXDIFFCTRL"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TXDIFFCTRL"] = _AF6CNC0011_ETH10G_RD_SER_TUR._ETH_10G_TXDIFFCTRL._TXDIFFCTRL()
            return allFields

    class _ETH_10G_TXPOSTCURSOR(AtRegister.AtRegister):
        def name(self):
            return "ETH 10G TXPOSTCURSOR"
    
        def description(self):
            return "Transmitter post-cursor TX pre-emphasis control, see \"Table 3-35: TX Configurable Driver Ports\" page 160 of UG578 for more detail"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000011
            
        def endAddress(self):
            return 0xffffffff

        class _TXPOSTCURSOR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TXPOSTCURSOR"
            
            def description(self):
                return "TXPOSTCURSOR"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TXPOSTCURSOR"] = _AF6CNC0011_ETH10G_RD_SER_TUR._ETH_10G_TXPOSTCURSOR._TXPOSTCURSOR()
            return allFields

    class _ETH_10G_TXPRECURSOR(AtRegister.AtRegister):
        def name(self):
            return "ETH 10G TXPRECURSOR"
    
        def description(self):
            return "Transmitter pre-cursor TX pre-emphasis control, see \"Table 3-35: TX Configurable Driver Ports\" page 161 of UG578 for more detail"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000012
            
        def endAddress(self):
            return 0xffffffff

        class _TXPRECURSOR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TXPRECURSOR"
            
            def description(self):
                return "TXPRECURSOR"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TXPRECURSOR"] = _AF6CNC0011_ETH10G_RD_SER_TUR._ETH_10G_TXPRECURSOR._TXPRECURSOR()
            return allFields

    class _ETH_10G_Ctrl_FCS(AtRegister.AtRegister):
        def name(self):
            return "ETH 10G Ctrl FCS"
    
        def description(self):
            return "configure FCS mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000080
            
        def endAddress(self):
            return 0xffffffff

        class _txfcs_ignore(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "txfcs_ignore"
            
            def description(self):
                return "TX ignore check FCS when txfcs_ins is low"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _txfcs_ins(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "txfcs_ins"
            
            def description(self):
                return "TX inserts 4bytes FCS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _rxfcs_ignore(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "rxfcs_ignore"
            
            def description(self):
                return "RX ignore check FCS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _rxfcs_rmv(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxfcs_rmv"
            
            def description(self):
                return "RX remove 4bytes FCS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txfcs_ignore"] = _AF6CNC0011_ETH10G_RD_SER_TUR._ETH_10G_Ctrl_FCS._txfcs_ignore()
            allFields["txfcs_ins"] = _AF6CNC0011_ETH10G_RD_SER_TUR._ETH_10G_Ctrl_FCS._txfcs_ins()
            allFields["rxfcs_ignore"] = _AF6CNC0011_ETH10G_RD_SER_TUR._ETH_10G_Ctrl_FCS._rxfcs_ignore()
            allFields["rxfcs_rmv"] = _AF6CNC0011_ETH10G_RD_SER_TUR._ETH_10G_Ctrl_FCS._rxfcs_rmv()
            return allFields

    class _ETH_10G_AutoNeg(AtRegister.AtRegister):
        def name(self):
            return "ETH 10G AutoNeg"
    
        def description(self):
            return "configure Auto-Neg"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000081
            
        def endAddress(self):
            return 0xffffffff

        class _an_lt_sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "an_lt_sta"
            
            def description(self):
                return "Link Control outputs from the auto-negotiationcontroller for the various Ethernet protocols."
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lt_restart(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "lt_restart"
            
            def description(self):
                return "This signal triggers a restart of link training regardless of the current state."
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lt_enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "lt_enb"
            
            def description(self):
                return "Enables link training. When link training is disabled, all PCS lanes function in mission mode."
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _an_restart(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "an_restart"
            
            def description(self):
                return "This input is used to trigger a restart of the auto-negotiation, regardless of what state the circuit is currently in."
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _an_bypass(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "an_bypass"
            
            def description(self):
                return "Input to disable auto-negotiation and bypass the auto-negotiation function. If this input is asserted, auto-negotiation is turned off, but the PCS is connected to the output to allow operation."
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _an_enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "an_enb"
            
            def description(self):
                return "Enable signal for auto-negotiation"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["an_lt_sta"] = _AF6CNC0011_ETH10G_RD_SER_TUR._ETH_10G_AutoNeg._an_lt_sta()
            allFields["lt_restart"] = _AF6CNC0011_ETH10G_RD_SER_TUR._ETH_10G_AutoNeg._lt_restart()
            allFields["lt_enb"] = _AF6CNC0011_ETH10G_RD_SER_TUR._ETH_10G_AutoNeg._lt_enb()
            allFields["an_restart"] = _AF6CNC0011_ETH10G_RD_SER_TUR._ETH_10G_AutoNeg._an_restart()
            allFields["an_bypass"] = _AF6CNC0011_ETH10G_RD_SER_TUR._ETH_10G_AutoNeg._an_bypass()
            allFields["an_enb"] = _AF6CNC0011_ETH10G_RD_SER_TUR._ETH_10G_AutoNeg._an_enb()
            return allFields

import AtRegister

class _AF6CNC0021_XFI_RD_DIAG(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["Configure_register_1"] = _AF6CNC0021_XFI_RD_DIAG._Configure_register_1()
        allRegisters["Configure_register_2"] = _AF6CNC0021_XFI_RD_DIAG._Configure_register_2()
        allRegisters["Configure_register_3"] = _AF6CNC0021_XFI_RD_DIAG._Configure_register_3()
        allRegisters["Configure_register_4"] = _AF6CNC0021_XFI_RD_DIAG._Configure_register_4()
        allRegisters["icfgtime"] = _AF6CNC0021_XFI_RD_DIAG._icfgtime()
        allRegisters["istatuscfgtime"] = _AF6CNC0021_XFI_RD_DIAG._istatuscfgtime()
        allRegisters["Status_register_1"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_1()
        allRegisters["Status_register_2"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_2()
        allRegisters["Configure_register_5"] = _AF6CNC0021_XFI_RD_DIAG._Configure_register_5()
        allRegisters["Configure_register_6"] = _AF6CNC0021_XFI_RD_DIAG._Configure_register_6()
        allRegisters["Configure_register_7"] = _AF6CNC0021_XFI_RD_DIAG._Configure_register_7()
        allRegisters["Status_register_3"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_3()
        allRegisters["Status_register_4"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_4()
        allRegisters["Status_register_5"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_5()
        allRegisters["Status_register_6"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_6()
        allRegisters["Status_register_7"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_7()
        allRegisters["Status_register_8"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_8()
        allRegisters["Status_register_9"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_9()
        allRegisters["Status_register_10"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_10()
        allRegisters["Status_register_11"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_11()
        allRegisters["Status_register_12"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_12()
        allRegisters["Status_register_13"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_13()
        allRegisters["Status_register_14"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_14()
        allRegisters["Status_register_15"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_15()
        allRegisters["Status_register_16"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_16()
        allRegisters["Status_register_17"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_17()
        allRegisters["Status_register_18"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_18()
        allRegisters["Status_register_19"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_19()
        allRegisters["Status_register_20"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_20()
        allRegisters["Status_register_21"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_21()
        allRegisters["Status_register_22"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_22()
        allRegisters["Status_register_23"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_23()
        allRegisters["Status_register_24"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_24()
        return allRegisters

    class _Configure_register_1(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Pattern Generate Configure"
    
        def description(self):
            return "XFI PRBS Pattern Generate Configure"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "0x0"
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _cfgdainc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfgdainc"
            
            def description(self):
                return "Configure DA Address mode 1 : INC / 0: fixed"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfglengthinc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfglengthinc"
            
            def description(self):
                return "Configure length mode 1 : INC / 0: fixed"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgnumall(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "cfgnumall"
            
            def description(self):
                return "Configure Packet number mode 1 : continuity   / 0: fixed"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgins_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "cfgins_err"
            
            def description(self):
                return "Configure Packet Insert Error 1 : Insert / 0: Un-insert"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgforcsop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "cfgforcsop"
            
            def description(self):
                return "Configure Force Start of packet 1 : Force / 0: Un-Force"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgforceop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "cfgforceop"
            
            def description(self):
                return "Configure Force End of packet 1 : Force / 0: Un-Force"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfggenen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "cfggenen"
            
            def description(self):
                return "Configure PRBS packet test enable 1 : Enable / 0: Disnable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _flush(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "flush"
            
            def description(self):
                return "Configure flush PRBS packet gen  1 : flush / 0: Un-flush"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfgdainc"] = _AF6CNC0021_XFI_RD_DIAG._Configure_register_1._cfgdainc()
            allFields["cfglengthinc"] = _AF6CNC0021_XFI_RD_DIAG._Configure_register_1._cfglengthinc()
            allFields["cfgnumall"] = _AF6CNC0021_XFI_RD_DIAG._Configure_register_1._cfgnumall()
            allFields["cfgins_err"] = _AF6CNC0021_XFI_RD_DIAG._Configure_register_1._cfgins_err()
            allFields["cfgforcsop"] = _AF6CNC0021_XFI_RD_DIAG._Configure_register_1._cfgforcsop()
            allFields["cfgforceop"] = _AF6CNC0021_XFI_RD_DIAG._Configure_register_1._cfgforceop()
            allFields["Reserved"] = _AF6CNC0021_XFI_RD_DIAG._Configure_register_1._Reserved()
            allFields["cfggenen"] = _AF6CNC0021_XFI_RD_DIAG._Configure_register_1._cfggenen()
            allFields["flush"] = _AF6CNC0021_XFI_RD_DIAG._Configure_register_1._flush()
            return allFields

    class _Configure_register_2(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Pattern Packet Number Configure"
    
        def description(self):
            return "XFI PRBS Pattern Packet Number Configure"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "0x1"
            
        def startAddress(self):
            return 0x00000001
            
        def endAddress(self):
            return 0xffffffff

        class _cfgnumpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfgnumpkt"
            
            def description(self):
                return "Configure number of packet generate"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfgnumpkt"] = _AF6CNC0021_XFI_RD_DIAG._Configure_register_2._cfgnumpkt()
            return allFields

    class _Configure_register_3(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Pattern Packet lengthg Configure"
    
        def description(self):
            return "XFI PRBS Pattern Packet lengthg Configure"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "0x2"
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0xffffffff

        class _cfglengthmin(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfglengthmin"
            
            def description(self):
                return "Configure length minimum of packet generate"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfglengthmax(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "cfglengthmax"
            
            def description(self):
                return "Configure length maximum of packet generate"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfglengthmin"] = _AF6CNC0021_XFI_RD_DIAG._Configure_register_3._cfglengthmin()
            allFields["cfglengthmax"] = _AF6CNC0021_XFI_RD_DIAG._Configure_register_3._cfglengthmax()
            return allFields

    class _Configure_register_4(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Pattern Packet Data Configure"
    
        def description(self):
            return "XFI PRBS Pattern Packet Data Configure"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "0x3"
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _cfgdatfix(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfgdatfix"
            
            def description(self):
                return "Configure Fixed byte Data generate"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgdatmod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "cfgdatmod"
            
            def description(self):
                return "Configure data mode generate 0x0 : Unused 0x1 : Fixed Data 0x2 : PRBS31 data 0x3 : PRBS15 data 0x4 : PRBS7  data"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfgdatfix"] = _AF6CNC0021_XFI_RD_DIAG._Configure_register_4._cfgdatfix()
            allFields["cfgdatmod"] = _AF6CNC0021_XFI_RD_DIAG._Configure_register_4._cfgdatmod()
            return allFields

    class _icfgtime(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Diagnostic Gatetime Configuration"
    
        def description(self):
            return "XFI PRBS Diagnostic Gatetime Configuration"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control and Status"
            
        def fomular(self):
            return "0x8"
            
        def startAddress(self):
            return 0x00000008
            
        def endAddress(self):
            return 0xffffffff

        class _status_gatetime_diag(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "status_gatetime_diag"
            
            def description(self):
                return "Status Gatetime diagnostic 1:Running 0:Done-Ready"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _start_gatetime_diag(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "start_gatetime_diag"
            
            def description(self):
                return "Config start Diagnostic trigger 0 to 1 for Start auto run with Gatetime Configuration"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Unsed(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 17
        
            def name(self):
                return "Unsed"
            
            def description(self):
                return "Unsed"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _time_cfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 0
        
            def name(self):
                return "time_cfg"
            
            def description(self):
                return "Gatetime Configuration 1-86400 second"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["status_gatetime_diag"] = _AF6CNC0021_XFI_RD_DIAG._icfgtime._status_gatetime_diag()
            allFields["start_gatetime_diag"] = _AF6CNC0021_XFI_RD_DIAG._icfgtime._start_gatetime_diag()
            allFields["Unsed"] = _AF6CNC0021_XFI_RD_DIAG._icfgtime._Unsed()
            allFields["time_cfg"] = _AF6CNC0021_XFI_RD_DIAG._icfgtime._time_cfg()
            return allFields

    class _istatuscfgtime(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Diagnostic Gatetime Current"
    
        def description(self):
            return "XFI PRBS Diagnostic Gatetime_Status"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return "0x9"
            
        def startAddress(self):
            return 0x00000009
            
        def endAddress(self):
            return 0xffffffff

        class _Unsed(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 17
        
            def name(self):
                return "Unsed"
            
            def description(self):
                return "Unsed"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _currert_gatetime_diag(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 0
        
            def name(self):
                return "currert_gatetime_diag"
            
            def description(self):
                return "Current running time of Gatetime diagnostic"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Unsed"] = _AF6CNC0021_XFI_RD_DIAG._istatuscfgtime._Unsed()
            allFields["currert_gatetime_diag"] = _AF6CNC0021_XFI_RD_DIAG._istatuscfgtime._currert_gatetime_diag()
            return allFields

    class _Status_register_1(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Pattern Packet generate Counter RO"
    
        def description(self):
            return "XFI PRBS Pattern Packet generate Counter RO"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "0x10"
            
        def startAddress(self):
            return 0x00000010
            
        def endAddress(self):
            return 0xffffffff

        class _penr_opkttotal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "penr_opkttotal"
            
            def description(self):
                return "TX Packet Counterer"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["penr_opkttotal"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_1._penr_opkttotal()
            return allFields

    class _Status_register_2(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Pattern Packet generate Counter R2C"
    
        def description(self):
            return "XFI PRBS Pattern Packet generate Counter R2C"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "0x20"
            
        def startAddress(self):
            return 0x00000020
            
        def endAddress(self):
            return 0xffffffff

        class _penr2cpkttotal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "penr2cpkttotal"
            
            def description(self):
                return "TX Packet Counterer"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["penr2cpkttotal"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_2._penr2cpkttotal()
            return allFields

    class _Configure_register_5(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Pattern Packet analyzer PRBS Sticky Error"
    
        def description(self):
            return "XFI PRBS Pattern Packet analyzer PRBS Sticky Error"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "0x700"
            
        def startAddress(self):
            return 0x00000700
            
        def endAddress(self):
            return 0xffffffff

        class _err_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "err_stk"
            
            def description(self):
                return "Packet analyzer PRBS Sticky Error <0 = RX is synchronized /1 = RX is not synchronized>"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["err_stk"] = _AF6CNC0021_XFI_RD_DIAG._Configure_register_5._err_stk()
            return allFields

    class _Configure_register_6(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Pattern Packet analyzer PRBS Status Error"
    
        def description(self):
            return "XFI PRBS Pattern Packet analyzer PRBS Status Error"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "0x701"
            
        def startAddress(self):
            return 0x00000701
            
        def endAddress(self):
            return 0xffffffff

        class _err_sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "err_sta"
            
            def description(self):
                return "Packet analyzer PRBS Status Error <0 = RX is synchronized /1 = RX is not synchronized>"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["err_sta"] = _AF6CNC0021_XFI_RD_DIAG._Configure_register_6._err_sta()
            return allFields

    class _Configure_register_7(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Pattern Packet analyzer Data Configure"
    
        def description(self):
            return "XFI PRBS Pattern Packet analyzer Data Configure"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "0x601"
            
        def startAddress(self):
            return 0x00000601
            
        def endAddress(self):
            return 0xffffffff

        class _cfgfcsdrop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "cfgfcsdrop"
            
            def description(self):
                return "Configure FCS Drop"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgdatmod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "cfgdatmod"
            
            def description(self):
                return "Configure data mode generate 0x0 : Unused 0x1 : Fixed Data 0x2 : PRBS31 data 0x3 : PRBS15 data 0x4 : PRBS7  data"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfgfcsdrop"] = _AF6CNC0021_XFI_RD_DIAG._Configure_register_7._cfgfcsdrop()
            allFields["cfgdatmod"] = _AF6CNC0021_XFI_RD_DIAG._Configure_register_7._cfgdatmod()
            return allFields

    class _Status_register_3(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkttotal"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkttotal"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "0x400"
            
        def startAddress(self):
            return 0x00000400
            
        def endAddress(self):
            return 0xffffffff

        class _pkttotal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pkttotal"
            
            def description(self):
                return "RX Packet total"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pkttotal"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_3._pkttotal()
            return allFields

    class _Status_register_4(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pktgood"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pktgood"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "0x401"
            
        def startAddress(self):
            return 0x00000401
            
        def endAddress(self):
            return 0xffffffff

        class _pktgood(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktgood"
            
            def description(self):
                return "RX Packet good"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktgood"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_4._pktgood()
            return allFields

    class _Status_register_5(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pktfcserr"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pktfcserr"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "0x402"
            
        def startAddress(self):
            return 0x00000402
            
        def endAddress(self):
            return 0xffffffff

        class _pktfcserr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktfcserr"
            
            def description(self):
                return "RX Packet FCS error"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktfcserr"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_5._pktfcserr()
            return allFields

    class _Status_register_6(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkt length err"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkt length err"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "0x403"
            
        def startAddress(self):
            return 0x00000403
            
        def endAddress(self):
            return 0xffffffff

        class _pktlengtherr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktlengtherr"
            
            def description(self):
                return "RX Packet lengthgth error"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktlengtherr"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_6._pktlengtherr()
            return allFields

    class _Status_register_7(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkt PRBS Data err"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkt PRBS Data err"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "0x407"
            
        def startAddress(self):
            return 0x00000407
            
        def endAddress(self):
            return 0xffffffff

        class _pktdaterr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktdaterr"
            
            def description(self):
                return "RX Packet PRBS data error"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktdaterr"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_7._pktdaterr()
            return allFields

    class _Status_register_8(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkt length 0-64"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkt length 0-64"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "0x40A"
            
        def startAddress(self):
            return 0x0000040a
            
        def endAddress(self):
            return 0xffffffff

        class _pktlen64(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktlen64"
            
            def description(self):
                return "RX Counter pkt length 0-64"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktlen64"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_8._pktlen64()
            return allFields

    class _Status_register_9(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkt length 65-128"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkt length 65-128"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "0x40B"
            
        def startAddress(self):
            return 0x0000040b
            
        def endAddress(self):
            return 0xffffffff

        class _pktlen128(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktlen128"
            
            def description(self):
                return "RX Counter pkt length 65-128"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktlen128"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_9._pktlen128()
            return allFields

    class _Status_register_10(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkt length 129-256"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkt length 129-256"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "0x40C"
            
        def startAddress(self):
            return 0x0000040c
            
        def endAddress(self):
            return 0xffffffff

        class _pktlen256(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktlen256"
            
            def description(self):
                return "RX Counter pkt length 129-256"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktlen256"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_10._pktlen256()
            return allFields

    class _Status_register_11(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkt length 257-512"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkt length 257-512"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "0x40D"
            
        def startAddress(self):
            return 0x0000040d
            
        def endAddress(self):
            return 0xffffffff

        class _pktlen512(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktlen512"
            
            def description(self):
                return "RX Counter pkt length 257-215"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktlen512"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_11._pktlen512()
            return allFields

    class _Status_register_12(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkt length 513-1024"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkt length 513-1024"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "0x40E"
            
        def startAddress(self):
            return 0x0000040e
            
        def endAddress(self):
            return 0xffffffff

        class _pktlen1024(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktlen1024"
            
            def description(self):
                return "RX Counter pkt length 513-1024"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktlen1024"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_12._pktlen1024()
            return allFields

    class _Status_register_13(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkt length 1025-2048"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkt length 1025-2048"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "0x40F"
            
        def startAddress(self):
            return 0x0000040f
            
        def endAddress(self):
            return 0xffffffff

        class _pktlen2048(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktlen2048"
            
            def description(self):
                return "RX Counter pkt length 1025-2048"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktlen2048"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_13._pktlen2048()
            return allFields

    class _Status_register_14(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkttotal"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkttotal"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "0x500"
            
        def startAddress(self):
            return 0x00000500
            
        def endAddress(self):
            return 0xffffffff

        class _pkttotal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pkttotal"
            
            def description(self):
                return "RX Packet total"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pkttotal"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_14._pkttotal()
            return allFields

    class _Status_register_15(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pktgood"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pktgood"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "0x501"
            
        def startAddress(self):
            return 0x00000501
            
        def endAddress(self):
            return 0xffffffff

        class _pktgood(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktgood"
            
            def description(self):
                return "RX Packet good"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktgood"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_15._pktgood()
            return allFields

    class _Status_register_16(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pktfcserr"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pktfcserr"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "0x502"
            
        def startAddress(self):
            return 0x00000502
            
        def endAddress(self):
            return 0xffffffff

        class _pktfcserr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktfcserr"
            
            def description(self):
                return "RX Packet FCS error"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktfcserr"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_16._pktfcserr()
            return allFields

    class _Status_register_17(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkt length err"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkt length err"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "0x503"
            
        def startAddress(self):
            return 0x00000503
            
        def endAddress(self):
            return 0xffffffff

        class _pktlengtherr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktlengtherr"
            
            def description(self):
                return "RX Packet lengthgth error"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktlengtherr"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_17._pktlengtherr()
            return allFields

    class _Status_register_18(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkt PRBS Data err"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkt PRBS Data err"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "0x507"
            
        def startAddress(self):
            return 0x00000507
            
        def endAddress(self):
            return 0xffffffff

        class _pktdaterr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktdaterr"
            
            def description(self):
                return "RX Packet PRBS data error"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktdaterr"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_18._pktdaterr()
            return allFields

    class _Status_register_19(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkt length 0-64"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkt length 0-64"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "0x50A"
            
        def startAddress(self):
            return 0x0000050a
            
        def endAddress(self):
            return 0xffffffff

        class _pktlen64(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktlen64"
            
            def description(self):
                return "RX Counter pkt length 0-64"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktlen64"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_19._pktlen64()
            return allFields

    class _Status_register_20(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkt length 65-128"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkt length 65-128"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "0x50B"
            
        def startAddress(self):
            return 0x0000050b
            
        def endAddress(self):
            return 0xffffffff

        class _pktlen128(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktlen128"
            
            def description(self):
                return "RX Counter pkt length 65-128"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktlen128"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_20._pktlen128()
            return allFields

    class _Status_register_21(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkt length 129-256"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkt length 129-256"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "0x50C"
            
        def startAddress(self):
            return 0x0000050c
            
        def endAddress(self):
            return 0xffffffff

        class _pktlen256(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktlen256"
            
            def description(self):
                return "RX Counter pkt length 129-256"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktlen256"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_21._pktlen256()
            return allFields

    class _Status_register_22(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkt length 257-512"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkt length 257-512"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "0x50D"
            
        def startAddress(self):
            return 0x0000050d
            
        def endAddress(self):
            return 0xffffffff

        class _pktlen512(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktlen512"
            
            def description(self):
                return "RX Counter pkt length 257-215"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktlen512"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_22._pktlen512()
            return allFields

    class _Status_register_23(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkt length 513-1024"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkt length 513-1024"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "0x50E"
            
        def startAddress(self):
            return 0x0000050e
            
        def endAddress(self):
            return 0xffffffff

        class _pktlen1024(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktlen1024"
            
            def description(self):
                return "RX Counter pkt length 513-1024"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktlen1024"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_23._pktlen1024()
            return allFields

    class _Status_register_24(AtRegister.AtRegister):
        def name(self):
            return "XFI PRBS Packet analyzer Counter pkt length 1025-2048"
    
        def description(self):
            return "XFI PRBS Packet analyzer Counter pkt length 1025-2048"
            
        def width(self):
            return 32
        
        def type(self):
            return "Control"
            
        def fomular(self):
            return "0x50F"
            
        def startAddress(self):
            return 0x0000050f
            
        def endAddress(self):
            return 0xffffffff

        class _pktlen2048(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktlen2048"
            
            def description(self):
                return "RX Counter pkt length 1025-2048"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktlen2048"] = _AF6CNC0021_XFI_RD_DIAG._Status_register_24._pktlen2048()
            return allFields

import AtRegister

class _AF6CNC0011_RD_TOP(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["ProductID"] = _AF6CNC0011_RD_TOP._ProductID()
        allRegisters["YYMMDD_VerID"] = _AF6CNC0011_RD_TOP._YYMMDD_VerID()
        allRegisters["InternalID"] = _AF6CNC0011_RD_TOP._InternalID()
        allRegisters["o_control0"] = _AF6CNC0011_RD_TOP._o_control0()
        allRegisters["o_control1"] = _AF6CNC0011_RD_TOP._o_control1()
        allRegisters["o_control2"] = _AF6CNC0011_RD_TOP._o_control2()
        allRegisters["o_control3"] = _AF6CNC0011_RD_TOP._o_control3()
        allRegisters["o_control4"] = _AF6CNC0011_RD_TOP._o_control4()
        allRegisters["o_control9"] = _AF6CNC0011_RD_TOP._o_control9()
        allRegisters["c_uart"] = _AF6CNC0011_RD_TOP._c_uart()
        allRegisters["unused"] = _AF6CNC0011_RD_TOP._unused()
        allRegisters["status_top"] = _AF6CNC0011_RD_TOP._status_top()
        allRegisters["i_sticky0"] = _AF6CNC0011_RD_TOP._i_sticky0()
        allRegisters["clock_mon_high"] = _AF6CNC0011_RD_TOP._clock_mon_high()
        allRegisters["clock_mon_slow"] = _AF6CNC0011_RD_TOP._clock_mon_slow()
        return allRegisters

    class _ProductID(AtRegister.AtRegister):
        def name(self):
            return "Device Product ID"
    
        def description(self):
            return "This register indicates Product ID."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00f00000
            
        def endAddress(self):
            return 0xffffffff

        class _ProductID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ProductID"
            
            def description(self):
                return "ProductId"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ProductID"] = _AF6CNC0011_RD_TOP._ProductID._ProductID()
            return allFields

    class _YYMMDD_VerID(AtRegister.AtRegister):
        def name(self):
            return "Device Year Month Day Version ID"
    
        def description(self):
            return "This register indicates Year Month Day and main version ID."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00f00001
            
        def endAddress(self):
            return 0xffffffff

        class _Year(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "Year"
            
            def description(self):
                return "Year"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Month(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Month"
            
            def description(self):
                return "Month"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Day(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Day"
            
            def description(self):
                return "Day"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Version(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Version"
            
            def description(self):
                return "Version"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Year"] = _AF6CNC0011_RD_TOP._YYMMDD_VerID._Year()
            allFields["Month"] = _AF6CNC0011_RD_TOP._YYMMDD_VerID._Month()
            allFields["Day"] = _AF6CNC0011_RD_TOP._YYMMDD_VerID._Day()
            allFields["Version"] = _AF6CNC0011_RD_TOP._YYMMDD_VerID._Version()
            return allFields

    class _InternalID(AtRegister.AtRegister):
        def name(self):
            return "Device Internal ID"
    
        def description(self):
            return "This register indicates internal ID."
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00f00003
            
        def endAddress(self):
            return 0xffffffff

        class _InternalID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "InternalID"
            
            def description(self):
                return "InternalID"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["InternalID"] = _AF6CNC0011_RD_TOP._InternalID._InternalID()
            return allFields

    class _o_control0(AtRegister.AtRegister):
        def name(self):
            return "general purpose inputs for TX Uart"
    
        def description(self):
            return "This is the global configuration register for the Global Serdes Control"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x40"
            
        def startAddress(self):
            return 0x00000040
            
        def endAddress(self):
            return 0xffffffff

        class _tx_trig_send(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "tx_trig_send"
            
            def description(self):
                return "Uart TX trig start send out byte"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _tx_data_out(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tx_data_out"
            
            def description(self):
                return "Uart TX 8-bit data out"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tx_trig_send"] = _AF6CNC0011_RD_TOP._o_control0._tx_trig_send()
            allFields["tx_data_out"] = _AF6CNC0011_RD_TOP._o_control0._tx_data_out()
            return allFields

    class _o_control1(AtRegister.AtRegister):
        def name(self):
            return "Flow Control Mechanism for Ethernet pass-through Diagnostic Value"
    
        def description(self):
            return "This is the global configuration register for Flow Control Mechanism for Ethernet pass-through Diagnostic"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x41"
            
        def startAddress(self):
            return 0x00000041
            
        def endAddress(self):
            return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            return allFields

    class _o_control2(AtRegister.AtRegister):
        def name(self):
            return "o_control2"
    
        def description(self):
            return "This is the global configuration Error register for Flow Control Mechanism for Ethernet pass-through Diagnostic"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x42"
            
        def startAddress(self):
            return 0x00000042
            
        def endAddress(self):
            return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            return allFields

    class _o_control3(AtRegister.AtRegister):
        def name(self):
            return "Diagnostic Enable Control"
    
        def description(self):
            return "This is the global configuration register for the MIG (DDR/QDR) Diagnostic Control"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x43"
            
        def startAddress(self):
            return 0x00000043
            
        def endAddress(self):
            return 0xffffffff

        class _SGMII_SP_DiagEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "SGMII_SP_DiagEn"
            
            def description(self):
                return "Enable Daignostic for SGMII_SP 0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SGMII_DCC_DiagEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "SGMII_DCC_DiagEn"
            
            def description(self):
                return "Enable Daignostic for SGMII_DCC 0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SGMII_DIM_DiagEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "SGMII_DIM_DiagEn"
            
            def description(self):
                return "Enable Daignostic for SGMII_DIM 0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Basex2500_DiagEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Basex2500_DiagEn"
            
            def description(self):
                return "Enable Daignostic for Basex2500 0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _STBY_XFI_DiagEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "STBY_XFI_DiagEn"
            
            def description(self):
                return "Enable Daignostic for ACT_STBY 0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ACT_XFI_DiagEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "ACT_XFI_DiagEn"
            
            def description(self):
                return "Enable Daignostic for ACT_XFI 0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DDR3_DiagEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "DDR3_DiagEn"
            
            def description(self):
                return "Enable Daignostic for DDR#2 0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DDR2_DiagEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "DDR2_DiagEn"
            
            def description(self):
                return "Enable Daignostic for DDR#1 0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DDR1_DiagEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DDR1_DiagEn"
            
            def description(self):
                return "Enable Daignostic for DDR#1 0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SGMII_SP_DiagEn"] = _AF6CNC0011_RD_TOP._o_control3._SGMII_SP_DiagEn()
            allFields["SGMII_DCC_DiagEn"] = _AF6CNC0011_RD_TOP._o_control3._SGMII_DCC_DiagEn()
            allFields["SGMII_DIM_DiagEn"] = _AF6CNC0011_RD_TOP._o_control3._SGMII_DIM_DiagEn()
            allFields["Basex2500_DiagEn"] = _AF6CNC0011_RD_TOP._o_control3._Basex2500_DiagEn()
            allFields["STBY_XFI_DiagEn"] = _AF6CNC0011_RD_TOP._o_control3._STBY_XFI_DiagEn()
            allFields["ACT_XFI_DiagEn"] = _AF6CNC0011_RD_TOP._o_control3._ACT_XFI_DiagEn()
            allFields["DDR3_DiagEn"] = _AF6CNC0011_RD_TOP._o_control3._DDR3_DiagEn()
            allFields["DDR2_DiagEn"] = _AF6CNC0011_RD_TOP._o_control3._DDR2_DiagEn()
            allFields["DDR1_DiagEn"] = _AF6CNC0011_RD_TOP._o_control3._DDR1_DiagEn()
            return allFields

    class _o_control4(AtRegister.AtRegister):
        def name(self):
            return "Serdes Turning Bus Select for XFI"
    
        def description(self):
            return "This is the global configuration register for the Global Serdes Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag port 1 to 8"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x44"
            
        def startAddress(self):
            return 0x00000044
            
        def endAddress(self):
            return 0xffffffff

        class _xfi1_drp_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "xfi1_drp_en"
            
            def description(self):
                return "Select cpu control for serdes DRP or Global control of XFI_STBY 0: Global control 1: DRP access"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _xfi0_drp_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "xfi0_drp_en"
            
            def description(self):
                return "Select CPU control for serdes DRP or Global control of XFI_ACT 0: Global control 1: DRP access"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["xfi1_drp_en"] = _AF6CNC0011_RD_TOP._o_control4._xfi1_drp_en()
            allFields["xfi0_drp_en"] = _AF6CNC0011_RD_TOP._o_control4._xfi0_drp_en()
            return allFields

    class _o_control9(AtRegister.AtRegister):
        def name(self):
            return "Configure Clock monitor output"
    
        def description(self):
            return "This is the TOP global configuration register"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x49"
            
        def startAddress(self):
            return 0x00000049
            
        def endAddress(self):
            return 0xffffffff

        class _cfgrefpid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfgrefpid"
            
            def description(self):
                return "Configure Source Clock monitor output for clock_mon 0x0 : pcie_refclk_div2 0x1 : System PLL 155.52Mhz output 0x2 : refin_prc 0x3 : ext_ref_timing 0x4 : xfi_156p25_refclk#0_div2 0x5 : xfi_156p25_refclk#1_div2 0x6 : spare_serdes_refclk_div2 0x7 : basex2500_ref_div2 0x8 : ddr3_refclk#1 0x9 : ddr3_refclk#2 0xa : ddr3_refclk#3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfgrefpid"] = _AF6CNC0011_RD_TOP._o_control9._cfgrefpid()
            return allFields

    class _c_uart(AtRegister.AtRegister):
        def name(self):
            return "general purpose outputs from RX Uart"
    
        def description(self):
            return "This is value output from RX Uart"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x60"
            
        def startAddress(self):
            return 0x00000060
            
        def endAddress(self):
            return 0xffffffff

        class _i_status0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "i_status0"
            
            def description(self):
                return "Uart RX 8-bit data in"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["i_status0"] = _AF6CNC0011_RD_TOP._c_uart._i_status0()
            return allFields

    class _unused(AtRegister.AtRegister):
        def name(self):
            return "unused"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x61"
            
        def startAddress(self):
            return 0x00000061
            
        def endAddress(self):
            return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            return allFields

    class _status_top(AtRegister.AtRegister):
        def name(self):
            return "Top Status"
    
        def description(self):
            return "Top Interface Alarm Status"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x62"
            
        def startAddress(self):
            return 0x00000062
            
        def endAddress(self):
            return 0xffffffff

        class _ot_out(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "ot_out"
            
            def description(self):
                return "Current Status  Over-Temperature alarm ."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _vbram_alarm_out(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "vbram_alarm_out"
            
            def description(self):
                return "Current Status  VCCBRAM-sensor alarm ."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _vccaux_alarm_out(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "vccaux_alarm_out"
            
            def description(self):
                return "Current Status  VCCAUX-sensor alarm ."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _vccint_alarm_out(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "vccint_alarm_out"
            
            def description(self):
                return "Current Status  VCCINT-sensor alarm ."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _user_temp_alarm_out(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "user_temp_alarm_out"
            
            def description(self):
                return "Current Status  Temperature-sensor alarm ."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _alarm_out(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "alarm_out"
            
            def description(self):
                return "Current Status  SysMon Error ."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _SystemPll(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "SystemPll"
            
            def description(self):
                return "Current Status System PLL not locked. 0: Locked 1: Unlocked"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Ddr33Urst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "Ddr33Urst"
            
            def description(self):
                return "Current Status DDR#3 user Reset. 0: OutReseset 1: InReset"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Ddr32Urst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "Ddr32Urst"
            
            def description(self):
                return "Current Status DDR#2 user Reset. 0: OutReseset 1: InReset"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Ddr31Urst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Ddr31Urst"
            
            def description(self):
                return "Current Status DDR#1 user Reset. 0: OutReseset 1: InReset"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Ddr33Calib(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Ddr33Calib"
            
            def description(self):
                return "Current Status DDR#3 Calib . 0: Calib PASS 1: Calib FAIL"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Ddr32Calib(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Ddr32Calib"
            
            def description(self):
                return "Current Status DDR#2 Calib . 0: Calib PASS 1: Calib FAIL"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Ddr31Calib(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Ddr31Calib"
            
            def description(self):
                return "Current Status DDR#1 Calib . 0: Calib PASS 1: Calib FAIL"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ot_out"] = _AF6CNC0011_RD_TOP._status_top._ot_out()
            allFields["vbram_alarm_out"] = _AF6CNC0011_RD_TOP._status_top._vbram_alarm_out()
            allFields["vccaux_alarm_out"] = _AF6CNC0011_RD_TOP._status_top._vccaux_alarm_out()
            allFields["vccint_alarm_out"] = _AF6CNC0011_RD_TOP._status_top._vccint_alarm_out()
            allFields["user_temp_alarm_out"] = _AF6CNC0011_RD_TOP._status_top._user_temp_alarm_out()
            allFields["alarm_out"] = _AF6CNC0011_RD_TOP._status_top._alarm_out()
            allFields["SystemPll"] = _AF6CNC0011_RD_TOP._status_top._SystemPll()
            allFields["Ddr33Urst"] = _AF6CNC0011_RD_TOP._status_top._Ddr33Urst()
            allFields["Ddr32Urst"] = _AF6CNC0011_RD_TOP._status_top._Ddr32Urst()
            allFields["Ddr31Urst"] = _AF6CNC0011_RD_TOP._status_top._Ddr31Urst()
            allFields["Ddr33Calib"] = _AF6CNC0011_RD_TOP._status_top._Ddr33Calib()
            allFields["Ddr32Calib"] = _AF6CNC0011_RD_TOP._status_top._Ddr32Calib()
            allFields["Ddr31Calib"] = _AF6CNC0011_RD_TOP._status_top._Ddr31Calib()
            return allFields

    class _i_sticky0(AtRegister.AtRegister):
        def name(self):
            return "Top Interface Alarm Sticky"
    
        def description(self):
            return "Top Interface Alarm Sticky"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000050
            
        def endAddress(self):
            return 0xffffffff

        class _ot_out(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "ot_out"
            
            def description(self):
                return "Set 1 When Over-Temperature alarm Happens."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _vbram_alarm_out(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "vbram_alarm_out"
            
            def description(self):
                return "Set 1 When VCCBRAM-sensor alarm Happens."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _vccaux_alarm_out(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "vccaux_alarm_out"
            
            def description(self):
                return "Set 1 When VCCAUX-sensor alarm Happens."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _vccint_alarm_out(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "vccint_alarm_out"
            
            def description(self):
                return "Set 1 When VCCINT-sensor alarm Happens."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _user_temp_alarm_out(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "user_temp_alarm_out"
            
            def description(self):
                return "Set 1 When Temperature-sensor alarm Happens."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _alarm_out(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "alarm_out"
            
            def description(self):
                return "Set 1 When SysMon Error Happens."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _SystemPll(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "SystemPll"
            
            def description(self):
                return "Set 1 while PLL Locked state change event happens when System PLL not locked."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Ddr33Urst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "Ddr33Urst"
            
            def description(self):
                return "Set 1 while user Reset State Change Happens When DDR#3 Reset User."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Ddr32Urst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "Ddr32Urst"
            
            def description(self):
                return "Set 1 while user Reset State Change Happens When DDR#2 Reset User."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Ddr31Urst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Ddr31Urst"
            
            def description(self):
                return "Set 1 while user Reset State Change Happens When DDR#1 Reset User."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Ddr33Calib(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Ddr33Calib"
            
            def description(self):
                return "Set 1 while Calib State Change Event Happens When DDR#3 Calib Fail."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Ddr32Calib(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Ddr32Calib"
            
            def description(self):
                return "Set 1 while Calib State Change Event Happens When DDR#2 Calib Fail."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Ddr31Calib(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Ddr31Calib"
            
            def description(self):
                return "Set 1 while Calib State Change Event Happens When DDR#1 Calib Fail."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ot_out"] = _AF6CNC0011_RD_TOP._i_sticky0._ot_out()
            allFields["vbram_alarm_out"] = _AF6CNC0011_RD_TOP._i_sticky0._vbram_alarm_out()
            allFields["vccaux_alarm_out"] = _AF6CNC0011_RD_TOP._i_sticky0._vccaux_alarm_out()
            allFields["vccint_alarm_out"] = _AF6CNC0011_RD_TOP._i_sticky0._vccint_alarm_out()
            allFields["user_temp_alarm_out"] = _AF6CNC0011_RD_TOP._i_sticky0._user_temp_alarm_out()
            allFields["alarm_out"] = _AF6CNC0011_RD_TOP._i_sticky0._alarm_out()
            allFields["SystemPll"] = _AF6CNC0011_RD_TOP._i_sticky0._SystemPll()
            allFields["Ddr33Urst"] = _AF6CNC0011_RD_TOP._i_sticky0._Ddr33Urst()
            allFields["Ddr32Urst"] = _AF6CNC0011_RD_TOP._i_sticky0._Ddr32Urst()
            allFields["Ddr31Urst"] = _AF6CNC0011_RD_TOP._i_sticky0._Ddr31Urst()
            allFields["Ddr33Calib"] = _AF6CNC0011_RD_TOP._i_sticky0._Ddr33Calib()
            allFields["Ddr32Calib"] = _AF6CNC0011_RD_TOP._i_sticky0._Ddr32Calib()
            allFields["Ddr31Calib"] = _AF6CNC0011_RD_TOP._i_sticky0._Ddr31Calib()
            return allFields

    class _clock_mon_high(AtRegister.AtRegister):
        def name(self):
            return "Clock Monitoring Status"
    
        def description(self):
            return "0x0e : System clk 		     (155.52 Mhz) 0x0d : prc_ref clock        (155.52 Mhz) 0x0c : ext_ref clock        (155.52 Mhz) 0x0b : spgmii_user_clk      (125 Mhz) 0x0a : dccgmii_user_clk     (125 Mhz) 0x09 : dimgmii_user_clk     (125 Mhz) 0x08 : basex2500_user_clk	 (312.5 Mhz) 0x07 : 10g_stby_rxclk     	 (156.25 Mhz) 0x06 : 10g_act_rxclk     	 (156.25 Mhz) 0x05 : 10g_stby_txclk     	 (156.25 Mhz) 0x04 : 10g_act_txclk     	 (156.25 Mhz) 0x03 : DDR3#3 user clock    (200 Mhz) 0x02 : DDR3#2 user clock    (200 Mhz) 0x01 : DDR3#1 user clock    (200 Mhz) 0x00 : pcie_upclk clock     (62.5 Mhz)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x46000 + port_id"
            
        def startAddress(self):
            return 0x00046000
            
        def endAddress(self):
            return 0x0004601f

        class _i_statusx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "i_statusx"
            
            def description(self):
                return "Clock Value Monitor Change from hex format to DEC format to get Clock Value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["i_statusx"] = _AF6CNC0011_RD_TOP._clock_mon_high._i_statusx()
            return allFields

    class _clock_mon_slow(AtRegister.AtRegister):
        def name(self):
            return "Clock Monitoring Status"
    
        def description(self):
            return "0x1f : unused (0 Mhz) 0x1e : unused (0 Mhz) 0x1d : unused (0 Mhz) 0x1c : pm_tick              (1 Hz) for get value frequency SW must use : F = 155520000/register value 0x1b : unused (0 Mhz) 0x1a : unused (0 Mhz) 0x19 : unused (0 Mhz) 0x18 : unused (0 Mhz) 0x17 : unused (0 Mhz) 0x16 : unused (0 Mhz) 0x15 : unused (0 Mhz) 0x14 : unused (0 Mhz) 0x13 : unused (0 Mhz) 0x12 : unused (0 Mhz) 0x11 : unused (0 Mhz) 0x10 : unused (0 Mhz) 0x0f : unused (0 Mhz) 0x0e : unused (0 Mhz) 0x0d : unused (0 Mhz) 0x0c : fsm_pcp_2   		(1 Mhz or 2 Mhz  frequency clk Diagnostic) 0x0b : fsm_pcp_1   		(1 Mhz or 2 Mhz  frequency clk Diagnostic) 0x0a : fsm_pcp_0   		(1 Mhz or 2 Mhz  frequency clk Diagnostic) 0x09 : spare_gpio_7        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN) 0x08 : spare_gpio_6        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN) 0x07 : spare_gpio_5        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN) 0x06 : spare_gpio_4        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN) 0x05 : spare_gpio_3        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN) 0x04 : spare_gpio_2        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN) 0x03 : spare_gpio_1        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN) 0x02 : spare_gpio_0        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN) 0x01 : spare_clk_1         (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN) 0x00 : spare_clk_0         (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x47000 + port_id"
            
        def startAddress(self):
            return 0x00047000
            
        def endAddress(self):
            return 0x0004701f

        class _i_statusx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "i_statusx"
            
            def description(self):
                return "Clock Value Monitor Change from hex format to DEC format to get Clock Value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["i_statusx"] = _AF6CNC0011_RD_TOP._clock_mon_slow._i_statusx()
            return allFields

import AtRegister

class _Xilinx_Serdes_Turning_1x(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["SERDES_DRP_PORT"] = _Xilinx_Serdes_Turning_1x._SERDES_DRP_PORT()
        allRegisters["SERDES_LoopBack"] = _Xilinx_Serdes_Turning_1x._SERDES_LoopBack()
        allRegisters["SERDES_POWER_DOWN"] = _Xilinx_Serdes_Turning_1x._SERDES_POWER_DOWN()
        allRegisters["SERDES_TX_Reset"] = _Xilinx_Serdes_Turning_1x._SERDES_TX_Reset()
        allRegisters["SERDES_RX_Reset"] = _Xilinx_Serdes_Turning_1x._SERDES_RX_Reset()
        allRegisters["SERDES_LPMDFE_Mode"] = _Xilinx_Serdes_Turning_1x._SERDES_LPMDFE_Mode()
        allRegisters["SERDES_LPMDFE_Reset"] = _Xilinx_Serdes_Turning_1x._SERDES_LPMDFE_Reset()
        allRegisters["SERDES_TXDIFFCTRL"] = _Xilinx_Serdes_Turning_1x._SERDES_TXDIFFCTRL()
        allRegisters["SERDES_TXPOSTCURSOR"] = _Xilinx_Serdes_Turning_1x._SERDES_TXPOSTCURSOR()
        allRegisters["SERDES_TXPRECURSOR"] = _Xilinx_Serdes_Turning_1x._SERDES_TXPRECURSOR()
        return allRegisters

    class _SERDES_DRP_PORT(AtRegister.AtRegister):
        def name(self):
            return "SERDES DRP PORT"
    
        def description(self):
            return "Read/Write DRP address of SERDES,"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "XFI + $DRP"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _drp_rw(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "drp_rw"
            
            def description(self):
                return "DRP read/write value"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["drp_rw"] = _Xilinx_Serdes_Turning_1x._SERDES_DRP_PORT._drp_rw()
            return allFields

    class _SERDES_LoopBack(AtRegister.AtRegister):
        def name(self):
            return "SERDES LoopBack"
    
        def description(self):
            return "Configurate LoopBack,"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0xffffffff

        class _lpback_serdes(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lpback_serdes"
            
            def description(self):
                return "Loop back mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lpback_serdes"] = _Xilinx_Serdes_Turning_1x._SERDES_LoopBack._lpback_serdes()
            return allFields

    class _SERDES_POWER_DOWN(AtRegister.AtRegister):
        def name(self):
            return "SERDES POWER DOWN"
    
        def description(self):
            return "Configurate Power Down ,"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _TXELECIDLE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "TXELECIDLE"
            
            def description(self):
                return "TXELECIDLE must be strapped to TXPD[1] and TXPD[0] of Port"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TXPD(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "TXPD"
            
            def description(self):
                return "TX Power Down"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _RXPD(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RXPD"
            
            def description(self):
                return "RX Power Down"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TXELECIDLE"] = _Xilinx_Serdes_Turning_1x._SERDES_POWER_DOWN._TXELECIDLE()
            allFields["TXPD"] = _Xilinx_Serdes_Turning_1x._SERDES_POWER_DOWN._TXPD()
            allFields["RXPD"] = _Xilinx_Serdes_Turning_1x._SERDES_POWER_DOWN._RXPD()
            return allFields

    class _SERDES_TX_Reset(AtRegister.AtRegister):
        def name(self):
            return "SERDES TX Reset"
    
        def description(self):
            return "Reset TX SERDES,"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000c
            
        def endAddress(self):
            return 0xffffffff

        class _txrst_done(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 16
        
            def name(self):
                return "txrst_done"
            
            def description(self):
                return "TX Reset Done, bit per sub port"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _txrst_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txrst_en"
            
            def description(self):
                return "Should reset TX_PMA SERDES about 300-500 ns"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txrst_done"] = _Xilinx_Serdes_Turning_1x._SERDES_TX_Reset._txrst_done()
            allFields["txrst_en"] = _Xilinx_Serdes_Turning_1x._SERDES_TX_Reset._txrst_en()
            return allFields

    class _SERDES_RX_Reset(AtRegister.AtRegister):
        def name(self):
            return "SERDES RX Reset"
    
        def description(self):
            return "Reset RX SERDES,"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000d
            
        def endAddress(self):
            return 0xffffffff

        class _rxrst_done(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 16
        
            def name(self):
                return "rxrst_done"
            
            def description(self):
                return "RX Reset Done"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _rxrst_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxrst_en"
            
            def description(self):
                return "Should reset reset RX_PMA SERDES about 300-500 ns"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxrst_done"] = _Xilinx_Serdes_Turning_1x._SERDES_RX_Reset._rxrst_done()
            allFields["rxrst_en"] = _Xilinx_Serdes_Turning_1x._SERDES_RX_Reset._rxrst_en()
            return allFields

    class _SERDES_LPMDFE_Mode(AtRegister.AtRegister):
        def name(self):
            return "SERDES LPMDFE Mode"
    
        def description(self):
            return "Configure LPM/DFE mode ,"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000e
            
        def endAddress(self):
            return 0xffffffff

        class _lpmdfe_mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lpmdfe_mode"
            
            def description(self):
                return "bit per sub port"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lpmdfe_mode"] = _Xilinx_Serdes_Turning_1x._SERDES_LPMDFE_Mode._lpmdfe_mode()
            return allFields

    class _SERDES_LPMDFE_Reset(AtRegister.AtRegister):
        def name(self):
            return "SERDES LPMDFE Reset"
    
        def description(self):
            return "Reset LPM/DFE ,"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000f
            
        def endAddress(self):
            return 0xffffffff

        class _lpmdfe_reset(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lpmdfe_reset"
            
            def description(self):
                return "bit per sub port, Must be toggled after switching between modes to initialize adaptation"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lpmdfe_reset"] = _Xilinx_Serdes_Turning_1x._SERDES_LPMDFE_Reset._lpmdfe_reset()
            return allFields

    class _SERDES_TXDIFFCTRL(AtRegister.AtRegister):
        def name(self):
            return "SERDES TXDIFFCTRL"
    
        def description(self):
            return "Driver Swing Control, see \"Table 3-35: TX Configurable Driver Ports\" page 158 of UG578 for more detail, there is  2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000010
            
        def endAddress(self):
            return 0xffffffff

        class _TXDIFFCTRL(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TXDIFFCTRL"
            
            def description(self):
                return ""
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TXDIFFCTRL"] = _Xilinx_Serdes_Turning_1x._SERDES_TXDIFFCTRL._TXDIFFCTRL()
            return allFields

    class _SERDES_TXPOSTCURSOR(AtRegister.AtRegister):
        def name(self):
            return "SERDES TXPOSTCURSOR"
    
        def description(self):
            return "Transmitter post-cursor TX pre-emphasis control, see \"Table 3-35: TX Configurable Driver Ports\" page 160 of UG578 for more detail, there is  2 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000011
            
        def endAddress(self):
            return 0xffffffff

        class _TXPOSTCURSOR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TXPOSTCURSOR"
            
            def description(self):
                return ""
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TXPOSTCURSOR"] = _Xilinx_Serdes_Turning_1x._SERDES_TXPOSTCURSOR._TXPOSTCURSOR()
            return allFields

    class _SERDES_TXPRECURSOR(AtRegister.AtRegister):
        def name(self):
            return "SERDES TXPRECURSOR"
    
        def description(self):
            return "Transmitter pre-cursor TX pre-emphasis control, see \"Table 3-35: TX Configurable Driver Ports\" page 161 of UG578 for more detail, there is  2 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000012
            
        def endAddress(self):
            return 0xffffffff

        class _TXPRECURSOR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TXPRECURSOR"
            
            def description(self):
                return ""
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TXPRECURSOR"] = _Xilinx_Serdes_Turning_1x._SERDES_TXPRECURSOR._TXPRECURSOR()
            return allFields

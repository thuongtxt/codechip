import AtRegister

class RegisterProviderFactory(AtRegister.AtRegisterProviderFactory):
    def _allRegisterProviders(self):
        allProviders = {}

        from _Xilinx_Serdes_Turning_1x import _Xilinx_Serdes_Turning_1x
        allProviders["_Xilinx_Serdes_Turning_1x"] = _Xilinx_Serdes_Turning_1x()

        from _ES_Local_Bus_2_1G_MDIO import _ES_Local_Bus_2_1G_MDIO
        allProviders["_ES_Local_Bus_2_1G_MDIO"] = _ES_Local_Bus_2_1G_MDIO()

        from _AF6CNC0011_RD_TOP import _AF6CNC0011_RD_TOP
        allProviders["_AF6CNC0011_RD_TOP"] = _AF6CNC0011_RD_TOP()

        from _AF6CNC0011_ETH10G_RD_SER_TUR import _AF6CNC0011_ETH10G_RD_SER_TUR
        allProviders["_AF6CNC0011_ETH10G_RD_SER_TUR"] = _AF6CNC0011_ETH10G_RD_SER_TUR()

        from _AF6CNC0021_XFI_RD_DIAG import _AF6CNC0021_XFI_RD_DIAG
        allProviders["_AF6CNC0021_XFI_RD_DIAG"] = _AF6CNC0021_XFI_RD_DIAG()

        from _ES_MII_RD_DIAG import _ES_MII_RD_DIAG
        allProviders["_ES_MII_RD_DIAG"] = _ES_MII_RD_DIAG()

        from _ES_Local_Bus_2_TenG_MDIO import _ES_Local_Bus_2_TenG_MDIO
        allProviders["_ES_Local_Bus_2_TenG_MDIO"] = _ES_Local_Bus_2_TenG_MDIO()

        from _AF6CNC0021_GMII_RD_DIAG import _AF6CNC0021_GMII_RD_DIAG
        allProviders["_AF6CNC0021_GMII_RD_DIAG"] = _AF6CNC0021_GMII_RD_DIAG()


        return allProviders

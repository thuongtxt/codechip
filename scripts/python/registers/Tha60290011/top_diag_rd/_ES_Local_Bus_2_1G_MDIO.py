import AtRegister

class _ES_Local_Bus_2_1G_MDIO(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["ack_bit"] = _ES_Local_Bus_2_1G_MDIO._ack_bit()
        allRegisters["pcfg_ctl1"] = _ES_Local_Bus_2_1G_MDIO._pcfg_ctl1()
        allRegisters["md_ack_clr"] = _ES_Local_Bus_2_1G_MDIO._md_ack_clr()
        allRegisters["pcfg_ctl2"] = _ES_Local_Bus_2_1G_MDIO._pcfg_ctl2()
        allRegisters["pcfg_ctl3"] = _ES_Local_Bus_2_1G_MDIO._pcfg_ctl3()
        return allRegisters

    class _ack_bit(AtRegister.AtRegister):
        def name(self):
            return "Configure Raise mode for Interrupt Pin"
    
        def description(self):
            return "ack bit status this bit set to 1 when Read/Write OP done and will be clear when ACK_CLR bit set"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "Base_0x0 + 0x0"
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _ack_bit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ack_bit"
            
            def description(self):
                return "set to 1 when Read/Write OP done and will be clear when ACK_CLR bit set"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ack_bit"] = _ES_Local_Bus_2_1G_MDIO._ack_bit._ack_bit()
            return allFields

    class _pcfg_ctl1(AtRegister.AtRegister):
        def name(self):
            return "Config PHY address"
    
        def description(self):
            return "This is Config PHY address and Port Select"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "Base_0x10 + 0x10"
            
        def startAddress(self):
            return 0x00000010
            
        def endAddress(self):
            return 0xffffffff

        class _md_cs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "md_cs"
            
            def description(self):
                return "Select MDIO control port 1->4 0 : Un-Select 1 : Select MDIO P1 2 : Select MDIO P2 3 : Select MDIO P3 4 : Select MDIO P4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _md_phyadr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "md_phyadr"
            
            def description(self):
                return "MDIO Device Addresses it is fix 0x1 for Xilinx SGMII Serdes"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["md_cs"] = _ES_Local_Bus_2_1G_MDIO._pcfg_ctl1._md_cs()
            allFields["md_phyadr"] = _ES_Local_Bus_2_1G_MDIO._pcfg_ctl1._md_phyadr()
            return allFields

    class _md_ack_clr(AtRegister.AtRegister):
        def name(self):
            return "Status of Diagnostic Interrupt"
    
        def description(self):
            return "This is Status of Diagnostic Interrupt 1-2"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "Base_0x11 + 0x11"
            
        def startAddress(self):
            return 0x00000011
            
        def endAddress(self):
            return 0xffffffff

        class _ack_clr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ack_clr"
            
            def description(self):
                return "Write to Clear ACK OP"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ack_clr"] = _ES_Local_Bus_2_1G_MDIO._md_ack_clr._ack_clr()
            return allFields

    class _pcfg_ctl2(AtRegister.AtRegister):
        def name(self):
            return "Config Reg address"
    
        def description(self):
            return "This is Config Reg address and RNW"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "Base_0x12 + 0x12"
            
        def startAddress(self):
            return 0x00000012
            
        def endAddress(self):
            return 0xffffffff

        class _md_op(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 5
        
            def name(self):
                return "md_op"
            
            def description(self):
                return "OP: operation code 2'b00 : Open 2'b10 : Read Operation 2'b01 : Write Operation"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _md_regadr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "md_regadr"
            
            def description(self):
                return "MDIO Register  Addresses"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["md_op"] = _ES_Local_Bus_2_1G_MDIO._pcfg_ctl2._md_op()
            allFields["md_regadr"] = _ES_Local_Bus_2_1G_MDIO._pcfg_ctl2._md_regadr()
            return allFields

    class _pcfg_ctl3(AtRegister.AtRegister):
        def name(self):
            return "Config Reg address"
    
        def description(self):
            return "This is Config Reg address and RNW"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "Base_0x20 + 0x20"
            
        def startAddress(self):
            return 0x00000020
            
        def endAddress(self):
            return 0xffffffff

        class _md_data_read(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "md_data_read"
            
            def description(self):
                return "MDIO Data  write to PHY chip"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _md_data_read(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "md_data_read"
            
            def description(self):
                return "MDIO Data read from PHY chip"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["md_data_read"] = _ES_Local_Bus_2_1G_MDIO._pcfg_ctl3._md_data_read()
            allFields["md_data_read"] = _ES_Local_Bus_2_1G_MDIO._pcfg_ctl3._md_data_read()
            return allFields

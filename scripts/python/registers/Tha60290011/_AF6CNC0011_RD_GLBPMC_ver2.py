import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0011_RD_GLBPMC_ver2(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["upen_txpwcnt_info1_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_txpwcnt_info1_rw()
        allRegisters["upen_txpwcnt_info1_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_txpwcnt_info1_rw()
        allRegisters["upen_txpwcnt_info0"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_txpwcnt_info0()
        allRegisters["upen_rxpwcnt_info0_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_rxpwcnt_info0_rw()
        allRegisters["upen_rxpwcnt_info0_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_rxpwcnt_info0_rw()
        allRegisters["upen_rxpwcnt_info1_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_rxpwcnt_info1_rw()
        allRegisters["upen_rxpwcnt_info1_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_rxpwcnt_info1_rw()
        allRegisters["upen_rxpwcnt_info2_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_rxpwcnt_info2_rw()
        allRegisters["upen_rxpwcnt_info2_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_rxpwcnt_info2_rw()
        allRegisters["upen_poh_pmr_cnt0_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_poh_pmr_cnt0_rw()
        allRegisters["upen_poh_pmr_cnt0_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_poh_pmr_cnt0_rw()
        allRegisters["upen_sts_pohpm_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_sts_pohpm_rw()
        allRegisters["upen_sts_pohpm_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_sts_pohpm_rw()
        allRegisters["upen_vt_pohpm_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_vt_pohpm_rw()
        allRegisters["upen_vt_pohpm_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_vt_pohpm_rw()
        allRegisters["upen_poh_vt_cnt0_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_poh_vt_cnt0_rw()
        allRegisters["upen_poh_vt_cnt0_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_poh_vt_cnt0_rw()
        allRegisters["upen_poh_vt_cnt1_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_poh_vt_cnt1_rw()
        allRegisters["upen_poh_vt_cnt1_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_poh_vt_cnt1_rw()
        allRegisters["upen_poh_sts_cnt0_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_poh_sts_cnt0_rw()
        allRegisters["upen_poh_sts_cnt0_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_poh_sts_cnt0_rw()
        allRegisters["upen_poh_sts_cnt1_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_poh_sts_cnt1_rw()
        allRegisters["upen_poh_sts_cnt1_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_poh_sts_cnt1_rw()
        allRegisters["upen_pdh_de1cntval30_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_pdh_de1cntval30_rw()
        allRegisters["upen_pdh_de1cntval30_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_pdh_de1cntval30_rw()
        allRegisters["upen_pdh_de3cnt0_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_pdh_de3cnt0_rw()
        allRegisters["upen_pdh_de3cnt0_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_pdh_de3cnt0_rw()
        allRegisters["upen_cla0_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_cla0_rw()
        allRegisters["upen_cla0_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_cla0_rw()
        allRegisters["upen_cnttxoam"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_cnttxoam()
        allRegisters["upen_cls_bcnt_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_cls_bcnt_rw()
        allRegisters["upen_cls_bcnt_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_cls_bcnt_rw()
        allRegisters["upen_cls_opcnt0_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_cls_opcnt0_rw()
        allRegisters["upen_cls_opcnt0_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_cls_opcnt0_rw()
        allRegisters["upen_cls_opcnt1_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_cls_opcnt1_rw()
        allRegisters["upen_cls_opcnt1_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_cls_opcnt1_rw()
        allRegisters["upen_cls_ipcnt_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_cls_ipcnt_rw()
        allRegisters["upen_cls_ipcnt_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_cls_ipcnt_rw()
        allRegisters["upen_eth_bytecnt_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_eth_bytecnt_rw()
        allRegisters["upen_eth_bytecnt_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_eth_bytecnt_rw()
        allRegisters["upen_eth_bytecnt_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_eth_bytecnt_rw()
        allRegisters["upen_eth_bytecnt_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_eth_bytecnt_rw()
        allRegisters["upen_de1_tdmvld_cnt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_de1_tdmvld_cnt()
        allRegisters["upen_de3_tdmvld_cnt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_de3_tdmvld_cnt()
        allRegisters["upen_ge00lltx_typepkt_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_ge00lltx_typepkt_rw()
        allRegisters["upen_ge00lltx_typepkt_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_ge00lltx_typepkt_rw()
        allRegisters["upen_ge00lltx_pkt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_ge00lltx_pkt()
        allRegisters["upen_ge00lltx_byte"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_ge00lltx_byte()
        allRegisters["upen_ge00llrx_typepkt_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_ge00llrx_typepkt_rw()
        allRegisters["upen_ge00llrx_typepkt_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_ge00llrx_typepkt_rw()
        allRegisters["upen_ge00llrx_pkt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_ge00llrx_pkt()
        allRegisters["upen_ge00llrx_byte"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_ge00llrx_byte()
        allRegisters["upen_ge10lltx_typepkt_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_ge10lltx_typepkt_rw()
        allRegisters["upen_ge10lltx_typepkt_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_ge10lltx_typepkt_rw()
        allRegisters["upen_ge10lltx_pkt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_ge10lltx_pkt()
        allRegisters["upen_ge10lltx_byte"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_ge10lltx_byte()
        allRegisters["upen_ge10llrx_typepkt_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_ge10llrx_typepkt_rw()
        allRegisters["upen_ge10llrx_typepkt_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_ge10llrx_typepkt_rw()
        allRegisters["upen_ge10llrx_pkt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_ge10llrx_pkt()
        allRegisters["upen_ge10llrx_byte"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_ge10llrx_byte()
        allRegisters["upen_dimtx_typepkt_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_dimtx_typepkt_rw()
        allRegisters["upen_dimtx_typepkt_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_dimtx_typepkt_rw()
        allRegisters["upen_dimtx_pkt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_dimtx_pkt()
        allRegisters["upen_dimtx_byte"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_dimtx_byte()
        allRegisters["upen_dimrx_typepkt_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_dimrx_typepkt_rw()
        allRegisters["upen_dimrx_typepkt_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_dimrx_typepkt_rw()
        allRegisters["upen_dimrx_pkt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_dimrx_pkt()
        allRegisters["upen_dimrx_byte"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_dimrx_byte()
        allRegisters["upen_mac2_5tx_typepkt_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_mac2_5tx_typepkt_rw()
        allRegisters["upen_mac2_5tx_typepkt_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_mac2_5tx_typepkt_rw()
        allRegisters["upen_mac2_5tx_pkt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_mac2_5tx_pkt()
        allRegisters["upen_mac2_5tx_byte"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_mac2_5tx_byte()
        allRegisters["upen_mac2_5rx_typepkt_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_mac2_5rx_typepkt_rw()
        allRegisters["upen_mac2_5rx_typepkt_rw"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_mac2_5rx_typepkt_rw()
        allRegisters["upen_ge2_5rx_pkt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_ge2_5rx_pkt()
        allRegisters["upen_mac2_5rx_byte"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_mac2_5rx_byte()
        return allRegisters

    class _upen_txpwcnt_info1_rw(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0_0000 + 1024*$offset + $pwid"
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0x00000bff

        class _txpwcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txpwcnt"
            
            def description(self):
                return "in case of txpwcnt0 side: + offset = 0 : txrbit + offset = 1 : txnbit + offset = 2 : txpkt in case of txpwcnt1 side: + offset = 0 : txpbit + offset = 1 : txlbit + offset = 2 : unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txpwcnt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_txpwcnt_info1_rw._txpwcnt()
            return allFields

    class _upen_txpwcnt_info1_rw(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0_8000 + 1024*$offset + $pwid"
            
        def startAddress(self):
            return 0x00008000
            
        def endAddress(self):
            return 0x00008bff

        class _txpwcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txpwcnt"
            
            def description(self):
                return "in case of txpwcnt0 side: + offset = 0 : txrbit + offset = 1 : txnbit + offset = 2 : txpkt in case of txpwcnt1 side: + offset = 0 : txpbit + offset = 1 : txlbit + offset = 2 : unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txpwcnt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_txpwcnt_info1_rw._txpwcnt()
            return allFields

    class _upen_txpwcnt_info0(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire TX Byte Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_0000 + $pwid"
            
        def startAddress(self):
            return 0x00080000
            
        def endAddress(self):
            return 0x000803ff

        class _pwcntbyte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pwcntbyte"
            
            def description(self):
                return "txpwcntbyte"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pwcntbyte"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_txpwcnt_info0._pwcntbyte()
            return allFields

    class _upen_rxpwcnt_info0_rw(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receiver Counter Info0"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1_0000 + $pwid"
            
        def startAddress(self):
            return 0x00010000
            
        def endAddress(self):
            return 0x000103ff

        class _rxpwcnt_info0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpwcnt_info0"
            
            def description(self):
                return "in case of rxpwcnt_info0_cnt0 side: + rxcntbyte in case of rxpwcnt_info0_cnt1 side: + rxpkt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpwcnt_info0"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_rxpwcnt_info0_rw._rxpwcnt_info0()
            return allFields

    class _upen_rxpwcnt_info0_rw(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receiver Counter Info0"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1_4000 + $pwid"
            
        def startAddress(self):
            return 0x00014000
            
        def endAddress(self):
            return 0x000143ff

        class _rxpwcnt_info0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpwcnt_info0"
            
            def description(self):
                return "in case of rxpwcnt_info0_cnt0 side: + rxcntbyte in case of rxpwcnt_info0_cnt1 side: + rxpkt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpwcnt_info0"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_rxpwcnt_info0_rw._rxpwcnt_info0()
            return allFields

    class _upen_rxpwcnt_info1_rw(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receiver Counter Info1"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x2_0000 + 1024*$offset + $pwid"
            
        def startAddress(self):
            return 0x00020000
            
        def endAddress(self):
            return 0x00020bff

        class _rxpwcnt_info0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpwcnt_info0"
            
            def description(self):
                return "in case of rxpwcnt_info0_cnt0 side: + offset = 0 : rxrbit + offset = 1 : rxnbit + offset = 2 : rxstray in case of rxpwcnt_info0_cnt1 side: + offset = 0 : rxpbit + offset = 1 : rxlbit + offset = 2 : rxmalform"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpwcnt_info0"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_rxpwcnt_info1_rw._rxpwcnt_info0()
            return allFields

    class _upen_rxpwcnt_info1_rw(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receiver Counter Info1"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x2_8000 + 1024*$offset + $pwid"
            
        def startAddress(self):
            return 0x00028000
            
        def endAddress(self):
            return 0x00028bff

        class _rxpwcnt_info0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpwcnt_info0"
            
            def description(self):
                return "in case of rxpwcnt_info0_cnt0 side: + offset = 0 : rxrbit + offset = 1 : rxnbit + offset = 2 : rxstray in case of rxpwcnt_info0_cnt1 side: + offset = 0 : rxpbit + offset = 1 : rxlbit + offset = 2 : rxmalform"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpwcnt_info0"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_rxpwcnt_info1_rw._rxpwcnt_info0()
            return allFields

    class _upen_rxpwcnt_info2_rw(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receiver Counter Info2"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_0000 + 1024*$offset + $pwid"
            
        def startAddress(self):
            return 0x00030000
            
        def endAddress(self):
            return 0x00030bff

        class _rxpwcnt_info1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpwcnt_info1"
            
            def description(self):
                return "in case of rxpwcnt_info1_cnt0 side: + offset = 0 : rxunderrun + offset = 1 : rxlops + offset = 2 : rxearly in case of rxpwcnt_info1_cnt1 side: + offset = 0 : rxoverrun + offset = 1 : rxlost + offset = 2 : rxlate"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpwcnt_info1"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_rxpwcnt_info2_rw._rxpwcnt_info1()
            return allFields

    class _upen_rxpwcnt_info2_rw(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receiver Counter Info2"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_8000 + 1024*$offset + $pwid"
            
        def startAddress(self):
            return 0x00038000
            
        def endAddress(self):
            return 0x00038bff

        class _rxpwcnt_info1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpwcnt_info1"
            
            def description(self):
                return "in case of rxpwcnt_info1_cnt0 side: + offset = 0 : rxunderrun + offset = 1 : rxlops + offset = 2 : rxearly in case of rxpwcnt_info1_cnt1 side: + offset = 0 : rxoverrun + offset = 1 : rxlost + offset = 2 : rxlate"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpwcnt_info1"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_rxpwcnt_info2_rw._rxpwcnt_info1()
            return allFields

    class _upen_poh_pmr_cnt0_rw(AtRegister.AtRegister):
        def name(self):
            return "PMR Error Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_1800 + 16*$offset + $lineid"
            
        def startAddress(self):
            return 0x00091800
            
        def endAddress(self):
            return 0x0009182f

        class _pmr_err_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pmr_err_cnt"
            
            def description(self):
                return "in case of pmr_err_cnt0 side: + offset = 0 : rei_l + offset = 1 : b2 + offset = 2 : b1 in case of pmr_err_cnt1 side: + offset = 0 : rei_l_block_err + offset = 1 : b2_block_err + offset = 2 : b1_block_err"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pmr_err_cnt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_poh_pmr_cnt0_rw._pmr_err_cnt()
            return allFields

    class _upen_poh_pmr_cnt0_rw(AtRegister.AtRegister):
        def name(self):
            return "PMR Error Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_1840 + 16*$offset + $lineid"
            
        def startAddress(self):
            return 0x00091840
            
        def endAddress(self):
            return 0x0009186f

        class _pmr_err_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pmr_err_cnt"
            
            def description(self):
                return "in case of pmr_err_cnt0 side: + offset = 0 : rei_l + offset = 1 : b2 + offset = 2 : b1 in case of pmr_err_cnt1 side: + offset = 0 : rei_l_block_err + offset = 1 : b2_block_err + offset = 2 : b1_block_err"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pmr_err_cnt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_poh_pmr_cnt0_rw._pmr_err_cnt()
            return allFields

    class _upen_sts_pohpm_rw(AtRegister.AtRegister):
        def name(self):
            return "POH Path STS Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_2000 + 8*$stsid + $slcid"
            
        def startAddress(self):
            return 0x00092000
            
        def endAddress(self):
            return 0x000921ff

        class _pohpath_vt_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pohpath_vt_cnt"
            
            def description(self):
                return "in case of pohpath_cnt0 side: + sts_rei in case of pohpath_cnt1 side: + sts_bip(B3)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pohpath_vt_cnt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_sts_pohpm_rw._pohpath_vt_cnt()
            return allFields

    class _upen_sts_pohpm_rw(AtRegister.AtRegister):
        def name(self):
            return "POH Path STS Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_2200 + 8*$stsid + $slcid"
            
        def startAddress(self):
            return 0x00092200
            
        def endAddress(self):
            return 0x000923ff

        class _pohpath_vt_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pohpath_vt_cnt"
            
            def description(self):
                return "in case of pohpath_cnt0 side: + sts_rei in case of pohpath_cnt1 side: + sts_bip(B3)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pohpath_vt_cnt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_sts_pohpm_rw._pohpath_vt_cnt()
            return allFields

    class _upen_vt_pohpm_rw(AtRegister.AtRegister):
        def name(self):
            return "POH Path VT Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x7_0000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid"
            
        def startAddress(self):
            return 0x00070000
            
        def endAddress(self):
            return 0x00073fff

        class _pohpath_vt_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pohpath_vt_cnt"
            
            def description(self):
                return "in case of pohpath_cnt0 side: + vt_rei in case of pohpath_cnt1 side: + vt_bip"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pohpath_vt_cnt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_vt_pohpm_rw._pohpath_vt_cnt()
            return allFields

    class _upen_vt_pohpm_rw(AtRegister.AtRegister):
        def name(self):
            return "POH Path VT Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x7_4000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid"
            
        def startAddress(self):
            return 0x00074000
            
        def endAddress(self):
            return 0x00077fff

        class _pohpath_vt_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pohpath_vt_cnt"
            
            def description(self):
                return "in case of pohpath_cnt0 side: + vt_rei in case of pohpath_cnt1 side: + vt_bip"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pohpath_vt_cnt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_vt_pohpm_rw._pohpath_vt_cnt()
            return allFields

    class _upen_poh_vt_cnt0_rw(AtRegister.AtRegister):
        def name(self):
            return "POH vt pointer Counter 0"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x5_0000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid"
            
        def startAddress(self):
            return 0x00050000
            
        def endAddress(self):
            return 0x00053fff

        class _poh_vt_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "poh_vt_cnt"
            
            def description(self):
                return "in case of poh_vt_cnt0 side: + pohtx_vt_dec in case of poh_vt_cnt1 side: + pohtx_vt_inc"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["poh_vt_cnt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_poh_vt_cnt0_rw._poh_vt_cnt()
            return allFields

    class _upen_poh_vt_cnt0_rw(AtRegister.AtRegister):
        def name(self):
            return "POH vt pointer Counter 0"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x5_4000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid"
            
        def startAddress(self):
            return 0x00054000
            
        def endAddress(self):
            return 0x00057fff

        class _poh_vt_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "poh_vt_cnt"
            
            def description(self):
                return "in case of poh_vt_cnt0 side: + pohtx_vt_dec in case of poh_vt_cnt1 side: + pohtx_vt_inc"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["poh_vt_cnt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_poh_vt_cnt0_rw._poh_vt_cnt()
            return allFields

    class _upen_poh_vt_cnt1_rw(AtRegister.AtRegister):
        def name(self):
            return "POH vt pointer Counter 1"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x6_0000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid"
            
        def startAddress(self):
            return 0x00060000
            
        def endAddress(self):
            return 0x00063fff

        class _poh_vt_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "poh_vt_cnt"
            
            def description(self):
                return "in case of poh_vt_cnt0 side: + pohrx_vt_dec in case of poh_vt_cnt1 side: + pohrx_vt_inc"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["poh_vt_cnt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_poh_vt_cnt1_rw._poh_vt_cnt()
            return allFields

    class _upen_poh_vt_cnt1_rw(AtRegister.AtRegister):
        def name(self):
            return "POH vt pointer Counter 1"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x6_4000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid"
            
        def startAddress(self):
            return 0x00064000
            
        def endAddress(self):
            return 0x00067fff

        class _poh_vt_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "poh_vt_cnt"
            
            def description(self):
                return "in case of poh_vt_cnt0 side: + pohrx_vt_dec in case of poh_vt_cnt1 side: + pohrx_vt_inc"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["poh_vt_cnt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_poh_vt_cnt1_rw._poh_vt_cnt()
            return allFields

    class _upen_poh_sts_cnt0_rw(AtRegister.AtRegister):
        def name(self):
            return "POH sts pointer Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_0800 + 8*$stsid + $slcid"
            
        def startAddress(self):
            return 0x00090800
            
        def endAddress(self):
            return 0x000909ff

        class _poh_sts_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "poh_sts_cnt"
            
            def description(self):
                return "in case of poh_vt_cnt0 side: + pohtx_sts_dec in case of poh_vt_cnt1 side: + pohtx_sts_inc"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["poh_sts_cnt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_poh_sts_cnt0_rw._poh_sts_cnt()
            return allFields

    class _upen_poh_sts_cnt0_rw(AtRegister.AtRegister):
        def name(self):
            return "POH sts pointer Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_0A00 + 8*$stsid + $slcid"
            
        def startAddress(self):
            return 0x00090a00
            
        def endAddress(self):
            return 0x00090bff

        class _poh_sts_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "poh_sts_cnt"
            
            def description(self):
                return "in case of poh_vt_cnt0 side: + pohtx_sts_dec in case of poh_vt_cnt1 side: + pohtx_sts_inc"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["poh_sts_cnt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_poh_sts_cnt0_rw._poh_sts_cnt()
            return allFields

    class _upen_poh_sts_cnt1_rw(AtRegister.AtRegister):
        def name(self):
            return "POH sts pointer Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_1000 + 8*$stsid + $slcid"
            
        def startAddress(self):
            return 0x00091000
            
        def endAddress(self):
            return 0x000911ff

        class _poh_sts_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "poh_sts_cnt"
            
            def description(self):
                return "in case of poh_vt_cnt0 side: + pohrx_sts_dec in case of poh_vt_cnt1 side: + pohrx_sts_inc"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["poh_sts_cnt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_poh_sts_cnt1_rw._poh_sts_cnt()
            return allFields

    class _upen_poh_sts_cnt1_rw(AtRegister.AtRegister):
        def name(self):
            return "POH sts pointer Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_1200 + 8*$stsid + $slcid"
            
        def startAddress(self):
            return 0x00091200
            
        def endAddress(self):
            return 0x000913ff

        class _poh_sts_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "poh_sts_cnt"
            
            def description(self):
                return "in case of poh_vt_cnt0 side: + pohrx_sts_dec in case of poh_vt_cnt1 side: + pohrx_sts_inc"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["poh_sts_cnt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_poh_sts_cnt1_rw._poh_sts_cnt()
            return allFields

    class _upen_pdh_de1cntval30_rw(AtRegister.AtRegister):
        def name(self):
            return "PDH ds1 cntval Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x4_0000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid"
            
        def startAddress(self):
            return 0x00040000
            
        def endAddress(self):
            return 0x00043fff

        class _ds1_cntval_bus1_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ds1_cntval_bus1_cnt"
            
            def description(self):
                return "(rei/fe/crc)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ds1_cntval_bus1_cnt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_pdh_de1cntval30_rw._ds1_cntval_bus1_cnt()
            return allFields

    class _upen_pdh_de1cntval30_rw(AtRegister.AtRegister):
        def name(self):
            return "PDH ds1 cntval Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x4_4000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid"
            
        def startAddress(self):
            return 0x00044000
            
        def endAddress(self):
            return 0x00047fff

        class _ds1_cntval_bus1_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ds1_cntval_bus1_cnt"
            
            def description(self):
                return "(rei/fe/crc)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ds1_cntval_bus1_cnt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_pdh_de1cntval30_rw._ds1_cntval_bus1_cnt()
            return allFields

    class _upen_pdh_de3cnt0_rw(AtRegister.AtRegister):
        def name(self):
            return "PDH ds3 cntval Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_0000 + 8*$stsid + $slcid"
            
        def startAddress(self):
            return 0x00090000
            
        def endAddress(self):
            return 0x000901ff

        class _ds3_cntval_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ds3_cntval_cnt"
            
            def description(self):
                return "(cb/pb/rei/fe)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ds3_cntval_cnt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_pdh_de3cnt0_rw._ds3_cntval_cnt()
            return allFields

    class _upen_pdh_de3cnt0_rw(AtRegister.AtRegister):
        def name(self):
            return "PDH ds3 cntval Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_0200 + 8*$stsid + $slcid"
            
        def startAddress(self):
            return 0x00090200
            
        def endAddress(self):
            return 0x000903ff

        class _ds3_cntval_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ds3_cntval_cnt"
            
            def description(self):
                return "(cb/pb/rei/fe)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ds3_cntval_cnt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_pdh_de3cnt0_rw._ds3_cntval_cnt()
            return allFields

    class _upen_cla0_rw(AtRegister.AtRegister):
        def name(self):
            return "CLA Counter00"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0003b321
            
        def endAddress(self):
            return 0xffffffff

        class _cla_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cla_err"
            
            def description(self):
                return "cla_err"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cla_err"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_cla0_rw._cla_err()
            return allFields

    class _upen_cla0_rw(AtRegister.AtRegister):
        def name(self):
            return "CLA Counter00"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0003b323
            
        def endAddress(self):
            return 0xffffffff

        class _cla_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cla_err"
            
            def description(self):
                return "cla_err"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cla_err"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_cla0_rw._cla_err()
            return allFields

    class _upen_cnttxoam(AtRegister.AtRegister):
        def name(self):
            return "ETH TX Packet OAM CNT"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00093134
            
        def endAddress(self):
            return 0xffffffff

        class _cnttxoamt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnttxoamt"
            
            def description(self):
                return "TX Packet OAM CNT"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnttxoamt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_cnttxoam._cnttxoamt()
            return allFields

    class _upen_cls_bcnt_rw(AtRegister.AtRegister):
        def name(self):
            return "Classifier Bytes Counter vld"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_3140 + $clsid"
            
        def startAddress(self):
            return 0x00093140
            
        def endAddress(self):
            return 0x00093143

        class _bytecnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "bytecnt"
            
            def description(self):
                return "Classifier bytecnt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["bytecnt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_cls_bcnt_rw._bytecnt()
            return allFields

    class _upen_cls_bcnt_rw(AtRegister.AtRegister):
        def name(self):
            return "Classifier Bytes Counter vld"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_3160 + $clsid"
            
        def startAddress(self):
            return 0x00093160
            
        def endAddress(self):
            return 0x00093163

        class _bytecnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "bytecnt"
            
            def description(self):
                return "Classifier bytecnt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["bytecnt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_cls_bcnt_rw._bytecnt()
            return allFields

    class _upen_cls_opcnt0_rw(AtRegister.AtRegister):
        def name(self):
            return "Classifier output counter pkt0"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_B34C + $offset"
            
        def startAddress(self):
            return 0x0003b34c
            
        def endAddress(self):
            return 0x0003b34f

        class _opktcnt0_opktcnt1_(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "opktcnt0_opktcnt1_"
            
            def description(self):
                return "in case of opktcnt0 + offset = 0 : DS1/E1 good pkt counter + offset = 1 : Control pkt good counter + offset = 2 : FCs err cnt in case of opktcnt1 + offset = 0 : DS3/E3/EC1 good pkt counter + offset = 1 : Sequence err  counter + offset = 2 : len pkt err cnt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["opktcnt0_opktcnt1_"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_cls_opcnt0_rw._opktcnt0_opktcnt1_()
            return allFields

    class _upen_cls_opcnt0_rw(AtRegister.AtRegister):
        def name(self):
            return "Classifier output counter pkt0"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB_B34C + $offset"
            
        def startAddress(self):
            return 0x000bb34c
            
        def endAddress(self):
            return 0x000bb34f

        class _opktcnt0_opktcnt1_(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "opktcnt0_opktcnt1_"
            
            def description(self):
                return "in case of opktcnt0 + offset = 0 : DS1/E1 good pkt counter + offset = 1 : Control pkt good counter + offset = 2 : FCs err cnt in case of opktcnt1 + offset = 0 : DS3/E3/EC1 good pkt counter + offset = 1 : Sequence err  counter + offset = 2 : len pkt err cnt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["opktcnt0_opktcnt1_"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_cls_opcnt0_rw._opktcnt0_opktcnt1_()
            return allFields

    class _upen_cls_opcnt1_rw(AtRegister.AtRegister):
        def name(self):
            return "Classifier output counter pkt1"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_B354 + $offset"
            
        def startAddress(self):
            return 0x0003b354
            
        def endAddress(self):
            return 0x0003b357

        class _opktcnt0_opktcnt1_(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "opktcnt0_opktcnt1_"
            
            def description(self):
                return "in case of opktcnt0 + offset = 0 : DA error pkt counter + offset = 1 : E-type err pkt counter + offset = 2 : Unused in case of opktcnt1 + offset = 0 : SA error pkt counter + offset = 1 : Length field err counter + offset = 2 : Unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["opktcnt0_opktcnt1_"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_cls_opcnt1_rw._opktcnt0_opktcnt1_()
            return allFields

    class _upen_cls_opcnt1_rw(AtRegister.AtRegister):
        def name(self):
            return "Classifier output counter pkt1"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB_B354 + $offset"
            
        def startAddress(self):
            return 0x000bb354
            
        def endAddress(self):
            return 0x000bb357

        class _opktcnt0_opktcnt1_(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "opktcnt0_opktcnt1_"
            
            def description(self):
                return "in case of opktcnt0 + offset = 0 : DA error pkt counter + offset = 1 : E-type err pkt counter + offset = 2 : Unused in case of opktcnt1 + offset = 0 : SA error pkt counter + offset = 1 : Length field err counter + offset = 2 : Unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["opktcnt0_opktcnt1_"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_cls_opcnt1_rw._opktcnt0_opktcnt1_()
            return allFields

    class _upen_cls_ipcnt_rw(AtRegister.AtRegister):
        def name(self):
            return "Classifier input counter pkt"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00093136
            
        def endAddress(self):
            return 0xffffffff

        class _ipktcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ipktcnt"
            
            def description(self):
                return "input packet cnt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ipktcnt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_cls_ipcnt_rw._ipktcnt()
            return allFields

    class _upen_cls_ipcnt_rw(AtRegister.AtRegister):
        def name(self):
            return "Classifier input counter pkt"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00093138
            
        def endAddress(self):
            return 0xffffffff

        class _ipktcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ipktcnt"
            
            def description(self):
                return "input packet cnt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ipktcnt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_cls_ipcnt_rw._ipktcnt()
            return allFields

    class _upen_eth_bytecnt_rw(AtRegister.AtRegister):
        def name(self):
            return "PDH Mux Ethernet Byte Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00093120
            
        def endAddress(self):
            return 0xffffffff

        class _eth_bytecnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "eth_bytecnt"
            
            def description(self):
                return "ETH Byte Counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["eth_bytecnt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_eth_bytecnt_rw._eth_bytecnt()
            return allFields

    class _upen_eth_bytecnt_rw(AtRegister.AtRegister):
        def name(self):
            return "PDH Mux Ethernet Byte Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00093122
            
        def endAddress(self):
            return 0xffffffff

        class _eth_bytecnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "eth_bytecnt"
            
            def description(self):
                return "ETH Byte Counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["eth_bytecnt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_eth_bytecnt_rw._eth_bytecnt()
            return allFields

    class _upen_eth_bytecnt_rw(AtRegister.AtRegister):
        def name(self):
            return "PDH Mux Ethernet Packet Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00093126
            
        def endAddress(self):
            return 0xffffffff

        class _eth_pktcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "eth_pktcnt"
            
            def description(self):
                return "ETH Packet Counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["eth_pktcnt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_eth_bytecnt_rw._eth_pktcnt()
            return allFields

    class _upen_eth_bytecnt_rw(AtRegister.AtRegister):
        def name(self):
            return "PDH Mux Ethernet Packet Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00093128
            
        def endAddress(self):
            return 0xffffffff

        class _eth_pktcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "eth_pktcnt"
            
            def description(self):
                return "ETH Packet Counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["eth_pktcnt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_eth_bytecnt_rw._eth_pktcnt()
            return allFields

    class _upen_de1_tdmvld_cnt(AtRegister.AtRegister):
        def name(self):
            return "DE1 TDM bit counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_3000 + $lid"
            
        def startAddress(self):
            return 0x00093000
            
        def endAddress(self):
            return 0x0009307f

        class _de1_bit_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "de1_bit_cnt"
            
            def description(self):
                return "bit_cnt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["de1_bit_cnt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_de1_tdmvld_cnt._de1_bit_cnt()
            return allFields

    class _upen_de3_tdmvld_cnt(AtRegister.AtRegister):
        def name(self):
            return "DE3 TDM bit counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_3100 + $lid"
            
        def startAddress(self):
            return 0x00093100
            
        def endAddress(self):
            return 0x0009311f

        class _de3_bit_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "de3_bit_cnt"
            
            def description(self):
                return "bit_cnt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["de3_bit_cnt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_de3_tdmvld_cnt._de3_bit_cnt()
            return allFields

    class _upen_ge00lltx_typepkt_rw(AtRegister.AtRegister):
        def name(self):
            return "MACGE tx type packet"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00093080
            
        def endAddress(self):
            return 0xffffffff

        class _txpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txpkt"
            
            def description(self):
                return "txpkt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txpkt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_ge00lltx_typepkt_rw._txpkt()
            return allFields

    class _upen_ge00lltx_typepkt_rw(AtRegister.AtRegister):
        def name(self):
            return "MACGE tx type packet"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00093081
            
        def endAddress(self):
            return 0xffffffff

        class _txpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txpkt"
            
            def description(self):
                return "txpkt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txpkt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_ge00lltx_typepkt_rw._txpkt()
            return allFields

    class _upen_ge00lltx_pkt(AtRegister.AtRegister):
        def name(self):
            return "MACGE tx type packet"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00093087
            
        def endAddress(self):
            return 0xffffffff

        class _cnttxpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnttxpkt"
            
            def description(self):
                return "tx Packet Counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnttxpkt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_ge00lltx_pkt._cnttxpkt()
            return allFields

    class _upen_ge00lltx_byte(AtRegister.AtRegister):
        def name(self):
            return "MACGE tx type packet"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00093088
            
        def endAddress(self):
            return 0xffffffff

        class _cnttxbyte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnttxbyte"
            
            def description(self):
                return "tx Byte Counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnttxbyte"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_ge00lltx_byte._cnttxbyte()
            return allFields

    class _upen_ge00llrx_typepkt_rw(AtRegister.AtRegister):
        def name(self):
            return "MACGE rx type packet"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00093090
            
        def endAddress(self):
            return 0xffffffff

        class _rxpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpkt"
            
            def description(self):
                return "rxpkt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpkt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_ge00llrx_typepkt_rw._rxpkt()
            return allFields

    class _upen_ge00llrx_typepkt_rw(AtRegister.AtRegister):
        def name(self):
            return "MACGE rx type packet"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00093091
            
        def endAddress(self):
            return 0xffffffff

        class _rxpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpkt"
            
            def description(self):
                return "rxpkt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpkt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_ge00llrx_typepkt_rw._rxpkt()
            return allFields

    class _upen_ge00llrx_pkt(AtRegister.AtRegister):
        def name(self):
            return "MACGE rx type packet"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00093097
            
        def endAddress(self):
            return 0xffffffff

        class _cntrxpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntrxpkt"
            
            def description(self):
                return "rx Packet Counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntrxpkt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_ge00llrx_pkt._cntrxpkt()
            return allFields

    class _upen_ge00llrx_byte(AtRegister.AtRegister):
        def name(self):
            return "MACGE rx type packet"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00093098
            
        def endAddress(self):
            return 0xffffffff

        class _cntrxbyte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntrxbyte"
            
            def description(self):
                return "rx Byte Counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntrxbyte"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_ge00llrx_byte._cntrxbyte()
            return allFields

    class _upen_ge10lltx_typepkt_rw(AtRegister.AtRegister):
        def name(self):
            return "MACTGE tx type packet"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000930a0
            
        def endAddress(self):
            return 0xffffffff

        class _txpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txpkt"
            
            def description(self):
                return "txpkt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txpkt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_ge10lltx_typepkt_rw._txpkt()
            return allFields

    class _upen_ge10lltx_typepkt_rw(AtRegister.AtRegister):
        def name(self):
            return "MACTGE tx type packet"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000930a1
            
        def endAddress(self):
            return 0xffffffff

        class _txpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txpkt"
            
            def description(self):
                return "txpkt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txpkt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_ge10lltx_typepkt_rw._txpkt()
            return allFields

    class _upen_ge10lltx_pkt(AtRegister.AtRegister):
        def name(self):
            return "MACTGE tx type packet"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000930a7
            
        def endAddress(self):
            return 0xffffffff

        class _cnttxpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnttxpkt"
            
            def description(self):
                return "tx Packet Counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnttxpkt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_ge10lltx_pkt._cnttxpkt()
            return allFields

    class _upen_ge10lltx_byte(AtRegister.AtRegister):
        def name(self):
            return "MACTGE tx type packet"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000930a8
            
        def endAddress(self):
            return 0xffffffff

        class _cnttxbyte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnttxbyte"
            
            def description(self):
                return "tx Byte Counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnttxbyte"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_ge10lltx_byte._cnttxbyte()
            return allFields

    class _upen_ge10llrx_typepkt_rw(AtRegister.AtRegister):
        def name(self):
            return "MACTGE rx type packet"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000930b0
            
        def endAddress(self):
            return 0xffffffff

        class _rxpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpkt"
            
            def description(self):
                return "rxpkt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpkt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_ge10llrx_typepkt_rw._rxpkt()
            return allFields

    class _upen_ge10llrx_typepkt_rw(AtRegister.AtRegister):
        def name(self):
            return "MACTGE rx type packet"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000930b1
            
        def endAddress(self):
            return 0xffffffff

        class _rxpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpkt"
            
            def description(self):
                return "rxpkt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpkt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_ge10llrx_typepkt_rw._rxpkt()
            return allFields

    class _upen_ge10llrx_pkt(AtRegister.AtRegister):
        def name(self):
            return "MACTGE rx type packet"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000930b7
            
        def endAddress(self):
            return 0xffffffff

        class _cntrxpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntrxpkt"
            
            def description(self):
                return "rx Packet Counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntrxpkt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_ge10llrx_pkt._cntrxpkt()
            return allFields

    class _upen_ge10llrx_byte(AtRegister.AtRegister):
        def name(self):
            return "MACTGE rx type packet"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000930b8
            
        def endAddress(self):
            return 0xffffffff

        class _cntrxbyte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntrxbyte"
            
            def description(self):
                return "rx Byte Counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntrxbyte"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_ge10llrx_byte._cntrxbyte()
            return allFields

    class _upen_dimtx_typepkt_rw(AtRegister.AtRegister):
        def name(self):
            return "MACDIM tx type packet"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000930c0
            
        def endAddress(self):
            return 0xffffffff

        class _txpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txpkt"
            
            def description(self):
                return "txpkt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txpkt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_dimtx_typepkt_rw._txpkt()
            return allFields

    class _upen_dimtx_typepkt_rw(AtRegister.AtRegister):
        def name(self):
            return "MACDIM tx type packet"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000930c1
            
        def endAddress(self):
            return 0xffffffff

        class _txpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txpkt"
            
            def description(self):
                return "txpkt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txpkt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_dimtx_typepkt_rw._txpkt()
            return allFields

    class _upen_dimtx_pkt(AtRegister.AtRegister):
        def name(self):
            return "MACDIM tx type packet"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000930c7
            
        def endAddress(self):
            return 0xffffffff

        class _cnttxpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnttxpkt"
            
            def description(self):
                return "tx Packet Counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnttxpkt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_dimtx_pkt._cnttxpkt()
            return allFields

    class _upen_dimtx_byte(AtRegister.AtRegister):
        def name(self):
            return "MACDIM tx type packet"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000930c8
            
        def endAddress(self):
            return 0xffffffff

        class _cnttxbyte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnttxbyte"
            
            def description(self):
                return "tx Byte Counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnttxbyte"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_dimtx_byte._cnttxbyte()
            return allFields

    class _upen_dimrx_typepkt_rw(AtRegister.AtRegister):
        def name(self):
            return "MACDIM rx type packet"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000930d0
            
        def endAddress(self):
            return 0xffffffff

        class _rxpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpkt"
            
            def description(self):
                return "rxpkt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpkt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_dimrx_typepkt_rw._rxpkt()
            return allFields

    class _upen_dimrx_typepkt_rw(AtRegister.AtRegister):
        def name(self):
            return "MACDIM rx type packet"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000930d1
            
        def endAddress(self):
            return 0xffffffff

        class _rxpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpkt"
            
            def description(self):
                return "rxpkt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpkt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_dimrx_typepkt_rw._rxpkt()
            return allFields

    class _upen_dimrx_pkt(AtRegister.AtRegister):
        def name(self):
            return "MACDIM rx type packet"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000930d7
            
        def endAddress(self):
            return 0xffffffff

        class _cntrxpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntrxpkt"
            
            def description(self):
                return "rx Packet Counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntrxpkt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_dimrx_pkt._cntrxpkt()
            return allFields

    class _upen_dimrx_byte(AtRegister.AtRegister):
        def name(self):
            return "MACDIM rx type packet"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000930d8
            
        def endAddress(self):
            return 0xffffffff

        class _cntrxbyte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntrxbyte"
            
            def description(self):
                return "rx Byte Counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntrxbyte"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_dimrx_byte._cntrxbyte()
            return allFields

    class _upen_mac2_5tx_typepkt_rw(AtRegister.AtRegister):
        def name(self):
            return "MAC2_5 tx type packet"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000930e0
            
        def endAddress(self):
            return 0xffffffff

        class _txpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txpkt"
            
            def description(self):
                return "txpkt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txpkt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_mac2_5tx_typepkt_rw._txpkt()
            return allFields

    class _upen_mac2_5tx_typepkt_rw(AtRegister.AtRegister):
        def name(self):
            return "MAC2_5 tx type packet"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000930e1
            
        def endAddress(self):
            return 0xffffffff

        class _txpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txpkt"
            
            def description(self):
                return "txpkt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txpkt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_mac2_5tx_typepkt_rw._txpkt()
            return allFields

    class _upen_mac2_5tx_pkt(AtRegister.AtRegister):
        def name(self):
            return "MAC2_5 tx type packet"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000930e7
            
        def endAddress(self):
            return 0xffffffff

        class _cnttxpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnttxpkt"
            
            def description(self):
                return "tx Packet Counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnttxpkt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_mac2_5tx_pkt._cnttxpkt()
            return allFields

    class _upen_mac2_5tx_byte(AtRegister.AtRegister):
        def name(self):
            return "MAC2_5 tx type packet"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000930e8
            
        def endAddress(self):
            return 0xffffffff

        class _cnttxbyte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnttxbyte"
            
            def description(self):
                return "tx Byte Counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnttxbyte"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_mac2_5tx_byte._cnttxbyte()
            return allFields

    class _upen_mac2_5rx_typepkt_rw(AtRegister.AtRegister):
        def name(self):
            return "MAC2_5 rx type packet"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000930f0
            
        def endAddress(self):
            return 0xffffffff

        class _rxpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpkt"
            
            def description(self):
                return "rxpkt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpkt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_mac2_5rx_typepkt_rw._rxpkt()
            return allFields

    class _upen_mac2_5rx_typepkt_rw(AtRegister.AtRegister):
        def name(self):
            return "MAC2_5 rx type packet"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000930f1
            
        def endAddress(self):
            return 0xffffffff

        class _rxpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpkt"
            
            def description(self):
                return "rxpkt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpkt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_mac2_5rx_typepkt_rw._rxpkt()
            return allFields

    class _upen_ge2_5rx_pkt(AtRegister.AtRegister):
        def name(self):
            return "MAC2_5 rx type packet"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000930f7
            
        def endAddress(self):
            return 0xffffffff

        class _cntrxpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntrxpkt"
            
            def description(self):
                return "rx Packet Counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntrxpkt"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_ge2_5rx_pkt._cntrxpkt()
            return allFields

    class _upen_mac2_5rx_byte(AtRegister.AtRegister):
        def name(self):
            return "MAC2_5 rx type packet"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000930f8
            
        def endAddress(self):
            return 0xffffffff

        class _cntrxbyte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntrxbyte"
            
            def description(self):
                return "rx Byte Counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntrxbyte"] = _AF6CNC0011_RD_GLBPMC_ver2._upen_mac2_5rx_byte._cntrxbyte()
            return allFields

import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0011_RD_PDH_MDLPRM(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["upen_cfg_debug"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_cfg_debug()
        allRegisters["upen_cfgid_mon"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_cfgid_mon()
        allRegisters["upen_prm_txcfgcr"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_prm_txcfgcr()
        allRegisters["upen_prm_txcfglb"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_prm_txcfglb()
        allRegisters["upen_cfg_ctrl0"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_cfg_ctrl0()
        allRegisters["upen_txprm_cnt_byte"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_txprm_cnt_byte()
        allRegisters["upen_txprm_cnt_pkt"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_txprm_cnt_pkt()
        allRegisters["upen_txprm_cnt_info"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_txprm_cnt_info()
        allRegisters["upen_mdl_idle1"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_mdl_idle1()
        allRegisters["upen_mdl_idle2"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_mdl_idle2()
        allRegisters["upen_sta_idle_alren"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_sta_idle_alren()
        allRegisters["upen_cfg_mdl"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_cfg_mdl()
        allRegisters["upen_txmdl_cnt_byteidle"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_txmdl_cnt_byteidle()
        allRegisters["upen_txmdl_cnt_bytepath"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_txmdl_cnt_bytepath()
        allRegisters["upen_txmdl_cnt_bytetest"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_txmdl_cnt_bytetest()
        allRegisters["upen_txmdl_cnt_valididle"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_txmdl_cnt_valididle()
        allRegisters["upen_txmdl_cnt_validpath"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_txmdl_cnt_validpath()
        allRegisters["upen_txmdl_cnt_validtest"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_txmdl_cnt_validtest()
        allRegisters["upen_rxprm_cfgstd"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxprm_cfgstd()
        allRegisters["upen_prm_ctrl"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_prm_ctrl()
        allRegisters["upen_rxprm_mon"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxprm_mon()
        allRegisters["upen_rxmdl_typebuff1"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxmdl_typebuff1()
        allRegisters["upen_rxmdl_cfgtype"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxmdl_cfgtype()
        allRegisters["upen_destuff_ctrl0"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_destuff_ctrl0()
        allRegisters["upen_mdl_stk_cfg1"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_mdl_stk_cfg1()
        allRegisters["upen_rxprm_gmess"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxprm_gmess()
        allRegisters["upen_rxprm_drmess"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxprm_drmess()
        allRegisters["upen_rxprm_mmess"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxprm_mmess()
        allRegisters["upen_rxprm_cnt_byte"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxprm_cnt_byte()
        allRegisters["upen_rxmdl_cnt_byteidle"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxmdl_cnt_byteidle()
        allRegisters["upen_rxmdl_cnt_bytepath"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxmdl_cnt_bytepath()
        allRegisters["upen_rxmdl_cnt_bytetest"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxmdl_cnt_bytetest()
        allRegisters["upen_rxmdl_cnt_goodidle"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxmdl_cnt_goodidle()
        allRegisters["upen_rxmdl_cnt_goodpath"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxmdl_cnt_goodpath()
        allRegisters["upen_rxmdl_cnt_goodtest"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxmdl_cnt_goodtest()
        allRegisters["upen_rxmdl_cntr2c_dropidle"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxmdl_cntr2c_dropidle()
        allRegisters["upen_rxmdl_cnt_droppath"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxmdl_cnt_droppath()
        allRegisters["upen_rxmdl_cnt_droptest"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxmdl_cnt_droptest()
        allRegisters["prm_cfg_lben_int"] = _AF6CNC0011_RD_PDH_MDLPRM._prm_cfg_lben_int()
        allRegisters["prm_lb_int_sta"] = _AF6CNC0011_RD_PDH_MDLPRM._prm_lb_int_sta()
        allRegisters["prm_lb_int_crrsta"] = _AF6CNC0011_RD_PDH_MDLPRM._prm_lb_int_crrsta()
        allRegisters["prm_lb_intsta"] = _AF6CNC0011_RD_PDH_MDLPRM._prm_lb_intsta()
        allRegisters["prm_lb_sta_int"] = _AF6CNC0011_RD_PDH_MDLPRM._prm_lb_sta_int()
        allRegisters["prm_lb_en_int"] = _AF6CNC0011_RD_PDH_MDLPRM._prm_lb_en_int()
        allRegisters["mdl_cfgen_int"] = _AF6CNC0011_RD_PDH_MDLPRM._mdl_cfgen_int()
        allRegisters["mdl_int_sta"] = _AF6CNC0011_RD_PDH_MDLPRM._mdl_int_sta()
        allRegisters["mdl_int_crrsta"] = _AF6CNC0011_RD_PDH_MDLPRM._mdl_int_crrsta()
        allRegisters["mdl_intsta"] = _AF6CNC0011_RD_PDH_MDLPRM._mdl_intsta()
        allRegisters["mdl_sta_int"] = _AF6CNC0011_RD_PDH_MDLPRM._mdl_sta_int()
        allRegisters["mdl_en_int"] = _AF6CNC0011_RD_PDH_MDLPRM._mdl_en_int()
        return allRegisters

    class _upen_cfg_debug(AtRegister.AtRegister):
        def name(self):
            return "CONFIG ENABLE MONITOR FOR DEBUG BOARD"
    
        def description(self):
            return "config enable monitor data before stuff"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00014001
            
        def endAddress(self):
            return 0xffffffff

        class _out_cfg_bar(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 19
        
            def name(self):
                return "out_cfg_bar"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _out_cfg_selinfo1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "out_cfg_selinfo1"
            
            def description(self):
                return "select just 8 bit INFO from PRM FORCE but except 2 bit NmNi, (1) is enable, (0) is disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_loopaback_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "cfg_loopaback_mdl"
            
            def description(self):
                return "loop back MDL DS3, (1) is enable, (0) is disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_loopaback_prm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "cfg_loopaback_prm"
            
            def description(self):
                return "loop back PRM DS1, (1) is enable, (0) is disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _out_info_force(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "out_info_force"
            
            def description(self):
                return "10 bits info force [15:13] : 3'd1 : to set bit G1 3'd2 :  to set bit G2 3'd3 :  to set bit G3 3'd4 : to set bit G4 3'd5 : to set bit G5 3'd6 :  to set bit G6 [12] : to set bit SE [11] : to set bit FE [10] : to set bit LV [9] : to set bit SL [8] : to set bit LB"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 1
        
            def name(self):
                return "reserve"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _out_cfg_dis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "out_cfg_dis"
            
            def description(self):
                return "disable monitor, reset address buffer monitor, (1) is disable, (0) is enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["out_cfg_bar"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_cfg_debug._out_cfg_bar()
            allFields["out_cfg_selinfo1"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_cfg_debug._out_cfg_selinfo1()
            allFields["cfg_loopaback_mdl"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_cfg_debug._cfg_loopaback_mdl()
            allFields["cfg_loopaback_prm"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_cfg_debug._cfg_loopaback_prm()
            allFields["out_info_force"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_cfg_debug._out_info_force()
            allFields["reserve"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_cfg_debug._reserve()
            allFields["out_cfg_dis"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_cfg_debug._out_cfg_dis()
            return allFields

    class _upen_cfgid_mon(AtRegister.AtRegister):
        def name(self):
            return "CONFIG ID TO MONITOR FOR DEBUG BOARD"
    
        def description(self):
            return "config ID to monitor tx message"
            
        def width(self):
            return 10
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00014002
            
        def endAddress(self):
            return 0xffffffff

        class _out_cfgid_mon(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "out_cfgid_mon"
            
            def description(self):
                return "ID which is configured to monitor"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["out_cfgid_mon"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_cfgid_mon._out_cfgid_mon()
            return allFields

    class _upen_prm_txcfgcr(AtRegister.AtRegister):
        def name(self):
            return "CONFIG PRM BIT C/R & STANDARD Tx"
    
        def description(self):
            return "Config PRM bit C/R & Standard"
            
        def width(self):
            return 4
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1000+ $STSID*32 + *$VTGID*4 + $VTID"
            
        def startAddress(self):
            return 0x00001000
            
        def endAddress(self):
            return 0x0000153f

        class _cfg_prmen_tx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "cfg_prmen_tx"
            
            def description(self):
                return "config enable Tx, (0) is disable (1) is enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_prmfcs_tx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "cfg_prmfcs_tx"
            
            def description(self):
                return "config mode transmit FCS, (1) is T.403-MSB, (0) is T.107-LSB"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_prmstd_tx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfg_prmstd_tx"
            
            def description(self):
                return "config standard Tx, (0) is ANSI, (1) is AT&T"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_prm_cr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_prm_cr"
            
            def description(self):
                return "config bit command/respond PRM message"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_prmen_tx"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_prm_txcfgcr._cfg_prmen_tx()
            allFields["cfg_prmfcs_tx"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_prm_txcfgcr._cfg_prmfcs_tx()
            allFields["cfg_prmstd_tx"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_prm_txcfgcr._cfg_prmstd_tx()
            allFields["cfg_prm_cr"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_prm_txcfgcr._cfg_prm_cr()
            return allFields

    class _upen_prm_txcfglb(AtRegister.AtRegister):
        def name(self):
            return "CONFIG PRM BIT L/B Tx"
    
        def description(self):
            return "CONFIG PRM BIT L/B Tx"
            
        def width(self):
            return 2
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x1800+$STSID*32 + $VTGID*4 + $VTID"
            
        def startAddress(self):
            return 0x00001800
            
        def endAddress(self):
            return 0x00001d3f

        class _cfg_prm_enlb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfg_prm_enlb"
            
            def description(self):
                return "config enable CPU config LB bit, (0) is disable, (1) is enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_prm_lb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_prm_lb"
            
            def description(self):
                return "config bit Loopback PRM message"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_prm_enlb"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_prm_txcfglb._cfg_prm_enlb()
            allFields["cfg_prm_lb"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_prm_txcfglb._cfg_prm_lb()
            return allFields

    class _upen_cfg_ctrl0(AtRegister.AtRegister):
        def name(self):
            return "CONFIG CONTROL STUFF 0"
    
        def description(self):
            return "config control Stuff global"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00003000
            
        def endAddress(self):
            return 0xffffffff

        class _out_txcfg_ctrl0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 7
        
            def name(self):
                return "out_txcfg_ctrl0"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _reserve1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 2
        
            def name(self):
                return "reserve1"
            
            def description(self):
                return "reserve1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fcsinscfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "fcsinscfg"
            
            def description(self):
                return "(1) enable insert FCS, (0) disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "reserve"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["out_txcfg_ctrl0"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_cfg_ctrl0._out_txcfg_ctrl0()
            allFields["reserve1"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_cfg_ctrl0._reserve1()
            allFields["fcsinscfg"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_cfg_ctrl0._fcsinscfg()
            allFields["reserve"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_cfg_ctrl0._reserve()
            return allFields

    class _upen_txprm_cnt_byte(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE TX PRM"
    
        def description(self):
            return "counter byte message PRM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x4000 + $STSID*32 + $VTGID*4 + $VTID + $UPRO * 1024 "
            
        def startAddress(self):
            return 0x00004000
            
        def endAddress(self):
            return 0x00004d3f

        class _cnt_byte_prm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_byte_prm"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_byte_prm"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_txprm_cnt_byte._cnt_byte_prm()
            return allFields

    class _upen_txprm_cnt_pkt(AtRegister.AtRegister):
        def name(self):
            return "COUNTER PACKET MESSAGE TX PRM"
    
        def description(self):
            return "counter packet message PRM"
            
        def width(self):
            return 15
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x5000+ $STSID*32 + $VTGID*4 + $VTID + $UPRO * 1024 "
            
        def startAddress(self):
            return 0x00005000
            
        def endAddress(self):
            return 0x00005d3f

        class _cnt_pkt_prm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_pkt_prm"
            
            def description(self):
                return "value counter packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_pkt_prm"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_txprm_cnt_pkt._cnt_pkt_prm()
            return allFields

    class _upen_txprm_cnt_info(AtRegister.AtRegister):
        def name(self):
            return "COUNTER VALID INFO TX PRM"
    
        def description(self):
            return "counter valid info PRM"
            
        def width(self):
            return 15
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x6000 + $STSID*32 + $VTGID*4 + $VTID + $UPRO * 1024 "
            
        def startAddress(self):
            return 0x00006000
            
        def endAddress(self):
            return 0x00006d3f

        class _cnt_vld_prm_info(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_vld_prm_info"
            
            def description(self):
                return "value counter valid info"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_vld_prm_info"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_txprm_cnt_info._cnt_vld_prm_info()
            return allFields

    class _upen_mdl_idle1(AtRegister.AtRegister):
        def name(self):
            return "Buff Message MDL IDLE1"
    
        def description(self):
            return "config message MDL IDLE Channel ID 0-15"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0000+ $DWORDID*16 + $DE3ID1"
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0x0000012f

        class _idle_byte13(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "idle_byte13"
            
            def description(self):
                return "MS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _ilde_byte12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ilde_byte12"
            
            def description(self):
                return "BYTE 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _idle_byte11(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "idle_byte11"
            
            def description(self):
                return "BYTE 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _idle_byte10(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "idle_byte10"
            
            def description(self):
                return "LS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["idle_byte13"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_mdl_idle1._idle_byte13()
            allFields["ilde_byte12"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_mdl_idle1._ilde_byte12()
            allFields["idle_byte11"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_mdl_idle1._idle_byte11()
            allFields["idle_byte10"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_mdl_idle1._idle_byte10()
            return allFields

    class _upen_mdl_idle2(AtRegister.AtRegister):
        def name(self):
            return "Buff Message MDL IDLE2"
    
        def description(self):
            return "config message MDL IDLE Channel ID 16-23"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0130+ $DWORDID*8 + $DE3ID2"
            
        def startAddress(self):
            return 0x00000130
            
        def endAddress(self):
            return 0x000001c7

        class _idle_byte23(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "idle_byte23"
            
            def description(self):
                return "MS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _ilde_byte22(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ilde_byte22"
            
            def description(self):
                return "BYTE 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _idle_byte21(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "idle_byte21"
            
            def description(self):
                return "BYTE 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _idle_byte20(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "idle_byte20"
            
            def description(self):
                return "LS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["idle_byte23"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_mdl_idle2._idle_byte23()
            allFields["ilde_byte22"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_mdl_idle2._ilde_byte22()
            allFields["idle_byte21"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_mdl_idle2._idle_byte21()
            allFields["idle_byte20"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_mdl_idle2._idle_byte20()
            return allFields

    class _upen_sta_idle_alren(AtRegister.AtRegister):
        def name(self):
            return "SET TO HAVE PACKET MDL BUFFER 1"
    
        def description(self):
            return "SET/CLEAR to ALRM STATUS MESSAGE in BUFFER 1"
            
        def width(self):
            return 1
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0420+$DE3ID"
            
        def startAddress(self):
            return 0x00000420
            
        def endAddress(self):
            return 0x00000437

        class _idle_cfgen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "idle_cfgen"
            
            def description(self):
                return "(0) : engine clear for indication to have sent, (1) CPU set for indication to have new message which must send for buffer 0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["idle_cfgen"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_sta_idle_alren._idle_cfgen()
            return allFields

    class _upen_cfg_mdl(AtRegister.AtRegister):
        def name(self):
            return "CONFIG MDL Tx"
    
        def description(self):
            return "Config Tx MDL"
            
        def width(self):
            return 6
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0460+$DE3ID"
            
        def startAddress(self):
            return 0x00000460
            
        def endAddress(self):
            return 0x00000477

        class _reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "reserve"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_seq_tx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "cfg_seq_tx"
            
            def description(self):
                return "config enable Tx continous, (0) is disable, (1) is enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_entx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "cfg_entx"
            
            def description(self):
                return "config enable Tx, (0) is disable, (1) is enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_fcs_tx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "cfg_fcs_tx"
            
            def description(self):
                return "config mode transmit FCS, (1) is T.403-MSB, (0) is T.107-LSB"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_mdlstd_tx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfg_mdlstd_tx"
            
            def description(self):
                return "config standard Tx, (0) is ANSI, (1) is AT&T"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_mdl_cr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_mdl_cr"
            
            def description(self):
                return "config bit command/respond MDL message, bit 0 is channel 0 of DS3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["reserve"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_cfg_mdl._reserve()
            allFields["cfg_seq_tx"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_cfg_mdl._cfg_seq_tx()
            allFields["cfg_entx"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_cfg_mdl._cfg_entx()
            allFields["cfg_fcs_tx"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_cfg_mdl._cfg_fcs_tx()
            allFields["cfg_mdlstd_tx"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_cfg_mdl._cfg_mdlstd_tx()
            allFields["cfg_mdl_cr"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_cfg_mdl._cfg_mdl_cr()
            return allFields

    class _upen_txmdl_cnt_byteidle(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE TX MDL IDLE"
    
        def description(self):
            return "counter byte  IDLE message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0700+ $DE3ID + $UPRO * 128"
            
        def startAddress(self):
            return 0x00000700
            
        def endAddress(self):
            return 0x00000797

        class _cnt_byte_idle_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_byte_idle_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_byte_idle_mdl"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_txmdl_cnt_byteidle._cnt_byte_idle_mdl()
            return allFields

    class _upen_txmdl_cnt_bytepath(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE TX MDL PATH"
    
        def description(self):
            return "counter byte  PATH message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0720+ $DE3ID + $UPRO * 128"
            
        def startAddress(self):
            return 0x00000720
            
        def endAddress(self):
            return 0x000007b7

        class _cnt_byte_path_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_byte_path_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_byte_path_mdl"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_txmdl_cnt_bytepath._cnt_byte_path_mdl()
            return allFields

    class _upen_txmdl_cnt_bytetest(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE TX MDL TEST"
    
        def description(self):
            return "counter byte  TEST message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0740+ $DE3ID + $UPRO * 128"
            
        def startAddress(self):
            return 0x00000740
            
        def endAddress(self):
            return 0x000007d7

        class _cnt_byte_test_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_byte_test_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_byte_test_mdl"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_txmdl_cnt_bytetest._cnt_byte_test_mdl()
            return allFields

    class _upen_txmdl_cnt_valididle(AtRegister.AtRegister):
        def name(self):
            return "COUNTER VALID MESSAGE TX MDL IDLE"
    
        def description(self):
            return "counter valid  IDLE message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0600+ $DE3ID + $UPRO * 128"
            
        def startAddress(self):
            return 0x00000600
            
        def endAddress(self):
            return 0x00000697

        class _cnt_valid_idle_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_valid_idle_mdl"
            
            def description(self):
                return "value counter valid"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_valid_idle_mdl"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_txmdl_cnt_valididle._cnt_valid_idle_mdl()
            return allFields

    class _upen_txmdl_cnt_validpath(AtRegister.AtRegister):
        def name(self):
            return "COUNTER VALID MESSAGE TX MDL PATH"
    
        def description(self):
            return "counter valid PATH message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0620+ $DE3ID + $UPRO * 128"
            
        def startAddress(self):
            return 0x00000620
            
        def endAddress(self):
            return 0x000006b7

        class _cnt_valid_path_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_valid_path_mdl"
            
            def description(self):
                return "value counter valid"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_valid_path_mdl"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_txmdl_cnt_validpath._cnt_valid_path_mdl()
            return allFields

    class _upen_txmdl_cnt_validtest(AtRegister.AtRegister):
        def name(self):
            return "COUNTER VALID MESSAGE TX MDL TEST"
    
        def description(self):
            return "counter valid  TEST message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0640+ $DE3ID + $UPRO * 128"
            
        def startAddress(self):
            return 0x00000640
            
        def endAddress(self):
            return 0x000006d7

        class _cnt_valid_test_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_valid_test_mdl"
            
            def description(self):
                return "value counter valid"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_valid_test_mdl"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_txmdl_cnt_validtest._cnt_valid_test_mdl()
            return allFields

    class _upen_rxprm_cfgstd(AtRegister.AtRegister):
        def name(self):
            return "Config Buff Message PRM STANDARD"
    
        def description(self):
            return "config standard message PRM"
            
        def width(self):
            return 4
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x08000+ $STSID*32 + *$VTGID*4 + $VTID"
            
        def startAddress(self):
            return 0x00008000
            
        def endAddress(self):
            return 0x0000853f

        class _cfg_prm_rxen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "cfg_prm_rxen"
            
            def description(self):
                return "config enable RX PRM, (0) is disable, (1) is enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_prm_cr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "cfg_prm_cr"
            
            def description(self):
                return "config C/R bit expected"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_prmfcs_rx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfg_prmfcs_rx"
            
            def description(self):
                return "config mode receive FCS, (1) is T.403-MSB, (0) is T.107-LSB"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_std_prm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_std_prm"
            
            def description(self):
                return "config standard   (0) is ANSI, (1) is AT&T"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_prm_rxen"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxprm_cfgstd._cfg_prm_rxen()
            allFields["cfg_prm_cr"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxprm_cfgstd._cfg_prm_cr()
            allFields["cfg_prmfcs_rx"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxprm_cfgstd._cfg_prmfcs_rx()
            allFields["cfg_std_prm"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxprm_cfgstd._cfg_std_prm()
            return allFields

    class _upen_prm_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CONFIG CONTROL RX PRM MONITOR"
    
        def description(self):
            return "config control DeStuff global"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000a042
            
        def endAddress(self):
            return 0xffffffff

        class _reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 14
        
            def name(self):
                return "reserve"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _prmid_cfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 4
        
            def name(self):
                return "prmid_cfg"
            
            def description(self):
                return "PRM ID config monitor,"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 1
        
            def name(self):
                return "reserve"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _prm_mon_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "prm_mon_en"
            
            def description(self):
                return "(1) : enable monitor, (0) disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["reserve"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_prm_ctrl._reserve()
            allFields["prmid_cfg"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_prm_ctrl._prmid_cfg()
            allFields["reserve"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_prm_ctrl._reserve()
            allFields["prm_mon_en"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_prm_ctrl._prm_mon_en()
            return allFields

    class _upen_rxprm_mon(AtRegister.AtRegister):
        def name(self):
            return "BUFFER RX PRM MONITOR MESSAGE"
    
        def description(self):
            return "config standard message PRM"
            
        def width(self):
            return 4
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x08800+ $LOC"
            
        def startAddress(self):
            return 0x00008800
            
        def endAddress(self):
            return 0x0000887f

        class _val_mes(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "val_mes"
            
            def description(self):
                return "value message of PRM"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["val_mes"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxprm_mon._val_mes()
            return allFields

    class _upen_rxmdl_typebuff1(AtRegister.AtRegister):
        def name(self):
            return "Buff Message MDL with configuration type1"
    
        def description(self):
            return "buffer message MDL which is configured type"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9800 + $STSID* 32 + $RXDWORDID"
            
        def startAddress(self):
            return 0x00009800
            
        def endAddress(self):
            return 0x00009af3

        class _mdl_tbyte3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "mdl_tbyte3"
            
            def description(self):
                return "MS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _mdl_tbyte2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "mdl_tbyte2"
            
            def description(self):
                return "BYTE 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _mdl_tbyte1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "mdl_tbyte1"
            
            def description(self):
                return "BYTE 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _mdl_tbyte0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdl_tbyte0"
            
            def description(self):
                return "LS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdl_tbyte3"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxmdl_typebuff1._mdl_tbyte3()
            allFields["mdl_tbyte2"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxmdl_typebuff1._mdl_tbyte2()
            allFields["mdl_tbyte1"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxmdl_typebuff1._mdl_tbyte1()
            allFields["mdl_tbyte0"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxmdl_typebuff1._mdl_tbyte0()
            return allFields

    class _upen_rxmdl_cfgtype(AtRegister.AtRegister):
        def name(self):
            return "Config Buff Message MDL TYPE"
    
        def description(self):
            return "config type message MDL"
            
        def width(self):
            return 6
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0A000+$MDLID"
            
        def startAddress(self):
            return 0x0000a000
            
        def endAddress(self):
            return 0x0000a017

        class _cfg_fcs_rx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "cfg_fcs_rx"
            
            def description(self):
                return "config mode receive FCS, (1) is T.403-MSB, (0) is T.107-LSB"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_mdl_cr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "cfg_mdl_cr"
            
            def description(self):
                return "config C/R bit expected"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_mdl_std(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "cfg_mdl_std"
            
            def description(self):
                return "(0) is ANSI, (1) is AT&T"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_mdl_mask_test(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "cfg_mdl_mask_test"
            
            def description(self):
                return "config enable mask to moitor test massage (1): enable, (0): disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_mdl_mask_path(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfg_mdl_mask_path"
            
            def description(self):
                return "config enable mask to moitor path massage (1): enable, (0): disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_mdl_mask_idle(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_mdl_mask_idle"
            
            def description(self):
                return "config enable mask to moitor idle massage (1): enable, (0): disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_fcs_rx"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxmdl_cfgtype._cfg_fcs_rx()
            allFields["cfg_mdl_cr"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxmdl_cfgtype._cfg_mdl_cr()
            allFields["cfg_mdl_std"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxmdl_cfgtype._cfg_mdl_std()
            allFields["cfg_mdl_mask_test"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxmdl_cfgtype._cfg_mdl_mask_test()
            allFields["cfg_mdl_mask_path"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxmdl_cfgtype._cfg_mdl_mask_path()
            allFields["cfg_mdl_mask_idle"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxmdl_cfgtype._cfg_mdl_mask_idle()
            return allFields

    class _upen_destuff_ctrl0(AtRegister.AtRegister):
        def name(self):
            return "CONFIG CONTROL RX DESTUFF 0"
    
        def description(self):
            return "config control DeStuff global"
            
        def width(self):
            return 6
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000a041
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_crmon(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "cfg_crmon"
            
            def description(self):
                return "(0) : disable, (1) : enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _reserve1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "reserve1"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fcsmon(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "fcsmon"
            
            def description(self):
                return "(0) : disable monitor, (1) : enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 1
        
            def name(self):
                return "reserve"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _headermon_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "headermon_en"
            
            def description(self):
                return "(1) : enable monitor, (0) disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_crmon"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_destuff_ctrl0._cfg_crmon()
            allFields["reserve1"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_destuff_ctrl0._reserve1()
            allFields["fcsmon"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_destuff_ctrl0._fcsmon()
            allFields["reserve"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_destuff_ctrl0._reserve()
            allFields["headermon_en"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_destuff_ctrl0._headermon_en()
            return allFields

    class _upen_mdl_stk_cfg1(AtRegister.AtRegister):
        def name(self):
            return "STICKY TO RECEIVE PACKET MDL BUFF CONFIG1"
    
        def description(self):
            return "sticky row for buffer of type message is configured 0-31"
            
        def width(self):
            return 24
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000a045
            
        def endAddress(self):
            return 0xffffffff

        class _stk_cfg_alr1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "stk_cfg_alr1"
            
            def description(self):
                return "sticky for buffer of type message is configured 0-23"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["stk_cfg_alr1"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_mdl_stk_cfg1._stk_cfg_alr1()
            return allFields

    class _upen_rxprm_gmess(AtRegister.AtRegister):
        def name(self):
            return "COUNTER GOOD MESSAGE RX PRM"
    
        def description(self):
            return "counter good message PRM"
            
        def width(self):
            return 15
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xC000 + $STSID*32 + $VTGID*4 + $VTID + $UPRO * 1024 "
            
        def startAddress(self):
            return 0x0000c000
            
        def endAddress(self):
            return 0x0000cd3f

        class _cnt_gmess_prm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_gmess_prm"
            
            def description(self):
                return "value counter packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_gmess_prm"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxprm_gmess._cnt_gmess_prm()
            return allFields

    class _upen_rxprm_drmess(AtRegister.AtRegister):
        def name(self):
            return "COUNTER DROP MESSAGE RX PRM"
    
        def description(self):
            return "counter drop message PRM"
            
        def width(self):
            return 15
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xD000 + $STSID*32 + $VTGID*4 + $VTID + $UPRO * 1024 "
            
        def startAddress(self):
            return 0x0000d000
            
        def endAddress(self):
            return 0x0000dd3f

        class _cnt_drmess_prm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_drmess_prm"
            
            def description(self):
                return "value counter packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_drmess_prm"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxprm_drmess._cnt_drmess_prm()
            return allFields

    class _upen_rxprm_mmess(AtRegister.AtRegister):
        def name(self):
            return "COUNTER MISS MESSAGE RX PRM"
    
        def description(self):
            return "counter miss message PRM"
            
        def width(self):
            return 15
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xE000 + $STSID*32 + $VTGID*4 + $VTID + $UPRO * 1024 "
            
        def startAddress(self):
            return 0x0000e000
            
        def endAddress(self):
            return 0x0000ed3f

        class _cnt_mmess_prm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_mmess_prm"
            
            def description(self):
                return "value counter packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_mmess_prm"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxprm_mmess._cnt_mmess_prm()
            return allFields

    class _upen_rxprm_cnt_byte(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE RX PRM"
    
        def description(self):
            return "counter byte message PRM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xF000 + $STSID*32 + $VTGID*4 + $VTID + $UPRO * 1024 "
            
        def startAddress(self):
            return 0x0000f000
            
        def endAddress(self):
            return 0x0000fd3f

        class _cnt_byte_rxprm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_byte_rxprm"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_byte_rxprm"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxprm_cnt_byte._cnt_byte_rxprm()
            return allFields

    class _upen_rxmdl_cnt_byteidle(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE RX MDL IDLE"
    
        def description(self):
            return "counter byte IDLE message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xB400+ $MDLID + $UPRO * 128"
            
        def startAddress(self):
            return 0x0000b400
            
        def endAddress(self):
            return 0x0000b497

        class _cnt_byte_idle_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_byte_idle_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_byte_idle_mdl"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxmdl_cnt_byteidle._cnt_byte_idle_mdl()
            return allFields

    class _upen_rxmdl_cnt_bytepath(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE RX MDL PATH"
    
        def description(self):
            return "counter byte PATH message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xB420+ $MDLID + $UPRO * 128"
            
        def startAddress(self):
            return 0x0000b420
            
        def endAddress(self):
            return 0x0000b4b7

        class _cntr2c_byte_path_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_byte_path_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_byte_path_mdl"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxmdl_cnt_bytepath._cntr2c_byte_path_mdl()
            return allFields

    class _upen_rxmdl_cnt_bytetest(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE RX MDL TEST"
    
        def description(self):
            return "counter byte TEST message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xB440+ $MDLID + $UPRO * 128"
            
        def startAddress(self):
            return 0x0000b440
            
        def endAddress(self):
            return 0x0000b4d7

        class _cnt_byte_test_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_byte_test_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_byte_test_mdl"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxmdl_cnt_bytetest._cnt_byte_test_mdl()
            return allFields

    class _upen_rxmdl_cnt_goodidle(AtRegister.AtRegister):
        def name(self):
            return "COUNTER GOOD MESSAGE RX MDL IDLE"
    
        def description(self):
            return "counter good IDLE message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xB000+ $MDLID + $UPRO * 256"
            
        def startAddress(self):
            return 0x0000b000
            
        def endAddress(self):
            return 0x0000b117

        class _cnt_good_idle_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_good_idle_mdl"
            
            def description(self):
                return "value counter good"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_good_idle_mdl"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxmdl_cnt_goodidle._cnt_good_idle_mdl()
            return allFields

    class _upen_rxmdl_cnt_goodpath(AtRegister.AtRegister):
        def name(self):
            return "COUNTER GOOD MESSAGE RX MDL PATH"
    
        def description(self):
            return "counter good PATH message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xB020+ $MDLID + $UPRO * 256"
            
        def startAddress(self):
            return 0x0000b020
            
        def endAddress(self):
            return 0x0000b137

        class _cnt_good_path_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_good_path_mdl"
            
            def description(self):
                return "value counter good"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_good_path_mdl"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxmdl_cnt_goodpath._cnt_good_path_mdl()
            return allFields

    class _upen_rxmdl_cnt_goodtest(AtRegister.AtRegister):
        def name(self):
            return "COUNTER GOOD MESSAGE RX MDL TEST"
    
        def description(self):
            return "counter good TEST message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xB040+ $MDLID + $UPRO * 256"
            
        def startAddress(self):
            return 0x0000b040
            
        def endAddress(self):
            return 0x0000b157

        class _cnt_good_test_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_good_test_mdl"
            
            def description(self):
                return "value counter good"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_good_test_mdl"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxmdl_cnt_goodtest._cnt_good_test_mdl()
            return allFields

    class _upen_rxmdl_cntr2c_dropidle(AtRegister.AtRegister):
        def name(self):
            return "COUNTER DROP MESSAGE RX MDL IDLE"
    
        def description(self):
            return "counter drop IDLE message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xB060+ $MDLID + $UPRO * 256"
            
        def startAddress(self):
            return 0x0000b060
            
        def endAddress(self):
            return 0x0000b177

        class _cnt_drop_idle_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_drop_idle_mdl"
            
            def description(self):
                return "value counter drop"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_drop_idle_mdl"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxmdl_cntr2c_dropidle._cnt_drop_idle_mdl()
            return allFields

    class _upen_rxmdl_cnt_droppath(AtRegister.AtRegister):
        def name(self):
            return "COUNTER DROP MESSAGE RX MDL PATH"
    
        def description(self):
            return "counter drop PATH message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xB080+ $MDLID + $UPRO * 256"
            
        def startAddress(self):
            return 0x0000b080
            
        def endAddress(self):
            return 0x0000b197

        class _cnt_drop_path_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_drop_path_mdl"
            
            def description(self):
                return "value counter drop"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_drop_path_mdl"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxmdl_cnt_droppath._cnt_drop_path_mdl()
            return allFields

    class _upen_rxmdl_cnt_droptest(AtRegister.AtRegister):
        def name(self):
            return "COUNTER DROP MESSAGE RX MDL TEST"
    
        def description(self):
            return "counter drop TEST message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xB0A0+ $MDLID + $UPRO * 256"
            
        def startAddress(self):
            return 0x0000b0a0
            
        def endAddress(self):
            return 0x0000b1b7

        class _cnt_drop_test_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_drop_test_mdl"
            
            def description(self):
                return "value counter drop"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_drop_test_mdl"] = _AF6CNC0011_RD_PDH_MDLPRM._upen_rxmdl_cnt_droptest._cnt_drop_test_mdl()
            return allFields

    class _prm_cfg_lben_int(AtRegister.AtRegister):
        def name(self):
            return "PRM LB per Channel Interrupt Enable Control"
    
        def description(self):
            return "This is the per Channel interrupt enable"
            
        def width(self):
            return 2
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1A000+ $STSID*32 + $VTGID*4 + $VTID"
            
        def startAddress(self):
            return 0x0001a000
            
        def endAddress(self):
            return 0x0001a3ff

        class _prm_cfglben_int1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "prm_cfglben_int1"
            
            def description(self):
                return "Set 1 to enable change event to generate an interrupt slice 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _prm_cfglben_int0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "prm_cfglben_int0"
            
            def description(self):
                return "Set 1 to enable change event to generate an interrupt slice 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prm_cfglben_int1"] = _AF6CNC0011_RD_PDH_MDLPRM._prm_cfg_lben_int._prm_cfglben_int1()
            allFields["prm_cfglben_int0"] = _AF6CNC0011_RD_PDH_MDLPRM._prm_cfg_lben_int._prm_cfglben_int0()
            return allFields

    class _prm_lb_int_sta(AtRegister.AtRegister):
        def name(self):
            return "PRM LB Interrupt per Channel Interrupt Status"
    
        def description(self):
            return "This is the per Channel interrupt status."
            
        def width(self):
            return 2
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x1A400+ $STSID*32 + $VTGID*4 + $VTID"
            
        def startAddress(self):
            return 0x0001a400
            
        def endAddress(self):
            return 0x0001a7ff

        class _prm_lbint_sta1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "prm_lbint_sta1"
            
            def description(self):
                return "Set 1 if there is a change event slice 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _prm_lbint_sta0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "prm_lbint_sta0"
            
            def description(self):
                return "Set 1 if there is a change event slice 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prm_lbint_sta1"] = _AF6CNC0011_RD_PDH_MDLPRM._prm_lb_int_sta._prm_lbint_sta1()
            allFields["prm_lbint_sta0"] = _AF6CNC0011_RD_PDH_MDLPRM._prm_lb_int_sta._prm_lbint_sta0()
            return allFields

    class _prm_lb_int_crrsta(AtRegister.AtRegister):
        def name(self):
            return "PRM LB Interrupt per Channel Current Status"
    
        def description(self):
            return "This is the per Channel Current status."
            
        def width(self):
            return 2
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x1A800+ $STSID*32 + $VTGID*4 + $VTID"
            
        def startAddress(self):
            return 0x0001a800
            
        def endAddress(self):
            return 0x0001abff

        class _prm_lbint_crrsta1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "prm_lbint_crrsta1"
            
            def description(self):
                return "Current status of event slice 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _prm_lbint_crrsta0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "prm_lbint_crrsta0"
            
            def description(self):
                return "Current status of event slice 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prm_lbint_crrsta1"] = _AF6CNC0011_RD_PDH_MDLPRM._prm_lb_int_crrsta._prm_lbint_crrsta1()
            allFields["prm_lbint_crrsta0"] = _AF6CNC0011_RD_PDH_MDLPRM._prm_lb_int_crrsta._prm_lbint_crrsta0()
            return allFields

    class _prm_lb_intsta(AtRegister.AtRegister):
        def name(self):
            return "PRM LB Interrupt per Channel Interrupt OR Status"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return "0x1AC00 +  $GID"
            
        def startAddress(self):
            return 0x0001ac00
            
        def endAddress(self):
            return 0x0001ac20

        class _prm_lbintsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "prm_lbintsta"
            
            def description(self):
                return "Set to 1 if any interrupt status bit of corresponding channel is set and its interrupt is enabled."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prm_lbintsta"] = _AF6CNC0011_RD_PDH_MDLPRM._prm_lb_intsta._prm_lbintsta()
            return allFields

    class _prm_lb_sta_int(AtRegister.AtRegister):
        def name(self):
            return "PRM LB Interrupt OR Status"
    
        def description(self):
            return "The register consists of 2 bits. Each bit is used to store Interrupt OR status."
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0001afff
            
        def endAddress(self):
            return 0xffffffff

        class _prm_lbsta_int(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "prm_lbsta_int"
            
            def description(self):
                return "Set to 1 if any interrupt status bit is set and its interrupt is enabled"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prm_lbsta_int"] = _AF6CNC0011_RD_PDH_MDLPRM._prm_lb_sta_int._prm_lbsta_int()
            return allFields

    class _prm_lb_en_int(AtRegister.AtRegister):
        def name(self):
            return "PRM LB Interrupt Enable Control"
    
        def description(self):
            return "The register consists of 2 interrupt enable bits ."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0001affe
            
        def endAddress(self):
            return 0xffffffff

        class _prm_lben_int(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "prm_lben_int"
            
            def description(self):
                return "Set to 1 to enable to generate interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prm_lben_int"] = _AF6CNC0011_RD_PDH_MDLPRM._prm_lb_en_int._prm_lben_int()
            return allFields

    class _mdl_cfgen_int(AtRegister.AtRegister):
        def name(self):
            return "MDL per Channel Interrupt Enable Control"
    
        def description(self):
            return "This is the per Channel interrupt enable"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xA100 +  $MDLDE3ID"
            
        def startAddress(self):
            return 0x0000a100
            
        def endAddress(self):
            return 0x0000a11f

        class _mdl_cfgen_int(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdl_cfgen_int"
            
            def description(self):
                return "Set 1 to enable change event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdl_cfgen_int"] = _AF6CNC0011_RD_PDH_MDLPRM._mdl_cfgen_int._mdl_cfgen_int()
            return allFields

    class _mdl_int_sta(AtRegister.AtRegister):
        def name(self):
            return "MDL Interrupt per Channel Interrupt Status"
    
        def description(self):
            return "This is the per Channel interrupt status."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xA120 +  $MDLDE3ID + $LINEID * 32"
            
        def startAddress(self):
            return 0x0000a120
            
        def endAddress(self):
            return 0x0000a13f

        class _mdlint_sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdlint_sta"
            
            def description(self):
                return "Set 1 if there is a change event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdlint_sta"] = _AF6CNC0011_RD_PDH_MDLPRM._mdl_int_sta._mdlint_sta()
            return allFields

    class _mdl_int_crrsta(AtRegister.AtRegister):
        def name(self):
            return "MDL Interrupt per Channel Current Status"
    
        def description(self):
            return "This is the per Channel Current status."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xA140 +  $MDLDE3ID + $LINEID * 32"
            
        def startAddress(self):
            return 0x0000a140
            
        def endAddress(self):
            return 0x0000a15f

        class _mdlint_crrsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdlint_crrsta"
            
            def description(self):
                return "Current status of event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdlint_crrsta"] = _AF6CNC0011_RD_PDH_MDLPRM._mdl_int_crrsta._mdlint_crrsta()
            return allFields

    class _mdl_intsta(AtRegister.AtRegister):
        def name(self):
            return "MDL Interrupt per Channel Interrupt OR Status"
    
        def description(self):
            return "The register consists of 32 bits for 32 VT/TUs of the related STS/VC in the Rx DS1/E1/J1 Framer. Each bit is used to store Interrupt OR tus of the related DS1/E1/J1."
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return "0xA160 +  $GID"
            
        def startAddress(self):
            return 0x0000a160
            
        def endAddress(self):
            return 0x0000a161

        class _mdlintsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdlintsta"
            
            def description(self):
                return "Set to 1 if any interrupt status bit of corresponding channel is set and its interrupt is enabled."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdlintsta"] = _AF6CNC0011_RD_PDH_MDLPRM._mdl_intsta._mdlintsta()
            return allFields

    class _mdl_sta_int(AtRegister.AtRegister):
        def name(self):
            return "MDL Interrupt OR Status"
    
        def description(self):
            return "The register consists of 2 bits. Each bit is used to store Interrupt OR status."
            
        def width(self):
            return 2
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000a17f
            
        def endAddress(self):
            return 0xffffffff

        class _mdlsta_int(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdlsta_int"
            
            def description(self):
                return "Set to 1 if any interrupt status bit is set and its interrupt is enabled"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdlsta_int"] = _AF6CNC0011_RD_PDH_MDLPRM._mdl_sta_int._mdlsta_int()
            return allFields

    class _mdl_en_int(AtRegister.AtRegister):
        def name(self):
            return "MDL Interrupt Enable Control"
    
        def description(self):
            return "The register consists of 2 interrupt enable bits ."
            
        def width(self):
            return 2
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000a17e
            
        def endAddress(self):
            return 0xffffffff

        class _mdlen_int(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdlen_int"
            
            def description(self):
                return "Set to 1 to enable to generate interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdlen_int"] = _AF6CNC0011_RD_PDH_MDLPRM._mdl_en_int._mdlen_int()
            return allFields

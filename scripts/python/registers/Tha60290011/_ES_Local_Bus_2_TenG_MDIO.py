import AtRegister

class _ES_Local_Bus_2_TenG_MDIO(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["ack_bit"] = _ES_Local_Bus_2_TenG_MDIO._ack_bit()
        allRegisters["pcfg_ctl1"] = _ES_Local_Bus_2_TenG_MDIO._pcfg_ctl1()
        allRegisters["md_ack_clr"] = _ES_Local_Bus_2_TenG_MDIO._md_ack_clr()
        allRegisters["pcfg_ctl2"] = _ES_Local_Bus_2_TenG_MDIO._pcfg_ctl2()
        allRegisters["pcfg_ctl4"] = _ES_Local_Bus_2_TenG_MDIO._pcfg_ctl4()
        allRegisters["pcfg_datw"] = _ES_Local_Bus_2_TenG_MDIO._pcfg_datw()
        allRegisters["pcfg_datr"] = _ES_Local_Bus_2_TenG_MDIO._pcfg_datr()
        return allRegisters

    class _ack_bit(AtRegister.AtRegister):
        def name(self):
            return "Configure Raise mode for Interrupt Pin"
    
        def description(self):
            return "ack bit status this bit set to 1 when Read/Write OP done and will be clear when ACK_CLR bit set"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "Base_0x00 + 0x00"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _ack_bit(AtRegister.AtRegisterField):
            def startBit(self):
                return 0
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "ack_bit"
            
            def description(self):
                return "set to 1 when Read/Write OP done and will be clear when ACK_CLR bit set"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ack_bit"] = _ES_Local_Bus_2_TenG_MDIO._ack_bit._ack_bit()
            return allFields

    class _pcfg_ctl1(AtRegister.AtRegister):
        def name(self):
            return "Config PHY address"
    
        def description(self):
            return "This is Config PHY address and Port Select"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "Base_0x10 + 0x10"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _md_phyadr(AtRegister.AtRegisterField):
            def startBit(self):
                return 7
                
            def stopBit(self):
                return 4
        
            def name(self):
                return "md_phyadr"
            
            def description(self):
                return "MDIO Device   Addresses"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _md_regadr(AtRegister.AtRegisterField):
            def startBit(self):
                return 3
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "md_regadr"
            
            def description(self):
                return "MDIO Register  Addresses"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["md_phyadr"] = _ES_Local_Bus_2_TenG_MDIO._pcfg_ctl1._md_phyadr()
            allFields["md_regadr"] = _ES_Local_Bus_2_TenG_MDIO._pcfg_ctl1._md_regadr()
            return allFields

    class _md_ack_clr(AtRegister.AtRegister):
        def name(self):
            return "Status of Diagnostic Interrupt"
    
        def description(self):
            return "This is Status of Diagnostic Interrupt 1-2"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "Base_0x11 + 0x11"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _ack_clr(AtRegister.AtRegisterField):
            def startBit(self):
                return 0
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "ack_clr"
            
            def description(self):
                return "Write to Clear ACK OP"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ack_clr"] = _ES_Local_Bus_2_TenG_MDIO._md_ack_clr._ack_clr()
            return allFields

    class _pcfg_ctl2(AtRegister.AtRegister):
        def name(self):
            return "Config Reg address"
    
        def description(self):
            return "This is Config Reg address and RNW"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "Base_0x12 + 0x12"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _md_wr_en(AtRegister.AtRegisterField):
            def startBit(self):
                return 4
                
            def stopBit(self):
                return 4
        
            def name(self):
                return "md_wr_en"
            
            def description(self):
                return "Begin read/write (Need write 0 after read write done )"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _md_op(AtRegister.AtRegisterField):
            def startBit(self):
                return 1
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "md_op"
            
            def description(self):
                return "OP: operation code 2'b00 : Address transaction defined Set Address is used to set the internal 16-bit address register of the XAUI core for subsequent data transactions (called the current address in the following sections). 2'b10 : Read transaction defined The XAUI core returns the 16-bit word from the register at the current address. 2'b01 : Write transaction defined The XAUI core takes the 16-bit word in the data field and writes it to the register at the current address."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["md_wr_en"] = _ES_Local_Bus_2_TenG_MDIO._pcfg_ctl2._md_wr_en()
            allFields["md_op"] = _ES_Local_Bus_2_TenG_MDIO._pcfg_ctl2._md_op()
            return allFields

    class _pcfg_ctl4(AtRegister.AtRegister):
        def name(self):
            return "Config Reg address"
    
        def description(self):
            return "This is Config Reg address and RNW"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "Base_0x13 + 0x13"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _md_port_select(AtRegister.AtRegisterField):
            def startBit(self):
                return 7
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "md_port_select"
            
            def description(self):
                return "MDIO Slave port select 0x0 Un-Select"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["md_port_select"] = _ES_Local_Bus_2_TenG_MDIO._pcfg_ctl4._md_port_select()
            return allFields

    class _pcfg_datw(AtRegister.AtRegister):
        def name(self):
            return "Config Reg address"
    
        def description(self):
            return "This is Config Reg address and RNW"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "Base_0x20 + 0x20"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _md_data_read(AtRegister.AtRegisterField):
            def startBit(self):
                return 15
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "md_data_read"
            
            def description(self):
                return "MDIO Data  write to PHY chip"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["md_data_read"] = _ES_Local_Bus_2_TenG_MDIO._pcfg_datw._md_data_read()
            return allFields

    class _pcfg_datr(AtRegister.AtRegister):
        def name(self):
            return "Config Reg address"
    
        def description(self):
            return "This is Config Reg address and RNW"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "Base_0x21 + 0x21"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _md_data_read(AtRegister.AtRegisterField):
            def startBit(self):
                return 15
                
            def stopBit(self):
                return 0
        
            def name(self):
                return "md_data_read"
            
            def description(self):
                return "MDIO Data read from PHY chip"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["md_data_read"] = _ES_Local_Bus_2_TenG_MDIO._pcfg_datr._md_data_read()
            return allFields

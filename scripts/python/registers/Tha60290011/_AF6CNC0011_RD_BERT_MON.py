import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0011_RD_BERT_MON(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["upen_id_reg_mon"] = _AF6CNC0011_RD_BERT_MON._upen_id_reg_mon()
        allRegisters["mglb_pen"] = _AF6CNC0011_RD_BERT_MON._mglb_pen()
        allRegisters["stkcfg_pen"] = _AF6CNC0011_RD_BERT_MON._stkcfg_pen()
        allRegisters["stk_pen"] = _AF6CNC0011_RD_BERT_MON._stk_pen()
        allRegisters["errlat_pen"] = _AF6CNC0011_RD_BERT_MON._errlat_pen()
        allRegisters["goodlat_pen"] = _AF6CNC0011_RD_BERT_MON._goodlat_pen()
        allRegisters["lostlat_pen"] = _AF6CNC0011_RD_BERT_MON._lostlat_pen()
        allRegisters["mon_ctrl_pen"] = _AF6CNC0011_RD_BERT_MON._mon_ctrl_pen()
        allRegisters["thrnfix_pen"] = _AF6CNC0011_RD_BERT_MON._thrnfix_pen()
        allRegisters["mon_status_pen"] = _AF6CNC0011_RD_BERT_MON._mon_status_pen()
        allRegisters["merrcnt_pen"] = _AF6CNC0011_RD_BERT_MON._merrcnt_pen()
        allRegisters["mgoodbit_pen"] = _AF6CNC0011_RD_BERT_MON._mgoodbit_pen()
        allRegisters["mlostbit_pen"] = _AF6CNC0011_RD_BERT_MON._mlostbit_pen()
        allRegisters["mtsen_pen"] = _AF6CNC0011_RD_BERT_MON._mtsen_pen()
        allRegisters["cntld_strb"] = _AF6CNC0011_RD_BERT_MON._cntld_strb()
        return allRegisters

    class _upen_id_reg_mon(AtRegister.AtRegister):
        def name(self):
            return "Sel Bert ID Mon"
    
        def description(self):
            return "Sel 16 chanel id which enable moniter BERT from 12xOC24 channel"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return "0x8500 + $mid"
            
        def startAddress(self):
            return 0x00008500
            
        def endAddress(self):
            return 0x0000851f

        class _mon_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "mon_en"
            
            def description(self):
                return "bit enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DS3_bus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "DS3_bus"
            
            def description(self):
                return "indicate DS3 bus select \"0\" : DS3: bus1 \"1\" : DS3: bus2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _mon_id(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mon_id"
            
            def description(self):
                return "channel id"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mon_en"] = _AF6CNC0011_RD_BERT_MON._upen_id_reg_mon._mon_en()
            allFields["DS3_bus"] = _AF6CNC0011_RD_BERT_MON._upen_id_reg_mon._DS3_bus()
            allFields["mon_id"] = _AF6CNC0011_RD_BERT_MON._upen_id_reg_mon._mon_id()
            return allFields

    class _mglb_pen(AtRegister.AtRegister):
        def name(self):
            return "Bert Gen Global Register"
    
        def description(self):
            return "global mon config"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00008400
            
        def endAddress(self):
            return 0xffffffff

        class _lopthrcfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 6
        
            def name(self):
                return "lopthrcfg"
            
            def description(self):
                return "Threshold for lost pattern moniter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _synthrcfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 2
        
            def name(self):
                return "synthrcfg"
            
            def description(self):
                return "Threshold for gosyn state of pattern moniter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _swapmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "swapmode"
            
            def description(self):
                return "enable swap data"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _invmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "invmode"
            
            def description(self):
                return "enable invert data"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lopthrcfg"] = _AF6CNC0011_RD_BERT_MON._mglb_pen._lopthrcfg()
            allFields["synthrcfg"] = _AF6CNC0011_RD_BERT_MON._mglb_pen._synthrcfg()
            allFields["swapmode"] = _AF6CNC0011_RD_BERT_MON._mglb_pen._swapmode()
            allFields["invmode"] = _AF6CNC0011_RD_BERT_MON._mglb_pen._invmode()
            return allFields

    class _stkcfg_pen(AtRegister.AtRegister):
        def name(self):
            return "Sticky Enable"
    
        def description(self):
            return "This register is used to configure the test pattern sticky mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00008401
            
        def endAddress(self):
            return 0xffffffff

        class _stkcfg_reg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "stkcfg_reg"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["stkcfg_reg"] = _AF6CNC0011_RD_BERT_MON._stkcfg_pen._stkcfg_reg()
            return allFields

    class _stk_pen(AtRegister.AtRegister):
        def name(self):
            return "Sticky Error"
    
        def description(self):
            return "Bert sticky error"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00008402
            
        def endAddress(self):
            return 0xffffffff

        class _stkdi(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "stkdi"
            
            def description(self):
                return "indicate error of 16id moniter bert"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["stkdi"] = _AF6CNC0011_RD_BERT_MON._stk_pen._stkdi()
            return allFields

    class _errlat_pen(AtRegister.AtRegister):
        def name(self):
            return "Counter Error"
    
        def description(self):
            return "Latch error counter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00008404
            
        def endAddress(self):
            return 0xffffffff

        class _errcnt_lat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "errcnt_lat"
            
            def description(self):
                return "counter err"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["errcnt_lat"] = _AF6CNC0011_RD_BERT_MON._errlat_pen._errcnt_lat()
            return allFields

    class _goodlat_pen(AtRegister.AtRegister):
        def name(self):
            return "Counter Good"
    
        def description(self):
            return "Latch good counter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00008405
            
        def endAddress(self):
            return 0xffffffff

        class _goodcnt_lat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "goodcnt_lat"
            
            def description(self):
                return "counter good"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["goodcnt_lat"] = _AF6CNC0011_RD_BERT_MON._goodlat_pen._goodcnt_lat()
            return allFields

    class _lostlat_pen(AtRegister.AtRegister):
        def name(self):
            return "Counter Lost"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00008406
            
        def endAddress(self):
            return 0xffffffff

        class _lostcnt_lat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lostcnt_lat"
            
            def description(self):
                return "counter good"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lostcnt_lat"] = _AF6CNC0011_RD_BERT_MON._lostlat_pen._lostcnt_lat()
            return allFields

    class _mon_ctrl_pen(AtRegister.AtRegister):
        def name(self):
            return "Mon Control Bert Generate"
    
        def description(self):
            return "Used to control bert gen mode of 16 id"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return "0x8420 + $mctrl_id"
            
        def startAddress(self):
            return 0x00008420
            
        def endAddress(self):
            return 0x0000843f

        class _mswapmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "mswapmode"
            
            def description(self):
                return "swap data"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _minvmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "minvmode"
            
            def description(self):
                return "invert data"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _mpatbit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 5
        
            def name(self):
                return "mpatbit"
            
            def description(self):
                return "number of bit fix patt use"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _mpatt_mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mpatt_mode"
            
            def description(self):
                return "sel pattern gen # 0x0 : Prbs9 # 0x1 : Prbs11 # 0x2 : Prbs15 # 0x3 : Prbs20r # 0x4 : Prbs20 # 0x5 : qrss20 # 0x6 : Prbs23 # 0x7 : dds1 # 0x8 : dds2 # 0x9 : dds3 # 0xA : dds4 # 0xB : dds5 # 0xC : daly # 0XD : octet55_v2 # 0xE : octet55_v3 # 0xF : fix3in24 # 0x10: fix1in8 # 0x11: fix2in8 # 0x12: fixpat # 0x13: sequence # 0x14: ds0prbs # 0x15: fixnin32"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mswapmode"] = _AF6CNC0011_RD_BERT_MON._mon_ctrl_pen._mswapmode()
            allFields["minvmode"] = _AF6CNC0011_RD_BERT_MON._mon_ctrl_pen._minvmode()
            allFields["mpatbit"] = _AF6CNC0011_RD_BERT_MON._mon_ctrl_pen._mpatbit()
            allFields["mpatt_mode"] = _AF6CNC0011_RD_BERT_MON._mon_ctrl_pen._mpatt_mode()
            return allFields

    class _thrnfix_pen(AtRegister.AtRegister):
        def name(self):
            return "Config expected Fix pattern gen"
    
        def description(self):
            return "config fix pattern gen"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return "0x8440 + $genid"
            
        def startAddress(self):
            return 0x00008440
            
        def endAddress(self):
            return 0x0000845f

        class _crrthrnfix(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "crrthrnfix"
            
            def description(self):
                return "config fix pattern gen"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["crrthrnfix"] = _AF6CNC0011_RD_BERT_MON._thrnfix_pen._crrthrnfix()
            return allFields

    class _mon_status_pen(AtRegister.AtRegister):
        def name(self):
            return "Status of mon bert"
    
        def description(self):
            return "Indicate current status of bert mon"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return "0x84E0 + $msttid"
            
        def startAddress(self):
            return 0x000084e0
            
        def endAddress(self):
            return 0x000084ff

        class _mon_crr_stt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mon_crr_stt"
            
            def description(self):
                return "Status [1:0]: Prbs status LOPSTA = 2'd0; SRCSTA = 2'd1; VERSTA = 2'd2; INFSTA = 2'd3;"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mon_crr_stt"] = _AF6CNC0011_RD_BERT_MON._mon_status_pen._mon_crr_stt()
            return allFields

    class _merrcnt_pen(AtRegister.AtRegister):
        def name(self):
            return "counter error"
    
        def description(self):
            return "Moniter error bert counter of 16 chanel id"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return "0x8540 + $errid"
            
        def startAddress(self):
            return 0x00008540
            
        def endAddress(self):
            return 0x0000855f

        class _errcnt_pdo(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "errcnt_pdo"
            
            def description(self):
                return "value of err counter"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["errcnt_pdo"] = _AF6CNC0011_RD_BERT_MON._merrcnt_pen._errcnt_pdo()
            return allFields

    class _mgoodbit_pen(AtRegister.AtRegister):
        def name(self):
            return "Moniter Good Bit"
    
        def description(self):
            return "Moniter good bert counter of 16 chanel id"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return "0x8580 + $mgb_id"
            
        def startAddress(self):
            return 0x00008580
            
        def endAddress(self):
            return 0x0000859f

        class _goodbit_pdo(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "goodbit_pdo"
            
            def description(self):
                return "value of good cnt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["goodbit_pdo"] = _AF6CNC0011_RD_BERT_MON._mgoodbit_pen._goodbit_pdo()
            return allFields

    class _mlostbit_pen(AtRegister.AtRegister):
        def name(self):
            return "Moniter Lost Bit"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return "0x85C0 + $mlos_id"
            
        def startAddress(self):
            return 0x000085c0
            
        def endAddress(self):
            return 0x000084df

        class _lostbit_pdo(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lostbit_pdo"
            
            def description(self):
                return "Value of lost cnt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lostbit_pdo"] = _AF6CNC0011_RD_BERT_MON._mlostbit_pen._lostbit_pdo()
            return allFields

    class _mtsen_pen(AtRegister.AtRegister):
        def name(self):
            return "Config nxDS0 moniter"
    
        def description(self):
            return "enable moniter 32 timeslot E1 or 24timeslot T1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return "0x84C0 + $mid"
            
        def startAddress(self):
            return 0x000084c0
            
        def endAddress(self):
            return 0x000084cf

        class _mtsen_pdo(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mtsen_pdo"
            
            def description(self):
                return "set \"1\" to en moniter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mtsen_pdo"] = _AF6CNC0011_RD_BERT_MON._mtsen_pen._mtsen_pdo()
            return allFields

    class _cntld_strb(AtRegister.AtRegister):
        def name(self):
            return "moniter counter load id"
    
        def description(self):
            return "used to load the error/good/loss counter report of bert id which the channel ID is value of register."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00008403
            
        def endAddress(self):
            return 0xffffffff

        class _idload(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "idload"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["idload"] = _AF6CNC0011_RD_BERT_MON._cntld_strb._idload()
            return allFields

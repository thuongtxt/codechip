import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0011_RD_ETH(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["IP_Ethernet_Triple_Speed_Global_Version"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_Version()
        allRegisters["IP_Ethernet_Triple_Speed_Global_Control"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_Control()
        allRegisters["IP_Ethernet_Triple_Speed_Global_Interrupt_Type_Request"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_Interrupt_Type_Request()
        allRegisters["IP_Ethernet_Triple_Speed_Global_Interrupt_Status"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_Interrupt_Status()
        allRegisters["IP_Ethernet_Triple_Speed_Global_Interface_Control"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_Interface_Control()
        allRegisters["IP_Ethernet_Triple_Speed_Global_User_Request_Status"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_User_Request_Status()
        allRegisters["IP_Ethernet_Triple_Speed_PCS_Control"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Control()
        allRegisters["IP_Ethernet_Triple_Speed_PCS_Status"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Status()
        allRegisters["IP_Ethernet_Triple_Speed_PCS_PHY_Identifier_0"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_PHY_Identifier_0()
        allRegisters["IP_Ethernet_Triple_Speed_PCS_PHY_Identifier_1"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_PHY_Identifier_1()
        allRegisters["IP_Ethernet_Triple_Speed_PCS_Advertisement"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Advertisement()
        allRegisters["IP_Ethernet_Triple_Speed_PCS_Link_Partner_Ability_Base"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Link_Partner_Ability_Base()
        allRegisters["IP_Ethernet_Triple_Speed_PCS_An_Expansion"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_An_Expansion()
        allRegisters["IP_Ethernet_Triple_Speed_PCS_An_Next_Page_Transmitter"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_An_Next_Page_Transmitter()
        allRegisters["IP_Ethernet_Triple_Speed_PCS_An_Next_Page_Receiver"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_An_Next_Page_Receiver()
        allRegisters["IP_Ethernet_Triple_Speed_PCS_Extended_Status"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Extended_Status()
        allRegisters["IP_Ethernet_Triple_Speed_PCS_Option_Control"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_Control()
        allRegisters["IP_Ethernet_Triple_Speed_PCS_Option_Status"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_Status()
        allRegisters["IP_Ethernet_Triple_Speed_PCS_Option_FSM_Status_0"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_FSM_Status_0()
        allRegisters["IP_Ethernet_Triple_Speed_PCS_Option_FSM_Status_1"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_FSM_Status_1()
        allRegisters["IP_Ethernet_Triple_Speed_PCS_Option_Link_Timer"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_Link_Timer()
        allRegisters["IP_Ethernet_Triple_Speed_PCS_Option_Sync_Timer"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_Sync_Timer()
        allRegisters["IP_Ethernet_Triple_Speed_PCS_Option_PRBS_Test_Control"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_PRBS_Test_Control()
        allRegisters["IP_Ethernet_Triple_Speed_PCS_Option_PRBS_Test_Status"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_PRBS_Test_Status()
        allRegisters["IP_Ethernet_Triple_Speed_Simple_MAC_Active_Control"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Active_Control()
        allRegisters["IP_Ethernet_Triple_Speed_Simple_MAC_Receiver_Control"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Receiver_Control()
        allRegisters["IP_Ethernet_Triple_Speed_Simple_MAC_Transmitter_Control"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Transmitter_Control()
        allRegisters["IP_Ethernet_Triple_Speed_Simple_MAC_Data_Inter_Frame"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Data_Inter_Frame()
        allRegisters["IP_Ethernet_Triple_Speed_Simple_MAC_flow_Control_Time_Interval"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_flow_Control_Time_Interval()
        allRegisters["IP_Ethernet_Triple_Speed_Simple_MAC_flow_Control_Quanta"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_flow_Control_Quanta()
        allRegisters["IP_Ethernet_Triple_Speed_Simple_MAC_Interrupt_Status"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Interrupt_Status()
        allRegisters["IP_Ethernet_Triple_Speed_Simple_MAC_Latch_Status"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Latch_Status()
        allRegisters["ETH_FCS_errins_en_cfg"] = _AF6CNC0011_RD_ETH._ETH_FCS_errins_en_cfg()
        return allRegisters

    class _IP_Ethernet_Triple_Speed_Global_Version(AtRegister.AtRegister):
        def name(self):
            return "IP Ethernet Triple Speed Global Version"
    
        def description(self):
            return "This register is used to indicate version"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _IpEthTripGlbVendor(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "IpEthTripGlbVendor"
            
            def description(self):
                return ""
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripGlbVerionID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IpEthTripGlbVerionID"
            
            def description(self):
                return ""
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IpEthTripGlbVendor"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_Version._IpEthTripGlbVendor()
            allFields["IpEthTripGlbVerionID"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_Version._IpEthTripGlbVerionID()
            return allFields

    class _IP_Ethernet_Triple_Speed_Global_Control(AtRegister.AtRegister):
        def name(self):
            return "IP Ethernet Triple Speed Global Control"
    
        def description(self):
            return "This register is global Control"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000001
            
        def endAddress(self):
            return 0xffffffff

        class _IpEthTripGlbCtlElFifoDepth(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 8
        
            def name(self):
                return "IpEthTripGlbCtlElFifoDepth"
            
            def description(self):
                return "Elastic FIFO Depth, Only used to NONE PCS"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripGlbCtlSpStable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "IpEthTripGlbCtlSpStable"
            
            def description(self):
                return "Software must write 0x1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripGlbCtlSpIsPhy(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "IpEthTripGlbCtlSpIsPhy"
            
            def description(self):
                return "1: Enable SMAC auto update speed operation by PHY device (or PCS) 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripCfgCtlSelect(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "IpEthTripCfgCtlSelect"
            
            def description(self):
                return "1: Enable engine used con-fig interface by register IP Ethernet Triple Speed Global Interface Control 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripGlbCtlFlush(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "IpEthTripGlbCtlFlush"
            
            def description(self):
                return "Software reset 1: Software Reset 0: Normal"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripGlbCtlIntEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "IpEthTripGlbCtlIntEn"
            
            def description(self):
                return "Interrupt Enable 1: Enable Interrupt 0: Disable Interrupt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripGlbCtlActive(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IpEthTripGlbCtlActive"
            
            def description(self):
                return "Port Active 1: Active 0: No Active"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IpEthTripGlbCtlElFifoDepth"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_Control._IpEthTripGlbCtlElFifoDepth()
            allFields["IpEthTripGlbCtlSpStable"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_Control._IpEthTripGlbCtlSpStable()
            allFields["IpEthTripGlbCtlSpIsPhy"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_Control._IpEthTripGlbCtlSpIsPhy()
            allFields["IpEthTripCfgCtlSelect"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_Control._IpEthTripCfgCtlSelect()
            allFields["IpEthTripGlbCtlFlush"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_Control._IpEthTripGlbCtlFlush()
            allFields["IpEthTripGlbCtlIntEn"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_Control._IpEthTripGlbCtlIntEn()
            allFields["IpEthTripGlbCtlActive"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_Control._IpEthTripGlbCtlActive()
            return allFields

    class _IP_Ethernet_Triple_Speed_Global_Interrupt_Type_Request(AtRegister.AtRegister):
        def name(self):
            return "IP Ethernet Triple Speed Global Interrupt Type Request"
    
        def description(self):
            return "This register is global interrupt type request"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0xffffffff

        class _IpEthTripGlbIntTypMdio(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "IpEthTripGlbIntTypMdio"
            
            def description(self):
                return "MDIO interrupt request 1: SMAC interrupt request 0: SMAC no request"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripGlbIntTypSmac(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "IpEthTripGlbIntTypSmac"
            
            def description(self):
                return "Simple MAC interrupt request 1: SMAC interrupt request 0: SMAC no request"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripGlbIntTypGlb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "IpEthTripGlbIntTypGlb"
            
            def description(self):
                return "Global interrupt request 1: Global interrupt request 0: Global no request"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripGlbIntTypPcs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IpEthTripGlbIntTypPcs"
            
            def description(self):
                return "PCS interrupt request 1: PCS interrupt request 0: PCS no request"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IpEthTripGlbIntTypMdio"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_Interrupt_Type_Request._IpEthTripGlbIntTypMdio()
            allFields["IpEthTripGlbIntTypSmac"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_Interrupt_Type_Request._IpEthTripGlbIntTypSmac()
            allFields["IpEthTripGlbIntTypGlb"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_Interrupt_Type_Request._IpEthTripGlbIntTypGlb()
            allFields["IpEthTripGlbIntTypPcs"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_Interrupt_Type_Request._IpEthTripGlbIntTypPcs()
            return allFields

    class _IP_Ethernet_Triple_Speed_Global_Interrupt_Status(AtRegister.AtRegister):
        def name(self):
            return "IP Ethernet Triple Speed Global Interrupt Status"
    
        def description(self):
            return "This register is global status"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _IpEthTripGlbStaTxifEr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "IpEthTripGlbStaTxifEr"
            
            def description(self):
                return "Transmitter Interface check error occur"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripGlbStaRxifOver(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "IpEthTripGlbStaRxifOver"
            
            def description(self):
                return "Receiver Interface Rate match FIFO overflow occur"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripGlbStaRxifUnder(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "IpEthTripGlbStaRxifUnder"
            
            def description(self):
                return "Receiver Interface Rate match FIFO underflow occur"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripGlbStaRxffRdEr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "IpEthTripGlbStaRxffRdEr"
            
            def description(self):
                return "Receiver User FIFO convert clock domain read errror occur"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripGlbStaTxffWrEr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "IpEthTripGlbStaTxffWrEr"
            
            def description(self):
                return "Transmitter User FIFO convert clock domain write error occur"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripGlbStaTxshRdEr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "IpEthTripGlbStaTxshRdEr"
            
            def description(self):
                return "Transmitter User shift engine read error assert"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripGlbStaTxshWrEr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IpEthTripGlbStaTxshWrEr"
            
            def description(self):
                return "Transmitter User shift engine write error assert"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IpEthTripGlbStaTxifEr"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_Interrupt_Status._IpEthTripGlbStaTxifEr()
            allFields["IpEthTripGlbStaRxifOver"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_Interrupt_Status._IpEthTripGlbStaRxifOver()
            allFields["IpEthTripGlbStaRxifUnder"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_Interrupt_Status._IpEthTripGlbStaRxifUnder()
            allFields["IpEthTripGlbStaRxffRdEr"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_Interrupt_Status._IpEthTripGlbStaRxffRdEr()
            allFields["IpEthTripGlbStaTxffWrEr"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_Interrupt_Status._IpEthTripGlbStaTxffWrEr()
            allFields["IpEthTripGlbStaTxshRdEr"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_Interrupt_Status._IpEthTripGlbStaTxshRdEr()
            allFields["IpEthTripGlbStaTxshWrEr"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_Interrupt_Status._IpEthTripGlbStaTxshWrEr()
            return allFields

    class _IP_Ethernet_Triple_Speed_Global_Interface_Control(AtRegister.AtRegister):
        def name(self):
            return "IP Ethernet Triple Speed Global Interface Control"
    
        def description(self):
            return "This register is Interface Control %% Ethernet interface will description below %% TBI RGMII SGMII Functional %% 1 0 0 - TBI ( GMII) <--> PCS 1000BaseX <--> SMAC <---> USER speed 1000Mbps - rxclk (125Mhz), gtxclk (125Mhz) %% 1 0 1 - TBI ( GMII) <--> PCS SGMII <--> SMAC <---> USER speed 10/100/1000Mbps - rxclk (125Mhz), gtxclk (125Mhz) %% 1 1 0 - TBI (RGMII) <--> PCS 1000BaseX <--> SMAC <---> USER speed 1000Mbps - rxclk (125Mhz), gtxclk (125Mhz) %% 1 1 1 - TBI (RGMII) <--> PCS SGMII <--> SMAC <---> USER speed 10/100/1000Mbps - rxclk (125Mhz), gtxclk (125Mhz) %% 0 0 X - GMII/MII <------------------> SMAC <---> USER speed 10/100/1000Mbps - rxclk (2.5/25/125Mhz), gtxclk (2.5/25/125Mhz) %% 0 1 X - RGMII <------------------> SMAC <---> USER speed 10/100/1000Mbps - rxclk (2.5/25/125Mhz), gtxclk (2.5/25/125Mhz)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000004
            
        def endAddress(self):
            return 0xffffffff

        class _IpEthTripGlbStaRgmii(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "IpEthTripGlbStaRgmii"
            
            def description(self):
                return "RGMII interface Status 0x1: RGMII Active 0x0: GMII/MII Active"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripGlbStaSgmii(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "IpEthTripGlbStaSgmii"
            
            def description(self):
                return "SGMII interface status 0x1: PCS-SGMII 0x0: PCS-1000Base-X"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripGlbStaTbi(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "IpEthTripGlbStaTbi"
            
            def description(self):
                return "TBI interface status 0x1:  TBI interface Acitve (include PCS 1000Base-X or SGMII) 0x0:  None PCS"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripGlbStaSpeed(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "IpEthTripGlbStaSpeed"
            
            def description(self):
                return "Speed operation status 0x0: 10Mbps 0x1: 100Mbps 0x2: 1000Mbps 0x3: Unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripGlbCfgRgmii(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "IpEthTripGlbCfgRgmii"
            
            def description(self):
                return "RGMII interface Active 0x1: RGMII Active 0x0: GMII Active"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripGlbCfgSgmii(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "IpEthTripGlbCfgSgmii"
            
            def description(self):
                return "SGMII interface Active 0x1: PCS-SGMII 0x0: PCS-1000Base-X"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripGlbCfgTbi(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "IpEthTripGlbCfgTbi"
            
            def description(self):
                return "TBI interface 0x1:  TBI interface Acitve (include PCS 1000Base-X or SGMII) 0x0:  None PCS"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripGlbCfgSpeed(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IpEthTripGlbCfgSpeed"
            
            def description(self):
                return "Speed MAC operation 0x0: 10Mbps 0x1: 100Mbps 0x2: 1000Mbps 0x3: Unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IpEthTripGlbStaRgmii"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_Interface_Control._IpEthTripGlbStaRgmii()
            allFields["IpEthTripGlbStaSgmii"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_Interface_Control._IpEthTripGlbStaSgmii()
            allFields["IpEthTripGlbStaTbi"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_Interface_Control._IpEthTripGlbStaTbi()
            allFields["IpEthTripGlbStaSpeed"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_Interface_Control._IpEthTripGlbStaSpeed()
            allFields["IpEthTripGlbCfgRgmii"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_Interface_Control._IpEthTripGlbCfgRgmii()
            allFields["IpEthTripGlbCfgSgmii"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_Interface_Control._IpEthTripGlbCfgSgmii()
            allFields["IpEthTripGlbCfgTbi"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_Interface_Control._IpEthTripGlbCfgTbi()
            allFields["IpEthTripGlbCfgSpeed"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_Interface_Control._IpEthTripGlbCfgSpeed()
            return allFields

    class _IP_Ethernet_Triple_Speed_Global_User_Request_Status(AtRegister.AtRegister):
        def name(self):
            return "IP Ethernet Triple Speed Global User Request Status"
    
        def description(self):
            return "This register is User status, Used to HW debug"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000008
            
        def endAddress(self):
            return 0xffffffff

        class _IpEthTripGlbStaUsrRxVl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "IpEthTripGlbStaUsrRxVl"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripGlbStaUsrTxVl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "IpEthTripGlbStaUsrTxVl"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripGlbStaPauGen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "IpEthTripGlbStaPauGen"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripGlbStaUsrTxEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IpEthTripGlbStaUsrTxEnb"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IpEthTripGlbStaUsrRxVl"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_User_Request_Status._IpEthTripGlbStaUsrRxVl()
            allFields["IpEthTripGlbStaUsrTxVl"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_User_Request_Status._IpEthTripGlbStaUsrTxVl()
            allFields["IpEthTripGlbStaPauGen"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_User_Request_Status._IpEthTripGlbStaPauGen()
            allFields["IpEthTripGlbStaUsrTxEnb"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Global_User_Request_Status._IpEthTripGlbStaUsrTxEnb()
            return allFields

    class _IP_Ethernet_Triple_Speed_PCS_Control(AtRegister.AtRegister):
        def name(self):
            return "IP Ethernet Triple Speed PCS Control"
    
        def description(self):
            return "This register is PCS Control"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000020
            
        def endAddress(self):
            return 0xffffffff

        class _IpEthTripPcsReset(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "IpEthTripPcsReset"
            
            def description(self):
                return "1: Core Reset request 0: Normal Operation"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsLoopback(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "IpEthTripPcsLoopback"
            
            def description(self):
                return "1: Enable Line Loop in (LVDS) 0: No loop"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsSpeedLsb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "IpEthTripPcsSpeedLsb"
            
            def description(self):
                return "Speed Selection (LSB)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsAnEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "IpEthTripPcsAnEn"
            
            def description(self):
                return "1: Enable Auto-Negotiation process 0: Disable Auto-Negotiation process"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsPowerdown(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "IpEthTripPcsPowerdown"
            
            def description(self):
                return "1: Power down, Force Lost of Signal 0: Normal Operation"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsIsolate(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "IpEthTripPcsIsolate"
            
            def description(self):
                return "1: Electrically Isolate PCS from GMII 0: Normal Operation"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsRestartAn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "IpEthTripPcsRestartAn"
            
            def description(self):
                return "1: Restart Auto-Negotiation Process 0: Normal Operation"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsFullduplex(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "IpEthTripPcsFullduplex"
            
            def description(self):
                return "1: Full-Duplex Mode 0: Haft-duplex Mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsCollisionTest(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "IpEthTripPcsCollisionTest"
            
            def description(self):
                return "1: Enable Collision test 0: Disable Collision test"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsSpeedMsb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "IpEthTripPcsSpeedMsb"
            
            def description(self):
                return "Speed Selection (MSB) Case:"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IpEthTripPcsReset"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Control._IpEthTripPcsReset()
            allFields["IpEthTripPcsLoopback"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Control._IpEthTripPcsLoopback()
            allFields["IpEthTripPcsSpeedLsb"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Control._IpEthTripPcsSpeedLsb()
            allFields["IpEthTripPcsAnEn"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Control._IpEthTripPcsAnEn()
            allFields["IpEthTripPcsPowerdown"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Control._IpEthTripPcsPowerdown()
            allFields["IpEthTripPcsIsolate"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Control._IpEthTripPcsIsolate()
            allFields["IpEthTripPcsRestartAn"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Control._IpEthTripPcsRestartAn()
            allFields["IpEthTripPcsFullduplex"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Control._IpEthTripPcsFullduplex()
            allFields["IpEthTripPcsCollisionTest"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Control._IpEthTripPcsCollisionTest()
            allFields["IpEthTripPcsSpeedMsb"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Control._IpEthTripPcsSpeedMsb()
            return allFields

    class _IP_Ethernet_Triple_Speed_PCS_Status(AtRegister.AtRegister):
        def name(self):
            return "IP Ethernet Triple Speed PCS Status"
    
        def description(self):
            return "This register is PCS Status"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000021
            
        def endAddress(self):
            return 0xffffffff

        class _IpEthTripPcsStaAutoNegComplete(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "IpEthTripPcsStaAutoNegComplete"
            
            def description(self):
                return "1: Auto-Negotiation process completed 0: Auto-Negotiation process not completed"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsStaRemoteFault(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "IpEthTripPcsStaRemoteFault"
            
            def description(self):
                return "1: Remote fault condition detected 0: No remote fault condition detected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsStaLinkStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "IpEthTripPcsStaLinkStatus"
            
            def description(self):
                return "1: Link is up 0: Link is down"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IpEthTripPcsStaAutoNegComplete"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Status._IpEthTripPcsStaAutoNegComplete()
            allFields["IpEthTripPcsStaRemoteFault"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Status._IpEthTripPcsStaRemoteFault()
            allFields["IpEthTripPcsStaLinkStatus"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Status._IpEthTripPcsStaLinkStatus()
            return allFields

    class _IP_Ethernet_Triple_Speed_PCS_PHY_Identifier_0(AtRegister.AtRegister):
        def name(self):
            return "IP Ethernet Triple Speed PCS PHY Identifier 0"
    
        def description(self):
            return "This register is PHY Identifier 0"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000022
            
        def endAddress(self):
            return 0xffffffff

        class _value(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "value"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["value"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_PHY_Identifier_0._value()
            return allFields

    class _IP_Ethernet_Triple_Speed_PCS_PHY_Identifier_1(AtRegister.AtRegister):
        def name(self):
            return "IP Ethernet Triple Speed PCS PHY Identifier 1"
    
        def description(self):
            return "This register is PHY Identifier 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000023
            
        def endAddress(self):
            return 0xffffffff

        class _IpEthTripPcsVendor21_7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IpEthTripPcsVendor21_7"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IpEthTripPcsVendor21_7"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_PHY_Identifier_1._IpEthTripPcsVendor21_7()
            return allFields

    class _IP_Ethernet_Triple_Speed_PCS_Advertisement(AtRegister.AtRegister):
        def name(self):
            return "IP Ethernet Triple Speed PCS Advertisement"
    
        def description(self):
            return "This register is Auto-Negotiation Advertisement"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000024
            
        def endAddress(self):
            return 0xffffffff

        class _IpEthTripPcsAnAdvNextPage(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "IpEthTripPcsAnAdvNextPage"
            
            def description(self):
                return "1 : Next Page functionality is supported 0 : Next Page functionality is not supported"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsAnAdvRemoteFault(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "IpEthTripPcsAnAdvRemoteFault"
            
            def description(self):
                return "Remote Fault, self clearing to 0x0 after auto-negotiation 0 : No Error 1 : Offline 2 : Link Failure 3 : Auto-Negotiation Error"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsAnAdvPause(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 7
        
            def name(self):
                return "IpEthTripPcsAnAdvPause"
            
            def description(self):
                return "0 : No PAUSE 1 : Symmetric PAUSE 2 : Asymmetric PAUSE towards link partner 3 : Both Symmetric PAUSE and Asymmetric PAUSE supported"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsAnAdvHalfDuplex(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "IpEthTripPcsAnAdvHalfDuplex"
            
            def description(self):
                return "1 : Half Duplex Mode is supported 0 : Half Duplex Mode is not supported"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsAnAdvFullDuplex(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "IpEthTripPcsAnAdvFullDuplex"
            
            def description(self):
                return "1 : Full Duplex Mode is supported 0 : Full Duplex Mode is not supported"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IpEthTripPcsAnAdvNextPage"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Advertisement._IpEthTripPcsAnAdvNextPage()
            allFields["IpEthTripPcsAnAdvRemoteFault"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Advertisement._IpEthTripPcsAnAdvRemoteFault()
            allFields["IpEthTripPcsAnAdvPause"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Advertisement._IpEthTripPcsAnAdvPause()
            allFields["IpEthTripPcsAnAdvHalfDuplex"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Advertisement._IpEthTripPcsAnAdvHalfDuplex()
            allFields["IpEthTripPcsAnAdvFullDuplex"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Advertisement._IpEthTripPcsAnAdvFullDuplex()
            return allFields

    class _IP_Ethernet_Triple_Speed_PCS_Link_Partner_Ability_Base(AtRegister.AtRegister):
        def name(self):
            return "IP Ethernet Triple Speed PCS Link Partner Ability Base"
    
        def description(self):
            return "This register is Auto-Negotiation Link Partner Ability Base"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000025
            
        def endAddress(self):
            return 0xffffffff

        class _IpEthTripPcsAnLpANextPage(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "IpEthTripPcsAnLpANextPage"
            
            def description(self):
                return "1 : Next Page functionality is supported 0 : Next Page functionality is not supported"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsAnLpAAcknowledge(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "IpEthTripPcsAnLpAAcknowledge"
            
            def description(self):
                return "Used by Auto-Negotiation function to indicate reception of a link partners base or next page"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsAnLpARemoteFault(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 9
        
            def name(self):
                return "IpEthTripPcsAnLpARemoteFault"
            
            def description(self):
                return "Remote Fault 0 : No Error 1 : Offline 2 : Link Failure 3 : Auto-Negotiation Error"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsAnLpAPause(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 7
        
            def name(self):
                return "IpEthTripPcsAnLpAPause"
            
            def description(self):
                return "0 : No PAUSE 1 : Symmetric PAUSE 2 : Asymmetric PAUSE towards link partner 3 : Both Symmetric PAUSE and Asymmetric PAUSE supported"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsAnLpAHalfDuplex(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "IpEthTripPcsAnLpAHalfDuplex"
            
            def description(self):
                return "1 : Half Duplex Mode is supported 0 : Half Duplex Mode is not supported"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsAnLpAFullDuplex(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "IpEthTripPcsAnLpAFullDuplex"
            
            def description(self):
                return "1 : Full Duplex Mode is supported 0 : Full Duplex Mode is not supported"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IpEthTripPcsAnLpANextPage"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Link_Partner_Ability_Base._IpEthTripPcsAnLpANextPage()
            allFields["IpEthTripPcsAnLpAAcknowledge"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Link_Partner_Ability_Base._IpEthTripPcsAnLpAAcknowledge()
            allFields["IpEthTripPcsAnLpARemoteFault"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Link_Partner_Ability_Base._IpEthTripPcsAnLpARemoteFault()
            allFields["IpEthTripPcsAnLpAPause"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Link_Partner_Ability_Base._IpEthTripPcsAnLpAPause()
            allFields["IpEthTripPcsAnLpAHalfDuplex"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Link_Partner_Ability_Base._IpEthTripPcsAnLpAHalfDuplex()
            allFields["IpEthTripPcsAnLpAFullDuplex"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Link_Partner_Ability_Base._IpEthTripPcsAnLpAFullDuplex()
            return allFields

    class _IP_Ethernet_Triple_Speed_PCS_An_Expansion(AtRegister.AtRegister):
        def name(self):
            return "IP Ethernet Triple Speed PCS An Expansion"
    
        def description(self):
            return "This register is Auto-Negotiation Expansion Register"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000026
            
        def endAddress(self):
            return 0xffffffff

        class _IpEthTripPcsAnExpNextPageAble(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "IpEthTripPcsAnExpNextPageAble"
            
            def description(self):
                return "Next Page Able"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsAnExpPageReceived(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "IpEthTripPcsAnExpPageReceived"
            
            def description(self):
                return "Page Received 1 : A new page has been received 0 : A new page has not been received"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IpEthTripPcsAnExpNextPageAble"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_An_Expansion._IpEthTripPcsAnExpNextPageAble()
            allFields["IpEthTripPcsAnExpPageReceived"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_An_Expansion._IpEthTripPcsAnExpPageReceived()
            return allFields

    class _IP_Ethernet_Triple_Speed_PCS_An_Next_Page_Transmitter(AtRegister.AtRegister):
        def name(self):
            return "IP Ethernet Triple Speed PCS An Next Page Transmitter"
    
        def description(self):
            return "This register is Auto-Negotiation Next Page Transmitter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000027
            
        def endAddress(self):
            return 0xffffffff

        class _IpEthTripPcsAnNpTxNextPage(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "IpEthTripPcsAnNpTxNextPage"
            
            def description(self):
                return "1: Additional Next Page(s) will follow 0: Last page"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsAnNpTxMessagePage(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "IpEthTripPcsAnNpTxMessagePage"
            
            def description(self):
                return "Message Page 1: Message Page 0: Unformatted Page"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsAnNpTxAck(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "IpEthTripPcsAnNpTxAck"
            
            def description(self):
                return "Acknowledge 1: Comply with message 0: Cannot comply with message"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsAnNpTxToggle(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "IpEthTripPcsAnNpTxToggle"
            
            def description(self):
                return "Value toggles between subsequent Next Page"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcaAnNpTxMeassge(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IpEthTripPcaAnNpTxMeassge"
            
            def description(self):
                return "Message Code Field or Unformatted Page, (00000000001 is Null Message Code)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IpEthTripPcsAnNpTxNextPage"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_An_Next_Page_Transmitter._IpEthTripPcsAnNpTxNextPage()
            allFields["IpEthTripPcsAnNpTxMessagePage"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_An_Next_Page_Transmitter._IpEthTripPcsAnNpTxMessagePage()
            allFields["IpEthTripPcsAnNpTxAck"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_An_Next_Page_Transmitter._IpEthTripPcsAnNpTxAck()
            allFields["IpEthTripPcsAnNpTxToggle"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_An_Next_Page_Transmitter._IpEthTripPcsAnNpTxToggle()
            allFields["IpEthTripPcaAnNpTxMeassge"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_An_Next_Page_Transmitter._IpEthTripPcaAnNpTxMeassge()
            return allFields

    class _IP_Ethernet_Triple_Speed_PCS_An_Next_Page_Receiver(AtRegister.AtRegister):
        def name(self):
            return "IP Ethernet Triple Speed PCS An Next Page Receiver"
    
        def description(self):
            return "This register is Auto-Negotiation Next Page Receiver"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000028
            
        def endAddress(self):
            return 0xffffffff

        class _IpEthTripPcsAnNpRxNextPage(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "IpEthTripPcsAnNpRxNextPage"
            
            def description(self):
                return "1: Additional Next Page(s) will follow 0: Last page"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsAnNpRxAck(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "IpEthTripPcsAnNpRxAck"
            
            def description(self):
                return "Acknowledge, Used by Auto-Negotiation function to indicate reception of a link partners base or next page"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsAnNpRxMessagePage(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "IpEthTripPcsAnNpRxMessagePage"
            
            def description(self):
                return "Message Page 1: Message Page 0: Unformatted Page"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsAnNpRxAck2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "IpEthTripPcsAnNpRxAck2"
            
            def description(self):
                return "Acknowledge2 1: Comply with message 0: Cannot comply with message"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsAnNpRxToggle(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "IpEthTripPcsAnNpRxToggle"
            
            def description(self):
                return "Value toggles between subsequent Next Page"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcaAnNpRxMeassge(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IpEthTripPcaAnNpRxMeassge"
            
            def description(self):
                return "Message Code Field or Unformatted Page"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IpEthTripPcsAnNpRxNextPage"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_An_Next_Page_Receiver._IpEthTripPcsAnNpRxNextPage()
            allFields["IpEthTripPcsAnNpRxAck"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_An_Next_Page_Receiver._IpEthTripPcsAnNpRxAck()
            allFields["IpEthTripPcsAnNpRxMessagePage"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_An_Next_Page_Receiver._IpEthTripPcsAnNpRxMessagePage()
            allFields["IpEthTripPcsAnNpRxAck2"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_An_Next_Page_Receiver._IpEthTripPcsAnNpRxAck2()
            allFields["IpEthTripPcsAnNpRxToggle"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_An_Next_Page_Receiver._IpEthTripPcsAnNpRxToggle()
            allFields["IpEthTripPcaAnNpRxMeassge"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_An_Next_Page_Receiver._IpEthTripPcaAnNpRxMeassge()
            return allFields

    class _IP_Ethernet_Triple_Speed_PCS_Extended_Status(AtRegister.AtRegister):
        def name(self):
            return "IP Ethernet Triple Speed PCS Extended Status"
    
        def description(self):
            return "This register is Extended Status"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000002f
            
        def endAddress(self):
            return 0xffffffff

        class _IpEthTripPcsEx1000BXFdupx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "IpEthTripPcsEx1000BXFdupx"
            
            def description(self):
                return "Support 1000Base_X full duplex 1: Supported 0: No Supported"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsEx1000BXHdupx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "IpEthTripPcsEx1000BXHdupx"
            
            def description(self):
                return "Support 1000Base_X halfl duplex 1: Supported 0: No Supported"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsEx1000BTFdupx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "IpEthTripPcsEx1000BTFdupx"
            
            def description(self):
                return "Support 1000Base_T full duplex 1: Supported 0: No Supported"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsEx1000BTHdupx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "IpEthTripPcsEx1000BTHdupx"
            
            def description(self):
                return "Support 1000Base_T halfl duplex 1: Supported 0: No Supported"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IpEthTripPcsEx1000BXFdupx"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Extended_Status._IpEthTripPcsEx1000BXFdupx()
            allFields["IpEthTripPcsEx1000BXHdupx"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Extended_Status._IpEthTripPcsEx1000BXHdupx()
            allFields["IpEthTripPcsEx1000BTFdupx"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Extended_Status._IpEthTripPcsEx1000BTFdupx()
            allFields["IpEthTripPcsEx1000BTHdupx"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Extended_Status._IpEthTripPcsEx1000BTHdupx()
            return allFields

    class _IP_Ethernet_Triple_Speed_PCS_Option_Control(AtRegister.AtRegister):
        def name(self):
            return "IP Ethernet Triple Speed PCS Option Control"
    
        def description(self):
            return "This register is PCS Option Control"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000032
            
        def endAddress(self):
            return 0xffffffff

        class _IpEthTripPcsCtlFifoAdjC2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "IpEthTripPcsCtlFifoAdjC2"
            
            def description(self):
                return "Enable rate match FIFO used adjust C2 code word when Auto-Negotiation enable 0 : Disable 1 : Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsCtlFifoDepth(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 12
        
            def name(self):
                return "IpEthTripPcsCtlFifoDepth"
            
            def description(self):
                return "Rate match FIFO depth, support support up to SGMII speed 10Mbps jumbo frame 12K bytes 0 : +/- 4 words 1 : +/- 8 words 2 : +/- 16 words 3 : +/- 32 words 4 : +/- 64 words 5 : +/- 128 words 6 : +/- 256 words 7 : Unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsCtlLoopin(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "IpEthTripPcsCtlLoopin"
            
            def description(self):
                return "Diagnostic, loop in enable 1 : Loop back in enable 0 : Normal Operations"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsCtlLoopout(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "IpEthTripPcsCtlLoopout"
            
            def description(self):
                return "Diagnostic, loop out enable 1 : Loop back out enable 0 : Normal Operations"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsCtlTestDisp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "IpEthTripPcsCtlTestDisp"
            
            def description(self):
                return "Value Disparity Test"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsCtlTestFDisp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "IpEthTripPcsCtlTestFDisp"
            
            def description(self):
                return "1 : Enable Disparity Test 0 : Normal Operations"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsCtlTxSwap(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "IpEthTripPcsCtlTxSwap"
            
            def description(self):
                return "Enable Swap output TBI data 1 :  Enable 0 :  Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsCtlTxPolar(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "IpEthTripPcsCtlTxPolar"
            
            def description(self):
                return "Enable Invert output TBI data 1 :  Enable 0 :  Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsCtlRxSwap(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "IpEthTripPcsCtlRxSwap"
            
            def description(self):
                return "Enable Swap input  TBI data 1 :  Enable 0 :  Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsCtlRxPolar(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "IpEthTripPcsCtlRxPolar"
            
            def description(self):
                return "Enable Invert input TBI data 1 :  Enable 0 :  Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsCtlSgmiiSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "IpEthTripPcsCtlSgmiiSel"
            
            def description(self):
                return "Enable select SGMII mode when IpEthTripPcsCtlModSel assert 1 :  SGMII mode 0 :  1000BASE-X"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsCtlModSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "IpEthTripPcsCtlModSel"
            
            def description(self):
                return "Mode Interface select 1 :  Enable select sgmii/1000base_x depend on  IpEthTripPcsCtlSgmiiSel bit 0 :  Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsIntLinkEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "IpEthTripPcsIntLinkEn"
            
            def description(self):
                return "Enable interrupt Link status 1 :  Enable 0 :  Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsIntAnEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IpEthTripPcsIntAnEn"
            
            def description(self):
                return "Enable interrupt Auto-Negotiation 1 :  Enable 0 :  Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IpEthTripPcsCtlFifoAdjC2"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_Control._IpEthTripPcsCtlFifoAdjC2()
            allFields["IpEthTripPcsCtlFifoDepth"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_Control._IpEthTripPcsCtlFifoDepth()
            allFields["IpEthTripPcsCtlLoopin"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_Control._IpEthTripPcsCtlLoopin()
            allFields["IpEthTripPcsCtlLoopout"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_Control._IpEthTripPcsCtlLoopout()
            allFields["IpEthTripPcsCtlTestDisp"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_Control._IpEthTripPcsCtlTestDisp()
            allFields["IpEthTripPcsCtlTestFDisp"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_Control._IpEthTripPcsCtlTestFDisp()
            allFields["IpEthTripPcsCtlTxSwap"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_Control._IpEthTripPcsCtlTxSwap()
            allFields["IpEthTripPcsCtlTxPolar"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_Control._IpEthTripPcsCtlTxPolar()
            allFields["IpEthTripPcsCtlRxSwap"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_Control._IpEthTripPcsCtlRxSwap()
            allFields["IpEthTripPcsCtlRxPolar"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_Control._IpEthTripPcsCtlRxPolar()
            allFields["IpEthTripPcsCtlSgmiiSel"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_Control._IpEthTripPcsCtlSgmiiSel()
            allFields["IpEthTripPcsCtlModSel"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_Control._IpEthTripPcsCtlModSel()
            allFields["IpEthTripPcsIntLinkEn"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_Control._IpEthTripPcsIntLinkEn()
            allFields["IpEthTripPcsIntAnEn"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_Control._IpEthTripPcsIntAnEn()
            return allFields

    class _IP_Ethernet_Triple_Speed_PCS_Option_Status(AtRegister.AtRegister):
        def name(self):
            return "IP Ethernet Triple Speed PCS Option Status"
    
        def description(self):
            return "This register is PCS Option Status"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000033
            
        def endAddress(self):
            return 0xffffffff

        class _IpEthTripPcsStaFifoUnderflow(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "IpEthTripPcsStaFifoUnderflow"
            
            def description(self):
                return "Under flow elastic FIFO status 1: Under flow elastic FIFO occurred,  resolve by increase  Rate match FIFO depth at  IpEthTripPcsCtlFifoDepth[2:0] at IP Ethernet Triple Speed PCS Option Control register 0: Under flow elastic FIFO occurred"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsStaFifoOverflow(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "IpEthTripPcsStaFifoOverflow"
            
            def description(self):
                return "Over flow elastic FIFO status 1: Over flow elastic FIFO occurred, resolve by increase  Rate match FIFO depth at  IpEthTripPcsCtlFifoDepth[2:0] at IP Ethernet Triple Speed PCS Option Control register 0: No over flow elastic FIFO occurred"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsStaDispEr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "IpEthTripPcsStaDispEr"
            
            def description(self):
                return "Disparity violation 1: Disparity violation 0: Disparity no violation"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsStaCodeEr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "IpEthTripPcsStaCodeEr"
            
            def description(self):
                return "Code word invalid detection 1: Detect an code word invalid 0: No Code word invalid detection"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsStaLinkFail(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "IpEthTripPcsStaLinkFail"
            
            def description(self):
                return "Link fail status 1: Link down 0: Link up"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsStaAnComplete(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "IpEthTripPcsStaAnComplete"
            
            def description(self):
                return "Auto-Negotiation status 1: Auto-Neg complete 0: Auto-Neg Fail"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsIntLinkReq(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "IpEthTripPcsIntLinkReq"
            
            def description(self):
                return "Link interrupt request 1: Interrupt request 0: No Interrupt request"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsIntAnReq(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IpEthTripPcsIntAnReq"
            
            def description(self):
                return "Auto-Negotiation interrupt request 1: Interrupt request 0: No Interrupt request"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IpEthTripPcsStaFifoUnderflow"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_Status._IpEthTripPcsStaFifoUnderflow()
            allFields["IpEthTripPcsStaFifoOverflow"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_Status._IpEthTripPcsStaFifoOverflow()
            allFields["IpEthTripPcsStaDispEr"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_Status._IpEthTripPcsStaDispEr()
            allFields["IpEthTripPcsStaCodeEr"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_Status._IpEthTripPcsStaCodeEr()
            allFields["IpEthTripPcsStaLinkFail"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_Status._IpEthTripPcsStaLinkFail()
            allFields["IpEthTripPcsStaAnComplete"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_Status._IpEthTripPcsStaAnComplete()
            allFields["IpEthTripPcsIntLinkReq"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_Status._IpEthTripPcsIntLinkReq()
            allFields["IpEthTripPcsIntAnReq"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_Status._IpEthTripPcsIntAnReq()
            return allFields

    class _IP_Ethernet_Triple_Speed_PCS_Option_FSM_Status_0(AtRegister.AtRegister):
        def name(self):
            return "IP Ethernet Triple Speed PCS Option FSM Status 0"
    
        def description(self):
            return "This register is PCS FSM Current Status 0, used to HW debug"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000034
            
        def endAddress(self):
            return 0xffffffff

        class _IpEthTripPcsCurXmit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "IpEthTripPcsCurXmit"
            
            def description(self):
                return "Xmit current status (define at 802.3 section 3) 0x0 : XMIT_IDL 0x1 : XMIT_CFG 0x2 : XMIT_DAT"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsCurAnFsm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "IpEthTripPcsCurAnFsm"
            
            def description(self):
                return "Auto-Negotiation Current FSM 0x0 : AN_ENABLE 0x1 : AN_RESTART 0x2 : AN_DIS_LINK_OK 0x3 : ABILITY_DET 0x4 : ACK_DET 0x5 : NEXT_PAGE_WAIT 0x6 : COMPLETE_ACK 0x7 : DLE_DET 0x8 : LINK_OK"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsCurSyncFsm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IpEthTripPcsCurSyncFsm"
            
            def description(self):
                return "Synchronization current FSM 0x0 : LOSYNC 0x1 : COM_DET1 0x2 : ACQ_SYNC1 0x3 : COM_DET2 0x4 : ACQ_SYNC2 0x5 : ACQ_SYNC2 0x6 : SYNC_ACQ1 0x7 : SYNC_ACQ2 0x8 : SYNC_ACQ3 0x9 : SYNC_ACQ4 0xa : SYNC_ACQ2A 0xb : SYNC_ACQ3A 0xc : SYNC_ACQ3A"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IpEthTripPcsCurXmit"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_FSM_Status_0._IpEthTripPcsCurXmit()
            allFields["IpEthTripPcsCurAnFsm"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_FSM_Status_0._IpEthTripPcsCurAnFsm()
            allFields["IpEthTripPcsCurSyncFsm"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_FSM_Status_0._IpEthTripPcsCurSyncFsm()
            return allFields

    class _IP_Ethernet_Triple_Speed_PCS_Option_FSM_Status_1(AtRegister.AtRegister):
        def name(self):
            return "IP Ethernet Triple Speed PCS Option FSM Status 1"
    
        def description(self):
            return "This register is PCS FSM Current Status 1, only used to HW debug"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000035
            
        def endAddress(self):
            return 0xffffffff

        class _IpEthTripPcsCurRxFsm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 5
        
            def name(self):
                return "IpEthTripPcsCurRxFsm"
            
            def description(self):
                return "Receiver Engine current FSM 0x00 :  LINK_FAIL 0x01 :  WAIT_FOR_K 0x02 :  RX_K 0x03 :  RX_CB 0x04 :  RX_CC 0x05 :  RX_CD 0x06 :  IDLE 0x07 :  CARRIER_DET 0x08 :  FALSE_CARR 0x09 :  RX_INVALID 0x0A :  START_OF_PACKET 0x0B :  RECEIVE 0x0C :  EARLY_END 0x0D :  TRI_RRI 0x0E :  TRR_EXTEND 0x0F :  RX_DATA_ERR 0x10 :   RX_DATA 0x11 :   EARLY_END_EXT 0x12 :   EPD2_CHECK_END 0x13 :   EXTEND_ERR"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsCurTxFsm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IpEthTripPcsCurTxFsm"
            
            def description(self):
                return "Transmitter Engine current FSM 0x00 :   CFG_C1A 0x01 :   CFG_C1B 0x02 :   CFG_C1C 0x03 :   CFG_C1D 0x04 :   CFG_C2A 0x05 :   CFG_C2B 0x06 :   CFG_C2C 0x07 :   CFG_C2D 0x08 :   IDLE_DISP 0x09 :   IDLE_I1B 0x0A :  IDLE_I2B 0x0B :  START_ERROR 0x0C :  TX_DATA_ERROR 0x0D :  START_OF_PACKET 0x0E :  TX_DATA 0x0F :   EOP_NOEXT 0x10 :   EOP_EXT 0x11 :   EPD2_NOEXT 0x12 :   EPD3 0x13 :   EXTEND_BY_1 0x14 :   CARR_EXT"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IpEthTripPcsCurRxFsm"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_FSM_Status_1._IpEthTripPcsCurRxFsm()
            allFields["IpEthTripPcsCurTxFsm"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_FSM_Status_1._IpEthTripPcsCurTxFsm()
            return allFields

    class _IP_Ethernet_Triple_Speed_PCS_Option_Link_Timer(AtRegister.AtRegister):
        def name(self):
            return "IP Ethernet Triple Speed PCS Option Link Timer"
    
        def description(self):
            return "This register is PCS Link Timer, unit = 65535 ns ~ 65us"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000038
            
        def endAddress(self):
            return 0xffffffff

        class _IpEthTripPcsCfgLinkTimerSgmii(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "IpEthTripPcsCfgLinkTimerSgmii"
            
            def description(self):
                return "Auto-Negotiation cycle by the Link timer with SGMII standard. Default (0x19h * 65us ~ 1.6ms)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsCfgLinkTimerBasex(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IpEthTripPcsCfgLinkTimerBasex"
            
            def description(self):
                return "Auto-Negotiation cycle by the Link timer with 1000 BASE-XI standard, Default  (0x99h * 65us ~ 10ms)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IpEthTripPcsCfgLinkTimerSgmii"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_Link_Timer._IpEthTripPcsCfgLinkTimerSgmii()
            allFields["IpEthTripPcsCfgLinkTimerBasex"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_Link_Timer._IpEthTripPcsCfgLinkTimerBasex()
            return allFields

    class _IP_Ethernet_Triple_Speed_PCS_Option_Sync_Timer(AtRegister.AtRegister):
        def name(self):
            return "IP Ethernet Triple Speed PCS Option Sync Timer"
    
        def description(self):
            return "This register is PCS Sync Timer, unit = 65535 ns ~ 65us"
            
        def width(self):
            return 17
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000039
            
        def endAddress(self):
            return 0xffffffff

        class _IpEthTripPcsCfgSyncDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "IpEthTripPcsCfgSyncDis"
            
            def description(self):
                return "Disable monitor sync"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsCfgSyncTimer(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "IpEthTripPcsCfgSyncTimer"
            
            def description(self):
                return "standard. Default (0x7Fh * 65us ~ 8.25ms)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IpEthTripPcsCfgSyncDis"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_Sync_Timer._IpEthTripPcsCfgSyncDis()
            allFields["IpEthTripPcsCfgSyncTimer"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_Sync_Timer._IpEthTripPcsCfgSyncTimer()
            return allFields

    class _IP_Ethernet_Triple_Speed_PCS_Option_PRBS_Test_Control(AtRegister.AtRegister):
        def name(self):
            return "IP Ethernet Triple Speed PCS Option PRBS Test Control"
    
        def description(self):
            return "This register is PRBS test status"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000003c
            
        def endAddress(self):
            return 0xffffffff

        class _IpEthTripPcsCtlTestPrbsEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "IpEthTripPcsCtlTestPrbsEn"
            
            def description(self):
                return "Enable PRBS test 1 : Enable 0 : Normal Operations"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripPcsCtlTestAlignEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "IpEthTripPcsCtlTestAlignEn"
            
            def description(self):
                return "Enable PRBS monitor request align to SERDES 1 : Enable 0 : Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IPEthTripPcsTestFixDat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 4
        
            def name(self):
                return "IPEthTripPcsTestFixDat"
            
            def description(self):
                return "Fix Data generation"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IPEthTripPcsTestMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IPEthTripPcsTestMode"
            
            def description(self):
                return "Test mode [0]: Set 1 PRBS15 Test, else Fix Pattern Test [3:2]: Force Error"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IpEthTripPcsCtlTestPrbsEn"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_PRBS_Test_Control._IpEthTripPcsCtlTestPrbsEn()
            allFields["IpEthTripPcsCtlTestAlignEn"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_PRBS_Test_Control._IpEthTripPcsCtlTestAlignEn()
            allFields["IPEthTripPcsTestFixDat"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_PRBS_Test_Control._IPEthTripPcsTestFixDat()
            allFields["IPEthTripPcsTestMode"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_PRBS_Test_Control._IPEthTripPcsTestMode()
            return allFields

    class _IP_Ethernet_Triple_Speed_PCS_Option_PRBS_Test_Status(AtRegister.AtRegister):
        def name(self):
            return "IP Ethernet Triple Speed PCS Option PRBS Test Status"
    
        def description(self):
            return "This register is PRBS test status"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000003d
            
        def endAddress(self):
            return 0xffffffff

        class _IPEthTripPcsTestRxAlign(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "IPEthTripPcsTestRxAlign"
            
            def description(self):
                return "0x0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IPEthTripPcsTestCurrRxData(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 4
        
            def name(self):
                return "IPEthTripPcsTestCurrRxData"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IPEthTripPcsTestCurrPrbs15Er(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "IPEthTripPcsTestCurrPrbs15Er"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IPEthTripPcsTestCurrFixDatEr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "IPEthTripPcsTestCurrFixDatEr"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IPEthTripPcsTestLatchPrbs15Er(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "IPEthTripPcsTestLatchPrbs15Er"
            
            def description(self):
                return "0x0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IPEthTripPcsTestLatchFixDatEr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IPEthTripPcsTestLatchFixDatEr"
            
            def description(self):
                return "0x0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IPEthTripPcsTestRxAlign"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_PRBS_Test_Status._IPEthTripPcsTestRxAlign()
            allFields["IPEthTripPcsTestCurrRxData"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_PRBS_Test_Status._IPEthTripPcsTestCurrRxData()
            allFields["IPEthTripPcsTestCurrPrbs15Er"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_PRBS_Test_Status._IPEthTripPcsTestCurrPrbs15Er()
            allFields["IPEthTripPcsTestCurrFixDatEr"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_PRBS_Test_Status._IPEthTripPcsTestCurrFixDatEr()
            allFields["IPEthTripPcsTestLatchPrbs15Er"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_PRBS_Test_Status._IPEthTripPcsTestLatchPrbs15Er()
            allFields["IPEthTripPcsTestLatchFixDatEr"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_PCS_Option_PRBS_Test_Status._IPEthTripPcsTestLatchFixDatEr()
            return allFields

    class _IP_Ethernet_Triple_Speed_Simple_MAC_Active_Control(AtRegister.AtRegister):
        def name(self):
            return "IP Ethernet Triple Speed Simple MAC Active Control"
    
        def description(self):
            return "This register is Active Control"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000040
            
        def endAddress(self):
            return 0xffffffff

        class _IpEthTripSmacLoopout(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "IpEthTripSmacLoopout"
            
            def description(self):
                return "Loop out Enable (TX ? RX) 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripSmacTxActive(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "IpEthTripSmacTxActive"
            
            def description(self):
                return "Active transmitter side 1: Active 0: No Active"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripSmacLoopin(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "IpEthTripSmacLoopin"
            
            def description(self):
                return "Loop In Enable (TX ? RX) 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripSmacRxActive(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IpEthTripSmacRxActive"
            
            def description(self):
                return "Active receiver side 1: Active 0: No Active"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IpEthTripSmacLoopout"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Active_Control._IpEthTripSmacLoopout()
            allFields["IpEthTripSmacTxActive"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Active_Control._IpEthTripSmacTxActive()
            allFields["IpEthTripSmacLoopin"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Active_Control._IpEthTripSmacLoopin()
            allFields["IpEthTripSmacRxActive"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Active_Control._IpEthTripSmacRxActive()
            return allFields

    class _IP_Ethernet_Triple_Speed_Simple_MAC_Receiver_Control(AtRegister.AtRegister):
        def name(self):
            return "IP Ethernet Triple Speed Simple MAC Receiver Control"
    
        def description(self):
            return "This register is Simple MAC Receiver Control"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000042
            
        def endAddress(self):
            return 0xffffffff

        class _IpEthTripSmacRxMask(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "IpEthTripSmacRxMask"
            
            def description(self):
                return "Mask Error forward to user [3]: Ipg_Error: set 1 to Enable, else is disable [2]: Rx_Error: set 1 to Enable, else is disable [1]: Prm_Error: set 1 to Enable, else is disable [0]: Fcs_Error: set 1 to Enable, else is disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripSmacRxIpg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "IpEthTripSmacRxIpg"
            
            def description(self):
                return "Number of Inter packet Gap can detected for Receiver sides"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripSmacRxPrm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "IpEthTripSmacRxPrm"
            
            def description(self):
                return "Number of preamble bytes that receiver side can be received, this is maximum preamble bytes need to detect and one SFD byte"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripSamcRxPauSaChk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "IpEthTripSamcRxPauSaChk"
            
            def description(self):
                return "Enable Pause  frame terminate SA check 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripSamcRxPauDaChk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "IpEthTripSamcRxPauDaChk"
            
            def description(self):
                return "Enable Pause  frame terminate DA check 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripSmacRxPauDiChk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "IpEthTripSmacRxPauDiChk"
            
            def description(self):
                return "Disable Pause frame termination 1: Disable 0: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripSmacRxFcsPass(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IpEthTripSmacRxFcsPass"
            
            def description(self):
                return "Enable pass 4 byte FCS 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IpEthTripSmacRxMask"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Receiver_Control._IpEthTripSmacRxMask()
            allFields["IpEthTripSmacRxIpg"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Receiver_Control._IpEthTripSmacRxIpg()
            allFields["IpEthTripSmacRxPrm"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Receiver_Control._IpEthTripSmacRxPrm()
            allFields["IpEthTripSamcRxPauSaChk"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Receiver_Control._IpEthTripSamcRxPauSaChk()
            allFields["IpEthTripSamcRxPauDaChk"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Receiver_Control._IpEthTripSamcRxPauDaChk()
            allFields["IpEthTripSmacRxPauDiChk"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Receiver_Control._IpEthTripSmacRxPauDiChk()
            allFields["IpEthTripSmacRxFcsPass"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Receiver_Control._IpEthTripSmacRxFcsPass()
            return allFields

    class _IP_Ethernet_Triple_Speed_Simple_MAC_Transmitter_Control(AtRegister.AtRegister):
        def name(self):
            return "IP Ethernet Triple Speed Simple MAC Transmitter Control"
    
        def description(self):
            return "This register is Simple MAC Transmitter Control"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _IpEthTripSmacTxIpg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 8
        
            def name(self):
                return "IpEthTripSmacTxIpg"
            
            def description(self):
                return "Number of Inter packet Gap generated for Transmitter sides. Value range of parameter is from 4 to 31"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripSmacTxPrm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "IpEthTripSmacTxPrm"
            
            def description(self):
                return "Number of preamble bytes (not include SFD byte) that Transmitter generator."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripSmacTxEr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "IpEthTripSmacTxEr"
            
            def description(self):
                return "Force TX_ER"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripSamcTxPadDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "IpEthTripSamcTxPadDis"
            
            def description(self):
                return "Enable PAD insertion 1: Disable 0: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripSmacTxPauDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "IpEthTripSmacTxPauDis"
            
            def description(self):
                return "Enable Pause  frame generator 1: Disable 0: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripSmacTxFcsIns(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IpEthTripSmacTxFcsIns"
            
            def description(self):
                return "Enable FCS Insertion 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IpEthTripSmacTxIpg"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Transmitter_Control._IpEthTripSmacTxIpg()
            allFields["IpEthTripSmacTxPrm"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Transmitter_Control._IpEthTripSmacTxPrm()
            allFields["IpEthTripSmacTxEr"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Transmitter_Control._IpEthTripSmacTxEr()
            allFields["IpEthTripSamcTxPadDis"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Transmitter_Control._IpEthTripSamcTxPadDis()
            allFields["IpEthTripSmacTxPauDis"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Transmitter_Control._IpEthTripSmacTxPauDis()
            allFields["IpEthTripSmacTxFcsIns"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Transmitter_Control._IpEthTripSmacTxFcsIns()
            return allFields

    class _IP_Ethernet_Triple_Speed_Simple_MAC_Data_Inter_Frame(AtRegister.AtRegister):
        def name(self):
            return "IP Ethernet Triple Speed Simple MAC Data Inter Frame"
    
        def description(self):
            return "This register is Simple MAC Data Inter Frame Control"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000045
            
        def endAddress(self):
            return 0xffffffff

        class _IpEthTripSmacIdleRxData(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "IpEthTripSmacIdleRxData"
            
            def description(self):
                return "Data Inter Frame Receive from PHY or PCS"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripSmacIdleTxData(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IpEthTripSmacIdleTxData"
            
            def description(self):
                return "Data Inter Frame Transmit to PHY or PCS"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IpEthTripSmacIdleRxData"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Data_Inter_Frame._IpEthTripSmacIdleRxData()
            allFields["IpEthTripSmacIdleTxData"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Data_Inter_Frame._IpEthTripSmacIdleTxData()
            return allFields

    class _IP_Ethernet_Triple_Speed_Simple_MAC_flow_Control_Time_Interval(AtRegister.AtRegister):
        def name(self):
            return "IP Ethernet Triple Speed Simple MAC flow Control Time Interval"
    
        def description(self):
            return "This register is Simple MAC the Time Interval, it used for engine flow control."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000046
            
        def endAddress(self):
            return 0xffffffff

        class _IpEthTripSmacTimeInterval(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IpEthTripSmacTimeInterval"
            
            def description(self):
                return "In an time interval, a pause frame with quanta value that is configured bel0, will be transmitted if received packet FIFO is full, unit value is 512 bits time or 64 bytes time"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IpEthTripSmacTimeInterval"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_flow_Control_Time_Interval._IpEthTripSmacTimeInterval()
            return allFields

    class _IP_Ethernet_Triple_Speed_Simple_MAC_flow_Control_Quanta(AtRegister.AtRegister):
        def name(self):
            return "IP Ethernet Triple Speed Simple MAC flow Control Quanta"
    
        def description(self):
            return "This register is Simple MAC the Quanta, it used for engine flow control."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000047
            
        def endAddress(self):
            return 0xffffffff

        class _IpEthTripSmacQuanta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IpEthTripSmacQuanta"
            
            def description(self):
                return "Quanta value of pause ON frame, unit is 512 bits time or 64 bytes time"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IpEthTripSmacQuanta"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_flow_Control_Quanta._IpEthTripSmacQuanta()
            return allFields

    class _IP_Ethernet_Triple_Speed_Simple_MAC_Interrupt_Status(AtRegister.AtRegister):
        def name(self):
            return "IP Ethernet Triple Speed Simple MAC Interrupt Status"
    
        def description(self):
            return "This register is Interrupt status."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000048
            
        def endAddress(self):
            return 0xffffffff

        class _IpEthTripSmacViolInfo(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "IpEthTripSmacViolInfo"
            
            def description(self):
                return "Information violation between SMAC and USER at TxSide"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripSmacLostSop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "IpEthTripSmacLostSop"
            
            def description(self):
                return "Lost SOP detection"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripSmacLostEop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IpEthTripSmacLostEop"
            
            def description(self):
                return "Lost EOP detection or TX FIFO empty when packet transmitting"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IpEthTripSmacViolInfo"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Interrupt_Status._IpEthTripSmacViolInfo()
            allFields["IpEthTripSmacLostSop"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Interrupt_Status._IpEthTripSmacLostSop()
            allFields["IpEthTripSmacLostEop"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Interrupt_Status._IpEthTripSmacLostEop()
            return allFields

    class _IP_Ethernet_Triple_Speed_Simple_MAC_Latch_Status(AtRegister.AtRegister):
        def name(self):
            return "IP Ethernet Triple Speed Simple MAC Latch Status"
    
        def description(self):
            return "This register is latch status, Used to HW debug"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000049
            
        def endAddress(self):
            return 0xffffffff

        class _IpEthTripSmacLatPauGen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "IpEthTripSmacLatPauGen"
            
            def description(self):
                return "0x0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripSmacLatUsrRxVl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "IpEthTripSmacLatUsrRxVl"
            
            def description(self):
                return "0x0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripSmacLatUsrTxVl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "IpEthTripSmacLatUsrTxVl"
            
            def description(self):
                return "0x0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripSmacLatSmacTxRdEr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "IpEthTripSmacLatSmacTxRdEr"
            
            def description(self):
                return "0x0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripSmacLatSmacTxReq(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "IpEthTripSmacLatSmacTxReq"
            
            def description(self):
                return "0x0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripSmacLatSmacTxVl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "IpEthTripSmacLatSmacTxVl"
            
            def description(self):
                return "0x0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripSmacLatSmacRxEr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "IpEthTripSmacLatSmacRxEr"
            
            def description(self):
                return "0x0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripSmacLatSmacRxDv(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "IpEthTripSmacLatSmacRxDv"
            
            def description(self):
                return "0x0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripSmacLatSmacTxEr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "IpEthTripSmacLatSmacTxEr"
            
            def description(self):
                return "0x0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IpEthTripSmacLatSmacTxEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IpEthTripSmacLatSmacTxEn"
            
            def description(self):
                return "0x0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IpEthTripSmacLatPauGen"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Latch_Status._IpEthTripSmacLatPauGen()
            allFields["IpEthTripSmacLatUsrRxVl"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Latch_Status._IpEthTripSmacLatUsrRxVl()
            allFields["IpEthTripSmacLatUsrTxVl"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Latch_Status._IpEthTripSmacLatUsrTxVl()
            allFields["IpEthTripSmacLatSmacTxRdEr"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Latch_Status._IpEthTripSmacLatSmacTxRdEr()
            allFields["IpEthTripSmacLatSmacTxReq"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Latch_Status._IpEthTripSmacLatSmacTxReq()
            allFields["IpEthTripSmacLatSmacTxVl"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Latch_Status._IpEthTripSmacLatSmacTxVl()
            allFields["IpEthTripSmacLatSmacRxEr"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Latch_Status._IpEthTripSmacLatSmacRxEr()
            allFields["IpEthTripSmacLatSmacRxDv"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Latch_Status._IpEthTripSmacLatSmacRxDv()
            allFields["IpEthTripSmacLatSmacTxEr"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Latch_Status._IpEthTripSmacLatSmacTxEr()
            allFields["IpEthTripSmacLatSmacTxEn"] = _AF6CNC0011_RD_ETH._IP_Ethernet_Triple_Speed_Simple_MAC_Latch_Status._IpEthTripSmacLatSmacTxEn()
            return allFields

    class _ETH_FCS_errins_en_cfg(AtRegister.AtRegister):
        def name(self):
            return "ETH FCS Force Error Control"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00001007
            
        def endAddress(self):
            return 0xffffffff

        class _ETHFCSErrMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 29
        
            def name(self):
                return "ETHFCSErrMode"
            
            def description(self):
                return "0: FCS (MAC); 1:Tx interface MAC2PCS; 2: Rx interface PCS2MAC"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ETHFCSErrRate(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "ETHFCSErrRate"
            
            def description(self):
                return "0: One-shot with number of errors; 1:Line rate"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ETHFCSErrThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ETHFCSErrThr"
            
            def description(self):
                return "Error Threshold in bit unit or the number of error in one-shot mode. Valid value from 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ETHFCSErrMode"] = _AF6CNC0011_RD_ETH._ETH_FCS_errins_en_cfg._ETHFCSErrMode()
            allFields["ETHFCSErrRate"] = _AF6CNC0011_RD_ETH._ETH_FCS_errins_en_cfg._ETHFCSErrRate()
            allFields["ETHFCSErrThr"] = _AF6CNC0011_RD_ETH._ETH_FCS_errins_en_cfg._ETHFCSErrThr()
            return allFields

import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0011_RD_CLS(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["upen_dasa"] = _AF6CNC0011_RD_CLS._upen_dasa()
        allRegisters["upen_da31"] = _AF6CNC0011_RD_CLS._upen_da31()
        allRegisters["upen_sa31"] = _AF6CNC0011_RD_CLS._upen_sa31()
        allRegisters["upen_type"] = _AF6CNC0011_RD_CLS._upen_type()
        allRegisters["upen_mask"] = _AF6CNC0011_RD_CLS._upen_mask()
        allRegisters["upen_dump"] = _AF6CNC0011_RD_CLS._upen_dump()
        allRegisters["upen_count"] = _AF6CNC0011_RD_CLS._upen_count()
        return allRegisters

    class _upen_dasa(AtRegister.AtRegister):
        def name(self):
            return "Ethernet DA bit47_32 Configuration"
    
        def description(self):
            return "This register  is used to configure the DA bit47_32"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _DA_bit47_32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "DA_bit47_32"
            
            def description(self):
                return "DA bit47_32"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _SA_bit47_32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SA_bit47_32"
            
            def description(self):
                return "SA bit47_32"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DA_bit47_32"] = _AF6CNC0011_RD_CLS._upen_dasa._DA_bit47_32()
            allFields["SA_bit47_32"] = _AF6CNC0011_RD_CLS._upen_dasa._SA_bit47_32()
            return allFields

    class _upen_da31(AtRegister.AtRegister):
        def name(self):
            return "Ethernet DA bit31_00 Configuration"
    
        def description(self):
            return "This register  is used to configure the DA bit32_00"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000001
            
        def endAddress(self):
            return 0xffffffff

        class _DA_bit31_00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DA_bit31_00"
            
            def description(self):
                return "DA bit31_00"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DA_bit31_00"] = _AF6CNC0011_RD_CLS._upen_da31._DA_bit31_00()
            return allFields

    class _upen_sa31(AtRegister.AtRegister):
        def name(self):
            return "Ethernet SA bit31_00 Configuration"
    
        def description(self):
            return "This register  is used to configure the SA bit31_00"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0xffffffff

        class _SA_bit31_00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SA_bit31_00"
            
            def description(self):
                return "SA bit31_00"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SA_bit31_00"] = _AF6CNC0011_RD_CLS._upen_sa31._SA_bit31_00()
            return allFields

    class _upen_type(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Sub_Type Configuration"
    
        def description(self):
            return "This register  is used to configure E-type and Sub_Type"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _Sub_type_mask(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "Sub_type_mask"
            
            def description(self):
                return "Bit enable to mask a sub-type bit"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Sub_type_value(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Sub_type_value"
            
            def description(self):
                return "Ethernet SubType of control packet 0thers 	: data frame 0x80-0xFF	: control frames"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _E_type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "E_type"
            
            def description(self):
                return "E_type of pkt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Sub_type_mask"] = _AF6CNC0011_RD_CLS._upen_type._Sub_type_mask()
            allFields["Sub_type_value"] = _AF6CNC0011_RD_CLS._upen_type._Sub_type_value()
            allFields["E_type"] = _AF6CNC0011_RD_CLS._upen_type._E_type()
            return allFields

    class _upen_mask(AtRegister.AtRegister):
        def name(self):
            return "Classified Ethernet field mask  Configuration"
    
        def description(self):
            return "This register  is used to mask some field of packet. These mask field are checked to discard or forward pkt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000004
            
        def endAddress(self):
            return 0xffffffff

        class _E_type_mask(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "E_type_mask"
            
            def description(self):
                return "mask E_type of packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _SA_mask(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "SA_mask"
            
            def description(self):
                return "mask SA of packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _DA_mask(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DA_mask"
            
            def description(self):
                return "mask DA of packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["E_type_mask"] = _AF6CNC0011_RD_CLS._upen_mask._E_type_mask()
            allFields["SA_mask"] = _AF6CNC0011_RD_CLS._upen_mask._SA_mask()
            allFields["DA_mask"] = _AF6CNC0011_RD_CLS._upen_mask._DA_mask()
            return allFields

    class _upen_dump(AtRegister.AtRegister):
        def name(self):
            return "Dump packet  Configuration"
    
        def description(self):
            return "This register  is used to mask some field of packet. These mask field are checked to discard or forward pkt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000006
            
        def endAddress(self):
            return 0xffffffff

        class _dump_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 1
        
            def name(self):
                return "dump_typ"
            
            def description(self):
                return "Dump pkt type 00 : dump pkt input 01 : dump pkt data 10 : dump pkt control 11 : dump pkt data & control"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _dump_enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dump_enb"
            
            def description(self):
                return "dump pkt enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dump_typ"] = _AF6CNC0011_RD_CLS._upen_dump._dump_typ()
            allFields["dump_enb"] = _AF6CNC0011_RD_CLS._upen_dump._dump_enb()
            return allFields

    class _upen_count(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Encapsulation EtheType and EtherLength  Configuration"
    
        def description(self):
            return "This register is used to read classified packet counters , support both mode r2c and ro Counter ID detail : + 0 	: data    packet counter + 1 	: control packet counter + 2 	: total  discard packet counter + 3		: da err discard packet counter + 4		: sa err discard packet counter + 5		: etype err discard packet counter + 6		: fcs_err discard packet counter + 7		: unused r2c		: enable mean read to clear"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1000 + $r2c*8 + $id"
            
        def startAddress(self):
            return 0x00001000
            
        def endAddress(self):
            return 0x0000100f

        class _Ether_Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Ether_Type"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Ether_Type"] = _AF6CNC0011_RD_CLS._upen_count._Ether_Type()
            return allFields

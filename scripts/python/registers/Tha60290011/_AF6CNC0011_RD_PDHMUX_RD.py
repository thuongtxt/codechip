import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0011_RD_PDHMUX_RD(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["upen0"] = _AF6CNC0011_RD_PDHMUX_RD._upen0()
        allRegisters["upen1"] = _AF6CNC0011_RD_PDHMUX_RD._upen1()
        allRegisters["upen_c_0"] = _AF6CNC0011_RD_PDHMUX_RD._upen_c_0()
        allRegisters["upen_c_1"] = _AF6CNC0011_RD_PDHMUX_RD._upen_c_1()
        allRegisters["upen_c_2"] = _AF6CNC0011_RD_PDHMUX_RD._upen_c_2()
        allRegisters["upen2"] = _AF6CNC0011_RD_PDHMUX_RD._upen2()
        allRegisters["upen3"] = _AF6CNC0011_RD_PDHMUX_RD._upen3()
        allRegisters["upen4"] = _AF6CNC0011_RD_PDHMUX_RD._upen4()
        allRegisters["upen5"] = _AF6CNC0011_RD_PDHMUX_RD._upen5()
        allRegisters["upen6"] = _AF6CNC0011_RD_PDHMUX_RD._upen6()
        allRegisters["upen7"] = _AF6CNC0011_RD_PDHMUX_RD._upen7()
        allRegisters["upen8"] = _AF6CNC0011_RD_PDHMUX_RD._upen8()
        allRegisters["upen9"] = _AF6CNC0011_RD_PDHMUX_RD._upen9()
        allRegisters["upen12"] = _AF6CNC0011_RD_PDHMUX_RD._upen12()
        allRegisters["upen13"] = _AF6CNC0011_RD_PDHMUX_RD._upen13()
        allRegisters["upen14"] = _AF6CNC0011_RD_PDHMUX_RD._upen14()
        allRegisters["upen15"] = _AF6CNC0011_RD_PDHMUX_RD._upen15()
        allRegisters["upen16"] = _AF6CNC0011_RD_PDHMUX_RD._upen16()
        allRegisters["upen17"] = _AF6CNC0011_RD_PDHMUX_RD._upen17()
        return allRegisters

    class _upen0(AtRegister.AtRegister):
        def name(self):
            return "Active Regiter"
    
        def description(self):
            return "This register configures Active for all Arrive modules"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _AXI4_Stream_loopout(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "AXI4_Stream_loopout"
            
            def description(self):
                return "AXI4 Stream loop_out"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _AXI4_Stream_loopin(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "AXI4_Stream_loopin"
            
            def description(self):
                return "AXI4 Stream loop_in"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Active(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Active"
            
            def description(self):
                return "Module Active - Set 1: Active all modules - Set 0: De-active all modules + Flush all FIFOs + Reset all Status and Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["AXI4_Stream_loopout"] = _AF6CNC0011_RD_PDHMUX_RD._upen0._AXI4_Stream_loopout()
            allFields["AXI4_Stream_loopin"] = _AF6CNC0011_RD_PDHMUX_RD._upen0._AXI4_Stream_loopin()
            allFields["Active"] = _AF6CNC0011_RD_PDHMUX_RD._upen0._Active()
            return allFields

    class _upen1(AtRegister.AtRegister):
        def name(self):
            return "Version ID"
    
        def description(self):
            return "This register shows Version ID"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000001
            
        def endAddress(self):
            return 0xffffffff

        class _Verion_Year(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "Verion_Year"
            
            def description(self):
                return "Version Year"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Verion_Month(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Verion_Month"
            
            def description(self):
                return "Version Month"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Verion_Date(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Verion_Date"
            
            def description(self):
                return "Version Date"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Verion_Number(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Verion_Number"
            
            def description(self):
                return "Version Number"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Verion_Year"] = _AF6CNC0011_RD_PDHMUX_RD._upen1._Verion_Year()
            allFields["Verion_Month"] = _AF6CNC0011_RD_PDHMUX_RD._upen1._Verion_Month()
            allFields["Verion_Date"] = _AF6CNC0011_RD_PDHMUX_RD._upen1._Verion_Date()
            allFields["Verion_Number"] = _AF6CNC0011_RD_PDHMUX_RD._upen1._Verion_Number()
            return allFields

    class _upen_c_0(AtRegister.AtRegister):
        def name(self):
            return "PDH to LIU Look-up Table"
    
        def description(self):
            return "This configure is used to convert ID PDH to LIU ID"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1000 + stsid*32 + vtgid*4 + vtid"
            
        def startAddress(self):
            return 0x00001000
            
        def endAddress(self):
            return 0x000013ff

        class _LiuEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "LiuEnb"
            
            def description(self):
                return "LIU Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _LiuMod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "LiuMod"
            
            def description(self):
                return "LIU Mode - 0: For DE1 - 1: For DE3/EC1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _LiuId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LiuId"
            
            def description(self):
                return "Liu ID - 0-83:For DE1 - 0-23:For DE3/EC1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["LiuEnb"] = _AF6CNC0011_RD_PDHMUX_RD._upen_c_0._LiuEnb()
            allFields["LiuMod"] = _AF6CNC0011_RD_PDHMUX_RD._upen_c_0._LiuMod()
            allFields["LiuId"] = _AF6CNC0011_RD_PDHMUX_RD._upen_c_0._LiuId()
            return allFields

    class _upen_c_1(AtRegister.AtRegister):
        def name(self):
            return "LIU to PDH Look-up Table"
    
        def description(self):
            return "This configure is used to convert ID PDH to LIU ID"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1400 + liuid"
            
        def startAddress(self):
            return 0x00001400
            
        def endAddress(self):
            return 0x00001453

        class _LiuEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "LiuEnb"
            
            def description(self):
                return "LIU Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDHId"
            
            def description(self):
                return "PDH ID (STS,VTG,VT)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["LiuEnb"] = _AF6CNC0011_RD_PDHMUX_RD._upen_c_1._LiuEnb()
            allFields["PDHId"] = _AF6CNC0011_RD_PDHMUX_RD._upen_c_1._PDHId()
            return allFields

    class _upen_c_2(AtRegister.AtRegister):
        def name(self):
            return "NCO mode LIU to PDH"
    
        def description(self):
            return "This configure is used to convert ID PDH to LIU ID"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1500 + liuid"
            
        def startAddress(self):
            return 0x00001500
            
        def endAddress(self):
            return 0x00001553

        class _txncomd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txncomd"
            
            def description(self):
                return "tx_nco_mode - Set 4: EC1. - Set 3: E3. - Set 2: DS3. - Set 1: E1. - Set 0: DS1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txncomd"] = _AF6CNC0011_RD_PDHMUX_RD._upen_c_2._txncomd()
            return allFields

    class _upen2(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Encapsulation DA bit47_32 Configuration"
    
        def description(self):
            return "This register  is used to configure the DA bit47_32"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0xffffffff

        class _DA_bit47_32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DA_bit47_32"
            
            def description(self):
                return "DA bit47_32"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DA_bit47_32"] = _AF6CNC0011_RD_PDHMUX_RD._upen2._DA_bit47_32()
            return allFields

    class _upen3(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Encapsulation DA bit32_00 Configuration"
    
        def description(self):
            return "This register  is used to configure the DA bit32_00"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _DA_bit31_00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DA_bit31_00"
            
            def description(self):
                return "DA bit31_00"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DA_bit31_00"] = _AF6CNC0011_RD_PDHMUX_RD._upen3._DA_bit31_00()
            return allFields

    class _upen4(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Encapsulation SA bit47_32 Configuration"
    
        def description(self):
            return "This register  is used to configure the SA bit47_32 and Sub_Type"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000004
            
        def endAddress(self):
            return 0xffffffff

        class _Ether_Subtype(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Ether_Subtype"
            
            def description(self):
                return "Ethernet SubType - 0x0		: DS1/E1 Frames - 0x1-0xF 	: reserved - 0x10		: DS3/E3/EC-1 Frames - 0x11-0x7F	: reserved - 0x80-0xFF	: control frames"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _SA_bit47_32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SA_bit47_32"
            
            def description(self):
                return "SA bit47_32"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Ether_Subtype"] = _AF6CNC0011_RD_PDHMUX_RD._upen4._Ether_Subtype()
            allFields["SA_bit47_32"] = _AF6CNC0011_RD_PDHMUX_RD._upen4._SA_bit47_32()
            return allFields

    class _upen5(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Encapsulation SA bit32_00 Configuration"
    
        def description(self):
            return "This register  is used to configure the SA bit32_00"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000005
            
        def endAddress(self):
            return 0xffffffff

        class _SA_bit31_00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SA_bit31_00"
            
            def description(self):
                return "SA bit31_00"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SA_bit31_00"] = _AF6CNC0011_RD_PDHMUX_RD._upen5._SA_bit31_00()
            return allFields

    class _upen6(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Encapsulation EtheType and EtherLength  Configuration"
    
        def description(self):
            return "This register is used to configure the EtheType and EtherLength"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000006
            
        def endAddress(self):
            return 0xffffffff

        class _Ether_Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Ether_Type"
            
            def description(self):
                return "Ethernet Type"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Ether_Len(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Ether_Len"
            
            def description(self):
                return "Ethernet Length - DS1/E1: number of DS1/E1 * 2 bytes + 2 (seq num) - DS3/E3/EC-1: number of DS3/E3/EC-1 * 6 bytes + 2 - Control frames: length of control frame payload"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Ether_Type"] = _AF6CNC0011_RD_PDHMUX_RD._upen6._Ether_Type()
            allFields["Ether_Len"] = _AF6CNC0011_RD_PDHMUX_RD._upen6._Ether_Len()
            return allFields

    class _upen7(AtRegister.AtRegister):
        def name(self):
            return "TX LIU FIFO Overrun Counter"
    
        def description(self):
            return "TX LIU overrun counter. Write 0x0 to clear"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0200 + liuid"
            
        def startAddress(self):
            return 0x00000200
            
        def endAddress(self):
            return 0x00000253

        class _txliuoverrun_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txliuoverrun_cnt"
            
            def description(self):
                return "Tx_LUI_overrun_counter"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txliuoverrun_cnt"] = _AF6CNC0011_RD_PDHMUX_RD._upen7._txliuoverrun_cnt()
            return allFields

    class _upen8(AtRegister.AtRegister):
        def name(self):
            return "TX LIU FIFO Underrun Counter"
    
        def description(self):
            return "TX LIU Underrun counter. Write 0x0 to clear"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0280 + liuid"
            
        def startAddress(self):
            return 0x00000280
            
        def endAddress(self):
            return 0x000002d3

        class _txliuunderrun_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txliuunderrun_cnt"
            
            def description(self):
                return "Tx_LUI_underrun_counter"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txliuunderrun_cnt"] = _AF6CNC0011_RD_PDHMUX_RD._upen8._txliuunderrun_cnt()
            return allFields

    class _upen9(AtRegister.AtRegister):
        def name(self):
            return "RX PDH FIFO Overrun"
    
        def description(self):
            return "RX LIU FIFO is full. Write 0xFFFF_FFFF to clear"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000010
            
        def endAddress(self):
            return 0xffffffff

        class _rxliufull(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxliufull"
            
            def description(self):
                return "Rx_Liu_Fifo_Full - Bit#0 for LIU ID#0 - Bit#1 for LIU ID#1 - Bit#31 for LIU ID#31 - Set 1: shown FIFO Full"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxliufull"] = _AF6CNC0011_RD_PDHMUX_RD._upen9._rxliufull()
            return allFields

    class _upen12(AtRegister.AtRegister):
        def name(self):
            return "AXI4-Stream (PDH to MAC) Packet Counter"
    
        def description(self):
            return "AXI4-Stream Packet Counter. Write 0x0 to clear"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000013
            
        def endAddress(self):
            return 0xffffffff

        class _axispktcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "axispktcnt"
            
            def description(self):
                return "axis_packect_counter"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["axispktcnt"] = _AF6CNC0011_RD_PDHMUX_RD._upen12._axispktcnt()
            return allFields

    class _upen13(AtRegister.AtRegister):
        def name(self):
            return "AXI4-Stream (MAC to PDH) Byte Counter"
    
        def description(self):
            return "AXI4-Stream Byte Counter. Write 0x0 to clear"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000014
            
        def endAddress(self):
            return 0xffffffff

        class _axisbytecnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "axisbytecnt"
            
            def description(self):
                return "axis_byte_counter"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["axisbytecnt"] = _AF6CNC0011_RD_PDHMUX_RD._upen13._axisbytecnt()
            return allFields

    class _upen14(AtRegister.AtRegister):
        def name(self):
            return "AXI4-Stream (PDH to MAC) Packet Counter"
    
        def description(self):
            return "AXI4-Stream Packet Counter. Write 0x0 to clear"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000015
            
        def endAddress(self):
            return 0xffffffff

        class _axispktcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "axispktcnt"
            
            def description(self):
                return "axis_packect_counter"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["axispktcnt"] = _AF6CNC0011_RD_PDHMUX_RD._upen14._axispktcnt()
            return allFields

    class _upen15(AtRegister.AtRegister):
        def name(self):
            return "AXI4-Stream (PDH to MAC) Byte Counter"
    
        def description(self):
            return "AXI4-Stream Byte Counter. Write 0x0 to clear"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000016
            
        def endAddress(self):
            return 0xffffffff

        class _axisbytecnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "axisbytecnt"
            
            def description(self):
                return "axis_byte_counter"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["axisbytecnt"] = _AF6CNC0011_RD_PDHMUX_RD._upen15._axisbytecnt()
            return allFields

    class _upen16(AtRegister.AtRegister):
        def name(self):
            return "DA/SA error packet counter"
    
        def description(self):
            return "Counter DA/SA error packet of Decapsulation. Write 0x0 to clear"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000016
            
        def endAddress(self):
            return 0xffffffff

        class _pktcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pktcnt"
            
            def description(self):
                return "axis_byte_counter"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pktcnt"] = _AF6CNC0011_RD_PDHMUX_RD._upen16._pktcnt()
            return allFields

    class _upen17(AtRegister.AtRegister):
        def name(self):
            return "Debug Stick 0"
    
        def description(self):
            return "This sticky is used to debug. Write 0xFFFF_FFFF to clear"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000017
            
        def endAddress(self):
            return 0xffffffff

        class _Dec_Seq_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "Dec_Seq_err"
            
            def description(self):
                return "Dec detect Sequence Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Dec_Eop_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "Dec_Eop_err"
            
            def description(self):
                return "Dec detect EOP Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Dec_EthLen_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Dec_EthLen_err"
            
            def description(self):
                return "Dec detect Ethernet Lenght Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Dec_EthTyp_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "Dec_EthTyp_err"
            
            def description(self):
                return "Dec detect Ethernet Type Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Dec_SubTyp_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Dec_SubTyp_err"
            
            def description(self):
                return "Dec detect Sub Type Error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Dec_DA_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Dec_DA_err"
            
            def description(self):
                return "Dec detect DA error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Dec_SA_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Dec_SA_err"
            
            def description(self):
                return "Dec detect SA error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Decbuf_read_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "Decbuf_read_err"
            
            def description(self):
                return "Dec buffer read when buffer is empty"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Decbuf_write_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Decbuf_write_err"
            
            def description(self):
                return "Dec buffer write when buffer is full"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Encbuf_read_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Encbuf_read_err"
            
            def description(self):
                return "Enc buffer read when buffer is empty"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Encbuf_write_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Encbuf_write_err"
            
            def description(self):
                return "Enc buffer write when buffer is full"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Dec_Seq_err"] = _AF6CNC0011_RD_PDHMUX_RD._upen17._Dec_Seq_err()
            allFields["Dec_Eop_err"] = _AF6CNC0011_RD_PDHMUX_RD._upen17._Dec_Eop_err()
            allFields["Dec_EthLen_err"] = _AF6CNC0011_RD_PDHMUX_RD._upen17._Dec_EthLen_err()
            allFields["Dec_EthTyp_err"] = _AF6CNC0011_RD_PDHMUX_RD._upen17._Dec_EthTyp_err()
            allFields["Dec_SubTyp_err"] = _AF6CNC0011_RD_PDHMUX_RD._upen17._Dec_SubTyp_err()
            allFields["Dec_DA_err"] = _AF6CNC0011_RD_PDHMUX_RD._upen17._Dec_DA_err()
            allFields["Dec_SA_err"] = _AF6CNC0011_RD_PDHMUX_RD._upen17._Dec_SA_err()
            allFields["Decbuf_read_err"] = _AF6CNC0011_RD_PDHMUX_RD._upen17._Decbuf_read_err()
            allFields["Decbuf_write_err"] = _AF6CNC0011_RD_PDHMUX_RD._upen17._Decbuf_write_err()
            allFields["Encbuf_read_err"] = _AF6CNC0011_RD_PDHMUX_RD._upen17._Encbuf_read_err()
            allFields["Encbuf_write_err"] = _AF6CNC0011_RD_PDHMUX_RD._upen17._Encbuf_write_err()
            return allFields

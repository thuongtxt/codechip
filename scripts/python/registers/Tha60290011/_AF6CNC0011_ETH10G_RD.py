import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0011_ETH10G_RD(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["OETH_10G_DRP"] = _AF6CNC0011_ETH10G_RD._OETH_10G_DRP()
        allRegisters["ETH_10G_LoopBack"] = _AF6CNC0011_ETH10G_RD._ETH_10G_LoopBack()
        allRegisters["ETH_10G_QLL_Status"] = _AF6CNC0011_ETH10G_RD._ETH_10G_QLL_Status()
        allRegisters["ETH_10G_TX_Reset"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TX_Reset()
        allRegisters["ETH_49G_RX_Reset"] = _AF6CNC0011_ETH10G_RD._ETH_49G_RX_Reset()
        allRegisters["ETH_10G_LPMDFE_Mode"] = _AF6CNC0011_ETH10G_RD._ETH_10G_LPMDFE_Mode()
        allRegisters["ETH_10G_LPMDFE_Reset"] = _AF6CNC0011_ETH10G_RD._ETH_10G_LPMDFE_Reset()
        allRegisters["ETH_10G_TXDIFFCTRL"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TXDIFFCTRL()
        allRegisters["ETH_10G_TXPOSTCURSOR"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TXPOSTCURSOR()
        allRegisters["ETH_10G_TXPRECURSOR"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TXPRECURSOR()
        allRegisters["ETH_10G_Ctrl_FCS"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Ctrl_FCS()
        allRegisters["ETH_10G_AutoNeg"] = _AF6CNC0011_ETH10G_RD._ETH_10G_AutoNeg()
        allRegisters["ETH_10G_Diag_ctrl0"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_ctrl0()
        allRegisters["ETH_10G_Diag_ctrl1"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_ctrl1()
        allRegisters["ETH_10G_Diag_ctrl2"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_ctrl2()
        allRegisters["ETH_10G_Diag_ctrl3"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_ctrl3()
        allRegisters["ETH_10G_Diag_ctrl4"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_ctrl4()
        allRegisters["ETH_10G_Diag_ctrl6"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_ctrl6()
        allRegisters["ETH_10G_Diag_ctrl7"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_ctrl7()
        allRegisters["Gatetime_current"] = _AF6CNC0011_ETH10G_RD._Gatetime_current()
        allRegisters["ETH_10G_Diag_Sta0"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_Sta0()
        allRegisters["ETH_10G_Diag_TXPKT"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_TXPKT()
        allRegisters["ETH_10G_Diag_TXNOB"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_TXNOB()
        allRegisters["ETH_10G_Diag_RXPKT"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_RXPKT()
        allRegisters["ETH_10G_Diag_RXNOB"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_RXNOB()
        allRegisters["ETH_10G_TX_CFG"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TX_CFG()
        allRegisters["ETH_10G_RX_CFG"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_CFG()
        allRegisters["ETH_10G_CFG_TICK_REG"] = _AF6CNC0011_ETH10G_RD._ETH_10G_CFG_TICK_REG()
        allRegisters["ETH_10G_TX_STICKY"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TX_STICKY()
        allRegisters["ETH_10G_TX_ALARM"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TX_ALARM()
        allRegisters["ETH_10G_RX_STICKY"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_STICKY()
        allRegisters["ETH_10G_RX_ALARM"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_ALARM()
        allRegisters["ETH_10G_AN_STICKY"] = _AF6CNC0011_ETH10G_RD._ETH_10G_AN_STICKY()
        allRegisters["ETH_10G_AN_ALARM"] = _AF6CNC0011_ETH10G_RD._ETH_10G_AN_ALARM()
        allRegisters["ETH_10G_TX_INTEN"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TX_INTEN()
        allRegisters["ETH_10G_RX_INTEN"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_INTEN()
        allRegisters["ETH_10G_AN_INTEN"] = _AF6CNC0011_ETH10G_RD._ETH_10G_AN_INTEN()
        allRegisters["ETH_10G_Statistics_TX_COUNTER"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Statistics_TX_COUNTER()
        allRegisters["ETH_10G_Statistics_RX_Counters"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Statistics_RX_Counters()
        return allRegisters

    class _OETH_10G_DRP(AtRegister.AtRegister):
        def name(self):
            return "ETH 10GDRP"
    
        def description(self):
            return "Read/Write DRP address of SERDES"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1000+$P*0x400+$DRP"
            
        def startAddress(self):
            return 0x00001000
            
        def endAddress(self):
            return 0x00001fff

        class _drp_rw(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "drp_rw"
            
            def description(self):
                return "DRP read/write value"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["drp_rw"] = _AF6CNC0011_ETH10G_RD._OETH_10G_DRP._drp_rw()
            return allFields

    class _ETH_10G_LoopBack(AtRegister.AtRegister):
        def name(self):
            return "ETH 10GLoopBack"
    
        def description(self):
            return "Configurate LoopBack"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0xffffffff

        class _lpback_lane0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lpback_lane0"
            
            def description(self):
                return "Loopback lane0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lpback_lane0"] = _AF6CNC0011_ETH10G_RD._ETH_10G_LoopBack._lpback_lane0()
            return allFields

    class _ETH_10G_QLL_Status(AtRegister.AtRegister):
        def name(self):
            return "ETH 10GQLL Status"
    
        def description(self):
            return "QPLL status"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000b
            
        def endAddress(self):
            return 0xffffffff

        class _QPLL1_Lock_change(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "QPLL1_Lock_change"
            
            def description(self):
                return "QPLL1 has transition lock/unlock, Group 0-3"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _QPLL0_Lock_change(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "QPLL0_Lock_change"
            
            def description(self):
                return "QPLL0 has transition lock/unlock, Group 0-3"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _QPLL1_Lock(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "QPLL1_Lock"
            
            def description(self):
                return "QPLL0 is Locked, Group 0-3"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _QPLL0_Lock(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "QPLL0_Lock"
            
            def description(self):
                return "QPLL0 is Locked, Group 0-3"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["QPLL1_Lock_change"] = _AF6CNC0011_ETH10G_RD._ETH_10G_QLL_Status._QPLL1_Lock_change()
            allFields["QPLL0_Lock_change"] = _AF6CNC0011_ETH10G_RD._ETH_10G_QLL_Status._QPLL0_Lock_change()
            allFields["QPLL1_Lock"] = _AF6CNC0011_ETH10G_RD._ETH_10G_QLL_Status._QPLL1_Lock()
            allFields["QPLL0_Lock"] = _AF6CNC0011_ETH10G_RD._ETH_10G_QLL_Status._QPLL0_Lock()
            return allFields

    class _ETH_10G_TX_Reset(AtRegister.AtRegister):
        def name(self):
            return "ETH 10GTX Reset"
    
        def description(self):
            return "Reset TX SERDES"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000c
            
        def endAddress(self):
            return 0xffffffff

        class _txrst_done(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "txrst_done"
            
            def description(self):
                return "TX Reset Done"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _txrst_trig(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txrst_trig"
            
            def description(self):
                return "Trige 0->1 to start reset TX SERDES"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txrst_done"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TX_Reset._txrst_done()
            allFields["txrst_trig"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TX_Reset._txrst_trig()
            return allFields

    class _ETH_49G_RX_Reset(AtRegister.AtRegister):
        def name(self):
            return "ETH 10GRX Reset"
    
        def description(self):
            return "Reset RX SERDES"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000d
            
        def endAddress(self):
            return 0xffffffff

        class _rxrst_done(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "rxrst_done"
            
            def description(self):
                return "RX Reset Done"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _rxrst_trig(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxrst_trig"
            
            def description(self):
                return "Trige 0->1 to start reset RX SERDES"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxrst_done"] = _AF6CNC0011_ETH10G_RD._ETH_49G_RX_Reset._rxrst_done()
            allFields["rxrst_trig"] = _AF6CNC0011_ETH10G_RD._ETH_49G_RX_Reset._rxrst_trig()
            return allFields

    class _ETH_10G_LPMDFE_Mode(AtRegister.AtRegister):
        def name(self):
            return "ETH 10GLPMDFE Mode"
    
        def description(self):
            return "Configure LPM/DFE mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000e
            
        def endAddress(self):
            return 0xffffffff

        class _lpmdfe_mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lpmdfe_mode"
            
            def description(self):
                return "LPM/DFE mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lpmdfe_mode"] = _AF6CNC0011_ETH10G_RD._ETH_10G_LPMDFE_Mode._lpmdfe_mode()
            return allFields

    class _ETH_10G_LPMDFE_Reset(AtRegister.AtRegister):
        def name(self):
            return "ETH 10GLPMDFE Reset"
    
        def description(self):
            return "Reset LPM/DFE"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000f
            
        def endAddress(self):
            return 0xffffffff

        class _lpmdfe_reset(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lpmdfe_reset"
            
            def description(self):
                return "LPM/DFE reset"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lpmdfe_reset"] = _AF6CNC0011_ETH10G_RD._ETH_10G_LPMDFE_Reset._lpmdfe_reset()
            return allFields

    class _ETH_10G_TXDIFFCTRL(AtRegister.AtRegister):
        def name(self):
            return "ETH 10GTXDIFFCTRL"
    
        def description(self):
            return "Driver Swing Control, see \"Table 3-35: TX Configurable Driver Ports\" page 158 of UG578 for more detail"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000010
            
        def endAddress(self):
            return 0xffffffff

        class _TXDIFFCTRL(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TXDIFFCTRL"
            
            def description(self):
                return "TXDIFFCTRL"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TXDIFFCTRL"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TXDIFFCTRL._TXDIFFCTRL()
            return allFields

    class _ETH_10G_TXPOSTCURSOR(AtRegister.AtRegister):
        def name(self):
            return "ETH 10GTXPOSTCURSOR"
    
        def description(self):
            return "Transmitter post-cursor TX pre-emphasis control, see \"Table 3-35: TX Configurable Driver Ports\" page 160 of UG578 for more detail"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000011
            
        def endAddress(self):
            return 0xffffffff

        class _TXPOSTCURSOR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TXPOSTCURSOR"
            
            def description(self):
                return "TXPOSTCURSOR"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TXPOSTCURSOR"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TXPOSTCURSOR._TXPOSTCURSOR()
            return allFields

    class _ETH_10G_TXPRECURSOR(AtRegister.AtRegister):
        def name(self):
            return "ETH 10GTXPRECURSOR"
    
        def description(self):
            return "Transmitter pre-cursor TX pre-emphasis control, see \"Table 3-35: TX Configurable Driver Ports\" page 161 of UG578 for more detail"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000012
            
        def endAddress(self):
            return 0xffffffff

        class _TXPRECURSOR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TXPRECURSOR"
            
            def description(self):
                return "TXPRECURSOR"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TXPRECURSOR"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TXPRECURSOR._TXPRECURSOR()
            return allFields

    class _ETH_10G_Ctrl_FCS(AtRegister.AtRegister):
        def name(self):
            return "ETH 10GCtrl FCS"
    
        def description(self):
            return "configure FCS mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000080
            
        def endAddress(self):
            return 0xffffffff

        class _txfcs_ignore(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "txfcs_ignore"
            
            def description(self):
                return "TX ignore check FCS when txfcs_ins is low"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _txfcs_ins(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "txfcs_ins"
            
            def description(self):
                return "TX inserts 4bytes FCS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _rxfcs_ignore(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "rxfcs_ignore"
            
            def description(self):
                return "RX ignore check FCS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _rxfcs_rmv(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxfcs_rmv"
            
            def description(self):
                return "RX remove 4bytes FCS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txfcs_ignore"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Ctrl_FCS._txfcs_ignore()
            allFields["txfcs_ins"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Ctrl_FCS._txfcs_ins()
            allFields["rxfcs_ignore"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Ctrl_FCS._rxfcs_ignore()
            allFields["rxfcs_rmv"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Ctrl_FCS._rxfcs_rmv()
            return allFields

    class _ETH_10G_AutoNeg(AtRegister.AtRegister):
        def name(self):
            return "ETH 10GAutoNeg"
    
        def description(self):
            return "configure Auto-Neg"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000081
            
        def endAddress(self):
            return 0xffffffff

        class _fec_rx_enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "fec_rx_enb"
            
            def description(self):
                return "FEC RX enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fec_tx_enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "fec_tx_enb"
            
            def description(self):
                return "FEC TX enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _an_pseudo_sel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "an_pseudo_sel"
            
            def description(self):
                return "Selects the polynomial generator for the bit 49 random bit generator"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _an_nonce_seed(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "an_nonce_seed"
            
            def description(self):
                return "8-bit seed to initialize the nonce field polynomial generator. Non-zero."
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _an_lt_sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "an_lt_sta"
            
            def description(self):
                return "Link Control outputs from the auto-negotiationcontroller for the various Ethernet protocols."
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lt_restart(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "lt_restart"
            
            def description(self):
                return "This signal triggers a restart of link training regardless of the current state."
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lt_enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "lt_enb"
            
            def description(self):
                return "Enables link training. When link training is disabled, all PCS lanes function in mission mode."
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _an_restart(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "an_restart"
            
            def description(self):
                return "This input is used to trigger a restart of the auto-negotiation, regardless of what state the circuit is currently in."
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _an_bypass(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "an_bypass"
            
            def description(self):
                return "Input to disable auto-negotiation and bypass the auto-negotiation function. If this input is asserted, auto-negotiation is turned off, but the PCS is connected to the output to allow operation."
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _an_enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "an_enb"
            
            def description(self):
                return "Enable signal for auto-negotiation"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["fec_rx_enb"] = _AF6CNC0011_ETH10G_RD._ETH_10G_AutoNeg._fec_rx_enb()
            allFields["fec_tx_enb"] = _AF6CNC0011_ETH10G_RD._ETH_10G_AutoNeg._fec_tx_enb()
            allFields["an_pseudo_sel"] = _AF6CNC0011_ETH10G_RD._ETH_10G_AutoNeg._an_pseudo_sel()
            allFields["an_nonce_seed"] = _AF6CNC0011_ETH10G_RD._ETH_10G_AutoNeg._an_nonce_seed()
            allFields["an_lt_sta"] = _AF6CNC0011_ETH10G_RD._ETH_10G_AutoNeg._an_lt_sta()
            allFields["lt_restart"] = _AF6CNC0011_ETH10G_RD._ETH_10G_AutoNeg._lt_restart()
            allFields["lt_enb"] = _AF6CNC0011_ETH10G_RD._ETH_10G_AutoNeg._lt_enb()
            allFields["an_restart"] = _AF6CNC0011_ETH10G_RD._ETH_10G_AutoNeg._an_restart()
            allFields["an_bypass"] = _AF6CNC0011_ETH10G_RD._ETH_10G_AutoNeg._an_bypass()
            allFields["an_enb"] = _AF6CNC0011_ETH10G_RD._ETH_10G_AutoNeg._an_enb()
            return allFields

    class _ETH_10G_Diag_ctrl0(AtRegister.AtRegister):
        def name(self):
            return "ETH 10GDiag Ctrl0"
    
        def description(self):
            return "Diagnostic control 0"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000020
            
        def endAddress(self):
            return 0xffffffff

        class _diag_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "diag_err"
            
            def description(self):
                return "Error detection"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _diag_ferr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "diag_ferr"
            
            def description(self):
                return "Enable force error data of diagnostic packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _diag_datmod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "diag_datmod"
            
            def description(self):
                return "payload mod of ethernet frame"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _diag_enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "diag_enb"
            
            def description(self):
                return "enable diagnostic block"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["diag_err"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_ctrl0._diag_err()
            allFields["diag_ferr"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_ctrl0._diag_ferr()
            allFields["diag_datmod"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_ctrl0._diag_datmod()
            allFields["diag_enb"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_ctrl0._diag_enb()
            return allFields

    class _ETH_10G_Diag_ctrl1(AtRegister.AtRegister):
        def name(self):
            return "ETH 10GDiag Ctrl1"
    
        def description(self):
            return "Diagnostic control 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000021
            
        def endAddress(self):
            return 0xffffffff

        class _diag_lenmax(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "diag_lenmax"
            
            def description(self):
                return "Maximum length of diagnostic packet, count from 0, min value is 63"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _diag_lenmin(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "diag_lenmin"
            
            def description(self):
                return "Minimum length of diagnostic packet, count from 0, min value is 63"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["diag_lenmax"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_ctrl1._diag_lenmax()
            allFields["diag_lenmin"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_ctrl1._diag_lenmin()
            return allFields

    class _ETH_10G_Diag_ctrl2(AtRegister.AtRegister):
        def name(self):
            return "ETH 10GDiag Ctrl2"
    
        def description(self):
            return "Diagnostic control 2"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000022
            
        def endAddress(self):
            return 0xffffffff

        class _diag_dalsb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "diag_dalsb"
            
            def description(self):
                return "32bit-LSB DA of diagnostic packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["diag_dalsb"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_ctrl2._diag_dalsb()
            return allFields

    class _ETH_10G_Diag_ctrl3(AtRegister.AtRegister):
        def name(self):
            return "ETH 10GDiag Ctrl3"
    
        def description(self):
            return "Diagnostic control 3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000023
            
        def endAddress(self):
            return 0xffffffff

        class _diag_salsb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "diag_salsb"
            
            def description(self):
                return "32bit-LSB SA of diagnostic packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["diag_salsb"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_ctrl3._diag_salsb()
            return allFields

    class _ETH_10G_Diag_ctrl4(AtRegister.AtRegister):
        def name(self):
            return "ETH 10GDiag Ctrl4"
    
        def description(self):
            return "Diagnostic control 4"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000024
            
        def endAddress(self):
            return 0xffffffff

        class _diag_damsb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "diag_damsb"
            
            def description(self):
                return "16bit-MSB DA of diagnostic packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _diag_samsb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "diag_samsb"
            
            def description(self):
                return "16bit-MSB SA of diagnostic packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["diag_damsb"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_ctrl4._diag_damsb()
            allFields["diag_samsb"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_ctrl4._diag_samsb()
            return allFields

    class _ETH_10G_Diag_ctrl6(AtRegister.AtRegister):
        def name(self):
            return "ETH 10GDiag Ctrl6"
    
        def description(self):
            return "Diagnostic control 7"
            
        def width(self):
            return 17
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000026
            
        def endAddress(self):
            return 0xffffffff

        class _enb_type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "enb_type"
            
            def description(self):
                return "Config enable insert type from CPU, (1) is enable, (0) is disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _type_cfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 0
        
            def name(self):
                return "type_cfg"
            
            def description(self):
                return "value type that is configured"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["enb_type"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_ctrl6._enb_type()
            allFields["type_cfg"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_ctrl6._type_cfg()
            return allFields

    class _ETH_10G_Diag_ctrl7(AtRegister.AtRegister):
        def name(self):
            return "ETH 10GDiag Ctrl7"
    
        def description(self):
            return "Diagnostic control 7"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000027
            
        def endAddress(self):
            return 0xffffffff

        class _Unsed(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 19
        
            def name(self):
                return "Unsed"
            
            def description(self):
                return "Unsed"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _status_gatetime_diag(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "status_gatetime_diag"
            
            def description(self):
                return "Status Gatetime diagnostic 1:Running 0:Done-Ready"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _start_gatetime_diag(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "start_gatetime_diag"
            
            def description(self):
                return "Config start Diagnostic trigger 0 to 1 for Start auto run with Gatetime Configuration"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _time_cfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 0
        
            def name(self):
                return "time_cfg"
            
            def description(self):
                return "Gatetime Configuration 1-86400 second"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Unsed"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_ctrl7._Unsed()
            allFields["status_gatetime_diag"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_ctrl7._status_gatetime_diag()
            allFields["start_gatetime_diag"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_ctrl7._start_gatetime_diag()
            allFields["time_cfg"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_ctrl7._time_cfg()
            return allFields

    class _Gatetime_current(AtRegister.AtRegister):
        def name(self):
            return "Gatetime Current"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000028
            
        def endAddress(self):
            return 0xffffffff

        class _Unsed(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 17
        
            def name(self):
                return "Unsed"
            
            def description(self):
                return "Unsed"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _currert_gatetime_diag(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 0
        
            def name(self):
                return "currert_gatetime_diag"
            
            def description(self):
                return "Current running time of Gatetime diagnostic"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Unsed"] = _AF6CNC0011_ETH10G_RD._Gatetime_current._Unsed()
            allFields["currert_gatetime_diag"] = _AF6CNC0011_ETH10G_RD._Gatetime_current._currert_gatetime_diag()
            return allFields

    class _ETH_10G_Diag_Sta0(AtRegister.AtRegister):
        def name(self):
            return "ETH 10GDiag Sta0"
    
        def description(self):
            return "Diagnostic Sta0"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000040
            
        def endAddress(self):
            return 0xffffffff

        class _diag_txmis_sop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "diag_txmis_sop"
            
            def description(self):
                return "Packet miss SOP"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _diag_txmis_eop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "diag_txmis_eop"
            
            def description(self):
                return "Packet miss EOP"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _diag_txsop_eop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "diag_txsop_eop"
            
            def description(self):
                return "Short packet, length is less than 16bytes"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _diag_txwff_ful(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "diag_txwff_ful"
            
            def description(self):
                return "TX-Fifo is full"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _diag_rxmis_sop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "diag_rxmis_sop"
            
            def description(self):
                return "Packet miss SOP"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _diag_rxmis_eop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "diag_rxmis_eop"
            
            def description(self):
                return "Packet miss EOP"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _diag_rxsop_eop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "diag_rxsop_eop"
            
            def description(self):
                return "Short packet, length is less than 16bytes  t"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _diag_rxwff_ful(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "diag_rxwff_ful"
            
            def description(self):
                return "RX-Fifo is full"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["diag_txmis_sop"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_Sta0._diag_txmis_sop()
            allFields["diag_txmis_eop"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_Sta0._diag_txmis_eop()
            allFields["diag_txsop_eop"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_Sta0._diag_txsop_eop()
            allFields["diag_txwff_ful"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_Sta0._diag_txwff_ful()
            allFields["diag_rxmis_sop"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_Sta0._diag_rxmis_sop()
            allFields["diag_rxmis_eop"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_Sta0._diag_rxmis_eop()
            allFields["diag_rxsop_eop"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_Sta0._diag_rxsop_eop()
            allFields["diag_rxwff_ful"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_Sta0._diag_rxwff_ful()
            return allFields

    class _ETH_10G_Diag_TXPKT(AtRegister.AtRegister):
        def name(self):
            return "ETH 10GDiag TXPKT"
    
        def description(self):
            return "Diagnostic TX packet counter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000042
            
        def endAddress(self):
            return 0xffffffff

        class _diag_txpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "diag_txpkt"
            
            def description(self):
                return "TX packet counter"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["diag_txpkt"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_TXPKT._diag_txpkt()
            return allFields

    class _ETH_10G_Diag_TXNOB(AtRegister.AtRegister):
        def name(self):
            return "ETH 10GDiag TXNOB"
    
        def description(self):
            return "Diagnostic TX number of bytes counter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000043
            
        def endAddress(self):
            return 0xffffffff

        class _diag_txnob(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "diag_txnob"
            
            def description(self):
                return "TX number of byte counter"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["diag_txnob"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_TXNOB._diag_txnob()
            return allFields

    class _ETH_10G_Diag_RXPKT(AtRegister.AtRegister):
        def name(self):
            return "ETH 10GDiag RXPKT"
    
        def description(self):
            return "Diagnostic RX packet counter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000044
            
        def endAddress(self):
            return 0xffffffff

        class _diag_rxpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "diag_rxpkt"
            
            def description(self):
                return "RX packet counter"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["diag_rxpkt"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_RXPKT._diag_rxpkt()
            return allFields

    class _ETH_10G_Diag_RXNOB(AtRegister.AtRegister):
        def name(self):
            return "ETH 10GDiag RXNOB"
    
        def description(self):
            return "Diagnostic RX number of bytes counter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000045
            
        def endAddress(self):
            return 0xffffffff

        class _diag_rxnob(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "diag_rxnob"
            
            def description(self):
                return "RX number of byte counter"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["diag_rxnob"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Diag_RXNOB._diag_rxnob()
            return allFields

    class _ETH_10G_TX_CFG(AtRegister.AtRegister):
        def name(self):
            return "ETH 10GTX CFG"
    
        def description(self):
            return "ETH 10G- pg210-25g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 57-64)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002101
            
        def endAddress(self):
            return 0xffffffff

        class _reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 9
        
            def name(self):
                return "reserve"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_sel_tick(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "cfg_sel_tick"
            
            def description(self):
                return "configure select pm_tick from CPU configure (tick_reg) or from signal pm_tick, (1) from signal pm_tick, (0) from CPU"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "reserve"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _upactive(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "upactive"
            
            def description(self):
                return "enable active,"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_txen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "cfg_txen"
            
            def description(self):
                return "enable transmit side,"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ipg_cfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ipg_cfg"
            
            def description(self):
                return "configure Tx IPG"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["reserve"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TX_CFG._reserve()
            allFields["cfg_sel_tick"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TX_CFG._cfg_sel_tick()
            allFields["reserve"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TX_CFG._reserve()
            allFields["upactive"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TX_CFG._upactive()
            allFields["cfg_txen"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TX_CFG._cfg_txen()
            allFields["ipg_cfg"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TX_CFG._ipg_cfg()
            return allFields

    class _ETH_10G_RX_CFG(AtRegister.AtRegister):
        def name(self):
            return "ETH 10GRX CFG"
    
        def description(self):
            return "ETH 10G- pg210-25g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 57-64)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002102
            
        def endAddress(self):
            return 0xffffffff

        class _reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 25
        
            def name(self):
                return "reserve"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_rxen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "cfg_rxen"
            
            def description(self):
                return "enable receive side,"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_minlen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "cfg_minlen"
            
            def description(self):
                return ""
            
            def type(self):
                return "configure Rx MTU, min len packet receive"
            
            def resetValue(self):
                return 0xffffffff

        class _reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "reserve"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_maxlen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_maxlen"
            
            def description(self):
                return "configure Rx MTU, max len packet receive"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["reserve"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_CFG._reserve()
            allFields["cfg_rxen"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_CFG._cfg_rxen()
            allFields["cfg_minlen"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_CFG._cfg_minlen()
            allFields["reserve"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_CFG._reserve()
            allFields["cfg_maxlen"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_CFG._cfg_maxlen()
            return allFields

    class _ETH_10G_CFG_TICK_REG(AtRegister.AtRegister):
        def name(self):
            return "ETH 10GCFG TICK REG"
    
        def description(self):
            return ""
            
        def width(self):
            return 1
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002109
            
        def endAddress(self):
            return 0xffffffff

        class _tick_reg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tick_reg"
            
            def description(self):
                return "write value \"1\" for tick, auto low (value \"0\")"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tick_reg"] = _AF6CNC0011_ETH10G_RD._ETH_10G_CFG_TICK_REG._tick_reg()
            return allFields

    class _ETH_10G_TX_STICKY(AtRegister.AtRegister):
        def name(self):
            return "ETH 10GTX STICKY"
    
        def description(self):
            return "ETH 10G- pg210-25g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 57-64)"
            
        def width(self):
            return 7
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002110
            
        def endAddress(self):
            return 0xffffffff

        class _Tx_underflow_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Tx_underflow_err"
            
            def description(self):
                return "not support with xilinx IP- reserve"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stktx_frame_error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "stktx_frame_error"
            
            def description(self):
                return "for packets with tx_axis_tuser set to indicate an End of Packet (EOP) abort."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stklt_tx_bad_fcs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "stklt_tx_bad_fcs"
            
            def description(self):
                return "for packets greater than 64 bytes that have FCS errors."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stktx_local_fault(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "stktx_local_fault"
            
            def description(self):
                return "A value of 1 indicates the transmit encoder state machine is in the TX_INIT state"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stklt_signal_detect(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "stklt_signal_detect"
            
            def description(self):
                return "This signal indicates when the respective link training state machine has entered the SEND_DATA state, in which normal PCS operation can resume"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stklt_training(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "stklt_training"
            
            def description(self):
                return "This signal indicates when the respective link training state machine is performing link training, per lane"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stklt_training_fail(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "stklt_training_fail"
            
            def description(self):
                return "This signal is asserted during link training if the corresponding link training state machine detects a time-out during the training period"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Tx_underflow_err"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TX_STICKY._Tx_underflow_err()
            allFields["stktx_frame_error"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TX_STICKY._stktx_frame_error()
            allFields["stklt_tx_bad_fcs"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TX_STICKY._stklt_tx_bad_fcs()
            allFields["stktx_local_fault"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TX_STICKY._stktx_local_fault()
            allFields["stklt_signal_detect"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TX_STICKY._stklt_signal_detect()
            allFields["stklt_training"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TX_STICKY._stklt_training()
            allFields["stklt_training_fail"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TX_STICKY._stklt_training_fail()
            return allFields

    class _ETH_10G_TX_ALARM(AtRegister.AtRegister):
        def name(self):
            return "ETH 10GTX ALARM"
    
        def description(self):
            return "ETH 10G- pg210-25g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 57-64)"
            
        def width(self):
            return 7
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002111
            
        def endAddress(self):
            return 0xffffffff

        class _Tx_underflow_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Tx_underflow_err"
            
            def description(self):
                return "not support with xilinx IP- reserve"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stat_tx_frame_error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "stat_tx_frame_error"
            
            def description(self):
                return "for packets with tx_axis_tuser set to indicate an End of Packet (EOP) abort."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stat_tx_bad_fcs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "stat_tx_bad_fcs"
            
            def description(self):
                return "for packets greater than 64 bytes that have FCS errors."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stat_tx_local_fault(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "stat_tx_local_fault"
            
            def description(self):
                return "A value of 1 indicates the transmit encoder state machine is in the TX_INIT state"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stat_lt_signal_detect(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "stat_lt_signal_detect"
            
            def description(self):
                return "This signal indicates when the respective link training state machine has entered the SEND_DATA state, in which normal PCS operation can resume"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stat_lt_training(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "stat_lt_training"
            
            def description(self):
                return "This signal indicates when the respective link training state machine is performing link training, per lane"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stat_lt_training_fail(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "stat_lt_training_fail"
            
            def description(self):
                return "This signal is asserted during link training if the corresponding link training state machine detects a time-out during the training period"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Tx_underflow_err"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TX_ALARM._Tx_underflow_err()
            allFields["stat_tx_frame_error"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TX_ALARM._stat_tx_frame_error()
            allFields["stat_tx_bad_fcs"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TX_ALARM._stat_tx_bad_fcs()
            allFields["stat_tx_local_fault"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TX_ALARM._stat_tx_local_fault()
            allFields["stat_lt_signal_detect"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TX_ALARM._stat_lt_signal_detect()
            allFields["stat_lt_training"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TX_ALARM._stat_lt_training()
            allFields["stat_lt_training_fail"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TX_ALARM._stat_lt_training_fail()
            return allFields

    class _ETH_10G_RX_STICKY(AtRegister.AtRegister):
        def name(self):
            return "ETH 10GRX STICKY"
    
        def description(self):
            return "ETH 10G- pg210-25g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 57-64)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002112
            
        def endAddress(self):
            return 0xffffffff

        class _reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 22
        
            def name(self):
                return "reserve"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _stkrx_got_signal_os_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "stkrx_got_signal_os_0"
            
            def description(self):
                return "stat_rx_got_signal_os_0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stkrx_inrangeerr_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "stkrx_inrangeerr_0"
            
            def description(self):
                return "stat_rx_inrangeerr_0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stklt_frame_lock(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "stklt_frame_lock"
            
            def description(self):
                return "When link training has begun, these signals are asserted"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stklt_rx_sof_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "stklt_rx_sof_0"
            
            def description(self):
                return "stklt_rx_sof_0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stklt_preset_from_rx_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "stklt_preset_from_rx_0"
            
            def description(self):
                return "stklt_preset_from_rx_0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stklt_initialize_from_rx_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "stklt_initialize_from_rx_0"
            
            def description(self):
                return "stklt_initialize_from_rx_0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stkfec_inc_cant_correct_count(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "stkfec_inc_cant_correct_count"
            
            def description(self):
                return "stkfec_inc_cant_correct_count, per lane"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stkfec_inc_correct_count(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "stkfec_inc_correct_count"
            
            def description(self):
                return "stkfec_inc_correct_count , per lane"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stkfec_lock_error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "stkfec_lock_error"
            
            def description(self):
                return "stkfec_lock_error, per lane"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stklt_fec_rx_lock_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "stklt_fec_rx_lock_0"
            
            def description(self):
                return "This signal is asserted while the ctl_fec_rx_enable is asserted when the FEC decoder detects the frame boundary"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stkrx_local_fault(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "stkrx_local_fault"
            
            def description(self):
                return "stkrx_local_fault"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stkrx_remote_fault(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "stkrx_remote_fault"
            
            def description(self):
                return "stkrx_remote_fault"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stkrx_internal_local_fault(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "stkrx_internal_local_fault"
            
            def description(self):
                return "stkrx_internal_local_fault"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stkrx_received_local_fault(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "stkrx_received_local_fault"
            
            def description(self):
                return "stkrx_received_local_fault"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stkrx_framing_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "stkrx_framing_err"
            
            def description(self):
                return "stk_framing_err, per lane"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stkrx_truncated(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "stkrx_truncated"
            
            def description(self):
                return "stk truncated"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stkrx_hi_ber(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "stkrx_hi_ber"
            
            def description(self):
                return "stk stkrx_hi_ber"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stkrx_rx_valid_ctrl_code_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "stkrx_rx_valid_ctrl_code_0"
            
            def description(self):
                return "Indicates that a PCS block with a valid control code was received."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stkrx_bad_sfd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "stkrx_bad_sfd"
            
            def description(self):
                return "This signal indicates if the Ethernet packet received was preceded by a valid SFD. A value of 1 indicates that an invalid SFD was received."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stkrx_bad_preamble(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "stkrx_bad_preamble"
            
            def description(self):
                return "This signal indicates if the Ethernet packet received was preceded by a valid preamble. A value of 1 indicates that an invalid preamble was received."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stkrx_block_lock(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "stkrx_block_lock"
            
            def description(self):
                return "Block lock status. A value of 1 indicates that block lock is achieved"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stkrx_status(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "stkrx_status"
            
            def description(self):
                return "Indicates current status of the link."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["reserve"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_STICKY._reserve()
            allFields["stkrx_got_signal_os_0"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_STICKY._stkrx_got_signal_os_0()
            allFields["stkrx_inrangeerr_0"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_STICKY._stkrx_inrangeerr_0()
            allFields["stklt_frame_lock"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_STICKY._stklt_frame_lock()
            allFields["stklt_rx_sof_0"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_STICKY._stklt_rx_sof_0()
            allFields["stklt_preset_from_rx_0"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_STICKY._stklt_preset_from_rx_0()
            allFields["stklt_initialize_from_rx_0"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_STICKY._stklt_initialize_from_rx_0()
            allFields["stkfec_inc_cant_correct_count"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_STICKY._stkfec_inc_cant_correct_count()
            allFields["stkfec_inc_correct_count"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_STICKY._stkfec_inc_correct_count()
            allFields["stkfec_lock_error"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_STICKY._stkfec_lock_error()
            allFields["stklt_fec_rx_lock_0"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_STICKY._stklt_fec_rx_lock_0()
            allFields["stkrx_local_fault"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_STICKY._stkrx_local_fault()
            allFields["stkrx_remote_fault"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_STICKY._stkrx_remote_fault()
            allFields["stkrx_internal_local_fault"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_STICKY._stkrx_internal_local_fault()
            allFields["stkrx_received_local_fault"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_STICKY._stkrx_received_local_fault()
            allFields["stkrx_framing_err"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_STICKY._stkrx_framing_err()
            allFields["stkrx_truncated"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_STICKY._stkrx_truncated()
            allFields["stkrx_hi_ber"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_STICKY._stkrx_hi_ber()
            allFields["stkrx_rx_valid_ctrl_code_0"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_STICKY._stkrx_rx_valid_ctrl_code_0()
            allFields["stkrx_bad_sfd"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_STICKY._stkrx_bad_sfd()
            allFields["stkrx_bad_preamble"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_STICKY._stkrx_bad_preamble()
            allFields["stkrx_block_lock"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_STICKY._stkrx_block_lock()
            allFields["stkrx_status"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_STICKY._stkrx_status()
            return allFields

    class _ETH_10G_RX_ALARM(AtRegister.AtRegister):
        def name(self):
            return "ETH 10GRX ALARM"
    
        def description(self):
            return "ETH 10G- pg210-25g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 57-64)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002113
            
        def endAddress(self):
            return 0xffffffff

        class _reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 22
        
            def name(self):
                return "reserve"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _stat_rx_got_signal_os_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "stat_rx_got_signal_os_0"
            
            def description(self):
                return "stat_rx_got_signal_os_0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stat_rx_inrangeerr_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "stat_rx_inrangeerr_0"
            
            def description(self):
                return "stat_rx_inrangeerr_0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stat_lt_frame_lock(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "stat_lt_frame_lock"
            
            def description(self):
                return "When link training has begun, these signals are asserted"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stat_lt_rx_sof_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "stat_lt_rx_sof_0"
            
            def description(self):
                return "stat_lt_rx_sof_0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stat_lt_preset_from_rx_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "stat_lt_preset_from_rx_0"
            
            def description(self):
                return "stat_lt_preset_from_rx_0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stat_lt_initialize_from_rx_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "stat_lt_initialize_from_rx_0"
            
            def description(self):
                return "stat_lt_initialize_from_rx_0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stat_fec_inc_cant_correct_count(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "stat_fec_inc_cant_correct_count"
            
            def description(self):
                return "stat_fec_inc_cant_correct_count, per lane"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stat_fec_inc_correct_count(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "stat_fec_inc_correct_count"
            
            def description(self):
                return "stat_fec_inc_correct_count , per lane"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stat_fec_lock_error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "stat_fec_lock_error"
            
            def description(self):
                return "stat_fec_lock_error, per lane"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stat_lt_fec_rx_lock_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "stat_lt_fec_rx_lock_0"
            
            def description(self):
                return "This signal is asserted while the ctl_fec_rx_enable is asserted when the FEC decoder detects the frame boundary"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stat_rx_local_fault(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "stat_rx_local_fault"
            
            def description(self):
                return "stat_rx_local_fault"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stat_rx_remote_fault(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "stat_rx_remote_fault"
            
            def description(self):
                return "stat_rx_remote_fault"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stat_rx_internal_local_fault(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "stat_rx_internal_local_fault"
            
            def description(self):
                return "stat_rx_internal_local_fault"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stat_rx_received_local_fault(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "stat_rx_received_local_fault"
            
            def description(self):
                return "stat_rx_received_local_fault"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stat_rx_framing_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "stat_rx_framing_err"
            
            def description(self):
                return "stat_framing_err, per lane"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stat_rx_truncated(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "stat_rx_truncated"
            
            def description(self):
                return "stat_truncated"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stat_rx_hi_ber(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "stat_rx_hi_ber"
            
            def description(self):
                return "stat_rx_hi_ber"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stat_rx_rx_valid_ctrl_code_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "stat_rx_rx_valid_ctrl_code_0"
            
            def description(self):
                return "Indicates that a PCS block with a valid control code was received."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stat_rx_bad_sfd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "stat_rx_bad_sfd"
            
            def description(self):
                return "This signal indicates if the Ethernet packet received was preceded by a valid SFD. A value of 1 indicates that an invalid SFD was received."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stat_rx_bad_preamble(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "stat_rx_bad_preamble"
            
            def description(self):
                return "This signal indicates if the Ethernet packet received was preceded by a valid preamble. A value of 1 indicates that an invalid preamble was received."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stat_rx_block_lock(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "stat_rx_block_lock"
            
            def description(self):
                return "Block lock status. A value of 1 indicates that block lock is achieved"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stat_rx_status(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "stat_rx_status"
            
            def description(self):
                return "Indicates current status of the link."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["reserve"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_ALARM._reserve()
            allFields["stat_rx_got_signal_os_0"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_ALARM._stat_rx_got_signal_os_0()
            allFields["stat_rx_inrangeerr_0"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_ALARM._stat_rx_inrangeerr_0()
            allFields["stat_lt_frame_lock"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_ALARM._stat_lt_frame_lock()
            allFields["stat_lt_rx_sof_0"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_ALARM._stat_lt_rx_sof_0()
            allFields["stat_lt_preset_from_rx_0"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_ALARM._stat_lt_preset_from_rx_0()
            allFields["stat_lt_initialize_from_rx_0"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_ALARM._stat_lt_initialize_from_rx_0()
            allFields["stat_fec_inc_cant_correct_count"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_ALARM._stat_fec_inc_cant_correct_count()
            allFields["stat_fec_inc_correct_count"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_ALARM._stat_fec_inc_correct_count()
            allFields["stat_fec_lock_error"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_ALARM._stat_fec_lock_error()
            allFields["stat_lt_fec_rx_lock_0"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_ALARM._stat_lt_fec_rx_lock_0()
            allFields["stat_rx_local_fault"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_ALARM._stat_rx_local_fault()
            allFields["stat_rx_remote_fault"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_ALARM._stat_rx_remote_fault()
            allFields["stat_rx_internal_local_fault"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_ALARM._stat_rx_internal_local_fault()
            allFields["stat_rx_received_local_fault"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_ALARM._stat_rx_received_local_fault()
            allFields["stat_rx_framing_err"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_ALARM._stat_rx_framing_err()
            allFields["stat_rx_truncated"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_ALARM._stat_rx_truncated()
            allFields["stat_rx_hi_ber"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_ALARM._stat_rx_hi_ber()
            allFields["stat_rx_rx_valid_ctrl_code_0"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_ALARM._stat_rx_rx_valid_ctrl_code_0()
            allFields["stat_rx_bad_sfd"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_ALARM._stat_rx_bad_sfd()
            allFields["stat_rx_bad_preamble"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_ALARM._stat_rx_bad_preamble()
            allFields["stat_rx_block_lock"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_ALARM._stat_rx_block_lock()
            allFields["stat_rx_status"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_ALARM._stat_rx_status()
            return allFields

    class _ETH_10G_AN_STICKY(AtRegister.AtRegister):
        def name(self):
            return "ETH 10G_AN STICKY"
    
        def description(self):
            return "ETH 10G-pg211-50g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 44-45)"
            
        def width(self):
            return 4
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002114
            
        def endAddress(self):
            return 0xffffffff

        class _stkan_start_tx_disable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "stkan_start_tx_disable"
            
            def description(self):
                return "stkan_start_tx_disable"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stkan_start_an_good_check(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "stkan_start_an_good_check"
            
            def description(self):
                return "stkan_start_an_good_check"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stkan_autoneg_complete(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "stkan_autoneg_complete"
            
            def description(self):
                return "stkan_autoneg_complete"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stkan_parallel_detection_fault(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "stkan_parallel_detection_fault"
            
            def description(self):
                return "stkan_parallel_detection_fault"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["stkan_start_tx_disable"] = _AF6CNC0011_ETH10G_RD._ETH_10G_AN_STICKY._stkan_start_tx_disable()
            allFields["stkan_start_an_good_check"] = _AF6CNC0011_ETH10G_RD._ETH_10G_AN_STICKY._stkan_start_an_good_check()
            allFields["stkan_autoneg_complete"] = _AF6CNC0011_ETH10G_RD._ETH_10G_AN_STICKY._stkan_autoneg_complete()
            allFields["stkan_parallel_detection_fault"] = _AF6CNC0011_ETH10G_RD._ETH_10G_AN_STICKY._stkan_parallel_detection_fault()
            return allFields

    class _ETH_10G_AN_ALARM(AtRegister.AtRegister):
        def name(self):
            return "ETH 10G_AN ALARM"
    
        def description(self):
            return "ETH 10G-pg211-50g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 44-45)"
            
        def width(self):
            return 4
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002115
            
        def endAddress(self):
            return 0xffffffff

        class _stat_an_start_tx_disable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "stat_an_start_tx_disable"
            
            def description(self):
                return "stat_an_start_tx_disable"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stat_an_start_an_good_check(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "stat_an_start_an_good_check"
            
            def description(self):
                return "stat_an_start_an_good_check"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stat_an_autoneg_complete(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "stat_an_autoneg_complete"
            
            def description(self):
                return "stat_an_autoneg_complete"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _stat_an_parallel_detection_fault(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "stat_an_parallel_detection_fault"
            
            def description(self):
                return "stat_an_parallel_detection_fault"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["stat_an_start_tx_disable"] = _AF6CNC0011_ETH10G_RD._ETH_10G_AN_ALARM._stat_an_start_tx_disable()
            allFields["stat_an_start_an_good_check"] = _AF6CNC0011_ETH10G_RD._ETH_10G_AN_ALARM._stat_an_start_an_good_check()
            allFields["stat_an_autoneg_complete"] = _AF6CNC0011_ETH10G_RD._ETH_10G_AN_ALARM._stat_an_autoneg_complete()
            allFields["stat_an_parallel_detection_fault"] = _AF6CNC0011_ETH10G_RD._ETH_10G_AN_ALARM._stat_an_parallel_detection_fault()
            return allFields

    class _ETH_10G_TX_INTEN(AtRegister.AtRegister):
        def name(self):
            return "ETH 10GTX INTTERUPT ENABLE"
    
        def description(self):
            return "ETH 10G- pg210-25g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 57-64)"
            
        def width(self):
            return 6
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002116
            
        def endAddress(self):
            return 0xffffffff

        class _int_en_tx_frame_error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "int_en_tx_frame_error"
            
            def description(self):
                return "for packets with tx_axis_tuser set to indicate an End of Packet (EOP) abort."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _int_en_tx_bad_fcs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "int_en_tx_bad_fcs"
            
            def description(self):
                return "for packets greater than 64 bytes that have FCS errors."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _int_en_tx_local_fault(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "int_en_tx_local_fault"
            
            def description(self):
                return "A value of 1 indicates the transmit encoder state machine is in the TX_INIT state"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _int_en_lt_signal_detect(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "int_en_lt_signal_detect"
            
            def description(self):
                return "This signal indicates when the respective link training state machine has entered the SEND_DATA state, in which normal PCS operation can resume"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _int_en_lt_training(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "int_en_lt_training"
            
            def description(self):
                return "This signal indicates when the respective link training state machine is performing link training, per lane"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _int_en_lt_training_fail(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "int_en_lt_training_fail"
            
            def description(self):
                return "This signal is asserted during link training if the corresponding link training state machine detects a time-out during the training period"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["int_en_tx_frame_error"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TX_INTEN._int_en_tx_frame_error()
            allFields["int_en_tx_bad_fcs"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TX_INTEN._int_en_tx_bad_fcs()
            allFields["int_en_tx_local_fault"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TX_INTEN._int_en_tx_local_fault()
            allFields["int_en_lt_signal_detect"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TX_INTEN._int_en_lt_signal_detect()
            allFields["int_en_lt_training"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TX_INTEN._int_en_lt_training()
            allFields["int_en_lt_training_fail"] = _AF6CNC0011_ETH10G_RD._ETH_10G_TX_INTEN._int_en_lt_training_fail()
            return allFields

    class _ETH_10G_RX_INTEN(AtRegister.AtRegister):
        def name(self):
            return "ETH 10GRX INTTERUPT ENABLE"
    
        def description(self):
            return "ETH 10G- pg210-25g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 57-64)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002117
            
        def endAddress(self):
            return 0xffffffff

        class _int_en_rx_got_signal_os_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "int_en_rx_got_signal_os_0"
            
            def description(self):
                return "int_en_rx_got_signal_os_0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _int_en_rx_inrangeerr_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "int_en_rx_inrangeerr_0"
            
            def description(self):
                return "int_en_rx_inrangeerr_0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _int_en_lt_frame_lock(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "int_en_lt_frame_lock"
            
            def description(self):
                return "When link training has begun, these signals are asserted"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _int_en_lt_rx_sof_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "int_en_lt_rx_sof_0"
            
            def description(self):
                return "int_en_lt_rx_sof_0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _int_en_lt_preset_from_rx_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "int_en_lt_preset_from_rx_0"
            
            def description(self):
                return "int_en_lt_preset_from_rx_0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _int_en_lt_initialize_from_rx_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "int_en_lt_initialize_from_rx_0"
            
            def description(self):
                return "int_en_lt_initialize_from_rx_0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _int_en_fec_inc_cant_correct_count(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "int_en_fec_inc_cant_correct_count"
            
            def description(self):
                return "int_en_fec_inc_cant_correct_count, per lane"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _int_en_fec_inc_correct_count(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "int_en_fec_inc_correct_count"
            
            def description(self):
                return "int_en_fec_inc_correct_count , per lane"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _int_en_fec_lock_error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "int_en_fec_lock_error"
            
            def description(self):
                return "int_en_fec_lock_error, per lane"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _int_en_lt_fec_rx_lock_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "int_en_lt_fec_rx_lock_0"
            
            def description(self):
                return "This signal is asserted while the ctl_fec_rx_enable is asserted when the FEC decoder detects the frame boundary"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _int_en_rx_local_fault(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "int_en_rx_local_fault"
            
            def description(self):
                return "int_en_rx_local_fault"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _int_en_rx_remote_fault(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "int_en_rx_remote_fault"
            
            def description(self):
                return "int_en_rx_remote_fault"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _int_en_rx_internal_local_fault(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "int_en_rx_internal_local_fault"
            
            def description(self):
                return "int_en_rx_internal_local_fault"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _int_en_rx_received_local_fault(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "int_en_rx_received_local_fault"
            
            def description(self):
                return "int_en_rx_received_local_fault"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _int_en_rx_framing_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "int_en_rx_framing_err"
            
            def description(self):
                return "int_en_framing_err, per lane"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _int_en_rx_truncated(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "int_en_rx_truncated"
            
            def description(self):
                return "int_en_truncated"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _int_en_rx_hi_ber(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "int_en_rx_hi_ber"
            
            def description(self):
                return "int_en_rx_hi_ber"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _int_en_rx_rx_valid_ctrl_code_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "int_en_rx_rx_valid_ctrl_code_0"
            
            def description(self):
                return "Indicates that a PCS block with a valid control code was received."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _int_en_rx_bad_sfd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "int_en_rx_bad_sfd"
            
            def description(self):
                return "This signal indicates if the Ethernet packet received was preceded by a valid SFD. A value of 1 indicates that an invalid SFD was received."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _int_en_rx_bad_preamble(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "int_en_rx_bad_preamble"
            
            def description(self):
                return "This signal indicates if the Ethernet packet received was preceded by a valid preamble. A value of 1 indicates that an invalid preamble was received."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _int_en_rx_block_lock(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "int_en_rx_block_lock"
            
            def description(self):
                return "Block lock status. A value of 1 indicates that block lock is achieved"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _int_en_rx_status(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "int_en_rx_status"
            
            def description(self):
                return "Indicates current status of the link."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["int_en_rx_got_signal_os_0"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_INTEN._int_en_rx_got_signal_os_0()
            allFields["int_en_rx_inrangeerr_0"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_INTEN._int_en_rx_inrangeerr_0()
            allFields["int_en_lt_frame_lock"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_INTEN._int_en_lt_frame_lock()
            allFields["int_en_lt_rx_sof_0"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_INTEN._int_en_lt_rx_sof_0()
            allFields["int_en_lt_preset_from_rx_0"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_INTEN._int_en_lt_preset_from_rx_0()
            allFields["int_en_lt_initialize_from_rx_0"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_INTEN._int_en_lt_initialize_from_rx_0()
            allFields["int_en_fec_inc_cant_correct_count"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_INTEN._int_en_fec_inc_cant_correct_count()
            allFields["int_en_fec_inc_correct_count"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_INTEN._int_en_fec_inc_correct_count()
            allFields["int_en_fec_lock_error"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_INTEN._int_en_fec_lock_error()
            allFields["int_en_lt_fec_rx_lock_0"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_INTEN._int_en_lt_fec_rx_lock_0()
            allFields["int_en_rx_local_fault"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_INTEN._int_en_rx_local_fault()
            allFields["int_en_rx_remote_fault"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_INTEN._int_en_rx_remote_fault()
            allFields["int_en_rx_internal_local_fault"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_INTEN._int_en_rx_internal_local_fault()
            allFields["int_en_rx_received_local_fault"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_INTEN._int_en_rx_received_local_fault()
            allFields["int_en_rx_framing_err"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_INTEN._int_en_rx_framing_err()
            allFields["int_en_rx_truncated"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_INTEN._int_en_rx_truncated()
            allFields["int_en_rx_hi_ber"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_INTEN._int_en_rx_hi_ber()
            allFields["int_en_rx_rx_valid_ctrl_code_0"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_INTEN._int_en_rx_rx_valid_ctrl_code_0()
            allFields["int_en_rx_bad_sfd"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_INTEN._int_en_rx_bad_sfd()
            allFields["int_en_rx_bad_preamble"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_INTEN._int_en_rx_bad_preamble()
            allFields["int_en_rx_block_lock"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_INTEN._int_en_rx_block_lock()
            allFields["int_en_rx_status"] = _AF6CNC0011_ETH10G_RD._ETH_10G_RX_INTEN._int_en_rx_status()
            return allFields

    class _ETH_10G_AN_INTEN(AtRegister.AtRegister):
        def name(self):
            return "ETH 10G_AN INTTERUPT ENABLE"
    
        def description(self):
            return "ETH 10G-pg211-50g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 44-45)"
            
        def width(self):
            return 4
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002118
            
        def endAddress(self):
            return 0xffffffff

        class _int_en_an_start_tx_disable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "int_en_an_start_tx_disable"
            
            def description(self):
                return "int_en_an_start_tx_disable"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _int_en_an_start_an_good_check(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "int_en_an_start_an_good_check"
            
            def description(self):
                return "int_en_an_start_an_good_check"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _int_en_an_autoneg_complete(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "int_en_an_autoneg_complete"
            
            def description(self):
                return "int_en_an_autoneg_complete"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _int_en_an_parallel_detection_fault(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "int_en_an_parallel_detection_fault"
            
            def description(self):
                return "int_en_an_parallel_detection_fault"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["int_en_an_start_tx_disable"] = _AF6CNC0011_ETH10G_RD._ETH_10G_AN_INTEN._int_en_an_start_tx_disable()
            allFields["int_en_an_start_an_good_check"] = _AF6CNC0011_ETH10G_RD._ETH_10G_AN_INTEN._int_en_an_start_an_good_check()
            allFields["int_en_an_autoneg_complete"] = _AF6CNC0011_ETH10G_RD._ETH_10G_AN_INTEN._int_en_an_autoneg_complete()
            allFields["int_en_an_parallel_detection_fault"] = _AF6CNC0011_ETH10G_RD._ETH_10G_AN_INTEN._int_en_an_parallel_detection_fault()
            return allFields

    class _ETH_10G_Statistics_TX_COUNTER(AtRegister.AtRegister):
        def name(self):
            return "ETH 10GStatistics TX COUNTER"
    
        def description(self):
            return "ETH 10G- pg210-25g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 69-70/table 2-22)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002000
            
        def endAddress(self):
            return 0x0000201b

        class _cnt_tx_val(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_tx_val"
            
            def description(self):
                return "value of resgister Statistics Tx Counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_tx_val"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Statistics_TX_COUNTER._cnt_tx_val()
            return allFields

    class _ETH_10G_Statistics_RX_Counters(AtRegister.AtRegister):
        def name(self):
            return "ETH 10GStatistics RX Counters"
    
        def description(self):
            return "AXI4 Statistics Counters ETH 10G- pg210-25g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 71-75/table 2-22)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002080
            
        def endAddress(self):
            return 0x000020a6

        class _cnt_rx_val(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_rx_val"
            
            def description(self):
                return "value of resgister Statistics Rx Counters"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_rx_val"] = _AF6CNC0011_ETH10G_RD._ETH_10G_Statistics_RX_Counters._cnt_rx_val()
            return allFields

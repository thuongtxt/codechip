import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0011_RD_GLB(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["VersionID"] = _AF6CNC0011_RD_GLB._VersionID()
        allRegisters["Active"] = _AF6CNC0011_RD_GLB._Active()
        allRegisters["inten_status"] = _AF6CNC0011_RD_GLB._inten_status()
        allRegisters["inten_ctrl"] = _AF6CNC0011_RD_GLB._inten_ctrl()
        allRegisters["inten_restore"] = _AF6CNC0011_RD_GLB._inten_restore()
        allRegisters["inten_Eport"] = _AF6CNC0011_RD_GLB._inten_Eport()
        allRegisters["linkalm_Eport"] = _AF6CNC0011_RD_GLB._linkalm_Eport()
        allRegisters["linksta_Eport"] = _AF6CNC0011_RD_GLB._linksta_Eport()
        allRegisters["linkfrc_Eport"] = _AF6CNC0011_RD_GLB._linkfrc_Eport()
        allRegisters["wr_hold_63_32"] = _AF6CNC0011_RD_GLB._wr_hold_63_32()
        allRegisters["wr_hold_95_64"] = _AF6CNC0011_RD_GLB._wr_hold_95_64()
        allRegisters["wr_hold_127_96"] = _AF6CNC0011_RD_GLB._wr_hold_127_96()
        allRegisters["rd_hold_63_32"] = _AF6CNC0011_RD_GLB._rd_hold_63_32()
        allRegisters["rd_hold_95_64"] = _AF6CNC0011_RD_GLB._rd_hold_95_64()
        allRegisters["rd_hold_127_96"] = _AF6CNC0011_RD_GLB._rd_hold_127_96()
        allRegisters["mac_add_31_0_ctrl"] = _AF6CNC0011_RD_GLB._mac_add_31_0_ctrl()
        allRegisters["mac_add_47_32_ctrl"] = _AF6CNC0011_RD_GLB._mac_add_47_32_ctrl()
        allRegisters["ip_add_31_0_ctrl"] = _AF6CNC0011_RD_GLB._ip_add_31_0_ctrl()
        allRegisters["ip_add_63_32_ctrl"] = _AF6CNC0011_RD_GLB._ip_add_63_32_ctrl()
        allRegisters["ip_add_95_64_ctrl"] = _AF6CNC0011_RD_GLB._ip_add_95_64_ctrl()
        allRegisters["ip_add_127_96_ctrl"] = _AF6CNC0011_RD_GLB._ip_add_127_96_ctrl()
        allRegisters["cdr_refout_8khz_ctrl"] = _AF6CNC0011_RD_GLB._cdr_refout_8khz_ctrl()
        allRegisters["RAM_Parity_Force_Control"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control()
        allRegisters["RAM_Parity_Disable_Control"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control()
        allRegisters["RAM_Parity_Error_Sticky"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky()
        allRegisters["RAM_Parity_Error_Intr_Group"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Intr_Group()
        allRegisters["rdha3_0_control"] = _AF6CNC0011_RD_GLB._rdha3_0_control()
        allRegisters["rdha7_4_control"] = _AF6CNC0011_RD_GLB._rdha7_4_control()
        allRegisters["rdha11_8_control"] = _AF6CNC0011_RD_GLB._rdha11_8_control()
        allRegisters["rdha15_12_control"] = _AF6CNC0011_RD_GLB._rdha15_12_control()
        allRegisters["rdha19_16_control"] = _AF6CNC0011_RD_GLB._rdha19_16_control()
        allRegisters["rdha23_20_control"] = _AF6CNC0011_RD_GLB._rdha23_20_control()
        allRegisters["rdha24data_control"] = _AF6CNC0011_RD_GLB._rdha24data_control()
        allRegisters["rdha_hold63_32"] = _AF6CNC0011_RD_GLB._rdha_hold63_32()
        allRegisters["rdindr_hold95_64"] = _AF6CNC0011_RD_GLB._rdindr_hold95_64()
        allRegisters["rdindr_hold127_96"] = _AF6CNC0011_RD_GLB._rdindr_hold127_96()
        allRegisters["alm_temp"] = _AF6CNC0011_RD_GLB._alm_temp()
        allRegisters["PDA_DDR_CRC_Error_Counter_ro"] = _AF6CNC0011_RD_GLB._PDA_DDR_CRC_Error_Counter_ro()
        allRegisters["PDA_DDR_CRC_Error_Counter_r2c"] = _AF6CNC0011_RD_GLB._PDA_DDR_CRC_Error_Counter_r2c()
        allRegisters["PDA_DDR_errins_en_cfg"] = _AF6CNC0011_RD_GLB._PDA_DDR_errins_en_cfg()
        allRegisters["Rx_DDR_ECC_Cor_Error_Counter_ro"] = _AF6CNC0011_RD_GLB._Rx_DDR_ECC_Cor_Error_Counter_ro()
        allRegisters["Rx_DDR_ECC_Cor_Error_Counter_r2c"] = _AF6CNC0011_RD_GLB._Rx_DDR_ECC_Cor_Error_Counter_r2c()
        allRegisters["Rx_DDR_ECC_UnCor_Error_Counter_ro"] = _AF6CNC0011_RD_GLB._Rx_DDR_ECC_UnCor_Error_Counter_ro()
        allRegisters["Rx_DDR_ECC_UnCor_Error_Counter_r2c"] = _AF6CNC0011_RD_GLB._Rx_DDR_ECC_UnCor_Error_Counter_r2c()
        allRegisters["Rx_DDR_errins_en_cfg"] = _AF6CNC0011_RD_GLB._Rx_DDR_errins_en_cfg()
        allRegisters["Tx_DDR_ECC_Cor_Error_Counter_ro"] = _AF6CNC0011_RD_GLB._Tx_DDR_ECC_Cor_Error_Counter_ro()
        allRegisters["Tx_DDR_ECC_Cor_Error_Counter_r2c"] = _AF6CNC0011_RD_GLB._Tx_DDR_ECC_Cor_Error_Counter_r2c()
        allRegisters["Tx_DDR_ECC_UnCor_Error_Counter_ro"] = _AF6CNC0011_RD_GLB._Tx_DDR_ECC_UnCor_Error_Counter_ro()
        allRegisters["Tx_DDR_ECC_UnCor_Error_Counter_r2c"] = _AF6CNC0011_RD_GLB._Tx_DDR_ECC_UnCor_Error_Counter_r2c()
        allRegisters["Tx_DDR_errins_en_cfg"] = _AF6CNC0011_RD_GLB._Tx_DDR_errins_en_cfg()
        return allRegisters

    class _VersionID(AtRegister.AtRegister):
        def name(self):
            return "Device Version ID"
    
        def description(self):
            return "This register indicates the module' name and version."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _Year(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "Year"
            
            def description(self):
                return "2013"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Week(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Week"
            
            def description(self):
                return "Week Synthesized FPGA"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Day(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Day"
            
            def description(self):
                return "Day Synthesized FPGA"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _VerID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "VerID"
            
            def description(self):
                return "FPGA Version"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _NumID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "NumID"
            
            def description(self):
                return "Number of FPGA Version"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Year"] = _AF6CNC0011_RD_GLB._VersionID._Year()
            allFields["Week"] = _AF6CNC0011_RD_GLB._VersionID._Week()
            allFields["Day"] = _AF6CNC0011_RD_GLB._VersionID._Day()
            allFields["VerID"] = _AF6CNC0011_RD_GLB._VersionID._VerID()
            allFields["NumID"] = _AF6CNC0011_RD_GLB._VersionID._NumID()
            return allFields

    class _Active(AtRegister.AtRegister):
        def name(self):
            return "GLB Sub-Core Active"
    
        def description(self):
            return "This register indicates the active ports."
            
        def width(self):
            return 10
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000001
            
        def endAddress(self):
            return 0xffffffff

        class _PmcPact(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "PmcPact"
            
            def description(self):
                return "PMC Active 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _EthPact(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "EthPact"
            
            def description(self):
                return "Ethernet Interface Active 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _cdrPact(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "cdrPact"
            
            def description(self):
                return "CDR Active 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _claPact(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "claPact"
            
            def description(self):
                return "CLA Active 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwePact(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PwePact"
            
            def description(self):
                return "PWE Active 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PdaPact(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PdaPact"
            
            def description(self):
                return "PDA Active 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPact(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "PlaPact"
            
            def description(self):
                return "PLA Active 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MapPact(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "MapPact"
            
            def description(self):
                return "MAP Active 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PdhPact(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PdhPact"
            
            def description(self):
                return "PDH Active 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OcnPact(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "OcnPact"
            
            def description(self):
                return "OCN Active 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PmcPact"] = _AF6CNC0011_RD_GLB._Active._PmcPact()
            allFields["EthPact"] = _AF6CNC0011_RD_GLB._Active._EthPact()
            allFields["cdrPact"] = _AF6CNC0011_RD_GLB._Active._cdrPact()
            allFields["claPact"] = _AF6CNC0011_RD_GLB._Active._claPact()
            allFields["PwePact"] = _AF6CNC0011_RD_GLB._Active._PwePact()
            allFields["PdaPact"] = _AF6CNC0011_RD_GLB._Active._PdaPact()
            allFields["PlaPact"] = _AF6CNC0011_RD_GLB._Active._PlaPact()
            allFields["MapPact"] = _AF6CNC0011_RD_GLB._Active._MapPact()
            allFields["PdhPact"] = _AF6CNC0011_RD_GLB._Active._PdhPact()
            allFields["OcnPact"] = _AF6CNC0011_RD_GLB._Active._OcnPact()
            return allFields

    class _inten_status(AtRegister.AtRegister):
        def name(self):
            return "Chip Interrupt Status"
    
        def description(self):
            return "This register configure interrupt enable."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0xffffffff

        class _CDR2IntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "CDR2IntEnable"
            
            def description(self):
                return "CDR Group1 Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDR1IntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "CDR1IntEnable"
            
            def description(self):
                return "CDR Group0 Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SysMonTempEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "SysMonTempEnable"
            
            def description(self):
                return "SysMon temperature Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "ParIntEnable"
            
            def description(self):
                return "RAM Parity Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MDLIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "MDLIntEnable"
            
            def description(self):
                return "MDL DE3 Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PRMIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "PRMIntEnable"
            
            def description(self):
                return "PRM DE1 Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PMIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "PMIntEnable"
            
            def description(self):
                return "PM Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FMIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "FMIntEnable"
            
            def description(self):
                return "FM Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SEUIntEnble(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "SEUIntEnble"
            
            def description(self):
                return "SEU Interrupt Alarm"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _EthIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "EthIntEnable"
            
            def description(self):
                return "ETH Port Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PWIntEnable"
            
            def description(self):
                return "PW Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1Grp1IntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "DE1Grp1IntEnable"
            
            def description(self):
                return "PDH DE1 Group0 Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3Grp1IntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "DE3Grp1IntEnable"
            
            def description(self):
                return "PDH DE3 Group0 Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1Grp0IntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "DE1Grp0IntEnable"
            
            def description(self):
                return "PDH DE1 Group0 Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3Grp0IntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "DE3Grp0IntEnable"
            
            def description(self):
                return "PDH DE3 Group0 Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNVTIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "OCNVTIntEnable"
            
            def description(self):
                return "OCN VT Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNSTSIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "OCNSTSIntEnable"
            
            def description(self):
                return "OCN STS Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNLineIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "OCNLineIntEnable"
            
            def description(self):
                return "OCN Line Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDR2IntEnable"] = _AF6CNC0011_RD_GLB._inten_status._CDR2IntEnable()
            allFields["CDR1IntEnable"] = _AF6CNC0011_RD_GLB._inten_status._CDR1IntEnable()
            allFields["SysMonTempEnable"] = _AF6CNC0011_RD_GLB._inten_status._SysMonTempEnable()
            allFields["ParIntEnable"] = _AF6CNC0011_RD_GLB._inten_status._ParIntEnable()
            allFields["MDLIntEnable"] = _AF6CNC0011_RD_GLB._inten_status._MDLIntEnable()
            allFields["PRMIntEnable"] = _AF6CNC0011_RD_GLB._inten_status._PRMIntEnable()
            allFields["PMIntEnable"] = _AF6CNC0011_RD_GLB._inten_status._PMIntEnable()
            allFields["FMIntEnable"] = _AF6CNC0011_RD_GLB._inten_status._FMIntEnable()
            allFields["SEUIntEnble"] = _AF6CNC0011_RD_GLB._inten_status._SEUIntEnble()
            allFields["EthIntEnable"] = _AF6CNC0011_RD_GLB._inten_status._EthIntEnable()
            allFields["PWIntEnable"] = _AF6CNC0011_RD_GLB._inten_status._PWIntEnable()
            allFields["DE1Grp1IntEnable"] = _AF6CNC0011_RD_GLB._inten_status._DE1Grp1IntEnable()
            allFields["DE3Grp1IntEnable"] = _AF6CNC0011_RD_GLB._inten_status._DE3Grp1IntEnable()
            allFields["DE1Grp0IntEnable"] = _AF6CNC0011_RD_GLB._inten_status._DE1Grp0IntEnable()
            allFields["DE3Grp0IntEnable"] = _AF6CNC0011_RD_GLB._inten_status._DE3Grp0IntEnable()
            allFields["OCNVTIntEnable"] = _AF6CNC0011_RD_GLB._inten_status._OCNVTIntEnable()
            allFields["OCNSTSIntEnable"] = _AF6CNC0011_RD_GLB._inten_status._OCNSTSIntEnable()
            allFields["OCNLineIntEnable"] = _AF6CNC0011_RD_GLB._inten_status._OCNLineIntEnable()
            return allFields

    class _inten_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Chip Interrupt Enable Control"
    
        def description(self):
            return "This register configure interrupt enable."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _CDR2IntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "CDR2IntEnable"
            
            def description(self):
                return "CDR Group1 Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDR1IntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "CDR1IntEnable"
            
            def description(self):
                return "CDR Group0 Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SysMonTempEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "SysMonTempEnable"
            
            def description(self):
                return "SysMon temperature Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "ParIntEnable"
            
            def description(self):
                return "RAM Parity Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MDLIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "MDLIntEnable"
            
            def description(self):
                return "MDL DE3 Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PRMIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "PRMIntEnable"
            
            def description(self):
                return "PRM DE1 Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PMIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "PMIntEnable"
            
            def description(self):
                return "PM Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FMIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "FMIntEnable"
            
            def description(self):
                return "FM Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SEUIntEnble(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "SEUIntEnble"
            
            def description(self):
                return "SEU Interrupt Alarm"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _EthIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "EthIntEnable"
            
            def description(self):
                return "ETH Port Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PWIntEnable"
            
            def description(self):
                return "PW Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1Grp1IntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "DE1Grp1IntEnable"
            
            def description(self):
                return "PDH DE1 Group0 Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3Grp1IntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "DE3Grp1IntEnable"
            
            def description(self):
                return "PDH DE3 Group0 Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1Grp0IntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "DE1Grp0IntEnable"
            
            def description(self):
                return "PDH DE1 Group0 Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3Grp0IntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "DE3Grp0IntEnable"
            
            def description(self):
                return "PDH DE3 Group0 Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNVTIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "OCNVTIntEnable"
            
            def description(self):
                return "OCN VT Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNSTSIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "OCNSTSIntEnable"
            
            def description(self):
                return "OCN STS Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNLineIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "OCNLineIntEnable"
            
            def description(self):
                return "OCN Line Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDR2IntEnable"] = _AF6CNC0011_RD_GLB._inten_ctrl._CDR2IntEnable()
            allFields["CDR1IntEnable"] = _AF6CNC0011_RD_GLB._inten_ctrl._CDR1IntEnable()
            allFields["SysMonTempEnable"] = _AF6CNC0011_RD_GLB._inten_ctrl._SysMonTempEnable()
            allFields["ParIntEnable"] = _AF6CNC0011_RD_GLB._inten_ctrl._ParIntEnable()
            allFields["MDLIntEnable"] = _AF6CNC0011_RD_GLB._inten_ctrl._MDLIntEnable()
            allFields["PRMIntEnable"] = _AF6CNC0011_RD_GLB._inten_ctrl._PRMIntEnable()
            allFields["PMIntEnable"] = _AF6CNC0011_RD_GLB._inten_ctrl._PMIntEnable()
            allFields["FMIntEnable"] = _AF6CNC0011_RD_GLB._inten_ctrl._FMIntEnable()
            allFields["SEUIntEnble"] = _AF6CNC0011_RD_GLB._inten_ctrl._SEUIntEnble()
            allFields["EthIntEnable"] = _AF6CNC0011_RD_GLB._inten_ctrl._EthIntEnable()
            allFields["PWIntEnable"] = _AF6CNC0011_RD_GLB._inten_ctrl._PWIntEnable()
            allFields["DE1Grp1IntEnable"] = _AF6CNC0011_RD_GLB._inten_ctrl._DE1Grp1IntEnable()
            allFields["DE3Grp1IntEnable"] = _AF6CNC0011_RD_GLB._inten_ctrl._DE3Grp1IntEnable()
            allFields["DE1Grp0IntEnable"] = _AF6CNC0011_RD_GLB._inten_ctrl._DE1Grp0IntEnable()
            allFields["DE3Grp0IntEnable"] = _AF6CNC0011_RD_GLB._inten_ctrl._DE3Grp0IntEnable()
            allFields["OCNVTIntEnable"] = _AF6CNC0011_RD_GLB._inten_ctrl._OCNVTIntEnable()
            allFields["OCNSTSIntEnable"] = _AF6CNC0011_RD_GLB._inten_ctrl._OCNSTSIntEnable()
            allFields["OCNLineIntEnable"] = _AF6CNC0011_RD_GLB._inten_ctrl._OCNLineIntEnable()
            return allFields

    class _inten_restore(AtRegister.AtRegister):
        def name(self):
            return "Chip Interrupt Restore Control"
    
        def description(self):
            return "This register configure interrupt restore enable."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000004
            
        def endAddress(self):
            return 0xffffffff

        class _CDR2IntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "CDR2IntEnable"
            
            def description(self):
                return "CDR Group1 Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDR1IntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "CDR1IntEnable"
            
            def description(self):
                return "CDR Group0 Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SysMonTempEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "SysMonTempEnable"
            
            def description(self):
                return "SysMon temperature Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "ParIntEnable"
            
            def description(self):
                return "RAM Parity Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MDLIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "MDLIntEnable"
            
            def description(self):
                return "MDL DE3 Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PRMIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "PRMIntEnable"
            
            def description(self):
                return "PRM DE1 Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PMIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "PMIntEnable"
            
            def description(self):
                return "PM Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FMIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "FMIntEnable"
            
            def description(self):
                return "FM Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _EthIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "EthIntEnable"
            
            def description(self):
                return "ETH Port Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PWIntEnable"
            
            def description(self):
                return "PW Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1Grp1IntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "DE1Grp1IntEnable"
            
            def description(self):
                return "PDH DE1 Group0 Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3Grp1IntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "DE3Grp1IntEnable"
            
            def description(self):
                return "PDH DE3 Group0 Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1Grp0IntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "DE1Grp0IntEnable"
            
            def description(self):
                return "PDH DE1 Group0 Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3Grp0IntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "DE3Grp0IntEnable"
            
            def description(self):
                return "PDH DE3 Group0 Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNVTIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "OCNVTIntEnable"
            
            def description(self):
                return "OCN VT Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNSTSIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "OCNSTSIntEnable"
            
            def description(self):
                return "OCN STS Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNLineIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "OCNLineIntEnable"
            
            def description(self):
                return "OCN Line Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDR2IntEnable"] = _AF6CNC0011_RD_GLB._inten_restore._CDR2IntEnable()
            allFields["CDR1IntEnable"] = _AF6CNC0011_RD_GLB._inten_restore._CDR1IntEnable()
            allFields["SysMonTempEnable"] = _AF6CNC0011_RD_GLB._inten_restore._SysMonTempEnable()
            allFields["ParIntEnable"] = _AF6CNC0011_RD_GLB._inten_restore._ParIntEnable()
            allFields["MDLIntEnable"] = _AF6CNC0011_RD_GLB._inten_restore._MDLIntEnable()
            allFields["PRMIntEnable"] = _AF6CNC0011_RD_GLB._inten_restore._PRMIntEnable()
            allFields["PMIntEnable"] = _AF6CNC0011_RD_GLB._inten_restore._PMIntEnable()
            allFields["FMIntEnable"] = _AF6CNC0011_RD_GLB._inten_restore._FMIntEnable()
            allFields["EthIntEnable"] = _AF6CNC0011_RD_GLB._inten_restore._EthIntEnable()
            allFields["PWIntEnable"] = _AF6CNC0011_RD_GLB._inten_restore._PWIntEnable()
            allFields["DE1Grp1IntEnable"] = _AF6CNC0011_RD_GLB._inten_restore._DE1Grp1IntEnable()
            allFields["DE3Grp1IntEnable"] = _AF6CNC0011_RD_GLB._inten_restore._DE3Grp1IntEnable()
            allFields["DE1Grp0IntEnable"] = _AF6CNC0011_RD_GLB._inten_restore._DE1Grp0IntEnable()
            allFields["DE3Grp0IntEnable"] = _AF6CNC0011_RD_GLB._inten_restore._DE3Grp0IntEnable()
            allFields["OCNVTIntEnable"] = _AF6CNC0011_RD_GLB._inten_restore._OCNVTIntEnable()
            allFields["OCNSTSIntEnable"] = _AF6CNC0011_RD_GLB._inten_restore._OCNSTSIntEnable()
            allFields["OCNLineIntEnable"] = _AF6CNC0011_RD_GLB._inten_restore._OCNLineIntEnable()
            return allFields

    class _inten_Eport(AtRegister.AtRegister):
        def name(self):
            return "EPort Interrupt Enable Control"
    
        def description(self):
            return "This register configure interrupt enable."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000000c0
            
        def endAddress(self):
            return 0xffffffff

        class _XFI_3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "XFI_3"
            
            def description(self):
                return "XFI_3    Interrupt Enable  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _XFI_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "XFI_2"
            
            def description(self):
                return "XFI_2    Interrupt Enable  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _XFI_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "XFI_1"
            
            def description(self):
                return "XFI_1    Interrupt Enable  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _XFI_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "XFI_0"
            
            def description(self):
                return "XFI_0    Interrupt Enable  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _QSGMII_3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "QSGMII_3"
            
            def description(self):
                return "QSGMII_3 Interrupt Enable  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _QSGMII_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "QSGMII_2"
            
            def description(self):
                return "QSGMII_2 Interrupt Enable  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _QSGMII_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "QSGMII_1"
            
            def description(self):
                return "QSGMII_1 Interrupt Enable  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _QSGMII_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "QSGMII_0"
            
            def description(self):
                return "QSGMII_0 Interrupt Enable  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["XFI_3"] = _AF6CNC0011_RD_GLB._inten_Eport._XFI_3()
            allFields["XFI_2"] = _AF6CNC0011_RD_GLB._inten_Eport._XFI_2()
            allFields["XFI_1"] = _AF6CNC0011_RD_GLB._inten_Eport._XFI_1()
            allFields["XFI_0"] = _AF6CNC0011_RD_GLB._inten_Eport._XFI_0()
            allFields["QSGMII_3"] = _AF6CNC0011_RD_GLB._inten_Eport._QSGMII_3()
            allFields["QSGMII_2"] = _AF6CNC0011_RD_GLB._inten_Eport._QSGMII_2()
            allFields["QSGMII_1"] = _AF6CNC0011_RD_GLB._inten_Eport._QSGMII_1()
            allFields["QSGMII_0"] = _AF6CNC0011_RD_GLB._inten_Eport._QSGMII_0()
            return allFields

    class _linkalm_Eport(AtRegister.AtRegister):
        def name(self):
            return "EPort Link Event"
    
        def description(self):
            return "This register report state change of link status."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000000c4
            
        def endAddress(self):
            return 0xffffffff

        class _XFI_3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "XFI_3"
            
            def description(self):
                return "XFI_3    Link State change Event  1: Changed  0: Normal"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        class _XFI_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "XFI_2"
            
            def description(self):
                return "XFI_2    Link State change Event  1: Changed  0: Normal"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        class _XFI_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "XFI_1"
            
            def description(self):
                return "XFI_1    Link State change Event  1: Changed  0: Normal"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        class _XFI_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "XFI_0"
            
            def description(self):
                return "XFI_0    Link State change Event  1: Changed  0: Normal"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        class _QSGMII_3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "QSGMII_3"
            
            def description(self):
                return "QSGMII_3 Link State change Event  1: Changed  0: Normal"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        class _QSGMII_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "QSGMII_2"
            
            def description(self):
                return "QSGMII_2 Link State change Event  1: Changed  0: Normal"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        class _QSGMII_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "QSGMII_1"
            
            def description(self):
                return "QSGMII_1 Link State change Event  1: Changed  0: Normal"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        class _QSGMII_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "QSGMII_0"
            
            def description(self):
                return "QSGMII_0 Link State change Event  1: Changed  0: Normal"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["XFI_3"] = _AF6CNC0011_RD_GLB._linkalm_Eport._XFI_3()
            allFields["XFI_2"] = _AF6CNC0011_RD_GLB._linkalm_Eport._XFI_2()
            allFields["XFI_1"] = _AF6CNC0011_RD_GLB._linkalm_Eport._XFI_1()
            allFields["XFI_0"] = _AF6CNC0011_RD_GLB._linkalm_Eport._XFI_0()
            allFields["QSGMII_3"] = _AF6CNC0011_RD_GLB._linkalm_Eport._QSGMII_3()
            allFields["QSGMII_2"] = _AF6CNC0011_RD_GLB._linkalm_Eport._QSGMII_2()
            allFields["QSGMII_1"] = _AF6CNC0011_RD_GLB._linkalm_Eport._QSGMII_1()
            allFields["QSGMII_0"] = _AF6CNC0011_RD_GLB._linkalm_Eport._QSGMII_0()
            return allFields

    class _linksta_Eport(AtRegister.AtRegister):
        def name(self):
            return "EPort Link Status"
    
        def description(self):
            return "This register configure interrupt enable."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000000c8
            
        def endAddress(self):
            return 0xffffffff

        class _XFI_3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "XFI_3"
            
            def description(self):
                return "XFI_3 Link Status  1: Up  0: Down"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _XFI_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "XFI_2"
            
            def description(self):
                return "XFI_2 Link Status  1: Up  0: Down"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _XFI_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "XFI_1"
            
            def description(self):
                return "XFI_1 Link Status  1: Up  0: Down"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _XFI_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "XFI_0"
            
            def description(self):
                return "XFI_0 Link Status  1: Up  0: Down"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _QSGMII_3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "QSGMII_3"
            
            def description(self):
                return "QSGMII_3 Link Status 1: Up  0: Down"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _QSGMII_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "QSGMII_2"
            
            def description(self):
                return "QSGMII_2 Link Status 1: Up  0: Down"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _QSGMII_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "QSGMII_1"
            
            def description(self):
                return "QSGMII_1 Link Status 1: Up  0: Down"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _QSGMII_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "QSGMII_0"
            
            def description(self):
                return "QSGMII_0 Link Status 1: Up  0: Down"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["XFI_3"] = _AF6CNC0011_RD_GLB._linksta_Eport._XFI_3()
            allFields["XFI_2"] = _AF6CNC0011_RD_GLB._linksta_Eport._XFI_2()
            allFields["XFI_1"] = _AF6CNC0011_RD_GLB._linksta_Eport._XFI_1()
            allFields["XFI_0"] = _AF6CNC0011_RD_GLB._linksta_Eport._XFI_0()
            allFields["QSGMII_3"] = _AF6CNC0011_RD_GLB._linksta_Eport._QSGMII_3()
            allFields["QSGMII_2"] = _AF6CNC0011_RD_GLB._linksta_Eport._QSGMII_2()
            allFields["QSGMII_1"] = _AF6CNC0011_RD_GLB._linksta_Eport._QSGMII_1()
            allFields["QSGMII_0"] = _AF6CNC0011_RD_GLB._linksta_Eport._QSGMII_0()
            return allFields

    class _linkfrc_Eport(AtRegister.AtRegister):
        def name(self):
            return "EPort Link Force"
    
        def description(self):
            return "This register configure interrupt enable."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000000cc
            
        def endAddress(self):
            return 0xffffffff

        class _XFI_3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "XFI_3"
            
            def description(self):
                return "XFI_3 Link Force  1: Up  0: Down"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _XFI_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "XFI_2"
            
            def description(self):
                return "XFI_2 Link Force  1: Up  0: Down"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _XFI_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "XFI_1"
            
            def description(self):
                return "XFI_1 Link Force  1: Up  0: Down"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _XFI_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "XFI_0"
            
            def description(self):
                return "XFI_0 Link Force  1: Up  0: Down"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _QSGMII_3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "QSGMII_3"
            
            def description(self):
                return "QSGMII_3 Link Force 1: Up  0: Down"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _QSGMII_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "QSGMII_2"
            
            def description(self):
                return "QSGMII_2 Link Force 1: Up  0: Down"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _QSGMII_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "QSGMII_1"
            
            def description(self):
                return "QSGMII_1 Link Force 1: Up  0: Down"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _QSGMII_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "QSGMII_0"
            
            def description(self):
                return "QSGMII_0 Link Force 1: Up  0: Down"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["XFI_3"] = _AF6CNC0011_RD_GLB._linkfrc_Eport._XFI_3()
            allFields["XFI_2"] = _AF6CNC0011_RD_GLB._linkfrc_Eport._XFI_2()
            allFields["XFI_1"] = _AF6CNC0011_RD_GLB._linkfrc_Eport._XFI_1()
            allFields["XFI_0"] = _AF6CNC0011_RD_GLB._linkfrc_Eport._XFI_0()
            allFields["QSGMII_3"] = _AF6CNC0011_RD_GLB._linkfrc_Eport._QSGMII_3()
            allFields["QSGMII_2"] = _AF6CNC0011_RD_GLB._linkfrc_Eport._QSGMII_2()
            allFields["QSGMII_1"] = _AF6CNC0011_RD_GLB._linkfrc_Eport._QSGMII_1()
            allFields["QSGMII_0"] = _AF6CNC0011_RD_GLB._linkfrc_Eport._QSGMII_0()
            return allFields

    class _wr_hold_63_32(AtRegister.AtRegister):
        def name(self):
            return "Write Hold Data63_32"
    
        def description(self):
            return "This register is used to write dword #2 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000011
            
        def endAddress(self):
            return 0xffffffff

        class _WrHold63_32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "WrHold63_32"
            
            def description(self):
                return "The write dword #2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["WrHold63_32"] = _AF6CNC0011_RD_GLB._wr_hold_63_32._WrHold63_32()
            return allFields

    class _wr_hold_95_64(AtRegister.AtRegister):
        def name(self):
            return "Write Hold Data95_64"
    
        def description(self):
            return "This register is used to write dword #3 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000012
            
        def endAddress(self):
            return 0xffffffff

        class _WrHold95_64(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "WrHold95_64"
            
            def description(self):
                return "The write dword #3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["WrHold95_64"] = _AF6CNC0011_RD_GLB._wr_hold_95_64._WrHold95_64()
            return allFields

    class _wr_hold_127_96(AtRegister.AtRegister):
        def name(self):
            return "Write Hold Data127_96"
    
        def description(self):
            return "This register is used to write dword #4 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000013
            
        def endAddress(self):
            return 0xffffffff

        class _WrHold127_96(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "WrHold127_96"
            
            def description(self):
                return "The write dword #4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["WrHold127_96"] = _AF6CNC0011_RD_GLB._wr_hold_127_96._WrHold127_96()
            return allFields

    class _rd_hold_63_32(AtRegister.AtRegister):
        def name(self):
            return "Read Hold Data63_32"
    
        def description(self):
            return "This register is used to Read dword #2 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000021
            
        def endAddress(self):
            return 0xffffffff

        class _rdHold63_32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rdHold63_32"
            
            def description(self):
                return "The Read dword #2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rdHold63_32"] = _AF6CNC0011_RD_GLB._rd_hold_63_32._rdHold63_32()
            return allFields

    class _rd_hold_95_64(AtRegister.AtRegister):
        def name(self):
            return "Read Hold Data95_64"
    
        def description(self):
            return "This register is used to Read dword #3 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000022
            
        def endAddress(self):
            return 0xffffffff

        class _rdHold95_64(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rdHold95_64"
            
            def description(self):
                return "The Read dword #3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rdHold95_64"] = _AF6CNC0011_RD_GLB._rd_hold_95_64._rdHold95_64()
            return allFields

    class _rd_hold_127_96(AtRegister.AtRegister):
        def name(self):
            return "Read Hold Data127_96"
    
        def description(self):
            return "This register is used to Read dword #4 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000023
            
        def endAddress(self):
            return 0xffffffff

        class _rdHold127_96(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rdHold127_96"
            
            def description(self):
                return "The Read dword #4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rdHold127_96"] = _AF6CNC0011_RD_GLB._rd_hold_127_96._rdHold127_96()
            return allFields

    class _mac_add_31_0_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Mac Address Bit31_0 Control"
    
        def description(self):
            return "This register configures bit 31 to 0 of MAC address."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000100
            
        def endAddress(self):
            return 0xffffffff

        class _MacAddress31_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MacAddress31_0"
            
            def description(self):
                return "Bit 31 to 0 of MAC Address. Bit 31 is MSB bit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MacAddress31_0"] = _AF6CNC0011_RD_GLB._mac_add_31_0_ctrl._MacAddress31_0()
            return allFields

    class _mac_add_47_32_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Mac Address Bit47_32 Control"
    
        def description(self):
            return "This register configures bit 47to 32 of MAC address."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000101
            
        def endAddress(self):
            return 0xffffffff

        class _MacAddress47_32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MacAddress47_32"
            
            def description(self):
                return "Bit 47 to 32 of MAC Address. Bit 47 is MSB bit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _GeMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "GeMode"
            
            def description(self):
                return "GE mode 0:XGMII; 1:GMII"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHLoopOut(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "PDHLoopOut"
            
            def description(self):
                return "PDH parallel loopback"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHGlbMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "PDHGlbMode"
            
            def description(self):
                return "PDH global mode for loopback, 0:DS3; 1:E3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MacAddress47_32"] = _AF6CNC0011_RD_GLB._mac_add_47_32_ctrl._MacAddress47_32()
            allFields["GeMode"] = _AF6CNC0011_RD_GLB._mac_add_47_32_ctrl._GeMode()
            allFields["PDHLoopOut"] = _AF6CNC0011_RD_GLB._mac_add_47_32_ctrl._PDHLoopOut()
            allFields["PDHGlbMode"] = _AF6CNC0011_RD_GLB._mac_add_47_32_ctrl._PDHGlbMode()
            return allFields

    class _ip_add_31_0_ctrl(AtRegister.AtRegister):
        def name(self):
            return "IP Address Bit31_0 Control"
    
        def description(self):
            return "This register configures bit 31 to 0 of IP address."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000102
            
        def endAddress(self):
            return 0xffffffff

        class _IpAddress31_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IpAddress31_0"
            
            def description(self):
                return "Bit 31 to 0 of IP Address. Bit 31 is MSB bit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IpAddress31_0"] = _AF6CNC0011_RD_GLB._ip_add_31_0_ctrl._IpAddress31_0()
            return allFields

    class _ip_add_63_32_ctrl(AtRegister.AtRegister):
        def name(self):
            return "IP Address Bit63_32 Control"
    
        def description(self):
            return "This register configures bit 63to 32 of IP address."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000103
            
        def endAddress(self):
            return 0xffffffff

        class _IpAddress63_32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IpAddress63_32"
            
            def description(self):
                return "Bit 63 to 32 of IP Address. Bit 63 is MSB bit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IpAddress63_32"] = _AF6CNC0011_RD_GLB._ip_add_63_32_ctrl._IpAddress63_32()
            return allFields

    class _ip_add_95_64_ctrl(AtRegister.AtRegister):
        def name(self):
            return "IP Address Bit95_64 Control"
    
        def description(self):
            return "This register configures bit 95 to 64 of IP address."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000104
            
        def endAddress(self):
            return 0xffffffff

        class _IpAddress95_64(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IpAddress95_64"
            
            def description(self):
                return "Bit 95 to 64 of IP Address. Bit 95 is MSB bit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IpAddress95_64"] = _AF6CNC0011_RD_GLB._ip_add_95_64_ctrl._IpAddress95_64()
            return allFields

    class _ip_add_127_96_ctrl(AtRegister.AtRegister):
        def name(self):
            return "IP Address Bit127_96 Control"
    
        def description(self):
            return "This register configures bit 127 to 96 of IP address."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000105
            
        def endAddress(self):
            return 0xffffffff

        class _IpAddress127_96(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IpAddress127_96"
            
            def description(self):
                return "Bit 127 to 96 of IP Address. Bit 127 is MSB bit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IpAddress127_96"] = _AF6CNC0011_RD_GLB._ip_add_127_96_ctrl._IpAddress127_96()
            return allFields

    class _cdr_refout_8khz_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR REF OUT 8KHZ Control"
    
        def description(self):
            return "This register is used to get the status of RX NCO engine"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000010c
            
        def endAddress(self):
            return 0xffffffff

        class _Cdr1RefMaskLosDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "Cdr1RefMaskLosDis"
            
            def description(self):
                return ": 0: enable mask out8k in case of LOS 1: disable mask out8k in case of LOS"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Cdr1RefID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 20
        
            def name(self):
                return "Cdr1RefID"
            
            def description(self):
                return "CDR engine ID (in CDR mode) or Rx LIU ID (in loop mode, bit[10:5]) that used for 8Khz ref out"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Cdr1RefDs1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 18
        
            def name(self):
                return "Cdr1RefDs1"
            
            def description(self):
                return "0: Line ID used for 8KHZ ref out is E3 mode 1: Line ID used for 8KHZ ref out is DS3 mode 2: Line ID used for 8KHZ ref out is EC1 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Cdr1RefSelect(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Cdr1RefSelect"
            
            def description(self):
                return ": 0: System mode, used system clock to generate 8Khz ref out 1: Loop timing mode,  used rx LIU to generate 8Khz ref out 2: CDR timing mode,  used clock from CDR engine to generate 8KHZ out. CDR clock can be ACR/DCR/Loop/System/Ext1/..."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Cdr0RefMaskLosDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "Cdr0RefMaskLosDis"
            
            def description(self):
                return ": 0: enable mask out8k in case of LOS 1: disable mask out8k in case of LOS"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Cdr0RefID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Cdr0RefID"
            
            def description(self):
                return "CDR engine ID (in CDR mode) or Rx LIU ID (in loop mode, bit[10:5]) that used for 8Khz ref out"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Cdr0RefDs1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Cdr0RefDs1"
            
            def description(self):
                return ": 0: Line ID used for 8KHZ ref out is E3 mode 1: Line ID used for 8KHZ ref out is DS3 mode 2: Line ID used for 8KHZ ref out is EC1 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Cdr0RefSelect(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Cdr0RefSelect"
            
            def description(self):
                return "0: System mode, used system clock to generate 8Khz ref out 1: Loop timing mode,  used rx LIU  to generate 8Khz ref out 2: CDR timing mode,  used clock from CDR engine to generate 8KHZ out. CDR clock can be ACR/DCR/Loop/System/Ext1/..."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Cdr1RefMaskLosDis"] = _AF6CNC0011_RD_GLB._cdr_refout_8khz_ctrl._Cdr1RefMaskLosDis()
            allFields["Cdr1RefID"] = _AF6CNC0011_RD_GLB._cdr_refout_8khz_ctrl._Cdr1RefID()
            allFields["Cdr1RefDs1"] = _AF6CNC0011_RD_GLB._cdr_refout_8khz_ctrl._Cdr1RefDs1()
            allFields["Cdr1RefSelect"] = _AF6CNC0011_RD_GLB._cdr_refout_8khz_ctrl._Cdr1RefSelect()
            allFields["Cdr0RefMaskLosDis"] = _AF6CNC0011_RD_GLB._cdr_refout_8khz_ctrl._Cdr0RefMaskLosDis()
            allFields["Cdr0RefID"] = _AF6CNC0011_RD_GLB._cdr_refout_8khz_ctrl._Cdr0RefID()
            allFields["Cdr0RefDs1"] = _AF6CNC0011_RD_GLB._cdr_refout_8khz_ctrl._Cdr0RefDs1()
            allFields["Cdr0RefSelect"] = _AF6CNC0011_RD_GLB._cdr_refout_8khz_ctrl._Cdr0RefSelect()
            return allFields

    class _RAM_Parity_Force_Control(AtRegister.AtRegister):
        def name(self):
            return "RAM Parity Force Control"
    
        def description(self):
            return "This register configures force parity for internal RAM"
            
        def width(self):
            return 96
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000130
            
        def endAddress(self):
            return 0xffffffff

        class _DDRTxEccNonCorErr_ErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 86
                
            def startBit(self):
                return 86
        
            def name(self):
                return "DDRTxEccNonCorErr_ErrFrc"
            
            def description(self):
                return "Force ECC non-correctable error for Tx DDR"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DDRTxEccCorErr_ErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 85
                
            def startBit(self):
                return 85
        
            def name(self):
                return "DDRTxEccCorErr_ErrFrc"
            
            def description(self):
                return "Force ECC correctable error for Tx DDR"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DDRRxEccNonCorErr_ErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 84
                
            def startBit(self):
                return 84
        
            def name(self):
                return "DDRRxEccNonCorErr_ErrFrc"
            
            def description(self):
                return "Force ECC non-correctable error for Rx DDR"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DDRRxEccCorErr_ErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 83
                
            def startBit(self):
                return 83
        
            def name(self):
                return "DDRRxEccCorErr_ErrFrc"
            
            def description(self):
                return "Force ECC correctable error for Rx DDR"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLASoftwaveCtl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 82
                
            def startBit(self):
                return 82
        
            def name(self):
                return "CLASoftwaveCtl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Softwave Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRPweLookupCtl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 81
                
            def startBit(self):
                return 81
        
            def name(self):
                return "CDRPweLookupCtl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"CDR Pseudowire Look Up Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAPerGrpEnbCtl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 80
                
            def startBit(self):
                return 80
        
            def name(self):
                return "CLAPerGrpEnbCtl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Classify Per Group Enable Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAMefOverMplsCtl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 79
                
            def startBit(self):
                return 79
        
            def name(self):
                return "CLAMefOverMplsCtl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Classify Pseudowire MEF over MPLS Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAPerPweTypeCtl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 78
                
            def startBit(self):
                return 78
        
            def name(self):
                return "CLAPerPweTypeCtl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Classify Per Pseudowire Type Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHbceLookupInfCtl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 77
                
            def startBit(self):
                return 77
        
            def name(self):
                return "CLAHbceLookupInfCtl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Classify HBCE Looking Up Information Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHbceHashTab1Ctl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 76
                
            def startBit(self):
                return 76
        
            def name(self):
                return "CLAHbceHashTab1Ctl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Classify HBCE Hashing Table Control\" PageID 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHbceHashTab0Ctl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 75
                
            def startBit(self):
                return 75
        
            def name(self):
                return "CLAHbceHashTab0Ctl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Classify HBCE Hashing Table Control\" PageID 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWETxHspwGrpProCtl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 74
                
            def startBit(self):
                return 74
        
            def name(self):
                return "PWETxHspwGrpProCtl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Pseudowire Transmit HSPW Group Protection Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWETxUpsaGrpEnbCtl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 73
                
            def startBit(self):
                return 73
        
            def name(self):
                return "PWETxUpsaGrpEnbCtl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Pseudowire Transmit UPSR Group Enable Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWETxUpsaHspwModCtl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 72
                
            def startBit(self):
                return 72
        
            def name(self):
                return "PWETxUpsaHspwModCtl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Pseudowire Transmit UPSR and HSPW mode Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWETxHspwLabelCtl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 71
                
            def startBit(self):
                return 71
        
            def name(self):
                return "PWETxHspwLabelCtl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Pseudowire Transmit HSPW Label Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWETxHdrRtpSsrcCtl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 70
                
            def startBit(self):
                return 70
        
            def name(self):
                return "PWETxHdrRtpSsrcCtl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Pseudowire Transmit Header RTP SSRC Value Control \""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWETxEthHdrLenCtl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 69
                
            def startBit(self):
                return 69
        
            def name(self):
                return "PWETxEthHdrLenCtl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Pseudowire Transmit Ethernet Header Length Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWETxEthHdrValCtl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 68
                
            def startBit(self):
                return 68
        
            def name(self):
                return "PWETxEthHdrValCtl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Pseudowire Transmit Ethernet Header Value Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWETxEnbCtl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 67
                
            def startBit(self):
                return 67
        
            def name(self):
                return "PWETxEnbCtl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Pseudowire Transmit Enable Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAddrCrc_ErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 66
                
            def startBit(self):
                return 66
        
            def name(self):
                return "PDAddrCrc_ErrFrc"
            
            def description(self):
                return "Force check DDR CRC error"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDATdmMode_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 65
                
            def startBit(self):
                return 65
        
            def name(self):
                return "PDATdmMode_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Pseudowire PDA TDM Mode Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAJitbuf_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 64
                
            def startBit(self):
                return 64
        
            def name(self):
                return "PDAJitbuf_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Pseudowire PDA Jitter Buffer Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAReor_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 63
        
            def name(self):
                return "PDAReor_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Pseudowire PDA Reorder Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PLAPaylSizeSlc1_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 62
                
            def startBit(self):
                return 62
        
            def name(self):
                return "PLAPaylSizeSlc1_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Thalassa PDHPW Payload Assemble Payload Size Control\"SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PLAPaylSizeSlc0_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 61
                
            def startBit(self):
                return 61
        
            def name(self):
                return "PLAPaylSizeSlc0_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Thalassa PDHPW Payload Assemble Payload Size Control\"SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRACRTimingCtrl_ParErrFrc_Slice1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 60
                
            def startBit(self):
                return 60
        
            def name(self):
                return "CDRACRTimingCtrl_ParErrFrc_Slice1"
            
            def description(self):
                return "Force parity For RAM Control \"CDR ACR Engine Timing control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRVTTimingCtrl_ParErrFrc_Slice1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 59
                
            def startBit(self):
                return 59
        
            def name(self):
                return "CDRVTTimingCtrl_ParErrFrc_Slice1"
            
            def description(self):
                return "Force parity For RAM Control \"CDR VT Timing control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRSTSTimingCtrl_ParErrFrc_Slice1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 58
                
            def startBit(self):
                return 58
        
            def name(self):
                return "CDRSTSTimingCtrl_ParErrFrc_Slice1"
            
            def description(self):
                return "Force parity For RAM Control \"CDR STS Timing control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRACRTimingCtrl_ParErrFrc_Slice0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 57
                
            def startBit(self):
                return 57
        
            def name(self):
                return "CDRACRTimingCtrl_ParErrFrc_Slice0"
            
            def description(self):
                return "Force parity For RAM Control \"CDR ACR Engine Timing control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRVTTimingCtrl_ParErrFrc_Slice0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 56
                
            def startBit(self):
                return 56
        
            def name(self):
                return "CDRVTTimingCtrl_ParErrFrc_Slice0"
            
            def description(self):
                return "Force parity For RAM Control \"CDR VT Timing control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRSTSTimingCtrl_ParErrFrc_Slice0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 55
                
            def startBit(self):
                return 55
        
            def name(self):
                return "CDRSTSTimingCtrl_ParErrFrc_Slice0"
            
            def description(self):
                return "Force parity For RAM Control \"CDR STS Timing control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DeMapChlCtrl_ParErrFrc_Slice1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 54
                
            def startBit(self):
                return 54
        
            def name(self):
                return "DeMapChlCtrl_ParErrFrc_Slice1"
            
            def description(self):
                return "Force parity For RAM Control \"DEMAP Thalassa Demap Channel Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DeMapChlCtrl_ParErrFrc_Slice0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 53
                
            def startBit(self):
                return 53
        
            def name(self):
                return "DeMapChlCtrl_ParErrFrc_Slice0"
            
            def description(self):
                return "Force parity For RAM Control \"DEMAP Thalassa Demap Channel Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPLineCtrl_ParErrFrc_Slice1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 52
                
            def startBit(self):
                return 52
        
            def name(self):
                return "MAPLineCtrl_ParErrFrc_Slice1"
            
            def description(self):
                return "Force parity For RAM Control \"MAP Thalassa Map Line Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPChlCtrl_ParErrFrc_Slice1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 51
                
            def startBit(self):
                return 51
        
            def name(self):
                return "MAPChlCtrl_ParErrFrc_Slice1"
            
            def description(self):
                return "Force parity For RAM Control \"MAP Thalassa Map Channel Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPLineCtrl_ParErrFrc_Slice0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 50
                
            def startBit(self):
                return 50
        
            def name(self):
                return "MAPLineCtrl_ParErrFrc_Slice0"
            
            def description(self):
                return "Force parity For RAM Control \"MAP Thalassa Map Line Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPChlCtrl_ParErrFrc_Slice0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 49
                
            def startBit(self):
                return 49
        
            def name(self):
                return "MAPChlCtrl_ParErrFrc_Slice0"
            
            def description(self):
                return "Force parity For RAM Control \"MAP Thalassa Map Channel Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHVTAsyncMapCtrl_ParErrFrc_Slice1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 48
                
            def startBit(self):
                return 48
        
            def name(self):
                return "PDHVTAsyncMapCtrl_ParErrFrc_Slice1"
            
            def description(self):
                return "Force parity For RAM Control \"PDH VT Async Map Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHSTSVTMapCtrl_ParErrFrc_Slice1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 47
        
            def name(self):
                return "PDHSTSVTMapCtrl_ParErrFrc_Slice1"
            
            def description(self):
                return "Force parity For RAM Control \"PDH STS/VT Map Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM23E23Trace_ParErrFrc_Slice1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 46
                
            def startBit(self):
                return 46
        
            def name(self):
                return "PDHTxM23E23Trace_ParErrFrc_Slice1"
            
            def description(self):
                return "Force parity For RAM Control \"PDH TxM23E23 E3g832 Trace Byte\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM23E23Ctrl_ParErrFrc_Slice1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 45
                
            def startBit(self):
                return 45
        
            def name(self):
                return "PDHTxM23E23Ctrl_ParErrFrc_Slice1"
            
            def description(self):
                return "Force parity For RAM Control \"PDH TxM23E23 Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM12E12Ctrl_ParErrFrc_Slice1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 44
                
            def startBit(self):
                return 44
        
            def name(self):
                return "PDHTxM12E12Ctrl_ParErrFrc_Slice1"
            
            def description(self):
                return "Force parity For RAM Control \"PDH TxM12E12 Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxDE1SigCtrl_ParErrFrc_Slice1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 43
                
            def startBit(self):
                return 43
        
            def name(self):
                return "PDHTxDE1SigCtrl_ParErrFrc_Slice1"
            
            def description(self):
                return "Force parity For RAM Control \"PDH DS1/E1/J1 Tx Framer Signalling Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxDE1FrmCtrl_ParErrFrc_Slice1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 42
                
            def startBit(self):
                return 42
        
            def name(self):
                return "PDHTxDE1FrmCtrl_ParErrFrc_Slice1"
            
            def description(self):
                return "Force parity For RAM Control \"PDH DS1/E1/J1 Tx Framer Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxDE1FrmCtrl_ParErrFrc_Slice1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 41
                
            def startBit(self):
                return 41
        
            def name(self):
                return "PDHRxDE1FrmCtrl_ParErrFrc_Slice1"
            
            def description(self):
                return "Force parity For RAM Control \"PDH DS1/E1/J1 Rx Framer Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxM12E12Ctrl_ParErrFrc_Slice1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 40
                
            def startBit(self):
                return 40
        
            def name(self):
                return "PDHRxM12E12Ctrl_ParErrFrc_Slice1"
            
            def description(self):
                return "Force parity For RAM Control \"PDH RxM12E12 Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxDS3E3OHCtrl_ParErrFrc_Slice1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 39
                
            def startBit(self):
                return 39
        
            def name(self):
                return "PDHRxDS3E3OHCtrl_ParErrFrc_Slice1"
            
            def description(self):
                return "Force parity For RAM Control \"PDH RxDS3E3 OH Pro Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxM23E23Ctrl_ParErrFrc_Slice1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 38
                
            def startBit(self):
                return 38
        
            def name(self):
                return "PDHRxM23E23Ctrl_ParErrFrc_Slice1"
            
            def description(self):
                return "Force parity For RAM Control \"PDH RxM23E23 Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHSTSVTDeMapCtrl_ParErrFrc_Slice1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 37
                
            def startBit(self):
                return 37
        
            def name(self):
                return "PDHSTSVTDeMapCtrl_ParErrFrc_Slice1"
            
            def description(self):
                return "Force parity For RAM Control \"PDH STS/VT Demap Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHMuxCtrl_ParErrFrc_Slice1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 36
                
            def startBit(self):
                return 36
        
            def name(self):
                return "PDHMuxCtrl_ParErrFrc_Slice1"
            
            def description(self):
                return "Force parity For RAM Control \"PDH DS1/E1/J1 Rx Framer Mux Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHVTAsyncMapCtrl_ParErrFrc_Slice0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 35
                
            def startBit(self):
                return 35
        
            def name(self):
                return "PDHVTAsyncMapCtrl_ParErrFrc_Slice0"
            
            def description(self):
                return "Force parity For RAM Control \"PDH VT Async Map Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHSTSVTMapCtrl_ParErrFrc_Slice0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 34
                
            def startBit(self):
                return 34
        
            def name(self):
                return "PDHSTSVTMapCtrl_ParErrFrc_Slice0"
            
            def description(self):
                return "Force parity For RAM Control \"PDH STS/VT Map Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM23E23Trace_ParErrFrc_Slice0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 33
                
            def startBit(self):
                return 33
        
            def name(self):
                return "PDHTxM23E23Trace_ParErrFrc_Slice0"
            
            def description(self):
                return "Force parity For RAM Control \"PDH TxM23E23 E3g832 Trace Byte\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM23E23Ctrl_ParErrFrc_Slice0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 32
                
            def startBit(self):
                return 32
        
            def name(self):
                return "PDHTxM23E23Ctrl_ParErrFrc_Slice0"
            
            def description(self):
                return "Force parity For RAM Control \"PDH TxM23E23 Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM12E12Ctrl_ParErrFrc_Slice0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "PDHTxM12E12Ctrl_ParErrFrc_Slice0"
            
            def description(self):
                return "Force parity For RAM Control \"PDH TxM12E12 Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxDE1SigCtrl_ParErrFrc_Slice0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "PDHTxDE1SigCtrl_ParErrFrc_Slice0"
            
            def description(self):
                return "Force parity For RAM Control \"PDH DS1/E1/J1 Tx Framer Signalling Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxDE1FrmCtrl_ParErrFrc_Slice0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "PDHTxDE1FrmCtrl_ParErrFrc_Slice0"
            
            def description(self):
                return "Force parity For RAM Control \"PDH DS1/E1/J1 Tx Framer Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxDE1FrmCtrl_ParErrFrc_Slice0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "PDHRxDE1FrmCtrl_ParErrFrc_Slice0"
            
            def description(self):
                return "Force parity For RAM Control \"PDH DS1/E1/J1 Rx Framer Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxM12E12Ctrl_ParErrFrc_Slice0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "PDHRxM12E12Ctrl_ParErrFrc_Slice0"
            
            def description(self):
                return "Force parity For RAM Control \"PDH RxM12E12 Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxDS3E3OHCtrl_ParErrFrc_Slice0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "PDHRxDS3E3OHCtrl_ParErrFrc_Slice0"
            
            def description(self):
                return "Force parity For RAM Control \"PDH RxDS3E3 OH Pro Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxM23E23Ctrl_ParErrFrc_Slice0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "PDHRxM23E23Ctrl_ParErrFrc_Slice0"
            
            def description(self):
                return "Force parity For RAM Control \"PDH RxM23E23 Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHSTSVTDeMapCtrl_ParErrFrc_Slice0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "PDHSTSVTDeMapCtrl_ParErrFrc_Slice0"
            
            def description(self):
                return "Force parity For RAM Control \"PDH STS/VT Demap Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHMuxCtrl_ParErrFrc_Slice0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "PDHMuxCtrl_ParErrFrc_Slice0"
            
            def description(self):
                return "Force parity For RAM Control \"PDH DS1/E1/J1 Rx Framer Mux Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _POHWrCtrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "POHWrCtrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"POH BER Control VT/DSN\", \"POH BER Control STS/TU3\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _POHWrTrsh_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "POHWrTrsh_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"POH BER Threshold 2\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _POHTerCtrlLo_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "POHTerCtrlLo_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"POH Termintate Insert Control VT/TU3\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _POHTerCtrlHi_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "POHTerCtrlHi_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"POH Termintate Insert Control STS\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _POHCpeVtCtr_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "POHCpeVtCtr_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"POH CPE VT Control Register\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _POHCpeStsCtr_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "POHCpeStsCtr_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"POH CPE STS/TU3 Control Register\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNTohCtlSlc1_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "OCNTohCtlSlc1_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"OCN TOH Monitoring Per Channel Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNSpiCtlSlc1_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "OCNSpiCtlSlc1_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"OCN STS Pointer Interpreter Per Channel Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNSpiCtlSlc0_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "OCNSpiCtlSlc0_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"OCN STS Pointer Interpreter Per Channel Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNVpiDemSlc1_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "OCNVpiDemSlc1_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"OCN RXPP Per STS payload Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNVpiDemSlc0_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "OCNVpiDemSlc0_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"OCN RXPP Per STS payload Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNVpiCtlSlc1_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "OCNVpiCtlSlc1_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"OCN VTTU Pointer Interpreter Per Channel Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNVpiCtlSlc0_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "OCNVpiCtlSlc0_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"OCN VTTU Pointer Interpreter Per Channel Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNVpgDemSlc1_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "OCNVpgDemSlc1_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"OCN TXPP Per STS Multiplexing Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNVpgDemSlc0_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "OCNVpgDemSlc0_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"OCN TXPP Per STS Multiplexing Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNVpgCtlSlc1_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "OCNVpgCtlSlc1_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"OCN VTTU Pointer Generator Per Channel Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNVpgCtlSlc0_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "OCNVpgCtlSlc0_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"OCN VTTU Pointer Generator Per Channel Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNSpgCtlSlc1_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "OCNSpgCtlSlc1_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"OCN STS Pointer Generator Per Channel Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNSpgCtlSlc0_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "OCNSpgCtlSlc0_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"OCN STS Pointer Generator Per Channel Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNTfmCtlSlc1_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "OCNTfmCtlSlc1_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"OCN Tx Framer Per Channel Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNTfmCtlSlc0_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "OCNTfmCtlSlc0_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"OCN Tx Framer Per Channel Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNTfmJ0Slc1_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "OCNTfmJ0Slc1_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"OCN Tx J0 Insertion Buffer\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNTfmJ0Slc0_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "OCNTfmJ0Slc0_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"OCN Tx J0 Insertion Buffer\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DDRTxEccNonCorErr_ErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._DDRTxEccNonCorErr_ErrFrc()
            allFields["DDRTxEccCorErr_ErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._DDRTxEccCorErr_ErrFrc()
            allFields["DDRRxEccNonCorErr_ErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._DDRRxEccNonCorErr_ErrFrc()
            allFields["DDRRxEccCorErr_ErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._DDRRxEccCorErr_ErrFrc()
            allFields["CLASoftwaveCtl_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._CLASoftwaveCtl_ParErrFrc()
            allFields["CDRPweLookupCtl_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._CDRPweLookupCtl_ParErrFrc()
            allFields["CLAPerGrpEnbCtl_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._CLAPerGrpEnbCtl_ParErrFrc()
            allFields["CLAMefOverMplsCtl_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._CLAMefOverMplsCtl_ParErrFrc()
            allFields["CLAPerPweTypeCtl_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._CLAPerPweTypeCtl_ParErrFrc()
            allFields["CLAHbceLookupInfCtl_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._CLAHbceLookupInfCtl_ParErrFrc()
            allFields["CLAHbceHashTab1Ctl_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._CLAHbceHashTab1Ctl_ParErrFrc()
            allFields["CLAHbceHashTab0Ctl_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._CLAHbceHashTab0Ctl_ParErrFrc()
            allFields["PWETxHspwGrpProCtl_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PWETxHspwGrpProCtl_ParErrFrc()
            allFields["PWETxUpsaGrpEnbCtl_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PWETxUpsaGrpEnbCtl_ParErrFrc()
            allFields["PWETxUpsaHspwModCtl_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PWETxUpsaHspwModCtl_ParErrFrc()
            allFields["PWETxHspwLabelCtl_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PWETxHspwLabelCtl_ParErrFrc()
            allFields["PWETxHdrRtpSsrcCtl_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PWETxHdrRtpSsrcCtl_ParErrFrc()
            allFields["PWETxEthHdrLenCtl_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PWETxEthHdrLenCtl_ParErrFrc()
            allFields["PWETxEthHdrValCtl_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PWETxEthHdrValCtl_ParErrFrc()
            allFields["PWETxEnbCtl_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PWETxEnbCtl_ParErrFrc()
            allFields["PDAddrCrc_ErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PDAddrCrc_ErrFrc()
            allFields["PDATdmMode_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PDATdmMode_ParErrFrc()
            allFields["PDAJitbuf_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PDAJitbuf_ParErrFrc()
            allFields["PDAReor_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PDAReor_ParErrFrc()
            allFields["PLAPaylSizeSlc1_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PLAPaylSizeSlc1_ParErrFrc()
            allFields["PLAPaylSizeSlc0_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PLAPaylSizeSlc0_ParErrFrc()
            allFields["CDRACRTimingCtrl_ParErrFrc_Slice1"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._CDRACRTimingCtrl_ParErrFrc_Slice1()
            allFields["CDRVTTimingCtrl_ParErrFrc_Slice1"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._CDRVTTimingCtrl_ParErrFrc_Slice1()
            allFields["CDRSTSTimingCtrl_ParErrFrc_Slice1"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._CDRSTSTimingCtrl_ParErrFrc_Slice1()
            allFields["CDRACRTimingCtrl_ParErrFrc_Slice0"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._CDRACRTimingCtrl_ParErrFrc_Slice0()
            allFields["CDRVTTimingCtrl_ParErrFrc_Slice0"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._CDRVTTimingCtrl_ParErrFrc_Slice0()
            allFields["CDRSTSTimingCtrl_ParErrFrc_Slice0"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._CDRSTSTimingCtrl_ParErrFrc_Slice0()
            allFields["DeMapChlCtrl_ParErrFrc_Slice1"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._DeMapChlCtrl_ParErrFrc_Slice1()
            allFields["DeMapChlCtrl_ParErrFrc_Slice0"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._DeMapChlCtrl_ParErrFrc_Slice0()
            allFields["MAPLineCtrl_ParErrFrc_Slice1"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._MAPLineCtrl_ParErrFrc_Slice1()
            allFields["MAPChlCtrl_ParErrFrc_Slice1"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._MAPChlCtrl_ParErrFrc_Slice1()
            allFields["MAPLineCtrl_ParErrFrc_Slice0"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._MAPLineCtrl_ParErrFrc_Slice0()
            allFields["MAPChlCtrl_ParErrFrc_Slice0"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._MAPChlCtrl_ParErrFrc_Slice0()
            allFields["PDHVTAsyncMapCtrl_ParErrFrc_Slice1"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PDHVTAsyncMapCtrl_ParErrFrc_Slice1()
            allFields["PDHSTSVTMapCtrl_ParErrFrc_Slice1"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PDHSTSVTMapCtrl_ParErrFrc_Slice1()
            allFields["PDHTxM23E23Trace_ParErrFrc_Slice1"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PDHTxM23E23Trace_ParErrFrc_Slice1()
            allFields["PDHTxM23E23Ctrl_ParErrFrc_Slice1"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PDHTxM23E23Ctrl_ParErrFrc_Slice1()
            allFields["PDHTxM12E12Ctrl_ParErrFrc_Slice1"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PDHTxM12E12Ctrl_ParErrFrc_Slice1()
            allFields["PDHTxDE1SigCtrl_ParErrFrc_Slice1"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PDHTxDE1SigCtrl_ParErrFrc_Slice1()
            allFields["PDHTxDE1FrmCtrl_ParErrFrc_Slice1"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PDHTxDE1FrmCtrl_ParErrFrc_Slice1()
            allFields["PDHRxDE1FrmCtrl_ParErrFrc_Slice1"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PDHRxDE1FrmCtrl_ParErrFrc_Slice1()
            allFields["PDHRxM12E12Ctrl_ParErrFrc_Slice1"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PDHRxM12E12Ctrl_ParErrFrc_Slice1()
            allFields["PDHRxDS3E3OHCtrl_ParErrFrc_Slice1"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PDHRxDS3E3OHCtrl_ParErrFrc_Slice1()
            allFields["PDHRxM23E23Ctrl_ParErrFrc_Slice1"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PDHRxM23E23Ctrl_ParErrFrc_Slice1()
            allFields["PDHSTSVTDeMapCtrl_ParErrFrc_Slice1"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PDHSTSVTDeMapCtrl_ParErrFrc_Slice1()
            allFields["PDHMuxCtrl_ParErrFrc_Slice1"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PDHMuxCtrl_ParErrFrc_Slice1()
            allFields["PDHVTAsyncMapCtrl_ParErrFrc_Slice0"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PDHVTAsyncMapCtrl_ParErrFrc_Slice0()
            allFields["PDHSTSVTMapCtrl_ParErrFrc_Slice0"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PDHSTSVTMapCtrl_ParErrFrc_Slice0()
            allFields["PDHTxM23E23Trace_ParErrFrc_Slice0"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PDHTxM23E23Trace_ParErrFrc_Slice0()
            allFields["PDHTxM23E23Ctrl_ParErrFrc_Slice0"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PDHTxM23E23Ctrl_ParErrFrc_Slice0()
            allFields["PDHTxM12E12Ctrl_ParErrFrc_Slice0"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PDHTxM12E12Ctrl_ParErrFrc_Slice0()
            allFields["PDHTxDE1SigCtrl_ParErrFrc_Slice0"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PDHTxDE1SigCtrl_ParErrFrc_Slice0()
            allFields["PDHTxDE1FrmCtrl_ParErrFrc_Slice0"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PDHTxDE1FrmCtrl_ParErrFrc_Slice0()
            allFields["PDHRxDE1FrmCtrl_ParErrFrc_Slice0"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PDHRxDE1FrmCtrl_ParErrFrc_Slice0()
            allFields["PDHRxM12E12Ctrl_ParErrFrc_Slice0"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PDHRxM12E12Ctrl_ParErrFrc_Slice0()
            allFields["PDHRxDS3E3OHCtrl_ParErrFrc_Slice0"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PDHRxDS3E3OHCtrl_ParErrFrc_Slice0()
            allFields["PDHRxM23E23Ctrl_ParErrFrc_Slice0"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PDHRxM23E23Ctrl_ParErrFrc_Slice0()
            allFields["PDHSTSVTDeMapCtrl_ParErrFrc_Slice0"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PDHSTSVTDeMapCtrl_ParErrFrc_Slice0()
            allFields["PDHMuxCtrl_ParErrFrc_Slice0"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._PDHMuxCtrl_ParErrFrc_Slice0()
            allFields["POHWrCtrl_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._POHWrCtrl_ParErrFrc()
            allFields["POHWrTrsh_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._POHWrTrsh_ParErrFrc()
            allFields["POHTerCtrlLo_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._POHTerCtrlLo_ParErrFrc()
            allFields["POHTerCtrlHi_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._POHTerCtrlHi_ParErrFrc()
            allFields["POHCpeVtCtr_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._POHCpeVtCtr_ParErrFrc()
            allFields["POHCpeStsCtr_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._POHCpeStsCtr_ParErrFrc()
            allFields["OCNTohCtlSlc1_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._OCNTohCtlSlc1_ParErrFrc()
            allFields["OCNSpiCtlSlc1_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._OCNSpiCtlSlc1_ParErrFrc()
            allFields["OCNSpiCtlSlc0_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._OCNSpiCtlSlc0_ParErrFrc()
            allFields["OCNVpiDemSlc1_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._OCNVpiDemSlc1_ParErrFrc()
            allFields["OCNVpiDemSlc0_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._OCNVpiDemSlc0_ParErrFrc()
            allFields["OCNVpiCtlSlc1_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._OCNVpiCtlSlc1_ParErrFrc()
            allFields["OCNVpiCtlSlc0_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._OCNVpiCtlSlc0_ParErrFrc()
            allFields["OCNVpgDemSlc1_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._OCNVpgDemSlc1_ParErrFrc()
            allFields["OCNVpgDemSlc0_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._OCNVpgDemSlc0_ParErrFrc()
            allFields["OCNVpgCtlSlc1_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._OCNVpgCtlSlc1_ParErrFrc()
            allFields["OCNVpgCtlSlc0_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._OCNVpgCtlSlc0_ParErrFrc()
            allFields["OCNSpgCtlSlc1_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._OCNSpgCtlSlc1_ParErrFrc()
            allFields["OCNSpgCtlSlc0_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._OCNSpgCtlSlc0_ParErrFrc()
            allFields["OCNTfmCtlSlc1_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._OCNTfmCtlSlc1_ParErrFrc()
            allFields["OCNTfmCtlSlc0_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._OCNTfmCtlSlc0_ParErrFrc()
            allFields["OCNTfmJ0Slc1_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._OCNTfmJ0Slc1_ParErrFrc()
            allFields["OCNTfmJ0Slc0_ParErrFrc"] = _AF6CNC0011_RD_GLB._RAM_Parity_Force_Control._OCNTfmJ0Slc0_ParErrFrc()
            return allFields

    class _RAM_Parity_Disable_Control(AtRegister.AtRegister):
        def name(self):
            return "RAM parity Disable Control"
    
        def description(self):
            return "This register configures disable parity for internal RAM"
            
        def width(self):
            return 96
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000131
            
        def endAddress(self):
            return 0xffffffff

        class _DDRTxEccNonCorErr_ErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 86
                
            def startBit(self):
                return 86
        
            def name(self):
                return "DDRTxEccNonCorErr_ErrDis"
            
            def description(self):
                return "Disable check ECC non-correctable error for Tx DDR"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DDRTxEccCorErr_ErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 85
                
            def startBit(self):
                return 85
        
            def name(self):
                return "DDRTxEccCorErr_ErrDis"
            
            def description(self):
                return "Disable check ECC correctable error for Tx DDR"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DDRRxEccNonCorErr_ErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 84
                
            def startBit(self):
                return 84
        
            def name(self):
                return "DDRRxEccNonCorErr_ErrDis"
            
            def description(self):
                return "Disable check ECC non-correctable error for Rx DDR"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DDRRxEccCorErr_ErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 83
                
            def startBit(self):
                return 83
        
            def name(self):
                return "DDRRxEccCorErr_ErrDis"
            
            def description(self):
                return "Disable check ECC correctable error for Rx DDR"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLASoftwaveCtl_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 82
                
            def startBit(self):
                return 82
        
            def name(self):
                return "CLASoftwaveCtl_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Softwave Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRPweLookupCtl_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 81
                
            def startBit(self):
                return 81
        
            def name(self):
                return "CDRPweLookupCtl_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"CDR Pseudowire Look Up Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAPerGrpEnbCtl_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 80
                
            def startBit(self):
                return 80
        
            def name(self):
                return "CLAPerGrpEnbCtl_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Classify Per Group Enable Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAMefOverMplsCtl_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 79
                
            def startBit(self):
                return 79
        
            def name(self):
                return "CLAMefOverMplsCtl_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Classify Pseudowire MEF over MPLS Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAPerPweTypeCtl_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 78
                
            def startBit(self):
                return 78
        
            def name(self):
                return "CLAPerPweTypeCtl_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Classify Per Pseudowire Type Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHbceLookupInfCtl_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 77
                
            def startBit(self):
                return 77
        
            def name(self):
                return "CLAHbceLookupInfCtl_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Classify HBCE Looking Up Information Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHbceHashTab1Ctl_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 76
                
            def startBit(self):
                return 76
        
            def name(self):
                return "CLAHbceHashTab1Ctl_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Classify HBCE Hashing Table Control\" PageID 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHbceHashTab0Ctl_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 75
                
            def startBit(self):
                return 75
        
            def name(self):
                return "CLAHbceHashTab0Ctl_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Classify HBCE Hashing Table Control\" PageID 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWETxHspwGrpProCtl_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 74
                
            def startBit(self):
                return 74
        
            def name(self):
                return "PWETxHspwGrpProCtl_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Pseudowire Transmit HSPW Group Protection Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWETxUpsaGrpEnbCtl_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 73
                
            def startBit(self):
                return 73
        
            def name(self):
                return "PWETxUpsaGrpEnbCtl_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Pseudowire Transmit UPSR Group Enable Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWETxUpsaHspwModCtl_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 72
                
            def startBit(self):
                return 72
        
            def name(self):
                return "PWETxUpsaHspwModCtl_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Pseudowire Transmit UPSR and HSPW mode Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWETxHspwLabelCtl_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 71
                
            def startBit(self):
                return 71
        
            def name(self):
                return "PWETxHspwLabelCtl_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Pseudowire Transmit HSPW Label Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWETxHdrRtpSsrcCtl_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 70
                
            def startBit(self):
                return 70
        
            def name(self):
                return "PWETxHdrRtpSsrcCtl_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Pseudowire Transmit Header RTP SSRC Value Control \""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWETxEthHdrLenCtl_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 69
                
            def startBit(self):
                return 69
        
            def name(self):
                return "PWETxEthHdrLenCtl_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Pseudowire Transmit Ethernet Header Length Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWETxEthHdrValCtl_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 68
                
            def startBit(self):
                return 68
        
            def name(self):
                return "PWETxEthHdrValCtl_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Pseudowire Transmit Ethernet Header Value Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWETxEnbCtl_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 67
                
            def startBit(self):
                return 67
        
            def name(self):
                return "PWETxEnbCtl_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Pseudowire Transmit Enable Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAddrCrc_ErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 66
                
            def startBit(self):
                return 66
        
            def name(self):
                return "PDAddrCrc_ErrDis"
            
            def description(self):
                return "Disable check DDR CRC error"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDATdmMode_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 65
                
            def startBit(self):
                return 65
        
            def name(self):
                return "PDATdmMode_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Pseudowire PDA TDM Mode Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAJitbuf_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 64
                
            def startBit(self):
                return 64
        
            def name(self):
                return "PDAJitbuf_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Pseudowire PDA Jitter Buffer Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAReor_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 63
        
            def name(self):
                return "PDAReor_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Pseudowire PDA Reorder Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PLAPaylSizeSlc1_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 62
                
            def startBit(self):
                return 62
        
            def name(self):
                return "PLAPaylSizeSlc1_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Thalassa PDHPW Payload Assemble Payload Size Control\"SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PLAPaylSizeSlc0_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 61
                
            def startBit(self):
                return 61
        
            def name(self):
                return "PLAPaylSizeSlc0_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Thalassa PDHPW Payload Assemble Payload Size Control\"SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRACRTimingCtrlSlc1_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 60
                
            def startBit(self):
                return 60
        
            def name(self):
                return "CDRACRTimingCtrlSlc1_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"CDR ACR Engine Timing control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRVTTimingCtrlSlc1_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 59
                
            def startBit(self):
                return 59
        
            def name(self):
                return "CDRVTTimingCtrlSlc1_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"CDR VT Timing control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRSTSTimingCtrlSlc1_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 58
                
            def startBit(self):
                return 58
        
            def name(self):
                return "CDRSTSTimingCtrlSlc1_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"CDR STS Timing control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRACRTimingCtrlSlc0_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 57
                
            def startBit(self):
                return 57
        
            def name(self):
                return "CDRACRTimingCtrlSlc0_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"CDR ACR Engine Timing control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRVTTimingCtrlSlc0_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 56
                
            def startBit(self):
                return 56
        
            def name(self):
                return "CDRVTTimingCtrlSlc0_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"CDR VT Timing control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRSTSTimingCtrlSlc0_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 55
                
            def startBit(self):
                return 55
        
            def name(self):
                return "CDRSTSTimingCtrlSlc0_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"CDR STS Timing control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DeMapChlCtrlSlc1_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 54
                
            def startBit(self):
                return 54
        
            def name(self):
                return "DeMapChlCtrlSlc1_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"DEMAP Thalassa Demap Channel Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DeMapChlCtrlSlc0_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 53
                
            def startBit(self):
                return 53
        
            def name(self):
                return "DeMapChlCtrlSlc0_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"DEMAP Thalassa Demap Channel Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPLineCtrlSlc1_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 52
                
            def startBit(self):
                return 52
        
            def name(self):
                return "MAPLineCtrlSlc1_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"MAP Thalassa Map Line Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPChlCtrlSlc1_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 51
                
            def startBit(self):
                return 51
        
            def name(self):
                return "MAPChlCtrlSlc1_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"MAP Thalassa Map Channel Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPLineCtrlSlc0_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 50
                
            def startBit(self):
                return 50
        
            def name(self):
                return "MAPLineCtrlSlc0_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"MAP Thalassa Map Line Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPChlCtrlSlc0_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 49
                
            def startBit(self):
                return 49
        
            def name(self):
                return "MAPChlCtrlSlc0_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"MAP Thalassa Map Channel Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHVTAsyncMapCtrlSlc1_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 48
                
            def startBit(self):
                return 48
        
            def name(self):
                return "PDHVTAsyncMapCtrlSlc1_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH VT Async Map Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHSTSVTMapCtrlSlc1_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 47
        
            def name(self):
                return "PDHSTSVTMapCtrlSlc1_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH STS/VT Map Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM23E23TraceSlc1_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 46
                
            def startBit(self):
                return 46
        
            def name(self):
                return "PDHTxM23E23TraceSlc1_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH TxM23E23 E3g832 Trace Byte\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM23E23CtrlSlc1_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 45
                
            def startBit(self):
                return 45
        
            def name(self):
                return "PDHTxM23E23CtrlSlc1_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH TxM23E23 Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM12E12CtrlSlc1_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 44
                
            def startBit(self):
                return 44
        
            def name(self):
                return "PDHTxM12E12CtrlSlc1_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH TxM12E12 Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxDE1SigCtrlSlc1_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 43
                
            def startBit(self):
                return 43
        
            def name(self):
                return "PDHTxDE1SigCtrlSlc1_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH DS1/E1/J1 Tx Framer Signalling Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxDE1FrmCtrlSlc1_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 42
                
            def startBit(self):
                return 42
        
            def name(self):
                return "PDHTxDE1FrmCtrlSlc1_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH DS1/E1/J1 Tx Framer Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxDE1FrmCtrlSlc1_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 41
                
            def startBit(self):
                return 41
        
            def name(self):
                return "PDHRxDE1FrmCtrlSlc1_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH DS1/E1/J1 Rx Framer Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxM12E12CtrlSlc1_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 40
                
            def startBit(self):
                return 40
        
            def name(self):
                return "PDHRxM12E12CtrlSlc1_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH RxM12E12 Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxDS3E3OHCtrlSlc1_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 39
                
            def startBit(self):
                return 39
        
            def name(self):
                return "PDHRxDS3E3OHCtrlSlc1_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH RxDS3E3 OH Pro Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxM23E23CtrlSlc1_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 38
                
            def startBit(self):
                return 38
        
            def name(self):
                return "PDHRxM23E23CtrlSlc1_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH RxM23E23 Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHSTSVTDeMapCtrlSlc1_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 37
                
            def startBit(self):
                return 37
        
            def name(self):
                return "PDHSTSVTDeMapCtrlSlc1_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH STS/VT Demap Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHMuxCtrlSlc1_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 36
                
            def startBit(self):
                return 36
        
            def name(self):
                return "PDHMuxCtrlSlc1_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH DS1/E1/J1 Rx Framer Mux Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHVTAsyncMapCtrlSlc0_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 35
                
            def startBit(self):
                return 35
        
            def name(self):
                return "PDHVTAsyncMapCtrlSlc0_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH VT Async Map Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHSTSVTMapCtrlSlc0_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 34
                
            def startBit(self):
                return 34
        
            def name(self):
                return "PDHSTSVTMapCtrlSlc0_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH STS/VT Map Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM23E23TraceSlc0_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 33
                
            def startBit(self):
                return 33
        
            def name(self):
                return "PDHTxM23E23TraceSlc0_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH TxM23E23 E3g832 Trace Byte\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM23E23CtrlSlc0_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 32
                
            def startBit(self):
                return 32
        
            def name(self):
                return "PDHTxM23E23CtrlSlc0_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH TxM23E23 Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM12E12CtrlSlc0_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "PDHTxM12E12CtrlSlc0_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH TxM12E12 Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxDE1SigCtrlSlc0_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "PDHTxDE1SigCtrlSlc0_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH DS1/E1/J1 Tx Framer Signalling Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxDE1FrmCtrlSlc0_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "PDHTxDE1FrmCtrlSlc0_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH DS1/E1/J1 Tx Framer Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxDE1FrmCtrlSlc0_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "PDHRxDE1FrmCtrlSlc0_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH DS1/E1/J1 Rx Framer Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxM12E12CtrlSlc0_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "PDHRxM12E12CtrlSlc0_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH RxM12E12 Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxDS3E3OHCtrlSlc0_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "PDHRxDS3E3OHCtrlSlc0_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH RxDS3E3 OH Pro Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxM23E23CtrlSlc0_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "PDHRxM23E23CtrlSlc0_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH RxM23E23 Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHSTSVTDeMapCtrlSlc0_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "PDHSTSVTDeMapCtrlSlc0_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH STS/VT Demap Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHMuxCtrl_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "PDHMuxCtrl_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH DS1/E1/J1 Rx Framer Mux Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _POHWrCtrl_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "POHWrCtrl_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"POH BER Control VT/DSN\", \"POH BER Control STS/TU3\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _POHWrTrsh_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "POHWrTrsh_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"POH BER Threshold 2\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _POHTerCtrlLo_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "POHTerCtrlLo_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"POH Termintate Insert Control VT/TU3\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _POHTerCtrlHi_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "POHTerCtrlHi_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"POH Termintate Insert Control STS\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _POHCpeVtCtr_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "POHCpeVtCtr_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"POH CPE VT Control Register\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _POHCpeStsCtr_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "POHCpeStsCtr_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"POH CPE STS/TU3 Control Register\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNTohCtlSlc1_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "OCNTohCtlSlc1_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"OCN TOH Monitoring Per Channel Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNSpiCtlSlc1_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "OCNSpiCtlSlc1_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"OCN STS Pointer Interpreter Per Channel Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNSpiCtlSlc0_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "OCNSpiCtlSlc0_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"OCN STS Pointer Interpreter Per Channel Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNVpiDemSlc1_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "OCNVpiDemSlc1_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"OCN RXPP Per STS payload Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNVpiDemSlc0_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "OCNVpiDemSlc0_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"OCN RXPP Per STS payload Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNVpiCtlSlc1_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "OCNVpiCtlSlc1_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"OCN VTTU Pointer Interpreter Per Channel Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNVpiCtlSlc0_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "OCNVpiCtlSlc0_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"OCN VTTU Pointer Interpreter Per Channel Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNVpgDemSlc1_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "OCNVpgDemSlc1_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"OCN TXPP Per STS Multiplexing Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNVpgDemSlc0_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "OCNVpgDemSlc0_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"OCN TXPP Per STS Multiplexing Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNVpgCtlSlc1_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "OCNVpgCtlSlc1_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"OCN VTTU Pointer Generator Per Channel Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNVpgCtlSlc0_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "OCNVpgCtlSlc0_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"OCN VTTU Pointer Generator Per Channel Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNSpgCtlSlc1_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "OCNSpgCtlSlc1_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"OCN STS Pointer Generator Per Channel Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNSpgCtlSlc0_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "OCNSpgCtlSlc0_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"OCN STS Pointer Generator Per Channel Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNTfmCtlSlc1_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "OCNTfmCtlSlc1_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"OCN Tx Framer Per Channel Control\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNTfmCtlSlc0_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "OCNTfmCtlSlc0_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"OCN Tx Framer Per Channel Control\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNTfmJ0Slc1_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "OCNTfmJ0Slc1_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"OCN Tx J0 Insertion Buffer\" SliceId 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNTfmJ0Slc0_ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "OCNTfmJ0Slc0_ParDis"
            
            def description(self):
                return "Disable parity For RAM Control \"OCN Tx J0 Insertion Buffer\" SliceId 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DDRTxEccNonCorErr_ErrDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._DDRTxEccNonCorErr_ErrDis()
            allFields["DDRTxEccCorErr_ErrDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._DDRTxEccCorErr_ErrDis()
            allFields["DDRRxEccNonCorErr_ErrDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._DDRRxEccNonCorErr_ErrDis()
            allFields["DDRRxEccCorErr_ErrDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._DDRRxEccCorErr_ErrDis()
            allFields["CLASoftwaveCtl_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._CLASoftwaveCtl_ParDis()
            allFields["CDRPweLookupCtl_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._CDRPweLookupCtl_ParDis()
            allFields["CLAPerGrpEnbCtl_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._CLAPerGrpEnbCtl_ParDis()
            allFields["CLAMefOverMplsCtl_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._CLAMefOverMplsCtl_ParDis()
            allFields["CLAPerPweTypeCtl_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._CLAPerPweTypeCtl_ParDis()
            allFields["CLAHbceLookupInfCtl_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._CLAHbceLookupInfCtl_ParDis()
            allFields["CLAHbceHashTab1Ctl_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._CLAHbceHashTab1Ctl_ParDis()
            allFields["CLAHbceHashTab0Ctl_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._CLAHbceHashTab0Ctl_ParDis()
            allFields["PWETxHspwGrpProCtl_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PWETxHspwGrpProCtl_ParDis()
            allFields["PWETxUpsaGrpEnbCtl_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PWETxUpsaGrpEnbCtl_ParDis()
            allFields["PWETxUpsaHspwModCtl_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PWETxUpsaHspwModCtl_ParDis()
            allFields["PWETxHspwLabelCtl_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PWETxHspwLabelCtl_ParDis()
            allFields["PWETxHdrRtpSsrcCtl_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PWETxHdrRtpSsrcCtl_ParDis()
            allFields["PWETxEthHdrLenCtl_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PWETxEthHdrLenCtl_ParDis()
            allFields["PWETxEthHdrValCtl_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PWETxEthHdrValCtl_ParDis()
            allFields["PWETxEnbCtl_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PWETxEnbCtl_ParDis()
            allFields["PDAddrCrc_ErrDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PDAddrCrc_ErrDis()
            allFields["PDATdmMode_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PDATdmMode_ParDis()
            allFields["PDAJitbuf_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PDAJitbuf_ParDis()
            allFields["PDAReor_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PDAReor_ParDis()
            allFields["PLAPaylSizeSlc1_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PLAPaylSizeSlc1_ParDis()
            allFields["PLAPaylSizeSlc0_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PLAPaylSizeSlc0_ParDis()
            allFields["CDRACRTimingCtrlSlc1_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._CDRACRTimingCtrlSlc1_ParDis()
            allFields["CDRVTTimingCtrlSlc1_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._CDRVTTimingCtrlSlc1_ParDis()
            allFields["CDRSTSTimingCtrlSlc1_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._CDRSTSTimingCtrlSlc1_ParDis()
            allFields["CDRACRTimingCtrlSlc0_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._CDRACRTimingCtrlSlc0_ParDis()
            allFields["CDRVTTimingCtrlSlc0_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._CDRVTTimingCtrlSlc0_ParDis()
            allFields["CDRSTSTimingCtrlSlc0_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._CDRSTSTimingCtrlSlc0_ParDis()
            allFields["DeMapChlCtrlSlc1_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._DeMapChlCtrlSlc1_ParDis()
            allFields["DeMapChlCtrlSlc0_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._DeMapChlCtrlSlc0_ParDis()
            allFields["MAPLineCtrlSlc1_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._MAPLineCtrlSlc1_ParDis()
            allFields["MAPChlCtrlSlc1_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._MAPChlCtrlSlc1_ParDis()
            allFields["MAPLineCtrlSlc0_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._MAPLineCtrlSlc0_ParDis()
            allFields["MAPChlCtrlSlc0_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._MAPChlCtrlSlc0_ParDis()
            allFields["PDHVTAsyncMapCtrlSlc1_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PDHVTAsyncMapCtrlSlc1_ParDis()
            allFields["PDHSTSVTMapCtrlSlc1_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PDHSTSVTMapCtrlSlc1_ParDis()
            allFields["PDHTxM23E23TraceSlc1_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PDHTxM23E23TraceSlc1_ParDis()
            allFields["PDHTxM23E23CtrlSlc1_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PDHTxM23E23CtrlSlc1_ParDis()
            allFields["PDHTxM12E12CtrlSlc1_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PDHTxM12E12CtrlSlc1_ParDis()
            allFields["PDHTxDE1SigCtrlSlc1_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PDHTxDE1SigCtrlSlc1_ParDis()
            allFields["PDHTxDE1FrmCtrlSlc1_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PDHTxDE1FrmCtrlSlc1_ParDis()
            allFields["PDHRxDE1FrmCtrlSlc1_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PDHRxDE1FrmCtrlSlc1_ParDis()
            allFields["PDHRxM12E12CtrlSlc1_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PDHRxM12E12CtrlSlc1_ParDis()
            allFields["PDHRxDS3E3OHCtrlSlc1_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PDHRxDS3E3OHCtrlSlc1_ParDis()
            allFields["PDHRxM23E23CtrlSlc1_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PDHRxM23E23CtrlSlc1_ParDis()
            allFields["PDHSTSVTDeMapCtrlSlc1_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PDHSTSVTDeMapCtrlSlc1_ParDis()
            allFields["PDHMuxCtrlSlc1_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PDHMuxCtrlSlc1_ParDis()
            allFields["PDHVTAsyncMapCtrlSlc0_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PDHVTAsyncMapCtrlSlc0_ParDis()
            allFields["PDHSTSVTMapCtrlSlc0_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PDHSTSVTMapCtrlSlc0_ParDis()
            allFields["PDHTxM23E23TraceSlc0_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PDHTxM23E23TraceSlc0_ParDis()
            allFields["PDHTxM23E23CtrlSlc0_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PDHTxM23E23CtrlSlc0_ParDis()
            allFields["PDHTxM12E12CtrlSlc0_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PDHTxM12E12CtrlSlc0_ParDis()
            allFields["PDHTxDE1SigCtrlSlc0_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PDHTxDE1SigCtrlSlc0_ParDis()
            allFields["PDHTxDE1FrmCtrlSlc0_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PDHTxDE1FrmCtrlSlc0_ParDis()
            allFields["PDHRxDE1FrmCtrlSlc0_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PDHRxDE1FrmCtrlSlc0_ParDis()
            allFields["PDHRxM12E12CtrlSlc0_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PDHRxM12E12CtrlSlc0_ParDis()
            allFields["PDHRxDS3E3OHCtrlSlc0_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PDHRxDS3E3OHCtrlSlc0_ParDis()
            allFields["PDHRxM23E23CtrlSlc0_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PDHRxM23E23CtrlSlc0_ParDis()
            allFields["PDHSTSVTDeMapCtrlSlc0_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PDHSTSVTDeMapCtrlSlc0_ParDis()
            allFields["PDHMuxCtrl_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._PDHMuxCtrl_ParDis()
            allFields["POHWrCtrl_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._POHWrCtrl_ParDis()
            allFields["POHWrTrsh_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._POHWrTrsh_ParDis()
            allFields["POHTerCtrlLo_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._POHTerCtrlLo_ParDis()
            allFields["POHTerCtrlHi_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._POHTerCtrlHi_ParDis()
            allFields["POHCpeVtCtr_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._POHCpeVtCtr_ParDis()
            allFields["POHCpeStsCtr_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._POHCpeStsCtr_ParDis()
            allFields["OCNTohCtlSlc1_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._OCNTohCtlSlc1_ParDis()
            allFields["OCNSpiCtlSlc1_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._OCNSpiCtlSlc1_ParDis()
            allFields["OCNSpiCtlSlc0_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._OCNSpiCtlSlc0_ParDis()
            allFields["OCNVpiDemSlc1_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._OCNVpiDemSlc1_ParDis()
            allFields["OCNVpiDemSlc0_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._OCNVpiDemSlc0_ParDis()
            allFields["OCNVpiCtlSlc1_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._OCNVpiCtlSlc1_ParDis()
            allFields["OCNVpiCtlSlc0_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._OCNVpiCtlSlc0_ParDis()
            allFields["OCNVpgDemSlc1_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._OCNVpgDemSlc1_ParDis()
            allFields["OCNVpgDemSlc0_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._OCNVpgDemSlc0_ParDis()
            allFields["OCNVpgCtlSlc1_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._OCNVpgCtlSlc1_ParDis()
            allFields["OCNVpgCtlSlc0_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._OCNVpgCtlSlc0_ParDis()
            allFields["OCNSpgCtlSlc1_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._OCNSpgCtlSlc1_ParDis()
            allFields["OCNSpgCtlSlc0_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._OCNSpgCtlSlc0_ParDis()
            allFields["OCNTfmCtlSlc1_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._OCNTfmCtlSlc1_ParDis()
            allFields["OCNTfmCtlSlc0_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._OCNTfmCtlSlc0_ParDis()
            allFields["OCNTfmJ0Slc1_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._OCNTfmJ0Slc1_ParDis()
            allFields["OCNTfmJ0Slc0_ParDis"] = _AF6CNC0011_RD_GLB._RAM_Parity_Disable_Control._OCNTfmJ0Slc0_ParDis()
            return allFields

    class _RAM_Parity_Error_Sticky(AtRegister.AtRegister):
        def name(self):
            return "RAM parity Error Sticky"
    
        def description(self):
            return "This register configures disable parity for internal RAM"
            
        def width(self):
            return 96
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x112 + Wrd"
            
        def startAddress(self):
            return 0x00000112
            
        def endAddress(self):
            return 0xffffffff

        class _DDRTxEccNonCorErr_ErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 86
                
            def startBit(self):
                return 86
        
            def name(self):
                return "DDRTxEccNonCorErr_ErrStk"
            
            def description(self):
                return "Error Sticky ECC non-correctable error for Tx DDR"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _DDRTxEccCorErr_ErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 85
                
            def startBit(self):
                return 85
        
            def name(self):
                return "DDRTxEccCorErr_ErrStk"
            
            def description(self):
                return "Error Sticky ECC correctable error for Tx DDR"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _DDRRxEccNonCorErr_ErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 84
                
            def startBit(self):
                return 84
        
            def name(self):
                return "DDRRxEccNonCorErr_ErrStk"
            
            def description(self):
                return "Error Sticky ECC non-correctable error for Rx DDR"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _DDRRxEccCorErr_ErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 83
                
            def startBit(self):
                return 83
        
            def name(self):
                return "DDRRxEccCorErr_ErrStk"
            
            def description(self):
                return "Error Sticky ECC correctable error for Rx DDR"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _CLASoftwaveCtl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 82
                
            def startBit(self):
                return 82
        
            def name(self):
                return "CLASoftwaveCtl_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"Softwave Control\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRPweLookupCtl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 81
                
            def startBit(self):
                return 81
        
            def name(self):
                return "CDRPweLookupCtl_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"CDR Pseudowire Look Up Control\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAPerGrpEnbCtl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 80
                
            def startBit(self):
                return 80
        
            def name(self):
                return "CLAPerGrpEnbCtl_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"Classify Per Group Enable Control\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAMefOverMplsCtl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 79
                
            def startBit(self):
                return 79
        
            def name(self):
                return "CLAMefOverMplsCtl_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"Classify Pseudowire MEF over MPLS Control\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAPerPweTypeCtl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 78
                
            def startBit(self):
                return 78
        
            def name(self):
                return "CLAPerPweTypeCtl_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"Classify Per Pseudowire Type Control\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHbceLookupInfCtl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 77
                
            def startBit(self):
                return 77
        
            def name(self):
                return "CLAHbceLookupInfCtl_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"Classify HBCE Looking Up Information Control\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHbceHashTab1Ctl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 76
                
            def startBit(self):
                return 76
        
            def name(self):
                return "CLAHbceHashTab1Ctl_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"Classify HBCE Hashing Table Control\" PageID 1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHbceHashTab0Ctl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 75
                
            def startBit(self):
                return 75
        
            def name(self):
                return "CLAHbceHashTab0Ctl_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"Classify HBCE Hashing Table Control\" PageID 0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PWETxHspwGrpProCtl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 74
                
            def startBit(self):
                return 74
        
            def name(self):
                return "PWETxHspwGrpProCtl_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"Pseudowire Transmit HSPW Group Protection Control\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PWETxUpsaGrpEnbCtl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 73
                
            def startBit(self):
                return 73
        
            def name(self):
                return "PWETxUpsaGrpEnbCtl_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"Pseudowire Transmit UPSR Group Enable Control\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PWETxUpsaHspwModCtl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 72
                
            def startBit(self):
                return 72
        
            def name(self):
                return "PWETxUpsaHspwModCtl_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"Pseudowire Transmit UPSR and HSPW mode Control\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PWETxHspwLabelCtl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 71
                
            def startBit(self):
                return 71
        
            def name(self):
                return "PWETxHspwLabelCtl_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"Pseudowire Transmit HSPW Label Control\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PWETxHdrRtpSsrcCtl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 70
                
            def startBit(self):
                return 70
        
            def name(self):
                return "PWETxHdrRtpSsrcCtl_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"Pseudowire Transmit Header RTP SSRC Value Control \""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PWETxEthHdrLenCtl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 69
                
            def startBit(self):
                return 69
        
            def name(self):
                return "PWETxEthHdrLenCtl_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"Pseudowire Transmit Ethernet Header Length Control\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PWETxEthHdrValCtl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 68
                
            def startBit(self):
                return 68
        
            def name(self):
                return "PWETxEthHdrValCtl_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"Pseudowire Transmit Ethernet Header Value Control\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PWETxEnbCtl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 67
                
            def startBit(self):
                return 67
        
            def name(self):
                return "PWETxEnbCtl_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"Pseudowire Transmit Enable Control\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAddrCrc_ErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 66
                
            def startBit(self):
                return 66
        
            def name(self):
                return "PDAddrCrc_ErrStk"
            
            def description(self):
                return "Sticky check DDR CRC error"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDATdmMode_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 65
                
            def startBit(self):
                return 65
        
            def name(self):
                return "PDATdmMode_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"Pseudowire PDA TDM Mode Control\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAJitbuf_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 64
                
            def startBit(self):
                return 64
        
            def name(self):
                return "PDAJitbuf_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"Pseudowire PDA Jitter Buffer Control\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAReor_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 63
        
            def name(self):
                return "PDAReor_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"Pseudowire PDA Reorder Control\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PLAPaylSizeSlc1_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 62
                
            def startBit(self):
                return 62
        
            def name(self):
                return "PLAPaylSizeSlc1_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"Thalassa PDHPW Payload Assemble Payload Size Control\"SliceId 1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PLAPaylSizeSlc0_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 61
                
            def startBit(self):
                return 61
        
            def name(self):
                return "PLAPaylSizeSlc0_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"Thalassa PDHPW Payload Assemble Payload Size Control\"SliceId 0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRACRTimingCtrlSlc1_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 60
                
            def startBit(self):
                return 60
        
            def name(self):
                return "CDRACRTimingCtrlSlc1_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"CDR ACR Engine Timing control\" SliceId 1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRVTTimingCtrlSlc1_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 59
                
            def startBit(self):
                return 59
        
            def name(self):
                return "CDRVTTimingCtrlSlc1_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"CDR VT Timing control\" SliceId 1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRSTSTimingCtrlSlc1_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 58
                
            def startBit(self):
                return 58
        
            def name(self):
                return "CDRSTSTimingCtrlSlc1_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"CDR STS Timing control\" SliceId 1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRACRTimingCtrlSlc0_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 57
                
            def startBit(self):
                return 57
        
            def name(self):
                return "CDRACRTimingCtrlSlc0_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"CDR ACR Engine Timing control\" SliceId 0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRVTTimingCtrlSlc0_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 56
                
            def startBit(self):
                return 56
        
            def name(self):
                return "CDRVTTimingCtrlSlc0_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"CDR VT Timing control\" SliceId 0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRSTSTimingCtrlSlc0_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 55
                
            def startBit(self):
                return 55
        
            def name(self):
                return "CDRSTSTimingCtrlSlc0_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"CDR STS Timing control\" SliceId 0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _DeMapChlCtrlSlc1_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 54
                
            def startBit(self):
                return 54
        
            def name(self):
                return "DeMapChlCtrlSlc1_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"DEMAP Thalassa Demap Channel Control\" SliceId 1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _DeMapChlCtrlSlc0_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 53
                
            def startBit(self):
                return 53
        
            def name(self):
                return "DeMapChlCtrlSlc0_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"DEMAP Thalassa Demap Channel Control\" SliceId 0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPLineCtrlSlc1_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 52
                
            def startBit(self):
                return 52
        
            def name(self):
                return "MAPLineCtrlSlc1_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"MAP Thalassa Map Line Control\" SliceId 1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPChlCtrlSlc1_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 51
                
            def startBit(self):
                return 51
        
            def name(self):
                return "MAPChlCtrlSlc1_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"MAP Thalassa Map Channel Control\" SliceId 1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPLineCtrlSlc0_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 50
                
            def startBit(self):
                return 50
        
            def name(self):
                return "MAPLineCtrlSlc0_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"MAP Thalassa Map Line Control\" SliceId 0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPChlCtrlSlc0_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 49
                
            def startBit(self):
                return 49
        
            def name(self):
                return "MAPChlCtrlSlc0_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"MAP Thalassa Map Channel Control\" SliceId 0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHVTAsyncMapCtrlSlc1_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 48
                
            def startBit(self):
                return 48
        
            def name(self):
                return "PDHVTAsyncMapCtrlSlc1_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"PDH VT Async Map Control\" SliceId 1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHSTSVTMapCtrlSlc1_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 47
        
            def name(self):
                return "PDHSTSVTMapCtrlSlc1_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"PDH STS/VT Map Control\" SliceId 1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM23E23TraceSlc1_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 46
                
            def startBit(self):
                return 46
        
            def name(self):
                return "PDHTxM23E23TraceSlc1_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"PDH TxM23E23 E3g832 Trace Byte\" SliceId 1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM23E23CtrlSlc1_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 45
                
            def startBit(self):
                return 45
        
            def name(self):
                return "PDHTxM23E23CtrlSlc1_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"PDH TxM23E23 Control\" SliceId 1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM12E12CtrlSlc1_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 44
                
            def startBit(self):
                return 44
        
            def name(self):
                return "PDHTxM12E12CtrlSlc1_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"PDH TxM12E12 Control\" SliceId 1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxDE1SigCtrlSlc1_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 43
                
            def startBit(self):
                return 43
        
            def name(self):
                return "PDHTxDE1SigCtrlSlc1_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"PDH DS1/E1/J1 Tx Framer Signalling Control\" SliceId 1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxDE1FrmCtrlSlc1_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 42
                
            def startBit(self):
                return 42
        
            def name(self):
                return "PDHTxDE1FrmCtrlSlc1_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"PDH DS1/E1/J1 Tx Framer Control\" SliceId 1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxDE1FrmCtrlSlc1_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 41
                
            def startBit(self):
                return 41
        
            def name(self):
                return "PDHRxDE1FrmCtrlSlc1_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"PDH DS1/E1/J1 Rx Framer Control\" SliceId 1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxM12E12CtrlSlc1_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 40
                
            def startBit(self):
                return 40
        
            def name(self):
                return "PDHRxM12E12CtrlSlc1_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"PDH RxM12E12 Control\" SliceId 1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxDS3E3OHCtrlSlc1_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 39
                
            def startBit(self):
                return 39
        
            def name(self):
                return "PDHRxDS3E3OHCtrlSlc1_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"PDH RxDS3E3 OH Pro Control\" SliceId 1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxM23E23CtrlSlc1_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 38
                
            def startBit(self):
                return 38
        
            def name(self):
                return "PDHRxM23E23CtrlSlc1_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"PDH RxM23E23 Control\" SliceId 1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHSTSVTDeMapCtrlSlc1_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 37
                
            def startBit(self):
                return 37
        
            def name(self):
                return "PDHSTSVTDeMapCtrlSlc1_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"PDH STS/VT Demap Control\" SliceId 1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHMuxCtrlSlc1_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 36
                
            def startBit(self):
                return 36
        
            def name(self):
                return "PDHMuxCtrlSlc1_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"PDH DS1/E1/J1 Rx Framer Mux Control\" SliceId 1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHVTAsyncMapCtrl_Slc0ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 35
                
            def startBit(self):
                return 35
        
            def name(self):
                return "PDHVTAsyncMapCtrl_Slc0ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"PDH VT Async Map Control\" SliceId 0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHSTSVTMapCtrl_Slc0ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 34
                
            def startBit(self):
                return 34
        
            def name(self):
                return "PDHSTSVTMapCtrl_Slc0ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"PDH STS/VT Map Control\" SliceId 0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM23E23Trace_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 33
                
            def startBit(self):
                return 33
        
            def name(self):
                return "PDHTxM23E23Trace_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"PDH TxM23E23 E3g832 Trace Byte\" SliceId 0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM23E23Ctrl_Slc0ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 32
                
            def startBit(self):
                return 32
        
            def name(self):
                return "PDHTxM23E23Ctrl_Slc0ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"PDH TxM23E23 Control\" SliceId 0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM12E12Ctrl_Slc0ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "PDHTxM12E12Ctrl_Slc0ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"PDH TxM12E12 Control\" SliceId 0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxDE1SigCtrl_Slc0ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "PDHTxDE1SigCtrl_Slc0ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"PDH DS1/E1/J1 Tx Framer Signalling Control\" SliceId 0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxDE1FrmCtrl_Slc0ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "PDHTxDE1FrmCtrl_Slc0ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"PDH DS1/E1/J1 Tx Framer Control\" SliceId 0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxDE1FrmCtrl_Slc0ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "PDHRxDE1FrmCtrl_Slc0ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"PDH DS1/E1/J1 Rx Framer Control\" SliceId 0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxM12E12Ctrl_Slc0ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "PDHRxM12E12Ctrl_Slc0ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"PDH RxM12E12 Control\" SliceId 0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxDS3E3OHCtrl_Slc0ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "PDHRxDS3E3OHCtrl_Slc0ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"PDH RxDS3E3 OH Pro Control\" SliceId 0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxM23E23Ctrl_Slc0ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "PDHRxM23E23Ctrl_Slc0ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"PDH RxM23E23 Control\" SliceId 0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHSTSVTDeMapCtrl_Slc0ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "PDHSTSVTDeMapCtrl_Slc0ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"PDH STS/VT Demap Control\" SliceId 0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHMuxCtrl_Slc0ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "PDHMuxCtrl_Slc0ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"PDH DS1/E1/J1 Rx Framer Mux Control\" SliceId 0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _POHWrCtrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "POHWrCtrl_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"POH BER Control VT/DSN\", \"POH BER Control STS/TU3\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _POHWrTrsh_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "POHWrTrsh_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"POH BER Threshold 2\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _POHTerCtrlLo_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "POHTerCtrlLo_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"POH Termintate Insert Control VT/TU3\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _POHTerCtrlHi_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "POHTerCtrlHi_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"POH Termintate Insert Control STS\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _POHCpeVtCtr_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "POHCpeVtCtr_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"POH CPE VT Control Register\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _POHCpeStsCtr_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "POHCpeStsCtr_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"POH CPE STS/TU3 Control Register\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNTohCtlSlc1_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "OCNTohCtlSlc1_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"OCN TOH Monitoring Per Channel Control\" SliceId 1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNSpiCtlSlc1_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "OCNSpiCtlSlc1_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"OCN STS Pointer Interpreter Per Channel Control\" SliceId 1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNSpiCtlSlc0_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "OCNSpiCtlSlc0_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"OCN STS Pointer Interpreter Per Channel Control\" SliceId 0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNVpiDemSlc1_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "OCNVpiDemSlc1_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"OCN RXPP Per STS payload Control\" SliceId 1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNVpiDemSlc0_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "OCNVpiDemSlc0_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"OCN RXPP Per STS payload Control\" SliceId 0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNVpiCtlSlc1_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "OCNVpiCtlSlc1_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"OCN VTTU Pointer Interpreter Per Channel Control\" SliceId 1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNVpiCtlSlc0_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "OCNVpiCtlSlc0_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"OCN VTTU Pointer Interpreter Per Channel Control\" SliceId 0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNVpgDemSlc1_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "OCNVpgDemSlc1_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"OCN TXPP Per STS Multiplexing Control\" SliceId 1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNVpgDemSlc0_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "OCNVpgDemSlc0_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"OCN TXPP Per STS Multiplexing Control\" SliceId 0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNVpgCtlSlc1_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "OCNVpgCtlSlc1_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"OCN VTTU Pointer Generator Per Channel Control\" SliceId 1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNVpgCtlSlc0_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "OCNVpgCtlSlc0_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"OCN VTTU Pointer Generator Per Channel Control\" SliceId 0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNSpgCtlSlc1_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "OCNSpgCtlSlc1_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"OCN STS Pointer Generator Per Channel Control\" SliceId 1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNSpgCtlSlc0_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "OCNSpgCtlSlc0_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"OCN STS Pointer Generator Per Channel Control\" SliceId 0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNTfmCtlSlc1_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "OCNTfmCtlSlc1_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"OCN Tx Framer Per Channel Control\" SliceId 1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNTfmCtlSlc0_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "OCNTfmCtlSlc0_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"OCN Tx Framer Per Channel Control\" SliceId 0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNTfmJ0Slc1_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "OCNTfmJ0Slc1_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"OCN Tx J0 Insertion Buffer\" SliceId 1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNTfmJ0Slc0_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "OCNTfmJ0Slc0_ParErrStk"
            
            def description(self):
                return "Parity Error Sticky For RAM Control \"OCN Tx J0 Insertion Buffer\" SliceId 0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DDRTxEccNonCorErr_ErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._DDRTxEccNonCorErr_ErrStk()
            allFields["DDRTxEccCorErr_ErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._DDRTxEccCorErr_ErrStk()
            allFields["DDRRxEccNonCorErr_ErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._DDRRxEccNonCorErr_ErrStk()
            allFields["DDRRxEccCorErr_ErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._DDRRxEccCorErr_ErrStk()
            allFields["CLASoftwaveCtl_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._CLASoftwaveCtl_ParErrStk()
            allFields["CDRPweLookupCtl_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._CDRPweLookupCtl_ParErrStk()
            allFields["CLAPerGrpEnbCtl_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._CLAPerGrpEnbCtl_ParErrStk()
            allFields["CLAMefOverMplsCtl_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._CLAMefOverMplsCtl_ParErrStk()
            allFields["CLAPerPweTypeCtl_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._CLAPerPweTypeCtl_ParErrStk()
            allFields["CLAHbceLookupInfCtl_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._CLAHbceLookupInfCtl_ParErrStk()
            allFields["CLAHbceHashTab1Ctl_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._CLAHbceHashTab1Ctl_ParErrStk()
            allFields["CLAHbceHashTab0Ctl_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._CLAHbceHashTab0Ctl_ParErrStk()
            allFields["PWETxHspwGrpProCtl_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PWETxHspwGrpProCtl_ParErrStk()
            allFields["PWETxUpsaGrpEnbCtl_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PWETxUpsaGrpEnbCtl_ParErrStk()
            allFields["PWETxUpsaHspwModCtl_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PWETxUpsaHspwModCtl_ParErrStk()
            allFields["PWETxHspwLabelCtl_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PWETxHspwLabelCtl_ParErrStk()
            allFields["PWETxHdrRtpSsrcCtl_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PWETxHdrRtpSsrcCtl_ParErrStk()
            allFields["PWETxEthHdrLenCtl_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PWETxEthHdrLenCtl_ParErrStk()
            allFields["PWETxEthHdrValCtl_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PWETxEthHdrValCtl_ParErrStk()
            allFields["PWETxEnbCtl_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PWETxEnbCtl_ParErrStk()
            allFields["PDAddrCrc_ErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PDAddrCrc_ErrStk()
            allFields["PDATdmMode_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PDATdmMode_ParErrStk()
            allFields["PDAJitbuf_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PDAJitbuf_ParErrStk()
            allFields["PDAReor_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PDAReor_ParErrStk()
            allFields["PLAPaylSizeSlc1_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PLAPaylSizeSlc1_ParErrStk()
            allFields["PLAPaylSizeSlc0_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PLAPaylSizeSlc0_ParErrStk()
            allFields["CDRACRTimingCtrlSlc1_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._CDRACRTimingCtrlSlc1_ParErrStk()
            allFields["CDRVTTimingCtrlSlc1_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._CDRVTTimingCtrlSlc1_ParErrStk()
            allFields["CDRSTSTimingCtrlSlc1_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._CDRSTSTimingCtrlSlc1_ParErrStk()
            allFields["CDRACRTimingCtrlSlc0_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._CDRACRTimingCtrlSlc0_ParErrStk()
            allFields["CDRVTTimingCtrlSlc0_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._CDRVTTimingCtrlSlc0_ParErrStk()
            allFields["CDRSTSTimingCtrlSlc0_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._CDRSTSTimingCtrlSlc0_ParErrStk()
            allFields["DeMapChlCtrlSlc1_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._DeMapChlCtrlSlc1_ParErrStk()
            allFields["DeMapChlCtrlSlc0_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._DeMapChlCtrlSlc0_ParErrStk()
            allFields["MAPLineCtrlSlc1_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._MAPLineCtrlSlc1_ParErrStk()
            allFields["MAPChlCtrlSlc1_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._MAPChlCtrlSlc1_ParErrStk()
            allFields["MAPLineCtrlSlc0_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._MAPLineCtrlSlc0_ParErrStk()
            allFields["MAPChlCtrlSlc0_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._MAPChlCtrlSlc0_ParErrStk()
            allFields["PDHVTAsyncMapCtrlSlc1_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PDHVTAsyncMapCtrlSlc1_ParErrStk()
            allFields["PDHSTSVTMapCtrlSlc1_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PDHSTSVTMapCtrlSlc1_ParErrStk()
            allFields["PDHTxM23E23TraceSlc1_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PDHTxM23E23TraceSlc1_ParErrStk()
            allFields["PDHTxM23E23CtrlSlc1_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PDHTxM23E23CtrlSlc1_ParErrStk()
            allFields["PDHTxM12E12CtrlSlc1_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PDHTxM12E12CtrlSlc1_ParErrStk()
            allFields["PDHTxDE1SigCtrlSlc1_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PDHTxDE1SigCtrlSlc1_ParErrStk()
            allFields["PDHTxDE1FrmCtrlSlc1_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PDHTxDE1FrmCtrlSlc1_ParErrStk()
            allFields["PDHRxDE1FrmCtrlSlc1_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PDHRxDE1FrmCtrlSlc1_ParErrStk()
            allFields["PDHRxM12E12CtrlSlc1_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PDHRxM12E12CtrlSlc1_ParErrStk()
            allFields["PDHRxDS3E3OHCtrlSlc1_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PDHRxDS3E3OHCtrlSlc1_ParErrStk()
            allFields["PDHRxM23E23CtrlSlc1_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PDHRxM23E23CtrlSlc1_ParErrStk()
            allFields["PDHSTSVTDeMapCtrlSlc1_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PDHSTSVTDeMapCtrlSlc1_ParErrStk()
            allFields["PDHMuxCtrlSlc1_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PDHMuxCtrlSlc1_ParErrStk()
            allFields["PDHVTAsyncMapCtrl_Slc0ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PDHVTAsyncMapCtrl_Slc0ParErrStk()
            allFields["PDHSTSVTMapCtrl_Slc0ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PDHSTSVTMapCtrl_Slc0ParErrStk()
            allFields["PDHTxM23E23Trace_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PDHTxM23E23Trace_ParErrStk()
            allFields["PDHTxM23E23Ctrl_Slc0ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PDHTxM23E23Ctrl_Slc0ParErrStk()
            allFields["PDHTxM12E12Ctrl_Slc0ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PDHTxM12E12Ctrl_Slc0ParErrStk()
            allFields["PDHTxDE1SigCtrl_Slc0ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PDHTxDE1SigCtrl_Slc0ParErrStk()
            allFields["PDHTxDE1FrmCtrl_Slc0ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PDHTxDE1FrmCtrl_Slc0ParErrStk()
            allFields["PDHRxDE1FrmCtrl_Slc0ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PDHRxDE1FrmCtrl_Slc0ParErrStk()
            allFields["PDHRxM12E12Ctrl_Slc0ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PDHRxM12E12Ctrl_Slc0ParErrStk()
            allFields["PDHRxDS3E3OHCtrl_Slc0ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PDHRxDS3E3OHCtrl_Slc0ParErrStk()
            allFields["PDHRxM23E23Ctrl_Slc0ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PDHRxM23E23Ctrl_Slc0ParErrStk()
            allFields["PDHSTSVTDeMapCtrl_Slc0ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PDHSTSVTDeMapCtrl_Slc0ParErrStk()
            allFields["PDHMuxCtrl_Slc0ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._PDHMuxCtrl_Slc0ParErrStk()
            allFields["POHWrCtrl_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._POHWrCtrl_ParErrStk()
            allFields["POHWrTrsh_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._POHWrTrsh_ParErrStk()
            allFields["POHTerCtrlLo_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._POHTerCtrlLo_ParErrStk()
            allFields["POHTerCtrlHi_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._POHTerCtrlHi_ParErrStk()
            allFields["POHCpeVtCtr_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._POHCpeVtCtr_ParErrStk()
            allFields["POHCpeStsCtr_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._POHCpeStsCtr_ParErrStk()
            allFields["OCNTohCtlSlc1_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._OCNTohCtlSlc1_ParErrStk()
            allFields["OCNSpiCtlSlc1_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._OCNSpiCtlSlc1_ParErrStk()
            allFields["OCNSpiCtlSlc0_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._OCNSpiCtlSlc0_ParErrStk()
            allFields["OCNVpiDemSlc1_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._OCNVpiDemSlc1_ParErrStk()
            allFields["OCNVpiDemSlc0_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._OCNVpiDemSlc0_ParErrStk()
            allFields["OCNVpiCtlSlc1_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._OCNVpiCtlSlc1_ParErrStk()
            allFields["OCNVpiCtlSlc0_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._OCNVpiCtlSlc0_ParErrStk()
            allFields["OCNVpgDemSlc1_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._OCNVpgDemSlc1_ParErrStk()
            allFields["OCNVpgDemSlc0_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._OCNVpgDemSlc0_ParErrStk()
            allFields["OCNVpgCtlSlc1_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._OCNVpgCtlSlc1_ParErrStk()
            allFields["OCNVpgCtlSlc0_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._OCNVpgCtlSlc0_ParErrStk()
            allFields["OCNSpgCtlSlc1_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._OCNSpgCtlSlc1_ParErrStk()
            allFields["OCNSpgCtlSlc0_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._OCNSpgCtlSlc0_ParErrStk()
            allFields["OCNTfmCtlSlc1_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._OCNTfmCtlSlc1_ParErrStk()
            allFields["OCNTfmCtlSlc0_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._OCNTfmCtlSlc0_ParErrStk()
            allFields["OCNTfmJ0Slc1_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._OCNTfmJ0Slc1_ParErrStk()
            allFields["OCNTfmJ0Slc0_ParErrStk"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Sticky._OCNTfmJ0Slc0_ParErrStk()
            return allFields

    class _RAM_Parity_Error_Intr_Group(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data127_96"
    
        def description(self):
            return "This register is used to read HA dword4 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000121
            
        def endAddress(self):
            return 0xffffffff

        class _Wrd2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Wrd2"
            
            def description(self):
                return "Word2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0x0

        class _Wrd1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Wrd1"
            
            def description(self):
                return "Word1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0x0

        class _Wrd0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Wrd0"
            
            def description(self):
                return "Word0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0x0

        def _allFieldDicts(self):
            allFields = {}
            allFields["Wrd2"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Intr_Group._Wrd2()
            allFields["Wrd1"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Intr_Group._Wrd1()
            allFields["Wrd0"] = _AF6CNC0011_RD_GLB._RAM_Parity_Error_Intr_Group._Wrd0()
            return allFields

    class _rdha3_0_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit3_0 Control"
    
        def description(self):
            return "This register is used to send HA read address bit3_0 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00200 + HaAddr3_0"
            
        def startAddress(self):
            return 0x00000200
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr3_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr3_0"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr3_0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr3_0"] = _AF6CNC0011_RD_GLB._rdha3_0_control._ReadAddr3_0()
            return allFields

    class _rdha7_4_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit7_4 Control"
    
        def description(self):
            return "This register is used to send HA read address bit7_4 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00210 + HaAddr7_4"
            
        def startAddress(self):
            return 0x00000210
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr7_4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr7_4"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr7_4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr7_4"] = _AF6CNC0011_RD_GLB._rdha7_4_control._ReadAddr7_4()
            return allFields

    class _rdha11_8_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit11_8 Control"
    
        def description(self):
            return "This register is used to send HA read address bit11_8 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00220 + HaAddr11_8"
            
        def startAddress(self):
            return 0x00000220
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr11_8(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr11_8"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr11_8"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr11_8"] = _AF6CNC0011_RD_GLB._rdha11_8_control._ReadAddr11_8()
            return allFields

    class _rdha15_12_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit15_12 Control"
    
        def description(self):
            return "This register is used to send HA read address bit15_12 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00230 + HaAddr15_12"
            
        def startAddress(self):
            return 0x00000230
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr15_12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr15_12"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr15_12"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr15_12"] = _AF6CNC0011_RD_GLB._rdha15_12_control._ReadAddr15_12()
            return allFields

    class _rdha19_16_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit19_16 Control"
    
        def description(self):
            return "This register is used to send HA read address bit19_16 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00240 + HaAddr19_16"
            
        def startAddress(self):
            return 0x00000240
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr19_16(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr19_16"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr19_16"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr19_16"] = _AF6CNC0011_RD_GLB._rdha19_16_control._ReadAddr19_16()
            return allFields

    class _rdha23_20_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit23_20 Control"
    
        def description(self):
            return "This register is used to send HA read address bit23_20 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00250 + HaAddr23_20"
            
        def startAddress(self):
            return 0x00000250
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr23_20(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr23_20"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr23_20"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr23_20"] = _AF6CNC0011_RD_GLB._rdha23_20_control._ReadAddr23_20()
            return allFields

    class _rdha24data_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit24 and Data Control"
    
        def description(self):
            return "This register is used to send HA read address bit24 to HA engine to read data"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00260 + HaAddr24"
            
        def startAddress(self):
            return 0x00000260
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData31_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData31_0"
            
            def description(self):
                return "HA read data bit31_0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData31_0"] = _AF6CNC0011_RD_GLB._rdha24data_control._ReadHaData31_0()
            return allFields

    class _rdha_hold63_32(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data63_32"
    
        def description(self):
            return "This register is used to read HA dword2 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000270
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData63_32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData63_32"
            
            def description(self):
                return "HA read data bit63_32"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData63_32"] = _AF6CNC0011_RD_GLB._rdha_hold63_32._ReadHaData63_32()
            return allFields

    class _rdindr_hold95_64(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data95_64"
    
        def description(self):
            return "This register is used to read HA dword3 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000271
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData95_64(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData95_64"
            
            def description(self):
                return "HA read data bit95_64"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData95_64"] = _AF6CNC0011_RD_GLB._rdindr_hold95_64._ReadHaData95_64()
            return allFields

    class _rdindr_hold127_96(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data127_96"
    
        def description(self):
            return "This register is used to read HA dword4 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000272
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData127_96(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData127_96"
            
            def description(self):
                return "HA read data bit127_96"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData127_96"] = _AF6CNC0011_RD_GLB._rdindr_hold127_96._ReadHaData127_96()
            return allFields

    class _alm_temp(AtRegister.AtRegister):
        def name(self):
            return "Temperate Event"
    
        def description(self):
            return "This register report state change of Temperate status."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000000d0
            
        def endAddress(self):
            return 0xffffffff

        class _Temp_alarm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Temp_alarm"
            
            def description(self):
                return "Temperate change Event  1: Changed  0: Normal"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Temp_alarm"] = _AF6CNC0011_RD_GLB._alm_temp._Temp_alarm()
            return allFields

    class _PDA_DDR_CRC_Error_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "PDA DDR CRC Error Counter"
    
        def description(self):
            return "Count number of CRC error detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000142
            
        def endAddress(self):
            return 0xffffffff

        class _PDADDRCRCError(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDADDRCRCError"
            
            def description(self):
                return "This counter count the number of DDR CRC error."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDADDRCRCError"] = _AF6CNC0011_RD_GLB._PDA_DDR_CRC_Error_Counter_ro._PDADDRCRCError()
            return allFields

    class _PDA_DDR_CRC_Error_Counter_r2c(AtRegister.AtRegister):
        def name(self):
            return "PDA DDR CRC Error Counter"
    
        def description(self):
            return "Count number of CRC error detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000141
            
        def endAddress(self):
            return 0xffffffff

        class _PDADDRCRCError(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDADDRCRCError"
            
            def description(self):
                return "This counter count the number of DDR CRC error."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDADDRCRCError"] = _AF6CNC0011_RD_GLB._PDA_DDR_CRC_Error_Counter_r2c._PDADDRCRCError()
            return allFields

    class _PDA_DDR_errins_en_cfg(AtRegister.AtRegister):
        def name(self):
            return "PDA DDR Force Error Control"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000140
            
        def endAddress(self):
            return 0xffffffff

        class _PDADDRErrRate(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "PDADDRErrRate"
            
            def description(self):
                return "0: One-shot with number of errors; 1:Line rate"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDADDRErrThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDADDRErrThr"
            
            def description(self):
                return "Error Threshold in bit unit or the number of error in one-shot mode. Valid value from 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDADDRErrRate"] = _AF6CNC0011_RD_GLB._PDA_DDR_errins_en_cfg._PDADDRErrRate()
            allFields["PDADDRErrThr"] = _AF6CNC0011_RD_GLB._PDA_DDR_errins_en_cfg._PDADDRErrThr()
            return allFields

    class _Rx_DDR_ECC_Cor_Error_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Rx DDR ECC Correctable Error Counter"
    
        def description(self):
            return "Count number of Rx DDR ECC Correctable error detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000014a
            
        def endAddress(self):
            return 0xffffffff

        class _RxDDRECCCorError(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDDRECCCorError"
            
            def description(self):
                return "This counter count the number of RxDDR ECC Correctable error."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDDRECCCorError"] = _AF6CNC0011_RD_GLB._Rx_DDR_ECC_Cor_Error_Counter_ro._RxDDRECCCorError()
            return allFields

    class _Rx_DDR_ECC_Cor_Error_Counter_r2c(AtRegister.AtRegister):
        def name(self):
            return "Rx DDR ECC Correctable Error Counter"
    
        def description(self):
            return "Count number of Rx DDR ECC Correctable error detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000149
            
        def endAddress(self):
            return 0xffffffff

        class _RxDDRECCCorError(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDDRECCCorError"
            
            def description(self):
                return "This counter count the number of RxDDR ECC Correctable error."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDDRECCCorError"] = _AF6CNC0011_RD_GLB._Rx_DDR_ECC_Cor_Error_Counter_r2c._RxDDRECCCorError()
            return allFields

    class _Rx_DDR_ECC_UnCor_Error_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Rx DDR ECC UnCorrectable Error Counter"
    
        def description(self):
            return "Count number of Rx DDR ECC UnCorrectable error detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000014c
            
        def endAddress(self):
            return 0xffffffff

        class _RxDDRECCUnCorError(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDDRECCUnCorError"
            
            def description(self):
                return "This counter count the number of Rx DDR ECC unCorrectable error."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDDRECCUnCorError"] = _AF6CNC0011_RD_GLB._Rx_DDR_ECC_UnCor_Error_Counter_ro._RxDDRECCUnCorError()
            return allFields

    class _Rx_DDR_ECC_UnCor_Error_Counter_r2c(AtRegister.AtRegister):
        def name(self):
            return "Rx DDR ECC UnCorrectable Error Counter"
    
        def description(self):
            return "Count number of Rx DDR ECC UnCorrectable error detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000014b
            
        def endAddress(self):
            return 0xffffffff

        class _RxDDRECCUnCorError(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDDRECCUnCorError"
            
            def description(self):
                return "This counter count the number of Rx DDR ECC unCorrectable error."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDDRECCUnCorError"] = _AF6CNC0011_RD_GLB._Rx_DDR_ECC_UnCor_Error_Counter_r2c._RxDDRECCUnCorError()
            return allFields

    class _Rx_DDR_errins_en_cfg(AtRegister.AtRegister):
        def name(self):
            return "Rx DDR Force Error Control"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000148
            
        def endAddress(self):
            return 0xffffffff

        class _RxDDRErrMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "RxDDRErrMode"
            
            def description(self):
                return "0: Correctable (1bit); 1:UnCorrectable (2bit)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDDRErrRate(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "RxDDRErrRate"
            
            def description(self):
                return "0: One-shot with number of errors; 1:Line rate"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDDRErrThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDDRErrThr"
            
            def description(self):
                return "Error Threshold in bit unit or the number of error in one-shot mode. Valid value from 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDDRErrMode"] = _AF6CNC0011_RD_GLB._Rx_DDR_errins_en_cfg._RxDDRErrMode()
            allFields["RxDDRErrRate"] = _AF6CNC0011_RD_GLB._Rx_DDR_errins_en_cfg._RxDDRErrRate()
            allFields["RxDDRErrThr"] = _AF6CNC0011_RD_GLB._Rx_DDR_errins_en_cfg._RxDDRErrThr()
            return allFields

    class _Tx_DDR_ECC_Cor_Error_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Tx DDR ECC Correctable Error Counter"
    
        def description(self):
            return "Count number of Tx DDR ECC Correctable error detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000152
            
        def endAddress(self):
            return 0xffffffff

        class _TxDDRECCCorError(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDDRECCCorError"
            
            def description(self):
                return "This counter count the number of TxDDR ECC Correctable error."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDDRECCCorError"] = _AF6CNC0011_RD_GLB._Tx_DDR_ECC_Cor_Error_Counter_ro._TxDDRECCCorError()
            return allFields

    class _Tx_DDR_ECC_Cor_Error_Counter_r2c(AtRegister.AtRegister):
        def name(self):
            return "Tx DDR ECC Correctable Error Counter"
    
        def description(self):
            return "Count number of Tx DDR ECC Correctable error detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000151
            
        def endAddress(self):
            return 0xffffffff

        class _TxDDRECCCorError(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDDRECCCorError"
            
            def description(self):
                return "This counter count the number of TxDDR ECC Correctable error."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDDRECCCorError"] = _AF6CNC0011_RD_GLB._Tx_DDR_ECC_Cor_Error_Counter_r2c._TxDDRECCCorError()
            return allFields

    class _Tx_DDR_ECC_UnCor_Error_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Tx DDR ECC UnCorrectable Error Counter"
    
        def description(self):
            return "Count number of Rx DDR ECC UnCorrectable error detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000154
            
        def endAddress(self):
            return 0xffffffff

        class _TxDDRECCUnCorError(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDDRECCUnCorError"
            
            def description(self):
                return "This counter count the number of Tx DDR ECC unCorrectable error."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDDRECCUnCorError"] = _AF6CNC0011_RD_GLB._Tx_DDR_ECC_UnCor_Error_Counter_ro._TxDDRECCUnCorError()
            return allFields

    class _Tx_DDR_ECC_UnCor_Error_Counter_r2c(AtRegister.AtRegister):
        def name(self):
            return "Tx DDR ECC UnCorrectable Error Counter"
    
        def description(self):
            return "Count number of Rx DDR ECC UnCorrectable error detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000153
            
        def endAddress(self):
            return 0xffffffff

        class _TxDDRECCUnCorError(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDDRECCUnCorError"
            
            def description(self):
                return "This counter count the number of Tx DDR ECC unCorrectable error."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDDRECCUnCorError"] = _AF6CNC0011_RD_GLB._Tx_DDR_ECC_UnCor_Error_Counter_r2c._TxDDRECCUnCorError()
            return allFields

    class _Tx_DDR_errins_en_cfg(AtRegister.AtRegister):
        def name(self):
            return "Tx DDR Force Error Control"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000150
            
        def endAddress(self):
            return 0xffffffff

        class _TxDDRErrMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "TxDDRErrMode"
            
            def description(self):
                return "0: Correctable (1bit); 1:UnCorrectable (2bit)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDDRErrRate(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "TxDDRErrRate"
            
            def description(self):
                return "0: One-shot with number of errors; 1:Line rate"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDDRErrThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDDRErrThr"
            
            def description(self):
                return "Error Threshold in bit unit or the number of error in one-shot mode. Valid value from 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDDRErrMode"] = _AF6CNC0011_RD_GLB._Tx_DDR_errins_en_cfg._TxDDRErrMode()
            allFields["TxDDRErrRate"] = _AF6CNC0011_RD_GLB._Tx_DDR_errins_en_cfg._TxDDRErrRate()
            allFields["TxDDRErrThr"] = _AF6CNC0011_RD_GLB._Tx_DDR_errins_en_cfg._TxDDRErrThr()
            return allFields

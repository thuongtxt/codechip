import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0011_RD_PDA(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["jitbuf_ctrl"] = _AF6CNC0011_RD_PDA._jitbuf_ctrl()
        allRegisters["jitbuf_hot_cfg_ctrl1"] = _AF6CNC0011_RD_PDA._jitbuf_hot_cfg_ctrl1()
        allRegisters["jitbuf_hot_cfg_ctrl2"] = _AF6CNC0011_RD_PDA._jitbuf_hot_cfg_ctrl2()
        allRegisters["reorder_ctrl"] = _AF6CNC0011_RD_PDA._reorder_ctrl()
        allRegisters["tdm_mode_ctrl"] = _AF6CNC0011_RD_PDA._tdm_mode_ctrl()
        allRegisters["idle_code_ctrl"] = _AF6CNC0011_RD_PDA._idle_code_ctrl()
        allRegisters["oam_cpu_seg_info_sta"] = _AF6CNC0011_RD_PDA._oam_cpu_seg_info_sta()
        allRegisters["pw_pda_pw_prbs_gen_ctrl"] = _AF6CNC0011_RD_PDA._pw_pda_pw_prbs_gen_ctrl()
        allRegisters["pw_pda_pw_prbs_mon_ctrl"] = _AF6CNC0011_RD_PDA._pw_pda_pw_prbs_mon_ctrl()
        allRegisters["epar_ctrl"] = _AF6CNC0011_RD_PDA._epar_ctrl()
        return allRegisters

    class _jitbuf_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA Jitter Buffer Control"
    
        def description(self):
            return "This register sets jitter buffer parameters"
            
        def width(self):
            return 64
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00003000  + $PwId"
            
        def startAddress(self):
            return 0x00003000
            
        def endAddress(self):
            return 0x0000329f

        class _PwLOPSPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 56
                
            def startBit(self):
                return 48
        
            def name(self):
                return "PwLOPSPk"
            
            def description(self):
                return "Number of consecutive empty packets to declare LOPS (lost of packet synchronization) follow rfc4842"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwPayloadLen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 34
        
            def name(self):
                return "PwPayloadLen"
            
            def description(self):
                return "Payload length of receive pseudowire packet, set 0 for ATM PW"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwSetLofs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 33
                
            def startBit(self):
                return 29
        
            def name(self):
                return "PwSetLofs"
            
            def description(self):
                return "Number of consecutive lost packets to enter lost of frame state(LOFS), set 0 for ATM PW"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwClearLofs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 24
        
            def name(self):
                return "PwClearLofs"
            
            def description(self):
                return "Number of consecutive good packets to exit lost of frame state(LOFS), set 0 for ATM PW"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PdvSizePk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PdvSizePk"
            
            def description(self):
                return "Set 0 for ATM PW. This parameter is to prevent packet delay variation(PDV) from PSN. PdvSizePk is packet delay variation measured in packet unit. The formula is as follow: PdvSizePk = (Pw64kbpsSpeed * PDVus)/(125*PwPayloadLen) + ((Pw64kbpsSpeed * PDVus)%(125*PwPayloadLen) !=0) Case: Pseudowire speed =   Pw64kbpsSpeed * 64kbps PDVus is packet delay variation in microsecond unit. This value should be multiple of 125us for more accurate"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _JitBufSizePk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "JitBufSizePk"
            
            def description(self):
                return "Set default value for ATM PW. Jitter buffer size measured in packet unit. The formula is as follow In common: JitBufSizePk = 2 * PdvSizePk When user want to configure PDV and jitter buffer as independent parameters, the formular is: JitBufSizePk = (Pw64kbpsSpeed * JITBUFus)/(125*PwPayloadLen) + ((Pw64kbpsSpeed * JITBUFus)%(125*PwPayloadLen) !=0) where: Pseudowire speed =   Pw64kbpsSpeed * 64kbps JITBUFus is jitter buffer size measured in microsecond unit. This value should be multiple of 125us for more accurate"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PwLOPSPk"] = _AF6CNC0011_RD_PDA._jitbuf_ctrl._PwLOPSPk()
            allFields["PwPayloadLen"] = _AF6CNC0011_RD_PDA._jitbuf_ctrl._PwPayloadLen()
            allFields["PwSetLofs"] = _AF6CNC0011_RD_PDA._jitbuf_ctrl._PwSetLofs()
            allFields["PwClearLofs"] = _AF6CNC0011_RD_PDA._jitbuf_ctrl._PwClearLofs()
            allFields["PdvSizePk"] = _AF6CNC0011_RD_PDA._jitbuf_ctrl._PdvSizePk()
            allFields["JitBufSizePk"] = _AF6CNC0011_RD_PDA._jitbuf_ctrl._JitBufSizePk()
            return allFields

    class _jitbuf_hot_cfg_ctrl1(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit Ethernet PAD Control"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00001000
            
        def endAddress(self):
            return 0xffffffff

        class _pwid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 1
        
            def name(self):
                return "pwid"
            
            def description(self):
                return "pwid"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _enable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "enable"
            
            def description(self):
                return "enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pwid"] = _AF6CNC0011_RD_PDA._jitbuf_hot_cfg_ctrl1._pwid()
            allFields["enable"] = _AF6CNC0011_RD_PDA._jitbuf_hot_cfg_ctrl1._enable()
            return allFields

    class _jitbuf_hot_cfg_ctrl2(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit Ethernet PAD Control"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00001001
            
        def endAddress(self):
            return 0xffffffff

        class _ready(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ready"
            
            def description(self):
                return "ready"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ready"] = _AF6CNC0011_RD_PDA._jitbuf_hot_cfg_ctrl2._ready()
            return allFields

    class _reorder_ctrl(AtRegister.AtRegister):
        def name(self):
            return "4.1.4. Pseudowire PDA Reorder Control"
    
        def description(self):
            return "Used for TDM PW. This register configures RTP SSRC value."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0004000 +  $PWID"
            
        def startAddress(self):
            return 0x00004000
            
        def endAddress(self):
            return 0xffffffff

        class _PwReorPdv(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PwReorPdv"
            
            def description(self):
                return "This value is exact the same as above PdvSizePkt parameter, in case PDVSizePkt > 32 packets, set PwReorPdv to 32 because reorder only support maximum 32 packets"
            
            def type(self):
                return "// RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwReorEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "PwReorEn"
            
            def description(self):
                return "0: Enable reorder pseudowire packets from Ethernet side before sending to jitter buffer 1: Disable reorder function"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwReorTimeOut(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PwReorTimeOut"
            
            def description(self):
                return "Timer to detect lost of packet sequence. The formula to calculate PwReorTimeOut value from PdvSizePk (defined in Pseudowire PDA Jitter Buffer Control register) is as follow: PwReorTimeout = (Min(32,PdvSizePk) * PwPayloadLen)/ Pw64kbpsSpeed where  Pseudowire speed =   Pw64kbpsSpeed * 64kbps"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PwReorPdv"] = _AF6CNC0011_RD_PDA._reorder_ctrl._PwReorPdv()
            allFields["PwReorEn"] = _AF6CNC0011_RD_PDA._reorder_ctrl._PwReorEn()
            allFields["PwReorTimeOut"] = _AF6CNC0011_RD_PDA._reorder_ctrl._PwReorTimeOut()
            return allFields

    class _tdm_mode_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA TDM Mode Control"
    
        def description(self):
            return "The register configures modes of TDM Payload De-Assembler function"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0005000 +  $PWID"
            
        def startAddress(self):
            return 0x00005000
            
        def endAddress(self):
            return 0xffffffff

        class _PDANxDS0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 22
        
            def name(self):
                return "PDANxDS0"
            
            def description(self):
                return "Number of DS0 allocated for CESoP PW"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAIdleCode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 14
        
            def name(self):
                return "PDAIdleCode"
            
            def description(self):
                return "Idle pattern to replace data in case of Lost/Lbit packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAAisUneqOff(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "PDAAisUneqOff"
            
            def description(self):
                return "0: Enable sending AIS/Uneq alarms from PW to TDM (default) 1: Disable sending AIS/Uneq alarms from PW to TDM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDARepMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 11
        
            def name(self):
                return "PDARepMode"
            
            def description(self):
                return "Mode to replace lost packets 0: Replace by replaying previous good packet (often for voice or video application) 1: Replace by AIS (default) 2: Replace by a configuration idle code"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDASlice(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "PDASlice"
            
            def description(self):
                return "0: Slice 0, sts 0-23 (Only for DE3 card mapping port 0-23 into 0-23 of engine#0) 1: Slice 1, sts 24-47 (Only for DE3 card mapping port 24-47 into 0-23 of engine#1)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAFirstStsId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PDAFirstStsId"
            
            def description(self):
                return "In case of TOH PW  & CES PW: don't care In case of DE3 SAToP PW: this is the STS ID itself In case of  CEP basic , VC4-4c basic: don't care In case of  VC3  fractional (similar STS-1) : this is the STS ID itself In case of  VC4  fractional (similar STS-3) : must be 0(group sts 0,8,16) or 1(group sts 1,9,17) or 2(group sts 2,10,18) or 3(group sts 3,11,19)    to group 7 (total 8 group)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PDAMode"
            
            def description(self):
                return "TDM Payload De-Assembler modes 0: SAToP PW 1: TOH PW (similar to SAToP) 2: CESoP without CAS PW 3: CESoP with CAS PW 8: VC3/VC4/VC4-4c basic 9: VC3 fractional VC11/VC12 10: VC3 asynchronous T3/E3 11: VC4 fractional VC11/VC12/VC3 12: VC11/VC12 basic 15: DE3 SAToP Others: Reserved"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAEbmEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDAEbmEn"
            
            def description(self):
                return "In case of VC3/VC4 fractional, we have option EBM enable or not, EBM is field at CEP control word to indicated which VT is unequipped 0: Disable 1: Enable (default)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDANxDS0"] = _AF6CNC0011_RD_PDA._tdm_mode_ctrl._PDANxDS0()
            allFields["PDAIdleCode"] = _AF6CNC0011_RD_PDA._tdm_mode_ctrl._PDAIdleCode()
            allFields["PDAAisUneqOff"] = _AF6CNC0011_RD_PDA._tdm_mode_ctrl._PDAAisUneqOff()
            allFields["PDARepMode"] = _AF6CNC0011_RD_PDA._tdm_mode_ctrl._PDARepMode()
            allFields["PDASlice"] = _AF6CNC0011_RD_PDA._tdm_mode_ctrl._PDASlice()
            allFields["PDAFirstStsId"] = _AF6CNC0011_RD_PDA._tdm_mode_ctrl._PDAFirstStsId()
            allFields["PDAMode"] = _AF6CNC0011_RD_PDA._tdm_mode_ctrl._PDAMode()
            allFields["PDAEbmEn"] = _AF6CNC0011_RD_PDA._tdm_mode_ctrl._PDAEbmEn()
            return allFields

    class _idle_code_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA Idle Code Control"
    
        def description(self):
            return "The register configures idle code for TDM Payload De-Assembler replay function"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00005c00
            
        def endAddress(self):
            return 0xffffffff

        class _PDAIdleCode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDAIdleCode"
            
            def description(self):
                return "Idle code value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDAIdleCode"] = _AF6CNC0011_RD_PDA._idle_code_ctrl._PDAIdleCode()
            return allFields

    class _oam_cpu_seg_info_sta(AtRegister.AtRegister):
        def name(self):
            return "Receive OAM CPU Segment Information Status"
    
        def description(self):
            return "This register stores information of OAM packet that will be read and processed by CPU. CPU would serve this address by regularly polling or by scanning OAM interrupt. PDA divides an //received OAM packet into 32-byte segments. CPU has to serve this address to know whether a segment is ready to be read. When a segment (not EOP segment) has just been fullly read by CPU, the maximum //time needed for PDA to prepare next segment is 25 microseconds. CPU can depend on this parameter for polling state machine"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002000
            
        def endAddress(self):
            return 0xffffffff

        class _RxCpuOamSegLen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 3
        
            def name(self):
                return "RxCpuOamSegLen"
            
            def description(self):
                return "Length in byte of OAM segment. Value 0 indicates a 1-byte segment, value 31 indicate a 32-byte segment and so on. This field is useful in case of EOP segment"
            
            def type(self):
                return "//        RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxCpuOamSegSop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "RxCpuOamSegSop"
            
            def description(self):
                return "Set 1 to indicate this is an SOP segment"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxCpuOamSegEop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "RxCpuOamSegEop"
            
            def description(self):
                return "Set 1 to indicate this is an EOP segment."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxCpuOamSegRdy(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxCpuOamSegRdy"
            
            def description(self):
                return "Set 1 to indicate a segment data is ready to be read by CPU"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxCpuOamSegLen"] = _AF6CNC0011_RD_PDA._oam_cpu_seg_info_sta._RxCpuOamSegLen()
            allFields["RxCpuOamSegSop"] = _AF6CNC0011_RD_PDA._oam_cpu_seg_info_sta._RxCpuOamSegSop()
            allFields["RxCpuOamSegEop"] = _AF6CNC0011_RD_PDA._oam_cpu_seg_info_sta._RxCpuOamSegEop()
            allFields["RxCpuOamSegRdy"] = _AF6CNC0011_RD_PDA._oam_cpu_seg_info_sta._RxCpuOamSegRdy()
            return allFields

    class _pw_pda_pw_prbs_gen_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA PW PRBS Generation Control"
    
        def description(self):
            return "The register configures modes of PW PRBS Generation function"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x007000 + 32768*slice + PwId"
            
        def startAddress(self):
            return 0x00007000
            
        def endAddress(self):
            return 0xffffffff

        class _PrbsData(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 3
        
            def name(self):
                return "PrbsData"
            
            def description(self):
                return "Prbs data random"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PrbsError(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PrbsError"
            
            def description(self):
                return "Force PRBS Error"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PrbsMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PrbsMode"
            
            def description(self):
                return "Prbs modes 0: Prbs15 (x15 + x14 + 1) 1: Prbs31 (x31 + x28 + 1)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PrbsEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PrbsEnb"
            
            def description(self):
                return "0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PrbsData"] = _AF6CNC0011_RD_PDA._pw_pda_pw_prbs_gen_ctrl._PrbsData()
            allFields["PrbsError"] = _AF6CNC0011_RD_PDA._pw_pda_pw_prbs_gen_ctrl._PrbsError()
            allFields["PrbsMode"] = _AF6CNC0011_RD_PDA._pw_pda_pw_prbs_gen_ctrl._PrbsMode()
            allFields["PrbsEnb"] = _AF6CNC0011_RD_PDA._pw_pda_pw_prbs_gen_ctrl._PrbsEnb()
            return allFields

    class _pw_pda_pw_prbs_mon_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA PW PRBS Monitor Control"
    
        def description(self):
            return "The register configures modes of PW PRBS Generation function"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x007800 + 32768*slice + PwId"
            
        def startAddress(self):
            return 0x00007800
            
        def endAddress(self):
            return 0xffffffff

        class _PrbsError(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "PrbsError"
            
            def description(self):
                return "Force PRBS Error Sticky, Read to clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PrbsSync(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PrbsSync"
            
            def description(self):
                return "0: Prbs Loss Status 1: Prbs Sync Status"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PrbsData(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PrbsData"
            
            def description(self):
                return "Prbs data random"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PrbsMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PrbsMode"
            
            def description(self):
                return "Prbs modes 0: Prbs15 (x15 + x14 + 1) 1: Prbs31 (x31 + x28 + 1)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PrbsError"] = _AF6CNC0011_RD_PDA._pw_pda_pw_prbs_mon_ctrl._PrbsError()
            allFields["PrbsSync"] = _AF6CNC0011_RD_PDA._pw_pda_pw_prbs_mon_ctrl._PrbsSync()
            allFields["PrbsData"] = _AF6CNC0011_RD_PDA._pw_pda_pw_prbs_mon_ctrl._PrbsData()
            allFields["PrbsMode"] = _AF6CNC0011_RD_PDA._pw_pda_pw_prbs_mon_ctrl._PrbsMode()
            return allFields

    class _epar_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA EPAR Control"
    
        def description(self):
            return "This register show jitter buffer status"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00009000 +  $PWID"
            
        def startAddress(self):
            return 0x00009000
            
        def endAddress(self):
            return 0xffffffff

        class _oeparslice(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "oeparslice"
            
            def description(self):
                return "OC24 Slice"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _oeparlid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 1
        
            def name(self):
                return "oeparlid"
            
            def description(self):
                return "OC24 TDM ID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _oeparen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "oeparen"
            
            def description(self):
                return "EPAR enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["oeparslice"] = _AF6CNC0011_RD_PDA._epar_ctrl._oeparslice()
            allFields["oeparlid"] = _AF6CNC0011_RD_PDA._epar_ctrl._oeparlid()
            allFields["oeparen"] = _AF6CNC0011_RD_PDA._epar_ctrl._oeparen()
            return allFields

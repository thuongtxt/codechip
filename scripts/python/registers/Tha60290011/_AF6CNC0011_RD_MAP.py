import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0011_RD_MAP(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["demap_channel_ctrl"] = _AF6CNC0011_RD_MAP._demap_channel_ctrl()
        allRegisters["map_channel_ctrl"] = _AF6CNC0011_RD_MAP._map_channel_ctrl()
        allRegisters["map_line_ctrl"] = _AF6CNC0011_RD_MAP._map_line_ctrl()
        allRegisters["map_idle_code"] = _AF6CNC0011_RD_MAP._map_idle_code()
        allRegisters["Bert_Force_Single_Bit_Error"] = _AF6CNC0011_RD_MAP._Bert_Force_Single_Bit_Error()
        return allRegisters

    class _demap_channel_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Demap Channel Control"
    
        def description(self):
            return "The registers are used by the hardware to convert channel identification number to STS/VT/PDH identification number. All STS/VT/PDHs of a concatenation are assigned the same channel identification number."
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000000 + 672*stsid + 96*vtgid + 32*vtid + slotid"
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0x00003fff

        class _DemapFirstTs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "DemapFirstTs"
            
            def description(self):
                return "First timeslot indication. Use for CESoP mode only"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DemapChannelType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 11
        
            def name(self):
                return "DemapChannelType"
            
            def description(self):
                return "Specify which type of mapping for this. Specify which type of mapping for this. 0: SAToP encapsulation from DS1/E1, DS3/E3 1: CESoP encapsulation from DS1/E1 2: ATM over DS1/ E1, SONET/SDH (unused) 3: IMA over DS1/E1, SONET/SDH(unused) 4: HDLC/PPP/LAPS over DS1/E1, SONET/SDH(unused) 5: MLPPP over DS1/E1, SONET/SDH(unused) 6: CEP encapsulation from SONET/SDH 7: Reserve"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DemapChEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "DemapChEn"
            
            def description(self):
                return "PW/Channels Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DemapPWIdField(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DemapPWIdField"
            
            def description(self):
                return "PW ID field that uses for Encapsulation"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DemapFirstTs"] = _AF6CNC0011_RD_MAP._demap_channel_ctrl._DemapFirstTs()
            allFields["DemapChannelType"] = _AF6CNC0011_RD_MAP._demap_channel_ctrl._DemapChannelType()
            allFields["DemapChEn"] = _AF6CNC0011_RD_MAP._demap_channel_ctrl._DemapChEn()
            allFields["DemapPWIdField"] = _AF6CNC0011_RD_MAP._demap_channel_ctrl._DemapPWIdField()
            return allFields

    class _map_channel_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Map Channel Control"
    
        def description(self):
            return "The registers are used by the hardware to convert channel identification number to STS/VT/PDH identification number. All STS/VT/PDHs of a concatenation are assigned the same channel identification number."
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x014000 + 672*stsid + 96*vtgid + 32*vtid + slotid"
            
        def startAddress(self):
            return 0x00014000
            
        def endAddress(self):
            return 0x00017fff

        class _MapFirstTs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "MapFirstTs"
            
            def description(self):
                return "First timeslot indication. Use for CESoP mode only"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MapChannelType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 11
        
            def name(self):
                return "MapChannelType"
            
            def description(self):
                return "Specify which type of mapping for this. Specify which type of mapping for this. 0: SAToP encapsulation from DS1/E1, DS3/E3 1: CESoP encapsulation from DS1/E1 2: CESoP with CAS 3: ATM(unused) 4: HDLC/PPP/LAPS over DS1/E1, SONET/SDH(unused) 5: MLPPP over DS1/E1, SONET/SDH(unused) 6: CEP encapsulation from SONET/SDH 7: Reserve"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MapChEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "MapChEn"
            
            def description(self):
                return "PW/Channels Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MapPWIdField(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MapPWIdField"
            
            def description(self):
                return "PW ID field that uses for Encapsulation."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MapFirstTs"] = _AF6CNC0011_RD_MAP._map_channel_ctrl._MapFirstTs()
            allFields["MapChannelType"] = _AF6CNC0011_RD_MAP._map_channel_ctrl._MapChannelType()
            allFields["MapChEn"] = _AF6CNC0011_RD_MAP._map_channel_ctrl._MapChEn()
            allFields["MapPWIdField"] = _AF6CNC0011_RD_MAP._map_channel_ctrl._MapPWIdField()
            return allFields

    class _map_line_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Map Line Control"
    
        def description(self):
            return "The registers provide the per line configurations for STS/VT/DS1/E1 line."
            
        def width(self):
            return 20
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x010000 + 32*stsid + 4*vtgid + vtid"
            
        def startAddress(self):
            return 0x00010000
            
        def endAddress(self):
            return 0x000103ff

        class _MapDs1LcEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 17
        
            def name(self):
                return "MapDs1LcEn"
            
            def description(self):
                return "DS1E1 Loop code insert enable 0: Disable 1: CSU Loop up 2: CSU Loop down 3: FA1 Loop up 4: FA1 Loop down 5: FA2 Loop up 6: FA2 Loop down"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MapFrameType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 15
        
            def name(self):
                return "MapFrameType"
            
            def description(self):
                return "depend on MapSigType[3:0], the MapFrameType[1:0] bit field can have difference meaning. 0: DS1 SF/E1 BF/E3 G.751(unused) /DS3 framing(unused)/VT1.5,VT2,VT6 normal/STS no POH, no Stuff 1: DS1 ESF/E1 CRC/E3 G.832(unused)/DS1,E1,VT1.5,VT2 fractional 2: CEP DS3/E3/VC3 fractional 3:CEP basic mode or VT with POH/STS with POH, Stuff/DS1,E1,DS3,E3 unframe mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MapSigType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 11
        
            def name(self):
                return "MapSigType"
            
            def description(self):
                return "0: DS1 1: E1 2: DS3 (unused) 3: E3 (unused) 4: VT 1.5 5: VT 2 6: unused 7: unused 8: VC3 9: VC4 10: STS12c/VC4_4c 12: TU3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MapTimeSrcMaster(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "MapTimeSrcMaster"
            
            def description(self):
                return "This bit is used to indicate the master timing or the master VC3 in VC4/VC4-Xc"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MapTimeSrcId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MapTimeSrcId"
            
            def description(self):
                return "The reference line ID used for timing reference or the master VC3 ID in VC4/VC4-Xc"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MapDs1LcEn"] = _AF6CNC0011_RD_MAP._map_line_ctrl._MapDs1LcEn()
            allFields["MapFrameType"] = _AF6CNC0011_RD_MAP._map_line_ctrl._MapFrameType()
            allFields["MapSigType"] = _AF6CNC0011_RD_MAP._map_line_ctrl._MapSigType()
            allFields["MapTimeSrcMaster"] = _AF6CNC0011_RD_MAP._map_line_ctrl._MapTimeSrcMaster()
            allFields["MapTimeSrcId"] = _AF6CNC0011_RD_MAP._map_line_ctrl._MapTimeSrcId()
            return allFields

    class _map_idle_code(AtRegister.AtRegister):
        def name(self):
            return "Map IDLE Code"
    
        def description(self):
            return "The registers provide the idle pattern"
            
        def width(self):
            return 8
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00018200
            
        def endAddress(self):
            return 0xffffffff

        class _IdleCode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IdleCode"
            
            def description(self):
                return "Idle code"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IdleCode"] = _AF6CNC0011_RD_MAP._map_idle_code._IdleCode()
            return allFields

    class _Bert_Force_Single_Bit_Error(AtRegister.AtRegister):
        def name(self):
            return "Thalassa Bert Force Single Bit Error"
    
        def description(self):
            return "The registers provide the threshold  for force bit err."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x008500 + Ber_level"
            
        def startAddress(self):
            return 0x00008500
            
        def endAddress(self):
            return 0x00008509

        class _ber_threshold(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ber_threshold"
            
            def description(self):
                return "Threshold HW force one bit error."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ber_threshold"] = _AF6CNC0011_RD_MAP._Bert_Force_Single_Bit_Error._ber_threshold()
            return allFields

import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0011_RD_DCCK(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["upen_glbint_stt"] = _AF6CNC0011_RD_DCCK._upen_glbint_stt()
        allRegisters["upen_int_rxdcc0_stt_r0"] = _AF6CNC0011_RD_DCCK._upen_int_rxdcc0_stt_r0()
        allRegisters["upen_int_rxdcc0_stt_r1"] = _AF6CNC0011_RD_DCCK._upen_int_rxdcc0_stt_r1()
        allRegisters["upen_int_rxdcc1_stt"] = _AF6CNC0011_RD_DCCK._upen_int_rxdcc1_stt()
        allRegisters["upen_int_rxdcc2_r0_stt"] = _AF6CNC0011_RD_DCCK._upen_int_rxdcc2_r0_stt()
        allRegisters["upen_int_rxdcc2_r1_stt"] = _AF6CNC0011_RD_DCCK._upen_int_rxdcc2_r1_stt()
        allRegisters["upen_int_txdcc0_r0_stt"] = _AF6CNC0011_RD_DCCK._upen_int_txdcc0_r0_stt()
        allRegisters["upen_int_txdcc0_r1_stt"] = _AF6CNC0011_RD_DCCK._upen_int_txdcc0_r1_stt()
        allRegisters["upen_int_txdcc1_r0_stt"] = _AF6CNC0011_RD_DCCK._upen_int_txdcc1_r0_stt()
        allRegisters["upen_int_txdcc1_r1_stt"] = _AF6CNC0011_RD_DCCK._upen_int_txdcc1_r1_stt()
        allRegisters["upen_int_txdcc2_r0_stt"] = _AF6CNC0011_RD_DCCK._upen_int_txdcc2_r0_stt()
        allRegisters["upen_int_txdcc2_r1_stt"] = _AF6CNC0011_RD_DCCK._upen_int_txdcc2_r1_stt()
        allRegisters["upen_int_txdcc3_r0_stt"] = _AF6CNC0011_RD_DCCK._upen_int_txdcc3_r0_stt()
        allRegisters["upen_int_txdcc3_r1_stt"] = _AF6CNC0011_RD_DCCK._upen_int_txdcc3_r1_stt()
        allRegisters["upen_int_txdcc_glb_stt"] = _AF6CNC0011_RD_DCCK._upen_int_txdcc_glb_stt()
        allRegisters["upen_glb_intenb"] = _AF6CNC0011_RD_DCCK._upen_glb_intenb()
        allRegisters["upen_rxdcc0_intenb_r0"] = _AF6CNC0011_RD_DCCK._upen_rxdcc0_intenb_r0()
        allRegisters["upen_rxdcc0_intenb_r1"] = _AF6CNC0011_RD_DCCK._upen_rxdcc0_intenb_r1()
        allRegisters["upen_rxdcc1_intenb"] = _AF6CNC0011_RD_DCCK._upen_rxdcc1_intenb()
        allRegisters["upen_rxdcc2_intenb_r0"] = _AF6CNC0011_RD_DCCK._upen_rxdcc2_intenb_r0()
        allRegisters["upen_rxdcc2_intenb_r1"] = _AF6CNC0011_RD_DCCK._upen_rxdcc2_intenb_r1()
        allRegisters["upen_txdcc_intenb_ept_r0"] = _AF6CNC0011_RD_DCCK._upen_txdcc_intenb_ept_r0()
        allRegisters["upen_txdcc_intenb_ept_r1"] = _AF6CNC0011_RD_DCCK._upen_txdcc_intenb_ept_r1()
        allRegisters["upen_txdcc_intenb_crc_r0"] = _AF6CNC0011_RD_DCCK._upen_txdcc_intenb_crc_r0()
        allRegisters["upen_txdcc_intenb_crc_r1"] = _AF6CNC0011_RD_DCCK._upen_txdcc_intenb_crc_r1()
        allRegisters["upen_txdcc_intenb_ovr_r0"] = _AF6CNC0011_RD_DCCK._upen_txdcc_intenb_ovr_r0()
        allRegisters["upen_txdcc_intenb_ovr_r1"] = _AF6CNC0011_RD_DCCK._upen_txdcc_intenb_ovr_r1()
        allRegisters["upen_txdcc_intenb_und_r0"] = _AF6CNC0011_RD_DCCK._upen_txdcc_intenb_und_r0()
        allRegisters["upen_txdcc_intenb_und_r1"] = _AF6CNC0011_RD_DCCK._upen_txdcc_intenb_und_r1()
        allRegisters["upen_txdcc_intenb_glb"] = _AF6CNC0011_RD_DCCK._upen_txdcc_intenb_glb()
        allRegisters["upen_loopen"] = _AF6CNC0011_RD_DCCK._upen_loopen()
        allRegisters["upen_cfg_dcc_en"] = _AF6CNC0011_RD_DCCK._upen_cfg_dcc_en()
        allRegisters["upen_dcctx_fcsrem"] = _AF6CNC0011_RD_DCCK._upen_dcctx_fcsrem()
        allRegisters["upen_dcchdr"] = _AF6CNC0011_RD_DCCK._upen_dcchdr()
        allRegisters["upen_dcctx_enacid"] = _AF6CNC0011_RD_DCCK._upen_dcctx_enacid()
        allRegisters["upen_dccrxhdr"] = _AF6CNC0011_RD_DCCK._upen_dccrxhdr()
        allRegisters["upen_dccrxmacenb"] = _AF6CNC0011_RD_DCCK._upen_dccrxmacenb()
        allRegisters["upen_dccrxcvlenb"] = _AF6CNC0011_RD_DCCK._upen_dccrxcvlenb()
        allRegisters["upen_dccdec"] = _AF6CNC0011_RD_DCCK._upen_dccdec()
        allRegisters["upen_stkerr_pktlen"] = _AF6CNC0011_RD_DCCK._upen_stkerr_pktlen()
        allRegisters["upen_stkerr_crcbuf"] = _AF6CNC0011_RD_DCCK._upen_stkerr_crcbuf()
        allRegisters["upen_stkerr_rx_eth2ocn"] = _AF6CNC0011_RD_DCCK._upen_stkerr_rx_eth2ocn()
        allRegisters["upen_curerr_undsz_r0"] = _AF6CNC0011_RD_DCCK._upen_curerr_undsz_r0()
        allRegisters["upen_curerr_undsz_r1"] = _AF6CNC0011_RD_DCCK._upen_curerr_undsz_r1()
        allRegisters["upen_curerr_ovrsz_r0"] = _AF6CNC0011_RD_DCCK._upen_curerr_ovrsz_r0()
        allRegisters["upen_curerr_ovrsz_r1"] = _AF6CNC0011_RD_DCCK._upen_curerr_ovrsz_r1()
        allRegisters["upen_curerr_crcbuf_r0"] = _AF6CNC0011_RD_DCCK._upen_curerr_crcbuf_r0()
        allRegisters["upen_curerr_crcbuf_r1"] = _AF6CNC0011_RD_DCCK._upen_curerr_crcbuf_r1()
        allRegisters["upen_curerr_bufept_r0"] = _AF6CNC0011_RD_DCCK._upen_curerr_bufept_r0()
        allRegisters["upen_curerr_bufept_r1"] = _AF6CNC0011_RD_DCCK._upen_curerr_bufept_r1()
        allRegisters["upen_curerr_glb"] = _AF6CNC0011_RD_DCCK._upen_curerr_glb()
        allRegisters["upen_cur_rxdcc0_r0"] = _AF6CNC0011_RD_DCCK._upen_cur_rxdcc0_r0()
        allRegisters["upen_cur_rxdcc0_r1"] = _AF6CNC0011_RD_DCCK._upen_cur_rxdcc0_r1()
        allRegisters["upen_cur_rxdcc2_r0"] = _AF6CNC0011_RD_DCCK._upen_cur_rxdcc2_r0()
        allRegisters["upen_cur_rxdcc2_r1"] = _AF6CNC0011_RD_DCCK._upen_cur_rxdcc2_r1()
        allRegisters["upen_dcc_rx_glb_sta"] = _AF6CNC0011_RD_DCCK._upen_dcc_rx_glb_sta()
        allRegisters["upen_dcc_glbcnt_sgmrxeop"] = _AF6CNC0011_RD_DCCK._upen_dcc_glbcnt_sgmrxeop()
        allRegisters["upen_dcc_glbcnt_sgmrxerr"] = _AF6CNC0011_RD_DCCK._upen_dcc_glbcnt_sgmrxerr()
        allRegisters["upen_dcc_glbcnt_sgmrxbyt"] = _AF6CNC0011_RD_DCCK._upen_dcc_glbcnt_sgmrxbyt()
        allRegisters["upen_dcc_glbcnt_sgmrxfail"] = _AF6CNC0011_RD_DCCK._upen_dcc_glbcnt_sgmrxfail()
        allRegisters["upen_dcc_glbcnt_sgmtxeop"] = _AF6CNC0011_RD_DCCK._upen_dcc_glbcnt_sgmtxeop()
        allRegisters["upen_dcc_glbcnt_sgmtxerr"] = _AF6CNC0011_RD_DCCK._upen_dcc_glbcnt_sgmtxerr()
        allRegisters["upen_dcc_glbcnt_sgmtxbyt"] = _AF6CNC0011_RD_DCCK._upen_dcc_glbcnt_sgmtxbyt()
        allRegisters["upen_dcc_cnt"] = _AF6CNC0011_RD_DCCK._upen_dcc_cnt()
        allRegisters["upen_hdlc_locfg"] = _AF6CNC0011_RD_DCCK._upen_hdlc_locfg()
        allRegisters["upen_hdlc_loglbcfg"] = _AF6CNC0011_RD_DCCK._upen_hdlc_loglbcfg()
        allRegisters["upen_hdlc_enc_master_ctrl"] = _AF6CNC0011_RD_DCCK._upen_hdlc_enc_master_ctrl()
        allRegisters["upen_hdlc_enc_ctrl_reg1"] = _AF6CNC0011_RD_DCCK._upen_hdlc_enc_ctrl_reg1()
        allRegisters["upen_hdlc_enc_ctrl_reg2"] = _AF6CNC0011_RD_DCCK._upen_hdlc_enc_ctrl_reg2()
        allRegisters["upen_genmon_reqint"] = _AF6CNC0011_RD_DCCK._upen_genmon_reqint()
        allRegisters["upen_dcc_cfg_testgen_hdr"] = _AF6CNC0011_RD_DCCK._upen_dcc_cfg_testgen_hdr()
        allRegisters["upen_dcc_cfg_testgen_mod"] = _AF6CNC0011_RD_DCCK._upen_dcc_cfg_testgen_mod()
        allRegisters["upen_dcc_cfg_testgen_enacid"] = _AF6CNC0011_RD_DCCK._upen_dcc_cfg_testgen_enacid()
        allRegisters["upen_dcc_cfg_testgen_glb_gen_mode"] = _AF6CNC0011_RD_DCCK._upen_dcc_cfg_testgen_glb_gen_mode()
        allRegisters["upen_dcc_cfg_testgen_glb_length"] = _AF6CNC0011_RD_DCCK._upen_dcc_cfg_testgen_glb_length()
        allRegisters["upen_dcc_cfg_testgen_glb_gen_interval"] = _AF6CNC0011_RD_DCCK._upen_dcc_cfg_testgen_glb_gen_interval()
        allRegisters["upen_mon_godpkt"] = _AF6CNC0011_RD_DCCK._upen_mon_godpkt()
        allRegisters["upen_mon_errpkt"] = _AF6CNC0011_RD_DCCK._upen_mon_errpkt()
        allRegisters["upen_mon_errvcg"] = _AF6CNC0011_RD_DCCK._upen_mon_errvcg()
        allRegisters["upen_mon_errseq"] = _AF6CNC0011_RD_DCCK._upen_mon_errseq()
        allRegisters["upen_mon_errfcs"] = _AF6CNC0011_RD_DCCK._upen_mon_errfcs()
        allRegisters["upen_mon_abrpkt"] = _AF6CNC0011_RD_DCCK._upen_mon_abrpkt()
        return allRegisters

    class _upen_glbint_stt(AtRegister.AtRegister):
        def name(self):
            return "Global Interrupt Status"
    
        def description(self):
            return "The register provides global interrupt status"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002001
            
        def endAddress(self):
            return 0xffffffff

        class _kbyte_interrupt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "kbyte_interrupt"
            
            def description(self):
                return "interrupt from Kbyte event"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_interrupt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_interrupt"
            
            def description(self):
                return "interrupt from DCC event"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["kbyte_interrupt"] = _AF6CNC0011_RD_DCCK._upen_glbint_stt._kbyte_interrupt()
            allFields["dcc_interrupt"] = _AF6CNC0011_RD_DCCK._upen_glbint_stt._dcc_interrupt()
            return allFields

    class _upen_int_rxdcc0_stt_r0(AtRegister.AtRegister):
        def name(self):
            return "DCC ETH2OCN Direction Interupt Status "Local Channel Mapping Is Disable Channel 31 to 0""
    
        def description(self):
            return "The register provides interrupt status of DCC event ETH2OCN direction"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002004
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_channel_disable_31_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_channel_disable_31_0"
            
            def description(self):
                return "Indicate mapping from 6LSB bit of received MAC DA value from SGMII port to local channel ID is disable Bit[0] -> Bit[31] indicate mapping is disable for MAC_DA[5:0] value from 0->31 ."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_channel_disable_31_0"] = _AF6CNC0011_RD_DCCK._upen_int_rxdcc0_stt_r0._dcc_channel_disable_31_0()
            return allFields

    class _upen_int_rxdcc0_stt_r1(AtRegister.AtRegister):
        def name(self):
            return "DCC ETH2OCN Direction Interupt Status "Local Channel Mapping Is Disable Channel 47 to 32""
    
        def description(self):
            return "The register provides interrupt status of DCC event ETH2OCN direction"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002014
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_channel_disable_47_32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_channel_disable_47_32"
            
            def description(self):
                return "Indicate mapping from 6LSB bit of received MAC DA value from SGMII port to local channel ID is disable Bit[0] -> Bit[15] indicate mapping is disable for MAC_DA[5:0] value from 32->47 ."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_channel_disable_47_32"] = _AF6CNC0011_RD_DCCK._upen_int_rxdcc0_stt_r1._dcc_channel_disable_47_32()
            return allFields

    class _upen_int_rxdcc1_stt(AtRegister.AtRegister):
        def name(self):
            return "DCC ETH2OCN Direction Interupt Status of Packet Classification Error"
    
        def description(self):
            return "The register provides interrupt status of DCC event ETH2OCN direction"
            
        def width(self):
            return 6
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002005
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_eth2ocn_buffer_full(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "dcc_eth2ocn_buffer_full"
            
            def description(self):
                return "ETH2OCN: One or more channels has buffer full error (reg 0x02006, 0x02016)"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_eth2ocn_rxchid_dis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "dcc_eth2ocn_rxchid_dis"
            
            def description(self):
                return "One or more local channels is not be enabled mapping from received MAC DA value (reg 0x02004, 0x02014)"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_eth2ocn_ovrsize_len(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "dcc_eth2ocn_ovrsize_len"
            
            def description(self):
                return "Received DCC packet's length from SGMII port over maximum allowed length	(1318 bytes)"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_eth2ocn_crc_error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "dcc_eth2ocn_crc_error"
            
            def description(self):
                return "Received packet from SGMII port has FCS error"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_eth2ocn_cvlid_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "dcc_eth2ocn_cvlid_mismat"
            
            def description(self):
                return "Received 12b CVLAN ID value of DCC frame different from global provisioned CVID"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_eth2ocn_macda_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_eth2ocn_macda_mismat"
            
            def description(self):
                return "Received 43b MAC DA value of DCC frame different from global provisioned DA"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_eth2ocn_buffer_full"] = _AF6CNC0011_RD_DCCK._upen_int_rxdcc1_stt._dcc_eth2ocn_buffer_full()
            allFields["dcc_eth2ocn_rxchid_dis"] = _AF6CNC0011_RD_DCCK._upen_int_rxdcc1_stt._dcc_eth2ocn_rxchid_dis()
            allFields["dcc_eth2ocn_ovrsize_len"] = _AF6CNC0011_RD_DCCK._upen_int_rxdcc1_stt._dcc_eth2ocn_ovrsize_len()
            allFields["dcc_eth2ocn_crc_error"] = _AF6CNC0011_RD_DCCK._upen_int_rxdcc1_stt._dcc_eth2ocn_crc_error()
            allFields["dcc_eth2ocn_cvlid_mismat"] = _AF6CNC0011_RD_DCCK._upen_int_rxdcc1_stt._dcc_eth2ocn_cvlid_mismat()
            allFields["dcc_eth2ocn_macda_mismat"] = _AF6CNC0011_RD_DCCK._upen_int_rxdcc1_stt._dcc_eth2ocn_macda_mismat()
            return allFields

    class _upen_int_rxdcc2_r0_stt(AtRegister.AtRegister):
        def name(self):
            return "DCC ETH2OCN Direction Interupt Status Buffer Full Channel 31 to 0"
    
        def description(self):
            return "The register provides interrupt status of DCC event ETH2OCN direction"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002006
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_eth2ocn_buf_ful_31_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_eth2ocn_buf_ful_31_0"
            
            def description(self):
                return "DCC packet buffer for ETH2OCN direction was fulled, some packets will be lost. Bit[00] -> Bit[31] indicate status of channel 0 -> 31."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_eth2ocn_buf_ful_31_0"] = _AF6CNC0011_RD_DCCK._upen_int_rxdcc2_r0_stt._dcc_eth2ocn_buf_ful_31_0()
            return allFields

    class _upen_int_rxdcc2_r1_stt(AtRegister.AtRegister):
        def name(self):
            return "DCC ETH2OCN Direction Interupt Status Buffer Full Channel 47 to 32"
    
        def description(self):
            return "The register provides interrupt status of DCC event ETH2OCN direction"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002016
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_eth2ocn_buf_ful_47_32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_eth2ocn_buf_ful_47_32"
            
            def description(self):
                return "DCC packet buffer for ETH2OCN direction was fulled, some packets will be lost. Bit[00] -> Bit[15] indicate status of channel 32 -> 47."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_eth2ocn_buf_ful_47_32"] = _AF6CNC0011_RD_DCCK._upen_int_rxdcc2_r1_stt._dcc_eth2ocn_buf_ful_47_32()
            return allFields

    class _upen_int_txdcc0_r0_stt(AtRegister.AtRegister):
        def name(self):
            return "DCC OCN2ETH Direction Interupt Status Buffer Full Channel 31 to 0"
    
        def description(self):
            return "The register provides interrupt status of DCC event OCN2ETH direction"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002008
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_ocn2eth_buf_ful_31_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_ocn2eth_buf_ful_31_0"
            
            def description(self):
                return "DCC packet buffer for OCN2ETH direction was fulled, some packets will be lost. Bit[00] -> Bit[31] indicate status of channel 0 -> 31."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_ocn2eth_buf_ful_31_0"] = _AF6CNC0011_RD_DCCK._upen_int_txdcc0_r0_stt._dcc_ocn2eth_buf_ful_31_0()
            return allFields

    class _upen_int_txdcc0_r1_stt(AtRegister.AtRegister):
        def name(self):
            return "DCC OCN2ETH Direction Interupt Status Buffer Full Channel 47 to 32"
    
        def description(self):
            return "The register provides interrupt status of DCC event OCN2ETH direction"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002018
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_ocn2eth_buf_ful_47_32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_ocn2eth_buf_ful_47_32"
            
            def description(self):
                return "DCC packet buffer for OCN2ETH direction was fulled, some packets will be lost. Bit[00] -> Bit[15] indicate status of channel 32 -> 47."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_ocn2eth_buf_ful_47_32"] = _AF6CNC0011_RD_DCCK._upen_int_txdcc0_r1_stt._dcc_ocn2eth_buf_ful_47_32()
            return allFields

    class _upen_int_txdcc1_r0_stt(AtRegister.AtRegister):
        def name(self):
            return "DCC OCN2ETH Direction Interupt CRC Error Status Channel 31 to 0"
    
        def description(self):
            return "The register provides interrupt status of DCC event OCN2ETH direction"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002009
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_hdlc_crc_error_31_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_hdlc_crc_error_31_0"
            
            def description(self):
                return "Indicate Received HDLC frame from OCN has FCS error"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_hdlc_crc_error_31_0"] = _AF6CNC0011_RD_DCCK._upen_int_txdcc1_r0_stt._dcc_hdlc_crc_error_31_0()
            return allFields

    class _upen_int_txdcc1_r1_stt(AtRegister.AtRegister):
        def name(self):
            return "DCC OCN2ETH Direction Interupt CRC Error Status Channel 47 to 32"
    
        def description(self):
            return "The register provides interrupt status of DCC event OCN2ETH direction"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002019
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_hdlc_crc_error_47_32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_hdlc_crc_error_47_32"
            
            def description(self):
                return "Indicate Received HDLC frame from OCN has FCS error"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_hdlc_crc_error_47_32"] = _AF6CNC0011_RD_DCCK._upen_int_txdcc1_r1_stt._dcc_hdlc_crc_error_47_32()
            return allFields

    class _upen_int_txdcc2_r0_stt(AtRegister.AtRegister):
        def name(self):
            return "DCC OCN2ETH Direction Interupt Oversize HDLC Length Status Channel 31 to 0"
    
        def description(self):
            return "The register provides interrupt status of DCC event OCN2ETH direction"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000200d
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_hdlc_ovrsize_len_31_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_hdlc_ovrsize_len_31_0"
            
            def description(self):
                return "Received HDLC packet's length from OCN overed maximum allowed length	(1536 bytes)"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_hdlc_ovrsize_len_31_0"] = _AF6CNC0011_RD_DCCK._upen_int_txdcc2_r0_stt._dcc_hdlc_ovrsize_len_31_0()
            return allFields

    class _upen_int_txdcc2_r1_stt(AtRegister.AtRegister):
        def name(self):
            return "DCC OCN2ETH Direction Interupt Oversize Length Status Channel 47 to 32"
    
        def description(self):
            return "The register provides interrupt status of DCC event OCN2ETH direction"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000201d
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_hdlc_ovrsize_len_47_32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_hdlc_ovrsize_len_47_32"
            
            def description(self):
                return "Received HDLC packet's length from OCN overed maximum allowed length	(1536 bytes)"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_hdlc_ovrsize_len_47_32"] = _AF6CNC0011_RD_DCCK._upen_int_txdcc2_r1_stt._dcc_hdlc_ovrsize_len_47_32()
            return allFields

    class _upen_int_txdcc3_r0_stt(AtRegister.AtRegister):
        def name(self):
            return "DCC OCN2ETH Direction Interupt Status Channel 32 to 0"
    
        def description(self):
            return "The register provides interrupt status of DCC event OCN2ETH direction"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000200e
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_hdlc_undsize_len_31_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_hdlc_undsize_len_31_0"
            
            def description(self):
                return "Received HDLC packet's length from OCN undered minimum allowed length (2 bytes)"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_hdlc_undsize_len_31_0"] = _AF6CNC0011_RD_DCCK._upen_int_txdcc3_r0_stt._dcc_hdlc_undsize_len_31_0()
            return allFields

    class _upen_int_txdcc3_r1_stt(AtRegister.AtRegister):
        def name(self):
            return "DCC OCN2ETH Direction HDLC Interupt Status Channel 47 to 32"
    
        def description(self):
            return "The register provides interrupt status of DCC event OCN2ETH direction"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000201e
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_hdlc_undsize_len_47_32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_hdlc_undsize_len_47_32"
            
            def description(self):
                return "Received HDLC packet's length from OCN undered minimum allowed length (2 bytes)"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_hdlc_undsize_len_47_32"] = _AF6CNC0011_RD_DCCK._upen_int_txdcc3_r1_stt._dcc_hdlc_undsize_len_47_32()
            return allFields

    class _upen_int_txdcc_glb_stt(AtRegister.AtRegister):
        def name(self):
            return "DCC ETH2OCN Direction Interupt Status of Packet Classification Error"
    
        def description(self):
            return "The register provides interrupt status of DCC event ETH2OCN direction"
            
        def width(self):
            return 4
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002010
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_ocn2eth_buf_ful(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "dcc_ocn2eth_buf_ful"
            
            def description(self):
                return "OCN2ETH: One or more channels has buffer full error (reg 0x02008, 0x02018)"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_ocn2eth_crc_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "dcc_ocn2eth_crc_err"
            
            def description(self):
                return "OCN2ETH: One or more channels has CRC error packet (reg 0x02009, 0x02019)"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_ocn2eth_undsize_len(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "dcc_ocn2eth_undsize_len"
            
            def description(self):
                return "OCN2ETH: One or more channels has minimum packet length violation (reg 0x0200E, 0x0201E)"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_ocn2eth_ovrsize_len(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_ocn2eth_ovrsize_len"
            
            def description(self):
                return "OCN2ETH: One or more channels has maximum packet length violation (reg 0x0200D, 0x0201D)"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_ocn2eth_buf_ful"] = _AF6CNC0011_RD_DCCK._upen_int_txdcc_glb_stt._dcc_ocn2eth_buf_ful()
            allFields["dcc_ocn2eth_crc_err"] = _AF6CNC0011_RD_DCCK._upen_int_txdcc_glb_stt._dcc_ocn2eth_crc_err()
            allFields["dcc_ocn2eth_undsize_len"] = _AF6CNC0011_RD_DCCK._upen_int_txdcc_glb_stt._dcc_ocn2eth_undsize_len()
            allFields["dcc_ocn2eth_ovrsize_len"] = _AF6CNC0011_RD_DCCK._upen_int_txdcc_glb_stt._dcc_ocn2eth_ovrsize_len()
            return allFields

    class _upen_glb_intenb(AtRegister.AtRegister):
        def name(self):
            return "Global Interrupt Enable"
    
        def description(self):
            return "The register provides configuration to enable global interrupt"
            
        def width(self):
            return 2
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0xffffffff

        class _kbyte_interrupt_enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "kbyte_interrupt_enb"
            
            def description(self):
                return "Enable interrupt from Kbyte event"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_interrupt_enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_interrupt_enb"
            
            def description(self):
                return "Enable interrupt from DCC even"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["kbyte_interrupt_enb"] = _AF6CNC0011_RD_DCCK._upen_glb_intenb._kbyte_interrupt_enb()
            allFields["dcc_interrupt_enb"] = _AF6CNC0011_RD_DCCK._upen_glb_intenb._dcc_interrupt_enb()
            return allFields

    class _upen_rxdcc0_intenb_r0(AtRegister.AtRegister):
        def name(self):
            return "DCC ETH2OCN Direction Enable Interupt " Local Channel Mapping Is Disable" Channel 31 to 0"
    
        def description(self):
            return "The register provides configuration to enable interrupt of DCC events ETH2OCN direction"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000004
            
        def endAddress(self):
            return 0xffffffff

        class _enb_int_dcc_chan_dis_31_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "enb_int_dcc_chan_dis_31_0"
            
            def description(self):
                return "Enable Interrupt of DCC Local Channel Identifier mapping per channel (reg 0x02004) Bit[00] -> Bit[31] indicate channel 0 -> 31."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["enb_int_dcc_chan_dis_31_0"] = _AF6CNC0011_RD_DCCK._upen_rxdcc0_intenb_r0._enb_int_dcc_chan_dis_31_0()
            return allFields

    class _upen_rxdcc0_intenb_r1(AtRegister.AtRegister):
        def name(self):
            return "DCC ETH2OCN Direction Enable Interupt " Local Channel Mapping Is Disable" Channel 47 to 32"
    
        def description(self):
            return "The register provides configuration to enable interrupt of DCC events ETH2OCN direction"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000014
            
        def endAddress(self):
            return 0xffffffff

        class _enb_int_dcc_chan_dis_47_32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "enb_int_dcc_chan_dis_47_32"
            
            def description(self):
                return "Enable Interrupt of DCC Local Channel Identifier mapping per channel (reg 0x02014) Bit[00] -> Bit[15] indicate channel 32 -> 47."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["enb_int_dcc_chan_dis_47_32"] = _AF6CNC0011_RD_DCCK._upen_rxdcc0_intenb_r1._enb_int_dcc_chan_dis_47_32()
            return allFields

    class _upen_rxdcc1_intenb(AtRegister.AtRegister):
        def name(self):
            return "DCC ETH2OCN Direction Enable Interrupt of "Packet Classification Error""
    
        def description(self):
            return "The register provides configuration to enable interrupt of DCC events ETH2OCN direction"
            
        def width(self):
            return 6
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000005
            
        def endAddress(self):
            return 0xffffffff

        class _enb_int_dcc_eth2eth_buffer_full(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "enb_int_dcc_eth2eth_buffer_full"
            
            def description(self):
                return "Enable Interrupt of \"ETH2OCN: One or more channels has buffer full error\" (reg 0x02005)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _enb_int_dcc_eth2ocn_rxchid_dis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "enb_int_dcc_eth2ocn_rxchid_dis"
            
            def description(self):
                return "Enable interrupt of \"One or more local channels is not be enabled mapping from received MAC DA value\" (reg 0x02005)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _enb_int_dcc_eth2ocn_ovrsize_len(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "enb_int_dcc_eth2ocn_ovrsize_len"
            
            def description(self):
                return "Enable Interrupt of \"Received DCC packet's length from SGMII port over maximum allowed length (1318 bytes)\"(reg 0x02005)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _enb_int_dcc_eth2ocn_crc_error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "enb_int_dcc_eth2ocn_crc_error"
            
            def description(self):
                return "Enable Interrupt of \"Received packet from SGMII port has FCS error\" (reg 0x02005)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _enb_int_dcc_eth2ocn_cvlid_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "enb_int_dcc_eth2ocn_cvlid_mismat"
            
            def description(self):
                return "Enable Interrupt of \"Received 12b CVLAN ID value of DCC frame different from global provisioned CVID \" (reg 0x02005)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _enb_int_dcc_eth2ocn_macda_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "enb_int_dcc_eth2ocn_macda_mismat"
            
            def description(self):
                return "Enable Interrupt of \"Received 43b MAC DA value of DCC frame different from global provisioned DA\" (reg 0x02005)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["enb_int_dcc_eth2eth_buffer_full"] = _AF6CNC0011_RD_DCCK._upen_rxdcc1_intenb._enb_int_dcc_eth2eth_buffer_full()
            allFields["enb_int_dcc_eth2ocn_rxchid_dis"] = _AF6CNC0011_RD_DCCK._upen_rxdcc1_intenb._enb_int_dcc_eth2ocn_rxchid_dis()
            allFields["enb_int_dcc_eth2ocn_ovrsize_len"] = _AF6CNC0011_RD_DCCK._upen_rxdcc1_intenb._enb_int_dcc_eth2ocn_ovrsize_len()
            allFields["enb_int_dcc_eth2ocn_crc_error"] = _AF6CNC0011_RD_DCCK._upen_rxdcc1_intenb._enb_int_dcc_eth2ocn_crc_error()
            allFields["enb_int_dcc_eth2ocn_cvlid_mismat"] = _AF6CNC0011_RD_DCCK._upen_rxdcc1_intenb._enb_int_dcc_eth2ocn_cvlid_mismat()
            allFields["enb_int_dcc_eth2ocn_macda_mismat"] = _AF6CNC0011_RD_DCCK._upen_rxdcc1_intenb._enb_int_dcc_eth2ocn_macda_mismat()
            return allFields

    class _upen_rxdcc2_intenb_r0(AtRegister.AtRegister):
        def name(self):
            return "DCC ETH2OCN Direction Enable Interrupt of "Buffer Full per Channel" Channel 31 to 0"
    
        def description(self):
            return "The register provides configuration to enable interrupt of DCC events ETH2OCN direction"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000006
            
        def endAddress(self):
            return 0xffffffff

        class _enb_int_dcc_eth2ocn_buf_ful_31_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "enb_int_dcc_eth2ocn_buf_ful_31_0"
            
            def description(self):
                return "Enable Interrupt of \"DCC packet buffer for ETH2OCN direction fulled\"	(reg 0x2006) Bit[00] -> Bit[31] indicate channel 0 -> 31."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["enb_int_dcc_eth2ocn_buf_ful_31_0"] = _AF6CNC0011_RD_DCCK._upen_rxdcc2_intenb_r0._enb_int_dcc_eth2ocn_buf_ful_31_0()
            return allFields

    class _upen_rxdcc2_intenb_r1(AtRegister.AtRegister):
        def name(self):
            return "DCC ETH2OCN Direction Enable Interrupt of "Buffer Full per Channel" Channel 31 to 0"
    
        def description(self):
            return "The register provides configuration to enable interrupt of DCC events ETH2OCN direction"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000016
            
        def endAddress(self):
            return 0xffffffff

        class _enb_int_dcc_eth2ocn_buf_ful_32_47(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "enb_int_dcc_eth2ocn_buf_ful_32_47"
            
            def description(self):
                return "Enable Interrupt of \"DCC packet buffer for ETH2OCN direction fulled\"	(reg 0x2016) Bit[00] -> Bit[15] indicate channel 32-> 47."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["enb_int_dcc_eth2ocn_buf_ful_32_47"] = _AF6CNC0011_RD_DCCK._upen_rxdcc2_intenb_r1._enb_int_dcc_eth2ocn_buf_ful_32_47()
            return allFields

    class _upen_txdcc_intenb_ept_r0(AtRegister.AtRegister):
        def name(self):
            return "DCC OCN2ETH Direction Enable Interrupt of "Buffer Full per Channel" Channel 31 to 0"
    
        def description(self):
            return "The register provides configuration to enable interrupt of DCC events OCN2ETH direction"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000008
            
        def endAddress(self):
            return 0xffffffff

        class _enb_int_dcc_ocn2eth_buf_ful_31_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "enb_int_dcc_ocn2eth_buf_ful_31_0"
            
            def description(self):
                return "Enable Interrupt of \"DCC packet buffer for OCN2ETH direction fulled\"	(reg 0x2008) Bit[00] -> Bit[31] indicate channel 0 -> 31."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["enb_int_dcc_ocn2eth_buf_ful_31_0"] = _AF6CNC0011_RD_DCCK._upen_txdcc_intenb_ept_r0._enb_int_dcc_ocn2eth_buf_ful_31_0()
            return allFields

    class _upen_txdcc_intenb_ept_r1(AtRegister.AtRegister):
        def name(self):
            return "DCC OCN2ETH Direction Enable Interrupt of "Buffer Full per Channel" Channel 31 to 0"
    
        def description(self):
            return "The register provides configuration to enable interrupt of DCC events OCN2ETH direction"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000018
            
        def endAddress(self):
            return 0xffffffff

        class _enb_int_dcc_ocn2eth_buf_ful_32_47(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "enb_int_dcc_ocn2eth_buf_ful_32_47"
            
            def description(self):
                return "Enable Interrupt of \"DCC packet buffer for OCN2ETH direction fulled\"	(reg 0x2018) Bit[00] -> Bit[15] indicate channel 32-> 47."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["enb_int_dcc_ocn2eth_buf_ful_32_47"] = _AF6CNC0011_RD_DCCK._upen_txdcc_intenb_ept_r1._enb_int_dcc_ocn2eth_buf_ful_32_47()
            return allFields

    class _upen_txdcc_intenb_crc_r0(AtRegister.AtRegister):
        def name(self):
            return "DCC OCN2ETH Direction Enable Interrupt "Packet's FCS Error" Channel 31 to 0"
    
        def description(self):
            return "The register provides configuration to enable interrupt of DCC event OCN2ETH direction"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000009
            
        def endAddress(self):
            return 0xffffffff

        class _enb_int_dcc_hdlc_crc_error_31_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "enb_int_dcc_hdlc_crc_error_31_0"
            
            def description(self):
                return "Enable Interrupt of \"Received HDLC frame from OCN has FCS error	\" per channel(Ref reg 0x02009) Bit[00] -> Bit[31] indicate channel 0 -> 31."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["enb_int_dcc_hdlc_crc_error_31_0"] = _AF6CNC0011_RD_DCCK._upen_txdcc_intenb_crc_r0._enb_int_dcc_hdlc_crc_error_31_0()
            return allFields

    class _upen_txdcc_intenb_crc_r1(AtRegister.AtRegister):
        def name(self):
            return "DCC OCN2ETH Direction Enable Interrupt "Packet's FCS Error" Channel 47 to 32"
    
        def description(self):
            return "The register provides configuration to enable interrupt of DCC event OCN2ETH direction"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000019
            
        def endAddress(self):
            return 0xffffffff

        class _enb_int_dcc_hdlc_crc_error_47_32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "enb_int_dcc_hdlc_crc_error_47_32"
            
            def description(self):
                return "Enable Interrupt of \"Received HDLC frame from OCN has FCS error	\" per channel (Ref reg 0x02019) Bit[00] -> Bit[15] indicate channel 32-> 47."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["enb_int_dcc_hdlc_crc_error_47_32"] = _AF6CNC0011_RD_DCCK._upen_txdcc_intenb_crc_r1._enb_int_dcc_hdlc_crc_error_47_32()
            return allFields

    class _upen_txdcc_intenb_ovr_r0(AtRegister.AtRegister):
        def name(self):
            return "DCC OCN2ETH Direction Enable Interrupt "Oversize HDLC Packet's Length" Channel 31 to 0"
    
        def description(self):
            return "The register provides configuration to enable interrupt of DCC event OCN2ETH direction"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000d
            
        def endAddress(self):
            return 0xffffffff

        class _enb_int_dcc_hdlc_ovrsize_len_31_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "enb_int_dcc_hdlc_ovrsize_len_31_0"
            
            def description(self):
                return "Enable Interrupt of \"Received HDLC packet's length from OCN overed maximum allowed length (1536 bytes)	\" (Ref reg 0x0200D) Bit[00] -> Bit[31] indicate channel 0 -> 31."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["enb_int_dcc_hdlc_ovrsize_len_31_0"] = _AF6CNC0011_RD_DCCK._upen_txdcc_intenb_ovr_r0._enb_int_dcc_hdlc_ovrsize_len_31_0()
            return allFields

    class _upen_txdcc_intenb_ovr_r1(AtRegister.AtRegister):
        def name(self):
            return "DCC OCN2ETH Direction Enable Interrupt "Oversize HDLC Packet's Length" Channel 31 to 0"
    
        def description(self):
            return "The register provides configuration to enable interrupt of DCC event OCN2ETH direction"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000001d
            
        def endAddress(self):
            return 0xffffffff

        class _enb_int_dcc_hdlc_ovrsize_len_47_32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "enb_int_dcc_hdlc_ovrsize_len_47_32"
            
            def description(self):
                return "Enable Interrupt of \"Received HDLC packet's length from OCN overed maximum allowed length (1536 bytes)\" (Ref reg 0x0201D) Bit[00] -> Bit[15] indicate channel 32 -> 47."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["enb_int_dcc_hdlc_ovrsize_len_47_32"] = _AF6CNC0011_RD_DCCK._upen_txdcc_intenb_ovr_r1._enb_int_dcc_hdlc_ovrsize_len_47_32()
            return allFields

    class _upen_txdcc_intenb_und_r0(AtRegister.AtRegister):
        def name(self):
            return "DCC OCN2ETH Direction Enable Interrupt "Undersize HDLC Packet's Length " Channel 31 to 0"
    
        def description(self):
            return "The register provides configuration to enable interrupt of DCC event OCN2ETH direction"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000e
            
        def endAddress(self):
            return 0xffffffff

        class _enb_int_dcc_hdlc_und_len_31_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "enb_int_dcc_hdlc_und_len_31_0"
            
            def description(self):
                return "Enable Interrupt of \"Received HDLC packet's length from OCN undered minimum allowed length (2 bytes)\" (Ref reg 0x200E) Bit[00] -> Bit[31] indicate channel 0 -> 31."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["enb_int_dcc_hdlc_und_len_31_0"] = _AF6CNC0011_RD_DCCK._upen_txdcc_intenb_und_r0._enb_int_dcc_hdlc_und_len_31_0()
            return allFields

    class _upen_txdcc_intenb_und_r1(AtRegister.AtRegister):
        def name(self):
            return "DCC OCN2ETH Direction Enable Interrupt "Undersize HDLC Packet's Length " Channel 47 to 32"
    
        def description(self):
            return "The register provides configuration to enable interrupt of DCC event OCN2ETH direction"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000001e
            
        def endAddress(self):
            return 0xffffffff

        class _enb_int_dcc_hdlc_und_len_47_32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "enb_int_dcc_hdlc_und_len_47_32"
            
            def description(self):
                return "Enable Interrupt of \"Received HDLC packet's length from OCN undered minimum allowed length (2 bytes)\" (Ref reg 0x201E) Bit[00] -> Bit[15] indicate channel 32 -> 47."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["enb_int_dcc_hdlc_und_len_47_32"] = _AF6CNC0011_RD_DCCK._upen_txdcc_intenb_und_r1._enb_int_dcc_hdlc_und_len_47_32()
            return allFields

    class _upen_txdcc_intenb_glb(AtRegister.AtRegister):
        def name(self):
            return "DCC ETH2OCN Direction Enable Interrupt Global Packet Error"
    
        def description(self):
            return "The register provides configuration to enable interrupt of DCC events OCN2ETH direction"
            
        def width(self):
            return 4
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000010
            
        def endAddress(self):
            return 0xffffffff

        class _int_enb_dcc_ocn2eth_buf_ful(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "int_enb_dcc_ocn2eth_buf_ful"
            
            def description(self):
                return "Enable Interrupt of  \"OCN2ETH: One or more channels has buffer full error\" (Ref Reg 0x02010)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _int_enb_dcc_ocn2eth_crc_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "int_enb_dcc_ocn2eth_crc_err"
            
            def description(self):
                return "Enable Interrupt of  \"OCN2ETH: One or more channels has CRC error packet \""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _int_enb_dcc_ocn2eth_undsize_len(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "int_enb_dcc_ocn2eth_undsize_len"
            
            def description(self):
                return "Enable Interrupt of  \"OCN2ETH: One or more channels has minimum packet length violation\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _enb_int_dcc_ocn2eth_ovrsize_len(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "enb_int_dcc_ocn2eth_ovrsize_len"
            
            def description(self):
                return "Enable Interrupt of  \"OCN2ETH: One or more channels has maximum packet length violation \""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["int_enb_dcc_ocn2eth_buf_ful"] = _AF6CNC0011_RD_DCCK._upen_txdcc_intenb_glb._int_enb_dcc_ocn2eth_buf_ful()
            allFields["int_enb_dcc_ocn2eth_crc_err"] = _AF6CNC0011_RD_DCCK._upen_txdcc_intenb_glb._int_enb_dcc_ocn2eth_crc_err()
            allFields["int_enb_dcc_ocn2eth_undsize_len"] = _AF6CNC0011_RD_DCCK._upen_txdcc_intenb_glb._int_enb_dcc_ocn2eth_undsize_len()
            allFields["enb_int_dcc_ocn2eth_ovrsize_len"] = _AF6CNC0011_RD_DCCK._upen_txdcc_intenb_glb._enb_int_dcc_ocn2eth_ovrsize_len()
            return allFields

    class _upen_loopen(AtRegister.AtRegister):
        def name(self):
            return "Loopback Enable Configuration"
    
        def description(self):
            return "The register provides loopback enable configuration"
            
        def width(self):
            return 6
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000001
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_buffer_loop_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "dcc_buffer_loop_en"
            
            def description(self):
                return "Enable loopback of RX-BUFFER to TX-BUFFER      ."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _genmon_loop_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "genmon_loop_en"
            
            def description(self):
                return "Enable loopback of data from DCC generator to RX-DCC  ."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ksdh_loop_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "ksdh_loop_en"
            
            def description(self):
                return "Enable loopback of Kbyte information           ."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ksgm_loop_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "ksgm_loop_en"
            
            def description(self):
                return "Enable SGMII loopback of Kbyte Port."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _hdlc_loop_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "hdlc_loop_en"
            
            def description(self):
                return "Enable loopback from HDLC Encap to HDLC DEcap of DCC byte."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _dsgm_loop_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dsgm_loop_en"
            
            def description(self):
                return "Enable SGMII loopback of DCC Port."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_buffer_loop_en"] = _AF6CNC0011_RD_DCCK._upen_loopen._dcc_buffer_loop_en()
            allFields["genmon_loop_en"] = _AF6CNC0011_RD_DCCK._upen_loopen._genmon_loop_en()
            allFields["ksdh_loop_en"] = _AF6CNC0011_RD_DCCK._upen_loopen._ksdh_loop_en()
            allFields["ksgm_loop_en"] = _AF6CNC0011_RD_DCCK._upen_loopen._ksgm_loop_en()
            allFields["hdlc_loop_en"] = _AF6CNC0011_RD_DCCK._upen_loopen._hdlc_loop_en()
            allFields["dsgm_loop_en"] = _AF6CNC0011_RD_DCCK._upen_loopen._dsgm_loop_en()
            return allFields

    class _upen_cfg_dcc_en(AtRegister.AtRegister):
        def name(self):
            return "Enable DCC Engine"
    
        def description(self):
            return "The register is used to enable DCC Engine to receive/transmit data from/to OCN"
            
        def width(self):
            return 1
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000a
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_dcc_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_dcc_en"
            
            def description(self):
                return "Enable DCC Engine to receive/transmit data from/to OCN."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_dcc_en"] = _AF6CNC0011_RD_DCCK._upen_cfg_dcc_en._cfg_dcc_en()
            return allFields

    class _upen_dcctx_fcsrem(AtRegister.AtRegister):
        def name(self):
            return "Dcc_Fcs_Rem_Mode"
    
        def description(self):
            return "The register provides configuration to remove FCS32 or FCS16"
            
        def width(self):
            return 48
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00010003
            
        def endAddress(self):
            return 0xffffffff

        class _FCS_Remove_Mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FCS_Remove_Mode"
            
            def description(self):
                return "Remove 4 bytes FCS32 or 2byte FCS16 This configuration depend on the FCS checking mode at HDLC DEC (which is configed at reg address : 0x04000 - 0x04037) Bit 0 -> 31 : channel 0 -> 47"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FCS_Remove_Mode"] = _AF6CNC0011_RD_DCCK._upen_dcctx_fcsrem._FCS_Remove_Mode()
            return allFields

    class _upen_dcchdr(AtRegister.AtRegister):
        def name(self):
            return "DDC_TX_Header_Per_Channel"
    
        def description(self):
            return "The register provides data for configuration of 22bytes Header of each channel ID, in which, only 16byte is configurable the configuration postion of header byte is as follow; DA(6byte) + SA(6byte) + {PCP,DEI,VID}(2byte) + VERSION(1byte) + TYPE(1byte) hdrpos = 0 is position of DA[47:40] and hdrpos = 15 is position of TYPE field"
            
        def width(self):
            return 8
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x11400 + $channelid*16 + $hdrpos"
            
        def startAddress(self):
            return 0x00011400
            
        def endAddress(self):
            return 0x000117ff

        class _HEADER_POS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "HEADER_POS"
            
            def description(self):
                return "8b value of header per Channel ID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["HEADER_POS"] = _AF6CNC0011_RD_DCCK._upen_dcchdr._HEADER_POS()
            return allFields

    class _upen_dcctx_enacid(AtRegister.AtRegister):
        def name(self):
            return "DDC_Channel_Enable"
    
        def description(self):
            return "The register provides configuration for enable transmission DDC packet per channel"
            
        def width(self):
            return 48
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00011000
            
        def endAddress(self):
            return 0xffffffff

        class _Channel_Enable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Channel_Enable"
            
            def description(self):
                return "Enable transmitting of DDC packet per channel. Bit[47:00] represent for channel 47->00"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Channel_Enable"] = _AF6CNC0011_RD_DCCK._upen_dcctx_enacid._Channel_Enable()
            return allFields

    class _upen_dccrxhdr(AtRegister.AtRegister):
        def name(self):
            return "DDC_RX_Global_ProvisionedHeader_Configuration"
    
        def description(self):
            return "The register provides data for configuration of 22bytes Header of each channel ID"
            
        def width(self):
            return 70
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x12001"
            
        def startAddress(self):
            return 0x00012001
            
        def endAddress(self):
            return 0xffffffff

        class _ETH_TYP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 69
                
            def startBit(self):
                return 54
        
            def name(self):
                return "ETH_TYP"
            
            def description(self):
                return "16b value of Provisioned ETHERTYPE of DCC"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CVID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 53
                
            def startBit(self):
                return 42
        
            def name(self):
                return "CVID"
            
            def description(self):
                return "12b value of Provisioned C-VLAN ID. This value is used to compared with received CVLAN ID value."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAC_DA(AtRegister.AtRegisterField):
            def stopBit(self):
                return 41
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MAC_DA"
            
            def description(self):
                return "42b MSB of Provisioned MAC DA value. This value is used to compared with received MAC_DA[47:06] value. If a match is confirmed, MAC_DA[05:00] is used to represent channelID value before mapping."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ETH_TYP"] = _AF6CNC0011_RD_DCCK._upen_dccrxhdr._ETH_TYP()
            allFields["CVID"] = _AF6CNC0011_RD_DCCK._upen_dccrxhdr._CVID()
            allFields["MAC_DA"] = _AF6CNC0011_RD_DCCK._upen_dccrxhdr._MAC_DA()
            return allFields

    class _upen_dccrxmacenb(AtRegister.AtRegister):
        def name(self):
            return "DDC_RX_MAC_Check_Enable_Configuration"
    
        def description(self):
            return "The register provides configuration that enable base MAC DA check per channel"
            
        def width(self):
            return 48
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x12002"
            
        def startAddress(self):
            return 0x00012002
            
        def endAddress(self):
            return 0xffffffff

        class _MAC_check_enable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MAC_check_enable"
            
            def description(self):
                return "Enable channel checking of received MAC DA compare to globally provisioned Base MAC.On mismatch, frame is discarded."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MAC_check_enable"] = _AF6CNC0011_RD_DCCK._upen_dccrxmacenb._MAC_check_enable()
            return allFields

    class _upen_dccrxcvlenb(AtRegister.AtRegister):
        def name(self):
            return "DDC_RX_CVLAN_Check_Enable_Configuration"
    
        def description(self):
            return "The register provides configuration that enable base CVLAN Check"
            
        def width(self):
            return 48
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x12003"
            
        def startAddress(self):
            return 0x00012003
            
        def endAddress(self):
            return 0xffffffff

        class _CVL_check_enable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CVL_check_enable"
            
            def description(self):
                return "Enable channel checking of received CVLAN ID compare to globally provisioned CVLAN ID.On mismatch, frame is discarded."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CVL_check_enable"] = _AF6CNC0011_RD_DCCK._upen_dccrxcvlenb._CVL_check_enable()
            return allFields

    class _upen_dccdec(AtRegister.AtRegister):
        def name(self):
            return "DDC_RX_Channel_Mapping"
    
        def description(self):
            return "The register provides channel mapping from received MAC DA to internal channelID"
            
        def width(self):
            return 7
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x12400 + $channelid"
            
        def startAddress(self):
            return 0x00012400
            
        def endAddress(self):
            return 0x0001242f

        class _Channel_enable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Channel_enable"
            
            def description(self):
                return "Enable Local Channel Identifier. Rx packet is discarded if enable is not set"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Mapping_ChannelID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Mapping_ChannelID"
            
            def description(self):
                return "Local ChannelID that is mapped from received bit[5:0] of MAC DA"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Channel_enable"] = _AF6CNC0011_RD_DCCK._upen_dccdec._Channel_enable()
            allFields["Mapping_ChannelID"] = _AF6CNC0011_RD_DCCK._upen_dccdec._Mapping_ChannelID()
            return allFields

    class _upen_stkerr_pktlen(AtRegister.AtRegister):
        def name(self):
            return "DCC_OCN2ETH_Pkt_Length_Alarm_Sticky"
    
        def description(self):
            return "The register provides Alarm Related to HDLC Length Error (OCN2ETH Direction)"
            
        def width(self):
            return 96
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x11004"
            
        def startAddress(self):
            return 0x00011004
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_overize_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 95
                
            def startBit(self):
                return 48
        
            def name(self):
                return "dcc_overize_err"
            
            def description(self):
                return "HDLC Length Oversize Error. Alarm per channel Received HDLC Frame from OCN has frame length over maximum allowed length (1536bytes) Bit[95] -> Bit[48]: channel ID 47 ->0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_undsize_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_undsize_err"
            
            def description(self):
                return "HDLC Length Undersize Error. Alarm per channel Received HDLC Frame from OCN has frame length below minimum allowed length (2bytes) Bit[47] -> Bit[0]: channel ID 47 ->0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_overize_err"] = _AF6CNC0011_RD_DCCK._upen_stkerr_pktlen._dcc_overize_err()
            allFields["dcc_undsize_err"] = _AF6CNC0011_RD_DCCK._upen_stkerr_pktlen._dcc_undsize_err()
            return allFields

    class _upen_stkerr_crcbuf(AtRegister.AtRegister):
        def name(self):
            return "DCC_OCN2ETH_Pkt_Error_And_Buffer_Full_Alarm_Sticky"
    
        def description(self):
            return "The register provides Alarm Related to HDLC CRC Error and Buffer Full (OCN2ETH Direction)"
            
        def width(self):
            return 96
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x11005"
            
        def startAddress(self):
            return 0x00011005
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_crc_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 95
                
            def startBit(self):
                return 48
        
            def name(self):
                return "dcc_crc_err"
            
            def description(self):
                return "HDLC Packet has CRC error (OCN to ETH direction) . Alarm per channel Bit[95] -> Bit[48]: channel ID 47 ->0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_buffull_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_buffull_err"
            
            def description(self):
                return "HDLC Packet buffer full (OCN to ETH direction) . Alarm per channel Buffer for HDLC Frame has been full. Some frames will be dropped Bit[47] -> Bit[0]: channel ID 47 ->0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_crc_err"] = _AF6CNC0011_RD_DCCK._upen_stkerr_crcbuf._dcc_crc_err()
            allFields["dcc_buffull_err"] = _AF6CNC0011_RD_DCCK._upen_stkerr_crcbuf._dcc_buffull_err()
            return allFields

    class _upen_stkerr_rx_eth2ocn(AtRegister.AtRegister):
        def name(self):
            return "DCC_ETH2OCN_Alarm_Sticky"
    
        def description(self):
            return "The register provides Alarms of ETH2OCN Direction"
            
        def width(self):
            return 102
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x12008"
            
        def startAddress(self):
            return 0x00012008
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_eth2ocn_buffull_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 101
                
            def startBit(self):
                return 53
        
            def name(self):
                return "dcc_eth2ocn_buffull_err"
            
            def description(self):
                return "Packet buffer full (ETH to OCN direction) . Alarm per channel Buffer of Ethernet Frame has been full. Some frames will be dropped Bit[101] -> Bit[53]: channel ID 47 ->0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_rxeth_maxlenerr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 52
                
            def startBit(self):
                return 52
        
            def name(self):
                return "dcc_rxeth_maxlenerr"
            
            def description(self):
                return "Received DCC packet from SGMII port has violated maximum packet's length error"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_rxeth_crcerr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 51
                
            def startBit(self):
                return 51
        
            def name(self):
                return "dcc_rxeth_crcerr"
            
            def description(self):
                return "Received DCC packet from SGMII port has CRC error"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_channel_disable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 50
                
            def startBit(self):
                return 3
        
            def name(self):
                return "dcc_channel_disable"
            
            def description(self):
                return "DCC Local Channel Identifier mapping is disable Bit[50] -> Bit[03] indicate channel 47-> 00."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_cvlid_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "dcc_cvlid_mismat"
            
            def description(self):
                return "Received 12b CVLAN ID value of DCC frame different from global provisioned CVID"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_macda_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "dcc_macda_mismat"
            
            def description(self):
                return "Received 43b MAC DA value of DCC frame different from global provisioned DA"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_ethtp_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_ethtp_mismat"
            
            def description(self):
                return "Received Ethernet Type of DCC frame different from global provisioned value"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_eth2ocn_buffull_err"] = _AF6CNC0011_RD_DCCK._upen_stkerr_rx_eth2ocn._dcc_eth2ocn_buffull_err()
            allFields["dcc_rxeth_maxlenerr"] = _AF6CNC0011_RD_DCCK._upen_stkerr_rx_eth2ocn._dcc_rxeth_maxlenerr()
            allFields["dcc_rxeth_crcerr"] = _AF6CNC0011_RD_DCCK._upen_stkerr_rx_eth2ocn._dcc_rxeth_crcerr()
            allFields["dcc_channel_disable"] = _AF6CNC0011_RD_DCCK._upen_stkerr_rx_eth2ocn._dcc_channel_disable()
            allFields["dcc_cvlid_mismat"] = _AF6CNC0011_RD_DCCK._upen_stkerr_rx_eth2ocn._dcc_cvlid_mismat()
            allFields["dcc_macda_mismat"] = _AF6CNC0011_RD_DCCK._upen_stkerr_rx_eth2ocn._dcc_macda_mismat()
            allFields["dcc_ethtp_mismat"] = _AF6CNC0011_RD_DCCK._upen_stkerr_rx_eth2ocn._dcc_ethtp_mismat()
            return allFields

    class _upen_curerr_undsz_r0(AtRegister.AtRegister):
        def name(self):
            return "DCC OCN2ETH Min Pkt Length Alarm Current Status Reg0"
    
        def description(self):
            return "The register provides Current Status Alarm Related to HDLC Length Error Undersize (OCN2ETH Direction)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x11008"
            
        def startAddress(self):
            return 0x00011008
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_ocn2eth_undsz_err_cursta_0_31(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_ocn2eth_undsz_err_cursta_0_31"
            
            def description(self):
                return "Current status of HDLC Length Undersize Error. Status per channel Received HDLC Frame from OCN has frame length below minimum allowed length (2bytes) Bit[31] -> Bit[0]: channel ID 31 ->0"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_ocn2eth_undsz_err_cursta_0_31"] = _AF6CNC0011_RD_DCCK._upen_curerr_undsz_r0._dcc_ocn2eth_undsz_err_cursta_0_31()
            return allFields

    class _upen_curerr_undsz_r1(AtRegister.AtRegister):
        def name(self):
            return "DCC OCN2ETH Min Pkt Length Current Status Reg1"
    
        def description(self):
            return "The register provides Current Status Alarm Related to HDLC Length Error Undersize (OCN2ETH Direction)"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x11018"
            
        def startAddress(self):
            return 0x00011018
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_ocn2eth_undsz_err_cursta_32_47(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_ocn2eth_undsz_err_cursta_32_47"
            
            def description(self):
                return "HDLC Length Undersize Error. Status per channel Received HDLC Frame from OCN has frame length below minimum allowed length (2bytes) Bit[0] -> Bit[15]: channel ID 32 -> 47"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_ocn2eth_undsz_err_cursta_32_47"] = _AF6CNC0011_RD_DCCK._upen_curerr_undsz_r1._dcc_ocn2eth_undsz_err_cursta_32_47()
            return allFields

    class _upen_curerr_ovrsz_r0(AtRegister.AtRegister):
        def name(self):
            return "DCC OCN2ETH Max Pkt Length urrent Status Reg0"
    
        def description(self):
            return "The register provides Alarm Status Related to HDLC Length Error (OCN2ETH Direction)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x11009"
            
        def startAddress(self):
            return 0x00011009
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_ocn2eth_ovrsz_err_cursta_0_31(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_ocn2eth_ovrsz_err_cursta_0_31"
            
            def description(self):
                return "Current status of HDLC Length Oversize Error. Status per channel Received HDLC Frame from OCN has frame length over maximum allowed length (1536bytes) Bit[] -> Bit[31]: channel ID 0 -> 31"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_ocn2eth_ovrsz_err_cursta_0_31"] = _AF6CNC0011_RD_DCCK._upen_curerr_ovrsz_r0._dcc_ocn2eth_ovrsz_err_cursta_0_31()
            return allFields

    class _upen_curerr_ovrsz_r1(AtRegister.AtRegister):
        def name(self):
            return "DCC OCN2ETH Max Pkt Length Current Status Reg1"
    
        def description(self):
            return "The register provides Alarm Status Related to HDLC Length Error (OCN2ETH Direction)"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x11019"
            
        def startAddress(self):
            return 0x00011019
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_ocn2eth_ovrsz_err_cursta_32_47(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_ocn2eth_ovrsz_err_cursta_32_47"
            
            def description(self):
                return "HDLC Length Oversize Error. Status per channel Received HDLC Frame from OCN has frame length over maximum allowed length (1536bytes) Bit[0 ] -> Bit[15]: channel ID 32 -> 47"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_ocn2eth_ovrsz_err_cursta_32_47"] = _AF6CNC0011_RD_DCCK._upen_curerr_ovrsz_r1._dcc_ocn2eth_ovrsz_err_cursta_32_47()
            return allFields

    class _upen_curerr_crcbuf_r0(AtRegister.AtRegister):
        def name(self):
            return "DCC_OCN2ETH_Pkt_CRC_Error_Current_Alarm_Status_Reg0"
    
        def description(self):
            return "The register provides Alarm Status Related to HDLC CRC Error (OCN2ETH Direction)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1100A"
            
        def startAddress(self):
            return 0x0001100a
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_ocn2eth_crc_err_cursta_0_31(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_ocn2eth_crc_err_cursta_0_31"
            
            def description(self):
                return "HDLC Packet has CRC error (OCN to ETH direction) . Alarm per channel Bit[0] -> Bit[31]: channel ID 0 ->31"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_ocn2eth_crc_err_cursta_0_31"] = _AF6CNC0011_RD_DCCK._upen_curerr_crcbuf_r0._dcc_ocn2eth_crc_err_cursta_0_31()
            return allFields

    class _upen_curerr_crcbuf_r1(AtRegister.AtRegister):
        def name(self):
            return "DCC_OCN2ETH_Pkt_CRC_Error_Alarm_Status"
    
        def description(self):
            return "The register provides Alarm Status Related to HDLC CRC Error (OCN2ETH Direction)"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1101A"
            
        def startAddress(self):
            return 0x0001101a
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_ocn2eth_crc_err_cursta_32_47(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_ocn2eth_crc_err_cursta_32_47"
            
            def description(self):
                return "HDLC Packet has CRC error (OCN to ETH direction) . Alarm per channel Bit[0] -> Bit[15]: channel ID 32 ->47"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_ocn2eth_crc_err_cursta_32_47"] = _AF6CNC0011_RD_DCCK._upen_curerr_crcbuf_r1._dcc_ocn2eth_crc_err_cursta_32_47()
            return allFields

    class _upen_curerr_bufept_r0(AtRegister.AtRegister):
        def name(self):
            return "DCC OCN2ETH Buffer Full Current Status Reg 0"
    
        def description(self):
            return "The register provides Alarm Related to HDLC Buffer Full (OCN2ETH Direction)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1100B"
            
        def startAddress(self):
            return 0x0001100b
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_ocn2eth_bufful_err_cursta_0_31(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_ocn2eth_bufful_err_cursta_0_31"
            
            def description(self):
                return "HDLC Packet buffer full (OCN to ETH direction) . Alarm per channel Buffer for HDLC Frame has been full. Some frames will be dropped Bit[0] -> Bit[31]: channel ID 0 -> 31"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_ocn2eth_bufful_err_cursta_0_31"] = _AF6CNC0011_RD_DCCK._upen_curerr_bufept_r0._dcc_ocn2eth_bufful_err_cursta_0_31()
            return allFields

    class _upen_curerr_bufept_r1(AtRegister.AtRegister):
        def name(self):
            return "DCC_OCN2ETH_Buffer_Full_Alarm_Status"
    
        def description(self):
            return "The register provides Alarm Related to HDLC Buffer Full (OCN2ETH Direction)"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1101B"
            
        def startAddress(self):
            return 0x0001101b
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_ocn2eth_bufful_err_cursta_32_47(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_ocn2eth_bufful_err_cursta_32_47"
            
            def description(self):
                return "HDLC Packet buffer full (OCN to ETH direction) . Alarm per channel Buffer for HDLC Frame has been full. Some frames will be dropped Bit[0] -> Bit[15]: channel ID 32 ->47"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_ocn2eth_bufful_err_cursta_32_47"] = _AF6CNC0011_RD_DCCK._upen_curerr_bufept_r1._dcc_ocn2eth_bufful_err_cursta_32_47()
            return allFields

    class _upen_curerr_glb(AtRegister.AtRegister):
        def name(self):
            return "DCC OCN2ETH Current Status of Global Alarm"
    
        def description(self):
            return "The register provides current status of global alarm of DCC, OCN2ETH direction"
            
        def width(self):
            return 4
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0001100e
            
        def endAddress(self):
            return 0xffffffff

        class _mux_dcc_ocn2eth_cursta_ful_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "mux_dcc_ocn2eth_cursta_ful_err"
            
            def description(self):
                return "Current Status of  \"OCN2ETH: One or more channels has buffer full error\""
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _mux_dcc_ocn2eth_cursta_crc_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "mux_dcc_ocn2eth_cursta_crc_err"
            
            def description(self):
                return "Current Status of  \"OCN2ETH: One or more channels has CRC error packet \""
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _mux_dcc_ocn2eth_cursta_undsz_len(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "mux_dcc_ocn2eth_cursta_undsz_len"
            
            def description(self):
                return "Current Status of  \"OCN2ETH: One or more channels has minimum packet length violation\""
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _mux_dcc_ocn2eth_cursta_ovrsz_len(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mux_dcc_ocn2eth_cursta_ovrsz_len"
            
            def description(self):
                return "Current Status of  \"OCN2ETH: One or more channels has maximum packet length violation \""
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mux_dcc_ocn2eth_cursta_ful_err"] = _AF6CNC0011_RD_DCCK._upen_curerr_glb._mux_dcc_ocn2eth_cursta_ful_err()
            allFields["mux_dcc_ocn2eth_cursta_crc_err"] = _AF6CNC0011_RD_DCCK._upen_curerr_glb._mux_dcc_ocn2eth_cursta_crc_err()
            allFields["mux_dcc_ocn2eth_cursta_undsz_len"] = _AF6CNC0011_RD_DCCK._upen_curerr_glb._mux_dcc_ocn2eth_cursta_undsz_len()
            allFields["mux_dcc_ocn2eth_cursta_ovrsz_len"] = _AF6CNC0011_RD_DCCK._upen_curerr_glb._mux_dcc_ocn2eth_cursta_ovrsz_len()
            return allFields

    class _upen_cur_rxdcc0_r0(AtRegister.AtRegister):
        def name(self):
            return "DCC ETH2OCN Direction Alarm " Local Channel Mapping Is Disable" Current Status of Channel 31 to 0"
    
        def description(self):
            return "The register provides configuration to enable interrupt of DCC events ETH2OCN direction"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00012004
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_eth2ocn_chandis_cursta_31_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_eth2ocn_chandis_cursta_31_0"
            
            def description(self):
                return "Current Status of Alarm \"DCC Local Channel Identifier mapping is disable\" of channel 0 to 31 Bit[00] -> Bit[31] indicate channel 0 -> 31."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_eth2ocn_chandis_cursta_31_0"] = _AF6CNC0011_RD_DCCK._upen_cur_rxdcc0_r0._dcc_eth2ocn_chandis_cursta_31_0()
            return allFields

    class _upen_cur_rxdcc0_r1(AtRegister.AtRegister):
        def name(self):
            return "DCC ETH2OCN Direction Alarm " Local Channel Mapping Is Disable" Current Status of Channel 32 to 47"
    
        def description(self):
            return "The register provides configuration to enable interrupt of DCC events ETH2OCN direction"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00012014
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_eth2ocn_chandis_cursta_32_47(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_eth2ocn_chandis_cursta_32_47"
            
            def description(self):
                return "Current Status of Alarm \"DCC Local Channel Identifier mapping is disable\" of Channel 32 to 47 Bit[00] -> Bit[15] indicate channel 32 -> 47."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_eth2ocn_chandis_cursta_32_47"] = _AF6CNC0011_RD_DCCK._upen_cur_rxdcc0_r1._dcc_eth2ocn_chandis_cursta_32_47()
            return allFields

    class _upen_cur_rxdcc2_r0(AtRegister.AtRegister):
        def name(self):
            return "DCC ETH2OCN Direction Curren Status of Buffer Full Channel 31 to 0"
    
        def description(self):
            return "The register provides current alarm status of DCC event ETH2OCN direction"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00012006
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_eth2ocn_bufful_cursta_31_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_eth2ocn_bufful_cursta_31_0"
            
            def description(self):
                return "Current status of \"DCC packet buffer for ETH2OCN direction was fulled, some packets will be lost.\" channel 0 to 31 Bit[00] -> Bit[31] indicate status of channel 0 -> 31."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_eth2ocn_bufful_cursta_31_0"] = _AF6CNC0011_RD_DCCK._upen_cur_rxdcc2_r0._dcc_eth2ocn_bufful_cursta_31_0()
            return allFields

    class _upen_cur_rxdcc2_r1(AtRegister.AtRegister):
        def name(self):
            return "DCC ETH2OCN Direction Curren Status of Buffer Full Channel 32 to 47"
    
        def description(self):
            return "The register provides current alarm status of DCC event ETH2OCN direction"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00012016
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_eth2ocn_bufful_cursta_32_47(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_eth2ocn_bufful_cursta_32_47"
            
            def description(self):
                return "Current status of \"DCC packet buffer for ETH2OCN direction was fulled, some packets will be lost.\" channel 32 to 47 Bit[00] -> Bit[15] indicate status of channel 32 -> 47."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_eth2ocn_bufful_cursta_32_47"] = _AF6CNC0011_RD_DCCK._upen_cur_rxdcc2_r1._dcc_eth2ocn_bufful_cursta_32_47()
            return allFields

    class _upen_dcc_rx_glb_sta(AtRegister.AtRegister):
        def name(self):
            return "DCC_ETH2OCN_Alarm_Status"
    
        def description(self):
            return "The register provides Alarms of ETH2OCN Direction"
            
        def width(self):
            return 6
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x12005"
            
        def startAddress(self):
            return 0x00012005
            
        def endAddress(self):
            return 0xffffffff

        class _sta_dcc_eth2ocn_bufful(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "sta_dcc_eth2ocn_bufful"
            
            def description(self):
                return "One or more channels has buffer full error"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _sta_dcc_eth2ocn_rxchid_dis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "sta_dcc_eth2ocn_rxchid_dis"
            
            def description(self):
                return "One or more \"DCC Local Channel Identifier mapping is disable\" status"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _sta_dcc_eth2ocn_ovrsz_len(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "sta_dcc_eth2ocn_ovrsz_len"
            
            def description(self):
                return "Received DCC packet from SGMII port has violated maximum packet's length error"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _sta_dcc_eth2ocn_crcerr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "sta_dcc_eth2ocn_crcerr"
            
            def description(self):
                return "Received packet from SGMII port has FCS error"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _sta_dcc_eth2ocn_cvlid_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "sta_dcc_eth2ocn_cvlid_mismat"
            
            def description(self):
                return "Received 12b CVLAN ID value of DCC frame different from global provisioned CVID"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _sta_dcc_eth2ocn_macda_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "sta_dcc_eth2ocn_macda_mismat"
            
            def description(self):
                return "Received 43b MAC DA value of DCC frame different from global provisioned DA"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["sta_dcc_eth2ocn_bufful"] = _AF6CNC0011_RD_DCCK._upen_dcc_rx_glb_sta._sta_dcc_eth2ocn_bufful()
            allFields["sta_dcc_eth2ocn_rxchid_dis"] = _AF6CNC0011_RD_DCCK._upen_dcc_rx_glb_sta._sta_dcc_eth2ocn_rxchid_dis()
            allFields["sta_dcc_eth2ocn_ovrsz_len"] = _AF6CNC0011_RD_DCCK._upen_dcc_rx_glb_sta._sta_dcc_eth2ocn_ovrsz_len()
            allFields["sta_dcc_eth2ocn_crcerr"] = _AF6CNC0011_RD_DCCK._upen_dcc_rx_glb_sta._sta_dcc_eth2ocn_crcerr()
            allFields["sta_dcc_eth2ocn_cvlid_mismat"] = _AF6CNC0011_RD_DCCK._upen_dcc_rx_glb_sta._sta_dcc_eth2ocn_cvlid_mismat()
            allFields["sta_dcc_eth2ocn_macda_mismat"] = _AF6CNC0011_RD_DCCK._upen_dcc_rx_glb_sta._sta_dcc_eth2ocn_macda_mismat()
            return allFields

    class _upen_dcc_glbcnt_sgmrxeop(AtRegister.AtRegister):
        def name(self):
            return "Counter_Rx_Eop_From_SGMII_Port1"
    
        def description(self):
            return "Counter of number of EOP receive from SGMII port 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x12021"
            
        def startAddress(self):
            return 0x00012021
            
        def endAddress(self):
            return 0xffffffff

        class _eop_counter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "eop_counter"
            
            def description(self):
                return "Counter of EOP receive from SGMII port 1"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["eop_counter"] = _AF6CNC0011_RD_DCCK._upen_dcc_glbcnt_sgmrxeop._eop_counter()
            return allFields

    class _upen_dcc_glbcnt_sgmrxerr(AtRegister.AtRegister):
        def name(self):
            return "Counter_Rx_Err_From_SGMII_Port1"
    
        def description(self):
            return "Counter of number of ERR receive from SGMII port 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x12022"
            
        def startAddress(self):
            return 0x00012022
            
        def endAddress(self):
            return 0xffffffff

        class _err_counter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "err_counter"
            
            def description(self):
                return "Counter of ERR receive from SGMII port 1"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["err_counter"] = _AF6CNC0011_RD_DCCK._upen_dcc_glbcnt_sgmrxerr._err_counter()
            return allFields

    class _upen_dcc_glbcnt_sgmrxbyt(AtRegister.AtRegister):
        def name(self):
            return "Counter_Rx_Byte_From_SGMII_Port1"
    
        def description(self):
            return "Counter of number of BYTE receive from SGMII port 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x12023"
            
        def startAddress(self):
            return 0x00012023
            
        def endAddress(self):
            return 0xffffffff

        class _byte_counter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "byte_counter"
            
            def description(self):
                return "Counter of BYTE receive from SGMII port 1"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["byte_counter"] = _AF6CNC0011_RD_DCCK._upen_dcc_glbcnt_sgmrxbyt._byte_counter()
            return allFields

    class _upen_dcc_glbcnt_sgmrxfail(AtRegister.AtRegister):
        def name(self):
            return "Counter_Rx_Unknow_From_SGMII_Port1"
    
        def description(self):
            return "Counter of number of Unknow Packet (not DCC packet) receive from SGMII port 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x12024"
            
        def startAddress(self):
            return 0x00012024
            
        def endAddress(self):
            return 0xffffffff

        class _unk_counter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "unk_counter"
            
            def description(self):
                return "Counter of unknow receive packet from SGMII port 1"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["unk_counter"] = _AF6CNC0011_RD_DCCK._upen_dcc_glbcnt_sgmrxfail._unk_counter()
            return allFields

    class _upen_dcc_glbcnt_sgmtxeop(AtRegister.AtRegister):
        def name(self):
            return "Counter_Tx_Eop_To_SGMII_Port1"
    
        def description(self):
            return "Counter of number of EOP transmit to SGMII port 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x11011"
            
        def startAddress(self):
            return 0x00011011
            
        def endAddress(self):
            return 0xffffffff

        class _sgm_txglb_eop_counter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "sgm_txglb_eop_counter"
            
            def description(self):
                return "Counter of EOP transmit to SGMII port 1"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["sgm_txglb_eop_counter"] = _AF6CNC0011_RD_DCCK._upen_dcc_glbcnt_sgmtxeop._sgm_txglb_eop_counter()
            return allFields

    class _upen_dcc_glbcnt_sgmtxerr(AtRegister.AtRegister):
        def name(self):
            return "Counter_Tx_Err_To_SGMII_Port1"
    
        def description(self):
            return "Counter of number of ERR transmit to SGMII port 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x11012"
            
        def startAddress(self):
            return 0x00011012
            
        def endAddress(self):
            return 0xffffffff

        class _sgm_txglb_err_counter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "sgm_txglb_err_counter"
            
            def description(self):
                return "Counter of ERR  transmit to SGMII port 1"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["sgm_txglb_err_counter"] = _AF6CNC0011_RD_DCCK._upen_dcc_glbcnt_sgmtxerr._sgm_txglb_err_counter()
            return allFields

    class _upen_dcc_glbcnt_sgmtxbyt(AtRegister.AtRegister):
        def name(self):
            return "Counter_Tx_Byte_To_SGMII_Port1"
    
        def description(self):
            return "Counter of number of BYTE transmit to SGMII port 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x11013"
            
        def startAddress(self):
            return 0x00011013
            
        def endAddress(self):
            return 0xffffffff

        class _sgm_txglb_byte_counter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "sgm_txglb_byte_counter"
            
            def description(self):
                return "Counter of TX BYTE to SGMII port 1"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["sgm_txglb_byte_counter"] = _AF6CNC0011_RD_DCCK._upen_dcc_glbcnt_sgmtxbyt._sgm_txglb_byte_counter()
            return allFields

    class _upen_dcc_cnt(AtRegister.AtRegister):
        def name(self):
            return "Counter_Packet_And_Byte_Per_Channel"
    
        def description(self):
            return "Counter of DCC at different point represent by cnt_type value: {1} : counter RX bytes from HDLC DECAP to DCC TX BUFFER (DEC2BUF), TDM2PSN direction {2} : counter RX good packets from HDLC DECAP to DCC TX BUFFER (DEC2BUF), TDM2PSN direction {3} : counter RX error packets from HDLC DECAP to DCC TX BUFFER (DEC2BUF), TDM2PSN direction {4} : counter RX lost packets from HDLC DECAP to DCC TX BUFFER (DEC2BUF), TDM2PSN direction {5} : counter TX bytes from DCC TX BUFFER to SGMII port (BUF2SGM), TDM2PSN direction {6} : counter TX packets from DCC TX BUFFER to SGMII port (BUF2SGM), TDM2PSN direction {7} : counter TX error packet from DCC TX BUFFER to SGMII port (BUF2SGM), TDM2PSN direction {8} : counter RX bytes from SGMII port to DCC RX BUFFER (SGM2BUF), PSN2TDM direction {9} : counter RX good packets from SGMII port to DCC RX BUFFER (SGM2BUF), PSN2TDM direction {10}: counter RX error packets from SGMII port to DCC RX BUFFER (SGM2BUF), PSN2TDM direction {11}: counter RX lost packets from SGMII port to DCC RX BUFFER (SGM2BUF), PSN2TDM direction {12}: counter TX bytes from DCC RX BUFFER to HDLC ENCAP (BUF2ENC), PSN2TDM direction {13}: counter TX good packets from DCC RX BUFFER to HDLC ENCAP (BUF2ENC), PSN2TDM direction {14}: counter TX error packets from DCC RX BUFFER to HDLC ENCAP (BUF2ENC), PSN2TDM direction"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x13000 + $cnt_type*64 + $channelID"
            
        def startAddress(self):
            return 0x00013000
            
        def endAddress(self):
            return 0x00013fff

        class _dcc_counter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_counter"
            
            def description(self):
                return "Counter of DCC packet and byte"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_counter"] = _AF6CNC0011_RD_DCCK._upen_dcc_cnt._dcc_counter()
            return allFields

    class _upen_hdlc_locfg(AtRegister.AtRegister):
        def name(self):
            return "CONFIG HDLC LO DEC"
    
        def description(self):
            return "config HDLC ID 0-31 HDL_PATH: iaf6cci0012_lodec_core.ihdlc_cfg.imem113x.ram.ram[$CID]"
            
        def width(self):
            return 5
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x04000+ $CID"
            
        def startAddress(self):
            return 0x00004000
            
        def endAddress(self):
            return 0x00004037

        class _cfg_scren(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "cfg_scren"
            
            def description(self):
                return "config to enable scramble, (1) is enable, (0) is disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "reserve"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_bitstuff(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "cfg_bitstuff"
            
            def description(self):
                return "config to select bit stuff or byte sutff, (1) is bit stuff, (0) is byte stuff"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_fcsmsb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfg_fcsmsb"
            
            def description(self):
                return "config to calculate FCS from MSB or LSB, (1) is MSB, (0) is LSB"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_fcsmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_fcsmode"
            
            def description(self):
                return "config to calculate FCS 32 or FCS 16, (1) is FCS 32, (0) is FCS 16"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_scren"] = _AF6CNC0011_RD_DCCK._upen_hdlc_locfg._cfg_scren()
            allFields["reserve"] = _AF6CNC0011_RD_DCCK._upen_hdlc_locfg._reserve()
            allFields["cfg_bitstuff"] = _AF6CNC0011_RD_DCCK._upen_hdlc_locfg._cfg_bitstuff()
            allFields["cfg_fcsmsb"] = _AF6CNC0011_RD_DCCK._upen_hdlc_locfg._cfg_fcsmsb()
            allFields["cfg_fcsmode"] = _AF6CNC0011_RD_DCCK._upen_hdlc_locfg._cfg_fcsmode()
            return allFields

    class _upen_hdlc_loglbcfg(AtRegister.AtRegister):
        def name(self):
            return "CONFIG HDLC GLOBAL LO DEC"
    
        def description(self):
            return "config HDLC global"
            
        def width(self):
            return 4
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00004404
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_lsbfirst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "cfg_lsbfirst"
            
            def description(self):
                return "config to receive LSB first, (1) is LSB first, (0) is default MSB"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_lsbfirst"] = _AF6CNC0011_RD_DCCK._upen_hdlc_loglbcfg._cfg_lsbfirst()
            return allFields

    class _upen_hdlc_enc_master_ctrl(AtRegister.AtRegister):
        def name(self):
            return "HDLC Encode Master Control"
    
        def description(self):
            return "config HDLC Encode Master"
            
        def width(self):
            return 1
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00030003
            
        def endAddress(self):
            return 0xffffffff

        class _encap_lsbfirst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "encap_lsbfirst"
            
            def description(self):
                return "config to transmit LSB first, (1) is LSB first, (0) is default MSB"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["encap_lsbfirst"] = _AF6CNC0011_RD_DCCK._upen_hdlc_enc_master_ctrl._encap_lsbfirst()
            return allFields

    class _upen_hdlc_enc_ctrl_reg1(AtRegister.AtRegister):
        def name(self):
            return "HDLC Encode Control Reg 1"
    
        def description(self):
            return "config HDLC Encode Control Register 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x38000+ $CID"
            
        def startAddress(self):
            return 0x00038000
            
        def endAddress(self):
            return 0x00038037

        class _encap_screnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "encap_screnb"
            
            def description(self):
                return "Enable Scamble (1) Enable      , (0) disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _encap_idlemod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "encap_idlemod"
            
            def description(self):
                return "This bit is only used in Bit Stuffing mode Used to configured IDLE Mode When it is active, the ENC engine will insert '1' pattern when the ENC is idle. Otherwise the ENC will insert FLAG '7E' pattern (1) Enable            , (0) Disabe"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _encap_sabimod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "encap_sabimod"
            
            def description(self):
                return "Sabi/Protocol Field Mode (1) Field has 2 bytes , (0) field has 1 byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _encap_sabiins(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "encap_sabiins"
            
            def description(self):
                return "Sabi/Protocol Field Insert Enable (1) Enable Insert     , (0) Disable Insert"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _encap_ctrlins(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "encap_ctrlins"
            
            def description(self):
                return "Address/Control Field Insert Enable (1) Enable Insert     , (0) Disable Insert"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _encap_fcsmod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "encap_fcsmod"
            
            def description(self):
                return "FCS Select Mode (1) 32b FCS           , (0) 16b FCS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _encap_fcsins(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "encap_fcsins"
            
            def description(self):
                return "FCS Insert Enable (1) Enable Insert     , (0) Disable Insert"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _encap_flgmod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "encap_flgmod"
            
            def description(self):
                return "Flag Mode (1) Minimum 2 Flag    , (0) Minimum 1 Flag"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _encap_stfmod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "encap_stfmod"
            
            def description(self):
                return "Stuffing Mode (1) Bit Stuffing      , (0) Byte Stuffing"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["encap_screnb"] = _AF6CNC0011_RD_DCCK._upen_hdlc_enc_ctrl_reg1._encap_screnb()
            allFields["Reserved"] = _AF6CNC0011_RD_DCCK._upen_hdlc_enc_ctrl_reg1._Reserved()
            allFields["encap_idlemod"] = _AF6CNC0011_RD_DCCK._upen_hdlc_enc_ctrl_reg1._encap_idlemod()
            allFields["encap_sabimod"] = _AF6CNC0011_RD_DCCK._upen_hdlc_enc_ctrl_reg1._encap_sabimod()
            allFields["encap_sabiins"] = _AF6CNC0011_RD_DCCK._upen_hdlc_enc_ctrl_reg1._encap_sabiins()
            allFields["encap_ctrlins"] = _AF6CNC0011_RD_DCCK._upen_hdlc_enc_ctrl_reg1._encap_ctrlins()
            allFields["encap_fcsmod"] = _AF6CNC0011_RD_DCCK._upen_hdlc_enc_ctrl_reg1._encap_fcsmod()
            allFields["encap_fcsins"] = _AF6CNC0011_RD_DCCK._upen_hdlc_enc_ctrl_reg1._encap_fcsins()
            allFields["encap_flgmod"] = _AF6CNC0011_RD_DCCK._upen_hdlc_enc_ctrl_reg1._encap_flgmod()
            allFields["encap_stfmod"] = _AF6CNC0011_RD_DCCK._upen_hdlc_enc_ctrl_reg1._encap_stfmod()
            return allFields

    class _upen_hdlc_enc_ctrl_reg2(AtRegister.AtRegister):
        def name(self):
            return "HDLC Encode Control Reg 2"
    
        def description(self):
            return "config HDLC Encode Control Register 2 Byte Stuff Mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x39000+ $CID"
            
        def startAddress(self):
            return 0x00039000
            
        def endAddress(self):
            return 0x00039037

        class _encap_addrval(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "encap_addrval"
            
            def description(self):
                return "Address Field"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _encap_ctlrval(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "encap_ctlrval"
            
            def description(self):
                return "Control Field"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _encap_sapival0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "encap_sapival0"
            
            def description(self):
                return "SAPI/PROTOCOL Field 1st byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _encap_sapival1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "encap_sapival1"
            
            def description(self):
                return "SAPI/PROTOCOL Field 2nd byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["encap_addrval"] = _AF6CNC0011_RD_DCCK._upen_hdlc_enc_ctrl_reg2._encap_addrval()
            allFields["encap_ctlrval"] = _AF6CNC0011_RD_DCCK._upen_hdlc_enc_ctrl_reg2._encap_ctlrval()
            allFields["encap_sapival0"] = _AF6CNC0011_RD_DCCK._upen_hdlc_enc_ctrl_reg2._encap_sapival0()
            allFields["encap_sapival1"] = _AF6CNC0011_RD_DCCK._upen_hdlc_enc_ctrl_reg2._encap_sapival1()
            return allFields

    class _upen_genmon_reqint(AtRegister.AtRegister):
        def name(self):
            return "DDC_Test_Sdh_Req_Interval_Configuration"
    
        def description(self):
            return "The register is used to configure time interval to generate fake SDH request signal"
            
        def width(self):
            return 29
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _Timer_Enable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "Timer_Enable"
            
            def description(self):
                return "Enable generation of fake request SDH signal"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Timer_Value(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Timer_Value"
            
            def description(self):
                return "Counter of number of clk 155MHz between fake request SDH signal. This counter is used make a delay interval between 2 consecutive SDH requests generated by DCC GENERATOR. For example: to create a delay of 125us between SDH request signals, with a clock 155Mz, the value of counter to be configed to this field is (125*10^3)ns/(10^3/155)ns = 125*155 = 19375 = 0x4BAF"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Timer_Enable"] = _AF6CNC0011_RD_DCCK._upen_genmon_reqint._Timer_Enable()
            allFields["Timer_Value"] = _AF6CNC0011_RD_DCCK._upen_genmon_reqint._Timer_Value()
            return allFields

    class _upen_dcc_cfg_testgen_hdr(AtRegister.AtRegister):
        def name(self):
            return "DDC_Config_Test_Gen_Header_Per_Channel"
    
        def description(self):
            return "The register provides data for configuration of VID and MAC DA Header of each channel ID"
            
        def width(self):
            return 20
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0C100 + $channelid"
            
        def startAddress(self):
            return 0x0000c100
            
        def endAddress(self):
            return 0x0000c120

        class _HDR_VID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 8
        
            def name(self):
                return "HDR_VID"
            
            def description(self):
                return "12b of VID value in header of Generated Packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAC_DA(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MAC_DA"
            
            def description(self):
                return "Bit [7:0] of MAC DA value [47:0] of generated Packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["HDR_VID"] = _AF6CNC0011_RD_DCCK._upen_dcc_cfg_testgen_hdr._HDR_VID()
            allFields["MAC_DA"] = _AF6CNC0011_RD_DCCK._upen_dcc_cfg_testgen_hdr._MAC_DA()
            return allFields

    class _upen_dcc_cfg_testgen_mod(AtRegister.AtRegister):
        def name(self):
            return "DDC_Config_Test_Gen_Mode_Per_Channel"
    
        def description(self):
            return "The register provides data for configuration of generation mode of each channel ID"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0C200 + $channelid"
            
        def startAddress(self):
            return 0x0000c200
            
        def endAddress(self):
            return 0x0000c220

        class _gen_payload_mod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 30
        
            def name(self):
                return "gen_payload_mod"
            
            def description(self):
                return "Modes of generated packet payload:"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _gen_len_mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 28
        
            def name(self):
                return "gen_len_mode"
            
            def description(self):
                return "Modes of generated packet length :"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _gen_fix_pattern(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 20
        
            def name(self):
                return "gen_fix_pattern"
            
            def description(self):
                return "8b fix pattern payload if mode \"fix payload\" is used"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _gen_number_of_packet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "gen_number_of_packet"
            
            def description(self):
                return "Number of packets the GEN generates for each channelID in gen burst mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["gen_payload_mod"] = _AF6CNC0011_RD_DCCK._upen_dcc_cfg_testgen_mod._gen_payload_mod()
            allFields["gen_len_mode"] = _AF6CNC0011_RD_DCCK._upen_dcc_cfg_testgen_mod._gen_len_mode()
            allFields["gen_fix_pattern"] = _AF6CNC0011_RD_DCCK._upen_dcc_cfg_testgen_mod._gen_fix_pattern()
            allFields["gen_number_of_packet"] = _AF6CNC0011_RD_DCCK._upen_dcc_cfg_testgen_mod._gen_number_of_packet()
            return allFields

    class _upen_dcc_cfg_testgen_enacid(AtRegister.AtRegister):
        def name(self):
            return "DDC_Config_Test_Gen_Global_Gen_Enable_Channel"
    
        def description(self):
            return "The register provides configuration to enable Channel to generate DCC packets"
            
        def width(self):
            return 48
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000c000
            
        def endAddress(self):
            return 0xffffffff

        class _Channel_enable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Channel_enable"
            
            def description(self):
                return "Enable Channel to generate DCC packets. Bit[47:0] <-> Channel 47->0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Channel_enable"] = _AF6CNC0011_RD_DCCK._upen_dcc_cfg_testgen_enacid._Channel_enable()
            return allFields

    class _upen_dcc_cfg_testgen_glb_gen_mode(AtRegister.AtRegister):
        def name(self):
            return "DDC_Config_Test_Gen_Global_Gen_Mode"
    
        def description(self):
            return "The register provides global configuration of GEN mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000c001
            
        def endAddress(self):
            return 0xffffffff

        class _gen_force_err_len(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "gen_force_err_len"
            
            def description(self):
                return "Create wrong packet's length fiedl generated packets"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _gen_force_err_fcs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "gen_force_err_fcs"
            
            def description(self):
                return "Create wrong FCS field in generated packets"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _gen_force_err_dat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "gen_force_err_dat"
            
            def description(self):
                return "Create wrong data value in generated packets"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _gen_force_err_seq(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "gen_force_err_seq"
            
            def description(self):
                return "Create wrong Sequence field in generated packets"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _gen_force_err_vcg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "gen_force_err_vcg"
            
            def description(self):
                return "Create wrong VCG field in generated packets"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _gen_burst_mod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "gen_burst_mod"
            
            def description(self):
                return "Gen each Channel a number of packet as configured in \"Gen mode per channel\" resgister"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _gen_conti_mod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "gen_conti_mod"
            
            def description(self):
                return "Gen packet forever without stopping"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["gen_force_err_len"] = _AF6CNC0011_RD_DCCK._upen_dcc_cfg_testgen_glb_gen_mode._gen_force_err_len()
            allFields["gen_force_err_fcs"] = _AF6CNC0011_RD_DCCK._upen_dcc_cfg_testgen_glb_gen_mode._gen_force_err_fcs()
            allFields["gen_force_err_dat"] = _AF6CNC0011_RD_DCCK._upen_dcc_cfg_testgen_glb_gen_mode._gen_force_err_dat()
            allFields["gen_force_err_seq"] = _AF6CNC0011_RD_DCCK._upen_dcc_cfg_testgen_glb_gen_mode._gen_force_err_seq()
            allFields["gen_force_err_vcg"] = _AF6CNC0011_RD_DCCK._upen_dcc_cfg_testgen_glb_gen_mode._gen_force_err_vcg()
            allFields["gen_burst_mod"] = _AF6CNC0011_RD_DCCK._upen_dcc_cfg_testgen_glb_gen_mode._gen_burst_mod()
            allFields["gen_conti_mod"] = _AF6CNC0011_RD_DCCK._upen_dcc_cfg_testgen_glb_gen_mode._gen_conti_mod()
            return allFields

    class _upen_dcc_cfg_testgen_glb_length(AtRegister.AtRegister):
        def name(self):
            return "DDC_Config_Test_Gen_Global_Packet_Length"
    
        def description(self):
            return "The register provides configuration min length max length of generated Packets"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000c002
            
        def endAddress(self):
            return 0xffffffff

        class _gen_max_length(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "gen_max_length"
            
            def description(self):
                return "Maximum length of generated packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _gen_min_length(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "gen_min_length"
            
            def description(self):
                return "Minimum length of generated packet, also used for fix length packets in case fix length mode is used"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["gen_max_length"] = _AF6CNC0011_RD_DCCK._upen_dcc_cfg_testgen_glb_length._gen_max_length()
            allFields["gen_min_length"] = _AF6CNC0011_RD_DCCK._upen_dcc_cfg_testgen_glb_length._gen_min_length()
            return allFields

    class _upen_dcc_cfg_testgen_glb_gen_interval(AtRegister.AtRegister):
        def name(self):
            return "DDC_Config_Test_Gen_Global_Gen_Interval"
    
        def description(self):
            return "The register provides configuration for packet generating interval"
            
        def width(self):
            return 29
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000c003
            
        def endAddress(self):
            return 0xffffffff

        class _Gen_Intv_Enable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "Gen_Intv_Enable"
            
            def description(self):
                return "Enable using this interval value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Gen_Intv_Value(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Gen_Intv_Value"
            
            def description(self):
                return "Counter of number of clk 155MHz between packet generation. This counter is used make a delay interval between 2 consecutive packet generation. For example: to create a delay of 125us between 2 packet generating, with a clock 155Mz, the value of counter to be configed to this field is (125*10^3)ns/(10^3/155)ns = 125*155 = 19375 = 0x4BAF"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Gen_Intv_Enable"] = _AF6CNC0011_RD_DCCK._upen_dcc_cfg_testgen_glb_gen_interval._Gen_Intv_Enable()
            allFields["Gen_Intv_Value"] = _AF6CNC0011_RD_DCCK._upen_dcc_cfg_testgen_glb_gen_interval._Gen_Intv_Value()
            return allFields

    class _upen_mon_godpkt(AtRegister.AtRegister):
        def name(self):
            return "DDC_Test_Mon_Good_Packet_Counter"
    
        def description(self):
            return "Counter of receive good packet"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0E100 + $channelid"
            
        def startAddress(self):
            return 0x0000e100
            
        def endAddress(self):
            return 0x0000e130

        class _dcc_test_mon_good_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_test_mon_good_cnt"
            
            def description(self):
                return "Counter of Receive Good Packet from TEST GEN"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_test_mon_good_cnt"] = _AF6CNC0011_RD_DCCK._upen_mon_godpkt._dcc_test_mon_good_cnt()
            return allFields

    class _upen_mon_errpkt(AtRegister.AtRegister):
        def name(self):
            return "DDC_Test_Mon_Error_Data_Packet_Counter"
    
        def description(self):
            return "Counter of received packets has error data"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0E200 + $channelid"
            
        def startAddress(self):
            return 0x0000e200
            
        def endAddress(self):
            return 0x0000e230

        class _dcc_test_mon_errdat_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_test_mon_errdat_cnt"
            
            def description(self):
                return "Counter of Receive Error Data Packet from TEST GEN"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_test_mon_errdat_cnt"] = _AF6CNC0011_RD_DCCK._upen_mon_errpkt._dcc_test_mon_errdat_cnt()
            return allFields

    class _upen_mon_errvcg(AtRegister.AtRegister):
        def name(self):
            return "DDC_Test_Mon_Error_VCG_Packet_Counter"
    
        def description(self):
            return "Counter of received packet has wrong VCG value"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0E300 + $channelid"
            
        def startAddress(self):
            return 0x0000e300
            
        def endAddress(self):
            return 0x0000e330

        class _dcc_test_mon_errvcg_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_test_mon_errvcg_cnt"
            
            def description(self):
                return "Counter of Receive Error VCG Packet from TEST GEN"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_test_mon_errvcg_cnt"] = _AF6CNC0011_RD_DCCK._upen_mon_errvcg._dcc_test_mon_errvcg_cnt()
            return allFields

    class _upen_mon_errseq(AtRegister.AtRegister):
        def name(self):
            return "DDC_Test_Mon_Error_SEQ_Packet_Counter"
    
        def description(self):
            return "Counter of received packet has wrong Sequence value"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0E400 + $channelid"
            
        def startAddress(self):
            return 0x0000e400
            
        def endAddress(self):
            return 0x0000e430

        class _dcc_test_mon_errseq_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_test_mon_errseq_cnt"
            
            def description(self):
                return "Counter of Receive Error Sequence Packet from TEST GEN"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_test_mon_errseq_cnt"] = _AF6CNC0011_RD_DCCK._upen_mon_errseq._dcc_test_mon_errseq_cnt()
            return allFields

    class _upen_mon_errfcs(AtRegister.AtRegister):
        def name(self):
            return "DDC_Test_Mon_Error_FCS_Packet_Counter"
    
        def description(self):
            return "Counter of received packet has wrong FCS value"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0E500 + $channelid"
            
        def startAddress(self):
            return 0x0000e500
            
        def endAddress(self):
            return 0x0000e530

        class _dcc_test_mon_errfcs_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_test_mon_errfcs_cnt"
            
            def description(self):
                return "Counter of Receive Error FCS Packet from TEST GEN"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_test_mon_errfcs_cnt"] = _AF6CNC0011_RD_DCCK._upen_mon_errfcs._dcc_test_mon_errfcs_cnt()
            return allFields

    class _upen_mon_abrpkt(AtRegister.AtRegister):
        def name(self):
            return "DDC_Test_Mon_Abort_VCG_Packet_Counter"
    
        def description(self):
            return "Counter of received packet has been abbort due to wrong length information"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0E600 + $channelid"
            
        def startAddress(self):
            return 0x0000e600
            
        def endAddress(self):
            return 0x0000e630

        class _dcc_test_mon_abrpkt_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_test_mon_abrpkt_cnt"
            
            def description(self):
                return "Counter of Abbort Packet from TEST GEN"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_test_mon_abrpkt_cnt"] = _AF6CNC0011_RD_DCCK._upen_mon_abrpkt._dcc_test_mon_abrpkt_cnt()
            return allFields

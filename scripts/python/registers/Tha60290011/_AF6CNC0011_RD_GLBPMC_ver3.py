import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0011_RD_GLBPMC_ver3(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["upen_txpwcnt_info1_rw"] = _AF6CNC0011_RD_GLBPMC_ver3._upen_txpwcnt_info1_rw()
        allRegisters["upen_txpwcnt_info1_rw"] = _AF6CNC0011_RD_GLBPMC_ver3._upen_txpwcnt_info1_rw()
        allRegisters["upen_txpwcnt_info0"] = _AF6CNC0011_RD_GLBPMC_ver3._upen_txpwcnt_info0()
        allRegisters["upen_rxpwcnt_info0_rw"] = _AF6CNC0011_RD_GLBPMC_ver3._upen_rxpwcnt_info0_rw()
        allRegisters["upen_rxpwcnt_info0_rw"] = _AF6CNC0011_RD_GLBPMC_ver3._upen_rxpwcnt_info0_rw()
        return allRegisters

    class _upen_txpwcnt_info1_rw(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0_0000 + $pwid"
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0x000003ff

        class _txpwcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txpwcnt"
            
            def description(self):
                return "txpwcnt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txpwcnt"] = _AF6CNC0011_RD_GLBPMC_ver3._upen_txpwcnt_info1_rw._txpwcnt()
            return allFields

    class _upen_txpwcnt_info1_rw(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0_0800 + $pwid"
            
        def startAddress(self):
            return 0x00000800
            
        def endAddress(self):
            return 0x00000fff

        class _txpwcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txpwcnt"
            
            def description(self):
                return "txpwcnt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txpwcnt"] = _AF6CNC0011_RD_GLBPMC_ver3._upen_txpwcnt_info1_rw._txpwcnt()
            return allFields

    class _upen_txpwcnt_info0(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire TX Byte Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_0000 + $pwid"
            
        def startAddress(self):
            return 0x00080000
            
        def endAddress(self):
            return 0x000803ff

        class _pwcntbyte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pwcntbyte"
            
            def description(self):
                return "txpwcntbyte"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pwcntbyte"] = _AF6CNC0011_RD_GLBPMC_ver3._upen_txpwcnt_info0._pwcntbyte()
            return allFields

    class _upen_rxpwcnt_info0_rw(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receiver Counter Info0"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1_0000 + $pwid"
            
        def startAddress(self):
            return 0x00010000
            
        def endAddress(self):
            return 0x000103ff

        class _rxpwcnt_info0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpwcnt_info0"
            
            def description(self):
                return "in case of rxpwcnt_info0_cnt0 side: + rxcntbyte in case of rxpwcnt_info0_cnt1 side: + rxpkt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpwcnt_info0"] = _AF6CNC0011_RD_GLBPMC_ver3._upen_rxpwcnt_info0_rw._rxpwcnt_info0()
            return allFields

    class _upen_rxpwcnt_info0_rw(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receiver Counter Info0"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1_0800 + $pwid"
            
        def startAddress(self):
            return 0x00010800
            
        def endAddress(self):
            return 0x00001bff

        class _rxpwcnt_info0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpwcnt_info0"
            
            def description(self):
                return "in case of rxpwcnt_info0_cnt0 side: + rxcntbyte in case of rxpwcnt_info0_cnt1 side: + rxpkt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpwcnt_info0"] = _AF6CNC0011_RD_GLBPMC_ver3._upen_rxpwcnt_info0_rw._rxpwcnt_info0()
            return allFields

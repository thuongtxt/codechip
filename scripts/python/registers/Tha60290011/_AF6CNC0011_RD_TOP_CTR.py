import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0011_RD_TOP_CTR(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["o_controlx"] = _AF6CNC0011_RD_TOP_CTR._o_controlx()
        allRegisters["TOP_Status_1_d"] = _AF6CNC0011_RD_TOP_CTR._TOP_Status_1_d()
        allRegisters["i_sticky0"] = _AF6CNC0011_RD_TOP_CTR._i_sticky0()
        allRegisters["i_sticky0"] = _AF6CNC0011_RD_TOP_CTR._i_sticky0()
        allRegisters["i_sticky1"] = _AF6CNC0011_RD_TOP_CTR._i_sticky1()
        allRegisters["i_sticky1"] = _AF6CNC0011_RD_TOP_CTR._i_sticky1()
        return allRegisters

    class _o_controlx(AtRegister.AtRegister):
        def name(self):
            return "XFI drp port select"
    
        def description(self):
            return "This is the global configuration register for the Global Serdes Control"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "Base Addr + 0x00043"
            
        def startAddress(self):
            return 0x00000043
            
        def endAddress(self):
            return 0xffffffff

        class _XFI_port_select(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "XFI_port_select"
            
            def description(self):
                return "XFI port select, 1/2 -> XFI#0/XFI#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["XFI_port_select"] = _AF6CNC0011_RD_TOP_CTR._o_controlx._XFI_port_select()
            return allFields

    class _TOP_Status_1_d(AtRegister.AtRegister):
        def name(self):
            return "TOP Status 1-d"
    
        def description(self):
            return "0x61 : PCIe user clock (62.5 Mhz) 0x62 : DDR3#0 user clock (200 Mhz) 0x63 : DDR3#1 user clock (200 Mhz) 0x64 : DDR3#2 user clock (200 Mhz) 0x65 : XFI#0 user clock (156.25 Mhz) 0x66 : XFI#1 user clock (156.25 Mhz) 0x67 : SGMII2G5#0 user clock (312.5 Mhz) 0x68 : SGMII1G#0 user clock (125 Mhz) 0x69 : SGMII1G#1 user clock (125 Mhz) 0x6a : sysPLL user clock (155.52 Mhz) 0x6b : External reference clock (19.44 Mhz) 0x6c : PRC reference clock (19.44 Mhz) 0x6d : 1PPS reference clock (1 Hz)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000061
            
        def endAddress(self):
            return 0x0000006d

        class _i_statusx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "i_statusx"
            
            def description(self):
                return "Clock Value Monitor Change from hex format to DEC format to get Clock Value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["i_statusx"] = _AF6CNC0011_RD_TOP_CTR._TOP_Status_1_d._i_statusx()
            return allFields

    class _i_sticky0(AtRegister.AtRegister):
        def name(self):
            return "Top Interface Alarm Sticky"
    
        def description(self):
            return "Top Interface Status Sticky"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000060
            
        def endAddress(self):
            return 0xffffffff

        class _System_Pll_status(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "System_Pll_status"
            
            def description(self):
                return "System PLL status, 0/1->Locked/Unlocked"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _DDR3_0_calib_status(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "DDR3_0_calib_status"
            
            def description(self):
                return "DDR3#0 calib status, 0/1->OK/Fail"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _DDR3_1_calib_status(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "DDR3_1_calib_status"
            
            def description(self):
                return "DDR3#1 calib status, 0/1->OK/Fail"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _DDR3_2_calib_status(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "DDR3_2_calib_status"
            
            def description(self):
                return "DDR3#2 calib status, 0/1->OK/Fail"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["System_Pll_status"] = _AF6CNC0011_RD_TOP_CTR._i_sticky0._System_Pll_status()
            allFields["DDR3_0_calib_status"] = _AF6CNC0011_RD_TOP_CTR._i_sticky0._DDR3_0_calib_status()
            allFields["DDR3_1_calib_status"] = _AF6CNC0011_RD_TOP_CTR._i_sticky0._DDR3_1_calib_status()
            allFields["DDR3_2_calib_status"] = _AF6CNC0011_RD_TOP_CTR._i_sticky0._DDR3_2_calib_status()
            return allFields

    class _i_sticky0(AtRegister.AtRegister):
        def name(self):
            return "Top Interface Alarm Sticky 0"
    
        def description(self):
            return "Top Interface Alarm Sticky"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000050
            
        def endAddress(self):
            return 0xffffffff

        class _System_Pll_status(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "System_Pll_status"
            
            def description(self):
                return "System PLL status, 0/1->Locked/Unlocked"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _DDR3_0_calib_status(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "DDR3_0_calib_status"
            
            def description(self):
                return "DDR3#0 calib status, 0/1->OK/Fail"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _DDR3_1_calib_status(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "DDR3_1_calib_status"
            
            def description(self):
                return "DDR3#1 calib status, 0/1->OK/Fail"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _DDR3_2_calib_status(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "DDR3_2_calib_status"
            
            def description(self):
                return "DDR3#2 calib status, 0/1->OK/Fail"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["System_Pll_status"] = _AF6CNC0011_RD_TOP_CTR._i_sticky0._System_Pll_status()
            allFields["DDR3_0_calib_status"] = _AF6CNC0011_RD_TOP_CTR._i_sticky0._DDR3_0_calib_status()
            allFields["DDR3_1_calib_status"] = _AF6CNC0011_RD_TOP_CTR._i_sticky0._DDR3_1_calib_status()
            allFields["DDR3_2_calib_status"] = _AF6CNC0011_RD_TOP_CTR._i_sticky0._DDR3_2_calib_status()
            return allFields

    class _i_sticky1(AtRegister.AtRegister):
        def name(self):
            return "Top Interface Alarm Sticky 1"
    
        def description(self):
            return "Top Interface Alarm Sticky"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000051
            
        def endAddress(self):
            return 0xffffffff

        class _XFI_1_reset_done(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "XFI_1_reset_done"
            
            def description(self):
                return "XFI#1 reset done, 0/1->Fail/Done"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _XFI_0_reset_done(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "XFI_0_reset_done"
            
            def description(self):
                return "XFI#0 reset done, 0/1->Fail/Done"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _XFI_1_core_status__block_lock_(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "XFI_1_core_status__block_lock_"
            
            def description(self):
                return "XFI#1 core status (block lock), 0/1->Unlocked/Lock"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _XFI_0_core_status__block_lock_(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "XFI_0_core_status__block_lock_"
            
            def description(self):
                return "XFI#0 core status (block lock), 0/1->Unlocked/Lock"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["XFI_1_reset_done"] = _AF6CNC0011_RD_TOP_CTR._i_sticky1._XFI_1_reset_done()
            allFields["XFI_0_reset_done"] = _AF6CNC0011_RD_TOP_CTR._i_sticky1._XFI_0_reset_done()
            allFields["XFI_1_core_status__block_lock_"] = _AF6CNC0011_RD_TOP_CTR._i_sticky1._XFI_1_core_status__block_lock_()
            allFields["XFI_0_core_status__block_lock_"] = _AF6CNC0011_RD_TOP_CTR._i_sticky1._XFI_0_core_status__block_lock_()
            return allFields

    class _i_sticky1(AtRegister.AtRegister):
        def name(self):
            return "Top Interface Alarm Sticky 2"
    
        def description(self):
            return "Top Interface Alarm Sticky"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000052
            
        def endAddress(self):
            return 0xffffffff

        class _SGMII1G_1_reset_done(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "SGMII1G_1_reset_done"
            
            def description(self):
                return "SGMII1G#1 reset done, 0/1->Fail/Done"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _SGMII1G_0_reset_done(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "SGMII1G_0_reset_done"
            
            def description(self):
                return "SGMII1G#0 reset done, 0/1->Fail/Done"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _SGMII2G5_0_reset_done(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "SGMII2G5_0_reset_done"
            
            def description(self):
                return "SGMII2G5#0 reset done, 0/1->Fail/Done"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _SGMII1G_1_Link_status(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "SGMII1G_1_Link_status"
            
            def description(self):
                return "SGMII1G#1 Link status, 0/1->Down/Up"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _SGMII1G_0_Link_status(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "SGMII1G_0_Link_status"
            
            def description(self):
                return "SGMII1G#0 Link status, 0/1->Down/Up"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _SGMII2G5_0_Link_status(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SGMII2G5_0_Link_status"
            
            def description(self):
                return "SGMII2G5#0 Link status, 0/1->Down/Up"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SGMII1G_1_reset_done"] = _AF6CNC0011_RD_TOP_CTR._i_sticky1._SGMII1G_1_reset_done()
            allFields["SGMII1G_0_reset_done"] = _AF6CNC0011_RD_TOP_CTR._i_sticky1._SGMII1G_0_reset_done()
            allFields["SGMII2G5_0_reset_done"] = _AF6CNC0011_RD_TOP_CTR._i_sticky1._SGMII2G5_0_reset_done()
            allFields["SGMII1G_1_Link_status"] = _AF6CNC0011_RD_TOP_CTR._i_sticky1._SGMII1G_1_Link_status()
            allFields["SGMII1G_0_Link_status"] = _AF6CNC0011_RD_TOP_CTR._i_sticky1._SGMII1G_0_Link_status()
            allFields["SGMII2G5_0_Link_status"] = _AF6CNC0011_RD_TOP_CTR._i_sticky1._SGMII2G5_0_Link_status()
            return allFields

import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0011_RD_OCN(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["glbrfm_reg"] = _AF6CNC0011_RD_OCN._glbrfm_reg()
        allRegisters["glblosthr_reg"] = _AF6CNC0011_RD_OCN._glblosthr_reg()
        allRegisters["glblofthr_reg"] = _AF6CNC0011_RD_OCN._glblofthr_reg()
        allRegisters["glbtfm_reg"] = _AF6CNC0011_RD_OCN._glbtfm_reg()
        allRegisters["glbclksel_reg1"] = _AF6CNC0011_RD_OCN._glbclksel_reg1()
        allRegisters["glbclksel_reg2"] = _AF6CNC0011_RD_OCN._glbclksel_reg2()
        allRegisters["glbclklooptime_reg1"] = _AF6CNC0011_RD_OCN._glbclklooptime_reg1()
        allRegisters["glbclklooptime_reg2"] = _AF6CNC0011_RD_OCN._glbclklooptime_reg2()
        allRegisters["glbrdiinsthr_reg"] = _AF6CNC0011_RD_OCN._glbrdiinsthr_reg()
        allRegisters["glbspi_reg"] = _AF6CNC0011_RD_OCN._glbspi_reg()
        allRegisters["glbvpi_reg"] = _AF6CNC0011_RD_OCN._glbvpi_reg()
        allRegisters["glbtpg_reg"] = _AF6CNC0011_RD_OCN._glbtpg_reg()
        allRegisters["glbloop_reg"] = _AF6CNC0011_RD_OCN._glbloop_reg()
        allRegisters["glbds3osdh_reg1"] = _AF6CNC0011_RD_OCN._glbds3osdh_reg1()
        allRegisters["glbds3osdh_reg2"] = _AF6CNC0011_RD_OCN._glbds3osdh_reg2()
        allRegisters["glbrxtohbusctl_reg"] = _AF6CNC0011_RD_OCN._glbrxtohbusctl_reg()
        allRegisters["glbtohbusthr_reg"] = _AF6CNC0011_RD_OCN._glbtohbusthr_reg()
        allRegisters["glbtxtohbusctl_reg"] = _AF6CNC0011_RD_OCN._glbtxtohbusctl_reg()
        allRegisters["spiramctl"] = _AF6CNC0011_RD_OCN._spiramctl()
        allRegisters["spgramctl"] = _AF6CNC0011_RD_OCN._spgramctl()
        allRegisters["demramctl"] = _AF6CNC0011_RD_OCN._demramctl()
        allRegisters["vpiramctl"] = _AF6CNC0011_RD_OCN._vpiramctl()
        allRegisters["pgdemramctl"] = _AF6CNC0011_RD_OCN._pgdemramctl()
        allRegisters["vpgramctl"] = _AF6CNC0011_RD_OCN._vpgramctl()
        allRegisters["upstschstkram"] = _AF6CNC0011_RD_OCN._upstschstkram()
        allRegisters["upstschstaram"] = _AF6CNC0011_RD_OCN._upstschstaram()
        allRegisters["upvtchstkram"] = _AF6CNC0011_RD_OCN._upvtchstkram()
        allRegisters["upvtchstaram"] = _AF6CNC0011_RD_OCN._upvtchstaram()
        allRegisters["adjcntperstsram"] = _AF6CNC0011_RD_OCN._adjcntperstsram()
        allRegisters["adjcntperstkram"] = _AF6CNC0011_RD_OCN._adjcntperstkram()
        allRegisters["stspgstkram"] = _AF6CNC0011_RD_OCN._stspgstkram()
        allRegisters["adjcntpgperstsram"] = _AF6CNC0011_RD_OCN._adjcntpgperstsram()
        allRegisters["vtpgstkram"] = _AF6CNC0011_RD_OCN._vtpgstkram()
        allRegisters["adjcntpgpervtram"] = _AF6CNC0011_RD_OCN._adjcntpgpervtram()
        allRegisters["tohglbk1stbthr_reg"] = _AF6CNC0011_RD_OCN._tohglbk1stbthr_reg()
        allRegisters["tohglbk2stbthr_reg"] = _AF6CNC0011_RD_OCN._tohglbk2stbthr_reg()
        allRegisters["tohglbs1stbthr_reg"] = _AF6CNC0011_RD_OCN._tohglbs1stbthr_reg()
        allRegisters["tohglbrdidetthr_reg"] = _AF6CNC0011_RD_OCN._tohglbrdidetthr_reg()
        allRegisters["tohglbaisdetthr_reg"] = _AF6CNC0011_RD_OCN._tohglbaisdetthr_reg()
        allRegisters["tohglbk1smpthr_reg"] = _AF6CNC0011_RD_OCN._tohglbk1smpthr_reg()
        allRegisters["tohglberrcntmod_reg"] = _AF6CNC0011_RD_OCN._tohglberrcntmod_reg()
        allRegisters["tohglbaffen_reg"] = _AF6CNC0011_RD_OCN._tohglbaffen_reg()
        allRegisters["tohramctl"] = _AF6CNC0011_RD_OCN._tohramctl()
        allRegisters["tohb1errrocnt"] = _AF6CNC0011_RD_OCN._tohb1errrocnt()
        allRegisters["tohb1errr2ccnt"] = _AF6CNC0011_RD_OCN._tohb1errr2ccnt()
        allRegisters["tohb1blkerrrocnt"] = _AF6CNC0011_RD_OCN._tohb1blkerrrocnt()
        allRegisters["tohb1blkerrr2ccnt"] = _AF6CNC0011_RD_OCN._tohb1blkerrr2ccnt()
        allRegisters["tohb2errrocnt"] = _AF6CNC0011_RD_OCN._tohb2errrocnt()
        allRegisters["tohb2errr2ccnt"] = _AF6CNC0011_RD_OCN._tohb2errr2ccnt()
        allRegisters["tohb2blkerrrocnt"] = _AF6CNC0011_RD_OCN._tohb2blkerrrocnt()
        allRegisters["tohb2blkerrr2ccnt"] = _AF6CNC0011_RD_OCN._tohb2blkerrr2ccnt()
        allRegisters["tohreierrrocnt"] = _AF6CNC0011_RD_OCN._tohreierrrocnt()
        allRegisters["tohreierrr2ccnt"] = _AF6CNC0011_RD_OCN._tohreierrr2ccnt()
        allRegisters["tohreiblkerrrocnt"] = _AF6CNC0011_RD_OCN._tohreiblkerrrocnt()
        allRegisters["tohreiblkerrr2ccnt"] = _AF6CNC0011_RD_OCN._tohreiblkerrr2ccnt()
        allRegisters["tohk1monsta"] = _AF6CNC0011_RD_OCN._tohk1monsta()
        allRegisters["tohk2monsta"] = _AF6CNC0011_RD_OCN._tohk2monsta()
        allRegisters["tohs1monsta"] = _AF6CNC0011_RD_OCN._tohs1monsta()
        allRegisters["tohintperalrenbctl"] = _AF6CNC0011_RD_OCN._tohintperalrenbctl()
        allRegisters["tohintsta"] = _AF6CNC0011_RD_OCN._tohintsta()
        allRegisters["tohcursta"] = _AF6CNC0011_RD_OCN._tohcursta()
        allRegisters["tohintperlineenctl1"] = _AF6CNC0011_RD_OCN._tohintperlineenctl1()
        allRegisters["tohintperlineenctl2"] = _AF6CNC0011_RD_OCN._tohintperlineenctl2()
        allRegisters["tohintperline1"] = _AF6CNC0011_RD_OCN._tohintperline1()
        allRegisters["tohintperline2"] = _AF6CNC0011_RD_OCN._tohintperline2()
        allRegisters["tohintperslice"] = _AF6CNC0011_RD_OCN._tohintperslice()
        allRegisters["tfmramctl"] = _AF6CNC0011_RD_OCN._tfmramctl()
        allRegisters["tfmj0insctl"] = _AF6CNC0011_RD_OCN._tfmj0insctl()
        return allRegisters

    class _glbrfm_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Rx Framer Master  Control"
    
        def description(self):
            return "This is the global configuration register for Rx Framer"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _RxFrmAislRdiLEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "RxFrmAislRdiLEn"
            
            def description(self):
                return "Enable/disable the insertion of RDI-L at TOH insertion when AIS_L condition detected. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmTimRdiLEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "RxFrmTimRdiLEn"
            
            def description(self):
                return "Enable/disable the insertion of RDI-L at TOH insertion when TIM condition detected. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmOofRdiLEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "RxFrmOofRdiLEn"
            
            def description(self):
                return "Enable/disable the insertion of RDI-L at TOH insertion when OOF condition detected. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmLofRdiLEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "RxFrmLofRdiLEn"
            
            def description(self):
                return "Enable/disable the insertion of RDI-L at TOH insertion when LOF condition detected. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmLosRdiLEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "RxFrmLosRdiLEn"
            
            def description(self):
                return "Enable/disable the insertion of RDI-L at TOH insertion when LOS condition detected. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmAislAisPEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "RxFrmAislAisPEn"
            
            def description(self):
                return "Enable/disable the insertion of P-AIS at STS pointer interpreter when AIS_L condition detected. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmTimAisPEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "RxFrmTimAisPEn"
            
            def description(self):
                return "Enable/disable the insertion of P-AIS at STS pointer interpreter when TIM condition detected. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmOofAisPEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "RxFrmOofAisPEn"
            
            def description(self):
                return "Enable/disable the insertion of P-AIS at STS pointer interpreter when OOF condition detected. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmLofAisPEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "RxFrmLofAisPEn"
            
            def description(self):
                return "Enable/disable the insertion of P-AIS at STS pointer interpreter when LOF condition detected. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmLosAisPEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RxFrmLosAisPEn"
            
            def description(self):
                return "Enable/disable the insertion of P-AIS at STS pointer interpreter when LOS condition detected. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmLofCfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "RxFrmLofCfg"
            
            def description(self):
                return "Detected LOF mode. 1: OOF counter is reset in In-frame state. 0: OOF counter is only reset when In-frame state exists continuously more than 3 ms"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmLosDetDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "RxFrmLosDetDis"
            
            def description(self):
                return "Enable/Disable detect LOS. 1: Disable 0: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmLosCfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "RxFrmLosCfg"
            
            def description(self):
                return "Detected LOS mode. 1: the LOS declare when all zero or all one detected 0: the LOS declare when all zero detected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmDescrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxFrmDescrEn"
            
            def description(self):
                return "Enable/disable de-scrambling of the Rx coming data stream. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxFrmAislRdiLEn"] = _AF6CNC0011_RD_OCN._glbrfm_reg._RxFrmAislRdiLEn()
            allFields["RxFrmTimRdiLEn"] = _AF6CNC0011_RD_OCN._glbrfm_reg._RxFrmTimRdiLEn()
            allFields["RxFrmOofRdiLEn"] = _AF6CNC0011_RD_OCN._glbrfm_reg._RxFrmOofRdiLEn()
            allFields["RxFrmLofRdiLEn"] = _AF6CNC0011_RD_OCN._glbrfm_reg._RxFrmLofRdiLEn()
            allFields["RxFrmLosRdiLEn"] = _AF6CNC0011_RD_OCN._glbrfm_reg._RxFrmLosRdiLEn()
            allFields["RxFrmAislAisPEn"] = _AF6CNC0011_RD_OCN._glbrfm_reg._RxFrmAislAisPEn()
            allFields["RxFrmTimAisPEn"] = _AF6CNC0011_RD_OCN._glbrfm_reg._RxFrmTimAisPEn()
            allFields["RxFrmOofAisPEn"] = _AF6CNC0011_RD_OCN._glbrfm_reg._RxFrmOofAisPEn()
            allFields["RxFrmLofAisPEn"] = _AF6CNC0011_RD_OCN._glbrfm_reg._RxFrmLofAisPEn()
            allFields["RxFrmLosAisPEn"] = _AF6CNC0011_RD_OCN._glbrfm_reg._RxFrmLosAisPEn()
            allFields["RxFrmLofCfg"] = _AF6CNC0011_RD_OCN._glbrfm_reg._RxFrmLofCfg()
            allFields["RxFrmLosDetDis"] = _AF6CNC0011_RD_OCN._glbrfm_reg._RxFrmLosDetDis()
            allFields["RxFrmLosCfg"] = _AF6CNC0011_RD_OCN._glbrfm_reg._RxFrmLosCfg()
            allFields["RxFrmDescrEn"] = _AF6CNC0011_RD_OCN._glbrfm_reg._RxFrmDescrEn()
            return allFields

    class _glblosthr_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Rx Framer LOS Threshold"
    
        def description(self):
            return "Configure thresholds for LOS detection at receive SONET/SDH framer,There are three thresholds"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000006
            
        def endAddress(self):
            return 0xffffffff

        class _RxFrmNumValMonDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "RxFrmNumValMonDis"
            
            def description(self):
                return "Disable to generate LOS to Rx Framer base on detecting number of valid on Rx data valid. 1: Disable 0: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmNumValMonThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 20
        
            def name(self):
                return "RxFrmNumValMonThr"
            
            def description(self):
                return "Threshold to generate LOS to Rx Framer base on detecting number of valid on Rx data valid.(Threshold = PPM * 1). Default 0x20 ~ 32ppm"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmLosClr2Thr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "RxFrmLosClr2Thr"
            
            def description(self):
                return "Configure the period of time during which there is no period of consecutive data without transition, used for reset no transition counter. The recommended value is 0x8 (~5us)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmLosSetThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "RxFrmLosSetThr"
            
            def description(self):
                return "Configure the value that define the period of time in which there is no transition detected from incoming data from SERDES. The recommended value is 0x28 (~25us)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmLosClr1Thr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxFrmLosClr1Thr"
            
            def description(self):
                return "Configure the period of time during which there is no period of consecutive data without transition, used for counting at INFRM to change state to LOS The recommended value is 0x51 (~50us)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxFrmNumValMonDis"] = _AF6CNC0011_RD_OCN._glblosthr_reg._RxFrmNumValMonDis()
            allFields["RxFrmNumValMonThr"] = _AF6CNC0011_RD_OCN._glblosthr_reg._RxFrmNumValMonThr()
            allFields["RxFrmLosClr2Thr"] = _AF6CNC0011_RD_OCN._glblosthr_reg._RxFrmLosClr2Thr()
            allFields["RxFrmLosSetThr"] = _AF6CNC0011_RD_OCN._glblosthr_reg._RxFrmLosSetThr()
            allFields["RxFrmLosClr1Thr"] = _AF6CNC0011_RD_OCN._glblosthr_reg._RxFrmLosClr1Thr()
            return allFields

    class _glblofthr_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Rx Framer LOF Threshold"
    
        def description(self):
            return "Configure thresholds for LOF detection at receive SONET/SDH framer,There are two thresholds"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000007
            
        def endAddress(self):
            return 0xffffffff

        class _RxFrmLofSetThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "RxFrmLofSetThr"
            
            def description(self):
                return "Configure the OOF time counter threshold for entering LOF state. Resolution of this threshold is one frame."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmLofClrThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxFrmLofClrThr"
            
            def description(self):
                return "Configure the In-frame time counter threshold for exiting LOF state. Resolution of this threshold is one frame."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxFrmLofSetThr"] = _AF6CNC0011_RD_OCN._glblofthr_reg._RxFrmLofSetThr()
            allFields["RxFrmLofClrThr"] = _AF6CNC0011_RD_OCN._glblofthr_reg._RxFrmLofClrThr()
            return allFields

    class _glbtfm_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Tx Framer Control"
    
        def description(self):
            return "This is the global configuration register for the Tx Framer"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000001
            
        def endAddress(self):
            return 0xffffffff

        class _TxLineOofFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "TxLineOofFrc"
            
            def description(self):
                return "Enable/disable force OOF for tx lines."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxLineB1errFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "TxLineB1errFrc"
            
            def description(self):
                return "Enable/disable force B1 error for  tx lines."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxLineScrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxLineScrEn"
            
            def description(self):
                return "Enable/disable scrambling of the Tx data stream. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxLineOofFrc"] = _AF6CNC0011_RD_OCN._glbtfm_reg._TxLineOofFrc()
            allFields["TxLineB1errFrc"] = _AF6CNC0011_RD_OCN._glbtfm_reg._TxLineB1errFrc()
            allFields["TxLineScrEn"] = _AF6CNC0011_RD_OCN._glbtfm_reg._TxLineScrEn()
            return allFields

    class _glbclksel_reg1(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Tx Line clock 6M selection 1"
    
        def description(self):
            return "This is the global configuration register for selecting clock 6M source to transmit Tx line stream (EC1 Id: 0-23)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000a
            
        def endAddress(self):
            return 0xffffffff

        class _TxLineClk6mSel1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxLineClk6mSel1"
            
            def description(self):
                return "Bit #0 for line #0. 1: Clock 6M is soure2 (External) 0: Clock 6M is soure1 (System)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxLineClk6mSel1"] = _AF6CNC0011_RD_OCN._glbclksel_reg1._TxLineClk6mSel1()
            return allFields

    class _glbclksel_reg2(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Tx Line clock 6M selection 2"
    
        def description(self):
            return "This is the global configuration register for selecting clock 6M source to transmit Tx line stream (EC1 Id: 24-47)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000b
            
        def endAddress(self):
            return 0xffffffff

        class _TxLineClk6mSel2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxLineClk6mSel2"
            
            def description(self):
                return "Bit #0 for line #24. 1: Clock 6M is source2 (External) 0: Clock 6M is source1 (System)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxLineClk6mSel2"] = _AF6CNC0011_RD_OCN._glbclksel_reg2._TxLineClk6mSel2()
            return allFields

    class _glbclklooptime_reg1(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Tx Line clock Looptime mode 1"
    
        def description(self):
            return "This is the global configuration register for selecting clock source to transmit Tx line stream (EC1 Id: 0-23) is looptime or selected from 2 sources 6M (sysclk & extclk)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000c
            
        def endAddress(self):
            return 0xffffffff

        class _TxLineClkLoopTime1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxLineClkLoopTime1"
            
            def description(self):
                return "Bit #0 for line #0. 1: Looptime Mode 0: Using TxLineClk6mSel1 for transmit Tx line source clock."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxLineClkLoopTime1"] = _AF6CNC0011_RD_OCN._glbclklooptime_reg1._TxLineClkLoopTime1()
            return allFields

    class _glbclklooptime_reg2(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Tx Line clock Looptime mode 2"
    
        def description(self):
            return "This is the global configuration register for selecting clock source to transmit Tx line stream (EC1 Id: 24-47) is looptime or selected from 2 sources(sysclk & extclk)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000d
            
        def endAddress(self):
            return 0xffffffff

        class _TxLineClkLoopTime2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxLineClkLoopTime2"
            
            def description(self):
                return "Bit #0 for line #24. 1: Looptime Mode 0: Using TxLineClk6mSel2 for transmit Tx line source clock."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxLineClkLoopTime2"] = _AF6CNC0011_RD_OCN._glbclklooptime_reg2._TxLineClkLoopTime2()
            return allFields

    class _glbrdiinsthr_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Tx Framer RDI-L Insertion Threshold Control"
    
        def description(self):
            return "Configure the number of frames in which L-RDI will be inserted when an L-RDI event is triggered. The register contains two numbers"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000009
            
        def endAddress(self):
            return 0xffffffff

        class _TxFrmRdiInsThr2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TxFrmRdiInsThr2"
            
            def description(self):
                return "Threshold 2 for SDH mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxFrmRdiInsThr1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxFrmRdiInsThr1"
            
            def description(self):
                return "Threshold 1 for Sonet mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxFrmRdiInsThr2"] = _AF6CNC0011_RD_OCN._glbrdiinsthr_reg._TxFrmRdiInsThr2()
            allFields["TxFrmRdiInsThr1"] = _AF6CNC0011_RD_OCN._glbrdiinsthr_reg._TxFrmRdiInsThr1()
            return allFields

    class _glbspi_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global STS Pointer Interpreter Control"
    
        def description(self):
            return "This is the global configuration register for the STS Pointer Interpreter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0xffffffff

        class _StsPiHoBusSel2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "StsPiHoBusSel2"
            
            def description(self):
                return "Select bus between SPI slice2  and HOBus slice2  for downstream to PDH. 1: Hobus slice2 selected 0: SPI slice2 (normal) selected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiHoBusSel1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "StsPiHoBusSel1"
            
            def description(self):
                return "Select bus between SPI slice1  and HOBus slice1  for downstream to PDH. 1: Hobus slice1 selected 0: SPI slice1 (normal) selected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPgFlowThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 24
        
            def name(self):
                return "RxPgFlowThresh"
            
            def description(self):
                return "Overflow/underflow threshold to resynchronize read/write pointer."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPgAdjThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 20
        
            def name(self):
                return "RxPgAdjThresh"
            
            def description(self):
                return "Adjustment threshold to make a pointer increment/decrement."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiAisAisPEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "StsPiAisAisPEn"
            
            def description(self):
                return "Enable/Disable forwarding P_AIS when AIS state is detected at STS Pointer interpreter. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiLopAisPEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "StsPiLopAisPEn"
            
            def description(self):
                return "Enable/Disable forwarding P_AIS when LOP state is detected at STS Pointer interpreter. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiMajorMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "StsPiMajorMode"
            
            def description(self):
                return "Majority mode for detecting increment/decrement at STS pointer Interpreter. It is used for rule n of 5. 1: n = 3 0: n = 5"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiNorPtrThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "StsPiNorPtrThresh"
            
            def description(self):
                return "Threshold of number of normal pointers between two contiguous frames within pointer adjustments."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiNdfPtrThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "StsPiNdfPtrThresh"
            
            def description(self):
                return "Threshold of number of contiguous NDF pointers for entering LOP state at FSM."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiBadPtrThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "StsPiBadPtrThresh"
            
            def description(self):
                return "Threshold of number of contiguous invalid pointers for entering LOP state at FSM."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiPohAisType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "StsPiPohAisType"
            
            def description(self):
                return "Enable/disable STS POH defect types to downstream AIS in case of terminating the related STS such as the STS carries VT/TU. [0]: Enable for TIM defect [1]: Enable for Unequiped defect [2]: Enable for VC-AIS [3]: Enable for PLM defect"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["StsPiHoBusSel2"] = _AF6CNC0011_RD_OCN._glbspi_reg._StsPiHoBusSel2()
            allFields["StsPiHoBusSel1"] = _AF6CNC0011_RD_OCN._glbspi_reg._StsPiHoBusSel1()
            allFields["RxPgFlowThresh"] = _AF6CNC0011_RD_OCN._glbspi_reg._RxPgFlowThresh()
            allFields["RxPgAdjThresh"] = _AF6CNC0011_RD_OCN._glbspi_reg._RxPgAdjThresh()
            allFields["StsPiAisAisPEn"] = _AF6CNC0011_RD_OCN._glbspi_reg._StsPiAisAisPEn()
            allFields["StsPiLopAisPEn"] = _AF6CNC0011_RD_OCN._glbspi_reg._StsPiLopAisPEn()
            allFields["StsPiMajorMode"] = _AF6CNC0011_RD_OCN._glbspi_reg._StsPiMajorMode()
            allFields["StsPiNorPtrThresh"] = _AF6CNC0011_RD_OCN._glbspi_reg._StsPiNorPtrThresh()
            allFields["StsPiNdfPtrThresh"] = _AF6CNC0011_RD_OCN._glbspi_reg._StsPiNdfPtrThresh()
            allFields["StsPiBadPtrThresh"] = _AF6CNC0011_RD_OCN._glbspi_reg._StsPiBadPtrThresh()
            allFields["StsPiPohAisType"] = _AF6CNC0011_RD_OCN._glbspi_reg._StsPiPohAisType()
            return allFields

    class _glbvpi_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global VTTU Pointer Interpreter Control"
    
        def description(self):
            return "This is the global configuration register for the VTTU Pointer Interpreter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _VtPiLomAisPEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "VtPiLomAisPEn"
            
            def description(self):
                return "Enable/Disable forwarding AIS when LOM is detected. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiLomInvlCntMod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "VtPiLomInvlCntMod"
            
            def description(self):
                return "H4 monitoring mode. 1: Expected H4 is current frame in the validated sequence plus one. 0: Expected H4 is the last received value plus one."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiLomGoodThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 24
        
            def name(self):
                return "VtPiLomGoodThresh"
            
            def description(self):
                return "Threshold of number of contiguous frames with validated sequence	of multi framers in LOM state for condition to entering IM state."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiLomInvlThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 20
        
            def name(self):
                return "VtPiLomInvlThresh"
            
            def description(self):
                return "Threshold of number of contiguous frames with invalidated sequence of multi framers in IM state  for condition to entering LOM state."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiAisAisPEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "VtPiAisAisPEn"
            
            def description(self):
                return "Enable/Disable forwarding AIS when AIS state is detected at VTTU Pointer interpreter. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiLopAisPEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "VtPiLopAisPEn"
            
            def description(self):
                return "Enable/Disable forwarding AIS when LOP state is detected at VTTU Pointer interpreter. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiMajorMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "VtPiMajorMode"
            
            def description(self):
                return "Majority mode detecting increment/decrement in VTTU pointer Interpreter. It is used for rule n of 5. 1: n = 3 0: n = 5"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiNorPtrThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "VtPiNorPtrThresh"
            
            def description(self):
                return "Threshold of number of normal pointers between two contiguous frames within pointer adjustments."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiNdfPtrThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "VtPiNdfPtrThresh"
            
            def description(self):
                return "Threshold of number of contiguous NDF pointers for entering LOP state at FSM."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiBadPtrThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "VtPiBadPtrThresh"
            
            def description(self):
                return "Threshold of number of contiguous invalid pointers for entering LOP state at FSM."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiPohAisType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "VtPiPohAisType"
            
            def description(self):
                return "Enable/disable VTTU POH defect types to downstream AIS in case of terminating the related VTTU. [0]: Enable for TIM defect [1]: Enable for Un-equipment defect [2]: Enable for VC-AIS [3]: Enable for PLM defect"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["VtPiLomAisPEn"] = _AF6CNC0011_RD_OCN._glbvpi_reg._VtPiLomAisPEn()
            allFields["VtPiLomInvlCntMod"] = _AF6CNC0011_RD_OCN._glbvpi_reg._VtPiLomInvlCntMod()
            allFields["VtPiLomGoodThresh"] = _AF6CNC0011_RD_OCN._glbvpi_reg._VtPiLomGoodThresh()
            allFields["VtPiLomInvlThresh"] = _AF6CNC0011_RD_OCN._glbvpi_reg._VtPiLomInvlThresh()
            allFields["VtPiAisAisPEn"] = _AF6CNC0011_RD_OCN._glbvpi_reg._VtPiAisAisPEn()
            allFields["VtPiLopAisPEn"] = _AF6CNC0011_RD_OCN._glbvpi_reg._VtPiLopAisPEn()
            allFields["VtPiMajorMode"] = _AF6CNC0011_RD_OCN._glbvpi_reg._VtPiMajorMode()
            allFields["VtPiNorPtrThresh"] = _AF6CNC0011_RD_OCN._glbvpi_reg._VtPiNorPtrThresh()
            allFields["VtPiNdfPtrThresh"] = _AF6CNC0011_RD_OCN._glbvpi_reg._VtPiNdfPtrThresh()
            allFields["VtPiBadPtrThresh"] = _AF6CNC0011_RD_OCN._glbvpi_reg._VtPiBadPtrThresh()
            allFields["VtPiPohAisType"] = _AF6CNC0011_RD_OCN._glbvpi_reg._VtPiPohAisType()
            return allFields

    class _glbtpg_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Pointer Generator Control"
    
        def description(self):
            return "This is the global configuration register for the Tx Pointer Generator"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000004
            
        def endAddress(self):
            return 0xffffffff

        class _TxPgNorPtrThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TxPgNorPtrThresh"
            
            def description(self):
                return "Threshold of number of normal pointers between two contiguous frames to make a condition of pointer adjustments."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxPgFlowThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "TxPgFlowThresh"
            
            def description(self):
                return "Overflow/underflow threshold to resynchronize read/write pointer of TxFiFo."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxPgAdjThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxPgAdjThresh"
            
            def description(self):
                return "Adjustment threshold to make a condition of pointer increment/decrement."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxPgNorPtrThresh"] = _AF6CNC0011_RD_OCN._glbtpg_reg._TxPgNorPtrThresh()
            allFields["TxPgFlowThresh"] = _AF6CNC0011_RD_OCN._glbtpg_reg._TxPgFlowThresh()
            allFields["TxPgAdjThresh"] = _AF6CNC0011_RD_OCN._glbtpg_reg._TxPgAdjThresh()
            return allFields

    class _glbloop_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Loopback Control"
    
        def description(self):
            return "This is the global configuration register for loopback modes at OCN block"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000005
            
        def endAddress(self):
            return 0xffffffff

        class _GlbLbEc1Remote(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "GlbLbEc1Remote"
            
            def description(self):
                return "Remote loopback for 48 EC1 lines."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _GlbLbEc1Local(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "GlbLbEc1Local"
            
            def description(self):
                return "Local loopback for 48 EC1 lines ."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _GlbLbHoLocal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "GlbLbHoLocal"
            
            def description(self):
                return "Local loopback for HoBus."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _GlbLbLoLocal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "GlbLbLoLocal"
            
            def description(self):
                return "Local loopback for LoBus."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["GlbLbEc1Remote"] = _AF6CNC0011_RD_OCN._glbloop_reg._GlbLbEc1Remote()
            allFields["GlbLbEc1Local"] = _AF6CNC0011_RD_OCN._glbloop_reg._GlbLbEc1Local()
            allFields["GlbLbHoLocal"] = _AF6CNC0011_RD_OCN._glbloop_reg._GlbLbHoLocal()
            allFields["GlbLbLoLocal"] = _AF6CNC0011_RD_OCN._glbloop_reg._GlbLbLoLocal()
            return allFields

    class _glbds3osdh_reg1(AtRegister.AtRegister):
        def name(self):
            return "OCN Global select path DS3 over SDH config 1"
    
        def description(self):
            return "This is the global configuration register for selecting path DS3 over SDH (STS1 Id: 0-23) at SPI"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000010
            
        def endAddress(self):
            return 0xffffffff

        class _SpiDs3overSdhSel1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SpiDs3overSdhSel1"
            
            def description(self):
                return "Bit #0 for STS1 #0. 1: Path  DS3 over SDH selected. 0: Normal path."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SpiDs3overSdhSel1"] = _AF6CNC0011_RD_OCN._glbds3osdh_reg1._SpiDs3overSdhSel1()
            return allFields

    class _glbds3osdh_reg2(AtRegister.AtRegister):
        def name(self):
            return "OCN Global select path DS3 over SDH config 2"
    
        def description(self):
            return "This is the global configuration register for selecting path DS3 over SDH (STS1 Id: 24-47) at SPI"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000011
            
        def endAddress(self):
            return 0xffffffff

        class _SpiDs3overSdhSel2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SpiDs3overSdhSel2"
            
            def description(self):
                return "Bit #0 for STS1 #24. 1: Path  DS3 over SDH selected. 0: Normal path."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SpiDs3overSdhSel2"] = _AF6CNC0011_RD_OCN._glbds3osdh_reg2._SpiDs3overSdhSel2()
            return allFields

    class _glbrxtohbusctl_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Rx_TOHBUS K Byte Control"
    
        def description(self):
            return "Configure TOHBUS for K Bytes at RX OCN."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000014
            
        def endAddress(self):
            return 0xffffffff

        class _RxTohBusKextCtl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxTohBusKextCtl"
            
            def description(self):
                return "K Byte Configuration for 24 Lines at Rx OCN. Each line use 1 bit to enable/disable Masking FF to Rx_OHBUS when Section alarm happen at Rx OCN. 1: Disable. 0: Enable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxTohBusKextCtl"] = _AF6CNC0011_RD_OCN._glbrxtohbusctl_reg._RxTohBusKextCtl()
            return allFields

    class _glbtohbusthr_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global TOHBUS K_Byte Threshold"
    
        def description(self):
            return "Configure TOHBUS for K_extention Bytes."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000015
            
        def endAddress(self):
            return 0xffffffff

        class _RxTohBusK2StbThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RxTohBusK2StbThr"
            
            def description(self):
                return "Stable threshold Configuration for K2 Byte."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxTohBusK1StbThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxTohBusK1StbThr"
            
            def description(self):
                return "Stable threshold Configuration for K1 Byte."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxTohBusK2StbThr"] = _AF6CNC0011_RD_OCN._glbtohbusthr_reg._RxTohBusK2StbThr()
            allFields["RxTohBusK1StbThr"] = _AF6CNC0011_RD_OCN._glbtohbusthr_reg._RxTohBusK1StbThr()
            return allFields

    class _glbtxtohbusctl_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Tx_TOHBUS K Byte Control"
    
        def description(self):
            return "Configure TOHBUS for K Bytes at TX OCN."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000016
            
        def endAddress(self):
            return 0xffffffff

        class _TxTohBusKextCtl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxTohBusKextCtl"
            
            def description(self):
                return "K Byte Configuration for 24 Lines at Rx OCN. Each line use 1 bit to enable/disable propagation suppression RDI,AIS in K2[2:0] at Tx OCN. Default is enable this function - when detect RDI/AIS at K2[2:0] from OHBUS, we must overwrite 3'b000 into K2[2:0]. 1: Disable. 0: Enable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxTohBusKextCtl"] = _AF6CNC0011_RD_OCN._glbtxtohbusctl_reg._TxTohBusKextCtl()
            return allFields

    class _spiramctl(AtRegister.AtRegister):
        def name(self):
            return "OCN STS Pointer Interpreter Per Channel Control"
    
        def description(self):
            return "Each register is used to configure for STS pointer interpreter engines of the STS-1/VC-3. Backdoor		: irxpp_stspp_inst.irxpp_stspp[0].rxpp_stspiramctl.array"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x22000 + 512*SliceId + StsId"
            
        def startAddress(self):
            return 0x00022000
            
        def endAddress(self):
            return 0x00022e2f

        class _StsPiChkLom(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "StsPiChkLom"
            
            def description(self):
                return "Enable/disable LOM checking. This field will be set to 1 when payload type of VC3/VC4 includes any VC11/VC12 1: Enable. 0: Disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiSSDetPatt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "StsPiSSDetPatt"
            
            def description(self):
                return "Configure pattern SS bits that is used to compare with the extracted SS bits from receive direction."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiAisFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "StsPiAisFrc"
            
            def description(self):
                return "Forcing SFM to AIS state. 1: Force 0: Not force"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiSSDetEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "StsPiSSDetEn"
            
            def description(self):
                return "Enable/disable checking SS bits in STSPI state machine. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiAdjRule(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "StsPiAdjRule"
            
            def description(self):
                return "Configure the rule for detecting adjustment condition. 1: The n of 5 rule is selected. This mode is applied for SDH mode 0: The 8 of 10 rule is selected. This mode is applied for SONET mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["StsPiChkLom"] = _AF6CNC0011_RD_OCN._spiramctl._StsPiChkLom()
            allFields["StsPiSSDetPatt"] = _AF6CNC0011_RD_OCN._spiramctl._StsPiSSDetPatt()
            allFields["StsPiAisFrc"] = _AF6CNC0011_RD_OCN._spiramctl._StsPiAisFrc()
            allFields["StsPiSSDetEn"] = _AF6CNC0011_RD_OCN._spiramctl._StsPiSSDetEn()
            allFields["StsPiAdjRule"] = _AF6CNC0011_RD_OCN._spiramctl._StsPiAdjRule()
            return allFields

    class _spgramctl(AtRegister.AtRegister):
        def name(self):
            return "OCN STS Pointer Generator Per Channel Control"
    
        def description(self):
            return "Each register is used to configure for STS pointer Generator engines. Backdoor		: itxpp_stspp_inst.itxpp_stspp[0].txpg_stspgctl.array"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x23000 + 512*SliceId + StsId"
            
        def startAddress(self):
            return 0x00023000
            
        def endAddress(self):
            return 0x00023e2f

        class _StsPiB3BipErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "StsPiB3BipErrFrc"
            
            def description(self):
                return "Forcing B3 Bip error. 1: Force 0: Not force"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiLopFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "StsPiLopFrc"
            
            def description(self):
                return "Forcing LOP. 1: Force 0: Not force"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiUeqFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "StsPiUeqFrc"
            
            def description(self):
                return "Forcing SFM to UEQ state. 1: Force 0: Not force"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiAisFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "StsPiAisFrc"
            
            def description(self):
                return "Forcing SFM to AIS state. 1: Force 0: Not force"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiSSInsPatt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "StsPiSSInsPatt"
            
            def description(self):
                return "Configure pattern SS bits that is used to insert to pointer value."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiSSInsEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "StsPiSSInsEn"
            
            def description(self):
                return "Enable/disable SS bits insertion. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiPohIns(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "StsPiPohIns"
            
            def description(self):
                return "Enable/disable POH Insertion. High to enable insertion of POH."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["StsPiB3BipErrFrc"] = _AF6CNC0011_RD_OCN._spgramctl._StsPiB3BipErrFrc()
            allFields["StsPiLopFrc"] = _AF6CNC0011_RD_OCN._spgramctl._StsPiLopFrc()
            allFields["StsPiUeqFrc"] = _AF6CNC0011_RD_OCN._spgramctl._StsPiUeqFrc()
            allFields["StsPiAisFrc"] = _AF6CNC0011_RD_OCN._spgramctl._StsPiAisFrc()
            allFields["StsPiSSInsPatt"] = _AF6CNC0011_RD_OCN._spgramctl._StsPiSSInsPatt()
            allFields["StsPiSSInsEn"] = _AF6CNC0011_RD_OCN._spgramctl._StsPiSSInsEn()
            allFields["StsPiPohIns"] = _AF6CNC0011_RD_OCN._spgramctl._StsPiPohIns()
            return allFields

    class _demramctl(AtRegister.AtRegister):
        def name(self):
            return "OCN RXPP Per STS payload Control"
    
        def description(self):
            return "Each register is used to configure VT payload mode per STS. Backdoor		: irxpp_vtpp_inst.irxpp_vtpp[0].rxpp_demramctl.array"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x40000 + 16384*SliceId + StsId"
            
        def startAddress(self):
            return 0x00040000
            
        def endAddress(self):
            return 0x0005402f

        class _PiDemStsTerm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PiDemStsTerm"
            
            def description(self):
                return "Enable to terminate the related STS/VC. It means that STS POH defects related to the STS/VC to generate AIS to downstream. Must be set to 1. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PiDemSpeType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 14
        
            def name(self):
                return "PiDemSpeType"
            
            def description(self):
                return "Configure types of SPE. 0: Disable processing pointers of all VT/TUs in this SPE. 1: VC-3 type or STS SPE containing VT/TU exception TU-3 2: TUG-3 type containing VT/TU exception TU-3 3: TUG-3 type containing TU-3."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PiDemTug26Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PiDemTug26Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #6. 0: TU11 1: TU12"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PiDemTug25Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "PiDemTug25Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #5."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PiDemTug24Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PiDemTug24Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #4."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PiDemTug23Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PiDemTug23Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #3."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PiDemTug22Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PiDemTug22Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #2."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PiDemTug21Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PiDemTug21Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #1."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PiDemTug20Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PiDemTug20Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #0."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PiDemStsTerm"] = _AF6CNC0011_RD_OCN._demramctl._PiDemStsTerm()
            allFields["PiDemSpeType"] = _AF6CNC0011_RD_OCN._demramctl._PiDemSpeType()
            allFields["PiDemTug26Type"] = _AF6CNC0011_RD_OCN._demramctl._PiDemTug26Type()
            allFields["PiDemTug25Type"] = _AF6CNC0011_RD_OCN._demramctl._PiDemTug25Type()
            allFields["PiDemTug24Type"] = _AF6CNC0011_RD_OCN._demramctl._PiDemTug24Type()
            allFields["PiDemTug23Type"] = _AF6CNC0011_RD_OCN._demramctl._PiDemTug23Type()
            allFields["PiDemTug22Type"] = _AF6CNC0011_RD_OCN._demramctl._PiDemTug22Type()
            allFields["PiDemTug21Type"] = _AF6CNC0011_RD_OCN._demramctl._PiDemTug21Type()
            allFields["PiDemTug20Type"] = _AF6CNC0011_RD_OCN._demramctl._PiDemTug20Type()
            return allFields

    class _vpiramctl(AtRegister.AtRegister):
        def name(self):
            return "OCN VTTU Pointer Interpreter Per Channel Control"
    
        def description(self):
            return "Each register is used to configure for VTTU pointer interpreter engines. Backdoor		: irxpp_vtpp_inst.irxpp_vtpp[0].rxpp_vpiramctl.array"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x40800 + 16384*SliceId + 32*StsId + 4*VtgId + VtId"
            
        def startAddress(self):
            return 0x00040800
            
        def endAddress(self):
            return 0x00054fff

        class _VtPiLoTerm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "VtPiLoTerm"
            
            def description(self):
                return "Enable to terminate the related VTTU. It means that VTTU POH defects related to the VTTU to generate AIS to downstream."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiAisFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "VtPiAisFrc"
            
            def description(self):
                return "Forcing SFM to AIS state. 1: Force 0: Not force"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiSSDetPatt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "VtPiSSDetPatt"
            
            def description(self):
                return "Configure pattern SS bits that is used to compare with the extracted SS bits from receive direction."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiSSDetEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "VtPiSSDetEn"
            
            def description(self):
                return "Enable/disable checking SS bits in PI State Machine. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiAdjRule(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "VtPiAdjRule"
            
            def description(self):
                return "Configure the rule for detecting adjustment condition. 1: The n of 5 rule is selected. This mode is applied for SDH mode 0: The 8 of 10 rule is selected. This mode is applied for SONET mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["VtPiLoTerm"] = _AF6CNC0011_RD_OCN._vpiramctl._VtPiLoTerm()
            allFields["VtPiAisFrc"] = _AF6CNC0011_RD_OCN._vpiramctl._VtPiAisFrc()
            allFields["VtPiSSDetPatt"] = _AF6CNC0011_RD_OCN._vpiramctl._VtPiSSDetPatt()
            allFields["VtPiSSDetEn"] = _AF6CNC0011_RD_OCN._vpiramctl._VtPiSSDetEn()
            allFields["VtPiAdjRule"] = _AF6CNC0011_RD_OCN._vpiramctl._VtPiAdjRule()
            return allFields

    class _pgdemramctl(AtRegister.AtRegister):
        def name(self):
            return "OCN TXPP Per STS Multiplexing Control"
    
        def description(self):
            return "Each register is used to configure VT payload mode per STS at Tx pointer generator. Backdoor		: itxpp_vtpp_inst.itxpp_vtpp[0].txpg_pgdmctl.array"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x60000 + 16384*SliceId + StsId"
            
        def startAddress(self):
            return 0x00060000
            
        def endAddress(self):
            return 0x0007402f

        class _PgDemSpeType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 14
        
            def name(self):
                return "PgDemSpeType"
            
            def description(self):
                return "Configure types of SPE. 0: Disable processing pointers of all VT/TUs in this SPE. 1: VC-3 type or STS SPE containing VT/TU exception TU-3 2: TUG-3 type containing VT/TU exception TU-3 3: TUG-3 type containing TU-3."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PgDemTug26Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PgDemTug26Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #6. 0: TU11 1: TU12"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PgDemTug25Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "PgDemTug25Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #5."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PgDemTug24Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PgDemTug24Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #4."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PgDemTug23Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PgDemTug23Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #3."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PgDemTug22Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PgDemTug22Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #2."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PgDemTug21Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PgDemTug21Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #1."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PgDemTug20Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PgDemTug20Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #0."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PgDemSpeType"] = _AF6CNC0011_RD_OCN._pgdemramctl._PgDemSpeType()
            allFields["PgDemTug26Type"] = _AF6CNC0011_RD_OCN._pgdemramctl._PgDemTug26Type()
            allFields["PgDemTug25Type"] = _AF6CNC0011_RD_OCN._pgdemramctl._PgDemTug25Type()
            allFields["PgDemTug24Type"] = _AF6CNC0011_RD_OCN._pgdemramctl._PgDemTug24Type()
            allFields["PgDemTug23Type"] = _AF6CNC0011_RD_OCN._pgdemramctl._PgDemTug23Type()
            allFields["PgDemTug22Type"] = _AF6CNC0011_RD_OCN._pgdemramctl._PgDemTug22Type()
            allFields["PgDemTug21Type"] = _AF6CNC0011_RD_OCN._pgdemramctl._PgDemTug21Type()
            allFields["PgDemTug20Type"] = _AF6CNC0011_RD_OCN._pgdemramctl._PgDemTug20Type()
            return allFields

    class _vpgramctl(AtRegister.AtRegister):
        def name(self):
            return "OCN VTTU Pointer Generator Per Channel Control"
    
        def description(self):
            return "Each register is used to configure for VTTU pointer Generator engines. Backdoor		: itxpp_vtpp_inst.itxpp_vtpp[0].txpg_pgctl.array"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x60800 + 16384*SliceId + 32*StsId + 4*VtgId + VtId"
            
        def startAddress(self):
            return 0x00060800
            
        def endAddress(self):
            return 0x00074fff

        class _VtPgBipErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "VtPgBipErrFrc"
            
            def description(self):
                return "Forcing Bip error. 1: Force 0: Not force"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPgLopFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "VtPgLopFrc"
            
            def description(self):
                return "Forcing LOP. 1: Force 0: Not force"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPgUeqFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "VtPgUeqFrc"
            
            def description(self):
                return "Forcing SFM to UEQ state. 1: Force 0: Not force"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPgAisFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "VtPgAisFrc"
            
            def description(self):
                return "Forcing SFM to AIS state. 1: Force 0: Not force"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPgSSInsPatt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "VtPgSSInsPatt"
            
            def description(self):
                return "Configure pattern SS bits that is used to insert to Pointer value."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPgSSInsEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "VtPgSSInsEn"
            
            def description(self):
                return "Enable/disable SS bits insertion. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPgPohIns(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "VtPgPohIns"
            
            def description(self):
                return "Enable/ disable POH Insertion. High to enable insertion of POH."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["VtPgBipErrFrc"] = _AF6CNC0011_RD_OCN._vpgramctl._VtPgBipErrFrc()
            allFields["VtPgLopFrc"] = _AF6CNC0011_RD_OCN._vpgramctl._VtPgLopFrc()
            allFields["VtPgUeqFrc"] = _AF6CNC0011_RD_OCN._vpgramctl._VtPgUeqFrc()
            allFields["VtPgAisFrc"] = _AF6CNC0011_RD_OCN._vpgramctl._VtPgAisFrc()
            allFields["VtPgSSInsPatt"] = _AF6CNC0011_RD_OCN._vpgramctl._VtPgSSInsPatt()
            allFields["VtPgSSInsEn"] = _AF6CNC0011_RD_OCN._vpgramctl._VtPgSSInsEn()
            allFields["VtPgPohIns"] = _AF6CNC0011_RD_OCN._vpgramctl._VtPgPohIns()
            return allFields

    class _upstschstkram(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx STS/VC per Alarm Interrupt Status"
    
        def description(self):
            return "This is the per Alarm interrupt status of STS/VC pointer interpreter.  %% Each register is used to store 6 sticky bits for 6 alarms in the STS/VC."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x22140 + 512*SliceId + StsId"
            
        def startAddress(self):
            return 0x00022140
            
        def endAddress(self):
            return 0x00022f6f

        class _StsPiStsNewDetIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "StsPiStsNewDetIntr"
            
            def description(self):
                return "Set to 1 while an New Pointer Detection event is detected at STS/VC pointer interpreter. This event doesn't raise interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiStsNdfIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "StsPiStsNdfIntr"
            
            def description(self):
                return "Set to 1 while an NDF event is detected at STS/VC pointer interpreter. This event doesn't raise interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiCepUneqStatePChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "StsPiCepUneqStatePChgIntr"
            
            def description(self):
                return "Set to 1  while there is change in CEP Un-equip Path in the related STS/VC to generate an interrupt. Read the OCN Rx STS/VC per Alarm Current Status register of the related STS/VC to know the STS/VC whether in CEP Un-equip Path state or not."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiStsAISStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "StsPiStsAISStateChgIntr"
            
            def description(self):
                return "Set to 1 while there is change in AIS state in the related STS/VC to generate an interrupt. Read the OCN Rx STS/VC per Alarm Current Status register of the related STS/VC to know the STS/VC whether in AIS state or not."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiStsLopStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "StsPiStsLopStateChgIntr"
            
            def description(self):
                return "Set 1 to while there is change in LOP state in the related STS/VC to generate an interrupt. Read the OCN Rx STS/VC per Alarm Current Status register of the related STS/VC to know the STS/VC whether in LOP state or not."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["StsPiStsNewDetIntr"] = _AF6CNC0011_RD_OCN._upstschstkram._StsPiStsNewDetIntr()
            allFields["StsPiStsNdfIntr"] = _AF6CNC0011_RD_OCN._upstschstkram._StsPiStsNdfIntr()
            allFields["StsPiCepUneqStatePChgIntr"] = _AF6CNC0011_RD_OCN._upstschstkram._StsPiCepUneqStatePChgIntr()
            allFields["StsPiStsAISStateChgIntr"] = _AF6CNC0011_RD_OCN._upstschstkram._StsPiStsAISStateChgIntr()
            allFields["StsPiStsLopStateChgIntr"] = _AF6CNC0011_RD_OCN._upstschstkram._StsPiStsLopStateChgIntr()
            return allFields

    class _upstschstaram(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx STS/VC per Alarm Current Status"
    
        def description(self):
            return "This is the per Alarm current status of STS/VC pointer interpreter.  %% Each register is used to store 3 bits to store current status of 3 alarms in the STS/VC."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x22180 + 512*SliceId + StsId"
            
        def startAddress(self):
            return 0x00022180
            
        def endAddress(self):
            return 0x00022faf

        class _StsPiStsCepUneqPCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "StsPiStsCepUneqPCurStatus"
            
            def description(self):
                return "CEP Un-eqip Path current status in the related STS/VC. When it changes for 0 to 1 or vice versa, the  StsPiStsCepUeqPStateChgIntr bit in the OCN Rx STS/VC per Alarm Interrupt Status register of the related STS/VC is set."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiStsAisCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "StsPiStsAisCurStatus"
            
            def description(self):
                return "AIS current status in the related STS/VC. When it changes for 0 to 1 or vice versa, the  StsPiStsAISStateChgIntr bit in the OCN Rx STS/VC per Alarm Interrupt Status register of the related STS/VC is set."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiStsLopCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "StsPiStsLopCurStatus"
            
            def description(self):
                return "LOP current status in the related STS/VC. When it changes for 0 to 1 or vice versa, the  StsPiStsLopStateChgIntr bit in the OCN Rx STS/VC per Alarm Interrupt Status register of the related STS/VC is set."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["StsPiStsCepUneqPCurStatus"] = _AF6CNC0011_RD_OCN._upstschstaram._StsPiStsCepUneqPCurStatus()
            allFields["StsPiStsAisCurStatus"] = _AF6CNC0011_RD_OCN._upstschstaram._StsPiStsAisCurStatus()
            allFields["StsPiStsLopCurStatus"] = _AF6CNC0011_RD_OCN._upstschstaram._StsPiStsLopCurStatus()
            return allFields

    class _upvtchstkram(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx VT/TU per Alarm Interrupt Status"
    
        def description(self):
            return "This is the per Alarm interrupt status of VT/TU pointer interpreter . Each register is used to store 5 sticky bits for 5 alarms in the VT/TU."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x42800 + 16384*SliceId + 32*StsId + 4*VtgId + VtId"
            
        def startAddress(self):
            return 0x00042800
            
        def endAddress(self):
            return 0x00056fff

        class _VtPiStsNewDetIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "VtPiStsNewDetIntr"
            
            def description(self):
                return "Set to 1 while an New Pointer Detection event is detected at VT/TU pointer interpreter. This event doesn't raise interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiStsNdfIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "VtPiStsNdfIntr"
            
            def description(self):
                return "Set to 1 while an NDF event is detected at VT/TU pointer interpreter. This event doesn't raise interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiStsCepUneqVStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "VtPiStsCepUneqVStateChgIntr"
            
            def description(self):
                return "Set 1 to while there is change in Unequip state in the related VT/TU to generate an interrupt. Read the OCN Rx VT/TU per Alarm Current Status register of the related VT/TU to know the VT/TU whether in Uneqip state or not."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiStsAISStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "VtPiStsAISStateChgIntr"
            
            def description(self):
                return "Set 1 to while there is change in AIS state in the related VT/TU to generate an interrupt. Read the OCN Rx VT/TU per Alarm Current Status register of the related VT/TU to know the VT/TU whether in LOP state or not."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiStsLopStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "VtPiStsLopStateChgIntr"
            
            def description(self):
                return "Set 1 to while there is change in LOP state in the related VT/TU to generate an interrupt. Read the OCN Rx VT/TU per Alarm Current Status register of the related VT/TU to know the VT/TU whether in AIS state or not."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["VtPiStsNewDetIntr"] = _AF6CNC0011_RD_OCN._upvtchstkram._VtPiStsNewDetIntr()
            allFields["VtPiStsNdfIntr"] = _AF6CNC0011_RD_OCN._upvtchstkram._VtPiStsNdfIntr()
            allFields["VtPiStsCepUneqVStateChgIntr"] = _AF6CNC0011_RD_OCN._upvtchstkram._VtPiStsCepUneqVStateChgIntr()
            allFields["VtPiStsAISStateChgIntr"] = _AF6CNC0011_RD_OCN._upvtchstkram._VtPiStsAISStateChgIntr()
            allFields["VtPiStsLopStateChgIntr"] = _AF6CNC0011_RD_OCN._upvtchstkram._VtPiStsLopStateChgIntr()
            return allFields

    class _upvtchstaram(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx VT/TU per Alarm Current Status"
    
        def description(self):
            return "This is the per Alarm current status of VT/TU pointer interpreter. Each register is used to store 3 bits to store current status of 3 alarms in the VT/TU."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x43000 + 16384*SliceId + 32*StsId + 4*VtgId + VtId"
            
        def startAddress(self):
            return 0x00043000
            
        def endAddress(self):
            return 0x00057fff

        class _VtPiStsCepUneqCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "VtPiStsCepUneqCurStatus"
            
            def description(self):
                return "Unequip current status in the related VT/TU. When it changes for 0 to 1 or vice versa, the VtPiStsCepUneqVStateChgIntr bit in the OCN Rx VT/TU per Alarm Interrupt Status register of the related VT/TU is set."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiStsAisCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "VtPiStsAisCurStatus"
            
            def description(self):
                return "AIS current status in the related VT/TU. When it changes for 0 to 1 or vice versa, the VtPiStsAISStateChgIntr bit in the OCN Rx VT/TU per Alarm Interrupt Status register of the related VT/TU is set."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiStsLopCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "VtPiStsLopCurStatus"
            
            def description(self):
                return "LOP current status in the related VT/TU. When it changes for 0 to 1 or vice versa, the VtPiStsLopStateChgIntr bit in the OCN Rx VT/TU per Alarm Interrupt Status register of the related VT/TU is set."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["VtPiStsCepUneqCurStatus"] = _AF6CNC0011_RD_OCN._upvtchstaram._VtPiStsCepUneqCurStatus()
            allFields["VtPiStsAisCurStatus"] = _AF6CNC0011_RD_OCN._upvtchstaram._VtPiStsAisCurStatus()
            allFields["VtPiStsLopCurStatus"] = _AF6CNC0011_RD_OCN._upvtchstaram._VtPiStsLopCurStatus()
            return allFields

    class _adjcntperstsram(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx PP STS/VC Pointer interpreter pointer adjustment per channel counter"
    
        def description(self):
            return "Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in STS/VC pointer interpreter. These counters are in saturation mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x22080 + 512*SliceId + 64*ReadOnlyMode + 32*AdjMode + StsId"
            
        def startAddress(self):
            return 0x00022080
            
        def endAddress(self):
            return 0x00022eaf

        class _RxPpStsPiPtAdjCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPpStsPiPtAdjCnt"
            
            def description(self):
                return "The pointer Increment counter or decrease counter. Bit [6] of address is used to indicate that the counter is pointer increment or decrement counter. The counter will stop at maximum value (0x3FFFF)."
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPpStsPiPtAdjCnt"] = _AF6CNC0011_RD_OCN._adjcntperstsram._RxPpStsPiPtAdjCnt()
            return allFields

    class _adjcntperstkram(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx PP VT/TU Pointer interpreter pointer adjustment per channel counter"
    
        def description(self):
            return "Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in VT/TU pointer interpreter. These counters are in saturation mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x41000 + 16384*SliceId + 2048*ReadOnlyMode + 1024*AdjMode + 32*StsId + 4*VtgId + VtId"
            
        def startAddress(self):
            return 0x00041000
            
        def endAddress(self):
            return 0x00055fff

        class _RxPpStsPiPtAdjCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPpStsPiPtAdjCnt"
            
            def description(self):
                return "The pointer Increment counter or decrease counter. Bit [11] of address is used to indicate that the counter is pointer increment or decrement counter. The counter will stop at maximum value (0x3FFFF)."
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPpStsPiPtAdjCnt"] = _AF6CNC0011_RD_OCN._adjcntperstkram._RxPpStsPiPtAdjCnt()
            return allFields

    class _stspgstkram(AtRegister.AtRegister):
        def name(self):
            return "OCN TxPg STS per Alarm Interrupt Status"
    
        def description(self):
            return "This is the per Alarm interrupt status of STS pointer generator . Each register is used to store 3 sticky bits for 3 alarms"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x23140 + 512*SliceId + StsId"
            
        def startAddress(self):
            return 0x00023140
            
        def endAddress(self):
            return 0x00023f6f

        class _StsPgAisIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "StsPgAisIntr"
            
            def description(self):
                return "Set to 1 while an AIS status event (at h2pos) is detected at Tx STS Pointer Generator."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPgFiFoOvfIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "StsPgFiFoOvfIntr"
            
            def description(self):
                return "Set to 1 while an FIFO Overflowed event is detected at Tx STS Pointer Generator."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPgNdfIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "StsPgNdfIntr"
            
            def description(self):
                return "Set to 1 while an NDF status event (at h2pos) is detected at Tx STS Pointer Generator."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["StsPgAisIntr"] = _AF6CNC0011_RD_OCN._stspgstkram._StsPgAisIntr()
            allFields["StsPgFiFoOvfIntr"] = _AF6CNC0011_RD_OCN._stspgstkram._StsPgFiFoOvfIntr()
            allFields["StsPgNdfIntr"] = _AF6CNC0011_RD_OCN._stspgstkram._StsPgNdfIntr()
            return allFields

    class _adjcntpgperstsram(AtRegister.AtRegister):
        def name(self):
            return "OCN TxPg STS pointer adjustment per channel counter"
    
        def description(self):
            return "Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in STS pointer generator. These counters are in saturation mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x23080 + 512*SliceId + 64*ReadOnlyMode + 32*AdjMode + StsId"
            
        def startAddress(self):
            return 0x00023080
            
        def endAddress(self):
            return 0x00023eaf

        class _StsPgPtAdjCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "StsPgPtAdjCnt"
            
            def description(self):
                return "The pointer Increment counter or decrease counter. Bit [11] of address is used to indicate that the counter is pointer increment or decrement counter. The counter will stop at maximum value (0x3FFFF)."
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["StsPgPtAdjCnt"] = _AF6CNC0011_RD_OCN._adjcntpgperstsram._StsPgPtAdjCnt()
            return allFields

    class _vtpgstkram(AtRegister.AtRegister):
        def name(self):
            return "OCN TxPg VTTU per Alarm Interrupt Status"
    
        def description(self):
            return "This is the per Alarm interrupt status of STS/VT/TU pointer generator . Each register is used to store 3 sticky bits for 3 alarms"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x62800 + 16384*SliceId + 32*StsId + 4*VtgId + VtId"
            
        def startAddress(self):
            return 0x00062800
            
        def endAddress(self):
            return 0x00076fff

        class _VtPgAisIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "VtPgAisIntr"
            
            def description(self):
                return "Set to 1 while an AIS status event (at h2pos) is detected at Tx VTTU Pointer Generator."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPgFiFoOvfIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "VtPgFiFoOvfIntr"
            
            def description(self):
                return "Set to 1 while an FIFO Overflowed event is detected at Tx VTTU Pointer Generator."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPgNdfIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "VtPgNdfIntr"
            
            def description(self):
                return "Set to 1 while an NDF status event (at h2pos) is detected at Tx VTTU Pointer Generator."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["VtPgAisIntr"] = _AF6CNC0011_RD_OCN._vtpgstkram._VtPgAisIntr()
            allFields["VtPgFiFoOvfIntr"] = _AF6CNC0011_RD_OCN._vtpgstkram._VtPgFiFoOvfIntr()
            allFields["VtPgNdfIntr"] = _AF6CNC0011_RD_OCN._vtpgstkram._VtPgNdfIntr()
            return allFields

    class _adjcntpgpervtram(AtRegister.AtRegister):
        def name(self):
            return "OCN TxPg VTTU pointer adjustment per channel counter"
    
        def description(self):
            return "Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in VTTU pointer generator. These counters are in saturation mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x61000 + 16384*SliceId + 2048*ReadOnlyMode + 1024*AdjMode + 32*StsId + 4*VtgId + VtId"
            
        def startAddress(self):
            return 0x00061000
            
        def endAddress(self):
            return 0x00075fff

        class _VtpgPtAdjCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "VtpgPtAdjCnt"
            
            def description(self):
                return "The pointer Increment counter or decrease counter. Bit [11] of address is used to indicate that the counter is pointer increment or decrement counter. The counter will stop at maximum value (0x3FFFF)."
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["VtpgPtAdjCnt"] = _AF6CNC0011_RD_OCN._adjcntpgpervtram._VtpgPtAdjCnt()
            return allFields

    class _tohglbk1stbthr_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Global K1 Stable Monitoring Threshold Control"
    
        def description(self):
            return "Configure thresholds for monitoring change of K1 state at TOH monitoring. There are two thresholds for each kind of threshold."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00024000
            
        def endAddress(self):
            return 0xffffffff

        class _TohK1StbThr2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TohK1StbThr2"
            
            def description(self):
                return "The second threshold for detecting stable K1 status."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TohK1StbThr1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TohK1StbThr1"
            
            def description(self):
                return "The first threshold for detecting stable K1 status."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TohK1StbThr2"] = _AF6CNC0011_RD_OCN._tohglbk1stbthr_reg._TohK1StbThr2()
            allFields["TohK1StbThr1"] = _AF6CNC0011_RD_OCN._tohglbk1stbthr_reg._TohK1StbThr1()
            return allFields

    class _tohglbk2stbthr_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Global K2 Stable Monitoring Threshold Control"
    
        def description(self):
            return "Configure thresholds for monitoring change of K2 state at TOH monitoring. There are two thresholds for each kind of threshold."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00024001
            
        def endAddress(self):
            return 0xffffffff

        class _TohK2StbThr2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TohK2StbThr2"
            
            def description(self):
                return "The second threshold for detecting stable K2 status."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TohK2StbThr1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TohK2StbThr1"
            
            def description(self):
                return "The first threshold for detecting stable K2 status."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TohK2StbThr2"] = _AF6CNC0011_RD_OCN._tohglbk2stbthr_reg._TohK2StbThr2()
            allFields["TohK2StbThr1"] = _AF6CNC0011_RD_OCN._tohglbk2stbthr_reg._TohK2StbThr1()
            return allFields

    class _tohglbs1stbthr_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Global S1 Stable Monitoring Threshold Control"
    
        def description(self):
            return "Configure thresholds for monitoring change of S1 state at TOH monitoring. There are two thresholds for each kind of threshold."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00024002
            
        def endAddress(self):
            return 0xffffffff

        class _TohS1StbThr2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TohS1StbThr2"
            
            def description(self):
                return "The second threshold for detecting stable S1 status."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TohS1StbThr1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TohS1StbThr1"
            
            def description(self):
                return "The first threshold for detecting stable S1 status."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TohS1StbThr2"] = _AF6CNC0011_RD_OCN._tohglbs1stbthr_reg._TohS1StbThr2()
            allFields["TohS1StbThr1"] = _AF6CNC0011_RD_OCN._tohglbs1stbthr_reg._TohS1StbThr1()
            return allFields

    class _tohglbrdidetthr_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Global RDI_L Detecting Threshold Control"
    
        def description(self):
            return "Configure thresholds for detecting RDI_L at TOH monitoring. There are two thresholds for each kind of threshold."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00024003
            
        def endAddress(self):
            return 0xffffffff

        class _TohRdiDetThr2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TohRdiDetThr2"
            
            def description(self):
                return "The second threshold for detecting RDI_L."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TohRdiDetThr1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TohRdiDetThr1"
            
            def description(self):
                return "The first threshold for detecting RDI_L."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TohRdiDetThr2"] = _AF6CNC0011_RD_OCN._tohglbrdidetthr_reg._TohRdiDetThr2()
            allFields["TohRdiDetThr1"] = _AF6CNC0011_RD_OCN._tohglbrdidetthr_reg._TohRdiDetThr1()
            return allFields

    class _tohglbaisdetthr_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Global AIS_L Detecting Threshold Control"
    
        def description(self):
            return "Configure thresholds for detecting AIS_L at TOH monitoring. There are two thresholds for each kind of threshold."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00024004
            
        def endAddress(self):
            return 0xffffffff

        class _TohAisDetThr2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TohAisDetThr2"
            
            def description(self):
                return "The second threshold for detecting AIS_L."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TohAisDetThr1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TohAisDetThr1"
            
            def description(self):
                return "The first threshold for detecting AIS_L."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TohAisDetThr2"] = _AF6CNC0011_RD_OCN._tohglbaisdetthr_reg._TohAisDetThr2()
            allFields["TohAisDetThr1"] = _AF6CNC0011_RD_OCN._tohglbaisdetthr_reg._TohAisDetThr1()
            return allFields

    class _tohglbk1smpthr_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Global K1 Sampling Threshold Control"
    
        def description(self):
            return "Configure thresholds for sampling K1 bytes to detect APS defect at TOH monitoring. There are two thresholds."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00024005
            
        def endAddress(self):
            return 0xffffffff

        class _TohK1SmpThr2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TohK1SmpThr2"
            
            def description(self):
                return "The second threshold for sampling K1 to detect APS defect."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TohK1SmpThr1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TohK1SmpThr1"
            
            def description(self):
                return "The first threshold for sampling K1 to detect APS defect."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TohK1SmpThr2"] = _AF6CNC0011_RD_OCN._tohglbk1smpthr_reg._TohK1SmpThr2()
            allFields["TohK1SmpThr1"] = _AF6CNC0011_RD_OCN._tohglbk1smpthr_reg._TohK1SmpThr1()
            return allFields

    class _tohglberrcntmod_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Global Error Counter Control"
    
        def description(self):
            return "Configure mode for counters in TOH monitoring."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00024006
            
        def endAddress(self):
            return 0xffffffff

        class _TohReiErrCntMod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "TohReiErrCntMod"
            
            def description(self):
                return "REI counter Mode. 1: Saturation mode 0: Roll over mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TohB2ErrCntMod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "TohB2ErrCntMod"
            
            def description(self):
                return "B2 counter Mode. 1: Saturation mode 0: Roll over mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TohB1ErrCntMod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TohB1ErrCntMod"
            
            def description(self):
                return "B1 counter Mode. 1: Saturation mode 0: Roll over mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TohReiErrCntMod"] = _AF6CNC0011_RD_OCN._tohglberrcntmod_reg._TohReiErrCntMod()
            allFields["TohB2ErrCntMod"] = _AF6CNC0011_RD_OCN._tohglberrcntmod_reg._TohB2ErrCntMod()
            allFields["TohB1ErrCntMod"] = _AF6CNC0011_RD_OCN._tohglberrcntmod_reg._TohB1ErrCntMod()
            return allFields

    class _tohglbaffen_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Montoring Affect Control"
    
        def description(self):
            return "Configure affective mode for TOH monitoring."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00024007
            
        def endAddress(self):
            return 0xffffffff

        class _TohAisAffStbMon(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "TohAisAffStbMon"
            
            def description(self):
                return "AIS affects to Stable monitoring status of the line at which LOF or LOS is detected 1: Disable 0: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TohAisAffRdilMon(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "TohAisAffRdilMon"
            
            def description(self):
                return "AIS affects to RDI-L monitoring status of the line at which LOF or LOS is detected. 1: Disable 0: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TohAisAffAislMon(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "TohAisAffAislMon"
            
            def description(self):
                return "AIS affects to AIS-L monitoring  status of the line at which LOF or LOS is detected. 1: Disable 0: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TohAisAffErrCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TohAisAffErrCnt"
            
            def description(self):
                return "AIS affects to error counters (B1,B2,REI) of the line at which LOF or LOS is detected. 1: Disable 0: Enable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TohAisAffStbMon"] = _AF6CNC0011_RD_OCN._tohglbaffen_reg._TohAisAffStbMon()
            allFields["TohAisAffRdilMon"] = _AF6CNC0011_RD_OCN._tohglbaffen_reg._TohAisAffRdilMon()
            allFields["TohAisAffAislMon"] = _AF6CNC0011_RD_OCN._tohglbaffen_reg._TohAisAffAislMon()
            allFields["TohAisAffErrCnt"] = _AF6CNC0011_RD_OCN._tohglbaffen_reg._TohAisAffErrCnt()
            return allFields

    class _tohramctl(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Monitoring Per Channel Control"
    
        def description(self):
            return "Each register is used to configure for TOH monitoring engine of the related line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x24100  + Ec1Id"
            
        def startAddress(self):
            return 0x00024100
            
        def endAddress(self):
            return 0x0002412f

        class _B1ErrCntBlkMod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "B1ErrCntBlkMod"
            
            def description(self):
                return "B1 counter Block Mode for fedding PM. 1: Block count mode 0: Bit count mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _B2ErrCntBlkMod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "B2ErrCntBlkMod"
            
            def description(self):
                return "B2 counter Block Mode for fedding PM. 1: Block count mode 0: Bit count mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ReiErrCntBlkMod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "ReiErrCntBlkMod"
            
            def description(self):
                return "REI counter Block Mode for fedding PM. 1: Block count mode 0: Bit count mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StbRdiisLThresSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "StbRdiisLThresSel"
            
            def description(self):
                return "Select the thresholds for detecting RDI_L 1: Threshold 2 is selected 0: Threshold 1 is selected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StbAisLThresSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "StbAisLThresSel"
            
            def description(self):
                return "Select the thresholds for detecting AIS_L 1: Threshold 2 is selected 0: Threshold 1 is selected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _K2StbMd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "K2StbMd"
            
            def description(self):
                return "Select K2[7:3] or K2[7:0] to detect validated K2 value 1: K2[7:0] value is selected 0: K2[7:3] value is selected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StbK1K2ThresSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "StbK1K2ThresSel"
            
            def description(self):
                return "Select the thresholds for detecting stable or non-stable K1/K2 status 1: Threshold 2 is selected 0: Threshold 1 is selected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StbS1ThresSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "StbS1ThresSel"
            
            def description(self):
                return "Select the thresholds for detecting stable or non-stable S1 status 1: Threshold 2 is selected 0: Threshold 1 is selected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SmpK1ThresSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SmpK1ThresSel"
            
            def description(self):
                return "Select the sample threshold for detecting APS defect. 1: Threshold 2 is selected 0: Threshold 1 is selected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["B1ErrCntBlkMod"] = _AF6CNC0011_RD_OCN._tohramctl._B1ErrCntBlkMod()
            allFields["B2ErrCntBlkMod"] = _AF6CNC0011_RD_OCN._tohramctl._B2ErrCntBlkMod()
            allFields["ReiErrCntBlkMod"] = _AF6CNC0011_RD_OCN._tohramctl._ReiErrCntBlkMod()
            allFields["StbRdiisLThresSel"] = _AF6CNC0011_RD_OCN._tohramctl._StbRdiisLThresSel()
            allFields["StbAisLThresSel"] = _AF6CNC0011_RD_OCN._tohramctl._StbAisLThresSel()
            allFields["K2StbMd"] = _AF6CNC0011_RD_OCN._tohramctl._K2StbMd()
            allFields["StbK1K2ThresSel"] = _AF6CNC0011_RD_OCN._tohramctl._StbK1K2ThresSel()
            allFields["StbS1ThresSel"] = _AF6CNC0011_RD_OCN._tohramctl._StbS1ThresSel()
            allFields["SmpK1ThresSel"] = _AF6CNC0011_RD_OCN._tohramctl._SmpK1ThresSel()
            return allFields

    class _tohb1errrocnt(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Monitoring B1 Error Read Only Counter"
    
        def description(self):
            return "Each register is used to store B1 error read only counter of the related line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x24800 + Ec1Id"
            
        def startAddress(self):
            return 0x00024800
            
        def endAddress(self):
            return 0x0002482f

        class _B1ErrRoCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 0
        
            def name(self):
                return "B1ErrRoCnt"
            
            def description(self):
                return "B1 Error Read Only Counter."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["B1ErrRoCnt"] = _AF6CNC0011_RD_OCN._tohb1errrocnt._B1ErrRoCnt()
            return allFields

    class _tohb1errr2ccnt(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Monitoring B1 Error Read to Clear Counter"
    
        def description(self):
            return "Each register is used to store B1 error read to clear counter of the related line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x24a00 + Ec1Id"
            
        def startAddress(self):
            return 0x00024a00
            
        def endAddress(self):
            return 0x00024a2f

        class _B1ErrR2cCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 0
        
            def name(self):
                return "B1ErrR2cCnt"
            
            def description(self):
                return "B1 Error Read To Clear Counter."
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["B1ErrR2cCnt"] = _AF6CNC0011_RD_OCN._tohb1errr2ccnt._B1ErrR2cCnt()
            return allFields

    class _tohb1blkerrrocnt(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Monitoring B1 Block Error Read Only Counter"
    
        def description(self):
            return "Each register is used to store B1 Block error read only counter of the related line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x24c00 + Ec1Id"
            
        def startAddress(self):
            return 0x00024c00
            
        def endAddress(self):
            return 0x00024c2f

        class _B1BlkErrRoCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "B1BlkErrRoCnt"
            
            def description(self):
                return "B1 Block Error Read Only Counter."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["B1BlkErrRoCnt"] = _AF6CNC0011_RD_OCN._tohb1blkerrrocnt._B1BlkErrRoCnt()
            return allFields

    class _tohb1blkerrr2ccnt(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Monitoring B1 Block Error Read to Clear Counter"
    
        def description(self):
            return "Each register is used to store B1 Block error read to clear counter of the related line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x24e00 + Ec1Id"
            
        def startAddress(self):
            return 0x00024e00
            
        def endAddress(self):
            return 0x00024e2f

        class _B1BlkErrR2cCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "B1BlkErrR2cCnt"
            
            def description(self):
                return "B1 Block Error Read To Clear Counter."
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["B1BlkErrR2cCnt"] = _AF6CNC0011_RD_OCN._tohb1blkerrr2ccnt._B1BlkErrR2cCnt()
            return allFields

    class _tohb2errrocnt(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Monitoring B2 Error Read Only Counter"
    
        def description(self):
            return "Each register is used to store B2 error read only counter of the related line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x24840 + Ec1Id"
            
        def startAddress(self):
            return 0x00024840
            
        def endAddress(self):
            return 0x0002486f

        class _B2ErrRoCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 0
        
            def name(self):
                return "B2ErrRoCnt"
            
            def description(self):
                return "B2 Error Read Only Counter."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["B2ErrRoCnt"] = _AF6CNC0011_RD_OCN._tohb2errrocnt._B2ErrRoCnt()
            return allFields

    class _tohb2errr2ccnt(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Monitoring B2 Error Read to Clear Counter"
    
        def description(self):
            return "Each register is used to store B2 error read to clear counter of the related line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x24a40 + Ec1Id"
            
        def startAddress(self):
            return 0x00024a40
            
        def endAddress(self):
            return 0x00024a6f

        class _B2ErrR2cCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 0
        
            def name(self):
                return "B2ErrR2cCnt"
            
            def description(self):
                return "B2 Error Read To Clear Counter."
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["B2ErrR2cCnt"] = _AF6CNC0011_RD_OCN._tohb2errr2ccnt._B2ErrR2cCnt()
            return allFields

    class _tohb2blkerrrocnt(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Monitoring B2 Block Error Read Only Counter"
    
        def description(self):
            return "Each register is used to store B2 Block error read only counter of the related line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x24c40 + Ec1Id"
            
        def startAddress(self):
            return 0x00024c40
            
        def endAddress(self):
            return 0x00024c6f

        class _B2BlkErrRoCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "B2BlkErrRoCnt"
            
            def description(self):
                return "B2 Block Error Read Only Counter."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["B2BlkErrRoCnt"] = _AF6CNC0011_RD_OCN._tohb2blkerrrocnt._B2BlkErrRoCnt()
            return allFields

    class _tohb2blkerrr2ccnt(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Monitoring B2 Block Error Read to Clear Counter"
    
        def description(self):
            return "Each register is used to store B2 Block error read to clear counter of the related line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x24e40 + Ec1Id"
            
        def startAddress(self):
            return 0x00024e40
            
        def endAddress(self):
            return 0x00024e6f

        class _B2BlkErrR2cCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "B2BlkErrR2cCnt"
            
            def description(self):
                return "B2 Block Error Read To Clear Counter."
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["B2BlkErrR2cCnt"] = _AF6CNC0011_RD_OCN._tohb2blkerrr2ccnt._B2BlkErrR2cCnt()
            return allFields

    class _tohreierrrocnt(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Monitoring REI Error Read Only Counter"
    
        def description(self):
            return "Each register is used to store REI error read only counter of the related line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x24940 + Ec1Id"
            
        def startAddress(self):
            return 0x00024940
            
        def endAddress(self):
            return 0x0002496f

        class _ReiErrRoCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReiErrRoCnt"
            
            def description(self):
                return "REI Error Read Only Counter."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReiErrRoCnt"] = _AF6CNC0011_RD_OCN._tohreierrrocnt._ReiErrRoCnt()
            return allFields

    class _tohreierrr2ccnt(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Monitoring REI Error Read to Clear Counter"
    
        def description(self):
            return "Each register is used to store REI error read to clear counter of the related line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x24b40 + Ec1Id"
            
        def startAddress(self):
            return 0x00024b40
            
        def endAddress(self):
            return 0x00024b6f

        class _ReiErrR2cCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReiErrR2cCnt"
            
            def description(self):
                return "REI Error Read To Clear Counter."
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReiErrR2cCnt"] = _AF6CNC0011_RD_OCN._tohreierrr2ccnt._ReiErrR2cCnt()
            return allFields

    class _tohreiblkerrrocnt(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Monitoring REI Block Error Read Only Counter"
    
        def description(self):
            return "Each register is used to store REI Block error read only counter of the related line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x24d40 + Ec1Id"
            
        def startAddress(self):
            return 0x00024d40
            
        def endAddress(self):
            return 0x00024d6f

        class _ReiBlkErrRoCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReiBlkErrRoCnt"
            
            def description(self):
                return "REI Block Error Read Only Counter."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReiBlkErrRoCnt"] = _AF6CNC0011_RD_OCN._tohreiblkerrrocnt._ReiBlkErrRoCnt()
            return allFields

    class _tohreiblkerrr2ccnt(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH Monitoring REI Block Error Read to Clear Counter"
    
        def description(self):
            return "Each register is used to store REI Block error read to clear counter of the related line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x24f40 + Ec1Id"
            
        def startAddress(self):
            return 0x00024f40
            
        def endAddress(self):
            return 0x00024f6f

        class _ReiBlkErrR2cCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReiBlkErrR2cCnt"
            
            def description(self):
                return "REI Block Error Read To Clear Counter."
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReiBlkErrR2cCnt"] = _AF6CNC0011_RD_OCN._tohreiblkerrr2ccnt._ReiBlkErrR2cCnt()
            return allFields

    class _tohk1monsta(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH K1 Monitoring Status"
    
        def description(self):
            return "Each register is used to store K1 Monitoring Status of the related line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x24880 + Ec1Id"
            
        def startAddress(self):
            return 0x00024880
            
        def endAddress(self):
            return 0x000248af

        class _CurApsDef(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "CurApsDef"
            
            def description(self):
                return "current APS Defect. 1: APS defect is detected from APS bytes. 0: APS defect is not detected from APS bytes."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _K1SmpCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 19
        
            def name(self):
                return "K1SmpCnt"
            
            def description(self):
                return "Sampling counter."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SameK1Cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 16
        
            def name(self):
                return "SameK1Cnt"
            
            def description(self):
                return "The number of same contiguous K1 bytes. It is held at StbK1Thr value when the number of same contiguous K1 bytes is equal to or more than the StbK1Thr value.In this case, K1 bytes are stable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _K1StbVal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "K1StbVal"
            
            def description(self):
                return "Stable K1 value. It is updated when detecting a new stable value."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _K1CurVal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "K1CurVal"
            
            def description(self):
                return "Current K1 byte."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CurApsDef"] = _AF6CNC0011_RD_OCN._tohk1monsta._CurApsDef()
            allFields["K1SmpCnt"] = _AF6CNC0011_RD_OCN._tohk1monsta._K1SmpCnt()
            allFields["SameK1Cnt"] = _AF6CNC0011_RD_OCN._tohk1monsta._SameK1Cnt()
            allFields["K1StbVal"] = _AF6CNC0011_RD_OCN._tohk1monsta._K1StbVal()
            allFields["K1CurVal"] = _AF6CNC0011_RD_OCN._tohk1monsta._K1CurVal()
            return allFields

    class _tohk2monsta(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH K2 Monitoring Status"
    
        def description(self):
            return "Each register is used to store K2 Monitoring Status of the related line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x248c0 + Ec1Id"
            
        def startAddress(self):
            return 0x000248c0
            
        def endAddress(self):
            return 0x000248ef

        class _Internal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 21
        
            def name(self):
                return "Internal"
            
            def description(self):
                return "Internal."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CurAisL(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "CurAisL"
            
            def description(self):
                return "current AIS-L Defect. 1: AIS-L defect is detected from K2 bytes. 0: AIS-L defect is not detected from K2 bytes."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CurRdiL(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "CurRdiL"
            
            def description(self):
                return "current RDI-L Defect. 1: RDI-L defect is detected from K2 bytes. 0: RDI-L defect is not detected from K2 bytes."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SameK2Cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 16
        
            def name(self):
                return "SameK2Cnt"
            
            def description(self):
                return "The number of same contiguous K2 bytes. It is held at StbK2Thr value when the number of same contiguous K2 bytes is equal to or more than the StbK2Thr value.In this case, K2 bytes are stable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _K2StbVal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "K2StbVal"
            
            def description(self):
                return "Stable K2 value. It is updated when detecting a new stable value."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _K2CurVal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "K2CurVal"
            
            def description(self):
                return "Current K2 byte."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Internal"] = _AF6CNC0011_RD_OCN._tohk2monsta._Internal()
            allFields["CurAisL"] = _AF6CNC0011_RD_OCN._tohk2monsta._CurAisL()
            allFields["CurRdiL"] = _AF6CNC0011_RD_OCN._tohk2monsta._CurRdiL()
            allFields["SameK2Cnt"] = _AF6CNC0011_RD_OCN._tohk2monsta._SameK2Cnt()
            allFields["K2StbVal"] = _AF6CNC0011_RD_OCN._tohk2monsta._K2StbVal()
            allFields["K2CurVal"] = _AF6CNC0011_RD_OCN._tohk2monsta._K2CurVal()
            return allFields

    class _tohs1monsta(AtRegister.AtRegister):
        def name(self):
            return "OCN TOH S1 Monitoring Status"
    
        def description(self):
            return "Each register is used to store S1 Monitoring Status of the related line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x24900 + Ec1Id"
            
        def startAddress(self):
            return 0x00024900
            
        def endAddress(self):
            return 0x0002492f

        class _SameS1Cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "SameS1Cnt"
            
            def description(self):
                return "The number of same contiguous S1 bytes. It is held at StbS1Thr value when the number of same contiguous S1 bytes is equal to or more than the StbS1Thr value.In this case, S1 bytes are stable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _S1StbVal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "S1StbVal"
            
            def description(self):
                return "Stable S1 value. It is updated when detecting a new stable value."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _S1CurVal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "S1CurVal"
            
            def description(self):
                return "Current S1 byte."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SameS1Cnt"] = _AF6CNC0011_RD_OCN._tohs1monsta._SameS1Cnt()
            allFields["S1StbVal"] = _AF6CNC0011_RD_OCN._tohs1monsta._S1StbVal()
            allFields["S1CurVal"] = _AF6CNC0011_RD_OCN._tohs1monsta._S1CurVal()
            return allFields

    class _tohintperalrenbctl(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx Line per Alarm Interrupt Enable Control"
    
        def description(self):
            return "This is the per Alarm interrupt enable of Rx framer and TOH monitoring. Each register is used to store 9 bits to enable interrupts when the related alarms in related line happen."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x24200 + Ec1Id"
            
        def startAddress(self):
            return 0x00024200
            
        def endAddress(self):
            return 0x0002422f

        class _TcaLStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "TcaLStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable TCA-L state change event in the related line to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SdLStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "SdLStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable SD-L state change event in the related line to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SfLStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "SfLStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable SF-L state change event in the related line to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TimLStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "TimLStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable TIM-L state change event in the related line to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _S1StbStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "S1StbStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable S1 Stable state change event in the related line to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _K1StbStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "K1StbStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable K1 Stable state change event in the related line to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ApsLStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "ApsLStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable APS-L Defect Stable state change event in the related line to generate an interrupt.."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _K2StbStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "K2StbStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable K2 Stable state change event in the related line to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RdiLStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RdiLStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable RDI-L Defect Stable state change event in the related line to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _AisLStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "AisLStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable AIS-L Defect Stable state change event in the related line to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OofStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "OofStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable OOF Defect Stable state change event in the related line to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LofStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "LofStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable LOF Defect Stable state change event in the related line to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LosStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LosStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable LOS Defect Stable state change event in the related line to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TcaLStateChgIntrEn"] = _AF6CNC0011_RD_OCN._tohintperalrenbctl._TcaLStateChgIntrEn()
            allFields["SdLStateChgIntrEn"] = _AF6CNC0011_RD_OCN._tohintperalrenbctl._SdLStateChgIntrEn()
            allFields["SfLStateChgIntrEn"] = _AF6CNC0011_RD_OCN._tohintperalrenbctl._SfLStateChgIntrEn()
            allFields["TimLStateChgIntrEn"] = _AF6CNC0011_RD_OCN._tohintperalrenbctl._TimLStateChgIntrEn()
            allFields["S1StbStateChgIntrEn"] = _AF6CNC0011_RD_OCN._tohintperalrenbctl._S1StbStateChgIntrEn()
            allFields["K1StbStateChgIntrEn"] = _AF6CNC0011_RD_OCN._tohintperalrenbctl._K1StbStateChgIntrEn()
            allFields["ApsLStateChgIntrEn"] = _AF6CNC0011_RD_OCN._tohintperalrenbctl._ApsLStateChgIntrEn()
            allFields["K2StbStateChgIntrEn"] = _AF6CNC0011_RD_OCN._tohintperalrenbctl._K2StbStateChgIntrEn()
            allFields["RdiLStateChgIntrEn"] = _AF6CNC0011_RD_OCN._tohintperalrenbctl._RdiLStateChgIntrEn()
            allFields["AisLStateChgIntrEn"] = _AF6CNC0011_RD_OCN._tohintperalrenbctl._AisLStateChgIntrEn()
            allFields["OofStateChgIntrEn"] = _AF6CNC0011_RD_OCN._tohintperalrenbctl._OofStateChgIntrEn()
            allFields["LofStateChgIntrEn"] = _AF6CNC0011_RD_OCN._tohintperalrenbctl._LofStateChgIntrEn()
            allFields["LosStateChgIntrEn"] = _AF6CNC0011_RD_OCN._tohintperalrenbctl._LosStateChgIntrEn()
            return allFields

    class _tohintsta(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx Line per Alarm Interrupt Status"
    
        def description(self):
            return "This is the per Alarm interrupt status of Rx framer and TOH monitoring. Each register is used to store 9 sticky bits for 9 alarms in the line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x24240 + Ec1Id"
            
        def startAddress(self):
            return 0x00024240
            
        def endAddress(self):
            return 0x0002426f

        class _TcaLStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "TcaLStateChgIntr"
            
            def description(self):
                return "Set 1 while TCA-L state change detected in the related line, and it is generated an interrupt if it is enabled."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _SdLStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "SdLStateChgIntr"
            
            def description(self):
                return "Set 1 while SD-L state change detected in the related line, and it is generated an interrupt if it is enabled."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _SfLStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "SfLStateChgIntr"
            
            def description(self):
                return "Set 1 while SF-L state change detected in the related line, and it is generated an interrupt if it is enabled."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _TimLStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "TimLStateChgIntr"
            
            def description(self):
                return "Set 1 while Tim-L state change detected in the related line, and it is generated an interrupt if it is enabled."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _S1StbStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "S1StbStateChgIntr"
            
            def description(self):
                return "Set 1 one new stable S1 value detected in the related line, and it is generated an interrupt if it is enabled."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _K1StbStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "K1StbStateChgIntr"
            
            def description(self):
                return "Set 1 one new stable K1 value detected in the related line, and it is generated an interrupt if it is enabled."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _ApsLStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "ApsLStateChgIntr"
            
            def description(self):
                return "Set 1 while APS-L Defect state change event happens in the related line, and it is generated an interrupt if it is enabled."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _K2StbStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "K2StbStateChgIntr"
            
            def description(self):
                return "Set 1 one new stable K2 value detected in the related line, and it is generated an interrupt if it is enabled."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _RdiLStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RdiLStateChgIntr"
            
            def description(self):
                return "Set 1 while RDI-L Defect state change event happens in the related line, and it is generated an interrupt if it is enabled."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _AisLStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "AisLStateChgIntr"
            
            def description(self):
                return "Set 1 while AIS-L Defect state change event happens in the related line, and it is generated an interrupt if it is enabled."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _OofStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "OofStateChgIntr"
            
            def description(self):
                return "Set 1 while OOF state change event happens in the related line, and it is generated an interrupt if it is enabled."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _LofStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "LofStateChgIntr"
            
            def description(self):
                return "Set 1 while LOF state change event happens in the related line, and it is generated an interrupt if it is enabled."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _LosStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LosStateChgIntr"
            
            def description(self):
                return "Set 1 while LOS state change event happens in the related line, and it is generated an interrupt if it is enabled."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TcaLStateChgIntr"] = _AF6CNC0011_RD_OCN._tohintsta._TcaLStateChgIntr()
            allFields["SdLStateChgIntr"] = _AF6CNC0011_RD_OCN._tohintsta._SdLStateChgIntr()
            allFields["SfLStateChgIntr"] = _AF6CNC0011_RD_OCN._tohintsta._SfLStateChgIntr()
            allFields["TimLStateChgIntr"] = _AF6CNC0011_RD_OCN._tohintsta._TimLStateChgIntr()
            allFields["S1StbStateChgIntr"] = _AF6CNC0011_RD_OCN._tohintsta._S1StbStateChgIntr()
            allFields["K1StbStateChgIntr"] = _AF6CNC0011_RD_OCN._tohintsta._K1StbStateChgIntr()
            allFields["ApsLStateChgIntr"] = _AF6CNC0011_RD_OCN._tohintsta._ApsLStateChgIntr()
            allFields["K2StbStateChgIntr"] = _AF6CNC0011_RD_OCN._tohintsta._K2StbStateChgIntr()
            allFields["RdiLStateChgIntr"] = _AF6CNC0011_RD_OCN._tohintsta._RdiLStateChgIntr()
            allFields["AisLStateChgIntr"] = _AF6CNC0011_RD_OCN._tohintsta._AisLStateChgIntr()
            allFields["OofStateChgIntr"] = _AF6CNC0011_RD_OCN._tohintsta._OofStateChgIntr()
            allFields["LofStateChgIntr"] = _AF6CNC0011_RD_OCN._tohintsta._LofStateChgIntr()
            allFields["LosStateChgIntr"] = _AF6CNC0011_RD_OCN._tohintsta._LosStateChgIntr()
            return allFields

    class _tohcursta(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx Line per Alarm Current Status"
    
        def description(self):
            return "This is the per Alarm interrupt status of Rx framer and TOH monitoring. Each register is used to store 9 sticky bits for 9 alarms in the line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x24280 + Ec1Id"
            
        def startAddress(self):
            return 0x00024280
            
        def endAddress(self):
            return 0x000242af

        class _TcaLDefCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "TcaLDefCurStatus"
            
            def description(self):
                return "TCA-L Defect  in the related line. 1: TCA-L defect is set 0: TCA-L defect is cleared."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SdLDefCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "SdLDefCurStatus"
            
            def description(self):
                return "SD-L Defect  in the related line. 1: SD-L defect is set 0: SD-L defect is cleared."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SfLDefCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "SfLDefCurStatus"
            
            def description(self):
                return "SF-L Defect  in the related line. 1: SF-L defect is set 0: SF-L defect is cleared."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TimLDefCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "TimLDefCurStatus"
            
            def description(self):
                return "TIM-L Defect  in the related line. 1: TIM-L defect is set 0: TIM-L defect is cleared."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _S1StbStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "S1StbStateChgIntr"
            
            def description(self):
                return "S1 stable status  in the related line."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _K1StbCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "K1StbCurStatus"
            
            def description(self):
                return "K1 stable status  in the related line."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ApsLCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "ApsLCurStatus"
            
            def description(self):
                return "APS-L Defect  in the related line. 1: APS-L defect is set 0: APS-L defect is cleared."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _K2StbCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "K2StbCurStatus"
            
            def description(self):
                return "K2 stable status  in the related line."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RdiLDefCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RdiLDefCurStatus"
            
            def description(self):
                return "RDI-L Defect  in the related line. 1: RDI-L defect is set 0: RDI-L defect is cleared."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _AisLDefCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "AisLDefCurStatus"
            
            def description(self):
                return "AIS-L Defect  in the related line. 1: AIS-L defect is set 0: AIS-L defect is cleared."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OofCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "OofCurStatus"
            
            def description(self):
                return "OOF current status in the related line. 1: OOF state 0: Not OOF state."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LofCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "LofCurStatus"
            
            def description(self):
                return "LOF current status in the related line. 1: LOF state 0: Not LOF state."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LosCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LosCurStatus"
            
            def description(self):
                return "LOS current status in the related line. 1: LOS state 0: Not LOS state."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TcaLDefCurStatus"] = _AF6CNC0011_RD_OCN._tohcursta._TcaLDefCurStatus()
            allFields["SdLDefCurStatus"] = _AF6CNC0011_RD_OCN._tohcursta._SdLDefCurStatus()
            allFields["SfLDefCurStatus"] = _AF6CNC0011_RD_OCN._tohcursta._SfLDefCurStatus()
            allFields["TimLDefCurStatus"] = _AF6CNC0011_RD_OCN._tohcursta._TimLDefCurStatus()
            allFields["S1StbStateChgIntr"] = _AF6CNC0011_RD_OCN._tohcursta._S1StbStateChgIntr()
            allFields["K1StbCurStatus"] = _AF6CNC0011_RD_OCN._tohcursta._K1StbCurStatus()
            allFields["ApsLCurStatus"] = _AF6CNC0011_RD_OCN._tohcursta._ApsLCurStatus()
            allFields["K2StbCurStatus"] = _AF6CNC0011_RD_OCN._tohcursta._K2StbCurStatus()
            allFields["RdiLDefCurStatus"] = _AF6CNC0011_RD_OCN._tohcursta._RdiLDefCurStatus()
            allFields["AisLDefCurStatus"] = _AF6CNC0011_RD_OCN._tohcursta._AisLDefCurStatus()
            allFields["OofCurStatus"] = _AF6CNC0011_RD_OCN._tohcursta._OofCurStatus()
            allFields["LofCurStatus"] = _AF6CNC0011_RD_OCN._tohcursta._LofCurStatus()
            allFields["LosCurStatus"] = _AF6CNC0011_RD_OCN._tohcursta._LosCurStatus()
            return allFields

    class _tohintperlineenctl1(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx Line Per Line Interrupt Enable Control 1"
    
        def description(self):
            return "The register consists of 24 bits for 24 lines (EC1 Id: 0-23) at Rx side to enable interrupts when alarms of related lines happen. It is noted that this register is higher priority than the OCN Rx Line per Alarm Interrupt Enable Control register."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000242f0
            
        def endAddress(self):
            return 0xffffffff

        class _OCNRxLLineIntrEn1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "OCNRxLLineIntrEn1"
            
            def description(self):
                return "Bit #0 to enable for line #0."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["OCNRxLLineIntrEn1"] = _AF6CNC0011_RD_OCN._tohintperlineenctl1._OCNRxLLineIntrEn1()
            return allFields

    class _tohintperlineenctl2(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx Line Per Line Interrupt Enable Control 2"
    
        def description(self):
            return "The register consists of 24 bits for 24 lines (EC1 Id: 24-47) at Rx side to enable interrupts when alarms of related lines happen. It is noted that this register is higher priority than the OCN Rx Line per Alarm Interrupt Enable Control register."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000242f1
            
        def endAddress(self):
            return 0xffffffff

        class _OCNRxLLineIntrEn2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "OCNRxLLineIntrEn2"
            
            def description(self):
                return "Bit #0 to enable for line #24."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["OCNRxLLineIntrEn2"] = _AF6CNC0011_RD_OCN._tohintperlineenctl2._OCNRxLLineIntrEn2()
            return allFields

    class _tohintperline1(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx Line per Line Interrupt OR Status 1"
    
        def description(self):
            return "The register consists of 24 bits for 24 lines (EC1 Id: 0-23) at Rx side. Each bit is used to store Interrupt OR status of the related line. If there are any bits in this register and they are enabled to raise interrupt, the Rx line level interrupt bit is set."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000242f2
            
        def endAddress(self):
            return 0xffffffff

        class _OCNRxLLineIntr1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "OCNRxLLineIntr1"
            
            def description(self):
                return "Set to 1 to indicate that there is any interrupt status bit in the OCN Rx Line per Alarm Interrupt Status register of the related STS/VC to be set and they are enabled to raise interrupt. Bit 0 for line #0, respectively."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["OCNRxLLineIntr1"] = _AF6CNC0011_RD_OCN._tohintperline1._OCNRxLLineIntr1()
            return allFields

    class _tohintperline2(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx Line per Line Interrupt OR Status 2"
    
        def description(self):
            return "The register consists of 24 bits for 24 lines (EC1 Id: 24-47) at Rx side. Each bit is used to store Interrupt OR status of the related line. If there are any bits in this register and they are enabled to raise interrupt, the Rx line level interrupt bit is set."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000242f3
            
        def endAddress(self):
            return 0xffffffff

        class _OCNRxLLineIntr2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "OCNRxLLineIntr2"
            
            def description(self):
                return "Set to 1 to indicate that there is any interrupt status bit in the OCN Rx Line per Alarm Interrupt Status register of the related STS/VC to be set and they are enabled to raise interrupt. Bit 0 for line #24, respectively."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["OCNRxLLineIntr2"] = _AF6CNC0011_RD_OCN._tohintperline2._OCNRxLLineIntr2()
            return allFields

    class _tohintperslice(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx Line per Slice24 Interrupt OR Status"
    
        def description(self):
            return "The register consists of 2 bits for 2 slice at Rx side. Each bit is used to store Interrupt OR status of the related slice24."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000242f4
            
        def endAddress(self):
            return 0xffffffff

        class _OCNRxLSliceIntr2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "OCNRxLSliceIntr2"
            
            def description(self):
                return "Set to 1 to indicate that there is any interrupt status bit in the OCN Rx Line per Line Interrupt OR Status 2 register of the related Slice24 #1 to be set and they are enabled to raise interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNRxLSliceIntr1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "OCNRxLSliceIntr1"
            
            def description(self):
                return "Set to 1 to indicate that there is any interrupt status bit in the OCN Rx Line per Line Interrupt OR Status 1 register of the related Slice24 #1 to be set and they are enabled to raise interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["OCNRxLSliceIntr2"] = _AF6CNC0011_RD_OCN._tohintperslice._OCNRxLSliceIntr2()
            allFields["OCNRxLSliceIntr1"] = _AF6CNC0011_RD_OCN._tohintperslice._OCNRxLSliceIntr1()
            return allFields

    class _tfmramctl(AtRegister.AtRegister):
        def name(self):
            return "OCN Tx Framer Per Channel Control"
    
        def description(self):
            return "Each register is used to configure operation modes for Tx framer engine and APS pattern inserted into APS bytes (K1 and K2 bytes) of the SONET/SDH line being relative with the address of the address of the register."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x21000 + 1024*SliceId + StsId"
            
        def startAddress(self):
            return 0x00021000
            
        def endAddress(self):
            return 0x0002142f

        class _OCNTxS1Pat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "OCNTxS1Pat"
            
            def description(self):
                return "Pattern to insert into S1 byte."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNTxApsPat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 8
        
            def name(self):
                return "OCNTxApsPat"
            
            def description(self):
                return "Pattern to insert into APS bytes (K1, K2)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNTxS1LDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "OCNTxS1LDis"
            
            def description(self):
                return "S1 insertion disable 1: Disable 0: Enable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNTxApsDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "OCNTxApsDis"
            
            def description(self):
                return "Disable to process APS 1: Disable 0: Enable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNTxReiLDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "OCNTxReiLDis"
            
            def description(self):
                return "Auto REI_L insertion disable 1: Disable inserting REI_L automatically. 0: Enable automatically inserting REI_L."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNTxRdiLDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "OCNTxRdiLDis"
            
            def description(self):
                return "Auto RDI_L insertion disable 1: Disable inserting RDI_L automatically. 0: Enable automatically inserting RDI_L."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNTxAutoB2Dis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "OCNTxAutoB2Dis"
            
            def description(self):
                return "Auto B2 disable 1: Disable inserting calculated B2 values into B2 positions automatically. 0: Enable automatically inserting calculated B2 values into B2 positions."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNTxRdiLThresSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "OCNTxRdiLThresSel"
            
            def description(self):
                return "Select the number of frames being inserted RDI-L defects when receive direction requests generating RDI-L at transmit direction. 1: Threshold2 is selected. 0: Threshold1 is selected."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNTxRdiLFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "OCNTxRdiLFrc"
            
            def description(self):
                return "RDI-L force. 1: Force RDI-L defect into transmit data. 0: Not force RDI-L"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OCNTxAisLFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "OCNTxAisLFrc"
            
            def description(self):
                return "AIS-L force. 1: Force AIS-L defect into transmit data. 0: Not force AIS-L"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["OCNTxS1Pat"] = _AF6CNC0011_RD_OCN._tfmramctl._OCNTxS1Pat()
            allFields["OCNTxApsPat"] = _AF6CNC0011_RD_OCN._tfmramctl._OCNTxApsPat()
            allFields["OCNTxS1LDis"] = _AF6CNC0011_RD_OCN._tfmramctl._OCNTxS1LDis()
            allFields["OCNTxApsDis"] = _AF6CNC0011_RD_OCN._tfmramctl._OCNTxApsDis()
            allFields["OCNTxReiLDis"] = _AF6CNC0011_RD_OCN._tfmramctl._OCNTxReiLDis()
            allFields["OCNTxRdiLDis"] = _AF6CNC0011_RD_OCN._tfmramctl._OCNTxRdiLDis()
            allFields["OCNTxAutoB2Dis"] = _AF6CNC0011_RD_OCN._tfmramctl._OCNTxAutoB2Dis()
            allFields["OCNTxRdiLThresSel"] = _AF6CNC0011_RD_OCN._tfmramctl._OCNTxRdiLThresSel()
            allFields["OCNTxRdiLFrc"] = _AF6CNC0011_RD_OCN._tfmramctl._OCNTxRdiLFrc()
            allFields["OCNTxAisLFrc"] = _AF6CNC0011_RD_OCN._tfmramctl._OCNTxAisLFrc()
            return allFields

    class _tfmj0insctl(AtRegister.AtRegister):
        def name(self):
            return "OCN Tx J0 Insertion Buffer"
    
        def description(self):
            return "Each register is used to store one J0 double word of in 64-byte message of the SONET/SDH line being relative with the address of the address of the register inserted into J0 positions."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x21200 + 1024*SliceId + 16*StsId + DwId"
            
        def startAddress(self):
            return 0x00021200
            
        def endAddress(self):
            return 0x0002162f

        class _OCNTxJ0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "OCNTxJ0"
            
            def description(self):
                return "J0 pattern for insertion."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["OCNTxJ0"] = _AF6CNC0011_RD_OCN._tfmj0insctl._OCNTxJ0()
            return allFields

import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0011_RD_CLA(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["glb_psn"] = _AF6CNC0011_RD_CLA._glb_psn()
        allRegisters["per_pw_ctrl"] = _AF6CNC0011_RD_CLA._per_pw_ctrl()
        allRegisters["hbce_glb"] = _AF6CNC0011_RD_CLA._hbce_glb()
        allRegisters["hbce_hash_table"] = _AF6CNC0011_RD_CLA._hbce_hash_table()
        allRegisters["hbce_lkup_info"] = _AF6CNC0011_RD_CLA._hbce_lkup_info()
        allRegisters["cdr_pw_lkup_ctrl"] = _AF6CNC0011_RD_CLA._cdr_pw_lkup_ctrl()
        allRegisters["pw_mef_over_mpls_ctrl"] = _AF6CNC0011_RD_CLA._pw_mef_over_mpls_ctrl()
        allRegisters["cla_per_grp_enb"] = _AF6CNC0011_RD_CLA._cla_per_grp_enb()
        return allRegisters

    class _glb_psn(AtRegister.AtRegister):
        def name(self):
            return "Classify Global PSN Control"
    
        def description(self):
            return "This register controls operation modes of PSN interface."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _RxPsnVlanTagModeEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "RxPsnVlanTagModeEn"
            
            def description(self):
                return "1: Enable (default) VLAN TAG mode which contain PWID 0:  Disable VLAN TAG mode, normal PSN operation"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxSendLbit2CdrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "RxSendLbit2CdrEn"
            
            def description(self):
                return "Enable to send Lbit to CDR engine 1: Enable to send Lbit 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPsnMplsOutLabelCheckEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "RxPsnMplsOutLabelCheckEn"
            
            def description(self):
                return "Enable to check MPLS outer 1: Enable checking 0: Disable checking"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPsnCpuBfdCtlEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "RxPsnCpuBfdCtlEn"
            
            def description(self):
                return "Enable VCCV BFD control packet sending to CPU for processing 1: Enable sending 0: Discard"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxMacCheckDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "RxMacCheckDis"
            
            def description(self):
                return "Disable to check MAC address at Ethernet port receive direction 1: Disable checking 0: Enable checking"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPsnCpuIcmpEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "RxPsnCpuIcmpEn"
            
            def description(self):
                return "Enable ICMP control packet sending to CPU for processing 1: Enable sending 0: Discard"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPsnCpuArpEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "RxPsnCpuArpEn"
            
            def description(self):
                return "Enable ARP control packet sending to CPU for processing 1: Enable sending 0: Discard"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PweLoopClaEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "PweLoopClaEn"
            
            def description(self):
                return "Enable Loop back traffic from PW Encapsulation to Classification 1: Enable Loop back mode 0: Normal, not loop back"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPsnIpUdpMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "RxPsnIpUdpMode"
            
            def description(self):
                return "This bit is applicable for Ipv4/Ipv6 packet from Ethernet side 1: Classify engine uses RxPsnIpUdpSel to decide which UDP port (Source or Destination) is used to identify pseudowire packet 0: Classify engine will automatically search for value 0x85E in source or destination UDP port. The remaining UDP port is used to identify pseudowire packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPsnIpUdpSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "RxPsnIpUdpSel"
            
            def description(self):
                return "This bit is applicable for Ipv4/Ipv6 using to select Source or Destination to identify pseudowire packet from Ethernet side. It is not use when RxPsnIpUdpMode is zero 1: Classify engine selects source UDP port to identify pseudowire packet 0: Classify engine selects destination UDP port to identify pseudowire packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPsnIpTtlChkEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "RxPsnIpTtlChkEn"
            
            def description(self):
                return "Enable check TTL field in MPLS/Ipv4 or Hop Limit field in Ipv6 1: Enable checking 0: Disable checking"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPsnMplsOutLabel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPsnMplsOutLabel"
            
            def description(self):
                return "Received 2-label MPLS packet from PSN side will be discarded when it's outer label is different than RxPsnMplsOutLabel"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPsnVlanTagModeEn"] = _AF6CNC0011_RD_CLA._glb_psn._RxPsnVlanTagModeEn()
            allFields["RxSendLbit2CdrEn"] = _AF6CNC0011_RD_CLA._glb_psn._RxSendLbit2CdrEn()
            allFields["RxPsnMplsOutLabelCheckEn"] = _AF6CNC0011_RD_CLA._glb_psn._RxPsnMplsOutLabelCheckEn()
            allFields["RxPsnCpuBfdCtlEn"] = _AF6CNC0011_RD_CLA._glb_psn._RxPsnCpuBfdCtlEn()
            allFields["RxMacCheckDis"] = _AF6CNC0011_RD_CLA._glb_psn._RxMacCheckDis()
            allFields["RxPsnCpuIcmpEn"] = _AF6CNC0011_RD_CLA._glb_psn._RxPsnCpuIcmpEn()
            allFields["RxPsnCpuArpEn"] = _AF6CNC0011_RD_CLA._glb_psn._RxPsnCpuArpEn()
            allFields["PweLoopClaEn"] = _AF6CNC0011_RD_CLA._glb_psn._PweLoopClaEn()
            allFields["RxPsnIpUdpMode"] = _AF6CNC0011_RD_CLA._glb_psn._RxPsnIpUdpMode()
            allFields["RxPsnIpUdpSel"] = _AF6CNC0011_RD_CLA._glb_psn._RxPsnIpUdpSel()
            allFields["RxPsnIpTtlChkEn"] = _AF6CNC0011_RD_CLA._glb_psn._RxPsnIpTtlChkEn()
            allFields["RxPsnMplsOutLabel"] = _AF6CNC0011_RD_CLA._glb_psn._RxPsnMplsOutLabel()
            return allFields

    class _per_pw_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Classify Per Pseudowire Type Control"
    
        def description(self):
            return "This register configures identification types per pseudowire"
            
        def width(self):
            return 62
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0000B000 +  $PWID"
            
        def startAddress(self):
            return 0x0000b000
            
        def endAddress(self):
            return 0xffffffff

        class _RxEthPwOnflyVC34(AtRegister.AtRegisterField):
            def stopBit(self):
                return 61
                
            def startBit(self):
                return 61
        
            def name(self):
                return "RxEthPwOnflyVC34"
            
            def description(self):
                return "CEP EBM on-fly fractional, length changed"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthPwLen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 60
                
            def startBit(self):
                return 47
        
            def name(self):
                return "RxEthPwLen"
            
            def description(self):
                return "length of packet to check malform"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthCepMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 46
                
            def startBit(self):
                return 45
        
            def name(self):
                return "RxEthCepMode"
            
            def description(self):
                return "CEP mode working 0,1: CEP basic 2: VC3 fractional 3: VC4 fractional"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthPwEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 44
                
            def startBit(self):
                return 44
        
            def name(self):
                return "RxEthPwEn"
            
            def description(self):
                return "PW enable working 1: Enable PW 0: Disable PW"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthRtpSsrcValue(AtRegister.AtRegisterField):
            def stopBit(self):
                return 43
                
            def startBit(self):
                return 12
        
            def name(self):
                return "RxEthRtpSsrcValue"
            
            def description(self):
                return "This value is used to compare with SSRC value in RTP header of received TDM PW packets"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthRtpPtValue(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 5
        
            def name(self):
                return "RxEthRtpPtValue"
            
            def description(self):
                return "This value is used to compare with PT value in RTP header of received TDM PW packets"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthRtpEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RxEthRtpEn"
            
            def description(self):
                return "Enable RTP 1: Enable RTP 0: Disable RTP"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthRtpSsrcChkEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "RxEthRtpSsrcChkEn"
            
            def description(self):
                return "Enable checking SSRC field of RTP header in received TDM PW packet 1: Enable checking 0: Disable checking"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthRtpPtChkEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "RxEthRtpPtChkEn"
            
            def description(self):
                return "Enable checking PT field of RTP header in received TDM PW packet 1: Enable checking 0: Disable checking"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthPwType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthPwType"
            
            def description(self):
                return "this is PW type working 0: CES mode without CAS 1: CES mode with CAS 2: CEP mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthPwOnflyVC34"] = _AF6CNC0011_RD_CLA._per_pw_ctrl._RxEthPwOnflyVC34()
            allFields["RxEthPwLen"] = _AF6CNC0011_RD_CLA._per_pw_ctrl._RxEthPwLen()
            allFields["RxEthCepMode"] = _AF6CNC0011_RD_CLA._per_pw_ctrl._RxEthCepMode()
            allFields["RxEthPwEn"] = _AF6CNC0011_RD_CLA._per_pw_ctrl._RxEthPwEn()
            allFields["RxEthRtpSsrcValue"] = _AF6CNC0011_RD_CLA._per_pw_ctrl._RxEthRtpSsrcValue()
            allFields["RxEthRtpPtValue"] = _AF6CNC0011_RD_CLA._per_pw_ctrl._RxEthRtpPtValue()
            allFields["RxEthRtpEn"] = _AF6CNC0011_RD_CLA._per_pw_ctrl._RxEthRtpEn()
            allFields["RxEthRtpSsrcChkEn"] = _AF6CNC0011_RD_CLA._per_pw_ctrl._RxEthRtpSsrcChkEn()
            allFields["RxEthRtpPtChkEn"] = _AF6CNC0011_RD_CLA._per_pw_ctrl._RxEthRtpPtChkEn()
            allFields["RxEthPwType"] = _AF6CNC0011_RD_CLA._per_pw_ctrl._RxEthPwType()
            return allFields

    class _hbce_glb(AtRegister.AtRegister):
        def name(self):
            return "Classify HBCE Global Control"
    
        def description(self):
            return "This register is used to configure global for HBCE module"
            
        def width(self):
            return 20
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00008000
            
        def endAddress(self):
            return 0xffffffff

        class _CLAHbceTimeoutValue(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 4
        
            def name(self):
                return "CLAHbceTimeoutValue"
            
            def description(self):
                return "this is time out value for HBCE module, packing be discarded"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHbceHashingTableSelectedPage(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "CLAHbceHashingTableSelectedPage"
            
            def description(self):
                return "this is to select hashing table page to access looking up information buffer 1: PageID 1 0: PageID 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHbceCodingSelectedMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAHbceCodingSelectedMode"
            
            def description(self):
                return "selects mode to code origin id 4/5/6/7: code level 4/5/6/7 3: code level 3 (xor 4 subgroups origin id together) 2: code level 2 1: code level 1 0: none xor origin id"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAHbceTimeoutValue"] = _AF6CNC0011_RD_CLA._hbce_glb._CLAHbceTimeoutValue()
            allFields["CLAHbceHashingTableSelectedPage"] = _AF6CNC0011_RD_CLA._hbce_glb._CLAHbceHashingTableSelectedPage()
            allFields["CLAHbceCodingSelectedMode"] = _AF6CNC0011_RD_CLA._hbce_glb._CLAHbceCodingSelectedMode()
            return allFields

    class _hbce_hash_table(AtRegister.AtRegister):
        def name(self):
            return "Classify HBCE Hashing Table Control"
    
        def description(self):
            return "HBCE module uses 10 bits for tabindex. tabindex is generated by hasding function applied to the origin id. The collisions due to hashing are handled by pointer to variable size blocks. // // Handling variable  size blocks requires a dynamic memory management scheme. The number of entries in each variable size block is defined by the number of rules that collide within a specific entry of //the tabindex. Hashing function applies an XOR function to all bits of tabindex. The indexes to the tabindex are generated by hashing function and therefore collisions may occur. In order to resolve //these collisions efficiently, HBCE define a complex data structure associated with each entry of the orgin id. Hashing table has two fields, flow number (flownum) (number of labelid mapped to this //particular table entry) and memory start pointer (memsptr) (hold a pointer to the variable size block and the number of  rules that collide). In case, a table entry might be empty  which means that it //is not mapped to any flow address rule, flownum = 0. Moreover, a table entry may be mapped to many flow address rules. In this case, where collisions occur, hashing table have to store a pointer to  //the variable size block and the number of rules that collide. The number of colliding rules also indicates the size of the block. The formating of hashing table page0 in each case is shown as below.%% Hash ID:%% PATERN HASH %% Label ID (20bit)  +  Lookup mode (2bit) + port (1bit)%% With Lookup Mode: 0/1/2 ~ PSN MEF/UDP/MPLS%% + NOXOR       :  Hashid[10:0]  = Patern Hash [10:0], StoreID[11:0] = Patern Hash [22:11]%% + ONEXOR     :  Hashid[10:0]  = Patern Hash [10:0] XOR Patern Hash [21:11],%% StoreID = Patern Hash [22]%% + TWOXOR    :   Hashid  = Patern Hash [10:0] XOR Patern Hash [21:11] XOR {7'b0,Patern Hash[22]}%% StoreID = Patern Hash [22:11]"
            
        def width(self):
            return 17
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00009000 + $PageID*0x1000 + $HashID"
            
        def startAddress(self):
            return 0x00009000
            
        def endAddress(self):
            return 0x00009fff

        class _CLAHbceFlowNumber(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 13
        
            def name(self):
                return "CLAHbceFlowNumber"
            
            def description(self):
                return "this is to identify HashID is empty or win or collision 0: empty format 1: normal format others: collision format"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHbceMemoryStartPointer(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAHbceMemoryStartPointer"
            
            def description(self):
                return "this is a MemPtr to read the Classify HBCE Looking Up Information Control"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAHbceFlowNumber"] = _AF6CNC0011_RD_CLA._hbce_hash_table._CLAHbceFlowNumber()
            allFields["CLAHbceMemoryStartPointer"] = _AF6CNC0011_RD_CLA._hbce_hash_table._CLAHbceMemoryStartPointer()
            return allFields

    class _hbce_lkup_info(AtRegister.AtRegister):
        def name(self):
            return "Classify HBCE Looking Up Information Control"
    
        def description(self):
            return "In HBCE module, all operations access this memory in order to examine whether an exact match exists or not. The Memory Pool implements dynamic memory management scheme and supports //variable size blocks. It supports requests for allocation and deallocation of variable size blocks when inserting and deleting lookup address occur. Linking between multiple blocks is implemented by //writing the address of the  next  block  in  the  previous block. In general, HBCE implementation is based on sequential accesses to memory pool and to the dynamically allocated collision nodes. The //formating of memory pool word is shown as below.%% The format of memory pool word is shown as below. There are two case formats depend on field[23]"
            
        def width(self):
            return 37
        
        def type(self):
            return "Config #Normal format"
            
        def fomular(self):
            return "0x0000A000 +  $CellID"
            
        def startAddress(self):
            return 0x0000a000
            
        def endAddress(self):
            return 0xffffffff

        class _CLAHbceFlowDirect(AtRegister.AtRegisterField):
            def stopBit(self):
                return 35
                
            def startBit(self):
                return 35
        
            def name(self):
                return "CLAHbceFlowDirect"
            
            def description(self):
                return "this configure FlowID working in normal mode (not in any group)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHbceGrpWorking(AtRegister.AtRegisterField):
            def stopBit(self):
                return 34
                
            def startBit(self):
                return 34
        
            def name(self):
                return "CLAHbceGrpWorking"
            
            def description(self):
                return "this configure group working or protection"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHbceGrpIDFlow(AtRegister.AtRegisterField):
            def stopBit(self):
                return 33
                
            def startBit(self):
                return 24
        
            def name(self):
                return "CLAHbceGrpIDFlow"
            
            def description(self):
                return "this configure a group ID that FlowID following"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHbceMemoryStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "CLAHbceMemoryStatus"
            
            def description(self):
                return "This is zero for \"normal format\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHbceFlowID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 13
        
            def name(self):
                return "CLAHbceFlowID"
            
            def description(self):
                return "a number identifying the output port of HBCE module  this configure a group ID that FlowID following"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHbceFlowEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "CLAHbceFlowEnb"
            
            def description(self):
                return "The flow is identified in this table, it mean the traffic is identified 1: Flow identified 0: Flow look up fail"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHbceStoreID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAHbceStoreID"
            
            def description(self):
                return "this is saving some additional information to identify a certain lookup address rule within a particular table entry HBCE and also to be able to distinguish those that collide"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAHbceFlowDirect"] = _AF6CNC0011_RD_CLA._hbce_lkup_info._CLAHbceFlowDirect()
            allFields["CLAHbceGrpWorking"] = _AF6CNC0011_RD_CLA._hbce_lkup_info._CLAHbceGrpWorking()
            allFields["CLAHbceGrpIDFlow"] = _AF6CNC0011_RD_CLA._hbce_lkup_info._CLAHbceGrpIDFlow()
            allFields["CLAHbceMemoryStatus"] = _AF6CNC0011_RD_CLA._hbce_lkup_info._CLAHbceMemoryStatus()
            allFields["CLAHbceFlowID"] = _AF6CNC0011_RD_CLA._hbce_lkup_info._CLAHbceFlowID()
            allFields["CLAHbceFlowEnb"] = _AF6CNC0011_RD_CLA._hbce_lkup_info._CLAHbceFlowEnb()
            allFields["CLAHbceStoreID"] = _AF6CNC0011_RD_CLA._hbce_lkup_info._CLAHbceStoreID()
            return allFields

    class _cdr_pw_lkup_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR Pseudowire Look Up Control"
    
        def description(self):
            return "Used for TDM PW. This register is used to select PW for CDR function."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00003000 + PWID"
            
        def startAddress(self):
            return 0x00003000
            
        def endAddress(self):
            return 0x0000329f

        class _PwCdrSlice(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "PwCdrSlice"
            
            def description(self):
                return "0: Slice 0 corresponding to sts 0,2,4,...,46 1: Slice 1 corresponding to sts 1,3,5,...,47"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwCdrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "PwCdrDis"
            
            def description(self):
                return "applicable for pseudowire that is enabled CDR function (eg: ACR/DCR) 0: Enable CDR (default) 1: Disable CDR"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwCdrLineId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PwCdrLineId"
            
            def description(self):
                return "CDR line ID lookup from pseudowire Ids."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PwCdrSlice"] = _AF6CNC0011_RD_CLA._cdr_pw_lkup_ctrl._PwCdrSlice()
            allFields["PwCdrDis"] = _AF6CNC0011_RD_CLA._cdr_pw_lkup_ctrl._PwCdrDis()
            allFields["PwCdrLineId"] = _AF6CNC0011_RD_CLA._cdr_pw_lkup_ctrl._PwCdrLineId()
            return allFields

    class _pw_mef_over_mpls_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Classify Pseudowire MEF over MPLS Control"
    
        def description(self):
            return "Used for TDM PW. This register is used to configure MEF over MPLS"
            
        def width(self):
            return 51
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0000C000 + $PWID"
            
        def startAddress(self):
            return 0x0000c000
            
        def endAddress(self):
            return 0x0000c29f

        class _RxPwMEFoMPLSDaValue(AtRegister.AtRegisterField):
            def stopBit(self):
                return 50
                
            def startBit(self):
                return 3
        
            def name(self):
                return "RxPwMEFoMPLSDaValue"
            
            def description(self):
                return "DA value used to compare with DA in Ethernet MEF packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPwMEFoMPLSDaCheckEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "RxPwMEFoMPLSDaCheckEn"
            
            def description(self):
                return "1: Enable check  RxPwMEFoMPLSDaValue with DA in Ethernet MEF packet 0: Disable check  RxPwMEFoMPLSDaValue with DA in Ethernet MEF packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPwMEFoMPLSEcidCheckEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "RxPwMEFoMPLSEcidCheckEn"
            
            def description(self):
                return "1: Enable check  ECID in Ethernet MEF packet with MPLS PW label 0: Disable check  ECID in Ethernet MEF packet with MPLS PW label"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPwMEFoMPLSEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwMEFoMPLSEn"
            
            def description(self):
                return "1: Enable MEF over MPLS PW mode, in this case, after MPLS PW label is ETH MEF packet 0: Normal case, disable MEF over MPLS PW mode, in this case, after MPLS PW label is PW control word"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwMEFoMPLSDaValue"] = _AF6CNC0011_RD_CLA._pw_mef_over_mpls_ctrl._RxPwMEFoMPLSDaValue()
            allFields["RxPwMEFoMPLSDaCheckEn"] = _AF6CNC0011_RD_CLA._pw_mef_over_mpls_ctrl._RxPwMEFoMPLSDaCheckEn()
            allFields["RxPwMEFoMPLSEcidCheckEn"] = _AF6CNC0011_RD_CLA._pw_mef_over_mpls_ctrl._RxPwMEFoMPLSEcidCheckEn()
            allFields["RxPwMEFoMPLSEn"] = _AF6CNC0011_RD_CLA._pw_mef_over_mpls_ctrl._RxPwMEFoMPLSEn()
            return allFields

    class _cla_per_grp_enb(AtRegister.AtRegister):
        def name(self):
            return "Classify Per Group Enable Control"
    
        def description(self):
            return "This register configures Group that Flowid (or Pseudowire ID) is enable or not"
            
        def width(self):
            return 1
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0000D000 + Working_ID*0x400 + Grp_ID"
            
        def startAddress(self):
            return 0x0000d000
            
        def endAddress(self):
            return 0x0000d29f

        class _CLAGrpPWEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAGrpPWEn"
            
            def description(self):
                return "This indicate the FlowID (or Pseudowire ID) is enable or not 1: FlowID enable 0: FlowID disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAGrpPWEn"] = _AF6CNC0011_RD_CLA._cla_per_grp_enb._CLAGrpPWEn()
            return allFields
